package com.sok.runway.offline.transmissionManager;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletConfig;

import org.apache.commons.codec.binary.*;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.exporters.PublishConnectionImpl;
import com.sok.exporters.newhomesguide.NewHomesGuideConnection;
import com.sok.exporters.realestate.RealEstateFTP;
import com.sok.exporters.realestate.RealEstateMapper;
import com.sok.exporters.realestate.RealEstatePackager;
import com.sok.exporters.trademe.TrademeConnection;
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.MailerBean;
import com.sok.framework.RowBean;
import com.sok.framework.RunwayUtil;
import com.sok.framework.SpecManager;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.TableSpec;
import com.sok.framework.VelocityManager;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyEntity.ResizeOption;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;
import com.sok.runway.email.EmailerMap;
import com.sok.runway.houseandland.HouseAndLandAction;
import com.sok.runway.offline.AbstractDaemonWorker;
import com.sok.runway.offline.rpmManager.RPMtask;
import com.sok.runway.security.SecurityFactory;
import com.sok.runway.util.ImageResizeUtil;
import com.sok.service.crm.cms.properties.DripService;
import com.sok.service.crm.cms.properties.DripService.DripCollection;

public class Transmissions extends AbstractDaemonWorker
{   
	private static final String DEFAULT_VELOCITY_PATH = "/cms/emails/statusalerts/";
	String REIWA_DTD_STRING = "<!DOCTYPE REIWA_Import SYSTEM \"ReiwaAgencyFeed.dtd\">";
	private static final Logger logger = LoggerFactory.getLogger(Transmissions.class);

	protected String alerttext = "generic/generic_text.vt";
	protected String alerthtml = "generic/generic.vt";

	protected static final String contactsearchurl = "/crm/actions/nextcontact.jsp";
	protected static final String companysearchurl = "/crm/actions/nextcompanyfromsearch.jsp";

	protected String emailalertsurlhome;

	private Vector<QueueItem>		queue = new Vector<QueueItem>();

	private String realestateUser, realestatePassword, domainUser, domainPassword, myPackageUser, myPackagePassword, hnlUser, hnlPassword, homeSalesUser, homeSalesPassword;
	private String allHomesUser, allHomesPassword;
	private String reiwaUser, reiwaPassword;
	private String meHouseUser, meHousePassword;	
	private String seniorsHousingOnlineUser, seniorsHousingOnlinePassword;
	private String newHomesGuideUser, newHomesGuidePassword;
	private String trademeKey, trademeSecret, trademeToken, trademeTokenSecret;

	SimpleDateFormat					yyyymmdd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat					ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	SimpleDateFormat					yyyymmdd_2 = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
	SimpleDateFormat					yyyymmdd_date = new SimpleDateFormat("yyyy-MM-dd");
	
	private boolean				isRunning = false;
	private boolean				pretend = false;

		
	protected String	to = "";

	public Transmissions(String configfilepath)
	{     
		super(configfilepath);

		config = TransmissionsConfig.getTransmissionConfig(configfilepath);
		to = config.getDebugAddress();
		pretend = config.getPretend();
		
		readSystemParameters();
	}  
	
	protected void readSystemParameters() {
		Properties sysprops = InitServlet.getSystemParams();
		if(sysprops!=null)
		{
			emailalertsurlhome = sysprops.getProperty("EmailAlertsURLHome", urlhome);

			realestateUser = sysprops.getProperty("realestateUser");
			realestatePassword = sysprops.getProperty("realestatePassword");

			domainUser = sysprops.getProperty("domainUser");
			domainPassword = sysprops.getProperty("domainPassword");

			myPackageUser = sysprops.getProperty("mypackageUser");
			myPackagePassword = sysprops.getProperty("mypackagePassword"); 
			
			hnlUser = sysprops.getProperty("hnlUser");
			hnlPassword = sysprops.getProperty("hnlPassword");
			
			homeSalesUser = sysprops.getProperty("homesalesUser");
			homeSalesPassword = sysprops.getProperty("homesalesPassword");
			
			allHomesUser = sysprops.getProperty("allhomesUser");
			allHomesPassword = sysprops.getProperty("allhomesPassword");
			
			reiwaUser = sysprops.getProperty("reiwaUser");
			reiwaPassword = sysprops.getProperty("reiwaPassword");
			
			meHouseUser = sysprops.getProperty("mehouseUser");
			meHousePassword = sysprops.getProperty("mehousePassword");
			
			seniorsHousingOnlineUser = sysprops.getProperty("seniorshousingonlineUser");
			seniorsHousingOnlinePassword = sysprops.getProperty("seniorshousingonlinePassword");
			
			newHomesGuideUser = sysprops.getProperty("newHomesGuideUser");
			newHomesGuidePassword = sysprops.getProperty("newHomesGuidePassword");
			
			trademeKey = sysprops.getProperty("trademeKey");
			trademeSecret = sysprops.getProperty("trademeSecret");
			trademeToken = sysprops.getProperty("trademeToken");
			trademeTokenSecret = sysprops.getProperty("trademeTokenSecret");
			
			TrademeConnection.getInstance().setHost(PublishMethod.Trademe.getFeedHost());
		} 
		else
		{
			throw new RuntimeException("System parameters not available, halting transmissions");
		}
	}  

	protected void init()
	{
		if (!isRunning) {
			isRunning = true;
			message = new StringBuffer();
			queue = new Vector<QueueItem>();
			super.init();
			
			if("true".equals(InitServlet.getSystemParam("Publish-ReloadSystemParameters"))) {
				log("Reloading all params");
				readSystemParameters();
			}
			
			loadTranmissions();
			loadOutOfTranmissions();
			loadQueue();
			if (queue.size() > 0) {
				packageProducts();
				sendProducts();
			} else if (debug) {
				log("There was no packages to send");
			}
	
			if (super.message.length() > 0 && "true".equals(InitServlet.getSystemParam("Publish-DebugEnabled"))) {
				sendUserEmail(to);
			}
			isRunning = false;
		} else {
			log("Another instance seems to be running");
		}
	}
	
	public void debugInit(boolean debug) {
		super.debugInit(debug);
		if("true".equals(InitServlet.getSystemParam("Publish-ReloadSystemParameters"))) {
			log("Reloading all params");
			readSystemParameters();
		}
	}
	
	protected void loadQueue() {
		GenRow row = new GenRow();
		row.setTableSpec("ProductTransmissionQueue");
		row.setParameter("-select0", "*");
		row.doAction("search");
		
		row.getResults();
		
		while (row.getNext()) {
			queue.add(new QueueItem(row));
		}
		
		row.close();
	}

	protected void loadTranmissions() {
		loadTranmissions(null);
	}
	
	protected void loadOutOfTranmissions() {
		loadOutOfTranmissions(null);
	}
	
	protected void loadTranmissions(String transmissionID) {
		
		String now = RunwayUtil.getCurDateInServerTimeZone(ddmmyyyy);		
		
		log("Starting loadTranmissions()");
		GenRow search = new GenRow(); 
		search.setConnection(getConnection()); 
		search.setViewSpec("ProductTransmissionView");
		search.addParameter("EndDate|1", "NULL");
		search.addParameter("EndDate|1", "01/01/2000 00:00:00");
		//search.addParameter("EndDate|1", ">NOW");
		search.addParameter("EndDate|1", ">" + now);
		search.addParameter("StartDate|2", "NULL");
		//search.addParameter("StartDate|2", "<NOW");
		search.addParameter("StartDate|2", "<" + now);
		//search.addParameter("RefreshFeed|1","Y");
		//search.addParameter("RefreshFeed|2","Y");
		search.setParameter("Active","!N");
		search.setParameter("Name","!NULL;!EMPTY");

		if(StringUtils.isNotBlank(transmissionID)) {
			search.setParameter("TransmissionID", transmissionID);
		}
		
		search.doAction("search");

		log("loadTranmissions() SQL Search");
		log(search.getStatement());

		search.getResults();

		int added = 0;
		while (search.getNext()) {
			if(StringUtils.isNotBlank(search.getString("ProductID"))) {
				if ("0M1B447S3X8M3Y140M572R4M7Q6X".equals(search.getString("ProductID"))) {
					System.out.println("Here");
				}
				this.productMessage = new StringBuffer();
				
				boolean forceRemove = false;
				boolean markForDel = "Y".equals(search.getString("MarkForDelete"));
				
				if(!productExists(search.getString("ProductID"))) {
					markForDel = true;
					forceRemove = true;
					log("Product {} does not exist. Setting MarkForDelete,forceRemove = Y.", search.getString("ProductID") );
				}
				
				if (search.getString("TransmissionSuccess").length() == 0) {
					QueueItem q = new QueueItem(search.getData("TransmissionID"), search.getString("ProductID"), search.getString("ProductPublishingID"), 
							search.getData("Name"), search.getString("ProductName"), getRepUserID(search.getString("ProductID")),
							search.getData("StartDate"), search.getData("EndDate"), search.getData("File"), 
							"Y".equals(search.getString("PublishedFlag")), markForDel, forceRemove, search.getData("ListingID"));
					if (q.save()) {
						++added;
						log("added for transmission : {} to queue. RF = {}; TS = {}", search.getString("ProductID"),  search.getString("RefreshFeed"), String.valueOf(search.getString("TransmissionSuccess").length()));
						//log("added for transmission : " + search.getString("ProductID") + " to queue. RF = " + search.getString("RefreshFeed") + "; TS = " +  search.getString("TransmissionSuccess").length());
						insertLog(search.getData("TransmissionID"), productMessage, "Pick", true);
					} else {
						log("failed addin transmission : {} to queue. P = {}", search.getString("TransmissionID"), search.getString("ProductID"));
					}
				} else if ("01/01/2000 00:00:00".equals(search.getString("TransmissionDate")) || "2000-01-01 00:00:00".equals(search.getString("TransmissionDate")) 
						|| productModified(search.getString("ProductID"), search.getData("Name"), search.getString("TransmissionDate"))) {
					
					QueueItem q = new QueueItem(search.getData("TransmissionID"), search.getString("ProductID"), search.getString("ProductPublishingID"), 
							search.getData("Name"), search.getString("ProductName"), getRepUserID(search.getString("ProductID")), 
							search.getData("StartDate"), search.getData("EndDate"), search.getData("File"), 
							"Y".equals(search.getString("PublishedFlag")), markForDel, forceRemove, search.getData("ListingID"));
					if (q.save()) {
						++added;
						//log("added for transmission : " + search.getString("ProductID"));
						log("added for transmission : {}", search.getString("TransmissionID"));
						insertLog(search.getData("TransmissionID"), productMessage, "Pick", true);
					} else {
						log("failed adding transmission : {} to queue. P = {}", search.getString("TransmissionID"), search.getString("ProductID"));
					}
				}
			} else {
				log("Found a record with blank ProductID : t = {}", search.getData("TransmissionID"));
			}
		}

		log("added {}", String.valueOf(added));

		search.close();

	}

	protected void loadOutOfTranmissions(String transmissionID) {
		String now = RunwayUtil.getCurDateInServerTimeZone(ddmmyyyy);
		
		GenRow search = new GenRow(); 
		search.setConnection(getConnection()); 
		search.setViewSpec("ProductTransmissionView");
		//search.addParameter("EndDate|1", "<NOW");
		search.addParameter("EndDate|1", "<" + now);
		search.addParameter("StartDate|2", "NULL");
		//search.addParameter("StartDate|2", "<NOW");
		search.addParameter("StartDate|2", "<" + now);
		search.setParameter("Active|3","!N");
		search.setParameter("Active^1|3","N");
		search.setParameter("EndDate^1|3","LAST3DAYS+NULL");
		search.setParameter("TransmissionDate^1","<TODAY");
		search.setParameter("Name","!NULL;!EMPTY");

		if(StringUtils.isNotBlank(transmissionID)) {
			search.setParameter("TransmissionID", transmissionID);
		}
		
		search.doAction("search");
		
		log("loadOutOfTranmissions() SQL Search");
		log(search.getStatement());

		search.getResults();
		int added = 0;
		while (search.getNext()) {
			if(StringUtils.isNotBlank(search.getString("ProductID"))) {
				if ("0M1B447S3X8M3Y140M572R4M7Q6X".equals(search.getString("ProductID"))) {
					System.out.println("Here");
				}
				QueueItem q = new QueueItem(search.getData("TransmissionID"), search.getString("ProductID"), search.getString("ProductPublishingID"), 
						search.getData("Name"), search.getString("ProductName"), getRepUserID(search.getString("ProductID")), 
						search.getData("StartDate"), search.getData("EndDate"), search.getData("File"), 
						"Y".equals(search.getString("PublishedFlag")), true, true /* mark for delete */, search.getData("ListingID"));
	
				q.save();
				
				++added;
				
				log("added for out of transmission : t = {} p = {}", search.getData("TransmissionID"), search.getString("ProductID"));
			} else {
				log("Found a record with blank ProductID : t = {}", search.getData("TransmissionID"));
			}
		}
		log("added " + added);

		search.close();

	}
	
	/** Product must exist in Quick index as well if it is H&L
	 * @param productID
	 * @return
	 */
	private boolean productExists(String productID) {
		GenRow products = new GenRow();
		products.setConnection(getConnection()); 
		products.setTableSpec("Products");
		products.setParameter("-select0", "ProductType");
		products.setColumn("ProductID", productID);		
		products.doAction(GenerationKeys.SELECTFIRST);
		
		if(products.isSuccessful() && isHouseAndLand(products)) {
			GenRow qq = new GenRow();
			qq.setConnection(getConnection()); 
			qq.setTableSpec("properties/ProductQuickIndex");
			qq.setParameter("-select0", "QuickIndexID");
			qq.setColumn("ProductID", productID);		
			qq.doAction(GenerationKeys.SELECTFIRST);
			log("H&L product, exists in QI = " + qq.isSuccessful() + qq.getStatement());
			return qq.isSuccessful();
		} else {
			return products.isSuccessful();
		}
	}

	private boolean productModified(String productID, String feedName, String date ) {
		PublishMethod pm;
		
		Date now = RunwayUtil.getCurDateInServerTimeZone();		
		log("Current server time is = " + now);
		now.setTime(now.getTime() - (10 * 60 * 1000));

		try {
			pm = PublishMethod.getFromFeedName(feedName);
		} catch (IllegalArgumentException e) {
			log(productID + " has no Provider for " + feedName);
			return false;
		}

		// has the product been linked since the last change transmission?
		if (date != null && date.indexOf(".") > 0) date = date.substring(0,date.indexOf("."));
		GenRow products = new GenRow();
		products.setConnection(getConnection()); 
		products.setViewSpec("ProductView");
		products.setColumn("ProductID", productID);
		
		products.doAction(GenerationKeys.SELECTFIRST);
		
		if (!products.isSuccessful() || products.getString("ProductType").length() == 0) {
			log("product missing. Last transmission date = " + date + "; ProductID = " + productID);
			return true;
		}
		
		try {
			products.setParameter("ModifiedDate|d^1", ">" + ddmmyyyy.format(yyyymmdd.parse(date)));
			products.setParameter("DripModifiedDate|d^1", ">" + ddmmyyyy.format(yyyymmdd.parse(date)));
		} catch (ParseException e) {
			products.setParameter("ModifiedDate|d^1", ">" + date);
			products.setParameter("DripModifiedDate|d^1", ">" + date);
		}
		products.addParameter("ModifiedDate^1", "<" + ddmmyyyy.format(now));
		products.doAction(GenerationKeys.SELECTFIRST);

		log("Product " + products.getStatement());

		if (products.isSuccessful() && StringUtils.isNotBlank(products.getData("ProductID"))) {
			log("product modified. Last transmission date = " + date + "; ProductID = " + productID + "; Product modified date = " + products.getData("ModifiedDate"));
			return true;
		}
		
		// we should not check sub products if the product is a House and Land as we need a recreate to get the modified date.
		if (!"House and Land".equals(products.getString("ProductType"))) {
			// has any sub product been linked since the last change transmission?
			products.clear();
			products.setViewSpec("ProductLinkedProductView");
			products.setColumn("ProductID", productID);
			try {
				products.setParameter("ProductProductsLinkedProduct.ModifiedDate|d^1", ">" + ddmmyyyy.format(yyyymmdd.parse(date)));
				products.setParameter("ProductProductsLinkedProduct.DripModifiedDate|d^1", ">" + ddmmyyyy.format(yyyymmdd.parse(date)));
			} catch (ParseException e) {
				products.setParameter("ProductProductsLinkedProduct.ModifiedDate|d^1", ">" + date);
				products.setParameter("ProductProductsLinkedProduct.DripModifiedDate|d^1", ">" + date);
			}
			products.addParameter("ProductProductsLinkedProduct.ModifiedDate^1", "<" + ddmmyyyy.format(now));
			products.doAction(GenerationKeys.SELECTFIRST);
	
			if (products.isSuccessful() && StringUtils.isNotBlank(products.getData("ProductID"))) {
				log("H&L product modified. Last transmission date = " + date + "; ProductID = " + productID + "; Product modified date = " + products.getData("ModifiedDate"));
				return true;
			}
		}
		
		GenRow productPublishing = new GenRow();
		productPublishing.setConnection(getConnection()); 
		productPublishing.setViewSpec("properties/ProductPublishingView");
		productPublishing.setColumn("ProductID", productID);
		productPublishing.setColumn("PublishMethod" , pm.getName() );
		try {
			productPublishing.setParameter("ModifiedDate^1", ">" + ddmmyyyy.format(yyyymmdd.parse(date)));
		} catch (ParseException e) {
			productPublishing.setParameter("ModifiedDate^1", ">" + date);
		}
		productPublishing.addParameter("ModifiedDate^1", "<" + ddmmyyyy.format(now));
		productPublishing.doAction(GenerationKeys.SELECTFIRST);

		//System.out.println("Linked Product " + products.getStatement());
		log(" ");
		log("ProductPublishing modified query: [   " + productPublishing.getStatement() + "  ]");

		if(StringUtils.isNotBlank(productPublishing.getData("ModifiedDate"))) {
			log("productPublishing modified on " + productPublishing.getData("ModifiedDate"));
		}
		
		boolean isProductModified = ((products.isSuccessful() && products.getString("LinkedProductID").length() > 0) || (productPublishing.isSuccessful() && productPublishing.getString("ProductPublishingID").length() > 0));
		products.close();
		productPublishing.close();
		
		log("isProductModified = " + isProductModified);
		return isProductModified; 
	}

	public static boolean isConfigured(PublishMethod pm) {
		logger.trace("Checking if isConfigured for {}" , pm);
		switch(pm) {
		case REA:
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("realestateUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("realestatePassword")));
		case Domain:
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("domainUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("domainPassword")));
		case MyPackage:
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("mypackageUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("mypackagePassword")));
		case Hnl:
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("hnlUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("hnlPassword")));
		case HomeSales:
			logger.trace("HomeSales user [{}], password [{}]", InitServlet.getSystemParam("homesalesUser"), InitServlet.getSystemParam("homesalesPassword"));
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("homesalesUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("homesalesPassword")));
		case Trademe:
			logger.trace("Trade user [{}], password [{}]", InitServlet.getSystemParam("trademeKey"), InitServlet.getSystemParam("trademeSecret"));
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("trademeKey")) && StringUtils.isNotBlank(InitServlet.getSystemParam("trademeSecret")) && StringUtils.isNotBlank(InitServlet.getSystemParam("trademeToken")) && StringUtils.isNotBlank(InitServlet.getSystemParam("trademeTokenSecret")));
		case AllHomes:
			logger.trace("AllHomes user [{}], password [{}]", InitServlet.getSystemParam("allhomesUser"), InitServlet.getSystemParam("allhomesPassword"));
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("allhomesUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("allhomesPassword")));
		case Reiwa:
			logger.trace("Reiwa user [{}], password [{}]", InitServlet.getSystemParam("reiwaUser"), InitServlet.getSystemParam("reiwaPassword"));
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("reiwaUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("reiwaPassword")));
		case MeHouse:
			logger.trace("MeHouse user [{}], password [{}]", InitServlet.getSystemParam("mehouseUser"), InitServlet.getSystemParam("mehousePassword"));
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("mehouseUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("mehousePassword")));
		case SeniorsHousingOnline:
			logger.trace("SeniorsHousingOnline user [{}], password [{}]", InitServlet.getSystemParam("SeniorsHousingOnlineUser"), InitServlet.getSystemParam("seniorshousingonlinePassword"));
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("seniorshousingonlineUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("seniorshousingonlinePassword")));
		case NewHomesGuide:
			logger.trace("NewHomesGuide user [{}], password [{}]", InitServlet.getSystemParam("newHomesGuideUser"), InitServlet.getSystemParam("newHomesGuidePassword"));
			return (StringUtils.isNotBlank(InitServlet.getSystemParam("newHomesGuideUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("newHomesGuidePassword")));
		}
		return false;

	}

	private boolean hasCredentials(PublishMethod pm) {
		switch(pm) {
		case REA:
			return (StringUtils.isNotBlank(realestateUser) && StringUtils.isNotBlank(realestatePassword));
		case Domain:
			return (StringUtils.isNotBlank(domainUser) && StringUtils.isNotBlank(domainPassword));
		case MyPackage:
			return (StringUtils.isNotBlank(myPackageUser) && StringUtils.isNotBlank(myPackagePassword));
		case Hnl:
			return (StringUtils.isNotBlank(hnlUser) && StringUtils.isNotBlank(hnlPassword));
		case HomeSales:
			return (StringUtils.isNotBlank(homeSalesUser) && StringUtils.isNotBlank(homeSalesPassword));
		case Trademe:
			return (StringUtils.isNotBlank(trademeKey) && StringUtils.isNotBlank(trademeSecret) && StringUtils.isNotBlank(trademeToken) && StringUtils.isNotBlank(trademeTokenSecret));
		case AllHomes:
			return (StringUtils.isNotBlank(allHomesUser) && StringUtils.isNotBlank(allHomesPassword));
		case Reiwa:
			return (StringUtils.isNotBlank(reiwaUser) && StringUtils.isNotBlank(reiwaPassword));
		case MeHouse:
			return (StringUtils.isNotBlank(meHouseUser) && StringUtils.isNotBlank(meHousePassword));
		case SeniorsHousingOnline:
			return (StringUtils.isNotBlank(seniorsHousingOnlineUser) && StringUtils.isNotBlank(seniorsHousingOnlinePassword));
		case NewHomesGuide:
			return (StringUtils.isNotBlank(newHomesGuideUser) && StringUtils.isNotBlank(newHomesGuidePassword));
		}
		return false;
	}


	/**
	 * Provide a public method to forcibly do a transmission so that they can be tested manually.
	 * Force: if true will make sure that the product is also available for Transmision
	 * @param transmissionID
	 */
	public void debugTransmission(String transmissionID, String force) {
		debugTransmission(transmissionID, force, "false");
	}
	
	public void debugTransmission(String transmissionID, String force, String removeOnly) {
		this.queue.clear();
		
		/*
		GenRow search = new GenRow(); 
		search.setConnection(getConnection()); 
		search.setViewSpec("ProductTransmissionView");
		search.setParameter("TransmissionID", transmissionID);
		/*
		search.addParameter("EndDate|1", "NULL");
		search.addParameter("EndDate|1", "01/01/2000 00:00:00");
		search.addParameter("EndDate|1", ">NOW");
		search.addParameter("StartDate|2", "NULL");
		search.addParameter("StartDate|2", "<NOW");
		//search.addParameter("RefreshFeed|1","Y");
		//search.addParameter("RefreshFeed|2","Y");
		search.setParameter("Active","!N");
		search.setParameter("Name","!NULL;!EMPTY");
		 */
		/*
		if(logger.isTraceEnabled()) { 
			search.doAction("search");
			logger.trace(search.getStatement());
		} 
		if(search.isSet("TransmissionID")) search.doAction(GenerationKeys.SELECTFIRST);
		if (search.isSuccessful()) {
			this.queue.clear();

			QueueItem debugItem = new QueueItem(search.getString("TransmissionID"), search.getString("ProductID"), search.getString("ProductPublishingID"), 
					search.getData("Name"), search.getString("ProductName"), getRepUserID(search.getString("ProductID")), 
					search.getData("StartDate"), search.getData("EndDate"), search.getData("File"), 
					"Y".equals(search.getString("PublishedFlag")), "Y".equals(search.getString("MarkForDelete")));
			this.queue.add(debugItem);
			this.processQueueItem(debugItem);
		} else {
			logger.debug("unable to find transmission with id {} ", transmissionID);
		}

		sendProducts();
		*/
		if("true".equals(force)){
			loadTranmissions(transmissionID);
			loadOutOfTranmissions(transmissionID);
		} else {			
			GenRow search = new GenRow(); 
			search.setConnection(getConnection()); 
			search.setViewSpec("ProductTransmissionView");
			search.setParameter("TransmissionID", transmissionID);
			/*
			search.addParameter("EndDate|1", "NULL");
			search.addParameter("EndDate|1", "01/01/2000 00:00:00");
			search.addParameter("EndDate|1", ">NOW");
			search.addParameter("StartDate|2", "NULL");
			search.addParameter("StartDate|2", "<NOW");
			//search.addParameter("RefreshFeed|1","Y");
			//search.addParameter("RefreshFeed|2","Y");
			search.setParameter("Active","!N");
			search.setParameter("Name","!NULL;!EMPTY");
			 */
			
			if(logger.isTraceEnabled()) { 
				search.doAction("search");
				logger.trace(search.getStatement());
			} 
			if(search.isSet("TransmissionID")) search.doAction(GenerationKeys.SELECTFIRST);
			if (search.isSuccessful()) {
				this.queue.clear();

				QueueItem debugItem = new QueueItem(search.getString("TransmissionID"), search.getString("ProductID"), search.getString("ProductPublishingID"), 
						search.getData("Name"), search.getString("ProductName"), getRepUserID(search.getString("ProductID")), 
						search.getData("StartDate"), search.getData("EndDate"), search.getData("File"), 
						"Y".equals(search.getString("PublishedFlag")), "Y".equals(search.getString("MarkForDelete")), "true".equalsIgnoreCase(removeOnly), search.getData("ListingID"));				
				debugItem.save();
			} else {
				logger.debug("unable to find transmission with id {} ", transmissionID);
			}
			search.close();
		}
		
		loadQueue();
		
		if (queue.size() > 0) {
			packageProducts();
			sendProducts();
		} else if (debug) {
			log("There was no packages to send");
		}
	}

	private void packageProducts() {
		for(QueueItem qi: queue) {	//uses iterator 
			processQueueItem(qi);
			qi.delete();
		}
	}

	/**
	 * Split out process Item to enable simpler debugging
	 * @param queueItem
	 */
	private void processQueueItem(QueueItem queueItem) { 
		//clear the log
		this.productMessage = new StringBuffer(); 
				
		final String productID = queueItem.productID;
		RealEstatePackager reP = null;
		//unused variable 18/10/2012 mikey
		//String feedName = queueItem.name;

		GenRow product = getProduct(productID);
		log("Product data as modified at {}", product.getString("ModifiedDate"));	
		
		PublishMethod pm = null;
		try {
			pm = PublishMethod.getFromFeedName(queueItem.provider);
		} catch (IllegalArgumentException e) {
			log(queueItem.transmissionID + " has no Provider for " + queueItem.provider);
			return;
		}

		if(!hasCredentials(pm)) {
			throw new RuntimeException("no credentials found for provider " + pm.name());
		}
		
		boolean expireListing = false;
		if (!"Y".equals(product.getString("Active")) || queueItem.markForDelete) {
			expireListing = true;
		}
		//unused variable 18/10/2012 mikey
		//String folder = new StringBuilder().append(configfilepath).append("/specs/feeds/").append(pm.getSpecFolder()).append("/").toString();

		//String filename = StringUtil.join(folder, product.getString("GroupID"),"-", product.getString("ProductType").replaceAll(" ",""),".xml"); 
		File f = null;
		//if (!(f = new File(filename)).canRead()) filename = StringUtil.join(folder,product.getString("ProductType").replaceAll(" ",""),".xml" );
		//if (!(f = new File(filename)).canRead()) filename = folder + "default.xml";
		//if (!(f = new File(filename)).canRead()) throw new RuntimeException("default feed not found for pm " + pm.toString());

		String specFileName = (pm == PublishMethod.Trademe && expireListing) ? StringUtil.join("/","Withdraw.xml" )
								: StringUtil.join("/",product.getString("ProductType").replaceAll(" ",""),".xml" );

		if(StringUtils.isBlank(specFileName)) specFileName = "/default.xml";

		String filename = configfilepath + "/specific/specs/feeds/" + pm.getSpecFolder()	+ specFileName;
		if (!(f = new File(filename)).canRead()) filename = configfilepath + "/specs/feeds/" + pm.getSpecFolder() + specFileName;
		if (!(f = new File(filename)).canRead()) filename = configfilepath + "/specs/feeds/"	+ pm.getSpecFolder() + "/default.xml";
		if (!(f = new File(filename)).canRead()) {
			log("No XML file found " + filename);
			updateValidationLog(productID, pm.toString(), "No XML file found " + filename);
			return;
		}
		
		log("\n\nProcessing for [ProductID: {} - TxmID: {} - PM: {}] :-", queueItem.productID, queueItem.transmissionID, pm.name());
		log("Using File {}",filename);
		
		String branchID = "";
		String agentID = getPublishAgentID(getConnection(), product, pm);
		log("AgentID is set to : {}", agentID);
		
		if (pm == PublishMethod.Reiwa && agentID.contains("-")) {
			// Reiwa needs to have AgentID-BranchID

			String a[] = agentID.split("-");
			agentID = a[0];
			logger.debug("Changed PublishAgentID {}", agentID);
			if (a.length > 1) {
				branchID = a[1];
				logger.debug("{} is {}", pm.getBranchIDKey(), agentID);
			}

		}
		
		// we must upload the images as part of a zip file for domain.

		//if ("RealEstate.com.au".equals(queueItem.provider)) {
		// we need a file for each group, if there isn't one then lets use a default one
		//	String filename = configfilepath + "/specific/realestate/realestate-" + product.getString("GroupID") + "-" + product.getString("ProductType").replaceAll(" ","") + ".xml";
		//	if (!(new File(filename)).canRead()) filename = configfilepath + "/specific/realestate/realestate-" + product.getString("ProductType").replaceAll(" ","") + ".xml";
		//	if (!(new File(filename)).canRead()) mv  = configfilepath + "/specific/realestate/realestate.xml";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String uploadname = pm == PublishMethod.REA ?  StringUtil.join(configfilepath,"/specific/realestate/uploads/",realestateUser,"_",productID,"_",sdf.format(new Date()),".xml")
				: pm == PublishMethod.Domain ? StringUtil.join(configfilepath,"/specific/domain/uploads/",domainUser,"_",productID,"_",sdf.format(new Date()),".xml")
						: pm == PublishMethod.MyPackage ? StringUtil.join(configfilepath,"/specific/mypackage/uploads/",myPackageUser,"_",productID,"_",sdf.format(new Date()),".xml") 
						: pm == PublishMethod.Hnl ? StringUtil.join(configfilepath,"/specific/hnl/uploads/",hnlUser,"_",productID,"_",sdf.format(new Date()),".xml")
						: pm == PublishMethod.HomeSales ? StringUtil.join(configfilepath,"/specific/homesales/uploads/",homeSalesUser,"_",productID,"_",sdf.format(new Date()),".xml")
						: pm == PublishMethod.AllHomes ? StringUtil.join(configfilepath,"/specific/allhomes/uploads/",allHomesUser,"_",productID,"_",sdf.format(new Date()),".xml")
						: pm == PublishMethod.Reiwa ? StringUtil.join(configfilepath,"/specific/reiwa/uploads/",agentID,"_",branchID ,"_",sdf.format(new Date()),".xml")
						: pm == PublishMethod.MeHouse ? StringUtil.join(configfilepath,"/specific/mehouse/uploads/",meHouseUser,"_",productID,"_",sdf.format(new Date()),".xml")
						: pm == PublishMethod.SeniorsHousingOnline ? StringUtil.join(configfilepath,"/specific/seniorshousingonline/uploads/",seniorsHousingOnlineUser,"_",productID,"_",sdf.format(new Date()),".xml")
						: pm == PublishMethod.NewHomesGuide ? StringUtil.join(configfilepath,"/specific/newhomesguide/uploads/", productID,"_",agentID,"-",sdf.format(new Date()),".xml")
						: pm == PublishMethod.Trademe && expireListing ? StringUtil.join(configfilepath, "/specific/trademe/uploads/", "switched_", productID, "_withdraw_", sdf.format(new Date()), ".xml")
						: pm == PublishMethod.Trademe ? StringUtil.join(configfilepath, "/specific/trademe/uploads/", "switched_", productID, "_", sdf.format(new Date()), ".xml") /* not yet implemented */ : null;

						Map<String, String> imageMap = pm == PublishMethod.Domain ? new HashMap<String, String>() : null;

						String errors = "";
						boolean isValid = true;
						try {
							reP = new RealEstatePackager(new RealEstateMapper(f),uploadname);
							
							//create directories if not there
							File file = new File(uploadname); file.getParentFile().mkdirs();

							final String host = urlhome, hostS = host + "/";
							log("Feed host is {}", pm.getFeedHost());								

							String status = "current";

							if (pm == PublishMethod.Domain) {									
								status = "Insert";
								log("Status for domain is {}", status);

								reP.setParameter("IsNew", queueItem.isNew ? "True" : "False");
							}
							
							if (pm == PublishMethod.Reiwa) {
								status = "Active";
								log("Status for Reiwa is {}", status);
							}

							if (expireListing) {
								
								status = getInactiveStatus(pm);
								queueItem.markForDelete = true;
								
								if(queueItem.markForDelete)
									log("Product is markedForDelete, status {}", status);
								else 
									log("Updating the product to markForDelete as it is inactive, status {}", status);
							}

							setRepDetails(reP, productID , queueItem.markForDelete, getConnection());
							reP.setParameter("ProductID",productID);
							reP.setParameter("Host",host);
							reP.setParameter("Status", status);
							reP.setParameter("DATE",pm == PublishMethod.REA? yyyymmdd_2.format(new Date()) : yyyymmdd.format(new Date()));
							reP.setParameter("DATE2",pm == PublishMethod.REA? yyyymmdd_2.format(new Date()) : yyyymmdd_date.format(new Date()));
							reP.setParameter("DATETIME2", yyyymmdd_2.format(new Date()));
							/* unused variable 18/10/2012 mikey
								String pIDs = product.getData("ProductID");
								if ("House and Land".equals(product.getString("ProductType"))) {
									pIDs += "+" + product.getData("HomeProductID") + "+" + product.getData("PlanID")  + "+" + product.getData("LotID") + "+" + product.getData("CopyPlanID");
								} else if ("Land".equals(product.getString("ProductType"))) {
									pIDs += "+" + product.getData("StageProductID") + "+" + product.getData("EstateProductID");
								}

							 */
							/*
					GenRow imgs = new GenRow();
					imgs.setViewSpec("LinkedDocumentImageView");
					imgs.setConnection(getConnection());
					imgs.setColumn("ProductID",pIDs);
					imgs.setColumn("LinkedDocumentDocument.DocumentType","Image");
					imgs.setColumn("ShowOnREmain","Y");
					imgs.setColumn("FilePath", "!NULL;!EMPTY");
					imgs.doAction("selectfirst");
					if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) reP.setParameter("ImageM", hostS + imgs.getData("FilePath"));

					imgs = new GenRow();
					imgs.setViewSpec("LinkedDocumentImageView");
					imgs.setConnection(getConnection());
					imgs.setColumn("ProductID",pIDs);
					imgs.setColumn("LinkedDocumentDocument.DocumentType","Image");
					imgs.setColumn("LinkedDocumentDocument.DocumentSubType","!Floorplan");
					imgs.setColumn("ShowOnREorder", ">0");
					imgs.setColumn("FilePath", "!NULL;!EMPTY");
					imgs.sortBy("ShowOnREorder",0);
					imgs.sortBy("SortOrder",1);
					imgs.doAction(ActionBean.SEARCH);

					imgs.getResults();

					int i = 1;
					while (imgs.getNext() && i < 10) {
						if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) {
							reP.setParameter("Image" + i, hostS + imgs.getData("FilePath"));
							++i;
						}
					}

					imgs.setColumn("LinkedDocumentDocument.DocumentSubType","Floorplan");
					imgs.setColumn("ShowOnREorder", ">0");
					imgs.setColumn("FilePath", "!NULL;!EMPTY");
					imgs.sortBy("ShowOnREorder",0);
					imgs.sortBy("SortOrder",1);
					imgs.doAction(ActionBean.SEARCH);

					imgs.getResults();

					i = 1;
					while (imgs.getNext() && i <= 5) {
						if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) {
							reP.setParameter("FloorPlan" + i, hostS + imgs.getString("FilePath"));
							++i;
						}
					}

					imgs.close();
							 */

							GenRow pp = new GenRow();
							pp.setConnection(getConnection());
							pp.setTableSpec("properties/ProductPublishing");
							pp.setParameter("ProductID", product.getData("ProductID"));
							pp.setParameter("PublishMethod", pm.getName());
							pp.setParameter("-select0", "*");
							pp.doAction("selectfirst");

							reP.setParameter("Headline", pp.getData("Summary"));

							if("Existing Property".equals(product.getData("ProductType"))){					
								if("Home".equals(product.getData("ProductSubType")))
									reP.setParameter("ProductSubType", "House");
								else
									reP.setParameter("ProductSubType", product.getData("ProductSubType"));
								
							} else if("House and Land".equals(product.getData("ProductType"))) {
								reP.setParameter("ParentRegion", getProductRegion(product));
								
								doFloorPlanCheck(product.getData("PlanImage"));
								
								reP.setParameter("Headline", HouseAndLandAction.evaluateHeadline(getConnection(), product.getData("ProductID"),pp.getData("Summary")));
								reP.setParameter("PublishingDescription", HouseAndLandAction.evaluateHeadline(getConnection(), product.getData("ProductID"),pp.getData("Description")));
								reP.setParameter("PublishDescription", HouseAndLandAction.evaluateHeadline(getConnection(), product.getData("ProductID"),pp.getData("Description"))); // Just have both versions
								
								// PDF URL
								GenRow rep = new GenRow();
								rep.setViewSpec("ProductSecurityGroupView"); 
								rep.setConnection(getConnection()); 
								rep.setParameter("ProductID", product.getData("ProductID")); 
								rep.setParameter("RepUserID", "!EMPTY");
								if(rep.isSet("ProductID")) rep.doAction(GenerationKeys.SELECTFIRST);
								reP.setParameter("PDFBrochureURL", String.format("%1$s/property.pdf?-api=true&ProductID=%2$s&v=%3$d&LocationID=%4$s&RepUserID=%5$s", InitServlet.getSystemParam("URLHome"), productID, new Double(Math.random()*100000).intValue(), product.getData("LocationID"), rep.getData("RepUserID")));
								
								// PDF uploaded at plan level
								GenRow brochureDoc = new GenRow();
								brochureDoc.setConnection(getConnection());
								brochureDoc.setViewSpec("LinkedDocumentImageView");
								brochureDoc.setColumn("ProductID", product.getData("ProductID"));
								brochureDoc.setColumn("LinkedDocumentDocument.DocumentSubType","Brochure");
								brochureDoc.doAction("selectfirst");
								logger.trace("Brochure Doc Query {}" , brochureDoc.getStatement());
								if(StringUtils.isNotBlank(brochureDoc.getData("FilePath"))) {
									logger.trace("Brochure filepath {}" , brochureDoc.getData("FilePath"));
									if(brochureDoc.getData("FilePath").startsWith("/")) {
										reP.setParameter("PDFBrochureURL-Plan", InitServlet.getSystemParam("URLHome") + brochureDoc.getData("FilePath"));
									} else {
										reP.setParameter("PDFBrochureURL-Plan", InitServlet.getSystemParam("URLHome") + "/" + brochureDoc.getData("FilePath"));
									}
								}
					}
							
							queueItem.agentID = agentID;
							reP.setParameter(pm.getAgentIDKey(), agentID);
							reP.setParameter(pm.getBranchIDKey(), branchID);
							
							PublishMethod[] legacyIDMethods = PublishMethod.getLegacyIDMethods();
							if(Arrays.asList(legacyIDMethods).contains(pm)) {
								String listingID = setPublishListingID(getConnection(), reP, product, pm);
								log("ListingID is set to : {}", listingID);
							}
							
							HashMap<String, String> dimMap = getDimensions(getConnection(),product.getData("ProductID"));

							if("Yes".equals(dimMap.get("Study.YesNo"))){
								reP.setParameter("Study", "1");
							} else if(StringUtils.isNotBlank(dimMap.get("Study.NumberOf"))) {
								reP.setParameter("Study", String.valueOf(new Double(dimMap.get("Study.NumberOf")).intValue()));
							}
							
							// ReDo the drip calculation to be extra sure
							if (RPMtask.getInstance() != null && ("House and Land".equals(product.getData("ProductType")) || "Home Plan".equals(product.getData("ProductType")) || "Existing Property".equals(product.getData("ProductType")))) {
								SimpleDateFormat ldf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

								boolean doDRIP = false;
								if (StringUtils.isBlank(product.getString("DripModifiedDate"))) {
									doDRIP = true;
								} else if (StringUtils.isNotBlank(product.getString("ModifiedDate"))) {
									try {
										if (ldf.parse(product.getString("ModifiedDate")).getTime() > ldf.parse(product.getString("DripModifiedDate")).getTime()) doDRIP = true;
									} catch (Exception e) {
										doDRIP = true;
									}
								}
								if (doDRIP) {
									RPMtask.getInstance().calculatePackageDrips(null, new String[] { product.getString("ProductID"), product.getString("TotalCost"), product.getString("DripResult")});
									// Reload
									product = getProduct(productID);
								}
							}
							
							String price = product.getData("DripResult");

							if (price.replaceAll("[\\$0\\.\\-]","").length() == 0) {
								price = product.getData("TotalCost");
								log("using TotalCost as price : " + price);
							} else {
								log("using dripresult as price : " + price);
							}
							
							if (product.getData("TotalCost").equals(product.getData("PlanTotalCost"))) {
								// ohh crap, this should not be sent, lets abort.
								log("ohh crap, this cost {} should not be sent, lets abort for Product ID: {}" , product.getData("TotalCost"), product.getString("ProductID"));
								errors = "The house (" + product.getData("TotalCost") + ") and plan (" + product.getData("PlanTotalCost") + ") are the same, aborting product: " + product.getData("ProductID");
								isValid = false;
							}
							
							if (price != null) {
								try {
									price = StringUtil.getRoundedFormattedValue(price, false);
									
								} catch (Exception e) {}
							} 
							log("Price : " + price);
							
							reP.setParameter("Price", price);
							
							double priceDouble = StringUtil.getRoundedValue(price);
							log("Unformatted Price : " + priceDouble);
							reP.setParameter("UnformattedPrice", "" + priceDouble);
							reP.setParameter("IntPrice", "" + (int) priceDouble);
							
							if(pm == PublishMethod.Trademe) {
								String logMsg = setTrademeParameters(reP, price, product, queueItem.listingID, pp);
								log(logMsg);
							}

							if(StringUtils.isNotBlank(pp.getData("HeroImageID"))) {
								GenRow img = new GenRow();
								img.setConnection(getConnection());
								img.setTableSpec("Documents");
								img.setParameter("DocumentID", pp.getData("HeroImageID"));
								img.setColumn("FilePath", "!NULL;!EMPTY");
								img.setParameter("-select0", "*");
								img.doAction("selectfirst");
								if (img.getData("FilePath") != null && img.getData("FilePath").length() > 0) {
									if(pm == PublishMethod.Domain) {
										// 14-Jan-2013 NI: String fp = PropertyEntity.resizeImage(img.getData("FilePath"), 800, 600, ResizeOption.Pad, "jpg").substring(hostS.length()), hash = SecurityFactory.hashString(fp) + fp.substring(fp.lastIndexOf("."));
										String fp = ImageResizeUtil.imageResizeByHeightWidth (img.getData("FilePath"), 800, 600, ResizeOption.Crop, "jpg").substring(hostS.length()), hash = SecurityFactory.hashString(fp) + fp.substring(fp.lastIndexOf("."));
										reP.setParameter("ImageM",  hash);
										imageMap.put(fp, hash);
									} else if(pm == PublishMethod.Trademe) {
										String  trademeID = getTrademePhotoID(img);
										if(StringUtils.isNotBlank(trademeID)) {
											reP.setParameter("ImageM",  trademeID);
										}
									} else {
										
										// 14-Jan-2013 NI: reP.setParameter("ImageM", hostS + img.getData("FilePath"));
										String fp = ImageResizeUtil.imageResizeByHeightWidth (img.getData("FilePath"), 800, 600, ResizeOption.Crop, "jpg").substring(hostS.length());
										reP.setParameter("ImageM", hostS + fp);
									}
								}
								reP.setParameter("ImageMType", "jpg");
								img.close();
							}
							
							/* Need to load Home Design Gallery for Home Plan*/
							String galleryProductID = "Home Plan".equals(product.getData("ProductType")) ? product.getData("HomeProductID") : product.getData("ProductID");
							
							GenRow gallery = new GenRow();
							gallery.setConnection(getConnection());
							gallery.setViewSpec("GalleryView");
							gallery.setParameter("ProductID", galleryProductID);
							gallery.setParameter("Publisher", new StringBuilder().append("%").append(pm.getPublisher()).append("%").toString());
							gallery.doAction("selectfirst");
							
							int i = 1;
							GenRow imgs = new GenRow();
							imgs.setViewSpec("LinkedDocumentImageView");
							imgs.setConnection(getConnection());
							
							logger.trace("Starting off with images...");
							
							if(gallery.isSuccessful()) {
								logger.trace("Gallery is present...");
								//imgs.setColumn("ProductID",product.getData("ProductID"));
								imgs.setColumn("GalleryID",gallery.getData("GalleryID"));							
								imgs.setColumn("LinkedDocumentDocument.DocumentType","Image");
								//imgs.setColumn("LinkedDocumentDocument.DocumentSubType","NULL+EMPTY+!Floorplan");
								imgs.setColumn("LinkedDocumentDocument.DocumentSubType|1","NULL+EMPTY");
								imgs.setColumn("LinkedDocumentDocument.DocumentSubType|1","!Floorplan;!Floor Plan");
								//imgs.setColumn("ShowOnREorder", ">0");
								imgs.setColumn("FilePath", "!NULL;!EMPTY");
								imgs.sortBy("ShowOnGalleryOrder", 0);
								imgs.sortBy("ShowOnREorder", 1);
								imgs.sortBy("SortOrder", 2);
								imgs.doAction(ActionBean.SEARCH);

								logger.trace("Normal Images Query {}" , imgs.getStatement());

								imgs.getResults();
								
								while (imgs.getNext() && i < 25) {
									if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) {
										// Do not add if it is a hero image
										if(! (imgs.getData("DocumentID").equals(pp.getData("HeroImageID")))) {
											if(pm == PublishMethod.Domain) {
												
												// 14-Jan-2013 NI: String fp = PropertyEntity.resizeImage(imgs.getData("FilePath"), 800, 600, ResizeOption.Pad, "jpg").substring(hostS.length());
												String fp = ImageResizeUtil.imageResizeByHeightWidth (imgs.getData("FilePath"), 800, 600, ResizeOption.Crop, "jpg").substring(hostS.length());
												
												if(imageMap.containsKey(fp)) {
													reP.setParameter("Image" + i, imageMap.get(fp));
												} else {
													String hash = SecurityFactory.hashString(fp) + fp.substring(fp.lastIndexOf("."));
													reP.setParameter("Image" + i,  hash);
													imageMap.put(fp, hash);
												}
											} else if(pm == PublishMethod.Trademe) {
												String  trademeID = getTrademePhotoID(imgs);
												if(StringUtils.isNotBlank(trademeID)) {
													reP.setParameter("Image" + i,  trademeID);
												}
											} else {
												
												// 14-Jan-2013 NI: reP.setParameter("Image" + i, hostS + imgs.getData("FilePath"));
												String fp = ImageResizeUtil.imageResizeByHeightWidth (imgs.getData("FilePath"), 800, 600, ResizeOption.Crop, "jpg").substring(hostS.length());
												reP.setParameter("Image" + i, hostS + fp);
											}
											reP.setParameter("ImageType" + i,"jpg");
											++i;
										} else { 
											logger.debug("skipping image as it was the hero {}", imgs.getData("DocumentID"));
										}
									} else if(logger.isTraceEnabled()) 
										logger.trace("Document {} had no FilePath", imgs.getData("DocumentID"));
								}
							}
								imgs.clear();
								
								if("Home Plan".equals(product.getData("ProductType"))) {
									logger.trace("It is a home plan");
									imgs.setViewSpec("DocumentView");
									imgs.setConnection(getConnection());
									imgs.setColumn("DocumentID", product.getString("Image")); // Plan image is the hero image
									imgs.setColumn("DocumentType","Image");
									imgs.setColumn("DocumentSubType", "Floorplan+Floor Plan");
									imgs.setColumn("FilePath", "!NULL;!EMPTY");
									imgs.doAction(ActionBean.SEARCH);
									imgs.getResults();									
								} else {
									imgs.setViewSpec("LinkedDocumentImageView");
									imgs.setConnection(getConnection());
									imgs.setColumn("GalleryID",gallery.getData("GalleryID"));							
									imgs.setColumn("LinkedDocumentDocument.DocumentType","Image");
									imgs.setColumn("LinkedDocumentDocument.DocumentSubType", "Floorplan+Floor Plan");
									//imgs.setColumn("LinkedDocumentDocument.DocumentSubType","Floorplan");
									//imgs.setColumn("ShowOnREorder", ">0");
									imgs.setColumn("FilePath", "!NULL;!EMPTY");
									imgs.sortBy("ShowOnREorder", 0);
									imgs.sortBy("SortOrder", 1);
									if(StringUtils.isNotBlank(gallery.getData("GalleryID"))) {
										imgs.doAction(ActionBean.SEARCH);
										imgs.getResults();
									}
									logger.trace("Normal Images Query {}" , imgs.getStatement());
								}
								
								
								logger.trace("Floor Plan Query is : {}", imgs.getStatement());
								
								// TODO ImageResizeUtil.imageResizeByHeightWidth(inFile, outFile, newWidth, newHeight, option);
								
								i = 1;
								while (imgs.getNext() && i <= 5) {
									if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) {
										String imgType = StringUtils.defaultIfEmpty(getImageType(imgs.getData("FilePath")), "");
										if(pm == PublishMethod.Domain) {
											
											/* image must be correct size, etc. */
											// 14-Jan-2013 NI: String fp = PropertyEntity.resizeImage(imgs.getData("FilePath"), 800, 600, ResizeOption.Pad, "gif").substring(hostS.length());
											
											String fp = ImageResizeUtil.imageResizeByHeightWidth (imgs.getData("FilePath"), 800, 600, ResizeOption.Pad, "gif").substring(hostS.length());
											
											if(imageMap.containsKey(fp)) {
												reP.setParameter("FloorPlan" + i, imageMap.get(fp));
											} else {
												String hash = SecurityFactory.hashString(fp) + fp.substring(fp.lastIndexOf("."));
												reP.setParameter("FloorPlan" + i,  hash);
												imageMap.put(fp, hash);
											}
										} else if(pm == PublishMethod.Trademe) {
											String  trademeID = getTrademePhotoID(imgs);
											if(StringUtils.isNotBlank(trademeID)) {
												reP.setParameter("FloorPlan" + i,  trademeID);
											}
										} else {
											// 14-Jan-2013 NI: reP.setParameter("FloorPlan" + i, hostS + imgs.getData("FilePath"));
											// only jpeg and gif are supported for REA											
											imgType = (imgType.startsWith("jp") || imgType.startsWith("gif")) ? imgType : "gif";
											String fp = ImageResizeUtil.imageResizeByHeightWidth (imgs.getData("FilePath"), 800, 600, ResizeOption.Pad, imgType).substring(hostS.length());
											
											log("FinalOutputFilePath to REA is (via trace): " + fp);
											log("FinalHostS is (via trace): " +  hostS + imgs.getData("FilePath"));
											
											reP.setParameter("FloorPlan" + i, hostS + fp);
										}
										reP.setParameter("FloorPlanImageType" + i, imgType);
										
										// when plan images are needed to be the Highest Quality possible. Eg:  
										reP.setParameter("HQFloorPlan" + i, hostS + imgs.getData("FilePath"));
										reP.setParameter("HQFloorPlanImageType" + i, StringUtils.defaultIfEmpty(getImageType(imgs.getData("FilePath")), ""));
										
										++i;
									} else if(logger.isTraceEnabled()) 
										logger.trace("Document {} had no FilePath", imgs.getData("DocumentID"));
								}
								imgs.close();
							gallery.close();
							pp.close();
							
							// DRIPS
							if(isHouseAndLand(product)) {
								PropertyEntity property = PropertyFactory.getPropertyEntity(con, productID);
								for (i = 1; i < 16; i++) {
									String dripText = property.getDripInfo(con, new UserBean(), "Text", "PDF", "Detail", i + "");
									String dripImage = property.getDripInfo(con, new UserBean(), "Image", "PDF", "Detail", i + "");

									log("Loading dripText for feed {}, {}" +  i + dripText);
									log("Loading dripImage for feed {}, {}" + i + dripImage);
									
									if (StringUtils.isNotBlank(dripText))
										reP.setParameter("DRIP_TEXT_" + i, dripText);
									else
										reP.setParameter("DRIP_TEXT_" + i, "");
									
									if (StringUtils.isNotBlank(dripImage)) {
										dripImage = urlhome + dripImage;
										reP.setParameter("DRIP_IMAGE_" + i, dripImage);
									} else {
										reP.setParameter("DRIP_IMAGE_" + i, "");
									}
									
								}
								
								property.clearDripMap();
								
								for (i = 1; i < 16; i++) {
									String dripText = property.getDripInfo(con, new UserBean(), "Text", "Publishing", "Publishing", i + "");
									String dripImage = property.getDripInfo(con, new UserBean(), "Image", "Publishing", "Publishing", i + "");

									log("Loading publishing dripText  for feed {}, {}" +  i + dripText);
									log("Loading publishing dripImage  for feed {}, {}" + i + dripImage);
									if (StringUtils.isNotBlank(dripText))
										reP.setParameter("DRIP_TEXT_PUBLISHING_" + i, dripText);
									else
										reP.setParameter("DRIP_TEXT_PUBLISHING_" + i, "");
									
									if (StringUtils.isNotBlank(dripImage)) {
										dripImage = urlhome + dripImage;
										reP.setParameter("DRIP_IMAGE_PUBLISHING_" + i, dripImage);
									} else {
										reP.setParameter("DRIP_IMAGE_PUBLISHING_" + i, "");
									}
									
								}
							}

							if(logger.isTraceEnabled()) 
								logger.trace(reP.toString());
							
							try {
								isValid = reP.generateXML();
								
							} catch (Exception e) {
								errors = "Product " + queueItem.name + " Failed: validation error " + e.getMessage();
								error(errors);
								++errorcount;
								isValid = false;
								log(errors);
							}
							
							
							errors = reP.getError();
							log("\nValidated the xml file. Valid = " + isValid);
							
							updateValidationLog(productID, pm.toString(), errors);
							if (!isValid) {
								reP.setFilename(uploadname.replaceAll("uploads", "failed"));
								error("Product " + queueItem.name + " has Errors");
								error(errors);
								++errorcount;
								queueItem.valid =  false;
								log(errors);
							} else if (errors != null && errors.length() > 0) {
								log("Product " + queueItem.name + " has Warnings : ");
								log(errors);
							} else {
								errors = "there were no errors, packaged successfuly";
								log(errors);
							}
							if (isValid && !queueItem.forceRemove) {
								log("isValid && !queueItem.forceRemove");
								String forceTransmissions = InitServlet.getSystemParams().getProperty("ForceTransmissions");
								if ("true".equals(forceTransmissions) || isDifferent(uploadname, queueItem.filename)) {									
									//removeFileName(queueItem.filename);
									queueItem.filename = uploadname;
									if(pm == PublishMethod.Domain) {
										removeFileName(queueItem.zipName);
										uploadname = queueItem.zipName = zipXMLImages(uploadname, imageMap);
									} 
									
									if(pm == PublishMethod.Reiwa) {
										removeFileName(queueItem.zipName);
										uploadname = queueItem.zipName = zipXMLImages(uploadname, new HashMap<String, String>());
									} 
									//removeFile(queueItem.transmissionID);
								} else {
									log("Skipping transmission as file contents are same new file : {}  old file : {}" , new String[]{uploadname, queueItem.filename});
									queueItem.filename = "";
									removeFileName(uploadname);
									queueItem.skip = true;
								}
							} else if (!isValid) {
								log("!isValid");
								// if it is invalid forcefully unpublish from REA
								if(!forceUnpublish(pm, reP, queueItem, product, uploadname)) {	
									log("!forceUnpublish");
									queueItem.filename = "";
									removeFileName(uploadname);
									queueItem.skip = true;
								}
							} else if (isValid) {
								log("isValid");
								queueItem.filename = uploadname;
								if(pm == PublishMethod.Domain) {
									removeFileName(queueItem.zipName);
									uploadname = queueItem.zipName = zipXMLImages(uploadname, imageMap);
								} 
								
								if(pm == PublishMethod.Reiwa) {
									removeFileName(queueItem.zipName);
									uploadname = queueItem.zipName = zipXMLImages(uploadname, new HashMap<String, String>());
								}
							}
						} catch (IOException e) {
							errors = "Product " + queueItem.name + " Failed: file io error " + e.getMessage();
							error(errors);
							++errorcount;
							isValid = false;
							log(errors);
						} catch (Exception e) {
							errors = "Product " + queueItem.name + " Failed: validation error " + e.getMessage();
							error(errors);
							++errorcount;
							isValid = false;
							log(errors);
						}
						queueItem.valid = isValid;
						updatePackage(queueItem.transmissionID, queueItem.productID, queueItem.publishingID, isValid, errors, queueItem.filename ,!queueItem.markForDelete, queueItem.skip);
						//}
						product.close();
	}


	/** Returns true if multiaccount is supported for the ProductType
	 * @param pType
	 * @return
	 */
	public static boolean isMultiAccountFeedSupported(GenRow product) {
		//log("Checking Multi account feed for {}", product.getString("ProductType"));
		boolean isSupported = false;

		if ("House and Land".equals(product.getString("ProductType")) || 
			"Existing Property".equals(product.getString("ProductType")) || 
			"Apartment".equals(product.getString("ProductType")) ||
			"Home Plan".equals(product.getString("ProductType")) || 
			"Land".equals(product.getString("ProductType"))) {
			
			isSupported = true;
			//log("Multi account feed is supported");
		} else {
			isSupported = false;
		}

		return isSupported;
	}
	
	private static boolean isHouseAndLand(GenRow product) {
		return "House and Land".equals(product.getString("ProductType"));
	}

	private static boolean isEP(GenRow product) {
		return "Existing Property".equals(product.getString("ProductType"));
	}
	
	private static boolean isApartment(GenRow product) {
		return "Apartment".equals(product.getString("ProductType"));
	}
	
	private static boolean isHomePlan(GenRow product) {
		return "Home Plan".equals(product.getString("ProductType"));
	}	
	
	private static boolean isLand(GenRow product) {
		return "Land".equals(product.getString("ProductType"));
	}
	
	private String getProductRegion(GenRow product) {
		String regionID = product.getString("RegionID");
		log("Region is {}", regionID);
		
		// get top parent
		String parentRegionName = getParentRegion(regionID);
		log("Parent Region name is {}", parentRegionName);
		return parentRegionName;
	}

	private String getParentRegion(String regionID) {
		GenRow displayEntity = new GenRow();
		displayEntity.setTableSpec("DisplayEntities");
		displayEntity.setConnection(getConnection());
		displayEntity.setParameter("DisplayEntityID", regionID);
        displayEntity.setParameter("-select1", "ParentDisplayEntityID");
        displayEntity.setParameter("-select2", "Name");
        displayEntity.doAction("selectfirst");
        
        if(StringUtils.isNotBlank(displayEntity.getString("ParentDisplayEntityID")))
        	return getParentRegion(displayEntity.getString("ParentDisplayEntityID"));
       
        // only send the first word in the region name, needs to be revisited if used by any one else other than JG King
        return  StringUtils.defaultString(displayEntity.getString("Name")).split(" ")[0].toLowerCase();
	}

	/** Checks if the Plan image has been marked as Floor plan or not, if not it will mark it as floor plan document type
	 *  Clien
	 * @param data
	 */
	private void doFloorPlanCheck(String planImage) {
		log("Plan image is = " + planImage);

		GenRow documentImage = new GenRow();
		documentImage.setConnection(getConnection());
		documentImage.setTableSpec("Documents");
		documentImage.setParameter("DocumentID", planImage);
		documentImage.setParameter("DocumentSubType", "Floorplan+Floor Plan");
		documentImage.setParameter("-select0", "DocumentSubType");
		documentImage.doAction("selectfirst");

		if (!documentImage.isSuccessful()) {
			documentImage.setParameter("DocumentSubType", "Floor Plan");
			documentImage.doAction("update");

			log("Updated DocumentSubType for Plan image : " + documentImage.getStatement());
		}

		documentImage.close();

	}
	
	
	/** if product specific agent ids will be set if they are configured else the common one will be set
	 * @param reP
	 * @param product
	 */
	public static String getPublishAgentID(Connection con, GenRow product, PublishMethod pm) {
		logger.debug("Checking {}" , pm.getName());
		String agentID = null;
		boolean isEnabledEstate = "true".equals(InitServlet.getSystemParams().getProperty("Publish-EnableFeedForEstate"));
		boolean isEnabledBuilding = "true".equals(InitServlet.getSystemParams().getProperty("Publish-EnableFeedForBuilding"));
		boolean isEnabledHomeRange = "true".equals(InitServlet.getSystemParams().getProperty("Publish-EnableFeedForHomeRange"));
 
		logger.debug("{} is a multi account" , PublishMethod.getMultiAccountMethods());
		
		if (ArrayUtils.contains(PublishMethod.getMultiAccountMethods(), pm)) {
			logger.debug("Checking {} is a multi account" , pm.getName());
			String agentProductID = null;
			
			if(isHouseAndLand(product)) {
				if (isEnabledEstate) {
					agentProductID = product.getString("EstateProductID");
				} else if (isEnabledHomeRange) {
					agentProductID = product.getString("RangeProductID");
				}
			} else if(isEP(product)) {
				GenRow epRow = getExistingPropertyQuickIndex(con, product.getString("ProductID"));
				
				if (isEnabledEstate) {
					agentProductID = epRow.getString("EstateID");
				} else if (isEnabledHomeRange) {
					agentProductID = epRow.getString("RangeID");
				}
				epRow.close();
				
			} else if(isApartment(product) && isEnabledBuilding) {
				agentProductID = product.getString("BuildingProductID");
				
			} else if(isHomePlan(product) && isEnabledHomeRange) {
				agentProductID = product.getString("RangeProductID");
				
			} else if(isLand(product) && isEnabledEstate) {
				agentProductID = product.getString("EstateProductID");
			}
			
			logger.debug("Using  agentProductID {}" , agentProductID);
			
			if(StringUtils.isNotBlank(agentProductID)) {
				GenRow productFeedDetails = RunwayUtil.getProductFeedDetails(con, agentProductID);
				agentID = productFeedDetails.getString(pm.getAgentIDKey());
				productFeedDetails.close();
				logger.debug("Retrieved agentID from ProductFeedDetails [{}]" , agentID);
			}
			
		}		
		
		// If specific agentid is not obtained just fill the global one based on the PM
		if(StringUtils.isBlank(agentID)) {
			// REA key is not in conformance with the rest of the naming convention of ID's in systemparameter.
			// It has been created as realestateAgentID, instead of REAAgentID, Some coding below to handle this special case			
			String systemParameterKey = pm == PublishMethod.REA ? "realestateAgentID" : pm.getSystemParameterKey() + "AgentID"; 
			logger.debug("Retrieving agentID from SystemParameters {}" , systemParameterKey);
			agentID = InitServlet.getSystemParams().getProperty(systemParameterKey);
		}
		
		logger.debug("Setting PublishAgentID {} for {}" , pm.getAgentIDKey() , agentID);
		agentID = StringUtils.defaultIfEmpty(agentID, "");
		
		return agentID;
	}
	
	/** if product specific agent ids will be set if they are configured else the common one will be set
	 * @param reP
	 * @param product
	 */
	public static String setPublishListingID(Connection con, RealEstatePackager reP, GenRow product, PublishMethod pm) {
		//EG: set REAListingID
		String listingID = "";
		if (StringUtils.isNotBlank(product.getString("ProductID"))) {
			GenRow productFeedDetails = RunwayUtil.getProductFeedDetails(con, product.getString("ProductID"));
			listingID = productFeedDetails.getString(pm.getListingIDKey());
			logger.debug("Retrieved Listing from ProductFeedDetails [{}]", listingID);
		}
		listingID = StringUtils.isBlank(listingID) ? product.getString("ProductID") : listingID;

		reP.setParameter(pm.getListingIDKey(), listingID);
		return listingID;
	}

	private boolean forceUnpublish(PublishMethod pm, RealEstatePackager reP, QueueItem queueItem, GenRow product, String uploadname) throws IOException, NoSuchAlgorithmException {		
		File sf = null;
		String shortSpecFileName = configfilepath + "/specs/feeds/" + pm.getSpecFolder() + StringUtil.join("/","Product-Short.xml" );
		
		log("Generating file using " + shortSpecFileName);
		
		if ((sf = new File(shortSpecFileName)).canRead()) {
			log("Generating file to forcefully unpublish as the file is invalid");		
			
			queueItem.skip = false;
			queueItem.markForDelete = true;
			queueItem.filename = uploadname.replace(".xml", "forceunpublish.xml");
			
			reP = new RealEstatePackager(new RealEstateMapper(sf),queueItem.filename);
			
			reP.setParameter("Status", getInactiveStatus(pm));
			reP.setParameter("ProductID",product.getString("ProductID"));
			reP.setParameter("DATE",pm == PublishMethod.REA? yyyymmdd_2.format(new Date()) : yyyymmdd.format(new Date()));
			
			reP.generateXML();
			
			if(pm == PublishMethod.Domain) {
				zipXMLImages(queueItem.filename, new HashMap<String, String>());
			}
			log("forcefully unpublishing {}" , new String[]{uploadname});
			
			return true;
					
		}  else {
			log("return false");
			return false;
		}
	}


	/**
	 * 
	 */
	private void setRepDetails(RealEstatePackager reP, String productID, boolean offmarket, Connection con) {
		String title = "", repName = "", repPhone = "", repMobile = "", repEmail = "";

		GenRow row = new GenRow();
		row.setViewSpec("ProductSecurityGroupView");
		row.setConnection(con);
		row.setParameter("-join0", "ProductSecurityGroupRep");
		row.setParameter("ProductID", productID);
		row.setParameter("RepUserID", "!NULL;!EMPTY");		
		row.doAction("selectfirst");

		if (row.isSuccessful()) {
			title = row.getString("RepTitle");
			repName = new StringBuilder().append(row.getString("RepFirstName")).append(" ").append(row.getString("RepLastName")).toString();
			repPhone = 	row.getString("RepPhone");
			repMobile = row.getString("RepMobile");
			repEmail = row.getString("RepEmail");
			row.close();
		} else if (offmarket){
			// if it is marked for delete and if the package happens to be in the unclaimed pool use a dummy sales rep
			title = "Mr";
			repName = "Test Rep";
			repPhone = 	"0000";
			repMobile = "0000";
			repEmail = "test@test.com";
		}
		reP.setParameter("RepTitle", title);
		reP.setParameter("RepName", repName);
		reP.setParameter("RepPhone", repPhone);
		reP.setParameter("RepMobile", repMobile);		
		reP.setParameter("RepEmail", repEmail);
		reP.setParameter("RepFirstName", row.getData("RepFirstName"));
		reP.setParameter("RepLastName", row.getData("RepLastName"));
		reP.setParameter("RepUserID", row.getData("RepUserID"));
	}

	/**
	 *  so if we get to here, then we're sending and if domain we should package up the images 
	 *  
	 *  @param uploadname = a full path to the upload xml file
	 *  @param imageSet = a set of images with *relative* paths to the root of the app (as we're using those paths as references in the xml file)
	 */
	private String zipXMLImages(String uploadname, Map<String, String> imageMap) throws IOException {
		logger.debug("zipXMLImages({},{})", uploadname, imageMap);
		final File xmlFile = new File(uploadname);
		if(!xmlFile.exists()) {
			throw new RuntimeException("zipXMLImages was passed an uploadname which did not exist " + uploadname);
		}
		final String zipName = uploadname.substring(0, uploadname.length()-4) + ".zip"; //remove .xml and replace with zip.

		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipName));
		try { 
			/* the xml file must be in the root */
			addXMLFileToZip(xmlFile.getAbsolutePath(), xmlFile.getName(), out);
			for(Map.Entry<String, String> me: imageMap.entrySet()) {
				logger.debug("processing image to zip {} as {}", me.getKey(), me.getValue());
				try {
					addFileToZip(InitServlet.getRealPath("/" + me.getKey()), me.getValue(), out);
				} catch(Exception e) {
					e.printStackTrace();
					logger.error("Exception e while processing image {} {}", me.getKey(), me.getValue());
					// Just log and continue as it could be an issue with just one image
				}
			}
		} finally { 
			out.close();
		}
		return zipName;
	}

	private void addXMLToZip(String filePath, String zipPath, ZipOutputStream out) throws IOException {
		out.putNextEntry(new ZipEntry(zipPath));
		//FileInputStream fis = null;
		BufferedReader br = null;
		try { 
			//fis = new FileInputStream(filePath);
			br = new BufferedReader(new FileReader(new File(filePath)));
			String s = null;
			while((s = br.readLine()) != null) {
				if(s.indexOf("<File Format=\"Photograph\"></File>") == -1 
						&& s.indexOf("<File Format=\"FloorPlan\"></File>") == -1) {
					out.write(s.getBytes());
				}
			}
		} finally {
			br.close();
		}
	}

	private void addXMLFileToZip(String filePath, String zipPath, ZipOutputStream out) throws IOException {
		out.putNextEntry(new ZipEntry(zipPath));
		InputStream fis = null;
		try { 
			fis = new FileInputStream(filePath);
			
			String xml = readFile(filePath);
			xml = xml.replaceAll(REIWA_DTD_STRING, "");
			
			fis = new ByteArrayInputStream(xml.getBytes());
			
			byte[] buf = new byte[1024];
			int r = -1;
			while((r = fis.read(buf)) > 0) {
				out.write(buf, 0, r);
			}
		} finally {
			fis.close();
		}
	}
	

	private void addFileToZip(String filePath, String zipPath, ZipOutputStream out) throws Exception {		
		FileInputStream fis = null;
		try { 
			fis = new FileInputStream(filePath);
			if(fis != null) {
				out.putNextEntry(new ZipEntry(zipPath));
				byte[] buf = new byte[1024];
				int r = -1;
				while((r = fis.read(buf)) > 0) {
					out.write(buf, 0, r);
				}
			}
		} finally {
			fis.close();
		}
	}

	private boolean isDifferent(String newFilename, String oldFilename) {
		if (newFilename == null || newFilename.length() == 0 || oldFilename == null || oldFilename.length() == 0) return true;
		BufferedReader inNew = null;
		BufferedReader inOld = null;
		try {
			inNew = new BufferedReader(new FileReader(newFilename));
			inOld = new BufferedReader(new FileReader(oldFilename));

			String lineNew = "", lineOld = "";

			while ((lineNew = inNew.readLine()) != null && (lineOld = inOld.readLine()) != null) {
				if (!lineNew.equalsIgnoreCase(lineOld)) {
					if (!lineNew.contains("TransmissionTime")) { // This is Reiwa timestamp
						System.out.println("Transmissions: " + lineNew + " != " + lineOld);
						return true;
					}
				}
			}

			if (inNew.readLine() == null && inOld.readLine() == null) {
				System.out.println("Transmissions: " + newFilename + " == " + oldFilename);
				return false;
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			try {
				if (inNew != null) inNew.close();
			} catch (IOException e) {
			}
			try {
				if (inOld != null) inOld.close();
			} catch (IOException e) {
			}
		}
		System.out.println("Transmissions: " + newFilename + " != " + oldFilename);
		return true;
	}

	private String getImageType(String fileName) {
		if (StringUtils.isNotBlank(fileName) && fileName.length() > 4)
			return fileName.trim().substring(fileName.length() - 3);

		return "";
	}

	private void sendProducts() {
		PublishConnectionImpl reFTP = null; // new RealEstateFTP();

		logger.debug("\n\nStarting Send of Transmissions");log("\n\nStarting Send of Tranmissions");

		int sent = 0;

		HashMap<String, PublishConnectionImpl> providerMap = new HashMap<String, PublishConnectionImpl>();
		PublishConnectionImpl ftp = null;
		for(PublishMethod pm: PublishMethod.values()) {
			if(pm.hasFeed()) {
				switch(pm) { 
				case REA:
					if(StringUtils.isNotBlank(realestateUser) && StringUtils.isNotBlank(realestatePassword)) {
						ftp = new RealEstateFTP();
						if (!pretend) ftp.connect(realestateUser, realestatePassword);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to Real estate using {} and {}, {}" , realestateUser, realestatePassword, pm.getFeedHost());
					}
					break;
				case Domain:
					if(StringUtils.isNotBlank(domainUser) && StringUtils.isNotBlank(domainPassword)) {
						ftp = new RealEstateFTP(pm.getFeedHost());
						if (!pretend) ftp.connect(domainUser, domainPassword);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to domain using {} and {}, {}" , domainUser, domainPassword,  pm.getFeedHost());
					}
					break;
				case MyPackage:
					if(StringUtils.isNotBlank(myPackageUser) && StringUtils.isNotBlank(myPackagePassword)) {
						ftp = new RealEstateFTP(pm.getFeedHost());
						if (!pretend) ftp.connect(myPackageUser, myPackagePassword);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to domain using {} and {}, {}" , myPackageUser, myPackagePassword,  pm.getFeedHost());
					}
					break;
				case Hnl:
					if(StringUtils.isNotBlank(hnlUser) && StringUtils.isNotBlank(hnlPassword)) {
						ftp = new RealEstateFTP(pm.getFeedHost());
						if (!pretend) ftp.connect(hnlUser, hnlPassword);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to HNL using {} and {}, {}" , hnlUser, hnlPassword,  pm.getFeedHost());
					}
					break;
				case HomeSales:
					if(StringUtils.isNotBlank(homeSalesUser) && StringUtils.isNotBlank(homeSalesPassword)) {
						ftp = new RealEstateFTP(pm.getFeedHost());
						if (!pretend) ftp.connect(homeSalesUser, homeSalesPassword);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to HomeSales using {} and {}, {}" , homeSalesUser, homeSalesPassword, pm.getFeedHost());
					}
					break;
				case Reiwa:
					if(StringUtils.isNotBlank(reiwaUser) && StringUtils.isNotBlank(reiwaPassword)) {
						if (!pretend) ftp = new RealEstateFTP(pm.getFeedHost());
						ftp.connect(reiwaUser, reiwaPassword);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to REIwa using {} and {}, {}" , reiwaUser, reiwaPassword);
					}
					break;
				case MeHouse:
					if(StringUtils.isNotBlank(meHouseUser) && StringUtils.isNotBlank(meHousePassword)) {
						ftp = new RealEstateFTP(pm.getFeedHost());
						if (!pretend) ftp.connect(meHouseUser, meHousePassword);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to MeHouse using {} and {}, {} " , meHouseUser, meHousePassword, pm.getFeedHost());
					}
					break;
				case SeniorsHousingOnline:
					if(StringUtils.isNotBlank(seniorsHousingOnlineUser) && StringUtils.isNotBlank(seniorsHousingOnlinePassword)) {
						ftp = new RealEstateFTP(pm.getFeedHost());
						if (!pretend) ftp.connect(seniorsHousingOnlineUser, seniorsHousingOnlinePassword);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to SeniorsHousingOnline using {} and {}, {}" , seniorsHousingOnlineUser, seniorsHousingOnlinePassword, pm.getFeedHost());
					}
					break;
				case Trademe:
					if (StringUtils.isNotBlank(trademeKey) && StringUtils.isNotBlank(trademeSecret) && StringUtils.isNotBlank(trademeToken) && StringUtils.isNotBlank(trademeTokenSecret)) {
						//ftp = new TrademeConnection(pm.getFeedHost());
						ftp = TrademeConnection.getInstance();
						if (!pretend) ftp.connect(trademeKey, trademeSecret, trademeToken, trademeTokenSecret);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());
						log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to Trademe using {}, {}, {}, {}" , trademeKey, trademeSecret, trademeToken, trademeTokenSecret);
					}
					break;
				case AllHomes:
					if(StringUtils.isNotBlank(allHomesUser) && StringUtils.isNotBlank(allHomesPassword)) {
						ftp = new RealEstateFTP(pm.getFeedHost());
						if (!pretend) ftp.connect(allHomesUser, allHomesPassword);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to HomeSales using {} and {}, {}" , allHomesUser, allHomesPassword, pm.getFeedHost());
					}
					break;
				case NewHomesGuide:
					if (StringUtils.isNotBlank(newHomesGuideUser) && StringUtils.isNotBlank(newHomesGuidePassword)) {
						ftp = NewHomesGuideConnection.getInstance();
						if (!pretend) ftp.connect(newHomesGuideUser, newHomesGuidePassword);
						providerMap.put(pm.getFeedName(), ftp);
						logger.debug("Added " + pm.getFeedName(), ftp.isConnected());
						log("Added " + pm.getFeedName() + " " + ftp.isConnected());
						log("Sending to NewHomesGuide using {} and {}, {}" , newHomesGuideUser, newHomesGuidePassword, pm.getFeedHost());
					}
					break;
				}
			}
		}

		log("No. of packages attempting to send: " + queue.size());
		for(QueueItem queueItem: queue) {
			//clear the log
			this.productMessage = new StringBuffer(); 
			
			if(providerMap.containsKey(queueItem.provider))
			{
				reFTP = providerMap.get(queueItem.provider);
				
				// Initialization
				reFTP.setListingID(queueItem.listingID);
				boolean isNHG = isNHG(queueItem.provider);
				boolean isTrademe = isTrademe(queueItem.provider);
				log("Sending to: " + queueItem.provider);
				log("isTrademe: " + isTrademe);
				
				// For Trademe if the file is invalid do not send where as for REA of invalid we send a forceunpublish file
				if (queueItem.filename != null && queueItem.filename.length() != 0 && (!isTrademe || ( isTrademe && queueItem.valid))) {
					if (!reFTP.isConnected()) {
						log("Attempting to reconnect to send file: " + queueItem.filename);
						if (!pretend && !reFTP.connect(realestateUser,realestatePassword)) {	//this isn't correct if the provider is not rea
							updateTransmission(queueItem.transmissionID, queueItem.productID,queueItem.publishingID, queueItem.startDate, queueItem.endDate, false,reFTP.getError(),!queueItem.markForDelete, queueItem.skip, queueItem.valid);
							logger.error("Product {} Error {}", queueItem.name, reFTP.getError());
							log(StringUtil.join("Product ", queueItem.name, " Error ", reFTP.getError()));
							++errorcount;
							queueItem.delete();
							continue;
						}
					}
					String sendFile = StringUtils.isNotBlank(queueItem.zipName) ? queueItem.zipName : queueItem.filename; 

					log("Attempting to send file: " + sendFile);
					
					// if pretend to send always return true
					boolean isSent = false; 
					if(isNHG) {
						// NHG needs the agaent id to be sent but for other feeds it causes problems
						isSent = !pretend? reFTP.send(sendFile.replaceAll("\\\\", "/"), queueItem.agentID) : true;
					} else { 
						isSent = !pretend? reFTP.send(sendFile.replaceAll("\\\\", "/")) : true;
					}
					
					log(reFTP.getLogMessage().toString());
					
					if (!isSent) {
						queueItem.valid = false;
						updateTransmission(queueItem.transmissionID, queueItem.productID,queueItem.publishingID, queueItem.startDate, queueItem.endDate, false,reFTP.getError(),!queueItem.markForDelete, queueItem.skip, queueItem.valid);
						logger.error("Product {} file={} Error {}", new String[]{queueItem.name, sendFile, reFTP.getError()});
						log(StringUtil.join("Product name : ", queueItem.name, ", Error : ", reFTP.getError()));
						++errorcount;
						queueItem.delete();
						continue;
					} else {
						queueItem.listingID = reFTP.getListingID();
						updateTransmission(queueItem.transmissionID, queueItem.productID,queueItem.publishingID, queueItem.startDate, queueItem.endDate, true, !pretend? "File sent sucessfuly." : "File pretend sent sucessfuly.",!queueItem.markForDelete, queueItem.skip, queueItem.valid);
						logPublishingHistory(queueItem.publishingID, queueItem.startDate, queueItem.endDate, "Published", queueItem.repUserID);
						updatePublishedFlag(queueItem.publishingID, queueItem.publishedFlag, queueItem.listingID);
						log(StringUtil.join("Product ", queueItem.name, ", file=[", sendFile, "] Sent Successfully"));
						logger.debug(StringUtil.join("Product ", queueItem.name, "file=[", sendFile, "] Sent Successfully"));						
						++sent;
					}
				} else if (!queueItem.skip){
						String errorMsg = queueItem.valid ? " Filename is missing, has this product been packaged?, exiting" : "File is invalid, exiting";
						updateTransmission(queueItem.transmissionID, queueItem.productID, queueItem.publishingID, queueItem.startDate, queueItem.endDate, false, errorMsg, !queueItem.markForDelete, queueItem.skip, queueItem.valid);
						logger.error(errorMsg);
						log(errorMsg);
					++errorcount;
				}else if (queueItem.skip){
					
					if(queueItem.valid) {
						log("Skipping transmission for (ProductPublishingID: {}) as it has been sent before", queueItem.publishingID);
						logger.debug("Skipping transmission for (ProductPublishingID: {}) as it has been sent before", queueItem.publishingID);
						updateTransmission(queueItem.transmissionID, queueItem.productID, queueItem.publishingID, queueItem.startDate, queueItem.endDate, true, "File not sent because it is has been sent before", !queueItem.markForDelete, queueItem.skip, queueItem.valid);

					} else {
						log("File not sent for (ProductPublishingID: {}) as it is invalid" , queueItem.publishingID);
						logger.debug("File not sent for (ProductPublishingID: {}) as it is invalid" , queueItem.publishingID);
						updateTransmission(queueItem.transmissionID, queueItem.productID,queueItem.publishingID, queueItem.startDate, queueItem.endDate, true,"File not sent because as it is invalid",!queueItem.markForDelete, queueItem.skip, queueItem.valid);						
						
					}
				}
			} else {
				logger.error("Unknown provider {}", queueItem.provider);
				log(StringUtil.join("Unknown provider ", queueItem.provider));
			}
			queueItem.delete();
		}
		for(PublishConnectionImpl ref: providerMap.values()) { 
			reFTP = ref;
			if (reFTP.isConnected()) {
				try {
					reFTP.close();
				} catch (IOException e) {
					logger.error("Error closing ftp connectoin", e);
				}
			}
		}

		logger.debug("Finished Send of Transmissions (sent " + sent + " errors " + errorcount + ")");
		log("Finished Send of Transmissions (sent " + sent + " errors " + errorcount + ")");


	}

	public void updatePublishing(String publishingID, String status) {
		// update the transmission data
		GenRow entry = new GenRow();
		entry.setTableSpec("properties/ProductPublishing");
		entry.setConnection(getConnection());
		entry.setParameter("ProductPublishingID",publishingID);
		entry.setParameter("PublishStatus",status);
		//entry.setParameter("ModifiedDate","NOW");
		//entry.setParameter("ModifiedBy",systemuserid);
		entry.doAction(GenerationKeys.UPDATE);
		entry.close();
		log("Updating ProductPublishing to (PublishStatus={}, ProductPublishingID={})", new String[]{status, publishingID});
	}

	public void updatePublishedFlag(String publishingID, boolean publishedFlag, String listingID) {
		// If the package has never been published then update first publish date and publishedflag
		GenRow entry = new GenRow();
		entry.setTableSpec("properties/ProductPublishing");
		entry.setConnection(getConnection());
		entry.setParameter("ProductPublishingID", publishingID);
		
		if (!publishedFlag && StringUtils.isNotBlank(publishingID)) {			
			entry.setParameter("PublishedFlag", "Y");
			entry.setParameter("FirstPublishDate", "NOW()");			
			log("Maiden Publish! Updating publish record for ProductPublishingID = " + publishingID);
		}
		
		if(StringUtils.isNotBlank(publishingID)) {
			entry.setParameter("ListingID", listingID);
			entry.doAction(GenerationKeys.UPDATE);
			log("ListingID updated to {} ", listingID);
		}
		
		entry.close();
	}

	public void logPublishingHistory(String publishingID,  String startDate, String endDate, String status, String repUserID) {
		GenRow settingHistory = new GenRow();
		settingHistory.setConnection(getConnection());
		settingHistory.setViewSpec("properties/ProductPublishingHistoryView");
		settingHistory.setParameter("ProductPublishingHistoryID", KeyMaker.generate());
		settingHistory.setParameter("ProductPublishingID", publishingID);

		settingHistory.setParameter("Status", status);
		settingHistory.setParameter("RepUserID", repUserID);
		settingHistory.setParameter("CreatedDate","NOW");
		settingHistory.setParameter("CreatedBy","system");
		settingHistory.setParameter("StartDate", startDate);
		settingHistory.setParameter("EndDate", endDate);
		settingHistory.setParameter("ModifiedDate","NOW");
		settingHistory.setParameter("ModifiedBy","system");
		settingHistory.doAction(GenerationKeys.INSERT);
		settingHistory.close();
	}

	public void updateProduct(String productID, String status, String errors) {
		// update the transmission data
		GenRow entry = new GenRow();
		entry.setTableSpec("Products");
		entry.setConnection(getConnection());
		entry.setParameter("ProductID",productID);
		entry.setParameter("PublishingStatus",status);
		//if (errors != null && errors.length() > 0) entry.setParameter("PublishingValidation",errors);
		//entry.setParameter("ModifiedDate","NOW");
		//entry.setParameter("ModifiedBy",systemuserid);
		entry.doAction(GenerationKeys.UPDATE);
		
		entry.setTableSpec("properties/ProductQuickIndex");
		entry.setParameter("ON-ProductID",productID);
		entry.doAction(GenerationKeys.UPDATEALL);
		
		log("Updating Products/ProductQuickIndex to (PublishStatus={})", new String[]{status, productID});
		
		entry.close();
	}
	public void updatePackage(String transmissionID, String productID, String publishingID, boolean success, String errors, String filename) {
		updatePackage(transmissionID, productID, publishingID, success, errors, filename, true, false);
	}
	public void updatePackage(String transmissionID, String productID, String publishingID, boolean success, String errors, String filename, boolean active, boolean skip) {
		// update the transmission data
		GenRow entry = new GenRow();
		entry.setTableSpec("ProductTransmission");
		//entry.setRequest(feed.);
		entry.setConnection(getConnection());
		entry.setParameter("TransmissionID",transmissionID);
		entry.setParameter("Active",(active? "Y" : "N"));
		entry.setParameter("ValidatedSuccess",(success? "Y" : "N"));
		entry.setParameter("ValidatedDate","NOW");
		entry.setParameter("PackagedSuccess",(success? "Y" : "N"));
		entry.setParameter("PackagedDate","NOW");
		
		if(StringUtils.isNotBlank(filename)) {
			entry.setParameter("File",filename);
		}
		
		entry.setParameter("ModifiedDate","NOW");
		entry.setParameter("ModifiedBy",systemuserid);
		entry.doAction(GenerationKeys.UPDATE);		
		 
				
		if (publishingID != null && publishingID.length() > 0) {
			String status = "Published";
			if (!success) status = "Invalid";
			else if (!active) status = "Expired";
			updatePublishing(publishingID, status);
			updateProduct(productID, status, errors);
		} else {
			log("Not updating status");
		}

		GenRow log = new GenRow();
		log.setTableSpec("ProductTransmissionLog");
		log.setConnection(getConnection());
		log.setParameter("TransmissionID",transmissionID);
		log.setParameter("CreatedDate","NOW");
		log.setParameter("CreatedBy",systemuserid);
		log.setParameter("Success",(success? "Y" : "N"));
		log.setParameter("Type","Pack");
		log.setParameter("Error",errors + "\r\nDetail Logs:\r\n" + productMessage.toString() );
		log.createNewID();
		log.doAction(GenerationKeys.INSERT);

		if(logger.isTraceEnabled()) logger.trace("ProductTransmissionLog(TransmissionID={}, Success={}, Error={})", new String[]{transmissionID, String.valueOf(success), errors});
		
		log.setParameter("Type","Valid");
		log.remove("TransmissionLogID");
		log.createNewID();
		log.doAction(GenerationKeys.INSERT);
		
		entry.close();
	}
	public void updateTransmission(String transmissionID, String productID, String publishingID, String startDate, String endDate, boolean success, String errors) {
		updateTransmission(transmissionID, productID, publishingID, startDate, endDate, success, errors, true, false, false);
	}

	public void updateTransmission(String transmissionID, String productID, String publishingID, String startDate, String endDate, boolean success, String errors, boolean active, boolean skip, boolean valid) {
		// update the transmission data
		GenRow entry = new GenRow();
		entry.setTableSpec("ProductTransmission");
		//entry.setRequest(feed.);
		entry.setConnection(getConnection());
		entry.setParameter("TransmissionID",transmissionID);
		entry.setParameter("TransmissionSuccess",(success? "Y" : "N"));
		entry.setParameter("Active",(active? "Y" : "N"));		
		entry.setParameter("MarkForDelete",(active? "N" : "Y"));
		entry.setParameter("RefreshFeed","N");
		entry.setParameter("TransmissionDate","NOW");
		entry.setParameter("ModifiedDate","NOW");
		entry.setParameter("ModifiedBy",systemuserid);
		entry.doAction(GenerationKeys.UPDATE);

		if (publishingID != null && publishingID.length() > 0 ) {
			String status = "Published";
			
			if (!valid) 
				status = "Invalid";
			else if (!active) 
				status = "Expired";
			
			updatePublishing(publishingID, status);
			updateProduct(productID, status, "");
			if (RPMtask.getInstance() != null) RPMtask.getInstance().doIndexRefreash("update",  productID);
		} else {
			log("Not updating status");
		}
		entry.close();
		
		GenRow log = new GenRow();
		log.setTableSpec("ProductTransmissionLog");
		log.setConnection(getConnection());
		log.setParameter("TransmissionID",transmissionID);
		log.setParameter("CreatedDate","NOW");
		log.setParameter("CreatedBy",systemuserid);
		log.setParameter("StartDate",startDate);
		log.setParameter("EndDate",endDate);		
		log.setParameter("Success",(success? "Y" : "N"));
		log.setParameter("Type","Sent");
		log.setParameter("Error",errors + "\r\nDetail Logs:\r\n" + productMessage.toString() );
		log.createNewID();
		log.doAction(GenerationKeys.INSERT);
		log.close();
		
	}

	private GenRow getProduct(String productID) {
		GenRow product = new GenRow();
		product.setConnection(getConnection());
		product.setColumn("ProductID", productID);
		product.setViewSpec("ProductView");
		product.doAction("selectfirst");

		if ("House and Land".equals(product.getString("ProductType"))) {
			product.clear();
			product.setViewSpec("properties/ProductHouseAndLandView");
			product.setColumn("ProductID", productID);
			product.doAction("selectfirst");
		} else if ("Land".equals(product.getString("ProductType"))) {
			product.clear();
			product.setViewSpec("properties/ProductLotView");
			product.setColumn("ProductID", productID);
			product.doAction("selectfirst");
		} else if ("Apartment".equals(product.getString("ProductType"))) {
			product.clear();
			product.setViewSpec("properties/ProductApartmentView");
			product.setColumn("ProductID", productID);
			product.doAction("selectfirst");
		} else if ("Home Plan".equals(product.getString("ProductType"))) {
			product.clear();
			product.setViewSpec("properties/ProductPlanView");
			product.setColumn("ProductID", productID);
			product.doAction("selectfirst");
		}

		return product;
	}

	private void removeFile(String transmissionID) {
		GenRow entry = new GenRow();
		entry.setTableSpec("ProductTransmission");
		entry.setParameter("-select", "File");
		entry.setConnection(getConnection());
		entry.setParameter("TransmissionID",transmissionID);
		entry.doAction(GenerationKeys.SELECTFIRST);

		File file = new File(entry.getString("File"));
		if (file.exists()) file.delete();
		entry.close();
	}

	private void removeFileName(String filename) {
		if(filename != null) { 
			File file = new File(filename);
			if (file.exists()) file.delete();
		}
	}

	private void updateValidationLog(String productID, String publishMethod, String errors) {
		if (errors == null || errors.length() == 0) errors = "";
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductValidation");
		row.setConnection(getConnection());
		row.setParameter("-select1", "*");
		row.setParameter("ProductID", productID);
		row.setParameter("PublishMethod", publishMethod);
		row.doAction(GenerationKeys.SELECTFIRST);

		if (!row.isSuccessful()) {
			row.setColumn("ProductValidationID", KeyMaker.generate());
			row.setAction("insert");
		} else {
			row.setAction("update");
		}
		row.setParameter("ProductID", productID);
		row.setParameter("PublishMethod", publishMethod);
		row.setColumn("Message", errors);
		if (errors.indexOf("Required:") >= 0) {
			row.setColumn("Status", "Content Required");
		} else if (errors.indexOf("Optional:") >= 0) {
			row.setColumn("Status", "Validated - Optional");
		} else if (errors.indexOf("Deferred:") >= 0) {
			row.setColumn("Status", "Validated - Deferred");
		} else {
			row.setColumn("Status", "Validated - Passed");
		}
		row.setColumn("ValidationDate", "NOW");
		row.setColumn("ValidationBy", row.getString("CURRENTUSERID"));
		row.doAction();
		row.close();
	}

	public String getProcessName()
	{
		return "Product Transmissions";
	}

	protected void processItem()
	{

	}

	protected void finish()
	{
	}

	protected void sendUserEmail(String alert)
	{
		debug("sending email to "+ alert);
		log("\r\nCOMPLETED SUCCESSFULLY\r\n");
		try{

			RowBean context = new RowBean();
			context.put("URLHome", urlhome);
			context.put("EmailAlertsURLHome", emailalertsurlhome);
			context.put("alert", alert);
			//context.put("body", message);

			//String text = generateEmail(context, alerttext);
			//String html = generateEmail(context, alerthtml);

			ServletConfig sc = InitServlet.getConfig();
			if(sc!=null && alert.length()!=0)
			{
				MailerBean mailer = new MailerBean();
				mailer.setServletContext(sc.getServletContext());
				mailer.setFrom(defaultFrom);
				mailer.setTo(alert);
				mailer.setSubject("Transmission Logs");
				mailer.setTextbody(message.toString());
				mailer.setHtmlbody(message.toString().replaceAll("\\n","<br />"));
				if(config.getPretend())
				{
					debug("email to "+ alert + "\r\n " + message);
				}
				else
				{
					String response = mailer.getResponse();
					debug("email to "+ alert + " " + response);
				}
			}

		}catch(Exception e){
			error(e);
		}
	}

	protected String generateEmail(TableData context, String script)
	{
		StringWriter text = new StringWriter();
		try{
			StringBuffer roottemplate = new StringBuffer();
			roottemplate.append("#parse(\"");
			roottemplate.append(DEFAULT_VELOCITY_PATH);
			roottemplate.append(script);
			roottemplate.append("\")");

			VelocityEngine ve = VelocityManager.getEngine();
			ve.evaluate( context, text, "StatusAlerts", roottemplate.toString());

			text.flush();
			text.close();

		}catch(Exception e){
			error(e);
		}
		return(text.toString());
	}

	public void close()
	{
		super.close();
	}

	public void checkConfig()
	{
		log("Running transmissions on " + configfilepath);
		debug("Checking config");
		config = TransmissionsConfig.getTransmissionConfig(configfilepath);
		debug("Wait interval set to "+String.valueOf(config.getInterval()));
	}

	protected String getRepUserID(String productID) {
		String repID = null;
		
		GenRow row = new GenRow();
		row.setViewSpec("ProductSecurityGroupView");
		row.setConnection(getConnection());
		row.setParameter("ProductID",productID);
		row.doAction("selectfirst");

		if(row.isSuccessful() && StringUtils.isNotBlank(row.getString("RepUserID")))
			repID = row.getString("RepUserID");
		
		row.close();
		return repID;
	}

	protected EmailerMap getEmailerMap(GenRow row)
	{
		EmailerMap map = new EmailerMap();
		map.putAll(row);
		return(map);
	}

	protected String getProductUrl(String productid)
	{
		StringBuffer url = new StringBuffer(emailalertsurlhome);
		url.append("/runway/product.view?ProductID=");
		url.append(productid);
		return(url.toString());
	}

	public static void main(String[] args)
	{


	}

	private HashMap<String, String> getDimensions(Connection con, String productid) {
		HashMap<String, String> dimMap = new HashMap<String,String>();
		if (productid == null || productid.length() == 0) return dimMap;

		String ignorelist = "ProductDetailsID,ProductID,CopiedFromProductID,ProductType,DetailsType,SortOrder";

		TableSpec detailsSpec = SpecManager.getTableSpec("properties/ProductDetails");

		GenRow details = new GenRow();
		details.setTableSpec("properties/ProductDetails");
		details.setConnection(con);
		details.setParameter("-select","*");
		details.setColumn("ProductID",productid);
		details.sortBy("SortOrder",0);
		details.sortBy("DetailsType",1);
		if (details.getString("ProductID").length() > 0) {
			details.doAction("search");
			details.getResults(true);
		}

		int d = 0;
		while (details.getNext()) {
			for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
				if (ignorelist.indexOf(detailsSpec.getFieldName(cs)) == -1) {
					dimMap.put((details.getString("DetailsType") + "." + detailsSpec.getFieldName(cs)), details.getString(detailsSpec.getFieldName(cs)));
				}
			}
			++d;
		}
		details.close();
		return dimMap;		
	}

	private class QueueItem {
		public String		transmissionID;
		public String		publishingID;
		public String		productID;
		public String		name;
		public String		provider;
		public String		filename;
		public String		repUserID;
		public String		startDate;
		public String		endDate;
		public String 		zipName;
		public String 		listingID;
		public String 		agentID;
		public boolean		publishedFlag;
		public boolean		markForDelete = false;
		public boolean		forceRemove = false;
		public boolean		skip = false;
		public boolean		isNew = true;
		public boolean		valid = true;
		
		private String		ts = "ProductTransmissionQueue";

		public QueueItem(String transmissionID, String productID, String publishingID, String provider, String name, String repUserID, String startDate, String endDate, String filename, boolean publishedFlag, boolean markForDelete, boolean forceRemove, String listingID) {
			this.transmissionID = transmissionID;
			this.publishingID = publishingID;
			this.productID = productID;
			this.provider = provider;
			this.name = name;
			this.repUserID = repUserID;
			this.startDate = startDate;
			this.endDate = endDate;
			this.publishedFlag =  publishedFlag;
			this.markForDelete = markForDelete;
			this.forceRemove = forceRemove;
			this.filename = filename;
			if (filename != null && filename.length() > 0) {
				isNew = false;
				if(filename.endsWith(".zip")) {
					this.zipName = filename;
					this.filename = this.zipName.substring(0, this.zipName.length() - 3) + "xml";
				}
			}
			this.listingID = listingID;
		}
		
		public QueueItem(GenRow row) {
			this.transmissionID = row.getData("TransmissionID");
			this.publishingID = row.getString("PublishingID");
			this.productID = row.getString("ProductID");
			this.provider = row.getString("Provider");
			this.name = row.getString("Name");
			this.repUserID = row.getString("RepUserID");
			this.startDate = row.getString("StartDate");
			this.endDate = row.getString("EndDate");
			this.publishedFlag =  "Y".equals(row.getString("PublishedFlag"));
			this.markForDelete = "Y".equals(row.getString("MarkForDelete"));
			this.forceRemove = "Y".equals(row.getString("ForceRemove"));
			this.filename = row.getString("Filename");
			if (filename != null && filename.length() > 0) {
				isNew = false;
				if(filename.endsWith(".zip")) {
					this.zipName = filename;
					this.filename = this.zipName.substring(0, this.zipName.length() - 3) + "xml";
				}
			}
			this.listingID = row.getData("ListingID");
		}
		
		public boolean delete() {
			GenRow row = new GenRow();
			row.setTableSpec(ts);
			
			row.setParameter("TransmissionID", transmissionID);
			
			row.doAction("delete");
			
			return row.isSuccessful();
		}
		
		public boolean save() {
			GenRow row = new GenRow();
			row.setTableSpec(ts);
			row.setParameter("TransmissionID", transmissionID);
			row.setParameter("PublishingID", publishingID);
			row.setParameter("ProductID", productID);
			row.setParameter("Provider", provider);
			row.setParameter("Name", name);
			row.setParameter("RepUserID", repUserID);
			row.setParameter("StartDate", startDate);
			row.setParameter("EndDate", endDate);
			row.setParameter("PublishedFlag", publishedFlag? "Y" : "N");
			row.setParameter("MarkForDelete", markForDelete? "Y" : "N");
			row.setParameter("ForceRemove", forceRemove? "Y" : "N");
			row.setParameter("Filename", filename);
			row.setParameter("ListingID", listingID);

			try {
				row.doAction("insert");
			} catch (Exception e) {
				row.clearStatement();
				row.doAction("update");
			}
			
			return row.isSuccessful();
		}
	}
	
	public String getInactiveStatus(PublishMethod pm) {
		String status = "offmarket";

		if (pm == PublishMethod.Domain) {
			status = "Delete";
			log("Package inactive or marked for delete. Status for domain changed to {}", status);

		}
		
		if (pm == PublishMethod.Reiwa) {
			status = "Withdrawn";
			log("Package inactive or marked for delete. Status for Reiwa changed to {}", status);

		}
		return status;
	}
	
	private void insertLog(String transmissionID, StringBuffer productMessage, String type, boolean success) {
		GenRow log = new GenRow();
		log.setTableSpec("ProductTransmissionLog");
		log.setConnection(getConnection());
		log.setParameter("TransmissionID",transmissionID);
		log.setParameter("CreatedDate","NOW");
		log.setParameter("CreatedBy",systemuserid);
		log.setParameter("Success",(success? "Y" : "N"));
		log.setParameter("Type", type);
		log.setParameter("Error", productMessage.toString() );
		log.createNewID();
		log.doAction(GenerationKeys.INSERT);
		log.close();
	}
	
	public void resetRunning() {
		isRunning = false;
	}
	
	private static GenRow getExistingPropertyQuickIndex(Connection con, String ProductID) {
		GenRow quickRow = new GenRow();
		quickRow.setViewSpec("properties/ProductQuickIndexView");
		quickRow.setConnection(con);
		quickRow.setParameter("ProductID", ProductID);
		quickRow.doAction(GenerationKeys.SELECTFIRST);

		return quickRow;
	}
	
	private String getTrademePhotoID(GenRow imageRow) throws IOException {
		String photoID = imageRow.getString("TrademeID");
		Date lastEncodedDate = imageRow.getDate("EncodedDate");
		
		log("\nPhotoID from DB is {} encoded on lastEncodedDate {}", photoID, lastEncodedDate == null?  "": lastEncodedDate.toString());
		
		if (StringUtils.isBlank(photoID) || (lastEncodedDate == null || imageRow.getDate("ModifiedDate").after(lastEncodedDate))) {
			photoID = uploadPhoto(imageRow.getData("FilePath"), imageRow.getString("FileName"));
			// update document
			if (StringUtils.isNotBlank(photoID)) {

				GenRow document = new GenRow();
				document.setTableSpec("Documents");
				document.setConnection(getConnection());
				document.setParameter("DocumentID", imageRow.getString("DocumentID"));
				document.setParameter("TrademeID", photoID);
				document.setParameter("EncodedDate", "NOW()");
				document.doAction(GenerationKeys.UPDATE);
				log("PhotoID updated");
				document.close();

			}
		}
		
		log("PhotoID is {} for documentID {}", photoID, imageRow.getString("DocumentID"));
		
		return photoID;
	}

	private String uploadPhoto(String inFilePath, String fileName) throws IOException {
		String photoID = null;
		if (StringUtils.isNotBlank(inFilePath) && StringUtils.isNotBlank(trademeKey) && StringUtils.isNotBlank(trademeSecret) && StringUtils.isNotBlank(trademeToken) && StringUtils.isNotBlank(trademeTokenSecret)) {
			String data = getPhotodata(inFilePath);
			TrademeConnection conn = TrademeConnection.getInstance();
			conn.connect(trademeKey, trademeSecret, trademeToken, trademeTokenSecret);
			photoID = conn.sendPhoto(data, fileName, getImageType(inFilePath));
			log(conn.getLogMessage().toString());
		}
		return photoID;
	}
	
	
	private String getPhotodata(String inFilePath) throws IOException {
		String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");

		if (!StringUtil.isBlankOrEmpty(inFilePath)) {
			if (inFilePath.indexOf(homePath) == 0) {
				inFilePath = inFilePath.substring(homePath.length());
			}
			if (inFilePath.startsWith("/")) {
				inFilePath = inFilePath.substring(1);
			}
		}
		inFilePath = InitServlet.getRealPath("/" + inFilePath);

		// Reading a Image file from file system
		File file = new File(inFilePath);
		FileInputStream imageInFile = new FileInputStream(file);
		byte imageData[] = new byte[(int) file.length()];
		imageInFile.read(imageData);

		// Converting Image byte array into Base64 String
		return Base64.encodeBase64String(imageData);
	}
	
	public static String setTrademeParameters(RealEstatePackager reP, String price, GenRow product, String listingID, GenRow pp) {
		StringBuilder logMessage = new StringBuilder();

		reP.setParameter("ListingID", listingID);

		double priceDouble = StringUtil.getRoundedValue(price);

		// Price
		// Converting such that we remove any decimal values if present
		String unformattedPrice = Integer.toString((int) priceDouble);
		logMessage.append("Unformatted Price : " + unformattedPrice);
		reP.setParameter("UnformattedPrice", unformattedPrice);

		// Expected Sale Price, valid values are 100000, 150000, 200000 etc
		String expectedSalePrice = round(priceDouble);
		logMessage.append("\nExpectedSalePrice : " + expectedSalePrice);
		reP.setParameter("ExpectedSalePrice", expectedSalePrice);

		// Only 18 Characters are allowed, so truncate
		// Will not shown on the listing, Trademe currently allows only 18 at the moment, they are enhancing to accept 50 shortly
		if (StringUtils.isNotBlank(product.getString("ProductID"))) {
			reP.setParameter("ShortPropertyID", product.getString("ProductID").substring(10));
		}

		// Only 5 Characters are allowed, so truncate
		if (StringUtils.isNotBlank(product.getString("ProductID"))) {
			reP.setParameter("ShorterPropertyID", product.getString("ProductID").substring(20));
		}

		// Description has to be split into paragraphs
		StringBuilder formattedDesc = new StringBuilder();
		String description = pp.getString("Description");

		if (StringUtils.isNotBlank(description)) {
			StringTokenizer inclusionStr = new StringTokenizer(description, "\n");
			while (inclusionStr.hasMoreElements()) {
				formattedDesc.append("<Paragraph>");
				formattedDesc.append(inclusionStr.nextElement().toString());
				formattedDesc.append("</Paragraph>");
			}
			logMessage.append("\nFormatted Description is " + formattedDesc.toString());
		}
		reP.setParameter("Description", formattedDesc.toString());

		// Find trademe ids for Suburb, region and district
		if (isHouseAndLand(product)) {
			String stateID = "";
			String districtID = "";

			String state = product.getString("State");
			String district = product.getString("Street2");

			GenRow listItems = new GenRow();
			listItems.setViewSpec("ListItemView");

			listItems.setParameter("ItemLabel", state);
			listItems.setParameter("ListItemItemList.ListName", "NZState");
			listItems.doAction("selectfirst");
			if (StringUtils.isNotBlank(state) && listItems.isSuccessful()) {
				stateID = listItems.getString("SetLocationID");
			}

			listItems.setParameter("ItemLabel", district);
			listItems.setParameter("ListItemItemList.ListName", "NZDistrict");
			listItems.doAction("selectfirst");
			if (StringUtils.isNotBlank(district) && listItems.isSuccessful()) {
				districtID = listItems.getString("SetLocationID");
			}

			listItems.close();
			
			reP.setParameter("StateID", stateID);
			reP.setParameter("DistrictID", districtID);
			
			logMessage.append("\nState : "+ state + ", ID :" + stateID);
			logMessage.append("\nDistrict : "+ district + ", ID :" + districtID);
			logMessage.append("\nSuburb : "+ product.getString("City") + ", ID :" + product.getString("Postcode") );
		}

		return logMessage.toString();
	}
	
	
	/** Trademe requires ExpectedSalePrice to be rounded to multiples of 500000
	 * @param num
	 * @return
	 */
	private static String round(double num) {
		int multipleOf = 50000;
		return Integer.toString((int) Math.floor((num + (double) multipleOf / 2) / multipleOf) * multipleOf);
	}
	
	private boolean isTrademe(String provider) {
		if(PublishMethod.Trademe.getFeedName().equals(provider)) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean isNHG(String provider) {
		if(PublishMethod.NewHomesGuide.getFeedName().equals(provider)) {
			return true;
		} else {
			return false;
		}
	}
	
	public String readFile(String filename) {
		StringBuilder fileString = new StringBuilder();
		BufferedReader inNew = null;
		try {
			inNew = new BufferedReader(new FileReader(filename));

			String lineNew = "";

			while ((lineNew = inNew.readLine()) != null) {
				fileString.append(lineNew).append("\n");
			}

		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			try {
				if (inNew != null)
					inNew.close();
			} catch (IOException e) {
			}
		}
		return fileString.toString();
	}
}