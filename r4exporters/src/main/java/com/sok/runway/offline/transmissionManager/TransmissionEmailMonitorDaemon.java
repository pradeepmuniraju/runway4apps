package com.sok.runway.offline.transmissionManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.EmailBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.Note;
import com.sok.runway.offline.AbstractDaemonWorker;
import com.sok.runway.offline.DaemonWorkerConfig;
import com.sok.runway.offline.WorkerTimer;

public class TransmissionEmailMonitorDaemon extends AbstractDaemonWorker {
	private static final Logger logger = LoggerFactory.getLogger(TransmissionEmailMonitorDaemon.class);
	
	private String host, user, pass, domain, sysUserId, reaAgent; 
	private List<String> IDs = Collections.emptyList();
	private final ServletContext ctx;
	private static final int CHECK_INTERVAL = (60 * 15); // 15 minutes, we can't do this soon as it takes too long
	private static final long REFRESH_AGE = (1000L*60*60*24*1); // 1 day old
	private boolean offlineMode = Boolean.FALSE; // for local testing without network connection
	private boolean isRunning = Boolean.FALSE;
	
	public TransmissionEmailMonitorDaemon(String configfilepath, ServletContext ctx) {
		super(configfilepath);
		
		host = InitServlet.getSystemParams().getProperty("TransmissionEmailHost", "outlook.ozhosting.com");	// will prob all be the same, who knows though. 
		user = InitServlet.getSystemParams().getProperty("TransmissionEmailUser", "automation@switched-on.com.au");
		pass = InitServlet.getSystemParams().getProperty("TransmissionEmailPass", "southmelbourne");
		
		sysUserId = InitServlet.getSystemParams().getProperty("SystemUserID");
		config = this.new Config();
		this.ctx = ctx; 
	}
	
	private GenRow storage = null, pub = null;
	public static final String[] folders = {"inbox"};
	TransmissionEmailMonitorDaemon() { super(""); ctx = null; }

	@Override
	public void checkConfig() {
		if (isRunning) 
			return;
		isRunning = true;
		storage = new GenRow();
		storage.setConnection(con);
		storage.setViewSpec("properties/TransmissionEmailView");
		
		if (StringUtils.isBlank(host) || StringUtils.isBlank(user) || StringUtils.isBlank(pass) /* || StringUtils.isBlank(domain) */) {
			logger.error( "Not configured properly host[{}] user [{}] pass [{}] domain [{}]",
					new String[] { host, user, pass, domain });
			isRunning = false;
			return;
		}
		
		reaAgent = InitServlet.getSystemParam("realestateAgentID");
		
		if(!offlineMode) {
			logger.info("Checking for available records");
			
			// Get system properties
			Properties properties = System.getProperties();
			properties.setProperty("mail.store.protocol", "imaps");
			
			// Get the default Session object.
			Session session = Session.getDefaultInstance(properties);
	
			try {
				// Get a Store object that implements the specified protocol.
				Store store = session.getStore("imaps");
				// Connect to the current host using the specified username and
				// password.
				logger.info("Connecting to {} with user {}", host, user);
				store.connect(host, user, pass);
				// Create a Folder object corresponding to the given name.
				for(String f: folders) { 
					logger.info("checking folder {} ", f);
					Folder folder = store.getFolder(f);
					folder.open(Folder.READ_ONLY);
					logger.info("found messages {}", folder.getMessageCount());
					final long minAge = System.currentTimeMillis() - REFRESH_AGE;
					for(int i=folder.getMessageCount(); ;i--) {
						logger.debug("Getting message #{}", i);
						Message message = folder.getMessage(i);
						if(message.getReceivedDate().getTime() < minAge) {
							logger.debug("Have message earlier than min Age {}", message.getReceivedDate());
							break;
						}
						storeMessage(message);
						if(i == 1) {
							break;
						}
					}
					folder.close(true);
				}
				store.close();
			} catch (MessagingException e) {
				logger.error("Error in messaging", e);
			} catch (Exception e) {
				logger.error("Some error we didn't know about?", e);
			}
		} 
		
		currentitem = 0;
		errorcount = 0;
		storage.clear();
		
		final GenRow lookup = new GenRow();
		lookup.setConnection(getConnection());
		lookup.setTableSpec("properties/TransmissionEmails");
		lookup.setParameter("Processed","N");
		lookup.setParameter("-select1","TransmissionEmailID");
		lookup.getResults();
		if(lookup.getNext()) {
			IDs = new ArrayList<String>(100);
			do {
				IDs.add(lookup.getData("TransmissionEmailID"));
			}
			while(lookup.getNext());
			processsize = IDs.size();
		}
		logger.debug("finished lookup, processsize set {}", processsize);
		lookup.close();
		isRunning = false;
	}
	

	public void storeMessage(Message message) throws MessagingException {
		final DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		final String id = message.getHeader("Message-ID")[0];
		logger.debug("Have message ID {}",id);
		
		storage.clear();
		storage.setParameter("MessageID",id);
		storage.doAction(GenerationKeys.SELECTFIRST);
		
		if(storage.isSuccessful()) {
			logger.debug("Skipping message {} as we already have it {}", id, storage.getData("TransmissionEmailID"));
			//test for missing?
			if(message.getSubject() != null && message.getSubject().indexOf("catalogue")>-1) {
				logger.error("OR DO WE?? {}", message.getSubject());
			}
			//we already have done this one.
			return;
		}

		logger.debug("Storing message {}", id);
		storage.setToNewID("TransmissionEmailID");
		storage.setParameter("EmailTo",InternetAddress.toString(message.getAllRecipients()));
		storage.setParameter("EmailFrom", InternetAddress.toString(message.getFrom()));
		storage.setParameter("Subject",message.getSubject());
		logger.debug("Dates-sent: {} , recieved {}", message.getSentDate(), message.getReceivedDate());
		storage.setParameter("SentDate",df.format(message.getSentDate()));
		storage.setParameter("RecievedDate",df.format(message.getReceivedDate()));
		storage.setParameter("LoggedDate","NOW");
		
		// if you do not want particular messages processed, add the criteria here and set processed to S 
		storage.setParameter("Processed","N");

		EmailBean eb = new EmailBean();
		storage.setParameter("Body", eb.getBody(message));
		if(StringUtils.isNotBlank(eb.getErrors())) {
			throw new RuntimeException(eb.getErrors());
		}
		logger.debug("Preparing to store message");
		try {
			storage.doAction(GenerationKeys.INSERT);
			
			if(storage.isSuccessful()) {
				logger.debug("Message stored successfully with id {}",storage.getString("TransmissionEmailID"));
			} else {
				logger.debug(storage.getStatement());
				logger.error("Error saving to DB {}", storage.getError());						
			}
		} catch (com.sok.framework.generation.DatabaseException de) {
			logger.debug(de.toString());
			logger.debug(storage.toString());
			logger.debug(storage.getStatement());
			
			//we don't want it to break if we get an email we don't expect 
			//throw de;
		}
	}

	@Override
	public String getProcessName() {
		// TODO Auto-generated method stub
		return "Incoming Transmission Email Monitor";
	}

	@Override
	protected void finish() {
		if(storage != null) { storage.clear(); storage.close(); storage = null; }
		if(pub != null) { pub.clear(); pub.close(); pub = null; } 
		IDs = Collections.emptyList();
	}

	@Override
	protected void processItem() {
		logger.debug("starting processItem() item {} of {}",currentitem, IDs.size());
		try {
			if(storage == null) {
				storage = new GenRow();
				storage.setConnection(con);
			} else {
				storage.clear();
				storage.setConnection(con);
				storage.setViewSpec("properties/TransmissionEmailView");
			}
			
			if(pub == null) {
				pub = new GenRow(); 
				pub.setConnection(con); 
				
			} else {
				pub.clear();
				pub.setConnection(con);
				pub.setViewSpec("properties/ProductPublishingView");
			}
			
			
			if(currentitem < IDs.size()) {
				final String id = IDs.get(currentitem);
				storage.setParameter("TransmissionEmailID", id);
				storage.doAction(GenerationKeys.SELECT);
				
				if(!storage.isSuccessful()) {
					//throwing an exception is how the counts are incremented.
					logger.error("Could not find TransmissionEmailID {} in DB", id);
					throw new RuntimeException("Could not find TransmissionEmailID "+ id);
				}
				processItem(storage);
				logger.info("Imagine we just processed TransmissionEmailID {}", id);
				
				storage.setParameter("ProcessedDate","NOW");
				storage.doAction(GenerationKeys.UPDATE);
				
				//if processing could not be completed, add to the notification email.
				if("C".equals(storage.getParameter("Processed"))) {
					throw new RuntimeException(String.format("No rules found or not all mandatory data given for TransmissionEmailID %1$s with subject %2$s and body:\n %3$s", 
											id, storage.getString("Subject"), storage.getString("Body")));
				}
			}
		} catch (RuntimeException e) {
			logger.error("Error in processItem",e);
			throw e;
		}
	}

	private void processItem(GenRow item) {

		final String subject = StringUtils.trimToEmpty(item.getData("Subject"));
		final String body = item.getData("Body");
		final String from = item.getData("EmailFrom");
		
		if("www@realestate.com.au".equals(from) || subject.startsWith("REAXML Logs")) {
			
			boolean foundUnique = false;
			
			for(String line: body.split("\n")) {
				line = StringUtils.trimToNull(line);
				if(line == null) continue;
				if(line.startsWith("UniqueID")) {
					foundUnique = true; 
				} else if(foundUnique && !line.startsWith("===") && !line.startsWith("---")) {
					// here is the line with the info we want on it 
					// 0N1J3B488F0BMDSMXJ 112407231 UPDT                 3      9
					
					int ai = line.indexOf(" "); 
					String ID = line.substring(0, ai); line = line.substring(ai + 1); //the rest of the format is fixed-width, but the ID messes it up. 
					
					
					String agent = "", reaID = "", result = "", msg = "";
					
					try {
						agent = StringUtils.trimToNull(line.substring(0, 6)); line = line.substring(7); 
						reaID = StringUtils.trimToNull(line.substring(0, 9)); line = line.substring(10); 
						result = StringUtils.trimToNull(line.substring(0, 4)); line = line.substring(5); 
						msg = StringUtils.trimToNull(line.substring(0, 15)); line = line.substring(16);
					} catch (StringIndexOutOfBoundsException iob) {
					}
					
					if(!StringUtils.equalsIgnoreCase(agent, reaAgent)) {
						if(logger.isTraceEnabled()) logger.trace("Skipping item with id {} and agent {} as it did not match the system agent {}", new String[]{ item.getData("TransmissionEmailID"), agent, reaAgent,  });
						break;
					}
					
					if(logger.isTraceEnabled()) {
						logger.trace("Processing Item with REA Status agent={}, reaID={}, result={}, msg={}",new String[]{agent, reaID, result, msg});
					}
					
					pub.setConnection(con);
					pub.setViewSpec("properties/ProductPublishingView");
					pub.setParameter("ProductID", ID);
					pub.setParameter("PublishMethod","REA");
					logger.trace(pub.toString());
					
					pub.doAction(GenerationKeys.SELECTFIRST);
					if(!pub.isSuccessful()) {
						logger.trace("Skipping product with ID {} as it was not found", ID); 
						item.setParameter("Processed", "X"); // it's not a product from this runway
						return;
					}
					
					String ppID = pub.getData("ProductPublishingID");
					item.setParameter("ProductPublishingID", ppID);
					 
					pub.clear();
					pub.setConnection(con);
					pub.setViewSpec("properties/ProductPublishingView");
					pub.setParameter("ProductPublishingID", ppID);
					
					if(!"UPDT".equals(result)) {
						pub.setParameter("PublishStatus","Invalid");
						pub.setParameter("PublishError", result + " - Invalidated - " + msg);
						pub.setParameter("PublishURL", "EMPTY");
						
						forceRemove(ppID);
					} else {
						pub.setParameter("PublishError", "EMPTY"); 
						pub.setParameter("PublishStatus", "Published"); 
						pub.setParameter("PublishURL", "http://www.realestate.com.au/" + reaID);
					}
					 
					pub.doAction(GenerationKeys.UPDATE); 
					
					if(pub.isSuccessful()) { 
						item.setParameter("Processed","Y");
						return;
					}
					logger.error("Error updating publishing information for TransmissionEmailID {} have error {}", item.getData("TransmissionEmailID"), pub.getError());
				}
			}
			item.setParameter("Processed","I");
			return;
		} 
		item.setParameter("Processed","C");
	}
	
	public void forceRemove(String publishingID) {
		
		GenRow row = new GenRow();
		row.setTableSpec("ProductTransmission");
		row.setParameter("-select0", "TransmissionID");
		row.setParameter("-select1", "ProductID");
		row.setParameter("ProductPublishingID", publishingID);
		row.doAction("selectfirst");
		
		if (row.isSuccessful()) {
			Transmissions t = new Transmissions(InitServlet.getRealPath());
			logger.error("Error response form REA, forcing the removal of Transmission {} for product", row.getData("TransmissionID"), row.getData("ProductID"));
			t.debugTransmission(row.getString("TransmissionID"), "false", "true");
		}
	}

	private class Config implements DaemonWorkerConfig {

		@Override
		public int getInterval() { //seconds
			return TransmissionEmailMonitorDaemon.CHECK_INTERVAL;// 5 minutes
		}

		@Override
		public boolean getDebug() {
			return "true".equals(InitServlet.getSystemParam("DeveloperMode"));
		}

		@Override
		public boolean getPretend() {
			return false;
		}

		@Override
		public String getDebugAddress() {
			return null;
		}

		@Override
		public boolean isModified() {
			return false;
		}

		@Override
		public boolean hasWorkerTimer() {
			return false;
		}

		@Override
		public WorkerTimer getWorkerTimer() {
			return null;
		}	
	}
}
