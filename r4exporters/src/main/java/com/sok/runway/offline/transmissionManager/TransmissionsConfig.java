package com.sok.runway.offline.transmissionManager;

import java.io.File;
import javax.servlet.ServletContext;
import org.dom4j.*;

import com.sok.runway.*;
import com.sok.runway.offline.*;
import com.sok.framework.*;

public class TransmissionsConfig extends AbstractDaemonWorkerConfig
{
   protected static String fullpath = null;
   protected static TransmissionsConfig tramissionsconfig = null;
   protected static final String filepath = "/WEB-INF/transmissions.xml";
   
   public static TransmissionsConfig getTransmissionConfig(String parentpath)
   {
      if(tramissionsconfig == null || tramissionsconfig.isModified())
      {
         loadConfig(parentpath);
      }
      return(tramissionsconfig);
   }

   protected static synchronized void loadConfig(String parentpath)
   {
      if(tramissionsconfig == null || tramissionsconfig.isModified())
      {
         if(fullpath == null)
         {
            StringBuffer sb = new StringBuffer(parentpath);
            sb.append(filepath);
            fullpath = sb.toString();
         }
         tramissionsconfig = new TransmissionsConfig(fullpath);
      }
   }

   public TransmissionsConfig(String path)
   {
      super(path);
   }

}
