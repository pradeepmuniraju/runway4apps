package com.sok.runway.offline.transmissionManager;

import javax.servlet.ServletContext;

import com.sok.framework.DaemonFactory;
import com.sok.framework.InitServlet;
import com.sok.runway.offline.AbstractDaemonWorkerController;

public class TransmissionEmailMonitorDaemonController extends AbstractDaemonWorkerController
{
   public static String NAME = "Transmission Email Daemon";
   private final ServletContext ctx;
   public TransmissionEmailMonitorDaemonController(DaemonFactory daemonFactory)
   {
      super(daemonFactory);
      ctx = daemonFactory.getServletContext();
   }
   
   protected void initWorker()
   {
      worker = new TransmissionEmailMonitorDaemon(InitServlet.getRealPath(), ctx);
   }

   public String getName()
   {
      return (NAME);
   }
}
