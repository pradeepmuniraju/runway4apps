package com.sok.runway.crm.cms.properties.publishing;

import java.sql.Connection;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.Apartment;
import com.sok.runway.crm.cms.properties.ExistingProperty;
import com.sok.runway.crm.cms.properties.HomePlan;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.Land;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.houseandland.CreateHouseAndLand;
import com.sok.runway.offline.ThreadManager;
import com.sok.runway.offline.transmissionManager.Transmissions;

public class ProductPublishing extends GenRow {

	private final static Logger logger = LoggerFactory.getLogger(ProductPublishing.class);
	private Map<PublishMethod, Setting> settings = Collections.emptyMap();
	private Map<PublishMethod, Future<GenRow>> feeds = new HashMap<PublishMethod, Future<GenRow>>();
	private static Map<String, String> PUBLISH_OPTIONS = new HashMap<String,String>();
	static {
		PUBLISH_OPTIONS.put("Approved", "grey-tick");
		PUBLISH_OPTIONS.put("Published", "green-tick");
		PUBLISH_OPTIONS.put("Pre-release", "yellow-exclaim");
		PUBLISH_OPTIONS.put("Unpublished", "grey-exclaim");
		PUBLISH_OPTIONS.put("To be approved", "grey-exclaim");
		PUBLISH_OPTIONS.put("To be published", "grey-exclaim");
		PUBLISH_OPTIONS.put("Invalid", "red-exclaim");
		PUBLISH_OPTIONS.put("Content Required", "red-exclaim");
		PUBLISH_OPTIONS.put("Removing", "yellow-tick");
		PUBLISH_OPTIONS.put("Expired", "green-minus");
	}
	
	public static Set<String> getPublishOptions() { 
		return PUBLISH_OPTIONS.keySet();
	}
	public static String getPublishOptionClass(Setting s) {
		return getPublishOptionClass(s.status);
	}
	/* may want to change this implementation later, so lets keep it here */
	public static String getPublishOptionClass(String s) {
		return PUBLISH_OPTIONS.get(s);
	}
	
	private String productType = null;
	private String lotProductID = null;
	private String productID = null;
	private String homeID = null;
	private String estateID = null;
	private String buildingID = null;
	private String repUserID = null;
	private String summary = null;
	private String description = null;
	private String heroImageID = null;
	
	public ProductPublishing(PropertyEntity pe) { 
		if(!pe.isSet("ProductID")) {
			throw new UnsupportedOperationException("ProductID is not set in entity");
		}
		productType = pe.getString("ProductType");
		
		switch(pe.getPropertyType()) {
			case HouseLandPackage:
				HouseLandPackage hlp = ((HouseLandPackage)pe);
				if(hlp.getHomePlan() != null && hlp.getHomePlan().getHome() != null) { 
					homeID = hlp.getHomePlan().getHome().getString("ProductID");
				}
				if(hlp.getLand() != null && hlp.getLand().getEstate() != null) { 
					estateID = hlp.getLand().getEstate().getString("ProductID");
				}
				if(hlp.getLand() != null && hlp.getLand().getEstate() != null) { 
					lotProductID = hlp.getLand().getString("ProductID");
				}
				summary = hlp.getString("Summary"); // Headline comes from a diff place
				description = hlp.getString("PublishDescription");
				heroImageID = hlp.getString("Image");
				break;
			case Land:
				Land l = (Land)pe;
				if(l.getEstate() != null) { 
					estateID = l.getEstate().getString("ProductID");
				}
				summary = l.getString("Summary");
				description = l.getString("PublishDescription");
				heroImageID = l.getString("Image");
				break;
			case Apartment:
				Apartment a = (Apartment)pe;
				if(a.getBuilding() != null) {
					//TODO BUILDING IMPLEMENTATION
					buildingID = a.getBuilding().toString();
				}
				break;
			case ExistingProperty:
				ExistingProperty ep = ((ExistingProperty)pe);
				summary = ep.getString("Summary");
				description = ep.getString("PublishDescription");
				heroImageID = ep.getString("Image");
				break;
			case HomePlan:
				HomePlan plan = ((HomePlan)pe);
				homeID = plan.getString("HomeProductID");
				summary = plan.getString("HomeSummary"); // Headline comes from a diff place
				description = plan.getString("PublishDescription");
				heroImageID = plan.getString("HomeImage");
				break;
			default:
				throw new UnsupportedOperationException("Property type not supported for Publishing: " + pe.getPropertyType().name());
		}
		repUserID = pe.getSalesRep().getData("RepUserID");
		productID = pe.getString("ProductID");
		load(pe);
	} 
	
	public ProductPublishing(GenRow pe, boolean connect) {
		if(!pe.isSet("ProductID")) {
			logger.debug(pe.toString());
			throw new UnsupportedOperationException("ProductID is not set in entity");
		}
		PropertyType pt = PropertyType.getPropertyType(pe.getString("ProductType"));
		switch(pt) {
			case HouseLandPackage:
				homeID = pe.getString("HomeProductID");
				estateID = pe.getString("EstateProductID");
				lotProductID = pe.getString("LotProductID");
				break;
			case Land:
				estateID = pe.getString("EstateProductID");
				break;
			case Apartment:
				buildingID = pe.getString("BuildingProductID");
				break;
			case ExistingProperty:
				summary = pe.getString("Summary");
				description = pe.getString("PublishDescription");
				heroImageID = pe.getString("Image");
				break;
			case HomePlan:
				summary = pe.getString("Summary");
				description = pe.getString("PublishDescription");
				heroImageID = pe.getString("Image");
				break;
			default:
				throw new UnsupportedOperationException("Property type not supported for Publishing: " +pt.name());
		}
		productID = pe.getData("ProductID");
		// isSet doesn't work if the row is populated but not retrieved from the database, getData returns nothing
		if (productID == null || productID.length() == 0) productID = pe.getString("ProductID"); 
		if(connect)  {
			ActionBean.connect(pe);
		}
		load(pe);
	}
	
	public static Future<ProductPublishing> getFuturePublishing(final PropertyEntity pe) {
		 return ThreadManager.startCallable(new Callable<ProductPublishing>() {
			  public ProductPublishing call() {
				  return new ProductPublishing(pe);
			  }
		  });
	}
	
	public static Future<ProductPublishing> getFuturePublishing(final GenRow ge, final boolean connect) { 
		 return ThreadManager.startCallable(new Callable<ProductPublishing>() {
			  public ProductPublishing call() {
				  return new ProductPublishing(ge, connect);
			  }
		  });
	}
	
	public void load(GenRow pe) {
		
		setConnection(pe.getConnection());
		setParameter(ActionBean.CURRENTUSERID,pe.getString(ActionBean.CURRENTUSERID));
		setViewSpec("properties/ProductPublishingView");
		setParameter("ProductID", productID);
		setParameter("-select1", "(DATEDIFF( NOW(), FirstPublishDate)) as Age");
		//doAction(GenerationKeys.SEARCH);
		if (getString("ProductID").length() > 0) getResults(); 
		if(getNext()) { 
			settings = new HashMap<PublishMethod, Setting>();
			do {
				Setting s = this.new Setting();
				PublishMethod m = PublishMethod.valueOf(getData("PublishMethod"));
				settings.put(m, s);
				s.productPublishingID = getData("ProductPublishingID");
				s.method = m; 
				s.status = getData("PublishStatus");
				s.startDate = getDate("StartDate");
				s.endDate = getDate("EndDate");
				s.age = getString("Age");
			} while(getNext());
		}
		close();
		
		final String pid = this.productID;
		final Connection con = getConnection();
		
		for(PublishMethod pm: PublishMethod.values()) {
			final String feedName = pm.getFeedName();
			Future<GenRow> fg = ThreadManager.startCallable(new Callable<GenRow>() {
				public GenRow call() {
					logger.trace("returning genrow for call and feedname {}",feedName);
					return getDataFeedDetails(con, pid, feedName);
				}
			}); 
			logger.trace("fg shouldn't be null, is it? {}", fg == null);
			feeds.put(pm, fg);
		}
	}
	
	public GenRow getDataFeedDetails(PublishMethod pm) throws RuntimeException {
		if(!pm.hasFeed()) return null;
		if(feeds.containsKey(pm)) {
			try {
				logger.trace("trying to get the feed details from the future");
				GenRow r = feeds.get(pm).get();
				r.close();
				return r;
			} catch (ExecutionException ee) { 
				logger.error("Error computing feed", ee);
			} catch (InterruptedException e) {
				logger.error("Thread was interrupted", e);
			}
			return new GenRow();
		}
		logger.error("Feed method not implemented {}", pm.name());
		throw new UnsupportedOperationException("Feed method not yet implemented for " + pm.name());
	}
	
	public static GenRow getDataFeedDetails(HttpServletRequest request, String lotProductID, String name){
		return getDataFeedDetails(ActionBean.getConnection(request), lotProductID, name);
	}
	
	public static GenRow getDataFeedDetails(java.sql.Connection con, String lotProductID, String name){
		GenRow transmission = new GenRow();
	    transmission.setTableSpec("ProductTransmission");
	    transmission.setConnection(con);
	    transmission.setParameter("-select","*");
	    transmission.setParameter("ProductID",lotProductID);
	    transmission.setParameter("Name",name);
	    transmission.doAction(GenerationKeys.SELECTFIRST);
	    transmission.close();
	    return(transmission);
	}

	/*
	public void saveSettings(List<Setting> slist) { 
		for(Setting s: slist) { 
			saveSetting(s);
		}
	}*/
	
	public static void processPublishingStatus(HttpServletRequest request, String productID, String publishStatus, UserBean currentuser) {
		if (productID == null || productID.length() == 0) return;
		
		GenRow pp = new GenRow();
		pp.setViewSpec("properties/ProductPublishingView");
		if (request != null) pp.setRequest(request);
		
		PropertyEntity entity = null;
		try {
			entity = PropertyFactory.getPropertyEntity(request, productID);
		} catch (Exception e) {
			return;
		}

		for (PublishMethod pm : PublishMethod.values()) {
			GenRow row = new GenRow();
			row.setTableSpec("properties/ProductValidation");
			row.setRequest(request);
			row.setParameter("-select1", "*");
			row.setParameter("ProductID", productID);
			row.setParameter("PublishMethod", pm.toString());
			row.doAction(GenerationKeys.SELECTFIRST);
			
			pp.clear();
			pp.setColumn("ProductID",productID);
			pp.setColumn("PublishMethod", pm.toString());
			pp.doAction("selectfirst");
			
			CreateHouseAndLand.checkPublishAlbums(request, productID, entity.getString("Image"), pm, currentuser.getString("UserID"));
			
			if (!pp.isSuccessful()) {
				PropertyType pt = PropertyType.getPropertyType(entity.getString("ProductType"));
				switch(pt) {
					case HouseLandPackage:
						pp.setColumn("HomeProductID",entity.getString("HomeProductID"));
						pp.setColumn("EstateProductID",entity.getString("EstateProductID"));
						pp.setColumn("LotProductID",entity.getString("LotProductID"));
						break;
					case Land:
						pp.setColumn("EstateProductID",entity.getString("EstateProductID"));
						break;
					case Apartment:
						pp.setColumn("BuildingProductID",entity.getString("BuildingProductID"));
						break;
				}
				
				
				pp.setColumn("Summary",entity.getString("Summary"));
				pp.setColumn("Description",entity.getString("PublishDescription"));
				pp.setColumn("HeroImageID",entity.getString("Image"));
				
				pp.setColumn("PublishStatus",publishStatus);
				pp.setColumn("ProductPublishingID",KeyMaker.generate());
				pp.setParameter("CreatedDate","NOW");
				pp.setParameter("CreatedBy",currentuser.getString("UserID"));
				pp.setAction("insert");
				
			} else {
				pp.setAction("update");
				if(pm.getFeedHost() != null) {
					String productPublishingID = pp.getString("ProductPublishingID");
					if (productPublishingID == null || productPublishingID.length() == 0) productPublishingID = "NOTHINGTOSEEHERE";
					GenRow transmission = new GenRow();
					transmission.setViewSpec("ProductTransmissionView");
					transmission.setRequest(request);
					transmission.setParameter("ProductPublishingID",productPublishingID);
					transmission.doAction("selectfirst");
					if (!transmission.isSuccessful()) {
						transmission.remove("ProductPublishingID");
						transmission.setParameter("ProductID",pp.getString("ProductID"));
						transmission.setParameter("Name",pm.getFeedName());
						transmission.doAction("selectfirst");
					}
					if (transmission.isSuccessful()) {
						String transmissionID = transmission.getString("TransmissionID");
						transmission.clear();
						transmission.setParameter("TransmissionID",transmissionID);
						if(StringUtils.isNotBlank(pp.getString("StartDate"))) {
							transmission.setParameter("StartDate",pp.getString("StartDate"));
						}
						if(StringUtils.isNotBlank(pp.getString("EndDate"))) {
							transmission.setParameter("EndDate",pp.getString("EndDate"));
						}
						if ("Approved".equals(publishStatus)) {
							//if ("N".equals(transmission.getString("Active"))) s.setStatusIfChanged("Pre-release");
							transmission.setParameter("Active","Y");
							transmission.setParameter("MarkForDelete","N");
						} else if ("Withdrawn".equals(publishStatus) || "Pending".equals(publishStatus)) {
							transmission.setParameter("MarkForDelete","Y");
						}
						transmission.setParameter("ModifiedDate","NOW");
						transmission.setParameter("ModifiedBy",currentuser.getString("UserID"));
						transmission.doAction("update");
					}
				}
			}
			pp.setColumn("PublishStatus",publishStatus);
			pp.setParameter("ModifiedDate","NOW");
			pp.setParameter("ModifiedBy",currentuser.getString("UserID"));
			if (("insert".equals(pp.getAction()) && "Approved".equals(pp.getString("PublishStatus")))) {
				try {
					pp.doAction();
				} catch (Exception e) {
					pp.setAction("update");
				}
			}
			if ("update".equals(pp.getAction())) {
				pp.doAction();
			}
		}
	}
	
	public static int getPublishedPackages(HttpServletRequest request, String limitProductID, String limitType, PublishMethod pm) {
		if (StringUtils.isBlank(limitType) || StringUtils.isBlank(limitProductID)) return 0;
		GenRow publishedPackages = new GenRow();

		publishedPackages.setRequest(request);
		publishedPackages.setTableSpec("Products");
		publishedPackages.setParameter("-join0", "ProductProductPublishing");

		if ("Lot".equals(limitType)) {
			publishedPackages.setParameter("-join1", "ProductProductProducts#Land.ProductProductsLinkedProductLand");
			publishedPackages.setParameter("ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductID", limitProductID);
		} else if ("Range".equals(limitType)) {
			publishedPackages.setParameter("ProductProductProducts#House.ProductProductsLinkedProductHouse.ProductParentHomeDesign.ProductVariationHomeDesign.ProductParentHomeRange.ProductVariationHomeRange.ProductID", limitProductID);
			publishedPackages.setParameter("-join1", "ProductProductProducts#House.ProductProductsLinkedProductHouse.ProductParentHomeDesign.ProductVariationHomeDesign.ProductParentHomeRange.ProductVariationHomeRange");
		} else {
			return 0;
		}

		publishedPackages.setParameter("ProductProductPublishing.PublishMethod", pm.getName());
		publishedPackages.setParameter("ProductProductPublishing.PublishStatus", "Pre-release+Published");
		publishedPackages.setParameter("-select0", "count(*) as Count");
		publishedPackages.doAction("selectfirst");
		logger.trace("getPublishedPackages query is {}", publishedPackages.getStatement());

		return (publishedPackages.getInt("Count"));
	}
	
	
	/**
	 * @param request
	 * @param entities
	 * @param limit
	 * @param limitType
	 * @param basedOnDates : true if you want to valid only for PM for which the user has entered
	 * @return
	 */
	public static boolean hasPackagesExceedingLimit(HttpServletRequest request, List<PropertyEntity> entities, int limit, String limitType, boolean basedOnDates) {
		/* Map with lotProductid and PM as the key */
		HashMap<String, Integer> lotPMMap = new HashMap<String, Integer>();

		for (PropertyEntity entity : entities) {			
			String limitProductID = entity.getString("LotProductID");
			if("Range".equals(limitType)) {
				limitProductID = entity.getString("RangeProductID");
			} else if("Runway".equals(limitType)) {
				limitProductID = "Runway";
			}
			
			String publishingStatus = entity.getString("PublishingStatus");
			
			// If the packages already published don't bother
			if (!("Pre-release".equals(publishingStatus) || "Published".equals(publishingStatus))) {
				logger.debug("publishingStatus: [{}]",publishingStatus);
				
				for (PublishMethod pm : PublishMethod.getVisibleLotLimitMethods()) {
					logger.debug("checking for : [{}]",pm);
					if(isAttemptingToPublish(request , basedOnDates, pm)) {
						
						logger.debug("isAttemptingToPublish true");
						
						String key = limitProductID + pm.getName();
						Integer currentCount = lotPMMap.get(key);
	
						if (currentCount == null) {
							// increment count from the DB as it does not include this package
							currentCount = new Integer(ProductPublishing.getPublishedPackages(request, limitProductID, limitType, pm) + 1);
						} else {
							currentCount++;
						}
						
						logger.debug("type : [" + limitType + "] key  : [{}] , currentCount  : [{}]",key , currentCount);
						lotPMMap.put(key, currentCount);
					}
				}
			}
		}
		
		for (Entry<String, Integer> entry : lotPMMap.entrySet()) {
			Integer count = entry.getValue();
			
			logger.debug("maxPackagePerLot  : [{}]",limit);
			
			logger.debug("count  : [{}] , currentCount  : [{}]",count , entry.getKey());
			
			if (count > limit)
				return true;
		}
		
		
		return false;
	}
	
	/** Uses start date to check if the user is intending to publish to lot limit method. 
	 * @param request
	 * @param basedOnDates
	 * @param method
	 * @return
	 */
	private static boolean isAttemptingToPublish(HttpServletRequest request, boolean basedOnDates, PublishMethod method) {
		if (basedOnDates) {
			final int field = "Y".equals(request.getParameter("-sameDates")) ? 0 : method.ordinal();
			
			logger.debug("checking for field name: [{}]",field);
			
			String startDate = request.getParameter("StartDate" + field);
			
			logger.debug("checking for startDate: [{}]", startDate);
			
			return StringUtils.isNotBlank(startDate);
			
		} else {
			return true;
		}
	}
	public void disablePublish(PublishMethod method) {
		
		if(settings.containsKey(method)) {
			Setting s = settings.get(method);
			
			final String currentUserID = StringUtils.trimToNull(getString(ActionBean.CURRENTUSERID));
			clear();
			setParameter(ActionBean.CURRENTUSERID,currentUserID);
			
			setParameter("ProductID", productID);
			setParameter("ProductPublishingID",s.productPublishingID);
			setParameter("StartDate","01/01/1970"); //set them so that they are not valid, we're not allowing nulls..
			setParameter("EndDate","01/01/1970");
			setParameter("ModifiedDate","NOW");
			setParameter("ModifiedBy",currentUserID);
			doAction(GenerationKeys.UPDATE);
			
			GenRow settingHistory = new GenRow();
			settingHistory.setConnection(getConnection());
			settingHistory.setViewSpec("properties/ProductPublishingHistoryView");
			settingHistory.setParameter(GenerationKeys.UPDATEON + "ProductPublishingID",s.productPublishingID);
			settingHistory.setParameter(GenerationKeys.UPDATEON + "Override","N");
			settingHistory.setParameter("Override","Y");
			settingHistory.setParameter("ModifiedDate","NOW");
			settingHistory.setParameter("ModifiedBy",currentUserID);
			settingHistory.doAction(GenerationKeys.UPDATEALL);
		}
	}
	
	public boolean saveSetting(Setting s, String repUserID) {
		if(!s.isValid() && !"Expired".equalsIgnoreCase(s.getStatus())) { 
			throw new RuntimeException("Cannot save, setting is not valid");
		}
		if(!isSet("ProductID")) {
			throw new RuntimeException("ProductID is not set");
		}
		
		String currentUserID = StringUtils.trimToNull(getString(ActionBean.CURRENTUSERID));
		
		// Ideally this should never happen 
		if(StringUtils.isBlank(currentUserID))
			currentUserID = "system";
			
		logger.trace("Current user: [{}]",currentUserID);
		logger.trace("ToString: []",toString());
		
		GenRow settingHistory = new GenRow();
		settingHistory.setConnection(getConnection());
		settingHistory.setViewSpec("properties/ProductPublishingHistoryView");
		
		GenRow setting = new GenRow();
		setting.setConnection(getConnection());
		setting.setViewSpec("properties/ProductPublishingView");	
		
		if(s.method.hasFeed()) { 
			//if(buildingID != null)  {
				//not configurable for apartments 
				//s.status = "Invalid";
				//s.statusMessage = "not_configurable";
			//}else
			if(!s.method.isFeedConfigured()) { 
				s.status = "Invalid";
				s.statusMessage = "not_configured";
			} else {
				GenRow feed = getDataFeedDetails(setting.getConnection(),  getString("ProductID"), s.method.getFeedName());
				if(!feed.isSuccessful()) { 
					//s.status = "Invalid";
					//s.statusMessage = "no_feed";
				} else {
 					if (s.startDate != null) feed.setParameter("StartDate", s.startDate);
 					if (s.endDate != null) feed.setParameter("EndDate", s.endDate);
					if("Published".equals(s.status) || "Pre-release".equals(s.status)) {
						if ("N".equals(feed.getString("Active"))) s.status = "Pre-release";
						feed.setParameter("Active","Y");
						feed.setParameter("MarkForDelete","N");
					} else if ("Expired".equals(s.status)) {
						feed.setParameter("MarkForDelete","Y");
						if ("Y".equals(feed.getString("Active"))) {
							s.status = "Removing";
							s.statusMessage = "This feed will be removed at the next update";
						}
					}
					try {
						feed.doAction(GenerationKeys.UPDATE);
					} catch (Exception e) {}
				}
			}
		}
		
		/* find if there is a currently active setting */
		if(settings.containsKey(s.method)) {
			
			Setting e = settings.get(s.method);
			if(e.equals(s)) {
				//no change
				logger.debug("no changes in setting for {}, returning", s.method.name());
				return false;
			}			
			/* nothing exists, lets create */
			setting.setParameter("ProductPublishingID",e.productPublishingID);
			setting.doAction(GenerationKeys.SELECT);

			if(!setting.isSuccessful()) { 
				logger.error("no publishing id for update Orig [{}] Setting [{}]", new String[]{getString("ProductID"), s.productPublishingID});
				throw new RuntimeException("somehow productid not found for update");
			}
			if(!setting.getString("ProductID").equals(getString("ProductID"))) {
				logger.error("non-matching productids Orig [{}] Setting[{}] for Setting [{}]", new String[]{getString("ProductID"), setting.getString("ProductID"),s.productPublishingID});
				throw new RuntimeException("ProductIDs did not match");
			}
			if (s.status == null || s.status.length() == 0) s.status = e.status;
			setting.setAction(GenerationKeys.UPDATE);

			s.productPublishingID = setting.getString("ProductPublishingID");
			
		} else {
			// check to see if its in the database and we do not know about it
			setting.setParameter("PublishMethod",s.method);
			setting.setParameter("ProductID",getString("ProductID"));
			setting.doAction(GenerationKeys.SELECTFIRST);
			
			if (!setting.isSuccessful()) {
				setting.setParameter("ProductPublishingID", KeyMaker.generate());
				setting.setParameter("CreatedDate","NOW");
				setting.setParameter("CreatedBy",currentUserID);
				setting.setAction(GenerationKeys.INSERT);
			} else if (s.status == null || s.status.length() == 0) {
				s.status = setting.getString("PublishStatus");
			}
			s.productPublishingID = setting.getString("ProductPublishingID");
		}
		

		if(s.method.hasFeed()) {
			/*if(buildingID != null) { 
				s.status = "Invalid";
				s.statusMessage = "not_configurable";
			} else */ 
			if(Transmissions.isConfigured(s.method)) { 
				GenRow feed = getDataFeedDetails(s.method);
				if(feed.isSuccessful()) { 
					//feed exists 
					feed.setParameter("StartDate", s.startDate);
					feed.setParameter("EndDate", s.endDate);
					feed.setParameter("ModifiedDate","NOW");
					feed.setParameter("ModifiedBy",currentUserID);
					feed.doAction(GenerationKeys.UPDATE);
				} else { 
					//s.status = "Invalid";
					//s.statusMessage = "no_feed";
				}
			} else {
				s.status = "Invalid";
				s.statusMessage = "not_configured";
			}
		} 
		
		setting.setParameter("PublishMethod",s.method.name());
		setting.setParameter("ProductID", getString("ProductID"));
		setting.setParameter("BuildingProductID",buildingID);
		setting.setParameter("HomeProductID",homeID);
		setting.setParameter("EstateProductID",estateID);
		setting.setParameter("LotProductID", lotProductID);
		setting.setParameter("StartDate", s.startDate);
		setting.setParameter("EndDate", s.endDate);
		if ("insert".equals(setting.getAction()) || (s.status != null && s.status.length() > 0)) setting.setParameter("PublishStatus",s.status); //used for reports, etc.
		
		// Insert occurs here only in case of EP
		if ("insert".equals(setting.getAction())) {
			if(StringUtils.isBlank(s.status)) {
				// IN case of EP, Status can be blank during insert
				setting.setParameter("PublishStatus", "Approved");				
			}
			setting.setParameter("Summary", summary);
			setting.setParameter("Description", description);
			setting.setParameter("HeroImageID", heroImageID);
		} else if("update".equals(setting.getAction()) && "Home Plan".equals(productType)) {
			// Update each time so we have the latest info here fro plans
			setting.setParameter("Summary", summary);
			setting.setParameter("Description", description);
			setting.setParameter("HeroImageID", heroImageID);
		}
		
		setting.setParameter("ModifiedDate","NOW");
		setting.setParameter("ModifiedBy",currentUserID);
		setting.doAction();
		
		s.productPublishingID = setting.getString("ProductPublishingID");

		setting.close();
		
		if("update".equals(setting.getAction())) {
			//disable previous actions
			settingHistory.setParameter(GenerationKeys.UPDATEON + "ProductPublishingID",setting.getString("ProductPublishingID"));
			settingHistory.setParameter(GenerationKeys.UPDATEON + "Override","N");
			settingHistory.setParameter("Override","Y");
			settingHistory.setParameter("ModifiedDate","NOW");
			settingHistory.setParameter("ModifiedBy",currentUserID);
			settingHistory.doAction(GenerationKeys.UPDATEALL);
			
			settingHistory.clear();
		}
		
		settingHistory.setParameter("ProductPublishingHistoryID", KeyMaker.generate());
		settingHistory.setParameter("ProductPublishingID", setting.getString("ProductPublishingID"));
		settingHistory.setParameter("Status", s.status);
		settingHistory.setParameter("RepUserID", repUserID);
		settingHistory.setParameter("CreatedDate","NOW");
		settingHistory.setParameter("CreatedBy",currentUserID);
		settingHistory.setParameter("StartDate", s.startDate);
		settingHistory.setParameter("EndDate", s.endDate);
		settingHistory.setParameter("ModifiedDate","NOW");
		settingHistory.setParameter("ModifiedBy",currentUserID);
		settingHistory.doAction(GenerationKeys.INSERT);
		
		settingHistory.close();
		if(!setting.isSuccessful()) {
			throw new RuntimeException("data failed to save");
		}
		
		//if we're reusing objects, enable/test this
		//settings.put(s.method, s);
		//updated / saved
		return true;
	}
	
	public Setting getSetting(PublishMethod method) {
		if(settings.containsKey(method)) {
			Setting s = settings.get(method); 
			if(s.method.hasFeed()) {
				/*if(buildingID != null) { 
					s.status = "Invalid";
					s.statusMessage = "not_configurable";
				} else */ 
				if(Transmissions.isConfigured(s.method)) { 
					GenRow feed = getDataFeedDetails(s.method);
					if(!feed.isSuccessful()) { 
						//s.status = "Invalid";
						s.statusMessage = "no_feed";
					}
				} else {
					s.status = "Invalid";
					s.statusMessage = "not_configured";
				}
			} 
			return s;
		}
		return new Setting(method, "To be approved");
	}
	
	public static String getStatusMessage(HttpServletRequest request, String productPublishingID, String productID, PublishMethod method) {
		String statusMessage = "To be approved";
		if (StringUtils.isNotBlank(productPublishingID)) {
			if (method.hasFeed()) {
				if (Transmissions.isConfigured(method)) {
					GenRow feed = getDataFeedDetails(request, productID, method.getName());
					if (!feed.isSuccessful()) {
						statusMessage = "no_feed";
					}
				} else {
					statusMessage = "not_configured";
				}
			}
		}
		return statusMessage;
	}
	
	/* NB, if settings are to be used in a multi-threaded process then these formats will need to go into the settings objects (or be synchronised) */
	private java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
	private java.text.DateFormat dtf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");
	private java.text.DateFormat tf = new java.text.SimpleDateFormat("HH:mm");
	
	public class Setting {
		PublishMethod method = null;
		String status = null, statusMessage = "", productPublishingID, repUserID;
		Date startDate = null, endDate = null;
		String age = "";
		
		private Setting(PublishMethod method, String status) { 
			this.method = method;
			this.status = status;
		}
		
		public Setting() { }
		
		public String getPublishingID() {
			return this.productPublishingID;
		}
		
		public PublishMethod getMethod() {
			return this.method;
		}
		public String getStatus() {
			return this.status;
		}
		
		public String getStatusMessage() {
			return statusMessage;
		}
		public boolean setStatusIfChanged(String status) { 
			String origStatus = this.status;
			this.status = status;
			return origStatus == null ? status != null : !origStatus.equals(status);
		}
		/**
		 * @throws IllegalArgumentException if the method cannot be matched
		 */
		public Setting(String method, HttpServletRequest request) throws IllegalArgumentException {
			this.method = PublishMethod.valueOf(method);
			parseRequest(request);
		}
		
		public Setting(PublishMethod method, HttpServletRequest request) {
			logger.debug("new Setting for method {}",method.name());
			this.method = method;
			parseRequest(request);
		}
		
		public void parseRequest(HttpServletRequest request)  {
			String d = null, t = null; 
			final int field = "Y".equals(request.getParameter("-sameDates")) ? 0 : method.ordinal();
			try {
				logger.debug("Parsing for {}, Start [{} {}], End [{} {}]", new String[]{method.name(), 
						request.getParameter("StartDate" + field),
						request.getParameter("StartTime" + field),
						request.getParameter("EndDate" + field),
						request.getParameter("EndTime" + field)
				});
				if((d = StringUtils.trimToNull(request.getParameter("StartDate" + field))) != null
						&& (t = StringUtils.trimToNull(request.getParameter("StartTime" + field))) != null) {
					logger.debug("parsing date {} with time {}",d,t);
					startDate = dtf.parse(new StringBuilder(16).append(d).append(" ").append(t).toString());
					logger.debug("start date parsed to {}",startDate.toString());
				} else if (d != null) {
					logger.debug("parsing date {} without time",d);
					startDate = df.parse(d);
					logger.debug("start date parsed to {}",startDate.toString());
				}
				if((d = StringUtils.trimToNull(request.getParameter("EndDate" + field))) != null
						&& (t = StringUtils.trimToNull(request.getParameter("EndTime" + field))) != null) {
					logger.debug("parsing date {} with time {}",d,t);
					endDate = dtf.parse(new StringBuilder(16).append(d).append(" ").append(t).toString());
					logger.debug("end date & time parsed to {}",endDate.toString());
				} else if (d != null) {
					logger.debug("parsing date {} without time",d);
					endDate = df.parse(d);
					logger.debug("end date parsed to {}",endDate.toString());
				}
				this.status = checkStatus();
				this.repUserID = StringUtils.trimToNull(request.getParameter("RepUserID"));
				
			} catch (ParseException pe) { 
				logger.error("Error parsing dates in Setting ",pe);
				startDate = null;
				endDate = null;
			}
		}

		public String checkStatus() { 
			if(startDate == null) { 
				return "Unpublished";
			}
			Date d = new Date();
			if(startDate.after(d)) {
				return "Pre-release";
			} 
			if(endDate == null || endDate.after(d)) { 
				return "Published";
			} else {
				return "Expired";
			}
		}
		
		public boolean isValid() {
			return startDate != null;
		}
		public void setStart(Date date) { 
			startDate = date;
		}
		public void setEnd(Date date) { 
			endDate = date;
		}
		public String getStart() { 
			return startDate != null ? dtf.format(startDate): ActionBean._blank;
		}
		public String getEnd() {
			return endDate != null ? dtf.format(endDate): ActionBean._blank;
		}
		public String getStartDate() { 
			return startDate != null ? df.format(startDate): ActionBean._blank;
		}
		public String getEndDate() {
			return endDate != null ? df.format(endDate): ActionBean._blank;
		}
		public String getStartTime() {
			return startDate != null ? tf.format(startDate): ActionBean._blank;
		}
		public String getEndTime() {
			return endDate != null ? tf.format(endDate): ActionBean._blank;
		}
		
		@Override
		public boolean equals(Object o) {
			if(!(o instanceof Setting)) {
				return false;
			}
			Setting s = (Setting)o;
			
			if(s.method != this.method) {
				return false;
			}
			if((this.startDate != null && !this.startDate.equals(s.startDate)) || this.startDate == null && s.startDate != null) { 
				return false;
			}
			if((this.endDate != null && !this.endDate.equals(s.endDate)) || this.endDate == null && s.endDate != null) { 
				return false;
			}
			//if(!this.status.equals(s.status)) { 
//				return false;
			//}
			return true;
		}

		public String getProductPublishingID() {
			return productPublishingID;
		}

		public String getAge() {
			return age;
		}
	}
}