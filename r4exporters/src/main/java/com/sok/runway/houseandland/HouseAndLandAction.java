package com.sok.runway.houseandland;

import java.io.File;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory; 

import com.sok.exporters.realestate.RealEstateMapper;
import com.sok.exporters.realestate.RealEstateValidator;
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.RowBean;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.publishing.ProductPublishing;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;
import com.sok.runway.offline.rpmManager.RPMtask;
import com.sok.service.crm.cms.properties.DripService;
import com.sok.service.crm.cms.properties.DripService.DripProduct;

public class HouseAndLandAction {

	private static final Logger log = LoggerFactory.getLogger(HouseAndLandAction.class);
	
	private int			 validationMax = 0;
	private int			 validationCurrent = 0;

	public boolean clearSelection(HttpServletRequest request) {
		ArrayList<String> pids = getSelectedPackagesFromSession(request);
		pids.clear();
		log.debug(" total packages selected " + pids.size());
		return true;
	}

	public boolean packageSelection(HttpServletRequest request) {
		String pid = (String) request.getParameter("productid");
		String checked = (String) request.getParameter("checked");
		ArrayList<String> pids = getSelectedPackagesFromSession(request);
		if (checked.trim().equalsIgnoreCase("true")) {
			add(pids, pid);
		} else {
			remove(pids, pid);
		}
		log.debug(" total packages selected " + pids.size());
		return true;
	}

	private void remove(ArrayList<String> list, String val) {
		for (String s : list) {
			if (s.trim().equals(val)) {
				list.remove(s);
				break;
			}
		}
	}

	private void add(ArrayList<String> list, String val) {
		boolean found = false;
		for (String s : list) {
			if (s.trim().equals(val)) {
				found = true;
				break;
			}
		}
		if (!found) {
			list.add(val);
		}
	}

	public boolean packageSelectionMultiple(HttpServletRequest request) {
		String pid = (String) request.getParameter("productid");
		String checked = (String) request.getParameter("checked");
		ArrayList<String> pidlist = getSelectedPackagesFromSession(request);
		ArrayList<String> pidTokenList = new ArrayList<String>();
		if (pid != null && pid.trim().length() > 0) {
			StringTokenizer tk = new StringTokenizer(pid, "#");
			while (tk.hasMoreTokens()) {
				String token = tk.nextToken();
				if (!token.trim().equals("#")) {
					pidTokenList.add(token);
				}
			}
		} else {
			for (String s : pidlist) {
				pidTokenList.add(s);
			}
		}
		if (checked.trim().equalsIgnoreCase("true")) {
			for (String p : pidTokenList) {
				add(pidlist, p);
			}
		} else {
			for (String p : pidTokenList) {
				remove(pidlist, p);
			}
		}
		log.debug("total selected " + pidlist.size());
		return true;
	}

	public ArrayList<String> getSelectedPackagesFromSession(HttpServletRequest request) {
		ArrayList[] objs = (ArrayList[]) request.getSession().getAttribute("com.sok.runway.houseandland.HouseAndLandAction");
		if (objs == null) {
			objs = new ArrayList[2];
			objs[0] = (ArrayList) request.getSession().getAttribute("-selectedhouseandlandproductids");
			objs[1] = new ArrayList<String>();
			request.getSession().setAttribute("com.sok.runway.houseandland.HouseAndLandAction", objs);
		} else {
			if (objs[0] != request.getSession().getAttribute("-selectedhouseandlandproductids")) {
				objs[0] = (ArrayList) request.getSession().getAttribute("-selectedhouseandlandproductids");
				objs[1] = new ArrayList<String>();
			}
		}
		return (ArrayList<String>) objs[1];
	}

	public ArrayList<String> getUnselectedPackagesFromSession(HttpServletRequest request) {
		ArrayList<String> selectedlist = getSelectedPackagesFromSession(request);
		ArrayList<String> all = (ArrayList<String>) request.getSession().getAttribute("-selectedhouseandlandproductids");
		ArrayList<String> unselected = new ArrayList<String>();
		if (all != null) {
			for (String s : all) {
				boolean found = false;
				for (String r : selectedlist) {
					if (r.trim().equalsIgnoreCase(s)) {
						found = true;
						break;
					}
				}
				if (!found) {
					unselected.add(s);
				}
			}
			log.debug("total unselected " + unselected.size() + " all " + all.size() + " selected " + selectedlist.size());
		}
		return unselected;
	}

	public int getTotalSelected(HttpServletRequest request) {
		return getSelectedPackagesFromSession(request).size();
	}

	public ArrayList<String> updateRightPanel(HttpServletRequest request) {
		ArrayList<String> errors = new ArrayList<String>();
		Date prereleaseDate = convertToDate(request.getParameter("prereleaseDate"), request.getParameter("select_Prerelease"));
		if (prereleaseDate == null) {
			errors.add("PRE-RELEASE date/time is mandatory");
		}
		Date releaseDate = convertToDate(request.getParameter("releaseDate"), request.getParameter("select_Release"));
		if (releaseDate == null) {
			errors.add("RELEASE date is mandatory");
		}
		Date removeDate = convertToDate(request.getParameter("removeDate"), request.getParameter("select_Remove"));
		if (StringUtils.isNotBlank(request.getParameter("removeDate")) && removeDate == null) {
			errors.add("Invalid REMOVE date");
		}

		Date realEstateDate = convertToDate(request.getParameter("realEstateDate"), request.getParameter("select_RealEstate"));
		if (StringUtils.isNotBlank(request.getParameter("realEstateDate")) && realEstateDate == null) {
			errors.add("Invalid REALESTATE date");
		}

		Date domainDate = convertToDate(request.getParameter("domainDate"), request.getParameter("select_Domain"));
		if (StringUtils.isNotBlank(request.getParameter("domainDate")) && domainDate == null) {
			errors.add("Invalid DOMAIN date");
		}

		String productStatus = request.getParameter("productStatus");
		if (StringUtils.isBlank(productStatus)) {
			errors.add("STATUS is mandatory");
		}
		String productActive = request.getParameter("productActive");
		if (releaseDate != null && prereleaseDate != null) {
			if (prereleaseDate.compareTo(releaseDate) > 0) {
				errors.add("PRE-RELEASE date must be prior to RELEASE date");
			}
		}
		if (removeDate != null && releaseDate != null) {
			if (releaseDate.compareTo(removeDate) > 0) {
				errors.add("RELEASE date must be prior to REMOVE date");
			}
		}
		String group = request.getParameter("select_Group");
		String salesRep = request.getParameter("select_SalesRep");
		if (StringUtils.isBlank(group)) {
			//	    if (StringUtils.isBlank(salesRep)) {
			errors.add("GROUP is mandatory");
			//	    }
		}
		String packageStatus = request.getParameter("PackageStatus");
		
		if (errors.size() == 0) {
			updateProduct(prereleaseDate, releaseDate, removeDate, productActive, packageStatus, request);
			updateSalesRep(request);
			// must be done last as it updates the QuickIndex as well
			updateProductStatus(request);
		}
		return errors;
	}

	private void updateSalesRep(HttpServletRequest request) {
		UserBean currentUser = (UserBean) request.getSession().getAttribute("currentuser");
		String group = request.getParameter("select_Group");
		String salesRep = request.getParameter("select_SalesRep");
		String productStatus = request.getParameter("productStatus");
		ArrayList<String> pidTokenList = new ArrayList<String>();
		if (group != null && group.length() > 0) {
			StringTokenizer tk = new StringTokenizer(request.getParameter("productIds"), "#");
			while (tk.hasMoreTokens()) {
				String token = tk.nextToken();
				if (!token.trim().equals("#")) {
					pidTokenList.add(token);
				}
			}
			for (String s : pidTokenList) {
				GenRow row = new GenRow();
				row.setTableSpec("ProductSecurityGroups");
				row.setParameter("ProductID", s);
				row.setParameter("RepUserID", salesRep);
				row.setParameter("GroupID", group);
				row.setParameter("CreatedDate", "NOW");
				row.setParameter("CreatedBy", currentUser.getString("UserID"));
				row.setToNewID("ProductSecurityGroupID");
				row.setAction(ActionBean.INSERT);
				/*
				 * Never worked, foreign key fields do not exist in table. 
				RowBean status = new RowBean();
				status.setRequest(request);
				status.setViewSpec("ProductSecurityGroupStatusView");
				status.setToNewID("GroupStatusID");
				status.setColumn("ProductSecurityGroupID",row.getString("ProductSecurityGroupID"));
				status.setColumn("StatusID",productStatus);
				status.setAction(ActionBean.INSERT);
				status.doAction(true);
				row.setColumn("ProductSecurityGroupStatusID", status.getString("GroupStatusID"));
				*/
				row.doAction();
			}
			log.debug("total " + pidTokenList.size() + " products are updated with the sales rep");
		} else {
			log.debug("sales rep is not selected skipping updation");
		}
	}

	private void updateProduct(Date prereleaseDate, Date releaseDate, Date removeDate, String productActive, String packageStatus, HttpServletRequest request) {
		UserBean currentUser = (UserBean) request.getSession().getAttribute("currentuser");

		ArrayList<String> pidTokenList = new ArrayList<String>();
		StringTokenizer tk = new StringTokenizer(request.getParameter("productIds"), "#");
		while (tk.hasMoreTokens()) {
			String token = tk.nextToken();
			if (!token.trim().equals("#")) {
				pidTokenList.add(token);
			}
		}
		for (String productId : pidTokenList) {
			GenRow row = new GenRow();
			row.setTableSpec("Products");
			row.setRequest(request);
			row.setParameter("AvailableDate", releaseDate);
			row.setParameter("PreAvailableDate", prereleaseDate);
			if (removeDate != null) {
				row.setParameter("ExpiryDate", removeDate);
			} else {
				row.setParameter("ExpiryDate", null);
			}
			if (StringUtils.isNotBlank(productActive)) {
				row.setParameter("Active", productActive);
			} else {
				row.setParameter("Active", "N");
			}
			
			if(StringUtils.isNotBlank(packageStatus)) {
				row.setParameter("PackageStatus", packageStatus);
				
				ProductPublishing.processPublishingStatus(request, productId, packageStatus, currentUser);
			}
			
			row.setParameter("ProductID", productId);
			row.doAction(ActionBean.UPDATE);
		}
		log.debug("total " + pidTokenList.size() + " products are updated successfuly");
	}

	private void updateProductStatus(HttpServletRequest request) {

		ArrayList<String> pidTokenList = new ArrayList<String>();
		StringTokenizer tk = new StringTokenizer(request.getParameter("productIds"), "#");
		while (tk.hasMoreTokens()) {
			String token = tk.nextToken();
			if (!token.trim().equals("#")) {
				pidTokenList.add(token);
			}
		}

		updateProductStatus(pidTokenList, request);

	}

	private void updateProductStatus(ArrayList<String> pidTokenList, HttpServletRequest request) {
		UserBean currentUser = (UserBean) request.getSession().getAttribute("currentuser");
		for (String productId : pidTokenList) {
			GenRow statusRow = new GenRow();
			String productStatusId = KeyMaker.generate();
			statusRow.setTableSpec("ProductStatus");
			statusRow.setRequest(request);
			statusRow.setParameter("ProductID", productId);
			statusRow.setParameter("CreatedDate", new Date());
			statusRow.setParameter("CreatedBy", currentUser.getUserID());
			statusRow.setParameter("CreatedDate", new Date());
			statusRow.setParameter("StatusID", request.getParameter("productStatus"));
			statusRow.setParameter("ProductStatusID", productStatusId);
			statusRow.doAction(ActionBean.INSERT);

			GenRow row = new GenRow();
			row.setTableSpec("Products");
			row.setRequest(request);
			row.setParameter("ProductID", productId);
			row.setParameter("ProductStatusID", productStatusId);
			row.doAction(ActionBean.UPDATE);

			if (RPMtask.getInstance() != null) RPMtask.getInstance().doIndexRefreash("update", productId, "House and Land");
		}
		log.debug("total " + pidTokenList.size() + " product status are updated successfuly");
	}
	
	
	
	private void updateHeadline(ArrayList<String> pidTokenList, HttpServletRequest request) {
		updateHeadline(ActionBean.getConnection(request), pidTokenList, request.getParameter("-headline") );
	}
	
	public static void updateHeadline(Connection con, ArrayList<String> pidTokenList, String headline) {
		for (String productId : pidTokenList) {
			
			GenRow row = new GenRow();
			row.setViewSpec("properties/ProductHouseAndLandView");
			row.setConnection(con);
			row.setParameter("ProductID", productId);
			if (headline != null && headline.length() > 0) {
				if (headline.indexOf("${") >= 0) {
					row.doAction(GenRow.SELECTFIRST);
					System.out.println(row.getStatement());
					if (row.isSuccessful()) {
						populateLand(row);
						populateFacade(row);
						populateDrips(row);
						while (headline.indexOf("${") >= 0) {
							int start = headline.indexOf("${");
							int end = headline.indexOf("}");
							if (start < end && start != -1 && end != -1) {
								String tag = headline.substring(start + 2,end);
								String value = row.getString(tag);
								
								value = value.replaceAll("\\$", "DOLLAR_SIGN");
								headline = headline.replaceAll((new StringBuilder("\\$\\{")).append(tag).append("\\}").toString(), value);
								headline = headline.replaceAll("DOLLAR_SIGN", "\\$");
							} else {
								break;
							}
						}
					}
				}
				row.setParameter("Summary", headline);
				row.doAction(ActionBean.UPDATE);
			}
		}
		log.debug("total " + pidTokenList.size() + " product status are updated headline");
	}
	
	public static String evaluateHeadline(HttpServletRequest request, String houseandlandproductid, String headline) {
		return evaluateHeadline(ActionBean.getConnection(request), houseandlandproductid, headline);
	}
	
	public static String evaluateHeadline(Connection conn, String houseandlandproductid, String headline) {
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHouseAndLandView");
		row.setConnection(conn);
		row.setParameter("ProductID", houseandlandproductid);
		if (headline != null && headline.length() > 0) {
			if (headline.indexOf("${") >= 0) {
				row.doAction(GenRow.SELECTFIRST);
				System.out.println(row.getStatement());
				if (row.isSuccessful()) {
					populateLand(row);
					populateFacade(row);
					populateDrips(row);
					populateProductDetailsMap(row);
					while (headline.indexOf("${") >= 0) {
						int start = headline.indexOf("${");
						int end = headline.indexOf("}");
						if (start < end && start != -1 && end != -1) {
							String tag = headline.substring(start + 2, end);
							String value = row.getString(tag);
							value = value.replaceAll("\\$", "DOLLAR_SIGN");
							headline = headline.replaceAll((new StringBuilder("\\$\\{")).append(tag).append("\\}").toString(), value);
							headline = headline.replaceAll("DOLLAR_SIGN", "\\$");
						} else {
							break;
						}
					}
				}
			}
		}
		return headline;
	}
	
	private static void populateDrips(GenRow row) {
		GenRow drips = new GenRow();
		drips.setViewSpec("ProductLinkedProductView");
		drips.setConnection(row.getConnection());
		drips.setColumn("-select0","ProductLinkedProductLinkedDocument.LinkedDocumentDocument.Description AS Text");
		drips.setColumn("-select1","ProductLinkedProductLinkedDocument.LinkedDocumentDocument.Placement AS Placement");
		drips.setColumn("-select2","ProductLinkedProductLinkedDocument.LinkedDocumentDocument.DocumentSubType AS DocumentSubType");
		drips.setColumn("ProductProductsLinkedProduct.ProductType","Drip");
		drips.setColumn("ProductLinkedProductLinkedDocument.LinkedDocumentDocument.DocumentSubType","Publishing");
		drips.setColumn("ProductProductsLinkedProduct.AvailableDate", "<=NOW+NULL");
		drips.setColumn("ProductProductsLinkedProduct.ExpiryDate", ">=NOW+NULL");
		drips.sortBy("ProductProductsLinkedProduct.AvailableDate", 0);
		drips.sortBy("ProductProductsLinkedProduct.ExpiryDate", 0);
		drips.setParameter("ProductID",row.getString("ProductID"));
		if (drips.getString("ProductID").length() > 0) {
			drips.doAction("search");
			drips.getResults();
			System.out.println(drips.getStatement());
		}
		
		
		
		GenRow drip = new GenRow();
		GenRow mainProduct = new GenRow();
		mainProduct.setViewSpec("properties/ProductHouseAndLandView");
		mainProduct.setConnection(row.getConnection());
		mainProduct.setParameter("ProductID", row.getString("ProductID"));
		if (mainProduct.getString("ProductID").length() > 0) mainProduct.doAction("selectfirst");
		
		DripService ds = new DripService();
		while (drips.getNext()) {
			drip.clear();
			drip.putAllDataAsParameters(drips);
			DripProduct dp = ds.getDripFromProduct(drip);
			dp.ProductID = drip.getString("LinkedProductID");
			
			ds.processDRIPvalue(row.getConnection(), mainProduct, dp, new Date(), null);
			
			if ("Y".equals(dp.Active) && drips.getString("Text").length() > 0) {
				row.setParameter("Drip" + drips.getString("Placement"), drips.getString("Text"));
			}
			
		}
		
		drips.close();
	}

	private static void populateFacade(GenRow row) {
		GenRow facade = new GenRow();
		facade.setViewSpec("ProductLinkedProductView");
		facade.setConnection(row.getConnection());
		facade.setColumn("ProductProductsLinkedProduct.ProductType","Home Facade");
		facade.setParameter("ProductID",row.getString("ProductID"));
		if (facade.getString("ProductID").length() > 0) facade.doAction("selectfirst");
		
		if (facade.isSuccessful()) {
			row.setParameter("FacadeName", facade.getString("Name"));
			row.setParameter("FacadeDescription", facade.getString("Description"));
		}
	}

	private static void populateLand(GenRow row) {
		GenRow land = new GenRow();
		land.setTableSpec("Products");
		land.setConnection(row.getConnection());
		land.setParameter("-select0","Summary");
		land.setParameter("-select1", "Description");
		land.setParameter("ProductID",row.getString("ProductID"));
		if (land.getString("ProductID").length() > 0) land.doAction("selectfirst");
		
		if (land.isSuccessful()) {
			row.setParameter("LandSummary", land.getString("Summary"));
			row.setParameter("LandDescription", land.getString("Description"));
		}
	}
	
	private static void populateProductDetailsMap(GenRow row) {
		if (row.isSet("ProductID")) {

			GenRow details = new GenRow();
			details.setConnection(row.getConnection());
			details.setTableSpec("properties/ProductDetails");
			details.setParameter("ProductID", row.getString("ProductID"));
			details.setParameter("-select", "*");
			details.getResults();

			while (details.getNext()) {
				String type = details.getColumn("DetailsType").replaceAll(" ", "");
				String pType = details.getColumn("ProductType").replaceAll("Details", "");
				for (int i = 0; i < details.getTableSpec().getFieldsLength(); i++) {
					String field = details.getTableSpec().getFieldName(i);
					Object o = details.getObject(field);
					if (o != null) {
						row.setParameter(type + "." + field, o.toString());
						row.setParameter(pType + "." + type + "." + field, o.toString());
					}
				}
			}
			details.close();
		}
	}

	

	@SuppressWarnings("deprecation")
	private void validatePackage(ArrayList<String> pidTokenList, HttpServletRequest request) {
		validationMax = pidTokenList.size();
		validationCurrent = 0;
		for (String productId : pidTokenList) {
			GenRow row = new GenRow();
			row.setViewSpec("properties/ProductHouseAndLandView");
			row.setParameter("ProductID", productId);
			row.doAction(GenerationKeys.SELECTFIRST);
			if (RPMtask.getInstance() != null) RPMtask.getInstance().doIndexRefreash("update", productId, "House and Land");
			
			if (row.isSuccessful()) {
				for (PublishMethod pm : PublishMethod.values()) {
					String filename = request.getRealPath("specific/specs/feeds/" + pm.getSpecFolder() + "/HouseandLand.xml");
					if (!(new File(filename)).canRead()) filename = request.getRealPath("specs/feeds/" + pm.getSpecFolder() + "/HouseandLand.xml");
					if (!(new File(filename)).canRead()) filename = request.getRealPath("specs/feeds/" + pm.getSpecFolder() + "/default.xml");
					if (!(new File(filename)).canRead()) {
						updateValidationLog(request, productId, pm.toString(), "");
						continue;
					}
	
					try {
						RealEstateValidator reV = new RealEstateValidator(new RealEstateMapper(filename));
						
					    String host = request.getRequestURL().toString();
						if (host.indexOf("/",9) > 0) {
							host = host.substring(0,host.indexOf("/",9));
						}
						host = host + request.getContextPath();
						
					    String status = "current";
					    if (!"Y".equals(row.getString("Active"))) status = "offmarket";
						reV.setParameter("Status", status);
						reV.setParameter("DATE","@DATE@");
	
					    reV.setParameter("ProductID",row.getData("ProductID"));
						reV.setParameter("Host",host);
						
						String pIDs = row.getData("ProductID") + "+" + row.getData("HomeProductID") + "+" + row.getData("PlanID")  + "+" + row.getData("LotID") + "+" + row.getData("CopyPlanID");
						GenRow imgs = new GenRow();
						imgs.setViewSpec("LinkedDocumentImageView");
						imgs.setRequest(request);
						imgs.setColumn("ProductID",pIDs);
						imgs.setColumn("LinkedDocumentDocument.DocumentType","Image");
						imgs.setColumn("ShowOnREmain","Y");
						imgs.setColumn("FilePath", "!NULL;!EMPTY");
						imgs.doAction("selectfirst");
						if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) reV.setParameter("ImageM", host + "/" + imgs.getData("FilePath"));
	
						imgs = new GenRow();
						imgs.setViewSpec("LinkedDocumentImageView");
						imgs.setRequest(request);
						imgs.setColumn("ProductID",pIDs);
						imgs.setColumn("LinkedDocumentDocument.DocumentType","Image");
						imgs.setColumn("LinkedDocumentDocument.DocumentSubType","!Floorplan");
						imgs.setColumn("ShowOnREorder", ">0");
						imgs.setColumn("FilePath", "!NULL;!EMPTY");
						imgs.sortBy("ShowOnREorder",0);
						imgs.sortBy("SortOrder",1);
						imgs.doAction(ActionBean.SEARCH);
						
						imgs.getResults();
						
						int i = 1;
						while (imgs.getNext() && i < 10) {
							if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) {
								reV.setParameter("Image" + i, host + "/" + imgs.getData("FilePath"));
								++i;
							}
						}
						
						imgs.setColumn("LinkedDocumentDocument.DocumentSubType","Floorplan");
						imgs.setColumn("ShowOnREorder", ">0");
						imgs.setColumn("FilePath", "!NULL;!EMPTY");
						imgs.sortBy("ShowOnREorder",0);
						imgs.sortBy("SortOrder",1);
						imgs.doAction(ActionBean.SEARCH);
						
						imgs.getResults();
						
						i = 1;
						while (imgs.getNext() && i <= 5) {
							if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) {
								reV.setParameter("FloorPlan" + i, host + "/" + imgs.getString("FilePath"));
								++i;
							}
						}
						
						imgs.close();
						
						boolean isValid = reV.validate();
						String errors = reV.getError();
						updateValidationLog(request, productId, pm.toString(), errors);
					} catch (Exception e) {
						log.error("validation error in " + filename + " is " + e.getMessage());
					}
				}
				++validationCurrent;
			}
		}
		log.debug("total " + pidTokenList.size() + " product status are updated headline");
	}
	
	private void updateValidationLog(HttpServletRequest request, String productID, String publishMethod, String errors) {
		if (errors == null || errors.length() == 0) errors = "";
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductValidation");
		row.setRequest(request);
		row.setParameter("-select1", "*");
		row.setParameter("ProductID", productID);
		row.setParameter("PublishMethod", publishMethod);
		row.doAction(GenerationKeys.SELECTFIRST);
		
		if (!row.isSuccessful()) {
			row.setColumn("ProductValidationID", KeyMaker.generate());
			row.setAction("insert");
		} else {
			row.setAction("update");
		}
		row.setParameter("ProductID", productID);
		row.setParameter("PublishMethod", publishMethod);
		row.setColumn("Message", errors);
		if (errors.indexOf("Required:") >= 0) {
			row.setColumn("Status", "Content Required");
		} else if (errors.indexOf("Optional:") >= 0) {
			row.setColumn("Status", "Validated - Optional");
		} else if (errors.indexOf("Deferred:") >= 0) {
			row.setColumn("Status", "Validated - Deferred");
		} else {
			row.setColumn("Status", "Validated - Passed");
		}
		row.setColumn("ValidationDate", "NOW");
		row.setColumn("ValidationBy", row.getString("CURRENTUSERID"));
		row.doAction();
	}

	private Date convertToDate(String dt, String tm) {
		try {
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyyhh:mma");
			Date date = fmt.parse(dt + tm);
			if (date != null) {
				return date;
			} else {
				return null;
			}
		} catch (ParseException e) {
			return null;
		} catch (NullPointerException e) {
			return null;
		}

	}

	public boolean deleteAllPackages(HttpServletRequest request) {
		String pid = (String) request.getParameter("productid");
		ArrayList<String> pidTokenList = new ArrayList<String>();
		if (pid != null && pid.trim().length() > 0) {
			if (pid.endsWith("#")) pid = pid.substring(0, pid.length() - 1);
			StringTokenizer tk = new StringTokenizer(pid, "#");
			while (tk.hasMoreTokens()) {
				String token = tk.nextToken();
				if (!token.trim().equals("#")) {
					pidTokenList.add(token);
				}
			}
			if (pidTokenList.size() == 0 && pid.indexOf("#") == -1) pidTokenList.add(pid);
		}
		if (pidTokenList.size() == 0) {
			pidTokenList = getSelectedPackagesFromSession(request);
		}
		for (String s : pidTokenList) {
			GenRow row = new GenRow();
			row.setTableSpec("Products");
			row.setRequest(request);
			row.setParameter("ProductID", s);
			row.doAction(ActionBean.DELETE);
			
			if (RPMtask.getInstance() != null) RPMtask.getInstance().doIndexRefreash("delete", s, "House and Land");
		
			GenRow ProductDetails = new GenRow();
			ProductDetails.setTableSpec("properties/ProductDetails");
			ProductDetails.setRequest(request);
			ProductDetails.setColumn("ON-ProductID", s);
			ProductDetails.doAction("deleteall");

			GenRow childVariation = new GenRow();
			childVariation.setTableSpec("ProductVariations");
			childVariation.setRequest(request);
			childVariation.setParameter("-select", "*");
			childVariation.setColumn("ON-ParentProductID|1", s);
			childVariation.setColumn("ON-ChildProductID|1", s);
			childVariation.doAction("deleteall");

			GenRow childProduct = new GenRow();
			childProduct.setTableSpec("ProductProducts");
			childProduct.setRequest(request);
			childProduct.setColumn("ON-ProductID|1", s);
			childProduct.setColumn("ON-LinkedProductID|1", s);
			childProduct.doAction("deleteall");
		}
		ArrayList<String> runway4 = (ArrayList<String>) request.getSession().getAttribute("-selectedhouseandlandproductids");
		ArrayList<String> list = getSelectedPackagesFromSession(request);
		for (String s : pidTokenList) {
			list.remove(s);
			remove(runway4, s);
		}
		return true;
	}

	public boolean isSelected(HttpServletRequest request, String id) {
		ArrayList<String> selectedlist = getSelectedPackagesFromSession(request);
		for (String s : selectedlist) {
			if (s.trim().equalsIgnoreCase(id.trim())) {
				return true;
			}
		}
		return false;
	}

	public boolean approveHouseAndLand(HttpServletRequest request) {
		String deleteOrKeep = request.getParameter("deleteRadio");
		String status = request.getParameter("selectStatus");
		ArrayList<String> pidSelectedList = getSelectedPackagesFromSession(request);
		ArrayList<String> runway4 = (ArrayList<String>) request.getSession().getAttribute("-selectedhouseandlandproductids");
		ArrayList<String> minusList = new ArrayList<String>();
		if (runway4 != null) {
			for (String r : runway4) {
				boolean found = false;
				for (String s : pidSelectedList) {
					if (s.equals(r)) {
						found = true;
						break;
					}
				}
				if (!found) {
					minusList.add(r);
				}
			}
			if ("delete".equals(deleteOrKeep)) {
				for (String s : minusList) {
					GenRow row = new GenRow();
					row.setTableSpec("Products");
					row.setRequest(request);
					row.setParameter("ProductID", s);
					row.doAction(ActionBean.DELETE);
					if (RPMtask.getInstance() != null) RPMtask.getInstance().doIndexRefreash("delete", s, "House and Land");
				}
				log.debug("total " + minusList.size() + " deleted successfuly.");
			} else {
				updateProductStatus(minusList, request);
				
			}
			String headline = request.getParameter("-headline");
			if (headline != null && headline.length() > 0) {
				//updateHeadline(pidSelectedList, request);
			}
		}
		//validatePackage(pidSelectedList, request);
		return true;
	}

	public GenRow getSalesRep(HttpServletRequest request) {
		GenRow linkedUserList = new GenRow();
		linkedUserList.setViewSpec("UserSelectView");
		linkedUserList.setRequest(request);
		linkedUserList.setParameter("UserLinkedUser.LinkedGroupID|1", request.getParameter("groupid"));
		linkedUserList.setParameter("Active", "Active");
		linkedUserList.setParameter("-sort1", "FirstName");
		linkedUserList.setParameter("-sort2", "LastName");
		linkedUserList.doAction("search");
		linkedUserList.getResults();
		return linkedUserList;
	}

	public int getValidationMax() {
		return validationMax;
	}

	public int getValidationCurrent() {
		return validationCurrent;
	}
}
