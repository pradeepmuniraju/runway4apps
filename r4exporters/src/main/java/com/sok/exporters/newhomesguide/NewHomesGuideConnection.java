/**
 * 
 */
package com.sok.exporters.newhomesguide;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.exporters.PublishConnectionImpl;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;

public class NewHomesGuideConnection implements PublishConnectionImpl {
	private static final Logger logger = LoggerFactory.getLogger(NewHomesGuideConnection.class);

	private static final String API_METHOD = "/v1/reaxml/";

	private static NewHomesGuideConnection instance = null;

	public static NewHomesGuideConnection getInstance() {
		if (instance == null) {
			instance = new NewHomesGuideConnection(PublishMethod.NewHomesGuide.getFeedHost());
		}
		return instance;
	}
	private boolean isConn = false;

	private String host = null;
	private String listingID = null;
	private String responseBody = null;

	private HttpPost httpPost = null;
	private HttpClient httpClient = null;

	private StringBuffer logMessage = new StringBuffer();

	public NewHomesGuideConnection(String host) {
		super();
		this.host = host;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.PublishConnectionImpl#connect(java.lang.String, java.lang.String)
	 */
	public boolean connect(String login, String password) {

		try {
			httpClient = new DefaultHttpClient();
			isConn = true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw e;
		}
		return isConn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#connect(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean connect(String key, String secret, String token, String tokenSecret) {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#send(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean send(String fullFileName, String token) {
		boolean success = false;
		clearLogMessage();
		try {
			httpPost = new HttpPost(host + API_METHOD);
			httpPost.setEntity(prepareEntity(fullFileName));

			httpPost.addHeader("Authorization", "Token " + token);
			httpPost.addHeader("Content-Type", "application/xml");
			httpPost.addHeader("Content-Disposition", "attachment; filename=import.xml");
			httpPost.addHeader("User-Agent", "truss-client-example/0.1 (The Cut; +https://www.thecut.net.au/)");

			boolean send = true; // send = true; token = "d473e64b50b454387ef56fa072b1723efaf8501a"; fullFileName= "C:/Users/Chandana/Desktop/NHG/Tested_Sample_2.xml"
			if (send) {
				HttpResponse response = httpClient.execute(httpPost);

				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

				String line = "";
				StringBuffer responseMessage = new StringBuffer();
				while ((line = rd.readLine()) != null) {
					responseMessage.append(line);
				}

				int responseCode = response.getStatusLine().getStatusCode();
				if (responseCode == HttpStatus.SC_OK || responseCode == HttpStatus.SC_CREATED || responseCode == HttpStatus.SC_ACCEPTED) {
					success = true;
					log("file upload successful");
				} else {
					success = false;
				}

				log("Response code {}", responseCode + "");
				log("Response Message {}", responseMessage.toString());
			} else {
				success = true;
			}

		} catch (Exception e) {
			logger.error("Exception connecting to api host - " + host, e);
			// error = e.getMessage();
			success = false;
		}

		return success;
	}

	public String getError() {
		return responseBody;
	}

	public void close() throws IOException {
		isConn = false;
		if (httpPost != null) {
			httpPost.releaseConnection();
		}
	}

	@Override
	public boolean isConnected() {
		return isConn;
	}

	public StringBuffer getLogMessage() {
		return logMessage;
	}

	public void setHost(String host) {
		this.host = host;
	}

	private void clearLogMessage() {
		responseBody = "";
		this.logMessage = new StringBuffer();
	}

	private void log(String s) {
		logMessage.append(s);
		logMessage.append("\r\n");

		logger.debug(s);
	}

	private void log(String msg, String... args) {
		if (args != null && args.length > 0) {
			for (String s : args) {
				msg = msg.replaceFirst("\\{\\}*", s);
			}
		}
		logMessage.append(msg);
		logMessage.append("\r\n");

		logger.debug(msg);

	}

	private FileEntity prepareEntity(String fileName) {
		FileEntity entity = new FileEntity(new File(fileName));
		return entity;
	}

	@Override
	public String getListingID() {
		return listingID;
	}

	@Override
	public void setListingID(String tmListingID) {
		listingID = tmListingID;
	}

	@Override
	public boolean send(String filename) {
		return false;
	}
}