/**
 * 
 */
package com.sok.exporters.trademe;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.exporters.PublishConnectionImpl;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;

/**
 * @author dion
 * 
 */
public class TrademeConnection implements PublishConnectionImpl {

	private static TrademeConnection instance = null;

	public static TrademeConnection getInstance() {
		if (instance == null) {
			instance = new TrademeConnection(PublishMethod.Trademe.getFeedHost());
		}
		return instance;
	}

	private static final Logger logger = LoggerFactory.getLogger(TrademeConnection.class);

	private StringBuffer logMessage = new StringBuffer();

	private static final int HTTP_STATUS_OK = 200;
	private static final int HTTP_STATUS_BAD_REQUEST = 400;

	private static final String API_NEW = "/v1/Selling.xml";
	private static final String API_EDIT = "/v1/Selling/Edit.xml";
	private static final String API_RELIST = "/v1/Selling/RelistWithEdits.xml";
	private static final String API_WITHDRAW = "/v1/Selling/Withdraw.xml";
	private static final String API_PHOTO_UPLOAD = "/v1/Photos.xml";

	private static final String XML_DUMMYTEXT_PLACEHOLDER = "REMOVE_THIS";
	// private static final String XML_DUMMYPHOTO_PLACEHOLDER = "<PhotoId></PhotoId>";

	private static final String RESPONSE_SUCCESS = "<Success>true</Success>";
	private static final String RESPONSE_SUCCESS_FALSE = "<Success>false</Success>";
	private static final String RESPONSE_STATUS_SUCCESS = "<Status>Success</Status>";
	private static final String RESPONSE_LISTING_NOT_VALID = "<ErrorDescription>Listing id %s is not valid</ErrorDescription>";
	private static final String RESPONSE_LISTING_NOT_PRESENT = "<Description>Could not find %s</Description>";

	private static final String PHOTO_UPLOAD_XML = "<PhotoUploadRequest xmlns=\"http://api.trademe.co.nz/v1\"><PhotoData>%s</PhotoData><FileName>%s</FileName><FileType>%s</FileType></PhotoUploadRequest>";

	Pattern PATTERN_PHOTOID = Pattern.compile("<PhotoId>(\\d+)</PhotoId>");
	Pattern PATTERN_LISTINGID = Pattern.compile("<ListingId>(\\d+)</ListingId>");

	private int responseCode;
	private boolean isConn = false;

	private HttpClient httpClient = null;
	private OAuthConsumer consumer = null;

	private String host = null;
	private String listingID = null;
	private String responseBody = "";

	private HttpPost request = null;

	public TrademeConnection(String host) {
		super();
		this.host = host;
	}

	/*
	 * (non-Javadoc) - Do not use this method for
	 * 
	 * @see com.sok.exporters.PublishConnectionImpl#connect(java.lang.String, java.lang.String)
	 */
	public boolean connect(String login, String password) {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#connect(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean connect(String key, String secret, String token, String tokenSecret) {
		if (!isConn) {
			try {
				if (key == null)
					key = "dummy";
				if (secret == null)
					secret = "dummy";
				log("Connecting to trademe host [{}]", host);

				httpClient = new DefaultHttpClient();
				consumer = new CommonsHttpOAuthConsumer(key, secret);
				consumer.setTokenWithSecret(token, tokenSecret);

				isConn = true;

			} catch (Exception e) {
				log("Exception connecting to ftp host - " + host, e.getMessage());
				// error = e.getMessage();
				return false;
			}
		}
		return isConn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#send(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean send(String fileName) {
		boolean success = false;
		clearLogMessage();
		try {
			String apiMethod = fileName.contains("withdraw") ? API_WITHDRAW : StringUtils.isBlank(getListingID()) ? API_NEW : API_EDIT;

			StringEntity inputEntity = prepareEntityFromFile(fileName);
			String responseXml = postRequest(apiMethod, inputEntity);

			if (responseCode == HTTP_STATUS_OK) {
				success = processResponse(responseXml);

				// if withdrawing and rejected because listing is no longer present, it is still a case success
				if (!success && API_WITHDRAW.equals(apiMethod)) {
					String expectedErrorMessage = String.format(RESPONSE_LISTING_NOT_PRESENT, getListingID());
					if (responseXml.contains(RESPONSE_SUCCESS_FALSE) && responseXml.contains(expectedErrorMessage)) {
						success = true;
						log("Listing {} not present, but anyways in the end listing is not up there , so all good.", getListingID());
					}
				}

			} else if (responseCode == HTTP_STATUS_BAD_REQUEST) {
				// <ErrorDescription>Listing id 1847317 is not valid</ErrorDescription>
				// Message that will be thrown when attempted to update a withdrawn item
				String expectedErrorMessage = String.format(RESPONSE_LISTING_NOT_VALID, getListingID());

				if (responseXml.contains(expectedErrorMessage)) {
					// seems like item has been withdrawn relist it
					responseXml = postRequest(API_RELIST, inputEntity);

					if (responseCode == HTTP_STATUS_OK) {
						success = processResponse(responseXml);
						if (!success) {
							// first edit failed, now relisting failed too, try as a new listing
							responseXml = postRequest(API_NEW, inputEntity);
							if (responseCode == HTTP_STATUS_OK) {
								success = processResponse(responseXml);
							}
						}
					}

				}
			}

		} catch (Exception e) {
			logger.error("Exception connecting to ftp host - " + host, e);
			// error = e.getMessage();
			success = false;
		}

		return success;
	}

	public String sendPhoto(String photoData, String fileName, String filePath) {
		String photoID = null;
		clearLogMessage();
		try {
			String apiMethod = API_PHOTO_UPLOAD;
			String xml = String.format(PHOTO_UPLOAD_XML, photoData, fileName, filePath);
			StringEntity entity = new StringEntity(xml, "UTF-8");
			String responseXml = postRequest(apiMethod, entity);

			if (responseCode == HTTP_STATUS_OK) {
				// <Status>Success</Status><PhotoId>247619</PhotoId>
				if (responseXml.contains(RESPONSE_STATUS_SUCCESS)) {
					Matcher m = PATTERN_PHOTOID.matcher(responseXml);
					m.find();
					photoID = m.group(1);
					log("PhotoID is [{}]", photoID);
				}
			}

		} catch (Exception e) {
			logger.error("Exception connecting to ftp host - " + host, e);
		}

		return photoID;
	}

	public String getError() {
		return responseBody;
	}

	public void close() throws IOException {
		isConn = false;
		httpClient.getConnectionManager().shutdown();
	}

	@Override
	public boolean isConnected() {
		// TODO Auto-generated method stub
		return isConn;
	}

	@Override
	public String getListingID() {
		return listingID;
	}

	@Override
	public void setListingID(String tmListingID) {
		listingID = tmListingID;
	}

	public StringBuffer getLogMessage() {
		return logMessage;
	}

	public void setHost(String host) {
		this.host = host;
	}

	private void setResponseBody(String responseBody) {
		if (null != responseBody) {
			this.responseBody = this.responseBody + "\n" + responseBody;
		}
	}

	private void clearLogMessage() {
		responseBody = "";
		this.logMessage = new StringBuffer();
	}

	private void log(String s) {
		logMessage.append(s);
		logMessage.append("\r\n");

		logger.debug(s);
	}

	private void log(String msg, String... args) {
		if (args != null && args.length > 0) {
			for (String s : args) {
				msg = msg.replaceFirst("\\{\\}*", s);
			}
		}
		logMessage.append(msg);
		logMessage.append("\r\n");

		logger.debug(msg);

	}

	private boolean processResponse(String responseXml) {
		if (responseXml.contains("<ListingId>") && responseXml.contains(RESPONSE_SUCCESS)) {
			Matcher m = PATTERN_LISTINGID.matcher(responseXml);
			m.find();
			setListingID(m.group(1));
			log("ListingID is [{}]", getListingID());
			return true;
		} else {
			return false;
		}
	}

	private String postRequest(String apiMethod, StringEntity inputEntity) throws Exception {
		logger.info("Connecting to trademe endpoint URL [{}]", host + apiMethod);

		request = new HttpPost(host + apiMethod);
		request.setEntity(inputEntity);
		consumer.sign(request);
		log("\nExecuting request " + request.getRequestLine());

		HttpResponse response = httpClient.execute(request);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		String line = "";
		StringBuffer sb = new StringBuffer();
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		responseCode = response.getStatusLine().getStatusCode();

		log("Response code : " + responseCode);
		log(sb.toString());

		setResponseBody("Response code - " + responseCode + "\nResponse Body - " + sb.toString());
		return sb.toString();
	}

	private StringEntity prepareEntityFromFile(String fileName) throws UnsupportedEncodingException {
		StringBuilder fileString = new StringBuilder();
		BufferedReader inNew = null;
		try {
			inNew = new BufferedReader(new FileReader(fileName));

			String lineNew = "";

			while ((lineNew = inNew.readLine()) != null) {
				// A quick fix to remove empty lines and <?xml version="1.0" encoding="UTF-8"?> added by the mapper
				if (StringUtils.isNotBlank(lineNew) && !lineNew.startsWith("<?xml version=") && !lineNew.startsWith("<!DOCTYPE")) {
					fileString.append(lineNew).append("\n");
				}
			}

		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			try {
				if (inNew != null)
					inNew.close();
			} catch (IOException e) {
			}
		}
		String xml = fileString.toString();

		// Dirty Coding to overcome some XML issues
		// This is added to HouseandLand.xml as the mapper does not parse when there is no data in a xml node
		// Its a bug in Dom4j - http://sourceforge.net/p/dom4j/mailman/message/24937421/
		xml = xml.replaceAll(XML_DUMMYTEXT_PLACEHOLDER, "");
		xml = xml.replaceAll("&lt;Paragraph&gt;", "<Paragraph>");
		xml = xml.replaceAll("&lt;/Paragraph&gt;", "</Paragraph>");

		logger.trace("Sending xml.. [{}]", xml);
		StringEntity entity = new StringEntity(xml, "UTF-8");
		return entity;
	}

	@Override
	public boolean send(String filename, String token) {
		throw new UnsupportedOperationException();
	}
}