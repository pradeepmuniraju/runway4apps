/**
 * 
 */
package com.sok.exporters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultDocument;

/**
 * @author dion
 *
 */
public abstract class Mapper {

	protected DefaultDocument	mapper = null;
	protected File				file = null;
	/**
	 * 
	 */
	private Mapper() {
		// TODO Auto-generated constructor stub
	}
	
	public Mapper(String filename) throws IOException {
		file = new File(filename);
		if (!file.canRead()) throw new IOException("File " + file.getAbsolutePath() + " Can't be read.");
	}

	public Mapper(File file) throws IOException {
		this.file = file;
		if (!file.canRead()) throw new IOException("File " + file.getAbsolutePath() + " Can't be read.");
	}
	
	public static String getVersion() {
		return "0.0.0.1";
	}

	protected void load() throws IOException{
        SAXReader reader = new SAXReader();
        FileReader fr = new FileReader(file);
        try {
			mapper = (DefaultDocument) reader.read(fr);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			throw new IOException(e.getMessage());
		}
	}

	public Element getElement(String name) {
		return mapper.getRootElement().element(name);
	}

	public Element getRootElement() {
		return mapper.getRootElement();
	}

	public String asXML() {
		return mapper.asXML();
	}

	public void write(Writer out) throws IOException {
		mapper.write(out);
		
	}

	public Document getDocument() {
		return mapper;
	}
}
