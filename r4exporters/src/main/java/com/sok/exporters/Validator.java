/**
 * 
 */
package com.sok.exporters;

/**
 * @author dion
 *
 */
public abstract class Validator {
	
	protected Mapper		mapper = null;

	/**
	 * 
	 */
	public Validator() {
	}

	public Validator(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
	
	public abstract boolean validate();
	public abstract String getError();
	public abstract String getName();
}
