package com.sok.exporters;

import java.io.IOException;

public interface PublishConnectionImpl {

	/**
	 * Suitable for FTP methods
	 * 
	 * @param login
	 * @param password
	 * @return
	 */
	public boolean connect(String login, String password) throws UnsupportedOperationException;

	/**
	 * Suitable for oAuth methods
	 * 
	 * @param key
	 * @param secret
	 * @param token
	 * @param tokenSecret
	 * @return
	 */
	public boolean connect(String key, String secret, String token, String tokenSecret) throws UnsupportedOperationException;

	/**
	 * Suitable for methods which contain the agentID in the xml file
	 * 
	 * @param filename
	 * @param listingID
	 * @return
	 */
	public boolean send(String filename);

	/**
	 * Suitable for methods need agentID to be sent in the HTTP header
	 * 
	 * @param filename
	 * @param token
	 * @return
	 */
	public boolean send(String filename, String token) throws UnsupportedOperationException;

	public String getError();

	public void close() throws IOException;

	public boolean isConnected();

	public String getListingID();

	public void setListingID(String listingID);

	public StringBuffer getLogMessage();
}
