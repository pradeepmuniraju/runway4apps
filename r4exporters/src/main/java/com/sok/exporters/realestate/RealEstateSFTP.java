/**
 * 
 */
package com.sok.exporters.realestate;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.exporters.FTPclient;
import com.sok.exporters.PublishConnectionImpl;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.verification.HostKeyVerifier;

/**
 * @author dion
 * 
 */
public class RealEstateSFTP extends FTPclient implements PublishConnectionImpl {

	public static final Logger logger = LoggerFactory.getLogger(RealEstateFTP.class);
	private StringBuffer logMessage = new StringBuffer();
	
	private SSHClient con2 = null;
	private SFTPClient sftp = null;
	
	/**
	 * 
	 */

	public RealEstateSFTP(String host) {
		super();
		this.host = host;
	}

	public RealEstateSFTP(String host, int port) {
		super();
		this.host = host;
		this.port = port;
	}

	public RealEstateSFTP() {
		super();

		host = "reaxml.realestate.com.au";

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#connect()
	 */
	@Override
	public boolean connect() {
		try {
			if (login == null)
				login = "anonymous";
			if (password == null)
				password = "info@switched-on.com.au";
			logger.info("SFTPClient Connecting to ftp host [{}]...", host);

			con2 = new SSHClient();
			con2.setConnectTimeout(300000);
			con2.setTimeout(600000);
			con2.addHostKeyVerifier("c7:09:14:1e:60:25:7b:00:87:18:44:fd:14:5e:b6:e2");
	        if (port != 0)
	        	con2.connect(host, port);
	        else
	        	con2.connect(host);

            con2.authPassword(login, password); // or ssh.authPublickey(System.getProperty("user.name"))
            sftp = con2.newSFTPClient();
			logger.info("SFTPClient Connected to ftp host [{}]", host);
            /*
            try {
                sftp.get("test_file", "/tmp/test.tmp");
            } finally {
                sftp.close();
            }
            */
		} catch (Exception e) {
			logger.error("Exception connecting to ftp host - " + host, e);
			error = e.getMessage();
			return false;
		}

		return con2.isConnected();
	}

	public boolean isConnected() {
		if (con2 == null)
			return false;
		return con2.isConnected();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#connect(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean connect(String login, String password) {
		try {
			this.login = login;
			this.password = password;

			if (login == null)
				login = "anonymous";
			if (password == null)
				password = "info@switched-on.com.au";
			
			con2 = new SSHClient();
			con2.setConnectTimeout(300000);
			con2.setTimeout(600000);
			con2.addHostKeyVerifier(new MyVerify());
	        //con2.loadKnownHosts();
	        if (port != 0)
	        	con2.connect(host, port);
	        else
	        	con2.connect(host);

            con2.authPassword(login, password); // or ssh.authPublickey(System.getProperty("user.name"))
            sftp = con2.newSFTPClient();
			logger.info("SFTPClient Connected to ftp host [{}]", host);
		} catch (Exception e) {
			logger.error("Exception connecting to ftp host - " + host, e);
			error = e.getMessage();
			return false;
		}

		return con2.isConnected();
	}

	public String readFile(String filename) {
		StringBuilder fileString = new StringBuilder();
		BufferedReader inNew = null;
		try {
			inNew = new BufferedReader(new FileReader(filename));

			String lineNew = "";

			while ((lineNew = inNew.readLine()) != null) {
				fileString.append(lineNew).append("\n");
			}

		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			try {
				if (inNew != null)
					inNew.close();
			} catch (IOException e) {
			}
		}
		return fileString.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#send(java.lang.String)
	 */
	@Override
	public boolean send(String filename) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
		try {
			String shortName = filename;
			if (shortName.lastIndexOf(File.separator) >= 0)
				shortName = shortName.substring(shortName.lastIndexOf(File.separator) + 1);
			if (shortName.lastIndexOf("/") >= 0)
				shortName = shortName.substring(shortName.lastIndexOf("/") + 1);
			// System.out.println("Sending File " + shortName + " as " + filename);
			// System.out.println("Size " + fis.available());
			sftp.put(filename, shortName);
			Long size = sftp.size(shortName);
			if (size == 0) {
				logger.error("Failed to store on ftp host [{}] file [{}] ({})received message [{}]", new String[] { host, filename, shortName, error });
				return false;
			}
			if (logger.isTraceEnabled())
				logger.trace("Successfully sent to host [{}] file  [{}] ({})", new String[] { host, filename, shortName });
		} catch (Exception e) {
			logger.error("Exception sending file via ftp", e);
			return false;
		}
		return con2.isConnected();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#send(java.io.File)
	 */
	@Override
	public boolean send(File file) {
		throw new UnsupportedOperationException();
	}

	public boolean send(String filename, String text) {
		throw new UnsupportedOperationException();
	}

	public String getError() {
		return error;
	}

	public void close() throws IOException {
		if (sftp != null) sftp.close();
		if (con2 != null && con2.isConnected()) {
			con2.disconnect();
		}
	}

	@Override
	public boolean connect(String key, String secret, String token, String tokenSecret) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}


	@Override
	/* To be used for future implementation*/
	public String getListingID() {
		return "";
	}

	@Override
	/* To be used for future implementation*/
	public void setListingID(String listingID) {
		
	}
	
	public StringBuffer getLogMessage() {
		return logMessage;
	}

	public void setLogMessage(StringBuffer logMessage) {
		this.logMessage = logMessage;
	}
	
	public class MyVerify implements HostKeyVerifier {

		@Override
		public boolean verify(String arg0, int arg1, PublicKey arg2) {
			System.out.println(arg0); 
			System.out.println(arg1); 
			System.out.println(arg2); 
			return true;
		}
		
	}
}
