/**
 * 
 */
package com.sok.exporters.realestate;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.exporters.FTPclient;
import com.sok.exporters.PublishConnectionImpl;

/**
 * @author dion
 * 
 */
public class RealEstateFTPS extends FTPclient implements PublishConnectionImpl {

	public static final Logger logger = LoggerFactory.getLogger(RealEstateFTP.class);
	private StringBuffer logMessage = new StringBuffer();
	
	private FTPSClient con2 = null;
	
	/**
	 * 
	 */

	public RealEstateFTPS(String host) {
		super();
		this.host = host;
	}

	public RealEstateFTPS(String host, int port) {
		super();
		this.host = host;
		this.port = port;
	}

	public RealEstateFTPS() {
		super();

		host = "reaxml.realestate.com.au";

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#connect()
	 */
	@Override
	public boolean connect() {
		try {
			if (login == null)
				login = "anonymous";
			if (password == null)
				password = "info@switched-on.com.au";
			logger.info("FTPClient Connecting to ftp host [{}]...", host);

			con2 = new FTPSClient();
			con2.setDefaultTimeout(300000);
			con2.setDataTimeout(600000);
			if (port == 0)
				con2.connect(host);
			else
				con2.connect(host, port);
			if (!FTPReply.isPositiveCompletion(con2.getReplyCode())) {
				error = con2.getReplyString();
				logger.error("Failed to connect to ftp host [{}] received message [{}]", host, error);
				con2.disconnect();
				return false;
			}
			con2.login(login, password);
			if (!FTPReply.isPositiveCompletion(con2.getReplyCode())) {
				error = con2.getReplyString();
				logger.error("Failed to login to ftp host [{}] with user [{}] received message [{}]", new String[] { host, login, error });
				con2.disconnect();
				return false;
			}
			if (dir != null)
				con2.changeWorkingDirectory(dir);
			if (!FTPReply.isPositiveCompletion(con2.getReplyCode())) {
				error = con2.getReplyString();
				logger.error("Failed to chdir on ftp host [{}] to [{}] received message [{}]", new String[] { host, dir, error });
				con2.disconnect();
				return false;
			}
			con2.enterLocalPassiveMode();
			logger.info("FTPClient Connected to ftp host [{}]", host);
		} catch (Exception e) {
			logger.error("Exception connecting to ftp host - " + host, e);
			error = e.getMessage();
			return false;
		}

		return con2.isConnected();
	}

	public boolean isConnected() {
		if (con == null)
			return false;
		return con2.isConnected();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#connect(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean connect(String login, String password) {
		try {
			this.login = login;
			this.password = password;

			if (login == null)
				login = "anonymous";
			if (password == null)
				password = "info@switched-on.com.au";
			logger.info("FTPClient Connecting to ftp host [{}]...", host);
			con2 = new FTPSClient("TLS");
			con2.setDefaultTimeout(300000);
			con2.setDataTimeout(600000);
			if (port != 0)
				con2.connect(host, port);
			else
				con2.connect(host);
			if (!FTPReply.isPositiveCompletion(con2.getReplyCode())) {
				error = con2.getReplyString();
				logger.error("Failed to connect to ftp host [{}] received message [{}]", host, error);
				con2.disconnect();
				return false;
			}
			con2.login(login, password);
			if (!FTPReply.isPositiveCompletion(con2.getReplyCode())) {
				error = con2.getReplyString();
				logger.error("Failed to login to ftp host [{}] with user [{}] received message [{}]", new String[] { host, login, error });
				con2.disconnect();
				return false;
			}
			if (dir != null)
				con2.changeWorkingDirectory(dir);
			if (!FTPReply.isPositiveCompletion(con2.getReplyCode())) {
				error = con2.getReplyString();
				logger.error("Failed to chdir on ftp host [{}] to [{}] received message [{}]", new String[] { host, dir, error });
				con2.disconnect();
				return false;
			}
			con2.enterLocalPassiveMode();
			logger.info("FTPClient Connected to ftp host [{}]", host);
		} catch (Exception e) {
			logger.error("Exception connecting to ftp host - " + host, e);
			error = e.getMessage();
			return false;
		}

		return con2.isConnected();
	}

	public String readFile(String filename) {
		StringBuilder fileString = new StringBuilder();
		BufferedReader inNew = null;
		try {
			inNew = new BufferedReader(new FileReader(filename));

			String lineNew = "";

			while ((lineNew = inNew.readLine()) != null) {
				fileString.append(lineNew).append("\n");
			}

		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			try {
				if (inNew != null)
					inNew.close();
			} catch (IOException e) {
			}
		}
		return fileString.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#send(java.lang.String)
	 */
	@Override
	public boolean send(String filename) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
		try {
			InputStream fis = null;
			if (filename.endsWith(".zip")) {
				fis = new FileInputStream(filename);
				con2.setFileType(FTP.BINARY_FILE_TYPE);
			} else {
				String xml = readFile(filename);
				con2.setFileType(FTP.ASCII_FILE_TYPE);
				xml = xml.replace("@DATE@", sdf.format(new Date()));
				fis = new ByteArrayInputStream(xml.getBytes());
			}
			if (fis.available() > 0) {
				String shortName = filename;
				if (shortName.lastIndexOf(File.separator) >= 0)
					shortName = shortName.substring(shortName.lastIndexOf(File.separator) + 1);
				if (shortName.lastIndexOf("/") >= 0)
					shortName = shortName.substring(shortName.lastIndexOf("/") + 1);
				// System.out.println("Sending File " + shortName + " as " + filename);
				// System.out.println("Size " + fis.available());
				con2.storeFile(shortName, fis);
				if (!FTPReply.isPositiveCompletion(con2.getReplyCode())) {
					fis.close();
					error = con2.getReplyString();
					logger.error("Failed to store on ftp host [{}] file [{}] ({})received message [{}]", new String[] { host, filename, shortName, error });
					con2.disconnect();
					return false;
				}
				fis.close();
				if (logger.isTraceEnabled())
					logger.trace("Successfully sent to host [{}] file  [{}] ({})", new String[] { host, filename, shortName });
			} else {
				logger.error("Failed to store on ftp host [{}] file [{}] - file was empty", new String[] { host, filename, error });
				error = "File was Empty";
			}
		} catch (Exception e) {
			if (con != null)
				logger.error("Exception sending file via ftp - recieved message - " + con2.getReplyString(), e);
			else
				logger.error("Exception sending file via ftp", e);
			error = new StringBuilder().append(e.getMessage()).append(" - ").append(con != null ? con2.getReplyString() : "connection-null").toString();
			return false;
		}
		return con2.isConnected();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.exporters.FTPclient#send(java.io.File)
	 */
	@Override
	public boolean send(File file) {
		try {
			FileInputStream fis = new FileInputStream(file);
			// System.out.println("Sending File " + file.getName() + " as " + file.getAbsoluteFile());
			// System.out.println("Size " + fis.available());
			con2.storeFile(file.getName(), fis);
			if (!FTPReply.isPositiveCompletion(con2.getReplyCode())) {
				fis.close();
				error = con2.getReplyString();
				logger.error("Failed to store on ftp host [{}] file [{}] ({})received message [{}]", new String[] { host, file.getAbsolutePath(), file.getName(), error });
				con2.disconnect();
				return false;
			}
			if (logger.isTraceEnabled())
				logger.trace("Successfully sent to host [{}] file  [{}] ({})", new String[] { host, file.getAbsolutePath(), file.getName() });
			fis.close();
		} catch (Exception e) {
			if (con != null)
				logger.error("Exception sending file via ftp - recieved message - " + con2.getReplyString(), e);
			else
				logger.error("Exception sending file via ftp", e);
			error = new StringBuilder().append(e.getMessage()).append(" - ").append(con != null ? con2.getReplyString() : "connection-null").toString();
			return false;
		}
		return con2.isConnected();
	}

	public boolean send(String filename, String text) {
		try {
			String shortName = filename;
			if (shortName.lastIndexOf(File.separator) >= 0)
				shortName = shortName.substring(shortName.lastIndexOf(File.separator) + 1);
			if (shortName.lastIndexOf("/") >= 0)
				shortName = shortName.substring(shortName.lastIndexOf("/") + 1);
			ByteArrayInputStream fis = new ByteArrayInputStream(text.getBytes());
			// System.out.println("Sending Text " + shortName + " as " + filename);
			// System.out.println("Size " + text.length());
			con2.storeFile(shortName, fis);
			if (!FTPReply.isPositiveCompletion(con2.getReplyCode())) {
				fis.close();
				error = con2.getReplyString();
				logger.error("Failed to store on ftp host [{}] file [{}] ({})received message [{}]", new String[] { host, filename, shortName, error });
				con2.disconnect();
				return false;
			}
			fis.close();
			if (logger.isTraceEnabled())
				logger.trace("Successfully sent to host [{}] file  [{}] ({})", new String[] { host, filename, shortName });
		} catch (Exception e) {
			if (con != null)
				logger.error("Exception sending file via ftp - recieved message - " + con2.getReplyString(), e);
			else
				logger.error("Exception sending file via ftp", e);
			error = new StringBuilder().append(e.getMessage()).append(" - ").append(con != null ? con2.getReplyString() : "connection-null").toString();
			return false;
		}
		return con2.isConnected();
	}

	public String getError() {
		return error;
	}

	public void close() throws IOException {
		if (con2.isConnected()) {
			con2.logout();
			con2.disconnect();
		}
	}

	@Override
	public boolean connect(String key, String secret, String token, String tokenSecret) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}


	@Override
	/* To be used for future implementation*/
	public String getListingID() {
		return "";
	}

	@Override
	/* To be used for future implementation*/
	public void setListingID(String listingID) {
		
	}
	
	public StringBuffer getLogMessage() {
		return logMessage;
	}

	public void setLogMessage(StringBuffer logMessage) {
		this.logMessage = logMessage;
	}
}
