/**
 * 
 */
package com.sok.exporters.realestate;

import java.io.File;
import java.io.IOException;

import org.dom4j.DocumentException;

/**
 * @author dion
 *
 */
public class RealEstateMapper extends com.sok.exporters.Mapper {

	/**
	 * @throws IOException If the file is not readable
	 * @throws DocumentException if the xml is incorrect 
	 * 
	 * @param filename the full path of the file to be used as a mapper in xml format
	 */
	public RealEstateMapper(String filename) throws IOException {
		super(filename);
		super.load();
	}

	/**
	 * @throws IOException If the file is not readable
	 * @throws DocumentException if the xml is incorrect 
	 * 
	 * @param file a File object to the file to be read 
	 */
	public RealEstateMapper(File file) throws IOException {
		super(file);
		super.load();
	}

}
