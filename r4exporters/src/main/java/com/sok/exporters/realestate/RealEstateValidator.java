/**
 * 
 */
package com.sok.exporters.realestate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.exporters.Mapper;
import com.sok.exporters.Validator;
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.SpecManager;
import com.sok.framework.StringUtil;
import com.sok.framework.TableSpec;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;

/**
 * @author dion
 *
 */
public class RealEstateValidator extends Validator {
	
	private static final Logger logger = LoggerFactory.getLogger(RealEstateValidator.class);
	
	private HashMap<String,String>		parameters = new HashMap<String,String>();
	private HashMap<String,GenRow>		data = new HashMap<String,GenRow>();
	
	private Document					export = null;
	
	private String						error = "";
	
	private SimpleDateFormat 			sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");

	private	boolean						allowDeferred = true;
	
	int errorCount = 0;
	/**
	 * 
	 */
	public RealEstateValidator() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param mapper
	 */
	public RealEstateValidator(Mapper mapper) {
		super(mapper);
		// TODO Auto-generated constructor stub
	}
	
	public String setParameter(String key, String value) {
		return parameters.put(key, value);
	}
	
	public String getParameter(String key) {
		return parameters.get(key);
	}

	public void setParameters(Map<String,String> map) {
		parameters.putAll(map);
	}
	
	@Override
	public boolean validate() {
		Element root = mapper.getRootElement();
		
		error = "";
		try {
			error += parseElements(root);
		} catch (Exception e) {
			logger.error("Error validating xml , {} ", e.getMessage());
			error += e.getMessage();
			//System.out.println("Error: msg " + e.getMessage() + " of type " + e.getClass().getName() + " at " + e.toString());
			//e.printStackTrace();
			return false;
		}
		return true;
	}
	
	protected String parseElements(Element node) throws Exception {
		boolean hasError = false;
		String errors = "";
    	String tag = node.getName();
    	//System.out.println("EL " + tag);
    	Iterator a = node.attributeIterator();
    	String format = (node.attribute("format") != null && StringUtils.isNotBlank(node.attribute("format").getValue())) ? node.attribute("format").getValue() : "";
    	
    	boolean required = "required".equals((node.attribute("runway") != null)? node.attribute("runway").getValue() : "");
    	boolean deferred = "deferred".equals((node.attribute("runway") != null)? node.attribute("runway").getValue() : "");
    	boolean optional = "optional".equals((node.attribute("runway") != null)? node.attribute("runway").getValue() : "");
    	boolean omit = "omit".equals((node.attribute("runway") != null)? node.attribute("runway").getValue() : "");
    	String msg = (node.attribute("msg") != null)? (node.attribute("msg").getValue() + "\r\n") : "";
    	while (a.hasNext()) {
    		Attribute att = (Attribute) a.next();
    		if (!"runway".equals(att.getName()) && !"msg".equals(att.getName())) {
    			String value = att.getValue();
    			if (value.indexOf("${") != -1 && value.lastIndexOf("}") != -1) {
    				String[] tokens = parseTokens(value);
    				value = parseValue(tokens, format);
    				if (omit && value == null) {
    					node.getParent().remove(node);
    					return errors;
    				}
    			}
    			if (value == null || value.trim().length() == 0 || "0.0".equals(value) || "0".equals(value)) {
    				if (required || (!allowDeferred && deferred)) { 
    					if (msg == null || msg.length() == 0) msg += "Required: Value for " + att.getValue() + " is not found\r\n";
    					errors += msg;
    					hasError = true;
    					errorCount++;
    				} else if (optional) { 
    					if (msg == null || msg.length() == 0) msg += "Optional: Value for " + att.getValue() + " is not found\r\n";
    					errors += msg;
    					errorCount++;
    				} else if (!"0.0".equals(value) || "0".equals(value)) {
    					value = "";
    				}
    			}
    			att.setValue(value);
    		}
    	}
    	// delete our tags, we don't want to send them to Real Estate
    	if (node.attribute("runway") != null) node.attribute("runway").detach();
    	if (node.attribute("msg") != null) node.attribute("msg").detach();
    	if (node.attribute("runwayformat") != null) node.attribute("runwayformat").detach();
    	
    	String txt = node.getText();
    	
    	if (txt.trim().length() > 0) {
    		String value = txt.trim();
			if (value.indexOf("${") != -1 && value.lastIndexOf("}") != -1) {
				String[] tokens = parseTokens(value);
				value = parseValue(tokens, format);
			}
			if (value == null || value.trim().length() == 0 || ( ("0.0".equals(value) || "0".equals(value)) && !"String".contains(format) )) {
				if(value == null && omit) {
					node.getParent().remove(node);
					return errors;
				} else if (required || (!allowDeferred && deferred)) { 
					if (msg == null || msg.length() == 0) msg += "Required: Value for " + txt + " is not found\r\n";
					errors += msg;
					hasError = true;
					errorCount++;
				} else if (optional) { 
					if (msg == null || msg.length() == 0) msg += "Optional: Value for " + txt + " is not found\r\n";
					errors += msg;
					errorCount++;
				} else if (!"0.0".equals(value) || "0".equals(value)) {
					value = "";
				}
				
				value = StringUtils.defaultString(value);
			}
			node.setText(value);
        	//System.out.println("TXT " + txt + " value " + value);
    	}
    	String search = (node.attribute("search") != null && StringUtils.isNotBlank(node.attribute("search").getValue())) ? node.attribute("search").getValue() : "";
    	String keys = (node.attribute("keys") != null && StringUtils.isNotBlank(node.attribute("keys").getValue())) ? node.attribute("keys").getValue() : "";
    	
    	if (StringUtils.isNotBlank(search) && StringUtils.isNotBlank(keys)) {
    		System.out.println("Here");
    		node.attribute("search").detach();
    		node.attribute("keys").detach();
    		errors += pareseSearch(search, keys, node);
    	} else {
	    	List nodes = node.elements();
	    	for (int i = 0; i < nodes.size(); ++i) {
	    		try {
	    			errors += parseElements((Element) nodes.get(i));
	    		} catch (Exception e) {
	    			logger.error("Error parseElements() , {}", e.getMessage());
	    			errors += e.getMessage();
	    			hasError = true;
	    		}
	    	}
    	}
		//if (errors.length() > 0 ) System.out.println(errors);
    	if (hasError) throw new Exception(errors);
    	
    	return errors;
	}
	
	private String pareseSearch(String search, String keys,  Element node) throws Exception {
		boolean hasError = false;
		String errors = "";

		String[] tokens = parseTokens(search);
		List nodes = node.elements();
		
		String[] parts = keys.split(",");
		
    	for (int i = 0; i < nodes.size(); ++i) {
    		Element repeat = (Element) nodes.get(i);
    		node.remove(repeat);
    		
    		GenRow row = new GenRow();
    		row.setStatement(tokens[0]);
    		
    		//row.doAction("execute");

    		row.getResults();
    		
    		while (row.getNext()) {
    			for (int c = 0; c < parts.length; ++c) {
    				String key = parts[c];
    				setParameter(key, row.getString(key));
    			}
        		Element clone = (Element) repeat.createCopy();
        		
        		node.add(clone);
	    		try {
	    			errors += parseElements(clone);
	    		} catch (Exception e) {
	    			logger.error("Error parseElements() , {}", e.getMessage());
	    			errors += e.getMessage();
	    			hasError = true;
	    		}
    		}
    	}
    	
    	if (hasError) throw new Exception(errors);

    	return errors;
	}

	protected String[] parseTokens(String value) {
		Vector<String> v = new Vector<String>();
		String token = "";
		
		// parse the value string and extract out tokens and literals
		int tokens = 0;
		for (int t = 0; t < value.length(); ++t) {
			char c = value.charAt(t);
			if (c == '$' && (t + 1) < value.length() && value.charAt(t + 1) == '{') {
				if (tokens == 0) {
					if (token.length() > 0) {
						v.add(token);
						token = "";
					} 
				}
				token += "$";
				++tokens;
			} else if (c == '}') {
				token += "}";
				if (tokens > 0) {
					--tokens;
					if (tokens == 0) {
						if (token.length() > 0) {
							v.add(token);
							token = "";
						}
					}
				}
			} else {
				token += c;
			}
		}
		if (token.length() > 0) v.add(token);
		
		if (v.size() == 0) return null;
		
		String[] ret = new String[v.size()];
		for (int i = 0; i < v.size(); ++i) {
			ret[i] = v.get(i);
		}
		
		return ret;
	}
	
	protected String parseValue(String[] values, String format) {
		String ret = "";
		
		for (int v = 0; v < values.length; ++v) {
			if (values[v].startsWith("${") && values[v].endsWith("}")) {
				String val = parseValue(values[v], format);
				if (val == null) return null;
				ret +=  val;
			} else {
				ret += values[v];
			}
		}
		
		return ret;
	}
	protected String parseValue(String value, String format){
		
		if ("${now()}".equalsIgnoreCase(value)) {
			return sdf.format(new Date());
		} else if (value.indexOf("[") == -1 && value.indexOf(".") == -1) {
			String key = value.substring(2, value.length() -1);
			return parameters.get(key);
		} else {
			String table = null;
			Vector<String>  key = new Vector<String>();
			Vector<String> keyValue = new Vector<String>();
			String field = null;
			
			if (value.indexOf("[") > 0) {
				table = value.substring(2, value.indexOf("["));
				String param = value.substring(value.indexOf("[") + 1, value.lastIndexOf("]"));
				String[] params = null;
				if (param.indexOf(",") == -1 && (param.indexOf("${") != -1 && param.lastIndexOf("}") != -1)) {
					params = new String[1];
					params[0] = param;
				} else {
					params = param.split(",");
				}
				for (int p = 0; p < params.length; ++p) {
					String tmp = params[p];
					if (tmp.indexOf("=") > 0) {
						int i = tmp.indexOf("=");
						key.add(tmp.substring(0, i));
						keyValue.add(tmp.substring(i + 1));
					} else {
						key.add("");
						keyValue.add(tmp);
					}
				}
				if (value.lastIndexOf(".") > 0) {
					field = value.substring(value.lastIndexOf(".") + 1, value.length() - 1);
				}
			} else {
				if (value.lastIndexOf(".") > 0) {
					field = value.substring(value.lastIndexOf(".") + 1);
				}
			}
			
			return retrieveData(table,key,keyValue,field, format);
		}
		
		//return null;
		
	}

	private String retrieveData(String table, Vector<String> keys, Vector<String> values,	String field, String format) {
		if (keys.size() != values.size()) return null;
		String hash = table + "[";
		for (int i = 0; i < keys.size(); ++i) {
			String key = keys.get(i);
			String value = values.get(i);
			if (value != null && value.length() > 0) value = StringUtil.urlDecode(value);
			if (value.startsWith("${") && value.endsWith("}")) { 
				value = parseValue(value, format);
				if (StringUtils.isBlank(value)) return "";
			}
			// get the substitution value;
			if (value.startsWith("$") && !value.startsWith("${")) {
				value = parameters.get(value.substring(1));
				if (value == null) return null;
			}
			values.set(i,value);
			// get the primary key if needed
			if (key == null || key.length() ==0) {
				TableSpec ts = SpecManager.getTableSpec(table);
				key = ts.getPrimaryKeyName();
				keys.set(i,key);
			}
			if (i > 0) hash += ",";
			hash += key + "=" + value;
		}
		// check to see if there is a condition on the answer
		String condition = "";
		String compare = "";
		if (field.indexOf("!=") > 0 || field.indexOf("==") > 0) {
			int index = (field.indexOf("!=") > 0)? field.indexOf("!=") : field.indexOf("==");
			compare = field.substring(index + 2);
			condition = field.substring(index, index + 2);
			field = field.substring(0, index);
		}

		// see if we have already got this data row
		hash += "]";
		GenRow row = data.get(hash);
		if (row == null) {
			row = new GenRow();
			if (!table.endsWith("View")) {
				row.setTableSpec(table);
				row.setParameter("-select", "*");
			} else {
				row.setViewSpec(table);
			}
			boolean missing = false;
			for (int i = 0; i < keys.size(); ++i) {
				if (values.get(i) == null || values.get(i).length() == 0) missing = true;
				row.setParameter(keys.get(i), values.get(i));
			}
			// we must not do a search if 1 or more values are missing, that is bad.
			if (!missing) row.doAction(GenerationKeys.SELECTFIRST);
			//System.out.println("Data " + row.getStatement() + " - " + row.getString(field));
			data.put(hash, row);
		} else {
			//System.out.println("Found " + hash);
		}
		
		format = StringUtils.defaultString(format);
		// If string format, use getString so we don't get , in the values 
		String ret = format.contains("String") ? row.getString(field) : row.getData(field);
		try {
			ret = format.contains("Integer") ? Integer.toString(row.getInt(field)) : ret;
		} catch(Exception e) {
			logger.error("Exception while converting to integer. {}" , e.getMessage());
		}
		
		// if we have a condition the do it
		if (condition.length() > 0 && compare.length() > 0) {
			if ("!=".equals(condition)) {
				if (!compare.equalsIgnoreCase(ret)) {
					ret = "yes";
				} else {
					ret = "no";
				}
			} else if ("==".equals(condition)) {
				if (compare.equalsIgnoreCase(ret)) {
					ret = "yes";
				} else {
					ret = "no";
				}
			}
		}
		
		return ret;
	}

	@Override
	public String getError() {
		return error;
	}
	
	public int getErrorCount() {
		return errorCount;
	}
	
	@Override
	public String getName() {
		return "realestate.com.au";
	}
	
	public void setAllowDeferred(boolean b) {
		allowDeferred = b;
	}
	
	@SuppressWarnings("deprecation")
	public static void validateSingleHome(HttpServletRequest request, String productId) {
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductPlanView");
		row.setParameter("ProductID", productId);
		row.doAction(GenerationKeys.SELECTFIRST);
		//if (RPMtask.getInstance() != null) RPMtask.getInstance().doIndexRefreash("update", productId, "Home Plan");
		
		if (row.isSuccessful()) {
			for (PublishMethod pm : PublishMethod.values()) {
			   	if (!"true".equals(InitServlet.getSystemParam("ProductValidation" + pm.toString()))) {
			   		continue;
			   	}
				String filename = request.getRealPath("specific/specs/feeds/" + pm.getSpecFolder() + "/validateHomePlan.xml");
				if (!(new File(filename)).canRead()) filename = request.getRealPath("specs/feeds/" + pm.getSpecFolder() + "/validateHomePlan.xml");
				if (!(new File(filename)).canRead()) {
					updateValidationLog(request, productId, pm.toString(), "");
					continue;
				}
	
				try {
					RealEstateValidator reV = new RealEstateValidator(new RealEstateMapper(filename));
					
				    String host = request.getRequestURL().toString();
					if (host.indexOf("/",9) > 0) {
						host = host.substring(0,host.indexOf("/",9));
					}
					host = host + request.getContextPath();
					
				    String status = "current";
				    if (!"Y".equals(row.getString("Active"))) status = "offmarket";
					reV.setParameter("Status", status);
					reV.setParameter("DATE","@DATE@");
	
				    reV.setParameter("ProductID",row.getData("ProductID"));
					reV.setParameter("Host",host);
					
					String pIDs = row.getData("ProductID") + "+" + row.getData("HomeProductID") + "+" + row.getData("PlanID")  + "+" + row.getData("LotID") + "+" + row.getData("CopyPlanID");
					GenRow imgs = new GenRow();
					imgs.setViewSpec("LinkedDocumentImageView");
					imgs.setRequest(request);
					imgs.setColumn("ProductID",pIDs);
					imgs.setColumn("LinkedDocumentDocument.DocumentType","Image");
					imgs.setColumn("ShowOnREmain","Y");
					imgs.setColumn("FilePath", "!NULL;!EMPTY");
					imgs.doAction("selectfirst");
					if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) reV.setParameter("ImageM", host + "/" + imgs.getData("FilePath"));
	
					imgs = new GenRow();
					imgs.setViewSpec("LinkedDocumentImageView");
					imgs.setRequest(request);
					imgs.setColumn("ProductID",pIDs);
					imgs.setColumn("LinkedDocumentDocument.DocumentType","Image");
					imgs.setColumn("LinkedDocumentDocument.DocumentSubType","!Floorplan");
					imgs.setColumn("ShowOnREorder", ">0");
					imgs.setColumn("FilePath", "!NULL;!EMPTY");
					imgs.sortBy("ShowOnREorder",0);
					imgs.sortBy("SortOrder",1);
					imgs.doAction(ActionBean.SEARCH);
					
					imgs.getResults();
					
					int i = 1;
					while (imgs.getNext() && i < 10) {
						if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) {
							reV.setParameter("Image" + i, host + "/" + imgs.getData("FilePath"));
							++i;
						}
					}
					
					imgs.setColumn("LinkedDocumentDocument.DocumentSubType","Floorplan");
					imgs.setColumn("ShowOnREorder", ">0");
					imgs.setColumn("FilePath", "!NULL;!EMPTY");
					imgs.sortBy("ShowOnREorder",0);
					imgs.sortBy("SortOrder",1);
					imgs.doAction(ActionBean.SEARCH);
					
					imgs.getResults();
					
					i = 1;
					while (imgs.getNext() && i <= 5) {
						if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) {
							reV.setParameter("FloorPlan" + i, host + "/" + imgs.getString("FilePath"));
							++i;
						}
					}
					
					imgs.close();
					
					boolean isValid = reV.validate();
					String errors = reV.getError();
					updateValidationLog(request, productId, pm.toString(), errors);
				} catch (Exception e) {
					logger.error("validation error in " + filename + " is " + e.getMessage());
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static void validateSingleLot(HttpServletRequest request, String productId) {
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductLotView");
		row.setParameter("ProductID", productId);
		row.doAction(GenerationKeys.SELECTFIRST);
		//if (RPMtask.getInstance() != null) RPMtask.getInstance().doIndexRefreash("update", productId, "Land");
		
		if (row.isSuccessful()) {
			for (PublishMethod pm : PublishMethod.values()) {
			   	if (!"true".equals(InitServlet.getSystemParam("ProductValidation" + pm.toString()))) {
			   		continue;
			   	}
				String filename = request.getRealPath("specific/specs/feeds/" + pm.getSpecFolder() + "/validateLand.xml");
				if (!(new File(filename)).canRead()) filename = request.getRealPath("specs/feeds/" + pm.getSpecFolder() + "/validateLand.xml");
				if (!(new File(filename)).canRead()) {
					updateValidationLog(request, productId, pm.toString(), "");
					continue;
				}
	
				try {
					RealEstateValidator reV = new RealEstateValidator(new RealEstateMapper(filename));
					
				    String host = request.getRequestURL().toString();
					if (host.indexOf("/",9) > 0) {
						host = host.substring(0,host.indexOf("/",9));
					}
					host = host + request.getContextPath();
					
				    String status = "current";
				    if (!"Y".equals(row.getString("Active"))) status = "offmarket";
					reV.setParameter("Status", status);
					reV.setParameter("DATE","@DATE@");
	
				    reV.setParameter("ProductID",row.getData("ProductID"));
					reV.setParameter("Host",host);
					
					String pIDs = row.getData("ProductID") + "+" + row.getData("StageProductID") + "+" + row.getData("EstateProductID");
					GenRow imgs = new GenRow();
					imgs.setViewSpec("LinkedDocumentImageView");
					imgs.setRequest(request);
					imgs.setColumn("ProductID",pIDs);
					imgs.setColumn("LinkedDocumentDocument.DocumentType","Image");
					imgs.setColumn("ShowOnREmain","Y");
					imgs.setColumn("FilePath", "!NULL;!EMPTY");
					imgs.doAction("selectfirst");
					if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) reV.setParameter("ImageM", host + "/" + imgs.getData("FilePath"));
	
					imgs = new GenRow();
					imgs.setViewSpec("LinkedDocumentImageView");
					imgs.setRequest(request);
					imgs.setColumn("ProductID",pIDs);
					imgs.setColumn("LinkedDocumentDocument.DocumentType","Image");
					imgs.setColumn("LinkedDocumentDocument.DocumentSubType","!Floorplan");
					imgs.setColumn("ShowOnREorder", ">0");
					imgs.setColumn("FilePath", "!NULL;!EMPTY");
					imgs.sortBy("ShowOnREorder",0);
					imgs.sortBy("SortOrder",1);
					imgs.doAction(ActionBean.SEARCH);
					
					imgs.getResults();
					
					int i = 1;
					while (imgs.getNext() && i < 10) {
						if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) {
							reV.setParameter("Image" + i, host + "/" + imgs.getData("FilePath"));
							++i;
						}
					}
					
					imgs.setColumn("LinkedDocumentDocument.DocumentSubType","Floorplan");
					imgs.setColumn("ShowOnREorder", ">0");
					imgs.setColumn("FilePath", "!NULL;!EMPTY");
					imgs.sortBy("ShowOnREorder",0);
					imgs.sortBy("SortOrder",1);
					imgs.doAction(ActionBean.SEARCH);
					
					imgs.getResults();
					
					i = 1;
					while (imgs.getNext() && i <= 5) {
						if (imgs.getData("FilePath") != null && imgs.getData("FilePath").length() > 0) {
							reV.setParameter("FloorPlan" + i, host + "/" + imgs.getString("FilePath"));
							++i;
						}
					}
					
					imgs.close();
					
					boolean isValid = reV.validate();
					String errors = reV.getError();
					updateValidationLog(request, productId, pm.toString(), errors);
				} catch (Exception e) {
					logger.error("validation error in " + filename + " is " + e.getMessage());
				}
			}
		}
	}
	
	private static void updateValidationLog(HttpServletRequest request, String productID, String publishMethod, String errors) {
		if (errors == null || errors.length() == 0) errors = "";
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductValidation");
		row.setRequest(request);
		row.setParameter("-select1", "*");
		row.setParameter("ProductID", productID);
		row.setParameter("PublishMethod", publishMethod);
		row.doAction(GenerationKeys.SELECTFIRST);
		
		if (!row.isSuccessful()) {
			row.setColumn("ProductValidationID", KeyMaker.generate());
			row.setAction("insert");
		} else {
			row.setAction("update");
		}
		row.setParameter("ProductID", productID);
		row.setParameter("PublishMethod", publishMethod);
		row.setColumn("Message", errors);
		if (errors.indexOf("Required:") >= 0) {
			row.setColumn("Status", "Content Required");
		} else if (errors.indexOf("Optional:") >= 0) {
			row.setColumn("Status", "Validated - Optional");
		} else if (errors.indexOf("Deferred:") >= 0) {
			row.setColumn("Status", "Validated - Deferred");
		} else {
			row.setColumn("Status", "Validated - Passed");
		}
		row.setColumn("ValidationDate", "NOW");
		row.setColumn("ValidationBy", row.getString("CURRENTUSERID"));
		row.doAction();
	}
}
