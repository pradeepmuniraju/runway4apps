/**
 * 
 */
package com.sok.exporters.realestate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import com.sok.exporters.Mapper;

/**
 * @author dion
 *
 */
public class RealEstatePackager extends RealEstateValidator {
	
	private static final Logger logger = LoggerFactory.getLogger(RealEstatePackager.class);
	
	private File				file = null;
	private OutputStream		out = null;
	private Writer				writer = null;
	
	private String				error = null;
	/**
	 * 
	 */
	private RealEstatePackager() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param mapper
	 * @param filename
	 * @throws IOException 
	 */
	public RealEstatePackager(Mapper mapper, String filename) throws IOException {
		super(mapper);
		setAllowDeferred(false);
		file = new File(filename);
		//if (!file.canWrite()) throw new IOException("File " + file.getAbsolutePath() + " Can't be written.");

	}

	/**
	 * @param mapper
	 * @param file
	 * @throws IOException 
	 */
	public RealEstatePackager(Mapper mapper, File file) throws IOException {
		super(mapper);
		setAllowDeferred(false);
		//if (!file.canWrite()) throw new IOException("File " + file.getAbsolutePath() + " Can't be written.");

	}

	/**
	 * @param mapper
	 * @param out
	 * @throws IOException 
	 */
	public RealEstatePackager(Mapper mapper, OutputStream out) throws IOException {
		super(mapper);
		setAllowDeferred(false);
		this.out = out;
	}
	
	/**
	 * @param mapper
	 * @param out
	 * @throws IOException 
	 */
	public RealEstatePackager(Mapper mapper, Writer writer) throws IOException {
		super(mapper);
		setAllowDeferred(false);
		this.writer = writer;
	}
	
	public boolean generateXML() {
		if (super.validate()) {
			if (writer == null) {
				if (out == null) {
					try {
						out = new FileOutputStream(file);
					} catch (FileNotFoundException e) {
						error = e.getMessage();
						return false;
					}
				}
				try {
					OutputFormat format = OutputFormat.createPrettyPrint();
					XMLWriter writer = new XMLWriter(out, format);
					writer.write(mapper.getDocument());
					writer.flush();
					writer.close();
					out.flush();
					out.close();
				} catch (UnsupportedEncodingException e) {
					logger.error("Unsupported Encoding in writer.writer(mapper.getDocument())", e);
					error = e.getMessage();
					return false;
				} catch (IOException e) {
					logger.error("IOException in writer.writer(mapper.getDocument()) ", e);
					error = e.getMessage();
					return false;
				}
				return true;
			}
			try {
				mapper.write(writer);
				writer.flush();
				writer.close();
			} catch (IOException e) {
				logger.error("IOException in mapper.write", e);
				error = e.getMessage();
				return false;
			}
		}
		
		return false;
	}
	
	public boolean saveXML() {
		if (writer == null) {
			if (out == null) {
				try {
					out = new FileOutputStream(file);
				} catch (FileNotFoundException e) {
					error = e.getMessage();
					return false;
				}
			}
			try {
				OutputFormat format = OutputFormat.createPrettyPrint();
				XMLWriter writer = new XMLWriter(out, format);
				writer.write(mapper.getDocument());
				writer.flush();
				writer.close();
				out.flush();
				out.close();
			} catch (UnsupportedEncodingException e) {
				logger.error("Unsupported Encoding in writer.writer(mapper.getDocument())", e);
				error = e.getMessage();
				return false;
			} catch (IOException e) {
				logger.error("IOException in writer.writer(mapper.getDocument()) ", e);
				error = e.getMessage();
				return false;
			}
			return true;
		}
		try {
			mapper.write(writer);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			logger.error("IOException in mapper.write", e);
			error = e.getMessage();
			return false;
		}
		return true;
	}
	
	public String getError() {
		if (error != null && error.length() > 0) return error;
		
		return super.getError();
	}

	public void setFilename(String filename) {
		file = new File(filename);
	}

	public String getFilename() {
		if (file == null) return "";
		return file.getAbsolutePath();
	}

}
