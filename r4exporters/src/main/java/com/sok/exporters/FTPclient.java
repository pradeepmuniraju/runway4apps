/**
 * 
 */
package com.sok.exporters;

import java.io.File;

import org.apache.commons.net.ftp.FTPClient;

/**
 * @author dion
 *
 */
public abstract class FTPclient {
	
	protected String		host = null;
	protected String		dir = null;
	protected String		login = null;
	protected String		password = null;
	protected int			port = 0;
	
	protected FTPClient		con = null;
	
	protected String		error = "";

	/**
	 * 
	 */
	public FTPclient() {
		// TODO Auto-generated constructor stub
	}
	
	
	public void setHost(String host) {
		this.host = host;
	}
	
	public void setDir(String dir) {
		this.dir = dir;
	}
	
	
	
	public abstract boolean connect();
	public abstract boolean connect(String login, String password);
	public abstract boolean send(String filename);
	public abstract boolean send(File file);
	public abstract boolean send(String filename, String text);


	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}
	
}
