package com.sok;

import com.sok.framework.ActionBean;
import java.util.*;

public class ErrorDebugger extends Debugger{
   
   private static final int BUFFER_SIZE = 2000000;
   
   private static ErrorDebugger errorDebugger;
   private int bufferSize;
   private StringBuffer errors = new StringBuffer();
   protected static HashMap<String, ErrorDebugger> errorDebuggers = new HashMap<String, ErrorDebugger>();
   
   private ErrorDebugger() {
      super();
      this.bufferSize = BUFFER_SIZE;
   }
   
   private ErrorDebugger(int bufferSize) {
      super();
      setBufferSize(bufferSize);
   }
   
   public static ErrorDebugger getErrorDebugger() {
      if (errorDebugger == null) {
         errorDebugger = new ErrorDebugger();
      }
      return errorDebugger;
   }  
   
   public static ErrorDebugger getErrorDebugger(String key) {
      ErrorDebugger errorDebugger = (ErrorDebugger)errorDebuggers.get(key);
      if (errorDebugger == null) {
         errorDebugger = new ErrorDebugger();
         errorDebuggers.put(key, errorDebugger);
      }
      return errorDebugger;
   }     
   
   public static ErrorDebugger getErrorDebugger(int bufferSize) {
      if (errorDebugger == null) {
         errorDebugger = new ErrorDebugger(bufferSize);
      } else {
         errorDebugger.setBufferSize(bufferSize);
      }
      
      return errorDebugger;
   }
   
   public int getBufferSize() {
      return bufferSize;
   }

   public void setBufferSize(int bufferSize) {
      if(bufferSize > 0) {
         this.bufferSize = bufferSize;
      } else {
         this.bufferSize = BUFFER_SIZE;
      }
   }
   
   public StringBuffer getErrors() {
      return errors;
   }   

   public void addError(String error) {
      if(errors.length() > bufferSize) { 
         int rem = errors.length() - bufferSize; 
         errors.delete(0, rem); 
       }       
      
      errors.append(error);
      if(this.isDebugging()) {
         this.debug(error);
      }
   }

   public void addError(Exception e) {
      addError(ActionBean.writeStackTraceToString(e));
   }
}
