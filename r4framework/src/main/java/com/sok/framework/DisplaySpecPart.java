package com.sok.framework;
import org.dom4j.*;

import com.sok.runway.view.FormFields;

public class DisplaySpecPart extends XMLSpec
{   
    private Element[] items;
    private Element[][] itemvalidations;
    private Element[][] itemvalues;
    private Element[] relateddisplays;
    
    //private Element[] includeditems;
    //private Element[] includeddisplays;

    private String label;
    
    //tags 
    public static final String item = "item";
    public static final String includeditem = "includeditem";
    public static final String includeddisplay = "includeddisplay";
    public static final String validation = "validation";
    public static final String valuestag = "values";
    public static final String relateddisplay = "relateddisplay";
    
    // item attributes
    public static final String _name = "name";
    public static final String _label = "label";  
    public static final String _fieldname = "fieldname";
    public static final String _inputtype = "inputtype";
    public static final String _listtype = "listtype";
    public static final String _defaultValue = "defaultValue";  
    public static final String _src = "src";      
    public static final String _display = "display";
    public static final String _notused = "notused";    
    public static final String _type = "type";
    
    //denotes items that are not visible by default, but able to be enabled via the ui 
    public static final String _optional = "optional";
    //a velocity string that when used in the current context should create a href link 
    public static final String _href = "href";
    //built in for lists, denotes the relative width required for this field in reference to the others.
    public static final String _size = "size";
    //should return a velocity expression to generate the value of the field	//call this render? view ? not sure  
    public static final String _value = "value";
    
    // related display attributes
    public static final String _relateddisplayname = "name";
    public static final String _relateddisplaylabel = "label";
    public static final String _relateddisplayrelationship = "relationship";
    public static final String _relateddisplaynotused= "notused";
    public static final String _relateddisplaycanaccess= "canaccess";
  
    //parameter values
    
    //types
    public static final String email = "email"; 
    public static final String date = "date";
    public static final String usdate = "usdate";
    public static final String time = "time";
    public static final String numeric = "numeric";
    public static final String integer = "integer";
    public static final String alpha = "alpha";
    public static final String alphanumeric = "alphanumeric";
  
    public static final String _true = "true";
    
    public static final String _blank = "";   
    
    public DisplaySpecPart(String path, boolean ignoreerror)   
    {   
        super(path, ignoreerror);
        if(super.document!=null)
        {
            Element root = super.document.getRootElement();
            if(root!=null)
            {
                items = super.getElementArray(root,item);
                itemvalidations = new Element[items.length][];
                for(int i=0; i<items.length; i++)
                {
                    itemvalidations[i] = getElementArray(items[i], validation);
                }
                
                itemvalues = new Element[items.length][];
                for(int i=0; i<items.length; i++) 
                {
                   itemvalues[i] = getElementArray(items[i], valuestag);
                }
                
                relateddisplays = super.getElementArray(root, relateddisplay);
                //includeditems = super.getElementArray(root,includeditem);
                //includeddisplays = super.getElementArray(root,includeddisplay);
                name = root.attributeValue(_name);
                label = root.attributeValue(_label);
            }
            else
            {
                System.out.print("Could not find root element in document - ");
                System.out.println(path);
            }
        }
    }

    public String getLabel()
    {
        if(label!=null)
        {
            return(label);
        }
        return(_blank);
    }          
    
    public int getItemsCount()
    {
        if(items!=null)
        {
            return(super.getCount(items));
        }
        return(0);
    }
/*    
    public int getIncludedItemsCount()
    {
        if(includeditems!=null)
        {
            return(super.getCount(includeditems));
        }
        return(0);        
    }
*/
/*    
    public int getIncludedDisplaysCount()
    {
        if(includeddisplays!=null)
        {
            return(super.getCount(includeddisplays));
        }
        return(0);
    }
*/
    public String getItemName(int i)
    {
        if(items!=null)
        {
            return(super.getAttribute(items,i,_name));
        }
        return(_blank);
    }

    public String getItemLabel(int i)
    {
        if(items!=null)
        {
            return(super.getAttribute(items,i,_label));
        }
        return(_blank);        
    }
    
    public String getItemFieldName(int i) {
       String itemFieldName = _blank;
       
       if(items != null) {
           itemFieldName = super.getAttribute(items,i,_fieldname);
           
           /* 
            * If fieldname attribute is missing or empty, 
            * assume the field name is the same as the label.
            */
           if(itemFieldName == null || itemFieldName.equals(_blank)) {
              itemFieldName = this.getItemLabel(i);
           }           
       }
       
       return itemFieldName;
    }
    
    public String getItemInputType(int i) {
       String itemInputType = _blank;
       
       if(items != null) {
           itemInputType = super.getAttribute(items,i,_inputtype);
           
           /* 
            * If inputtype attribute is missing or empty, 
            * assume the inputtype is Tiny.
            */
           if(itemInputType == null || itemInputType.equals(_blank)) {
              itemInputType = FormFields.FIELD_TEXT_TINY;
           }           
       }
       
       return itemInputType;
    }
    
    public String getItemValuesListType(int i) {
       String itemValuesListName = _blank;
       
       if(itemvalues != null && i < itemvalues.length && itemvalues[i] != null) {
           itemValuesListName = super.getAttribute(itemvalues[i],0,_listtype);
       }
       
       return itemValuesListName;
    }
    
    public String[] getItemValues(int i) {
       String[] values = null;
       
       if(itemvalues != null && i < itemvalues.length && itemvalues[i] != null) {
          Element[] valueElements = super.getElementArray(itemvalues[i][0], valuestag);
          if(valueElements != null) {
             values = new String[valueElements.length];
             for(int valueIndex=0; valueIndex<valueElements.length; valueIndex++) {
                values[valueIndex] = valueElements[valueIndex].getText();
             }
          }
       }
       
       return values;
    }

    public String getItemDefaultValue(int i)
    {
        if(items!=null)
        {
            return(super.getAttribute(items,i,_defaultValue));
        }
        return(_blank);        
    }

    public String getItemSource(int i)
    {
        if(items!=null)
        {
            return(super.getAttribute(items,i,_src));
        }
        return(_blank);        
    }
    
    public String getItemLink(int i) {
    	if(items != null) 
    	{
    		return(super.getAttribute(items,i,_href));
        }
        return(_blank);
    }
    
    public String getItemSize(int i) {
    	if(items != null) 
    	{
    		return(super.getAttribute(items,i,_size));
        }
        return(_blank);
    }
    
    public String getItemValue(int i) {
    	if(items != null) 
    	{
    		return(super.getAttribute(items,i,_value));
        }
        return(_blank);
    }
    
    public String getRelatedDisplayRelationship(int i) 
    {
       if(relateddisplays!=null)
       {
           return(super.getAttribute(relateddisplays,i,_relateddisplaylabel));
       }
       return(_blank);  
    }

    public boolean isItemUsed(int i)
    {
        if(items!=null)
        {
            String noteused = super.getAttribute(items,i,_notused);
            if(noteused != null && noteused.equals(_true))
            {
                return(false);
            }
        }
        return(true);
    }
    
    public boolean isItemOptional(int i) 
    {
    	if(items!=null)
        {
            String optional = super.getAttribute(items,i,_optional);
            if(optional != null && optional.equals(_true))
            {
                return(true);
            }
        }
        return(false);
    }
    
    public boolean hasItemValidation(int i)
    {
        return(itemvalidations[i]!=null && itemvalidations[i].length!=0);
    }
 
    public int getItemValidationCount(int i)
    {
        return(getCount(itemvalidations[i]));
    }   
    
    public String getItemValidationMessage(int i, int j)
    {
       return(super.getText(itemvalidations[i],j));
    }     
    
    public String getItemValidationType(int i, int j)
    {
       return(super.getAttribute(itemvalidations[i],j,_type));
    }
    
    public String getRelatedDisplayLabel(int i) {
       if(items!=null)
       {
           return(super.getAttribute(relateddisplays,i,_relateddisplaylabel));
       }
       return(_blank);
    }
    
    public int getRelatedDisplayCount() {
       if(relateddisplays!=null)
       {
           return(super.getCount(relateddisplays));
       }
       return(0);       
    }
    
    public boolean isRelatedDisplayUsed(int i) 
    {
       if(relateddisplays!=null)
       {
           String noteused = super.getAttribute(relateddisplays,i,_relateddisplaynotused);
           if(noteused != null && noteused.equals(_true))
           {
               return(false);
           }
       }
       return(true);       
    }
    
    public String getRelatedDisplayName(int i)
    {
       if(relateddisplays!=null)
       {
           return(super.getAttribute(relateddisplays,i,_relateddisplayname));
       }
       return(_blank);
   }
    
/*
    public String getItemDisplay(int i)
    {
        if(items!=null)
        {
            return(super.getAttribute(items,i,_display));
        }
        return(_blank);        
    }    
*/    
}
