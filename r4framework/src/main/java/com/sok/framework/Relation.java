package com.sok.framework;
import java.util.*;
public class Relation
{
	String name;
	Relation parent = null;
	LinkedHashMap subRelations = new LinkedHashMap();
	HashMap fields = new HashMap();

	public Relation(String name, Relation parent)
	{
		this.name = name;
		this.parent = parent;
	}
/*	
	public Relation(String name)
	{
		this.name = name;
	}
*/
	public String getName()
	{
		return(name);	
	}
	
	public Relation addRelation(String name)
	{
		Relation newRelation = null;
		if(subRelations.containsKey(name))
		{
			newRelation = (Relation)subRelations.get(name);
		}
		else
		{
			newRelation = new Relation(name, this);
			subRelations.put(name, newRelation);
		}
		return(newRelation);
	}

	public void removeRelation(String name)
	{
		subRelations.remove(name);
	}

	public Relation getRelation(String name)
	{
		return((Relation)subRelations.get(name));
	}

	public Relation getParent()
	{
		return(parent);
	}
	
	public boolean hasRelations()
	{
		return(!subRelations.isEmpty());
	}

	public Iterator getRelationsIterator()
	{
		return(subRelations.values().iterator());
	}
	
	public void addField(String name, String value)
	{
		//System.out.println("added field");
		fields.put(name,value);
	}

	public void removeField(String name)
	{
		//System.out.println("added field");
		fields.remove(name);
		removeEmptyRelations();
	}

	void removeEmptyRelations()
	{
		Relation temp = this;
		Relation tempparent = null;
		while(temp!=null && temp.isEmpty())
		{
			tempparent = temp.getParent();
			if(tempparent!=null)
			{
				tempparent.removeRelation(temp.getName());
			}
			temp = tempparent;
		}
	}

	public String getField(String name)
	{
		//System.out.println("added field");
		return((String)fields.get(name));
	}
	
	public boolean hasFields()
	{
		return(!fields.isEmpty());
	}
	
	public Object[] getKeys()
	{
		return(fields.keySet().toArray());
	}

	public boolean isEmpty()
	{
		return(subRelations.isEmpty() && fields.isEmpty());
	}
}
