package com.sok.framework;
import java.util.*;
public class KeyMaker
{
	public static String getTime()
	{
		return(String.valueOf(System.currentTimeMillis()));
	}

	public static String getTimeInHex()
	{
	   return(Long.toHexString(System.currentTimeMillis()));
	}
	
	public static String generate(String base, String suffix)
	{	
		StringBuffer temp = new StringBuffer();
		if(base != null && base.length() != 0)
		{
			int keylength = base.length();
			int zindex = base.indexOf('z');

			String sfx = suffix;
			if(sfx==null)
			{
				sfx = getTime();
			}

			if(zindex>0)
			{
				temp.append(base.substring(0,zindex));
				temp.append('z');
				temp.append(sfx);
			}
			else
			{
				temp.append(base);
				temp.append('z');
				temp.append(sfx);
			}
		}
		return(temp.toString());
	}

	public static String generate()
	{
		return(generate(14));
	}

	public static String generate(int l)
	{	
		char[] even = generateEven(l);
		//System.out.println(even);
		
		char[] odd = generateOdd(l);
		//System.out.println(odd);
		
		return(generateString(odd, even));
	}
	
	public static String generateString(char[] odd, char[] even)
	{
		char[] code = new char[odd.length+even.length];
		int evencount = 0;
		for(int i=0; i<code.length; i++)
		{
			if(i==0)
			{
				code[i] = odd[0];
			}
			if(i%2==0)
			{
				code[i] = odd[i/2];
			}
			else
			{
				code[i] = even[evencount];
				evencount++;
			}													
		}		
		return(new String(code));
	}
	
	protected static char[] generateEven(int l)
	{
		char[] even = new char[l];	
		for(int i=0; i<even.length; i++)
		{
			even[i] = nextAlpha();
		}
		return(even);
	}
	
	protected static char[] generateOdd(int l)
	{
		char[] odd = new char[l];
		char[] tempodd = String.valueOf(System.currentTimeMillis()).toCharArray();
		int paddcount = odd.length - tempodd.length;

		if(paddcount > 0)
		{
			for(int k=0; k<tempodd.length; k++)
			{
				odd[k+paddcount] = tempodd[k];
			}
			
			for(int j=0; j<paddcount; j++)
			{
				odd[j] = '0';
				//System.out.print("padded ");
				//System.out.println(tempodd);
			}
		}
		else
		{
			for(int k = odd.length - 1; k>=0; k--)
			{
				odd[k] = tempodd[k - paddcount];
			}
		}
		return(odd);
	}
	
	protected static char nextAlpha()
	{
		int i = -1;
		while(!((i>=65&&i<=90)||(i>=48&&i<=57)))
		{
			i = (int)(Math.random() * 100);
			//System.out.println(i);
		}
		return(getAsciiChar(i));
	}
	
   protected static char nextNumber()
   {
      int i = -1;
      while(!(i>=48&&i<=57))
      {
         i = (int)(Math.random() * 100);
      }
      return(getAsciiChar(i));
   }

	protected static char getAsciiChar(int ascii)
	{
		char c = '-';
		if(ascii > 128)
		{
			ascii = (ascii - 256);
		}
		//System.out.print("ascii");
		//System.out.println(ascii);
		c = (char)ascii;
		return(c);
	}
	
	protected static int getAsciiInt(char c)
	{
		int ascii = (int)c;
		if(ascii < 0)
		{
			ascii = (256 + ascii);
		}
		//System.out.print("ascii");
		//System.out.println(ascii);
		return(ascii);
	}
	
   public static String getRandomNumberString(int l)
   {  
      char[] numbers = new char[l];
      for(int j=0; j<numbers.length; j++)
      {
         numbers[j] = nextNumber();
      }
      return(new String(numbers));
   }
   	
   protected static char[] getOdd(String key)
   {
      char[] odd = new char[key.length()/2];
      for(int i=0; i<odd.length; i++)
      {
         odd[i] = key.charAt(i * 2);
      }
      return(odd);
   }	
   
   public static Date getTimeFromKey(String key) 
   {
      String time = new String(getOdd(key));
      long nanosec = Long.parseLong(time);
      Date date = new Date (nanosec);
      return(date);
   }   
   
	public static void main(String[] args)
	{
	
		for(int i=0; i<1000; i++)
		{
			System.out.println(getRandomNumberString(8));
		}
		
		//System.out.println(generate("adasdassda",null));
		//System.out.println(generate("adasdassdaz023",null));
	}	
}
