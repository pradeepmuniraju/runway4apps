package com.sok.framework;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * @deprecated
 * use com.sok.runway.ContactLoginFilter instead
 */   
public class LoginFilter implements Filter
{
	public static final String loginstatus = "-sessionstatus";
	public static final String failed = "failed";	
	public static final String authenticated = "authenticated";	
	public static final String deactivated = "deactivated";
	public static final String expired = "expired";
	
	String loginpage;
    String deactivatedpage;
	String realm;
	String loginbean;
	String escape;

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws ServletException, IOException
	{
		HttpServletRequest httprequest = (HttpServletRequest)request;
		HttpSession session = httprequest.getSession(false);
		String requesturi = httprequest.getRequestURI();
		if(realm==null || requesturi.indexOf(realm)>=0)
		{
			if(session!=null)
			{ 
				RowBean bean = (RowBean)session.getAttribute(loginbean);
				if(escape!=null && requesturi.indexOf(escape)>=0)
				{
					chain.doFilter(request,response);
				}
				else if(bean!=null && bean.getString(loginstatus).equals(authenticated))
				{
					chain.doFilter(request,response);
				}
				else
				{
					bounce(request ,response, bean);
				}
			}
			else if(escape!=null && requesturi.indexOf(escape)>=0)
			{
				chain.doFilter(request,response);
			}
			else
			{
				bounce(request, response, null);
			}
		}
		else
		{
			chain.doFilter(request,response);
		}
	}
	
    
    
	public void bounce(ServletRequest request, ServletResponse response, RowBean bean)throws IOException
	{
		StringBuffer bounceurl = new StringBuffer();
		bounceurl.append(((HttpServletRequest)request).getContextPath());
        if(bean == null || !bean.getString(loginstatus).equals(deactivated))
        {
            bounceurl.append(loginpage);
        }
        else
        {
            bounceurl.append(deactivatedpage);
        }
		((HttpServletResponse)response).sendRedirect(bounceurl.toString());
	}
	
	public void init(FilterConfig config)throws ServletException
	{
		loginpage = config.getInitParameter("loginPage");
        deactivatedpage = config.getInitParameter("deactivatedPage");
        if(deactivatedpage == null)
        {
            deactivatedpage = loginpage;
        }
		realm = config.getInitParameter("realm");
		loginbean = config.getServletContext().getInitParameter("CMSLoginBean");
		if(loginbean==null || loginbean.length()==0)
		{
			loginbean = config.getInitParameter("loginBean");
		}
		escape = config.getInitParameter("escape");
	}
	
	public void destroy()
	{
		
	}
}
