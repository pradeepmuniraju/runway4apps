package com.sok.framework;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HolidayUtil {

	private static final Logger logger = LoggerFactory.getLogger(HolidayUtil.class);
	public static Calendar[] holidays_VIC = {
	/* 2012 dates from: http://www.vic.gov.au/Victorian-Public-Holiday-Dates.html */
	new GregorianCalendar(2012, 0, 26, 0, 0), /* australia day */
	new GregorianCalendar(2012, 2, 12, 0, 0), /* labor day */
	new GregorianCalendar(2012, 3, 6, 0, 0), /* good friday */
	new GregorianCalendar(2012, 3, 9, 0, 0), /* easter monday */
	new GregorianCalendar(2012, 3, 25, 0, 0), /* anzac holiday */
	new GregorianCalendar(2012, 5, 11, 0, 0), /* queens birthday */
	new GregorianCalendar(2012, 10, 6, 0, 0), /* cup day */
	new GregorianCalendar(2012, 11, 26, 0, 0), /* boxing day */
	new GregorianCalendar(2012, 11, 25, 0, 0), /* xmas day holiday */
	new GregorianCalendar(2012, 11, 27, 0, 0), /* annual leave */
	new GregorianCalendar(2012, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2012, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2012, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2012, 11, 31, 0, 0), /* annual leave */
	new GregorianCalendar(2013, 0, 1, 0, 0), /* annual leave */
	new GregorianCalendar(2013, 0, 2, 0, 0), /* annual leave */
	new GregorianCalendar(2013, 0, 3, 0, 0), /* annual leave */
	new GregorianCalendar(2013, 0, 4, 0, 0), /* annual leave */
	new GregorianCalendar(2013, 0, 5, 0, 0), /* annual leave */
	new GregorianCalendar(2013, 0, 6, 0, 0), /* annual leave */

	new GregorianCalendar(2013, 0, 28, 0, 0), /* australia day */
	new GregorianCalendar(2013, 2, 11, 0, 0), /* labor day */
	new GregorianCalendar(2013, 2, 29, 0, 0), /* good friday */
	new GregorianCalendar(2013, 3, 1, 0, 0), /* easter monday */
	new GregorianCalendar(2013, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2013, 5, 10, 0, 0), /* queens bday */
	new GregorianCalendar(2013, 10, 5, 0, 0), /* melb cup */

	new GregorianCalendar(2013, 11, 25, 0, 0), /* xmas day holiday */
	new GregorianCalendar(2013, 11, 26, 0, 0), /* boxing day */

	new GregorianCalendar(2013, 11, 27, 0, 0), /* annual leave */
	new GregorianCalendar(2013, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2013, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2013, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2013, 11, 31, 0, 0), /* annual leave */

	new GregorianCalendar(2014, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2014, 0, 27, 0, 0), /* australia day */
	new GregorianCalendar(2014, 2, 10, 0, 0), /* labour day */
	new GregorianCalendar(2014, 3, 18, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2014, 3, 21, 0, 0), /* easter monday */
	new GregorianCalendar(2014, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2014, 5, 9, 0, 0), /* queens bday */
	new GregorianCalendar(2014, 10, 4, 0, 0), /* cup day */
	new GregorianCalendar(2014, 11, 25, 0, 0), /* xmas day */
	new GregorianCalendar(2014, 11, 26, 0, 0), /* boxing day */

	new GregorianCalendar(2014, 11, 27, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 31, 0, 0), /* annual leave */

	new GregorianCalendar(2015, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2015, 0, 2, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 0, 26, 0, 0), /* australia day */
	new GregorianCalendar(2015, 2, 9, 0, 0), /* labour day */
	new GregorianCalendar(2015, 3, 3, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2015, 3, 6, 0, 0), /* easter monday */
	new GregorianCalendar(2015, 5, 8, 0, 0), /* queens bday */
	new GregorianCalendar(2015, 10, 3, 0, 0), /* cup day */
	
	new GregorianCalendar(2015, 11, 22, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 23, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 24, 0, 0), /* annual leave */
	
	new GregorianCalendar(2015, 11, 25, 0, 0), /* xmas day */
	new GregorianCalendar(2015, 11, 28, 0, 0), /* boxing day (monday additional day) */

	new GregorianCalendar(2015, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 31, 0, 0), /* annual leave */
	
	new GregorianCalendar(2016, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 2, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 3, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 4, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 5, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 6, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 7, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 8, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 9, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 10, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 11, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 12, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 13, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 14, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 15, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 26, 0, 0), /* australia day */
	new GregorianCalendar(2016, 2, 14, 0, 0), /* labour day */
	new GregorianCalendar(2016, 2, 25, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2016, 2, 28, 0, 0), /* easter monday */
	new GregorianCalendar(2016, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2016, 5, 13, 0, 0), /* queens bday */
	new GregorianCalendar(2016, 8, 30, 0, 0), /* AFL bday */
	new GregorianCalendar(2016, 10, 1, 0, 0), /* cup day */
	
	new GregorianCalendar(2016, 11, 22, 0, 0), 
	new GregorianCalendar(2016, 11, 23, 0, 0),
	new GregorianCalendar(2016, 11, 26, 0, 0), 
	new GregorianCalendar(2016, 11, 27, 0, 0),
	new GregorianCalendar(2016, 11, 28, 0, 0), 
	new GregorianCalendar(2016, 11, 29, 0, 0),
	new GregorianCalendar(2016, 11, 30, 0, 0), /* annual leave */
	
	// 2017	
	new GregorianCalendar(2017, 0, 2, 0, 0), /* new years day */
	new GregorianCalendar(2017, 0, 3, 0, 0), /* new years day */
	new GregorianCalendar(2017, 0, 4, 0, 0), /* new years day */
	new GregorianCalendar(2017, 0, 5, 0, 0), /* new years day */
	new GregorianCalendar(2017, 0, 6, 0, 0), /* new years day */
	new GregorianCalendar(2017, 0, 9, 0, 0), /* new years day */
	new GregorianCalendar(2017, 0, 10, 0, 0), /* new years day */
	new GregorianCalendar(2017, 0, 11, 0, 0), /* new years day */
	new GregorianCalendar(2017, 0, 12, 0, 0), /* new years day */
	new GregorianCalendar(2017, 0, 13, 0, 0), /* new years day */
	
	new GregorianCalendar(2017, 0, 26, 0, 0), /* australia day */
	new GregorianCalendar(2017, 2, 13, 0, 0), /* labour day */
	new GregorianCalendar(2017, 3, 14, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2017, 3, 17, 0, 0), /* easter monday */
	new GregorianCalendar(2017, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2017, 5, 12, 0, 0), /* queens bday */
	new GregorianCalendar(2017, 8, 29, 0, 0), /* AFL bday */
	new GregorianCalendar(2017, 10, 7, 0, 0), /* cup day */
	new GregorianCalendar(2017, 11, 22, 0, 0), /* xmas day */
	new GregorianCalendar(2017, 11, 25, 0, 0), /* boxing day (monday additional day) */
	new GregorianCalendar(2017, 11, 26, 0, 0), /* annual leave */
	new GregorianCalendar(2017, 11, 27, 0, 0), /* annual leave */
	new GregorianCalendar(2017, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2017, 11, 29, 0, 0), /* annual leave */
	
	// 2018	
	new GregorianCalendar(2018, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2018, 0, 2, 0, 0), /* new years day */
	new GregorianCalendar(2018, 0, 3, 0, 0), /* new years day */
	new GregorianCalendar(2018, 0, 4, 0, 0), /* new years day */
	new GregorianCalendar(2018, 0, 5, 0, 0), /* new years day */
	new GregorianCalendar(2018, 0, 8, 0, 0), /* new years day */
	new GregorianCalendar(2018, 0, 9, 0, 0), /* new years day */
	new GregorianCalendar(2018, 0, 10, 0, 0), /* new years day */
	new GregorianCalendar(2018, 0, 11, 0, 0), /* new years day */
	new GregorianCalendar(2018, 0, 12, 0, 0) /* new years day */
	};

	public static Calendar[] holidays_NSW = {
	/* 2014 & 2015 dates from: http://australia.gov.au/topics/australian-facts-and-figures/public-holidays#NSW */

	new GregorianCalendar(2014, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2014, 0, 27, 0, 0), /* australia day */
	new GregorianCalendar(2014, 3, 18, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2014, 3, 21, 0, 0), /* easter monday */
	new GregorianCalendar(2014, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2014, 5, 9, 0, 0), /* queens bday */
	new GregorianCalendar(2014, 9, 6, 0, 0), /* labour day */
	new GregorianCalendar(2014, 11, 25, 0, 0), /* xmas day */
	new GregorianCalendar(2014, 11, 26, 0, 0), /* boxing day */

	new GregorianCalendar(2014, 11, 27, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 31, 0, 0), /* annual leave */

	new GregorianCalendar(2015, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2015, 0, 2, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 0, 26, 0, 0), /* australia day */
	new GregorianCalendar(2015, 3, 3, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2015, 3, 6, 0, 0), /* easter monday */
	new GregorianCalendar(2015, 5, 8, 0, 0), /* queens bday */
	new GregorianCalendar(2015, 9, 5, 0, 0), /* labour day */
	new GregorianCalendar(2015, 11, 25, 0, 0), /* xmas day */
	new GregorianCalendar(2015, 11, 28, 0, 0), /* boxing day (monday additional day) */

	new GregorianCalendar(2015, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 31, 0, 0), /* annual leave */
	
	new GregorianCalendar(2016, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 26, 0, 0), /* australia day */	
	new GregorianCalendar(2016, 2, 25, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2016, 2, 28, 0, 0), /* easter monday */
	new GregorianCalendar(2016, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2016, 5, 13, 0, 0), /* queens bday */
	new GregorianCalendar(2016, 9, 3, 0, 0), /* labour day */
	new GregorianCalendar(2016, 11, 27, 0, 0), /* xmas day */
	new GregorianCalendar(2016, 11, 26, 0, 0), /* boxing day (monday additional day) */

	new GregorianCalendar(2016, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2016, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2016, 11, 30, 0, 0) /* annual leave */
	};

	public static Calendar[] holidays_QLD = {
	/* 2014 & 2015 dates from: http://australia.gov.au/topics/australian-facts-and-figures/public-holidays#NSW */

	new GregorianCalendar(2014, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2014, 0, 27, 0, 0), /* australia day */
	new GregorianCalendar(2014, 3, 18, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2014, 3, 21, 0, 0), /* easter monday */
	new GregorianCalendar(2014, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2014, 5, 9, 0, 0), /* queens bday */
	new GregorianCalendar(2014, 9, 6, 0, 0), /* labour day */
	new GregorianCalendar(2014, 11, 25, 0, 0), /* xmas day */
	new GregorianCalendar(2014, 11, 26, 0, 0), /* boxing day */

	new GregorianCalendar(2014, 11, 27, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 31, 0, 0), /* annual leave */

	new GregorianCalendar(2015, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2015, 0, 2, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 0, 26, 0, 0), /* australia day */
	new GregorianCalendar(2015, 3, 3, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2015, 3, 6, 0, 0), /* easter monday */
	new GregorianCalendar(2015, 5, 8, 0, 0), /* queens bday */
	new GregorianCalendar(2015, 9, 5, 0, 0), /* labour day */
	new GregorianCalendar(2015, 11, 25, 0, 0), /* xmas day */
	new GregorianCalendar(2015, 11, 28, 0, 0), /* boxing day (monday additional day) */

	new GregorianCalendar(2015, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 31, 0, 0), /* annual leave */
	
	new GregorianCalendar(2016, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 26, 0, 0), /* australia day */	
	new GregorianCalendar(2016, 2, 25, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2016, 2, 28, 0, 0), /* easter monday */
	new GregorianCalendar(2016, 3, 25, 0, 0), /* anzac day */	
	new GregorianCalendar(2016, 4, 2, 0, 0), /* labour day */
	new GregorianCalendar(2016, 9, 3, 0, 0), /* queens bday */
	new GregorianCalendar(2016, 11, 27, 0, 0), /* xmas day */
	new GregorianCalendar(2016, 11, 26, 0, 0), /* boxing day (monday additional day) */

	new GregorianCalendar(2016, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2016, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2016, 11, 30, 0, 0) /* annual leave */
	
	
	};
	public static Calendar[] holidays_SA = {
	/* 2014 & 2015 dates from: http://australia.gov.au/topics/australian-facts-and-figures/public-holidays#NSW */

	new GregorianCalendar(2014, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2014, 0, 27, 0, 0), /* australia day */
	new GregorianCalendar(2014, 2, 10, 0, 0), /* March Public day */
	new GregorianCalendar(2014, 3, 18, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2014, 3, 21, 0, 0), /* easter monday */
	new GregorianCalendar(2014, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2014, 5, 9, 0, 0), /* queens bday */
	new GregorianCalendar(2014, 9, 6, 0, 0), /* labour day */
	new GregorianCalendar(2014, 11, 25, 0, 0), /* xmas day */
	new GregorianCalendar(2014, 11, 26, 0, 0), /* boxing day */

	new GregorianCalendar(2014, 11, 27, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 31, 0, 0), /* annual leave */

	new GregorianCalendar(2015, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2015, 0, 2, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 0, 26, 0, 0), /* australia day */
	new GregorianCalendar(2015, 3, 3, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2015, 3, 6, 0, 0), /* easter monday */
	new GregorianCalendar(2015, 5, 8, 0, 0), /* queens bday */
	new GregorianCalendar(2015, 9, 5, 0, 0), /* labour day */
	new GregorianCalendar(2015, 11, 25, 0, 0), /* xmas day */
	new GregorianCalendar(2015, 11, 28, 0, 0), /* boxing day (monday additional day) */

	new GregorianCalendar(2015, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 31, 0, 0), /* annual leave */
	
	new GregorianCalendar(2016, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 26, 0, 0), /* australia day */	
	new GregorianCalendar(2015, 2, 14, 0, 0), /* labour day */
	new GregorianCalendar(2016, 2, 25, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2016, 2, 28, 0, 0), /* easter monday */
	new GregorianCalendar(2016, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2016, 5, 13, 0, 0), /* queens bday */	
	new GregorianCalendar(2016, 9, 3, 0, 0), /* labour day */	
	new GregorianCalendar(2016, 11, 27, 0, 0), /* xmas day */
	new GregorianCalendar(2016, 11, 26, 0, 0), /* boxing day (monday additional day) */

	new GregorianCalendar(2016, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2016, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2016, 11, 30, 0, 0) /* annual leave */
	};

	public static Calendar[] holidays_WA = {
	/* 2014 & 2015 dates from: http://australia.gov.au/topics/australian-facts-and-figures/public-holidays#NSW */

	new GregorianCalendar(2014, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2014, 0, 27, 0, 0), /* australia day */
	new GregorianCalendar(2014, 2, 10, 0, 0), /* labour day */
	new GregorianCalendar(2014, 3, 18, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2014, 3, 21, 0, 0), /* easter monday */
	new GregorianCalendar(2014, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2014, 5, 2, 0, 0), /* WA day */
	new GregorianCalendar(2014, 29, 8, 0, 0), /* queens bday */
	new GregorianCalendar(2014, 11, 25, 0, 0), /* xmas day */
	new GregorianCalendar(2014, 11, 26, 0, 0), /* boxing day */

	new GregorianCalendar(2014, 11, 27, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2014, 11, 31, 0, 0), /* annual leave */

	new GregorianCalendar(2015, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2015, 0, 2, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 0, 26, 0, 0), /* australia day */
	new GregorianCalendar(2015, 3, 3, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2015, 3, 6, 0, 0), /* easter monday */
	new GregorianCalendar(2014, 5, 1, 0, 0), /* WA day */
	new GregorianCalendar(2014, 29, 28, 0, 0), /* queens bday */
	new GregorianCalendar(2015, 11, 25, 0, 0), /* xmas day */
	new GregorianCalendar(2015, 11, 28, 0, 0), /* boxing day (monday additional day) */

	new GregorianCalendar(2015, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 30, 0, 0), /* annual leave */
	new GregorianCalendar(2015, 11, 31, 0, 0), /* annual leave */
	
	new GregorianCalendar(2016, 0, 1, 0, 0), /* new years day */
	new GregorianCalendar(2016, 0, 26, 0, 0), /* australia day */	
	new GregorianCalendar(2015, 2, 7, 0, 0), /* labour day */
	new GregorianCalendar(2016, 2, 25, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
	new GregorianCalendar(2016, 2, 28, 0, 0), /* easter monday */
	new GregorianCalendar(2016, 3, 25, 0, 0), /* anzac day */
	new GregorianCalendar(2016, 5, 6, 0, 0), /* WA bday */
	new GregorianCalendar(2016, 8, 26, 0, 0), /* queens bday */
	new GregorianCalendar(2016, 11, 27, 0, 0), /* xmas day */
	new GregorianCalendar(2016, 11, 26, 0, 0), /* boxing day (monday additional day) */

	new GregorianCalendar(2016, 11, 28, 0, 0), /* annual leave */
	new GregorianCalendar(2016, 11, 29, 0, 0), /* annual leave */
	new GregorianCalendar(2016, 11, 30, 0, 0) /* annual leave */
	};
}