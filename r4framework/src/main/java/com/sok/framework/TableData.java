package com.sok.framework;

import java.util.Map;

import com.sok.framework.generation.util.CalendarKeys;
import com.sok.framework.sql.AbstractDatabase;

public interface TableData extends com.sok.framework.generation.RowData, CalendarKeys
{

	//actions	
	static final String UPDATE = ActionBean.UPDATE;
	static final String INSERT = ActionBean.INSERT;
	static final String DELETE = ActionBean.DELETE;
	static final String SELECT = ActionBean.SELECT;
	static final String SEARCH = ActionBean.SEARCH;
	static final String ACTION = ActionBean.ACTION;
	
	static final String SORT1 = ActionBean.SORT1;
	static final String SORT2 = ActionBean.SORT2;
	static final String SORT3 = ActionBean.SORT3;
	static final String SORT4 = ActionBean.SORT4;
	static final String SORT5 = ActionBean.SORT5;
	static final String ORDER1 = ActionBean.ORDER1;
	static final String ORDER2 = ActionBean.ORDER2;
	static final String ORDER3 = ActionBean.ORDER3;
	static final String ORDER4 = ActionBean.ORDER4;
	static final String ORDER5 = ActionBean.ORDER5;
	
	static final String TYPE_DATETIME = ActionBean.TYPE_DATETIME;
	static final String TYPE_DATE = ActionBean.TYPE_DATE;
	static final String TYPE_TIME = ActionBean.TYPE_TIME;
	static final String TYPE_NUMBER = ActionBean.TYPE_NUMBER;
	static final String TYPE_PERCENT = ActionBean.TYPE_PERCENT;
	static final String TYPE_CURRENCY = ActionBean.TYPE_CURRENCY;
	static final String TYPE_DECIMAL = ActionBean.TYPE_DECIMAL;
	
	static final String NULL = ActionBean.NULL;
	static final String EMPTY = ActionBean.EMPTY;
	
	//static final int GT = ActionBean.GT;
	static final int LT = AbstractDatabase.LT;
	static final int NE = AbstractDatabase.NE;
	static final int LK = AbstractDatabase.LK;
	static final String _blank = ActionBean._blank;
	static final String _dot = ActionBean._dot;
	static final String _comma = ActionBean._comma;
	static final String _and = ActionBean._and;
	static final String _or = ActionBean._or;
	static final String _as = ActionBean._as;
	static final String _omit = ActionBean._omit;

	static final String usdateinput = AbstractDatabase.usdateinput;
	static final String ukdateinput = AbstractDatabase.ukdateinput;
	
//	static final String usdateconvert = AbstractDatabase.usdateconvert;
//	static final String ukdateconvert = AbstractDatabase.ukdateconvert;

	public Relation getRelatedFields();

	public void setDateInputFormat(String d);
	
	public Object getObject(String name);

	public String getColumn(String name, String format);

	public String getColumn(String name);
	
	public String getColumnHTML(String name);

	public String getCodedString(String name);
	
	public String getCodedColumn(String name);
	
	public String getLockKey(); 
	
	public void setColumn(String name, String value);
	
	public void setParameter(String name, Object value);
	
	public String getParameter(String name);
	
	public String[] getParameterValues(String name);

   public String getData(String name);
   
	public String getDateTime(String name);

	public void sortBy(String field, int index);

	public void sortOrder(String order, int index);

	public void setToNewID(String name);

	public void createNewID();

	public StringBuffer createdDataOnIDStatment();

	//public void generateSQLStatment();
	public void generateSQLStatement();

	public void setSearchStatement(String s);

	public void setSearchCriteria(String s);
	
	public String getSearchCriteria();

	public void setSort(int index, String sort, String order);

	public String[] getSortfields();

 	public String[] getSortorder();

	public String getDateConversionsType();

	public void clear();

	//public Object put(Object key,Object obj);
	
	//public Object get(Object key);

	public boolean containsKey(Object key);

   /*
    * <NBCHANGE - 300403 - 2>
    */
   public void putAll(Map t);
   
   // </NBCHANGE - 300403 - 2>
    
	public Map getSearchGroups();

	public void setSearchGroups(Map groups);

	public int getTop();

	public void setTop(int topcount);
	
    //public void setActionSpec(String as); 
    
    //public ActionSpec getActionSpec();  

    public String getSearchStatement();

	public boolean isSet(String string); 
}
