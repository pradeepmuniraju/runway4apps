package com.sok.framework;

import com.sok.framework.generation.DatabaseException;
import com.sok.framework.generation.RowData;
import com.sok.framework.generation.util.CalendarFactory;
import com.sok.framework.generation.util.CalendarKeys;
import com.sok.framework.sql.AbstractDatabase;
import com.sok.framework.sql.MSSQL;
import com.sok.framework.sql.SqlDatabase;
import com.sok.runway.UserBean;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import javax.sql.RowSet;

import org.apache.commons.lang.StringUtils;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URLDecoder;
import java.sql.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class ActionBean implements CalendarKeys {
	
	private static final Logger logger = LoggerFactory.getLogger(ActionBean.class);

    //actions *
    public static final String UPDATE = "update";
    public static final String UPDATEALL = "updateall";
    public static final String INSERT = "insert";
    public static final String DELETE = "delete";
    public static final String DELETEALL = "deleteall";
    public static final String SELECT = "select";
    public static final String SEARCH = "search";
    public static final String EXECUTE = "execute";
    public static final String UPDATEON = "ON-";
    public static final String ACTION = "-action";
    public static final String UPDATESTMT = "-updatestmt";

    public static final String CON = "-connection";

    public static final String database = "-database";
    public static final String server = "-server";
    public static final String DBCREATOR = "dbo";

    static final String SORT1 = "-sort1";
    static final String SORT2 = "-sort2";
    static final String SORT3 = "-sort3";
    static final String SORT4 = "-sort4";
    static final String SORT5 = "-sort5";
    static final String ORDER1 = "-order1";
    static final String ORDER2 = "-order2";
    static final String ORDER3 = "-order3";
    static final String ORDER4 = "-order4";
    static final String ORDER5 = "-order5";

    public static final String TYPE_DATETIME = "datetime";
    public static final String TYPE_DATE = "date";
    public static final String TYPE_TIME = "time";
    public static final String TYPE_NUMBER = "number";
    public static final String TYPE_LONG = "long";
    public static final String TYPE_PERCENT = "percent";
    public static final String TYPE_CURRENCY = "currency";
    public static final String TYPE_DECIMAL = "decimal";
    public static final String TYPE_BOOLEAN = "boolean";
    public static final String TYPE_BLOB = "blob";

    public static final String CURRENTUSER = "CURRENTUSER";
    public static final String CURRENTUSERID = "CURRENTUSERID";
    public static final String CREATEDBY = "CreatedBy";
    public static final String CREATEDDATE = "CreatedDate";
    public static final String MODIFIEDDATE = "ModifiedDate";
    public static final String MODIFIEDBY = "ModifiedBy";
    public static final String userid = "UserID";
    public static final String currentuser = "currentuser";

    public static final String NULL = "NULL";
    public static final String EMPTY = "EMPTY";

    public static final String _blank = "";
    public static final String _dot = ".";
    public static final String _comma = ", ";
    public static final String _and = " and ";
    public static final String _or = " or ";
    public static final String _as = " as ";
    public static final String _omit = "omit";
    public static final String _equals = " = ";
    public static final String _plus = "+";
    public static final String _at = "@";
    public static final String _space = " ";
    public static final String _percent = "%";
    public static final String _underscore = "_";

    static final String RN = "\r\n";

    static final String AND = "AND";
    static final String OR = "OR";

    public static final String defaulttimezone = CalendarFactory.defaulttimezone;
    public static final String subcontextname = "java:/comp/env";

    public static final String utf8 = "UTF-8";

    static String jndiName = null;
    static SqlDatabase sqlDatabase = null;
    static Locale locale = null;


    public ActionBean() {

    }

    public static String escapeQuote(String s) {
        if (s.indexOf('\'') >= 0) {
            StringBuffer escaped = new StringBuffer();
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '\'') {
                    escaped.append(s.charAt(i));
                }
                escaped.append(s.charAt(i));
            }
            return (escaped.toString());
        }
        return (s);
    }

    public static SqlDatabase getDatabase() {
        return (sqlDatabase);
    }

    public static synchronized void setDatabase(String s) {
        if (s != null) {
            sqlDatabase = AbstractDatabase.getDatabase(s);
        } else {
            sqlDatabase = new MSSQL();
        }
    }

    public static synchronized void setJndiName(String t) {
        jndiName = t;
    }

    public static synchronized String getJndiName() {
        return (jndiName);
    }

    public static synchronized void setLocale(String l, String c) {
        Locale sysParamLocale = new Locale(l, c);
        locale = sysParamLocale;
    }

    public static synchronized String getLocale() {
        return (locale.toString());
    }

    public static void doAction(TableData row, boolean autosetvalues) {
        String action = row.getAction();
        if (action.equals(INSERT)) {
            if (autosetvalues) {
                String pkeyname = row.getTableSpec().getPrimaryKeyName();
                if (row.get(pkeyname) == null) {
                    row.setToNewID(pkeyname);
                }
                row.setColumn(CREATEDDATE, NOW);
                row.setColumn(MODIFIEDDATE, NOW);
                row.setColumn(MODIFIEDBY, row.getString(CURRENTUSERID));
                row.setColumn(CREATEDBY, row.getString(CURRENTUSERID));
            }
            insertRow(row);
        } else if (action.equals(UPDATE)) {
            if (autosetvalues) {
                row.setColumn(MODIFIEDDATE, NOW);
                row.setColumn(MODIFIEDBY, row.getString(CURRENTUSERID));
            }
            updateRow(row);
        } else if (action.equals(UPDATEALL)) {
            updateRow(row, true);
        } else if (action.equals(EXECUTE)) {
            executeStmt(row);
        } else if (action.equals(DELETE)) {
            deleteRow(row);
        } else if (action.equals(DELETEALL)) {
            deleteRow(row, true);
        } else if (action.equals(SELECT)) {
            retrieveDataOnID(row);
        } else if (action.equals(SEARCH)) {
            generateSQLStatment(row);
        }
    }

    public static void doAction(TableData row) {
        doAction(row, false);
    }

    public static void setColumn(String name, String value, TableData row) {
        if (!setRelated(name, value, row)) {
            String group = setSearchGroup(name, value, row);
            if (group == null) {
                row.put(name, value);
            } else {
                row.put(group, group);
            }
        }
    }

    /*
    * protected static String getRelated(String name, TableData row) {
    * StringTokenizer st = new StringTokenizer(name,"."); String field = null;
    * String value = null;
    *
    * value = row.getString(name); if(value == null) { Relation currentRelation =
    * row.getRelatedFields(); boolean foundrelation = false;
    * while(st.hasMoreTokens()) { field = st.nextToken();
    * if(st.hasMoreTokens()) { if(currentRelation!=null) { currentRelation =
    * currentRelation.getRelation(field); if(currentRelation != null) {
    * foundrelation = true; } } } else if(foundrelation) { if(currentRelation !=
    * null) { value = currentRelation.getField(field); } } } } if(value!=null) {
    * return(value); } return(_blank); }
    */

    protected static boolean setRelated(String name, String value, TableData row) {
        try {
            row.put(name, value);
            int dotindex = name.indexOf(_dot);
            if (dotindex >= 0) {
                Relation relatedfields = row.getRelatedFields();
                RelationSpec rspec = row.getRelationSpec();
                if (rspec != null) {
                    StringTokenizer st = new StringTokenizer(name, ".");
                    String field = null;
                    Relation currentRelation = relatedfields;
                    boolean foundrelation = false;
                    while (st.hasMoreTokens()) {
                        field = st.nextToken();
                        if (st.hasMoreTokens()) {
                            if (rspec.hasRelation(field) && currentRelation != null) {
                                if (field != null && field.length() != 0) {
                                    currentRelation = currentRelation.addRelation(field);
                                    if (currentRelation == null) {
                                        throw new Exception("addRelation(" + field
                                                + ") returned null");
                                    }
                                    foundrelation = true;
                                } else {
                                    currentRelation = currentRelation.getRelation(field);
                                    if (currentRelation != null) {
                                        foundrelation = true;
                                    }
                                }
                            }
                        } else if (foundrelation) {
                            String group = setSearchGroup(field, value, row);
                            if (group == null) {
                                if (value != null && value.length() != 0) {
                                    currentRelation.addField(field, value);
                                } else {
                                    currentRelation.removeField(field);
                                }
                            } else {
                                if (value != null && value.length() != 0) {
                                    currentRelation.addField(group, group);
                                } else {
                                    currentRelation.removeField(group);
                                }
                            }
                        }
                    }

                    return (true);
                }
            }

        }
        catch (Exception e) {
            row.setError("Error encountered processing parameters: ");
            row.setError(name);
            row.setError(" - ");
            row.setError(value);
            row.setError("\r\n");
            row.setError(writeStackTraceToString(e));
        }

        return (false);
    }

    static String setSearchGroup(String paramnamewithgroup, String paramvalue, TableData row) {
        return (setSearchGroup(paramnamewithgroup, paramvalue, row, null));
    }

    static String setSearchGroup(String paramnamewithgroup, String paramvalue, TableData row,
                                 Map parent) {
        String groupname = null;
        int orindex = paramnamewithgroup.lastIndexOf('|');
        int andindex = paramnamewithgroup.lastIndexOf('^');
        if (orindex > 0 || andindex > 0) {
            String name = null;

            if (orindex > 0 && orindex > andindex) {
                name = paramnamewithgroup.substring(0, orindex);
                groupname = paramnamewithgroup.substring(orindex, paramnamewithgroup.length());
                //System.out.println("found or group "+groupname);
            } else if (andindex > 0 && andindex > orindex) {
                name = paramnamewithgroup.substring(0, andindex);
                groupname = paramnamewithgroup.substring(andindex, paramnamewithgroup.length());
                //System.out.println("found and group "+groupname);
            }
            setSearchGroup(groupname, name, paramvalue, row, parent);
        }
        return (groupname);
    }

    static void setSearchGroup(String groupname, String paramname, String paramvalue,
                               TableData row, Map parent) {
        Map group = null;
        Map searchgroups = row.getSearchGroups();
        if (searchgroups == null) {
            searchgroups = new HashMap(10);
            group = new HashMap(10);
            searchgroups.put(groupname, group);
            row.setSearchGroups(searchgroups);
        } else {
            if (parent == null) {
                group = (Map) searchgroups.get(groupname);
                if (group == null) {
                    group = new HashMap(10);
                    searchgroups.put(groupname, group);
                }
            } else {
                group = (Map) parent.get(groupname);
                if (group == null) {
                    group = new HashMap(10);
                    parent.put(groupname, group);
                }
            }
        }
        String childgroup = setSearchGroup(paramname, paramvalue, row, group);
        if (childgroup == null) {
            group.put(paramname, paramvalue);
        }
    }

    /*
    * <NBCHANGE - 010503 - 1>
    *
    * This method provides teh equivalent of RowBean.getColumn
    *
    */

    public static String formatValue(TableData row, String name, Object obj) {
        String type = row.getViewSpec().getFormatTypeForName(name);
        String format = row.getViewSpec().getFormatForName(name);
        Locale locale = row.getLocale();
        TimeZone timezone = row.getUserTimeZoneObject();
        return formatValue(name, format, type, obj, locale, timezone);
    }

    // </NBCHANGE - 010503 - 1>

    // <NBCHANGE>
    // If you have perfomed a select which returns the objects in the DB and
    // later performed an Update (via ProcessRequestDelta or processrequest)
    // the row will contains strins instead of objects
    // this method will return the correct Object Type with the correct value
    // for the row

    static public Object getObjectFromString(TableData row, String name) {
        String type = row.getViewSpec().getFormatTypeForName(name);
        String format = row.getViewSpec().getFormatForName(name);
        Locale thislocale = row.getLocale();
        TimeZone timezone = row.getTimeZone();
        Object obj = row.get(name);
        if (obj != null) {
            if (!(obj instanceof String))
                return obj;
            try {
                if (type == null) {
                    return obj;
                } else if (type.equals(TYPE_BOOLEAN)) {
                    return new Boolean((String) obj);
                } else if (type.equals(TYPE_NUMBER)) {
                    NumberFormat nf = NumberFormat.getIntegerInstance(thislocale);
                    Integer intobj = (Integer) nf.parse((String) obj);
                    return intobj;
                } else if (type.equals(TYPE_LONG)) {
                    NumberFormat nf = NumberFormat.getIntegerInstance(thislocale);
                    Long intobj = (Long) nf.parse((String) obj);
                    return intobj;
                } else if (type.equals(TYPE_PERCENT)) {
                    NumberFormat nf = NumberFormat.getPercentInstance(thislocale);
                    Double doubleobj = (Double) nf.parse((String) obj);
                    return doubleobj;
                } else if (type.equals(TYPE_CURRENCY)) {
                    NumberFormat nf = NumberFormat.getCurrencyInstance();
                    Double doubleobj = (Double) nf.parse((String) obj);
                    return doubleobj;
                } else if (type.equals(TYPE_DECIMAL)) {
                    DecimalFormat nf = new DecimalFormat(format);
                    Double doubleobj = (Double) nf.parse((String) obj);
                    return doubleobj;
                } else if (type.equals(TYPE_DATETIME)) {
                    return (getTimeStampObject((String) obj, type, format, thislocale, timezone));
                } else if (type.equals(TYPE_DATE)) {
                    return (getDateObject((String) obj, type, format, thislocale, timezone));
                } else if (type.equals(TYPE_TIME)) {
                    return (getTimeObject((String) obj, type, format, thislocale, timezone));
                }
            }
            catch (Exception e) {
            }
        }
        return null;
    }

    public static String formatValue(String name, String format, String type, Object obj,
                                     Locale thislocale) {
        return (formatValue(name, format, type, obj, thislocale, TimeZone
                .getTimeZone(defaulttimezone)));
    }

    public static String formatValue(String name, String format, String type, Object obj, Locale thislocale, TimeZone timezone) {
        if (obj != null) {
            if (type == null) {
                if (obj instanceof java.sql.Timestamp) {
                    return (getFormatedColumn((java.sql.Timestamp) obj, type, format,
                            thislocale, timezone));
                } else if (obj instanceof java.sql.Date) {
                    return (getFormatedColumn((java.sql.Date) obj, type, format, thislocale,
                            timezone));
                } else if (obj instanceof java.sql.Time) {
                    return (getFormatedColumn((java.sql.Time) obj, type, format, thislocale,
                            timezone));
                } else if (obj instanceof java.util.Date) {
                    return (getFormatedColumn((java.util.Date) obj, type, format, thislocale,
                            timezone));
                } else if (obj instanceof Integer) {
                    return (getFormatedColumn((Integer) obj, type, format, thislocale));
                } else if (obj instanceof Long) {
                    return (getFormatedColumn((Long) obj, type, format, thislocale));
                } else if (obj instanceof Double) {
                    return (getFormatedColumn((Double) obj, type, format, thislocale));
                } else if (obj instanceof Float) {
                    return (getFormatedColumn((Float) obj, type, format, thislocale));
                } else if (obj instanceof java.lang.Boolean) {
                    return (getFormatedColumn((Boolean) obj, type, format, thislocale));
                } else {
                    return (obj.toString());
                }
            } else if (obj instanceof String) {
                return (obj.toString());
            } else if (type.equals(TYPE_BOOLEAN)) {
                return (getFormatedColumn((java.lang.Boolean) obj, type, format, thislocale));
            } else if (type.equals(TYPE_DATETIME)) {
                return (getFormatedColumn((java.sql.Timestamp) obj, type, format, thislocale,
                        timezone));
            } else if (type.equals(TYPE_DATE)) {
                return (getFormatedColumn((java.sql.Date) obj, type, format, thislocale,
                        timezone));
            } else if (type.equals(TYPE_TIME)) {
                return (getFormatedColumn((java.sql.Time) obj, type, format, thislocale,
                        timezone));
            } else if (type.equals(TYPE_NUMBER) || type.equals(TYPE_LONG)) {
                return (getFormatedNumberColumn(obj, type, format, thislocale));
            } else if (type.equals(TYPE_PERCENT)) {
                return (getFormatedPercentColumn(obj, type, format, thislocale));
            } else if (type.equals(TYPE_CURRENCY)) {
                return (getFormatedCurrencyColumn(obj, type, format, thislocale));
            } else if (type.equals(TYPE_DECIMAL)) {
                return (getFormatedDecimalColumn(obj, type, format, thislocale));
            }
        }
        return (_blank);
    }

    protected static String getFormatedCurrencyColumn(Object obj, String type,
                                                      String format, Locale thislocale) {
        NumberFormat nf = null;

        if (format != null) {
            nf = new DecimalFormat(format);
        } else if (thislocale == null) {
            nf = NumberFormat.getCurrencyInstance();
        } else {
            nf = NumberFormat.getCurrencyInstance(thislocale);
        }

        if (obj instanceof Double) {
            Double doubleobj = (Double) obj;
            return (nf.format(doubleobj.doubleValue()));
        } else if (obj instanceof Float) {
            Float floatobj = (Float) obj;
            return (nf.format(floatobj.floatValue()));
        } else if (obj instanceof Long) {
            Long longobj = (Long) obj;
            return (nf.format(longobj.longValue()));
        } else if (obj instanceof Integer) {
            Integer intobj = (Integer) obj;
            return (nf.format(intobj.intValue()));
        } else {
            return (obj.toString());
        }
    }

    protected static String getFormatedPercentColumn(Object obj, String type,
                                                     String format, Locale thislocale) {
        NumberFormat nf = null;
        if (thislocale == null) {
            nf = NumberFormat.getPercentInstance();
        } else {
            nf = NumberFormat.getPercentInstance(thislocale);
        }

        if (obj instanceof Double) {
            Double doubleobj = (Double) obj;
            return (nf.format(doubleobj.doubleValue()));
        } else if (obj instanceof Float) {
            Float floatobj = (Float) obj;
            return (nf.format(floatobj.floatValue()));
        } else if (obj instanceof Long) {
            Long longobj = (Long) obj;
            return (nf.format(longobj.longValue()));
        } else if (obj instanceof Integer) {
            Integer intobj = (Integer) obj;
            return (nf.format(intobj.intValue()));
        } else {
            return (obj.toString());
        }
    }

    protected static String getFormatedDecimalColumn(Object obj, String type,
                                                     String format, Locale thislocale) {
        NumberFormat nf = null;
        if (format != null) {
            nf = new DecimalFormat(format);
        } else if (thislocale == null) {
            nf = new DecimalFormat();
        } else {
            nf = NumberFormat.getNumberInstance(thislocale);
        }

        if (obj instanceof Double) {
            Double doubleobj = (Double) obj;
            return (nf.format(doubleobj.doubleValue()));
        } else if (obj instanceof Float) {
            Float floatobj = (Float) obj;
            return (nf.format(floatobj.floatValue()));
        } else if (obj instanceof Long) {
            Long longobj = (Long) obj;
            return (nf.format(longobj.longValue()));
        } else if (obj instanceof Integer) {
            Integer intobj = (Integer) obj;
            return (nf.format(intobj.intValue()));
        } else {
            return (obj.toString());
        }
    }

    protected static String getFormatedNumberColumn(Object obj, String type,
                                                    String format, Locale thislocale) {
        NumberFormat nf = null;
        if (format != null) {
            nf = new DecimalFormat(format);
            nf.setParseIntegerOnly(true);
        } else if (thislocale == null) {
            nf = NumberFormat.getIntegerInstance();
        } else {
            nf = NumberFormat.getIntegerInstance(thislocale);
        }

        if (obj instanceof Long) {
            Long longobj = (Long) obj;
            return (nf.format(longobj.longValue()));
        } else if (obj instanceof Integer) {
            Integer intobj = (Integer) obj;
            return (nf.format(intobj.intValue()));
        } else if (obj instanceof Double) {
            Double doubleobj = (Double) obj;
            return (nf.format(doubleobj.doubleValue()));
        } else if (obj instanceof Float) {
            Float floatobj = (Float) obj;
            return (nf.format(floatobj.floatValue()));
        }

        return (obj.toString());
    }

    protected static String getFormatedColumn(Long value, String formattype,
                                              String format, Locale thislocale) {
        NumberFormat nf = null;
        if (format != null) {
            nf = new DecimalFormat(format);
        } else if (thislocale != null) {
            nf = NumberFormat.getIntegerInstance(thislocale);
        } else {
            nf = NumberFormat.getIntegerInstance();
        }
        return (nf.format(value.longValue()));
    }

    protected static String getFormatedColumn(Integer value, String formattype,
                                              String format, Locale thislocale) {
        NumberFormat nf = null;
        if (format != null) {
            nf = new DecimalFormat(format);
        } else if (thislocale != null) {
            nf = NumberFormat.getIntegerInstance(thislocale);
        } else {
            nf = NumberFormat.getIntegerInstance();
        }
        return (nf.format(value.intValue()));
    }

    protected static String getFormatedColumn(java.lang.Double value, String formattype,
                                              String format, Locale thislocale) {
        NumberFormat nf = new DecimalFormat(format);
        ;
        return (nf.format(value.doubleValue()));
    }

    protected static String getFormatedColumn(java.lang.Float value, String formattype,
                                              String format, Locale thislocale) {
        NumberFormat nf = new DecimalFormat(format);
        ;
        return (nf.format(value.floatValue()));
    }

    protected static String getFormatedColumn(java.lang.Boolean value, String formattype,
                                              String format, Locale thislocale) {
        if (value.equals(Boolean.TRUE))
            return "true";
        return "false";
    }

    protected static String getFormatedColumn(java.sql.Timestamp value, String formattype,
                                              String format, Locale thislocale, TimeZone timezone) {
        DateFormat dt = getDateFormatter(value, format, thislocale, timezone);
        if (value == null) {
            return (null);
        } else if (dt == null) {
            return (value.toString());
        } else {
            return (dt.format(value));
        }
    }

    protected static String getFormatedColumn(java.sql.Time value, String formattype,
                                              String format, Locale thislocale, TimeZone timezone) {
        DateFormat dt = getDateFormatter(value, format, thislocale, timezone);
        if (value == null) {
            return (null);
        } else if (dt == null) {
            return (value.toString());
        } else {
            return (dt.format(value));
        }
    }

    protected static String getFormatedColumn(java.util.Date value, String formattype,
                                              String format, Locale thislocale, TimeZone timezone) {
        DateFormat dt = getDateFormatter(value, format, thislocale, timezone);
        if (value == null) {
            return (null);
        } else if (dt == null) {
            return (value.toString());
        } else {
            return (dt.format(value));
        }
    }

    // <NBCHANGE>

    protected static Timestamp getTimeStampObject(String value, String formattype, String format,
                                                  Locale thislocale, TimeZone thistimezone) {
        Timestamp ret = new Timestamp(0);
        DateFormat dt = getDateFormatter(ret, format, thislocale, thistimezone);

        if (null == value || null == dt) {
            return (null);
        } else {
            try {
                java.util.Date temp = dt.parse(value);
                ret = new Timestamp(temp.getTime());
            }
            catch (Exception ex) {
                // nothing to do
            }
        }
        return ret;
    }

    protected static java.sql.Time getTimeObject(String value, String formattype, String format,
                                                 Locale thislocale, TimeZone thistimezone) {
        java.sql.Time ret = new java.sql.Time(0);
        DateFormat dt = getDateFormatter(ret, format, thislocale, thistimezone);

        if (null == value || null == dt) {
            return (null);
        } else {
            try {
                java.util.Date temp = dt.parse(value);
                ret = new java.sql.Time(temp.getTime());
            }
            catch (Exception ex) {
                // nothing to do
            }
        }
        return ret;
    }

    protected static java.sql.Date getDateObject(String value, String formattype, String format,
                                                 Locale thislocale, TimeZone thistimezone) {
        java.sql.Date ret = new java.sql.Date(0);
        DateFormat dt = getDateFormatter(ret, format, thislocale, thistimezone);

        if (null == value || null == dt) {
            return (null);
        } else {
            try {
                java.util.Date temp = dt.parse(value);
                ret = new java.sql.Date(temp.getTime());
            }
            catch (Exception ex) {
                // nothing to do
            }
        }
        return ret;
    }

    protected static DateFormat getDateFormatter(Object obj, String format, Locale thislocale) {
        return (getDateFormatter(obj, format, thislocale, TimeZone.getTimeZone(defaulttimezone)));
    }

    protected static DateFormat getDateFormatter(Object obj, String format, Locale thislocale,
                                                 TimeZone timezone) {
        DateFormat dt = null;

        if (format != null) {
            int iFormat = -1;
            if (format.equals("M")) {
                iFormat = DateFormat.MEDIUM;
            } else if (format.equals("S")) {
                iFormat = DateFormat.SHORT;
            } else if (format.equals("L")) {
                iFormat = DateFormat.LONG;
            }
            if (iFormat != -1) {
                if (obj instanceof java.sql.Time) {
                    if (thislocale != null) {
                        dt = DateFormat.getTimeInstance(iFormat, thislocale);
                    } else {
                        dt = DateFormat.getTimeInstance(iFormat);
                    }
                    if (dt != null && timezone != null) { 
                    	dt.setTimeZone(timezone);
                    }
                } else if (obj instanceof java.sql.Date) {
                    if (thislocale != null) {
                        dt = DateFormat.getDateInstance(iFormat, thislocale);
                    } else {
                        dt = DateFormat.getDateInstance(iFormat);
                    }
                } else if (obj instanceof java.sql.Timestamp) {
                    if (thislocale != null) {
                        dt = DateFormat.getDateTimeInstance(iFormat, iFormat, thislocale);
                    } else {
                        dt = DateFormat.getDateTimeInstance(iFormat, iFormat);
                    }
                    if (dt != null && timezone != null) { 
                    	dt.setTimeZone(timezone);
                    }
                }
            }
            if (dt == null) {
                if (thislocale != null) {
                    dt = new SimpleDateFormat(format, thislocale);
                } else {
                    dt = new SimpleDateFormat(format);
                }
            }
        }
        //dt.setTimeZone(timezone);
        return (dt);
    }

    // </NBCHANGE>

    //deprecated, use timezone base getDateTime

    public static String getDateTime(String name, Map fields, Locale thislocale) {
        return (getDateTime(name, fields, thislocale, TimeZone.getTimeZone(defaulttimezone)));
    }

    public static String getDateTime(String name, Map fields, Locale thislocale,
                                     TimeZone thistimezone) {
        String temp = (String) fields.get(name);
        if (temp != null) {
            try {
                DateFormat thisformat = null;
                if (thislocale == null) {
                    thisformat = DateFormat.getDateInstance(DateFormat.MEDIUM);
                } else {
                    thisformat = DateFormat.getDateInstance(DateFormat.MEDIUM, thislocale);
                }
                //thisformat.setTimeZone(thistimezone);
                java.sql.Timestamp sqldate = java.sql.Timestamp.valueOf(temp);
                return (thisformat.format(sqldate));
            }
            catch (Exception ex) {
                return (_blank);
            }
        }
        return (_blank);
    }

    //deprecated, use timezone based getDate

    public static java.util.Date getDate(String value, String format, Locale thislocale) {
        return (getDate(value, format, thislocale, TimeZone.getTimeZone(ActionBean.defaulttimezone)));
    }

    //should return date in GMT but does locale time for now

    public static java.util.Date getDate(String value, String format, Locale thislocale,
                                         TimeZone thistimezone) {
    	if(value.equals(NOW)) {	            
            try {
            	Calendar cal = Calendar.getInstance();
        		// Adjust to GMT
        		cal.add(Calendar.MILLISECOND, -(cal.getTimeZone().getOffset(Calendar.ZONE_OFFSET)));
        		// Adjust to Offset
        		cal.add(Calendar.MILLISECOND, thistimezone.getOffset(Calendar.ZONE_OFFSET));
        		return new Date(cal.getTimeInMillis());
            }
            catch (Exception e) {
            }
    	}
            
    	if (format != null) {
	        // <NBCHANGE 22/04/03>
	        // just changed the (value != NOW) below to !value.equals(NOW)
	        // </NBCHANGE 22/04/03>
	        if (value != null && !value.equals(NOW)) {
	            SimpleDateFormat sdf = null;
	            if (format.length() - value.length() > 3 && format.length() > 9) {
	                format = format.substring(0, 9);
	            }
	
	            if (thislocale != null) {
	                sdf = new SimpleDateFormat(format, thislocale);
	            } else {
	                sdf = new SimpleDateFormat(format);
	            }
	            //sdf.setTimeZone(thistimezone);
	            try {
	                return (sdf.parse(value));
	            }
	            catch (Exception e) {
	            }
	        }
    	} else {
	        // if no format is given we will try some formats
    		String[] dates = new String[]{"dd/MM/yyyy h:mm:ss a","dd/MM/yyyy HH:mm:ss","yyyy-MM-dd h:mm:ss a","yyyy-MM-dd HH:mm:ss","dd/MM/yyyy h:mm a","dd/MM/yyyy HH:mm","yyyy-MM-dd h:mm a","yyyy-MM-dd HH:mm","dd/MM/yyyy","yyyy-MM-dd"};
    		if (value != null && !value.equals(NOW)) {
    			for (int d = 0; d < dates.length; ++d) {
    				SimpleDateFormat sdf = new SimpleDateFormat(dates[d]);
    				try {
    					return (sdf.parse(value));
    				} catch (Exception e){}
    			}
    		}
    	}

        return (new java.util.Date());
    }

    public static void initFromDB(RowSet rs, TableData row) {
        if (rs != null) {
            initFromDB((ResultSet) rs, row);
        }
    }

    public static String getFormatedString(String name, int i, ResultSet rs, TableData row) {
        ViewSpec vspec = row.getViewSpec();
        Locale thislocale = row.getLocale();
        TimeZone thistimezone = row.getTimeZone();
        String formattedvalue = null;
        Object value = null;
        if (rs != null) {
            String type = vspec.getFormatTypeForName(name);
            String format = vspec.getFormatForName(name);

            value = getResultObject(rs, i, type);
            formattedvalue = formatValue(name, format, type, value, thislocale, thistimezone);
        }
        return (formattedvalue);
    }

    static Object getResultObject(ResultSet rs, int i, String type) {
        Object value = null;
        try {
            if (type == null) {
                value = rs.getString(i + 1);
            } else if (type.equals(TYPE_BOOLEAN)) {
                value = new Boolean(rs.getBoolean(i + 1));
            } else if (type.equals(TYPE_DATETIME)) {
                value = rs.getTimestamp(i + 1);
            } else if (type.equals(TYPE_DATE)) {
                value = rs.getDate(i + 1);
            } else if (type.equals(TYPE_TIME)) {
                value = rs.getTime(i + 1);
            } else if (type.equals(TYPE_NUMBER)) {
                value = new Integer(rs.getInt(i + 1));
            } else if (type.equals(TYPE_LONG)) {
                value = new Long(rs.getLong(i + 1));
            } else if (type.equals(TYPE_PERCENT)) {
                value = new Double(rs.getDouble(i + 1));
            } else if (type.equals(TYPE_CURRENCY)) {
                value = new Double(rs.getDouble(i + 1));
            } else if (type.equals(TYPE_DECIMAL)) {
                value = new Double(rs.getDouble(i + 1));
            } else if (type.equals(TYPE_BLOB)) {
                Blob b = rs.getBlob(i + 1);
                value = b.getBytes(1, (int) b.length());
            }
        }
        catch (Exception e) {
        	logger.error("Error getting value object", e);
            return (_blank);
        }
        return (value);
    }

    static boolean initFromDB(ResultSet rs, TableData row) {
        ViewSpec vspec = row.getViewSpec();
        if (rs != null) {
            String name = null;
            String type = null;
            Object obj = null;
            try {
                //rs.beforeFirst();
                if (rs.next()) {
                    ResultSetMetaData meta = rs.getMetaData();
                    int columncount = meta.getColumnCount();
                    for (int i = 0; i < columncount; i++) {
                        name = meta.getColumnLabel(i + 1);
                        if (vspec != null) {
                            type = vspec.getFormatTypeForName(name);
                        }
                        obj = getResultObject(rs, i, type);
                        row.put(name, obj);
                    }
                    return (true);
                }
            }
            catch (Exception e) {
            	logger.error("Error encountered processing result set", e);
                row.setError("Error encountered processing result set: ");
                row.setError(writeStackTraceToString(e));
            }
        }
        return (false);
    }

    static boolean initFromDB(ResultSet rs, RowSetWrapperBean rowset) {
        TableData row = rowset.getResultBean();
        ViewSpec vspec = row.getViewSpec();
        if (rs != null) {
            String name = null;
            String type = null;
            Object obj = null;

            try {
                //rs.beforeFirst();
                if (rs.next()) {
                    for (int i = 0; i < rowset.getColumnCount(); i++) {
                        name = rowset.getColumnName(i + 1);
                        type = vspec.getFormatTypeForName(name);
                        obj = getResultObject(rs, i, type);
                        row.put(name, obj);
                    }
                    return (true);
                }
            }
            catch (Exception e) {
            	logger.error("Error encountered processing result set", e);
                row.setError("Error encountered processing result set: ");
                row.setError(writeStackTraceToString(e));
            }
        }
        return (false);
    }

    static boolean initFromDB(ResultSet rs, RowArrayBean row) {
        ViewSpec vspec = row.getViewSpec();
        boolean success = false;
        if (rs != null) {
            String name = null;
            String type = null;
            Object obj = null;

            RowBean newrow = null;
            try {
                //rs.beforeFirst();
                ResultSetMetaData meta = rs.getMetaData();
                int columncount = meta.getColumnCount();
                ArrayList temp = row.getList();
                if (temp == null) {
                    row.setArrayList(new ArrayList());
                } else {
                    row.clearList();
                }
                while (rs.next()) {
                    newrow = new RowBean();
                    newrow.setJndiName(row.getJndiName());
                    newrow.setViewSpec(row.getViewSpec());
                    newrow.setLocale(row.getLocale());
                    newrow.setTimeZone(row.getTimeZone());
                    for (int i = 0; i < columncount; i++) {
                        name = meta.getColumnLabel(i + 1);
                        type = vspec.getFormatTypeForName(name);
                        obj = getResultObject(rs, i, type);
                        newrow.put(name, obj);
                    }
                    row.add(newrow);
                }

                success = true;
            }
            catch (Exception e) {
            	logger.error("Error encountered processing result set", e);
                row.setError("Error encountered processing result set: ");
                row.setError(writeStackTraceToString(e));
            }
        }
        return (success);
    }

    static boolean initFromDB(ResultSet rs, int index, TableData row) {
        ViewSpec vspec = row.getViewSpec();
        if (rs != null) {
            String name = null;
            String type = null;

            try {
                //rs.beforeFirst();
                if (rs.absolute(index)) {
                    ResultSetMetaData meta = rs.getMetaData();
                    int columncount = meta.getColumnCount();
                    for (int i = 0; i < columncount; i++) {
                        name = meta.getColumnLabel(i + 1);
                        type = vspec.getFormatTypeForName(name);

                        row.put(name, getResultObject(rs, i, type));
                    }
                    return (true);
                }
            }
            catch (Exception e) {
                row.setError("Error encountered processing result set: ");
                row.setError(writeStackTraceToString(e));
            }
        }
        return (false);
    }

    public static void retrieveDataOnID(TableData row) {
        retrieveDataOnStmt(createdDataOnIDStatment(row), row);
    }

    /*
    * <NBCHANGE - 010503 - 2>
    *
    * This method will process the fields in the request object (regardless of
    * the fields in the beans hastset) and apply the results to the beans
    * hashset when completed, hence applying delta to the rowbean
    *
    */

    public static void processRequestDelta(HttpServletRequest request, TableData row) {
        RowBean requestProcessor = new RowBean();
        requestProcessor.setViewSpec(row.getViewSpec());
        requestProcessor.setJndiName(row.getJndiName());

        requestProcessor.processRequest(request);
        String res = requestProcessor.getError();
        if (0 == res.length()) {
            // the locale would have been in the form so we want it
            //row.setLocale(requestProcessor.getLocale());
            // now put all the results in aswell
            // and objectify them first so it still looks like it came from a
            // select
            Iterator keys = requestProcessor.keySet().iterator();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                Object obj = requestProcessor.getObjectFromString(key);
                row.put(key, obj);
            }
        }
    }

    // </NBCHANGE - 010503 - 2>

    public static StringBuffer createdDataOnIDStatment(TableData row) {
        return (sqlDatabase.createdDataOnIDStatment(row));
    }

    /**
     * @deprecated
     */
    public static String getNextLastHoursString(String s) {
        return (CalendarFactory.getNextLastHoursString(s));
    }

    /**
     * @deprecated
     */
    public static String getNextLastDaysString(String s) {
        return (CalendarFactory.getNextLastDaysString(s));
    }

    /**
     * @deprecated
     */
    public static String getNextLastWeeksString(String s) {
        return (CalendarFactory.getNextLastWeeksString(s));
    }

    /**
     * @deprecated
     */
    public static String getNextLastMonthsString(String s) {
        return (CalendarFactory.getNextLastMonthsString(s));
    }

    /**
     * @deprecated
     */
    public static String getNextLastMonthsInitString(String s) {
        return (CalendarFactory.getNextLastMonthsInitString(s));
    }

    /**
     * @deprecated
     */
    public static String getNextLastDaysInitString(String s) {
        return (CalendarFactory.getNextLastDaysInitString(s));
    }

    /**
     * @deprecated
     */
    public static int getInitDay(String field) {
        return (CalendarFactory.getInitDay(field));
    }

    /**
     * @deprecated
     */
    public static int getInitDayOfMonth(String field) {
        return (CalendarFactory.getInitDayOfMonth(field));
    }

    /**
     * @deprecated
     */
    public static int getInitDayOfWeek(String field) {
        return (CalendarFactory.getInitDayOfWeek(field));
    }

    /**
     * @deprecated
     */
    public static String getHourString(String s) {
        return (CalendarFactory.getHourString(s));
    }

    public static void retrieveResultSet(RowSetBean row) {
        retrieveResultSet(row, false);
    }

    public static void retrieveResultSet(RowSetWrapperBean rowset) {
        TableData row = rowset.getSearchBean();
        String stmtstring = row.getSearchStatement();
        Connection con = rowset.getConnection();
        ResultSet current = null;
        Statement stmt = null;
        try {
            if (stmtstring == null) {
                row.generateSQLStatement();
                stmtstring = row.getSearchStatement();
            }

            if (con == null) {
                con = connect(rowset);
            }

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

            current = stmt.executeQuery(stmtstring);
            rowset.setStatement(stmt);
            rowset.setResultSet(current);
        }
        catch (Exception ex) {
        	logger.error("Error encountered on retrieveResultSet", ex);
            row.setError(writeStackTraceToString(ex));
            row.setError("\r\n");
            row.setError(stmtstring.toString());
            //throw new ServletException(ex.getMessage(), ex);
        }
    }

    public static void retrieveResultSet(TableDataSet row, boolean scrollSensitive) {
        String stmtstring = row.getSearchStatement();
        Connection con = row.getConnection();
        ResultSet current = null;
        Statement stmt = null;
        try {
            if (stmtstring == null) {
                row.generateSQLStatement();
                stmtstring = row.getSearchStatement();
            }

            if (con == null) {
                con = connect(row);
            }
            //Statement stmt =
            // con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
            // ResultSet.CONCUR_UPDATABLE);
            if (scrollSensitive) {
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
            } else {
                stmt = con.createStatement();
            }
            current = stmt.executeQuery(stmtstring);
            row.setStatement(stmt);
            row.setResultSet(current);
        }
        catch (Exception ex) {
        	logger.error("Error encountered on retrieveResultSet", ex);
            row.setError(writeStackTraceToString(ex));
            row.setError("\r\n");
            row.setError(stmtstring.toString());
            //throw new ServletException(ex.getMessage(), ex);
        }
    }

    public static void retrieveDataOnStmt(TableData row) {
        retrieveDataOnStmt(row.getSearchStatement(), row);
    }

    public static Connection connect() {
    	return connect(null);	
    }
    
    /*
	public Connection getConnection()
	{
        Connection con = null;
        Context ctx = null;
		
		try
		{
			if(ctx == null)
			{
				ctx = new InitialContext();
			}

			String dbconn = "";
			DataSource ds = null;
			try
			{
				dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
				Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
				ds = (DataSource) envContext.lookup(dbconn);
			}
			catch(Exception e)
			{
			}
			if(ds == null)
			{
				ds = (DataSource) ctx.lookup(dbconn);
			}
			con = ds.getConnection();
		}
		catch(Exception e)
		{
		}
		finally
		{
			try
			{
				ctx.close();
				ctx = null;
			}
			catch(Exception e)
			{
			}
		}
		
		return con;
	}
	*/

	public static Connection connect(com.sok.framework.generation.TableSource row) {
        Connection con = null;
        Context ctx = null;
        String jndiname = row != null ? row.getJndiName(): null;
        if (jndiname == null) {
        	// Required Before InitServlet starts up. InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
            jndiname = ActionBean.getJndiName();
            if (jndiname == null) {
            	try {
            		jndiName = InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
            	} catch (Exception e) {}
            }
        }
        if (jndiname == null) {
            throw new DatabaseException("JNDI name not found");
        }
        try {

            ctx = new InitialContext();
            DataSource ds = null;

            try {
                Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
                ds = (DataSource) envContext.lookup(jndiname);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            if (ds == null) {
                ds = (DataSource) ctx.lookup(jndiname);
            }
            con = ds.getConnection();
            if(row != null) { 
	            row.setConnection(con);
	            row.setCloseConnection(true);
            }
        }
        catch (Exception ex) {
        	logger.error("Error encountered on connect", ex);
        	if(row != null) {
        		row.setError(writeStackTraceToString(ex));
        	} 
        }
        finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return (con);
    }
    
    
	public static String getSchemaName(String jndiname) {
        Context ctx = null;
        String db = "";
        if (jndiname == null) {
        	// Required Before InitServlet starts up. InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
            jndiname = ActionBean.getJndiName();
            if (jndiname == null) {
            	try {
            		jndiName = InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
            	} catch (Exception e) {}
            }
        }
        if (jndiname == null) {
            throw new DatabaseException("JNDI name not found");
        }
        try {

            ctx = new InitialContext();
            DataSource ds = null;

            try {
                Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
                ds = (DataSource) envContext.lookup(jndiname);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            if (ds == null) {
                ds = (DataSource) ctx.lookup(jndiname);
            }
            if (ds instanceof BasicDataSource) {
            	db = ((BasicDataSource) ds).getUrl();
            }
        }
        catch (Exception ex) {
        	logger.error("Error encountered on connect", ex);
        }
        finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return db;
    }
    
    
    protected static void retrieveDataOnStmt(String stmtstring, TableData row) {
        Connection con = null;
        Context ctx = null;
        ResultSet current = null;
        Statement stmt = null;

        try {
            con = row.getConnection();
            if (con == null) {
                ctx = new InitialContext();
                DataSource ds = null;
                try {
                    Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
                    ds = (DataSource) envContext.lookup(row.getJndiName());
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                if (ds == null) {
                    ds = (DataSource) ctx.lookup(row.getJndiName());
                }
                con = ds.getConnection();
                row.setConnection(con);
                row.setCloseConnection(true);
            }
            //Statement stmt =
            // con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
            // ResultSet.CONCUR_UPDATABLE);
            stmt = con.createStatement();
            current = stmt.executeQuery(stmtstring);
            initFromDB(current, row);
        }
        catch (Exception ex) {
        	logger.error("Error encountered on retrieveDataOnStmt", ex);
            row.setError(writeStackTraceToString(ex));
            row.setError("\r\n");
            row.setError(stmtstring.toString());
            //throw new ServletException(ex.getMessage(), ex);
        }
        finally {
            if (stmt != null) {
                try {
                    stmt.close();
                }
                catch (SQLException ex) {
                }
            }
            if (current != null) {
                try {
                    current.close();
                }
                catch (SQLException ex) {
                }
            }
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException ex) {
                }
            }
            if (row.getCloseConnection()) {
                row.close();
            }
        }
    }

    protected static void retrieveDataOnStmt(StringBuffer stmtstring, TableData row) {
        retrieveDataOnStmt(stmtstring.toString(), row);
    }

    public static void parseRequest(String query, TableData row) {
        parseRequest(query, row, true);
    }

    public static void parseRequest(String query, TableData row, boolean allowempty) {
        StringTokenizer pst = new StringTokenizer(query, "&");
        String name = null;
        String value = null;
        while (pst.hasMoreTokens()) {
            StringTokenizer nvst = new StringTokenizer(pst.nextToken(), "=");
            if (nvst.hasMoreTokens()) {
                name = nvst.nextToken();
                if (name != null && name.length() != 0) {
                    if (nvst.hasMoreTokens()) {
                        value = nvst.nextToken();
                        if (value != null && value.length() != 0) {
                            if (name.indexOf("-") == 0) {
                                setParam(StringUtil.urlDecode(name),
                                        StringUtil.urlDecode(value), row);
                            } else {
                                setColumn(StringUtil.urlDecode(name),
                                        StringUtil.urlDecode(value), row);
                            }
                        } else if (allowempty) {
                            row.setColumn(StringUtil.urlDecode(name), value);
                        }
                    } else if (allowempty) {
                        try {
                            row.setColumn(URLDecoder.decode(name, utf8), null);
                        }
                        catch (Exception e) {
                        }
                    }
                }
            }
        }
    }

    public static void parseRequest(HttpServletRequest request, TableData row) {
        parseRequest(request, row, true);
    }

    public static void parseRequest(HttpServletRequest request, TableData row, String groupPrefix) {
        parseRequest(request, row, true, groupPrefix);
    }

    static void setSpecs(HttpServletRequest request, TableData row) {
        String spec = request.getParameter(RowBean.viewspec);
        if (spec != null && spec.length() != 0) {
            row.setViewSpec(spec);
        }
        spec = request.getParameter(RowBean.tablespec);
        if (spec != null && spec.length() != 0) {
            row.setTableSpec(spec);
        }

    }

    static void setUserID(HttpServletRequest request, RowData row) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            TableData user = (TableData) session.getAttribute(currentuser);
            if (user != null) {
                row.put(CURRENTUSERID, user.getString(userid));
            }
        }
    }

    public static void setRequest(HttpServletRequest request, RowData row) {
        row.setLocale(locale);
        row.setConnection(request);
        setUserID(request, row);
        
        UserBean currentUser = (UserBean) request.getSession().getAttribute("currentuser");
		if (currentUser != null && StringUtils.isNotBlank(currentUser.getString("TimeZone"))) {
			row.setUserTimeZone(currentUser.getString("TimeZone"));
		}
    }

    public static void setRequest(HttpServletRequest request, RowSetWrapperBean row) {
        row.setConnection(request);
    }

    public static void parseRequest(HttpServletRequest request, TableData row, boolean allowempty) {
        parseRequest(request, row, allowempty, false, null);
    }

    public static void parseRequest(HttpServletRequest request, TableData row, boolean allowempty, String groupPrefix) {
        parseRequest(request, row, allowempty, false, groupPrefix);
    }

    public static void parseRequest(TableData request, TableData row, String groupPrefix) {
        Object[] params = request.getKeys();
        String paramname = null;
        String strippedParamname = null;
        String paramvalue = null;

        for (int i = 0; i < params.length; i++) {
            if (groupPrefix == null) {
                strippedParamname = paramname;
            } else if (paramname.startsWith(groupPrefix + _at)) {
                strippedParamname = paramname.substring(groupPrefix.length() + 1);
            }

            if (paramname.indexOf("-") == 0) {
                setParam(strippedParamname, paramvalue, row);
            } else {
                setColumn(strippedParamname, paramvalue, row);
            }
        }
    }

    public static void parseRequest(HttpServletRequest request, TableData row, boolean allowempty, boolean keepmulti, String groupPrefix) {
        try {
            setRequest(request, row);
            setSpecs(request, row);
            Enumeration params = request.getParameterNames();
            String paramname = null;
            String strippedParamname = null;
            String paramvalue = null;

            boolean evalParameter = true;
            while (params.hasMoreElements()) {
                paramname = (String) params.nextElement();
                paramvalue = request.getParameter(paramname);

                if (groupPrefix == null) {
                    evalParameter = true;
                    strippedParamname = paramname;
                } else if (paramname.startsWith(groupPrefix + _at)) {
                    strippedParamname = paramname.substring(groupPrefix.length() + 1);
                    evalParameter = true;
                } else {
                    evalParameter = false;
                }

                if (evalParameter) {
                    if (paramname.indexOf("-") == 0) {
                        if (allowempty || (paramvalue != null && paramvalue.length() != 0)) {
                            setParam(strippedParamname, paramvalue, row);
                        }
                    } else if (allowempty || (paramvalue != null && paramvalue.length() != 0)) {
                        if (keepmulti) {
                            row.put(strippedParamname, request.getParameterValues(paramname));
                        } else {
                            setColumn(strippedParamname, getParamValue(request, paramname), row);
                        }
                    }
                }
            }

            params = request.getAttributeNames();
            Object obj = null;
            while (params.hasMoreElements()) {
                paramname = (String) params.nextElement();
                obj = request.getAttribute(paramname);
                if (obj instanceof String) {
                    paramvalue = (String) obj;
                    if (paramname.indexOf("-") == 0) {
                        if (allowempty || (paramvalue != null && paramvalue.length() != 0)) {
                            setParam(paramname, paramvalue, row);
                        }
                    } else if (allowempty || (paramvalue != null && paramvalue.length() != 0)) {
                        setColumn(paramname, paramvalue, row);
                    }
                }
            }
        }
        catch (Exception e) {
            row.setError("Error encountered processing servlet request: ");
            row.setError(writeStackTraceToString(e));
        }
    }

    static void setParam(String paramname, String paramvalue, TableData row) {
        row.put(paramname, paramvalue);
        if (paramname != null && paramvalue != null) {
            try {
                if (paramname.equals(ACTION)) {
                    row.setAction(paramvalue);
                } else if (paramname.indexOf(RowBean.sort_prefix) == 0) {
                    int index = RowBean.sort_prefix.length();
                    row.sortBy(paramvalue,
                            Integer.parseInt(paramname.substring(index, index + 1)) - 1);
                } else if (paramname.indexOf(RowBean.order_prefix) == 0) {
                    int index = RowBean.order_prefix.length();
                    row.sortOrder(paramvalue, Integer.parseInt(paramname
                            .substring(index, index + 1)) - 1);
                } else if (paramname.equals(RowBean.limit_results)) {
                    try {
                        row.setTop(Integer.parseInt(paramvalue));
                    }
                    catch (Exception e) {
                    }
                }

            }
            catch (Exception e) {
                row.setError("Error encountered at setParam: ");
                row.setError(paramname);
                row.setError(paramvalue);
                row.setError(writeStackTraceToString(e));
            }
        }
    }

    public static void processRequest(HttpServletRequest request, TableData row) {
        parseRequest(request, row);
        doAction(row);
    }

    public static void processRequest(HttpServletRequest request, TableData row, String groupPrefix) {
        parseRequest(request, row, groupPrefix);
        doAction(row);
    }

    public static String getSubstituted(String template,
                                        org.apache.velocity.context.Context context, String name) {
        if (template.indexOf('$') >= 0) {
            StringWriter sw = new StringWriter();
            try {
                org.apache.velocity.app.VelocityEngine ve = VelocityManager.getEngine();
                ve.evaluate(context, sw, name, template);
                sw.flush();
                sw.close();
            }
            catch (Exception e) {
            }
            return (sw.toString());
        }
        return (template);
    }

    public static String getParamValue(HttpServletRequest request, String paramname) {
        String[] params = request.getParameterValues(paramname);
        String op = request.getParameter("-op." + paramname);
        if (params != null) {
            StringBuffer temp = new StringBuffer();
            if (op == null || op.length() == 0) {
                orParam(temp, params);
            } else if (op.equals(OR)) {
                orParam(temp, params);
            } else if (op.equals(AND)) {
                andParam(temp, params);
            }
            return (temp.toString());
        }
        return (null);
    }

    protected static void orParam(StringBuffer temp, String[] params) {
        int added = 0;
        for (int i = 0; i < params.length; i++) {
            if (params[i] != null && params[i].length() != 0) {
                if (added != 0) {
                    temp.append(_plus);
                }
                temp.append(params[i]);
                added++;
            }
        }
    }

    protected static void andParam(StringBuffer temp, String[] params) {
        int added = 0;
        for (int i = 0; i < params.length; i++) {
            if (params[i] != null && params[i].length() != 0) {
                if (added != 0) {
                    temp.append(";");
                }
                temp.append(params[i]);
                added++;
            }
        }
    }

    protected static void deleteRow(TableData row) {
        deleteRow(row, false);
    }

    protected static String getDeleteRowStmt(TableData row, boolean foreignkey) {
        boolean appendtablename = false;

        TableSpec tspec = row.getTableSpec();
        String idname = tspec.getPrimaryKeyName();
        String id = (String) row.get(idname);
        String tablename = tspec.getTableName();

        StringBuffer stmtstring = new StringBuffer();
        stmtstring.append("delete from ");

        SqlDatabase sqldatabase = row.getDatabase();
        sqldatabase.appendQualifiedTableName(stmtstring,
                row.getString(server),
                row.getString(database),
                tablename,
                null);

        stmtstring.append(" where ");

        if (foreignkey) {
            String fieldname;
            String fieldvalue;
            String updateonfieldvalue;
            boolean sanitycheck = false;
            for (int i = 0; i < tspec.getFieldsLength(); i++) {
                fieldname = tspec.getFieldName(i);
                fieldvalue = row.getString(fieldname);
                updateonfieldvalue = row.getString(UPDATEON + fieldname);
                if ((!fieldname.equals(idname) && fieldname.indexOf("ID") >= 0)
                        || updateonfieldvalue.length() != 0) {
                    if (updateonfieldvalue.length() != 0) {
                        fieldvalue = updateonfieldvalue;
                    }
                    if (fieldvalue.length() != 0) {
                        if (sanitycheck) {
                            stmtstring.append(_and);
                        }
                        if (appendtablename) {
                            stmtstring.append(tablename);
                            stmtstring.append(_dot);
                        }
                        stmtstring.append(fieldname);
                        stmtstring.append(" =\'");
                        stmtstring.append(fieldvalue);
                        stmtstring.append("\'");
                        sanitycheck = true;
                    }
                }
            }
            if (!sanitycheck) {
                stmtstring.append(tablename);
                stmtstring.append(_dot);
                stmtstring.append(idname);
                stmtstring.append(" =\'xxxxxxxxxxxxxxxx\'");
            }
        } else {
            if (appendtablename) {
                stmtstring.append(tablename);
                stmtstring.append(_dot);
            }
            stmtstring.append(idname);
            stmtstring.append(" =\'");
            stmtstring.append(id);
            stmtstring.append("\'");
        }
        return (stmtstring.toString());
    }

    protected static void deleteRow(TableData row, boolean foreignkey) {
        Connection con = null;

        try {
            con = row.getConnection();
            if (con == null) {
                con = connect(row);
            }

            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            String stmtstring = getDeleteRowStmt(row, foreignkey);
            row.put(UPDATESTMT, stmtstring);
            int r = stmt.executeUpdate(stmtstring);
            row.setUpdatedCount(r);
        }
        catch (Exception ex) {
            row.setError(writeStackTraceToString(ex));
            //throw new ServletException(ex.getMessage(), ex);
        }
        finally {
            if (row.getCloseConnection()) {
                row.close();
            }
        }
    }

    public static void generateSQLStatment(TableData row) {
        generateSQLStatement(row);
    }

    public static void generateSQLStatement(TableData row) {
        SqlDatabase sq = row.getDatabase();
        if (sq == null) {
            sq = sqlDatabase;
        }
        sq.generateSQLStatement(row);
    }

    public static String buildOrFieldClause(String fieldname, String fieldvalue) {
        return (sqlDatabase.buildOrFieldClause(fieldname, fieldvalue));
    }

    protected static String getInsertStmt(TableData row) {
        TableSpec tspec = row.getTableSpec();
        String tablename = tspec.getTableName();
        StringBuffer statement = new StringBuffer();
        statement.append("insert ");

        SqlDatabase sqldatabase = row.getDatabase();
        sqldatabase.appendQualifiedTableName(statement,
                row.getString(server),
                row.getString(database),
                tablename,
                null);

        statement.append(" (");
        int n = 0;
        for (int i = 0; i < tspec.getFieldsLength(); i++) {
            String name = tspec.getFieldName(i);
            if (row.containsKey(name)) {
                if (n != 0) {
                    statement.append(_comma);
                }
                sqldatabase.appendWithQuote(statement, name);
                n++;
            }
        }

        statement.append(") values (");

        for (int k = 0; k < n; k++) {
            if (k != 0) {
                statement.append(_comma);
            }
            statement.append("? ");
        }

        statement.append(")");
        return (statement.toString());
    }

    protected static void insertRow(TableData row) {
        Connection con = null;
        Context ctx = null;
        PreparedStatement updatestm = null;
        String stmt = _blank;

        try {
            con = row.getConnection();
            if (con == null) {
                ctx = new InitialContext();
                DataSource ds = null;
                try {
                    Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
                    ds = (DataSource) envContext.lookup(row.getJndiName());
                }
                catch (Exception e) {
                }
                if (ds == null) {
                    ds = (DataSource) ctx.lookup(row.getJndiName());
                }
                con = ds.getConnection();
                row.setConnection(con);
                row.setCloseConnection(true);
            }

            stmt = getInsertStmt(row);
            row.put(UPDATESTMT, stmt);

            updatestm = con.prepareStatement(stmt);
            setValues(updatestm, row);

            int r = updatestm.executeUpdate();
            row.setUpdatedCount(r);
        }
        catch (Exception ex) {
            row.setError(writeStackTraceToString(ex));
            row.setError("\r\n");
            row.setError(stmt);
            //throw new ServletException(ex.getMessage(), ex);
        }
        finally {
            if (updatestm != null) {
                try {
                    updatestm.close();
                }
                catch (SQLException ex) {
                }
            }
            if (row.getCloseConnection()) {
                row.close();
            }
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException ex) {
                }
            }
        }
    }

    protected static void executeStmt(TableData row) {
        row.setUpdatedCount(0);
        Connection con = null;
        PreparedStatement updatestm = null;
        String stmt = ActionBean._blank;
        try {
            con = row.getConnection();
            if (con == null) {
                con = connect(row);
            }
            stmt = row.getSearchStatement();
            if (stmt != null && stmt.length() != 0) {
                row.put(UPDATESTMT, stmt);
                updatestm = con.prepareStatement(stmt);
                int r = updatestm.executeUpdate();
                row.setUpdatedCount(r);
            }
        }
        catch (Exception ex) {
            row.setError(writeStackTraceToString(ex));
            row.setError("\r\n");
            row.setError(stmt);
        }
        finally {
            if (updatestm != null) {
                try {
                    updatestm.close();
                }
                catch (SQLException ex) {
                }
            }
            if (row.getCloseConnection()) {
                row.close();
            }
        }
    }

    protected static String getUpdateStmt(TableData row, boolean foreignkey) {
        boolean appendtablename = false;
        //stmts with qualified table names if set to true

        TableSpec tspec = row.getTableSpec();
        StringBuffer statement = new StringBuffer();
        statement.append("update ");
        String tablename = tspec.getTableName();
        SqlDatabase sqldatabase = row.getDatabase();
        sqldatabase.appendQualifiedTableName(statement,
                row.getString(server),
                row.getString(database),
                tablename,
                null);

        statement.append(" set ");

        int n = 0;
        for (int i = 0; i < tspec.getFieldsLength(); i++) {
            String fieldname = tspec.getFieldName(i);
            if (row.containsKey(fieldname)) {
                if (n != 0) {
                    statement.append(_comma);
                }
                if (appendtablename) {
                    sqldatabase.appendWithQuote(statement, tablename);
                    statement.append(_dot);
                }
                sqldatabase.appendWithQuote(statement, fieldname);
                statement.append(" = ?");
                n++;
            }
        }
        String keyname = tspec.getPrimaryKeyName();
        statement.append(" where ");
        if (foreignkey) {
            String fieldname = null;
            String updatekey = null;
            String updatekeyvalue = null;
            boolean sanitycheck = false;
            for (int i = 0; i < tspec.getFieldsLength(); i++) {
                fieldname = tspec.getFieldName(i);
                updatekey = UPDATEON + fieldname;
                if (row.containsKey(updatekey)) {
                    updatekeyvalue = row.getString(updatekey);
                    if (updatekeyvalue.length() != 0) {
                        if (sanitycheck) {
                            statement.append(_and);
                        }
                        if (appendtablename) {
                            statement.append(tablename);
                            statement.append(_dot);
                        }
                        sqldatabase.appendWithQuote(statement, fieldname);
                        statement.append(" = '");
                        statement.append(updatekeyvalue);
                        statement.append("'");
                        sanitycheck = true;
                    }
                }
            }
            if (!sanitycheck) {
                statement.append(tablename);
                statement.append(_dot);
                statement.append(keyname);
                statement.append(" =\'xxxxxxxxxxxxxxxx\'");
            }
        } else {
            if (appendtablename) {
                sqldatabase.appendWithQuote(statement, tablename);
                statement.append(_dot);
            }
            sqldatabase.appendWithQuote(statement, keyname);
            statement.append(" = \'");
            statement.append((String) row.get(keyname));
            statement.append("\'");
        }
        return (statement.toString());
    }

    protected static void updateRow(TableData row) {
        updateRow(row, false);
    }

    public static void setSelectStmt(TableData row, String select) {
        row.setSearchStatement(getSelectStmt(row, select));
    }

    protected static String getSelectStmt(TableData row, String select) {
        TableSpec tspec = row.getTableSpec();
        SqlDatabase sqldatabase = row.getDatabase();

        StringBuffer stmt = new StringBuffer();
        stmt.append("select ");
        stmt.append(select);
        stmt.append(" from ");

        SqlDatabase database = row.getDatabase();
        database.appendQualifiedTableName(stmt,
                row.getString(server),
                row.getString(ActionBean.database),
                tspec.getTableName(),
                null);

        stmt.append(" where ");
        sqldatabase.appendWithQuote(stmt, tspec.getPrimaryKeyName());
        stmt.append(" = '");
        stmt.append(row.getString(tspec.getPrimaryKeyName()));
        stmt.append("'");
        return (stmt.toString());
    }

    protected static void updateRow(Connection con, TableData row, boolean foreignkey) {
        PreparedStatement updatestm = null;
        String stmt = ActionBean._blank;
        try {
            stmt = getUpdateStmt(row, foreignkey);
            row.put(UPDATESTMT, stmt);

            updatestm = con.prepareStatement(stmt);
            setValues(updatestm, row);

            int r = updatestm.executeUpdate();
            row.setUpdatedCount(r);
        }
        catch (Exception ex) {
            row.setError(writeStackTraceToString(ex));
            row.setError("\r\n");
            row.setError(stmt);
            //throw new ServletException(ex.getMessage(), ex);
        }
        finally {
            if (updatestm != null) {
                try {
                    updatestm.close();
                }
                catch (SQLException ex) {
                }
            }
        }
    }

    protected static void updateRow(TableData row, boolean foreignkey) {
        row.setUpdatedCount(0);
        Connection con = null;

        try {
            con = row.getConnection();
            if (con == null) {
                con = connect(row);
            }

            updateRow(con, row, foreignkey);
        }
        catch (Exception ex) {
            row.setError(writeStackTraceToString(ex));
            //throw new ServletException(ex.getMessage(), ex);
        }
        finally {
            if (row.getCloseConnection()) {
                row.close();
            }
        }
    }

    protected static void setValues(PreparedStatement updatestm, TableData row) throws SQLException {
        TableSpec tspec = row.getTableSpec();
        int n = 0;
        for (int j = 0; j < tspec.getFieldsLength(); j++) {
            String fieldname = tspec.getFieldName(j);
            int fieldtype = tspec.getFieldType(j);
            if (row.containsKey(fieldname)) {
                Object obj = row.get(fieldname);
                String value = null;
                if (obj != null) {
                    value = obj.toString();
                }
                n++;

                if (fieldtype != Types.VARCHAR) {
                    if (value == null || value.length() == 0 || value.equals(NULL)
                            || value.equals(EMPTY)) {
                        updatestm.setNull(n, fieldtype);
                    } else if (fieldtype == Types.TIMESTAMP) {
                        java.util.Date tsd = null;
                        if (obj instanceof java.util.Date) {
                            tsd = (java.util.Date) obj;
                        } else {
                            tsd = getDate(value, tspec.getFormat(j), row.getLocale(),
                                    row.getTimeZone());

                        }
                        Timestamp ts = new Timestamp(tsd.getTime());
                        updatestm.setTimestamp(n, ts);
                    }
                    // <NBCHANGE 22/04/03>
                    else if (Types.BOOLEAN == fieldtype) {
                        updatestm.setObject(n, new Boolean(value));
                    }
                    // </NBCHANGE 22/04/03>
                    else {
                        updatestm.setString(n, value);
                    }
                } else {
                    if (value == null || value.length() == 0 || value.equals(EMPTY)) {
                        updatestm.setString(n, _blank);
                    } else if (value.equals(CURRENTUSER)) {
                        updatestm.setString(n, row.getString(CURRENTUSERID));
                    } else if (value.equals(NULL)) {
                        updatestm.setNull(n, fieldtype);
                    } else {
                        updatestm.setString(n, value);
                    }
                }
            }
        }
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getNextNumHoursCal(String field) {
        return (CalendarFactory.getNextNumHoursCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getNextNumHoursInitCal(String field) {
        return (CalendarFactory.getNextNumHoursInitCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getNextNumWeeksCal(String field) {
        return (CalendarFactory.getNextNumWeeksCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getNextNumWeeksInitCal(String field) {
        return (CalendarFactory.getNextNumWeeksInitCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getNextNumDaysCal(String field) {
        return (CalendarFactory.getNextNumDaysCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getNextNumMonthsCal(String field) {
        return (CalendarFactory.getNextNumMonthsCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getNextNumMonthsInitCal(String field) {
        return (CalendarFactory.getNextNumMonthsInitCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getLastNumHoursCal(String field) {
        return (CalendarFactory.getLastNumHoursCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getLastNumWeeksCal(String field) {
        return (CalendarFactory.getLastNumWeeksCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getLastNumWeeksInitCal(String field) {
        return (CalendarFactory.getLastNumWeeksInitCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getLastNumDaysCal(String field) {
        return (CalendarFactory.getLastNumDaysCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getLastNumDaysInitCal(String field) {
        return (CalendarFactory.getLastNumDaysInitCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getLastNumMonthsCal(String field) {
        return (CalendarFactory.getLastNumMonthsCal(field));
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getLastNumMonthsInitCal(String field) {
        return (CalendarFactory.getLastNumMonthsInitCal(field));
    }

    /**
     * @deprecated
     */
    public static Calendar getYesterdayCal() {
        return (CalendarFactory.getYesterdayCal());
    }

    /**
     * @deprecated
     */
    public static Calendar getYesterdayCal(Calendar cal) {
        return (CalendarFactory.getYesterdayCal(cal));
    }

    /**
     * @deprecated
     */
    public static Calendar getTomorrowCal(Calendar cal) {
        return (CalendarFactory.getTomorrowCal(cal));
    }

    /**
     * @deprecated
     */
    public static Calendar getTomorrowCal() {
        return (CalendarFactory.getTomorrowCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getEndOfMonthCal() {
        return (CalendarFactory.getEndOfMonthCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getStartOfMonthCal() {
        return (CalendarFactory.getStartOfMonthCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getEndOfYearCal() {
        return (CalendarFactory.getEndOfYearCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getStartOfYearCal() {
        return (CalendarFactory.getStartOfYearCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getEndOfLastYearCal() {
        return (CalendarFactory.getEndOfLastYearCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getStartOfLastYearCal() {
        return (CalendarFactory.getStartOfLastYearCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getStartOfWeekCal() {
        return (CalendarFactory.getStartOfWeekCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getEndOfWeekCal() {
        return (CalendarFactory.getEndOfWeekCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getStartOfLastWeekCal() {
        return (CalendarFactory.getStartOfLastWeekCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getEndOfLastWeekCal() {
        return (CalendarFactory.getEndOfLastWeekCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getEndOfLastMonthCal() {
        return (CalendarFactory.getEndOfLastMonthCal());
    }

    /**
     * @deprecated
     */
    public static GregorianCalendar getStartOfLastMonthCal() {
        return (CalendarFactory.getStartOfLastMonthCal());
    }
    
    /* writing to a writer is much more efficient than building strings, this should be done over write to string */

    public static void writeStackTrace(Throwable e, OutputStream os) throws IOException {
    	writeStackTrace(e, new PrintWriter(os));
    }
    public static void writeStackTrace(Throwable e, Writer w) throws IOException {
    	writeStackTrace(e, new PrintWriter(w));
    }
    public static void writeStackTrace(Throwable e, PrintWriter pw) throws IOException {
    	e.printStackTrace(pw);
    	pw.write("\r\n");
    	while((e = e.getCause()) != null) { 
    		pw.write("Caused By.. \r\n");
    		e.printStackTrace(pw);
    		pw.write("\r\n");
    	}
    }
    
    public static String writeStackTraceToString(Exception e) {
        //Create chararraywriter.
        CharArrayWriter c = new CharArrayWriter();
        //Print stack trace to chararraywriter.
        try {
        writeStackTrace(e, c);
        } catch (IOException ioe) { 
        	ioe.printStackTrace();
        	e.printStackTrace(new PrintWriter(c));
        }
        //e.printStackTrace(new PrintWriter(c));
        //return stack trace as string.
        return c.toString();
    }

    /**
     * @deprecated
     */
    public static String getCurrentDate(String format) {
        return (CalendarFactory.getCurrentDate(format));
    }

    /**
     * @deprecated
     */
    public static String getCurrentDate(String format, String timezone) {
        return (CalendarFactory.getCurrentDate(format, timezone));
    }

    public static Connection getConnection(ServletRequest request) {
    	if (request == null) return connect();
    	
        Connection conn = (Connection) request.getAttribute(ActionBean.CON);
        return (conn);
    }

}