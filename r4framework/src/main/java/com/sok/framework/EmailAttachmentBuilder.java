package com.sok.framework;
import java.util.*;
import java.io.*;

public class EmailAttachmentBuilder {
   
   LinkedHashMap attachments = null;
   LinkedHashMap attachFiles = null;
   LinkedHashMap documentIDs = null;
   String id = null;
   
   static Hashtable hashMap = null;
   
   public EmailAttachmentBuilder(String id) {
      attachments = new LinkedHashMap();
      attachFiles = new LinkedHashMap();
      documentIDs = new LinkedHashMap();
      this.id = id;
   }
   
   private static synchronized Hashtable getHashtable() {
      if (hashMap == null) {
         hashMap = new Hashtable();
      }
      return hashMap;
   }
   
   public static boolean hasEmailAttachmentBuilder(String id) {
      if (id == null || id.length() == 0) {
         return false;
      }
      Hashtable map = getHashtable();
      EmailAttachmentBuilder builder = (EmailAttachmentBuilder)map.get(id);
      if (builder == null) {
         return false;
      }
      else {
         return true;
      }
   }
   
   public static synchronized EmailAttachmentBuilder getEmailAttachmentBuilder(String id) {
      if (id == null || id.length() == 0) {
         return null;
      }
      Hashtable map = getHashtable();
      EmailAttachmentBuilder builder = (EmailAttachmentBuilder)map.get(id);
      if (builder == null) {
         builder = new EmailAttachmentBuilder(id);
         map.put(id, builder);
      }
      return builder;

   }
   
   public static synchronized void removeAllAttachments(String id) {
      Hashtable map = getHashtable();
      EmailAttachmentBuilder builder = (EmailAttachmentBuilder)map.get(id);
      if (builder != null) {
         builder.removeAllFiles();
      }
      getHashtable().remove(id);
   }
   
   public synchronized void addUploadedFile(File file) {
      attachments.put(file.getName(), file);
		attachFiles.put(file.getName(), file);
   }
   
   public synchronized void addSavedFile(File file) {
      attachments.put(file.getName(), file);
   }
   
   public synchronized void addLinkedDocument(File file, String documentID) {
	   documentIDs.put(file.getName(), documentID);
		attachments.put(file.getName(), file);
   }
   
   public synchronized void moveUploadedFileToLinkedDocument(File file, String documentID) {
		attachFiles.remove(file.getName());
	   documentIDs.put(file.getName(), documentID);
		attachments.put(file.getName(), file);
   }
   
   public synchronized void removeAttachment(String fileName) {
      attachments.remove(fileName);
      documentIDs.remove(fileName);
      
      File attachment = (File)attachFiles.get(fileName);
      if (attachment != null) {
         attachFiles.remove(fileName);
         attachment.delete();
      }
   }
   
   public synchronized void removeAllFiles() {
      Iterator iter = attachFiles.values().iterator();
      File parentDir = null;
      while (iter.hasNext()) {
         File attachment = (File)iter.next();
         if (attachment != null && attachment.exists()) {
            if (parentDir == null) {
               parentDir = attachment.getParentFile();
            }
            attachment.delete();
         }
         attachments.remove(attachment.getName());
         documentIDs.remove(attachment.getName());
      }   
      attachFiles.clear();
      if (parentDir != null) {
         parentDir.delete();
      }
   }
   
   public synchronized Collection getAttachedFiles() {
      return new Vector(attachments.values());
   }
   
   public synchronized Collection getUploadedFiles() {
      return new Vector(attachFiles.values());
   }
   
   public synchronized Collection getAttachedFileNames() {
      return new Vector(attachments.keySet());
   }
   
   public synchronized Collection getDocumentIDs() {
      return new Vector(documentIDs.values());
   }
   
}
