package com.sok.framework.sql;
import com.sok.framework.*;
import com.sok.framework.generation.util.*;
import com.sok.framework.generation.nodes.RootNode;
import com.sok.framework.generation.*;

import java.sql.Types;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class AbstractDatabase implements SqlDatabase, CalendarKeys
{
	public static final String _blank = "";
	public static final String _dot = ".";
	public static final String _comma = ", ";
	public static final String _and = " and ";
	public static final String _or = " or ";
	public static final String _as = " as ";
	public static final String _omit = "omit";
	public static final String _equals = " = ";
	public static final String _plus = "+";

	public static final String RN = "\r\n";

	static final String _inner_join = " inner join ";
	static final String _outer_left_join = " left outer join ";
	static final String _outer_right_join = " right outer join ";
	static final String _on = " on ";
	static final String _count = "count(*)";

	static final String INNER_JOIN = " = ";
	static final String OUTER_LEFT_JOIN = " *= ";
	static final String OUTER_RIGHT_JOIN = " =* ";

    static final String OPEN_BRACKET = " ( ";
    static final String CLOSE_BRACKET= " ) ";    
    
	public static final String usdateinput = "MM/DD/YYYY";
	public static final String ukdateinput = "DD/MM/YYYY";

	public static final int GT = 1;
	public static final int LT = -1;
	public static final int GTE = 11;
	public static final int LTE = -11;
	public static final int NE = 100;
	public static final int LK = 0;

	static final String DB_BIT_TRUE = "1";
	static final String DB_BIT_FALSE = "0";

	static final String dayend = " 23:59:59";
	static final String daystart = " 00:00:00";
    
    static final SqlDatabase mssqldb = new MSSQL();
    static final SqlDatabase mysqldb = new MySQL();  
    static final SqlDatabase mysql5db = new MySQL5();
    static final SqlDatabase mysql55db = new MySQL55();
    static final SqlDatabase fm8db = new FM8(); 
    static final SqlDatabase derbydb = new Derby(); 
    
	public AbstractDatabase()
	{
		
	}
	
	public static SqlDatabase getDatabase(String name)
	{
		SqlDatabase sql = null;
		
		if(name.equals(mssql))
		{
			sql = mssqldb;
		}
		else if(name.equals(mysql))
		{
			sql = mysqldb;
		}
		else if(name.equals(mysql5))
		{
			sql = mysql5db;
		}	
		else if(name.equals(mysql55)) 
		{
			sql = mysql55db;
		}
        else if(name.equals(fm8))
        {
            sql = fm8db;
        }      
        else if(name.equals(derby))
        {
            sql = derbydb;
        }           
		return(sql);
	}
	
    public abstract void generate(RootNode root, StringBuffer out);
    
	public abstract void appendTopStatement(StringBuffer stmtbuffer, int top);
    
    public abstract void appendTopStatementAtStart(StringBuffer stmtbuffer, int top);

    public abstract void appendTopStatementAtEnd(StringBuffer stmtbuffer, int top);
    
	public abstract void generateSQLStatement(TableData row);
	
	@Override
	public String alterColumn(String colname, String coltype, int len, boolean allowNull, boolean autoIncrement, String defaultValue) {
		throw new UnsupportedOperationException("Not Implemented"); 
	}
	
	@Override
	public String createTable(TableSpec table) {
		throw new UnsupportedOperationException("Not implemented"); 
	}
	
	@Override
	public String alterIndex(String indexName, String fields, boolean unique, boolean addDrop)
	{
		throw new UnsupportedOperationException("Not implemented");
	}

	public StringBuffer createdDataOnIDStatment(TableData row)
	{
		TableSpec tspec = row.getTableSpec();
		String idname = tspec.getPrimaryKeyName();
		String id = (String) row.get(idname);
		String tablename = tspec.getTableName();
		StringBuffer tempstmt = new StringBuffer();
		tempstmt.append("select ");

		appendDisplayfields(tempstmt, row);

		tempstmt.append(" from ");

		appendDisplayTables(tempstmt, row);

		tempstmt.append(" where ");

        appendWithQuote(tempstmt, tablename);
		tempstmt.append(_dot);
        appendWithQuote(tempstmt, idname);
		tempstmt.append(" = '");
		tempstmt.append(id);
		tempstmt.append("'");

		row.setSearchStatement(tempstmt.toString());
		return (tempstmt);
	}

	protected void appendDisplayTables(StringBuffer stmtstring, TableData row)
	{
		ViewSpec vspec = row.getViewSpec();
		RelationSpec rspec = row.getRelationSpec();

		LinkedHashMap tables = new LinkedHashMap();
		String rname = null;
		String tname = null;
		String lasttname = null;
		for(int i = 0; i < vspec.getRelatedLength(); i++)
		{
			rname = vspec.getRelationshipNameForRelatedItem(i);
			StringTokenizer st = new StringTokenizer(rname, ".");
			lasttname = null;

			while(st.hasMoreTokens())
			{
				rname = st.nextToken();
				if(rname != null && rname.length() != 0)
				{
					try
					{
						int rindex = rspec.getIndex(rname);
						if(rindex > -1)
						{
							tname = rspec.getToTableName(rindex);
							if(lasttname == null)
							{
								tables.put(rname, new RelationItem(null, tname));
							}
							else
							{
								tables.put(rname, new RelationItem(lasttname, tname));
							}

							lasttname = rname;
						}
					}
					catch(Exception e)
					{
						row.setError("@ appendDisplayTables with rname: ");
						row.setError(rname);
						row.setError("\r\n");
						row.setError(ActionBean.writeStackTraceToString(e));
						row.setError("\r\n");
					}
				}
			}
			//System.out.println(rname+":"+tname+String.valueOf(i));
		}
		Iterator it = tables.keySet().iterator();
		String ttablename = null;
		String ftablename = null;
		String relationname = null;

		SqlDatabase database = row.getDatabase();
		database.appendQualifiedTableName( stmtstring, 
				row.getString(ActionBean.server),
				row.getString(ActionBean.database),
				row.getTableSpec().getTableName(),
				row.getTableSpec().getTableName());		

		while(it.hasNext())
		{

			try
			{
				relationname = (String) it.next();
				int relationindex = rspec.getIndex(relationname);

				ttablename = ((RelationItem) tables.get(relationname)).getToTable();
				ftablename = ((RelationItem) tables.get(relationname)).getFromTable();
				
				relationname = RelationSpec.escapeRelationName(relationname);
				
				if(ftablename == null)
				{
					ftablename = rspec.getFromTableName(relationindex);
				}

				String type = rspec.getType(relationindex);

				stmtstring.append(RN);
				if(type.equals(OUTER_LEFT_JOIN))
				{
					stmtstring.append(_outer_left_join);
				}
				else if(type.equals(INNER_JOIN))
				{
					stmtstring.append(_inner_join);
				}
				else if(type.equals(OUTER_RIGHT_JOIN))
				{
					stmtstring.append(_outer_right_join);
				}

				database.appendQualifiedTableName( stmtstring, 
						row.getString(ActionBean.server),
						row.getString(ActionBean.database),
						ttablename,
						relationname);					
				
				stmtstring.append(_on);
                appendWithQuote(stmtstring, ftablename);
				stmtstring.append(_dot);
                appendWithQuote(stmtstring, rspec.getFromKey(relationindex));

				stmtstring.append(_equals);

				stmtstring.append(relationname);
				stmtstring.append(_dot);
                appendWithQuote(stmtstring, rspec.getToKey(relationindex));

				//for(int j = 0; j < rspec.getConditionLength(relationindex); j++)
				if (rspec.getConditionLength(relationindex) != 0)
				{
				   stmtstring.append(ActionBean._space);
					stmtstring.append(getSubstituted(rspec.getCondition(relationindex), row));
				}
			}
			catch(Exception e)
			{
				row.setError("@ appendDisplayTables with relationname: ");
				row.setError(relationname);
				row.setError("\r\n");
				row.setError(ActionBean.writeStackTraceToString(e));
				row.setError("\r\n");
			}
		}
	}
    
	public void appendDisplayfields(StringBuffer stmtstring, TableData row)
	{
		String alias = null;
		String rname = null;
		String fieldname = null;
		ViewSpec vspec = row.getViewSpec();
		TableSpec tspec = row.getTableSpec();
		for(int i = 0; i < vspec.getLocalLength(); i++)
		{
			if(i != 0)
			{
				stmtstring.append(_comma);
			}
			stmtstring.append(RN);
			fieldname = vspec.getLocalItemFieldName(i);
			if(fieldname.indexOf('(') < 0)
			{
                appendWithQuote(stmtstring, tspec.getTableName());;
				stmtstring.append(_dot);
			}
            appendWithQuote(stmtstring, fieldname); 

			alias = vspec.getLocalItemAsName(i);
			if(alias != null)
			{
				stmtstring.append(_as);
				stmtstring.append(alias);
			}
		}
		boolean addcoma = (vspec.getLocalLength() != 0);
		for(int j = 0; j < vspec.getCountsLength(); j++)
		{
			if(addcoma || j != 0)
			{
				stmtstring.append(_comma);
			}
			stmtstring.append(RN);
			stmtstring.append("count(");
			stmtstring.append(vspec.getCountItemName(j));
			stmtstring.append(")");
			alias = vspec.getCountItemAsName(j);
			if(alias != null)
			{
				stmtstring.append(_as);
				stmtstring.append(alias);
			}
		}
		addcoma = (vspec.getLocalLength() != 0 || vspec.getCountsLength() != 0);
		for(int j = 0; j < vspec.getRelatedLength(); j++)
		{
			if(addcoma || j != 0)
			{
				stmtstring.append(_comma);
			}
			stmtstring.append(RN);
			rname = vspec.getRelationshipNameForRelatedItem(j);
			if(rname.indexOf(".") > 0)
			{
				StringTokenizer st = new StringTokenizer(rname, ".");
				while(st.hasMoreTokens())
				{
					rname = st.nextToken();
				}
			}
			fieldname = vspec.getRelatedItemFieldName(j);
			if(fieldname.indexOf('(') < 0)
			{
				stmtstring.append(RelationSpec.escapeRelationName(rname));
				stmtstring.append(_dot);
			}
			stmtstring.append(fieldname);
			stmtstring.append(_as);
			stmtstring.append(vspec.getRelatedItemName(j));
		}
		addcoma = (vspec.getLocalLength() != 0 || vspec.getRelatedLength() != 0 || vspec
				.getCountsLength() != 0);
		for(int k = 0; k < vspec.getRelatedCountsLength(); k++)
		{
			if(!vspec.getRelatedCountHidden(k))
			{
				if(addcoma)
				{
					stmtstring.append(_comma);
				}
				appendRelatedCount(stmtstring, k, row);
				stmtstring.append(_as);
				stmtstring.append(vspec.getRelatedCountItemName(k));
				addcoma = true;
			}
		}
		addcoma = (vspec.getLocalLength() != 0 || vspec.getRelatedLength() != 0
				|| vspec.getCountsLength() != 0 || vspec.getRelatedCountsLength() != 0);
		for(int k = 0; k < vspec.getRelatedSumsLength(); k++)
		{
			if(!vspec.getRelatedSumHidden(k))
			{
				if(addcoma)
				{
					stmtstring.append(_comma);
				}
				appendRelatedSum(stmtstring, k, row);
				stmtstring.append(_as);
				stmtstring.append(vspec.getRelatedSumItemName(k));
				addcoma = true;
			}
		}
	}

	protected void appendRelatedCount(StringBuffer stmtstring, int k, TableData row)
	{
		appendRelated(_count, stmtstring, k, row);
	}

	protected void appendRelatedSum(StringBuffer stmtstring, int k, TableData row)
	{
		ViewSpec vspec = row.getViewSpec();
		StringBuffer temp = new StringBuffer();
		temp.append("sum(");
		temp.append(vspec.getFieldNameForRelatedSumItem(k));
		temp.append(")");
		appendRelated(temp.toString(), stmtstring, k, row);
	}

	protected void appendRelated(String type, StringBuffer stmtstring, int k, TableData row)
	{
		String rname = null;
		String tname = null;
		String lasttname = null;
		String relationname = null;
		ViewSpec vspec = row.getViewSpec();
		RelationSpec rspec = row.getRelationSpec();
		Map tables = new HashMap();

		if(type.equals(_count))
		{
			relationname = vspec.getRelationshipNameForRelatedCountItem(k);
		}
		else
		{
			relationname = vspec.getRelationshipNameForRelatedSumItem(k);
		}

		StringTokenizer st = new StringTokenizer(relationname, ".");

		while(st.hasMoreTokens())
		{
			rname = st.nextToken();
			try
			{
				int rindex = rspec.getIndex(rname);
                if(rindex<0)
                {
                    throw new IllegalConfigurationException("Relation not found: "+rname);
                }
				tname = rspec.getToTableName(rindex);

				if(lasttname == null)
				{
					tables.put(rname, new RelationItem(null, tname));
				}
				else
				{
					tables.put(rname, new RelationItem(lasttname, tname));
				}
			}
			catch(Exception e)
			{
				row.setError(ActionBean.writeStackTraceToString(e));
			}
			lasttname = rname;
		}

		stmtstring.append("(select ");
		stmtstring.append(type);
		stmtstring.append(" from ");

		Iterator it = tables.keySet().iterator();
		String ttablename = null;
		String ftablename = null;

		StringBuffer keymapping = new StringBuffer();
		//System.out.println(tables);
		boolean addand = false;
		boolean noskipkeymapand = false;
		while(it.hasNext())
		{
			relationname = (String) it.next();
			ttablename = ((RelationItem) tables.get(relationname)).getToTable();
			ftablename = ((RelationItem) tables.get(relationname)).getFromTable();
			int relationindex = rspec.getIndex(relationname);
			relationname = RelationSpec.escapeRelationName(relationname);
			
			if(addand)
			{
				stmtstring.append(_comma);
			}

			SqlDatabase database = row.getDatabase();
			database.appendQualifiedTableName( stmtstring, 
					row.getString(ActionBean.server),
					row.getString(ActionBean.database),
					ttablename,
					relationname);				
			
			if(noskipkeymapand)
			{
				keymapping.append(_and);
			}

			if(ftablename == null)
			{
				ftablename = rspec.getFromTableName(relationindex);
			}
			
			if(!ftablename.equals("NULLTABLE"))
			{
				keymapping.append(relationname);
				keymapping.append(_dot);
				keymapping.append(rspec.getToKey(relationindex));

				keymapping.append(" = ");
				
				keymapping.append(ftablename);
				keymapping.append(_dot);
				keymapping.append(rspec.getFromKey(relationindex));

				noskipkeymapand = true;
			}
			else
			{
				noskipkeymapand = false;
			}

			addand = true;
		}

		addand = noskipkeymapand;

		stmtstring.append(" where ");
		stmtstring.append(keymapping);
		int condcount = 0;
		if(type.equals(_count))
		{
			condcount = vspec.getRelatedCountConditionLength(k);
		}
		else
		{
			condcount = vspec.getRelatedSumConditionLength(k);
		}
		
		for(int i = 0; i < condcount; i++)
		{
			//if(addand)
			//{
			//	stmtstring.append(_and);
			//}
			String condition = null;
			if(type.equals(_count))
			{
				condition = getSubstituted(vspec.getRelatedCountCondition(k, i), row);
			}
			else
			{
				condition = getSubstituted(vspec.getRelatedSumCondition(k, i), row);
			}
			appendConditions(condition, stmtstring);

			addand = true;
		}

		stmtstring.append(")");
	}

   public void appendConditions(org.apache.velocity.context.Context context, String condition, StringBuffer stmtstring)
   {
      String vcondition = condition;
      if(vcondition.indexOf('#')>=0)
      {
         vcondition = getSubstituted(condition, context);

      }
      appendConditions(vcondition, stmtstring);
   }
	
	public void appendConditions(String condition, StringBuffer stmtstring)
   {
	   if(condition.indexOf('#')>=0)
	   {
	      appendCondition(condition, stmtstring);
	   }
	   else if(condition.indexOf('(')>=0)
	   {
   	   StringTokenizer st = new StringTokenizer(condition);
   	   String part = null;
   	   while(st.hasMoreTokens())
   	   {
   	      part = st.nextToken();
   	      if(part!=null && part.length()!=0)
   	      {
   	         appendCondition(part, stmtstring);
   	      }
   	   }
	   }
	   else
	   {
	      stmtstring.append(condition);
	   }
   }
	
	private final Pattern AGOMATCHER = Pattern.compile("(\\d+)(MINUTES|HOURS|DAYS|WEEKS|MONTHS|QUARTERS|YEARS)(BEFORE|AFTER)(\\d+)(MINUTES|HOURS|DAYS|WEEKS|MONTHS|QUARTERS|YEARS)AGO");
	
	protected void appendCondition(String condition, StringBuffer stmtstring)
	{
		Matcher m = null;
	    stmtstring.append(ActionBean._space);
		String[] fparams = getFunctionParam(condition);
		boolean not = false;
		if(condition.indexOf('!')==0)
		{
			not = true;
			condition = condition.substring(1,condition.length());
		}
		if(condition.indexOf(TODAY) >= 0)
		{
			appendTodayStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(THISWEEK) >= 0)
		{
			appendThisWeekStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(THISMONTH) >= 0)
		{
			appendThisMonthStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(OVERDUE) >= 0)
		{
			appendOverdueStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(CURRENT) >= 0 && condition.indexOf(YEARS) == -1)
		{
			appendCurrentStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(FUTURE) >= 0)
		{
			appendFutureStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(CURRENTWEEK) >= 0)
		{
			appendCurrentWeekStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(CURRENTMONTH) >= 0)
		{
			appendCurrentMonthStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(CURRENTYEAR) >= 0)
		{
			appendCurrentYearStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(LASTWEEK) >= 0)
		{
			appendLastWeekStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(LASTMONTH) >= 0)
		{
			appendLastMonthStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(LASTYEAR) >= 0)
		{
			appendLastYearStmt(stmtstring, fparams[0], not);
			appendCondition(stmtstring, fparams);
		}
		else if (condition.indexOf(LAST) >= 0 || condition.indexOf(NEXT) >= 0) 
		{ 
			Calendar from = null; 
			Calendar to = null; 
			
			boolean isExcept = condition.indexOf(EXCEPT) > -1; 
			String except = null; 
			
			if(isExcept) { 
				
				except = condition.substring(condition.indexOf(EXCEPT)+6); 
				condition = condition.substring(0, condition.indexOf(EXCEPT));
			}
			
			if(condition.indexOf(LAST) >= 0) { 
				if(condition.indexOf(HOURS) >= 0) { 
					from = CalendarFactory.getLastNumHoursCal(condition); 
                    to = new GregorianCalendar(); 
				} else if(condition.indexOf(DAYS) >= 0) { 
					from = CalendarFactory.getLastNumDaysCal(condition);
					to = CalendarFactory.getYesterdayCal();
				} else if(condition.indexOf(WEEKS) >= 0) { 
					from = CalendarFactory.getLastNumWeeksCal(condition);
                    to = CalendarFactory.getLastNumWeeksInitCal(condition);
				} else if(condition.indexOf(MONTHS) >= 0) { 
					from = CalendarFactory.getLastNumMonthsCal(condition);
					to = CalendarFactory.getLastNumMonthsInitCal(condition);                
				} else if(condition.indexOf(YEARS) >= 0) { 
					from = CalendarFactory.getLastNumYearsCal(condition);
					to = CalendarFactory.getNextNumYearsInitCal(condition);       
				} 
			} else if(condition.indexOf(NEXT) >= 0) {  
				if(condition.indexOf(HOURS) >= 0) { 
					from = new GregorianCalendar(); 
                    to = CalendarFactory.getNextNumHoursCal(condition); 
				} else if(condition.indexOf(DAYS) >= 0) { 
					from = new GregorianCalendar();
					to = CalendarFactory.getNextNumDaysCal(condition);
				} else if(condition.indexOf(WEEKS) >= 0) { 
					from = CalendarFactory.getNextNumWeeksInitCal(condition);
					to = CalendarFactory.getNextNumWeeksCal(condition);
				} else if(condition.indexOf(MONTHS) >= 0) { 
					from = CalendarFactory.getNextNumMonthsInitCal(condition);
					to = CalendarFactory.getNextNumMonthsCal(condition);
				} else if(condition.indexOf(YEARS) >= 0) { 
					from = CalendarFactory.getNextNumYearsInitCal(condition);
					to = CalendarFactory.getNextNumYearsCal(condition);
				} 
			}
			
			if(isExcept) { 
				
				int val = 0;
				int type = -1; 
				
				if(except.endsWith(HOURS)) {
					val = Integer.parseInt(except.substring(0,except.indexOf(HOURS)));
					type = Calendar.HOUR_OF_DAY; 
				} else if(except.endsWith(DAYS)) {
					val = Integer.parseInt(except.substring(0,except.indexOf(DAYS)));
					type = Calendar.DAY_OF_YEAR; 
				} else if(except.endsWith(WEEKS)) { 
					val = Integer.parseInt(except.substring(0,except.indexOf(WEEKS)));
					type = Calendar.WEEK_OF_YEAR; 
				} else if(except.endsWith(MONTHS)) { 
					val = Integer.parseInt(except.substring(0,except.indexOf(MONTHS)));
					type = Calendar.MONTH; 
				} else if(except.endsWith(YEARS)) { 
					val = Integer.parseInt(except.substring(0,except.indexOf(YEARS)));
					type = Calendar.YEAR; 
				} 
				
				if(val != 0) { 
					if(condition.indexOf(LAST) >= 0) { 
						to.add(type,(val*-1));
					} else { 
						from.add(type,val);
					}
				} 
			} 
			
			if(condition.indexOf(HOURS) == -1) { 
				appendLastRangeStmt(stmtstring, fparams[0], from, to, not);
			} else { //do hours 
				appendDateTimeRangeStmt(stmtstring, fparams[0],from, to, not);
			}
			appendCondition(stmtstring, fparams);
			
		} else if((m = AGOMATCHER.matcher(condition)).find() && m.groupCount() == 5) { 
			// examples :: 2DAYSBEFORE3MONTHSAGO   // 1DAYAFTER4MONTHSAGO 
			//Not sure how this code gets called, I had thought that it was *the* code but there is two blocks which do the same thing almost. 
			//This code is the same as the other block, but there is the offchance that it wont work.. we'll see. 
			CalendarFactory.CalendarPeriod beforeAfter = new CalendarFactory.CalendarPeriod(m.group(2));
			int beforeAfterNum = beforeAfter.num * new Integer(m.group(1));
			 
			CalendarFactory.CalendarPeriod agoPeriod = new CalendarFactory.CalendarPeriod(m.group(5));
			int agoNum = -1 * agoPeriod.num * new Integer(m.group(4));
			
			Calendar from = new GregorianCalendar();
			Calendar to = new GregorianCalendar();
			to.add(agoPeriod.field, agoNum);
			
			from.setTimeInMillis(to.getTimeInMillis());
			
			if("BEFORE".equals(m.group(3))) {
				from.add(beforeAfter.field, -1 * beforeAfterNum);
			} else {
				to.add(beforeAfter.field, beforeAfterNum);
			}
			
			appendDateTimeRangeStmt(stmtstring, fparams[0],from, to, not);
			appendCondition(stmtstring, fparams);
		}
		/*
		else if(condition.indexOf(LAST) >= 0 && condition.indexOf(WEEKS) > 0)
		{

			appendLastRangeStmt(stmtstring, fparams[0],
                    CalendarFactory.getLastNumWeeksCal(condition),
                    CalendarFactory.getLastNumWeeksInitCal(condition),
                    not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(LAST) >= 0 && condition.indexOf(DAYS) > 0)
		{
			appendLastRangeStmt(stmtstring, fparams[0],
			      CalendarFactory.getLastNumDaysCal(condition),
                    CalendarFactory.getYesterdayCal(),
					not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(LAST) >= 0 && condition.indexOf(MONTHS) > 0)
		{
			appendLastRangeStmt(stmtstring, fparams[0],
			      CalendarFactory.getLastNumMonthsCal(condition),
                    CalendarFactory.getLastNumMonthsInitCal(condition),                    
                    not);
			appendCondition(stmtstring, fparams);
		}
      else if(condition.indexOf(LAST) >= 0 && condition.indexOf(HOURS) > 0)
      {
         appendDateTimeRangeStmt(stmtstring, fparams[0],
               CalendarFactory.getLastNumHoursCal(condition),
                    new GregorianCalendar(),                   
                    not);
         appendCondition(stmtstring, fparams);
      }		
      else if(condition.indexOf(LAST) >= 0 && condition.indexOf(YEARS) > 0)
      {
         appendLastRangeStmt(stmtstring, fparams[0],
                    CalendarFactory.getLastNumYearsCal(condition),
                    CalendarFactory.getNextNumYearsInitCal(condition),                 
                    not);
         appendCondition(stmtstring, fparams);
      }*/
      else if(condition.indexOf(CURRENT) >= 0 && condition.indexOf(YEARS) > 0)
      {
         appendLastRangeStmt(stmtstring, fparams[0],
                    CalendarFactory.getCurrNumYearsCal(condition),
                    CalendarFactory.getCurrNumYearsInitCal(condition),                 
                    not);
         appendCondition(stmtstring, fparams);
      } /*
		else if(condition.indexOf(NEXT) >= 0 && condition.indexOf(WEEKS) > 0)
		{
			appendNextRangeStmt(stmtstring, fparams[0],
			      CalendarFactory.getNextNumWeeksCal(condition),
			      CalendarFactory.getNextNumWeeksInitCal(condition),
                    not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(NEXT) >= 0 && condition.indexOf(DAYS) > 0)
		{
            appendNextRangeStmt(stmtstring, fparams[0],
                  CalendarFactory.getNextNumDaysCal(condition),
                    new GregorianCalendar(),
                    not);
			appendCondition(stmtstring, fparams);
		}
		else if(condition.indexOf(NEXT) >= 0 && condition.indexOf(MONTHS) > 0)
		{
            appendNextRangeStmt(stmtstring, fparams[0],
                  CalendarFactory.getNextNumMonthsCal(condition),
                  CalendarFactory.getNextNumMonthsInitCal(condition),
                    not);
			appendCondition(stmtstring, fparams);
		} 
      else if(condition.indexOf(NEXT) >= 0 && condition.indexOf(HOURS) > 0)
      {
            appendDateTimeRangeStmt(stmtstring, fparams[0],
                  new GregorianCalendar(),
                    CalendarFactory.getNextNumHoursCal(condition),
                    not);
         appendCondition(stmtstring, fparams);
      }	
      else if(condition.indexOf(NEXT) >= 0 && condition.indexOf(YEARS) > 0)
      {
         appendNextRangeStmt(stmtstring, fparams[0],
               CalendarFactory.getNextNumYearsCal(condition),
               CalendarFactory.getNextNumYearsInitCal(condition),
               not);
         appendCondition(stmtstring, fparams);
      } */  		
        else if(condition.indexOf(HOUR) >= 0)
        {
            appendHourStmt(stmtstring, fparams[0], CalendarFactory
                    .getHourString(condition), not);
            appendCondition(stmtstring, fparams);
        } 
        else if(isDayOfWeekSearch(condition))
        {
        	appendDayOfWeekStmt(stmtstring, fparams[0], condition, not);
            appendCondition(stmtstring, fparams);
        }
		else
		{
			stmtstring.append(condition);
		}
	}

	protected boolean isDayOfWeekSearch(String condition)
	{
        if(condition.indexOf(MONDAY) >= 0 
        		|| condition.indexOf(TUESDAY) >= 0
        		|| condition.indexOf(WEDNESDAY) >= 0
        		|| condition.indexOf(THURSDAY) >= 0
        		|| condition.indexOf(FRIDAY) >= 0
        		|| condition.indexOf(SATURDAY) >= 0
        		|| condition.indexOf(SUNDAY) >= 0)
        {
        	return(true);
        }
        return(false);
	}
	
	protected String[] getFunctionParam(String functioncall)
	{
		String[] value = null;
		int begin = functioncall.indexOf("(");
		int end = functioncall.indexOf(")");
		//System.out.println("begin:"+String.valueOf(begin));
		//System.out.println("end:"+String.valueOf(end));
		if(begin >= 0 && end > 0)
		{

			String functionparam = functioncall.substring(begin + 1, end);
			if(functioncall.length() > end + 1)
			{
				value = new String[2];
				value[0] = functionparam;
				value[1] = functioncall.substring(end + 1, functioncall.length());
			}
			else
			{
				value = new String[1];
				value[0] = functionparam;
			}
		}
		else
		{
			value = new String[1];
			value[0] = functioncall;
		}
		return (value);
	}

	protected void appendCondition(StringBuffer stmtstring, String[] fparams)
	{
		if(fparams.length == 2)
		{
			stmtstring.append(fparams[1]);
		}
	}
	
	protected static String getSubstituted(String template,
			org.apache.velocity.context.Context context)
	{
		return(ActionBean.getSubstituted(template, context, "condition"));
	}

	public void appendField(StringBuffer searchstmtbuffer, String fieldname, String fieldvalue,
			int fieldtype, MapData row)
	{
		if(getRangeIndex(fieldvalue, fieldtype) > 0 && fieldtype != Types.VARCHAR)
		{
			appendFieldRangeValue(searchstmtbuffer, fieldname, fieldvalue, fieldtype, row.getDateConversionsType(), false);
		}
		else if(fieldvalue.indexOf(_plus) > 0 || fieldvalue.indexOf(";") > 0)
		{
			StringTokenizer st = new StringTokenizer(fieldvalue, "+;");
			searchstmtbuffer.append("(");
			int index = 0;
			String value = null;
			while(st.hasMoreTokens())
			{
				value = st.nextToken();
				appendFieldOpValue(searchstmtbuffer, fieldname, value, fieldtype, row);
				if(st.hasMoreTokens())
				{
					index = index + value.length();
					if(index != 0 && fieldvalue.charAt(index) == '+')
					{
						searchstmtbuffer.append(_or);
					}
					else
					{
						searchstmtbuffer.append(_and);
					}
				}
				index++;
			}
			searchstmtbuffer.append(")");
		}
		else
		{
			appendFieldOpValue(searchstmtbuffer, fieldname, fieldvalue, fieldtype, row);
		}
	}

	public abstract void appendDateRangeStmt(StringBuffer searchstmtbuffer, String fieldvalue, String gtvalue, String ltvalue);

    public abstract void appendDateRangeStmt(StringBuffer searchstmtbuffer, String fieldvalue, String gtvalue, String ltvalue, String format, boolean omit);    
    
	public void appendFieldRangeValue(StringBuffer searchstmtbuffer, String fieldname,
			String fieldvalue, int fieldtype, String format, boolean omit)
	{
		int index = getRangeIndex(fieldvalue, fieldtype);
		String gtvalue = fieldvalue.substring(0, index);
		String ltvalue = fieldvalue.substring(index + 1, fieldvalue.length());
        searchstmtbuffer.append(OPEN_BRACKET);
		if(fieldtype == Types.TIMESTAMP)
		{
			appendDateRangeStmt(searchstmtbuffer, fieldname, gtvalue, ltvalue, format, omit);
		}
		else
		{
            appendNumberRangeStmt(searchstmtbuffer, fieldname, gtvalue, ltvalue, format, omit);
		}
        searchstmtbuffer.append(CLOSE_BRACKET);
	}

    public void appendNumberRangeStmt(StringBuffer searchstmtbuffer, String fieldname, String gtvalue, String ltvalue, String format, boolean omit)
    {
        searchstmtbuffer.append(OPEN_BRACKET);
        searchstmtbuffer.append(fieldname);
        if(omit)
        {
            searchstmtbuffer.append(" < ");
        }
        else
        {
            searchstmtbuffer.append(" >= "); 
        }
        searchstmtbuffer.append(gtvalue);
        searchstmtbuffer.append(" and ");
        searchstmtbuffer.append(fieldname);
        if(omit)
        {
            searchstmtbuffer.append(" > ");
        }
        else
        {
            searchstmtbuffer.append(" <= ");
        }        
        searchstmtbuffer.append(ltvalue);
        searchstmtbuffer.append(CLOSE_BRACKET);
    }
    
	protected boolean appendPresetDateRange(StringBuffer stmtbuffer, String fieldname,
			String fieldvalue, int fieldtype)
	{
		if(fieldtype == Types.TIMESTAMP)
		{
			return(appendPresetDateRange(stmtbuffer, fieldname, fieldvalue));
		}
		return (false);
	}
	
	protected boolean appendPresetDateRange(StringBuffer stmtbuffer, String fieldname,
			String fieldvalue)
	{
        StringBuffer searchstmtbuffer = new StringBuffer();
        Matcher m = null;
		boolean not = false;
		if(fieldvalue.indexOf('!')==0)
		{
			not = true;
			fieldvalue = fieldvalue.substring(1,fieldvalue.length());
		}
		if(fieldvalue.equals(TODAY))
		{
			appendTodayStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(THISWEEK))
		{
			appendThisWeekStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(THISMONTH))
		{
			appendThisMonthStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(CURRENT))
		{
			appendCurrentStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(FUTURE))
		{
			appendFutureStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(OVERDUE))
		{
			appendOverdueStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(CURRENTWEEK))
		{
			appendCurrentWeekStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(CURRENTMONTH))
		{
			appendCurrentMonthStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(CURRENTQUARTER))
		{
			appendCurrentQuarterStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(CURRENTYEAR))
		{
			appendCurrentYearStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(LASTWEEK))
		{
			appendLastWeekStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(LASTMONTH))
		{
			appendLastMonthStmt(searchstmtbuffer, fieldname, not);
		}
		else if(fieldvalue.equals(LASTYEAR))
		{
			appendLastYearStmt(searchstmtbuffer, fieldname, not);
		} /*
		else if(fieldvalue.indexOf(LAST) == 0
				&& fieldvalue.indexOf(WEEKS) > 0)
		{
            appendLastRangeStmt(searchstmtbuffer, fieldname,
                    CalendarFactory.getLastNumWeeksCal(fieldvalue),
                    CalendarFactory.getLastNumWeeksInitCal(fieldvalue),
                    not);
		}
		else if(fieldvalue.indexOf(LAST) == 0
				&& fieldvalue.indexOf(DAYS) > 0)
		{
            appendLastRangeStmt(searchstmtbuffer, fieldname,
                    CalendarFactory.getLastNumDaysCal(fieldvalue),
                    CalendarFactory.getLastNumDaysInitCal(fieldvalue),
                    not);
		}
		else if(fieldvalue.indexOf(LAST) == 0
				&& fieldvalue.indexOf(MONTHS) > 0)
		{
			appendLastRangeStmt(searchstmtbuffer, fieldname,
                    CalendarFactory.getLastNumMonthsCal(fieldvalue),
                    CalendarFactory.getLastNumMonthsInitCal(fieldvalue),
                    not);
		}
      else if(fieldvalue.indexOf(LAST) == 0
            && fieldvalue.indexOf(HOURS) > 0)
      {
         appendDateTimeRangeStmt(searchstmtbuffer, fieldname,
                    CalendarFactory.getLastNumHoursCal(fieldvalue),
                    new GregorianCalendar(),
                    not);
      }		
      else if(fieldvalue.indexOf(LAST) == 0
            && fieldvalue.indexOf(YEARS) > 0)
      {
         appendLastRangeStmt(searchstmtbuffer, fieldname,
                    CalendarFactory.getLastNumYearsCal(fieldvalue),
                    CalendarFactory.getLastNumYearsInitCal(fieldvalue),
                    not);
      } */ 
      else if(fieldvalue.indexOf(CURRENT) == 0
            && fieldvalue.indexOf(YEARS) > 0)
      {
         appendLastRangeStmt(searchstmtbuffer, fieldname,
                    CalendarFactory.getCurrNumYearsCal(fieldvalue),
                    CalendarFactory.getCurrNumYearsInitCal(fieldvalue),
                    not);
      }
      else if (fieldvalue.indexOf(LAST) >= 0 || fieldvalue.indexOf(NEXT) >= 0) 
		{ 
			Calendar from = null; 
			Calendar to = null; 
			
			boolean isExcept = fieldvalue.indexOf(EXCEPT) > -1; 
			String except = null; 
			
			if(isExcept) { 
				
				except = fieldvalue.substring(fieldvalue.indexOf(EXCEPT)+6); 
				fieldvalue = fieldvalue.substring(0, fieldvalue.indexOf(EXCEPT));
			}
			
			if(fieldvalue.indexOf(LAST) >= 0) { 

				if(fieldvalue.indexOf(HOURS) >= 0) { 
					from = CalendarFactory.getLastNumHoursCal(fieldvalue); 
                  to = new GregorianCalendar(); 
				} else if(fieldvalue.indexOf(DAYS) >= 0) { 
					from = CalendarFactory.getLastNumDaysCal(fieldvalue);
					to = CalendarFactory.getYesterdayCal();
				} else if(fieldvalue.indexOf(WEEKS) >= 0) { 
					from = CalendarFactory.getLastNumWeeksCal(fieldvalue);
                  to = CalendarFactory.getLastNumWeeksInitCal(fieldvalue);
				} else if(fieldvalue.indexOf(MONTHS) >= 0) { 
					from = CalendarFactory.getLastNumMonthsCal(fieldvalue);
					to = CalendarFactory.getLastNumMonthsInitCal(fieldvalue);                
				} else if(fieldvalue.indexOf(YEARS) >= 0) { 
					from = CalendarFactory.getLastNumYearsCal(fieldvalue);
					to = CalendarFactory.getNextNumYearsInitCal(fieldvalue);       
				} 
			} else if(fieldvalue.indexOf(NEXT) >= 0) {  
				if(fieldvalue.indexOf(HOURS) >= 0) { 
					from = new GregorianCalendar(); 
                  to = CalendarFactory.getNextNumHoursCal(fieldvalue); 
				} else if(fieldvalue.indexOf(DAYS) >= 0) { 
					from = new GregorianCalendar();
					to = CalendarFactory.getNextNumDaysCal(fieldvalue);
				} else if(fieldvalue.indexOf(WEEKS) >= 0) { 
					from = CalendarFactory.getNextNumWeeksInitCal(fieldvalue);
					to = CalendarFactory.getNextNumWeeksCal(fieldvalue);
				} else if(fieldvalue.indexOf(MONTHS) >= 0) { 
					from = CalendarFactory.getNextNumMonthsInitCal(fieldvalue);
					to = CalendarFactory.getNextNumMonthsCal(fieldvalue);
				} else if(fieldvalue.indexOf(YEARS) >= 0) { 
					from = CalendarFactory.getNextNumYearsInitCal(fieldvalue);
					to = CalendarFactory.getNextNumYearsCal(fieldvalue);
				} 
			}
			
			if(isExcept) { 
				
				int val = 0;
				int type = -1; 
				
				if(except.endsWith(HOURS)) {
					val = Integer.parseInt(except.substring(0,except.indexOf(HOURS)));
					type = Calendar.HOUR_OF_DAY; 
				} else if(except.endsWith(DAYS)) {
					val = Integer.parseInt(except.substring(0,except.indexOf(DAYS)));
					type = Calendar.DAY_OF_YEAR; 
				} else if(except.endsWith(WEEKS)) { 
					val = Integer.parseInt(except.substring(0,except.indexOf(WEEKS)));
					type = Calendar.WEEK_OF_YEAR; 
				} else if(except.endsWith(MONTHS)) { 
					val = Integer.parseInt(except.substring(0,except.indexOf(MONTHS)));
					type = Calendar.MONTH; 
				} else if(except.endsWith(YEARS)) { 
					val = Integer.parseInt(except.substring(0,except.indexOf(YEARS)));
					type = Calendar.YEAR; 
				} 
				
				if(val != 0) { 
					if(fieldvalue.indexOf(LAST) >= 0) { 
						to.add(type,(val*-1));
					} else { 
						from.add(type,(val));
					} 
				} 
			} 
			
			if(fieldvalue.indexOf(HOURS) == -1) { 
				if(fieldvalue.indexOf(LAST) >= 0) { 
					appendLastRangeStmt(searchstmtbuffer, fieldname,from, to, not); 
				} else { 
					appendNextRangeStmt(searchstmtbuffer, fieldname,to, from, not); 
				} 
			} else { //do hours 
				appendDateTimeRangeStmt(searchstmtbuffer, fieldname,from, to, not);
			}
		}
		 else if((m = AGOMATCHER.matcher(fieldvalue)).find() && m.groupCount() == 5) {  
			// 2DAYSBEFORE3MONTHSAGO   // 1DAYAFTER4MONTHSAGO 
			CalendarFactory.CalendarPeriod beforeAfter = new CalendarFactory.CalendarPeriod(m.group(2));
			
			int beforeAfterNum = beforeAfter.num * new Integer(m.group(1));
			 
			CalendarFactory.CalendarPeriod agoPeriod = new CalendarFactory.CalendarPeriod(m.group(5));
			int agoNum = -1 * agoPeriod.num * new Integer(m.group(4));
			
			Calendar from = new GregorianCalendar();
			
			Calendar to = new GregorianCalendar();
			to.add(agoPeriod.field, agoNum);
			
			from.setTimeInMillis(to.getTimeInMillis());
			
			if("BEFORE".equals(m.group(3))) {
				from.add(beforeAfter.field, -1 * beforeAfterNum);
			} else {
				to.add(beforeAfter.field, beforeAfterNum);
			}
			appendDateTimeRangeStmt(searchstmtbuffer, fieldname,from, to, not);
		}
		/*
		else if(fieldvalue.indexOf(NEXT) == 0
				&& fieldvalue.indexOf(WEEKS) > 0)
		{
            appendNextRangeStmt(searchstmtbuffer, fieldname,
                    CalendarFactory.getNextNumWeeksCal(fieldvalue),
                    CalendarFactory.getNextNumWeeksInitCal(fieldvalue),
                    not);
		}
		else if(fieldvalue.indexOf(NEXT) == 0
				&& fieldvalue.indexOf(DAYS) > 0)
		{
            appendNextRangeStmt(searchstmtbuffer, fieldname,
                    CalendarFactory.getNextNumDaysCal(fieldvalue),
                    new GregorianCalendar(),
                    not);
		}
		else if(fieldvalue.indexOf(NEXT) == 0
				&& fieldvalue.indexOf(MONTHS) > 0)
		{
			appendNextRangeStmt(searchstmtbuffer, fieldname,
                    CalendarFactory.getNextNumMonthsCal(fieldvalue),
                    CalendarFactory.getNextNumMonthsInitCal(fieldvalue),
                    not);
		}
      else if(fieldvalue.indexOf(NEXT) == 0
            && fieldvalue.indexOf(HOURS) > 0)
      {
         appendDateTimeRangeStmt(searchstmtbuffer, fieldname,
                     new GregorianCalendar(),
                    CalendarFactory.getNextNumHoursCal(fieldvalue),
                    not);
      }		
      else if(fieldvalue.indexOf(NEXT) == 0
            && fieldvalue.indexOf(YEARS) > 0)
      {
         appendNextRangeStmt(searchstmtbuffer, fieldname,
                    CalendarFactory.getNextNumYearsCal(fieldvalue),
                    CalendarFactory.getNextNumYearsInitCal(fieldvalue),
                    not);
      }
		*/
        else if(fieldvalue.indexOf(HOUR) == 0)
        {
            appendHourStmt(searchstmtbuffer, fieldname, CalendarFactory
                    .getHourString(fieldvalue), not);
        }
        else if(isDayOfWeekSearch(fieldvalue))
        {
        	appendDayOfWeekStmt(searchstmtbuffer, fieldname, fieldvalue, not);
        }		    
        else
        {
            return (false);
        }
        stmtbuffer.append(OPEN_BRACKET);
        stmtbuffer.append(searchstmtbuffer);
        stmtbuffer.append(CLOSE_BRACKET);
        return (true);
	}

	public abstract void appendStrToIntStmt(StringBuffer searchstmtbuffer, String fieldname);	
	
	public abstract void appendDateToStrStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype);

    public abstract void appendDatetimeToStrStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype);    
    
	public abstract void appendStrToDateStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype);	

    public abstract void appendStrToDatetimeStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype);      
    
	public abstract String getCurrentDateMethod();
	
	public abstract String getNewIDMethod();
	
	public abstract void appendCurrentDateString(StringBuffer searchstmtbuffer);	

	public abstract void appendBeginOfToday(StringBuffer searchstmtbuffer);
	
	protected int getOp(String fieldvalue)
	{
		int op = LK;
		if(fieldvalue.indexOf(">=") == 0)
		{
			op = GTE;
		}
		else if(fieldvalue.indexOf("<=") == 0)
		{
			op = LTE;
		}
		else if(fieldvalue.indexOf(">") == 0)
		{
			op = GT;
		}
		else if(fieldvalue.indexOf("<") == 0)
		{
			op = LT;
		}
		else if(fieldvalue.indexOf("!") == 0)
		{
			op = NE;
		}
		return(op);
	}
	
	protected String appendOp(StringBuffer searchstmtbuffer, String fieldname, String value, int fieldtype, int op)
	{
		String fieldvalue = value;
		if(op == GTE)
		{
			searchstmtbuffer.append(" >= ");
			fieldvalue = fieldvalue.substring(2);
		}
		else if(op == LTE)
		{
			searchstmtbuffer.append(" <= ");
			fieldvalue = fieldvalue.substring(2);
		}
		else if(op == GT)
		{
			searchstmtbuffer.append(" > ");
			fieldvalue = fieldvalue.substring(1);
		}
		else if(op == LT)
		{
			searchstmtbuffer.append(" < ");
			fieldvalue = fieldvalue.substring(1);
		}
		else if(op == NE)
		{
			fieldvalue = fieldvalue.substring(1);
			if(fieldvalue.equals(ActionBean.NULL))
			{
				searchstmtbuffer.append(" is not ");
			}
			else if(fieldname.indexOf("ID") >= 0 || fieldtype != Types.VARCHAR
					|| fieldvalue.equals("EMPTY") || fieldvalue.indexOf("%") == -1)
			{
				searchstmtbuffer.append(" <> ");
			}
			else
			{
				searchstmtbuffer.append(" not like ");
			}
		}
		else if(fieldvalue.equals(ActionBean.NULL))
		{
			searchstmtbuffer.append(" is ");
		}
		else if(fieldname.indexOf("ID") >= 0 || fieldtype != Types.VARCHAR
				|| fieldvalue.equals("EMPTY") || fieldvalue.indexOf("%") == -1)
		{
			searchstmtbuffer.append(" = ");
		}
		else
		{
			searchstmtbuffer.append(" like ");
		}
		return(fieldvalue);
	}

	public void appendFieldOpTimestampValue(StringBuffer searchstmtbuffer, String fieldname,
			String value, MapData row)
	{
		String fieldvalue = value;
		if(!appendPresetDateRange(searchstmtbuffer, fieldname, fieldvalue, Types.TIMESTAMP))
		{
			int op = getOp(fieldvalue);
			
			if(op == LK && !fieldvalue.equals(ActionBean.NULL))
			{
				appendDateRangeStmt(searchstmtbuffer, fieldname, value, value, row.getDateConversionsType(), false);
			}
			else
			{
				searchstmtbuffer.append(fieldname);
				
				fieldvalue = appendOp(searchstmtbuffer, fieldname, fieldvalue, Types.TIMESTAMP, op);
			
				if(fieldvalue.equals(ActionBean.NULL))
				{
					searchstmtbuffer.append(ActionBean.NULL);
				}
				else if(fieldvalue.equals(NOW))
				{
					if(op != LK)
					{
						searchstmtbuffer.append(getCurrentDateMethod());
					}
					else
					{
						appendCurrentDateString(searchstmtbuffer);
					}
				}
		        else if(fieldvalue.indexOf("NOWADDDAYS") == 0)
		        {
		           appendDateAddFromNow(searchstmtbuffer, "DAY", getNowAddDayString(fieldvalue));
		        }
		        else if(fieldvalue.indexOf("NOWSUBDAYS") == 0)
		        {
                 appendDateAddFromNow(searchstmtbuffer, "DAY", "-"+getNowAddDayString(fieldvalue));
		        }    				
				else if(op != LK)
				{
					if(fieldvalue.equals(TODAY))
					{
						appendBeginOfToday(searchstmtbuffer);
					}
					else
					{
					   StringBuffer quotedField = new StringBuffer();
					   quotedField.append("'");
					   quotedField.append(fieldvalue);
					   quotedField.append("'");
	                   if(fieldvalue.indexOf(':')>0)
	                   {
	                       appendStrToDatetimeStmt(searchstmtbuffer, quotedField.toString(), row.getDateConversionsType());
	                   }
	                   else
	                   {
	                       appendStrToDateStmt(searchstmtbuffer, quotedField.toString(), row.getDateConversionsType());
	                   }
					}
				}				
			}	
		}
	}	
	
	protected String getNowAddDayString(String fieldname)
	{
	   return(fieldname.substring(10, fieldname.length()));
	}
	
	protected void appendDateAddFromNow(StringBuffer stmt, String interval, String amount)
   {
      appendDateAddStmt(stmt, interval, amount, getCurrentDateMethod());
   }  
	
	protected void appendFieldOpStringValue(StringBuffer searchstmtbuffer, String fieldname,
			String value, MapData row)
	{
		String fieldvalue = value;
		int op = getOp(fieldvalue);

		searchstmtbuffer.append(fieldname);

		fieldvalue = appendOp(searchstmtbuffer, fieldname, fieldvalue, Types.VARCHAR, op);

		if(fieldvalue.equals(ActionBean.NULL))
		{
			searchstmtbuffer.append(ActionBean.NULL);
		}
		else if(fieldvalue.equals(ActionBean.EMPTY))
		{
			searchstmtbuffer.append("''");
		}
		else if(fieldvalue.equals(ActionBean.CURRENTUSER))
		{
			searchstmtbuffer.append('\'');
			searchstmtbuffer.append(row.getString(ActionBean.CURRENTUSERID));
			searchstmtbuffer.append('\'');
		}
		else
		{
			int ai = fieldvalue.indexOf("'");
			if(ai >= 0)
			{
				if(fieldvalue.length() == ai + 1 || fieldvalue.charAt(ai + 1) != '\'')
				{
					fieldvalue = ActionBean.escapeQuote(fieldvalue);
				}
			}
			searchstmtbuffer.append("'");
			searchstmtbuffer.append(fieldvalue);
			searchstmtbuffer.append("'");
		}
	}

	protected void appendFieldOpBooleanValue(StringBuffer searchstmtbuffer, String fieldname,
			String value, MapData row)
	{
		String fieldvalue = value;
		int op = getOp(fieldvalue);

		searchstmtbuffer.append(fieldname);

		fieldvalue = appendOp(searchstmtbuffer, fieldname, fieldvalue, Types.BOOLEAN, op);

		if(fieldvalue.equals(ActionBean.NULL))
		{
			searchstmtbuffer.append(ActionBean.NULL);
		}
		else
		{
			Boolean b = new Boolean(fieldvalue);
			if(b.equals(Boolean.TRUE))
			{
				searchstmtbuffer.append(DB_BIT_TRUE);
			}
			else
			{
				searchstmtbuffer.append(DB_BIT_FALSE);
			}
		}
	}	
	
	protected void appendFieldOpValue(StringBuffer searchstmtbuffer, String fieldname,
			String fieldvalue, int fieldtype, MapData row)
	{
		if(fieldvalue != null && fieldvalue.length() != 0)
		{
			if(fieldtype == Types.TIMESTAMP)
			{
				appendFieldOpTimestampValue(searchstmtbuffer, fieldname, fieldvalue, row);
			}
			else if(fieldtype == Types.BOOLEAN)
			{
				appendFieldOpBooleanValue(searchstmtbuffer, fieldname, fieldvalue, row);
			}
			else
			{
				appendFieldOpStringValue(searchstmtbuffer, fieldname, fieldvalue, row);
			}
		}
	}

	protected void appendSortOrder(StringBuffer searchstmtbuffer, TableData row)
	{
		appendSortOrder(searchstmtbuffer, row.getSortfields(), row.getSortorder());
	}

	protected void appendSortOrder(StringBuffer searchstmtbuffer, String[] sortfields,
			String[] sortorder)
	{
		StringBuffer sortbuffer = new StringBuffer();
		if(sortfields != null)
		{
            int dotindex = 0;
            String sortfield;
			for(int i = 0; i < sortfields.length; i++)
			{
				if(sortfields[i] != null && sortfields[i].length() != 0)
				{
					if(sortbuffer.length() != 0)
					{
						sortbuffer.append(_comma);
					}
					else
					{
						sortbuffer.append(" order by ");
					}
                    sortfield = RelationSpec.escapeRelationName(sortfields[i]);
                    dotindex = sortfield.indexOf('.');
                    if(dotindex>0)
                    {
                        appendWithQuote(sortbuffer, sortfield.substring(0,dotindex));
                        sortbuffer.append('.');
                        appendWithQuote(sortbuffer, sortfield.substring(dotindex+1,sortfield.length()));
                    }
                    else
                    {
                        appendWithQuote(sortbuffer, sortfield);
                    }
					if(sortorder != null && sortorder[i] != null && sortorder[i].length() != 0)
					{
						sortbuffer.append(" ");
						sortbuffer.append(sortorder[i]);
					}
				}
			}
		}
		searchstmtbuffer.append(sortbuffer.toString());
	}

	protected void appendSearchGroups(StringBuffer searchstmtbuffer, Map group,
			String groupkey, TableSpec tspec, TableData row, String rname)
	{
		int fieldtype = 0;
		String fieldname = null;
		String fullfieldname = null;
		StringBuffer tempfieldname;

		Iterator groupedfields = group.keySet().iterator();
		int fieldsindex = -1;
		String grouptype = null;
		boolean groupfieldsadded = false;
		Object groupchild = null;
		if(groupkey.indexOf('|') == 0)
		{
			grouptype = _or;
		}
		else
		{
			grouptype = _and;
		}
		groupfieldsadded = false;
		searchstmtbuffer.append('(');
		while(groupedfields.hasNext())
		{
			fieldname = (String) groupedfields.next();
			//System.out.println(fieldname);
			//System.out.println(rname);
			groupchild = group.get(fieldname);
			if(groupfieldsadded)
			{
				searchstmtbuffer.append(grouptype);
			}
			if(groupchild instanceof String)
			{
				fieldsindex = tspec.getFieldsIndex(fieldname);

				//System.out.println(groupchild);
				if(fieldsindex > -1)
				{
					tempfieldname = new StringBuffer();
					if(rname != null)
					{
						tempfieldname.append(rname);
					}
					else
					{
						tempfieldname.append(tspec.getTableName());
					}
					tempfieldname.append(_dot);
					tempfieldname.append(fieldname);
					fullfieldname = tempfieldname.toString();
					fieldtype = tspec.getFieldType(fieldsindex);

					appendField(searchstmtbuffer, fullfieldname, (String) groupchild, fieldtype,
							row);
					groupfieldsadded = true;
				}
			}
			else
			{
				appendSearchGroups(searchstmtbuffer, (Map) groupchild, fieldname, tspec, row,
						rname);
				groupfieldsadded = true;
			}
		}
		searchstmtbuffer.append(')');

	}

	protected boolean appendRelatedSearch(boolean padded, StringBuffer searchstmtbuffer,
			TableData row)
	{
		TableSpec tspec = row.getTableSpec();
		boolean added = false;
		Relation relatedfields = row.getRelatedFields();
		if(relatedfields.hasRelations())
		{
			//System.out.println("has relations");
			Iterator relations = relatedfields.getRelationsIterator();
			Relation current = null;

			String ftablename = tspec.getTableName();

			while(relations.hasNext())
			{
				current = (Relation) relations.next();

				if(added || padded)
				{
					searchstmtbuffer.append(_and);
				}

				appendRelated(current, ftablename, searchstmtbuffer, row);
				//ftablename = current.getName();
				added = true;
			}
		}
		return (added || padded);
	}

	protected void appendRelated(Relation current, String lasttablename,
			StringBuffer searchstmtbuffer, TableData row)
	{
		RelationSpec rspec = row.getRelationSpec();

		try
		{
			//System.out.println("get spec");
			String rname = current.getName();
			rname = RelationSpec.escapeRelationName(rname);
			int relationindex = rspec.getIndex(current.getName());
			String tablename = rspec.getToTableName(relationindex);
			TableSpec cspec = SpecManager.getTableSpec(rspec.getToTableSpecLocation(relationindex));
			String omitname = "-" + current.getName();
			String omit = (String) row.get(omitname);
			//System.out.println("relationship name "+current.getName());
			//System.out.println("get related table");
			searchstmtbuffer.append(RN);
			if(omit != null && omit.equals(_omit))
			{
				searchstmtbuffer.append(" not ");
			}
			searchstmtbuffer.append(" exists(select ");
			searchstmtbuffer.append(rname);
			searchstmtbuffer.append(_dot);
			searchstmtbuffer.append(rspec.getToKey(relationindex));
			searchstmtbuffer.append(" from ");
			
			SqlDatabase database = row.getDatabase();
			database.appendQualifiedTableName( searchstmtbuffer, 
					row.getString(ActionBean.server),
					row.getString(ActionBean.database),
					tablename,
					rname);	

			searchstmtbuffer.append(" where ");
			boolean added1 = false;
			if(current.hasFields())
			{
				Object[] rfields = current.getKeys();
				StringBuffer fullfieldname = null;
				String fieldname = null;
				String fieldvalue = null;
				int fieldtype = 0;
				int fieldindex = -1;
				Map group = null;
				Map groups = row.getSearchGroups();
				for(int i = 0; i < rfields.length; i++)
				{
					fieldname = (String) rfields[i];
					if(fieldname.indexOf('|') == 0 || fieldname.indexOf('^') == 0)
					{
						group = (Map) groups.get(fieldname);

						if(added1)
						{
							searchstmtbuffer.append(_and);
						}
						appendSearchGroups(searchstmtbuffer, group, fieldname, cspec, row, rname);
						added1 = true;
					}
					else
					{
						fieldindex = cspec.getFieldsIndex(fieldname);
						fieldtype = cspec.getFieldType(fieldindex);
						fieldvalue = current.getField(fieldname);
						if(i != 0)
						{
							searchstmtbuffer.append(_and);
						}
						fullfieldname = new StringBuffer();
						fullfieldname.append(rname);
						fullfieldname.append(_dot);
						fullfieldname.append(fieldname);

						appendField(searchstmtbuffer, fullfieldname.toString(), fieldvalue,
								fieldtype, row);
						added1 = true;
					}
				}
			}
			if(added1)
			{
				searchstmtbuffer.append(" and ");
			}
			searchstmtbuffer.append(rname);
			searchstmtbuffer.append(_dot);
			searchstmtbuffer.append(rspec.getToKey(relationindex));
			searchstmtbuffer.append(" = ");
			searchstmtbuffer.append(lasttablename);
			searchstmtbuffer.append(_dot);
			searchstmtbuffer.append(rspec.getFromKey(relationindex));

			//for(int j = 0; j < rspec.getConditionLength(relationindex); j++)
		   if (rspec.getConditionLength(relationindex) != 0)
			{
				searchstmtbuffer.append(ActionBean._space);
				searchstmtbuffer.append(getSubstituted(rspec.getCondition(relationindex), row));
			}

			if(current.hasRelations())
			{
				Iterator relations = current.getRelationsIterator();
				Relation childrelation = null;
				while(relations.hasNext())
				{
					searchstmtbuffer.append(_and);
					childrelation = (Relation) relations.next();
					appendRelated(childrelation, current.getName(), searchstmtbuffer, row);
				}
			}
			searchstmtbuffer.append(")");
		}
		catch(Exception e)
		{
			row.setError(ActionBean.writeStackTraceToString(e));
			row.setError("\r\n");
		}
	}

	protected int getRangeIndex(String value, int fieldtype)
	{
		if(fieldtype == Types.TIMESTAMP)
		{
            return (getDateRangeIndex(value));
		}
		else
		{
			return (getRangeIndex(value));
		}

	}

    public int getRangeIndex(String value)
    {
        int index = value.indexOf("-");
        return (index);
    }
    
    public int getDateRangeIndex(String value)
    {
        int index = value.indexOf("-");
        if(index > 0 && index < 6)
        {
            index = value.indexOf("-", 9);
            if(index > 0)
            {
                return (index);
            }
        }
        else if(index > 6)
        {
            return (index);
        }
        return(-1);
    }
    
	public String buildOrFieldClause(String fieldname, String fieldvalue)
	{
		StringBuffer buffer = new StringBuffer();
		appendField(buffer, fieldname, fieldvalue, Types.VARCHAR, null);
		return buffer.toString();
	}

	public static void appendInTwoDigitFormat(StringBuffer b, String s)
	{
	   if(s.length() == 1)
	   {
	      b.append("0");
	   }
	   b.append(s);
	}
	
   public String getDateTimeString(Calendar cal)
   {
		if (cal == null) return "";
      StringBuffer datetime = new StringBuffer();
      datetime.append(String.valueOf(cal.get(Calendar.YEAR)));
      datetime.append("-");
      datetime.append(String.valueOf(cal.get(Calendar.MONTH) + 1));
      datetime.append("-");
      datetime.append(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
      datetime.append(" ");
      appendInTwoDigitFormat(datetime, String.valueOf(cal.get(Calendar.HOUR_OF_DAY)));
      datetime.append(":");
      appendInTwoDigitFormat(datetime, String.valueOf(cal.get(Calendar.MINUTE)));
      datetime.append(":");
      appendInTwoDigitFormat(datetime, String.valueOf(cal.get(Calendar.SECOND)));
      return (datetime.toString());
   }
	
	public String getDayStartString(Calendar cal)
	{
		if (cal == null) return "";
		StringBuffer end = new StringBuffer();
		end.append(String.valueOf(cal.get(Calendar.YEAR)));
		end.append("-");
		end.append(String.valueOf(cal.get(Calendar.MONTH) + 1));
		end.append("-");
		end.append(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
		end.append(daystart);
		return (end.toString());
	}

	public String getDayEndString(Calendar cal)
	{
		if (cal == null) return "";
		StringBuffer end = new StringBuffer();
		end.append(String.valueOf(cal.get(Calendar.YEAR)));
		end.append("-");
		end.append(String.valueOf(cal.get(Calendar.MONTH) + 1));
		end.append("-");
		end.append(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
		end.append(dayend);
		return (end.toString());
	}
	
	public String getContatenateString(String param1, String param2) {
	   String[] params = new String[2];
	   params[0] = param1;
	   params[1] = param2;
	   return getContatenateString(params);
	}
	public String getContatenateString(String param1, String param2, String param3) {
	   String[] params = new String[3];
	   params[0] = param1;
	   params[1] = param2;
	   params[2] = param3;
	   return getContatenateString(params);
	}
	
	public abstract String getContatenateString(String[] params);
	
	public String getReplaceString(String field, String fromString, String toString) {
	   StringBuffer result = new StringBuffer();
	   result.append(" REPLACE(");
	   result.append(field);
	   result.append(", '");
	   result.append(fromString);
	   result.append("', '");
	   result.append(toString);
	   result.append("') ");
	   
	   return result.toString();
	}

    public void appendCurrentWeekStmt(StringBuffer buffer, String field, boolean not)
    {
        appendRangeStmt(buffer, field, CalendarFactory.getStartOfWeekCal(), CalendarFactory.getEndOfWeekCal(), not);
    }

    public void appendCurrentMonthStmt(StringBuffer buffer, String field, boolean not)
    {
        appendRangeStmt(buffer, field, CalendarFactory.getStartOfMonthCal(), CalendarFactory.getEndOfMonthCal(), not);
    }
    
    public void appendCurrentQuarterStmt(StringBuffer buffer, String field, boolean not)
    {
        appendRangeStmt(buffer, field, CalendarFactory.getStartOfCurrentQuarterCal(), CalendarFactory.getEndOfCurrentQuarterCal(), not);
    }

    public void appendCurrentYearStmt(StringBuffer buffer, String field, boolean not)
    {
        appendRangeStmt(buffer, field, CalendarFactory.getStartOfYearCal(), CalendarFactory.getEndOfYearCal(), not);
    }
    
    public void appendLastWeekStmt(StringBuffer buffer, String field, boolean not)
    {
        appendRangeStmt(buffer, field, CalendarFactory.getStartOfLastWeekCal(), CalendarFactory.getEndOfLastWeekCal(), not);
    }

    public void appendLastMonthStmt(StringBuffer buffer, String field, boolean not)
    {
        appendRangeStmt(buffer, field, CalendarFactory.getStartOfLastMonthCal(), CalendarFactory.getEndOfLastMonthCal(), not);
    }

    public void appendLastYearStmt(StringBuffer buffer, String field, boolean not)
    {
        appendRangeStmt(buffer, field, CalendarFactory.getStartOfLastYearCal(), CalendarFactory.getEndOfLastYearCal(), not);
    }    

    public void appendNextRangeStmt(StringBuffer buffer, String field,
            Calendar date, Calendar init, boolean not)
    {
        appendRangeStmt(buffer, field, init, date, not);        
    }
    
    public void appendLastRangeStmt(StringBuffer buffer, String field,
            Calendar date, Calendar initdate, boolean not)
    {
        appendRangeStmt(buffer, field, date, initdate, not);
    }    
    
    public void appendFutureStmt(StringBuffer buffer, String field, boolean not)
    {
        buffer.append(field);
        if(not)
        {
            buffer.append(" <");
        }
        else
        {
            buffer.append(" >= ");
        }           
        buffer.append(getCurrentDateMethod());
    }    
    
    public abstract void appendRangeStmt(StringBuffer buffer, String field,
            Calendar date1, Calendar date2, boolean not);
    
    public abstract void appendDateTimeRangeStmt(StringBuffer buffer, String field,
          Calendar date1, Calendar date2, boolean not);
    
	public abstract void appendTodayStmt(StringBuffer buffer, String field, boolean not);
	
	
	public void appendCurrentStmt(StringBuffer buffer, String field, boolean not)
	{
		Calendar today = new GregorianCalendar();
		buffer.append(field);
		if(not) {
			buffer.append(" > '");
		}
		else {
			buffer.append(" <= '");
		}			
		buffer.append(getDayEndString(today));
		buffer.append("'");
	}

	public abstract void appendOverdueStmt(StringBuffer buffer, String field, boolean not);

	public abstract void appendThisWeekStmt(StringBuffer buffer, String field, boolean not);

	public abstract void appendThisMonthStmt(StringBuffer buffer, String field, boolean not);
    
    public abstract void appendHourStmt(StringBuffer buffer, String field, String num, boolean not);    
    
	public abstract void appendDateAddStmt(StringBuffer buffer, String interval, String amount, String datetime);
	
	public abstract void appendDateDiffStmt(StringBuffer buffer, String interval, String datetime1, String datetime2);
	
	public abstract void appendQualifiedTableName(StringBuffer buffer, String servername, String databasename, String tablename, String asname);

    public abstract void appendDatePartHour(StringBuffer buffer, String field);
    
    public abstract void appendDatePartYearMonth(StringBuffer buffer, String field);    
    
    public abstract void appendDatePartDayOfWeek(StringBuffer buffer, String field);        
    
    protected abstract String getDateConvertType(String type);    

    protected abstract String getDateTimeConvertType(String type);   
    
    public abstract void appendIndexOf(StringBuffer buffer, String value, String search);
    
    public abstract void appendStringLength(StringBuffer buffer, String expression);
    
    public abstract void appendTrim(StringBuffer buffer, String expression);
    
    public abstract void appendSubstring(StringBuffer buffer, String value, String start, String end);
    
    /**
     * Default and also legacy implementation, 
     */
    public void appendAliasWithQuote(StringBuffer buffer, String name) {
    	appendWithQuote(buffer, name);
    }
    
    public abstract void appendWithQuote(StringBuffer buffer, String name);
	
    public void appendDayOfWeekStmt(StringBuffer buffer, String field, String day, boolean not)
    {
        appendDatePartDayOfWeek(buffer, field);
        if(not)
        {
            buffer.append(" <> ");
        }
        else
        {
            buffer.append(" = ");
        }
        buffer.append(getDayOfWeekNum(day));         
    }
    
    public abstract String getDayOfWeekNum(String day);
}