package com.sok.framework.sql;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.SpecManager;
import com.sok.framework.generation.DatabaseException;
import com.sok.framework.generation.GenerationKeys;

/**
 * This is in the sql package as there's a few things I want to share with package level access.
 * @author mike
 *
 */
public class DatabaseSynchronizationListener implements ServletContextListener {

	private static final Logger logger = LoggerFactory.getLogger(DatabaseSynchronizationListener.class);
	private static List<String> startupErrors = Collections.emptyList();
	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		/* these are typically set by the init servlet, but we need these configured before starting up */
		logger.info("Starting up"); 
		SpecManager.setServletContext(event.getServletContext());
		//SchemaManager.setServletContext(event.getServletContext());
		ActionBean.setJndiName(event.getServletContext().getInitParameter("sqlJndiName"));
        ActionBean.setDatabase(event.getServletContext().getInitParameter("sqlDatabase"));
        
        Date lastUpdate = null;
        
        try {
        	GenRow last = new GenRow();
        	last.setViewSpec("ApplicationLogView");
        	last.setParameter("-select1","CreatedDate"); 
        	last.setParameter("ApplicationAction", "Application Start");
        	last.setParameter("-sort0","CreatedDate"); 
        	last.setParameter("-order0","DESC"); 
        	last.setParameter("-limit0","1"); 
        	last.doAction(GenerationKeys.SELECTFIRST);	// if the table does not exist, this will fail. 
        	logger.debug(last.getStatement());
        	lastUpdate = last.isSuccessful() ? last.getDate("CreatedDate") : null; 
        	
        } catch(DatabaseException de) {
        	logger.info("Error Retrieving Application Log for Last Startup - Initializing : " + de.getMessage());
        }
        
        try {
	        logger.info("Passing off to SchemaManager");
	        startupErrors = SchemaManager.prepareDatabase(lastUpdate);
	        SchemaManager.logUpdate("Application Start", StringUtils.join(startupErrors,"\n"));
        } catch (Exception e) {
        	e.printStackTrace();
        }
	}
	
	/**
	 * If this listener isn't active, the list will be empty and this will return false.
	 * @return
	 */
	public static boolean hasStartupErrors() {
		return !startupErrors.isEmpty();
	}
	
	public static List<String> getStartupErrors() {
		return Collections.unmodifiableList(startupErrors); 
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// Not sure if this will work, we may not have a context at this point 
		try { 
			//SchemaManager.logUpdate("Application Stop", null);
		} catch (DatabaseException de) { } 
	}
}
