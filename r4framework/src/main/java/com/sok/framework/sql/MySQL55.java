package com.sok.framework.sql;

public class MySQL55 extends MySQL5
{
	private static final char BACKTICK = '`';
	
	/**
	 * (non-Javadoc)
	 * @see com.sok.framework.sql.MySQL#appendWithQuote(java.lang.StringBuffer, java.lang.String)
	 * 
	 * This is being introduced as there are some reserved keywords in MySQL 5.5 which must be properly
	 * escaped to function. This could be used in previous versions as well, but 5.5 support is at this 
	 * stage experimental.
	 */
	@Override
    public void appendAliasWithQuote(StringBuffer buffer, String name)
    {
        buffer.append(BACKTICK).append(name).append(BACKTICK);
    }
	
	/*
	@Override
    public void appendWithQuote(StringBuffer buffer, String name)
    {
        buffer.append(BACKTICK).append(name).append(BACKTICK);
    }
    */
}