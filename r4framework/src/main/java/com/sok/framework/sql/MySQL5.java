package com.sok.framework.sql;

public class MySQL5 extends MySQL
{
	public void appendDateAddStmt(StringBuffer buffer, String interval, String amount, String datetime)
	{
		buffer.append("TIMESTAMPADD(");
		buffer.append(interval);
		buffer.append(',');
		buffer.append(amount);
		buffer.append(',');
		buffer.append(datetime);
		buffer.append(')');
	}	

	public void appendDateDiffStmt(StringBuffer buffer, String interval, String datetime1, String datetime2)
	{
		buffer.append("TIMESTAMPDIFF(");
		buffer.append(interval);
		buffer.append(',');
		buffer.append(datetime1);
		buffer.append(',');
		buffer.append(datetime2);
		buffer.append(')');
	}
}