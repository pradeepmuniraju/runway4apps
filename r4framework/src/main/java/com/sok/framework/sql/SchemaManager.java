package com.sok.framework.sql;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.SpecManager;
import com.sok.framework.SpecManager.SpecAction;
import com.sok.framework.StringUtil;
import com.sok.framework.TableSpec;
import com.sok.framework.TableSpec.TableUpdate;
import com.sok.framework.XMLSpec;
import com.sok.framework.generation.DatabaseException;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.offline.ThreadManager;
import com.sok.runway.security.SecurityFactory;

//import org.apache.catalina.util.ServerInfo;

/**
 * This class should handle any changes required to be made to the schema
 * 
 * @author mike
 * 
 */
public class SchemaManager {
	
	private static final Logger logger = LoggerFactory.getLogger(SchemaManager.class);
	private static boolean DROP_COLS = Boolean.FALSE;	// Works, but is not always what you want. 
	private static ServletContext context = null;
	private static String appID = "Unknown";
	
	public static void setServletContext(ServletContext sc)
	{
		logger.debug("setServletContext, context={}, sc={}", context == null ? "null" : "nn", sc == null ? "null" :"nn");
		if(context==null || sc == null)
		{
			logger.debug("setServletContext to " + (sc == null ? "null" : "not null")); 
			context = sc;
		}
		if(context != null) {
			// TODO - to be defined in web.xml in future for each application and implanted via the deployer
			appID = StringUtils.defaultIfEmpty(context.getInitParameter("app_id"), "Unknown");
		}
	} 
	
	private static int getFieldLen(String s) {
		if(StringUtils.isEmpty(s)) return -1; 
		else return Integer.parseInt(s);
	}
	
	public enum SchemaAction {
		Create,
		Alter,
		Error,
		None
	}
	
	public static class SchemaResponse {
		public final SchemaAction action; 
		public final String sql; 
		public final String table; 
		
		public SchemaResponse(String table, SchemaAction action, String sql) {
			this.table = table; 
			this.action = action; 
			this.sql = sql; 
		}
	}
	
	static void logUpdate(String action, String sql) {
		logTableUpdate(action, null, sql);
	}

	static void logTableUpdate(String action, String table, String sql) {
		try { 
			logUpdate(action, table, sql, SecurityFactory.hashString(StringUtils.trimToNull(sql) != null ? sql : KeyMaker.generate()));
		} catch (Exception e) { 
			logger.error("Error hashing sql", e); 
			throw new RuntimeException(e);
		}
	}

	static void logUpdate(String action, String sql, String hash) { 
		logUpdate(action, null, sql, hash);
	}
	
	static void logUpdate(String action, String ref, String sql, String hash) {
		logUpdate(action, ref, sql, hash, null);
	}
	
	static void logUpdate(String action, String ref, String sql, String hash, Date dateRef) {
		GenRow up = new GenRow();
		up.setTableSpec("ApplicationLog");
		up.setParameter("ApplicationLogID", StringUtils.trimToNull(hash));
		if(up.getParameter("ApplicationLogID") == null) up.setToNewID("ApplicationLogID");
		up.setParameter("ApplicationID", appID);
		up.setParameter("ApplicationAction", action); 
		up.setParameter("LogData", sql);
		up.setParameter("TableRef", ref);
		up.setParameter("DateRef", dateRef);
		up.setParameter("CreatedDate", "NOW");
		up.doAction(GenerationKeys.INSERT);
	}
	
	private static int getColIndex(ResultSetMetaData rsmd, String name) throws SQLException {
		for(int i=1; i<rsmd.getColumnCount(); i++) {
			if(StringUtils.equals(rsmd.getColumnName(i), name)) {
				return i; 
			}
		}
		return -1; 
	}
	
	/**
	 * Generate an alter-table statement based 
	 * @param tableSpecName
	 */
	public static SchemaResponse updateSchema(String tableSpecName) {
		SpecManager.reloadTableSpec(tableSpecName); // make sure it's current  
		
		if(!(ActionBean.getDatabase() instanceof MySQL)) {
			throw new UnsupportedOperationException("Database not yet supported for updateSchema : " + ActionBean.getDatabase().getClass().getName());
		}
		
		TableSpec ts = SpecManager.getTableSpec(tableSpecName);
		if(!ts.isSyncEnabled()) return new SchemaResponse(tableSpecName, SchemaAction.None, null);
		
		logger.debug("Checking Schema Differences for table={}", ts.getTableName());
		GenRow r = new GenRow();
		ActionBean.connect(r); 
		r.setCloseConnection(true); 
		ResultSet rs = null, rs2 = null, rs3 = null;
		try { 
			DatabaseMetaData dmd = r.getConnection().getMetaData();
			// parse jdbc url, like [jdbc:mysql://localhost:3306/sakila?profileSQL=true] 
			String DBNAME = SchemaManager.getDBNameWithException(r.getConnection());
			rs = dmd.getColumns(null /* don't use the catalog */, DBNAME, ts.getTableName(), "" /* column name match */);
			
			/* while generic-ish, this has only been tested with mysql */
			if(!rs.next()) {
				// could not find any columns, so this table doesn't exist and we need to generate a create statement
				// this format is mysql-y 
				return new SchemaResponse(tableSpecName, SchemaAction.Create, ActionBean.getDatabase().createTable(ts) + ";\n");
			} else {
				StringBuilder alter = new StringBuilder(); 
				rs.beforeFirst();
				// This query is run so that we have access to the MetaData, cthis is the only way to find if a column is auto-increment. 
				String q = new StringBuilder().append("select * from ").append(ts.getTableName()).append(" where 1 = 0").toString();
				rs2= r.getConnection().createStatement().executeQuery(q);
				ResultSetMetaData rsmd2 = rs2.getMetaData();
				
				int ix = 1;
				java.util.Set<String> foundCols = new java.util.HashSet<String>(ts.getFieldsLength()-1);
				while(rs.next()) {
					int tix = ts.getFieldsIndex(rs.getString("COLUMN_NAME"));
					foundCols.add(rs.getString("COLUMN_NAME")); 
					int mdix = getColIndex(rsmd2, rs.getString("COLUMN_NAME"));
					if(tix == -1) {
						// These are columns which do appear in the database but do not appear in the TableSpec. Generally, these are out of date and may be dropped, 
						// However there may be cases where the TableSpec itself is incorrect or out of date. Thus, we are not presently dropping them - this may change in future. 
						if(DROP_COLS) { 
							// The main issue found with this was that there was some tables which had columns which were not in the TableSpec which should be kept - Locations is like this.
							// The other issue is the use of 'partial' tablespecs. Those partials should be marked with sync=false, but if they're not could cause columns to be dropped - not good.
							if(alter.length()==0) {
								alter.append("ALTER TABLE `").append(ts.getTableName()).append("` DROP COLUMN `");
							} else {
								alter.append(", DROP COLUMN `");
							}
							alter.append(rs.getString("COLUMN_NAME")).append("`");
						} else {
							logger.warn("/* updateSchema: Drop Column Skipped for col={} spec={} */", rs.getString("COLUMN_NAME"), tableSpecName);
						}
					} else {
						//TODO - This is MySQL syntax
						if(!StringUtils.equals(rs.getString("COLUMN_DEF"),  ts.getDataDefault(tix))
								&& StringUtils.equalsIgnoreCase(rs.getString("TYPE_NAME"),  ts.getDataType(tix))) {
							// If the column types are different, you can use the standard modify call. ( ie col type(len) default 'xx' )  
							// If it's the same, you need to use set/drop default. 
							if(alter.length()==0) {
								alter.append("ALTER TABLE `").append(ts.getTableName()).append("` ALTER COLUMN `");
							} else {
								alter.append(", ALTER COLUMN `");
							}
							alter.append(rs.getString("COLUMN_NAME")).append("`"); 
							if(ts.getDataDefault(tix) != null) {
								alter.append(" SET DEFAULT '").append(ts.getDataDefault(tix)).append("'"); 
							} else {
								alter.append(" DROP DEFAULT"); 
							}
						}
						if(("YES".equals(rs.getString("IS_NULLABLE")) != ts.isDataNullable(tix)) || 
							(rsmd2.isAutoIncrement(ix) != ts.isDataAutoIncrement(tix)) ||
							(!StringUtils.equalsIgnoreCase(rs.getString("TYPE_NAME"),  ts.getDataType(tix))) ||
							(ts.getDataLength(tix) != null && !StringUtils.equals(rs.getString("COLUMN_SIZE"),  ts.getDataLength(tix)))
							) { 
							if(logger.isDebugEnabled()) {
								logger.debug("Field={}", rs.getString("COLUMN_NAME")); 
								logger.debug("dbNullable={} tNullable={}", "YES".equals(rs.getString("IS_NULLABLE")), ts.isDataNullable(tix));
								logger.debug("dbAutoIncr={} tAutoIncr={}", rsmd2.isAutoIncrement(ix), ts.isDataAutoIncrement(tix));
								logger.debug("dbType={} tType={}", rs.getString("TYPE_NAME"), ts.getDataType(tix));
								logger.debug("dbLen={} tLen={}", rs.getString("COLUMN_SIZE"), ts.getDataLength(tix));
							}
							// This column exists, but it's definition does not match what was found in the TableSpec. Thus, we will update it to match. 
							if(alter.length()==0) {
								alter.append("ALTER TABLE `").append(ts.getTableName()).append("` MODIFY ");
							} else {
								alter.append(", MODIFY ");
							}
							String alt = ActionBean.getDatabase().alterColumn(ts.getFieldName(tix), ts.getDataType(tix), getFieldLen(ts.getDataLength(tix)), ts.isDataNullable(tix), ts.isDataAutoIncrement(tix), ts.getDataDefault(tix));
							if (alt == null || alt.length() == 0) return null;
							alter.append(alt);
						}
					}
					ix++;
				}	
				for(int tix=0; tix< ts.getFieldsLength(); tix++) {
					if(!foundCols.contains(ts.getFieldName(tix))) {
						// This column did not exist in the DB, but it did exist in the TableSpec. We will add it into the Database Table. 
						if(alter.length()==0) {
							alter.append("ALTER TABLE `").append(ts.getTableName()).append("` ADD COLUMN ");
						} else {
							alter.append(", ADD COLUMN ");
						}
						alter.append(ActionBean.getDatabase().alterColumn(ts.getFieldName(tix), ts.getDataType(tix), getFieldLen(ts.getDataLength(tix)), ts.isDataNullable(tix), ts.isDataAutoIncrement(tix), ts.getDataDefault(tix)));
					}	
				}
				
				rs3 = dmd.getIndexInfo(null /* dont use the catalog */, DBNAME, ts.getTableName(), false, false);
				rsmd2 = rs3.getMetaData(); 
				
				/*
				 * <index name="IX_EstateStage" fields="EstateID,StageID" unique="false" />	<!-- current version -->
				 * <index name="IX_EstateStage" fields="EstateID/A,StageID/D" unique="false" /> <!-- future if you want to use asc/desc indexes -->
				 */
				
				Index last = null;
				List<Index> dbIndexes = new ArrayList<Index>(ts.getIndexLength()+2);
				boolean foundPrimary = false, dropPrimary = false;
				while(rs3.next()) {
					// each next is an index or a component of an index  
					if("PRIMARY".equals(rs3.getString("INDEX_NAME"))) {
						// NB, composite Primary Keys are not supported.
						if(ts.getPrimaryKeyName().equals(rs3.getString("COLUMN_NAME"))) {
							foundPrimary = true; 
						} else if("MarkerRuleLibrary".equals(ts.getTableName())) {
							// we want to only do this in this case, as we know it needs it.
							if(!dropPrimary) { 
								if(alter.length()==0) {
									alter.append("ALTER TABLE `").append(ts.getTableName()).append("` DROP PRIMARY KEY");
								} else {
									alter.append(", DROP PRIMARY KEY");
								}
								dropPrimary = true;
							}
						} else {
							// if we through this exception it will stop tomcat from starting, not good at all
							//throw new RuntimeException(tableSpecName + " / " + ts.getTableName() + " - TS PK != PK and changing Primary keys not supported - " + ts.getPrimaryKeyName() + " != " + rs3.getString("COLUMN_NAME"));
							
							// lets log it and continue
							logger.error(tableSpecName + " / " + ts.getTableName() + " - TS PK != PK and changing Primary keys not supported - " + ts.getPrimaryKeyName() + " != " + rs3.getString("COLUMN_NAME"));
							continue;
						}
					} else {
						String index_name = rs3.getString("INDEX_NAME");
						String index_col = rs3.getString("COLUMN_NAME"); 
						boolean asc = "A".equals(rs3.getString("ASC_OR_DESC"));
						if(!asc) index_col += "/D"; 
						
						if(last != null && last.name.equals(index_name)) {
							// we have the same index, which must act on several fields so add field to last index 
							last.cols.add(index_col);
							continue;
						} else if(last != null) {
							dbIndexes.add(last); 
						}
						last = new Index(); 
						last.name = index_name; 
						last.cols.add(index_col); 
						last.unique = !"true".equals(rs3.getString("NON_UNIQUE"));
					}
				} 
				if(last != null) {
					dbIndexes.add(last); 
				}
				java.util.Set<String> foundIndexes = new java.util.HashSet<String>(ts.getIndexLength());
				for(Index dbIx: dbIndexes) {
					foundIndexes.add(dbIx.name); 
					String field_cat = StringUtils.join(dbIx.cols,",");
					if(ts.hasIndex(dbIx.name)) {
						int ixi = ts.getIndexIndex(dbIx.name); 
						if((dbIx.unique && !ts.isIndexUnique(ixi)) 
								|| !StringUtils.equals(field_cat, ts.getIndexFields(ixi))) {
							// need to modify existing index 
							if(alter.length()==0) {
								alter.append("ALTER TABLE `").append(ts.getTableName()).append("` ");
							} else {
								alter.append(", ");
							}
							alter.append(ActionBean.getDatabase().alterIndex(dbIx.name, null, false, true));
							logger.debug("alterIndex(name={}, fields={}, unique={}, drop=false", new String[]{dbIx.name, field_cat,dbIx.unique?"true":"false"}); 
							alter.append(", ").append(ActionBean.getDatabase().alterIndex(dbIx.name, ts.getIndexFields(ixi), ts.isIndexUnique(ixi), false));
						} else {
							logger.trace("Index ok: name=[{}], ts_unique={}, db_unique={}, ts.fields={}, db_fields={}", new String[]{dbIx.name, String.valueOf(ts.isIndexUnique(ixi)), String.valueOf(dbIx.unique), ts.getIndexFields(ixi), field_cat});
						}
					} else {
						int ixs = ts.findIndex(field_cat); 
						if(ixs != -1) {
							// we found an existing index which we have it named differently. 
							// we will drop this index and it would be re-added in another portion. 
							if(alter.length()==0) {
								alter.append("ALTER TABLE `").append(ts.getTableName()).append("` ");
							} else {
								alter.append(", ");
							}
							alter.append(ActionBean.getDatabase().alterIndex(dbIx.name, null, false, true));
						} else { 
							// This means there is an additional index on the Table in the DB which is not in all other Runway systems. 
							// If this index is valid for all, it should be added to the spec. This index is not a duplicate of some other index that is known about
							// by Runway. This index may be a duplicate of some other index which Runway does not know about. 
							logger.warn("Found an index with name=[{}] for table=[{}] which did not appear in the spec", dbIx.name, ts.getTableName());
						}
					}
				}
				for(int tix=0; tix< ts.getIndexLength(); tix++) {
					if(!foundIndexes.contains(ts.getIndexName(tix))) {
						if(alter.length()==0) {
							alter.append("ALTER TABLE `").append(ts.getTableName()).append("` ");
						} else {
							alter.append(", ");
						}
						logger.debug("alterIndex(name={}, fields={}, unique={}, drop=false", new String[]{ts.getIndexName(tix), ts.getIndexFields(tix), ts.isIndexUnique(tix)?"true":"false"}); 
						alter.append(ActionBean.getDatabase().alterIndex(ts.getIndexName(tix), ts.getIndexFields(tix), ts.isIndexUnique(tix), false));
					}	
				}
				if(!foundPrimary && ts.getPrimaryKeyName() != null) { // ContactIDLists does not have a PK, for ex 
					if(alter.length()==0) {
						alter.append("ALTER TABLE `").append(ts.getTableName()).append("` ");
					} else {
						alter.append(", ");
					}
					//TODO move to MySQL 
					logger.info("Table Missing Primary Key -- {}!", ts.getTableName());
					alter.append(" ADD PRIMARY KEY (`").append(ts.getPrimaryKeyName()).append("`)");
				}
				if(alter.length()!=0) {
					alter.append(" /* ").append(System.currentTimeMillis()).append(" */;\n"); 
					return new SchemaResponse(tableSpecName, SchemaAction.Alter, alter.toString());
				}
			}
			return new SchemaResponse(tableSpecName, SchemaAction.None, null);
		} catch (Exception se) { 
			logger.error("Error producting alter statement for table " + tableSpecName, se);
			return new SchemaResponse(tableSpecName, SchemaAction.Error, "/* " + se.getMessage() + " */ \n");
		} finally {
			r.close();
			if(rs != null) {  try {	rs.close();	} catch (SQLException e) { e.printStackTrace();	} }
			if(rs2 != null) {  try {	rs2.close();	} catch (SQLException e) { e.printStackTrace();	} }
			if(rs3 != null) {  try {	rs3.close();	} catch (SQLException e) { e.printStackTrace();	} }
		}
	}
	
	public static void updateAllFromSchema(boolean specific) {
		SpecManager.performTableOperations(specific, new SpecAction<String>(){
			public String doIt(String s) {
				updateFromSchema(s);
				return null;
			}
		});
	}
	
	public static Collection<SchemaResponse> updateAllSchemas(boolean specific) {
		return SpecManager.performTableOperations(specific, new SpecAction<SchemaResponse>(){
			public SchemaResponse doIt(String s) {
				return updateSchema(s);
			}
		});
	}
	
	protected static String getDBName(Connection con) {
		try { 
			return getDBNameWithException(con);
		} catch (SQLException se) {
			throw new DatabaseException("Unable to retrieve connection meta data", se);
		}
	}
	
	protected static String getDBNameWithException(Connection con) throws SQLException {
		DatabaseMetaData dmd = con.getMetaData();
		// parse jdbc url, like [jdbc:mysql://localhost:3306/sakila?profileSQL=true] 
		String DBNAME = dmd.getURL();
		//dmd.getUserName()
		//dmd.get
		DBNAME = DBNAME.substring(DBNAME.lastIndexOf("/") + 1); 
		if(DBNAME.indexOf("?") != -1) {
			DBNAME = DBNAME.substring(0, DBNAME.indexOf("?")); 
		}
		return DBNAME;
	}
	
	protected static String generateTableData(boolean specific, String dataset) {
		
		final GenRow tc = new GenRow();
		try { 
			ActionBean.connect(tc); 
			
			String lastUpdate = null;
			final GenRow maxUpdate = new GenRow(); 
			maxUpdate.setConnection(tc.getConnection()); 
			maxUpdate.setParameter("-select1","max(UpdateDate) as max"); 
			maxUpdate.setTableSpec("Updates");
			try { 
				maxUpdate.getResults(); 
				if(maxUpdate.getNext()) {
					lastUpdate = StringUtils.trimToNull(maxUpdate.getData("max"));
					logger.debug("lastUpdate: {}", lastUpdate);
				}
			} finally {
				maxUpdate.close(); 
			}
			lastUpdate = lastUpdate != null ? new StringBuilder().append("'").append(lastUpdate).append("'").toString() : "now()";
			tc.setParameter("-select0", "count(*) as cnt"); 
			
			final String DBNAME = getDBName(tc.getConnection());
			final String folder = InitServlet.getRealPath("/WEB-INF/data/" + dataset); 
			final File parent = new File(folder); 
			if(!parent.exists())
				parent.mkdirs();
			 
			// TODO test output of dataset from built files. 
			final DataSetSpec ds = new DataSetSpec("{data/}" + dataset, StringUtil.removeNonFileSystemCharacters(dataset.toLowerCase())); 
			
			String ret = new StringBuilder().append("#!/bin/bash -ex \n\n").append(StringUtils.join(SpecManager.performTableOperations(specific, new SpecAction<String>(){
				public String doIt(String s) {
					tc.setTableSpec(s);
					if(tc.getTableSpec().isSyncEnabled() && !"Activities".equals(tc.getTableSpec().getTableName()) && !"Updates".equals(tc.getTableSpec().getTableName()) && tc.getTableSpec().getTableName().indexOf("UserLayout") == -1) {	// TODO create a full list of the logging tables.
						try { 
							tc.setStatement((String)null);
							tc.getResults(); 
							if(tc.getNext()) {
								int i = tc.getInt("cnt");
								if(i > 0) {
									StringBuilder sb = new StringBuilder(); 
									sb.append("#Table " + s + " had " + i + " records\n");
									// dont create the table, use the column names 
									sb.append("mysqldump -uroot -pjashap8 --no-create-info --complete-insert " + DBNAME + " " + tc.getTableSpec().getTableName() + " > " + parent.getAbsolutePath() + "/" + tc.getTableSpec().getTableName() + ".sql\n");
									ds.addTable(tc.getTableSpec().getTableName());
									return sb.toString();
								}
							}
						} finally {
							tc.close(); 
						}
					}
					return null;
				}
			}),"")).toString();
			
			ds.save();
			
			return ret; 
		} finally {
			tc.setCloseConnection(true); 
			tc.close();
		}
	}
	
	protected static void importDataset(String dataset) {
		final GenRow tc = new GenRow();
		try { 
			ActionBean.connect(tc);
			tc.setCloseConnection(false);
			final String folder = context.getRealPath("/WEB-INF/data/" + dataset); 
			final File parent = new File(folder);
			
			if(!parent.exists())
				throw new IllegalArgumentException("Folder does not exist for dataset"); 
			
			final File[] list = parent.listFiles();
			if(list == null || list.length == 0)
				throw new IllegalArgumentException("Data does not exist for dataset");
			
			FileInputStream fis = null;
			for(File f: list) {
				
			}
		} finally {
			tc.setCloseConnection(true); 
			tc.close();
		}
	}
	
	protected static void importTableDataset(TableSpec ts, File f, GenRow worker) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(f);
			importSQL(worker.getConnection(), fis);
			
			String t = f.getName();
			t = t.substring(0, t.length()-4); 
			try { 
					
				boolean mod = ts.hasField("ModifiedDate"); 
				boolean cre = ts.hasField("CreatedDate"); 
				
				if(mod || cre) {
					logger.debug("Table {} has mod/cre, attempting to reset", t);
					worker.clear();
					worker.setTableSpec(ts); 
					worker.setParameter("ON-" + ts.getPrimaryKeyName(), "!EMPTY"); 
					if(mod) worker.setParameter("ModifiedDate", "NOW");
					if(cre) worker.setParameter("CreatedDate","NOW");
					worker.doAction(GenerationKeys.UPDATEALL);
					logger.debug(worker.getStatement());
				}
			} catch (NullPointerException npe) { logger.debug("Table {} could not be found directly for mod/cre check", t); }
		} catch (SQLException se) {
			System.out.println("Error in SQL as generated from " + f.getAbsolutePath()); 
			se.printStackTrace();
		} catch (java.io.FileNotFoundException fnfe) {
			System.out.println("sky is falling"); 
			fnfe.printStackTrace();
		} finally {
			if(fis != null) {
				try {
					fis.close();
				} catch (IOException ioe) { System.out.println("error closing fis " + ioe.getMessage()); }
			}
		}
	}
	
	public static void importSQL(Connection conn, java.io.InputStream in) throws SQLException
	{
		Scanner s = new Scanner(in);
		s.useDelimiter("(;(\r)?\n)|(--\n)");
		Statement st = null;
		try
		{
			st = conn.createStatement();
			while (s.hasNext())
			{
				String line = s.next();
				if (line.startsWith("/*!") && line.endsWith("*/"))
				{
					int i = line.indexOf(' ');
					line = line.substring(i + 1, line.length() - " */".length());
				}

				if (line.trim().length() > 0)
				{
					//System.out.println(line);
					st.execute(line);
				}
			}
		}
		finally
		{
			if (st != null) st.close();
		}
	}
	
	
	public static String checkViewFields(boolean specific) {
		return SpecManager.performViewOperations(specific, new SpecAction<String>() {
			public String doIt(String s) { 
				try { 
					logger.debug("checkView({})", s);
					SpecManager.getViewSpec(s);
				} catch (UnsupportedOperationException oe) {
					return s + " - " + oe.getMessage(); 
				} 
				return null;
			}
		});
	}
	

	/** 
	 * This will update the tablespec based on what is in the database.
	 * Used for the initial update, when the DB is more likely to be correct than the tablespecs.
	 * @param tableSpecName
	 */
	public static void updateFromSchema(String tableSpecName) {
		SpecManager.reloadTableSpec(tableSpecName); // make sure it's current
		
		TableSpec ts = SpecManager.getTableSpec(tableSpecName);
		if(!ts.isSyncEnabled()) return;
		GenRow r = new GenRow();
		ActionBean.connect(r); 
		r.setCloseConnection(true); 
		ResultSet rs = null, rs2 = null, rs3 = null;
		ResultSetMetaData rsmd2 = null;
		try { 
			DatabaseMetaData dmd = r.getConnection().getMetaData();
			// parse jdbc url, like [jdbc:mysql://localhost:3306/sakila?profileSQL=true] 
			String DBNAME = dmd.getURL();
			DBNAME = DBNAME.substring(DBNAME.lastIndexOf("/") + 1); 
			if(DBNAME.indexOf("?") != -1) {
				DBNAME = DBNAME.substring(0, DBNAME.indexOf("?")); 
			}
			rs = dmd.getColumns(null /* don't use the catalog */, DBNAME, ts.getTableName(), "" /* column name match */);
			if(rs.next()) { 
				rs2= r.getConnection().createStatement().executeQuery("select * from " + ts.getTableName() + " where 1 = 0");
				rsmd2 = rs2.getMetaData();
				int ix = 1;
				do {
					int tix = ts.getFieldsIndex(rs.getString("COLUMN_NAME"));
					if(tix == -1) {
						logger.warn("{} was missing from TableSpec - not adding. spec was: {}", rs.getString("COLUMN_NAME"), tableSpecName);
					} else { 
						ts.setDataNullable(tix, "YES".equals(rs.getString("IS_NULLABLE")));
						ts.setDataAutoIncrement(tix, rsmd2.isAutoIncrement(ix));
						ts.setDataDefault(tix, rs.getString("COLUMN_DEF"));
						ts.setDataType(tix, rs.getString("TYPE_NAME"));	// poss from rsmd2 
						ts.setDataLength(tix, null);
						for(String t: new String[]{"CHAR","VARCHAR","INT","FLOAT"}) {
							if(rs.getString("TYPE_NAME").equals(t)) {	// we don't care about the other column sizes, they're not required for create / alter
								ts.setDataLength(tix, rs.getString("COLUMN_SIZE"));		
							}
						}		
					}
					ix++;
				} while(rs.next());

				rs3 = dmd.getIndexInfo(null /* dont use the catalog */, DBNAME, ts.getTableName(), false, false);
				rsmd2 = rs3.getMetaData(); 
				
				Index last = null; 
				
				while(rs3.next()) {
					// each next is an index or a component of an index  
					if("PRIMARY".equals(rs3.getString("INDEX_NAME"))) {
						// primary key 
						if(!ts.getPrimaryKeyName().equals(rs3.getString("COLUMN_NAME"))) {
							throw new RuntimeException(tableSpecName + " / " + ts.getTableName() + " - TS PK != PK " + ts.getPrimaryKeyName() + " != " + rs3.getString("COLUMN_NAME"));
						}
					} else {
						String index_name = rs3.getString("INDEX_NAME");
						String index_col = rs3.getString("COLUMN_NAME"); 
						//int ord = rs3.getInt("ORDINAL_POSITION"); // this is the position of the column in the table, we're not going to be picky about this as the existing tables are all over the place.
						boolean asc = "A".equals(rs3.getString("ASC_OR_DESC"));
						if(!asc) index_col += "/D"; 
						
						if(last != null && last.name.equals(index_name)) {
							// add field to last index 
							last.cols.add(index_col);
							continue;
						} else if(last != null) {
							String field_cat = StringUtils.join(last.cols,",");
							if(ts.hasIndex(last.name)) {
								int ixi = ts.getIndexIndex(last.name); 
								ts.setIndexUnique(ixi, last.unique); 
								ts.setIndexFields(ixi, field_cat);
							} else {
								int ixs = ts.findIndex(field_cat); 
								if(ixs != -1) {
									if(!StringUtils.equals(last.name, ts.getIndexName(ixs))) { 
										throw new RuntimeException(tableSpecName + " / " + ts.getTableName() + " - Found an index using the same fields which already existed " + ts.getIndexName(ixs));
									}
								} else { 
									int ixi = ts.createIndex(last.name, last.unique);
									ts.setIndexUnique(ixi, last.unique); 
									ts.setIndexFields(ixi, field_cat);
								}
							}
						}
						last = new Index(); 
						last.name = index_name; 
						last.cols.add(index_col); 
						last.unique = !"true".equals(rs3.getString("NON_UNIQUE"));
					}
				} 
				if(last != null) {
					String field_cat = StringUtils.join(last.cols,",");
					if(ts.hasIndex(last.name)) {
						int ixi = ts.getIndexIndex(last.name); 
						ts.setIndexUnique(ixi, last.unique); 
						ts.setIndexFields(ixi, field_cat);
					} else {
						int ixs = ts.findIndex(field_cat); 
						if(ixs != -1) {
							// this should be dropped from the template.
							throw new RuntimeException(tableSpecName + " / " + ts.getTableName() + " - Found an index using the same fields which already existed " + ts.getIndexName(ixs));
						} 
						int ixi = ts.createIndex(last.name, last.unique);
						ts.setIndexUnique(ixi, last.unique); 
						ts.setIndexFields(ixi, field_cat);
					}
				}
				ts.save();
			} else {
				logger.warn("Table {} did not exist for spec {}", ts.getTableName(), tableSpecName);
			}
		} catch (Exception se) { se.printStackTrace(); 
		} finally {
			r.close();
			if(rs != null) {  try {	rs.close();	} catch (SQLException e) { e.printStackTrace();	} }
			if(rs2 != null) {  try {	rs2.close();	} catch (SQLException e) { e.printStackTrace();	} }
			if(rs3 != null) {  try {	rs3.close();	} catch (SQLException e) { e.printStackTrace();	} }
		}
	}
	
	private static class Index {
		public String name; 
		private java.util.List<String> cols = new java.util.ArrayList<String>(3);  
		private boolean unique; 
	}
 
	protected static class DataSetSpec extends XMLSpec {
		private final Date dateCreated; 

		public static String _name = "name";
		public static String _sqlpath = "sqlpath";
		public static String _created = "created";
		public static String _table = "table";
		public static String _file = "file";
		public static String _dataset = "dataset";

		private Element[] tables = null; 
		private final String sqlpath; 

		public DataSetSpec(String name, String sqlpath) {
			super();
			dateCreated = new Date();
			Element root = super.document.addElement(_dataset);
			super.name = name; 
			this.sqlpath = sqlpath;
			super.setAttribute(root, _name, name);
			super.setAttribute(root, _sqlpath, sqlpath);
		}
		
		public void addTable(String tableName) {
			Element root = super.document.getRootElement();
			Element node = root.addElement(_table);
			super.setAttribute(node, _table, tableName);
			
			tables = super.getElementArray(root,_table);
		}

		public DataSetSpec(String path) {
			super(SpecManager.getFilePath(path));
			if(super.document!=null)
			{        
				Element root = super.document.getRootElement();
				if(root != null)
				{        
					if(!StringUtils.equalsIgnoreCase(root.getName(), _dataset)) {
						throw new IllegalArgumentException("Invalid DataSet for path " + path);
					}
					super.name = root.attributeValue(_name);
					sqlpath = root.attributeValue(_sqlpath);
					try {
						dateCreated = new SimpleDateFormat("yyyy-MM-dd").parse(root.attributeValue(_created));
					} catch (ParseException e) {
						throw new IllegalArgumentException("Invalid Created Date for path " + path);
					}
					tables = super.getElementArray(root,_table);
					if(StringUtils.isBlank(super.name)) {
						throw new IllegalArgumentException("Invalid DataSet for path " + path);
					}
				}
				else
				{
					throw new NullPointerException("Document has no root element - " + path);
				}
			}
			else
			{
				throw new NullPointerException("Document cannot be loaded - " + path);
			}        
		}
		public Date getCreatedDate() {
			return dateCreated; 
		}
		
		public File getTableData(String table) {
			for(Element e: tables) {
				if(StringUtils.equals(super.getAttribute(e, _name), table)) {
					File f = new File(context.getRealPath(new StringBuilder().append("/WEB-INF/data/").append(sqlpath).append("/").append(table).append(".sql").toString())); 
					if(!f.exists()) {
						throw new RuntimeException("DataSet " + super.name + "referred to data file which did not exist " + f.getAbsolutePath());
					}
					return f;
				}
			}
			return null;
		}
	}

	private static DataSetSpec getDataSet(String runwayType) {
		if("client".equals(runwayType)) {
			// load the clean dataset 
			return new DataSetSpec("{data/}BaseSet"); 
		} else {
			return null;
		}
	}

	public static List<String> prepareDatabase(Date lastUpdate) {
		logger.debug("prepareDatabase({})", lastUpdate); 
		final BlockingQueue<SchemaResponse> bq = new LinkedBlockingQueue<SchemaResponse>();
		List<String> schemaErrors = Collections.emptyList();
		final Boolean[] Init = { Boolean.FALSE };
		final String runwayType = StringUtils.defaultIfEmpty(context.getInitParameter("runway_type"), "client");
		ThreadManager.startThread(new Runnable() {
			public void run() {
				SchemaResponse sr = SchemaManager.updateSchema("ApplicationLog"); 
				if(sr != null) bq.add(sr); 
				SpecManager.performTableOperations(false, new SpecAction<SchemaResponse>(){
					public SchemaResponse doIt(String s) {
						logger.debug("Checking table for schema {}", s);
						if (s == null || s.length() == 0 || s.endsWith("/")) return null;
						return "ApplicationLog".equals(s) ? null : SchemaManager.updateSchema(s);
					}
				}, bq);
				logger.info("Completed initial schema check"); 
				Init[0] = Boolean.TRUE;
			}
		}, "DatabaseSchemaSync");
		
		final DataSetSpec ds = getDataSet(runwayType); 
		
		logger.debug("Setting up to sync"); 
		
		SchemaResponse next = null; 
		GenRow exec = new GenRow();
		GenRow check = new GenRow(); 
		GenRow worker = new GenRow(); 
		check.setViewSpec("ApplicationLogView");
		//take an element while the queue still has some 
		//while(!InitComplete && (next = bq.poll())) 
		try {
			ActionBean.connect(exec);
			exec.setCloseConnection(false);
			check.setConnection(exec.getConnection());
			worker.setConnection(exec.getConnection());
			//ActionBean.connect(worker);
			//worker.setCloseConnection(false);
			while((next = bq.poll(50, TimeUnit.MILLISECONDS)) != null || !Init[0].booleanValue()) {
				logger.debug("Waiting for sync complete"); 
				if(next != null) {
					logger.debug("Processing Update for table={}, action={}", next.table, next.action);
					TableSpec ts = SpecManager.getTableSpec(next.table);
					switch(next.action) {
						case Create:
						case Alter:
							try { 
								exec.setStatement(next.sql); 
								exec.doAction(GenerationKeys.EXECUTE);
								SchemaManager.logTableUpdate(next.action == SchemaAction.Create ? "Create Table" : "Alter Table", next.table, next.sql); 
							} catch (DatabaseException de) {
								if(schemaErrors.isEmpty()) {
									schemaErrors = new ArrayList<String>();
								}
								String err = String.format("%1$s - [%2$s] - %3$s", next.table, next.sql, de.getMessage()); 
								schemaErrors.add(err);
								logger.error("Error executing update for table " + err);
							}
							
							if(next.action == SchemaAction.Create) {
								// load the default data if it exists
								File f = ds.getTableData(ts.getTableName());
								if(f != null) {
									importTableDataset(ts, f, worker);
									SchemaManager.logTableUpdate("Imported Table Data", next.table, f.getAbsolutePath());
								}
							}
						case None:
							logger.trace("No schema changes required for table={}", next.table);
							
							for(TableUpdate update: ts.getUpdatesAfter(next.action != SchemaAction.Create ? getLastTableUpdate(next.table, check.getConnection()) : ds.getCreatedDate()))
							{
								boolean req = false;
								String hash = null;
								try {
									hash = SecurityFactory.hashString(update.u);
								} catch(Exception e) {
									if(schemaErrors.isEmpty()) {
										schemaErrors = new ArrayList<String>();
									} 
									schemaErrors.add("Error Hashing Statement " + update);
									continue;
								}
								
								check.clear();
								check.setParameter("ApplicationLogID", hash);
								check.doAction(GenerationKeys.SELECT);
								req = !check.isSuccessful(); 
								
								if(req) {
									try { 
										exec.setStatement(update.u); 
										exec.doAction(GenerationKeys.EXECUTE);
										
										SchemaManager.logUpdate("Update Table", next.table, update.u, hash, update.d);
									} catch (DatabaseException de) {
										// Don't stop execution on these, chances are they're failing as already run. 
										//	if(schemaErrors.isEmpty()) {
										//		schemaErrors = new ArrayList<String>();
										//	}
										//String err = String.format("%1$s - [%2$s] - %3$s", next.table, update, de.getMessage()); 
										//schemaErrors.add(err);
										// If we don't do this, it will run it again and again
										SchemaManager.logUpdate("Update Table Failed", next.table, update.u, hash, update.d);
										logger.error("Error executing update for table " + next.table, de);
									}
								}
							}
							break; 
						case Error:
							logger.debug("Error producing schema update for table={}, error={}", next.table, next.sql);
							break; 
					}					
				}
			}
			logger.info("Database Sync Complete");
			
		} catch (InterruptedException ie) {
			logger.error("Error producing db updates", ie); 
		} finally {
			exec.setCloseConnection(true);
			exec.close(); 
			
			worker.setCloseConnection(true);
			worker.close();
		}
		return schemaErrors;
	}

	private static Date getLastTableUpdate(String table, Connection con) {
		Date lastUpdate = null;
        try {
        	GenRow last = new GenRow();
        	last.setConnection(con);
        	last.setViewSpec("ApplicationLogView");
        	last.setParameter("-select1","CreatedDate"); 
        	last.setParameter("TableRef", table);
        	last.setParameter("DateRef", "!NULL");
        	last.setParameter("-sort0","DateRef"); 
        	last.setParameter("-order0","DESC"); 
        	last.setParameter("-limit0","1"); 
        	last.doAction(GenerationKeys.SELECTFIRST);	// if the table does not exist, this will fail. 
        	logger.debug(last.getStatement());
        	lastUpdate = last.isSuccessful() ? last.getDate("CreatedDate") : null; 
        } catch(DatabaseException de) {
        	logger.info("Error Retrieving Last update for table : " + table + " - " + de.getMessage());
        }
        return lastUpdate; 
	}
}
