package com.sok.framework.sql;
import com.sok.framework.*;
import com.sok.framework.generation.util.*;
import com.sok.framework.generation.MapData;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.nodes.sql.*;

import java.sql.Types;
import java.util.*;


public class FM8 extends AbstractDatabase
{
	static String limit = "limit";
	static char space = ' ';	

	public static String uk_dateformat = "'%d/%m/%Y'";
    public static String us_dateformat = "'%d/%m/%Y'";    
	public static String uk_datetimeformat = "'%d/%m/%Y %H:%i:%s'";	
    public static String us_datetimeformat = "'%d/%m/%Y %H:%i:%s'";     
    public static String odbc_datetimeformat = "'%Y-%m-%d %H:%i:%s'";
    public static String odbc_dateformat = "'%Y-%m-%d'";
    
	public FM8()
	{
		super();
	}	
	
    public void generate(RootNode root, StringBuffer out)
    {
        StatementNode statement = root.getStatementNode();
        StatementHeadNode statementhead = statement.getStatementHeadNode();
        if(statementhead!=null)
        {
            statementhead.generate(root, null, out);
            
            WhereSetNode wheresetnode = statement.getWhereSetNode();
            if(wheresetnode!=null)
            {
                wheresetnode.generate(root, out);
            }
            String vscript = statement.getScript();
            if(vscript!=null)
            {
                root.getDatabase().appendConditions(vscript, out);
            }              
            GroupBySetNode groupbysetnode = statement.getGroupBySetNode();
            if(groupbysetnode!=null)
            {
                groupbysetnode.generate(root, out);
            }
            HavingSetNode havingsetnode = statement.getHavingSetNode();
            if(havingsetnode!=null)
            {
                havingsetnode.generate(root, out);
            }              
            OrderBySetNode orderbysetnode = statement.getOrderBySetNode();
            if(orderbysetnode!=null)
            {
                orderbysetnode.generate(root, out);
            }
           
            LimitNode limitnode = statement.getLimitNode();
            if(limitnode!=null)
            {
                limitnode.generate(root, out);
            }  
        }
    }    
    
    public void appendTopStatementAtStart(StringBuffer stmtbuffer, int top)
    {

    }
    
    public void appendTopStatementAtEnd(StringBuffer stmtbuffer, int top)
    {
        appendTopStatement(stmtbuffer, top);
    }
    
	public void appendTopStatement(StringBuffer stmtbuffer, int top)
	{
		if(top>0)
		{
			stmtbuffer.append(space);
			stmtbuffer.append(limit);
			stmtbuffer.append(space);
			stmtbuffer.append(String.valueOf(top));
			stmtbuffer.append(space);
		}
	}	
	
    protected void appendFieldOpStringValue(StringBuffer searchstmtbuffer, String fieldname,
            String value, int fieldtype, MapData row)
    {
        String fieldvalue = value;
        int op = getOp(fieldvalue);


        if(fieldvalue.equals(ActionBean.NULL))
        {
            searchstmtbuffer.append(fieldname);
            fieldvalue = appendOp(searchstmtbuffer, fieldname, fieldvalue, fieldtype, op);            
            searchstmtbuffer.append(ActionBean.NULL);
        }
        else if(fieldvalue.equals(ActionBean.EMPTY))
        {
            searchstmtbuffer.append(fieldname);
            fieldvalue = appendOp(searchstmtbuffer, fieldname, fieldvalue, fieldtype, op);              
            searchstmtbuffer.append("''");
        }
        else if(fieldvalue.equals(ActionBean.CURRENTUSER))
        {
            searchstmtbuffer.append(fieldname);
            fieldvalue = appendOp(searchstmtbuffer, fieldname, fieldvalue, fieldtype, op);              
            searchstmtbuffer.append('\'');
            searchstmtbuffer.append(row.getString(ActionBean.CURRENTUSERID));
            searchstmtbuffer.append('\'');
        }
        else
        {
            if(fieldtype == Types.VARCHAR)
            {
                searchstmtbuffer.append("UCASE(");
                searchstmtbuffer.append(fieldname);
                searchstmtbuffer.append(")");
            }
            else
            {
                searchstmtbuffer.append(fieldname);
            }
            fieldvalue = appendOp(searchstmtbuffer, fieldname, fieldvalue, fieldtype, op);              
            int ai = fieldvalue.indexOf("'");
            if(ai >= 0)
            {
                if(fieldvalue.length() == ai + 1 || fieldvalue.charAt(ai + 1) != '\'')
                {
                    fieldvalue = ActionBean.escapeQuote(fieldvalue);
                }
            }
            if(fieldtype == Types.VARCHAR)
            {
                searchstmtbuffer.append("UCASE(");
                searchstmtbuffer.append("'");
                searchstmtbuffer.append(fieldvalue);
                searchstmtbuffer.append("'");
                searchstmtbuffer.append(")");
            }
            else
            {
                searchstmtbuffer.append("'");
                searchstmtbuffer.append(fieldvalue);
                searchstmtbuffer.append("'");
            }            
        }
    }    
    
	public void generateSQLStatement(TableData row)
	{
		TableSpec tspec = row.getTableSpec();
		ViewSpec vspec = row.getViewSpec();
		//System.out.println("generating ");
		StringBuffer searchstmtbuffer = new StringBuffer();
		StringBuffer stmtbuffer = new StringBuffer();
		stmtbuffer.append("select ");

		appendDisplayfields(stmtbuffer,row);
		//System.out.println("appendDisplayfields");
		stmtbuffer.append(RN);
		stmtbuffer.append(" from ");
		appendDisplayTables(stmtbuffer,row);
		//System.out.println("appendDisplayTables");
		
		boolean added = false;
		
		int fieldtype = 0;
	
		String fieldname = null;
		String temp = null;
		String fullfieldname = null;
		StringBuffer tempfieldname;
		for(int i=0; i<tspec.getFieldsLength(); i++)
		{
			tempfieldname = new StringBuffer();
			fieldname = tspec.getFieldName(i);
			//System.out.println("fieldname "+fieldname);
            appendWithQuote(tempfieldname, tspec.getTableName());
			tempfieldname.append(_dot);
            appendWithQuote(tempfieldname, fieldname);
			fullfieldname = tempfieldname.toString();
			fieldtype = tspec.getFieldType(i);
			temp = (String)row.get(fieldname);

			if(temp != null && temp.length()!=0)
			{
				if(added)
				{
					searchstmtbuffer.append(_and);
				}
				//System.out.println("adding "+ fullfieldname);
				appendField(searchstmtbuffer,fullfieldname,temp,fieldtype, row);
				added = true;
			}	
		}
		
		Map groups = row.getSearchGroups();
		if(groups!=null)
		{
			Map group = null;
			Iterator groupkeys = groups.keySet().iterator();

			String groupkey = null;

			while(groupkeys.hasNext())
			{	
				groupkey = (String)groupkeys.next();
				if(row.containsKey(groupkey))
				{
					group = (Map)groups.get(groupkey);

					if(added)
					{
						searchstmtbuffer.append(_and);
					}
					appendSearchGroups(searchstmtbuffer, group, groupkey, tspec, row, null);
					added = true;
				}
			}
		}
		
		for(int j=0; j<vspec.getRelatedCountsLength(); j++)
		{
			tempfieldname = new StringBuffer();
			fieldname = vspec.getRelatedCountItemName(j);
			temp = (String)row.get(fieldname);
			if(temp != null && temp.length()!=0)
			{
				if(added)
				{
					searchstmtbuffer.append(_and);
				}
				appendRelatedCount(tempfieldname, j, row);
				appendField(searchstmtbuffer,tempfieldname.toString(),temp,Types.INTEGER, row);
				added=true;
			}	
		}
        
        for(int j=0; j<vspec.getRelatedSumsLength(); j++)
        {
            tempfieldname = new StringBuffer();
            fieldname = vspec.getRelatedSumItemName(j);
            temp = (String)row.get(fieldname);
            if(temp != null && temp.length()!=0)
            {
                if(added)
                {
                    searchstmtbuffer.append(_and);
                }
                appendRelatedSum(tempfieldname, j, row);
                appendField(searchstmtbuffer,tempfieldname.toString(),temp,Types.FLOAT, row);
                added=true;
            }   
        }           
        
		// <NBCHANGE - 020503 - 1>
		String vCondition = vspec.getCondition();
		if(null != vCondition) {
			if(added){
				searchstmtbuffer.append(_and);
			}
			searchstmtbuffer.append(getSubstituted(vCondition,row));
		}
		// </NBCHANGE - 020503 - 1>

		added = appendRelatedSearch(added,searchstmtbuffer,row);
		
		if(searchstmtbuffer.length()!=0)
		{
			stmtbuffer.append(RN);
			stmtbuffer.append(" where ");
		}
		stmtbuffer.append(searchstmtbuffer);
		
		appendSortOrder(stmtbuffer, row);
		
		appendTopStatement(stmtbuffer, row.getTop());
		
		row.setSearchCriteria(searchstmtbuffer.toString());
		row.setSearchStatement(stmtbuffer.toString());
	}

	public String getCurrentDateMethod()
	{
		return("NOW()");
	}

	public String getNewIDMethod()
	{
		return("UUID()");	
	}	
	
	public void appendCurrentDateString(StringBuffer searchstmtbuffer)
	{
		appendDateToStrStmt(searchstmtbuffer, getCurrentDateMethod());
	}

	protected String getDateConvertType(String type)
	{
        if(type!=null)
        {
            if(type.equals(format_us))
            {
                return (us_dateformat); 
            }
            else if(type.equals(format_odbc))
            {
                return (odbc_dateformat);
            }
        }
		return (uk_dateformat);
	}		

    protected String getDateTimeConvertType(String type)
    {
        if(type!=null)
        {        
            if(type.equals(format_us))
            {
                return (us_datetimeformat);
            }
            else if(type.equals(format_odbc))
            {
                return (odbc_datetimeformat);
            }
        }
        return (uk_datetimeformat);
    }    
	
	public void appendBeginOfToday(StringBuffer searchstmtbuffer)
	{
		searchstmtbuffer.append("DATE()");
	}	
	
	public void appendStrToIntStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		searchstmtbuffer.append("INT(");
		searchstmtbuffer.append(fieldname);
		searchstmtbuffer.append(")");		
	}
	
	public void appendStrToDateStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
	{
        String format = this.getDateConvertType(formattype);
        appendStrToDateStmtWithFormat(searchstmtbuffer, fieldname, format);  
	}
    
    public void appendStrToDatetimeStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
    {
        String format = this.getDateTimeConvertType(formattype);
        appendStrToDateStmtWithFormat(searchstmtbuffer, fieldname, format);  
    }    
	
    public void appendStrToDateStmtWithFormat(StringBuffer searchstmtbuffer, String fieldname, String format)
    {
        searchstmtbuffer.append("DATEVAL(");
        searchstmtbuffer.append(fieldname);
        searchstmtbuffer.append(")");   
    }    
    
	public void appendDateToStrStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
	{
        String format = this.getDateConvertType(formattype);
        appendDateToStrStmtWithFormat(searchstmtbuffer, fieldname, format);
	}

    public void appendDatetimeToStrStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
    {
        String format = this.getDateTimeConvertType(formattype);
        appendDateToStrStmtWithFormat(searchstmtbuffer, fieldname, format);
    }    
    
    public void appendDateToStrStmtWithFormat(StringBuffer searchstmtbuffer, String fieldname, String format)
    {
        searchstmtbuffer.append("STRVAL(");
        searchstmtbuffer.append(fieldname);
        //searchstmtbuffer.append(", ");
        //searchstmtbuffer.append(format);
        searchstmtbuffer.append(")");
    }
    
	public void appendDateToStrStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendDateToStrStmt(searchstmtbuffer, fieldname, uk_dateformat);
	}		
	
	public void appendStrToDateStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendStrToDateStmt(searchstmtbuffer, fieldname, uk_dateformat);
	}

	public void appendDatetimeToStrStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendDatetimeToStrStmt(searchstmtbuffer, fieldname, uk_datetimeformat);
	}	
	
	public void appendStrToDatetimeStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendStrToDatetimeStmt(searchstmtbuffer, fieldname, uk_datetimeformat);
	}	

    public void appendDateRangeStmt(StringBuffer searchstmtbuffer, String fieldname, String gtvalue, String ltvalue)
    {
        appendDateRangeStmt(searchstmtbuffer, fieldname, gtvalue, ltvalue, uk_datetimeformat, false);
    }
    
	public void appendDateRangeStmt(StringBuffer searchstmtbuffer, String fieldname, String gtvalue, String ltvalue, String formattype, boolean omit)
	{
        //String format = this.getDateTimeConvertType(formattype);
		searchstmtbuffer.append(fieldname);
		searchstmtbuffer.append(" >= ");
		searchstmtbuffer.append("DATEVAL(");
		searchstmtbuffer.append("'");
		searchstmtbuffer.append(gtvalue);
        if(gtvalue.length()<11)
        {
            searchstmtbuffer.append(daystart); 
        } 
		searchstmtbuffer.append("'");
		//searchstmtbuffer.append(", ");
       //searchstmtbuffer.append(format);
		searchstmtbuffer.append(")");
		searchstmtbuffer.append(" and ");
		searchstmtbuffer.append(fieldname);
		searchstmtbuffer.append(" <= ");
		searchstmtbuffer.append("DATEVAL(");
		searchstmtbuffer.append("'");
		searchstmtbuffer.append(ltvalue);
        if(ltvalue.length()<11)
        {          
            searchstmtbuffer.append(dayend);
        }
		searchstmtbuffer.append("'");
		//searchstmtbuffer.append(", ");
        //searchstmtbuffer.append(format);
		searchstmtbuffer.append(")");
	}	
	
	public  String getContatenateString(String[] params) {
	   StringBuffer buffer = new StringBuffer();
	   
	   if (params !=null && params.length > 0) {
	      if (params.length == 1) {
   	      buffer.append(params[0]);
	      }
	      else {
	         buffer.append("CONCAT(");
   	      buffer.append(params[0]);

      	   for (int i=1; i < params.length; i++) {
      	      buffer.append(", ");
   	         buffer.append(params[i]);
      	   }
      	   buffer.append(")");
   	   }
	   }
	   return buffer.toString();
	}
	
    public void appendSubstring(StringBuffer buffer, String value, String start, String end)
    {
        buffer.append("SUBSTR(");
        buffer.append(value);
        buffer.append(", ");
        buffer.append(start);
        buffer.append(", ");
        buffer.append(end);        
        buffer.append(")");
    }    
    
    public void appendStringLength(StringBuffer buffer, String expression)
    {
        buffer.append("LEN(");
        buffer.append(expression);
        buffer.append(")");
    }
    
    public void appendTrim(StringBuffer buffer, String expression)
    {
        buffer.append("TRIM(");
        buffer.append(expression);
        buffer.append(")");
    }
    
    public void appendIndexOf(StringBuffer buffer, String value, String search)
    {
        //not supported by fm?
        buffer.append("INSTR(");
        buffer.append(value);
        buffer.append(", ");
        buffer.append(search);
        buffer.append(")");
    }  
    
	public void appendTodayStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append("DATEVAL(");
		buffer.append(field);
		buffer.append(") ");
		if(not)
		{
			buffer.append("<>");
		}
		else
		{
			buffer.append("=");
		}		
		buffer.append(" DATE()");
	}

	public void appendOverdueStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" >= '");
		}
		else
		{
			buffer.append(" < '");
		}			
		buffer.append(getDayEndString(CalendarFactory.getYesterdayCal()));
		buffer.append("'");
	}

	public void appendThisWeekStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" < ");
		}
		else
		{
			buffer.append(" >= ");
		}		
		buffer.append("DATE()");
		if(not)
		{
			buffer.append(" or ");
		}
		else
		{
			buffer.append(" and ");
		}		
		buffer.append(field);
		if(not)
		{
			buffer.append(" > '");
		}
		else
		{
			buffer.append(" <= '");
		}			
		buffer.append(getDayEndString(CalendarFactory.getEndOfWeekCal()));
		buffer.append("'");
	}

	public void appendThisMonthStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" < ");
		}
		else
		{
			buffer.append(" >= ");
		}		
		buffer.append("DATE()");
		if(not)
		{
			buffer.append(" or ");
		}
		else
		{
			buffer.append(" and ");
		}		
		buffer.append(field);
		if(not)
		{
			buffer.append(" > '");
		}
		else
		{
			buffer.append(" <= '");
		}			
		buffer.append(getDayEndString(CalendarFactory.getEndOfMonthCal()));
		buffer.append("'");
	}
    
	public void appendRangeStmt(StringBuffer buffer, String field,
            Calendar date1, Calendar date2, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" < '");
		}
		else
		{
			buffer.append(" >= '");
		}			
		buffer.append(getDayStartString(date1));
		buffer.append("'");
		if(not)
		{
			buffer.append(" or ");
		}
		else
		{
			buffer.append(" and ");
		}		
		buffer.append(field);
		if(not)
		{
			buffer.append(" > '");
		}
		else
		{
			buffer.append(" <= '");
		}			
		buffer.append(getDayEndString(date2));
		buffer.append("'");
	}

   public void appendDateTimeRangeStmt(StringBuffer buffer, String field,
         Calendar date1, Calendar date2, boolean not)
   {
      buffer.append(field);
      if(not)
      {
         buffer.append(" < '");
      }
      else
      {
         buffer.append(" >= '");
      }        
      buffer.append(getDateTimeString(date1));
      buffer.append("'");
      if(not)
      {
         buffer.append(" or ");
      }
      else
      {
         buffer.append(" and ");
      }     
      buffer.append(field);
      if(not)
      {
         buffer.append(" > '");
      }
      else
      {
         buffer.append(" <= '");
      }        
      buffer.append(getDateTimeString(date2));
      buffer.append("'");
   }	
	
    public void appendHourStmt(StringBuffer buffer, String field, String num,  boolean not)
    {
        appendDatePartHour(buffer, field);
        if(not)
        {
            buffer.append(" <> ");
        }
        else
        {
            buffer.append(" = ");
        }
        buffer.append(num); 
    }    
    
	public void appendDateAddStmt(StringBuffer buffer, String interval, String amount, String datetime)
	{
		buffer.append("DATE_ADD(");
		buffer.append(datetime);
		buffer.append(", INTERVAL ");
		buffer.append(amount);
		buffer.append(' ');
		buffer.append(interval);
		buffer.append(')');
	}	

	public void appendDateDiffStmt(StringBuffer buffer, String interval, String datetime1, String datetime2)
	{
		buffer.append("CAST(TIME_TO_SEC(TIMEDIFF(");
		buffer.append(datetime2);
		buffer.append(',');
		buffer.append(datetime1);
		buffer.append("))");
		if(interval.equalsIgnoreCase("MINUTE"))
		{
			buffer.append(" / 60");
		}
		else if(interval.equalsIgnoreCase("HOUR"))
		{
			buffer.append(" / 60 / 60");
		}
		else if(interval.equalsIgnoreCase("DAY"))
		{
			buffer.append(" / 60 / 60 / 24");
		}
		else if(interval.equalsIgnoreCase("WEEK"))
		{
			buffer.append(" / 60 / 60 / 24 / 7");
		}
		buffer.append(" AS SIGNED)");
	}	
	
	public void appendQualifiedTableName(StringBuffer buffer, String servername, String databasename, String tablename, String asname)
	{
		if(databasename!=null && databasename.length()!=0)
		{
            appendWithQuote(buffer, databasename);
			buffer.append(_dot);			
		}
        appendWithQuote(buffer, tablename);

		if(asname!=null && asname.length()!=0)
		{
			buffer.append(_as);
            appendWithQuote(buffer, asname);
		}
        
	}
    
    public void appendDatePartHour(StringBuffer buffer, String field)
    {
    	//not supported
    }
    
    public void appendDatePartYearMonth(StringBuffer buffer, String field)
    {
    	//not supported
    }    
    
    public void appendWithQuote(StringBuffer buffer, String name)
    {
        buffer.append("\"");
        buffer.append(name);
        buffer.append("\"");
    }
    
    public void appendDatePartDayOfWeek(StringBuffer buffer, String field)
    {
    	//not supported
    } 
    
    public String getDayOfWeekNum(String day)
    {
    	//not supported
        return(null);
    }  
}