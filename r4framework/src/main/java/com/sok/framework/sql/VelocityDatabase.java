package com.sok.framework.sql;

import com.sok.framework.*;

public class VelocityDatabase
{
	SqlDatabase database;
	static final char quote = '\'';
	
	public static final String DATABASE = "database";
	
	public VelocityDatabase(SqlDatabase database)
	{
		this.database = database;
	}
	
	public String getStrToIntStmt(String fieldname)
	{
		StringBuffer temp = new StringBuffer();
		database.appendStrToIntStmt(temp, fieldname);
		return(temp.toString());
	}
	
	public String getDateToStrStmt(String fieldname)
	{	
		StringBuffer temp = new StringBuffer();
		database.appendDateToStrStmt(temp, fieldname);
		return(temp.toString());
	}
	
	public String getStrToDateStmt(String fieldname)
	{
		StringBuffer value = new StringBuffer();
		value.append(quote);
		value.append(fieldname);
		value.append(quote);			
		StringBuffer temp = new StringBuffer();	
		database.appendStrToDateStmt(temp, value.toString());
		return(temp.toString());
	}

	public String getDateAddStmt(String interval, String amount, String datetime)
	{
		StringBuffer temp = new StringBuffer();
		database.appendDateAddStmt(temp, interval, amount, datetime);
		return(temp.toString());
	}	

	public String getDateAddFromNowStmt(String interval, String amount)
	{
		StringBuffer temp = new StringBuffer();
		database.appendDateAddStmt(temp, interval, amount, database.getCurrentDateMethod());
		return(temp.toString());
	}	
		
	public String getDateDiffStmt(String interval, String datetime1, String datetime2)
	{
		StringBuffer temp = new StringBuffer();
		database.appendDateDiffStmt(temp, interval, datetime1, datetime2);
		return(temp.toString());
	}

	public String getStrToTomorrowDateStmt(String fieldname)
	{
		return(getDateAddStmt("DAY", "1", getStrToDateStmt(fieldname)));
	}	
	
    public String getDatePartHourStmt(String fieldname)
    {
        StringBuffer temp = new StringBuffer();
        database.appendDatePartHour(temp, fieldname);
        return(temp.toString());
    }
    
    public String getDatePartYearMonthStmt(String fieldname)
    {
        StringBuffer temp = new StringBuffer();
        database.appendDatePartYearMonth(temp, fieldname);
        return(temp.toString());
    }    
    
    public String getDatePartDayOfWeekStmt(String fieldname)
    {
        StringBuffer temp = new StringBuffer();
        database.appendDatePartDayOfWeek(temp, fieldname);
        return(temp.toString());
    }        
    
	public String getContatenateString(String param1, String param2) {
	   return database.getContatenateString(param1, param2);
	}
	
	public String getContatenateString(String param1, String param2, String param3) {
	   return database.getContatenateString(param1, param2, param3);
	}
	
	public String getContatenateString(String[] params) {
	   return database.getContatenateString(params);
	}
	
	public String getDayOfWeekNum(String day){
		   return database.getDayOfWeekNum(day);
	}
	
	public String escapeSQLQuote(String s)
	{
		return(ActionBean.escapeQuote(s));
	}
	
    public String getIndexOf(String value, String search)
    {
        StringBuffer temp = new StringBuffer();
        database.appendIndexOf(temp, value, search);
        return(temp.toString()); 
    }
    
    public String getStringLength(String expression)
    {
        StringBuffer temp = new StringBuffer();
        database.appendStringLength(temp, expression);
        return(temp.toString());
    }
    
    public void appendTrim(StringBuffer buffer, String expression)
    {
        buffer.append("TRIM(");
        buffer.append(expression);
        buffer.append(")");
    }
    
    public String getSubstring(String value, String start, String end)
    {
        StringBuffer temp = new StringBuffer();
        database.appendSubstring(temp, value, start, end);
        return(temp.toString());
    }
    
	public String getFirstFromSplit(String value, String delimiter)
    {
	    return(getSubstring(value, "1", getIndexOf(value, delimiter)+" - 1"));
    }
    
    public String getLastFromSplit(String value, String delimiter)
    {
        return(getSubstring(value, getIndexOf(value, delimiter)+" + 1", getStringLength(value)));
    }  
    
    public String getTopStmtAtStart(String value)
    {
        StringBuffer temp = new StringBuffer();
        database.appendTopStatementAtStart(temp, Integer.parseInt(value));
        return(temp.toString());
    }    
    
    public String getTopStmtAtEnd(String value)
    {
        StringBuffer temp = new StringBuffer();
        database.appendTopStatementAtEnd(temp, Integer.parseInt(value));
        return(temp.toString());
    }     
    
    public String getCurrentDateMethod()
    {
       return(database.getCurrentDateMethod());
    }
    
    public String getBeginOfToday()
    {
       StringBuffer temp = new StringBuffer();
       database.appendBeginOfToday(temp);
       return(temp.toString());
    }
    
    public String urlDecode(String s)
    {
    	return(StringUtil.urlDecode(s));
    }
    
    public String urlEncode(String s)
    {
    	return(StringUtil.urlEncode(s));
    }    
}