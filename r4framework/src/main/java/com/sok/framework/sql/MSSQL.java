package com.sok.framework.sql;
import com.sok.framework.*;
import com.sok.framework.generation.util.*;
import com.sok.framework.generation.nodes.RootNode;
import com.sok.framework.generation.nodes.sql.*;

import java.sql.Types;
import java.util.*;


public class MSSQL extends AbstractDatabase
{
	public static final String usdateconvert = "101";
	public static final String ukdateconvert = "103";
    public static final String odbcdateconvert = "120";	
    
	public MSSQL()
	{
		super();
	}

    public void appendTopStatementAtStart(StringBuffer stmtbuffer, int top)
    {
        appendTopStatement(stmtbuffer, top);
    }
    
    public void appendTopStatementAtEnd(StringBuffer stmtbuffer, int top)
    {

    }    
    
	public void appendTopStatement(StringBuffer stmtbuffer, int top)
	{
		if(top>0)
		{
			stmtbuffer.append("top ");
			stmtbuffer.append(String.valueOf(top));
			stmtbuffer.append(" ");
		}
	}	
	
    public void generate(RootNode root, StringBuffer out)
    {
        StatementNode statement = root.getStatementNode();
        StatementHeadNode statementhead = statement.getStatementHeadNode();
        if(statementhead!=null)
        {
            LimitNode limtnode = statement.getLimitNode();            
            statementhead.generate(root, limtnode, out);

            WhereSetNode wheresetnode = statement.getWhereSetNode();
            if(wheresetnode!=null)
            {
                wheresetnode.generate(root, out);
            }
            String vscript = statement.getScript();
            if(vscript!=null)
            {
                root.getDatabase().appendConditions(vscript, out);
            }              
            GroupBySetNode groupbysetnode = statement.getGroupBySetNode();
            if(groupbysetnode!=null)
            {
                groupbysetnode.generate(root, out);
            }   
            HavingSetNode havingsetnode = statement.getHavingSetNode();
            if(havingsetnode!=null)
            {
                havingsetnode.generate(root, out);
            }              
            OrderBySetNode orderbysetnode = statement.getOrderBySetNode();
            if(orderbysetnode!=null)
            {
                orderbysetnode.generate(root, out);
            }            
        }
    }     
    
	public void generateSQLStatement(TableData row)
	{
		TableSpec tspec = row.getTableSpec();
		ViewSpec vspec = row.getViewSpec();
		//System.out.println("generating ");
		StringBuffer searchstmtbuffer = new StringBuffer();
		StringBuffer stmtbuffer = new StringBuffer();

		stmtbuffer.append("select ");
		appendTopStatement(stmtbuffer, row.getTop());
		appendDisplayfields(stmtbuffer,row);
		//System.out.println("appendDisplayfields");
		stmtbuffer.append(RN);
		stmtbuffer.append(" from ");
		appendDisplayTables(stmtbuffer,row);
		//System.out.println("appendDisplayTables");
		
		boolean added = false;

		int fieldtype = 0;
	
		String fieldname = null;
		String temp = null;
		String fullfieldname = null;
		StringBuffer tempfieldname;
		for(int i=0; i<tspec.getFieldsLength(); i++)
		{
			tempfieldname = new StringBuffer();
			fieldname = tspec.getFieldName(i);
			//System.out.println("fieldname "+fieldname);
			tempfieldname.append(tspec.getTableName());
			tempfieldname.append(_dot);
			tempfieldname.append(fieldname);
			fullfieldname = tempfieldname.toString();
			fieldtype = tspec.getFieldType(i);
			temp = (String)row.get(fieldname);

			if(temp != null && temp.length()!=0)
			{
				if(added)
				{
					searchstmtbuffer.append(_and);
				}
				//System.out.println("adding "+ fullfieldname);
				appendField(searchstmtbuffer,fullfieldname,temp,fieldtype, row);
				added = true;
			}	
		}
		
		Map groups = row.getSearchGroups();
		if(groups!=null)
		{
			Map group = null;
			Iterator groupkeys = groups.keySet().iterator();

			String groupkey = null;

			while(groupkeys.hasNext())
			{	
				groupkey = (String)groupkeys.next();
				if(row.containsKey(groupkey))
				{
					group = (Map)groups.get(groupkey);

					if(added)
					{
						searchstmtbuffer.append(_and);
					}
					appendSearchGroups(searchstmtbuffer, group, groupkey, tspec, row, null);
					added = true;
				}
			}
		}
		
		for(int j=0; j<vspec.getRelatedCountsLength(); j++)
		{
			tempfieldname = new StringBuffer();
			fieldname = vspec.getRelatedCountItemName(j);
			temp = (String)row.get(fieldname);
			if(temp != null && temp.length()!=0)
			{
				if(added)
				{
					searchstmtbuffer.append(_and);
				}
				appendRelatedCount(tempfieldname, j, row);
				appendField(searchstmtbuffer,tempfieldname.toString(),temp,Types.INTEGER, row);
				added=true;
			}	
		}
        
        for(int j=0; j<vspec.getRelatedSumsLength(); j++)
        {
            tempfieldname = new StringBuffer();
            fieldname = vspec.getRelatedSumItemName(j);
            temp = (String)row.get(fieldname);
            if(temp != null && temp.length()!=0)
            {
                if(added)
                {
                    searchstmtbuffer.append(_and);
                }
                appendRelatedSum(tempfieldname, j, row);
                appendField(searchstmtbuffer,tempfieldname.toString(),temp,Types.FLOAT, row);
                added=true;
            }   
        }        
		// <NBCHANGE - 020503 - 1>
		String vCondition = vspec.getCondition();
		if(null != vCondition) {
			if(added){
				searchstmtbuffer.append(_and);
			}
			searchstmtbuffer.append(getSubstituted(vCondition,row));
		}
		// </NBCHANGE - 020503 - 1>

		added = appendRelatedSearch(added,searchstmtbuffer,row);
		
		if(searchstmtbuffer.length()!=0)
		{
			stmtbuffer.append(RN);
			stmtbuffer.append(" where ");
		}
		stmtbuffer.append(searchstmtbuffer);
		
		appendSortOrder(stmtbuffer, row);
		
		row.setSearchCriteria(searchstmtbuffer.toString());
		row.setSearchStatement(stmtbuffer.toString());
	}

	public void appendBeginOfToday(StringBuffer searchstmtbuffer)
	{
		appendStrToDateStmt(searchstmtbuffer, new StringBuffer("'").append(CalendarFactory.getCurrentDate("dd/MM/yyyy")).append("'").toString());		
	}

	public void appendStrToIntStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		searchstmtbuffer.append("convert(int, ");
		searchstmtbuffer.append(fieldname);
		searchstmtbuffer.append(")");		
	}	
	
    public void appendStrToDateStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
    {
        String format = this.getDateConvertType(formattype);
        appendStrToDateStmtWithFormat(searchstmtbuffer, fieldname, format);  
    }
    
    public void appendStrToDatetimeStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
    {
        String format = this.getDateTimeConvertType(formattype);
        appendStrToDateStmtWithFormat(searchstmtbuffer, fieldname, format);  
    }     

	public void appendStrToDateStmtWithFormat(StringBuffer searchstmtbuffer, String fieldname, String format)
	{
		searchstmtbuffer.append("CONVERT(datetime, ");
		searchstmtbuffer.append(fieldname);
		searchstmtbuffer.append(", ");
		searchstmtbuffer.append(format);
		searchstmtbuffer.append(")");
	}
	
    public void appendDateToStrStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
    {
        String format = this.getDateConvertType(formattype);
        appendDateToStrStmtWithFormat(searchstmtbuffer, fieldname, format);
    }

    public void appendDatetimeToStrStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
    {
        String format = this.getDateTimeConvertType(formattype);
        appendDateToStrStmtWithFormat(searchstmtbuffer, fieldname, format);
    }     
    
    public void appendDateToStrStmtWithFormat(StringBuffer searchstmtbuffer, String fieldname, String format)
    {
        searchstmtbuffer.append("CONVERT(char, ");
        searchstmtbuffer.append(fieldname);
        searchstmtbuffer.append(", ");
        searchstmtbuffer.append(format);
        searchstmtbuffer.append(")");
    }    
    
	public void appendDateToStrStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendDateToStrStmt(searchstmtbuffer, fieldname, ukdateconvert);
	}
	
	public void appendStrToDateStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendStrToDateStmt(searchstmtbuffer, fieldname, ukdateconvert);
	}	

	public void appendDatetimeToStrStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendDatetimeToStrStmt(searchstmtbuffer, fieldname, ukdateconvert);
	}
	
	public void appendStrToDatetimeStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendStrToDatetimeStmt(searchstmtbuffer, fieldname, ukdateconvert);
	}	

    public void appendDateRangeStmt(StringBuffer searchstmtbuffer, String fieldname,
            String gtvalue, String ltvalue)
    {
        appendDateRangeStmt(searchstmtbuffer, fieldname, gtvalue, ltvalue, ukdateconvert, false);
    }
    
	public void appendDateRangeStmt(StringBuffer searchstmtbuffer, String fieldname,
	        String gtvalue, String ltvalue, String formattype, boolean omit)
	{   
        String format = this.getDateConvertType(formattype);
		searchstmtbuffer.append(fieldname);
        if(omit)
        {
            searchstmtbuffer.append(" < ");
        }
        else
        {
            searchstmtbuffer.append(" >= ");
        }
		searchstmtbuffer.append("CONVERT(datetime, ");
		searchstmtbuffer.append("'");
		searchstmtbuffer.append(gtvalue);
        if(gtvalue.length()<11)
        {
            searchstmtbuffer.append(daystart); 
        }        
		searchstmtbuffer.append("'");
		searchstmtbuffer.append(", ");
		searchstmtbuffer.append(format);
		searchstmtbuffer.append(")");
		searchstmtbuffer.append(" and ");
		searchstmtbuffer.append(fieldname);
        if(omit)
        {
            searchstmtbuffer.append(" > ");
        }
        else
        {
            searchstmtbuffer.append(" <= ");
        }        
		searchstmtbuffer.append("CONVERT(datetime, ");
		searchstmtbuffer.append("'");      
		searchstmtbuffer.append(ltvalue);
        if(ltvalue.length()<11)
        {          
            searchstmtbuffer.append(dayend);
        }
		searchstmtbuffer.append("'");
		searchstmtbuffer.append(", ");
		searchstmtbuffer.append(format);
		searchstmtbuffer.append(")");
	}

	public String getCurrentDateMethod()
	{
		return("getdate()");
	}	

	public String getNewIDMethod()
	{
		return("newid()");	
	}
	
	public void appendCurrentDateString(StringBuffer searchstmtbuffer)
	{
		appendDateToStrStmt(searchstmtbuffer, getCurrentDateMethod());
	}	
	
    protected String getDateConvertType(String type)
    {
        if(type!=null)
        {
            if(type.equals(format_us))
            {
                return (usdateconvert); 
            }
            else if(type.equals(format_odbc))
            {
                return (odbcdateconvert);
            }
        }
        return (ukdateconvert);
    }       

    protected String getDateTimeConvertType(String type)
    {
        return (getDateConvertType(type));
    }     
    
	public  String getContatenateString(String[] params) {
	   StringBuffer buffer = new StringBuffer();
	   
	   if (params !=null && params.length > 0) {
	      buffer.append(params[0]);
   	   for (int i=1; i < params.length; i++) {
   	      buffer.append(" + ");
	         buffer.append(params[i]);
   	   }
	   }
	   return buffer.toString();
	}
	
    public void appendSubstring(StringBuffer buffer, String value, String start, String end)
    {
        buffer.append("SUBSTRING(");
        buffer.append(value);
        buffer.append(", ");
        buffer.append(start);
        buffer.append(", ");
        buffer.append(end);        
        buffer.append(")");
    }    
    
    public void appendStringLength(StringBuffer buffer, String expression)
    {
        buffer.append("LEN(");
        buffer.append(expression);
        buffer.append(")");
    }
    
    public void appendTrim(StringBuffer buffer, String expression)
    {
        buffer.append("TRIM(");
        buffer.append(expression);
        buffer.append(")");
    }
    
    public void appendIndexOf(StringBuffer buffer, String value, String search)
    {
        buffer.append("CHARINDEX(");
        buffer.append(search);
        buffer.append(", ");
        buffer.append(value);
        buffer.append(")");
    }    
    
	public void appendTodayStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append("convert(varchar, ");
		buffer.append(field);
		buffer.append(", 103) ");
		if(not)
		{
			buffer.append("<>");
		}
		else
		{
			buffer.append("=");
		}
		buffer.append(" convert(varchar, getdate(), 103)");
	}

	public void appendOverdueStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" >=");
		}
		else
		{
			buffer.append(" <");
		}		
		buffer.append(" convert(datetime, '");
		buffer.append(getDayEndString(CalendarFactory.getYesterdayCal()));
		buffer.append("', 120)");
	}

	public void appendThisWeekStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" < ");
		}
		else
		{
			buffer.append(" >= ");
		}		
		buffer.append("getdate()");
		if(not)
		{
			buffer.append(" or ");
		}
		else
		{
			buffer.append(" and ");
		}			
		buffer.append(field);
		if(not)
		{
			buffer.append(" >");
		}
		else
		{
			buffer.append(" <=");
		}		
		buffer.append(" convert(datetime, '");
		buffer.append(getDayEndString(CalendarFactory.getEndOfWeekCal()));
		buffer.append("', 120)");
	}

	public void appendThisMonthStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" < ");
		}
		else
		{
			buffer.append(" >= ");
		}		
		buffer.append("getdate()");
		if(not)
		{
			buffer.append(" or ");
		}
		else
		{
			buffer.append(" and ");
		}			
		buffer.append(field);
		if(not)
		{
			buffer.append(" > ");
		}
		else
		{
			buffer.append(" <= ");
		}
		buffer.append("convert(datetime, '");
		buffer.append(getDayEndString(CalendarFactory.getEndOfMonthCal()));
		buffer.append("', 120)");
	}	
	
   public void appendDateTimeRangeStmt(StringBuffer buffer, String field,
         Calendar date1, Calendar date2, boolean not)
 {
     buffer.append(field);
     if(not)
     {
         buffer.append(" <");
     }
     else
     {
         buffer.append(" >=");
     }           
     buffer.append(" convert(datetime, '");
     buffer.append(getDateTimeString(date1));
     buffer.append("', 120)");
     if(not)
     {
         buffer.append(" or ");
     }
     else
     {
         buffer.append(" and ");
     }       
     buffer.append(field);
     if(not)
     {
         buffer.append(" >");
     }
     else
     {
         buffer.append(" <=");
     }       
     buffer.append(" convert(datetime, '");
     buffer.append(getDateTimeString(date2));
     buffer.append("', 120)");
 }	

    public void appendRangeStmt(StringBuffer buffer, String field,
            Calendar date1, Calendar date2, boolean not)
    {
        buffer.append(field);
        if(not)
        {
            buffer.append(" <");
        }
        else
        {
            buffer.append(" >=");
        }           
        buffer.append(" convert(datetime, '");
        buffer.append(getDayStartString(date1));
        buffer.append("', 120)");
        if(not)
        {
            buffer.append(" or ");
        }
        else
        {
            buffer.append(" and ");
        }       
        buffer.append(field);
        if(not)
        {
            buffer.append(" >");
        }
        else
        {
            buffer.append(" <=");
        }       
        buffer.append(" convert(datetime, '");
        buffer.append(getDayEndString(date2));
        buffer.append("', 120)");
    }
    
    public void appendHourStmt(StringBuffer buffer, String field, String num,  boolean not)
    {
        appendDatePartHour(buffer, field);
        if(not)
        {
            buffer.append(" <> ");
        }
        else
        {
            buffer.append(" = ");
        }
        buffer.append(num); 
    }     
    
	public void appendDateAddStmt(StringBuffer buffer, String interval, String amount, String datetime)
	{
		buffer.append("dateadd(");
		buffer.append(interval);
		buffer.append(',');
		buffer.append(amount);
		buffer.append(',');
		buffer.append(datetime);
		buffer.append(')');
	}
	
	public void appendDateDiffStmt(StringBuffer buffer, String interval, String datetime1, String datetime2)
	{
		buffer.append("datediff(");
		buffer.append(interval);
		buffer.append(',');
		buffer.append(datetime1);
		buffer.append(',');
		buffer.append(datetime2);
		buffer.append(')');
	}
	
	public void appendQualifiedTableName(StringBuffer buffer, String servername, String databasename, String tablename, String asname)
	{
		if(servername!=null && servername.length()!=0 && databasename!=null && databasename.length()!=0)
		{
			buffer.append(servername);
			buffer.append(_dot);
			buffer.append('[');
			buffer.append(databasename);
			buffer.append(']');
			buffer.append(_dot);
			buffer.append(ActionBean.DBCREATOR);
			buffer.append(_dot);				
		}
		else if(databasename!=null && databasename.length()!=0)
		{
			buffer.append('[');
			buffer.append(databasename);
			buffer.append(']');
			buffer.append(_dot);
			buffer.append(ActionBean.DBCREATOR);
			buffer.append(_dot);				
		}		
		buffer.append(tablename);
		if(asname!=null && asname.length()!=0)
		{
			buffer.append(_as);
			buffer.append(asname);
		}
	}	
    
    public void appendDatePartHour(StringBuffer buffer, String field)
    {
        buffer.append("DATEPART(HH, ");
        buffer.append(field);
        buffer.append(") ");
    }    
    
    public void appendDatePartDayOfWeek(StringBuffer buffer, String field)
    {
        buffer.append("DATEPART(dw, ");
        buffer.append(field);
        buffer.append(") ");
    }     
    
    public String getDayOfWeekNum(String day)
    {
        if(day.indexOf(CalendarFactory.MONDAY) == 0)
        {
        	return ("2");
        }
        else if(day.indexOf(CalendarFactory.TUESDAY) == 0)
        {
        	return ("3");
        }
        else if(day.indexOf(CalendarFactory.WEDNESDAY) == 0)
        {
        	return ("4");
        }
        else if(day.indexOf(CalendarFactory.THURSDAY) == 0)
        {
        	return ("5");
        }
        else if(day.indexOf(CalendarFactory.FRIDAY) == 0)
        {
        	return ("6");
        }
        else if(day.indexOf(CalendarFactory.SATURDAY) == 0)
        {
        	return ("7");
        }
        else if(day.indexOf(CalendarFactory.SUNDAY) == 0)
        {
        	return ("1");
        }
        return(null);
    }    
    
    public void appendDatePartYearMonth(StringBuffer buffer, String field)
    {
        buffer.append("SUBSTRING(");
        
        buffer.append("CONVERT(char, ");
        buffer.append(field);
        buffer.append(", ");
        buffer.append(odbcdateconvert);
        buffer.append(")");
        
        buffer.append(", ");
        buffer.append("0");
        buffer.append(", ");
        buffer.append("7");        
        buffer.append(")");        
    }
    
    public void appendWithQuote(StringBuffer buffer, String name)
    {
        buffer.append(name);
    }
    
}