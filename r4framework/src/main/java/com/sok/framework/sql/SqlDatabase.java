package com.sok.framework.sql;
import java.util.Calendar;

import com.sok.framework.*;
import com.sok.framework.generation.nodes.RootNode;
import com.sok.framework.generation.*;
public interface SqlDatabase
{
    public static String mssql = "MSSQL";
    public static String mysql = "MySQL";
    public static String mysql5 = "MySQL5";
    public static String mysql55 = "MySQL55";
    public static String fm8 = "FM8";
    public static String derby = "Derby";    
    
    public static final String format_uk = "uk"; 
    public static final String format_us = "us"; 
    public static final String format_odbc = "odbc"; 
    public static final String format_yearmonth = "yearmonth"; 
    
	public void generateSQLStatement(TableData row);
	
	public StringBuffer createdDataOnIDStatment(TableData row);
	
	public String buildOrFieldClause(String fieldname, String fieldvalue);

	public String getCurrentDateMethod();
	
	public String getNewIDMethod();
	
    public void generate(RootNode root, StringBuffer out);
    
	public String getContatenateString(String param1, String param2);
	public String getContatenateString(String param1, String param2, String param3);
	public String getContatenateString(String[] params);
	
	public String getReplaceString(String field, String fromString, String toString);

    public void appendTopStatementAtStart(StringBuffer stmtbuffer, int top);

    public void appendTopStatementAtEnd(StringBuffer stmtbuffer, int top);    
    
	public void appendTopStatement(StringBuffer stmtbuffer, int top);
	
	public void appendStrToIntStmt(StringBuffer searchstmtbuffer, String fieldname);	
	
	public void appendDateToStrStmt(StringBuffer searchstmtbuffer, String fieldname, String format);
	
	public void appendStrToDateStmt(StringBuffer searchstmtbuffer, String fieldname, String format);		
	
	public void appendDateToStrStmt(StringBuffer searchstmtbuffer, String fieldname);
	
	public void appendStrToDateStmt(StringBuffer searchstmtbuffer, String fieldname);	

	public void appendDatetimeToStrStmt(StringBuffer searchstmtbuffer, String fieldname);
	
	public void appendStrToDatetimeStmt(StringBuffer searchstmtbuffer, String fieldname);
	
    public void appendDatetimeToStrStmt(StringBuffer searchstmtbuffer, String fieldname, String format);
    
    public void appendStrToDatetimeStmt(StringBuffer searchstmtbuffer, String fieldname, String format);    
    
	public void appendDateAddStmt(StringBuffer buffer, String interval, String amount, String datetime);
	
	public void appendDateDiffStmt(StringBuffer buffer, String interval, String datetime1, String datetime2);	

   public abstract void appendDateRangeStmt(StringBuffer searchstmtbuffer, String fieldvalue, String gtvalue, String ltvalue);
   		
    public void appendDateRangeStmt(StringBuffer searchstmtbuffer, String fieldname, String gtvalue, String ltvalue, String format, boolean omit);
    
    public void appendQualifiedTableName(StringBuffer buffer, String servername, String databasename, String tablename, String asname);
    
    public void appendField(StringBuffer searchstmtbuffer, String fieldname, String fieldvalue,
			int fieldtype, MapData row)  ;  
      
    public void appendConditions(String condition, StringBuffer stmtstring);
    
    public void appendConditions(org.apache.velocity.context.Context context, String condition, StringBuffer stmtstring);    
    
    public void appendDatePartHour(StringBuffer buffer, String field);
    
    public void appendDatePartDayOfWeek(StringBuffer buffer, String field);    
    
    public String getDayOfWeekNum(String day);   
    
    public void appendDatePartYearMonth(StringBuffer buffer, String field);    
    
    public void appendHourStmt(StringBuffer buffer, String field, String num, boolean not);
    
	public void appendDayOfWeekStmt(StringBuffer buffer, String field, String day, boolean not);    
    
    public void appendIndexOf(StringBuffer buffer, String value, String search);
    
    public void appendStringLength(StringBuffer buffer, String expression);
    
    public void appendTrim(StringBuffer buffer, String expression);
    
    public void appendSubstring(StringBuffer buffer, String value, String start, String end);
    
    public int getDateRangeIndex(String value);
    
    public void appendTodayStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendThisWeekStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendThisMonthStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendFutureStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendOverdueStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendCurrentWeekStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendCurrentMonthStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendCurrentYearStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendLastWeekStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendLastMonthStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendLastYearStmt(StringBuffer buffer, String field, boolean not);
    
    public void appendNextRangeStmt(StringBuffer buffer, String field,
            Calendar date, Calendar init, boolean not);
    
    public void appendLastRangeStmt(StringBuffer buffer, String field,
            Calendar date, Calendar initdate, boolean not);
    
    public void appendNumberRangeStmt(StringBuffer searchstmtbuffer, String fieldname,
            String gtvalue, String ltvalue, String format, boolean omit);

    public int getRangeIndex(String value);
    
    public void appendAliasWithQuote(StringBuffer buffer, String name);
    
    public void appendWithQuote(StringBuffer buffer, String name);
    
	public void appendFieldOpTimestampValue(StringBuffer searchstmtbuffer, String fieldname,
			String value, MapData row);
	
	public void appendFieldRangeValue(StringBuffer searchstmtbuffer, String fieldname,
			String fieldvalue, int fieldtype, String format, boolean omit);
	
	public void appendBeginOfToday(StringBuffer searchstmtbuffer);

	public String createTable(TableSpec ts);
	public String alterIndex(String indexName, String fields, boolean unique, boolean drop);
	public String alterColumn(String colname, String coltype, int len, boolean allowNull, boolean autoIncrement, String defaultValue);
}