package com.sok.framework.sql;
import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.framework.generation.util.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.nodes.sql.*;

import java.sql.Types;
import java.util.*;

import org.apache.commons.lang.StringUtils;


public class MySQL extends AbstractDatabase
{
	static String limit = "limit";
	static char space = ' ';
	static String quote = "`";
	

	public static final String uk_dateformat = "'%d/%m/%Y'";
    public static final String us_dateformat = "'%m/%d/%Y'";    
	public static final String uk_datetimeformat = "'%d/%m/%Y %H:%i:%s'";	
    public static final String us_datetimeformat = "'%m/%d/%Y %H:%i:%s'";     
    public static final String odbc_datetimeformat = "'%Y-%m-%d %H:%i:%s'";
    public static final String odbc_dateformat = "'%Y-%m-%d'";
    public static final String yearmonth_dateformat = "'%Y-%m'";    
    
	public MySQL()
	{
		super();
	}	
	
    public void generate(RootNode root, StringBuffer out)
    {
        StatementNode statement = root.getStatementNode();
        StatementHeadNode statementhead = statement.getStatementHeadNode();
        if(statementhead!=null)
        {
            statementhead.generate(root, null, out);
            
            WhereSetNode wheresetnode = statement.getWhereSetNode();
            if(wheresetnode!=null)
            {
                wheresetnode.generate(root, out);
            }
            String vscript = statement.getScript();
            if(vscript!=null)
            {
                root.getDatabase().appendConditions(vscript, out);
            }              
            GroupBySetNode groupbysetnode = statement.getGroupBySetNode();
            if(groupbysetnode!=null)
            {
                groupbysetnode.generate(root, out);
            }
            HavingSetNode havingsetnode = statement.getHavingSetNode();
            if(havingsetnode!=null)
            {
                havingsetnode.generate(root, out);
            }                 
            OrderBySetNode orderbysetnode = statement.getOrderBySetNode();
            if(orderbysetnode!=null)
            {
                orderbysetnode.generate(root, out);
            }
        
            LimitNode limitnode = statement.getLimitNode();
            if(limitnode!=null)
            {
                limitnode.generate(root, out);
            }  
            
            String user = root.getString(GenerationKeys.key_CURRENTUSERID);
            out.append("\r\n/* ");
            out.append(user);
            out.append(" */");          
        }

    }    
    
    public void appendTopStatementAtStart(StringBuffer stmtbuffer, int top)
    {

    }
    
    public void appendTopStatementAtEnd(StringBuffer stmtbuffer, int top)
    {
        appendTopStatement(stmtbuffer, top);
    }
    
	public void appendTopStatement(StringBuffer stmtbuffer, int top)
	{
		if(top>0)
		{
			stmtbuffer.append(space);
			stmtbuffer.append(limit);
			stmtbuffer.append(space);
			stmtbuffer.append(String.valueOf(top));
			stmtbuffer.append(space);
		}
	}	
	
	private int getFieldLen(String s) {
		if(StringUtils.isEmpty(s)) return -1; 
		else return Integer.parseInt(s);
	}
	
	@Override
	public String createTable(TableSpec ts) {
		StringBuilder alter = new StringBuilder();
		alter.append("CREATE TABLE `").append(ts.getTableName()).append("`("); 				
		for(int tix=0; tix< ts.getFieldsLength(); tix++) {
			if(tix != 0) alter.append(",\n");
			alter.append(alterColumn(ts.getFieldName(tix), ts.getDataType(tix), getFieldLen(ts.getDataLength(tix)), ts.isDataNullable(tix), ts.isDataAutoIncrement(tix), ts.getDataDefault(tix)));	
		}
		if(ts.getPrimaryKeyName() != null) alter.append(",\nPRIMARY KEY (`").append(ts.getPrimaryKeyName()).append("`)");
		
		for(int tix=0; tix< ts.getIndexLength(); tix++) { 
			alter.append(",\n");
			alter.append(alterIndex(ts.getIndexName(tix), ts.getIndexFields(tix), ts.isIndexUnique(tix),false).substring(4) /* chop off the ADD */);
		}
		alter.append("\n) ENGINE=InnoDB, CHARSET=utf8");
		return alter.toString();
	}
	
	public String alterIndex(String indexName, String fields, boolean unique, boolean drop) {
		StringBuilder alter = new StringBuilder();
		if(!drop) {
			alter.append("ADD "); 
			if(unique) alter.append("UNIQUE "); 
			alter.append("KEY `").append(indexName).append("` (");
			boolean f = true;
			for(String s: fields.split(",")) {
				if(f) f = false; else alter.append(",");
				boolean desc = false;
				if(s.endsWith("/D")) {
					// desc 
					desc = true; 
					s = s.substring(0, s.indexOf("/D"));
				}
				alter.append("`").append(s).append("`"); 
				if(desc) alter.append(" DESC"); 
			}
			alter.append(")"); 
		} else {
			alter.append("DROP KEY `").append(indexName).append("`"); 
		}
		return alter.toString();
	}
	
	@Override
	public String alterColumn(String colname, String coltype, int len, boolean allowNull, boolean autoIncrement, String defaultValue) {
		StringBuilder alter = new StringBuilder();
		if (coltype == null || coltype.length() == 0) return "";
		alter.append("`").append(colname).append("`").append(space);
		if(coltype.equals("INT UNSIGNED")) {
			alter.append("INT"); 
			if(len > 0) 
				alter.append("(").append(len).append(")");
			alter.append(space).append("UNSIGNED");
		} else { 
			alter.append(coltype);
			for(String s: new String[]{"CHAR","VARCHAR","INT","FLOAT"}) {	// DOUBLE does not take a len
				if(s.equals(coltype)) {
					if(len > 0) 
						alter.append("(").append(len).append(")"); 
					break; 
				}
			}
		}
		//TODO remove option from parsing. 
		if("0000-00-00 00:00:00".equals(defaultValue)) {
			 if(!allowNull) allowNull = true; 
			 defaultValue = null;
		}
		if(!allowNull) alter.append(" NOT NULL"); 
		if(autoIncrement) {
			if(!coltype.startsWith("INT")) {
				throw new IllegalArgumentException("Auto Increment is only valid for INT types");
			}
			alter.append(" AUTO_INCREMENT"); 
		}
		if(defaultValue != null) {
			if("CURRENT_TIMESTAMP".equals(defaultValue)) {	// keyword, not string
				alter.append(" DEFAULT CURRENT_TIMESTAMP");
			} else {
				alter.append(" DEFAULT '").append(defaultValue).append("'");
			}
		}
		return alter.toString();
	}
	
	public void generateSQLStatement(TableData row)
	{
		TableSpec tspec = row.getTableSpec();
		ViewSpec vspec = row.getViewSpec();
		//System.out.println("generating ");
		StringBuffer searchstmtbuffer = new StringBuffer();
		StringBuffer stmtbuffer = new StringBuffer();
		stmtbuffer.append("select ");

		appendDisplayfields(stmtbuffer,row);
		//System.out.println("appendDisplayfields");
		stmtbuffer.append(RN);
		stmtbuffer.append(" from ");
		appendDisplayTables(stmtbuffer,row);
		//System.out.println("appendDisplayTables");
		
		boolean added = false;
		
		int fieldtype = 0;
	
		String fieldname = null;
		String temp = null;
		String fullfieldname = null;
		StringBuffer tempfieldname;
		for(int i=0; i<tspec.getFieldsLength(); i++)
		{
			tempfieldname = new StringBuffer();
			fieldname = tspec.getFieldName(i);
			//System.out.println("fieldname "+fieldname);
			tempfieldname.append(quote + tspec.getTableName() + quote);
			tempfieldname.append(_dot);
			tempfieldname.append(quote + fieldname + quote);
			fullfieldname = tempfieldname.toString();
			fieldtype = tspec.getFieldType(i);
			temp = (String)row.get(fieldname);

			if(temp != null && temp.length()!=0)
			{
				if(added)
				{
					searchstmtbuffer.append(_and);
				}
				//System.out.println("adding "+ fullfieldname);
				appendField(searchstmtbuffer,fullfieldname,temp,fieldtype, row);
				added = true;
			}	
		}
		
		Map groups = row.getSearchGroups();
		if(groups!=null)
		{
			Map group = null;
			Iterator groupkeys = groups.keySet().iterator();

			String groupkey = null;

			while(groupkeys.hasNext())
			{	
				groupkey = (String)groupkeys.next();
				if(row.containsKey(groupkey))
				{
					group = (Map)groups.get(groupkey);

					if(added)
					{
						searchstmtbuffer.append(_and);
					}
					appendSearchGroups(searchstmtbuffer, group, groupkey, tspec, row, null);
					added = true;
				}
			}
		}
		
		for(int j=0; j<vspec.getRelatedCountsLength(); j++)
		{
			tempfieldname = new StringBuffer();
			fieldname = vspec.getRelatedCountItemName(j);
			temp = (String)row.get(fieldname);
			if(temp != null && temp.length()!=0)
			{
				if(added)
				{
					searchstmtbuffer.append(_and);
				}
				appendRelatedCount(tempfieldname, j, row);
				appendField(searchstmtbuffer,tempfieldname.toString(),temp,Types.INTEGER, row);
				added=true;
			}	
		}
        
        for(int j=0; j<vspec.getRelatedSumsLength(); j++)
        {
            tempfieldname = new StringBuffer();
            fieldname = vspec.getRelatedSumItemName(j);
            temp = (String)row.get(fieldname);
            if(temp != null && temp.length()!=0)
            {
                if(added)
                {
                    searchstmtbuffer.append(_and);
                }
                appendRelatedSum(tempfieldname, j, row);
                appendField(searchstmtbuffer,tempfieldname.toString(),temp,Types.FLOAT, row);
                added=true;
            }   
        }           
        
		// <NBCHANGE - 020503 - 1>
		String vCondition = vspec.getCondition();
		if(null != vCondition) {
			if(added){
				searchstmtbuffer.append(_and);
			}
			searchstmtbuffer.append(getSubstituted(vCondition,row));
		}
		// </NBCHANGE - 020503 - 1>

		added = appendRelatedSearch(added,searchstmtbuffer,row);
		
		if(searchstmtbuffer.length()!=0)
		{
			stmtbuffer.append(RN);
			stmtbuffer.append(" where ");
		}
		stmtbuffer.append(searchstmtbuffer);
		
		appendSortOrder(stmtbuffer, row);
		
		appendTopStatement(stmtbuffer, row.getTop());
		
      String user = row.getString(GenerationKeys.key_CURRENTUSERID);
      stmtbuffer.append("\r\n/* ");
      stmtbuffer.append(user);
      stmtbuffer.append(" */");    		
		
		row.setSearchCriteria(searchstmtbuffer.toString());		
		row.setSearchStatement(stmtbuffer.toString());
	}

	public String getCurrentDateMethod()
	{
		return("NOW()");
	}

	public String getNewIDMethod()
	{
		return("UUID()");	
	}	
	
	public void appendCurrentDateString(StringBuffer searchstmtbuffer)
	{
		appendDateToStrStmt(searchstmtbuffer, getCurrentDateMethod());
	}

	protected String getDateConvertType(String type)
	{
        if(type!=null)
        {
            if(type.equals(format_us))
            {
                return (us_dateformat); 
            }
            else if(type.equals(format_odbc))
            {
                return (odbc_dateformat);
            }
        }
		return (uk_dateformat);
	}		

    protected String getDateTimeConvertType(String type)
    {
        if(type!=null)
        {        
            if(type.equals(format_us))
            {
                return (us_datetimeformat);
            }
            else if(type.equals(format_odbc))
            {
                return (odbc_datetimeformat);
            }
        }
        return (uk_datetimeformat);
    }    
	
	public void appendBeginOfToday(StringBuffer searchstmtbuffer)
	{
		searchstmtbuffer.append("CURDATE()");
	}	
	
	public void appendStrToIntStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		searchstmtbuffer.append("CAST(");
		searchstmtbuffer.append(fieldname);
		searchstmtbuffer.append(" AS SIGNED)");		
	}
	
	public void appendStrToDateStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
	{
        String format = this.getDateConvertType(formattype);
        appendStrToDateStmtWithFormat(searchstmtbuffer, fieldname, format);  
	}
    
    public void appendStrToDatetimeStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
    {
        String format = this.getDateTimeConvertType(formattype);
        appendStrToDateStmtWithFormat(searchstmtbuffer, fieldname, format);  
    }    
	
    public void appendStrToDateStmtWithFormat(StringBuffer searchstmtbuffer, String fieldname, String format)
    {
        searchstmtbuffer.append("STR_TO_DATE(");
        searchstmtbuffer.append(fieldname);
        searchstmtbuffer.append(", ");
        searchstmtbuffer.append(format);
        searchstmtbuffer.append(")");   
    }    
    
	public void appendDateToStrStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
	{
        String format = this.getDateConvertType(formattype);
        appendDateToStrStmtWithFormat(searchstmtbuffer, fieldname, format);
	}

    public void appendDatetimeToStrStmt(StringBuffer searchstmtbuffer, String fieldname, String formattype)
    {
        String format = this.getDateTimeConvertType(formattype);
        appendDateToStrStmtWithFormat(searchstmtbuffer, fieldname, format);
    }    
    
    public void appendDateToStrStmtWithFormat(StringBuffer searchstmtbuffer, String fieldname, String format)
    {
        searchstmtbuffer.append("DATE_FORMAT(");
        searchstmtbuffer.append(fieldname);
        searchstmtbuffer.append(", ");
        searchstmtbuffer.append(format);
        searchstmtbuffer.append(")");
    }
    
	public void appendDateToStrStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendDateToStrStmt(searchstmtbuffer, fieldname, uk_dateformat);
	}		
	
	public void appendStrToDateStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendStrToDateStmt(searchstmtbuffer, fieldname, uk_dateformat);
	}

	public void appendDatetimeToStrStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendDatetimeToStrStmt(searchstmtbuffer, fieldname, uk_datetimeformat);
	}	
	
	public void appendStrToDatetimeStmt(StringBuffer searchstmtbuffer, String fieldname)
	{
		appendStrToDatetimeStmt(searchstmtbuffer, fieldname, uk_datetimeformat);
	}	

    public void appendDateRangeStmt(StringBuffer searchstmtbuffer, String fieldname, String gtvalue, String ltvalue)
    {
        appendDateRangeStmt(searchstmtbuffer, fieldname, gtvalue, ltvalue, uk_datetimeformat, false);
    }
    
	public void appendDateRangeStmt(StringBuffer searchstmtbuffer, String fieldname, String gtvalue, String ltvalue, String formattype, boolean omit)
	{
        String format = this.getDateTimeConvertType(formattype);
		searchstmtbuffer.append(fieldname);
		searchstmtbuffer.append(" >= ");
		searchstmtbuffer.append("STR_TO_DATE(");
		searchstmtbuffer.append("'");
		searchstmtbuffer.append(gtvalue);
        if(gtvalue.length()<11)
        {
            searchstmtbuffer.append(daystart); 
        } 
		searchstmtbuffer.append("'");
		searchstmtbuffer.append(", ");
        searchstmtbuffer.append(format);
		searchstmtbuffer.append(")");
		searchstmtbuffer.append(" and ");
		searchstmtbuffer.append(fieldname);
		searchstmtbuffer.append(" <= ");
		searchstmtbuffer.append("STR_TO_DATE(");
		searchstmtbuffer.append("'");
		searchstmtbuffer.append(ltvalue);
        if(ltvalue.length()<11)
        {          
            searchstmtbuffer.append(dayend);
        }
		searchstmtbuffer.append("'");
		searchstmtbuffer.append(", ");
        searchstmtbuffer.append(format);
		searchstmtbuffer.append(")");
	}	
	
	public  String getContatenateString(String[] params) {
	   StringBuffer buffer = new StringBuffer();
	   
	   if (params !=null && params.length > 0) {
	      if (params.length == 1) {
   	      buffer.append(params[0]);
	      }
	      else {
	         buffer.append("CONCAT(");
   	      buffer.append(params[0]);

      	   for (int i=1; i < params.length; i++) {
      	      buffer.append(", ");
   	         buffer.append(params[i]);
      	   }
      	   buffer.append(")");
   	   }
	   }
	   return buffer.toString();
	}
	
    public void appendSubstring(StringBuffer buffer, String value, String start, String end)
    {
        buffer.append("SUBSTRING(");
        buffer.append(value);
        buffer.append(", ");
        buffer.append(start);
        buffer.append(", ");
        buffer.append(end);        
        buffer.append(")");
    }    
    
    public void appendStringLength(StringBuffer buffer, String expression)
    {
        buffer.append("CHAR_LENGTH(");
        buffer.append(expression);
        buffer.append(")");
    }
    
    public void appendTrim(StringBuffer buffer, String expression)
    {
        buffer.append("TRIM(");
        buffer.append(expression);
        buffer.append(")");
    }
    
    public void appendIndexOf(StringBuffer buffer, String value, String search)
    {
        buffer.append("INSTR(");
        buffer.append(value);
        buffer.append(", ");
        buffer.append(search);
        buffer.append(")");
    }  
 /*   
	public void appendTodayStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append("DATE(");
		buffer.append(field);
		buffer.append(") ");
		if(not)
		{
			buffer.append("<>");
		}
		else
		{
			buffer.append("=");
		}		
		buffer.append(" CURDATE()");
	}
*/
	public void appendTodayStmt(StringBuffer buffer, String field, boolean not)
	{
		Calendar today = new GregorianCalendar();
		appendRangeStmt(buffer, field, today, today, not);
	}
	
	public void appendOverdueStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" >= '");
		}
		else
		{
			buffer.append(" < '");
		}			
		buffer.append(getDayEndString(CalendarFactory.getYesterdayCal()));
		buffer.append("'");
	}

	public void appendThisWeekStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" < ");
		}
		else
		{
			buffer.append(" >= ");
		}		
		buffer.append("STR_TO_DATE(");
		buffer.append("'");
		
		buffer.append(getDayStartString(CalendarFactory.getStartOfWeekCal()));
		
		buffer.append("'");
        buffer.append(", ");
        buffer.append(odbc_datetimeformat);
        buffer.append(")");
		
		if(not)
		{
			buffer.append(" or ");
		}
		else
		{
			buffer.append(" and ");
		}		
		buffer.append(field);
		if(not)
		{
			buffer.append(" > ");
		}
		else
		{
			buffer.append(" <= ");
		}			
		
		buffer.append("STR_TO_DATE(");
		buffer.append("'");
		
		buffer.append(getDayEndString(CalendarFactory.getEndOfWeekCal()));
		
		buffer.append("'");
        buffer.append(", ");
        buffer.append(odbc_datetimeformat);
        buffer.append(")");
	}

	public void appendThisMonthStmt(StringBuffer buffer, String field, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" < ");
		}
		else
		{
			buffer.append(" >= ");
		}
		buffer.append("STR_TO_DATE(");
		buffer.append("'");
		
		buffer.append(getDayStartString(CalendarFactory.getStartOfMonthCal()));
		
        buffer.append("'");
        buffer.append(", ");
        buffer.append(odbc_datetimeformat);
        buffer.append(")");
		
		if(not)
		{
			buffer.append(" or ");
		}
		else
		{
			buffer.append(" and ");
		}		
		buffer.append(field);
		if(not)
		{
			buffer.append(" > ");
		}
		else
		{
			buffer.append(" <= ");
		}			
		buffer.append("STR_TO_DATE(");
		buffer.append("'");
		
		buffer.append(getDayEndString(CalendarFactory.getEndOfMonthCal()));		
		
        buffer.append("'");
        buffer.append(", ");
        buffer.append(odbc_datetimeformat);
        buffer.append(")");
	}
    
   public void appendDateTimeRangeStmt(StringBuffer buffer, String field,
         Calendar date1, Calendar date2, boolean not)
	{
      buffer.append(field);
      if(not)
      {
         buffer.append(" < '");
      }
      else
      {
         buffer.append(" >= '");
      }        
      buffer.append(getDateTimeString(date1));
      buffer.append("'");
      if(not)
      {
         buffer.append(" or ");
      }
      else
      {
         buffer.append(" and ");
      }     
      buffer.append(field);
      if(not)
      {
         buffer.append(" > '");
      }
      else
      {
         buffer.append(" <= '");
      }        
      buffer.append(getDateTimeString(date2));
      buffer.append("'");
	}
	
	public void appendRangeStmt(StringBuffer buffer, String field,
            Calendar date1, Calendar date2, boolean not)
	{
		buffer.append(field);
		if(not)
		{
			buffer.append(" < '");
		}
		else
		{
			buffer.append(" >= '");
		}			
		buffer.append(getDayStartString(date1));
		buffer.append("'");
		if(not)
		{
			buffer.append(" or ");
		}
		else
		{
			buffer.append(" and ");
		}		
		buffer.append(field);
		if(not)
		{
			buffer.append(" > '");
		}
		else
		{
			buffer.append(" <= '");
		}			
		buffer.append(getDayEndString(date2));
		buffer.append("'");
	}

    public void appendHourStmt(StringBuffer buffer, String field, String num,  boolean not)
    {
        appendDatePartHour(buffer, field);
        if(not)
        {
            buffer.append(" <> ");
        }
        else
        {
            buffer.append(" = ");
        }
        buffer.append(num); 
    }    
    
    public String getDayOfWeekNum(String day)
    {
        if(day.indexOf(CalendarFactory.MONDAY) == 0)
        {
        	return ("0");
        }
        else if(day.indexOf(CalendarFactory.TUESDAY) == 0)
        {
        	return ("1");
        }
        else if(day.indexOf(CalendarFactory.WEDNESDAY) == 0)
        {
        	return ("2");
        }
        else if(day.indexOf(CalendarFactory.THURSDAY) == 0)
        {
        	return ("3");
        }
        else if(day.indexOf(CalendarFactory.FRIDAY) == 0)
        {
        	return ("4");
        }
        else if(day.indexOf(CalendarFactory.SATURDAY) == 0)
        {
        	return ("5");
        }
        else if(day.indexOf(CalendarFactory.SUNDAY) == 0)
        {
        	return ("6");
        }
        return(null);
    }
    
	public void appendDateAddStmt(StringBuffer buffer, String interval, String amount, String datetime)
	{
		buffer.append("DATE_ADD(");
		buffer.append(datetime);
		buffer.append(", INTERVAL ");
		buffer.append(amount);
		buffer.append(' ');
		buffer.append(interval);
		buffer.append(')');
	}	
	
	/**
		Fix to solve loss of accuracy under Mysql 4.1
	*/ 
	public void appendDateDiffStmt(StringBuffer buffer, String interval, String datetime1, String datetime2)
	{
		if(interval.equalsIgnoreCase("DAY")) 
		{
			buffer.append("DATEDIFF(");
			buffer.append(datetime2);
			buffer.append(',');
			buffer.append(datetime1);
			buffer.append(")");
		} else { 
		
			buffer.append("CAST(TIME_TO_SEC(TIMEDIFF(");
			buffer.append(datetime2);
			buffer.append(',');
			buffer.append(datetime1);
			buffer.append("))");
			if(interval.equalsIgnoreCase("MINUTE"))
			{
				buffer.append(" / 60");
			}
			else if(interval.equalsIgnoreCase("HOUR"))
			{
				buffer.append(" / 60 / 60");
			}
			else if(interval.equalsIgnoreCase("DAY"))
			{
				buffer.append(" / 60 / 60 / 24");
			}
			else if(interval.equalsIgnoreCase("WEEK"))
			{
				buffer.append(" / 60 / 60 / 24 / 7");
			}
			buffer.append(" AS SIGNED)");
		} 
	}	
	
	public void appendQualifiedTableName(StringBuffer buffer, String servername, String databasename, String tablename, String asname)
	{
		if(databasename!=null && databasename.length()!=0)
		{
			buffer.append(databasename);
			buffer.append(_dot);			
		}		
		buffer.append(tablename);
		if(asname!=null && asname.length()!=0)
		{
			buffer.append(_as);
			buffer.append(asname);
		}
	}
    
    public void appendDatePartDayOfWeek(StringBuffer buffer, String field)
    {
        buffer.append("WEEKDAY(");
        buffer.append(field);
        buffer.append(") ");
    }
    
    public void appendDatePartHour(StringBuffer buffer, String field)
    {
        buffer.append("HOUR(");
        buffer.append(field);
        buffer.append(") ");
    }
    
    public void appendDatePartYearMonth(StringBuffer buffer, String field)
    {
    	buffer.append("STR_TO_DATE(");
    	buffer.append(field);
    	buffer.append(", ");
    	buffer.append(yearmonth_dateformat);
    	buffer.append(")");   
    }    
    
    public void appendWithQuote(StringBuffer buffer, String name)
    {
        buffer.append(name);
    }
      
}