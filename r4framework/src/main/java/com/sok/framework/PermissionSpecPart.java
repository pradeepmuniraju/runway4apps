package com.sok.framework;
import org.dom4j.*;

import com.sok.runway.XMLUtil;

public class PermissionSpecPart extends XMLSpec
{
   private Element root = null;
   
   public static String NAME = "DefaultPermission";
   
   public static String _name = "name";
   public static String _category = "category";
   public static String _label = "label";
   public static String _defaultPermissions = "defaultPermissions";
   public static String _permission = "permission";
   public static String _module = "module";
   public static String _displaySpec = "displaySpec";
   public static String _labelName = "labelName";
   
   
   private Element[] defaultPermissions = null;
   

   
   public PermissionSpecPart(String path, boolean ignoreerror) 
   {  
      super(path, ignoreerror);
      if(super.document!=null)
      {        
         this.root = super.document.getRootElement();
         if(root != null)
         {
            defaultPermissions = super.getElementArray(super.getElementArray(root,_defaultPermissions)[0],_permission);
         }
      }     
   }   
   
   public int getCategoryLength()
   {
      return(super.getCount(super.getElementArray(root,_category)));
   }   
   
   public String getCategoryLabel(int i)
   {
      return(super.getAttribute(super.getElementArray(root,_category), i, _label));
   }   
   
   public int getModuleLength(int i)
   {
      return(super.getCount(super.getElementArray(super.getElementArray(root,_category)[i],_module)));
   }   
   
   public String getModuleName(int i, int j)
   {
      return(super.getAttribute(super.getElementArray(super.getElementArray(root,_category)[i],_module), j, _name));
   }  
   
   public String getModuleLabel(int i, int j)
   {
      Element moduleelement = super.getElementArray(super.getElementArray(root,_category)[i],_module)[j];
      if(moduleelement!=null)
      {
         String displayspec = XMLUtil.getAttribute(moduleelement, _displaySpec);
         if(displayspec!=null)
         {
             return(SpecManager.getDisplaySpec(displayspec).getLabel());
         }
      }
      
      return(XMLUtil.getAttribute(moduleelement, _label));
   }    
     
   
   public int getPermissionLength()
   {
      return(super.getCount(defaultPermissions));
   }   
   
   public String getPermissionLabel(int i)
   {
      return(super.getAttribute(defaultPermissions, i, _label));
   }  
   
   public String getPermissionName(int i)
   {
      return(super.getAttribute(defaultPermissions, i, _name));
   }     
}
