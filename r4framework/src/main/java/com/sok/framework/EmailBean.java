package com.sok.framework;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.sok.runway.exchange.ExchangeConnect;
import com.sok.runway.exchange.ExchangeConnect.RunwayExchangeEmail;

import java.io.InputStream;
import java.io.StringWriter;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class EmailBean {

    String host = null;
    String username = null;
    String password = null;
    String protocol = null;
    String port = null;

    public static final String pop3 = "pop3";
    public static final String index = "_INDEX";

    public static final String SSL_FACTORY = "com.sok.ssl.SokSSLSocketFactory";

    Store store = null;
    Folder folder = null;
    Message[] messages = null;
    StringBuffer errors = null;
    Locale thislocale = null;
    TimeZone thistimezone = null;
    
    ExchangeConnect exchange = null;

    String dbconn = null;
    Connection con = null;
    Context ctx = null;

    boolean debug = false;
    boolean saveemails = false;
    
	Vector<RunwayExchangeEmail> mails = new Vector<RunwayExchangeEmail>();

    static String nosubject = "&lt;no subject&gt;";
    public final static String HTML_BODY_DISPLAY_NAME = "EmailMessage.html";

    ArrayList[] emailattachments = null;
    String[] messagebodies = null;

    EmailComparator comparator = new EmailComparator();

    public void setSort(String sorton, boolean sortorder) {
        comparator.setSort(sorton, sortorder);
    }

    public void setSort(String sorton) {
        comparator.setSort(sorton);
    }

    public void setDebug(boolean b) {
        debug = b;
    }

    public void setSaveEmailsAsNotes(boolean b) {
        saveemails = b;
    }

    public void setTimeZone(String s) {
        try {
            thistimezone = TimeZone.getTimeZone(s);
        } catch (Exception ex) {
            errors.append(ex.toString());
            errors.append("\r\n");
        }
    }

    public void setTimeZone(TimeZone tz) {
        thistimezone = tz;
    }

    public void setProtocol(String s) {
        protocol = s;
    }

    public void setPort(String s) {
        port = s;
    }

    public void setDBConnName(String name) {
        dbconn = name;
    }

    public void setLocale(Locale locale) {
        thislocale = locale;
    }

    public void setLocale(String s) {
        try {
            StringTokenizer st = new StringTokenizer(s, "_");
            String lang = st.nextToken();
            String country = st.nextToken();

            thislocale = new Locale(country, lang);
        } catch (Exception ex) {
            errors.append(ex.toString());
            errors.append("\r\n");
        }
    }

    public boolean isBodyHTML(int i) {
        Message message = messages[i];
        if (message != null) {
            try {
                return message.isMimeType("text/html");
            } catch (Exception e) {
                return false;
            }
        }
        return false;

    }

    public String getBody(String MessageID) {
        int i = getIndexForID(MessageID);
        return getBody(i);
    }

    public int getSize(int i) {
        try {
            Message message = messages[i];
            return message.getSize();
        } catch (Exception e) {
            return -1;
        }
    }

    public int getSizeInKB(int i) {
        int attachSize = getSize(i);
        if (attachSize > 0) {
            attachSize = attachSize / 1024 + 1;
        }
        return (attachSize);
    }

    public String getBody(int i) {
    	if (exchange == null) {
	        Message message = messages[i];
	        String msg = messagebodies[i];
	        if (msg == null) {
	            msg = getBody(message);
	            messagebodies[i] = msg;
	        }
	        if (msg != null) {
	            return (msg);
	        }
    	} else {
    		return mails.get(i).getBody();
    	}
        return ("");
    }

    public String getBody(Part part) {
        try {
            Object msg = part.getContent();
            // getContent() throws UnsupportedEncodingException
            // when part is of type text/plain; charset=unicode-1-1-utf-7

            if (msg instanceof Multipart && part.isMimeType("multipart/report")) {
                Multipart mp = (Multipart) msg;
                int numParts = mp.getCount();
                StringBuffer tempbuffer = new StringBuffer();
                String temp = null;
                String desc = null;
                for (int j = 0; j < numParts; j++) {
                    Part p = mp.getBodyPart(j);
                    temp = getBody(p);
                    if (temp != null) {
                        desc = p.getDescription();
                        tempbuffer.append("\r\n---");
                        if (desc != null) {
                            tempbuffer.append(desc);
                        }
                        tempbuffer.append("---\r\n\r\n");
                        tempbuffer.append(temp);
                    }
                }
                return (tempbuffer.toString());
            } else if (msg instanceof Multipart) {
                Multipart mp = (Multipart) msg;
                int numParts = mp.getCount();

                String temp = null;
                for (int j = 0; j < numParts; j++) {
                    temp = getBody(mp.getBodyPart(j));
                    if (temp != null) {
                        return (temp);
                    }
                }
            } else if (part.isMimeType("message/delivery-status")) {
                return (dump(part));
            } else if (msg instanceof String) {
                return ((String) msg);
            } else if (msg instanceof Part) {
                return (getBody((Part) msg));
            } else {
                return ("CONTENT_TYPE=" + part.getContentType());
            }

        } catch (java.io.UnsupportedEncodingException e) {
            // javamail can't seem to handle mime type text/plain;
            // charset=unicode-1-1-utf-7
            return (dump(part));
        } catch (Exception e) {
            errors.append("While retrieving message body: \r\n");
            errors.append(e.toString());
            errors.append("\r\n");
        }
        return (null);
    }

    public String dump(Part part) {
        StringWriter out = new StringWriter();
        try {
            InputStream is = part.getInputStream();
            int i;
            while ((i = is.read()) != -1) {
                out.write(i);
            }
        } catch (Exception e) {
            errors.append("While writing message part: \r\n");
            errors.append(e.toString());
            errors.append("\r\n");
        }
        return (out.toString());
    }

    public void connectToPop(String host, String username, String password) {
        connect(host, null, null, username, password);
    }

    public void connect(String host, String protocol, String username,
                        String password) {
        connect(host, null, protocol, username, password);
    }

    public void connect(String host, String port, String protocol,
                        String username, String password) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.port = port;
        this.protocol = protocol;
        if (this.protocol == null || this.protocol.length() == 0) {
            this.protocol = pop3;
        }
        if (host.length() != 0 && username.length() != 0
                && password.length() != 0) {
            connect();
        }
    }

    public void connectForMessage(String messageid) {
        int i = getIndexForID(messageid);
        ArrayList attachments = null;
        if (emailattachments != null && i >= 0) {
            attachments = emailattachments[i];

            if (attachments == null) {
                if (!store.isConnected()) {
                    connect();
                    // TODO: FIXME: dead store to attachments - does this code
                    // work?
                    attachments = emailattachments[i];
                }
            }
        }
    }

    void connect() {
        errors = new StringBuffer();
        if (!"exchange".equalsIgnoreCase(protocol)) {
	        Properties props = null;
	        try {
	            props = new Properties();
	            Session session = null;
	            if (protocol.equalsIgnoreCase("imaps")) {
	                if (!hasProvider(protocol)) {
	                    // If protocol set to secure imap, set properties to allow
	                    // the use
	                    // of
	                    // secure servers, no fallback to insecure.
	                    // errors.append("connecting to imaps >>");
	                    props.setProperty("mail.imap.socketFactory.class",
	                            SSL_FACTORY);
	                    props.setProperty("mail.imap.socketFactory.fallback",
	                            "false");
	                    java.security.Security.setProperty(
	                            "ssl.SocketFactory.provider", SSL_FACTORY);
	                    // Set protocol back to imap as JavaMail only understands
	                    // pop3 or
	                    // imap.
	                    protocol = "imap";
	                }
	                if (port == null || port.length() == 0) {
	                    props.setProperty("mail.imap.socketFactory.port", "993");
	                    props.put("mail.imap.port", "993");
	                } else {
	                    props.setProperty("mail.imap.socketFactory.port", port);
	                    props.put("mail.imap.port", port);
	                }
	            } else if (protocol.equalsIgnoreCase("imap")) {
	                if (port != null && port.length() != 0) {
	                    props.put("mail.imap.port", port);
	                }
	            } else {
	                if (port != null && port.length() != 0) {
	                    props.put("mail.pop3s.port", port);
	                }
	            }
	
	            session = Session.getDefaultInstance(props, null);
	            session.setDebug(debug);
	
	            if (session == null) {
	                errors.append("Email session is null.\r\n");
	            }
	            store = session.getStore(protocol);
	            // errors.append("connecting to host >>");
	            if (store == null) {
	                errors.append("Email store is null.\r\n");
	            }
	            store.connect(host, username, password);
	
	            folder = store.getFolder("INBOX");
	            // errors.append("about to open inbox >>");
	
	            folder.open(Folder.READ_WRITE);
	
	
	
	            messages = folder.getMessages();
	
	
	
	            Arrays.sort(messages, comparator);
	
	            emailattachments = new ArrayList[messages.length];
	            messagebodies = new String[messages.length];
	        } catch (Exception e) {
	            e.printStackTrace();
	            errors.append("while connecting to " + protocol + ": ");
	            errors.append(e.toString());
	            errors.append("\r\n");
	            
	            if(props!=null) { 
	            	for (Map.Entry<Object, Object> ek: props.entrySet()) { 
	            		System.out.println(String.valueOf(ek.getKey()) + " " + String.valueOf(ek.getValue()));
	            	}
	            }
	        }
        } else {
        	exchange = new ExchangeConnect(host, username, password);
        	if (!exchange.isConnected()) {
	            errors.append("while connecting to " + protocol + ": host " + host + " username " + username);
	            errors.append("\r\n");
        	}
        	SimpleDateFormat	sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	
            GenRow row = new GenRow();
            row.setConnection(con);
            row.setTableSpec("SystemParameters");
            row.setParameter("-select1","Value");
            row.setColumn("Name", "LastEmailRulesRun");
            row.doAction("selectfirst");
            String lastRun = row.getString("Value");
            row.setColumn("ON-Name", "LastEmailRulesRun");
            row.remove("Name");
            row.doAction("deleteall");
        	
            if (lastRun != null && lastRun.length() > 0) {
	        	mails = exchange.syncMailInbox(lastRun);
            } else {
            	Date date = new Date();
	        	date.setTime(date.getTime() - 3600 * 1000);

	        	mails = exchange.syncMailInbox(sdf.format(date));
            }
            
            row.clear();
            row.setColumn("Name","LastEmailRulesRun");
            row.setColumn("Value", sdf.format(new Date()));
            row.setColumn("Type", "Text");
            row.createNewID();
            row.doAction("insert");
        }
    }

    protected boolean hasProvider(String s) {
        java.util.Properties properties = System.getProperties();
        Session session = Session.getInstance(properties, null);
        try {
            Provider provider = session.getProvider(s);
            return (provider != null);
        } catch (Exception e) {
        }

        return (false);
    }

    public void connectDB() {
        DataSource ds = null;
        Context ctx = null;
        try {
            if (con == null || con.isClosed()) {
                ctx = new InitialContext();
                try {
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
                    Context envContext = (Context) ctx
                            .lookup(ActionBean.subcontextname);
                    ds = (DataSource) envContext.lookup(dbconn);
                } catch (Exception e) {
                }
                if (ds == null) {
                    ds = (DataSource) ctx.lookup(dbconn);
                }
                con = ds.getConnection();
            }
        } catch (Exception e) {
            errors.append("while connecting to DB: ");
            errors.append(e.toString());
            errors.append("\r\n");
        } finally {
            if (ctx != null) {
                try {
                    ctx.close();
                    ctx = null;
                } catch (Exception e) {
                }
            }
        }
    }

    public void closePop() {
        close();
    }

    /**
     * TODO: FIX ME!
     * <p/>
     * Folder and store used to be closed too early making it impossible for IMAP clients to download attachments.
     * For the moment I have commented out the part of code that closes stores and folders. We should fix this
     * in a stable and reliable way ASAP!
     */
    public void close() {        
        //try {
            // messages = null;
            //if (folder != null && folder.isOpen()) {                
                // folder.close(true);
            //}
            //if (store != null && store.isConnected()) {
                // store.close();
            //}
            //if (exchange != null && exchange.isConnected()) {
            // }
        //} catch (Exception e) {
        //    errors.append(e.toString());
        //    errors.append("\r\n");
        //}
    }

    public void closeDB() {
        if (con != null) {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (ctx != null) {
            try {
                ctx.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public int getMessageCount() {
    	if (exchange == null) {
	        if (messages != null) {
	            return (messages.length);
	        }
    	} else {
    		return mails.size();
    	}
        return (0);
    }

    public String getSubject(int i) {
        String subject = nosubject;

        if (exchange == null) {
	        try {
	            subject = messages[i].getSubject();
	            if (subject == null || subject.length() < 2) {
	                subject = nosubject;
	            }
	        } catch (Exception e) {
	            errors.append("while getting subject: ");
	            errors.append(e.toString());
	            errors.append("\r\n");
	        }
        } else {
	        try {
	            subject = mails.get(i).getSubject();
	            if (subject == null || subject.length() < 2) {
	                subject = nosubject;
	            }
	        } catch (Exception e) {
	            errors.append("while getting subject: ");
	            errors.append(e.toString());
	            errors.append("\r\n");
	        }
        }
        return (subject);
    }

    public Address[] getFrom(int i) {
    	if (exchange == null) {
	        try {
	            return messages[i].getFrom();
	        } catch (Exception e) {
	            errors.append("while getting from addresses: ");
	            errors.append(e.toString());
	            errors.append("\r\n");
	        }
    	} else {
	        try {
	            return (new Address[]{new InternetAddress(mails.get(i).getFrom())});
	        } catch (Exception e) {
	            errors.append("while getting from addresses: ");
	            errors.append(e.toString());
	            errors.append("\r\n");
	        }
    	}
        return (null);
    }

    public static String getMobile(String email) {
        int namelength = email.indexOf('@');
        if (namelength > 0) {
            if (namelength > 10) {
                return (email.substring(2, namelength));
            } else {
                return (email.substring(1, namelength));
            }
        }
        return (null);
    }

    public TableData getContactRow(String email, String messageid) {
        String mobile = getMobile(email);
        GenRow row = new GenRow();
        row.setTableSpec("Contacts");
        row.setParameter("-select1", "ContactID");
        row.setParameter("-select2", "RepUserID");
        row.setParameter("-select3", "GroupID");
        if (mobile != null) {
            row.setParameter("Email|1", "%" + email + "%");
            row.setParameter("Mobile|1", "%" + email);
        } else {
            row.setParameter("Email", "%" + email + "%");
        }
        row.setParameter("-ContactNotes", "omit");
        row.setParameter("ContactNotes.NoteID", "messageid");
        row.setParameter("-join1", "ContactNotes");
        row.doAction("selectfirst");
        if (row.isSuccessful()) {
            return (row);
        }
        return (null);
    }

    /**
     * @deprecated
     */
    String getContactStatement(String email, String messageid) {
        String mobile = getMobile(email);
        StringBuffer stmtbuffer = new StringBuffer();

        stmtbuffer
                .append("select ContactID, RepUserID, GroupID from Contacts where Email like '%");
        stmtbuffer.append(email);
        stmtbuffer.append("%'");
        if (mobile != null) {
            stmtbuffer.append(" or Mobile like '%");
            stmtbuffer.append(mobile);
            stmtbuffer.append("'");
        }
        stmtbuffer
                .append(" and not exists(select ContactID from Notes where Contacts.ContactID = Notes.ContactID and Notes.NoteID = '");
        stmtbuffer.append(messageid);
        stmtbuffer.append("')");
        return (stmtbuffer.toString());
    }

    /**
     * @deprecated
     */
    public String[] getContact(String email, String messageid) {
        if (email != null && messageid != null && email.length() > 0) {
            ResultSet current = null;
            connectDB();
            Statement stmt = null;
            try {
                stmt = con.createStatement();
                current = stmt.executeQuery(getContactStatement(email,
                        messageid));
                // errors.append(stmtbuffer.toString());
                if (current.next()) {
                    String[] contact = new String[3];
                    contact[0] = current.getString(1);
                    contact[1] = current.getString(2);
                    contact[2] = current.getString(3);
                    return (contact);
                }
            } catch (Exception e) {
                errors.append(e.toString());
                errors.append("\r\n");
            } finally {
                if (current != null) {
                    try {
                        current.close();
                    } catch (Exception ex) {
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (Exception ex) {
                    }
                }
                closeDB();
            }
        }
        return (null);
    }

    public String getReplayForwardBody(int i) {
        StringBuffer body = new StringBuffer();
        body.append("\r\n\r\n----- Original Message -----\r\n");

        body.append("From: ");
        body.append(getFromName(i));
        body.append(" <");
        body.append(getFromAddress(i));
        body.append(">\r\n");

        body.append("To: ");
        body.append(getToName(i));
        body.append(" <");
        body.append(getToAddress(i));
        body.append(">\r\n");

        body.append("Sent: ");
        body.append(getSentDateString(i));
        body.append("\r\n");

        body.append("Subject: ");
        body.append(getSubject(i));
        body.append("\r\n\r\n");

        body.append(">");
        body.append(StringUtil.replace(getBody(i), "\n", "\n>"));
        return (body.toString());
    }

    public MailerBean getForwardMailerBean(int i) throws MessagingException {
        MailerBean email = new MailerBean();

        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(getReplayForwardBody(i));

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        messageBodyPart = new MimeBodyPart();
        if (exchange == null) {
        	messageBodyPart.setDataHandler(messages[i].getDataHandler());
        } else {
        	messageBodyPart.setText(mails.get(i).getBody());
        }

        multipart.addBodyPart(messageBodyPart);

        email.setMultipart(multipart);

        email.setSubject(getSubject(i));
        email.setTo(getToAddress(i));
        email.setFrom(getFromAddress(i));

        return (email);
    }

    public GenRow getEmailAsGenRow(int i) {
        String messageid = getMessageID(i);
        String alpnumid = StringUtil.ToAlphaNumeric(messageid);
        String userid = "E3B3653517125741C947061350C510E4";
        java.util.Date date = getSentDate(i);
        GenRow row = new GenRow();
        row.setTableSpec("Notes");
        row.setParameter("NoteID", alpnumid);
        row.setParameter("Subject", getSubject(i));
        row.setParameter("Body", getBody(i));
        row.setParameter("ModifiedDate", "NOW");
        row.setParameter("SentDate", date);
        row.setParameter("NoteDate", date);
        row.setParameter("CreatedDate", date);
        row.setParameter("CreatedBy", userid);
        row.setParameter("ModifiedBy", userid);
        row.setParameter("Type", "Email");
        row.setParameter("Status", "Received");
        row.setParameter("EmailTo", getToAddress(i));
        row.setParameter("EmailFrom", getFromAddress(i));
        row.setParameter("EmailCC", getCCAddress(i));
        return (row);
    }

    public String saveAsNote(int i) {
        InternetAddress[] addresses = (InternetAddress[]) getFrom(i);
        // errors.append(addresses[0].getAddress());

        GenRow note = getEmailAsGenRow(i);

        TableData contact = this.getContactRow(addresses[0].getAddress(), note
                .getParameter("NoteID"));
        if (contact != null) {
            note.setParameter("ContactID", contact.getString("ContactID"));
            note.setParameter("RepUserID", contact.getString("RepUserID"));
            note.setParameter("GroupID", contact.getString("GroupID"));

            note.doAction("insert");
            if (note.isSuccessful()) {
                return (note.getString("NoteID"));
            }
        }
        return (null);
    }

    /*
      * public String saveAsNote(int i) { InternetAddress[] addresses =
      * (InternetAddress[]) getFrom(i); //
      * errors.append(addresses[0].getAddress()); String messageid =
      * getMessageID(i); String alpnumid = StringUtil.ToAlphaNumeric(messageid);
      * String[] contact = getContact(addresses[0].getAddress(), alpnumid);
      * String noteid = null; if(contact != null) { // errors.append(contact[0]);
      * connectDB(); StringBuffer updatestmt = new StringBuffer(); updatestmt
      * .append(
      * "insert Notes (NoteID, ContactID, RepUserID, GroupID, Subject, Body, ModifiedDate, SentDate, NoteDate, CreatedDate, CreatedBy, ModifiedBy, Type, Status, EmailTo, EmailFrom, EmailCC) values(?, ?, ?, ?, ?, ?, getDate(), convert(datetime, ?, 120), convert(datetime, ?, 120), convert(datetime, ?, 120), 'E3B3653517125741C947061350C510E4', 'E3B3653517125741C947061350C510E4', 'Email', 'Received', ?, ?, ?)"
      * );
      *
      * PreparedStatement updateprestmt = null; try { updateprestmt =
      * con.prepareStatement(updatestmt.toString()); String sentdate =
      * getDateString(getSentDate(i)); updateprestmt.setString(1, alpnumid);
      * updateprestmt.setString(2, contact[0]); updateprestmt.setString(3,
      * contact[1]); updateprestmt.setString(4, contact[2]);
      * updateprestmt.setString(5, getSubject(i)); updateprestmt.setString(6,
      * getBody(i)); updateprestmt.setString(7, sentdate);
      * updateprestmt.setString(8, sentdate); updateprestmt.setString(9,
      * sentdate); updateprestmt.setString(10, getToAddress(i));
      * updateprestmt.setString(11, getFromAddress(i));
      * updateprestmt.setString(12, getCCAddress(i)); int r =
      * updateprestmt.executeUpdate(); if(r == 1) { // MimeMessage msg =
      * (MimeMessage)messages[i]; // msg.setFlag(Flags.Flag.DELETED, true); }
      * return(noteid); } catch(Exception ex) { errors.append(ex.toString());
      * errors.append("\r\n"); } finally { if(updateprestmt != null) { try {
      * updateprestmt.close(); } catch(Exception e) { } ; } closeDB(); } }
      * return(null); }
      */

    public String getDateString(java.util.Date date) {
        GregorianCalendar day = new GregorianCalendar();
        if (thistimezone != null) {
            day.setTimeZone(thistimezone);
        }
        if (date == null) {
            day.setTime(new java.util.Date());
        } else {
            day.setTime(date);
        }

        StringBuffer end = new StringBuffer();

        end.append(String.valueOf(day.get(Calendar.YEAR)));
        end.append("-");
        end.append(String.valueOf(day.get(Calendar.MONTH) + 1));
        end.append("-");
        end.append(String.valueOf(day.get(Calendar.DAY_OF_MONTH)));
        end.append(" ");

        String hour = String.valueOf(day.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(day.get(Calendar.MINUTE));
        String seconds = String.valueOf(day.get(Calendar.SECOND));
        if (hour.length() == 1) {
            end.append("0");
        }
        end.append(hour);
        end.append(":");
        if (minute.length() == 1) {
            end.append("0");
        }
        end.append(minute);
        end.append(":");
        if (seconds.length() == 1) {
            end.append("0");
        }
        end.append(seconds);

        return (end.toString());
    }

    public Address[] getCC(int i) {
        try {
            return ((messages[i].getRecipients(Message.RecipientType.CC)));
        } catch (Exception e) {
            errors.append(e.toString());
            errors.append("\r\n");
        }
        return (null);
    }

    public Address[] getTo(int i) {
    	if (exchange == null) {
            try {
                return messages[i].getRecipients(Message.RecipientType.TO);
            } catch (Exception e) {
                errors.append(e.toString());
                errors.append("\r\n");
            }
    	} else {
            try {
            	Address[] address = new Address[3];
            	address[0] = new InternetAddress(mails.get(i).getTo());
            	address[1] = new InternetAddress(mails.get(i).getCc());
            	address[2] = new InternetAddress(mails.get(i).getBcc());
                return address;
            } catch (Exception e) {
                errors.append(e.toString());
                errors.append("\r\n");
            }
    	}
        return (null);
    }

    public String getFromAddress(int i) {
        Address[] addresses = getFrom(i);
        return (getAddress(addresses));
    }

    public String getFromName(int i) {
        Address[] addresses = getFrom(i);
        return (getAddressName(addresses));
    }

    public String getCCAddress(int i) {
        Address[] addresses = getCC(i);
        return (getAddress(addresses));
    }

    public String getCCName(int i) {
        Address[] addresses = getCC(i);
        return (getAddressName(addresses));
    }

    public String getToAddress(int i) {
        Address[] addresses = getTo(i);
        return (getAddress(addresses));
    }

    public String getToName(int i) {
        Address[] addresses = getTo(i);
        return (getAddressName(addresses));
    }

    static String getAddress(Address[] addresses) {
        if (addresses != null) {
            StringBuffer addressbuffer = new StringBuffer();
            if (addresses != null) {
                for (int j = 0; j < addresses.length; j++) {
                    if (j != 0) {
                        addressbuffer.append(",");
                    }
                    InternetAddress a = (InternetAddress) addresses[j];
                    addressbuffer.append(a.getAddress());
                }
            }
            return (addressbuffer.toString());
        }
        return ("");
    }

    static String getAddressName(Address[] addresses) {
        if (addresses != null) {
            StringBuffer addressbuffer = new StringBuffer();
            if (addresses != null) {
                for (int j = 0; j < addresses.length; j++) {
                    if (j != 0) {
                        addressbuffer.append(", ");
                    }
                    InternetAddress a = (InternetAddress) addresses[j];
                    String temp = a.getPersonal();
                    if (temp == null || temp.length() == 0) {
                        temp = a.getAddress();
                    }
                    addressbuffer.append(temp);
                }
            }
            return (addressbuffer.toString());
        }
        return ("");
    }

    public String getMessageID(int i) {
    	if (exchange == null) {
	        try {
	            String id = ((MimeMessage) messages[i]).getMessageID();
	            if (id != null) {
	                return (id);
	            }
	        } catch (Exception e) {
	            errors.append(e.toString());
	            errors.append("\r\n");
	        }
    	} else {
	        try {
	            String id = mails.get(i).getUid();
	            if (id != null) {
	                return (id);
	            }
	        } catch (Exception e) {
	            errors.append(e.toString());
	            errors.append("\r\n");
	        }
    	}
        return (index + String.valueOf(i));
    }

    public void setDelete(String messageID) {
    	if (exchange == null) {
	        int j = getIndexForID(messageID);
	        if (j != -1) {
	            MimeMessage msg = (MimeMessage) messages[j];
	            try {
	                msg.setFlag(Flags.Flag.DELETED, true);
	                folder.close(true);
	                store.close();
	            } catch (Exception e) {
	                errors.append(e.toString());
	                errors.append("\r\n");
	            }
	        }
    	}
    }

    public int getIndexForID(String messageID) {
        try {
            if (messageID.indexOf(index) == 0) {
                int i = Integer.parseInt(messageID.substring(index.length(),
                        messageID.length()));
                return (i);
            } else {
                for (int i = 0; i < messages.length; i++) {
                    MimeMessage msg = (MimeMessage) messages[i];
                    if (messageID.equals(msg.getMessageID())) {
                        return (i);
                    }
                }
            }
        } catch (Exception e) {
        }
        return (-1);
    }

    public Part getAttachment(int i, int j) {
        Part part = null;
    	if (exchange == null){
	        try {
	            part = (Part) buildAttachmentList(i).get(j);
	        } catch (Exception e) {
	
	        }
    	}
        return (part);
    }

    public String getAttachmentName(int i, int j) {
    	if (exchange == null) {
	        try {
	            Part part = (Part) buildAttachmentList(i).get(j);
	
	            if (part != null) {
	                if (part.isMimeType("text/html")) {
	                    return (HTML_BODY_DISPLAY_NAME);
	                } else {
	                    String filename = part.getFileName();
	                    if (filename != null) {
	                        return (filename);
	                    }
	                }
	            }
	        } catch (Exception e) {
	            errors.append("While retrieving attachment name: \r\n");
	            errors.append(e.toString());
	            errors.append("\r\n");
	        }
    	}
        return ("");
    }

    public int getAttachmentSize(int i, int j) {
    	if (exchange == null) {
	        try {
	            Part part = (Part) buildAttachmentList(i).get(j);
	            return part.getSize();
	        } catch (Exception e) {
	        }
    	}
    	return -1;
    }

    public int getAttachmentSizeInKB(int i, int j) {
        int attachSize = getAttachmentSize(i, j);
        if (attachSize > 0) {
            attachSize = attachSize / 1024 + 1;
        }
        return (attachSize);
    }

    public List<Part> buildAttachmentList(int i) {
    	if (exchange == null) {
	        ArrayList<Part> attachments = emailattachments[i];
	
	        if (attachments == null) {
	            attachments = new ArrayList<Part>();
	
	            try {
	                Object obj = messages[i].getContent();
	                if (obj instanceof Multipart) {
	                    addAttachments((Multipart) obj, attachments);
	                }
	            } catch (Exception e) {
	                errors.append("While retrieving attachment count: \r\n");
	                errors.append(e.toString());
	                errors.append("\r\n");
	            }
	            emailattachments[i] = attachments;
	        }
	        return (attachments);
    	}
    	return new ArrayList<Part>(); 
    }

    private void addAttachments(Multipart multipart, List<Part> attachList) {
    	if (exchange == null) {
	        try {
	            int i = multipart.getCount();
	
	            Object obj = null;
	            for (int j = 0; j < i; j++) {
	                BodyPart part = multipart.getBodyPart(j);
	                obj = part.getContent();
	                if (obj != null) {
	                    if (obj instanceof Multipart) {
	                        Multipart mobj = null;
	                        try {
	                            // getContent() throws UnsupportedEncodingException
	                            // when part is of type text/plain;
	                            // charset=unicode-1-1-utf-7
	                            mobj = (Multipart) part.getContent();
	                        } catch (Exception e) {
	                            mobj = null;
	                        }
	                        if (mobj != null) {
	                            addAttachments(mobj, attachList);
	                        }
	                    } else {
	                        attachList.add(part);
	                    }
	                }
	            }
	        } catch (Exception e) {
	            errors.append("While counting attachments : \r\n");
	            errors.append(e.toString());
	            errors.append("\r\n");
	        }
    	}
    }

    public int getAttachmentCount(int i) {
    	if (exchange == null) {
    		return buildAttachmentList(i).size();
    	}
    	return 0;
    }

    public Part getAttachment(String MessageID, int j) {
        int i = getIndexForID(MessageID);
        return (getAttachment(i, j));
    }

    public String getTextAttachments(int i) {
        List<Part> attachments = buildAttachmentList(i);
        int count = attachments.size();
        Part part = null;
        StringBuffer text = new StringBuffer();
        for (int j = 0; j < count; j++) {
            try {
                part = attachments.get(j);
                if (part.getContentType().indexOf("text") >= 0) {
                    text.append(part.getContent().toString());
                }
            } catch (Exception e) {
            }
        }
        return (text.toString());
    }

    public java.util.Date getSentDate(int i) {
        try {
            return ((messages[i].getSentDate()));
        } catch (Exception e) {
            errors.append(e.toString());
            errors.append("\r\n");
        }
        return (null);
    }

    public String getSentDateString(int i) {
        try {
            DateFormat dt = null;
            if (thislocale != null) {
                dt = DateFormat.getDateTimeInstance(DateFormat.SHORT,
                        DateFormat.SHORT, thislocale);
            } else {
                dt = DateFormat.getDateTimeInstance(DateFormat.SHORT,
                        DateFormat.SHORT);
            }
            if (thistimezone != null) {
                dt.setTimeZone(thistimezone);
            }
            java.util.Date date = getSentDate(i);
            if (date != null) {
                return (dt.format(date));
            }
        } catch (Exception e) {
            // System.out.println(ActionBean.writeStackTraceToString(e));
        }
        return ("");
    }

    public String getErrors() {
        if (errors != null) {
            return (errors.toString());
        }
        return (ActionBean._blank);
    }

    public Message getMessage(int i) {
        return (messages[i]);
    }

    public class EmailComparator implements Comparator {
        public static final String Sender = "SENDER";
        public static final String Date = "DATE";
        public static final String Subject = "SUBJECT";

        public static final boolean DESC = false;
        public static final boolean ASC = true;

        String sorton = Date;
        boolean sortorder = DESC;

        public EmailComparator() {
        }

        public void setSort(String sorton, boolean sortorder) {
            this.sorton = sorton;
            this.sortorder = sortorder;
        }

        public void setSort(String sorton) {
            if (sorton != null) {
                if (this.sorton.equals(sorton)) {
                    this.sortorder = sortorder ? false : true;
                } else {
                    this.sorton = sorton;
                    if (Sender.equals(sorton) || Subject.equals(sorton)) {
                        this.sortorder = ASC;
                    } else if (Date.equals(sorton)) {
                        this.sortorder = DESC;
                    }
                }
            }
        }

        public int compare(Object o1, Object o2) {
            return (compare((Message) o1, (Message) o2));
        }

        public int compare(Message m1, Message m2) {
            int result = 0;

            if (sorton.equals(Date)) {
                result = compareDate(m1, m2);
            } else if (sorton.equals(Sender)) {
                result = compareSender(m1, m2);
            } else if (sorton.equals(Subject)) {
                result = compareSubject(m1, m2);
            }

            if (!sortorder) {
                if (result < 0) {
                    result = 1;
                } else if (result > 0) {
                    result = -1;
                }
            }
            return (result);
        }

        public int compareDate(Message m1, Message m2) {
            java.util.Date d1 = null, d2 = null;
            try {
                d1 = m1.getSentDate();
            } catch (Exception e) {
            }
            try {
                d2 = m2.getSentDate();
            } catch (Exception e) {
            }

            if (d1 != null && d2 != null) {
                return (d1.compareTo(d2));
            } else if (d1 != null) {
                return (1);
            } else if (d2 != null) {
                return (-1);
            }
            return (0);
        }

        public int compareSender(Message m1, Message m2) {
            Address[] a1 = null, a2 = null;
            try {
                a1 = m1.getFrom();
            } catch (Exception e) {
            }
            try {
                a2 = m2.getFrom();
            } catch (Exception e) {
            }

            if (a1 != null && a2 != null) {
                return (EmailBean.getAddress(a1).compareTo(EmailBean
                        .getAddress(a2)));
            } else if (a1 != null) {
                return (1);
            } else if (a2 != null) {
                return (-1);
            }
            return (0);
        }

        public int compareSubject(Message m1, Message m2) {
            String a1 = null, a2 = null;
            try {
                a1 = m1.getSubject();
            } catch (Exception e) {
            }
            try {
                a2 = m2.getSubject();
            } catch (Exception e) {
            }

            if (a1 != null && a2 != null) {
                return (a1.compareTo(a2));
            } else if (a1 != null) {
                return (1);
            } else if (a2 != null) {
                return (-1);
            }
            return (0);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return (true);
            }
            return (false);
        }
    }

    public boolean hasFolders() {
        return ("imap".equals(this.protocol) || "imaps".equals(this.protocol));
    }

    public void moveMessageToFolder(String name, int i)
            throws MessagingException {
        Folder f = store.getFolder(name);
        if (!f.exists()) {
            f.create(Folder.HOLDS_MESSAGES);
        }
        Message[] msgs = new Message[1];
        msgs[0] = messages[i];
        folder.copyMessages(msgs, f);
        msgs[0].setFlag(Flags.Flag.DELETED, true);
    }

    public static void main(String[] args) {
        // com.sok.ssl.SokSSLSocketFactory ssl = new
        // com.sok.ssl.SokSSLSocketFactory();
        // System.out.print(ssl.getClass().getClassLoader().toString());

        // String host, String port, String protocol, String username, String
        // password
    	System.out.println("starting");
    	System.err.println("starting");
    	//try { 
    	//	EmailBean mailstore = new EmailBean();
        //	mailstore.connect("10.144.0.10", "993", "imaps", "bounce", "W3lcome!23");
    	//} catch (Exception e) { System.out.print(ActionBean.writeStackTraceToString(e)); } 
    	//System.out.println("done");
	}

}
