package com.sok.framework;
// Copyright (C) 2001 by Jason Hunter <jhunter_AT_acm_DOT_org>.
// All rights reserved.  Use of this class is limited.
// Please see the LICENSE for more information.


import java.io.*;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.*;
import javax.servlet.http.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A filter for easy semi-automatic handling of multipart/form-data requests 
 * (file uploads).  The filter capability requires Servlet API 2.3.
 * <p>
 * See Jason Hunter's June 2001 article in JavaWorld for a full explanation of
 * the class usage.
 *
 * @author <b>Jason Hunter</b>, Copyright &#169; 2001
 * @version 1.0, 2001/06/19
 */
public class UploadFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(UploadFilter.class);

	private FilterConfig config = null;
	private String dir = null;
	private int maxsize = 1024*512;
	private String errorpage = null;

	static int staticmaxsize = 0;

	String id = "";

	public void init(FilterConfig config) throws ServletException {
		this.config = config;

		// Determine the upload directory.  First look for an uploadDir filter
		// init parameter.  Then look for the context tempdir.
		dir = config.getInitParameter("uploadDir");
		errorpage = config.getInitParameter("errorPage");
		String sizestring = config.getInitParameter("maxSize");

		if (dir != null) {
			StringBuffer templatepath  = new StringBuffer();
			String realpath = config.getServletContext().getRealPath("");
			templatepath.append(realpath);

			char end = realpath.charAt(realpath.length()-1);
			if(end != '/' && end !='\\')
			{
				templatepath.append("/");
			}
			templatepath.append(dir);
			dir = templatepath.toString();
			//InitServlet.getSystemParams().put("uploadDir", dir);
		}
		else {
			throw new ServletException(
					"MultipartFilter: No upload directory found: set an uploadDir " +
							"init parameter or ensure the javax.servlet.context.tempdir " +
					"directory is valid");
		}
		if(sizestring!=null)
		{
			try{
				maxsize = Integer.parseInt(sizestring);
				staticmaxsize = maxsize;
			}catch(Exception e){}
		}
	}


	public static int getMaxUploadSize()
	{
		return(staticmaxsize);
	}

	public void destroy() {
		config = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		String type = req.getHeader("Content-Type");
		
		request.setAttribute("uploadDir", dir);

		if(logger.isTraceEnabled()) {
			logger.trace("Request: type={}, size={}", type, request.getContentLength());
		}

		// If this is not a multipart/form-data request continue
		if (type == null || !type.startsWith("multipart/form-data")) {
			chain.doFilter(request, response);
		}
		else {
			if(request.getContentLength() > maxsize)
			{
				((HttpServletResponse)response).sendRedirect(((HttpServletRequest)request).getContextPath()+"/" + errorpage);
			}
			else
			{
				id = KeyMaker.generate(10);
				Date d = new Date();
				UploadWrapper multi = null;
				try {
					multi = new UploadWrapper(req, dir, maxsize);
					Enumeration<String> param = multi.getFileNames();
					while (param.hasMoreElements()) {
						String s = (String) param.nextElement();
						logger.debug("{} - {} : {} = {}", id, new String[]{id, d.toString(), s, multi.getFilesystemName(s)});
						//System.out.println(id + " - " + d.toString() + " : " + s + " = " + multi.getFilesystemName(s));
					}

				} catch (IOException e) {
					d = new Date();
					logger.error("{} - {} upload failed {}", new String[]{id, d.toString(), e.getMessage()});
					//System.out.println(id + " - " + d.toString() + " upload failed " + e.getMessage());
					request.setAttribute("MultipartWrapperError",e.getMessage());
					//without this, if an exception were to be thrown you'd get an exception as multi is null rather than the io exception
					throw e;
				}
				if (InitServlet.getSystemParams() != null) InitServlet.getSystemParams().put("uploadDir", dir);
				multi.setAttribute("uploadDir", dir);
				request.setAttribute("MultipartWrapper",multi);
				chain.doFilter(multi, response);
				//System.out.println(id + " - " + d.toString() + " upload compleated ");
				logger.debug("{} - {} upload completed", id, d.toString());
			}
		}
	}
}
