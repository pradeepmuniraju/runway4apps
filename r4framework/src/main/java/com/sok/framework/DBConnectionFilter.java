package com.sok.framework;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.sql.*;
import javax.naming.*;
import java.sql.*;
import java.io.*;

public class DBConnectionFilter implements Filter
{
	Context ctx = null;
	Context envContext = null;
	DataSource ds = null;
	String jndiName = null;
	private static DBConnectionFilter _this;
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws ServletException, IOException
	{
		Connection con = null;
		try
		{
			try
			{
				con = ds.getConnection();
			}catch(Exception e){}
			request.setAttribute(ActionBean.CON, con);
			chain.doFilter(request,response);
		}
		finally
		{	
			if (con!=null) 
			{
				try{
					con.close();
				}catch (Exception ex){}
			}
		}
	}
	
	public static void setConnection(ServletRequest request) { 
		_this.addConnection(request); 
	}
	
	public static void closeConnection(ServletRequest request) {
		Connection con = null;
		try {
			try {
				con = (Connection)request.getAttribute(ActionBean.CON);
			} catch (Exception e) {}
		} finally {	
			if (con!=null) {
				try{
					con.close();
				}catch (Exception ex){}
			}
		}
	}
	
	/**
	 * Previously, this would add a connection, and then close it right away. So the request would have a closed connection attached.
	 * Not sure why you'd do that, so it's been removed. 
	 * @param request
	 */
	private void addConnection(ServletRequest request) { 
		Connection con = null;
		try
		{
			con = ds.getConnection();
		}catch(Exception e){}
		
		if(con != null)
			request.setAttribute(ActionBean.CON, con);
	}
	
	public void init(FilterConfig config)throws ServletException
	{
		_this = this; 
		jndiName = config.getServletContext().getInitParameter("sqlJndiName");
		try
		{
			ctx = new InitialContext();
			envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
			try{
				ds = (DataSource)envContext.lookup(jndiName);
			}catch(Exception e){}
			if(ds == null)
			{
				ds = (DataSource)ctx.lookup(jndiName);
			}
		}
		catch(Exception ex)
		{
			throw new ServletException(ex);
		}
	}
	
	public void destroy()
	{
		if (ctx != null) 
		{
			try{       
				ctx.close();
			}catch (NamingException ex){}
		}
		if (envContext != null) 
		{
			try{       
				envContext.close();
			}catch (NamingException ex){}
		}
	}
}
