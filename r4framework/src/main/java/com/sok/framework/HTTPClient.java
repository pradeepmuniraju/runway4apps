package com.sok.framework;

import java.net.*;
import java.io.*;
import java.util.*;
import javax.net.ssl.*;
import com.sok.framework.https.*;

public class HTTPClient
{
	StringBuffer logbuffer = null;
	static final String rn = "\r\n";
	boolean logToStdOut = false;
	boolean log = false;
	int status = 0;
	String response = null;
	Map header;
    
    public static final TrustManager[] trustAllCerts = new TrustManager[]{
        new X509TrustAllManager() };
    
    public static final HostnameVerifier hostnameVerifier = new EasyHostnameVerifier();
    
	public HTTPClient()
	{
		
	}

	public HTTPClient(boolean b)
	{
		log = b;
		if(log)
		{
			logbuffer = new StringBuffer();
		}
	}
	
	void log(String s)
	{
		if(log)
		{
			logbuffer.append(s);
		}
		if(logToStdOut)
		{
			System.out.print(s);
		}
	}

	public int getHTTPStatus()
	{
		return(status);
	}
	
	public String getHTTPResponse()
	{
		return(response);
	}
	
	public void setLogToStdOut(boolean b)
	{
		logToStdOut = b;
	}
	
	public String getLog()
	{
		if(logbuffer!=null)
		{
			return(logbuffer.toString());
		}
		return(null);
	}
	
    public void doGet(String urlstring, String query)
    {
        StringBuffer getstring = new StringBuffer();
        getstring.append(urlstring);
        getstring.append("?");
        getstring.append(query);
        connect(getstring.toString(), null, null);
    }
    
	public void doGet(String urlstring, RowBean data)
	{
		doPost(urlstring, data, true);
	}
	
	public void doGet(String urlstring, RowBean data, boolean all)
	{
		String param = data.toString(all); 
        doGet(urlstring, param);
	}
	
    public void doPost(String urlstring, String param)
    {
        connect(urlstring, param, null);
    }
    
	public void doPost(String urlstring, RowBean data)
	{
		doPost(urlstring, data, true);
	}
	
	public void doPost(String urlstring, RowBean data, boolean all)
	{
		String param = data.toString(all); 
		connect(urlstring, param, null);
	}
	
	public void doXMLPost(String urlstring, String xml)
	{
	   connect(urlstring, null, xml);
	}
	
    public Map getResponseHeader()
    {
        return(header);
    }
    
	void connect(String urlstring, String param, String xml)
	{
		OutputStreamWriter wr = null;
		BufferedReader rd = null;
		try
		{
			URL url = new URL(urlstring);

            if(url.getProtocol().equals("https"))
            {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                SSLSocketFactory sslSocketFactory = sc.getSocketFactory();

                HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);
                HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
            }
            
            URLConnection conn = url.openConnection();

			HttpURLConnection httpconn = null;
            
			if(conn instanceof HttpURLConnection)
			{
				httpconn = (HttpURLConnection)conn;
			}
			
			
			if(param!=null || xml!=null)
			{
            if(param!=null && xml!=null)
            {
               throw new com.sok.framework.generation.IllegalConfigurationException("Cannot add HTML parameters to XML request.");
            }
				conn.setDoOutput(true);
				wr = new OutputStreamWriter(conn.getOutputStream());

				if(param!=null)
				{
				   wr.write(param);
				}
				if(xml!=null)
				{
	            conn.setRequestProperty("Content-Type","text/xml");
				   wr.write(xml);
				}
				wr.flush();				
			}
			
			conn.connect();
			
			if(httpconn!=null)
			{
				status = httpconn.getResponseCode();
                header = httpconn.getHeaderFields();
				response = httpconn.getResponseMessage();
			}
			
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while((line = rd.readLine()) != null)
			{
				log(line);
				log(rn);
			}
		}
		catch(Exception e)
		{
			log("Error: ");
			log(rn);
			log(ActionBean.writeStackTraceToString(e));
		}
		finally
		{
			if(wr!=null)
			{
				try{
					wr.close();
				}catch(Exception e){}
			}
			if(rd!=null)
			{
				try{
					rd.close();
				}catch(Exception e){}
			}			
		}
	}
    

/*	
	public static void main(String[] args)
	{
		RowBean row = new RowBean();
		row.put("Hello","World");
		System.out.println(row.toString());
		HTTPClient client = new HTTPClient();
		client.setLogToStdOut(true);
		client.doGet("http://localhost/test.jsp", row);
		System.out.println(client.getHTTPResponse());
		System.out.println(client.getHTTPStatus());
	}
*/
}