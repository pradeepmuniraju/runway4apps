package com.sok.framework;

//TODO: FIXME - equals is defined without defining hashCode()
public class Range {
   
   private static final String[] lessThanTokens = {"UP TO","LESS THAN","<"};
   private static final String[] greaterTokens = {"OVER","GREATER THAN",">","PLUS", "+"};
   
   private static final String DEFAULT_DELIMITER = "-";
   
   private Double lower = null;
   private Double upper = null;
   private String delimiter = null;
   private String text = null;
      
   public Range(double lower, double upper) {
      this.lower = new Double(lower);
      this.upper = new Double(upper);
   }
      
   public Range(Double lower, Double upper) {
      this.lower = lower;
      this.upper = upper;
   }
   
   public Range(String text) {
      this(text, DEFAULT_DELIMITER);
   }
   
   public Range(String text, String delimiter) {
      this.delimiter = delimiter;
      this.text = text;
      
      if (text != null) {
         String[] values = text.split(delimiter);

         if (values.length == 1) {
            String upperText = text.toUpperCase();
            for (int i=0; i < lessThanTokens.length; i++) {
               if (upperText.indexOf(lessThanTokens[i]) != -1) {
                  lower = null;
                  upper = getValue(upperText);
                  break;
               }
            }
            for (int i=0; i < greaterTokens.length; i++) {
               if (upperText.indexOf(greaterTokens[i]) != -1) {
                  lower = getValue(upperText);
                  upper = null;
                  break;
               }
            }
            if (upper == null && lower == null) {
               lower = getValue(values[0]);
               upper = getValue(values[0]);
            }
         }
         else {
            lower = getValue(values[0]);
            upper = getValue(values[1]);
         }
      }
   }
   
   public void setUpperLimit(double upper) {
      this.upper = upper;
   }
   
   public void setUpperLimit(Double upper) {
      this.upper = new Double(upper);
   }
   
   public void setUpperLimit(String upper) {
      this.upper = getValue(upper);
   }
   
   public void setLowerLimit(double lower) {
      this.lower = lower;
   }
   
   public void setLowerLimit(Double lower) {
      this.lower = new Double(lower);
   }
   
   public void setLowerLimit(String lower) {
      this.lower = getValue(lower);
   }
   
   public Double getLowerLimit() {
      return this.lower;
   }
   public Double getUpperLimit() {
      return this.upper;
   }
   
   private Double getValue(String text) {
      try {
         String numberOnlyText = StringUtil.toDecimal(text);
         
         return new Double(numberOnlyText);
      }
      catch (Exception e) {
         return null;
      }
   }
      
   public boolean inRange(double value) {
      if (upper == null && lower == null) {
         return false;
      }
      else if (upper != null && upper < value) {
         return false;
      }
      else if (lower != null && lower > value) {
         return false;
      }
      return true;
   }
   
   public String getText() {
      if (text == null) {
         return toString();
      }
      else {
         return text;
      }
   }
   
   public String toString() {
      if (upper != null && lower != null) {
         return lower + delimiter + upper;
      }
      else if (lower != null) {
         return ">=" + lower;
      }
      else if (upper != null) {
         return "<=" + upper;
      }
      return ActionBean._blank;
   }
   
   public boolean equals(Object obj) {
      if (obj instanceof Range) {
         Range range = (Range)obj;
         
         boolean eqLower = this.getLowerLimit() == range.getLowerLimit() || (this.getLowerLimit() != null && this.getLowerLimit().equals(range.getLowerLimit()));
         
         if (eqLower) {
            return this.getUpperLimit() == range.getUpperLimit() || (this.getUpperLimit() != null && this.getUpperLimit().equals(range.getUpperLimit()));
         }
         return false;
      }
      else {
         return false;
      }
   }
}