package com.sok.framework;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import com.sok.runway.offline.runwayImport.ImportConfiguration;

public class SpecManager
{
	private static final Logger logger = LoggerFactory.getLogger(SpecManager.class);
	private static final HashMap<String, TableSpec> tms = new HashMap<String, TableSpec>();
	private static final HashMap<String, ViewSpec> vms = new HashMap<String, ViewSpec>();
	//private static HashMap rms = new HashMap();
	private static final HashMap<String, ReportSetSpec> rsms = new HashMap<String, ReportSetSpec>();
	//private static HashMap ams = new HashMap();
    private static final HashMap<String, DisplaySpec> dms = new HashMap<String, DisplaySpec>();     
    private static final HashMap<String, ImportConfiguration> ims = new HashMap<String, ImportConfiguration>();       
	private static RelationSpec rspec = null;
    private static PermissionSpec pspec = null;	
	private static final String spechome = "specs/";
	private static ServletContext context = null;

	private SpecManager()
	{

	}

	/**
	 * Updated to allow clearing of ServletContext 21/03
	 * @param sc
	 */
	public static void setServletContext(ServletContext sc)
	{
		logger.debug("setServletContext, context={}, sc={}", context == null ? "null" : "nn", sc == null ? "null" :"nn");
		if(context==null || sc == null)
		{
			logger.debug("setServletContext to " + (sc == null ? "null" : "not null")); 
			context = sc;
		}
	}  
 /*   
	public static ActionSpec getActionSpec(String name)
	{
		ActionSpec as = (ActionSpec)ams.get(name);
		if(as==null)
		{
			as = loadActionSpec(name);
		}
		return(as);
	}	
*/	
	public static TableSpec getTableSpec(String name)
	{
		TableSpec ts = tms.get(name);
		if(ts==null)
		{
			ts = loadTableSpec(name);
		}
		return(ts);
	}

	public static RelationSpec getRelationSpec()
	{
		if(rspec==null)
		{
			loadRelationSpec();
		}
		return(rspec);
	}
	
   public static PermissionSpec getPermissionSpec()
   {
      if(pspec==null)
      {
         loadPermissionSpec();
      }
      return(pspec);
   }	

	public static ViewSpec getViewSpec(String name)
	{
		ViewSpec vs = vms.get(name);
		if(vs==null)
		{
			vs = loadViewSpec(name);
		}
		return(vs);
	}

    public static DisplaySpec getDisplaySpec(String name)
    {
        DisplaySpec ds = dms.get(name);
        if(ds==null)
        {
            ds = loadDisplaySpec(name);
        }
        return(ds);
    }       
/*
	public static synchronized ReportSpec getReportSpec(String name)
	{
		ReportSpec rs = (ReportSpec)rms.get(name);
		if(rs==null)
		{
			rs = loadReportSpec(name);
		}
		return(rs);
	}
*/	
	public static ReportSetSpec getReportSetSpec(String name)
	{
		ReportSetSpec rs = rsms.get(name);
		if(rs==null)
		{
			rs = loadReportSetSpec(name);
		}
		return(rs);
	}
	
   public static ImportConfiguration getImportConfiguration(String name)
   {
      ImportConfiguration ic = ims.get(name);
      if(ic==null)
      {
         ic = loadImportConfiguration(name);
      }
      return(ic);
   }	

    public static synchronized void reloadDisplaySpec(String name)
    {
        dms.put(name,null);
        loadDisplaySpec(name);
    }    
 /*   
	public static synchronized void reloadActionSpec(String name)
	{
		ams.put(name,null);
		loadActionSpec(name);
	}	
*/	
	public static synchronized void reloadTableSpec(String name)
	{
		tms.put(name,null);
		loadTableSpec(name);
	}

	public static synchronized void reloadViewSpec(String name)
	{
		vms.put(name,null);
		loadViewSpec(name);
	}
/*
	public static synchronized void reloadReportSpec(String name)
	{
		loadReportSpec(name);
	}
*/
	public static synchronized void reloadReportSetSpec(String name)
	{
		rsms.put(name,null);
		loadReportSetSpec(name);
	}	

	public static synchronized void reloadRelationalSpec()
	{
		rspec = null;
		loadRelationSpec();
	}
	
   public static synchronized void reloadPermissionSpec()
   {
      pspec = null;
      loadPermissionSpec();
   }	
	
   public static synchronized void reloadImportConfiguration(String name)
   {
       ims.put(name,null);
       loadImportConfiguration(name);
   }      
   
	protected static synchronized void loadRelationSpec()
	{	
		if(rspec==null)
		{
			rspec = new RelationSpec(RelationSpec.NAME);
		}
	}
	
   protected static synchronized void loadPermissionSpec()
   {  
      if(pspec==null)
      {
         pspec = new PermissionSpec(PermissionSpec.NAME);
      }
   }	
/*
	protected static synchronized ActionSpec loadActionSpec(String name) 
	{
		ActionSpec as = (ActionSpec)ams.get(name);
		if(as==null)
		{
			as = new ActionSpec(getFilePath(name));
			as.setName(name);
			ams.put(name,as);
		}
		return(as);
	}	
*/
    protected static synchronized DisplaySpec loadDisplaySpec(String name) 
    {
        DisplaySpec ds = dms.get(name);
        if(ds==null)
        {
            ds = new DisplaySpec(name);
            //ds.setName(name);
            dms.put(name,ds);
        }
        return(ds);
    }
    
	protected static synchronized TableSpec loadTableSpec(String name) 
	{
		TableSpec ts = tms.get(name);
		if(ts==null)
		{
			// check to see if a specific spec of the same name exists, if yes load that and if not, load this.
			// Use case - ContactGroups where Unique key on ContactID-GroupID or ContactID-GroupID-RepUserID 
			ts = new TableSpec(getFilePath(name));
			ts.setName(name);
			tms.put(name,ts);
		}
		return(ts);
	}
	
	protected static synchronized ViewSpec loadViewSpec(String name) 
	{
		ViewSpec vs = vms.get(name);
		if(vs==null)
		{
			vs = new ViewSpec(getFilePath(name));
			vs.setName(name);	
			vms.put(name,vs);
		}
		return(vs);
	}	
/*
	protected static synchronized ReportSpec loadReportSpec(String name) 
	{
		ReportSpec rs = new ReportSpec(getFilePath(name));
		rs.setName(name);

		rms.put(name,rs);
		return(rs);
	}	
*/
	protected static synchronized ReportSetSpec loadReportSetSpec(String name) 
	{
		ReportSetSpec rs = rsms.get(name);
		if(rs==null)
		{
			rs = new ReportSetSpec(getFilePath(name));
			rs.setName(name);		
			rsms.put(name,rs);
		}
		return(rs);
	}	
	
   protected static synchronized ImportConfiguration loadImportConfiguration(String name) 
   {
      ImportConfiguration ic = ims.get(name);
      if(ic==null)
      {
         ic = new ImportConfiguration(getFilePath(name));
         ic.setName(name);    
         ims.put(name,ic);
      }
      return(ic);
   }  	
/*
	public static synchronized void saveActionSpec(ActionSpec as)
	{
		if(as!=null)
		{
			as.save();
			ams.put(as.getName(),as);
		}
	}	
*/	
	public static synchronized void saveTableSpec(TableSpec ts)
	{
		if(ts!=null)
		{
			ts.save();
			tms.put(ts.getName(),ts);
		}
	}

	public static synchronized void saveViewSpec(ViewSpec vs)
	{
		if(vs!=null)
		{
			vs.save();
			vms.put(vs.getName(),vs);
		}
	}

	public static synchronized void saveRelationSpec(RelationSpec rs)
	{
		rspec = rs;
		rspec.save();
	}

    public static String getFilePath(String name, String parentfolder, String subfolder)
    {
        StringBuffer specname = new StringBuffer();
        if(context!=null)
        {
            String contextPath = context.getRealPath("./");
            if (StringUtils.isNotBlank(contextPath)) {
	            specname.append(contextPath);
	            char end = contextPath.charAt(contextPath.length()-1);
	            if(end != '/' && end !='\\')
	            {
	                specname.append("/");
	            }
            }
        }
        if(parentfolder!=null)
        {
            specname.append(parentfolder);  
        }
        specname.append(spechome);
        if(subfolder!=null)
        {
            specname.append(subfolder);  
        }        
        specname.append(name);
        specname.append(".xml");
        return(specname.toString());
    }    
    
	public static String getName(String name)
	{
        int prefixbegin = name.indexOf('[');
        int prefixend = name.indexOf(']');
        if( prefixbegin >= 0 && prefixend > 0)
        {
            name = name.substring(prefixend+1, name.length());
        }
        
        int suffixbegin = name.indexOf('{');
        int suffixend = name.indexOf('}'); 
        if( suffixbegin >= 0 && suffixend > 0)
        {
            name = name.substring(suffixend+1, name.length());
        }    
        
        int index = name.lastIndexOf('/');
        if (index >= 0) {
        	name = name.substring(index+1);
        }

        return(name);
	}
    
	public static String getFilePath(String name)
	{
        String parentfolder = null;
        String subfolder = null;
        
        int prefixbegin = name.indexOf('[');
        int prefixend = name.indexOf(']');
        if( prefixbegin >= 0 && prefixend > 0)
        {
            parentfolder = name.substring(prefixbegin+1, prefixend);
            name = name.substring(prefixend+1, name.length());
        }
        
        int suffixbegin = name.indexOf('{');
        int suffixend = name.indexOf('}'); 
        if( suffixbegin >= 0 && suffixend > 0)
        {
            subfolder = name.substring(suffixbegin+1, suffixend);
            name = name.substring(suffixend+1, name.length());
        }      
        
		return(getFilePath(name, parentfolder, subfolder));
	}

	public static void save(Document document, String name)
	{
		FileWriter fo = null;
		try
 		{
			fo = new FileWriter(getFilePath(name));
			document.write( fo );
		}
 		catch (Exception e)
 		{
 			System.out.println(e.getMessage());
		}
		finally
		{
			if(fo!=null){try{fo.close();}catch(Exception e){}}
		}
	}
	
	/**
	 * Define an interface to a certain action which should be performed on all tables / views / etc
	 * @author mike
	 * @param <T>
	 */
	public interface SpecAction<T> {
		public T doIt(String s); 
	}
	
	public static String performViewOperations(boolean specific, SpecAction<String> sa) {
		
		StringBuilder specname = new StringBuilder();
		specname.append(context.getRealPath(""));
		specname.append("/specs/");
		
		java.io.File specfolder = new java.io.File(specname.toString());
		
		List<String> fileList = new ArrayList<String>(50);
		addFilePaths(fileList, specfolder,null);
		
		if(specific) { 
			StringBuilder specificspec = new StringBuilder();
			specificspec.append(context.getRealPath(""));
			specificspec.append("/specific/specs/");	
			java.io.File specificspecfolder = new java.io.File(specificspec.toString());
			addFilePaths(fileList, specificspecfolder, "[specific/]");
		} 
		
		StringBuilder ret = new StringBuilder(); 
		String v = null;
		for(String specvalue: fileList) {
			
			if(specvalue.equals("Relations") || specvalue.indexOf("ReportSet")!=-1 || specvalue.indexOf("Display")>0 || specvalue.indexOf("imports/")>=0 || specvalue.indexOf("feeds/") >=0 || specvalue.indexOf("CVS") >= 0)
			{
				// do nothing, this isn't a view
			}
			else 
			{
				// This will likely be a view, we're not sure though. 
				try {
					if((v = sa.doIt(specvalue))!=null)
						ret.append(v);
				} catch (IllegalArgumentException iae) {
					// this will occur if the file is not a table spec.
				} catch (Exception e) {
					logger.debug("Error with view path={}, error={}", specvalue, e.getMessage());
					logger.trace("Error stack trace", e); 
				}
			}
		}
		return ret.toString(); 
	}
	
	public static <T> Collection<T> performTableOperations(boolean specific, SpecAction<T> sa) {
		return performTableOperations(specific, sa, new ArrayList<T>());
	}
	
	public static <T> Collection<T> performTableOperations(boolean specific, SpecAction<T> sa, Collection<T> ret) {
		
		StringBuilder specname = new StringBuilder();
		specname.append(context.getRealPath(""));
		specname.append("/specs/");
		
		java.io.File specfolder = new java.io.File(specname.toString());
		
		List<String> fileList = new ArrayList<String>(50);
		addFilePaths(fileList, specfolder,null);
		
		if(specific) { 
			StringBuilder specificspec = new StringBuilder();
			specificspec.append(context.getRealPath(""));
			specificspec.append("/specific/specs/");	
			java.io.File specificspecfolder = new java.io.File(specificspec.toString());
			addFilePaths(fileList, specificspecfolder, "[specific/]");
		} 
		T v = null;
		for(String specvalue: fileList) {
			
			if(specvalue.equals("Relations"))
			{
				//setTypeRadio("relation");
			}
			else if(specvalue.endsWith("View"))
			{
				//setTypeRadio("view");
			}
			else if(specvalue.indexOf("ReportSet")>=0)
			{
				//setTypeRadio("reportset");
			}
			else if(specvalue.indexOf("Display")>0)
			{
				//setTypeRadio("display");
			}
			else if(specvalue.indexOf("imports/")>=0)
			{
				//setTypeRadio("import");
			}					
			else if(specvalue.indexOf("feeds/") >=0 || specvalue.indexOf("CVS") >= 0)
			{
				
			}
			else 
			{
				// This will likely be a table, we're not sure though. 
				try {
					if((v = sa.doIt(specvalue))!=null)
						ret.add(v);
				} catch (IllegalArgumentException iae) {
					// this will occur if the file is not a table spec.
				} catch (Exception e) {
					logger.debug("Error with table path={}, error={}", specvalue, e.getMessage());
					logger.trace("Error stack trace", e); 
				}
			}
		}
		return ret;
	}
	
	private static void addFilePaths(List<String> retSpecs, java.io.File parent, String prefix) {
		if(parent.isDirectory())
   		{
			for(java.io.File f: parent.listFiles()) { 
				if(f.isDirectory()) {
					addFilePaths(retSpecs, f, prefix != null ? new StringBuilder().append(prefix).append(f.getName()).append("/").toString() : (f.getName() + "/"));
				} else {
					String filename = f.getName();
					if(filename.endsWith(".xml")) {
						try { 
							StringBuilder value = new StringBuilder(); 
							if(prefix != null && prefix.indexOf("displays")!=0)
							{
								value.append(prefix);
							}
							value.append(filename.substring(0,filename.indexOf(".")));
							retSpecs.add(value.toString());
						}
						catch(Exception e)
						{
						   logger.error("Error adding spec from file={}", filename);
						}
					}// else {
					//	logger.warn("Unexpected filename={}{}", prefix, filename);
					//}
				}
			}
   		}
	}

	public static void main(String[] args)
	{
		try
		{


			PermissionSpec ps = getPermissionSpec();

			for(int i=0; i<ps.getCategoryLength(); i++)
			{
				System.out.print(i);
				System.out.println(ps.getCategoryLabel(i));
	         for(int j=0; j<ps.getModuleLength(i); j++)
	         {
	            System.out.print(j);
	            System.out.print(ps.getModuleLabel(i, j));
	            System.out.print(" - ");
	            System.out.println(ps.getModuleName(i, j));
	         }

			}
			for(int j=0; j<ps.getPermissionLength(); j++)
			{
				System.out.print(j);
				System.out.print(ps.getPermissionName(j));
            System.out.print(" - ");
            System.out.println(ps.getPermissionLabel(j));
			}
			 /*
			//ViewSpec vs = getViewSpec("ContactView");

			System.out.println(vs.getPrimaryKeyName());
			System.out.println(vs.getTableName());
			System.out.println(vs.getFieldsLength());
			for(int m=0; m<vs.getFieldsLength(); m++)
			{
				System.out.print(m);
				System.out.print(vs.getFieldName(m));
				System.out.print(vs.getFieldType(m));
				System.out.print(vs.getFieldTypeName(m));
				System.out.println(vs.getFieldIndex(m));
			}
			for(int n=0; n<vs.getRelationshipsLength(); n++)
			{
				System.out.print(n);
				System.out.print(vs.getRelationshipName(n));
				System.out.print(vs.getRelatedTableName(n));
				System.out.print(vs.getMapKey(n));
				System.out.println(vs.getLocalKey(n));
			}
*/
			//System.out.println("***************************************************");
			
			//ViewSpec vs = getViewSpec("ResponseExcelView");
			//TableBean bean = new TableBean();
			//bean.setViewSpec(vs);
			//bean.setAction("-select");
			//bean.setColumn("ResponseID","52938008C704F422C3E151C20481E182");
			//System.out.println(bean.createdDataOnIDStatment());	
			//System.out.println(vs.getLocalLength());

			
			//ViewSpec vs = new ViewSpec("Hello","World");
			//vs.addData("hello","world","1","2");
			//vs.addForeignData("hello","world","1","2","3");
			//vs.addForeignCount("hello","world","hello = world");
			//System.out.println(vs.getLocalLength());
			//vs.save();

			//TableBean bean = new TableBean();
			//bean.setViewSpec(vs);
			//bean.setAction("search");
			//bean.setColumn("ContactNotes.NoteMessage.Subject","%Subject%");
			//bean.setColumn("ContactNotes.Type","Email");
			//bean.setColumn("-ContactNotes","omit");
			//bean.setColumn("FirstName","Jong");
			//bean.setColumn("ResponseCount","0-1");
			//bean.setSort(0,"Contacts.FirstName",null);
			//bean.generateSQLStatment();
			//System.out.println(bean.getSearchStatement());	
			//System.out.println(bean.getError());
			//System.out.println(new java.sql.Timestamp(new java.util.Date().getTime()));
			//System.out.println(java.sql.Types.FLOAT);
			//System.out.println(java.sql.Types.INTEGER);
		}
		catch(Exception e){System.out.println(e);}
	}

}
	
