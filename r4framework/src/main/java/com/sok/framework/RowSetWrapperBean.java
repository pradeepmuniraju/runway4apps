package com.sok.framework;

import com.sok.framework.generation.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class RowSetWrapperBean implements TableSource
{
	ResultSet result = null;
	Connection con = null;
	Statement stmt = null;
	boolean closeconnection = true;

	int rowindex = 0;

	TableData searchbean = null;
	TableData resultbean = new RowBean();

	String[] columnnames = null;
	int colcount = 0;

	public void setSearchBean(TableData bean)
	{
		this.searchbean = bean;
		resultbean.setViewSpec(this.searchbean.getViewSpec());
		resultbean.setLocale(this.searchbean.getLocale());
	}

	public void setRequest(HttpServletRequest request)
	{
		ActionBean.setRequest(request, this);
	}

	public void setResultBean(TableData bean)
	{
		this.resultbean = bean;
	}

	public TableData getSearchBean()
	{
		return(searchbean);
	}

	public TableData getResultBean()
	{
		return(resultbean);
	}

	public void setCloseConnection(boolean b)
	{
		closeconnection = b;
	}

	public void setStatement(Statement stmt)
	{
		this.stmt = stmt;
	}

	public void setConnection(ServletRequest request)
	{
		Connection conn = (Connection)request.getAttribute(ActionBean.CON);
		if(conn!=null)
		{
			this.con = conn;
			closeconnection = false;
		}
	}		

	public void setConnection(Object conobj)
	{
		if(conobj instanceof ServletRequest)
		{
			setConnection((ServletRequest)conobj);
		}
		else
		{
			setConnection((Connection)conobj);
		}
	}

	public void setConnection(Object conobj, boolean close)
	{
		if(conobj instanceof ServletRequest)
		{
			setConnection((ServletRequest)conobj);
		}
		else
		{
			setConnection((Connection)conobj, close);
		}
	}

	public void setConnection(Connection con) {
		setConnection(con,false);
	}

	public void setConnection(Connection con, boolean close)
	{
		if (con != null) {
			this.con = con;
			closeconnection = close;
		}
	}

	public boolean getCloseConnection()
	{
		return(closeconnection);
	}

	public Connection getConnection()
	{
		return(con);
	}

	public void setResultSet(ResultSet current)
	{
		result = current;
	}

	public int getRowIndex()
	{
		return(rowindex);
	}

	public int getColumnCount()
	{
		return(colcount);
	}

	public String getColumnName(int i)
	{
		return(columnnames[i-1]);
	}

	public boolean getResults()
	{
		rowindex = 0;

		try
		{
			ActionBean.retrieveResultSet(this);
			if (result == null) return false;
			ResultSetMetaData meta = result.getMetaData();
			colcount = meta.getColumnCount();
			columnnames = new String[colcount];
			for(int i=1; i<=colcount; i++)
			{
				//TODO: columnnames[i-1] = meta.getColumnLabel(i);
				columnnames[i-1] = meta.getColumnLabel(i);
			}
			return(true);
		}
		catch(Exception e)
		{
			resultbean.setError(e.toString());
			resultbean.setError(ActionBean.RN);
		}

		return(false);
	}

	public boolean getNext()
	{
		if(result!=null)
		{
			resultbean.clear();
			try
			{
				if(ActionBean.initFromDB(result,this))
				{
					rowindex++;
					return(true);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				resultbean.setError(e.toString());
				resultbean.setError(ActionBean.RN);
			}
		}
		return(false);
	}

	public boolean skip()
	{
		if(result!=null)
		{

			try
			{
				rowindex++;
				return(result.next());
			}
			catch(Exception e)
			{
				resultbean.setError(e.toString());
				resultbean.setError(ActionBean.RN);
			}
		}
		return(false);
	}

	public boolean skipTo(int skip)
	{
		if(result!=null)
		{

			try
			{
				boolean b = result.absolute(skip);
				if(b)
				{
					rowindex = skip;
				}
				return(b);

			}
			catch(Exception e)
			{
				resultbean.setError(e.toString());
				resultbean.setError(ActionBean.RN);
			}
		}
		return(false);
	}

	public void close()
	{
		if(stmt!=null)
		{
			try{
				stmt.close();
			}catch(Exception e){}
		}
		if(result!=null)
		{
			try{
				result.close();
			}catch(Exception e){}
		}
		if(con!=null)
		{
			if(closeconnection)
			{
				try{
					con.close();
				}catch(Exception e){}
			}
			con = null;
		}
	}

	public void setError(String msg)
	{
		if(searchbean != null)
		{
			searchbean.setError(msg);
		}
	}

	public String getJndiName()
	{
		if(searchbean != null)
		{
			return(searchbean.getJndiName());
		}
		return(null);
	}
}
