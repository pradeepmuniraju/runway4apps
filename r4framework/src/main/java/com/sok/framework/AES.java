/*
 * AES.java
 *
 * Created on May 15, 2007, 5:29 PM
 *
 * This program generates a AES key, retrieves its raw bytes, and
 * then reinstantiates a AES key from the key bytes.
 * The reinstantiated key is used to initialize a AES cipher for
 * encryption and decryption.
 */

package com.sok.framework;
import com.sok.framework.generation.*;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.io.*;

/**
 *
 * @author root
 */
public class AES
{
   private String characterEncoding;
   private Cipher encryptCipher;
   private Cipher decryptCipher;
   
   private static final String _AES = "AES";
   private static final String _default = "NQdvaOA3bJ3Sc13tko00TA==";   
   
   public AES()
   {
      init(_default);
   }
   
   public AES(String key)
   {
      if(key!=null && key.length()!=0)
      {
         init(key);
      }
      else
      {
         init(_default);
      }
   }
   
   public AES(SecretKeySpec skeySpec)
   {
      init(skeySpec);
   }
   
   public void init(String key)
   {
      try
      {
         byte[] raw = Base64Codec.decode(key.toCharArray());
         SecretKeySpec skeySpec = new SecretKeySpec(raw, _AES);
         init(skeySpec);
      }
      catch(Exception e)
      {
         throw new IllegalConfigurationException(e);
      }
   }
   
   public void init(SecretKeySpec skeySpec)
   {
      try
      {
         encryptCipher = Cipher.getInstance(_AES);
         decryptCipher = Cipher.getInstance(_AES);
         encryptCipher.init(Cipher.ENCRYPT_MODE, skeySpec);
         decryptCipher.init(Cipher.DECRYPT_MODE, skeySpec);
      }
      catch(Exception e)
      {
         throw new IllegalConfigurationException(e);
      }
   }
   
   public static String generateKey()
   {
      try
      {
         KeyGenerator kgen = KeyGenerator.getInstance(_AES);
         kgen.init(128); // 192 and 256 bits may not be available
         SecretKey skey = kgen.generateKey();
         byte[] raw = skey.getEncoded();
         String key = new String(Base64Codec.encode(raw));
         return(key);
      }
      catch(Exception e)
      {
         throw new IllegalConfigurationException(e);
      }
   }
   
   public void setCharacterEncoding(String characterEncoding)
   {
      this.characterEncoding = characterEncoding;
   }
   
   public String encrypt(String str)
   {
      try
      {
         byte[] bytes = null;
         if(characterEncoding!=null)
         {
            bytes = str.getBytes(characterEncoding);
         }
         else
         {
            bytes = str.getBytes();
         }
         
         byte[] encryptedBytes = this.encryptCipher.doFinal(bytes);
         String encodedEncrypted = new String(Base64Codec.encode(encryptedBytes));
         return(encodedEncrypted);
      }
      catch(Exception e)
      {
         throw new IllegalConfigurationException(e);
      }
   }
   
   public String decrypt(String str)
   {
      try
      {
         byte[] encryptedBytes = Base64Codec.decode(str.toCharArray());
         byte[] bytes = this.decryptCipher.doFinal(encryptedBytes);
         String decodeddecrypted = null;
         if(characterEncoding!=null)
         {
            decodeddecrypted = new String(bytes, characterEncoding);
         }
         else
         {
            decodeddecrypted = new String(bytes);
         }
         return(decodeddecrypted);
      }
      catch(Exception e)
      {
         throw new IllegalConfigurationException(e);
      }
   }
   
   public static void main(String[] args) throws Exception
   {
      
      String message="Hello Hello324324235#@$#@!^$&&^(*";
      
      
      //String key = AES.generateKey();
      
      //System.out.println("key: " + key);
      
      // Instantiate the cipher
      
      //AES aes = new AES(key);
      AES aes = new AES();
      
      String encrypted = aes.encrypt(message);
      
      System.out.println("encrypted string: " + encrypted);
      
      System.out.println("Original string: *" +
            aes.decrypt(encrypted)+"*");
   }

}
