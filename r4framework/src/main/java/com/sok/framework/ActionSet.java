package com.sok.framework;

import java.sql.Connection;
import java.util.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


public class ActionSet
{
	
	public static void doActionSet(RowBean row, HttpServletRequest request, HttpServletResponse response)
	{
		row.setUpdatedCount(0);

		ActionSpec aspec = row.getActionSpec();
		
		row.parseRequest(request, aspec.isAllowEmpty(), aspec.isKeepMulti(), null);
		
		//TableSpec tspec = row.getTableSpec();
		
		try
		{	
			int actions = aspec.getActionCount();
			RowBean arow = new RowBean();

			int updated = -1;
			for(int i = 0; i<actions; i++)
			{
				updated = doAction(request, response, row, arow, i);
			}
		}
		catch(Exception ex)
		{
			row.setError(ActionBean.writeStackTraceToString(ex));
			//throw new ServletException(ex.getMessage(), ex);
		}
		finally
		{
			row.close();		
		}			
	}	
	
	protected static int doActionSteps(HttpServletRequest request, HttpServletResponse response, RowBean row, RowBean arow, int index)
	{
		ActionSpec aspec = row.getActionSpec();

		int steps = aspec.getActionStepCount(index);
		int updated = 0;
		for(int i=0; i<steps ;i++)
		{
			updated = updated + doActionStep(request, response, row, arow, index, i);
		}
		return(updated);
	}
	
	protected static int doAction(HttpServletRequest request, HttpServletResponse response, RowBean row, RowBean arow, int index)
	{
		int updated = -1;

		ActionSpec aspec = row.getActionSpec();
		if(aspec!=null)
		{
			String type = aspec.getActionType(index);

			if(type!=null)
			{
				if(type.equals(ActionSpec._subform))
				{
					if(aspec.hasActionSubform(index))
					{
						ActionBean.parseRequest(row, arow, aspec.getActionSubform(index));
					}
					else if(aspec.hasActionSubformSet(index))
					{
						String setparamname = aspec.getActionSubformSetParamName(index);
						String setparamvalue = request.getParameter(setparamname);
						StringTokenizer st = new StringTokenizer(setparamvalue,",");
						
						while(st.hasMoreTokens())
						{
							ActionBean.parseRequest(row, arow, st.nextToken());
							updated = updated + doActionSteps(request, response, row, arow, index);
							arow.clear();
						}
					}
				}
				else if(type.equals(ActionSpec._history))
				{
					//arow.setConnection(con);
					updated = doHistory(request, response, row, arow, index);
					arow.clear();
				}
				else if(type.equals(ActionSpec._extension))
				{
					if(aspec.hasActionMultiOn(index))
					{
						String multiname = aspec.getActionMultiOn(index);
						Object mulitobj = row.get(multiname);
						if(mulitobj instanceof String[])
						{
							String[] multivalues = (String[])mulitobj;
							if(multivalues!=null)
							{
								updated = 0;
								for(int i=0; i<multivalues.length; i++)
								{
									row.put(multiname, multivalues[i]);
									updated = updated + doActionSteps(request, response, row, arow, index);
									arow.clear();
								}
								row.put(multiname, multivalues);
							}
						}
						else
						{						
							updated = doActionSteps(request, response, row, arow, index);
							arow.clear();							
						}
					}
					else
					{
						updated = doActionSteps(request, response, row, arow, index);
						arow.clear();
					}
				}				
				else if(type.equals(ActionSpec._main))
				{
					updated = doActionSteps(request, response, row, row, index);
				}
				if(arow.hasError())
				{
					row.setError("ActionSet error on action - ");
					row.setError(String.valueOf(index));
					row.setError("\r\n");
					row.setError(arow.getError());
				}
			}
			else
			{
				System.out.print("type is null on ");
				System.out.print(index);
			}
		}
		else
		{
			System.out.print("aspec is null on ");
			System.out.print(index);
		}
		return(updated);
	}
	
	protected static int doHistory(HttpServletRequest request, HttpServletResponse response, RowBean row, RowBean arow, int index)
	{
		TableSpec tspec = row.getTableSpec();
		ActionSpec aspec = row.getActionSpec();
		arow.setTableSpec(tspec);
		arow.setRequest(request);
		String lastkeyname = aspec.getActionCurrentHistoryKey(index);
		String keyname = aspec.getActionHistoryKey(index);
		String pkey = tspec.getPrimaryKeyName();
		arow.setColumn(pkey,row.getString(pkey));
		arow.getColumnsFormDB(lastkeyname);
		String lastvalue = arow.getString(lastkeyname);
		String value = row.getString(keyname);
		int updated = 0;
		if(arow.hasError())
		{
			row.setError("ActionSet error on history action - ");
			row.setError(String.valueOf(index));
			row.setError("\r\n");
			row.setError(arow.getError());
		}
		arow.clear();
		if(!value.equals(lastvalue))
		{
			updated = doActionSteps(request, response, row, arow, index);
		}
		return(updated);
	}

	protected static int doActionStep(HttpServletRequest request, HttpServletResponse response, RowBean row, RowBean arow, int action, int step)
	{
		ActionSpec aspec = row.getActionSpec();
		String type = aspec.getActionStepType(action, step);
		int updated = 0;
		if(type.equals(ActionSpec._get))
		{
			String to = aspec.getActionStepTo(action, step);
			String from = aspec.getActionStepFrom(action, step);
			Object value = row.get(from);
			if(value==null && aspec.hasActionStepDefault(action, step))
			{
				value = aspec.getActionStepDefault(action, step);
			}
			arow.put(to, value);
		}
		else if(type.equals(ActionSpec._put))
		{
			String to = aspec.getActionStepTo(action, step);
			String from = aspec.getActionStepFrom(action, step);
			row.put(to,arow.get(from));
		}
		else if(type.equals(ActionSpec._putvalue))
		{
			String to = aspec.getActionStepTo(action, step);
			String from = aspec.getActionStepFrom(action, step);
			arow.put(from, to);
		}	
		else if(type.equals(ActionSpec._clear))
		{
			arow.clear();
		}				
		else if(type.equals(ActionSpec._doaction))
		{
			boolean autovalues = aspec.getActionStepAutoValues(action, step);
			String table = aspec.getActionTable(action);
			if(table!=null && table.length()!=0)
			{
				arow.setTableSpec(table);
			}			
			arow.setRequest(request);
			arow.doAction(autovalues);
			updated = arow.getUpdatedCount();
		}
		else if(type.equals(ActionSpec._setaction))
		{
			String to = aspec.getActionStepTo(action, step);
			if(to!=null)
			{
				arow.setAction(to);
			}
			else
			{
				arow.setAction(row.getAction());
			}
		}
		else if(type.equals(ActionSpec._setviewspec))
		{
			String to = aspec.getActionStepTo(action, step);
			arow.setViewSpec(to);
		}
		else if(type.equals(ActionSpec._settablespec))
		{
			String to = aspec.getActionStepTo(action, step);
			arow.setTableSpec(to);
		}
		else if(type.equals(ActionSpec._parserequest))
		{
			arow.parseRequest(request);
		}
		else if(type.equals(ActionSpec._history))
		{
			doHistoryStep(request, row, arow, action, step);
		}		
		return(updated);
	}	

	private static String getLastHistroyItemStmt(RelationSpec rspec, int rindex, String keyname, String fkeyname, String fkeyvalue)
	{
		String fromtable = rspec.getFromTableName(rindex);
		String totable = rspec.getToTableName(rindex);
		
		StringBuffer stmt = new StringBuffer();
		stmt.append("select ");
		stmt.append(keyname);
		stmt.append(" from ");
		stmt.append(totable);
		stmt.append(" where exists(select ");
		stmt.append(fkeyname);
		stmt.append(" from ");
		stmt.append(fromtable);
		stmt.append(" where ");
		stmt.append(fromtable);
		stmt.append(".");
		stmt.append(rspec.getFromKey(rindex));
		stmt.append(" = ");
		stmt.append(totable);
		stmt.append(".");
		stmt.append(rspec.getToKey(rindex));
		stmt.append(" and ");
		stmt.append(fromtable);
		stmt.append(".");
		stmt.append(fkeyname);
		stmt.append(" = '");
		stmt.append(fkeyvalue);
		stmt.append("')");
		return(stmt.toString());
	}
	
	protected static int doHistoryStep(HttpServletRequest request, RowBean row, RowBean arow, int action, int step)
	{
		int updated = 0;
		RowBean bean = new RowBean();
		bean.setRequest(request);
		try
		{
			row.setError("1\r\n");
			ActionSpec aspec = row.getActionSpec();
			RelationSpec rspec = SpecManager.getRelationSpec();
			String rname = aspec.getActionStepRspec(action, step);
			int rindex = rspec.getIndex(rname);
			row.setError("2\r\n");
			String lastkeyname = rspec.getFromKey(rindex); //aspec.getActionStepCurrentHistoryKey(action, step);
			String keyname = aspec.getActionStepHistoryKey(action, step);
			row.setError("3\r\n");
			String fromtable = rspec.getFromTableName(rindex);
			String totable = rspec.getToTableName(rindex);
			String pkey = SpecManager.getTableSpec(fromtable).getPrimaryKeyName();
			row.setError("4\r\n");

			bean.setSearchStatement(getLastHistroyItemStmt(rspec, rindex, keyname, pkey, row.getString(pkey)));
			bean.retrieveDataOnStmt();
			String lastvalue = bean.getString(keyname);
			String value = row.getString(keyname);
			bean.clear();
			row.setError("5\r\n");
			if(bean.hasError())
			{
				row.setError("ActionSet error on history action (a) - ");
				row.setError(String.valueOf(action));
				row.setError(" step - ");
				row.setError(String.valueOf(step));
				row.setError("\r\n");
				row.setError(bean.getError());
			}
			row.setError("6\r\n");
			if(value.length()!=0 && !value.equals(lastvalue))
			{
				row.setError("7\r\n");
				bean.setRequest(request);
				bean.setTableSpec(totable);
				bean.setColumn(keyname, value);
				bean.setColumn(pkey, row.getString(pkey));
				bean.setAction("insert");
				bean.doAction(true);
				row.setError(bean.toString());
				row.setError("8\r\n");
				updated = bean.getUpdatedCount();
				String historykey = bean.getTableSpec().getPrimaryKeyName();
				arow.setColumn(lastkeyname, bean.getString(historykey));
				row.setColumn(lastkeyname, bean.getString(historykey));
				bean.clear();
				row.setError("9\r\n");
				if(bean.hasError())
				{
					row.setError("ActionSet error on history action (b) - ");
					row.setError(String.valueOf(action));
					row.setError(" step - ");
					row.setError(String.valueOf(step));
					row.setError("\r\n");
					row.setError(bean.getError());
				}	
			}		
			
		}
		catch(Exception e)
		{
			row.setError("ActionSet error on history action - ");
			row.setError(String.valueOf(action));
			row.setError(" step - ");
			row.setError(String.valueOf(step));
			row.setError("\r\n");
			row.setError(e.getMessage());
		}
		finally
		{
			bean.close();
		}
		return(updated);
	}	
	
}