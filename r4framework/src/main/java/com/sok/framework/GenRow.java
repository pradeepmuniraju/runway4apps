package com.sok.framework;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.generation.IllegalConfigurationException;
import com.sok.framework.generation.Row;
import com.sok.framework.generation.Rows;
import com.sok.framework.generation.nodes.DataNode;
import com.sok.framework.generation.nodes.LeafNode;
import com.sok.framework.generation.nodes.LimitParameterNode;
import com.sok.framework.generation.nodes.Node;
import com.sok.framework.generation.nodes.Parameter;

public class GenRow extends Rows implements TableData
{
    public static final int PARSETYPEINT = 1;
    public static final int PARSETYPELONG = 2;
    public static final int PARSETYPEFLOAT = 3;    
    public static final int PARSETYPEDOUBLE = 4;
    
    public String getColumn(String name)
    {
        String value = getParameter(name);
        if(value == null)
        {
            value = getData(name);
        }
        if(value != null)
        {
            return(value);
        }        
        return(TableData._blank);
    }
    
    public String getColumnHTML(String name)
    {
   	 return StringUtil.toHTML(getColumn(name));
    }
    
    public void replaceData(String name, Object value)
    {
       ViewSpec viewspec = getViewSpec();
       if(viewspec!=null)
       {
          replaceData(name, value, viewspec.getFormatTypeForName(name), viewspec.getFormatForName(name));
       }
       replaceData(name, value, null, null);
    }
    
    public void replaceData(String name, Object value, String type, String format)
    {
       DataNode data = getDataNode(name);
       if(data!=null)
       {
          data.setValue(value);
       }
       else
       {
          addNode(new DataNode(name, value, type, format));
       }
    }    
    
    public String getDataHTML(String name)
    {
   	 return StringUtil.toHTML(getData(name));
    }
    
    public String getData(String name)
    {
        DataNode data = getDataNode(name);
        if(data!=null)
        {
            return(ActionBean.formatValue(data.getName(), data.getFormat(),
                    data.getType(), data.getValue(),
                    this.getLocale(), this.getUserTimeZoneObject()));
        }
        return(TableData._blank);
    }
    
    public String getColumn(String name, String format)
    {
        String value = getFormattedParameter(name, format);
        if(value == null)
        {
            value = getData(name, format);
        }
        if(value != null)
        {
            return(value);
        }        
        return(TableData._blank);
    }     

    public String getData(String name, String format)
    {
        DataNode data = getDataNode(name);
        if(data!=null)
        {
            return(ActionBean.formatValue(data.getName(), format,
                    data.getType(), data.getValue(),
                    this.getLocale(), this.getUserTimeZoneObject()));
        }
        return(TableData._blank);
    }     
    
    public String[] getDataNames()
    {
       return(getNames(type_DataNode));
    }
    
   public String getFormattedParameter(String name, String format) 
   {
      Node node = getNode(name, Parameter.type_MinParameter, Parameter.type_MaxParameter);
      if(node!=null)
      {
         Object obj = ((LeafNode)node).getValue();
            
         if(obj!=null)
         {
            return(ActionBean.formatValue(name, format,
                    null, obj,
                    this.getLocale(), this.getUserTimeZoneObject()));
         }
      }        
      return(null);
   }
   
   public String getFormattedParameter(String name)
   {
       Node node = getNode(name, Parameter.type_MinParameter, Parameter.type_MaxParameter);
       if(node!=null)
       {
           Object obj = ((LeafNode)node).getValue();
           if(obj!=null)
           {
               return(ActionBean.formatValue(this, name, obj));
           }
       }        
       return(null);
   }   
   
    public String getParameter(String name)
    {
        Node node = getNode(name, Parameter.type_MinParameter, Parameter.type_MaxParameter);
        if(node!=null)
        {
            Object obj = ((LeafNode)node).getValue();
            if(obj!=null)
            {
                return(obj.toString());
            }
        }        
        return(null);
    }
    
    public String getParameterString(String name)
    {
       String value = getParameter(name);
       if(value == null)
       {
          value = TableData._blank;
       }
       return(value);
    }    
    
    public String[] getParameterValues(String name)
    {
        Node node = getNode(name, Parameter.type_MinParameter, Parameter.type_MaxParameter);
        if(node!=null)
        {
            Object obj = ((LeafNode)node).getValue();
            if(obj instanceof String[])
            {
                return((String[])obj);
            }
            else
            {
            	return(new String[]{obj.toString()});
            }
        }        
        return(null);
    }    
    
   public boolean isNull(String name)
   {
      DataNode data = getDataNode(name);
      return isNull(data);
   }    

   public boolean isNull(DataNode data) 
   {
      if(data!=null)
      {
         return data.getValue() == null;
      }
      else
      {
         return true;
      }
   }        
   
   public Number getNumber(String name) 
   {
      DataNode data = getDataNode(name);
      return getNumber(data, 0);
   }
   
   public Number getNumber(DataNode data, int type) 
   {
      Number number = null;
      if(data!=null)
      {
         Object value = data.getValue();
         if (value != null) 
         {
            if(value instanceof Number)
            {
                number = (Number)value;
            }
            else if(value instanceof String && type != 0)
            {
                if(type == PARSETYPEINT)
                {
                    number = new Integer(value.toString());
                }
                else if(type == PARSETYPELONG)
                {
                    number = new Long(value.toString());
                }  
                else if(type == PARSETYPEFLOAT)
                {
                    number = new Float(value.toString());
                }
                else if(type == PARSETYPEDOUBLE)
                {
                    number = new Double(value.toString());
                }
                else
                {
                    throw new IllegalConfigurationException("Parse number type not valid");
                }
            }
            else
            {
                throw new IllegalConfigurationException("Data node value not instance of Number");
            }
         }
      }
      else
      {
         throw new IllegalConfigurationException("Data node "+name+" is null");
      }
      return(number);
   }
   
   public int getInt(String name)
   {
      DataNode data = getDataNode(name);
      if (isNull(data)) {
         return 0;
      }
      else {
         return getNumber(data, PARSETYPEINT).intValue();
      }
   }   
    
   public long getLong(String name)
   {
      DataNode data = getDataNode(name);
      if (isNull(data)) {
         return 0l;
      }
      else {
         return getNumber(data, PARSETYPELONG).longValue();
      }
   }

   public float getFloat(String name)
   {
      DataNode data = getDataNode(name);
      if (isNull(data)) {
         return 0f;
      }
      else {
         return getNumber(data, PARSETYPEFLOAT).floatValue();
      }
    }    
   
   /*
    * Zero is a value, null is no value.
    */
   public Double getDoubleNull(String name) {
	   DataNode data = getDataNode(name);
	   if (isNull(data)) {
		   return null;
	   }
	   else {
		   return getNumber(data, PARSETYPEDOUBLE).doubleValue();
	   }	   
   }
   
   public double getDouble(String name)
   {
      DataNode data = getDataNode(name);
      if (isNull(data)) {
         return 0d;
      }
      else {
         return getNumber(data, PARSETYPEDOUBLE).doubleValue();
      }
   }  
    
    public java.util.Date getDate(String name)
    {
        java.util.Date returnvalue = null;
        DataNode data = getDataNode(name);
        if(data!=null)
        {
            Object value = data.getValue();
            if (value != null) 
            {
               if(value instanceof java.util.Date)
               {
                   returnvalue = ((java.util.Date)value);
                   DateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");                   
                   // Set the formatter to use a different timezone  
                   formatter.setTimeZone(getUserTimeZoneObject()); 
                   formatter.format(returnvalue);
               } else if (value instanceof java.lang.String) {
            	   String val = (java.lang.String) value;
            	   if (val.indexOf(".") > 0) val = val.substring(0, val.indexOf("."));
                   DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");                   
                   formatter.setTimeZone(getUserTimeZoneObject()); 
            	   
                   try {
					returnvalue = formatter.parse(val);
					} catch (ParseException e) {
		            	   String dataClassName = value.getClass().getName();
		                   throw new IllegalConfigurationException("Data node value not instance of java.util.Date, but instance of " + dataClassName) ;
					}
               }
               else
               {
            	   String dataClassName = value.getClass().getName();
                   throw new IllegalConfigurationException("Data node value not instance of java.util.Date, but instance of " + dataClassName) ;
               }
            }
        }
        else
        {
            throw new IllegalConfigurationException("Data node "+name+" is null");
        }
        return(returnvalue);
    }
    
    public void addParameter(String name, String value)
    {
        add(name, value);
    }           
    
    public void setParameter(String name, Object value)
    {
        put(name, value);
    }       
    
    public void setColumn(String name, String value)
    {
        put(name, value);
    }       
    
    public void setSearchStatement(String s)
    {
        setStatement(s);
    }
    
    /**
     * @deprecated
     */ 
    public StringBuffer createdDataOnIDStatment()
    {
        return(null);
    }
    
    public void setDateInputFormat(String d)
    {
        
    }
    
    public void setToNewID(String name)
    {
        put(name,KeyMaker.generate());
    }

    public void createNewID()
    {
        put(getTableSpec().getPrimaryKeyName(),KeyMaker.generate());
    }
    
    public void setSearchCriteria(String s)
    {
        
    }
    
    public String getSearchCriteria()
    {
        return(getStatementCriteria());
    }    
    
    public Object getObject(String name)
    {
        return(get(name));
    }
    
    /**
     * @deprecated
     */    
    public Relation getRelatedFields()
    {
        return(null); 
    }
    
    public String getDateTime(String name)
    {
        return(ActionBean.getDateTime(name, this, thislocale)); 
    }
    
    /**
     * @deprecated
     */  
    public void generateSQLStatement()
    {
        doAction(ActionBean.SEARCH);
    }
    
    /**
     * @deprecated
     */    
    public String[] getSortfields()
    {
        return(null); 
    }

    /**
     * @deprecated
     */    
    public String[] getSortorder()
    {
        return(null); 
    }
    
    public String getCodedString(String name)
    {
        Object obj = super.get(name);
        String value = null;
        if(obj!=null)
        {
            value = obj.toString();
        }
        return(getCodedValue(value));
    }
    
    public String getCodedColumn(String name)
    {
        String format = getViewSpec().getFormatForName(name);   
        String value = getColumn(name, format);
        return(getCodedValue(value));   
    }
    
    protected String getCodedValue(String value)
    {
        if(value == null)
        {
            return(ActionBean.NULL);
        }
        else if(value.length() == 0)
        {
            return(ActionBean.EMPTY);
        }
        return(value);
    }    
    
    public void sortBy(String field, int index)
    {
        putOrderBy(field, index);
    }

    public void sortOrder(String order, int index)
    {
        putOrderByDirection(order, index);
    }
    
    public void setSort(int index, String sort, String order)
    {
        putOrderBy(sort, order, index);
    }
    
    public String getDateConversionsType()
    {
        return(null); 
    }
    
    /**
     * @deprecated
     */    
    public Map getSearchGroups()
    {
        return(null);
    }
    
    /**
     * @deprecated
     */
    public void setSearchGroups(Map groups)
    {
        
    }

    public int getTop()
    {
        LimitParameterNode node = (LimitParameterNode)getNode(_limit_results, Parameter.type_LimitParameter);
        if(node!=null)
        {
            return(node.getIntValue());
        }
        return(0);
    }

    public void setTop(int topcount)
    {
        putLimit(String.valueOf(topcount));
    }
    
    /**
     * @deprecated
     */
    public void setActionSpec(String as)
    {
        
    }
    
    /**
     * @deprecated
     */    
    public ActionSpec getActionSpec()
    {
        return(null);
    }

    public String getSearchStatement()
    {
        return(getStatement());
    }
    public void putAllDataAsParameters(Row row) {
    	putAllDataAsParameters(row, true);
	}    
    public void putAllDataAsParameters(Row row, boolean blank)
    {
        Iterator entries = row.entrySet(Node.type_DataNode).iterator();
        LeafNode entry = null;
        while(entries.hasNext())
        {
            entry = (LeafNode)entries.next();
            Object value = entry.getValue();
            if (blank) {
            	setParameter(entry.getKey().toString(), entry.getValue());
            } else if (value instanceof String) {
            	if (((String) value).length() > 0) {
            		setParameter(entry.getKey().toString(), entry.getValue());
            	}
            } else if (value instanceof Double) {
            	if (((Double)value).doubleValue() != 0) {
            		setParameter(entry.getKey().toString(), entry.getValue());
            	}
            } else if (value instanceof Float) {
            	if (((Float)value).floatValue() != 0) {
            		setParameter(entry.getKey().toString(), entry.getValue());
            	}
            } else if (value instanceof Integer) {
            	if (((Integer) value).intValue() != 0) {
            		setParameter(entry.getKey().toString(), entry.getValue());
            	}
            } else if (entry.getValue() != null) {
            	setParameter(entry.getKey().toString(), entry.getValue());
            }
        }
    }
    
	public String getLockKey()
	{
		String idname = this.getTableSpec().getPrimaryKeyName();
		String id = this.getColumn(idname);
		String tablename = this.getTableSpec().getTableName();
		if(id!=null && tablename!=null)
		{
			StringBuffer temp = new StringBuffer();
			temp.append(tablename);
			temp.append(_dot);
			temp.append(id);
			return(temp.toString());
		}
		return(GenerationKeys._blank);
	}
	
	public Map<String, String> getDataMap() {
		Map<String,String> map = new HashMap<String,String>();
		
		
		for (int i = 0; i < getColumnCount(); ++i) {
			String name = getColumnName(i + 1);
			if (name != null) map.put(name, String.valueOf(getData(name)));
		}
		return map;
	}
}