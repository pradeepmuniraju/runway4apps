package com.sok.framework;

import java.util.*;

public class CreditCard {
 
   String cardType = "CardObject";
   int[] rules = null;
   int[] length = null;
   
   public static final String CC_MASK_PREFIX = "XXXX XXXX XXXX ";
   
   String cardNumber = null;
   String expiryYear = null;
   String expiryMonth = null;
   
   public static CreditCard getVisa() {
      int[] a = {4};
      int[] b = {13,16};
      return new CreditCard("Visa", a, b);
   }
   
   public static CreditCard getMastercard() {
      int[] a = {51,52,53,54,55};
      int[] b = {13,16};
      return new CreditCard("Mastercard", a, b);
   }
   
   public static CreditCard getAmex() {
      int[] a = {34,37};
      int[] b = {14};
      return new CreditCard("Amex", a, b);
   }
   
   public static CreditCard getBankcard() {
      int[] a = {56};
      int[] b = {16};
      return new CreditCard("Bankcard", a, b);
   }
   
   public static CreditCard getDinersClub() {
      int[] a = {30,36,38};
      int[] b = {14};
      return new CreditCard("DinersClub", a, b);
   }
   
   public static CreditCard getDiscoverCard() {
      int[] a = {6011};
      int[] b = {16};
      return new CreditCard("DiscoverCard", a, b);
   }
   
   public static CreditCard getEnRouteCard() {
      int[] a = {2014,2149};
      int[] b = {15};
      return new CreditCard("enRouteCard", a, b);
   }
   
   public static CreditCard getJCBCard() {
      int[] a = {3088,3096,3112,3158,3337,3528};
      int[] b = {16};
      return new CreditCard("JCBCard", a, b);
   }
   
   public static String maskCreditCardNumber(String ccNumber) {
   	if (ccNumber == null || ccNumber.length() == 0) {
   		return "";
   	}
   	else if (ccNumber.length() <= 4) {
   		return CC_MASK_PREFIX + "XXXX";
   	}
   	else  {
   		return CC_MASK_PREFIX + ccNumber.substring(ccNumber.length() - 4);
   	}
   }
   
   public CreditCard() {
      this(null, null, null);
   }
   public CreditCard(String cardType) {
      this(cardType, null, null);
   }
   public CreditCard(String cardType, int[] rules) {
      this(cardType, rules, null);
   }
   public CreditCard(String cardType, int[] rules, int[] length) {
      setCardType(cardType);
      setRules(rules);
      setLen(length);
   }
   
   public void setCardNumber(String cardNumber) {
      this.cardNumber = cardNumber;
      if (this.cardNumber != null) {
         this.cardNumber = this.cardNumber.replace(" ", "");
      }
   }
   
   public void setCardType(String cardType) {
      this.cardType = cardType;
   }
   
   public void setLen(int[] length) {	
      if (length == null) {
         
         int[] a = {13,14,15,16,19};
   		this.length = a;
   	}
   	else {
   	   this.length = length;
   	}
   }
   
   public void setRules(int[] rules) {
      if (rules == null) {
         int[] a = {0,1,2,3,4,5,6,7,8,9};
   		this.rules = a;
   	}
   	else {
   	   this.rules = rules;
   	}
   }
   
   public void setExpiryDate(String expiryYear, String expiryMonth) {
      this.expiryYear = expiryYear;
      this.expiryMonth = expiryMonth;
   }
   
   public boolean checkCardNumber() {
      return isCardNumber() && isExpiryDate();
   }
   
   public boolean checkCardNumber(String cardNumber, String year, String month) {
      
   	this.setCardNumber(cardNumber);
   	this.setExpiryDate(year, month);
   	
      return isCardNumber() && isExpiryDate();
   }
   
   public String getExpiryDate() {
	   return this.expiryMonth + "/" + this.expiryYear;
   }
   
   public String getCardType() {
	   return this.cardType;
   }
   
   public boolean isCardNumber() {
   	
   	if (cardNumber != null) {
   	
      	if (!luhnCheck()) {
      		return false;
      	}
      	
      	for (int n=0; n < length.length; n++) {
      	   
      		if (cardNumber.length() == length[n]) {
      		   
      			for (int m = 0; m < rules.length; m++) {
      			   String rule = String.valueOf(rules[m]);
      				String headdigit = cardNumber.substring(0, rule.length());
      				
      				if (headdigit.equals(rule)) {
      					return true;
      				}
      			}
      			return false;
      		}
      	}
      }
   	return false;
   }
   
   public boolean isExpiryDate() {
   
   	if (!isNumeric(expiryYear)) {
   		return false;
   	}
   	if (!isNumeric(expiryMonth)) {
   		return false;
   	}
   	
   	int year = Integer.parseInt(expiryYear);
   	int month = Integer.parseInt(expiryMonth);
   	
   	Calendar today = new GregorianCalendar();
   	Calendar expiry = new GregorianCalendar();
   	expiry.set(Calendar.YEAR,year);
   	expiry.set(Calendar.MONTH,month-1);
   	
   	return (today.compareTo(expiry) <= 0);
   }
   
   private boolean isNumeric(String num) {
      try {
         Long.parseLong(num);
         return true;
      }
      catch (Exception e) {
         return false;
      }
   }
   
   
   public boolean luhnCheck() {
      if (cardNumber != null) {
      	if (!isNumeric(cardNumber)){
      		return false;
        	}
      
      	int no_digit = cardNumber.length();
      	int oddoeven = no_digit & 1;
      	int sum = 0;
      
      	for (int count=0; count < no_digit; count++)
      	{
      		int digit = Integer.parseInt(cardNumber.substring(count,count+1));
      		if (((count & 1) ^ oddoeven) == 0) {
      			digit *= 2;
      			if (digit > 9) {
      				digit -= 9;
      			}
      		}
      		sum += digit;
      	}
      	if (sum % 10 == 0) {
      		return true;
      	}
      }
      return false;
   }
   
  
}