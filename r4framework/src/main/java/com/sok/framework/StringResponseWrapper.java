package com.sok.framework;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

public class StringResponseWrapper extends HttpServletResponseWrapper
{
	StringStream stringout = new StringStream();
	PrintWriter stringwriter = new PrintWriter(stringout);
	
	public StringResponseWrapper(HttpServletResponse response)
	{
		super(response);
	}
	
	public ServletOutputStream getOutputStream()
	{
		return(stringout);
	}
	
	public PrintWriter getWriter()
	{
		return(stringwriter);
	}
	
	public StringBuffer getBuffer()
	{
		return(stringout.getBuffer());
	}
	
	public void flush()
	{
		try
		{	
			stringwriter.flush();
			stringout.flush();
		}
		catch(Exception e)
		{
			
		}
	}
	
	public String toString()
	{
		flush();
		return(stringout.toString());
	}
	
	class StringStream extends ServletOutputStream
	{
		StringWriter wirter = new StringWriter();
		
		public void write(int i)
		{
			wirter.write(i);
		}
		
		public StringBuffer getBuffer()
		{
			return(wirter.getBuffer());
		}
		
		public String toString()
		{
			return(wirter.toString());
		}
	}
}