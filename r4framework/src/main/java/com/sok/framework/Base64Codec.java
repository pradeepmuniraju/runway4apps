package com.sok.framework;

import java.util.Vector;

public class Base64Codec
{
   public static char[] Base64Tokens = new char[64];
   static
   {
      int i = 0;
      for (char c = 'A'; c <= 'Z'; c++)
         Base64Tokens[i++] = c;
      for (char c = 'a'; c <= 'z'; c++)
         Base64Tokens[i++] = c;
      for (char c = '0'; c <= '9'; c++)
         Base64Tokens[i++] = c;
      Base64Tokens[i++] = '+';
      Base64Tokens[i++] = '/';
   }

   public static final char Base64Pad = '=';
   public static final char Linefeed = (char) 10;
   public static final char Carriage = (char) 13;
   public static final int LineMax = 75;

   public Base64Codec()
   {
   }

   public static String encode(String source)
   {
      byte[] sourceBytes = source.getBytes();
      int byteTriad = sourceBytes.length % 3;
      StringBuffer encoding = new StringBuffer();
      int bitOffset = 7;
      int b64Offset = 5;
      int bytePlace = 0;
      byte tokenValue = (byte) 0;
      int lineLength = 0;
      while(bytePlace < sourceBytes.length)
      {
         tokenValue = (byte) ((byte) tokenValue | (byte) ((sourceBytes[bytePlace] & (1 << bitOffset)) > 0 ? (1 << b64Offset)
               : (byte) 0));
         bitOffset--;
         if(bitOffset < 0)
         {
            bitOffset = 7;
            bytePlace++;
         }
         b64Offset--;
         if(b64Offset < 0)
         {
            b64Offset = 5;
            encoding.append(Base64Tokens[tokenValue]);
            tokenValue = (byte) 0;
            lineLength++;
            if(lineLength > LineMax)
            {
               encoding.append(Carriage);
               encoding.append(Linefeed);
               lineLength = 0;
            }
         }
      }
      if(b64Offset != 5)
      {
         bytePlace--;
         for (int i = b64Offset; i >= 0; i--)
         {
            if(bitOffset >= 0)
            {
               tokenValue = (byte) ((byte) tokenValue | (byte) ((sourceBytes[bytePlace] & (1 << bitOffset)) > 0 ? (1 << i)
                     : (byte) 0));
            }
            bitOffset--;
         }
         encoding.append(Base64Tokens[tokenValue]);
      }
      if(byteTriad == 2)
      {
         encoding.append(Base64Pad);
      }
      else if(byteTriad == 1)
      {
         encoding.append(Base64Pad);
         encoding.append(Base64Pad);
      }
      return encoding.toString();
   }

   public static String decode(String source)
   {
      StringBuffer decoding = new StringBuffer();
      int bitOffset = 7;
      int b64Offset = 5;
      int bytePlace = 0;
      byte charValue = (byte) 0;
      while(bytePlace < source.length())
      {
         if(source.charAt(bytePlace) == Base64Pad)
         {
            // end processing when encountering special end-padding character
            break;
         }
         if(source.charAt(bytePlace) == Linefeed
               || source.charAt(bytePlace) == Carriage)
         {
            // ignore standard line break characters
            bytePlace++;
            continue;
         }
         else
         {
            int cindex = indexOf(source.charAt(bytePlace));
            if(cindex<0)
            {
               // ignore unknown characters (mostly implemented to deal with
               // other line break character sequences)
               bytePlace++;
               continue;
            }
            else
            {
               byte currentByte = (byte)cindex;
               charValue = (byte) ((byte) charValue | (byte) ((currentByte & (1 << b64Offset)) > 0 ? (1 << bitOffset)
                     : (byte) 0));
               bitOffset--;
               if(bitOffset < 0)
               {
                  bitOffset = 7;
                  decoding.append((char) charValue);
                  charValue = (byte) 0;
               }
               b64Offset--;
               if(b64Offset < 0)
               {
                  b64Offset = 5;
                  bytePlace++;
               }
            }
         }
      }
      return decoding.toString();
   }

   public static int indexOf(char c)
   {
      for(int i=0; i<Base64Tokens.length; i++)
      {
         if(Base64Tokens[i] == c)
         {
            return(i);
         }
      }
      return(-1);
   }
   
   // Mapping table from Base64 characters to 6-bit nibbles.
   private static byte[] map2 = new byte[128];
   static
   {
      for (int i = 0; i < map2.length; i++)
         map2[i] = -1;
      for (int i = 0; i < 64; i++)
         map2[Base64Tokens[i]] = (byte) i;
   }

   /**
    * Encodes a byte array into Base64 format. No blanks or line breaks are
    * inserted.
    * 
    * @param in
    *           an array containing the data bytes to be encoded.
    * @return A character array with the Base64 encoded data.
    */
   public static char[] encode(byte[] in)
   {
      return encode(in, in.length);
   }

   /**
    * Encodes a byte array into Base64 format. No blanks or line breaks are
    * inserted.
    * 
    * @param in
    *           an array containing the data bytes to be encoded.
    * @param iLen
    *           number of bytes to process in <code>in</code>.
    * @return A character array with the Base64 encoded data.
    */
   public static char[] encode(byte[] in, int iLen)
   {
      int oDataLen = (iLen * 4 + 2) / 3; // output length without padding
      int oLen = ((iLen + 2) / 3) * 4; // output length including padding
      char[] out = new char[oLen];
      int ip = 0;
      int op = 0;
      while(ip < iLen)
      {
         int i0 = in[ip++] & 0xff;
         int i1 = ip < iLen ? in[ip++] & 0xff : 0;
         int i2 = ip < iLen ? in[ip++] & 0xff : 0;
         int o0 = i0 >>> 2;
         int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
         int o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
         int o3 = i2 & 0x3F;
         out[op++] = Base64Tokens[o0];
         out[op++] = Base64Tokens[o1];
         out[op] = op < oDataLen ? Base64Tokens[o2] : '=';
         op++;
         out[op] = op < oDataLen ? Base64Tokens[o3] : '=';
         op++;
      }
      return out;
   }

   /**
    * Decodes a byte array from Base64 format. No blanks or line breaks are
    * allowed within the Base64 encoded data.
    * 
    * @param in
    *           a character array containing the Base64 encoded data.
    * @return An array containing the decoded data bytes.
    * @throws IllegalArgumentException
    *            if the input is not valid Base64 encoded data.
    */
   public static byte[] decode(char[] in)
   {
      int iLen = in.length;
      if(iLen % 4 != 0)
         throw new IllegalArgumentException(
               "Length of Base64 encoded input string is not a multiple of 4.");
      while(iLen > 0 && in[iLen - 1] == '=')
         iLen--;
      int oLen = (iLen * 3) / 4;
      byte[] out = new byte[oLen];
      int ip = 0;
      int op = 0;
      while(ip < iLen)
      {
         int i0 = in[ip++];
         int i1 = in[ip++];
         int i2 = ip < iLen ? in[ip++] : 'A';
         int i3 = ip < iLen ? in[ip++] : 'A';
         if(i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127)
            throw new IllegalArgumentException(
                  "Illegal character in Base64 encoded data.");
         int b0 = map2[i0];
         int b1 = map2[i1];
         int b2 = map2[i2];
         int b3 = map2[i3];
         if(b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0)
            throw new IllegalArgumentException(
                  "Illegal character in Base64 encoded data.");
         int o0 = (b0 << 2) | (b1 >>> 4);
         int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
         int o2 = ((b2 & 3) << 6) | b3;
         out[op++] = (byte) o0;
         if(op < oLen)
            out[op++] = (byte) o1;
         if(op < oLen)
            out[op++] = (byte) o2;
      }
      return out;
   }

}