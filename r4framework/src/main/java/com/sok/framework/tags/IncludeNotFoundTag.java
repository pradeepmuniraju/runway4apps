package com.sok.framework.tags;
import javax.servlet.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;

public class IncludeNotFoundTag extends BodyTagSupport
{
   public int doStartTag() throws JspException
   {   
      IncludeTag ancestorTag = (IncludeTag)findAncestorWithClass(this, IncludeTag.class);
      if (ancestorTag == null)
      {
         throw new JspTagException("Parent IncludeTag not found");
      }
      if(ancestorTag.getFileExits())
      {
         return(SKIP_BODY);
      }
      else
      {
         return(EVAL_BODY_INCLUDE);
      }
   }
}