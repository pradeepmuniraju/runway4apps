package com.sok.framework.tags;
import javax.servlet.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;

public class GetAttributeTag extends TagSupport
{
    public static final String PAGE = "page";
    public static final String REQUEST = "request";
    public static final String SESSION = "session";
    
    public static final String STRING = "java.lang.String";
    
    private String attributecontext = REQUEST;
    private String name;
    private String id;
    private String type = "java.lang.String";
    
    public void setName(String name)
    {
        this.name = name;
    }    
    
    public String getName()
    {
        return(name);
    }    
    
    public void setId(String id)
    {
        this.id = id;
    }    
    
    public String getId()
    {
        return(id);
    }    
    
    public void setType(String type)
    {
        this.type = type;
    }    
    
    public String getType()
    {
        return(type);
    }        
    
    public void setScope(String attributecontext)
    {
        this.attributecontext = attributecontext;
    }    
    
    public String getScope()
    {
        return(attributecontext);
    }
    
    public int doStartTag()
    {
        Object obj;
        
        if(attributecontext.equals(PAGE))
        {
            obj = checkNull(pageContext.getAttribute(name));
        }
        else if(attributecontext.equals(SESSION))
        {
            obj = checkNull(pageContext.getSession().getAttribute(name));
        }
        else
        {
            obj = checkNull(pageContext.getRequest().getAttribute(name));
        }
        
        pageContext.setAttribute(id, obj);
        
        return(SKIP_BODY);
    }
    
    protected Object checkNull(Object obj)
    {
        if(obj == null && type.equals(STRING))
        {
            return("");
        }
        return(obj);
    }
}