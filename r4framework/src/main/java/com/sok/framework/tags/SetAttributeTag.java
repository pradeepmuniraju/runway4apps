package com.sok.framework.tags;
import javax.servlet.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;

public class SetAttributeTag extends TagSupport
{
    public static final String PAGE = "page";
    public static final String REQUEST = "request";
    public static final String SESSION = "session";
     
    private String attributecontext = REQUEST;
    private String name;
    private String value;

    
    public void setName(String name)
    {
        this.name = name;
    }    
    
    public String getName()
    {
        return(name);
    }    
    
    public void setValue(String value)
    {
        this.value = value;
    }    
    
    public String getValue()
    {
        return(value);
    }         
    
    public void setScope(String attributecontext)
    {
        this.attributecontext = attributecontext;
    }    
    
    public String getScope()
    {
        return(attributecontext);
    }
    
    public int doStartTag()
    {
        if(attributecontext.equals(PAGE))
        {
            pageContext.setAttribute(name, value);
        }
        else if(attributecontext.equals(SESSION))
        {
            pageContext.getSession().setAttribute(name, value);
        }
        else
        {
            pageContext.getRequest().setAttribute(name, value);
        }
        
        return(SKIP_BODY);
    }
    
}