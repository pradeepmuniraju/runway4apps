package com.sok.framework.tags;
import javax.servlet.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;

public class IncludeTag extends TagSupport
{
    private String pagepath;
    private String defaultpath; 
    private boolean debug = false;
    private boolean ifExists = false;
    //private boolean flush = false; requires JSP2.0  
    
    private boolean fileExits = false;
    
    public void setPage(String pagepath)
    {
        this.pagepath = pagepath;
    }
    
    public String getPage()
    {
        return(this.pagepath);
    }    
    
    public void setDefault(String defaultpath)
    { 
    	this.defaultpath = defaultpath; 
    }
    
    public String getDefault()
    { 
    	return(this.defaultpath); 
    }
    
    public void setDebug(boolean debug)
    {
        this.debug = debug;
    }
    
    public boolean getDebug()
    {
        return(this.debug);
    }
    
    public boolean getFileExits()
    {
       return(this.fileExits);
    }
    
    public void setIfExists(boolean ifExists)
    {
        this.ifExists = ifExists;
    }
    
    public boolean getIfExists()
    {
        return(this.ifExists);
    }      
    
    protected boolean fileExists()
    {
        String path = pageContext.getServletContext().getRealPath(this.pagepath);
        return(new File(path).exists());
    }
    
    /* requires JSP2.0 
    public void setFlush(boolean flush)
    {
        this.flush = flush;
    }    
    
    public boolean getFlush()
    {
        return(this.flush);
    }         
    */
    
    public int doStartTag()
    {  
       fileExits = fileExists();
          return(EVAL_BODY_INCLUDE);

    }
    
    public int doEndTag()
    {
       JspWriter out = pageContext.getOut();
       doInclude(out);
        return(EVAL_PAGE);
    }
    
    protected void doInclude(JspWriter out)
    {
        if(pagepath!=null)
        {
            if(!this.ifExists || fileExits)
            {
                try
                {
                    pageContext.include(pagepath);
                    //pageContext.include(pagepath, flush); // requires JSP2.0 
                }
                catch(Exception e)
                {
                    try{
                        out.print(e.getMessage());
                    }catch(Exception ex){}
                }
            }
            else if(defaultpath != null && defaultpath.length()>0) 
            { 
            	try 
            	{ 
            		pageContext.include(this.defaultpath);
                    //pageContext.include(this.defaultpath, flush); // requires JSP2.0 
            	}
            	catch (Exception e) 
            	{ 
            		try{
                        out.print("Error using default: "+e.getMessage());
                    }catch(Exception ex){}
            	}
            }
            else if(debug)
            {
                try{
                    out.print(pagepath+" does not exist");
                }catch(Exception ex){}
            }
        }
        else
        {
            try{
                out.print("No page attribute defined for include tag.");
            }catch(Exception ex){}
        }
    }
    
    public void release()
    {
        pagepath = null;
        defaultpath = null; 
        debug = false;
        ifExists = false;
    }
    
}