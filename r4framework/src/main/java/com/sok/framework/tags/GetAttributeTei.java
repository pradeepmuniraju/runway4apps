package com.sok.framework.tags;
import javax.servlet.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;

public class GetAttributeTei extends TagExtraInfo
{
   public VariableInfo[] getVariableInfo(TagData data)
   {
       String type = data.getAttributeString("type");
       if (type == null)
       {
           type = "java.lang.String";
       }
       return new VariableInfo[] {
               new VariableInfo(data.getAttributeString("id"),
                       type,
                       true,
                       VariableInfo.AT_BEGIN)
       };
    }
}