package com.sok.framework.tags;

import com.sok.framework.ImageUtil;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.filemigration.FileMoveMap;
import com.sok.runway.ExcelServlet;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * The ResizeImage tag is designed to take an existing image, and resize / compress it before serving it to the client.
 * It will use a standardised filename convention, and will return a tag pointing to a previously converted image
 * if one already exists
 * <p>
 * You should be able to replace.. <img * /> with <sok:img * /> and have everything work correctly.
 * Assumes you've used valid HTML for the img tag.
 * There is a complimentary JSP for use with Javascript functions.
 *
 * @author Michael Dekmetzian
 * @date 7/1/2009
 */
public class ResizeImage extends TagSupport {
	private String src = null;
	private int iwidth = 0;
	private int iheight = 0;
	private int rwidth = 0;
	private int rheight = 0;
	private String width;
	private String height;
	private boolean tag = true;
	private String alt = null;
	private String border = null;
	private String style = null;
	private String usemap = null;
	private String id = null;
	private String onclick = null;
	private String align = null;
	private boolean debug = false;
	private boolean setWidthHeightAttributes = true;
	public static final String USC = "_";
	public static final String CALC = "CALC";
	public static final String QSPA = "\" ";
	public static final String COMP = "COMP";
	StringBuilder debugString = new StringBuilder();
	StringBuilder worker = new StringBuilder();

	private boolean sizeToFit = false;
	private boolean cropToFit = false;

	public void setSrc(String paramString) {
		this.src = paramString;
	}

	public String getSrc() {
		return this.src;
	}

	public void setSetWidthHeightAttributes(String paramString) {
		this.setWidthHeightAttributes = "true".equals(paramString);
	}

	public String getSetWidthHeightAttributes() {
		return String.valueOf(this.setWidthHeightAttributes);
	}

	public void setWidth(String paramString) {
		this.width = paramString;
		int i = 0;
		try {
			i = Integer.parseInt(paramString);
		} catch (Exception localException) {
			if (this.debug)
				this.debugString.append("Width fail, did not parse correctly. Value = '").append(paramString).append("'.\r\n");
		}
		this.iwidth = i;
	}

	public String getWidth() {
		return this.width;
	}

	public void setDebug(String paramString) {
		this.debug = "true".equals(paramString);
	}

	public String getDebug() {
		return String.valueOf(this.debug);
	}

	public String getDebugString() {
		return this.debugString.toString();
	}

	public void setSizeToFit(String paramString) {
		this.sizeToFit = "true".equals(paramString);
	}

	public void setCropToFit(String paramString) {
		this.cropToFit = "true".equals(paramString);
	}

	public String getSizeToFit() {
		return String.valueOf(this.sizeToFit);
	}

	public void setTag(String paramString) {
		this.tag = "true".equals(paramString);
	}

	public String getTag() {
		return String.valueOf(this.tag);
	}

	public void setHeight(String paramString) {
		this.height = paramString;
		int i = 0;
		try {
			i = Integer.parseInt(paramString);
		} catch (Exception localException) {
			if (this.debug)
				this.debugString.append("Height fail, did not parse correctly. Value = '").append(paramString).append("'.\r\n");
		}
		this.iheight = i;
	}

	public String getHeight() {
		return this.height;
	}

	public int doStartTag() {
		return 1;
	}

	public int doEndTag() {
		JspWriter localJspWriter = this.pageContext.getOut();
		doImage(localJspWriter);
		return 6;
	}

	public String getImage() {
		doImage(null);
		return this.worker.toString();
	}

	protected void doImage(JspWriter paramJspWriter) {
		boolean failed = false;
		if ((this.src == null) || (this.src.length() == 0)) {
			if (this.debug)
				this.debugString.append("Source was empty, no actions performed\r\n");
		} else {
			try {
				String ctxPath = ((HttpServletRequest) super.pageContext.getRequest()).getContextPath();
				if (this.src != null && this.src.startsWith(ctxPath)) this.src = this.src.substring(ctxPath.length());
			} catch (Exception e) {
			}

			String str = InitServlet.getRealPath("");
			this.worker.delete(0, this.worker.length());
			this.worker.append(InitServlet.getRealPath(this.src));
			File localFile1 = new File(this.worker.toString());

			if (localFile1.exists() && localFile1.length() > 0) {
				File localFile2 = new File(getNewImage(localFile1));
				this.debugString.append("New Path: " + localFile2.getAbsolutePath() + "\r\n");
				if (localFile2.exists()) {
					if (localFile1.lastModified() > localFile2.lastModified()) {
						localFile2.delete();
					}
				}
				if (!localFile2.exists() || this.debug) {
					ImageUtil localImageUtil = new ImageUtil(localFile1.getAbsolutePath(), localFile1);
					rwidth = localImageUtil.getWidth();
					rheight = localImageUtil.getHeight();
					if ((this.iwidth > 0) && (this.iheight > 0)) {
						if (sizeToFit || cropToFit) {
							localImageUtil.resizeToFit(this.iwidth, this.iheight, false, cropToFit);
						} else {
							localImageUtil.resize(this.iwidth, this.iheight);
						}
					} else if (this.iwidth > 0) {
						localImageUtil.resizeToWidth(this.iwidth);
					} else if (this.iheight > 0) {
						localImageUtil.resizeToHeight(this.iheight);
					}
					localImageUtil.setJpegQuality(0.8F);
					localImageUtil.saveImage(localFile2.getAbsolutePath());
					if (localImageUtil.getWidth() > iwidth || localImageUtil.getHeight() > iheight) failed = true;
				} else if (localFile2.canRead()) {
					ImageUtil localImageUtil = new ImageUtil(localFile2.getAbsolutePath(), localFile2);
					rwidth = localImageUtil.getWidth();
					rheight = localImageUtil.getHeight();
					if (rwidth > iwidth || rheight > iheight) failed = true;
				} else {
					ImageUtil localImageUtil = new ImageUtil(localFile1.getAbsolutePath(), localFile1);
					rwidth = localImageUtil.getWidth();
					rheight = localImageUtil.getHeight();
					if (rwidth > iwidth || rheight > iheight) failed = true;
				}
				this.worker.delete(0, this.worker.length());
				if (this.tag)
					this.worker.append("<img src=\"");
				this.worker.append(InitServlet.getConfig().getServletContext().getInitParameter("URLHome"));
				if (localFile2.exists() && localFile2.length() > 0) {
					this.worker.append(StringUtil.replace(localFile2.getAbsolutePath().substring(str.length()), "\\", "/"));
				} else {
					this.worker.append(StringUtil.replace(localFile1.getAbsolutePath().substring(str.length()), "\\", "/"));
					failed = true;
				}
				if (this.tag) {
					this.worker.append("\" ");
					if (this.alt != null)
						this.worker.append("alt=\"").append(this.alt).append("\" ");
					if (this.border != null)
						this.worker.append("border=\"").append(this.border).append("\" ");
					if (this.style != null)
						this.worker.append("style=\"").append(this.style).append("\" ");
					if (this.usemap != null)
						this.worker.append("usemap=\"").append(this.usemap).append("\" ");
					if (this.id != null)
						this.worker.append("id=\"").append(this.id).append("\" ");
					if (this.onclick != null)
						this.worker.append("onclick=\"").append(this.onclick).append("\" ");
					if (this.align != null)
						this.worker.append("align=\"").append(this.align).append("\" ");
					if (setWidthHeightAttributes) {
						if (this.width != null) {
							this.worker.append("width=\"").append(this.width).append("\" ");
						}
						if (this.height != null) {
							this.worker.append("height=\"").append(this.height).append("\" ");
						}
					} else if (failed && (rheight <= 0 || rwidth <= 0)) {
						this.worker.append("width=\"").append(this.width).append("\" ");
						this.worker.append("height=\"").append(this.height).append("\" ");
					} else if (failed) {
						if (this.iwidth > 0 && this.iwidth < this.rwidth)
							this.worker.append("width=\"").append(this.width).append("\" ");
						if (this.iheight > 0 && this.iheight < this.rheight)
							this.worker.append("height=\"").append(this.height).append("\" ");
					}
					this.worker.append("/>");
				}
				try {
					if (paramJspWriter != null) paramJspWriter.print(this.worker.toString());
				} catch (Exception localException2) {
					this.debugString.append(localException2.getMessage());
				}
			} else {

				String fileName = getNewImage(localFile1); // This renames the file by suffixing it with the dimensions e.g. file1 becomes file1_100_90.
				if (fileName.contains("/files"))
					fileName = fileName.substring(fileName.indexOf("/files"));
				else if (fileName.contains("/documents"))
					fileName = fileName.substring(fileName.indexOf("/documents"));

				String[] s = fileName.split("/");
				String documentName = null;
				if (s.length > 2) {
					this.id = s[s.length - 2];
					documentName = s[s.length - 1];
				}

				String s3url = null;
				if (StringUtils.isNotBlank(InitServlet.getSystemParam("Pipeline-ClientID"))) {
					try {
						s3url = FileMoveMap.getInstance().getDocumentURLByFilePath(fileName, documentName, this.id);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (s3url == null) {
					//possible that the image has not been resized

					s = localFile1.getAbsolutePath().split("/");
					if (s.length > 2) {
						this.id = s[s.length - 2];
						documentName = s[s.length - 1];
					}
					if (StringUtils.isNotBlank(InitServlet.getSystemParam("Pipeline-ClientID"))) {
						try {
							s3url = FileMoveMap.getInstance().getDocumentURLByFilePath(this.worker.toString(), documentName, this.id);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				double ratio = 1;
				
				try {
					if (s3url != null) {
						BufferedImage image = null;
						if (!s3url.equalsIgnoreCase("null")) {
							try {
								image = ImageIO.read(new URL(s3url));
							} catch (IIOException e) {
								System.out.println("Unable to read image for " + s3url + " " + e.getMessage());
							}
						}
						// So if a resized file was empty when it was originally moved over to s3 then it will come back as blank.
						// In order to cater for this, we need to check that the file is not null then try get the file based on the original filename.
						if (image != null) {
							rwidth = image.getWidth();
							rheight = image.getHeight();
							ratio = Integer.valueOf(rwidth).doubleValue() / Integer.valueOf(rheight).doubleValue();
						} else if (StringUtils.isNotBlank(InitServlet.getSystemParam("Pipeline-ClientID"))) { 
							try {
								s3url = FileMoveMap.getInstance().getDocumentURLByFilePath(localFile1.getName(), documentName, this.id);
							} catch (Exception e) {
								e.printStackTrace();
								s3url = localFile1.getName();
							}
							image = ImageIO.read(new URL(s3url));
							if (image != null) {
								rwidth = image.getWidth();
								rheight = image.getHeight();
								ratio = Integer.valueOf(rwidth).doubleValue() / Integer.valueOf(rheight).doubleValue();
							} else {
								this.debugString.append("Could not find image, no actions performed\r\n");
								this.debugString.append("Img Loc Searched: ").append(this.worker).append("\r\n");
							}
						}
					} else {
						BufferedImage image = null;
						try {
							image = ImageIO.read(localFile1);
						} catch (IIOException e) {
							System.out.println("Unable to read image for " + s3url + " " + e.getMessage());
						}
						// So if a resized file was empty when it was originally moved over to s3 then it will come back as blank.
						// In order to cater for this, we need to check that the file is not null then try get the file based on the original filename.
						if (image != null) {
							rwidth = image.getWidth();
							rheight = image.getHeight();
							ratio = Integer.valueOf(rwidth).doubleValue() / Integer.valueOf(rheight).doubleValue();
						} else if (StringUtils.isNotBlank(InitServlet.getSystemParam("Pipeline-ClientID"))) {
							try {
							s3url = FileMoveMap.getInstance().getDocumentURLByFilePath(localFile1.getName(), documentName, this.id);
							} catch (Exception e) {
								e.printStackTrace();
								s3url = localFile1.getName();
							}
							image = ImageIO.read(new URL(s3url));
							if (image != null) {
								rwidth = image.getWidth();
								rheight = image.getHeight();
								ratio = Integer.valueOf(rwidth).doubleValue() / Integer.valueOf(rheight).doubleValue();
							} else {
								this.debugString.append("Could not find image, no actions performed\r\n");
								this.debugString.append("Img Loc Searched: ").append(this.worker).append("\r\n");
							}
						}
					}
				} catch (IOException e) {
					System.out.println("Unable to read image for " + s3url + " " + e.getMessage());
				}

				this.worker.delete(0, this.worker.length());

				boolean resizeToRatio = true;
				//If the width and height are both provided then don't need to resize according to the ratio
				if (!width.equalsIgnoreCase(height))
					resizeToRatio = false;

				if (width == null || width.equalsIgnoreCase("")) {
					//Should calculate width of image based on the height
					if (height != null && !height.equals("0")) {
						width = String.valueOf(Double.valueOf(Integer.valueOf(height) * ratio).intValue());
					} else {
						width = "0";
					}
				}

				if (rwidth < Integer.valueOf(width))
					width = String.valueOf(rwidth);

				if (height == null || height.equalsIgnoreCase("")) {
					//Should calculate width of image based on the width
					if (width != null && !width.equals("0")) {
						height = String.valueOf(Double.valueOf(Integer.valueOf(width) / ratio).intValue());
					} else {
						height = "0";
					}
				}

				if (rheight < Integer.valueOf(height)) {
					height = String.valueOf(rheight);
				}

 				if (resizeToRatio || ratio>1.5 || ratio<0.5) {
					//Check the final dimensions based on the ratio between width and height and recalculate if necessary.
					if (ratio > 1) {
						// width of original image > height of original
						height = String.valueOf(Double.valueOf(Integer.valueOf(width) / ratio).intValue());
					} else if (ratio < 1) {
						// height of original image > width of original
						width = String.valueOf(Double.valueOf(Integer.valueOf(height) * ratio).intValue());
					}
				}

				if (s3url != null) {
					this.worker.append("<img src=\"" + s3url + "\" ");

					if (width != null && !width.equals("0")) {
						this.worker.append("width=\" " + width + "\"");
					} else if (rwidth != 0) {
						this.worker.append("width=\" " + rwidth + "\"");
					}

					if (height != null && !height.equals("0")) {
						this.worker.append("height=\" " + height + "\"");
					} else if (rheight != 0) {
						this.worker.append("height=\" " + rheight + "\"");
					}
					this.worker.append("/>");
				}

				try {
					if (paramJspWriter != null) paramJspWriter.print(this.worker.toString());
				} catch (Exception e) {
					this.debugString.append(e.getMessage());
				}
			}

		}
		if (!(this.debug))
			return;
		try {
			if (paramJspWriter != null) paramJspWriter.print(this.debugString.toString());
		} catch (Exception localException1) {
		}
	}

	protected String getNewImage(File paramFile) {
		this.worker.delete(0, this.worker.length());
		File localFile = paramFile.getParentFile();
		int i = paramFile.getName().lastIndexOf(".");
		if (i == -1) i = paramFile.getName().length();
		this.worker.append(localFile.getAbsolutePath());
		this.worker.append("/");
		this.worker.append(paramFile.getName().substring(0, i));
		if ((this.iwidth > 0) && (this.iheight > 0))
			this.worker.append("_").append(this.width).append("_").append(this.height);
		else if (this.iwidth > 0)
			this.worker.append("_").append(this.width).append("_").append("CALC");
		else if (this.iheight > 0)
			this.worker.append("_").append("CALC").append("_").append(this.height);
		else
			this.worker.append("_").append("COMP");
		this.worker.append(paramFile.getName().substring(i));
		return this.worker.toString();
	}

	public void setAlt(String paramString) {
		this.alt = paramString;
	}

	public String getAlt() {
		return this.alt;
	}

	public void setBorder(String paramString) {
		this.border = paramString;
	}

	public String getBorder() {
		return this.border;
	}

	public void setStyle(String paramString) {
		this.style = paramString;
	}

	public String getStyle() {
		return this.style;
	}

	public void setUsemap(String paramString) {
		this.usemap = paramString;
	}

	public String getUsemap() {
		return this.usemap;
	}

	public void setId(String paramString) {
		this.id = paramString;
	}

	public String getId() {
		return this.id;
	}

	public void setOnclick(String paramString) {
		this.onclick = paramString;
	}

	public String getOnclick() {
		return this.onclick;
	}

	public void setAlign(String paramString) {
		this.align = paramString;
	}

	public String getAlign() {
		return this.align;
	}
}