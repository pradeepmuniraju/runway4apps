package com.sok.framework;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.dom4j.*;


public class TableSpec extends XMLSpec
{ 
	private String tablename;
	private String primarykey;
	private String indexedtable = null;
	private Element[] tablefields;
	private Element[] tableindexes; 
	private Element[] tableupdates; 
	final boolean sync;

	public static String _name = "name";
	public static String _primarykey = "primarykey";
	public static String _indexedtable = "indexedtable";
	public static String _sync = "sync";
	
	public static String _field = "field";
	public static String _type = "type";
	public static String _format = "format";
	public static String _table = "table";
	public static String _label = "label";
	public static String _import = "import";
	public static String _inputmethod = "inputmethod";
	public static String _valuelist = "valuelist";
	
	/// <field name="ProductNum" type="12" import="true" label="Product Number" dtype="varchar" dlen="50" dnull="false" dautoincr="false" ddefault="N" />
	public static String _dtype = "dtype"; // varchar char int double float etc. 
	public static String _dlen = "dlen";	// numeric 
	public static String _dnull = "dnull";	// allow null, true/false 
	public static String _dautoincr = "dautoincr";		// true/false
	public static String _ddefault = "ddefault"; 		// null if not defined, value as given
	
	/*
	 * <index name="IX_EstateStage" fields="EstateID,StageID" unique="false" />	<!-- current version -->
	 * <index name="IX_EstateStage" fields="EstateID/A,StageID/D" unique="false" /> <!-- future if you want to use asc/desc indexes -->
	 */
	public static String _index = "index";
	public static String _ixunique = "unique"; 
	public static String _ixfields = "fields"; 
	
	public static String _update = "update"; 
	public static String _created = "created"; 
	/*
	 * The created date is used to validate against the date recorded on the dataset, so that it knows whether to run an update or not.
	 * <update created="2013-05-13"> update SomeTable set SomeField = 'SomeVal' where SomeField is null or SomeField = '' </update> 
	 */	
	public TableSpec(String specname, String tablename, String key)	
	{
		super();
        
		super.name = specname;
		this.tablename = tablename;
		this.primarykey = key;
		this.sync = false;

		Element root = super.document.addElement(_table);
		root.addAttribute(_name, tablename);
		root.addAttribute(_primarykey, primarykey);
		root.addText(_n);
	}

	public TableSpec(String path)	
	{	
		super(path);
        if(super.document!=null)
        {        
            Element root = super.document.getRootElement();
            if(root != null)
            {        
            	
               if(!StringUtils.equalsIgnoreCase(root.getName(), "table")) {
                 	throw new IllegalArgumentException("Invalid Table for path " + path);
               }
        	   tablename = root.attributeValue(_name);
        	   primarykey = root.attributeValue(_primarykey);
        	   indexedtable = root.attributeValue(_indexedtable);
        	   sync = !"false".equals(root.attributeValue(_sync));
        	   
        	   tablefields = super.getElementArray(root,_field);
        	   tableindexes = super.getElementArray(root,_index); 
        	   tableupdates = super.getElementArray(root,_update);
        	   if(tableindexes == null) {
        		   tableindexes = new Element[0];
        	   }
        	   if(tableupdates == null) {
        		   tableupdates = new Element[0];
        	   }
        	   if(StringUtils.isBlank(tablename)) {
        		   throw new IllegalArgumentException("Invalid Table for path " + path);
        	   }
            }
            else
            {
                throw new NullPointerException("Document has no root element - " + path);
            }
        }
        else
        {
            throw new NullPointerException("Document cannot be loaded - " + path);
        }        
	}
	
	public boolean isSyncEnabled() {
		return sync;
	}

	public synchronized void addField(String fieldname, String type, String format, String label, String inputmethod, String valuelist)
	{
		Element root = super.document.getRootElement();
		root.addText(_t);

		Element node = root.addElement(_field);
		
		node.addAttribute(_name, fieldname);
		node.addAttribute(_type, type);

		if(format!=null && format.length()!=0)
		{
			node.addAttribute(_format, format);
		}

		if(label!=null && label.length()!=0)
		{
			node.addAttribute(_label, label);
		}

		if(inputmethod!=null && inputmethod.length()!=0)
		{
			node.addAttribute(_inputmethod, inputmethod);
		}

		if(valuelist!=null && valuelist.length()!=0)
		{
			node.addAttribute(_valuelist, valuelist);
		}

		root.addText(_n);

		if(tablefields==null)
		{
			tablefields = super.getElementArray(root,_field);
		}
	}

	public String getPrimaryKeyName()
	{
		return(primarykey);
	}
	
	public String getSecurityTableName() {
		if(StringUtils.isNotBlank(indexedtable)) {
			return indexedtable;
		}
		return tablename;
	}
	
	public String getTableName()
	{
		return(tablename);
	}
	
	public int getFieldsLength()
	{
		return(super.getCount(tablefields));
	}
	
	public String getFieldName(int i)
	{
		return(super.getAttribute(tablefields, i, _name));
	}

	public int getFieldType(int i)
	{
		String num = super.getAttribute(tablefields, i, _type);
		if(num!=null)
		{
			return(Integer.parseInt(num));
		}
		return(-1);
	}

	public String getFormat(int i)
	{
		return(super.getAttribute(tablefields, i, _format));
	}

	public String getLabel(int i)
	{
		return(super.getAttribute(tablefields, i, _label));
	}

	public String getImport(int i)
	{
		String value = super.getAttribute(tablefields, i, _import);
		if(value!=null)
		{
			return(value);
		}
		return("false");
	}

	public String getInputMethod(int i)
	{
		return(super.getAttribute(tablefields, i, _inputmethod));
	}

	public String getValueList(int i)
	{
		return(super.getAttribute(tablefields, i, _valuelist));
	}
	
	public String getDataType(int i) {
		return super.getAttribute(tablefields, i, _dtype);
	}
	/*
	 * Primary Keys should not be null, nor should anything marked explicitly as such.
	 */
	public boolean isDataNullable(int i) {
		return !StringUtils.equals(this.getFieldName(i), this.getPrimaryKeyName()) && !"false".equals(super.getAttribute(tablefields, i, _dnull));
	}
	public String getDataLength(int i) {
		return super.getAttribute(tablefields, i, _dlen);
	}
	public boolean isDataAutoIncrement(int i) {
		return "true".equals(super.getAttribute(tablefields, i, _dautoincr));
	}	
	public String getDataDefault(int i) {
		return super.getAttribute(tablefields, i, _ddefault);
	}
	public void setDataType(int i, String type) {
		if(i>=0 && i<tablefields.length){
			tablefields[i].addAttribute(_dtype, type);
		}
	}
	public void setDataLength(int i, String len) {
		if(i>=0 && i<tablefields.length){
			if(len != null) 
				tablefields[i].addAttribute(_dlen, len);
			else {
				Attribute a = tablefields[i].attribute(_dlen); 
				if(a != null) {
					tablefields[i].remove(a); 
				}
			}
		}
	}
	public void setDataNullable(int i, boolean nullable) {
		if(i>=0 && i<tablefields.length){
			if(!nullable)tablefields[i].addAttribute(_dnull, String.valueOf(nullable)); 
			else {
				Attribute a = tablefields[i].attribute(_dnull); 
				if(a != null) {
					tablefields[i].remove(a); 
				}
			} 
		}
	}
	public void setDataDefault(int i, String def) {
		if(i>=0 && i<tablefields.length){
			if(def != null) tablefields[i].addAttribute(_ddefault, def);
			else {
				Attribute a = tablefields[i].attribute(_ddefault); 
				if(a != null) {
					tablefields[i].remove(a); 
				}
			}
		}
	}
	public void setDataAutoIncrement(int i, boolean auto) {
		if(i>=0 && i<tablefields.length){
			if(auto) tablefields[i].addAttribute(_dautoincr, String.valueOf(auto));
			else {
				Attribute a = tablefields[i].attribute(_dautoincr); 
				if(a != null) {
					tablefields[i].remove(a); 
				}
			} 
		} 
	}
	
	public int getFieldsIndex(String name)
	{
		return(super.getIndexForName(tablefields,_name,name));
	}

	public boolean hasField(String name)
	{
		return(super.getHasAttribute(tablefields,_name,name));
	}
	
	
	public int getIndexIndex(String name)
	{
		return(super.getIndexForName(tableindexes,_name,name));
	}

	public boolean hasIndex(String name)
	{
		return(super.getHasAttribute(tableindexes,_name,name));
	}
	
	public int getIndexLength() {
		return tableindexes != null ? tableindexes.length : 0;
	}
	
	public int createIndex(String name, boolean unique) {
		
		Element el = super.getDocument().getRootElement().addElement(_index); 
		el.addAttribute(_name, name);
		el.addAttribute(_ixunique, String.valueOf(unique));
		tableindexes = super.getElementArray(super.document.getRootElement(),_index);
		return getIndexIndex(name);
	}

	public void setIndexUnique(int i, boolean unique) {
		if(i>=0 && i<tableindexes.length){
			if(unique) tableindexes[i].addAttribute(_ixunique, String.valueOf(unique));
			else {
				Attribute a = tableindexes[i].attribute(_ixunique); 
				if(a != null) {
					tableindexes[i].remove(a); 
				}
			} 
		}
	}

	public boolean isIndexUnique(int i) {
		return "true".equals(super.getAttribute(tableindexes, i, _ixunique));
	}
	
	public String getIndexFields(int i) {
		return super.getAttribute(tableindexes, i, _ixfields);
	}
	
	public void setIndexFields(int i, String fields) {
		if(i>=0 && i<tableindexes.length){
			tableindexes[i].addAttribute(_ixfields, fields);
		}
	}

	public String getIndexName(int ixs) {
		return super.getAttribute(tableindexes, ixs, _name);
	}
	
	public int findIndex(String fields) {
		for(int i=0; i< tableindexes.length; i++) {
			if(StringUtils.equals(super.getAttribute(tableindexes, i, _ixfields), fields)) {
				return i;
			}
		}
		return -1;
	}
	
	public static class TableUpdate {
		public final Date d; 
		public final String u; 
		public TableUpdate(Date d, String u) {
			this.d = d; 
			this.u = u;
		}
	}

	public List<TableUpdate> getUpdatesAfter(Date createdDate) {
		org.slf4j.LoggerFactory.getLogger(this.getClass()).debug("getUpdatesAfter({})[{}]", createdDate, getTableName()); 
		if(tableupdates == null || tableupdates.length == 0) return Collections.emptyList();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		List<TableUpdate> updates = new ArrayList<TableUpdate>(tableupdates.length);
		for(Element e: tableupdates) {
			Date d = null; 
			try { 
				d = df.parse(super.getAttribute(e, _created)); 
			} catch(ParseException pe) {
				throw new RuntimeException("Invalid Update Created Date " + String.valueOf(super.getAttribute(e, _created)));
			}
			// if we do not know the last app startup, we assume that all updates must be run.
			if(createdDate == null || d.after(createdDate) || DateUtils.isSameDay(d, createdDate)) {
				String up = StringUtils.trimToNull(e.getText());
				if(up != null) {
					updates.add(new TableUpdate(d, up));
				} else {
					throw new RuntimeException("Invalid Update: " + d.toString());
				}
			} else if(createdDate != null){
				org.slf4j.LoggerFactory.getLogger(this.getClass()).debug("d={} after cd={} ? {}, isSameDay={}", new String[]{String.valueOf(d), String.valueOf(createdDate), String.valueOf(d.after(createdDate)), String.valueOf(DateUtils.isSameDay(d, createdDate))});
			}
		}
		return updates; 
	}
}
