package com.sok.framework;
public class SpecUtil
{
	public static String _tiny = "Tiny";
	public static String _short = "Short";
	public static String _medium = "Medium";
	public static String _long = "Long";
	public static String _multiline = "Multi-line";

	public static String _select = "Drop Down";
	public static String _select_set = "Multi Drop Down";
	public static String _list = "List";
	public static String _radio = "Radio Button";
	public static String _checkbox = "Checkbox";

	public static String _date = "Date";
	public static String _number = "Number";
	public static String _currency = "Currency";
	public static String _yesno = "YesNo";

	public static String sql_varchar5 = "varchar(5)";
	public static String sql_varchar10 = "varchar(10)";
	public static String sql_varchar50 = "varchar(50)";
	public static String sql_varchar100 = "varchar(100)";
	public static String sql_varchar200 = "varchar(200)";
	public static String sql_varchar500 = "varchar(500)";
	public static String sql_varchar2000 = "varchar(2000)";
	public static String sql_text = "text";
	public static String sql_datetime = "datetime";
	public static String sql_int = "int";
	public static String sql_decimal = "decimal";

	public static int sqlvarchar = 12;
	public static int sqldattime = 93;
	public static int sqlint = 4;
	public static int sqldecimal = 6;

	public static String[] inputtypes = {
		_tiny,
		_short,
 		_medium,
 		_long,
 		_multiline,
 		_select, 
		_list, 
		_checkbox, 
		_date, 
		_number, 
		_currency,
		_yesno,
		_radio
	};

	public static String[] maxlength = {
		"10", //_tiny
		"50", //_short
		"100", //_medium
		"200", //_long
		null, //_multiline
		"100", //_select
		"200", //_list
		"200", //_checkbox
		null, //_date
		null, //_number
		null, //_currency
		"5", //_yesno
		"200" //_radio
	};

	public static String[] sqltypes = {
		sql_varchar10, //_tiny
		sql_varchar50, //_short
		sql_varchar100, //_medium
		sql_varchar200, //_long
		sql_text, //_multiline
		sql_varchar100, //_select
		sql_varchar200, //_list
		sql_varchar200, //_checkbox
		sql_datetime, //_date
		sql_int, //_number
		sql_decimal, //_currency
		sql_varchar5, //_yesno
		sql_varchar200 //_radio
	};

	public static int[] sqlcodes = {
		sqlvarchar, //_tiny
		sqlvarchar, //_short
		sqlvarchar, //_medium
		sqlvarchar, //_long
		sqlvarchar, //_multiline
		sqlvarchar, //_select
		sqlvarchar, //_list
		sqlvarchar, //_checkbox
		sqldattime, //_date
		sqlint, //_number
		sqldecimal, //_currency
		sqlvarchar, //_yesno
		sqlvarchar //_radio
	};

	public static String getCreateStatement(TableSpec tspec)
	{
		String primarykey = tspec.getPrimaryKeyName();
		StringBuffer temp = new StringBuffer();
		temp.append("create table ");
		temp.append(tspec.getTableName());
		temp.append(" (");
		temp.append(primarykey);
		temp.append(" varchar(50) PRIMARY KEY");
		String tempname = null;
		
		for(int i=0; i<tspec.getFieldsLength(); i++)
		{
			tempname = tspec.getFieldName(i);
			if(!tempname.equals(primarykey))
			{
				temp.append(", ");
				temp.append(tempname);
				temp.append(" ");
				temp.append(getSQLTypeForInputType(tspec.getInputMethod(i)));
			}
		}
		temp.append(")");
		return(temp.toString());
	} 

	public static String getCreateTableStatement(TableSpec tspec)
	{
		StringBuffer temp = new StringBuffer();
		temp.append("create table ");
		temp.append(tspec.getTableName());
		temp.append(" (");
		temp.append(tspec.getPrimaryKeyName());
		temp.append(" varchar(50) PRIMARY KEY)");
		return(temp.toString());
	}

	public static String getAddColumnStatement(String fieldname, String type, TableSpec tspec)
	{
		StringBuffer temp = new StringBuffer();
		temp.append("alter table ");
		temp.append(tspec.getTableName());
		temp.append(" add ");
		temp.append(fieldname);
		temp.append(" ");
		temp.append(getSQLTypeForInputType(type));
		return(temp.toString());
	}

	public static String getAlterColumnStatement(String fieldname, String type, TableSpec tspec)
	{
		StringBuffer temp = new StringBuffer();
		temp.append("alter table ");
		temp.append(tspec.getTableName());
		temp.append(" alter ");
		temp.append(fieldname);
		temp.append(" ");
		temp.append(getSQLTypeForInputType(type));
		return(temp.toString());
	}

	public static String getDropColumnStatement(String fieldname, String type, TableSpec tspec)
	{
		StringBuffer temp = new StringBuffer();
		temp.append("alter table ");
		temp.append(tspec.getTableName());
		temp.append(" drop ");
		temp.append(fieldname);
		return(temp.toString());
	}

	public static String getSQLTypeForInputType(String type)
	{
		for(int i=0; i<inputtypes.length; i++)
		{
			if(type.equals(inputtypes[i]))
			{
				return(sqltypes[i]);
			}
		}
		return(null);
	}

	public static int getSQLCodeForInputType(String type)
	{
		for(int i=0; i<inputtypes.length; i++)
		{
			if(type.equals(inputtypes[i]))
			{
				return(sqlcodes[i]);
			}
		}
		return(-1);
	}

	public static String getMaxLengthForInputType(String type)
	{
		for(int i=0; i<inputtypes.length; i++)
		{
			if(type.equals(inputtypes[i]))
			{
				return(maxlength[i]);
			}
		}
		return(null);
	}
}
