package com.sok.framework;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.pdf.PropertyPDFGenerator;
import com.sok.runway.crm.cms.properties.pdf.templates.model.LetterOption;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;

import org.apache.commons.codec.binary.*;
import org.apache.velocity.app.VelocityEngine;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import org.bouncycastle.jce.provider.*;
import org.apache.commons.codec.binary.*;

public class RunwayUtil {

	private static final Logger logger = LoggerFactory.getLogger(RunwayUtil.class);

	public static final SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy");
	public static final SimpleDateFormat yyyymmdd = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat yyyymmddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final NumberFormat currency = NumberFormat.getCurrencyInstance();

	/*
	private static final char[] chars = new char[128];

	static {
		for (char c = 128; c < 256; ++c) chars[c - 128] = Character.valueOf(c);
	}
	 */
	private static final HashMap<String,String>	encodingMap = new HashMap<String,String>();
	static {
		/* we don't really need these ones 
		encodingMap.put("&#x0022;", "&quot;");
		encodingMap.put("&#x0027;", "&apos;");
		encodingMap.put("&#x003C;", "&lt;");
		encodingMap.put("&#x003E;", "&gt;");
		 */
		encodingMap.put("&#x0080;", "&euro;");
		//encodingMap.put("&#x0081;", "&;");
		encodingMap.put("&#x0082;", "&sbquo;");
		encodingMap.put("&#x0083;", "&fnof;");
		encodingMap.put("&#x0084;", "&bdquo;");
		encodingMap.put("&#x0085;", "&hellip;");
		encodingMap.put("&#x0086;", "&dagger;");
		encodingMap.put("&#x0087;", "&Dagger;");
		encodingMap.put("&#x0088;", "&circ;");
		encodingMap.put("&#x0089;", "&permil;");
		encodingMap.put("&#x008A;", "&Scaron;");
		encodingMap.put("&#x008B;", "&lsaquo;");
		encodingMap.put("&#x008C;", "&OElig;");
		//encodingMap.put("&#x008D;", "&;");
		encodingMap.put("&#x008E;", "&Zcaron;");
		//encodingMap.put("&#x008F;", "&;");
		//encodingMap.put("&#x0090;", "&;");
		encodingMap.put("&#x0091;", "&lsquo;");
		encodingMap.put("&#x0092;", "&rsquo;");
		encodingMap.put("&#x0093;", "&ldquo;");
		encodingMap.put("&#x0094;", "&rdquo;");
		encodingMap.put("&#x0095;", "&bull;");
		encodingMap.put("&#x0096;", "&ndash;");
		encodingMap.put("&#x0097;", "&mdash;");
		encodingMap.put("&#x0098;", "&tilde;");
		encodingMap.put("&#x0099;", "&trade;");
		encodingMap.put("&#x009A;", "&scaron;");
		encodingMap.put("&#x009B;", "&rsaquo;");
		encodingMap.put("&#x009C;", "&oelig;");
		//encodingMap.put("&#x009D;", "&;");
		encodingMap.put("&#x009E;", "&zcaron;");
		encodingMap.put("&#x009F;", "&Yuml;");

		encodingMap.put("&#x00A0;", "&nbsp;");
		encodingMap.put("&#x00A1;", "&iexcl;");
		encodingMap.put("&#x00A2;", "&cent;");
		encodingMap.put("&#x00A3;", "&pound;");
		encodingMap.put("&#x00A4;", "&curren;");
		encodingMap.put("&#x00A5;", "&yen;");
		encodingMap.put("&#x00A6;", "&brvbar;");
		encodingMap.put("&#x00A7;", "&sect;");
		encodingMap.put("&#x00A8;", "&uml;");
		encodingMap.put("&#x00A9;", "&copy;");
		encodingMap.put("&#x00AA;", "&ordf;");
		encodingMap.put("&#x00AB;", "&laquo;");
		encodingMap.put("&#x00AC;", "&not;");
		encodingMap.put("&#x00AD;", "&shy;");
		encodingMap.put("&#x00AE;", "&reg;");
		encodingMap.put("&#x00AF;", "&macr;");

		encodingMap.put("&#x00B0;", "&deg;");
		encodingMap.put("&#x00B1;", "&plusmn;");
		encodingMap.put("&#x00B2;", "&sup2;");
		encodingMap.put("&#x00B3;", "&sup3;");
		encodingMap.put("&#x00B4;", "&acute;");
		encodingMap.put("&#x00B5;", "&micro;");
		encodingMap.put("&#x00B6;", "&para;");
		encodingMap.put("&#x00B7;", "&middot;");
		encodingMap.put("&#x00B8;", "&cedil;");
		encodingMap.put("&#x00B9;", "&sup1;");
		encodingMap.put("&#x00BA;", "&ordm;");
		encodingMap.put("&#x00BB;", "&raquo;");
		encodingMap.put("&#x00BC;", "&frac14;");
		encodingMap.put("&#x00BD;", "&frac12;");
		encodingMap.put("&#x00BE;", "&frac34;");
		encodingMap.put("&#x00BF;", "&iquest;");

		encodingMap.put("&#x00C0;", "&Agrave;");
		encodingMap.put("&#x00C1;", "&Aacute;");
		encodingMap.put("&#x00C2;", "&Acirc;");
		encodingMap.put("&#x00C3;", "&Atilde;");
		encodingMap.put("&#x00C4;", "&Auml;");
		encodingMap.put("&#x00C5;", "&Aring;");
		encodingMap.put("&#x00C6;", "&AElig;");
		encodingMap.put("&#x00C7;", "&Ccedil;");
		encodingMap.put("&#x00C8;", "&Egrave;");
		encodingMap.put("&#x00C9;", "&Eacute;");
		encodingMap.put("&#x00CA;", "&Ecirc;");
		encodingMap.put("&#x00CB;", "&Euml;");
		encodingMap.put("&#x00CC;", "&Igrave;");
		encodingMap.put("&#x00CD;", "&Iacute;");
		encodingMap.put("&#x00CE;", "&Icirc;");
		encodingMap.put("&#x00CF;", "&Iuml;");

		encodingMap.put("&#x00D0;", "&ETH;");
		encodingMap.put("&#x00D1;", "&Ntilde;");
		encodingMap.put("&#x00D2;", "&Ograve;");
		encodingMap.put("&#x00D3;", "&Oacute;");
		encodingMap.put("&#x00D4;", "&Ocirc;");
		encodingMap.put("&#x00D5;", "&Otilde;");
		encodingMap.put("&#x00D6;", "&Ouml;");
		encodingMap.put("&#x00D7;", "&times;");
		encodingMap.put("&#x00D8;", "&Oslash;");
		encodingMap.put("&#x00D9;", "&Ugrave;");
		encodingMap.put("&#x00DA;", "&Uacute;");
		encodingMap.put("&#x00DB;", "&Ucirc;");
		encodingMap.put("&#x00DC;", "&Uuml;");
		encodingMap.put("&#x00DD;", "&Yacute;");
		encodingMap.put("&#x00DE;", "&THORN;");
		encodingMap.put("&#x00DF;", "&szlig;");

		encodingMap.put("&#x00E0;", "&agrave;");
		encodingMap.put("&#x00E1;", "&aacute;");
		encodingMap.put("&#x00E2;", "&acirc;");
		encodingMap.put("&#x00E3;", "&atilde;");
		encodingMap.put("&#x00E4;", "&auml;");
		encodingMap.put("&#x00E5;", "&aring;");
		encodingMap.put("&#x00E6;", "&aelig;");
		encodingMap.put("&#x00E7;", "&ccedil;");
		encodingMap.put("&#x00E8;", "&egrave;");
		encodingMap.put("&#x00E9;", "&eacute;");
		encodingMap.put("&#x00EA;", "&ecirc;");
		encodingMap.put("&#x00EB;", "&euml;");
		encodingMap.put("&#x00EC;", "&igrave;");
		encodingMap.put("&#x00ED;", "&iacute;");
		encodingMap.put("&#x00EE;", "&icirc;");
		encodingMap.put("&#x00EF;", "&iuml;");

		encodingMap.put("&#x00F0;", "&eth;");
		encodingMap.put("&#x00F1;", "&ntilde;");
		encodingMap.put("&#x00F2;", "&ograve;");
		encodingMap.put("&#x00F3;", "&oacute;");
		encodingMap.put("&#x00F4;", "&ocirc;");
		encodingMap.put("&#x00F5;", "&otilde;");
		encodingMap.put("&#x00F6;", "&ouml;");
		encodingMap.put("&#x00F7;", "&divide;");
		encodingMap.put("&#x00F8;", "&oslash;");
		encodingMap.put("&#x00F9;", "&ugrave;");
		encodingMap.put("&#x00FA;", "&uacute;");
		encodingMap.put("&#x00FB;", "&ucirc;");
		encodingMap.put("&#x00FC;", "&uuml;");
		encodingMap.put("&#x00FD;", "&yacute;");
		encodingMap.put("&#x00FE;", "&thorn;");
		encodingMap.put("&#x00FF;", "&yuml;");

		// the following are not normaly used but are included just in case

		encodingMap.put("&#x0152;", "&OElig;");
		encodingMap.put("&#x0153;", "&oelig;");
		encodingMap.put("&#x0160;", "&Scaron;");
		encodingMap.put("&#x0161;", "&scaron;");
		encodingMap.put("&#x0178;", "&Yuml;");
		encodingMap.put("&#x0192;", "&fnof;");
		encodingMap.put("&#x02C6;", "&circ;");
		encodingMap.put("&#x02DC;", "&tilde;");
		encodingMap.put("&#x0391;", "&Alpha;");
		encodingMap.put("&#x0392;", "&Beta;");
		encodingMap.put("&#x0393;", "&Gamma;");
		encodingMap.put("&#x0394;", "&Delta;");
		encodingMap.put("&#x0395;", "&Epsilon;");
		encodingMap.put("&#x0396;", "&Zeta;");
		encodingMap.put("&#x0397;", "&Eta;");
		encodingMap.put("&#x0398;", "&Theta;");

		encodingMap.put("&#x0399;", "&Iota;");
		encodingMap.put("&#x039A;", "&Kappa;");
		encodingMap.put("&#x039B;", "&Lambda;");
		encodingMap.put("&#x039C;", "&Mu;");
		encodingMap.put("&#x039D;", "&Nu;");
		encodingMap.put("&#x039E;", "&Xi;");
		encodingMap.put("&#x039F;", "&Omicron;");
		encodingMap.put("&#x03A0;", "&Pi;");
		encodingMap.put("&#x03A1;", "&Rho;");
		encodingMap.put("&#x03A3;", "&Sigma;");
		encodingMap.put("&#x03A4;", "&Tau;");
		encodingMap.put("&#x03A5;", "&Upsilon;");
		encodingMap.put("&#x03A6;", "&Phi;");
		encodingMap.put("&#x03A7;", "&Chi;");
		encodingMap.put("&#x03A8;", "&Psi;");
		encodingMap.put("&#x03A9;", "&Omega;");

		encodingMap.put("&#x03B1;", "&alpha;");
		encodingMap.put("&#x03B2;", "&beta;");
		encodingMap.put("&#x03B3;", "&gamma;");
		encodingMap.put("&#x03B4;", "&delta;");
		encodingMap.put("&#x03B5;", "&epsilon;");
		encodingMap.put("&#x03B6;", "&zeta;");
		encodingMap.put("&#x03B7;", "&eta;");
		encodingMap.put("&#x03B8;", "&theta;");
		encodingMap.put("&#x03B9;", "&iota;");
		encodingMap.put("&#x03BA;", "&kappa;");
		encodingMap.put("&#x03BB;", "&lambda;");
		encodingMap.put("&#x03BC;", "&mu;");
		encodingMap.put("&#x03BD;", "&nu;");
		encodingMap.put("&#x03BE;", "&xi;");
		encodingMap.put("&#x03BF;", "&omicron;");
		encodingMap.put("&#x03C0;", "&pi;");

		encodingMap.put("&#x03C1;", "&rho;");
		encodingMap.put("&#x03C2;", "&sigmaf;");
		encodingMap.put("&#x03C3;", "&sigma;");
		encodingMap.put("&#x03C4;", "&tau;");
		encodingMap.put("&#x03C5;", "&upsilon;");
		encodingMap.put("&#x03C6;", "&phi;");
		encodingMap.put("&#x03C7;", "&chi;");
		encodingMap.put("&#x03C8;", "&psi;");
		encodingMap.put("&#x03C9;", "&omega;");
		encodingMap.put("&#x03D1;", "&thetasym;");
		encodingMap.put("&#x03D2;", "&upsih;");
		encodingMap.put("&#x03D6;", "&piv;");

		encodingMap.put("&#x2002;", "&ensp;");
		encodingMap.put("&#x2003;", "&emsp;");
		encodingMap.put("&#x2009;", "&thinsp;");
		encodingMap.put("&#x200C;", "&zwnj;");
		encodingMap.put("&#x200D;", "&zwj;");
		encodingMap.put("&#x200E;", "&lrm;");
		encodingMap.put("&#x200F;", "&rlm;");
		encodingMap.put("&#x2013;", "&ndash;");
		encodingMap.put("&#x2014;", "&mdash;");
		encodingMap.put("&#x2018;", "&lsquo;");
		encodingMap.put("&#x2019;", "&rsquo;");
		encodingMap.put("&#x201A;", "&sbquo;");
		encodingMap.put("&#x201C;", "&ldquo;");
		encodingMap.put("&#x201D;", "&rdquo;");
		encodingMap.put("&#x201E;", "&bdquo;");
		encodingMap.put("&#x2020;", "&dagger;");
		encodingMap.put("&#x2021;", "&Dagger;");
		//encodingMap.put("&#x2022;", "&ball;");
		encodingMap.put("&#x2026;", "&hellip;");
		encodingMap.put("&#x2030;", "&permil;");

		encodingMap.put("&#x2032;", "&prime;");
		encodingMap.put("&#x2033;", "&Prime;");
		encodingMap.put("&#x2039;", "&lsaquo;");
		encodingMap.put("&#x203A;", "&rsaquo;");
		encodingMap.put("&#x203E;", "&oline;");
		encodingMap.put("&#x2044;", "&frasl;");
		encodingMap.put("&#x20AC;", "&euro;");
		encodingMap.put("&#x2111;", "&image;");
		encodingMap.put("&#x2118;", "&weierp;");
		encodingMap.put("&#x211C;", "&real;");
		encodingMap.put("&#x2122;", "&trade;");
		encodingMap.put("&#x2135;", "&alefsym;");
		encodingMap.put("&#x2190;", "&larr;");
		encodingMap.put("&#x2191;", "&uarr;");
		encodingMap.put("&#x2192;", "&rarr;");
		encodingMap.put("&#x2193;", "&darr;");
		encodingMap.put("&#x2194;", "&harr;");
		encodingMap.put("&#x21B5;", "&carr;");
		encodingMap.put("&#x21D0;", "&lArr;");
		encodingMap.put("&#x21D1;", "&uArr;");

		encodingMap.put("&#x21D2;", "&rArr;");
		encodingMap.put("&#x21D3;", "&dArr;");
		encodingMap.put("&#x21D4;", "&hArr;");
		encodingMap.put("&#x2200;", "&forall;");
		encodingMap.put("&#x2202;", "&part;");
		encodingMap.put("&#x2203;", "&exist;");
		encodingMap.put("&#x2205;", "&empty;");
		encodingMap.put("&#x2207;", "&nabla;");
		encodingMap.put("&#x2208;", "&isin;");
		encodingMap.put("&#x2209;", "&notin;");
		encodingMap.put("&#x220B;", "&ni;");
		encodingMap.put("&#x220F;", "&prod;");
		encodingMap.put("&#x2211;", "&sum;");
		encodingMap.put("&#x2212;", "&minus;");
		encodingMap.put("&#x2217;", "&lowast;");
		encodingMap.put("&#x221A;", "&radic;");
		encodingMap.put("&#x221D;", "&prop;");
		encodingMap.put("&#x221E;", "&infin;");
		encodingMap.put("&#x2220;", "&ang;");
		encodingMap.put("&#x2227;", "&and;");

		encodingMap.put("&#x2228;", "&or;");
		encodingMap.put("&#x2229;", "&cap;");
		encodingMap.put("&#x222A;", "&cup;");
		encodingMap.put("&#x222B;", "&int;");
		encodingMap.put("&#x2234;", "&there4;");
		encodingMap.put("&#x223C;", "&sim;");
		encodingMap.put("&#x2245;", "&cong;");
		encodingMap.put("&#x2248;", "&asymp;");
		encodingMap.put("&#x2260;", "&ne;");
		encodingMap.put("&#x2261;", "&equiv;");
		encodingMap.put("&#x2264;", "&le;");
		encodingMap.put("&#x2265;", "&ge;");
		encodingMap.put("&#x2282;", "&sub;");
		encodingMap.put("&#x2283;", "&sup;");
		encodingMap.put("&#x2284;", "&nsub;");
		encodingMap.put("&#x2286;", "&sube;");
		encodingMap.put("&#x2287;", "&supe;");
		encodingMap.put("&#x2295;", "&oplus;");
		encodingMap.put("&#x2297;", "&otimes;");
		encodingMap.put("&#x22A5;", "&perp;");

		encodingMap.put("&#x22C5;", "&sdot;");
		encodingMap.put("&#x22EE;", "&vellip;");
		encodingMap.put("&#x2308;", "&lceil;");
		encodingMap.put("&#x2309;", "&rceil;");
		encodingMap.put("&#x230A;", "&lfloor;");
		encodingMap.put("&#x230B;", "&rfloor;");
		encodingMap.put("&#x2329;", "&lang;");
		encodingMap.put("&#x232A;", "&rang;");
		encodingMap.put("&#x25CA;", "&loz;");
		encodingMap.put("&#x2660;", "&spades;");
		encodingMap.put("&#x2663;", "&clubs;");
		encodingMap.put("&#x2665;", "&hearts;");
		encodingMap.put("&#x2666;", "&diams;");
	}

	public static String LotNameLabel = "";

	public static String encodeWindows1252(String text) {
		String ret = "";

		if (StringUtils.isBlank(text)) return ret;

		ret = new String(text);

		for (char c = 128; c < 256; ++c) {
			if (ret.indexOf(c) >= 0) {
				String h = Integer.toHexString(c);
				String hex = Integer.toHexString((int) c); //new String(Hex.encodeHex(String.valueOf(c).getBytes()));
				if (hex.length() == 2) hex = "00" + hex;
				else if (hex.length() == 3) hex = "0" + hex;
				hex = "&#x" + hex.toUpperCase() + ";";
				//System.out.println(text.substring(x, x + 1) + " - " + hex + " > " + Integer.toHexString(c) + " > " + c);

				String rep = "";
				if (encodingMap.containsKey(hex)) {
					rep = encodingMap.get(hex);
				} else {
					rep = hex;
				}

				int x = 0;
				while ((x = ret.indexOf(c)) >= 0) {
					if (x < (ret.length() - 1))
						ret = ret.substring(0,x) + rep + ret.substring(x + 1);
					else
						ret = ret.substring(0,x) + rep;
				}
			}
		}

		/*
		for (int x = 0; x < text.length(); ++x) {
			int c = text.codePointAt(x);
			if (c < 0x80) {
				ret += (char) c;
				//} else if (c < 0x2000) {
				//	String hex = new String(Hex.encodeHex(String.valueOf(c).getBytes()));
				//	if (hex.length() == 2) hex = "00" + hex;
				//	else if (hex.length() == 3) hex = "0" + hex;
				//	hex = "&#x" + hex.toUpperCase() + ";";
				//	if (encodingMap.containsKey(hex)) ret += encodingMap.get(hex);
			} else {
				//int cp = text.codePointAt(x);
				String h = Integer.toHexString(c);
				String hex = Integer.toHexString((int) c); //new String(Hex.encodeHex(String.valueOf(c).getBytes()));
				if (hex.length() == 2) hex = "00" + hex;
				else if (hex.length() == 3) hex = "0" + hex;
				hex = "&#x" + hex.toUpperCase() + ";";
				//System.out.println(text.substring(x, x + 1) + " - " + hex + " > " + Integer.toHexString(c) + " > " + c);
				if (encodingMap.containsKey(hex)) {
					ret += encodingMap.get(hex);
				} else {
					ret += hex;
				}
			}

		} 
		 */

		return ret;
	}

	/**
	 * Returns the server time zone
	 * 
	 * @return
	 */
	public static String getGlobalTimeZone() {
		return StringUtils.isNotBlank(InitServlet.getSystemParam("ServerTimeZone")) ? InitServlet.getSystemParam("ServerTimeZone") : ActionBean.defaulttimezone;
	}

	public static String getUserName(HttpServletRequest request, String userID) {
		GenRow row = getUser(request, userID);
		String userName = row.getString("FirstName") + " " + row.getString("LastName");
		row.close();

		return userName;
	}

	public static String getContactName(HttpServletRequest request, String contactID) {
		GenRow row = getContact(request, contactID);
		String contactName = row.getString("FirstName") + " " + row.getString("LastName");
		row.close();

		return contactName;

	}

	public static GenRow getUser(HttpServletRequest request, String userID) {
		GenRow row = new GenRow();
		if (StringUtils.isNotBlank(userID)) {

			row.setViewSpec("UserView");
			row.setRequest(request);
			row.setColumn("UserID", userID);
			row.doAction("selectfirst");

		}
		return row;
	}

	public static GenRow getContact(HttpServletRequest request, String contactID) {
		GenRow row = new GenRow();
		if (StringUtils.isNotBlank(contactID)) {

			row.setViewSpec("ContactView");
			row.setRequest(request);
			row.setColumn("ContactID", contactID);
			row.doAction("selectfirst");

		}
		return row;
	}

	public static GenRow getProduct(HttpServletRequest request, String productID) {
		GenRow row = new GenRow();
		if (StringUtils.isNotBlank(productID)) {

			row.setViewSpec("ProductView");
			row.setRequest(request);
			row.setColumn("ProductID", productID);
			row.doAction("selectfirst");

		}
		return row;
	}

	public static GenRow getProductQuick(HttpServletRequest request, String productID) {
		GenRow row = new GenRow();
		if (StringUtils.isNotBlank(productID)) {

			row.setViewSpec("properties/ProductQuickIndexView");
			row.setRequest(request);
			row.setColumn("ProductID", productID);
			row.doAction("selectfirst");

		}
		String price = StringUtils.isNotBlank(row.getString("DripResult").replaceAll("[0\\-\\.]", ""))? row.getString("DripResult") : row.getString("TotalCost");
		String dp = RunwayUtil.displayPrice(row.getString("DisplayPriceType"), price);
		if (StringUtils.isNotBlank(dp)) row.setParameter("DisplayPrice", dp);
		return row;
	}

	public static int getUsedBulkLimit(Connection con, String userID, String type) {
		int count = 0;

		if (StringUtils.isNotBlank(userID)) {

			GenRow row = new GenRow();
			row.setTableSpec("Notes");
			row.setConnection(con);
			row.setColumn("-select0", "Count(*) as count");
			row.setColumn("Type", type);
			row.setColumn("CreatedBy", userID);
			row.setColumn("CreatedDate", "TODAY");
			row.setColumn("NoteTypeDescription", "Bulk" + type);
			row.setColumn("-group0", "CreatedBy");

			row.doAction("selectfirst");

			logger.debug("Used Limit Query: " + row.getStatement());

			if (row.isSuccessful()) {
				count = row.getInt("count");
				logger.debug("DB output: Daily Limit used for {} is {}", type, count);
			}

			logger.debug("Daily Limit used is {}", count);
		}
		return count;
	}

	public static int getLeftBulkLimit(Connection con, UserBean user, String type) {
		int left = Integer.MAX_VALUE;

		try {
			int used = getUsedBulkLimit(con, user.getString("UserID"), type);
			int dailyLimit = Integer.parseInt(user.getString("Bulk" + type + "Limit"));

			if (dailyLimit > 0) {
				left = dailyLimit - used;
				if (left < 0)
					left = 0;
			}

			logger.debug("Daily Limit is {}", dailyLimit);
			logger.debug("Daily Limit left is {}", left);

		} catch (Exception e) {
			logger.debug("Exception while parsing {} limit for user {}. Setting left to Integer.MAX_VALUE", type, user.getString("UserID"));
		}
		return left;
	}

	public static int getLeftSMSBulkLimit(Connection con, UserBean user) {
		return getLeftSMSBulkLimit(con, user);
	}

	public static int getLeftSMSBulkLimit(HttpServletRequest request, UserBean user) {
		return getLeftBulkLimit(ActionBean.getConnection(request), user, "SMS");
	}

	public static int getLeftEmailBulkLimit(HttpServletRequest request, UserBean user) {
		return getLeftBulkLimit(ActionBean.getConnection(request), user, "Email");
	}

	public static int getLeftEmailBulkLimit(Connection con, UserBean user) {
		return getLeftBulkLimit(con, user, "Email");
	}

	public static GenRow getProductFeedDetails(HttpServletRequest request, String productID) {
		return getProductFeedDetails(ActionBean.getConnection(request), productID);
	}

	public static GenRow getProductFeedDetails(Connection con, String productID) {
		GenRow productFeedDetails = new GenRow();
		productFeedDetails.setConnection(con);
		productFeedDetails.setViewSpec("properties/ProductFeedAgentDetailsView");
		productFeedDetails.setParameter("ProductID", productID);
		if (StringUtils.isNotBlank(productID)) {
			productFeedDetails.doAction(GenerationKeys.SELECTFIRST);
		}
		return productFeedDetails;
	}

	public static void updateProductFeedAgentDetails(HttpServletRequest request, String productID, String userID) {
		try {
			GenRow productFeedDetails = new GenRow();
			productFeedDetails.setRequest(request);
			productFeedDetails.setViewSpec("properties/ProductFeedAgentDetailsView");
			productFeedDetails.setParameter("ProductID", productID);

			PublishMethod[] pmList = PublishMethod.getMultiAccountMethods();
			for (PublishMethod pm : pmList) {
				productFeedDetails.setParameter(pm.getAgentIDKey(), request.getParameter(pm.getAgentIDKey()));
			}

			PublishMethod[] pmVisibleList = PublishMethod.getVisibleMethods();
			for (PublishMethod pmvisible : pmVisibleList) {
				productFeedDetails.setParameter(pmvisible.getProjectCodeKey(), request.getParameter(pmvisible.getProjectCodeKey()));
			}

			PublishMethod[] legacyList = PublishMethod.getLegacyIDMethods();
			for (PublishMethod legacyvisible : legacyList) {
				productFeedDetails.setParameter(legacyvisible.getListingIDKey(), request.getParameter(legacyvisible.getListingIDKey()));
			}

			productFeedDetails.setParameter("ModifiedBy", userID);
			productFeedDetails.setParameter("ModifiedDate", "NOW");
			productFeedDetails.doAction("update");

			if (!productFeedDetails.isSuccessful()) {
				productFeedDetails.setParameter("CreatedBy", userID);
				productFeedDetails.setParameter("CreatedDate", "NOW");
				productFeedDetails.doAction("insert");
			}
		} catch (Exception e) {
			logger.error("Error updating ProductFeedAgentDetails" + e);
			e.printStackTrace();
		}
	}

	public static String getUserLayoutOther(HttpServletRequest request, String userID, String requestPath) {
		GenRow visibility = new GenRow();
		visibility.setRequest(request);
		visibility.setTableSpec("UserLayout");
		visibility.setParameter("-select0", "DISTINCT OtherName");
		visibility.setParameter("UserID", userID);
		visibility.setParameter("RequestPath", requestPath);
		visibility.setParameter("BoxName", "NULL+EMPTY");
		visibility.setParameter("TabName", "NULL+EMPTY");
		visibility.doAction(GenerationKeys.SELECTFIRST);
		return visibility.getColumn("OtherName");
	}

	public static Date getCurDateInServerTimeZone() {
		Calendar mbCal = new GregorianCalendar(TimeZone.getTimeZone(getGlobalTimeZone()));
		logger.trace("Current server timezone is {} ", RunwayUtil.getGlobalTimeZone());
		mbCal.setTimeInMillis(new Date().getTime());

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, mbCal.get(Calendar.YEAR));
		cal.set(Calendar.MONTH, mbCal.get(Calendar.MONTH));
		cal.set(Calendar.DAY_OF_MONTH, mbCal.get(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.HOUR_OF_DAY, mbCal.get(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, mbCal.get(Calendar.MINUTE));
		cal.set(Calendar.SECOND, mbCal.get(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, mbCal.get(Calendar.MILLISECOND));

		logger.trace("Current server time is {} ", cal.getTime());

		return cal.getTime();
	}

	public static String getCurDateInServerTimeZone(SimpleDateFormat dateFormat) {
		Date curDate = getCurDateInServerTimeZone();
		String now = dateFormat.format(curDate);
		logger.trace("Current server timezone is {} ", RunwayUtil.getGlobalTimeZone());
		return now;
	}

	public static boolean isNewZealandRunway() {
		return "NZ".equals(getGlobalTimeZone());
	}

	public static String getDateStrDDMMYYY(String date) {
		try {
			return ddmmyyyy.format(getDateDDMMYYY(date));
		} catch (Exception e) {
			return "";
		}
	}

	public static Date getDateDDMMYYY(String date) {
		try {
			return yyyymmddHHmmss.parse(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static Date getDateObject(String date, SimpleDateFormat formatString) {
		try {
			return formatString.parse(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static String getDateString(String date, SimpleDateFormat inputFormat, SimpleDateFormat outputFormat) {
		try {
			return outputFormat.format(inputFormat.parse(date));
		} catch (Exception e) {
			return "";
		}
	}

	public static Calendar[] holidays_VIC = {
			/* 2012 dates from: http://www.vic.gov.au/Victorian-Public-Holiday-Dates.html */
			new GregorianCalendar(2012, 0, 26, 0, 0), /* australia day */
			new GregorianCalendar(2012, 2, 12, 0, 0), /* labor day */
			new GregorianCalendar(2012, 3, 6, 0, 0), /* good friday */
			new GregorianCalendar(2012, 3, 9, 0, 0), /* easter monday */
			new GregorianCalendar(2012, 3, 25, 0, 0), /* anzac holiday */
			new GregorianCalendar(2012, 5, 11, 0, 0), /* queens birthday */
			new GregorianCalendar(2012, 10, 6, 0, 0), /* cup day */
			new GregorianCalendar(2012, 11, 26, 0, 0), /* boxing day */
			new GregorianCalendar(2012, 11, 25, 0, 0), /* xmas day holiday */
			new GregorianCalendar(2012, 11, 27, 0, 0), /* annual leave */
			new GregorianCalendar(2012, 11, 28, 0, 0), /* annual leave */
			new GregorianCalendar(2012, 11, 29, 0, 0), /* annual leave */
			new GregorianCalendar(2012, 11, 30, 0, 0), /* annual leave */
			new GregorianCalendar(2012, 11, 31, 0, 0), /* annual leave */
			new GregorianCalendar(2013, 0, 1, 0, 0), /* annual leave */
			new GregorianCalendar(2013, 0, 2, 0, 0), /* annual leave */
			new GregorianCalendar(2013, 0, 3, 0, 0), /* annual leave */
			new GregorianCalendar(2013, 0, 4, 0, 0), /* annual leave */
			new GregorianCalendar(2013, 0, 5, 0, 0), /* annual leave */
			new GregorianCalendar(2013, 0, 6, 0, 0), /* annual leave */

			new GregorianCalendar(2013, 0, 28, 0, 0), /* australia day */
			new GregorianCalendar(2013, 2, 11, 0, 0), /* labor day */
			new GregorianCalendar(2013, 2, 29, 0, 0), /* good friday */
			new GregorianCalendar(2013, 3, 1, 0, 0), /* easter monday */
			new GregorianCalendar(2013, 3, 25, 0, 0), /* anzac day */
			new GregorianCalendar(2013, 5, 10, 0, 0), /* queens bday */
			new GregorianCalendar(2013, 10, 5, 0, 0), /* melb cup */

			new GregorianCalendar(2013, 11, 25, 0, 0), /* xmas day holiday */
			new GregorianCalendar(2013, 11, 26, 0, 0), /* boxing day */

			new GregorianCalendar(2013, 11, 27, 0, 0), /* annual leave */
			new GregorianCalendar(2013, 11, 28, 0, 0), /* annual leave */
			new GregorianCalendar(2013, 11, 29, 0, 0), /* annual leave */
			new GregorianCalendar(2013, 11, 30, 0, 0), /* annual leave */
			new GregorianCalendar(2013, 11, 31, 0, 0), /* annual leave */

			new GregorianCalendar(2014, 0, 1, 0, 0), /* new years day */
			new GregorianCalendar(2014, 0, 27, 0, 0), /* australia day */
			new GregorianCalendar(2014, 2, 10, 0, 0), /* labour day */
			new GregorianCalendar(2014, 3, 18, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
			new GregorianCalendar(2014, 3, 21, 0, 0), /* easter monday */
			new GregorianCalendar(2014, 3, 25, 0, 0), /* anzac day */
			new GregorianCalendar(2014, 5, 9, 0, 0), /* queens bday */
			new GregorianCalendar(2014, 10, 4, 0, 0), /* cup day */
			new GregorianCalendar(2014, 11, 25, 0, 0), /* xmas day */
			new GregorianCalendar(2014, 11, 26, 0, 0), /* boxing day */

			new GregorianCalendar(2014, 11, 27, 0, 0), /* annual leave */
			new GregorianCalendar(2014, 11, 28, 0, 0), /* annual leave */
			new GregorianCalendar(2014, 11, 29, 0, 0), /* annual leave */
			new GregorianCalendar(2014, 11, 30, 0, 0), /* annual leave */
			new GregorianCalendar(2014, 11, 31, 0, 0), /* annual leave */

			new GregorianCalendar(2015, 0, 1, 0, 0), /* new years day */
			new GregorianCalendar(2015, 0, 26, 0, 0), /* australia day */
			new GregorianCalendar(2015, 2, 9, 0, 0), /* labour day */
			new GregorianCalendar(2015, 3, 3, 0, 0), /* good friday (easter sat skipped as we don't count weekends anyway) */
			new GregorianCalendar(2015, 3, 6, 0, 0), /* easter monday */
			new GregorianCalendar(2015, 5, 8, 0, 0), /* queens bday */
			new GregorianCalendar(2015, 10, 3, 0, 0), /* cup day */
			new GregorianCalendar(2015, 11, 25, 0, 0), /* xmas day */
			new GregorianCalendar(2015, 11, 28, 0, 0), /* boxing day (monday additional day) */

			new GregorianCalendar(2015, 11, 29, 0, 0), /* annual leave */
			new GregorianCalendar(2015, 11, 30, 0, 0), /* annual leave */
			new GregorianCalendar(2015, 11, 31, 0, 0) /* annual leave */
	};

	public static int countOfHolidays(Calendar startDateInput, Calendar endDate, Calendar[] holidayList) {
		int noOfHolidays = 0;
		Calendar startDate = Calendar.getInstance();
		startDate.setTimeInMillis(startDateInput.getTimeInMillis());

		System.out.println(startDate.get(Calendar.DATE));
		System.out.println(endDate.get(Calendar.DATE));
		for (Calendar hol : holidayList) {
			if ((startDate.before(hol) && endDate.after(hol)) || startDate.compareTo(hol) == 0 || endDate.compareTo(hol) == 0) {
				System.out.println("\n");
				System.out.println(hol.get(Calendar.DATE));

				System.out.println("Because");
				System.out.println((startDate.before(hol) && endDate.after(hol)));
				System.out.println(startDate.compareTo(hol) == 0);
				System.out.println(endDate.compareTo(hol) == 0);
				if (hol.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && hol.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
					noOfHolidays++;
				}
			}
		}
		return noOfHolidays;
	}

	public static int getWeekDaysBetweenTwoDates(Calendar startCalInput, Calendar endCal) {
		int workDays = 0;
		Calendar startCal = Calendar.getInstance();
		startCal.setTimeInMillis(startCalInput.getTimeInMillis());

		// Return 0 if start and end are the same
		if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
			return 0;
		}

		if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
			Calendar tmpCal = startCal;
			endCal = startCal;
			startCal = tmpCal;
		}

		do {
			startCal.add(Calendar.DAY_OF_MONTH, 1);
			if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				++workDays;
			}
		} while (startCal.getTimeInMillis() < endCal.getTimeInMillis());

		return workDays;
	}

	public static int getDaysBetweenTwoDates(Calendar startCal, Calendar endCal) {
		int workDays = 0;
		boolean isNegative = false;
		// Return 0 if start and end are the same
		if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
			return 0;
		}

		if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
			Calendar tmpCal = endCal;
			endCal = startCal;
			startCal = tmpCal;
			isNegative = true;
		} else {
			isNegative = false;
		}

		do {
			startCal.add(Calendar.DAY_OF_MONTH, 1);
			++workDays;
		} while (startCal.getTimeInMillis() < endCal.getTimeInMillis());

		if(isNegative) {
			workDays = workDays * -1;
		}
		return workDays;
	}

	public static int getBizDays(Calendar startCal, Calendar endCal, String state) {
		Calendar[] holList = HolidayUtil.holidays_VIC;
		if ("NSW".equalsIgnoreCase(state)) {
			holList = HolidayUtil.holidays_NSW;
		} else if ("QLD".equalsIgnoreCase(state)) {
			holList = HolidayUtil.holidays_QLD;
		} else if ("WA".equalsIgnoreCase(state)) {
			holList = HolidayUtil.holidays_WA;
		} else if ("SA".equalsIgnoreCase(state)) {
			holList = HolidayUtil.holidays_SA;
		}

		return getWeekDaysBetweenTwoDates(startCal, endCal) - countOfHolidays(startCal, endCal, holList);
	}

	public static File doFileOutput(HttpServletRequest request, String urlPath, String fileKey, String folderName) throws Exception {
		com.sok.framework.servlet.ResponseOutputStream responseOS = null;
		FileOutputStream fos = null;
		try {

			java.io.File dir = new java.io.File(InitServlet.getRealPath("/" + folderName + "/"));
			dir.mkdirs();

			java.io.File file = new java.io.File(InitServlet.getRealPath("/" + folderName + "/" + fileKey));

			logger.trace("File {} {}", file.getAbsoluteFile(), file.exists());
			fos = new FileOutputStream(file);
			responseOS = new com.sok.framework.servlet.ResponseOutputStream(request.getSession(), fos, urlPath);
			responseOS.service(ActionBean.getConnection(request));
			request.getSession().setAttribute(fileKey, file);

			return file;
		} finally {
			if (fos != null) {
				try {
					fos.flush();
					fos.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			if (responseOS != null) {
				responseOS.close();
			}
		}
	}

	public static void doFileOutput(HttpServletRequest request, String urlPath) throws Exception {
		final String fileKey = KeyMaker.generate();
		com.sok.framework.servlet.ResponseOutputStream responseOS = null;
		java.io.FileOutputStream fos = null;
		try {
			String fileExtension = ".pdf";
			if (urlPath.contains("Excel")) {
				fileExtension = ".xls";
			}

			java.io.File file = java.io.File.createTempFile(fileKey, fileExtension);

			System.out.println("File " + file.getAbsoluteFile() + " " + file.exists());
			fos = new java.io.FileOutputStream(file);
			responseOS = new com.sok.framework.servlet.ResponseOutputStream(request.getSession(), fos, urlPath);
			GenRow g = new GenRow();
			ActionBean.connect(g);
			responseOS.service(ActionBean.getConnection(request));
			g.close();
			request.getSession().setAttribute(fileKey, file);
		} finally {
			if (fos != null) {
				try {
					fos.flush();
					fos.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			if (responseOS != null) {
				responseOS.close();
			}
		}
	}

	public static boolean isAllowIP(String remoteIP) {
		if (remoteIP == null) return false;

		if (remoteIP.startsWith("10.144.1.") || remoteIP.equals("127.0.0.1") || remoteIP.equals("0:0:0:0:0:0:0:1")) return true;		

		String allowIPs = (String) InitServlet.getSystemParams().get("AllowIPs");

		if (allowIPs != null && allowIPs.length() > 0) {
			String[] allow = allowIPs.split(",");
			for (String ip : allow) {
				if (remoteIP.startsWith(ip.trim())) return true;
			}
		}
		return false;
	}


	public static double getRegionalPrice(String productID, String regionID) {

		double totalCost = 0;
		double currentPrice = 0;
		if (StringUtils.isNotBlank(productID)) {
			try {
				GenRow curPrice = new GenRow();
				curPrice.setViewSpec("properties/ProductQuickIndexView");
				curPrice.setParameter("ProductID",productID);
				curPrice.doAction("selectfirst");
				currentPrice = curPrice.getDouble("TotalCost"); 

				if (StringUtils.isNotBlank(regionID)) {
					GenRow prices = new GenRow();
					// prices.setViewSpec("ProductRegionPriceView");
					prices.setTableSpec("ProductRegionPrice");
					prices.setParameter("-select0", "*");
					prices.setParameter("ProductID",productID);
					prices.setParameter("RegionID",regionID);
					prices.doAction("selectfirst");
					logger.debug("ProductRegionalPrice Query is: " + prices.getStatement());

					// Kavi : Added the if clause to set price to 0.0 for products inactive in region
					if(StringUtils.isNotBlank(prices.getString("Active")) && prices.getString("Active").equalsIgnoreCase("N"))
						totalCost = 0.0;
					else if (prices.getDouble("Percentage") != 0) {
						totalCost = currentPrice + (currentPrice * prices.getDouble("Percentage"));
					} else if (prices.getDouble("Margin") != 0) {
						totalCost = currentPrice + prices.getDouble("Margin");
					} else if (prices.getDouble("TotalCost") != 0) {
						totalCost = prices.getDouble("TotalCost");
					} else {
						totalCost = currentPrice;
					}
				} else {
					totalCost = currentPrice;
				}

			} catch(Exception ee) {
				logger.error("Exception occurred in getRegionalPrice(String productID, String regionID). Message is: " + ee.getMessage());
				ee.printStackTrace();
			}
		}

		logger.debug("getRegionalPrice. Regional TotalCost: " + totalCost + "     ,    Actual CurrentPrice: " + currentPrice);

		return totalCost;
	}

	public static ArrayList<String> getRegionIDList(String regionID) {
		ArrayList<String> listRegion = new ArrayList<String>();
		String prodAndRegID = "";
		if (StringUtils.isNotBlank(regionID)) {
			try {
				StringTokenizer regionTokens = new StringTokenizer(regionID, "+");
				while (regionTokens.hasMoreElements()) {
					try {
						prodAndRegID = regionTokens.nextElement().toString();
						String [] strArr = prodAndRegID.split(":");
						String regID = strArr[1];
						listRegion.add(regID);
					} catch(Exception ee){
						logger.debug("Problem Occurred while splitting RegionID and ProductID.  prodAndRegID is: " + prodAndRegID);
					}
				}
			} catch(Exception ee){
				logger.debug("Problem Occurred while making tokens for regionID: " + regionID);
			}
		}
		return listRegion;
	}




	public static void main(String[] args) {
		int noOfHolidays = countOfHolidays(new GregorianCalendar(), new GregorianCalendar(2014, 03, 25, 0, 0), HolidayUtil.holidays_VIC);
		int getWorkingDaysBetweenTwoDates = getWeekDaysBetweenTwoDates(new GregorianCalendar(), new GregorianCalendar(2014, 03, 07, 0, 0));
		System.out.println("count : " + noOfHolidays);
		System.out.println("wd: " + getWorkingDaysBetweenTwoDates);
	}

	public static List<LetterOption> getLetterVariants() {
		List letterOptionList = Collections.EMPTY_LIST;
		try {
			Class pdfClass = null;
			String METHOD_NAME = "getPDFVariants";
			String GENERIC_PDF = PropertyPDFGenerator.GENERIC_JASPER_PACKAGE + "LettersPdf";
			String SPECIFIC_PDF = PropertyPDFGenerator.JASPER_PACKAGE + "LettersPdf";

			try {
				pdfClass = Class.forName(SPECIFIC_PDF);
			} catch (Exception e) {
				try {
					pdfClass = Class.forName(GENERIC_PDF);
				} catch (Exception ex) {
					// Do nothing
				}
			}

			if (pdfClass != null) {
				//Class[] arguments = { Boolean.class } ;
				//Object[] parameters = { new Boolean(includeFinancial) };
				//Method method = pdfClass.getDeclaredMethod(METHOD_NAME, arguments);
				//List letterOption = (List) method.invoke(null, parameters);
				Method method = pdfClass.getDeclaredMethod(METHOD_NAME);
				letterOptionList = (List) method.invoke(null);
				logger.debug("returned value: " , letterOptionList);

			}

		} catch (Exception e) {
			// Do nothing
			logger.debug("Exception while accessing LettersPdf.java", e);
		}
		return letterOptionList;
	}

	public static String displayPrice(String displayPriceType, String totalCostStr) {
		double totalCost = 0;
		try {
			totalCost = Double.parseDouble(totalCostStr);
		} catch (Exception e) {}
		return displayPrice(displayPriceType, totalCost);
	}

	public static String displayPrice(String displayPriceType, double totalCost) {
		totalCost = StringUtil.getRoundedValue(totalCost);
		NumberFormat currancy = NumberFormat.getCurrencyInstance();
		currancy.setMaximumFractionDigits(0);
		if (StringUtils.isNotBlank(displayPriceType)) {
			if ("Free Form".equals(displayPriceType)) {
				return null;
			} else if ("POA".equals(displayPriceType) || totalCost <= 0) {
				return "POA";
			} else if (displayPriceType.startsWith("From")) {
				double price = totalCost;
				if (displayPriceType.endsWith("xxx,x00.00")) {
					price = Math.floor(totalCost / 100) * 100;
				} else if (displayPriceType.endsWith("xxx,000.00")) {
					price = Math.floor(totalCost / 1000) * 1000;
				} else if (displayPriceType.endsWith("xx0,000.00")) {
					price = Math.floor(totalCost / 10000) * 10000;
				} else if (displayPriceType.endsWith("x00,000.00")) {
					price = Math.floor(totalCost / 100000) * 100000;
				} 

				return ("From " + currancy.format(price)).replaceAll("\\.00", "");
			} else if (displayPriceType.startsWith("Round")) {
				double diff = 0, mod = 0;
				if (displayPriceType.endsWith("up/down 50,000")) {
					diff = 50000;
					mod = 50000;
				} else if (displayPriceType.endsWith("up/down 100,000")) {
					diff = 100000;
					mod = 100000;
				} else if (displayPriceType.endsWith("up 100,000 down 50,000")) {
					diff = 100000;
					mod = 50000;
				} else if (displayPriceType.endsWith("up 150,000 down 50,000")) {
					diff = 150000;
					mod = 50000;
				} else if (displayPriceType.endsWith("up 150,000 down 100,000")) {
					diff = 150000;
					mod = 100000;
				} else if (displayPriceType.endsWith("up 200,000 down 100,000")) {
					diff = 200000;
					mod = 100000;
				}
				double downPrice = totalCost - (totalCost % mod);
				double upPrice = downPrice + diff;

				return (currancy.format(downPrice) + " to " + currancy.format(upPrice)).replaceAll("\\.00", ""); 
			}
		}			
		return (currancy.format(totalCost)).replaceAll("\\.00", "");
	}

	public static String displayPrice(String displayPriceType, double minCost, double maxCost) {
		if (minCost == maxCost) return displayPrice(displayPriceType, minCost);

		NumberFormat currancy = NumberFormat.getCurrencyInstance();
		currancy.setMaximumFractionDigits(0);

		if (StringUtils.isNotBlank(displayPriceType)) {
			if ("Free Form".equals(displayPriceType)) {
				return null;
			} else if ("POA".equals(displayPriceType)) {
				return "POA";
			} else if (displayPriceType.startsWith("From")) {
				double priceMin = minCost;
				double priceMax = maxCost;
				if (displayPriceType.endsWith("xxx,x00.00")) {
					priceMin = Math.floor(minCost / 100) * 100;
					priceMax = Math.ceil(maxCost / 100) * 100;
				} else if (displayPriceType.endsWith("xxx,000.00")) {
					priceMin = Math.floor(minCost / 1000) * 1000;
					priceMax = Math.ceil(maxCost / 1000) * 1000;
				} else if (displayPriceType.endsWith("xx0,000.00")) {
					priceMin = Math.floor(minCost / 10000) * 10000;
					priceMax = Math.ceil(maxCost / 10000) * 10000;
				} else if (displayPriceType.endsWith("x00,000.00")) {
					priceMin = Math.floor(minCost / 100000) * 100000;
					priceMax = Math.ceil(maxCost / 100000) * 100000;
				} 

				return ("From " + currancy.format(priceMin) + " to " +  currancy.format(priceMax)).replaceAll("\\.00", "");
			} else if (displayPriceType.startsWith("Round")) {
				double diff = 0, mod = 0;
				if (displayPriceType.endsWith("up/down 50,000")) {
					diff = 50000;
					mod = 50000;
				} else if (displayPriceType.endsWith("up/down 100,000")) {
					diff = 100000;
					mod = 100000;
				} else if (displayPriceType.endsWith("up 100,000 down 50,000")) {
					diff = 100000;
					mod = 50000;
				} else if (displayPriceType.endsWith("up 150,000 down 50,000")) {
					diff = 150000;
					mod = 50000;
				} else if (displayPriceType.endsWith("up 150,000 down 100,000")) {
					diff = 150000;
					mod = 100000;
				} else if (displayPriceType.endsWith("up 200,000 down 100,000")) {
					diff = 200000;
					mod = 100000;
				}
				double downPrice = minCost - (minCost % mod);
				double tempPrice = maxCost - (maxCost % mod);
				double upPrice = tempPrice + diff;

				return (currancy.format(downPrice) + " to " + currancy.format(upPrice)).replaceAll("\\.00", ""); 
			}
		}			
		return (currancy.format(minCost) + " - " +  currancy.format(maxCost)).replaceAll("\\.00", "");
	}


	public static String getLotNameLabel() {

		try {
			if (StringUtils.isNotBlank(LotNameLabel)) {
				return LotNameLabel;
			} else {
				DisplaySpec dspec = SpecManager.getDisplaySpec("ProductLotDisplay");
				if (StringUtils.isNotBlank(dspec.getItemLabel("Name"))) {
					LotNameLabel = dspec.getItemLabel("Name");
				}
			}	
		} catch(Exception ee) {}

		if (StringUtils.isBlank(LotNameLabel)) {
			LotNameLabel = "Lot";
		}

		return LotNameLabel;
	}

	public static void setLotNameLabel(String tmpVal) {
		LotNameLabel = tmpVal;
	}

	public static String getUserLandView(UserBean user) {
		String defaultView = user.getData("DefaultLandView");
		if(StringUtils.isBlank(defaultView)) {
			if("true".equalsIgnoreCase(InitServlet.getSystemParam("RPM-HideLandTreeView")))
				defaultView = "Overview";
		}

		// land view value is still black then set to Overview
		if(StringUtils.isBlank(defaultView)) {
			defaultView = "Tree View";
		}

		return defaultView;
	}

	public static void convertRow2Context(GenRow row, TableData specificcontext, String prefix) {

		if (row == null || specificcontext == null) return;

		if (StringUtils.isBlank(prefix)) 
			prefix = "";

		ViewSpec vs = row.getViewSpec();

		if (vs != null) {
			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put(prefix + vs.getLocalItemName(i), row.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put(prefix + vs.getRelatedItemName(i), row.getString(vs.getRelatedItemName(i)));
			}
		}

	}

	public static void convertRow2Map(GenRow row, Map<String,String> map, String prefix) {
		if (row == null || map == null) return;

		if (StringUtils.isBlank(prefix)) 
			prefix = "";

		ViewSpec vs = row.getViewSpec();

		if (vs != null) {
			for (int i = 0; i < vs.getLocalLength(); ++i) {
				map.put(prefix + vs.getLocalItemName(i), row.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				map.put(prefix + vs.getRelatedItemName(i), row.getString(vs.getRelatedItemName(i)));
			}
		}
	}

	public static void generateTokenList(TableData specificcontext) {
		StringBuilder sb = new StringBuilder();
		for (Object o : specificcontext.entrySet()) {
			@SuppressWarnings("rawtypes")
			Map.Entry e = (Map.Entry) o;
			if (e.getKey() != null && !"${Toekns}".equals(e.getKey())) {
				sb.append("{").append(e.getKey()).append("} = ").append(e.getValue()).append("<br>\n");
			}
		}      

		specificcontext.put("Tokens", sb.toString());       
	}

	public static String decodeTokens(TableData emailContext, String value) {
		try
		{
			VelocityEngine ve = VelocityManager.getEngine();

			StringWriter text = new StringWriter();
			ve.evaluate(emailContext, text, "Email", value);
			text.flush();
			text.close();
			return(text.toString());
		}
		catch(Exception e)
		{
			return value;
		}
	}

	public static String encodeKey(String primaryKey, String key, String value) {
		if (StringUtils.isBlank(value)) return "";
		if (StringUtils.isBlank(primaryKey)) return value;

		if (StringUtils.isBlank(key)) key = "";

		try {
			StringBuilder dateStr = new StringBuilder();

			for (int x = 0; x < primaryKey.length(); x += 2) {
				dateStr.append(primaryKey.charAt(x));
			}

			String pKey = new String(Hex.encodeHex((primaryKey + key).getBytes()));
			String salt = new String(Hex.encodeHex((dateStr + key).getBytes()));

			String result = new String(Hex.encodeHex(encrypt(value, pKey + salt)));

			return new String("*" + result);
			//		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			//			e.printStackTrace();
			//		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			//			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return value;
	}

	public static String decodeKey(String primaryKey, String key, String value) {
		if (StringUtils.isBlank(value)) return "";
		if (StringUtils.isBlank(primaryKey) || !value.startsWith("*")) return value;

		value = value.substring(1);

		if (StringUtils.isBlank(key)) key = "";

		try {
			StringBuilder dateStr = new StringBuilder();

			for (int x = 0; x < primaryKey.length(); x += 2) {
				dateStr.append(primaryKey.charAt(x));
			}

			String pKey = new String(Hex.encodeHex((primaryKey + key).getBytes()));
			String salt = new String(Hex.encodeHex((dateStr + key).getBytes()));

			return decrypt(Hex.decodeHex(value.toCharArray()), pKey + salt);
			//		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			//			e.printStackTrace();
			//		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			//			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return value;
	}

	public static byte[] encrypt(String key, String salt, String toEncrypt) throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		SecretKey skeySpec = generateKeySpec(key, salt);
		final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
		Cipher cipher = Cipher.getInstance("ADESede/CBC/PKCS5Padding", new BouncyCastleProvider());
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
		byte[] encrypted = cipher.doFinal(toEncrypt.getBytes());
		byte[] encryptedValue = Base64.encodeBase64(encrypted);
		return encryptedValue;
	}

	public static String decrypt(String key, String salt, String encrypted) throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		SecretKey skeySpec = generateKeySpec(key, salt);
		final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
		Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding", new BouncyCastleProvider());
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
		byte[] decodedBytes = Base64.decodeBase64(encrypted.getBytes());
		byte[] original = cipher.doFinal(decodedBytes);
		return new String(original);
	}

	private static SecretKey generateKeySpec(String key, String salt) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
		SecretKeyFactory kf = SecretKeyFactory.getInstance("DESede","BC");
		PBEKeySpec ks = new PBEKeySpec(key.toCharArray(), salt.getBytes(), 65536, 128);
		SecretKey tmp = kf.generateSecret(ks);
		return tmp;
		//return new SecretKeySpec(tmp.getEncoded(), "AES");

		/*
        SecretKeyFactory factorybc = SecretKeyFactory.getInstance("PBEWITHHMACSHA1", "BC");
	    KeySpec keyspecbc = new PBEKeySpec(key.toCharArray(), salt.getBytes(), 500, 256);

	    return factorybc.generateSecret(keyspecbc);
		 */
	}
	public static byte[] encrypt(String message, String keyWords)
	{
		try
		{
			final MessageDigest md = MessageDigest.getInstance("SHA-256");
			final byte[] digestOfPassword = md.digest(keyWords.getBytes("utf-8"));
			final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
			/*
	        for (int j = 0,  k = 16; j < 8;)
	        {
	            keyBytes[k++] = keyBytes[j++];
	        }
			 */
			final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
			final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
			final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key, iv);

			final byte[] plainTextBytes = message.getBytes("utf-8");
			final byte[] cipherText = cipher.doFinal(plainTextBytes);
			//final String encodedCipherText = new sun.misc.BASE64Encoder().encode(cipherText);

			return cipherText;    
		}
		catch (java.security.InvalidAlgorithmParameterException e) { System.out.println("Invalid Algorithm"); }
		catch (javax.crypto.NoSuchPaddingException e) { System.out.println("No Such Padding"); }
		catch (java.security.NoSuchAlgorithmException e) { System.out.println("No Such Algorithm"); }
		catch (java.security.InvalidKeyException e) { System.out.println("Invalid Key"); }
		catch (BadPaddingException e) { System.out.println("Invalid Key");}
		catch (IllegalBlockSizeException e) { System.out.println("Invalid Key");}
		catch (UnsupportedEncodingException e) { System.out.println("Invalid Key");}

		return null;
	}

	public static String decrypt(byte[] message, String keyWords)
	{
		try
		{
			final MessageDigest md = MessageDigest.getInstance("SHA-256");
			final byte[] digestOfPassword = md.digest(keyWords.getBytes("utf-8"));
			final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
			/*
	        for (int j = 0,  k = 16; j < 8;)
	        {
	            keyBytes[k++] = keyBytes[j++];
	        }
			 */
			final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
			final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
			final Cipher decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
			decipher.init(Cipher.DECRYPT_MODE, key, iv);

			//final byte[] encData = new sun.misc.BASE64Decoder().decodeBuffer(message);
			final byte[] plainText = decipher.doFinal(message);

			return new String(plainText);            
		}
		catch (java.security.InvalidAlgorithmParameterException e) { System.out.println("Invalid Algorithm"); }
		catch (javax.crypto.NoSuchPaddingException e) { System.out.println("No Such Padding"); }
		catch (java.security.NoSuchAlgorithmException e) { System.out.println("No Such Algorithm"); }
		catch (java.security.InvalidKeyException e) { System.out.println("Invalid Key"); }
		catch (BadPaddingException e) { System.out.println("Invalid Key");}
		catch (IllegalBlockSizeException e) { System.out.println("Invalid Key");}
		catch (UnsupportedEncodingException e) { System.out.println("Invalid Key");} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static boolean isAdvancedModePDF(HttpServletRequest request) {
		boolean isAdvanced = "true".equals(InitServlet.getSystemParam("PDF-AdvancedServer"));
		String reqPDFType = request.getParameter("PDF");
		if(StringUtils.isNotBlank(reqPDFType) && !"null".equals(reqPDFType)) {
			isAdvanced = "New".equalsIgnoreCase(reqPDFType);
		}
		return isAdvanced;
	}

	public static GenRow getProductRow(HttpServletRequest request, String productID) {
		GenRow product = new GenRow();
		if (StringUtils.isNotBlank(productID)) {
			product.setRequest(request);
			product.setColumn("ProductID", productID);
			product.setViewSpec("ProductView");
			product.doAction("selectfirst");

			if ("House and Land".equals(product.getString("ProductType"))) {
				product.clear();
				product.setViewSpec("properties/ProductHouseAndLandView");
				product.setColumn("ProductID", productID);
				product.doAction("selectfirst");
			} else if ("Land".equals(product.getString("ProductType"))) {
				product.clear();
				product.setViewSpec("properties/ProductLotView");
				product.setColumn("ProductID", productID);
				product.doAction("selectfirst");
			} else if ("Apartment".equals(product.getString("ProductType"))) {
				product.clear();
				product.setViewSpec("properties/ProductApartmentView");
				product.setColumn("ProductID", productID);
				product.doAction("selectfirst");
			} else if ("Home Plan".equals(product.getString("ProductType"))) {
				product.clear();
				product.setViewSpec("properties/ProductPlanView");
				product.setColumn("ProductID", productID);
				product.doAction("selectfirst");
			} else if ("Existing Property".equals(product.getString("ProductType"))) {
				product.clear();
				product.setViewSpec("properties/ProductExistingPropertyView");
				product.setColumn("ProductID", productID);
				product.doAction("selectfirst");
			}
		}
		String price = StringUtils.isNotBlank(product.getString("DripResult").replaceAll("[0\\-\\.]", ""))? product.getString("DripResult") : product.getString("TotalCost");
		String dp = RunwayUtil.displayPrice(product.getString("DisplayPriceType"), price);
		if (StringUtils.isNotBlank(dp)) product.setParameter("DisplayPrice", dp);
		
		return product;
	}

	public static String getNewReciept(HttpServletRequest request) {
		String key = "RPM-ReceiptNumber";
		String format = InitServlet.getSystemParam("RPM-ReceiptFormat");

		GenRow row = new GenRow();
		row.setTableSpec("SystemParameters");
		row.setRequest(request);
		row.setParameter("-select0", "*");
		row.setParameter("Name", key);
		row.doAction("selectfirst");

		String num = StringUtils.defaultIfBlank(row.getString("Value"), "1");

		int number = 1;
		try {
			number = Integer.parseInt(num) + 1;
		} catch (Exception e) {}

		num = "" + number;

		String results = num;

		if (format != null && format.length() > num.length()) {
			results = format.substring(0, (format.length() - num.length())) + num;
		}

		// Save it back
		row.setParameter("Value", num);

		if (row.isSuccessful() || row.isSet("SystemParameterID")) {		
			row.doAction("update");
		} else {
			row.createNewID();
			row.setParameter("Name",key);
			row.setParameter("Type", "Text");
			row.doAction("insert");
		}

		return results;
	}

	public static String updateImage(String imagePath) {
		if (StringUtils.isNotBlank(imagePath) && !imagePath.startsWith("/")) {
			imagePath = "/" + imagePath;
		}
		return imagePath;
	}
}
