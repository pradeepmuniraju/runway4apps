package com.sok.framework;
import java.io.Reader;


import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

public class VelocityTag extends BodyTagSupport
{
    private Tag          parent = null;
    private BodyContent  bodyContent = null;
    private PageContext  pageContext = null;
	 private String scope = null;
	 private String contextname = null;

    public void setVelocityContext(String contextname)
    {
        this.contextname = contextname;
    }

    public void setScope(String scope)
    {
        this.scope = scope;
    }

	 org.apache.velocity.context.Context getContext()
	 {
			if(contextname!=null && scope!=null)
			{
				return((org.apache.velocity.context.Context)getAttribute(contextname,scope));
			}
			return(null);
	 }

  protected Object getAttribute(String name, String scope)
	{
		Object obj = null;	
    if (scope.equals("request")) {
      obj = pageContext.getAttribute(name, PageContext.REQUEST_SCOPE);
    } else if (scope.equals("application")) {
      obj = pageContext.getAttribute(name, PageContext.APPLICATION_SCOPE);
    } else if (scope.equals("session")) {
      obj = pageContext.getAttribute(name, PageContext.SESSION_SCOPE);
    } else {
      obj = pageContext.getAttribute(name);
    }
		return(obj);
  }

    public Tag getParent()
    {
        return parent;
    }

    public void setParent( Tag parent)
    {
        this.parent = parent;
        return;
    }

    public int doStartTag()
        throws JspException
    {
        return EVAL_BODY_BUFFERED;
    }

    public void setBodyContent( BodyContent bc )
    {
        this.bodyContent = bc;
        return;
    }

    public void setPageContext( PageContext pc )
    {
        this.pageContext = pc;
        return;
    }

    public void doInitBody()
        throws JspException
    {
        return;
    }
   
    public int doAfterBody()
        throws JspException
    {
        return 0;
    }

    public void release()            
    {
        return;
    } 

    public int doEndTag()
        throws JspException
    {

        if ( bodyContent == null)
            return EVAL_PAGE;

        try
        {	
	 			org.apache.velocity.context.Context vc = getContext();
			
				if(vc!=null)
				{
	            JspWriter writer = pageContext.getOut();
	
	            Reader bodyreader = bodyContent.getReader();
	
	            VelocityManager.getEngine().evaluate( vc , writer, "runway", bodyreader );
				}
        }
        catch( Exception e )
        {
            System.out.println( e.toString() );
        }

        return EVAL_PAGE;
    }
}
