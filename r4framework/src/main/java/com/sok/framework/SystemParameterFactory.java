package com.sok.framework;

import javax.servlet.ServletContext;
import java.util.Enumeration;
import java.util.Properties;

public class SystemParameterFactory {

    public static Properties buildSystemParameters(ServletContext config) {
        Properties props = getInitProperties(config);

        RowSetBean bean = new RowSetBean();
        bean.setViewSpec("SystemParameterView");
        bean.generateSQLStatment();
        bean.getResults();
        while (bean.getNext()) {
            if (bean.getString("Value").length() > 0) {
                props.put(bean.getString("Name"), bean.getString("Value"));
            }
        }
        bean.close();

        return props;
    }

    public static Properties getInitProperties(ServletContext config) {
        Properties props = new Properties();
        Enumeration list = config.getInitParameterNames();
        String name = "";
        while (list.hasMoreElements()) {
            name = (String) list.nextElement();
            props.put(name, config.getInitParameter(name));
        }
        return props;
    }
}
