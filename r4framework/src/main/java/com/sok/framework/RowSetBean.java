package com.sok.framework;

import java.sql.*;

public class RowSetBean extends RowBean implements TableDataSet
{
	ResultSet result = null;
	ResultSetMetaData meta = null;

	Statement stmt = null;

	boolean scrollSensitive = false;
	int rowindex = 0;
	int rowcount = 0;
	
	public void setStatement(Statement stmt)
	{
		this.stmt = stmt;
	}

	public void setResultSet(ResultSet current)
	{
		result = current;
	}

	public int getRowIndex()
	{
		return(rowindex);
	}

	public int getSize()
	{
		return(rowcount);
	}

	public int getColumnCount()
	{
		try{
			return(meta.getColumnCount());
		}catch(Exception e){}
		return(0);
	}

	public String getColumnName(int i)
	{
		try{
		   //TODO return(meta.getColumnLabel(i));
			return(meta.getColumnLabel(i));
		}catch(Exception e){}
		return(null);
	}
        
	public int getColumnType(int i)
	{
		try{
			return(meta.getColumnType(i));
		}catch(Exception e){}
		return(-1);
	}       
        
	public String getColumnTypeName(int i)
	{
		try{
			return(meta.getColumnTypeName(i));
		}catch(Exception e){}
		return(null);
	}         

	public boolean getResults(boolean scrollSensitive)
	{
		this.scrollSensitive = scrollSensitive;
		rowindex = 0;
		if(searchstmt!=null)
		{
			try
			{
				ActionBean.retrieveResultSet(this, scrollSensitive);
				meta = result.getMetaData();
				if(scrollSensitive)
				{
	  				result.last();
					rowcount = result.getRow();
					result.beforeFirst();
				}
				return(true);
			}
			catch(Exception e)
			{
				errors.append(e.toString());
				errors.append(ActionBean.RN);
			}
		}
		return(false);
	}

	public boolean getResults()
	{
		return getResults(false);
	}

	public boolean getNext()
	{
		if(result!=null)
		{
			clear();
			try
			{
				if(ActionBean.initFromDB(result,this))
				{
					rowindex++;
					return(true);
				}
			}
			catch(Exception e)
			{
				errors.append(e.toString());
				errors.append(ActionBean.RN);
			}
		}
		return(false);
	}

	public void close()
	{
		if(stmt!=null)
		{
			try{
				stmt.close();
			}catch(Exception e){}
		}
		if(result!=null)
		{
			try{
				result.close();
			}catch(Exception e){}
		}
		meta = null;
		super.close();
	}

}
