package com.sok.framework;
import java.util.*;
public class CalendarSelect
{


	public static String renderMonth()
	{
		GregorianCalendar week = new GregorianCalendar();
		StringBuffer sb = new StringBuffer();
		week.set(Calendar.DAY_OF_MONTH,1);
		int rowcount = 0;
		for(int i=0; i<week.getActualMaximum(Calendar.DAY_OF_MONTH); i++)
		{
			int day = week.get(Calendar.DAY_OF_WEEK);
			if(i==0)
			{
				sb.append("<tr>");
				rowcount++;
				if(day==Calendar.MONDAY)
				{
					sb.append("<td></td>");
				}
				if(day==Calendar.TUESDAY)
				{
					sb.append("<td></td><td></td>");
				}
				else if(day==Calendar.WEDNESDAY)
				{
					sb.append("<td></td><td></td><td></td>");
				}
				else if(day==Calendar.THURSDAY)
				{
					sb.append("<td></td><td></td><td></td><td></td>");
				}
				else if(day==Calendar.FRIDAY)
				{
					sb.append("<td></td><td></td><td></td><td></td><td></td>");
				}
				else if(day==Calendar.SATURDAY)
				{
					sb.append("<td></td><td></td><td></td><td></td><td></td><td></td>");
				}										
			}
			
			sb.append("<td class=\"");
			if(day==Calendar.SUNDAY||day==Calendar.SATURDAY)
			{
				sb.append("weekendcell");
			}
			else
			{
				sb.append("daycell");
			}
			sb.append("\">");

				sb.append("<a href=\"javascript:setSelectedDate('");
				sb.append(String.valueOf(i+1));
				sb.append("','");
				sb.append(String.valueOf(week.get(Calendar.MONTH)+1));
				sb.append("','");
				sb.append(String.valueOf(week.get(Calendar.YEAR)));
				sb.append("'\">");
				sb.append(String.valueOf(i+1));
				sb.append("</a>");

			sb.append("</td>");
			if(day==Calendar.SATURDAY)
			{
				sb.append("</tr>");
			}
			week.add(Calendar.DATE, 1);
		}
		if(rowcount<=5)
		{
			sb.append("<tr><td><br></td></tr>");	
		}
		
		//week.setTime(current);
		return(sb.toString());
	}
}
