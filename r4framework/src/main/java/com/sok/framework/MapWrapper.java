package com.sok.framework;
import java.util.*;

public class MapWrapper implements org.apache.velocity.context.Context, java.util.Map
{
   protected Map map;
   
   public MapWrapper(Map map)
   {
      this.map = map;
   }
   
   //Returns the value to which this map maps the specified key.
   public Object get(String key)
   {
      return(map.get(key));
   } 
      
   //Returns the value to which this map maps the specified key.
   public Object get(Object key)
   {
      return(map.get(key));
   } 
      
   public Object put(String name, Object value)
   {  
      return(map.put(name, value));
   }     
   
   // Associates the specified value with the specified key in this map (optional operation).
   public Object put(Object name, Object value)
   {  
      return(map.put(name, value));
   }     
      
   //Removes all mappings from this map (optional operation).
   public void clear()
   {
      map.clear();
   }
   
   // Returns true if this map contains a mapping for the specified key.
   public boolean containsKey(Object key)
   {
      return(map.containsKey(key));
   }
      
   // Returns true if this map maps one or more keys to the specified value.
   public boolean containsValue(Object value)
   {
      return(map.containsValue(value));
   }
   
   // Returns a set view of the mappings contained in this map.   
   public Set entrySet()
   {
      return(map.entrySet());
   }   
    
   // Returns a set view of the keys contained in this map.
   public Set keySet()
   {
      return(map.keySet());
   }
    
   public Object[] getKeys()
   {
      return(map.keySet().toArray());
   }  
   
   public int size()
   {
      return(map.size());
   }
   
   public String toString()
   {
      return(map.toString());
   }
   
   // Copies all of the mappings from the specified map to this map (optional operation).
   public void putAll(Map t)
   {
      map.putAll(t);
   }
    
   // Removes the mapping for this key from this map if it is present (optional operation).
   public Object remove(Object key)
   {
      return(map.remove(key));
   }
   
   // Removes the mapping for this key from this map if it is present (optional operation).
   public Object remove(String name)
   {  
      return(map.remove(name));
   }
    
   // Returns a collection view of the values contained in this map.
   public Collection values()
   {
      return(map.values());
   } 
   
   public boolean isEmpty()
   {
      return(map.isEmpty());
   }    
}