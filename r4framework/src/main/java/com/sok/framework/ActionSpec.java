package com.sok.framework;

import org.dom4j.*;

public class ActionSpec extends XMLSpec
{
	//private String maintable;
	private boolean keepmulti = false;
	private boolean allowempty = true;	
	private Element[] actions;

	public static String _table = "table";
	public static String _subform = "subform";
	public static String _subformset = "subformset";		
	public static String _keepmulti = "keepmulti";
	public static String _allowempty = "allowempty";		
	public static String _action = "action";
	public static String _actionset = "actionset";
	public static String _step = "step";
	public static String _type = "type";
	public static String _currenthistorykey = "currenthistorykey";
	public static String _historykey = "historykey";
	public static String _to = "to";
	public static String _from = "from";
	public static String _multion = "multion";
	public static String _default = "default";	
	public static String _autovalues = "autovalues";
	public static String _setviewspec = "setviewspec";
	public static String _settablespec = "settablespec";
	public static String _rspec = "rspec";
	
	public static String _true = "true";
	public static String _false = "false";	
	public static String _history = "history";
	public static String _extension = "extension";
	public static String _main = "main";
	public static String _get = "get";
	public static String _put = "put";
	public static String _putvalue = "putvalue";	
	public static String _doaction = "doaction";
	public static String _setaction = "setaction";
	public static String _clear = "clear";	
	public static String _request = "request";	
	public static String _parserequest = "parserequest";		
	
	public ActionSpec(String path)
	{
		super(path);
		Element root = super.document.getRootElement();
		//maintable = root.attributeValue(_table);
		actions = super.getElementArray(root, _action);
		String multi = root.attributeValue(_keepmulti);
		if(multi!=null && multi.equals(_true))
		{
			keepmulti = true;
		}
		String empty = root.attributeValue(_allowempty);
		if(empty!=null && empty.equals(_false))
		{
			allowempty = false;
		}		
	}

	public boolean isKeepMulti()
	{
		return(keepmulti);
	}
	
	public boolean isAllowEmpty()
	{
		return(allowempty);
	}	
	
	//public String getActionSetTable()
	//{
	//	return(maintable);
	//}

	public int getActionCount()
	{
		return(super.getCount(actions));
	}	
	
	public String getActionTable(int i)
	{
		return(super.getAttribute(actions, i, _table));
	}

	public String getActionSubform(int i)
	{
		return(super.getAttribute(actions, i, _subform));
	}	
	
	public boolean hasActionSubform(int i)
	{
		String value = super.getAttribute(actions, i, _subform);
		if(value!=null && value.length()!=0)
		{
			return(true);
		}
		return(false);
	}		

	public String getActionSubformSetParamName(int i)
	{
		return(super.getAttribute(actions, i, _subformset));
	}		
	
	public boolean hasActionSubformSet(int i)
	{
		String value = super.getAttribute(actions, i, _subformset);
		if(value!=null && value.length()!=0)
		{
			return(true);
		}
		return(false);
	}	
	
	public String getActionType(int i)
	{
		return(super.getAttribute(actions, i, _type));
	}	

	public String getActionMultiOn(int i)
	{
		return(super.getAttribute(actions, i, _multion));
	}		
	
	public boolean hasActionMultiOn(int i)
	{
		String value = super.getAttribute(actions, i, _multion);
		if(value!=null && value.length()!=0)
		{
			return(true);
		}
		return(false);
	}		
	
	public String getActionHistoryKey(int i)
	{
		return(super.getAttribute(actions, i, _historykey));
	}
	
	public String getActionCurrentHistoryKey(int i)
	{
		return(super.getAttribute(actions, i, _currenthistorykey));
	}		

	public int getActionStepCount(int i)
	{
		Element[] steps = super.getElementArray(actions[i], _step);
		return(super.getCount(steps));
	}		
	
	public String getActionStepType(int i, int j)
	{
		Element[] steps = super.getElementArray(actions[i], _step);
		return(super.getAttribute(steps, j, _type));
	}	

	public String getActionStepDefault(int i, int j)
	{
		Element[] steps = super.getElementArray(actions[i], _step);
		return(super.getAttribute(steps, j, _default));
	}		
	
	public boolean hasActionStepDefault(int i, int j)
	{
		Element[] steps = super.getElementArray(actions[i], _step);
		String value = super.getAttribute(steps, j, _default);
		{
			if(value!=null && value.length()!=0)
			{
				return(true);
			}
		}
		return(false);
	}

	public String getActionStepRspec(int i, int j)
	{
		Element[] steps = super.getElementArray(actions[i], _step);
		return(super.getAttribute(steps, j, _rspec));
	}		

	public String getActionStepHistoryKey(int i, int j)
	{
		Element[] steps = super.getElementArray(actions[i], _step);
		return(super.getAttribute(steps, j, _historykey));
	}			
	
	public String getActionStepTo(int i, int j)
	{
		Element[] steps = super.getElementArray(actions[i], _step);
		return(super.getAttribute(steps, j, _to));
	}		
	
	public String getActionStepFrom(int i, int j)
	{
		Element[] steps = super.getElementArray(actions[i], _step);
		return(super.getAttribute(steps, j, _from));
	}
	
	public boolean getActionStepAutoValues(int i, int j)
	{
		Element[] steps = super.getElementArray(actions[i], _step);
		String value = super.getAttribute(steps, j, _autovalues);
		if(value!=null && value.equals(_true))
		{
			return(true);
		}
		return(false);
	}	
	
	public static void main(String[] args)
	{
		ActionSpec as = new ActionSpec("C:\\Development\\Tomcat 5.0\\webapps\\mysok\\specs\\actions\\ContactAction.xml");
		System.out.println(as.getActionCount());
		System.out.println(as.getActionStepCount(0));
	}
}