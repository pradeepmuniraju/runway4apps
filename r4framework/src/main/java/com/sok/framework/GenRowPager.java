package com.sok.framework;

import com.sok.framework.generation.Rows;

public class GenRowPager
{
   public static final String SKIP = "-skip";
   
   protected GenRow row = null;

   int set = 5;
   int size, skip, nextset, prevset, i = 0;

   
   public void getResults(GenRow row, int setsize)
   {
      this.row = row;
      set = setsize;
      prevset = 0;
      
      if(row.getString(SKIP).length()>0){
         skip = Integer.parseInt(row.getString(SKIP));
         prevset = skip - set;
      }        
      
      row.getResults(true);      

      size = row.getSize();

      nextset = skip + set;
      
      for(int i=0; i<skip; i++){
         row.getNext();
      }
   }
   
   public boolean getNext()
   {
      if(row.getNext() && i<set)
      {
         i++;
         return(true);
      }
      return(false);
   }
   
   public boolean hasPreviousSet()
   {
      return(skip!=0);
   }
   
   public boolean hasNextSet()
   {
      return((skip + set)<size);
   }   
   
   public String getPreviousSetParam()
   {
      return(SKIP + "=" + String.valueOf(prevset));
   }
   
   public String getNextSetParam()
   {
      return(SKIP + "=" + String.valueOf(nextset));
   }
   
   public int getPreviousSet()
   {
      return(prevset);
   }
   
   public int getNextSet()
   {
      return(nextset);
   }   
   
   public int getSetFrom() //displaying from
   {
      return(size==0?0:skip + 1);
   }
   
   public int getSetTo() //displaying from
   {
      return(nextset<size?nextset:size);
   }
   
   public int getSize() //search result size
   {
      return(size);
   }
}