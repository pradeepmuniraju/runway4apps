package com.sok.framework.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

public class ServletResponse implements javax.servlet.http.HttpServletResponse {

   private ServletOutputStream out = null;
   private java.io.PrintWriter writer = null;

   public ServletResponse (ServletOutputStream out) {
      this.out = out;
   }

   public void flushBuffer() {
      try {
         out.flush();
         if (writer != null) {
            writer.flush();
         }
      }
      catch (Exception e) {throw new RuntimeException(e);}
   }

/*
   public void closeWriter() {
      if (writer != null) {
         try {
               writer.flush();
               writer.close();
         }
         catch (Exception e) {throw new RuntimeException(e);}
      }
   }*/

   public int getBufferSize() {
      return -1;
   }

   public java.lang.String getCharacterEncoding() {
      throw new RuntimeException("getCharacterEncoding");
   }

   public java.lang.String getContentType() {
      return Locale.getDefault().toString();
   }

   public java.util.Locale getLocale() {
      return Locale.getDefault();
   }

   public ServletOutputStream getOutputStream() {
      return out;
   }

   public java.io.PrintWriter getWriter() {
      if (writer == null ) {
         writer = new java.io.PrintWriter(out, true);
      }
      return writer;
   }

   public boolean isCommitted() {
      return false;
   }

   public void reset() {}

   public void resetBuffer() {}

   public void setBufferSize(int size) {}

   public void setCharacterEncoding(java.lang.String charset) {}

   public void setContentLength(int len) {}

   public void setContentType(java.lang.String type) {}

   public void setLocale(java.util.Locale loc) {}


   // HTTP Methods

   public void addCookie(Cookie cookie) {}
   public void addDateHeader(java.lang.String name, long date) {}
   public void addHeader(java.lang.String name, java.lang.String value) {}
   public void addIntHeader(java.lang.String name, int value) {}

   public boolean containsHeader(java.lang.String name) {
      return false;
   }
   /**
    * @deprecated
    */
   public java.lang.String encodeRedirectUrl(java.lang.String url) {
      return encodeRedirectURL(url);
   }
   /**
    * @deprecated
    */
   public java.lang.String encodeRedirectURL(java.lang.String url) {
      throw new RuntimeException("encodeRedirectURL");
   }
   /**
    * @deprecated
    */
   public java.lang.String encodeUrl(java.lang.String url) {
      return encodeURL(url);
   }
   public java.lang.String encodeURL(java.lang.String url) {
      //throw new RuntimeException("encodeURL");
      return url;
   }

   public void sendError(int sc) {}
   public void sendError(int sc, java.lang.String msg) {}
   public void sendRedirect(java.lang.String location) {}
   public void setDateHeader(java.lang.String name, long date) {}
   public void setHeader(java.lang.String name, java.lang.String value) {}
   public void setIntHeader(java.lang.String name, int value) {}
   public void setStatus(int sc) {}
   /**
    * @deprecated
    */
   public void setStatus(int sc, java.lang.String sm)  {}

//@Override
public String getHeader(String arg0) {
	// TODO Auto-generated method stub
	return null;
}

//@Override
public Collection<String> getHeaderNames() {
	// TODO Auto-generated method stub
	return null;
}

//@Override
public Collection<String> getHeaders(String arg0) {
	// TODO Auto-generated method stub
	return null;
}

//@Override
public int getStatus() {
	// TODO Auto-generated method stub
	return 0;
}

}
