package com.sok.framework.servlet;

import com.sok.framework.*;

import javax.servlet.*;
import javax.servlet.ServletResponse;
import javax.servlet.http.*;

import org.apache.commons.lang.StringUtils;

import java.util.*;
import java.io.IOException;
import java.net.*;

public class ServletRequest implements javax.servlet.http.HttpServletRequest {
   
   private ServletContext context = null;
   
   private javax.servlet.http.HttpSession session = null;
   
   private Map attributes = null;
   private Map parameters = null;
   
   
   private String queryString = null;
   private String uri = null;
   private String contextPath = null;
   
   public ServletRequest(javax.servlet.http.HttpSession session, String path) {
      attributes = new Hashtable();
      parameters = new Hashtable();
      this.session = session;
      this.context = session.getServletContext();
      
      int i = path.indexOf("?");
      if (i != -1) {
         queryString = path.substring(i+1);;
         uri = path.substring(0,i);
         
         parseRequest(queryString);
      }
      else {
         uri = path;
      }
      String temp = com.sok.framework.StringUtil.replace(getURLHome().toString(),"://","");
      int i2 = temp.indexOf("/");
      
      if (i2 != -1) {
         contextPath = temp.substring(i2, temp.length());
      }
      else {
         contextPath = "";
      }
      
   }
   
   public ServletRequest(javax.servlet.http.HttpServletRequest request, String path) {

	   // create a fake request that takes the params of the underlying request 	   
	   parameters = new HashMap<String, Object>(request.getParameterMap());
	   attributes = new Hashtable();
	   Enumeration<String> en = request.getAttributeNames();
	   while(en.hasMoreElements()) {
		   String key = en.nextElement();
		   attributes.put(key, request.getAttribute(key));
	   }
	   int i = path.indexOf("?");
	   if (i != -1) {
		   queryString = path.substring(i+1);
		   uri = path.substring(0,i);
	   }
	   else {
		   uri = path;
	   }
	   session = request.getSession();
	   contextPath = request.getContextPath();
   }
 
 	private void parseRequest(String query) {
		StringTokenizer pst = new StringTokenizer(query, "&");
		String name = null;
		String value = null;
		while(pst.hasMoreTokens()) {
			StringTokenizer nvst = new StringTokenizer(pst.nextToken(), "=");
			if(nvst.hasMoreTokens()) {
				name = nvst.nextToken();
				if(name != null && name.length() != 0) {
					if(nvst.hasMoreTokens()) {
						value = nvst.nextToken();
						if(value != null && value.length() != 0) {
							try {
							   addRequest(URLDecoder.decode(name, ActionBean.utf8), URLDecoder.decode(value, ActionBean.utf8));
							}
							catch(Exception e) { }
						}

					}
				}
			}
		}
	}
 	
   protected Object addRequest(String name, String value)
   {
      Object exsistingvalue = parameters.get(name);
      if(exsistingvalue != null)
      {
         if((exsistingvalue instanceof String[]))
         {
            String[] strarr = (String[])exsistingvalue;
            String[] newstrarr = new String[strarr.length+1];
            System.arraycopy(strarr, 0, newstrarr, 0, strarr.length);
            newstrarr[strarr.length] = value;
            return(parameters.put(name, newstrarr));
         }
         else if(exsistingvalue instanceof String)
         {
            if(exsistingvalue.toString().length()!=0)
            {
               String[] newstrarr = new String[2];
               newstrarr[0] = (String)exsistingvalue;
               newstrarr[1] = value;
               return(parameters.put(name, newstrarr));
            }
         }
         else
         {
            return(parameters.put(name, value));
         }
      }
      return(parameters.put(name, value));
   } 	
	
   public java.lang.Object getAttribute(java.lang.String name) {
      return attributes.get(name);
   }
 
   public java.util.Enumeration getAttributeNames() {
      return new StringTokenizer(StringUtils.join(attributes.keySet(),","),",");
   }
 
   public java.lang.String getCharacterEncoding() {
      return null;
   }
 
   public int getContentLength() {
      return -1;
   }
 
   public java.lang.String getContentType() {
      return null;
   }
 
   public ServletInputStream getInputStream() {
      throw new RuntimeException("getInputStream");
   }
 
   public java.lang.String getLocalAddr() {
      return "127.0.0.1";
   }
 
   public java.util.Locale getLocale() {
      return Locale.getDefault();
   }
 
   public java.util.Enumeration getLocales() {
      Object[] obj = Locale.getAvailableLocales();
      
      Vector v = new Vector(obj.length);
      for (int i=0; i < obj.length; i++) {
         v.add(obj[i]);
      }
      
      return v.elements();
   }
 
   public java.lang.String getLocalName() {
      return "Runway";
   }
 
   public int getLocalPort() {
      return 8080;
   }
 
   public java.lang.String getParameter(java.lang.String name) {
	  Object o = parameters.get(name);
	  if(o instanceof String[]) {
		 String[] s = (String[])o;
		 if(s.length != 0) 
			 return s[0];
		 return null;
	  }
      return (String)parameters.get(name);
   }
 
   public java.util.Map getParameterMap() {
      return parameters;
   }
 
   public java.util.Enumeration getParameterNames() {
	   return new StringTokenizer(StringUtils.join(attributes.keySet(),","),",");
   }
 
   public java.lang.String[] getParameterValues(java.lang.String name) {
      String[] s = {(String)parameters.get(name)};
      
      return s;
   }
 
   public java.lang.String getProtocol() {
      return "HTTP/1.1";
   }
 
   public java.io.BufferedReader getReader() {
      throw new RuntimeException("getReader");
   }

   /**
    * @deprecated
    */
   public java.lang.String getRealPath(java.lang.String path) {
      return context.getRealPath(path);
   }
 
   public java.lang.String getRemoteAddr() {
      return "127.0.0.1";
   }
 
   public java.lang.String getRemoteHost() {
      throw new RuntimeException("getRemoteHost");
   }
 
   public int getRemotePort() {
      return -1;
   }
 
   public RequestDispatcher getRequestDispatcher(java.lang.String path) {
      return context.getRequestDispatcher(path);
   }
 
   public java.lang.String getScheme() {
      return "http";
   }
 
   public java.lang.String getServerName() {
      return "127.0.0.1";
   }
 
   public int getServerPort() {
      return 80;
   }
 
   public boolean isSecure() {
      return false;
   }
 
   public void removeAttribute(java.lang.String name) {
      attributes.remove(name);
   }
 
   public void setAttribute(java.lang.String name, java.lang.Object o) {
      if (o != null) {
         attributes.put(name, o);
      }
      else {
         attributes.remove(name);
      }
   }
 
   public void setCharacterEncoding(java.lang.String env) {}
   
   
   public java.lang.String getAuthType() {
      throw new RuntimeException("getAuthType");
   }
   public java.lang.String getContextPath() {
      return contextPath;
   }
   public Cookie[] getCookies() {
      throw new RuntimeException("getCookies");
   }
   public long getDateHeader(java.lang.String name) {
      throw new RuntimeException("getDateHeader");
   }
   public java.lang.String getHeader(java.lang.String name) {
      return null;
   }
   public java.util.Enumeration getHeaderNames() {
      throw new RuntimeException("getHeaderNames");
   }
   public java.util.Enumeration getHeaders(java.lang.String name) {
      throw new RuntimeException("getHeaders");
   }
   public int getIntHeader(java.lang.String name) {
      throw new RuntimeException("getIntHeader");
   }
   public java.lang.String getMethod() {
      return "GET";
   }
   public java.lang.String getPathInfo() {
      return null;
   }
   public java.lang.String getPathTranslated() {
      return null;
   }
   public java.lang.String getQueryString() {
      return queryString;
   }
   public java.lang.String getRemoteUser() {
      throw new RuntimeException("getRemoteUser");
   }
   public java.lang.String getRequestedSessionId() {
      throw new RuntimeException("getRequestedSessionId");
   }
   public java.lang.String getRequestURI() {
      return contextPath + uri;
   }
   public String getURLHome() {
	   return session.getServletContext().getInitParameter("URLHome");
   }
   public java.lang.StringBuffer getRequestURL() {
	   return new StringBuffer().append(getURLHome()).append(uri);
      //return session.getURLHome().append(uri);
   }
   public java.lang.String getServletPath() {
      return uri;
   }
   public javax.servlet.http.HttpSession getSession() {
      return session;
   }
   public javax.servlet.http.HttpSession getSession(boolean create) {
      return session;
   }
   public java.security.Principal getUserPrincipal() {
      throw new RuntimeException("getUserPrincipal");
   }
   public boolean isRequestedSessionIdFromCookie() {
      return false;
   }
   /**
    * @deprecated
    */
   public boolean isRequestedSessionIdFromUrl() {
      return false;
   }
   public boolean isRequestedSessionIdFromURL() {
      return false;
   }
   public boolean isRequestedSessionIdValid() {
      return true;
   }
   public boolean isUserInRole(java.lang.String role)  {
      return true;
   }

	public AsyncContext getAsyncContext() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public DispatcherType getDispatcherType() {
		// TODO Auto-generated method stub
		return DispatcherType.INCLUDE;
	}
	
	public ServletContext getServletContext() {
		// TODO Auto-generated method stub
		return InitServlet.getConfig().getServletContext();
	}
	
	public boolean isAsyncStarted() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean isAsyncSupported() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public AsyncContext startAsync() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public AsyncContext startAsync(javax.servlet.ServletRequest arg0,
			ServletResponse arg1) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public boolean authenticate(HttpServletResponse arg0) throws IOException,
			ServletException {
		// TODO Auto-generated method stub
		return false;
	}
	
	public Part getPart(String arg0) throws IOException, IllegalStateException,
			ServletException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Collection<Part> getParts() throws IOException, IllegalStateException,
			ServletException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void login(String arg0, String arg1) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
	public void logout() throws ServletException {
		// TODO Auto-generated method stub
		
	}
}