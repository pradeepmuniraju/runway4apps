package com.sok.framework.servlet;

import java.io.*;
import java.sql.*;
import java.util.Enumeration;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;

public class ResponseOutputStream extends ServletOutputStream {
    private HttpSession session = null;

    private OutputStream stream = null;

    private String path = null;

    public ResponseOutputStream(HttpSession session, OutputStream stream, String path) {
	this.session = session;
	this.path = path;
	this.stream = stream;
    }

    public void service() throws ServletException, java.io.IOException {
	service(null);
    }

    public void service(Connection con) throws ServletException, java.io.IOException {

		RequestDispatcher dispatcher = session.getServletContext().getRequestDispatcher(path);
	
		HttpServletRequest request = new com.sok.framework.servlet.ServletRequest(session, path);
    	if (request.getSession() == null) request.setAttribute("session", session);
	
		if (con != null) {
		    request.setAttribute(com.sok.framework.ActionBean.CON, con);
		}

		if (session != null) {
			Enumeration<String> param = session.getAttributeNames();
			while (param.hasMoreElements()) {
				String s = (String) param.nextElement();
				Object v = session.getAttribute(s);
				if (session.getAttribute(s) != null) request.setAttribute(s, v);
			}
		}
		
		dispatcher.include(request, new com.sok.framework.servlet.ServletResponse(this));
    }
    
    public void service(Connection con, HttpServletRequest request) throws ServletException, java.io.IOException {
    	if (request.getSession() == null) request.setAttribute("session", session);
    	
    	RequestDispatcher dispatcher = session.getServletContext().getRequestDispatcher(path);
    	if (con != null) {
		    request.setAttribute(com.sok.framework.ActionBean.CON, con);
		}
    	dispatcher.include(request, new com.sok.framework.servlet.ServletResponse(this));
    }

    public void close() {
	if (stream != null) {
	    try {
		stream.flush();
		stream.close();
	    } catch (Exception e) {
		throw new RuntimeException(e);
	    }
	}
    }

    public void flush() {
	if (stream != null) {
	    try {
		stream.flush();
	    } catch (Exception e) {
		throw new RuntimeException(e);
	    }
	}
    }

    public void write(int i) {
	if (stream != null) {
	    try {
		stream.write(i);
	    } catch (Exception e) {
		throw new RuntimeException(e);
	    }
	}
    }
}