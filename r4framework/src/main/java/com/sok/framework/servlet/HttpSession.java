package com.sok.framework.servlet;

import com.sok.framework.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

public class HttpSession implements javax.servlet.http.HttpSession {

   private ServletContext context = null;
   
   private Hashtable attributes = null;
   private Hashtable values = null;

   private String contextPath = null;
   private StringBuffer urlHome = null;
   
   private String sessionID = null;
   
   public HttpSession(ServletContext context) {
      this.context = context;
      attributes = new Hashtable();
      values = new Hashtable();
      
      sessionID = KeyMaker.generate();
      
      urlHome = new StringBuffer().append(context.getInitParameter("URLHome"));
      
      String temp = com.sok.framework.StringUtil.replace(urlHome.toString(),"://","");
      int i = temp.indexOf("/");
      
      if (i != -1) {
         contextPath = temp.substring(i, temp.length());
      }
      else {
         contextPath = "";
      }
   }
   
   public StringBuffer getURLHome() {
      return urlHome;
   }
   
   public String getContextPath() {
      return contextPath;
   }
   
   public java.lang.Object getAttribute(java.lang.String name) {
      return attributes.get(name);
   }
   public java.util.Enumeration 	getAttributeNames() {
      return attributes.keys();
   }
   public long getCreationTime() {
      return -1l;
   }
   public java.lang.String getId() {
      return sessionID;
   }
   public long 	getLastAccessedTime() {
      return -1l;
   }
   public int 	getMaxInactiveInterval() {
      return -1;
   }
   public ServletContext 	getServletContext() {
      return context;
   }
   /**
    * @deprecated
    */
   public HttpSessionContext 	getSessionContext() {
      return null;
   }
   /**
    * @deprecated
    */
   public java.lang.Object getValue(java.lang.String name) {
      return values.get(name);
   }
   /**
    * @deprecated
    */
   public java.lang.String[] 	getValueNames() {
      return (String[])values.keySet().toArray();
   }
   public void 	invalidate() {}
   public boolean 	isNew() {
      return false;
   }
   /**
    * @deprecated
    */
   public void putValue(java.lang.String name, java.lang.Object value) {
      values.put(name, value);
   }
   /**
    * @deprecated
    */
   public void removeAttribute(java.lang.String name) {
      attributes.remove(name);
   }
   /**
    * @deprecated
    */
   public void removeValue(java.lang.String name) {
      values.remove(name);
   }
   public void setAttribute(java.lang.String name, java.lang.Object value) {
      if (value != null) {
         attributes.put(name, value);
      }
      else {
         attributes.remove(name);
      }
   }
   public void setMaxInactiveInterval(int interval)  {}
}
