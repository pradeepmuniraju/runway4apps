package com.sok.framework.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

import com.sok.framework.StringUtil;

public class LoggingConfigurationListener  implements ServletContextListener  {

		/* configure logger to use configuration file */
	   public void contextInitialized(ServletContextEvent event) {
		   System.out.println("Initializing logging configuration");
           LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
           context.reset(); // override default configuration
           JoranConfigurator jc = new JoranConfigurator();
           jc.setContext(context);
           // inject the name of the current application as "application-name"
           // property of the LoggerContext
           context.putProperty("application-name", StringUtil.ToAlphaNumeric(event.getServletContext().getServletContextName()));
           try {
        	   // we do this so that we can have the logback output file configure where it logs to, different for each app.
        	   jc.doConfigure(event.getServletContext().getRealPath("/WEB-INF/logback.xml"));
			} catch (JoranException e) {
				System.out.println("Error in logging configuration");
				e.printStackTrace();
			} 
	   }

	   public void contextDestroyed(ServletContextEvent event) {
		   // release any reference to system assets, jndi / jmx / custom loggers etc.
		   LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		   if(context != null) 
			   context.stop();
	   }
}
