package com.sok.framework;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;

import org.apache.velocity.app.VelocityEngine;

public class PagingRowSetBean extends RowSetWrapperBean
{
	int listsize = 0;
	int pagesize = 40;
	int page = 1;	
	int currentstart = 0;
	//int currentend = 0;
	int loopcount = 0;
	int set = 0;
	int setsize = 10;
	int mapsetsize = 3;
	
	String pageurl = null;
	String query = null;
	String _first = "|&lt;";
	String _last = "&gt;|";
	String _prior = "&lt;";
	String _next = "&gt;";
	String _priorset = "&lt;&lt;";
	String _nextset = "&gt;&gt;";
	String _scope = "page";
	
	String contextPath = null;
	
	private static final String DEFAULT_VELOCITY_PATH = "/crm/pagingdisplay/";

	public void setPageUrl(String str)
	{
		pageurl = str;
	}

	public void setQueryString(String str)
	{
		query = str;
	}	
	
	/**
	 * Sets the url and the query string for use with page navigation, such that the 
	 * printPageNav() method will be able to function correctly. 
	 * @param request
	 * updated 12/4/07 so as pages served by PageServlet function correctly. 
	 */
	void setPageNav(HttpServletRequest request)
	{
		StringBuffer querybuffer = new StringBuffer();
		Enumeration params = request.getParameterNames();
		while(params.hasMoreElements())
		{
			String paramname = (String)params.nextElement();
			if(!paramname.equals("-page"))
			{
				querybuffer.append("&");
				querybuffer.append(paramname);
				querybuffer.append("=");
				querybuffer.append(request.getParameter(paramname));
			}
		}
				
		pageurl = request.getRequestURI();
		query = querybuffer.toString(); 
	}

	public int getSize()
	{
		return(listsize);
	}
	
	// It is recommended to use method printPageNavWithVelocityScript in 
	// Runway 4 and future releases
	public void printPageNav(JspWriter out)
	{
		int setstart = (set*setsize)+1;
		int setend = (set*setsize)+setsize;
		
    	int last = getLastIndex();

    	try
    	{
    		out.print("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"paging\">");
    		out.print(String.valueOf(listsize));
    		out.print(" found, displaying ");
    		out.print(String.valueOf(currentstart));
    		out.print(" to ");
    		out.print(String.valueOf(last));
    		out.print("</td><td align=\"right\" class=\"paging\">");
      	
      	int lastpage = getPageCount();
      	
      	if(page!=1)
      	{
      		out.print("<a href=\"");
    			out.print(pageurl);
    			out.print("?-page=1");
    			out.print(query);
    			out.print("\" title=\"First Page\">");		
      		out.print(_first);
      		out.print("</a> ");
 			}
 			if(set!=0)
 			{
      		out.print(" <a href=\"");
    			out.print(pageurl);
    			out.print("?-page=");
    			out.print(String.valueOf(page-setsize));	
    			out.print(query);
    			out.print("\" title=\"Prior Set\" class=\"pagingnav\">");        		
      		out.print(_priorset);
      		out.print("</a> ");  
 			}
      	if(page!=1)
      	{ 			     		
      		out.print("<a href=\"");
    			out.print(pageurl);
    			out.print("?-page=");
    			out.print(String.valueOf(page-1));    			
    			out.print(query);
    			out.print("\" title=\"Prior Page\" class=\"pagingnav\">");        		
      		out.print(_prior);
      		out.print("</a> ");      		
      	}
      	
      	int pagecount = setstart;
      	String count = null;

      	for(;pagecount<=lastpage && pagecount<=setend ;pagecount++)
    		{
    			count = String.valueOf(pagecount);

    			if(page == pagecount)
    			{
					out.print("<span>" + count + "</span>");
					out.print(" ");			
    			}
    			else
    			{
    				out.print("<a href=\"");
	    			out.print(pageurl);
	    			out.print("?-page=");
	    			out.print(count);	
	    			out.print(query);		
	    			out.print("\" title=\"Page ");
	    			out.print(count);
    				out.print("\">");
    				out.print(count);
    				out.print("</a> ");
    			}
      	}
			
			if(page != lastpage)
			{
      		out.print("<a href=\"");
    			out.print(pageurl);
    			out.print("?-page=");
    			out.print(String.valueOf(page+1));
    			out.print(query);	
    			out.print("\" title=\"Next Page\">");        		
				out.print(_next);
      		out.print("</a> ");
      	}

     		if(lastpage>setend)
     		{
      		out.print("<a href=\"");
    			out.print(pageurl);
    			out.print("?-page=");
    			if((setsize+page)>lastpage)
    			{
    				out.print(String.valueOf(lastpage));
    			}
    			else
    			{
    				out.print(String.valueOf(setsize+page));
    			}
    			out.print(query);    			
    			out.print("\" title=\"Next Set\">"); 
				out.print(_nextset);
      		out.print("</a> "); 
     		}
			if(page != lastpage)
			{      						
      		out.print("<a href=\"");
    			out.print(pageurl);
    			out.print("?-page=");
    			out.print(String.valueOf(lastpage));
    			out.print(query);    			
    			out.print("\" title=\"Last Page\">"); 
				out.print(_last);
      		out.print("</a>"); 				
      	}
      	
      	out.print("</td></tr></table>");
     
    	}
    	catch (Exception e)
    	{
      	super.resultbean.setError(e.toString());
    	}
	}	
	
	public void printPageNavWithVelocityScript(JspWriter out, String velocityScript) throws IOException
	{
		printPageNavWithVelocityScript(out, velocityScript, DEFAULT_VELOCITY_PATH, null);
	}
	

	/** Allows to pass customparasm to velocity script
	 * @param out
	 * @param velocityScript
	 * @param customParams
	 * @throws IOException
	 */
	public void printPageNavWithVelocityScript(JspWriter out, String velocityScript, HashMap<String, String> customParams) throws IOException
	{
		printPageNavWithVelocityScript(out, velocityScript, DEFAULT_VELOCITY_PATH, customParams);
	}

	/**
	 * This method allows a velocity script to be used for displaying the paging.
	 * Place the velocity script in folder /crm/pagingdisplay. 
	 * If the script is called 'runway4.vt', use 'runway4'. The file ext is not
	 * needed.
	 * ie. 
	 * PagingRowSetBean companylist = new PagingRowSetBean();
	 * companylist.printPageNavWithVelocityScript(out, "runway4")
	 * @author Simon Sher
	 * @date May 7, 2009
	 * @param out 
	 * @param velocityScript  The velocity script to be used for displaying
	 * the paging
	 * @param path Path of the velocity script
	 * @throws IOException
	 */
	public void printPageNavWithVelocityScript(JspWriter out, String velocityScript, String path, HashMap<String, String> customParams) throws IOException
	{	
		
    	int last = getLastIndex();
    	int lastpage = getPageCount();  	      	
      	List<String> links = new ArrayList<String>();
      	      	  
      	int setNumber;
      	
		if(page>setsize)
		{
			setNumber = page / setsize;
			if (page % setsize == 0) {
				setNumber = setNumber - 1;
			}
		}
		else
		{
			setNumber = 0;	
		}
  	
		int setstart = (setNumber*setsize)+1;
		int setend = (setNumber*setsize)+setsize;  
      	
      	//PAGE NUMBERS
      	for(;setstart<=lastpage && setstart<=setend ;setstart++)
    	{
			links.add( String.valueOf(setstart) );
      	}   		    	
    	
		GenRow bean = new GenRow();
		bean.setParameter("ContextPath",contextPath);
		bean.setParameter("ListSize",String.valueOf(listsize));
		bean.setParameter("CurrentStart",currentstart);
		bean.setParameter("Last",last);
		bean.setParameter("Page",page);
		bean.setParameter("PageForLoop",String.valueOf(page));
		bean.setParameter("LastPage",lastpage);
		bean.setParameter("PageUrl",pageurl);
		bean.setParameter("Query",query);
		bean.setParameter("SetStart",setstart);
		bean.setParameter("Links",links);
		bean.setParameter("Set",setNumber);
		bean.setParameter("SetEnd",setend);
		bean.setParameter("SetSize",setsize);
		
		//Adding custom params
		if (customParams != null) {			
			Iterator<String> it = customParams.keySet().iterator();

			while (it.hasNext()) {
				String key = it.next();
				bean.setParameter(key ,customParams.get(key));

			}
		}
		
		bean.put("searchbean", searchbean);
		StringWriter text = null;

		try{

			text = new StringWriter();
	
			StringBuffer roottemplate = new StringBuffer();
			roottemplate.append("#parse(\"");
			roottemplate.append(path);
			roottemplate.append(velocityScript);
			roottemplate.append(".vt\")");
	
			VelocityEngine ve = VelocityManager.getEngine();
			ve.evaluate( bean, text, "PagingDisplay", roottemplate.toString());
	
			text.flush();
			text.close();
		
			out.print(text);

		}catch(Exception e){
			out.print(e.toString());
		}

	}
	
	public void printPageNavWithMaps(JspWriter out)
	{
		int setstart = (set*setsize)+1;
		int setend = (set*setsize)+setsize;
		
    	int last = getLastIndex();
    	int lastpage = getPageCount();
    	
    	try
    	{    		
    		
    	// START Record Number Summary	
    		out.print("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
    		out.print("<tr>");
    		out.print("<td style=\"padding-left:16px\" width=\"25%\" class=\"pagination_results\"><span>");
    		out.print(String.valueOf(listsize));
    		out.print(" found, displaying ");
    		out.print(String.valueOf(currentstart));
    		out.print(" to ");
    		out.print(String.valueOf(last));
    		out.print("</span></td>");
    		
    	// END Record Number Summary	
    	
    	out.print("<td width=\"50%\" class=\"pagination_results_center\">");
      	out.print("<table style=\"margin: 0pt auto\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
      	out.print("<tr>");
    		
    	// Previous Button
    	if(page != 1)
    	{
    		out.print("<td class=\"paginationDetails\" style=\"text-align:center;\">");
      		out.print("<a href=\"");
    		out.print(pageurl);
    		out.print("?-page=");
    		out.print(String.valueOf(page-1));    			
    		out.print(query);
    		out.print("\" title=\"Prior Page\"");
    		out.print("onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('prev_roll','','" + contextPath + "/images/buttons/pagination/bttn_prev_over.jpg',1)\">");
    		out.print("<img class=\"paginationDetails\" src=\"" + contextPath + "/images/buttons/pagination/bttn_prev.jpg\" alt=\"Prev\" width=\"49\" height=\"28\" border=\"0\" name=\"prev_roll\" />");
    		
    		
    		out.print("</a>");
    		out.print("<img src=\"" + contextPath + "/images/buttons/pagination/num_divide.jpg\" alt=\" \" width=\"5\" height=\"28\" border=\"0\" />");
    		out.print("</td>");
    	}  	
    	
    	else if(page == 1)
    	{
    		out.print("<td class=\"paginationDetails\" style=\"text-align:center;\">");
    		out.print("<img class=\"paginationDetails\" src=\"" + contextPath + "/images/buttons/pagination/bttn_prev_off.jpg\" alt=\"Prev\" width=\"49\" height=\"28\" border=\"0\" />");
    		out.print("<img src=\"" + contextPath + "/images/buttons/pagination/num_divide.jpg\" alt=\" \" width=\"5\" height=\"28\" border=\"0\" />");
    		out.print("</td>");
    	}
    	
    	// Page Numbers
   		out.print("<td class=\"paginationDetails\" style=\"text-align:center;\"><div id=\"pagination\">");
 
      	int pagecount = setstart;
      	String count = null;   		
   		
      	//PAGE NUMBERS
      	for(;pagecount<=lastpage && pagecount<=setend ;pagecount++)
    		{
    			count = String.valueOf(pagecount);

    			if(page == pagecount)
    			{
					out.print("<a class=\"pagination_selected\" href=\"#\">" + count + "</a>");
					out.print(" ");			
    			}
    			else
    			{
    				out.print("<a href=\"");
	    			out.print(pageurl);
	    			out.print("?-page=");
	    			out.print(count);	
	    			out.print(query);		
	    			out.print("\" title=\"Page ");
	    			out.print(count);
    				out.print("\">");
    				out.print(count);
    				out.print("</a> ");
    			}
      	}   		
   		
   		out.print("</div></td>");
      	
   		// Next Button
   		if(page != lastpage)
   		{
   			out.print("<td class=\"paginationDetails\" style=\"text-align:left;\">");
   			out.print("<img src=\"" + contextPath + "/images/buttons/pagination/num_divide.jpg\" alt=\" \" width=\"5\" height=\"28\" border=\"0\" />");
      		out.print("<a href=\"");
			out.print(pageurl);
			out.print("?-page=");
			out.print(String.valueOf(page+1));
			out.print(query);	
			out.print("\" title=\"Next Page\"");
			out.print("onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('next_roll','','" + contextPath + "/images/buttons/pagination/bttn_next_over.jpg',1)\">");			
   			out.print("<img src=\"" + contextPath + "/images/buttons/pagination/bttn_next.jpg\" alt=\"Next\" width=\"49\" height=\"28\" border=\"0\" name=\"next_roll\" /></a>");
   			out.print("</td>");
   		}
   		else if(page == lastpage)
   		{
   			out.print("<td class=\"paginationDetails\" style=\"text-align:left;\">");
   			out.print("<img src=\"" + contextPath + "/images/buttons/pagination/num_divide.jpg\" alt=\" \" width=\"5\" height=\"28\" border=\"0\" />");
   			out.print("<img src=\"" + contextPath + "/images/buttons/pagination/bttn_next_off.jpg\" alt=\"Next\" width=\"49\" height=\"28\" border=\"0\" />");
   			out.print("</td>");
   		}    		
    	out.print("</tr>");
    	out.print("</table>");    		
  
    	
    	out.print("</td><td width=\"25%\"></td>");
    			
		out.print("<td class=\"paginationDetails\" style=\"text-align:right;padding-right:15px\">");
		out.print("<a title=\"Google Maps\" href=\"" + contextPath + "/crm/sog-googlemaps-list.jsp\"><img src=\"" + contextPath + "/images/bttn_maps.png\" alt=\" \" width=\"16\" height=\"16\" border=\"0\" /></a>");
		out.print("</td>");		
    	out.print("</tr></table>");
     
    	}
    	catch (Exception e)
    	{
      	super.resultbean.setError(e.toString());
    	}
	}	
	
	
	
	public void printMapPageNav(JspWriter out)
	{
		int setstart = (set*mapsetsize)+1;
		int setend = (set*mapsetsize)+mapsetsize;
		
    	int last = getLastIndex();
    	int lastpage = getPageCount();
    	
    	try
    	{    		
    		
    		out.print("<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
    		out.print("<tr>");    		
 	
    	out.print("<td width=\"100%\" class=\"pagination_results_center\">");
      	out.print("<table style=\"margin: 0pt auto\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
      	out.print("<tr>");
    		
    	// Previous Button
    	if(page != 1)
    	{
    		out.print("<td class=\"paginationDetails\" style=\"text-align:center;\">");
      		out.print("<a href=\"");
    		out.print(pageurl);
    		out.print("?-page=");
    		out.print(String.valueOf(page-1));    			
    		out.print(query);
    		out.print("\" title=\"Prior Page\"");
    		out.print("onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('prev_roll','','" + contextPath + "/images/buttons/pagination/bttn_prev_over.jpg',1)\">");
    		out.print("<img class=\"paginationDetails\" src=\"" + contextPath + "/images/buttons/pagination/bttn_prev.jpg\" alt=\"Prev\" width=\"49\" height=\"28\" border=\"0\" name=\"prev_roll\" />");
    		
    		
    		out.print("</a>");
    		out.print("<img src=\"" + contextPath + "/images/buttons/pagination/num_divide.jpg\" alt=\" \" width=\"5\" height=\"28\" border=\"0\" />");
    		out.print("</td>");
    	}  	
    	
    	else if(page == 1)
    	{
    		out.print("<td class=\"paginationDetails\" style=\"text-align:center;\">");
    		out.print("<img class=\"paginationDetails\" src=\"" + contextPath + "/images/buttons/pagination/bttn_prev_off.jpg\" alt=\"Prev\" width=\"49\" height=\"28\" border=\"0\" />");
    		out.print("<img src=\"" + contextPath + "/images/buttons/pagination/num_divide.jpg\" alt=\" \" width=\"5\" height=\"28\" border=\"0\" />");
    		out.print("</td>");
    	}
    	
    	// Page Numbers
   		out.print("<td class=\"paginationDetails\" style=\"text-align:center;\"><div id=\"pagination\">");
 
      	int pagecount = setstart;
      	String count = null;   		
   		
      	//PAGE NUMBERS
      	for(;pagecount<=lastpage && pagecount<=setend ;pagecount++)
    		{
    			count = String.valueOf(pagecount);

    			if(page == pagecount)
    			{
					out.print("<a class=\"pagination_selected\" href=\"#\">" + count + "</a>");
					out.print(" ");			
    			}
    			else
    			{
    				out.print("<a href=\"");
	    			out.print(pageurl);
	    			out.print("?-page=");
	    			out.print(count);	
	    			out.print(query);		
	    			out.print("\" title=\"Page ");
	    			out.print(count);
    				out.print("\">");
    				out.print(count);
    				out.print("</a> ");
    			}
      	}   		
   		
   		out.print("</div></td>");
      	
   		// Next Button
   		if(page != lastpage)
   		{
   			out.print("<td class=\"paginationDetails\" style=\"text-align:left;\">");
   			out.print("<img src=\"" + contextPath + "/images/buttons/pagination/num_divide.jpg\" alt=\" \" width=\"5\" height=\"28\" border=\"0\" />");
      		out.print("<a href=\"");
			out.print(pageurl);
			out.print("?-page=");
			out.print(String.valueOf(page+1));
			out.print(query);	
			out.print("\" title=\"Next Page\"");
			out.print("onmouseout=\"MM_swapImgRestore()\" onmouseover=\"MM_swapImage('next_roll','','" + contextPath + "/images/buttons/pagination/bttn_next_over.jpg',1)\">");			
   			out.print("<img src=\"" + contextPath + "/images/buttons/pagination/bttn_next.jpg\" alt=\"Next\" width=\"49\" height=\"28\" border=\"0\" name=\"next_roll\" /></a>");
   			out.print("</td>");
   		}
   		else if(page == lastpage)
   		{
   			out.print("<td class=\"paginationDetails\" style=\"text-align:left;\">");
   			out.print("<img src=\"" + contextPath + "/images/buttons/pagination/num_divide.jpg\" alt=\" \" width=\"5\" height=\"28\" border=\"0\" />");
   			out.print("<img src=\"" + contextPath + "/images/buttons/pagination/bttn_next_off.jpg\" alt=\"Next\" width=\"49\" height=\"28\" border=\"0\" />");
   			out.print("</td>");
   		}
    		
    	out.print("</tr>");
    	out.print("</table>");    		
  
    	
    	out.print("</td></tr></table>");
     
    	}
    	catch (Exception e)
    	{
      	super.resultbean.setError(e.toString());
    	}
	}	

	public void setRequest(HttpServletRequest request)
	{
		super.setRequest(request);
		setPageNav(request);
		contextPath = request.getContextPath();
		String pagenum = request.getParameter("-page");

		if(pagenum!=null)
		{
 			page = Integer.parseInt(pagenum);
 			if(page>setsize)
 			{
 				set = page / setsize;
 			}
 			else
 			{
 				set = 0;	
 			}
 		}
 		else
 		{
 			page = 1;	
 		}

		super.rowindex = ((page - 1) * pagesize ) + 1;
		currentstart = super.rowindex;
		loopcount = 0;
	}

	public void setPageSize(String num)
	{
		pagesize = Integer.parseInt(num);		
	}

	public void setPageSize(int num)
	{
		pagesize = num;		
	}

	public void setSetSize(String num)
	{
		setsize = Integer.parseInt(num);		
	}

	public boolean getResults()
	{
		try
		{
			ActionBean.retrieveResultSet(this);
			ResultSetMetaData meta = result.getMetaData();
			super.colcount = meta.getColumnCount();
			super.columnnames = new String[super.colcount];
			for(int i=0; i<super.colcount; i++)
			{
				super.columnnames[i] = meta.getColumnLabel(i+1);
			}
	  		super.result.last();
			listsize = super.result.getRow();
			super.result.beforeFirst();
			return(true);
		}
		catch(Exception e)
		{
			super.resultbean.setError(e.toString());
			super.resultbean.setError(ActionBean.RN);
		}

		return(false);
	}

	public boolean getNext()
	{
		if(super.result!=null)
		{
			super.resultbean.clear();
			try
			{
				if(ActionBean.initFromDB(super.result, super.rowindex, super.resultbean) && loopcount < pagesize)
				{
					super.rowindex++;
					loopcount++;
					return(true);
				}
			}
			catch(Exception e)
			{
				super.resultbean.setError(e.toString());
				super.resultbean.setError(ActionBean.RN);
			}
		}
		return(false);
	}

   public void setPageNumber(int page) {
      this.page = page;
      super.rowindex = ((page - 1) * pagesize ) + 1;
   }
   
	public void setPageNumber(String num)
	{
	   try {
		   setPageNumber(Integer.parseInt(num));		
		}
		catch (Exception e) {
		   setPageNumber(1);
		}
	}
   
   public int getPageNumber() {
      return page;
   }
   
   public int getPageCount() {
   	int lastpage = (listsize / pagesize);
   	if((listsize % pagesize)!=0)
   	{
   		lastpage++;
   	}
   	return  lastpage;
   }
   
   public int getFirstIndex() {
      return ((page-1) * pagesize) + 1;
   }
   
   public int getLastIndex() {
      int last = 0;
    	int adjrowindex = getFirstIndex() -1;
    	if((adjrowindex + pagesize) > listsize)
    	{
    		last = adjrowindex + (listsize % pagesize);
    	}
    	else if((adjrowindex + pagesize) <= listsize)
    	{
    		last = adjrowindex + pagesize;
    	}
    	return last;
   }
}
