package com.sok.framework;

import java.util.Date;

public class DateUtil {
   public static final String DATE_FORMAT_STANDARD = "dd/MMM/yyyy";
   public static final String DATE_FORMAT_SHORT = "dd/MM/yyyy";
   public static final String DATE_FORMAT_DETAILED = "EEEE d MMMM yyyy";

   public static final String DATE_FORMAT_FILE = "yyyy_MM_dd_HH_mm";
   
   public static final String TIME_FORMAT_STANDARD = "h:mm aaa";

   public static String formatDate(Date date, String format) {
      String formattedDate = "";
   
      if(date != null && format != null) {
         DateToken dateToken = new DateToken();
         dateToken.setDate(date);
         formattedDate = dateToken.format(format);
      }
      
      return formattedDate;
   }
}
