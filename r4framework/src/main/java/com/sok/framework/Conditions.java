/**
 * 
 */
package com.sok.framework;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sog.pdfserver.client.api.request.PDFRequest;
import com.sog.pdfserver.client.api.request.PageStaticData;
import com.sog.pdfserver.client.api.request.TemplateListTag;
import com.sok.runway.ProductNode;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.offline.halManager.HouseAndLandManager;

/**
 * @author Puggs
 *
 */
public class Conditions {
	
	private Map<String,String> 			map = new HashMap<String,String>(); 
	private GenRow						product = new GenRow();
	
	private final int					DAY = 24 * 60 * 60 * 1000;
	
	private final SimpleDateFormat		sdf = new SimpleDateFormat("dd/MM/yyyy");

	
	private static final Logger logger = LoggerFactory.getLogger(Conditions.class);

	/**
	 * 
	 */
	public Conditions(Map<String,String> map) {
		this.map = map;
		
		map.put("NOW", sdf.format(new Date()));
	}
	
	public Conditions(String productID) throws FileNotFoundException {
		if (productID == null || productID.length() == 0) {
			throw new FileNotFoundException("Product not specified");
		}
		loadQuickProduct(productID);
		loadProduct(productID);
		loadProductAnswers(productID);
		loadProductDetails(productID);
		loadProductProducts(productID);
		
		map.put("NOW", sdf.format(new Date()));
	}

	public Conditions(String productID, String orderID) throws FileNotFoundException {
		if (productID == null || productID.length() == 0 || orderID == null || orderID.length() == 0) {
			throw new FileNotFoundException("Product not specified");
		}
		loadQuickProduct(productID);
		loadProduct(productID);
		loadProductAnswers(productID);
		loadProductDetails(productID);
		loadProductProducts(productID);
		loadOrder(orderID);
		loadOrderDetails(orderID);
		loadOrderAnswers(orderID);
		
		map.put("NOW", sdf.format(new Date()));
	}

	public Conditions(String productID, String orderID, String opportunityID, String viewStageID) throws FileNotFoundException {
		if (productID == null || productID.length() == 0 || opportunityID == null || opportunityID.length() == 0) {
			throw new FileNotFoundException("Product not specified");
		}
		loadQuickProduct(productID);
		loadProduct(productID);
		loadProductAnswers(productID);
		loadProductDetails(productID);
		loadProductProducts(productID);
		if (StringUtils.isNotBlank(orderID)) {
			loadOrder(orderID);
			loadOrderDetails(orderID);
			loadOrderAnswers(orderID);
		}
		LoadOpportunity(opportunityID, viewStageID);
		
		map.put("NOW", sdf.format(new Date()));
	}

	private void loadProductDetails(String productID) {
		String ignorelist = "ProductDetailsID,ProductID,CopiedFromProductID,ProductType,DetailsType,SortOrder";
		
		TableSpec detailsSpec = null;
		try {
			detailsSpec = SpecManager.getTableSpec("[specific/]properties/PlanProductDetails");
		} catch (Exception e) {
			detailsSpec = SpecManager.getTableSpec("properties/PlanProductDetails");
		}
		
		GenRow details = new GenRow();
		details.setTableSpec("properties/ProductDetails");
		details.setParameter("-select","*");
		details.setColumn("ProductID",productID);
		details.sortBy("SortOrder",0);
		details.sortBy("DetailsType",1);
		if (details.getString("ProductID").length() > 0) {
			details.doAction("search");
			details.getResults(true);
		}
		
		int d = 0;
		while (details.getNext()) {
			String pType = details.getString("ProductType").replaceFirst("Details", "").replaceAll(" ", "") + "_";
			for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
				if (ignorelist.indexOf(detailsSpec.getFieldName(cs)) == -1) {
					map.put((pType + details.getString("DetailsType") + "_" + detailsSpec.getFieldName(cs)), details.getString(detailsSpec.getFieldName(cs)));
				}
			}
			++d;
		}
		details.close();
	}

	private void loadProductAnswers(String productID) {
		GenRow row = new GenRow();
		row.setViewSpec("answers/ProductAnswerView");
		row.setParameter("ProductID", productID);

		if (StringUtils.isNotBlank(productID)) {
			row.doAction("search");
			row.getResults();
		}
		
		while (row.getNext()) {
			if (row.isSet("QuestionShortLabel")) map.put("Profile." + row.getString("QuestionShortLabel") + ".Answer", row.getString("Answer").replaceAll(" ", "_"));
			if (row.isSet("QuestionLabel")) map.put("Profile." + row.getString("QuestionLabel") + ".Answer", row.getString("Answer").replaceAll(" ", "_"));
			if (row.isSet("QuestionShortLabel")) map.put("Profile_" + row.getString("QuestionShortLabel") + "_Answer", row.getString("Answer").replaceAll(" ", "_"));
			if (row.isSet("QuestionLabel")) map.put("Profile_" + row.getString("QuestionLabel") + "_Answer", row.getString("Answer").replaceAll(" ", "_"));
		}
		
		row.close();
	}

	private void loadContactAnswers(String contactID) {
		GenRow row = new GenRow();
		row.setViewSpec("answers/ContactAnswerView");
		row.setParameter("ContactID", contactID);
		
		if (StringUtils.isNotBlank(contactID)) {
			row.doAction("search");
			row.getResults();
		}
		
		while (row.getNext()) {
			if (row.isSet("QuestionShortLabel")) map.put("Profile." + row.getString("QuestionShortLabel") + ".Answer", row.getString("Answer").replaceAll(" ", "_"));
			if (row.isSet("QuestionLabel")) map.put("Profile." + row.getString("QuestionLabel") + ".Answer", row.getString("Answer").replaceAll(" ", "_"));
			if (row.isSet("QuestionShortLabel")) map.put("Profile_" + row.getString("QuestionShortLabel") + "_Answer", row.getString("Answer").replaceAll(" ", "_"));
			if (row.isSet("QuestionLabel")) map.put("Profile_" + row.getString("QuestionLabel") + "_Answer", row.getString("Answer").replaceAll(" ", "_"));
		}
		
		row.close();
	}

	private void loadOrderAnswers(String orderID) {
		GenRow row = new GenRow();
		row.setViewSpec("answers/OrderAnswerView");
		row.setParameter("OrderID", orderID);
		
		if (StringUtils.isNotBlank(orderID)) {
			row.doAction("search");
			row.getResults();
		}
		
		while (row.getNext()) {
			if (row.isSet("QuestionShortLabel")) map.put("Profile." + row.getString("QuestionShortLabel") + ".Answer", row.getString("Answer").replaceAll(" ", "_"));
			if (row.isSet("QuestionLabel")) map.put("Profile." + row.getString("QuestionLabel") + ".Answer", row.getString("Answer").replaceAll(" ", "_"));
			if (row.isSet("QuestionShortLabel")) map.put("Profile_" + row.getString("QuestionShortLabel") + "_Answer", row.getString("Answer").replaceAll(" ", "_"));
			if (row.isSet("QuestionLabel")) map.put("Profile_" + row.getString("QuestionLabel") + "_Answer", row.getString("Answer").replaceAll(" ", "_"));
		}
		
		row.close();
	}

	private void loadProductProducts(String productID) {
		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setParameter("ProductID", productID);
		row.setParameter("ProductType", "Package Cost");
		row.setParameter("ProductProductsLinkedProduct.ProductType", "Package Cost");
		row.doAction("search");
		
		row.getResults();
		
		//System.out.println(row.getStatement());
		
		while (row.getNext()) {
			//System.out.println(row.getData("ProductType").replaceAll(" ", "") + "." + row.getString("Name") + ".Applied " + row.getString("Active"));
			map.put(row.getData("ProductType").replaceAll(" ", "") + "." + row.getString("Name") + ".Applied", row.getString("Active"));
			map.put(row.getData("ProductType").replaceAll(" ", "") + "_" + row.getString("Name") + "_Applied", row.getString("Active"));
		}
		
		row.close();
	}

	private void loadOrder(String orderID) {
		GenRow row = new GenRow();
		row.setViewSpec("OrderView");
		row.setParameter("OrderID", orderID);
		row.doAction("selectfirst");

		loadContactAnswers(row.getString("ContactID"));
		
		row.close();
	}

	private void loadOrderDetails(String orderID) {
		GenRow row = new GenRow();
		row.setViewSpec("OrderItemsView");
		row.setParameter("OrderID", orderID);
		row.setParameter("Category", "DRIP");
		row.doAction("search");
		
		row.getResults();
		
		while (row.getNext()) {
			//System.out.println("Drip." + row.getString("Name") + ".Applied");
			map.put("Drip." + row.getString("Name") + ".Applied", "Y");
			map.put("Drip_" + row.getString("Name") + "_Applied", "Y");
		}
		
		row.close();
	}

	private void loadProduct(String productID) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setParameter("-select0", "ProductType");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");
		
		if ("House and Land".equals(row.getString("ProductType"))) {
			product.setViewSpec("properties/ProductHouseAndLandView");
		} else if ("Home Plan".equals(row.getString("ProductType"))) {
				product.setViewSpec("properties/ProductPlanView");
		} else if ("Land".equals(row.getString("ProductType"))) {
			product.setViewSpec("properties/ProductLotView");
		} else if ("Apartment".equals(row.getString("ProductType"))) {
			product.setViewSpec("properties/ProductApartmentView");
		} else if ("Existing Properties".equals(row.getString("ProductType"))) {
			product.setViewSpec("properties/ProductExistingPropertyView");
		}
		
		product.setParameter("ProductID", productID);
		product.setParameter("ProductType", row.getString("ProductType"));
		
		product.doAction("selectfirst");
		
		RunwayUtil.convertRow2Map(product, map, "Product.");
		RunwayUtil.convertRow2Map(product, map, "Product_");
		
	}
	
	private void loadQuickProduct(String productID) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setParameter("-select0", "ProductType");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");
		
		product.setViewSpec("properties/ProductQuickIndexDescriptionView");

		if ("Apartment".equals(row.getString("ProductType"))) {
			product.setViewSpec("properties/ProductQuickIndexApartmentDescriptionView");
		}
		
		product.setParameter("ProductID", productID);
		product.setParameter("ProductType", row.getString("ProductType"));
		
		product.doAction("selectfirst");
		
		RunwayUtil.convertRow2Map(product, map, "Quick.");
		RunwayUtil.convertRow2Map(product, map, "Quick_");
		
	}
	
	public String subValues(String string) {
		String results = " " + string + " ";
		
		Iterator<String> i = map.keySet().iterator();
		
		while (i.hasNext()) {
			String key = i.next();
			String value = map.get(key);
			if (value != null && value.length() > 0) 
				value = value.replaceAll(" ", "_");
			else 
				value = "-missing-";
			
			results = results.replaceAll(" " + key.replaceAll("\\[", "\\\\[")
					.replaceAll("\\]", "\\\\]")
					.replaceAll("\\(", "\\\\(")
					.replaceAll("\\)", "\\\\)") + " ", " " + value + " ");
			
			//System.out.println(key + " = " + value + " ~ " + results);
		}
		
		return results.trim();
	}
	
	private String makeNice(String string) {
		if (StringUtils.isNotBlank(string)) {
			string = string.replaceAll("[\\+\\-\\*\\/\\<\\=\\!\\>~]"," $0 ");
			string = string.replaceAll("[\\\\&\\\\|]{2}", " $0 ");
			while (string.indexOf("  ") != -1) string = string.replaceAll("  "," ");
			string = string.trim();
		}
		
		return string;
	}
	
	public boolean evaluate(String string) {
		String results = evaluateText(makeNice(string));

		if (results == null || results.length() == 0)
			return true;
		
		String temp = evaluateBoolean(results);
		while (!temp.equals(results)) {
			results = temp;;
			temp = evaluateBoolean(results);
		}

		return results.indexOf("false") == -1;
		
	}
	
	private String evaluateBoolean(String string) {
		StringTokenizer st = new StringTokenizer(makeNice(string), " ");
		if (st.hasMoreTokens()) {
			String token1 = st.nextToken().trim();
			while (st.hasMoreTokens()) {
				if (token1.indexOf("||") >= 0 || token1.indexOf("&&") >= 0) return "false";
				if (st.hasMoreTokens()) {
					String maths = st.nextToken().trim();
					if (maths.indexOf("||") == -1 && maths.indexOf("&&") == -1)	return "false";
					if (st.hasMoreTokens()) {
						String token2 = st.nextToken().trim();
						if (token2.indexOf("||") >= 0 || token2.indexOf("&&") >= 0) return "false";
						try {
							if ("&&".equals(maths)) { 
								if ("true".equalsIgnoreCase(token1) && "true".equals(token2)) {
									string = string.replaceFirst((token1 + " [\\\\&]{2} " + token2) ,"true");
									token1 = "true";
								} else {
									string = string.replaceFirst((token1 + " [\\\\&]{2} " + token2) ,"false");
									token1 = "false";
								}
							} else if ("||".equals(maths)) { 
								if ("true".equalsIgnoreCase(token1) || "true".equals(token2)) {
									string = string.replaceFirst((token1 + " [\\\\|]{2} " + token2) ,"true");
									token1 = "true";
								} else {
									string = string.replaceFirst((token1 + " [\\\\|]{2} " + token2) ,"false");
									token1 = "false";
								}
							} 
						} catch (Exception e) {
							return "false";
						}
					}
				}
			}
		}
		return string;
	}

	public String evaluateText(String string) {
		if (string == null || string.length() == 0)
			return "";
		
		String results = subValues(makeNice(string));
		
		String[] parts = results.split("[\\\\&\\\\|]{2}");

		// TODO: added code for || or method

		for (int p = 0; p < parts.length; ++p) {
			String tag = parts[p];
			boolean b = evaluateSingle(tag);
			//if (!b && rule.indexOf("||") == -1)
			//	return false;
			results = results.replaceAll(
					tag.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]")
							.replaceAll("\\(", "\\\\(")
							.replaceAll("\\)", "\\\\)")
							.replaceAll("\\+", "\\\\+")
							.replaceAll("\\-", "\\\\-")
							.replaceAll("\\/", "\\\\/")
							.replaceAll("\\*", "\\\\*"),
					" " + String.valueOf(b) + " ");
		}

		return results;
		
	}
	
	public boolean evaluateSingle(String rule) {
			String ifDim = makeNice(rule);

			boolean missing = false;

			if (ifDim.length() != ifDim
					.replaceAll("[\\+\\-\\*\\/\\<\\=\\!\\>~]", "").length()) {
				
				String[] parts = ifDim.split("[\\+\\-\\*\\/\\<\\=\\!\\>~]");
				if (parts != null && parts.length > 0) {
					for (int p = 0; p < parts.length; ++p) {
						if (StringUtils.isNotBlank(parts[p])) ifDim = ifDim.replaceAll(parts[p].trim(), parts[p].trim().replaceAll(" ", "_"));
					}
				}

				ifDim = calculate(ifDim);

				boolean ret = true;
				StringTokenizer st = new StringTokenizer(ifDim, " ");
				if (st.hasMoreTokens()) {
					String token1 = st.nextToken().trim();
					while (st.hasMoreTokens()) {
						if (token1.indexOf("=") >= 0 || token1.indexOf("<") >= 0
								|| token1.indexOf(">") >= 0)
							return false;
						if (st.hasMoreTokens()) {
							String maths = st.nextToken().trim();
							if (maths.indexOf("=") == -1 && maths.indexOf("~") == -1
									&& maths.indexOf("<") == -1
									&& maths.indexOf(">") == -1)
								return false;
							if (st.hasMoreTokens()) {
								String token2 = st.nextToken().trim();
								if (token2.indexOf("=") >= 0
										|| token2.indexOf("<") >= 0
										|| token2.indexOf(">") >= 0)
									return false;
								try {
									if (missing && "~".equals(maths.trim())) {
										if ("-missing-".equals(token1.trim())) token1 = "0";
										if ("-missing-".equals(token2.trim())) token2 = "0";
										maths = "=";
									}
									double d1 = Double.parseDouble(token1.trim());
									double d2 = Double.parseDouble(token2.trim());
									if ("=".equals(maths) || "==".equals(maths))
										ret = (d1 == d2);
									else if ("<".equals(maths))
										ret = (d1 < d2);
									else if ("<=".equals(maths))
										ret = (d1 <= d2);
									else if (">".equals(maths))
										ret = (d1 > d2);
									else if (">=".equals(maths) || "=>".equals(maths))
										ret = (d1 >= d2);
									else if ("!=".equals(maths))
										ret = (d1 != d2);
								} catch (Exception e) {
									logger.trace("error in {} of {}",
											"parseFreeRules", e.getMessage());
									try {
										if ("=".equals(maths) || "==".equals(maths))
											ret = token1.trim().equalsIgnoreCase(
													token2.trim());
										else if ("<".equals(maths))
											ret = token1.trim()
													.compareToIgnoreCase(
															token2.trim()) < 0;
										else if ("<=".equals(maths))
											ret = token1.trim()
													.compareToIgnoreCase(
															token2.trim()) <= 0;
										else if (">".equals(maths))
											ret = token1.trim()
													.compareToIgnoreCase(
															token2.trim()) > 0;
										else if (">=".equals(maths) || "=>".equals(maths))
											ret = token1.trim()
													.compareToIgnoreCase(
															token2.trim()) >= 0;
										else if ("!=".equals(maths))
											ret = !token1.trim().equalsIgnoreCase(
													token2.trim());
									} catch (Exception e2) {
										logger.trace("error in {} of {}",
												"parseFreeRules", e.getMessage());
										return false;
									}
								}
								token1 = token2;
							}
						}
						if (!ret)
							break;
					}
				}
				return ret;
			}
			return false;
		}

	public String calculate(String string) {
		if (string == null || string.length() == 0)
			return "";

		if ("-".equals(string))
			return string;
		
		string = subValues(makeNice(string));

		int count = 0;
		string = string.replaceAll("[\\+\\*\\/]", " $0 ").replaceAll("(\\d)(\\-)(\\d)","$1 $2 $3").trim();
		String result = string.replaceAll("  ", " ").replaceAll("  ", " ");
		// do this until there is no more maths or 20 attempts (so we don't get
		// into an endless loop due to bad maths
		while (result.length() != result.replaceAll("[\\+\\-\\*\\/]", "")
				.length() && ++count < 20) {
			// tokenize it, make sure all double spaces become single space
			StringTokenizer st = new StringTokenizer(result, " ");
			while (st.hasMoreTokens()) {
				String token1 = st.nextToken();
				if (token1.indexOf("=") >= 0 || token1.indexOf("<") >= 0
						|| token1.indexOf(">") >= 0)
					continue;
				if (token1.indexOf("+") >= 0 || (token1.indexOf("-")  >= 0 && !token1.startsWith("-"))
						|| token1.indexOf("*") >= 0 || token1.indexOf("/") >= 0)
					break;
				if (st.hasMoreTokens()) {
					String maths = st.nextToken();
					if (maths.indexOf("=") >= 0 || maths.indexOf("<") >= 0
							|| maths.indexOf(">") >= 0)
						continue;
					if (maths.indexOf("-") == -1 && maths.indexOf("+") == -1
							&& maths.indexOf("*") == -1
							&& maths.indexOf("/") == -1)
						continue;
					if (st.hasMoreTokens()) {
						String token2 = st.nextToken();
						if (token2.indexOf("=") >= 0
								|| token2.indexOf("<") >= 0
								|| token2.indexOf(">") >= 0)
							continue;
						double a = 0;
						try {
							double d1 = Double.parseDouble(token1);
							double d2 = Double.parseDouble(token2);
							if ("+".equals(maths))
								a = d1 + d2;
							else if ("-".equals(maths))
								a = d1 - d2;
							else if ("*".equals(maths))
								a = d1 * d2;
							else if ("/".equals(maths))
								a = d1 / d2;
							a = (double) Math.floor((double) a * 1000) / 1000;
						} catch (Exception e) {
							logger.trace("error in {} of {}", "calculate",
									e.getMessage());
						}
						StringBuilder sb = (new StringBuilder(token1))
								.append(" \\").append(maths).append(" ")
								.append(token2);
						if (!result.equals(result.replaceFirst(sb.toString(),
								String.valueOf(a)))) {
							result = result.replaceFirst(sb.toString(),
									String.valueOf(a));
						} else {
							sb = (new StringBuilder(token1)).append(" \\")
									.append(maths).append(" ").append(token2);
							if (!result.equals(result.replaceFirst(
									sb.toString(), String.valueOf(a)))) {
								result = result.replaceFirst(sb.toString(),
										String.valueOf(a));
							} else {
								sb = (new StringBuilder(token1)).append(maths)
										.append(token2);
								if (!result.equals(result.replaceFirst(
										sb.toString(), String.valueOf(a)))) {
									result = result.replaceFirst(sb.toString(),
											String.valueOf(a));
								} else {
									sb = (new StringBuilder(token1))
											.append("\\").append(maths)
											.append(token2);
									if (!result.equals(result.replaceFirst(
											sb.toString(), String.valueOf(a)))) {
										result = result.replaceFirst(
												sb.toString(),
												String.valueOf(a));
									}

								}

							}
						}
					}
				}
			}
		}

		return result;
	}
	
	public String calculateDate(String string) {
		if (string == null || string.length() == 0)
			return "";

		if ("-".equals(string))
			return string;
		
		string = subValues(makeNice(string));

		int count = 0;
		string = string.replaceAll("[\\+\\*]", " $0 ").replaceAll("(\\d)(\\-)(\\d)","$1 $2 $3").trim();
		String result = string.replaceAll("  ", " ").replaceAll("  ", " ");
		// do this until there is no more maths or 20 attempts (so we don't get
		// into an endless loop due to bad maths
		while (result.length() != result.replaceAll("[\\+\\-\\*\\/(MAX)(MIN)]", "")
				.length() && ++count < 20) {
			// tokenize it, make sure all double spaces become single space
			StringTokenizer st = new StringTokenizer(result, " ");
			while (st.hasMoreTokens()) {
				String token1 = st.nextToken();
				if (token1.indexOf("=") >= 0 || token1.indexOf("<") >= 0
						|| token1.indexOf(">") >= 0)
					continue;
				String token1a = convert(token1);
				if (token1a.indexOf("+") >= 0 || (token1.indexOf("-")  >= 0 && !token1.startsWith("-"))
						|| token1a.indexOf("*") >= 0 || token1a.indexOf("/") >= 0)
					break;
				if (st.hasMoreTokens()) {
					String maths = st.nextToken();
					if (maths.indexOf("=") >= 0 || maths.indexOf("<") >= 0
							|| maths.indexOf(">") >= 0)
						continue;
					if (maths.indexOf("-") == -1 && maths.indexOf("+") == -1
							&& maths.indexOf("*") == -1
							&& maths.indexOf("/") == -1 && maths.indexOf("MAX") == -1 && maths.indexOf("MIN") == -1 )
						continue;
					if (st.hasMoreTokens()) {
						String token2 = st.nextToken();
						if (token2.indexOf("=") >= 0
								|| token2.indexOf("<") >= 0
								|| token2.indexOf(">") >= 0)
							continue;
						String token2a = convert(token2);
						long a = 0;
						boolean isDiff = false;
						try {
							long d1 = Long.parseLong(token1a);
							long d2 = Long.parseLong(token2a);
							if ("+".equals(maths))
								a = d1 + d2;
							else if ("-".equals(maths))
								a = d1 - d2;
							else if ("*".equals(maths))
								a = d1 * d2;
							else if ("/".equals(maths))
								a = d1 / d2;
							else if ("MAX".equals(maths))
								a = Math.max(d1, d2);
							else if ("MIN".equals(maths))
								a = Math.min(d1, d2);
						} catch (Exception e) {
							logger.trace("error in {} of {}", "calculate",
									e.getMessage());
						}
						if (token1.length() - 2 == token1.replaceAll("\\/", "").length() && token2.length() - 2 == token2.replaceAll("\\/", "").length()) isDiff = true;
						if (token1.indexOf("/") == -1 && token2.indexOf("/") == -1) isDiff = true;
						if ("MAX".equals(maths) || "MIN".equals(maths)) isDiff = false;
						StringBuilder sb = (new StringBuilder(token1)).append(" ");
						if (!"MAX".equals(maths) && !"MIN".equals(maths)) sb.append("\\");
						sb.append(maths).append(" ").append(token2);
						if (!result.equals(result.replaceFirst(sb.toString(),
								dateOf(a)))) {
							result = result.replaceFirst(sb.toString(),
									isDiff? String.valueOf(a / DAY) : dateOf(a));
							if (isDiff && !result.matches("[0-9]{1,2}/[0-9]{2}/[0-9]{4}")) return calculate(result);
						} else {
							sb = (new StringBuilder(token1));
							if (!"MAX".equals(maths) && !"MIN".equals(maths)) sb.append("\\");
							sb.append(maths).append(token2);
							if (!result.equals(result.replaceFirst(
									sb.toString(), isDiff? String.valueOf(a / DAY) : dateOf(a)))) {
								result = result.replaceFirst(sb.toString(),
										isDiff? String.valueOf(a / DAY) : dateOf(a));
								if (isDiff && !result.matches("[0-9]{1,2}/[0-9]{2}/[0-9]{4}")) return calculate(result);
							} else {
								sb = (new StringBuilder(token1)).append(maths)
										.append(token2);
								if (!result.equals(result.replaceFirst(
										sb.toString(), isDiff? String.valueOf(a / DAY) : dateOf(a)))) {
									result = result.replaceFirst(sb.toString(),
											isDiff? String.valueOf(a / DAY) : dateOf(a));
									if (isDiff && !result.matches("[0-9]{1,2}/[0-9]{2}/[0-9]{4}")) return calculate(result);
								} else {
									sb = (new StringBuilder(token1));
									if (!"MAX".equals(maths) && !"MIN".equals(maths)) sb.append("\\");
									sb.append(maths).append(token2);
									if (!result.equals(result.replaceFirst(
											sb.toString(), isDiff? String.valueOf(a / DAY) : dateOf(a)))) {
										result = result.replaceFirst(
												sb.toString(),
												isDiff? String.valueOf(a / DAY) : dateOf(a));
										if (isDiff && !result.matches("[0-9]{1,2}/[0-9]{2}/[0-9]{4}")) return calculate(result);
									}

								}

							}
						}
					}
				}
			}
		}
		

		return result;
	}
	
	private String dateOf(Long r) {
		try {
			if (r < 0) return String.valueOf(r / DAY);
			Date d = new Date();
			d.setTime(r);
			return sdf.format(d);
		} catch (Exception e) {}

		return String.valueOf(r);
		
	}
	private String convert(String str) {
		try {
			return String.valueOf(sdf.parse(str).getTime());
		} catch (Exception e) {
			try {
				if (str.indexOf(".") >= 0) str = str.substring(0, str.indexOf("."));
				long i = Long.parseLong(str);
				i *= DAY;
				return String.valueOf(i);
			} catch (Exception e2) {}
		}
		
		return str;
	}
	
	public void put(String key, String value) {
		map.put(key, value);
	}
	
    public void putAll(Map<String, String> m) {
    	map.putAll(m);
    }

	public void LoadOpportunity(String opportunityID, String viewStageID) {
		// Child class can override, pdfRequest is passed so that if needed the template name can be overridden by the child class if needed
		//super.populateSpecificmap(request, map, listmap, property, pdfRequest);
		
		if (StringUtils.isBlank(opportunityID)) {
			throw  new RuntimeException("Opportunity was not found");
		}

		GenRow answers = new GenRow();
		
		GenRow opp = new GenRow();
		opp.setViewSpec("OpportunityView");
		opp.setParameter("OpportunityID", opportunityID);
		opp.doAction("selectfirst");

		answers.setViewSpec("answers/ContactAnswerSurveyView");
		RunwayUtil.convertRow2Map(opp, map, "Process.");
		RunwayUtil.convertRow2Map(opp, map, "Process_");
		
		if (opp.isSet("ContactID")) {
			GenRow primaryContact = getContact(opp.getString("ContactID"));
			RunwayUtil.convertRow2Map(primaryContact, map, "Process.Contact.");
			RunwayUtil.convertRow2Map(primaryContact, map, "Process_Contact_");
			loadLinkedmap(primaryContact, map, "Process.Contact.");
			loadLinkedmap(primaryContact, map, "Process_Contact_");
			
			GenRow link = new GenRow();
			link.setViewSpec("LinkeeContactListView");
			link.setParameter("LinkerID", opp.getString("ContactID"));
			link.setParameter("LinkedIn", "Y");
			link.doAction("search");
			link.getResults();

			while (link.getNext()) {
				if (link.getString("LinkeeID").length() > 0) {
					GenRow contact = getContact(link.getString("LinkeeID"));
					loadLinkedmap(contact, map, "Process.Contact.");
					loadLinkedmap(contact, map, "Process_Contact_");
					RunwayUtil.convertRow2Map(contact, map, "Process.Contact." + link.getString("LinkType").replaceAll("[^a-zA-Z0-9]", "") + ".");
					RunwayUtil.convertRow2Map(contact, map, "Process_Contact_" + link.getString("LinkType").replaceAll("[^a-zA-Z0-9]", "") + "_");
				}
			}
			link.close();
			
			answers.clear();
			answers.setParameter("ContactID", opp.getString("ContactID"));
			answers.doAction("search");

			answers.getResults();

			while (answers.getNext()) {
				RunwayUtil.convertRow2Map(answers, map, "Process.Contact." + answers.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + ".Question." + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + ".");
				RunwayUtil.convertRow2Map(answers, map, "Process_Contact_" + answers.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
			}
			
		}
		if (opp.isSet("RepUserID")) {
			RunwayUtil.convertRow2Map(RunwayUtil.getUser(null, opp.getString("RepUserID")), map, "Process.Rep.");
			RunwayUtil.convertRow2Map(RunwayUtil.getUser(null, opp.getString("RepUserID")), map, "Process_Rep_");
		}
		
		GenRow stage = new GenRow();
		stage.setViewSpec("OpportunityStageStageView");
		stage.setParameter("OpportunityID", opportunityID);
		
		stage.doAction("search");
		
		stage.getResults();
		
		answers.setViewSpec("answers/OpportunityAnswerView");
		
		
		while (stage.getNext()) {
			answers.clear();
			answers.setParameter("OpportunityID", opportunityID);
			answers.setParameter("ProcessStageID", stage.getString("ProcessStageID"));
			answers.doAction("search");

			answers.getResults();

			while (answers.getNext()) {
				RunwayUtil.convertRow2Map(answers, map, "Process." + stage.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + ".Question." + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + ".");
				RunwayUtil.convertRow2Map(answers, map, "Process_" + stage.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
			}
			
			RunwayUtil.convertRow2Map(stage, map, "Process_" + stage.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_");
			if (opp.getString("ProcessStageID").equals(stage.getString("ProcessStageID"))) {
				RunwayUtil.convertRow2Map(stage, map, "Process.CurrentStage.");
				RunwayUtil.convertRow2Map(stage, map, "Process_CurrentStage_");

				answers.getResults();

				while (answers.getNext()) {
					RunwayUtil.convertRow2Map(answers, map, "Process.CurrentStage.Question." + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + ".");
					RunwayUtil.convertRow2Map(answers, map, "Process_CurrentStage_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
				}
			}
		}
		
		answers.close();
		
		if (StringUtils.isNotBlank(viewStageID)) {
			stage.setParameter("ProcessStageID", viewStageID);
		
			stage.doAction("selectfirst");
			
			RunwayUtil.convertRow2Map(stage, map, "Process_CurrentViewStage_");
		}
		
		stage.close();
		
		GenRow link = new GenRow();
		link.setTableSpec("OpportunityContacts");
		link.setParameter("-select0", "ContactID");
		link.setParameter("-select1", "Role");
		link.setParameter("OpportunityID", opportunityID);
		
		link.doAction("search");
		
		link.getResults();
		
		answers.setViewSpec("answers/ContactAnswerSurveyView");
		while (link.getNext()) {
			if (link.isSet("ContactID")) {
				RunwayUtil.convertRow2Map(getContact(link.getString("ContactID")), map, "Process.Contact." + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + ".");
				RunwayUtil.convertRow2Map(getContact(link.getString("ContactID")), map, "Process_Contact_" + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + "_");

				answers.clear();
				answers.setParameter("ContactID", link.getString("ContactID"));
				answers.doAction("search");

				answers.getResults();

				while (answers.getNext()) {
					RunwayUtil.convertRow2Map(answers, map, "Process.Contact." + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + "_" + answers.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + ".Question." + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + ".");
					RunwayUtil.convertRow2Map(answers, map, "Process_Contact_" + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + "_" + answers.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
				}
			}
		}
		
		link.clear();
		link.setTableSpec("OpportunityCompanies");
		link.setParameter("-select0", "CompanyID");
		link.setParameter("-select1", "Role");
		link.setParameter("OpportunityID", opportunityID);
		
		link.doAction("search");
		
		link.getResults();
		
		answers.setViewSpec("answers/CompanyAnswerSurveyView");
		while (link.getNext()) {
			if (link.isSet("CompanyID")) {
				RunwayUtil.convertRow2Map(getCompany(link.getString("CompanyID")), map, "Process.Company." + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + ".");
				RunwayUtil.convertRow2Map(getCompany(link.getString("CompanyID")), map, "Process_Company_" + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + "_");

				answers.clear();
				answers.setParameter("CompanyID", link.getString("CompanyID"));
				answers.doAction("search");

				answers.getResults();

				while (answers.getNext()) {
					RunwayUtil.convertRow2Map(answers, map, "Process.Company." + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + "." + answers.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + ".Question." + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + ".");
					RunwayUtil.convertRow2Map(answers, map, "Process_Company_" + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + "_" + answers.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
				}
			}
		}
		
		link.close();

		getPayments(opportunityID);
	}
	
	private GenRow getContact(String contactID) {
		GenRow row = new GenRow();
		
		if (StringUtils.isBlank(contactID)) return row;
		
		row.setViewSpec("ContactView");
		row.setParameter("ContactID", contactID);
		
		row.doAction("selectfirst");
		
		return row;
	}
	
	private GenRow getCompany(String companyID) {
		GenRow row = new GenRow();
		
		if (StringUtils.isBlank(companyID)) return row;
		
		row.setViewSpec("CompanyView");
		row.setParameter("CompanyID", companyID);
		
		row.doAction("selectfirst");
		
		return row;
	}
	
	private void getPayments(String opportunityID) {
		if (StringUtils.isBlank(opportunityID)) return;
		
      	GenRow comms = new GenRow();
      	comms.setViewSpec("CommissionView");
      	/*
      	if (request.getParameter("CommissionName") != null && request.getParameter("CommissionName").length() > 0) {
      		payments.setColumn(commissionSortLong,"%" + request.getParameter("CommissionName") + "%");
      	}
      	if (request.getParameter("CommissionRole") != null && request.getParameter("CommissionRole").length() > 0) {
      		payments.setColumn("LinkType",request.getParameter("CommissionRole"));
      	}*/
      	comms.setColumn("OpportunityID", opportunityID);
		comms.setColumn("CommissionType","payments");
      	//payments.sortBy(commissionSort,0);
      	//payments.setParameter("-groupby0",commissionGroup);
		comms.doAction("search");
		comms.getResults();
		
		GenRow payments = new GenRow();
		payments.setViewSpec("CommissionPaymentsView");
		
      	while (comms.getNext()) {
      		double totalDue = 0, totalPaid = 0;
      		payments.clear();
      		payments.setParameter("CommissionID", comms.getString("CommissionID"));
      		payments.sortBy("SortOrder", 0);
      		payments.doAction("search");
      		
      		payments.getResults();
      		
      		while (payments.getNext()) {
      			RunwayUtil.convertRow2Map(payments, map, "Process.Payments." + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Payment." + payments.getString("Description").replaceAll("[^a-zA-Z0-9]", "") + ".");
      			RunwayUtil.convertRow2Map(payments, map, "Process_Payments_" + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Payment_" + payments.getString("Description").replaceAll("[^a-zA-Z0-9]", "") + "_");
				
				totalDue += payments.getDouble("DueAmount");
				totalPaid += payments.getDouble("PaidAmount");
      		}
      		
      		map.put("Process_Payments." + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + ".TotalPaid", "" + totalPaid);
      		map.put("Process_Payments_" + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_TotalPaid", "" + totalPaid);

      		map.put("Process_Payments." + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + ".TotalDue", "" + totalDue);
      		map.put("Process_Payments_" + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_TotalDue", "" + totalDue);

      		map.put("Process_Payments." + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + ".TotalOutStanding", "" + (totalDue - totalPaid));
      		map.put("Process_Payments_" + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_TotalOutStanding", "" + (totalDue - totalPaid));
      	}
      	comms.close();
      	payments.close();
	}
	
	private String[]		tokens = {"Title","FirstName","LastName","Salutation","Company"};
	protected void loadLinkedmap(GenRow linkedcontext, Map<String,String> map, String pretext)
	{
		if (!linkedcontext.isSet("ContactID")) return;
		
		for (int t = 0; t < tokens.length; ++t) {
			if (StringUtils.isBlank(map.get(pretext + "Linked" + tokens[t]))) {
				map.put(pretext + "Linked" + tokens[t], linkedcontext.getString(tokens[t]));
			} else {
				String value = (String) map.get(pretext + "Linked" + tokens[t]);
				value = value.replaceAll(" and ",", ") + " and " + linkedcontext.get(tokens[t]);
				if (value.endsWith(" and ")) value = value.replaceAll(" and ", "");
				map.put(pretext + "Linked" + tokens[t], value);
			}
		}

		if (StringUtils.isBlank(map.get(pretext + "LinkedNames"))) {
			map.put(pretext + "LinkedNames", (linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim());
		} else {
			String value = (String) map.get(pretext + "LinkedNames");
			value = value.replaceAll(" and ",", ") + " and " + (linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim();
			if (value.endsWith(" and ")) value = value.replaceAll(" and ", "");
			map.put(pretext + "LinkedNames", value);
		}

		if (StringUtils.isBlank(map.get(pretext + "LinkedFormalNames"))) {
			map.put(pretext + "LinkedFormalNames", (linkedcontext.get("Title") + " " + linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim());
		} else {
			String value = (String) map.get(pretext + "LinkedFormalNames");
			value = value.replaceAll(" and ",", ") + " and " + (linkedcontext.get("Title") + " " + linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim();
			if (value.endsWith(" and ")) value = value.replaceAll(" and ", "");
			map.put(pretext + "LinkedFormalNames", value);
		}
	}
	
	
}
