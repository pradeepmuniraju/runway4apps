package com.sok.framework;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
public class RedirectFilter implements Filter
{
	String targeturl = null;

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws ServletException, IOException
	{
		if(targeturl!=null)
		{
			((HttpServletResponse)response).sendRedirect(targeturl);
		}
	}
	
	public void init(FilterConfig config)throws ServletException
	{
		targeturl = config.getInitParameter("targetURL");
	}
	
	public void destroy()
	{
		
	}
}
