package com.sok.framework;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

public class DisplaySpec
{   
    private DisplaySpecPart defaultdisplay;
    private DisplaySpecPart specificdisplay;
    
    String name;
    
    public DisplaySpec(String name)   
    {   
        this.name = name; 
        defaultdisplay = new DisplaySpecPart(SpecManager.getFilePath(name, null, "displays/"), false);
        specificdisplay = new DisplaySpecPart(SpecManager.getFilePath(name, "specific/", "displays/"), true);

    }

    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getLabel()
    {
        String label = specificdisplay.getLabel();
        if(label.length()==0)
        {
            label = defaultdisplay.getLabel();
        }
        return(label);
    }   
    
    public String getName()
    {
        return(this.name);
    }      
    

    public int getSpecificItemIndex(String name)
    {
        String itemname = null;

        for(int i=0; i<specificdisplay.getItemsCount(); i++)
        {
            itemname = specificdisplay.getItemName(i);
            if(name.equals(itemname))
            {
                return(i);
            }
        }
        return(-1);
    }
    
    public int getDefaultItemIndex(String name)
    {
        String itemname = null;

        for(int i=0; i<defaultdisplay.getItemsCount(); i++)
        {
            itemname = defaultdisplay.getItemName(i);
            if(name.equals(itemname))
            {
                return(i);
            }
        }
        return(-1);
    }
    
    public int getSpecificRelatedDisplayIndex(String name) {
       String relatedDisplayName = null;

       for(int i=0; i<specificdisplay.getRelatedDisplayCount(); i++)
       {
           relatedDisplayName = specificdisplay.getRelatedDisplayName(i);
           if(name.equals(relatedDisplayName))
           {
               return(i);
           }
       }
       return(-1);
    }
    
    public int getDefaultRelatedDisplayIndex(String name)
    {
        String relatedDisplayName = null;

        for(int i=0; i<defaultdisplay.getRelatedDisplayCount(); i++)
        {
           relatedDisplayName = defaultdisplay.getRelatedDisplayName(i);
            if(name.equals(relatedDisplayName))
            {
                return(i);
            }
        }
        return(-1);
    }
    
    public boolean isLocalItemUsed(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.isItemUsed(index));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.isItemUsed(index));
        }
        return(true);
    }

    public boolean isItemUsed(String name)
    {
        int dotindex = name.indexOf('.');
        if(dotindex<0)
        {
            return(isLocalItemUsed(name));
        }
        else
        {
            String display = name.substring(0,dotindex);
            name = name.substring(dotindex+1,name.length());
            return(isIncludedItemUsed(display, name));
        }
    }
    
    public boolean isRelatedDisplayUsed(String relatedDisplayName)
    {
        int dotindex = relatedDisplayName.indexOf('.');
        if(dotindex<0)
        {
            return(isLocalRelatedDisplayUsed(relatedDisplayName));
        }
        else
        {
            String display = relatedDisplayName.substring(0,dotindex);
            relatedDisplayName = relatedDisplayName.substring(dotindex+1,relatedDisplayName.length());
            return(isIncludedRelatedDisplayUsed(display, relatedDisplayName));
        }
    }
    
    public boolean isLocalRelatedDisplayUsed(String relatedDisplayName)
    {
       int index = getSpecificRelatedDisplayIndex(relatedDisplayName);
       if(index>=0)
       {
           return(specificdisplay.isRelatedDisplayUsed(index));
       }
       index = getDefaultRelatedDisplayIndex(relatedDisplayName);
       if(index>=0)
       {
           return(defaultdisplay.isRelatedDisplayUsed(index));
       }
       return(true);
   }
    
    public String getLocalItemLabel(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.getItemLabel(index));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemLabel(index));
        }
        return(DisplaySpecPart._blank);
    }
    
    public String getLocalRelatedDisplayLabel(String relatedDisplayName) {
       int index = getSpecificRelatedDisplayIndex(relatedDisplayName);
       if(index>=0)
       {
           return(specificdisplay.getRelatedDisplayLabel(index));
       }
       index = getDefaultRelatedDisplayIndex(relatedDisplayName);
       if(index>=0)
       {
           return(defaultdisplay.getRelatedDisplayLabel(index));
       }
       return(DisplaySpecPart._blank);
    }
    
    public String getLocalItemFieldName(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.getItemFieldName(index));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemFieldName(index));
        }
        return(DisplaySpecPart._blank);
    }
    
    public String getLocalItemInputType(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.getItemInputType(index));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemInputType(index));
        }
        return(DisplaySpecPart._blank);
    }
    
    public String getLocalItemValuesListType(String name) {
       int index = getSpecificItemIndex(name);
       if(index>=0)
       {
           return(specificdisplay.getItemValuesListType(index));
       }
       index = getDefaultItemIndex(name);
       if(index>=0)
       {
           return(defaultdisplay.getItemValuesListType(index));
       }
       return(DisplaySpecPart._blank);
    }
    
    public String[] getLocalItemValues(String name) {
       int index = getSpecificItemIndex(name);
       if(index>=0)
       {
           return(specificdisplay.getItemValues(index));
       }
       index = getDefaultItemIndex(name);
       if(index>=0)
       {
           return(defaultdisplay.getItemValues(index));
       }
       return(null);
    }
    
    public String getLocalItemSource(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.getItemSource(index));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemSource(index));
        }
        return(DisplaySpecPart._blank);
    }    
    
    public String getLocalItemSize(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
        	String ss = specificdisplay.getItemSize(index); 
        	if(StringUtils.isNotBlank(ss)) {
        		return ss;
        	}
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemSize(index));
        }
        return(DisplaySpecPart._blank);
    }    
    
    public String getLocalItemValue(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
        	String ss = specificdisplay.getItemValue(index); 
        	if(StringUtils.isNotBlank(ss)) {
        		return ss;
        	}
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemValue(index));
        }
        return(DisplaySpecPart._blank);
    }    
    
    public String getLocalItemLink(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
        	String ss = specificdisplay.getItemLink(index); 
        	if(StringUtils.isNotBlank(ss)) {
        		return ss;
        	}
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemLink(index));
        }
        return(DisplaySpecPart._blank);
    }    
    
    public String getLocalItemDefaultValue(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.getItemDefaultValue(index));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemDefaultValue(index));
        }
        return(DisplaySpecPart._blank);
    }
    
    public String getLocalRelatedDisplayRelationship(String relatedDisplayName) {
       int index = getSpecificRelatedDisplayIndex(relatedDisplayName);
       if(index>=0)
       {
           return(specificdisplay.getRelatedDisplayRelationship(index));
       }
       index = getDefaultItemIndex(relatedDisplayName);
       if(index>=0)
       {
           return(defaultdisplay.getRelatedDisplayRelationship(index));
       }
       return(DisplaySpecPart._blank);
    }
    
    public String getItemLabel(String name)
    {
        int dotindex = name.indexOf('.');
        if(dotindex<0)
        {
            return(getLocalItemLabel(name));
        }
        else
        {
            String display = name.substring(0,dotindex);
            name = name.substring(dotindex+1,name.length());
            return(getIncludedItemLabel(display, name));
        }
    }    
    
    public String getItemFieldName(String name) 
    {
       int dotindex = name.indexOf('.');
       if(dotindex < 0) 
       {
          return(getLocalItemLabel(name));
       } 
       else 
       {
          String display = name.substring(0,dotindex);
          name = name.substring(dotindex+1,name.length());
          return(getIncludedItemFieldName(display, name));
       }
    }
    
    public String getItemInputType(String name) {
       int dotindex = name.indexOf('.');
       if(dotindex < 0) 
       {
          return(getLocalItemInputType(name));
       } 
       else 
       {
          String display = name.substring(0,dotindex);
          name = name.substring(dotindex+1,name.length());
          return(getIncludedItemInputType(display, name));
       }
    }
    
    public String getItemValuesListType(String name) {
       int dotindex = name.indexOf('.');
       if(dotindex < 0) 
       {
          return(getLocalItemValuesListType(name));
       } 
       else 
       {
          String display = name.substring(0,dotindex);
          name = name.substring(dotindex+1,name.length());
          return(getIncludedItemValuesListType(display, name));
       }
    }
    
    public String[] getItemValues(String name) {
       int dotindex = name.indexOf('.');
       if(dotindex < 0) 
       {
          return(getLocalItemValues(name));
       } 
       else 
       {
          String display = name.substring(0,dotindex);
          name = name.substring(dotindex+1,name.length());
          return(getIncludedItemValues(display, name));
       }
    }
    
    public String getItemSource(String name)
    {
        int dotindex = name.indexOf('.');
        if(dotindex<0)
        {
            return(getLocalItemSource(name));
        }
        else
        {
            String display = name.substring(0,dotindex);
            name = name.substring(dotindex+1,name.length());
            return(getIncludedItemSource(display, name));
        }
       
    }        
    
    public String getItemDefaultValue(String name)
    {
        int dotindex = name.indexOf('.');
        if(dotindex<0)
        {
            return(getLocalItemDefaultValue(name));
        }
        else
        {
            String display = name.substring(0,dotindex);
            name = name.substring(dotindex+1,name.length());
            return(getIncludedItemDefaultValue(display, name));
        }
       
    }
    
    public String getItemName(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.getItemName(index));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemName(index));
        }
        return(DisplaySpecPart._blank);
    }
  
    public boolean hasItemValidation(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.hasItemValidation(index));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.hasItemValidation(index));
        }      
        return(false);
    }
 
    public int getItemValidationCount(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.getItemValidationCount(index));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemValidationCount(index));
        }            
        return(0);
    }   
    
    public String getItemValidationMessage(String name, int j)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.getItemValidationMessage(index, j));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemValidationMessage(index, j));
        }                   
        return(DisplaySpecPart._blank);
    }     
    
    public String getItemValidationType(String name, int j)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.getItemValidationType(index, j));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemValidationType(index, j));
        }                   
        return(DisplaySpecPart._blank);
    }
    
    public String getRelatedDisplayLabel(String relatedDisplayName) 
    {
       int dotindex = relatedDisplayName.indexOf('.');
       if(dotindex < 0) 
       {
          return(getLocalRelatedDisplayLabel(relatedDisplayName));
       } 
       else 
       {
          String display = relatedDisplayName.substring(0,dotindex);
          relatedDisplayName = relatedDisplayName.substring(dotindex+1,relatedDisplayName.length());
          return(getIncludedRelatedDisplayLabel(display, relatedDisplayName));
       }
    }
    
    public String getRelatedDisplayRelationship(String relatedDisplayName) {
       int dotindex = relatedDisplayName.indexOf('.');
       if(dotindex < 0) 
       {
          return(getLocalRelatedDisplayRelationship(relatedDisplayName));
       } 
       else 
       {
          String display = relatedDisplayName.substring(0,dotindex);
          relatedDisplayName = relatedDisplayName.substring(dotindex+1,relatedDisplayName.length());
          return(getIncludedRelatedDisplayRelationship(display, relatedDisplayName));
       }
    }
    
/*    
    public String getItemDisplay(String name)
    {
        int index = getSpecificItemIndex(name);
        if(index>=0)
        {
            return(specificdisplay.getItemDisplay(index));
        }
        index = getDefaultItemIndex(name);
        if(index>=0)
        {
            return(defaultdisplay.getItemDisplay(index));
        }
        return(DisplaySpecPart._blank);
    }   
*/    
    public String getIncludedItemLabel(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getItemLabel(name));
    }
    
    public String getIncludedItemFieldName(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getItemFieldName(name));
    }
    
    public String getIncludedItemInputType(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getItemInputType(name));
    }
    
    public String getIncludedItemValuesListType(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getItemValuesListType(name));
    }
    
    public String[] getIncludedItemValues(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getItemValues(name));
    }
    
    public String getIncludedItemSource(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getItemSource(name));
    }
    
    public String getIncludedItemSize(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getItemSize(name));
    }     
    public String getIncludedItemValue(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getItemValue(name));
    }   
    public String getIncludedItemLink(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getItemLink(name));
    }   
    
    public String getIncludedItemDefaultValue(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getItemDefaultValue(name));
    }    

    public String getIncludedRelatedDisplayLabel(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.getRelatedDisplayLabel(name));
    }
    
    public String getIncludedRelatedDisplayRelationship(String display, String relatedDisplayName) 
    {
       DisplaySpec dspec = SpecManager.getDisplaySpec(display);
       return(dspec.getRelatedDisplayRelationship(relatedDisplayName));
    }
    
    public boolean isIncludedItemUsed(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.isItemUsed(name));
    }
    
    public boolean isIncludedRelatedDisplayUsed(String display, String name)
    {
        DisplaySpec dspec = SpecManager.getDisplaySpec(display);
        return(dspec.isRelatedDisplayUsed(name));
    }

    /**
     * 
     * @return string array of item names, excluding items with the 
     * <code>notused</code> attribute set to <code><b>true</b></code>.
     */
    public String[] getItemNames() {
       Set<String> itemNames = new LinkedHashSet<String>();
       this.addItemNames(this.specificdisplay, itemNames);
       this.addItemNames(this.defaultdisplay, itemNames);
       
       return itemNames.toArray(new String[0]);
    }
    
    /**
     * <p>Adds all item names in <code>displaySpecPart</code>, excluding items 
     * with the <code>notused</code> attribute set to <code><b>true</b></code>, 
     * to the <code>itemNames</code> set.</p>
     * 
     * @param displaySpecPart
     * @param itemNames
     */
    private void addItemNames(DisplaySpecPart displaySpecPart, Set<String> itemNames) {
       for(int i = 0; i < displaySpecPart.getItemsCount(); i++) {
          boolean isItemUsed = displaySpecPart.isItemUsed(i);
          if(isItemUsed) {
             String itemName = displaySpecPart.getItemName(i);
             itemNames.add(itemName);
          }
       }       
    }
    
    public String[] getRelatedDisplayNames() {
       Set<String> relatedDisplayNames = new HashSet<String>();
       this.addRelatedDisplayNames(this.specificdisplay, relatedDisplayNames);
       this.addRelatedDisplayNames(this.defaultdisplay, relatedDisplayNames);
       
       return relatedDisplayNames.toArray(new String[0]);
    }
    
    private void addRelatedDisplayNames(DisplaySpecPart displaySpecPart, Set<String> relatedDisplayNames) {
       for(int i = 0; i < displaySpecPart.getRelatedDisplayCount(); i++) {
          boolean isRelatedDisplayUsed = displaySpecPart.isRelatedDisplayUsed(i);
          if(isRelatedDisplayUsed) {
             String relatedDisplayName = displaySpecPart.getRelatedDisplayName(i);
             relatedDisplayNames.add(relatedDisplayName);
          }
       } 
    }

	public Set<String> getDefaultItemNames() {
		Set<String> itemNames = new LinkedHashSet<String>();
		this.addDefaultItemNames(this.defaultdisplay, itemNames, false);   
		this.addDefaultItemNames(this.specificdisplay, itemNames, true);
		return itemNames; 
	}
	
    private void addDefaultItemNames(DisplaySpecPart displaySpecPart, Set<String> itemNames, boolean isSpecific) {
        for(int i = 0; i < displaySpecPart.getItemsCount(); i++) {
           if(displaySpecPart.isItemUsed(i)) {
        	   if(!displaySpecPart.isItemOptional(i)) { 
        		   itemNames.add(displaySpecPart.getItemName(i));
        	   }
           } else if(isSpecific) {
        	   //if not used in specific spec then it should be removed.
        	   itemNames.remove(displaySpecPart.getItemName(i));
           }
        }       
     }

	public String getItemSize(String name)
    {
        int dotindex = name.indexOf('.');
        if(dotindex<0)
        {
            return(getLocalItemSize(name));
        }
        else
        {
            String display = name.substring(0,dotindex);
            name = name.substring(dotindex+1,name.length());
            return(getIncludedItemSize(display, name));
        }
    }   
	
	public String getItemValue(String name)
    {
        int dotindex = name.indexOf('.');
        if(dotindex<0)
        {
            return(getLocalItemValue(name));
        }
        else
        {
            String display = name.substring(0,dotindex);
            name = name.substring(dotindex+1,name.length());
            return(getIncludedItemValue(display, name));
        }
    } 
	
	public String getItemLink(String name)
    {
        int dotindex = name.indexOf('.');
        if(dotindex<0)
        {
            return(getLocalItemLink(name));
        }
        else
        {
            String display = name.substring(0,dotindex);
            name = name.substring(dotindex+1,name.length());
            return(getIncludedItemLink(display, name));
        }
    } 
}
