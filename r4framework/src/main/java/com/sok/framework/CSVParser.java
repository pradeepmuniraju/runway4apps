package com.sok.framework;

import java.io.*;
import java.util.*;

public class CSVParser {
   
   private File csvFile = null;
   private ArrayList rows = null;
   private StringBuffer errors = null;
   
   private int rowCount = 0;
   private int columnCount = 0;
   
   public CSVParser(File csvFile) {
      this.csvFile = csvFile;
      rows = new ArrayList();
      errors = new StringBuffer();
   }
   
   public void clear() {
      int rowCount = 0;
      int columnCount = 0;
      rows.clear();
      errors = new StringBuffer();
   }
   
   public int getRows() {
      return rowCount;
   }
   public int getColumns() {
      return columnCount;
   }
   
   public String getString(int row, int col) {
      Object obj = getData(row, col);
      if (obj == null) {
         return "";
      }
      else {
         return obj.toString();
      }
   }
   
   public Number getNumber(int row, int col) {
      Object obj = getData(row, col);
      try {
         return new Double(obj.toString());
      }
      catch (Exception e) {
         return new Integer(0);
      }
   }
   
   public Object getData(int row, int col) {
      if (row < rowCount) {
         ArrayList record = (ArrayList)rows.get(row);
         if (col < record.size()) {
            return record.get(col);
         }
         else {
            return null;
         }
      }
      else {
         return null;
      }
   }
   
   private void addRow(ArrayList row) {
      rows.add(row);
      rowCount++;
      if (row.size() > columnCount) {
         columnCount = row.size();
      }
   }
   
	public void parseFile() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(csvFile));
			 
			ArrayList record = null;
			String line = reader.readLine();
			
			while (line != null) {
			   record = new ArrayList();
			   while (line != null && !lineParse(record, line)) {
			      String newLine = reader.readLine();
			      line = line + "\r\n" + newLine;
   				record.clear();
			   }
			   addRow(record);
			   line = reader.readLine();
			}

		}
		catch (Exception e) { 
			errors.append(ActionBean.writeStackTraceToString(e));
		}
		finally {
			try { reader.close(); }
			catch (Exception ex) {}
		}
	}
   
   public boolean lineParse(ArrayList record, String line) {
		char[] chars = line.toCharArray();
		
		boolean inQuote = false;
		int startIndex = 0;
		for (int i=0; i < chars.length; i++) {
			if (chars[i] == '"') {
				inQuote = !inQuote;
			}
			else if (!inQuote && chars[i] == ',') {
				record.add(convertFragment(line.substring(startIndex,i)));
				startIndex = i+1;
			}
		}
		if (!inQuote) {
			if (startIndex == chars.length) {
				record.add("");
			}
			else if (startIndex < chars.length) {
				record.add(convertFragment(line.substring(startIndex)));
			}
		}
		return !inQuote;
	}
	
   public String convertFragment(String fragment) {
		if (fragment.startsWith("\"") && fragment.endsWith("\"")) {
			fragment = fragment.substring(1,fragment.length()-1);
		}
		fragment = StringUtil.replace(fragment, "\"\"","\"");
		return fragment;
	}
}