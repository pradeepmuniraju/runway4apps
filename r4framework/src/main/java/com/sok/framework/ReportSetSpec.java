package com.sok.framework;
import org.apache.commons.lang.StringUtils;
import org.dom4j.*;

public class ReportSetSpec extends XMLSpec
{ 	
	private Element[] variables;
	private Element[] reportUnits;
	private Element[] charts;
	private Element[] links;
	private Element[] reportStatements;
   private Element[] datetypes;	
	private String title;
	private boolean showCharts;
	private String moduletype;
	private String refinesearchurl;
	private String description;
   private String sort;
   
   private String customClass = null;
   private String customParameters = null;
   
	//tags 
	public static final String variable = "variable";
	public static final String reportUnit = "reportUnit";
	public static final String chart = "chart";
	public static final String link = "link";
	public static final String reportStatement = "reportStatement";
   public static final String datetypeset = "datetypeset";
   public static final String datetype = "datetype";
   
   public static final String custom = "custom";
   
	//parameters
	public static final String _description = "description";
	public static final String _columns = "columns";
	public static final String _name = "name";
	public static final String _title = "title";
	public static final String _moduletype = "moduletype";
	public static final String _refinesearchurl = "refinesearchurl";
	public static final String _type = "type";	
	public static final String _format = "format";
	public static final String _endtype = "endtype";
	public static final String _subset = "subset";
	public static final String _subsetdep = "subsetdep";
	public static final String _formattype = "formattype";
	public static final String _trigger = "trigger";
	public static final String _coltrigger = "coltrigger";
	public static final String _rowtrigger = "rowtrigger";
	public static final String _listlabel = "listlabel";	
	public static final String _listvalue = "listvalue";
	public static final String _listgroup = "listgroup";
	public static final String _tokenname = "tokenname";
	public static final String _minheight = "minheight";
	public static final String _minwidth = "minwidth";
	public static final String _colwidth = "colwidth";
	public static final String _rowheight = "rowheight";
	public static final String _reportset = "reportset";
	public static final String _default = "default";
	public static final String _start = "start";
	public static final String _end = "end";
	public static final String _restrict = "restrict";
   public static final String _value = "value";
   public static final String _field = "field";
   public static final String _search = "search";
   public static final String _sort = "sort";
   public static final String _showCharts = "show_charts";
   
   
	//parameter values
	public static final String statement = "statement";	
	public static final String dayrange = "dayrange";
	public static final String weekrange = "weekrange";
	public static final String monthrange = "monthrange";
	public static final String fortnightrange = "fortnightrange";
	public static final String quarterrange = "quarterrange";
	public static final String yearrange = "yearrange";
	public static final String finyearrange = "finyearrange";
	public static final String column = "column";
	public static final String row = "row";
	public static final String filter = "filter";
	public static final String _blank = "";
	
	public ReportSetSpec(String path)	
	{	
		super(path);
		Element root = super.document.getRootElement();
		if(root!=null)
		{
			variables = super.getElementArray(root,variable);
			reportUnits = super.getElementArray(root,reportUnit);
			reportStatements = super.getElementArray(root,reportStatement);
			charts = super.getElementArray(root,chart);
			links = super.getElementArray(root,link);
			Element daterangetypeset = super.getFirstElement(root, datetypeset);
			datetypes = super.getElementArray(daterangetypeset, datetype);
			title = root.attributeValue(_title);
         sort = root.attributeValue(_sort);
         showCharts = !"false".equals(root.attributeValue(_showCharts));
			description = super.getFirstElementText(root,_description);
			moduletype = root.attributeValue(_moduletype);
			refinesearchurl = root.attributeValue(_refinesearchurl);
			
			Element customElement = super.getFirstElement(root, custom);
			if(customElement != null) { 
				customClass = customElement.attributeValue("class");
				customParameters = customElement.attributeValue("parameters");
			}
		}
		else
		{
			System.out.print("Could not find root element in document - ");
			System.out.println(path);
		}
	}

	public String getCustomClass() {
		return customClass;
	}
	public String getCustomParameters() {
		return customParameters;
	}
	
	public String getDescription()
	{
		if(description!=null)
		{
			return(description);
		}
		return(_blank);
	}	
	
   public String getSort()
   {
      if(sort!=null)
      {
         return(sort);
      }
      return(_blank);
   }  	
   public boolean showCharts() {
	   return (true == showCharts);
   }
	
	public int getVariablesCount()
	{
		return(super.getCount(variables));
	}
	
	public int getChartsCount()
	{
		return(super.getCount(charts));
	}
	
	public int getLinksCount()
	{
		return(super.getCount(links));
	}

	public int getReportUnitsCount()
	{
		return(super.getCount(reportUnits));
	}

	public int getReportStatementsCount()
	{
		return(super.getCount(reportStatements));
	}

	public String getTitle()
	{
		if(title!=null)
		{
			return(title);
		}
		return(_blank);
	}
	
	public String getModuleType() 
	{ 
		return getModuleType(false);
	}

	public String getModuleType(boolean humanReadable)
	{
		if(moduletype!=null)
		{
			return humanReadable?StringUtils.capitalize(moduletype):moduletype;
		}
		return(_blank);
	}
	
	public String getRefineSearchURL()
	{
		if(refinesearchurl!=null)
		{
			return(refinesearchurl);
		}
		return(_blank);
	}
	
	public String getListTitle(int i)
	{
		return(super.getAttribute(variables,i,_title));
	}

	public String getListType(int i)
	{
		return(super.getAttribute(variables,i,_type));
	}

	public String getListName(int i)
	{
		return(super.getAttribute(variables,i,_name));
	}
	public String getListGroup(int i)
	{
		String val =super.getAttribute(variables,i,_listgroup);
		if(val != null) {
			return val;
		}
		return(ActionBean._blank);
	}
	
	public String getListValue(int i)
	{
		return(super.getAttribute(variables,i,_listvalue));
	}

	public String getListLabel(int i)
	{
		return(super.getAttribute(variables,i,_listlabel));
	}

	public String getListTokenName(int i)
	{
		return(super.getAttribute(variables,i,_tokenname));
	}

	public String getListTrigger(int i)
	{
		return(super.getAttribute(variables,i,_trigger));
	}

	public String getListSubset(int i)
	{
		if(i>-1)
		{
			return(super.getAttribute(variables,i,_subset));
		}
		return(ActionBean._blank);
	}	

	public String getListSubsetDep(int i)
	{
		return(super.getAttribute(variables,i,_subsetdep));
	}	
	
	public boolean isColTrigger(int i)
	{
		String r = super.getAttribute(variables,i,_restrict);
		if(r == null || r.equals(column))
		{
			return(true);
		}
		return(false);
	}
	public boolean isFilterTrigger(int i)
	{
		String r = super.getAttribute(variables,i,filter);
		if("true".equals(r))
		{
			return(true);
		}
		return(false);
	}	
	public boolean isDateTrigger(int i) { 
		String t = this.getListTitle(i).toLowerCase(); 
		if(t.indexOf("day")>-1 || t.indexOf("week")>-1 || t.indexOf("month")>-1 || t.indexOf("year")>-1 || t.indexOf("hour")>-1) 
			return true;
		return false;
	}

	public boolean isRowTrigger(int i)
	{
		String r = super.getAttribute(variables,i,_restrict);
		if(r == null || r.equals(row))
		{
			return(true);
		}
		return(false);
	}	

	public boolean isDefaultColTrigger(int i)
	{
		String r = super.getAttribute(variables,i,_default);
		if(r != null && r.equals(column))
		{
			return(true);
		}
		return(false);
	}	

	public boolean isDefaultFilterTrigger(int i)
	{
		String r = super.getAttribute(variables,i,_default);
		if(r != null && r.equals(column))
		{
			return(true);
		}
		return(false);
	}
	
	public boolean isDefaultRowTrigger(int i)
	{
		String r = super.getAttribute(variables,i,_default);
		if(r != null && r.equals(row))
		{
			return(true);
		}
		return(false);
	}		

	public boolean isSelectedRowTrigger(int i, String s)
	{
		if((s==null || s.length()==0) && isDefaultRowTrigger(i))
		{
			return(true);
		}
		else if(s!=null && s.equals(getListTrigger(i)))
		{
			return(true);
		}
		return(false);
	}	

	public boolean isSelectedColTrigger(int i, String s)
	{
		if((s==null || s.length()==0) && isDefaultColTrigger(i))
		{
			return(true);
		}
		else if(s!=null && s.equals(getListTrigger(i)))
		{
			return(true);
		}
		return(false);
	}	

	public boolean isSelectedFilterTrigger(int i, String s)
	{
		if((s==null || s.length()==0) && isDefaultFilterTrigger(i))
		{
			return(true);
		}
		else if(s!=null && s.equals(getListTrigger(i)))
		{
			return(true);
		}
		return(false);
	}	

	public boolean isListSubset(int i)
	{
		String r = super.getAttribute(variables,i,_subset);
		if(r != null && r.length()!=0)
		{
			return(true);
		}
		return(false);
	}
	
	public String getListDefault(int i)
	{
		return(super.getAttribute(variables,i,_default));
	}

	public String getListRangeStart(int i)
	{
		return(super.getAttribute(variables,i,_start));
	}

	public String getListRangeEnd(int i)
	{
		return(super.getAttribute(variables,i,_end));
	}

	public int getListIndexForTrigger(String trg)
	{
		return(super.getIndexForName(variables, _trigger, trg));
	}

	public int getDefaultListIndex(String defaulttype)
	{
		return(super.getIndexForName(variables, _default, defaulttype));
	}

	public String getListStatement(int i)
	{
		return(super.getText(variables,i));
	}	

	public String getReportUnitTitle(int i)
	{
		return(super.getAttribute(reportUnits,i,_title));
	}

	public String getReportUnitType(int i)
	{
		return(super.getAttribute(reportUnits,i,_type));
	}

	public String getReportUnitFormat(int i)
	{
		return(super.getAttribute(reportUnits,i,_format));
	}
	
	public String getReportUnitEndType(int i)
	{
		return(super.getAttribute(reportUnits,i,_endtype));
	}

	public String getReportUnitFormatType(int i)
	{
		return(super.getAttribute(reportUnits,i,_formattype));
	}

	public int getReportUnitColumns(int i)
	{
		String colsstring = super.getAttribute(reportUnits,i,_columns);
		int cols = -1;
		try{
			cols = Integer.parseInt(colsstring);
		}catch(Exception e){}
		return(cols);
	}

	public String getReportUnitStatement(int i)
	{
		return(super.getText(reportUnits,i));
	}

	public String getReportStatement(int i)
	{
		return(super.getText(reportStatements,i));
	}

	public String getLinkReportSet(int i)
	{
		return(super.getAttribute(links,i,_reportset));
	}

	public String getLinkTitle(int i)
	{
		return(super.getAttribute(links,i,_title));
	}

	public String getLinkTrigger(int i)
	{
		return(super.getAttribute(links,i,_trigger));
	}

	public String getLinkURL(int i)
	{
		return(super.getText(links,i));
	}

	public int getLinkIndexForTrigger(String coltrg, String rowtrig)
	{
		return(super.getIndexForName(links, _trigger, coltrg, rowtrig, XMLSpec.substring));
	}
	
	public String getChartMinHeight(int i)
	{
		return(super.getAttribute(charts,i,_minheight));
	}

	public String getChartMinWidth(int i)
	{
		return(super.getAttribute(charts,i,_minwidth));
	}

	public String getChartRowHeight(int i)
	{
		return(super.getAttribute(charts,i,_rowheight));
	}

	public String getChartColumnWidth(int i)
	{
		return(super.getAttribute(charts,i,_colwidth));
	}

	public String getChartType(int i)
	{
		return(super.getAttribute(charts,i,_type));
	}

	public String getChartTrigger(int i)
	{
		return(super.getAttribute(charts,i,_trigger));
	}
	
	public int getChartIndexForTrigger(String coltrg, String rowtrig)
	{
		return(super.getIndexForName(charts, _trigger, coltrg, rowtrig, XMLSpec.substring));
	}	
	
	public int getDateTypesCount()
	{
	   if(datetypes!=null)
	   {
	      return(datetypes.length);
	   }
	   return(0);
	}

   public int getDateTypeIndexForValue(String value)
   {
      int dtcount = getDateTypesCount();
      for(int i=0; i<dtcount; i++)
      {
         if(value.equals(getDateTypeValue(i)))
         {
            return(i);
         }
      }
      return(-1);
   }
	
   public String getDateTypeName(int i)
   {
      return(super.getAttribute(datetypes,i,_name));
   }
   
   public String getDateTypeValue(int i)
   {
      return(super.getAttribute(datetypes,i,_value));
   }   
   
   public String getDateTypeField(int i)
   {
      return(super.getAttribute(datetypes,i,_field));
   } 
   
   public String getDateTypeSearch(int i)
   {
      return(super.getAttribute(datetypes,i,_search));
   }      
}
