package com.sok.framework;

import java.io.File;

import com.sok.ErrorDebugger;
import com.sok.runway.offline.OfflineNotificationSender;
import com.sok.runway.offline.OfflineNotifier;
import com.sok.runway.offline.ThreadManager;

public abstract class DaemonProcess implements Runnable, OfflineNotifier {
   
   public static final String STATUS_INIT = "initialised";
   public static final String STATUS_SLEEPING = "sleeping";
   public static final String STATUS_PAUSED = "paused";
   public static final String STATUS_EXECUTING = "executing";
   public static final String STATUS_KILLED = "killed";
   
   private DaemonFactory daemonFactory = null;
   private Thread t = null;
   private boolean running = false;
   private boolean paused = false;
   private boolean killed = false;
   private String status = STATUS_INIT;
   protected ErrorDebugger d = null;
   
	public DaemonProcess(DaemonFactory daemonFactory) {
	   this.daemonFactory = daemonFactory;
	   this.d = ErrorDebugger.getErrorDebugger(getName());
	}
	
	public StringBuffer getErrors() {
	   return this.d.getErrors();
	}
	
	public void addError(String error) {
	   this.d.addError(error);
	}
	
	public void addError(Exception e) {
	   this.d.addError(e);
	}
	
	public abstract void run();
	
	public void interrupt() {
	   if (t != null && t.isAlive()) {
	      try {
	         t.interrupt();
	      }
	      catch (Exception e) { }
	   }
	}
	
	public void startDaemon() {
		
		
		
      t = new Thread(this,new StringBuilder(20).append(this.getName()).append("-DaemonProcessThread-").append(ThreadManager.getNextThreadId()).toString());
		try {
		   t.setPriority(Thread.MIN_PRIORITY);
		   t.setDaemon(true);
		}
		catch (Exception e) { }
		setRunning(true);
		ThreadManager.startThread(t);
		//t.start();
   }
	
	public boolean isRunning() {
	   return running;
	}
	
	public void setRunning(boolean running) {
	   this.running = running;
	   if(running)
	   {
	      status = STATUS_EXECUTING;
	   }
	   else
	   {
	      status = STATUS_SLEEPING;
	   }
	}
	
	public boolean isPaused() {
	   return paused;
	}
	
	public boolean isKilled() {
	   return killed;
	}
	
   public void pauseProcess() {
      paused = true;
      status = STATUS_PAUSED;
   }
   
   public void resumeProcess() {
      paused = false;
      status = STATUS_EXECUTING;
   }
   
   public void killProcess() {
      killProcess(true);
   }
   
   public void killProcess(boolean sendNotification) {
      paused = false;
      killed = true;
      status = STATUS_KILLED;
      
      if (sendNotification) {
   
            OfflineNotificationSender sender = new OfflineNotificationSender();
            sender.notify(this);
            
         /*try {
            
   			MailerBean mailer = new MailerBean();
   			
   		   java.util.Properties sysprops = InitServlet.getSystemParams();
   		   
            String defaultEmail = sysprops.getProperty("DefaultEmail");
   		   if (defaultEmail == null) {
   		      defaultEmail = "Runway<info@switched-on.com.au>";
   		   }
   			mailer.setFrom(defaultEmail);
   			mailer.setTo(defaultEmail);
   			mailer.setBcc("Runway Support <support@switched-on.com.au>");
   			        
   			mailer.setHost(daemonFactory.getSMTPHost());
   			mailer.setUsername(daemonFactory.getSMTPUsername());
   			mailer.setPassword(daemonFactory.getSMTPPassword());
            mailer.setPort(daemonFactory.getSMTPPort());
            
   			mailer.setSubject(getName() + " has been killed.");
            
            StringBuffer textBody = new StringBuffer();
            textBody.append(daemonFactory.getServletContextName());
      		textBody.append(" Runway AutoPilot has been killed\r\n");
      		textBody.append(" Cause is below:\r\n\r\n");
      		textBody.append(getErrors());
   			
   			mailer.setTextbody(textBody.toString());
   			mailer.getResponse();
   		}
   		catch(Exception e) {
   			System.err.println(ActionBean.writeStackTraceToString(e));
   			System.err.println(getErrors());			
   		}
   		*/
      }
      this.daemonFactory = null;
      this.d = null;
   }
   
   public String getStatus() {
      return status;
   }
   
   public void setStatus(String status) {
      this.status = status;
   }
   
   public void setStatus(String status, int current, int total) {
      this.status = status + " " + current + " of " + total;
   }
   
   public abstract String getName();
   
   /* OfflineNotifier Interface */
   
   public String getStatusMailRecipient() {
      return "Servers <servers@switched-on.com.au>";
   }
   
   public String getStatusMailBody() {
      
      StringBuffer textBody = new StringBuffer();
      textBody.append(daemonFactory.getServletContextName());
		textBody.append(" Runway AutoPilot has been killed\r\n");
		textBody.append(" Cause is below:\r\n\r\n");
		textBody.append(getErrors());
		
		return textBody.toString();
	}
   
   public String getStatusMailHtmlBody() {
	   return null;
   }
   
   public String getStatusMailSubject() {
      return getName() + " has been killed.";
   }
   
   public File getStatusAttachment() {
      return null;
   }
   
   public int getProcessSize() {
      return -1;
   }
   
   public int getProcessedCount() {
      return -1;
   }
   
   public int getErrorCount() {
      return -1;
   }
}