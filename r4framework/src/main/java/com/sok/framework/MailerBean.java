package com.sok.framework;

import java.io.*;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import com.sun.mail.smtp.SMTPMessage;

import de.agitos.dkim.DKIMSigner;
import de.agitos.dkim.SMTPDKIMMessage;

public class MailerBean
{

	String id = null;
	String to = null;
	String from = null;
	String sender = null;
	String cc = null;
	String bcc = null;
	String replyto = null;
	String subject = null;
	String htmlbody = null;
	String textbody = null;
	Collection attachedFiles = null;

	String host = null;
	String username = null;
	String password = null;
	String port = null;

	String messageid = null;	

	String protocol = "mail.smtp.host";

	MimeBodyPart attachment;
	Multipart setmulti = null;

	public static String utf8 = "utf-8";
	public static String iso = "iso-8859-1";  
	public static String windows = "windows-1252";  

	String charset = windows;

	protected boolean singlesend = true;

	protected Transport transport = null;
	protected Session session = null;
	protected Exception lastException = null; 

	public static String STATUS_SENT = "Message Sent";

	private String urlHome = null;
	private boolean simulateSend; 
	
	private String fullMessage = "";
	
	private boolean tracked = false;
	
	/*
            smtpMessage.setReturnOption(SMTPMessage.RETURN_FULL);
            smtpMessage.setNotifyOptions(SMTPMessage.NOTIFY_SUCCESS);
	 */
	
	public MailerBean()
	{
		simulateSend = "true".equals(InitServlet.getSystemParam("SimulateEmailSending"));
	}

	public Exception getLastException() { 
		return lastException; 
	}

	public void setServletContext(ServletContext context)
	{
		setHost(context.getInitParameter("SMTPHost"));
		setUsername(context.getInitParameter("SMTPUsername"));
		setPassword(context.getInitParameter("SMTPPassword"));
		setPort(context.getInitParameter("SMTPPort"));
		setSender(context.getInitParameter("SMTPFrom"));
		urlHome = context.getInitParameter("URLHome");

	}

	public void setTo(String s)
	{
		to = s;
	}

	public String getTo()
	{
		return(to);
	}	

	public void setSender(String s)
	{
		if (s != null) sender = s;
	}

	public String getSender() {
		return sender;
	}

	public void setFrom(String s)
	{
		from = s;
	}

	public String getFrom()
	{
		return(from);
	}	

	public void setReplyTo(String s)
	{
		replyto = s;
	}

	public String getReplyTo()
	{
		return(replyto);
	}  	

	public void setBcc(String s)
	{
		bcc = s;
	}

	public String getBcc()
	{
		return(bcc);
	}  	

	public void setCc(String s)
	{
		cc = s;
	}

	public String getCc()
	{
		return(cc);
	}	

	public void setSubject(String s)
	{
		subject = s;
	}

	public String getSubject()
	{
		return(subject);
	}	

	public void setHtmlbody(String s)
	{
		htmlbody = s;
	}

	public String getHtmlbody()
	{
		return(htmlbody);
	}	

	public void setTextbody(String s)
	{
		textbody = s;
	}

	public String getTextbody()
	{
		return(textbody);
	}	

	public void setHost(String s)
	{
		host = s;
	}

	public void setPort(String s)
	{
		port = s;
	}    

	public void setUsername(String s)
	{
		username = s;
	}

	public void setPassword(String s)
	{
		password = s;
	}



	//overrides MessageID in JavaMail J2EE1.4
	//has no effect in J2EE1.3	
	public void setMessageID(String s)
	{
		messageid = s;
	}	

	public void setNoteID(String id) {
		this.id = id;
	}

	public void setCharSet(String s)
	{
		charset = s;
	}

	public void setAttachments(Collection list)
	{
		if(list != null && list.size() > 0)
		{
			attachedFiles = list;
		}
	}

	public void setMultipart(Multipart message)
	{
		this.setmulti = message;
	}

	public String getResponse()
	{
		lastException = null; 

		if(host == null || host.length() == 0)
		{
			cleanup();
			return ("No Host specified");
		}
		if(to == null || to.length() == 0)
		{
			cleanup();
			return ("Empty email address for To");
		}
		if(from == null || from.length() == 0)
		{
			cleanup();
			return ("Empty email address From");
		}

		if(simulateSend) {
			return "Simulated Sent";
		}
		initSession();
		SMTPMessage msg = null;
		if ("true".equals(InitServlet.getSystemParam("UseDKIMemailSigning"))) {
			try {
				String path = InitServlet.getRealPath(InitServlet.getSystemParam("DKIMKey")==null?"":InitServlet.getSystemParam("DKIMKey"));
				String domain = InitServlet.getSystemParam("DKIMDomain");
				if (StringUtils.isNotBlank(domain)) 
					domain = "runway.com.au, " + domain;
				else
					domain = "runway.com.au";
				
				// the identity must mast the domain
				String ident = from;
				//if (StringUtils.isBlank(ident)) ident = from;
				if (ident.indexOf("<") >= 0) {
					ident = ident.substring(ident.indexOf("<") + 1);
					if (ident.indexOf(">") >= 0) ident = ident.substring(0,ident.indexOf(">"));
				} else if (ident.indexOf("[") >= 0) {
					ident = ident.substring(ident.indexOf("<") + 1);
					if (ident.indexOf("]") >= 0) ident = ident.substring(0,ident.indexOf("]"));
				}
				String sDomain = "";
				if (ident.indexOf("@") >= 0) sDomain = ident.substring(ident.indexOf("@") + 1);
				
				if (domain.indexOf(sDomain) >= 0 && StringUtils.isNotBlank(path) && StringUtils.isNotBlank(InitServlet.getSystemParam("DKIMSelector"))) {
					DKIMSigner dkimSigner = new DKIMSigner(sDomain, InitServlet.getSystemParam("DKIMSelector"), path);
					dkimSigner.setIdentity(ident);
					
					msg = new RunwayDKIMMessage(session, dkimSigner);
				} else {
					msg = new RunwayMessage(session);
				}
			} catch (Exception e) {
				msg = new RunwayMessage(session);
				e.printStackTrace();
			}
		} else {
			msg = new RunwayMessage(session);
		}
		
		// if the message is being returned to us, then we would like to get delivery notification
		if (StringUtils.isNotBlank(sender) && "postmaster@postmaster.runway.com.au".equals(sender)) {
			tracked = true;
			msg.setReturnOption(SMTPMessage.RETURN_FULL);
			msg.setNotifyOptions(SMTPMessage.NOTIFY_DELAY|SMTPMessage.NOTIFY_FAILURE|SMTPMessage.NOTIFY_SUCCESS);
			try {
				msg.setHeader("Return-Receipt-To", "<" + sender + ">");
				msg.setHeader("Read-Receipt-To", "<" + sender + ">");
				msg.setHeader("X-Envelope-From", "<" + sender + ">");
				msg.setHeader("Envelope-From", "<" + sender + ">");
				msg.setHeader("Return-Path", sender);
				msg.setHeader("Errors-To", "<" + sender + ">");
				//msg.setHeader("Sender", "<" + sender + ">");
				
				msg.setEnvelopeFrom(sender);
				
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
		
		String error = initMessage(msg);
		
		if(error!=null)
		{
			return(error);
		}
		try
		{
			connectTransport();
			transport.sendMessage(msg, msg.getAllRecipients());
		}
		catch(Exception e)
		{
			lastException = e;

			String emsg = e.getMessage();
			if(emsg == null || emsg.length()==0)
			{
				emsg = "Error " + e.getClass().getCanonicalName();
			}
			return (emsg);
		}
		finally
		{
			if(singlesend)
			{
				close();
			}
		}
		try {
			OutputStream os = new ByteArrayOutputStream();
			
			msg.writeTo(os);
			fullMessage = new String(os.toString());
			
			os.close();
		} catch (MessagingException e1) {
		} catch (IOException e1) {
		}
		
		Properties props = System.getProperties();
		props.remove("mail.smtp.from");
		return (STATUS_SENT);
	}

	protected String initMessage(Message msg)
	{
		messageid = id;
		if (msg instanceof RunwayMessage) {
			((RunwayMessage) msg).setMessageID(messageid);
		} else if (msg instanceof RunwayDKIMMessage) {
			((RunwayDKIMMessage) msg).setMessageID(messageid);
		}
		try {
			// new message header to allow tracking of 
			msg.setHeader("X-Runway", new StringBuilder().append(urlHome).append("/actions/bouncemanager.jsp?NoteID=").append(messageid).toString());
		} catch (MessagingException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		}

		try
		{
			InternetAddress[] addresses = InternetAddress.parse(to, false);
			msg.setRecipients(Message.RecipientType.TO, addresses);
		}
		catch(Exception e)
		{
			lastException = e; 
			return ("Invalid email address");
		}

		try
		{  
			msg.setFrom(new InternetAddress(from));
			// set the sender to the deafult emil addrerss
			//if (sender != null && sender.length() > 0) {
			//	 msg.setSender(new InternetAddress(sender));
			//} else {
			//	 msg.setSender(new InternetAddress(from));
			// }
		}
		catch(Exception e)
		{
			lastException = e; 
			return ("Invalid from email address");
		}

		if(cc != null && cc.length() != 0)
		{
			try
			{
				msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc, false));
			}
			catch(Exception e)
			{
				lastException = e; 
				return ("Invalid CC email address");
			}  
		}
		if(bcc != null && bcc.length() != 0)
		{   
			try
			{
				msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc, false));
			}
			catch(Exception e)
			{
				lastException = e; 
				return ("Invalid BCC email address");
			}        
		}
		if(StringUtils.isNotBlank(replyto))
		{
			try
			{   
				msg.setReplyTo(InternetAddress.parse(replyto, false));
			}
			catch(Exception e)
			{
				lastException = e; 
				return ("Invalid replyto email address");
			}  
		} else {
			try
			{  
				msg.setReplyTo(InternetAddress.parse(from, false));
			}
			catch(Exception e)
			{
				lastException = e; 
				return ("Invalid from email address");
			}
		}

		try
		{      
			msg.setSubject(subject);
			msg.setHeader("X-Mailer", "JavaMail API");
		}
		catch(Exception e)
		{
			lastException = e; 
			String emsg = e.getMessage();
			if(emsg == null || emsg.length()==0)
			{
				emsg = "Error " + e.getClass().getCanonicalName();
			}
			return (emsg);
		}       
		Multipart multi = null;
		try
		{

			if(textbody != null && htmlbody != null || attachedFiles != null || attachment != null || setmulti!=null)
			{
				if(setmulti != null)
				{
					multi = setmulti;
				}
				else
				{
					multi = new MimeMultipart("mixed");
				}            

				if(textbody != null && htmlbody != null)
				{
					Multipart alt = new MimeMultipart("alternative");

					MimeBodyPart plainPart = new MimeBodyPart();
					plainPart.setText(StringUtil.removeHtml(RunwayUtil.encodeWindows1252(htmlbody)), charset);
					alt.addBodyPart(plainPart);

					MimeBodyPart htmlPart = new MimeBodyPart();
					htmlPart.setContent(RunwayUtil.encodeWindows1252(htmlbody), "text/html; charset="+ charset);
					alt.addBodyPart(htmlPart);

					MimeBodyPart contentbody = new MimeBodyPart();
					contentbody.setContent(alt);
					multi.addBodyPart(contentbody);

				}
				else if(textbody != null)
				{
					MimeBodyPart plainPart = new MimeBodyPart();
					plainPart.setText(textbody, charset);
					multi.addBodyPart(plainPart);

				}
				else if(htmlbody != null)
				{
					/*
					MimeBodyPart htmlPart = new MimeBodyPart();
					htmlPart.setContent(htmlbody, "text/html; charset="+ charset);
					multi.addBodyPart(htmlPart);
					*/
					// we should add both HTML and Text
					Multipart alt = new MimeMultipart("alternative");

					MimeBodyPart plainPart = new MimeBodyPart();
					plainPart.setText(StringUtil.removeHtml(RunwayUtil.encodeWindows1252(htmlbody)), charset);
					alt.addBodyPart(plainPart);

					MimeBodyPart htmlPart = new MimeBodyPart();
					htmlPart.setContent(RunwayUtil.encodeWindows1252(htmlbody), "text/html; charset="+ charset);
					alt.addBodyPart(htmlPart);

					MimeBodyPart contentbody = new MimeBodyPart();
					contentbody.setContent(alt);
					multi.addBodyPart(contentbody);
					
				}

				if(attachedFiles != null)
				{
					try
					{
						addAttachments(multi);
					}
					catch(Exception e2)
					{
						return (e2.toString());
					}
				}

				if(attachment != null)
				{
					multi.addBodyPart(attachment);
				}

				msg.setContent(multi);

			}
			else if(htmlbody != null)
			{
				msg.setContent(htmlbody, "text/html");
			}
			else if(textbody != null)
			{
				msg.setText(textbody);
			}
			else
			{
				cleanup();
				return ("No Message Body specified");
			}
			msg.setSentDate(new Date());
			msg.saveChanges();
		}
		catch(Exception e)
		{
			lastException = e; 

			String emsg = e.getMessage();
			if(emsg == null || emsg.length()==0)
			{
				emsg = "Error " + e.getClass().getCanonicalName();
			}
			return (emsg);
		}	   
		return(null);
	}

	public String getMessageSource()
	{
		RunwayMessage msg = new RunwayMessage(session);
		initMessage(msg);
		OutputStream os = new ByteArrayOutputStream();
		try
		{
			msg.writeTo(os);
		}
		catch(IOException e)
		{

		}
		catch(MessagingException e)
		{

		}
		return(os.toString());    
	}

	public boolean isConnected()
	{
		return(transport!=null && transport.isConnected());
	}

	protected void initSession()
	{
		if(session == null)
		{
			Properties props = System.getProperties();	   
			props.put(protocol, host);
			if(username != null && username.length()!=0 && password!=null && password.length()!=0)
			{
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.user", username);
				props.put("mail.smtp.password", password);     
			} else {
				props.remove("mail.smtp.auth");
				props.remove("mail.smtp.user");
				props.remove("mail.smtp.password");     
			}
			if ("true".equals(InitServlet.getSystemParam("UseDKIMemailSigning"))) {
				if (StringUtils.isNotBlank(InitServlet.getSystemParam("DKIMDomain"))) props.put("mail.smtp.dkim.signingdomain", InitServlet.getSystemParam("DKIMDomain"));
				if (StringUtils.isNotBlank(InitServlet.getSystemParam("DKIMSelector"))) props.put("mail.smtp.dkim.selector", InitServlet.getSystemParam("DKIMSelector"));
				if (StringUtils.isNotBlank(InitServlet.getSystemParam("DKIMKey"))) props.put("mail.smtp.dkim.privatekey", InitServlet.getSystemParam("DKIMKey"));
			}
			if(port != null && port.length()!=0)
			{
				props.put("mail.smtp.port", port );
			} else {
				props.put("mail.smtp.port", 25 );
			}
			if(sender != null && sender.length()!=0)
			{
				String smtpFrom = sender;
				// remove everything but the address
				while (smtpFrom.indexOf(" ") >= 0 && smtpFrom.indexOf("@",smtpFrom.indexOf(" ")) >= 0) {
					smtpFrom = smtpFrom.substring(smtpFrom.indexOf(" ") + 1);
				}
				if (smtpFrom.indexOf("<") >= 0) smtpFrom = smtpFrom.substring(smtpFrom.indexOf("<") + 1);
				if (smtpFrom.indexOf("[") >= 0) smtpFrom = smtpFrom.substring(smtpFrom.indexOf("[") + 1);
				if (smtpFrom.indexOf(">") >= 0) smtpFrom = smtpFrom.substring(0,smtpFrom.indexOf(">"));
				if (smtpFrom.indexOf("]") >= 0) smtpFrom = smtpFrom.substring(0,smtpFrom.indexOf("]"));
				props.put("mail.smtp.from", smtpFrom);
			} else {
				props.remove("mail.smtp.from");
			}

			session = Session.getDefaultInstance(props, null);
			session.setDebug(false);	   
			if(username == null)
			{
				username = "";
			}
			if(password == null)
			{
				password = "";
			}
		}
	}

	protected void connectTransport() throws MessagingException
	{
		if(transport == null)
		{
			transport = session.getTransport("smtp");
			transport.connect(host, username, password);
		}
		else if (!transport.isConnected()) {
			transport.connect(host, username, password);
		}
	}

	public void connect() throws MessagingException
	{
		this.singlesend = false;
		initSession();
		connectTransport();
	}

	public void close()
	{
		try
		{
			if(transport != null)
			{
				transport.close();
				transport = null;
				session = null;
			}
		}
		catch(Exception te)
		{
		}
	}

	public void setAttachment(File attachFile)
	{
		try{
			attachment = new MimeBodyPart();
			FileDataSource fds = new FileDataSource(attachFile);
			attachment.setDataHandler(new DataHandler(fds));
			attachment.setFileName(attachFile.getName());			
		}catch(Exception e){}
	}	

	public void setAttachment(Multipart part)
	{
		try{
			attachment = new MimeBodyPart();
			attachment.setContent(part);
		}catch(Exception e){}
	}

	private void addAttachments(Multipart content) throws javax.mail.MessagingException
	{
		if(attachedFiles != null)
		{
			Object attachment;
			File attachFile = null;
			MimeBodyPart filePart;
			FileDataSource fds;

			Iterator iter = attachedFiles.iterator();

			while(iter.hasNext())
			{
				attachment = iter.next();
				if(attachment instanceof String)
				{
					attachFile = new File((String) attachment);
				}
				else if(attachment instanceof File)
				{
					attachFile = (File) attachment;
				}
				else
				{
					attachFile = null;
				}
				if(attachFile != null)
				{
					filePart = new MimeBodyPart();
					fds = new FileDataSource(attachFile);
					filePart.setDataHandler(new DataHandler(fds));
					filePart.setFileName(attachFile.getName());
					content.addBodyPart(filePart);
				}
			}
		}
	}

	public void setClear(String type)
	{
		cleanup();
	}

	private void cleanup()
	{
		to = null;
		from = null;
		cc = null;
		bcc = null;
		replyto = null;
		subject = null;
		htmlbody = null;
		textbody = null;
		attachedFiles = null;
		attachment = null;
		setmulti = null;
	}

	public static String removerAddressFromList(String address, String addressList)
	{
		try
		{
			InternetAddress[] senderAddresses = InternetAddress.parse(address, false);
			if(senderAddresses.length != 1)
			{
				return addressList;
			}
			else
			{
				InternetAddress[] addresses = InternetAddress.parse(addressList, false);
				if(addresses.length == 0)
				{
					return addressList;
				}
				else
				{
					ArrayList newAddresses = new ArrayList();
					for(int i = 0; i < addresses.length; i++)
					{

						if(!addresses[i].getAddress().equals(senderAddresses[0].getAddress()))
						{
							newAddresses.add(addresses[i]);
						}
					}
					if(newAddresses.size() == 0)
					{
						return "";
					}
					else
					{
						addresses = new InternetAddress[newAddresses.size()];
						for(int i = 0; i < addresses.length; i++)
						{
							addresses[i] = (InternetAddress) newAddresses.get(i);
						}
						return InternetAddress.toString(addresses);
					}
				}
			}
		}
		catch(Exception e)
		{
			return (addressList);
		}
	}

	public class RunwayMessage extends SMTPMessage
	{
		protected String messageID;

		public RunwayMessage(Session s)
		{
			super(s);
		}

		public void setMessageID(String messageID)
		{
			this.messageID = new StringBuffer()
			.append("<")
			.append(messageID)
			//.append(".")
			//.append(System.currentTimeMillis())
			.append(".info@runway.net.au>")
			.toString();
		}


		//overrides updateMessageID() in MimeMessage J2EE1.4
		//has no effect in J2EE1.3
		protected void updateMessageID() throws MessagingException
		{
			if(messageID!=null && messageID.length()!=0)
			{
				if (id == null || id.length() == 0) setMessageID(KeyMaker.generate());
			}

			setHeader("Message-ID", messageID);
		}

	}


	public class RunwayDKIMMessage extends SMTPDKIMMessage
	{
		protected String messageID;

		public RunwayDKIMMessage(Session s, DKIMSigner d)
		{
			super(s, d);
		}

		public void setMessageID(String messageID)
		{
			this.messageID = new StringBuffer()
			.append("<")
			.append(messageID)
			//.append(".")
			//.append(System.currentTimeMillis())
			.append(".info@runway.net.au>")
			.toString();
		}


		//overrides updateMessageID() in MimeMessage J2EE1.4
		//has no effect in J2EE1.3
		protected void updateMessageID() throws MessagingException
		{
			if(messageID!=null && messageID.length()!=0)
			{
				if (id == null || id.length() == 0) setMessageID(KeyMaker.generate());
			}

			setHeader("Message-ID", messageID);
		}

	}


	public static void main(String[] args) 
	{ 

		MailerBean webvision = new MailerBean();
		webvision.setHost("10.144.1.7");
		webvision.setFrom("update@mailguard.com.au");
		webvision.setTo("shinytoaster@gmail.com"); 
		webvision.setSubject("test email1"); 
		webvision.setTextbody("text body"); 
		webvision.setHtmlbody("<html><head></head><body><h1>HTMLbody</h1></body></html>");
		System.out.println(webvision.toString());

	}

	public void setURLHome(String s) {
		urlHome = s;
	}

	public String getFullMessage() {
		return fullMessage;
	}

	public void setFullMessage(String fullMessage) {
		this.fullMessage = fullMessage;
	}

	public boolean isTracked() {
		return tracked;
	}	  

}
