package com.sok.framework;

import java.sql.*;
import java.util.*;

import javax.servlet.http.*;
import javax.servlet.*;
import org.apache.commons.lang.StringUtils;
import java.net.*; 

import com.sok.framework.sql.*;

public class RowBean extends HashMap implements TableData
{   
	ViewSpec vspec = null;
	TableSpec tspec = null;
	RelationSpec rspec = null;
	ActionSpec aspec = null;
	
	public static final String viewspec = "-viewSpec";
	public static final String tablespec = "-tableSpec";
	public static final String actionspec = "-actionSpec";	
	public static final String jndiname = "-sqlJndiName";
	public static final String sort_prefix = "-sort";
	public static final String order_prefix = "-order";
	public static final String limit_results = "-top";

	String searchstmt = null;
	String searchcriteria = null;
	StringBuffer errors = new StringBuffer();

	Relation relatedfields = new Relation("root",null);
	Map searchgroups = null;

	Locale thislocale = null;
	TimeZone thistimezone = null;
	TimeZone usertimezone = null;
	String convert = null;
	String inputdateformat = null;

	boolean closeconnection = true;
	Connection con = null;
	String[] sortfields = null;
	String[] sortorder = null;

	int top = 0;

	SqlDatabase database = null;

	public void setDatabase(String databasename)
	{
		this.database = AbstractDatabase.getDatabase(databasename);
	}		
	
	public void setDatabase(SqlDatabase database)
	{
		this.database = database;
	}	
	
	public SqlDatabase getDatabase()
	{
		if(database==null)
		{
			database = ActionBean.getDatabase();
		}
		return(database);
	}	
	
	public void setTop(int topcount)
	{
		top = topcount;
	}

	public int getTop()
	{
		return(top);
	}

	public void putAll(RowBean td)
	{
		super.putAll(td);
	}

	public TimeZone getTimeZone()
	{
		if(thistimezone == null)
		{
			if (StringUtils.isNotBlank(InitServlet.getSystemParam("ServerTimeZone")))
				thistimezone = TimeZone.getTimeZone(InitServlet.getSystemParam("ServerTimeZone"));
			else
				thistimezone = TimeZone.getTimeZone(ActionBean.defaulttimezone);
		}
		return(thistimezone);
	}

	public void setTimeZone(String zonestring)
	{
		thistimezone = TimeZone.getTimeZone(zonestring);
	}

	public void setTimeZone(TimeZone zone)
	{
		thistimezone = zone;
	}
	
	public TimeZone getUserTimeZoneObject()
    {
		if (usertimezone == null) {
			usertimezone = TimeZone.getTimeZone(RunwayUtil.getGlobalTimeZone());
		}

		return (usertimezone);
	}

    public void setUserTimeZone(String zonestring)
    {
        usertimezone = TimeZone.getTimeZone(zonestring);
    }

    public void setUserTimeZone(TimeZone zone)
    {
    	usertimezone = zone;
    }    

	public String getLockKey()
	{
		String idname = tspec.getPrimaryKeyName();
		String id = (String)get(idname);
		String tablename = tspec.getTableName();
		if(id!=null && tablename!=null)
		{
			StringBuffer temp = new StringBuffer();
			temp.append(tablename);
			temp.append(_dot);
			temp.append(id);
			return(temp.toString());
		}
		return(_blank);
	}

	public void connect()
	{
		ActionBean.connect(this);
	}

	public void setCloseConnection(boolean b)
	{
		closeconnection = b;
	}

	public boolean getCloseConnection()
	{
		return(closeconnection);
	}

	public void setConnection(Object con)
	{
		setConnection((Connection)con);
	}

	public void setConnection(Connection con) {
		setConnection(con,false);
	}
	
	public void setConnection(Connection con, boolean close)
	{
		if (con != null) {
			this.con = con;
			closeconnection = close;
		}
	}

	public void setConnection(ServletRequest request)
	{
		Connection conn = ActionBean.getConnection(request);
		if(conn!=null)
		{
			con = conn;
			closeconnection = false;
		}
	}
	
	public void setRequest(HttpServletRequest request)
	{
		ActionBean.setRequest(request, this);
	}
	
	public Connection getConnection()
	{
		return(con);
	}

	int updatedcount = 0;

	public RowBean(){}

	public void setJndiName(String t)
	{
		put(jndiname,t);
	}

	public void setUpdatedCount(int c)
	{
		updatedcount = c;
	}

	public int getUpdatedCount()
	{
		return(updatedcount);
	}

	public String getJndiName()
	{
		String datasource = (String)get(jndiname);
		if(datasource == null)
		{
			datasource = ActionBean.getJndiName();
		}
		return(datasource);
	}

	public void setViewSpec(ViewSpec vs)
	{
		vspec = vs;
		tspec = SpecManager.getTableSpec(vspec.getMainTableName());
		rspec = SpecManager.getRelationSpec();
	}

	public void setTableSpec(TableSpec ts)
	{
		tspec = ts;
		rspec = SpecManager.getRelationSpec();
	}

	public void setViewSpec(String vs)
	{
		vspec = SpecManager.getViewSpec(vs);
		tspec = SpecManager.getTableSpec(vspec.getMainTableName());
		rspec = SpecManager.getRelationSpec();
	}

	public void setTableSpec(String ts)
	{
		tspec = SpecManager.getTableSpec(ts);
		rspec = SpecManager.getRelationSpec();
	}
/*
	public void setActionSpec(String as)
	{
		aspec = SpecManager.getActionSpec(as);
		//tspec = SpecManager.getTableSpec(aspec.getActionSetTable());
		//rspec = SpecManager.getRelationSpec();
	}	
*/	
	public TableSpec getTableSpec()
	{
		return(tspec);
	}

	public ViewSpec getViewSpec()
	{
		return(vspec);
	}

	public ActionSpec getActionSpec()
	{
		return(aspec);
	}	
	
	public RelationSpec getRelationSpec()
	{
		return(rspec);
	}

	public Relation getRelatedFields()
	{
		 return(relatedfields);
	}

	public void setAction(String s)
	{
		put(ActionBean.ACTION, s);
	}

	public void doActionSet(HttpServletRequest request, HttpServletResponse response)
	{
		ActionSet.doActionSet(this, request, response);
	}	
	
	public void doAction(boolean autosetvalues)
	{
		ActionBean.doAction(this ,autosetvalues);
	}
	
	public void doAction()
	{
		ActionBean.doAction(this);
	}
	
	public void doAction(String action)
	{
		setAction(action);
		ActionBean.doAction(this);
	}

	public void processRequest(HttpServletRequest request)
	{
		ActionBean.processRequest(request,this);
	}

	public void processRequest(HttpServletRequest request, String groupPrefix)
	{
		ActionBean.processRequest(request,this,groupPrefix);
	}

	public void parseRequest(HttpServletRequest request, String groupPrefix)
	{
		ActionBean.parseRequest(request,this,groupPrefix);
	}
	
	public void parseRequest(HttpServletRequest request)
	{
		ActionBean.parseRequest(request,this);
	}

	public void parseRequest(HttpServletRequest request, boolean allowempty, boolean keepmulti, String groupPrefix)
	{
		ActionBean.parseRequest(request,this, allowempty, keepmulti, groupPrefix);
	}	
	
	public void parseRequest(String query)
	{
		ActionBean.parseRequest(query,this);
	}

	public void setLocale(Locale l)
	{
		thislocale = l;
	}
	
	public Locale getLocale()
	{
		return(thislocale);
	}

	public String getAction()
	{
		return((String)get(ActionBean.ACTION));
	}	

	public void setDateInputFormat(String d)
	{
		//if(d.equals(AbstractDatabase.usdateinput))
		//{
			//convert = AbstractDatabase.usdateconvert;
		//}
		inputdateformat = d;
	}

	public void setError(String s)
	{
		errors.append(s);
	}
	
	public String getError()
	{
		return(errors.toString());
	}

	public boolean hasError()
	{
		if(errors.length()==0)
		{
			return(false);
		}
		return(true);
	}
	
	public String getString(String name)
	{
		//if(name.indexOf(ActionBean._dot)>=0)
		//{
			//return(ActionBean.getRelated(name,this));
		//}
		//else
		//{
			Object obj = super.get(name);
			if(obj!=null)
			{
				return(obj.toString());
			}
		//}
		return(ActionBean._blank);
	}

	public Object getObject(String name)
	{
		return(super.get(name));
	}
    
    public byte[] getBlob(String name)
    {
        return((byte[])super.get(name));
    }
    
	// NBCHANGE
	// If you have perfomed a select which returns the objects in the DB and 
	// later performed an Update (via ProcessRequestDelta or processrequest)
	// the row will contains strins instead of objects
	// this method will return the correct Object Type with the correct value 
	// for the row
	
	public Object getObjectFromString(String name)
	{
		return ActionBean.getObjectFromString(this, name);
	}

	public String getColumn(String name, String format)
	{
		String type = null;
		if(vspec!=null)
		{
			type = vspec.getFormatTypeForName(name);
		}
		Object obj = super.get(name);
		return (ActionBean.formatValue(name, format, type, obj, thislocale, getUserTimeZoneObject()));
	}

	public String getFormatInputObject(String name, String format)
	{
		String type = vspec.getFormatTypeForName(name);
		return (ActionBean.formatValue(name, format, type, getObjectFromString(name), thislocale));
	}
	
	public String getCodedValue(String value)
	{
		if(value == null)
		{
			return(ActionBean.NULL);
		}
		else if(value.length() == 0)
		{
			return(ActionBean.EMPTY);
		}
		return(value);
	}
	
	public String getCodedString(String name)
	{
		Object obj = super.get(name);
		String value = null;
		if(obj!=null)
		{
			value = obj.toString();
		}
		return(getCodedValue(value));
	}
	
	public String getCodedColumn(String name)
	{
		String format = vspec.getFormatForName(name);	
		String value = getColumn(name, format);
		return(getCodedValue(value));	
	}

	public String getColumn(String name)
	{
		String format = vspec.getFormatForName(name);	
		return(getColumn(name, format));
	}
	
	public String getColumnHTML(String name)
	{	
		return(StringUtil.toHTML(getColumn(name)));
	}
	
	public String getParameter(String name)
	{
		String format = vspec.getFormatForName(name);	
		return(getColumn(name, format));
	}
	
	public String[] getParameterValues(String name)
	{
		Object r = get(name); 
		if(r == null)
			return null;
		if(r instanceof String[]) 
			return (String[])r; 
		if(r instanceof String)
			return new String[]{(String)r};
		
		return new String[]{String.valueOf(r)};
	}
	
	public String getData(String name)
	{
		String format = vspec.getFormatForName(name);	
		return(getColumn(name, format));
	}

	public void setColumn(String name, String value)
	{
		ActionBean.setColumn(name, value, this);
	}
	
	public void setParameter(String name, Object value)
	{
		ActionBean.setColumn(name, value.toString(), this);
	}

	public String getDateTime(String name)
	{
		return(ActionBean.getDateTime(name, this, thislocale));
	}

	public void clearSort()
	{
		sortfields = null;
		sortorder = null;
	}
	
	public void sortBy(String field, int index)
	{
		super.put(sort_prefix+String.valueOf(index+1),field);
		if(sortfields==null)
		{
			sortfields = new String[5];
		}
		sortfields[index] = field;
	}

	public void sortOrder(String order, int index)
	{
		super.put(order_prefix+String.valueOf(index+1),order);
		if(sortorder==null)
		{
			sortorder = new String[5];
		}
		sortorder[index] = order;
	}

	public void setSort(int index, String sort, String order)
	{
		super.put(order_prefix+String.valueOf(index+1),order);
		super.put(sort_prefix+String.valueOf(index+1),sort);
		if(sortorder==null)
		{
			sortorder = new String[5];
		}
		if(sortfields==null)
		{
			sortfields = new String[5];
		}
		sortfields[index] = sort;
		sortorder[index] = order;
	}

	public String[] getSortfields()
	{
		return(sortfields);
	}

	public String[] getSortorder()
	{
		return(sortorder);
	}

	public void setToNewID(String name)
	{
		super.put(name,KeyMaker.generate());
	}

	public void createNewID()
	{
		if (tspec != null)
			super.put(tspec.getPrimaryKeyName(),KeyMaker.generate());
	}

	public StringBuffer createdDataOnIDStatment()
	{
		return(ActionBean.createdDataOnIDStatment(this));
	}

	public void generateSQLStatment()
	{
		ActionBean.generateSQLStatment(this);
	}

	public void generateSQLStatement()
	{
		generateSQLStatment();
	}

	public void retrieveData()
	{
		generateSQLStatment();
		retrieveDataOnStmt();
	}

	public void retrieveDataOnStmt()
	{
		ActionBean.retrieveDataOnStmt(this);
	}

	public void setSearchStatement(String s)
	{
		searchstmt = s;
	}  
    
	public Map getSearchGroups()
	{
		return(searchgroups);
	}

	public void setSearchGroups(Map groups)
	{
		searchgroups = groups;
	}

	public String getSearchStatement()
	{
		return(searchstmt);
	}

    public String getStatement()
    {
        return(getSearchStatement());
    }    
    
	public void setSearchCriteria(String s)
	{
		searchcriteria = s;
	}

   public void processRequestDelta(HttpServletRequest request) {
		ActionBean.processRequestDelta(request,this);
   }
	
	public String getSearchCriteria()
	{
		return(searchcriteria);	
	}

	public String getDateConversionsType()
	{
		return(convert);
	}
	
	public void setDateConversionsType(String s)
	{
		convert = s;
	}	

	public void clear() 
	{
	   clear(false);
   }

	public void clear(boolean clearAll)
	{
		String tviewspec = (String)get(viewspec);
		String ttablespec = (String)get(tablespec);
		String tjndiname = (String)get(jndiname); //tjndiname must not be empty
		String taction = (String)get(ActionBean.ACTION);
		super.clear();
		relatedfields = new Relation("root",null);
		searchgroups = null;
		updatedcount = 0;

      if (clearAll) {
         clearSort();
         top = 0;
      }
		if(errors.length()!=0)
		{
			errors = new StringBuffer();
		}
		if(tviewspec!=null)
		{
			setViewSpec(tviewspec);
		}
		if(ttablespec!=null)
		{
			setTableSpec(ttablespec);
		}
		if(taction!=null)
		{
			setAction(taction);
		}
		setJndiName(tjndiname);
	}

	public Object[] getKeys()
	{
		return(keySet().toArray());
	}

	public Object get(Object key)
	{
		return(super.get(key));
	}

	public Object get(String key)
	{
		return(super.get(key));
	}

	public Object put(Object key,Object obj)
	{
		return(super.put(key, obj));
	}

	public Object put(String key,Object obj)
	{
		return(super.put(key, obj));
	}

	public boolean containsKey(Object key)
	{
		return(super.containsKey(key));
	}

	public void close()
	{
		if(con!=null)
		{
			if(closeconnection)
			{
				try{
					con.close();
				}catch(Exception e){}
			}
			con = null;
		}
	}

   public void putAll(Map t) {
	  super.putAll(t);
   }

	public String toString()
	{
		return(toString(false,true));
	}
	
	public String toString(boolean showall)
	{
		return(toString(showall,true));
	}
    
	public String toString(boolean showall, boolean showtokens)
	{
		StringBuffer sb = new StringBuffer();
		Iterator keys = keySet().iterator();
		String key = null;
		Object obj = null;
		String value = null;
		while(keys.hasNext())
		{
			key = (String)keys.next();
			obj = get(key);
			//if key begins with | or ^ then it's a search group marker
			//and shouldn't be in output
			if(obj instanceof String && key.indexOf('|') != 0 && key.indexOf('^') != 0)
			{
				value = obj.toString();
				if(value.length()!=0 || showall)
				{
					if(!key.equals(ActionBean.ACTION) || showtokens)
					{
                        if(!key.equals(ActionBean.UPDATESTMT))
                        {
    						if(sb.length()!=0)
    						{
    							sb.append('&');
    						}
    						try{
    							sb.append(URLEncoder.encode(key,ActionBean.utf8));
    							sb.append('=');
    							sb.append(URLEncoder.encode(value,ActionBean.utf8));
    						}catch(Exception e){}
                        }
					}
				}
			}
		}
		return(sb.toString());
	}
	
	public void getColumnsFormDB(String columns)
	{
		ActionBean.setSelectStmt(this, columns);
		retrieveDataOnStmt();
	}
	
	public static void main(String[] args)
	{
		ActionBean.setDatabase("MySQL");
		RowBean bean = new RowBean();
		bean.setViewSpec("ContactView");
		bean.setColumn("ContactNotes.Subject|1","13342");
		bean.setColumn("FirstName|1","name");
		bean.generateSQLStatement();
		System.out.print(bean.getSearchStatement());
	}

	@Override
	public boolean isSet(String string) {
		return StringUtils.isNotBlank(getString(string));
	}
}
