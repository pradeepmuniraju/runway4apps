package com.sok.framework;
import com.sok.runway.offline.rpmManager.RPMtask;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
public class InternalFilter implements Filter
{
	String targeturl = "/403.html";
	String ip1 = null;
	String ip2 = null;
	String ip3 = null;

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws ServletException, IOException
	{
		RPMtask.getInstance().refreshData();
			String client = request.getRemoteAddr();
			if(ip1!=null && client.indexOf(ip1)==0)
			{
				chain.doFilter(request,response);
			}
			else if(ip2!=null && client.indexOf(ip2)==0)
			{
				chain.doFilter(request,response);
			}
			else if(ip3!=null && client.indexOf(ip3)==0)
			{
				chain.doFilter(request,response);
			}
			else if (isAllowIP(client))
			{
				chain.doFilter(request,response);
			}
			else
			{
				bounce(request ,response);
			}
	
	}
	
	public void bounce(ServletRequest request, ServletResponse response)throws IOException
	{
		StringBuffer bounceurl = new StringBuffer();
		bounceurl.append(((HttpServletRequest)request).getContextPath());
		bounceurl.append(targeturl);
		((HttpServletResponse)response).sendRedirect(bounceurl.toString());
	}
	
	public void init(FilterConfig config)throws ServletException
	{
		String temp = config.getInitParameter("targetURL");
		if(temp!=null)
		{
			targeturl = temp;
		}
		ip1 = config.getInitParameter("allowIP1");
		ip2 = config.getInitParameter("allowIP2");
		ip3 = config.getInitParameter("allowIP3");
	}
	
	boolean isAllowIP(String remoteIP) {
		if (remoteIP == null) return false;
		
		String allowIPs = (String) InitServlet.getSystemParams().get("AllowIPs");
		
		if (allowIPs != null && allowIPs.length() > 0) {
			String[] allow = allowIPs.split(",");
			for (String ip : allow) {
				if (remoteIP.startsWith(ip.trim())) return true;
			}
		}
		return false;
	}

	public void destroy()
	{
		
	}
}
