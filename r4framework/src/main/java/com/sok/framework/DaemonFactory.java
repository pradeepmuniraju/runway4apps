package com.sok.framework;

import java.util.*;
import java.lang.reflect.*;
import javax.servlet.*;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import com.sok.runway.offline.ThreadManager;
import com.sok.framework.sql.DatabaseSynchronizationListener;

public class DaemonFactory implements ServletContextListener {
   
	private static final Logger logger = LoggerFactory.getLogger(DaemonFactory.class);
	
   private ServletConfig config = null;
   private ServletContext context = null;
   private HashMap<String, DaemonProcess> processes = null;
   
	private String smtpHost = null;
	private String smtpUsername = null;
	private String smtpPassword = null;
	private String smtpPort = null;
      
   private static DaemonFactory daemonFactory;
   
   public static DaemonFactory getDaemonFactory(){
      if (DaemonFactory.daemonFactory == null) {
         DaemonFactory.daemonFactory = new DaemonFactory();
      }
      return(DaemonFactory.daemonFactory);
   }
   
   public DaemonFactory() {
      DaemonFactory.daemonFactory = this;
	}
    
   public Collection<DaemonProcess> getDaemonProcesses() {
      if (processes != null) {
         return processes.values();
      }
      else { 
         return null;
      }
   }
   
   public String getServletContextName() {
   	return context.getServletContextName();
   }
   
   public void init(ServletConfig config) {
	   if(DatabaseSynchronizationListener.hasStartupErrors()) {
		   logger.info("URLFilter has startup errors, not starting daemons"); 
		   return;
	   }
       this.config = config;
	   this.context = config.getServletContext();
	   smtpHost = (String)context.getInitParameter("SMTPHost");
	   smtpUsername = (String)context.getInitParameter("SMTPUsername");
	   smtpPassword = (String)context.getInitParameter("SMTPPassword");
       smtpPort = (String)context.getInitParameter("SMTPPort");
		
       processes = new HashMap<String, DaemonProcess>();
            
      int i=0;
      String daemonClassName = config.getInitParameter("daemonClass"+i);
      while (daemonClassName != null) {
         createDaemonProcess(daemonClassName);
         i++;
         daemonClassName = config.getInitParameter("daemonClass"+i);
      }
	}
	
	public String getSMTPHost() {
		return(smtpHost);
	}
	
	public String getSMTPUsername() {
	   if (smtpUsername == null) {
		   return "";
	   }
	   else {
		   return(smtpUsername);
		}
	}
	
	public String getSMTPPassword() {
	   if (smtpPassword == null) {
		   return "";
	   }
	   else {
		   return(smtpPassword);
		}
	}    
    
	public String getSMTPPort() {
	  return smtpPort;
	}
    
	public void removeDaemonProcess(String daemonProcessName) {
	   DaemonProcess process = processes.get(daemonProcessName);
	   if (process != null) {
	      if (!process.isKilled()) {
	         process.killProcess(false);
	         process.interrupt();
	         processes.remove(process.getName());
	      }
	   }
	   /*for (int i=0; i < processes.size(); i++) {
	      process = (DaemonProcess)processes.get(i);
	      if (process.getClass().getName().equals(daemonClassName)) {
	         processes.remove(i);
	         break;
	      }
	   }*/	   
	}
	
	
	public void createDaemonProcess(String daemonClassName) {
      Class<DaemonProcess> daemonClass = null;
      Constructor<DaemonProcess> daemonConstructor = null;
      DaemonProcess daemonProcess = null;
      
	   try {
         daemonClass = (Class<DaemonProcess>)Class.forName(daemonClassName);
         daemonConstructor = daemonClass.getConstructor(getClass());
         daemonProcess = daemonConstructor.newInstance(new Object[]{this});
         processes.put(daemonProcess.getName(), daemonProcess);
         daemonProcess.startDaemon();
      }
      catch (Exception e) {
    	  logger.error("Error initializing " + daemonClassName, e);
         System.out.println("Could not createDaemonProcess: "+daemonClassName+ " "+ e.getMessage());
         
      }
	}
	
	public ServletContext getServletContext() {
	   return config.getServletContext();
	}
	
	/*public void run() {
	   DaemonProcess process = null;
	   while (!killed) {
	      if (!paused) {
	         for (int i=0; i < processes.size(); i++) {
	            process = (DaemonProcess)processes.get(i);
	            if (!process.isKilled()) {
	               process.startDaemon();
	            }
	         }
	      }
	      try {
	         Thread.sleep(60000);
	      }
	      catch (Exception e) {}
	   }
	}*/
	
	public void killAllDaemons() {
      if(processes!=null) {
	      Iterator<DaemonProcess> iter = processes.values().iterator();
	      while(iter.hasNext()) {
	         DaemonProcess process = iter.next();
	         process.killProcess(false);
	      }
      }
	}
	
	public void pauseAllDaemons() {
      if(processes!=null) {
	      Iterator<DaemonProcess> iter = processes.values().iterator();
	      while(iter.hasNext()) {
	         DaemonProcess process = iter.next();
	         process.pauseProcess();
	      }
      }
	}
	
	public void resumeAllDaemons() {
      if(processes!=null) {
	      Iterator<DaemonProcess> iter = processes.values().iterator();
	      while(iter.hasNext()) {
	         DaemonProcess process = iter.next();
	         process.resumeProcess();
	      }
      }
	}
   
   public void contextInitialized(ServletContextEvent event) {
	   logger.info("DaemonFactory Initialized");
	   //this is a bit of a hack to get around initservlet starting up after the listeners, where a listener requires the SpecManager to work
	   SpecManager.setServletContext(event.getServletContext());
   }

   public void contextDestroyed(ServletContextEvent event) {
	   logger.info("DaemonFactory destroying, killing daemons");
       killAllDaemons();
       logger.debug("After KillAllDaemons");
       ThreadManager.shutdown();
       logger.debug("After ThreadMangaer shutdown");
   }     
}