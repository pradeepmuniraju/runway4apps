package com.sok.framework;

//import com.sok.email.validation.DNS;
//import com.sok.email.validation.DNSQuery;
//import com.sok.email.validation.Address;
//import com.sok.email.validation.MailExchanger;

import javax.naming.*;
import javax.naming.directory.*;
import java.util.*;

import java.io.*;
import java.net.*;
import javax.mail.internet.*;
import com.sok.framework.generation.*;

public class EmailValidationBean
{

	public static final int VALID = 1;
	public static final int INVALID = 0;	
	public static final int ERROR = 2;

	public static final int INVALIDFORMAT = 3;
	public static final int INVALIDDOMAIN = 4;
	public static final int ERROR_USER = 5;
	public static final int EMPTY = 6;

	public static final String[] Statuses = {"Validated", "Invalid User", "Invalid Format", "Invalid Domain", "Unverified","No Email Address","Bounced"};

	public static final String[] CODES = {"Invalid User","Validated","Unverified","Invalid Format","Invalid Domain","Unverified","No Email Address","Bounced"};

    private String dnsserver = "10.144.1.7";
    private String fromemail = "info@switched-on.com.au";
    private String domainname = "switched-on.com.au";
    
	StringBuffer status = null;	
	boolean debug = false;

	String emailaddress = null;

	int timeout = 10000;
	boolean tryallmxservers = false;

	private static Hashtable env = null;
	private static String defaultserver = null;
	
	private static Comparator comparator = null;
	
	public EmailValidationBean()
	{
		if(env==null)
		{
			init();
		}
		dnsserver = defaultserver;
	}

	public void setTryAllServers(boolean b)
	{
		tryallmxservers = b;
	}

	public void setTimeOut(int milisecs)
	{
		timeout = milisecs;
	}

	public void setTimeOut(String value)
	{
		timeout = Integer.parseInt(value);
	}
	
	public String getStatus()
	{
		return(status.toString());
	}

	public void setFromEmail(String s)
	{
		fromemail = s;
	}
	
	public void setGreetingDomain(String s)
	{
		domainname = s;
	}
	
	public String getDNSServer()
	{
		return(dnsserver);
	}	
	
	public void setDNSServer(String s)
	{
		dnsserver = s;
		env.put("java.naming.provider.url", "dns://"+dnsserver);
	}

	public void setDebug(boolean b)
	{
		debug = b;
	}

	String checkFormat(String s)
	{
		String emailaddress = null;
		try
		{
			InternetAddress email = new InternetAddress(s);
			emailaddress = email.getAddress();
			if(debug)
			{
				status.append("Parsed email address - ");
				status.append(emailaddress);
				status.append("\r\n");
			}
		}
		catch(Exception e)
		{
			status .append("Invalid format");
		}
		return(emailaddress);
	}

    /**
     * Checks email for syntax errors, and then connects to all listed MX servers to see if the server will accept the
     * email address for delivery.
     *
     * @param emailAddress Email address to validate
     *
     * @return boolean false for invalid email address
     */
	public void setEmailAddress(String s)
	{
		emailaddress = s;
	}

	public int getValidateEmail()
	{
		int result = ERROR;
		status = new StringBuffer();
		String emailAddress = checkFormat(emailaddress);
		String[] mxrecords = null;
		if(emailAddress!=null && emailAddress.length()!=0)
		{
			String domain = emailAddress.substring ( emailAddress.indexOf ( "@" ) + 1, emailAddress.length () );
			try
			{
				try
				{
					mxrecords = getMXRecords(domain);
				}
				catch(Exception e)
				{

				}

				if(mxrecords!=null)
				{
					int valid = ERROR_USER;

					String mxaddress = null;

					if(mxrecords != null)
					{
						if(debug)
						{
							for(int i=0; i<mxrecords.length; i++)
							{
								status.append("\r\n");
								status.append(String.valueOf(i));
								status.append(" ");
								status.append(mxrecords[i]);
							}
						}
						mxaddress = mxrecords[0];
	
						valid = validateUser(mxaddress, emailAddress);
						if(debug)
						{
							status.append("\r\n");
							status.append("User tested ");
							status.append(CODES[valid]);
							status.append(" on ");
							status.append(mxaddress);
							status.append("\r\n");
						}
						if(valid == VALID || !tryallmxservers)
						{
							return(valid);
						}
					}
					result = valid;
				}
				else
				{
					result = INVALIDDOMAIN;
				}
				//else if(validateUser(domain, emailAddress))
				//{
			    	//return(true);
				//}
			}
			catch (Exception x)
			{
				status.append("Error encountered while checking email - ");
				status.append(x.toString());
				status.append("\r\n");
				result = ERROR;
			}
		}
		else
		{
			result = EMPTY;
		}
		return(result);
	}

    /**
     * Checks all mx records smtp servers to see if they will accept delivery of a specified email address
     *
     * @param mxRecord MX record (cname) to validate email via remote smtp server from
     * @param emailAddress Email to validate
     *
     * @return boolean false invalid email
     */
	private int validateUser (String mxRecord, String emailAddress)
	{
		int valid = ERROR_USER;
		try
		{
			InetAddress ipaddress = getDomainIPAddress(mxRecord);
			if(ipaddress!=null)
			{
				valid = checkUser(emailAddress, ipaddress);
			}
		}
		catch ( Exception x )
		{
		}
		return(valid);
	}

	protected synchronized void init()
	{
		if(env==null)
		{
		try{
			comparator = new MXComparator();
			java.security.Security.setProperty("networkaddress.cache.ttl" , "0"); //stop dns cache
			env = new java.util.Hashtable();
			env.put("java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory");	
			javax.naming.directory.DirContext ictx = new javax.naming.directory.InitialDirContext(env);
			String defaultserveraddress = ictx.getEnvironment().get("java.naming.provider.url").toString();
			String dnssrt = "dns://";
			int dnsindex = defaultserveraddress.indexOf(dnssrt);
			if(dnsindex == 0)
			{
				defaultserver = defaultserveraddress.substring(dnsindex+dnssrt.length(), defaultserveraddress.length());
			}

		}catch(Exception e){}
		}
	}
	
	protected InetAddress getDomainIPAddress(String mxRecord)
	{
		InetAddress address = null;
		DirContext ctx = null;
		try
		{
			ctx = new InitialDirContext(env);
 
			Attributes attrs = ctx.getAttributes(mxRecord, new String[] {"A"});
			Attribute attributeA = attrs.get("A");
			if(attributeA.size()>0)
			{
				String addressstr = attributeA.get(0).toString();
				if(debug)
				{
					status.append("IP for ");
					status.append(mxRecord);
					status.append(": ");
					status.append(addressstr);
					status.append("\r\n");	
				}
				byte[] ip = StringUtil.parseIPAddress(addressstr);
				return(InetAddress.getByAddress(ip));
			}
		}
		catch (Exception ex)
		{
			status.append("Error encountered while querying DNS server for A records on host ");
			status.append(mxRecord);
			status.append(" - ");
			status.append(ex.toString());
			status.append("\r\n");		
		}
		finally
		{
			if(ctx!=null)
			{
				try{
					ctx.close();
				}catch(Exception e){}
			}
		}
		return(address);
	}



	int checkUser(String emailAddress, InetAddress serverAddress)
	{
		Socket smtpserversocket = null;
		int valid = ERROR_USER;
		try
		{
			smtpserversocket = new Socket(serverAddress, 25);
			smtpserversocket.setSoTimeout (timeout);
			smtpserversocket.setKeepAlive (true);
			//smtpserversocket.setSoLinger (true, 3000);
			PrintStream smtpserverstream = new PrintStream (smtpserversocket.getOutputStream());
			BufferedReader smtpserverreader = new BufferedReader(new InputStreamReader(smtpserversocket.getInputStream()));
			smtpserverreader.readLine ();
			smtpserverstream.print("HELO ");
			smtpserverstream.print(domainname);
			smtpserverstream.print("\r\n");
			smtpserverstream.flush();
			String serverresponse = smtpserverreader.readLine ();
			if(serverresponse.startsWith ( "250" ) )
			{
				smtpserverstream.print("MAIL FROM:<" );
				smtpserverstream.print(fromemail);
				smtpserverstream.print(">\r\n");
				serverresponse = smtpserverreader.readLine ();
				if(serverresponse.startsWith ( "250" ) )
				{
					smtpserverstream.print("RCPT TO:<");
					smtpserverstream.print(emailAddress);
					smtpserverstream.print(">\r\n");
					serverresponse = smtpserverreader.readLine();
					if(serverresponse.startsWith("250"))
					{
					    valid = VALID;
					}
					else
					{
						status.append("User does not exist.");
						valid = INVALID;
					}
				}
				else
				{
					status.append("Email server rejected static local email address");
				}
			}
			else
			{
				status.append("Email server refused connection");
			}
		}
		catch (Exception x)
		{
			status.append("Error encountered while querying email server - ");
			status.append(x.toString());
			status.append("\r\n");
		}
		finally
		{
			if(smtpserversocket!=null)
			{	
				try{
					smtpserversocket.close();
				}catch(Exception e){}
			}
		}
		return(valid);
	}


    /**
     * Gets MX records for a specified domain
     *
     * @param String host name to get MX records for
     *
     * @return String[]
     */
	private String[] getMXRecords(String hostname)
	{
		DirContext ctx = null;
		try
		{
			ctx = new InitialDirContext(env);
 
			Attributes attrs = ctx.getAttributes(hostname,new String[] {"MX"});
	        Attribute attributeMX = attrs.get("MX");


	        // split MX RRs into Preference Values(pvhn[0]) and Host Names(pvhn[1])
	        String[][] pvhn = new String[attributeMX.size()][2];
	        for (int i = 0; i < attributeMX.size(); i++)
	        {
	            pvhn[i] = ("" + attributeMX.get(i)).split("\\s+");
	        }

	        // sort the MX RRs by RR value (lower is preferred)
	        Arrays.sort(pvhn, comparator);

	        // put sorted host names in an array, get rid of any trailing '.' 
	        String[] sortedHostNames = new String[pvhn.length];
	        for (int i = 0; i < pvhn.length; i++)
	        {
	            sortedHostNames[i] = pvhn[i][1].endsWith(".") ? 
	                pvhn[i][1].substring(0, pvhn[i][1].length() - 1) : pvhn[i][1];
	        }
	        return sortedHostNames;
		}
		catch (Exception ex)
		{
			status.append("Error encountered while querying DNS server for MX records on host ");
			status.append(hostname);
			status.append(" - ");
			status.append(ex.toString());
			status.append("\r\n");		
		}
		finally
		{
			if(ctx!=null)
			{
				try{
					ctx.close();
				}catch(Exception e){}
			}
		}
		return(null);
	}

	public class MXComparator implements Comparator
    {
        public int compare(Object o1, Object o2)
        {
        	if(o1 instanceof String[] && o2 instanceof String[])
        	{
        		return (compare(((String[])o1)[0], ((String[])o2)[0]));
        	}
        	return(0);
        }
		
        public int compare(String o1, String o2)
        {
            return (Integer.parseInt(o1) - Integer.parseInt(o2));
        }
    }
	
	public static void main(String[] args)
	{
		EmailValidationBean bean = new EmailValidationBean();
		//bean.setDebug(true);
		System.out.println(bean.getDNSServer());
		bean.setDNSServer("10.144.1.7");
		System.out.println(bean.getDNSServer());
		bean.setEmailAddress("shinytoaster@gmail.com");
		System.out.println(CODES[bean.getValidateEmail()]);
		System.out.println(bean.getStatus());
	}
}
