package com.sok.framework;
import java.util.*;


public class TableDataHash extends HashMap<String, TableData>
{
	private static final long serialVersionUID = 3L;
	Iterator<String> it = null;

	public TableData getTableData(String key)
	{
		return get(key);
	}

	public void putTableData(String key, TableData bean)
	{
		put(key,bean);
		it = null;
	}

	public boolean hasNextTableData()
	{
		return(it.hasNext());
	}

	public TableData nextTableData()
	{
		return get(it.next());
	}

	public void resetIterator()
	{
		it = keySet().iterator();
	}

	public Iterator<String> getIterator()
	{
		return(it);
	}

	public void clearIterator()
	{
		it = null;
	}

}
