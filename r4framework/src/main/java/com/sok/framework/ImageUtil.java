package com.sok.framework;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.itextpdf.text.BaseColor;

import sun.awt.image.BufferedImageGraphicsConfig;

public class ImageUtil {
	private static final Logger logger = LoggerFactory.getLogger(ImageUtil.class);

	public static final String formatBMP = "bmp";
	public static final String formatJPG = "jpg";
	public static final String formatPNG = "png";
	public static final String formatGIF = "gif";

	protected String path = null;
	protected BufferedImage img = null;
	protected String format = formatJPG;
	protected float jpegQuality = 0.98f;
	protected boolean allowEnlarge = false;
	protected int sourceImageType;

	// protected WritableRaster alphaRaster;

	public ImageUtil(String ref) {
		loadImage(ref);
	}

	public ImageUtil(String ref, File file) {
		loadImage(ref, file);
	}

	public void loadImage(String path) {
		loadImage(path, new File(path));
	}

	public void trim() {

		int f = img.getRGB(0, 0);

		int x1 = 0, x2 = img.getWidth() - 1, y1 = 0, y2 = img.getHeight() - 1;

		// left >
		for (int i = x1; i < img.getWidth(); i++) {
			boolean eq = true;
			x1 = i;
			for (int j = 0; j < img.getHeight(); j++) {
				if (img.getRGB(i, j) != f) {
					eq = false;
					break;
				}
			}
			if (!eq) {
				break;
			}
		}
		// < right
		for (int i = x2; i > x1; i--) {
			x2 = i;
			boolean eq = true;
			for (int j = 0; j < img.getHeight(); j++) {
				if (img.getRGB(i, j) != f) {
					eq = false;
					break;
				}
			}
			if (!eq) {
				break;
			}
		}

		// top down
		for (int j = 0; j < img.getHeight(); j++) {
			boolean eq = true;
			y1 = j;
			for (int i = x1; i < x2; i++) {
				if (img.getRGB(i, j) != f) {
					eq = false;
					break;
				}
			}
			if (!eq) {
				break;
			}
		}

		// bottom ^
		for (int j = y2; j > y1; j--) {
			boolean eq = true;
			y2 = j;
			for (int i = x1; i < x2; i++) {
				if (img.getRGB(i, j) != f) {
					eq = false;
					break;
				}
			}
			if (!eq) {
				break;
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Trimming image original [{}x{}]", img.getWidth(), img.getHeight());
			logger.debug("New coordinates x[{},{}] y[{},{}]", new String[] { String.valueOf(x1), String.valueOf(x2), String.valueOf(y1), String.valueOf(y2) });
		}
		if (x1 != 0 || x2 != (img.getWidth() - 1) || y1 != 0 || y2 != (img.getHeight() - 1)) {
			logger.debug("Trimming image as is required");
			crop(x1, y1, x2 - x1, y2 - y1);
		}
	}

	/**
	 * Created Parameter with File as you may already have a reference to use, say if you checked
	 * file.exists()
	 */
	public void loadImage(String path, File file) {
		this.path = path;
		this.img = null;
		try {
			setFormat();
			img = ImageIO.read(file);
			sourceImageType = img.getType();
		} catch (Exception e) {
			logger.error("Error [{}] in loadImage({},{})", new String[] { e.getMessage(), path, file.getAbsolutePath() });
		}
	}

	protected void setFormat() {
		int dotindex = path.lastIndexOf('.');
		if (dotindex > 0 && dotindex < path.length()) {
			String suffix = path.substring(dotindex + 1, path.length());
			if (suffix.equalsIgnoreCase(formatJPG)) {
				format = formatJPG;
			} else if (suffix.equalsIgnoreCase(formatGIF)) {
				format = formatGIF;
			} else if (suffix.equalsIgnoreCase(formatPNG)) {
				format = formatBMP;
			} else if (suffix.equalsIgnoreCase(formatBMP)) {
				format = formatBMP;
			}
		}
	}

	public void setFormat(String s) {
		format = s;
	}

	public void setJpegQuality(float jpegQuality) {
		this.jpegQuality = jpegQuality;
	}

	public void setAllowEnlarge(boolean allowEnlarge) {
		this.allowEnlarge = allowEnlarge;
	}

	public void saveImage(String savepath) {
		/*
		 * try { ImageIO.write(img, format, new File(savepath)); } catch(IOException e) {
		 * e.printStackTrace(); }
		 */
		if (img != null)
			writeImage(new File(savepath));

	}

	protected void writeImage(File outfile) {
		ImageOutputStream ios = null;
		ImageWriter writer = null;
		try {
			Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(format);
			if (iter.hasNext()) {
				writer = iter.next();
			}

			ios = ImageIO.createImageOutputStream(outfile);

			if (ios == null) {
				throw new RuntimeException("Output Stream was null, probably because you don't have write access.");
			}

			writer.setOutput(ios);

			ImageWriteParam iwparam = writer.getDefaultWriteParam();

			if (ImageUtil.formatJPG.equals(format)) {
				iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
				iwparam.setCompressionQuality(jpegQuality);
			}

			IIOImage image = new IIOImage(img, null, null);
			writer.write(null, image, iwparam);

		} catch (IOException e) {
			logger.error("Count not write image file {}", outfile.getAbsoluteFile());
			//e.printStackTrace();
		} finally {
			try {
				ios.flush();
				writer.dispose();
				ios.close();
			} catch (Exception e) {
			}
		}
	}

	public void saveImage() {
		saveImage(path);
	}

	public void resizeToWidth(int newW) {
		try {
			float ratio = Float.valueOf(img.getWidth()) / newW;
			int newH = (int) (img.getHeight() / ratio);
			if (newH < 1) {
				newH = 1;
			}
			resize(newW, newH);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void resizeToHeight(int newH) {
		try {
			float ratio = Float.valueOf(img.getHeight()) / newH;
			int newW = (int) (img.getWidth() / ratio);
			if (newW < 1) {
				newW = 1;
			}
			resize(newW, newH);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void resize(int newW, int newH) {
		if (img == null)
			return;
		int w = img.getWidth();
		int h = img.getHeight();
		if ((newW < w && newH < h) || allowEnlarge) {
			BufferedImage dimg = new BufferedImage(newW, newH, img.getType());
			Graphics2D g = dimg.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g.drawImage(img, 0, 0, newW, newH, 0, 0, w, h, Color.white, null);
			g.dispose();
			this.img = dimg;
		}
	}

	public void padImage(int newW, int newH) {
		padImage(newW, newH, Color.white.getRGB());
	}

	public void padImage(int newW, int newH, int rgb) {
		int w = img.getWidth();
		int h = img.getHeight();
		if ((newW < w && newH < h) || allowEnlarge) {
			BufferedImage dimg = new BufferedImage(newW, newH, img.getType());

			for (int i = 0; i < newW; i++) {
				for (int j = 0; j < newH; j++) {
					dimg.setRGB(i, j, rgb);
				}
			}

			Graphics2D g = dimg.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

			int dx1 = newW == w ? 0 : (w - newW) / 2;
			if (dx1 < 0)
				dx1 *= -1;
			int dy1 = newH == h ? 0 : (h - newH) / 2;
			if (dy1 < 0)
				dy1 *= -1;

			g.drawImage(img, dx1, dy1, dx1 + w, dy1 + h, 0, 0, w, h, Color.white, null);
			g.dispose();
			this.img = dimg;
		}
	}

	public void resizeToFit(int newW, int newH) {
		resizeToFit(newW, newH, false, false);
	}

	public void resizeToFit(int newW, int newH, boolean padToSize) {
		resizeToFit(newW, newH, padToSize, false);
	}

	public void resizeToFit(int newW, int newH, boolean padToSize, boolean cropToSize) {
		if (img != null) {
			int w = img.getWidth();
			int h = img.getHeight();
			float hratio = Float.valueOf(h) / newH;
			float wratio = Float.valueOf(w) / newW;

			if ((hratio > wratio && !cropToSize) || (hratio < wratio && cropToSize)) {
				resizeToHeight(newH);
			} else {
				resizeToWidth(newW);
			}
			if (cropToSize) {
				cropToSize(newW, newH);
			}
			if (padToSize) {
				padImage(newW, newH);
			}
		}
	}

	public void resizeAndCrop(int newW, int newH) {
		resizeToFit(newW, newH, false, true);
	}

	/*
	 * GOOD FOR TESTING IMAGES public void getColor(BufferedImage img) {
	 * 
	 * 
	 * System.out.println("Image Type : "+img.getType());
	 * 
	 * int[] myBigIntArray = new int[1];
	 * 
	 * //img.getRaster().getDataElements(48, 244, myBigIntArray);
	 * img.getRaster().getDataElements(43, 68, myBigIntArray);
	 * 
	 * int alpha = (myBigIntArray[0] >> 24) & 0xff; int red = (myBigIntArray[0] >> 16) & 0xff; int
	 * green = (myBigIntArray[0] >> 8) & 0xff; int blue = (myBigIntArray[0] >> 0) & 0xff;
	 * 
	 * System.out.println("Alpha: "+alpha); System.out.println("red: "+red);
	 * System.out.println("green: "+green); System.out.println("blue: "+blue);
	 * 
	 * }
	 * 
	 * public void showImg() {
	 * 
	 * javax.swing.Icon icon = new javax.swing.ImageIcon(img); javax.swing.JLabel label = new
	 * javax.swing.JLabel(icon);
	 * 
	 * final javax.swing.JFrame f = new javax.swing.JFrame("ImageIconExample");
	 * f.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE); f.getContentPane().add(label);
	 * f.pack(); javax.swing.SwingUtilities.invokeLater(new Runnable(){ public void run() {
	 * f.setLocationRelativeTo(null); f.setVisible(true); } }); }
	 */

	public void cropToSize(int newW, int newH) {
		int w = img.getWidth();
		int h = img.getHeight();

		int xIndex = 0, yIndex = 0;
		if (newW < w) {
			xIndex = (w - newW) / 2;
		}
		if (newH < h) {
			yIndex = (h - newH) / 2;
		}
		crop(xIndex, yIndex, newW, newH);
	}

	public void crop(int cropX1, int cropY1, int cropW, int cropH) {
		if (logger.isDebugEnabled())
			logger.debug("crop({},{},{},{})", new String[] { String.valueOf(cropX1), String.valueOf(cropY1), String.valueOf(cropW), String.valueOf(cropH) });
		crop(cropX1, cropY1, cropW, cropH, cropW, cropH);
	}

	public void crop(int cropX1, int cropY1, int cropW, int cropH, int newW, int newH) {
		int w = img.getWidth();
		int h = img.getHeight();

		// Crop only if within image dimension
		// X1, Y1 is always the smaller coordinate pair (top left coordinate of the crop box)
		// The 0,0 coordinate is at the top left corner of the image
		if (cropX1 >= 0 && cropY1 >= 0 && (cropX1 + cropW) <= w && (cropY1 + cropH) <= h) {
			BufferedImage dimg = new BufferedImage(newW, newH, img.getType());
			Graphics2D g = dimg.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g.drawImage(img, 0, 0, newW, newH, cropX1, cropY1, cropX1 + cropW, cropY1 + cropH, Color.WHITE, null);
			g.dispose();
			this.img = dimg;
		}
	}

	public void convertToSource() {

		BufferedImage tempImage = new BufferedImage(img.getWidth(), img.getHeight(), sourceImageType);
		Graphics2D g = tempImage.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, 0, 0, null);
		g.dispose();
		img = tempImage;
	}

	/**
	 * Rotate an image around it's centre axis with the degrees shown. Image will be rotated
	 * clockwise 90 degrees
	 * 
	 * @param deg
	 */
	public void rotate(double deg) {

		double radians = Math.toRadians(deg); // need degrees in radians to work

		AffineTransform at = AffineTransform.getRotateInstance(radians); // new AffineTransform();
		AffineTransform trans = findTranslation(at, img);
		at.preConcatenate(trans);

		AffineTransformOp ato = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		img = ato.filter(this.img, null);

		// Sets image encoding correctly.
		convertToSource();
	}

	/*
	 * Taken from: http://www.java2s.com/Code/Java/2D-Graphics-GUI/RotateImage45Degrees.htm find
	 * proper translations to keep rotated image correctly displayed Note: Could be a static method
	 * if required.
	 */
	private AffineTransform findTranslation(AffineTransform at, BufferedImage bi) {
		Point2D p2din, p2dout;

		p2din = new Point2D.Double(0.0, 0.0);
		p2dout = at.transform(p2din, null);
		double ytrans = p2dout.getY();

		p2din = new Point2D.Double(0, bi.getHeight());
		p2dout = at.transform(p2din, null);
		double xtrans = p2dout.getX();

		AffineTransform tat = new AffineTransform();
		tat.translate(-xtrans, -ytrans);
		return tat;
	}

	/*
	 * // This class overrides the setCompressionQuality() method to workaround // a problem in
	 * compressing JPEG images using the javax.imageio package. public class MyImageWriteParam
	 * extends JPEGImageWriteParam { public MyImageWriteParam() { super(Locale.getDefault()); }
	 * 
	 * public void setCompressionQuality(int quality) { //between 0 and 256 this.compressionQuality
	 * = quality; } }
	 */

	public boolean isPortrait() {
		return img.getWidth() < img.getHeight();
	}

	public void createCompatibleImage() {
		GraphicsConfiguration gc = BufferedImageGraphicsConfig.getConfig(img);
		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage result = gc.createCompatibleImage(w, h, Transparency.TRANSLUCENT);
		Graphics2D g2 = result.createGraphics();
		g2.drawRenderedImage(img, null);
		g2.dispose();
		this.img = result;
	}

	public void blurImage() {
		float ninth = 1.0f / 9.0f;
		float[] blurKernel = { ninth, ninth, ninth, ninth, ninth, ninth, ninth, ninth, ninth };

		Map<RenderingHints.Key, Object> map = new HashMap<RenderingHints.Key, Object>();
		map.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		map.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		map.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		RenderingHints hints = new RenderingHints(map);
		BufferedImageOp op = new ConvolveOp(new Kernel(3, 3, blurKernel), ConvolveOp.EDGE_NO_OP, hints);
		this.img = op.filter(this.img, null);
	}

	public static void main(String[] args) {
		java.text.SimpleDateFormat timeStampFormat = new java.text.SimpleDateFormat("yyyyMMddhhmmss");

		// String path = "C:\\tomcat\\webapps\\runway\\specific\\pdf\\rivercityimg\\plan.jpg";

		String path = "C:\\tomcat\\webapps\\runway\\files\\0P1W20875I2V9T9X7F849U2Y7D8N\\0P1933204N315C723R188S767395\\External Double Story Townhouse.jpg";

		// String path = "G:\\TEMP\\img\\3149299_floorplan.JPG";
		// String path = "G:\\TEMP\\img\\ti01.gif";
		// String path = "G:\\TEMP\\img\\image1.jpg";
		ImageUtil img = new ImageUtil(path);
		img.cropToSize(118, 96);
		img.setJpegQuality(0.80f);
		String newPath = path.substring(0, path.lastIndexOf('.')) + "_11896_" + timeStampFormat.format(new Date()) + path.substring(path.lastIndexOf('.'));
		img.saveImage(newPath);

		// img.convertARGB();

		// img.setFormat(ImageUtil.formatPNG);

		// ImageUtil img = new ImageUtil("/root/Pictures/img_1802.jpg");
		// String[] types = ImageIO.getWriterFormatNames();
		// for(int i=1; i<1000; i++)
		// {
		// img.resizeToWidth(280);
		// img.setFormat(ImageUtil.formatGIF);
		// img.saveImage("/root/Pictures/img_1802j_r.jpg");
		// img.loadImage("/root/Pictures/img_1802.jpg");
		// }

	}

	/**
	 * 
	 * @return the image width, or -1 if image is null or not loaded
	 */
	public int getWidth() {
		return img == null ? -1 : img.getWidth();
	}

	/**
	 * 
	 * @return the image height, or -1 if image is null or not loaded
	 */
	public int getHeight() {
		return img == null ? -1 : img.getHeight();
	}

	/**
	 * <p>
	 * Check if this image can be resized.
	 * </p>
	 * 
	 * @return <b><code>true</code></b> if the image can be resized, <b><code>false</code></b>
	 *         otherwise.
	 */
	public boolean canResize() {
		/*
		 * BufferedImages with a custom type cannot be resized (see
		 * http://www.opensourcecf.com/forums
		 * /messages.cfm?threadid=DC7070F5-0829-A860-075579DFD48CC2FE).
		 */
		boolean canResize = false;
		if (this.img != null && this.img.getType() != BufferedImage.TYPE_CUSTOM) {
			canResize = true;
		}
		return canResize;
	}
}