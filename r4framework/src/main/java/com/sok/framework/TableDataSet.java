package com.sok.framework;

import java.sql.*;

public interface TableDataSet extends TableData
{
	public void setStatement(Statement stmt);

	public void setResultSet(ResultSet current);
}
