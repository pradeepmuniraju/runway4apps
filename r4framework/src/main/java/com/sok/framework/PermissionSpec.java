package com.sok.framework;
import org.dom4j.*;

import com.sok.runway.XMLUtil;

public class PermissionSpec extends XMLSpec
{
   
   public static String NAME = "DefaultPermission";
   
   private PermissionSpecPart defaultpermission;
   private PermissionSpecPart specificpermission;   
  
   public static String _permissions = "permissions/";   
   
   public PermissionSpec(String name)   
   {   
       this.name = name; 
       defaultpermission = new PermissionSpecPart(SpecManager.getFilePath(name, null, _permissions), false);
       specificpermission = new PermissionSpecPart(SpecManager.getFilePath(name, "specific/", _permissions), true);
       if(specificpermission.getCategoryLength()==0)
       {
          specificpermission = null;
       }
   }   
   
   public int getCategoryLength()
   {
      if(specificpermission!=null)
      {
         return(specificpermission.getCategoryLength());
      }
      return(defaultpermission.getCategoryLength());
   }   
   
   public String getCategoryLabel(int i)
   {
      if(specificpermission!=null)
      {
         return(specificpermission.getCategoryLabel(i));
      }
      return(defaultpermission.getCategoryLabel(i));
   }   
   
   public int getModuleLength(int i)
   {
      if(specificpermission!=null)
      {
         return(specificpermission.getModuleLength(i));
      }
      return(defaultpermission.getModuleLength(i));
   }   
   
   public String getModuleName(int i, int j)
   {
      if(specificpermission!=null)
      {
         return(specificpermission.getModuleName(i, j));
      }
      return(defaultpermission.getModuleName(i, j));
   }  
   
   public String getModuleLabel(int i, int j)
   {
      if(specificpermission!=null)
      {
         return(specificpermission.getModuleLabel(i, j));
      }
      return(defaultpermission.getModuleLabel(i, j));
   }    
     
   
   public int getPermissionLength()
   {
      if(specificpermission!=null)
      {
         return(specificpermission.getPermissionLength());
      }
      return(defaultpermission.getPermissionLength());
   }   
   
   public String getPermissionLabel(int i)
   {
      if(specificpermission!=null)
      {
         return(specificpermission.getPermissionLabel(i));
      }
      return(defaultpermission.getPermissionLabel(i));
   }  
   
   public String getPermissionName(int i)
   {
      if(specificpermission!=null)
      {
         return(specificpermission.getPermissionName(i));
      }
      return(defaultpermission.getPermissionName(i));
   }     
}
