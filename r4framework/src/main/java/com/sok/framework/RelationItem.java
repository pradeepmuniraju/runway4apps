package com.sok.framework;
public class RelationItem
{
	String fromtable;
	String totable;
	
	public RelationItem(String fromtable, String totable)
	{
		this.fromtable = fromtable;
		this.totable = totable;
	}
	
	public String getFromTable()
	{
		return(fromtable);	
	}

	public String getToTable()
	{
		return(totable);	
	}
		
}
