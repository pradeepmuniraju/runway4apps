package com.sok.framework;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

public class InitServlet extends HttpServlet {
	private static final Logger logger = LoggerFactory.getLogger(InitServlet.class);
    static Properties sysProps = null;
    private DaemonFactory daemonFactory = null;
    protected static CacheManager cacheManager = null;
    protected static ServletConfig servletconfig;
    protected static String realpath = "";
    protected static String contextPath = "";
    
    protected static Date date = new Date();

    public static Properties getSystemParams() {
        return sysProps;
    }

    public static String getSystemParam(String propertyName) {
    	return sysProps != null ?  sysProps.getProperty(propertyName) : null;
    }
    
    public static String getSystemParam(String propertyName, String defaultValue) {
    	return sysProps != null ? sysProps.getProperty(propertyName, defaultValue) : defaultValue;
    }
    
    public static void reloadSystemParams(ServletContext context) {
        sysProps = SystemParameterFactory.buildSystemParameters(context);
    }

    @Override
	public void destroy() {
    	logger.info("destroying initservlet");
		if(cacheManager != null) { 
			try { 
				cacheManager.shutdown();
			} catch (Exception e) { 
				 logger.error("InitServlet:destroy() - error shutting down cache", e);
			}
		}
		servletconfig = null;
		SpecManager.setServletContext(null);
		super.destroy();
	}

	public void init(ServletConfig config) {
        try {
            servletconfig = config;
            ServletContext servletcontext = config.getServletContext();

            //contextPath = servletconfig.getServletContext().getContextPath();

            realpath = servletcontext.getRealPath("/");
            //servletcontext.getSessionCookieConfig().setHttpOnly(false);
            //servletcontext.getSessionCookieConfig().setSecure(true);
            //if (StringUtils.isNotBlank(contextPath))
            //    servletcontext.getSessionCookieConfig().setPath(contextPath);
            //else
            //	  servletcontext.getSessionCookieConfig().setPath("/");
            VelocityManager.setEngine(realpath);
            SpecManager.setServletContext(servletcontext);
            ActionBean.setJndiName(servletcontext.getInitParameter("sqlJndiName"));
            ActionBean.setDatabase(servletcontext.getInitParameter("sqlDatabase"));
            if (servletcontext.getInitParameter("locale_language") != null) {
                ActionBean.setLocale(servletcontext.getInitParameter("locale_language"), servletcontext.getInitParameter("locale_country"));
            }
            reloadSystemParams(servletcontext);

            daemonFactory = DaemonFactory.getDaemonFactory();
            daemonFactory.init(config);

        }
        catch (Exception e) {
        	logger.error("InitServelet: could not create initialise: ", e);
        }
        try { 
        	java.io.File f = new java.io.File(realpath + "/WEB-INF/ehcache.xml"); 
        	if(f.exists()) { 
        		cacheManager = CacheManager.create(new java.io.FileInputStream(f)); 
        	} else {
        		logger.info("Did not initialise cache - cache config did not exist");
        	}
        } catch (Exception e) { 
        	logger.error("Initservlet: could not initialise cache", e);
        }
    }

    public static ServletConfig getConfig() {
        return (servletconfig);
    }

    public static String getRealPath() {
        return (realpath);
    }
    public static String getRealPath(String s) {
    	// Dion: added this so that we can add the context path and not have it double up
    	if (s != null && contextPath != null && s.startsWith(contextPath)) s = s.substring(contextPath.length());
    	
    	return servletconfig.getServletContext().getRealPath(s);
    }
    
    public static Cache getCache(String cachename) { 
    	if(cacheManager != null) { 
    		return cacheManager.getCache(cachename);
    	} 
    	return null;
    }
    
    public static CacheManager getCacheManager() { 
    	return cacheManager;
    }
    
    public DaemonFactory getDaemonFactory() {
        return daemonFactory;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException,
            IOException {
    }
    
    public static long getUpTime() {
    	return (new Date()).getTime() - date.getTime();
    }

}