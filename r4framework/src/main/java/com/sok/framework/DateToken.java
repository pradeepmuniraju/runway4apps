package com.sok.framework;
import java.util.*;
import java.text.SimpleDateFormat;
import com.sok.runway.crm.Holiday; 

public class DateToken
{
	Calendar calendar = new GregorianCalendar();
	
	SimpleDateFormat formater;

	public void setDate(Date date) {
	   if(date!=null){
		   getCalendar().setTime(date);
	   }
	}
   
   public Calendar getCalendar() {
	   if (calendar == null) {
	      calendar = new GregorianCalendar();
	   }
      return calendar;
   }
   
	public Date getDate() {
		return getCalendar().getTime();
	}
	
	public DateToken addDays(int days) {
	   getCalendar().add(Calendar.DATE, days);
	   return this;
	}
	
	public DateToken addMonths(int months) {
	   getCalendar().add(Calendar.MONTH, months);
	   return this;
	}
	
	public DateToken addYears(int years) {
	   getCalendar().add(Calendar.YEAR, years);
	   return this;
	}
	
	public String format(String format)
	{
		if(formater == null) {
			formater = new SimpleDateFormat(format);
		}
		else {
		   formater.applyPattern(format);
		}
		return(formater.format(getDate()));
	}
	
	//Potential to draw this value from a setup record for the company. 
	public DateToken addBusinessDays(int days) {
	   return addBusinessDays(days,Holiday.AUS_VIC);
	}	
	
	public DateToken addBusinessDays(int days, String state) {
		return addBusinessDays(days,state,Holiday.AUSTRALIA);
	}
	
	public DateToken addBusinessDays(int days, String state, String country) {
		if("UK".equals(country)) { 
			return addBusinessDays(days, state, Holiday.UK);
		} else if("AUS".equals(country)) { 
			return addBusinessDays(days, state, Holiday.AUSTRALIA);
		} 
		//Return without adding business days
		return this; 
	}
	
	public DateToken addBusinessDays(int days, String state, int country) {
		for (int i = 0; i< days; i++) {
			getCalendar().add(Calendar.DATE, 1);
			while(!Holiday.isBusinessDay(state, country, getCalendar())) { 
				getCalendar().add(Calendar.DATE, 1);
			}
		}
		return this; 
	}
}
