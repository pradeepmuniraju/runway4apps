package com.sok.framework;

import javax.servlet.http.HttpServletRequest;


public class MultiField
{
   private static String ENCODED_PLUS = "*#PLUS#*";
   
	String[] fielditems = null;
	
	public MultiField(Object field) {
	   if (field instanceof String[]) {
	      this.fielditems = (String[])field;
	   }
	   else if (field instanceof String) {
	      if (((String)field).length() == 0) {
	         fielditems = new String[0];
	      }
	      else {
	         fielditems = encodeLegitPluses((String)field).split("\\+");
	      }
	   }
	   else {
	      fielditems = new String[0];
	   }
	}
	
	public MultiField(String field)
	{
	   if (field != null) {
	      if (field.length() == 0) {
	         fielditems = new String[0];
	      }
	      else {
   	      field = encodeLegitPluses(field);
			int s = 0;
			while (field.indexOf("+",s) >= 0) {
				if (field.indexOf("+ ",s) != field.indexOf("+",s)) {
					s = field.indexOf("+",s);
					int n = s + 1;
					field = field.substring(0,s) + "||" + field.substring(n);
					s = n;
				} else {
					s = field.indexOf("+",s) + 1;
				}
			}
   		   fielditems = field.split("\\|\\|");
   		}
	   }
	}
    
    public MultiField(String[] fielditems)
    {
        this.fielditems = fielditems;
    }    
    
    public MultiField(int size)
    {
        this.fielditems = new String[size];
    }        
    
    public MultiField(HttpServletRequest request, String viewspec, String name, String value, String itemname)
    {
        RowSetBean selectedlist = new RowSetBean();
        selectedlist.setViewSpec(viewspec);
        selectedlist.setRequest(request);
        selectedlist.setColumn(name, value);
        selectedlist.generateSQLStatement();
        selectedlist.getResults(true);   

        int sucount = selectedlist.getSize();
        fielditems = new String[sucount];
        for(int i=0; i<sucount; i++)
        {
            selectedlist.getNext();
           fielditems[i] = selectedlist.getString(itemname);
        }
        selectedlist.close();  
    }
    
    public void set(int i, String value)
    {
        fielditems[i] = value;
    }    
    
   // <NBCHANGE - 300503 - 1>
   /**
     * Method to return the length of the array, i.e. the number of 
     * fields extracted form the string
	 * @return the number of fields in the multifield
     */
	public int length(){
		if(null == fielditems)
		   return 0;
		return fielditems.length;
	}
   // </NBCHANGE - 300503 - 1>
	
	public String get(int i)
	{
	   return get(i, false);
	}
	   
	public String get(int i, boolean search)
	{
		if(fielditems!=null)
		{
		   String item = decodePluses(fielditems[i]);		   
		   if (search) { //Strip leading and trailing %s
		      item = item.substring(1, item.length() -1);
		   }
			return(item);	
		}
		return(null);
	}

	public boolean contains(String s)
	{
		return(contains(s,false));
	}
	
	public boolean contains(String s, boolean search)
	{
	   String searchString = encodePluses(s);
		if(fielditems!=null)
		{
			for(int i=0; i< fielditems.length; i++)
			{
				if(search)
				{  // ignore leading and trailing %s
					if(fielditems[i].indexOf(searchString)==1 && fielditems[i].length() - 2 == searchString.length())
					{
						return(true);
					}
				}
				else
				{
					if(fielditems[i].equalsIgnoreCase(searchString))
					{
						return(true);	
					}
				}
			}
		}
		return(false);
	}

   public String toString() {
      return compress(fielditems);
   }
   
	public static String compress(String[] fields)
	{
		StringBuffer temp = new StringBuffer();
		if(fields!=null)
		{
			for(int i=0; i< fields.length;)
			{
				if(!fields[i].equals(""))
				{
                                   	if(temp.length()>0)
					{
						temp.append("+");
					}
					temp.append(fields[i]);
					i++;

				}
				else
				{
					i++;
				}			
			}		
			return(temp.toString());
		}
		return("");
	}
	
	public static String markup(String field)
	{
		StringBuffer temp = new StringBuffer();
		if(field!=null)
		{
		   field = encodeLegitPluses(field);
		   
		   String[] fielditems = null;
		   fielditems = field.split("\\+");
		   
			for(int i=0; i< fielditems.length; i++) 
			{
				if(i > 0)
				{
					temp.append("<br>");
				}
            temp.append(decodePluses(fielditems[i]));	

			}		
			return(temp.toString());
		}
		return("");
	}	
	
	public static String encodeLegitPluses(String field) {
	   if (field.startsWith("+")) {
	      field = ENCODED_PLUS + field.substring(1);
	   }
	   if (field.endsWith("+")) {
	      field = field.substring(0,field.length()-1) + ENCODED_PLUS;
	   }
	   return StringUtil.replace(StringUtil.replace(field,"++", ENCODED_PLUS + "+")," + ", ENCODED_PLUS);
	}
	
	public static String encodePluses(String field) {
	   return StringUtil.replace(field,"+",ENCODED_PLUS);
	}
	
	public static String decodePluses(String field) {
	   return StringUtil.replace(field,ENCODED_PLUS,"+");
	}
}
