package com.sok.framework;

import org.dom4j.*;

public class RelationSpecPart extends XMLSpec
{
   private Element[] relations;
   private String[] conditions;
   private Element[][] ons;
   private int indexoffset = 0;
   
    public RelationSpecPart(String path, boolean ignoreerror)    
    {   
        super(path, ignoreerror);
        super.name = RelationSpec.NAME; 
        if(super.document!=null)
        {
           Element root = super.document.getRootElement();
           if(root!=null)
           {
               relations = super.getElementArray(root,RelationSpec._relation);
               if(relations!=null)
               {
                  loadConditions();
                   loadOns();
               }
           }
           else
           {
               System.out.print("Could not find root element in document - ");
               System.out.println(path);
           }               
        }
    }
    
    public void setIndexOffset(int i)
    {
       indexoffset = i;
    }
    
    protected void loadConditions()
    {
       conditions = new String[relations.length];
       for(int i=0; i<conditions.length; i++)
       {
          conditions[i] = getFirstElementText(relations[i],RelationSpec._condition);
       }
    }
    
    protected void loadOns()
    {
       ons = new Element[relations.length][];
       for(int i=0; i<relations.length; i++)
       {
          ons[i] = getElementArray(relations[i], RelationSpec._on);
       }
    }

    public synchronized void addRelation(String relationname, String fromtable, String totable, String fromkey, String tokey, String relationtype, String condition)
    {
        Element root = super.document.getRootElement();
        root.addText(_t);
        Element node = root.addElement(RelationSpec._relation);
    
        node.addAttribute(RelationSpec._relationname, relationname);
        node.addAttribute(RelationSpec._fromtable, fromtable);
        node.addAttribute(RelationSpec._totable, totable);
        node.addAttribute(RelationSpec._fromkey, fromkey);
        node.addAttribute(RelationSpec._tokey, tokey);
        node.addAttribute(RelationSpec._relationtype, relationtype);

        if(condition!=null && condition.length()!=0)
        {
            node.addText(_ntt);
            Element cnode = node.addElement(RelationSpec._condition);
            cnode.addText(_nttt);
            cnode.addText(condition);
            cnode.addText(_ntt);
            node.addText(_nt);
        }

        root.addText(_n);
        
        if(relations==null)
        {
            resetCache();
        }
    }

    public synchronized void resetCache()
    {
        Element root = super.document.getRootElement();
        relations = super.getElementArray(root,RelationSpec._relation);
        conditions = new String[relations.length];
    }
    
    public int getLength()
    {
        return(super.getCount(relations));
    }
    
    public boolean hasRelationshipWith(String fromtable, String totable)
    {
        String fromtablename = null;
        String totablename = null;
        for(int i=0; i<getLength(); i++)
        {
            fromtablename = getFromTableName(i);
            if(fromtablename.equals(fromtable))
            {
                totablename = getToTableName(i);
                if(totablename.equals(totable))
                {
                    return(true);
                }
            }
        }
        return(false);
    }


    
    public int getIndex(String name)
    {
        if(name!=null)
        {
            String tname = name;
            String iname = null;
            int hashindex = tname.indexOf('#');
            if(hashindex>0)
            {
                tname = tname.substring(0,hashindex);
            }

            for(int i=0; i<getLength(); i++)
            {
                iname = getName(i + indexoffset);
        
                if(iname!=null && iname.equals(tname))
                {
                    //System.out.print("found "+iname+" = "+name);
                    //System.out.println(i);
                    return(i + indexoffset);
                }
            }
        }
        return(-1);
    }

    public boolean hasRelation(String name)
    {
        if(getIndex(name) == -1)
        {
            return(false);
        }
        return(true);
    }
    
    public String getName(int i)
    {
        return(super.getAttribute(relations,i - indexoffset,RelationSpec._relationname));
    }

    public String getType(int i)
    {
        return(super.getAttribute(relations,i - indexoffset,RelationSpec._relationtype));
    }
    
    public String getToTableName(int i)
    {
       String name = super.getAttribute(relations,i - indexoffset,RelationSpec._totable);
       return SpecManager.getName(name);
    }
    
    public String getToTableSpecLocation(int i)
    {
        return(super.getAttribute(relations,i - indexoffset,RelationSpec._totable));
    }

    public String getFromTableName(int i)
    {
       String name = super.getAttribute(relations,i - indexoffset,RelationSpec._fromtable);
       return SpecManager.getName(name);
    }

    public String getFromTableSpecLocation(int i)
    {
        return(super.getAttribute(relations,i - indexoffset,RelationSpec._fromtable));
    }
    
    public String getToKey(int i)
    {
        return(super.getAttribute(relations,i - indexoffset,RelationSpec._tokey));
    }
    
    public String getFromKey(int i)
    {
        return(super.getAttribute(relations,i - indexoffset,RelationSpec._fromkey));
    }   

    public String getCondition(int i)
    {
       int ii =  i - indexoffset;
       if(conditions[ii]!=null)
       {
           return(conditions[ii]);
       }
       return(null);
    }
    
    /**
     * @deprecated
     */ 
    public String getCondition(int i, int j)
    {
       return(getCondition(i));
    }

    public int getConditionLength(int i)
    {
        String condition = getCondition(i);
        
        if(condition!=null)
        {
            return(1);
        }
        return(0);
    }

    protected Element[] getOnsForRelation(int i)
    {
       return(getElementArray(relations[i - indexoffset], RelationSpec._on));
    }
    
    public String getOnToKey(int i, int j)
    {
       int ii =  i - indexoffset;
        if(ons[ii]!=null)
        {
            return(getAttribute(ons[ii], j, RelationSpec._tokey));
        }
        return(null);
    }
    
    public String getOnFromKey(int i, int j)
    {
       int ii =  i - indexoffset;
       if(ons[ii]!=null)
       {
           return(getAttribute(ons[ii], j, RelationSpec._fromkey));
       }
       return(null);
    }
    
    public String getOnKeyValue(int i, int j)
    {
       int ii =  i - indexoffset;
       if(ons[ii]!=null)
       {
           return(getAttribute(ons[ii], j, RelationSpec._keyvalue));
       }
       return(null);
    }     
    
    public String getOnKeyValueName(int i, int j)
    {
       int ii =  i - indexoffset;
       if(ons[ii]!=null)
       {
           return(getAttribute(ons[ii], j, RelationSpec._keyvaluename));
       }
       return(null);
    }    

    public int getOnLength(int i)
    {
       int ii =  i - indexoffset;
        if(ons[ii]!=null)
        {
            return(ons[ii].length);
        }
        return(0);
    }
    
    public int getPriority(int i)
	{
    	int priority = 0;
    	String attr = super.getAttribute(relations,i - indexoffset,RelationSpec._priority);
    	if(attr!=null)
    	{
    		try{
    			priority = Integer.parseInt(attr);
    		}catch(Exception e){}
    	}
    	return(priority);
	}
    
}
