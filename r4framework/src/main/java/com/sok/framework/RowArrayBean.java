package com.sok.framework;

import java.sql.*;
import java.util.*;


public class RowArrayBean extends RowBean implements TableDataSet
{
	ResultSet result = null;
	ResultSetMetaData meta = null;

	Statement stmt = null;

	ArrayList list = null;

	void setArrayList(ArrayList list)
	{
		this.list = list;
	}

	public ArrayList getList()
	{
		return(list);
	}

	public void setStatement(Statement stmt)
	{
		this.stmt = stmt;
	}

	public void setResultSet(ResultSet current)
	{
		result = current;
	}

	public boolean getResults()
	{
        boolean success = false;
		if(searchstmt==null)
		{
			ActionBean.generateSQLStatment(this);
		}
		if(searchstmt!=null)
		{
			try
			{

				ActionBean.retrieveResultSet(this, false);
                success = ActionBean.initFromDB(result,this);
			}
			catch(Exception e)
			{
				errors.append(e.toString());
				errors.append(ActionBean.RN);
			}
			finally
			{
 				close();
			}
		}
		return(success);
	}

	public void add(TableData row)
	{
		list.add(row);
	} 

	public TableData get(int index)
	{
		return((TableData)list.get(index));
	}

	public int find(String name, String value, int start)
	{
		TableData item = null;
		String ivalue = null;
		for(int i=start; i<getSize(); i++)
		{
			item = (TableData)list.get(i);
			ivalue = item.getString(name);
			if(ivalue!=null && ivalue.equals(value))
			{
				return(i);
			}
		}
		return(-1);
	}

	public TableData get(String name, String value)
	{
		return(get(name, value, 0));
	}

	public TableData get(String name, String value, int start)
	{
		int index = find(name, value, start);
		if(index > -1)
		{
			return((TableData)list.get(index));
		}
		return(null);
	}

	public int getSize()
	{
		if(list!=null)
		{
			return(list.size());
		}
		return(0);
	}

	public void close()
	{
		if(stmt!=null)
		{
			try{
				stmt.close();
			}catch(Exception e){}
		}
		if(result!=null)
		{
			try{
				result.close();
			}catch(Exception e){}
		}
		super.close();
	}
	
	public void clearList()
	{
		if(list!=null)
		{
			list.clear();
		}
	}
	
	public void clear()
	{
		clearList();
		super.clear();
	}
}
