package com.sok.framework;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class SecureFilter implements Filter
{
    String realm1;
    String realm2;
    String realm3;  
    String realm4;    
    String escape;
    String localnetwork;
    
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws ServletException, IOException
	{
		HttpServletRequest httprequest = (HttpServletRequest)request;
		String requesturi = httprequest.getRequestURI();
		if(realm1!=null && requesturi.indexOf(realm1)>=0)
		{
            filter(httprequest, requesturi, response, chain);
		}
        else if(realm2!=null && requesturi.indexOf(realm2)>=0)
        {
            filter(httprequest, requesturi, response, chain);
        }
        else if(realm3!=null && requesturi.indexOf(realm3)>=0)
        {
            filter(httprequest, requesturi, response, chain);
        } 
        else if(realm4!=null && requesturi.indexOf(realm4)>=0)
        {
            filter(httprequest, requesturi, response, chain);
        }          
		else
		{
			chain.doFilter(request,response);
		}
	}
	
    protected void filter(HttpServletRequest httprequest, String requesturi, ServletResponse response, FilterChain chain)throws ServletException, IOException
    {
        if(httprequest.isSecure() || 
        	(escape!=null && requesturi.indexOf(escape)>=0) ||
        	(localnetwork!=null && httprequest.getRemoteAddr().indexOf(localnetwork)>=0))
        {
            chain.doFilter(httprequest,response);
        }
        else
        {
            bounce(httprequest, response);
        }        
    }
    
	public void bounce(ServletRequest request, ServletResponse response)throws IOException
	{
		StringBuffer bounceurl = new StringBuffer();
        bounceurl.append("https://");
		bounceurl.append(((HttpServletRequest)request).getServerName());
		bounceurl.append(((HttpServletRequest)request).getRequestURI());
        String query = ((HttpServletRequest)request).getQueryString();
        if(query!=null && query.length()!=0)
        {
            bounceurl.append("?");
            bounceurl.append(query);
        }
		((HttpServletResponse)response).sendRedirect(bounceurl.toString());
	}
	
	public void init(FilterConfig config)throws ServletException
	{
        realm1 = config.getInitParameter("realm1");
        realm2 = config.getInitParameter("realm2");
        realm3 = config.getInitParameter("realm3");
        realm4 = config.getInitParameter("realm4");        
		escape = config.getInitParameter("escape");
		localnetwork = config.getInitParameter("localnetwork");
	}
	
	public void destroy()
	{
		
	}
}
