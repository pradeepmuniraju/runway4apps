package com.sok.framework.googlemap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

public class HomeMapAction {

	public String getGeocoderByDesign(HttpServletRequest request) {
		String productID = request.getParameter("ProductID");
		if (productID != null && productID.length() > 0) {
			productID = productID.replaceAll("\\|", "+");
		} else {
			productID = "";
		}
		if (productID == null || productID.length() == 0) {
			return "({\"ProductID\": \"" + productID + "\", \"product_count\": 0, \"product_found\": 0, \"products\": []})";
		}
		GenRow products = new GenRow();
		products.setRequest(request);
		products.setViewSpec("properties/ProductHouseAndLandView");
		products.setParameter(
				"ProductProductProducts#House.ProductProductsLinkedProductHouse.ProductParentHomeDesign.ProductVariationHomeDesign.ProductID",
				productID);
		products.setParameter("DisplayFlag", "Y");
		products.setParameter("-groupby0", "EstateProductID");

		products.setAction(GenerationKeys.SEARCH);

		products.doAction();
		products.getResults();
		Map<String, String> map = new HashMap<String, String>();
		while (products.getNext()) {
			if (products.getData("EstateProductID").length() > 0) {
				map.put(products.getData("EstateProductID"),
						products.getData("EstateName"));
			}
		}
		StringBuilder responseBuilder = new StringBuilder();
		GoogleMap gmap = new GoogleMap();
		int i = 0;
		int j = 0;
		for (Entry<String, String> entry : map.entrySet()) {
			GenRow estateAddress = new GenRow();
			estateAddress.setTableSpec("Addresses");
			estateAddress.setParameter("-select1", "*");
			estateAddress.setParameter("ProductID", entry.getKey());
			estateAddress.doAction("selectfirst");

			String streetName = estateAddress.getData("StreetNumber");
			String street = estateAddress.getData("Street");
			String street2 = estateAddress.getData("Street2");
			String city = estateAddress.getData("City");
			String state = estateAddress.getData("State");
			String postcode = estateAddress.getData("Postcode");
			String country = estateAddress.getData("Country");

			if (i != 0) {
				responseBuilder.append(",");
			}
			responseBuilder.append("{");

			String str = gmap.getAddressGeocode(streetName, street, street2,
					city, state, postcode, country);
			JSONObject json = (JSONObject) JSONValue.parse(str);

			if ("OK".equalsIgnoreCase((String) json.get("status"))) {
				responseBuilder.append("\"ProductID\": \"" + productID + "\", \"accuracy\": 1"
						+ ", \"productid\": \""
						+ entry.getKey()
						+ "\""
						+ ", \"lookupAddress\": \""
						+ streetName
						+ " "
						+ street
						+ " "
						+ street2
						+ " "
						+ city
						+ " "
						+ state
						+ " "
						+ postcode
						+ " "
						+ country
						+ "\""
						+ ", \"estate\": \""
						+ entry.getValue()
						+ "\""
						+ ", \"suburbName\": \""
						+ city
						+ "\""
						+ ", \"google\": [" + json.toJSONString() + "]");
				j++;
			} else {
				responseBuilder.append("\"ProductID\": \"" + productID + "\", \"accuracy\": -1"
						+ ", \"productid\": \""
						+ entry.getKey()
						+ "\""
						+ ", \"lookupAddress\": \""
						+ streetName
						+ " "
						+ street
						+ " "
						+ street2
						+ " "
						+ city
						+ " "
						+ state
						+ " "
						+ postcode
						+ " "
						+ country
						+ "\""
						+ ", \"estate\": \""
						+ entry.getValue()
						+ "\""
						+ ", \"suburbName\": \""
						+ city
						+ "\""
						+ ", \"google\": [" + json.toJSONString() + "]");
			}
			responseBuilder.append("}");
			i++;
		}

		String pjs = responseBuilder.toString();
		return "({\"ProductID\": \"" + productID + "\", \"product_count\": " + i + ", \"product_found\": " + j
				+ ", \"products\": [" + pjs + "]})";

	}

	public String getDisplayHomes(HttpServletRequest request) {
		String eid = request.getParameter("eproductid");
		String productID = request.getParameter("productid");
		if (productID == null || productID.length() == 0) return "";
		GenRow products = new GenRow();
		products.setRequest(request);
		products.setViewSpec("properties/ProductHouseAndLandView");
		if (productID != null && productID.length() > 0) {
			productID = productID.replaceAll(" ", "+");
			products.setParameter("HomeProductID", productID);
			products.setParameter("ProductProductProducts#House.ProductProductsLinkedProductHouse.ProductParentHomeDesign.ProductVariationHomeDesign.ProductID", productID);
		}
		products.setParameter(
				"EstateProductID",
				eid);
		products.setParameter(
				"ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ProductID",
				eid);
		products.setParameter("DisplayFlag", "Y");
		
		products.setParameter("-groupby0", "ProductID");
		
		products.sortBy("HomeName", 0);
		products.sortBy("PlanName", 1);

		products.setAction(GenerationKeys.SEARCH);

		products.doAction();
		products.getResults();

		StringBuilder responseBuilder = new StringBuilder();
		int i = 0;
		while (products.getNext()) {
			if (i != 0) {
				responseBuilder.append(",");
			}
			responseBuilder.append("{");

			responseBuilder.append("\"ProductID\": \""
					+ products.getData("ProductID") + "\""
					+ ", \"StreetNumber\": \""
					+ products.getData("StreetNumber") + "\""
					+ ", \"Street\": \"" + products.getData("Street") + "\""
					+ ", \"Street2\": \"" + products.getData("Street2") + "\""
					+ ", \"City\": \"" + products.getData("City") + "\""
					+ ", \"State\": \"" + products.getData("State") + "\""
					+ ", \"Postcode\": \"" + products.getData("Postcode")
					+ "\"" + ", \"PlanName\": \""
					+ products.getData("PlanName") + "\""
					+ ", \"PlanTotalCost\": \""
					+ products.getData("PlanTotalCost") + "\""
					+ ", \"HomeName\": \"" + products.getData("HomeName")
					+ "\"" + ", \"HomeProductID\": \""
					+ products.getData("HomeProductID") + "\""
					+ ", \"RangeName\": \"" + products.getData("RangeName")
					+ "\"" + ", \"RangeProductID\": \""
					+ products.getData("RangeProductID") + "\""
					+ ", \"BrandName\": \"" + products.getData("BrandName")
					+ "\"" + ", \"BrandProductID\": \""
					+ products.getData("BrandProductID") + "\"");

			responseBuilder.append("}");
			i++;
		}

		String pjs = responseBuilder.toString();
		return "({\"product_count\": " + i + ", \"products\": [" + pjs + "]})";
	}

}
