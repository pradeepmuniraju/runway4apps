package com.sok.framework.googlemap;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

public class LandMapAction {

	private static final String OR = "+";

	public String getGeocoderByDesign(HttpServletRequest request) {
		String productID = request.getParameter("ProductID");
		if (productID != null && productID.length() > 0) {
			productID = productID.replaceAll("\\|", "+");
		} else {
			productID = "";
		}
		if (productID == null || productID.length() == 0) {
			return "({\"ProductID\": \"" + productID + "\", \"product_count\": 0, \"product_found\": 0, \"products\": []})";
		}
		
		GenRow products = new GenRow();
		products.setRequest(request);
		products.setViewSpec("properties/ProductLotView");

		products.setParameter("ProductID", productID);
		products.setParameter("-groupby0", "EstateProductID");

		products.setAction(GenerationKeys.SEARCH);

		products.doAction();
		products.getResults();
		//System.out.println(products.getStatement());
		Map<String, String> map = new HashMap<String, String>();
		while (products.getNext()) {
			if (products.getData("EstateProductID").length() > 0) {
				map.put(products.getData("EstateProductID"),
						products.getData("EstateName"));
			}
		}
		StringBuilder responseBuilder = new StringBuilder();
		GoogleMap gmap = new GoogleMap();
		int i = 0;
		int j = 0;
		for (Entry<String, String> entry : map.entrySet()) {
			GenRow estateAddress = new GenRow();
			estateAddress.setTableSpec("Addresses");
			estateAddress.setParameter("-select1", "*");
			estateAddress.setParameter("ProductID", entry.getKey());
			estateAddress.doAction("selectfirst");

			String streetName = estateAddress.getString("StreetNumber");
			String street = (estateAddress.getString("Street") + " " + estateAddress.getString("Street2")).trim();
			String city = estateAddress.getString("City");
			String state = estateAddress.getString("State");
			String postcode = estateAddress.getString("Postcode");
			String country = estateAddress.getString("Country");

			if (i != 0) {
				responseBuilder.append(",");
			}
			responseBuilder.append("{");
			
			
			String str = gmap.getAddressGeocode(streetName, street, "",
					city, state, postcode, country);
			JSONObject json = (JSONObject) JSONValue.parse(str);

			if ("OK".equalsIgnoreCase((String) json.get("status"))) {
				responseBuilder.append("\"ProductID\": \"" + productID + "\", \"accuracy\": 1"
						+ ", \"productid\": \""
						+ entry.getKey()
						+ "\""
						+ ", \"lookupAddress\": \""
						+ streetName
						+ " "
						+ street
						+ ", "
						+ city
						+ " "
						+ state
						+ " "
						+ postcode
						+ " "
						+ country
						+ "\""
						+ ", \"estate\": \""
						+ entry.getValue()
						+ "\""
						+ ", \"suburbName\": \""
						+ city
						+ "\""
						+ ", \"google\": [" + json.toJSONString() + "]");
				j++;
			} else {
				responseBuilder.append("\"ProductID\": \"" + productID + "\", \"accuracy\": -1"
						+ ", \"productid\": \""
						+ entry.getKey()
						+ "\""
						+ ", \"lookupAddress\": \""
						+ streetName
						+ " "
						+ street
						+ ", "
						+ city
						+ " "
						+ state
						+ " "
						+ postcode
						+ " "
						+ country
						+ "\""
						+ ", \"estate\": \""
						+ entry.getValue()
						+ "\""
						+ ", \"suburbName\": \""
						+ city
						+ "\""
						+ ", \"google\": [" + json.toJSONString() + "]");
			}
			responseBuilder.append("}");
			i++;
		}

		String pjs = responseBuilder.toString();
		return "({\"ProductID\": \"" + productID + "\", \"product_count\": " + i + ", \"product_found\": " + j
				+ ", \"products\": [" + pjs + "]})";

	}

	public String getLotDetails(HttpServletRequest request) {
		String eid = request.getParameter("eproductid");
		String pid = request.getParameter("productid");
		if (pid != null && pid.indexOf(" ") >= 0) pid = pid.replaceAll(" ", "+");
		GenRow products = new GenRow();
		products.setRequest(request);
		products.setViewSpec("properties/ProductLotView");
		products.setParameter("ProductID",pid);
		products.setParameter("EstateProductID",eid);
		products.setParameter("ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ProductID",eid);
		products.sortBy("Name", 0);

		products.setAction(GenerationKeys.SEARCH);

		products.doAction();
		products.getResults();

		StringBuilder responseBuilder = new StringBuilder();
		int i = 0;
		while (products.getNext()) {
			if (i != 0) {
				responseBuilder.append(",");
			}
			responseBuilder.append("{");
			
			String street = (products.getString("Street") + " " + products.getString("Street2")).trim();

			responseBuilder.append("\"ProductID\": \""
					+ products.getData("ProductID") + "\""
					+ ", \"StreetNumber\": \""
					+ products.getString("StreetNumber") + "\""
					+ ", \"Street\": \"" + street + "\""
					+ ", \"City\": \"" + products.getString("City") + "\""
					+ ", \"State\": \"" + products.getString("State") + "\""
					+ ", \"Postcode\": \"" + products.getString("Postcode")
					+ "\"" + ", \"LotName\": \""
					+ products.getString("Name") + "\""
					+ ", \"PlanTotalCost\": \""
					+ products.getString("PlanTotalCost") + "\""
					+ ", \"StageName\": \"" + products.getString("StageName")
					+ "\"" + ", \"HomeProductID\": \""
					+ products.getString("HomeProductID") + "\""
					+ ", \"EstateName\": \"" + products.getString("EstateName") + "\"");

			responseBuilder.append("}");
			i++;
		}

		String pjs = responseBuilder.toString();
		return "({\"product_count\": " + i + ", \"products\": [" + pjs + "]})";
	}

}
