package com.sok.framework.googlemap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.TableData;
import com.sok.framework.generation.GenerationKeys;

public class GoogleMap {
    
    /**
     * Returns a 2-element array containing the latitude and longitude of the given address
     * returned using Google Maps' geocoding service.
     *
     * In case of an error, returns null;
     *
     * When passed a null or empty address, returns null.
     *     
     * @param address An address.
     * @return The geocode for the address or null in case of an error.
     */
    public String getAddressGeocode(String address) {

        String jsonResponse = callGoogleGeocodeService(address);
        if (jsonResponse == null) {
            return null;
        } else {
            return jsonResponse;
        }
    }

    private String callGoogleGeocodeService(String address) {
        final String geocodeBaseUrl = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=";
        final StringBuilder geocodeUrl = new StringBuilder();
        geocodeUrl.append(geocodeBaseUrl);

        try {
            geocodeUrl.append(URLEncoder.encode(address, "UTF-8"));
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
            return null;
        }
	
	
        final HttpClient client = new HttpClient();
        final HttpMethod request = new GetMethod(geocodeUrl.toString());
        // need at timeout if it fails
        client.setTimeout(5000);
        
        String jsonResponse;

        try {
            int statusCode = client.executeMethod(request);
            if (statusCode != HttpStatus.SC_OK) {
                System.out.println("Error occurred while getting the geocode for address: " + request.getStatusLine());
                return null;
            }
            jsonResponse = request.getResponseBodyAsString(); 
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return null;
        } finally {
            request.releaseConnection();
        }
        return jsonResponse;
    }
    

    
    public String getAddressGeocode(String streetName, String street, String street2, String city, String state, String postcode, String country) {
        String address = streetName + " " + street + " " + street2 + " " + city + " " + state + " " + postcode + " " + country;
        String jsonResponse = callGoogleGeocodeService(address);
        if (jsonResponse == null) {
            return null;
        } else {
            return jsonResponse;
        }
    }
    
    public String getAddressGeocode(String country, String state, String city) {

        String address = country + " " + state + ", " + city;
        
        String jsonResponse = callGoogleGeocodeService(address);
        if (jsonResponse == null) {
            return null;
        } else {
            return jsonResponse;
        }
    }

    
    public static void main(String[] args) {
	GoogleMap map = new GoogleMap();
	map.getAddressGeocode("sadfsadfasdfsdfsa");
	System.out.println(map.getAddressGeocode("sadfsadfasdfsdfsa"));
    }
}
