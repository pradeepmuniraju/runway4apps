package com.sok.framework;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang.StringUtils;

public class StringUtil
{
	static final String[] hexStrings = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
			"0A", "0B", "0C", "0D", "0E", "0F", "10", "11", "12", "13", "14", "15", "16", "17",
			"18", "19", "1A", "1B", "1C", "1D", "1E", "1F", "20", "21", "22", "23", "24", "25",
			"26", "27", "28", "29", "2A", "2B", "2C", "2D", "2E", "2F", "30", "31", "32", "33",
			"34", "35", "36", "37", "38", "39", "3A", "3B", "3C", "3D", "3E", "3F", "40", "41",
			"42", "43", "44", "45", "46", "47", "48", "49", "4A", "4B", "4C", "4D", "4E", "4F",
			"50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5A", "5B", "5C", "5D",
			"5E", "5F", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6A", "6B",
			"6C", "6D", "6E", "6F", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79",
			"7A", "7B", "7C", "7D", "7E", "7F", "80", "81", "82", "83", "84", "85", "86", "87",
			"88", "89", "8A", "8B", "8C", "8D", "8E", "8F", "90", "91", "92", "93", "94", "95",
			"96", "97", "98", "99", "9A", "9B", "9C", "9D", "9E", "9F", "A0", "A1", "A2", "A3",
			"A4", "A5", "A6", "A7", "A8", "A9", "AA", "AB", "AC", "AD", "AE", "AF", "B0", "B1",
			"B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "BA", "BB", "BC", "BD", "BE", "BF",
			"C0", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "CA", "CB", "CC", "CD",
			"CE", "CF", "D0", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "DA", "DB",
			"DC", "DD", "DE", "DF", "E0", "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9",
			"EA", "EB", "EC", "ED", "EE", "EF", "F0", "F1", "F2", "F3", "F4", "F5", "F6", "F7",
			"F8", "F9", "FA", "FB", "FC", "FD", "FE", "FF"};

	public static final char[] filesystemchars = {'#', ';', ':', '?', '*', '/', '\\', '&', '|'};

	public static final char[] nottextchars = {' ', '\'', '\\', '>', '<', '%', '&', '{', '}', '?'};
	
    /** Ellipses appended on the end of truncated string {@see StringUtil#truncateString} */
    private static final String ELLIPSES = "...";
    
	public static String hexEncode(String textString)
	{
		//byte[] text = textString.getBytes();
		char[] text = textString.toCharArray();
		StringBuffer output = new StringBuffer();
		for(int i = 0; i < text.length; i++)
		{

			int temp = (int) text[i];
			if(temp < 0)
			{
				temp = (256 + (int) temp);
			}
			output.append(hexStrings[temp]);
		}
		return (output.toString());
	}

	public static String escapeSQLWildcards(String text)
	{
		char aChar;
		int len = text.length();
		if(len != 0)
		{
			StringBuffer outBuffer = new StringBuffer(len);

			for(int x = 0; x < len;)
			{
				aChar = text.charAt(x++);

				if(aChar == '%')
				{
					outBuffer.append("[%]");
				}
				else if(aChar == '_')
				{
					outBuffer.append("[_]");
				}
				else if(aChar == '[')
				{
					outBuffer.append("[[]");
				}
				else
				{
					outBuffer.append(aChar);
				}
			}
			return(outBuffer.toString());
		}
		else
		{
			return(text);
		}
	}
	
	public static String hexDecode(String textString)
	{
		int count = textString.length() / 2;
		//byte[] output = new byte[count];
		char[] output = new char[count];
		for(int i = 0; i < count; i++)
		{
			String temp = textString.substring((i * 2), (i * 2) + 2);
			int num = Integer.parseInt(temp, 16);
			if(num > 127)
			{
				num = num - 256;
			}
			output[i] = (char) num;
		}
		return (new String(output));
	}

	public static String toHTML(String theString)
	{

		char aChar;
		int len = theString.length();
		if(len != 0)
		{
			StringBuffer outBuffer = new StringBuffer(len);

			for(int x = 0; x < len;)
			{
				aChar = theString.charAt(x++);

				if(aChar == '<')
				{
					outBuffer.append("&lt;");
				}
				else if(aChar == '>')
				{
					outBuffer.append("&gt;");
				}
				else if(aChar == '\n')
				{
					outBuffer.append("<br/>");
				}
				else if(aChar == '`')
				{
					outBuffer.append("&acute;");
				}
				else if(aChar == '\'')
				{
					outBuffer.append("&#39;");
				}
				else if(aChar == '"')
				{
					outBuffer.append("&quot;");
				}
				else
				{
					outBuffer.append(aChar);
				}
			}
			return outBuffer.toString();
		}
		else
		{
			return (theString);
		}
	}
	
   public static final Pattern htmlregex = Pattern.compile("(<([^>]+)>)");
   public static final Pattern newlineregex = Pattern.compile("\r|\n");
   public static final Pattern pbrregex = Pattern.compile("(<((br|p)\\/{0,1})>)");

	public static String htmlToText(String oldString)
	{
		//kill the comments, replace <br /> with <br/>
      Matcher matcher = newlineregex.matcher(StringUtil.replace(oldString.replaceAll("(?s)<!--.*?-->", ""), "<br />", "<br/>"));   //pls fix if your regex is better than mine
      String newString = matcher.replaceAll(""); 
      matcher = pbrregex.matcher(newString);
      newString = matcher.replaceAll("\r\n");
      matcher = htmlregex.matcher(newString);
      newString = matcher.replaceAll("");
      return(newString);   
	}
	
   public static final Pattern blockhtmlregex = Pattern.compile("(<(\\/{0,1}(br|li|p|div|td)\\/{0,1})>)");   
   public static String convertHtmlToText(String oldString)
   {
	   Matcher matcher = newlineregex.matcher(StringUtil.replace(oldString, "<br />", "<br/>"));	//pls fix if your regex is better than mine
      String newString = matcher.replaceAll(""); 
      matcher = blockhtmlregex.matcher(newString);		//different from above method
      newString = matcher.replaceAll("\r\n");      
      matcher = htmlregex.matcher(newString);
      newString = matcher.replaceAll("");
      
      return(newString);       
   }
   
   public static String stripBreaks(String oldString) 
   {
	   return  newlineregex.matcher(oldString).replaceAll(" ");
   }
   
   public static String stripHtml(String oldString)
   {  
      Matcher matcher = htmlregex.matcher(oldString);
      return matcher.replaceAll("");      
   }  
   
	public static String ToAlphaNumeric(String theString)
	{
		char aChar;
		int len = theString.length();
		if(len != 0)
		{
			StringBuffer outBuffer = new StringBuffer(len);

			for(int x = 0; x < len;)
			{
				aChar = theString.charAt(x++);

				if(aChar >= '0' && aChar <= '9')
				{
					outBuffer.append(aChar);
				}
				else if(aChar >= 'A' && aChar <= 'Z')
				{
					outBuffer.append(aChar);
				}
				else if(aChar >= 'a' && aChar <= 'z')
				{
					outBuffer.append(aChar);
				}
			}
			return outBuffer.toString();
		}
		else
		{
			return (theString);
		}
	}

	public static String toNumeric(String theString)
	{
		char aChar;
		int len = theString.length();
		if(len != 0)
		{
			StringBuffer outBuffer = new StringBuffer(len);

			for(int x = 0; x < len;)
			{
				aChar = theString.charAt(x++);

				if(aChar >= '0' && aChar <= '9')
				{
					outBuffer.append(aChar);
				}
			}
			return outBuffer.toString();
		}
		else
		{
			return (theString);
		}
	}

	public static String toDecimal(String theString)
	{
		char aChar;
		int len = theString.length();
		if(len != 0)
		{
			StringBuffer outBuffer = new StringBuffer(len);

			for(int x = 0; x < len;)
			{
				aChar = theString.charAt(x++);

				if((aChar >= '0' && aChar <= '9') || aChar == '.'|| aChar == '-')
				{
					outBuffer.append(aChar);
				}
			}
			return outBuffer.toString();
		}
		else
		{
			return (theString);
		}
	}

	public static String replace(String theString, String oldString, String newSring)
	{
		if (oldString == null || newSring == null){
			return theString;
		}
		else if(theString != null)
		{
			int len = theString.length();

			if(len != 0 && theString.indexOf(oldString) > -1)
			{
				String aChar;
				StringBuffer outBuffer = new StringBuffer(theString);
				int oldLen = oldString.length();
				int newLen = newSring.length();

				int pos = 0;
				pos = outBuffer.toString().indexOf(oldString);
				while(pos > -1)
				{
					outBuffer = outBuffer.replace(pos, pos + oldLen, newSring);
					pos = outBuffer.indexOf(oldString, pos + newLen);
				}
				return outBuffer.toString();
			}
		}
		return theString;
	}

	public static String escapeParameter(String parameter)
	{
	   String escaped = replace(parameter,"+","++");
	   escaped = replace(parameter,";",";;");
	   return(escaped);
	}
	
	public static String replace(String theString, char oldchar, char newchar)
	{
		if(theString != null)
		{
			int len = theString.length();

			if(len != 0 && theString.indexOf(oldchar) >= 0)
			{
				char aChar;
				StringBuffer outBuffer = new StringBuffer(len);

				for(int x = 0; x < len;)
				{
					aChar = theString.charAt(x++);

					if(aChar == oldchar)
					{
						outBuffer.append(newchar);
					}
					else
					{
						outBuffer.append(aChar);
					}
				}
				return outBuffer.toString();
			}
		}
		return (theString);
	}

	public static String urlEncode(String theString)
	{
		try
		{
			return URLEncoder.encode(theString, "UTF-8");
		}
		catch(java.io.UnsupportedEncodingException e)
		{
			return null;
		}
	}

	public static String urlDecode(String theString)
	{
		try
		{
			return URLDecoder.decode(theString, "UTF-8");
		}
		catch(java.io.UnsupportedEncodingException e)
		{
			return null;
		}
	}

	public static void printHTML(String theString, JspWriter out, boolean encodelinebreak)
	{
		char aChar;
		int len = theString.length();
		for(int x = 0; x < len;)
		{
			aChar = theString.charAt(x++);
			try
			{
				if(aChar == '<')
				{
					out.print("&lt;");
				}
				else if(aChar == '>')
				{
					out.print("&gt;");
				}
				else if(encodelinebreak && aChar == '\n')
				{
					out.print("<br/>");
				}
				else if(aChar == '`')
				{
					out.print("&acute;");
				}
				else if(aChar == '\'')
				{
					out.print("&#39");
				}
				else if(aChar == '"')
				{
					out.print("&quot;");
				}
				else
				{
					out.print(aChar);
				}
			}
			catch(Exception e)
			{
			}
		}
	}
	
	public static void printHTML(String theString, JspWriter out, boolean encodelinebreak, int linelength)
	{
		char aChar;
		int strlen = theString.length();
		String printOut = null;
		
		if( strlen > linelength )
		{
			strlen = linelength + 3;
			printOut = (theString.substring(0, linelength)) + "...";   
		}
		else
		{
			printOut = theString;
		}
		
		for(int x = 0; x < strlen;)
		{
			aChar = printOut.charAt(x++);
			try
			{
				if(aChar == '<')
				{
					out.print("&lt;");
				}
				else if(aChar == '>')
				{
					out.print("&gt;");
				}
				else if(encodelinebreak && aChar == '\n')
				{
					out.print("<br/>");
				}
				else if(aChar == '`')
				{
					out.print("&acute;");
				}
				else if(aChar == '\'')
				{
					out.print("&#39");
				}
				else if(aChar == '"')
				{
					out.print("&quot;");
				}
				else
				{
					out.print(aChar);
				}
			}
			catch(Exception e)
			{
			}
		}
	}	

	public static String toHTMLQuotes(String theString)
	{
		char aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);

		for(int x = 0; x < len;)
		{
			aChar = theString.charAt(x++);

			if(aChar == '`')
			{
				outBuffer.append("&acute;");
			}
			else if(aChar == '\'')
			{
				outBuffer.append("&#39");
			}
			else if(aChar == '"')
			{
				outBuffer.append("&quot;");
			}
			else if(aChar == '\n')
			{
				outBuffer.append("<br/>");
			}
			else
			{
				outBuffer.append(aChar);
			}
		}
		return outBuffer.toString();
	}

	public static String filter(String s, char[] filter)
	{
		char c;
		int len = s.length();
		StringBuffer outBuffer = new StringBuffer(len);
		boolean pass = true;
		for(int x = 0; x < len; x++)
		{
			pass = true;
			c = s.charAt(x);
			for(int i = 0; i < filter.length; i++)
			{
				if(c == filter[i])
				{
					pass = false;
				}
			}
			if(pass)
			{
				outBuffer.append(c);
			}
		}
		return outBuffer.toString();
	}

	public static String toTextOnlyString(String theString)
	{
		return filter(theString, nottextchars);
	}

	public static String encodeHTML(String theString)
	{
		char aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);
		//  &, <, >,
		//with
		//  &amp;, &lt;, &gt;,
		for(int x = 0; x < len;)
		{
			aChar = theString.charAt(x++);

			if(aChar == '<')
			{
				outBuffer.append("&lt;");
			}
			else if(aChar == '>')
			{
				outBuffer.append("&gt;");
			}
			else if(aChar == '&')
			{
				outBuffer.append("&amp;");
			}
			else if(aChar == '"')
			{
				outBuffer.append("&quot;");
			}
			else
			{
				outBuffer.append(aChar);
			}

		}
		return outBuffer.toString();
	}

	public static String toXML(String theString)
	{
		char aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);
		//  &, <, >, cr
		//with
		//  &amp;, &lt;, &gt;, &#xD;
		for(int x = 0; x < len;)
		{
			aChar = theString.charAt(x++);
			if(aChar >= ' ' && aChar <= '}')
			{
				if(aChar == '<')
				{
					outBuffer.append("&lt;");
				}
				else if(aChar == '>')
				{
					outBuffer.append("&gt;");
				}
				else if(aChar == '&')
				{
					outBuffer.append("&amp;");
				}
				else if(aChar == '"')
				{
					outBuffer.append("&quot;");
				}
				else
				{
					outBuffer.append(aChar);
				}
			}
			else if(aChar == '\n')
			{
				outBuffer.append("&#xD; ");
			}
		}
		return outBuffer.toString();
	}

	public static String toTitleCase(Object value)
	{
		return toTitleCase((String) value);
	}

	public static String toTitleCase(String value)
	{
		if(value != null)
		{
			char[] chars = value.toCharArray();
			for(int i = 0; i < chars.length; i++)
			{
				if(i == 0)
				{
					if(Character.isLowerCase(chars[i]))
					{
						chars[i] = Character.toUpperCase(chars[i]);
					}
				}
				else
				{
					if(Character.isDigit(chars[i - 1]) || Character.isWhitespace(chars[i - 1]) || chars[i - 1] == '-')
					{
						if(Character.isLowerCase(chars[i]))
						{
							chars[i] = Character.toUpperCase(chars[i]);
						}
					}
					else if(Character.isUpperCase(chars[i]))
					{
						chars[i] = Character.toLowerCase(chars[i]);
					}
				}
			}
			return (new String(chars));
		}
		return ("");
	}

	private static final char escapeChar = '\\';

	public static String toJavascriptString(Object theString)
	{
		return (toJavascriptString((String) theString));
	}

	public static String toJavascriptString(String theString)
	{
		char aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);

		for(int x = 0; x < len;)
		{
			aChar = theString.charAt(x++);

			if(aChar == '\t')
			{
				outBuffer.append("\\t");
			}
			else if(aChar == '\r')
			{
				outBuffer.append("\\r");
			}
			else if(aChar == '\n')
			{
				outBuffer.append("\\n");
			}
			else if(aChar == '\'')
			{
				outBuffer.append("\\'");
			}
			else if((aChar == '"') || (aChar == '\"'))
			{
				outBuffer.append("\\\"");
			}
			else
			{
				outBuffer.append(aChar);
			}
		}
		return (outBuffer.toString());
	}

	public static String toUnformattedNum(String theString)
	{
		char aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);

		for(int x = 0; x < len;)
		{
			aChar = theString.charAt(x++);

			if(!Character.isDigit(aChar))
			{
				if(aChar == '.')
				{
					outBuffer.append('.');
				}
			}
			else
			{
				outBuffer.append(aChar);
			}
		}
		return outBuffer.toString();
	}

	public static String toShort(String value, int words)
	{
		StringBuffer temp = new StringBuffer();
		if(value != null && value.length() != 0)
		{
			StringTokenizer st = new StringTokenizer(value, " ", false);
			for(int i = 0; i < words; i++)
			{
				if(st.hasMoreTokens())
				{
					// this is neater and doesn't end in " ..."
					if (temp.length() > 0) temp.append(" ");
					temp.append(st.nextToken());
				}
				if(i == (words - 1))
				{
					if(st.hasMoreTokens())
					{
						temp.append("...");
					}
				}
			}
		}
		return (temp.toString());
	}

	public static String toShort(Object value, int words)
	{
		return (toShort((Object) value, words));
	}

	public static List toWordWrappedList(String value, int width)
	{
		ArrayList list = new ArrayList();
		StringBuffer tempBuff = null;
		if(value != null && value.length() != 0)
		{
			StringTokenizer st = new StringTokenizer(value, " ");
			tempBuff = new StringBuffer();
			String temp = null;
			while(st.hasMoreTokens())
			{
				temp = st.nextToken();
				if(tempBuff.length() == 0)
				{
					tempBuff.append(temp);
				}
				else if(tempBuff.length() + temp.length() + 1 <= width)
				{
					tempBuff.append(" ");
					tempBuff.append(temp);
				}
				else
				{
					list.add(tempBuff.toString());
					tempBuff = new StringBuffer();
					tempBuff.append(temp);
				}
			}
			if (tempBuff.length() > 0) {
			   list.add(tempBuff.toString());
		}
		}
		return (list);
	}

	public static String mask(String password)
	{
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < password.length(); i++)
		{
			sb.append("*");
		}
		return (sb.toString());
	}

	public static String toURL(String value)
	{
		String vweburl = value;
		if(value != null && value.length() != 0)
		{
			if(value.indexOf("http://") < 0 && value.indexOf("HTTP://") < 0)
			{
				vweburl = "http://" + value;
			}
		}
		return (vweburl);
	}

	public static int getPathDepth(String theString, String root)
	{
		char aChar;
		int len = theString.length();
		int depth = 0;

		for(int x = 0; x < len; x++)
		{
			aChar = theString.charAt(x);
			if(aChar == '/')
			{
				depth++;
			}
		}
		if(root != null && root.length() != 0 && depth != 0)
		{
			return (depth - 1);
		}
		return (depth);
	}

	public static int getPathDepth(String theString)
	{
		return (getPathDepth(theString, null));
	}

	public static String getPathParent(String theString)
	{
		return (theString.substring(0, theString.lastIndexOf('/') + 1));
	}

	public static String getFirstParent(String theString)
	{
		int index = theString.indexOf('/', 1);
		if(index < 0)
		{
			return (theString);
		}
		return (theString.substring(0, index));
	}

	public static String getNameFromPath(String theString)
	{
		return (theString.substring(theString.lastIndexOf('/') + 1, theString.length()));
	}

	public static String getPathSearch(String theString)
	{
		int depth = getPathDepth(theString);
		String[] paths = new String[depth];
		StringTokenizer st = new StringTokenizer(theString, "/");
		StringBuffer temp = new StringBuffer();

		while(st.hasMoreTokens())
		{
			temp.append("/");
			temp.append(st.nextToken());
			depth--;
			paths[depth] = temp.toString();
		}

		temp = new StringBuffer();

		for(int i = 0; i < paths.length; i++)
		{
			if(i != 0)
			{
				temp.append("+");
			}
			temp.append(paths[i]);
		}
		return (temp.toString());
	}

	public static String getReplacePathRoot(String pathstring, String oldroot, String newroot)
	{
		String pathwithoutroot = null;
		if(oldroot.length() != 0)
		{
			pathwithoutroot = pathstring.substring(oldroot.length() + 1, pathstring.length());
		}
		else
		{
			pathwithoutroot = pathstring;
		}
		StringBuffer temp = new StringBuffer();
		temp.append("/");
		temp.append(newroot);
		temp.append(pathwithoutroot);
		return (temp.toString());
	}

	public static String removeNonFileSystemCharacters(String theString)
	{
		return replace(filter(theString, filesystemchars)," ","_");
	}

	public static String trim(String s)
	{
		if(s == null)
		{
			return null;
		}
		StringBuffer buf = new StringBuffer(s);
		while(buf.length() > 0 && Character.isSpaceChar(buf.charAt(0)))
		{
			buf.deleteCharAt(0);
		}
		while(buf.length() > 0 && Character.isSpaceChar(buf.charAt(buf.length() - 1)))
		{
			buf.deleteCharAt(buf.length() - 1);
		}
		return buf.toString();
	}

	public static int occurrencesOf(String text, String s)
	{
		return (occurrencesOf(text, s, 0, 0));
	}

	static int occurrencesOf(String text, String s, int lastindex, int count)
	{
		lastindex = text.indexOf(s, lastindex + 1);
		if(lastindex >= 0)
		{
			count = count + 1;
			return (occurrencesOf(text, s, lastindex, count));
		}
		else
		{
			return (count);
		}
	}

    public static final String[] htmlblocktags = {
        "<br>",
        "<div>",
        "<p>",
        "<table>",
        "<tr>",
        "<td>",
        "<ol>",
        "<ul>",
        "<br/>"
    };
    
    public static boolean needsHTMLLinefeedEncoding(String value)
    {
        Matcher matcher = htmlregex.matcher(value);
        String matchstr = null;
        while(matcher.find())
        {
            matchstr = value.substring(matcher.start(), matcher.end());
            for(int i=0; i<htmlblocktags.length; i++)
            {
                if(matchstr.equalsIgnoreCase(htmlblocktags[i]))
                {
                    return(false);
                }
            }
        }
        return(true);
    }
    
    public static byte[] parseIPAddress(String address)
    {
    	byte[] ip = new byte[4];
    	StringTokenizer tk = new StringTokenizer(address, ".");
    	int i=0;
    	while(tk.hasMoreTokens())
    	{
    		ip[i++] = (byte)Short.parseShort(tk.nextToken());
    	}
    	return(ip);
    }
    
    public static String encodeHTMLLinefeeds(String value)
    {
        if(needsHTMLLinefeedEncoding(value))
        {
            String s = replace(value,"\r\n","<br/>");
            return(replace(s,"\n","<br/>"));
        }
        return(value);
    }    
    
    /**
     * Sorts an Array using a TreeMap. Could be modified to use a specific sorting method. 
     * @param array
     * @return sortedarray 
     */
    public static String[] sortArray(String[] array) { 
    	TreeMap<String,String> list = new TreeMap<String,String>();
    	for (String s: array) { 
    		list.put(s, s); 
    	}
    	
    	String[] sorted = new String[list.size()]; 
		
		Iterator<String> i = list.keySet().iterator();
		int ic = 0; 
		while(i.hasNext()) { 
			sorted[ic] = (String)i.next(); 
			ic++; 
		}
		return sorted; 
    }
  
    public static void main(String[] args)
    {
        String test = "<html><body>asd  \r\n dssad  <b>adsad</b> sad \r\n</body></html>";
        System.out.println(htmlToText(test));
        String test2 = "<html><body>asd<br>  \r\n dssad  <b>adsad</b> sad \r\n</body></html>";
        System.out.println(htmlToText(test2));        
    }
  
  
   public static String htmlEntityEncode(String theString) {
		int aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);
		
		for(int x = 0; x < len; x++){
			aChar = theString.charAt(x);
			
		   outBuffer.append('&');
		   outBuffer.append('#');
		   outBuffer.append(aChar);
		   outBuffer.append(';');
		}
		return outBuffer.toString();
   }
   
   /**
    * <p>Capitalizes the specified {@code text} by changing the first letter 
    * to title case. No other characters are changed.</p>
    * 
    * @param text to capitalise
    * @return the text with the first letter capitalised
    */
   public static String capitalize(String text) {
	   if (text == null) return null;
       return StringUtils.capitalize(text.toLowerCase());
   }
   
   /**
    * <p>Truncates the specified {@code text} to {@code maxLength}, 
    * with an ellipses "..." appended to the end. If the {@code text} 
    * has less characters than {@code maxLength}, then this method 
    * returns the original {@code text}.</p>
    * 
    * @param text a non-null string
    * @param maxLength
    * @return the truncated {@code text} up to {@code maxLength} 
    * characters with an ellipses "..." appended to the end, 
    * if {@code text} has more characters than {@code maxLength},
    * otherwise return the original {@code text}
    */
   public static String truncateString(String text, int maxLength) {
       String truncatedLabelText = text;
       
       if (text.length() > maxLength) {
           truncatedLabelText = text.substring(0, maxLength) + ELLIPSES;
       }
       
       return truncatedLabelText;
   }
   
   public static URL getJarURL(Class clazz) {
       URL clsUrl = clazz.getResource(clazz.getSimpleName() + ".class");
       if (clsUrl != null) {
           try {
               URLConnection conn = clsUrl.openConnection();
               if (conn instanceof JarURLConnection) {
                   JarURLConnection connection = (JarURLConnection) conn;
                   return connection.getJarFileURL();
               }
           }
           catch (IOException e) {
               throw new RuntimeException(e);
           }
       }
       return null;
   } 

   public static String getFileDate(String filename) {
	   File f = new File(filename);
	   
	   if (f == null) return "";
	   
	   return (new Date(f.lastModified())).toString();
   }
   
   public static boolean isBlankOrEmpty(String str) {
	   return StringUtils.isBlank(str);
	   //return (str == null)||(str.trim().length() == 0);
   }
   public static String getDefaultString(String str, String def) {
	   if(StringUtils.isNotBlank(str)) {
		   return str;
	   }
	   return def;
   }
   
   /**
    * Convenience method to allow passing of var-args instead of having to supply an array.
    * @param args
    * @return
    */
   public static String join(String... args) {
	   //NB, StringUtils 3.0 will allow direct use of var-args. 
	   return StringUtils.join(args); 
	   /*
	   if(args == null || args.length == 0) return "";
	   StringBuilder str = new StringBuilder(); 
	   for(String s: args) { 
		   str.append(s);
	   }
	   return str.toString();
	   */
   }

	/**
	 * @param input
	 * @return
	 */
	public static double getRoundedValue(String input) {
		double output = 0;

		if (StringUtils.isNotBlank(input)) {
			return getRoundedValue(Double.parseDouble(input.replaceAll("[^0-9\\.\\-]", "")));
		}

		return output;
	}

	public static double getRoundedValue(double input) {
		String roundingType = InitServlet.getSystemParam("RoundingType");
		String roundingPosition = InitServlet.getSystemParam("RoundingPosition");

		// By default floor it
		double output = Math.floor(input);

		if ("ceil".equals(roundingType)) {
			output = Math.ceil(input);

		} else if ("round".equals(roundingType)) {
			output = Math.round(input);
		}

		if (StringUtils.isNotBlank(roundingPosition)) {
			int position = Integer.parseInt(roundingPosition);
			output = output / position;

			roundingType = StringUtils.defaultString(roundingType, "floor");

			if ("ceil".equals(roundingType)) {
				output = Math.ceil(output);

			} else if ("floor".equals(roundingType)) {
				output = Math.floor(output);

			} else if ("round".equals(roundingType)) {
				output = Math.round(output);
			}

			output = output * position;
		}

		// Format
		return output;

	}

	public static String getRoundedFormattedValue(double input) {
		return getRoundedFormattedValue(input, true);
	}

	public static String getRoundedFormattedValue(String input) {
		return getRoundedFormattedValue(input, true);
	}

	public static String getRoundedFormattedValue(String input, boolean keepCents) {
		if (StringUtils.isNotBlank(input)) {
			return getRoundedFormattedValue(Double.parseDouble(input.replaceAll("[^0-9\\.\\-]", "")), keepCents);
		}

		return "";
	}

	public static String getRoundedFormattedValue(double input, boolean keepCents) {
		double output = getRoundedValue(input);
		final NumberFormat currency = NumberFormat.getCurrencyInstance();
		if (!keepCents) {
			currency.setMaximumFractionDigits(0);
		}
		return currency.format(output);
	}
	
    // adapted from post by Phil Haack and modified to match better
    public final static String tagStart=
        "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)\\>";
    public final static String tagEnd=
        "\\</\\w+\\>";
    public final static String tagSelfClosing=
        "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)/\\>";
    public final static String htmlEntity=
        "&[a-zA-Z][a-zA-Z0-9]+;";
    public final static Pattern htmlPattern=Pattern.compile(
      "("+tagStart+".*"+tagEnd+")|("+tagSelfClosing+")|("+htmlEntity+")",
      Pattern.DOTALL
    );

    /**
     * Will return true if s contains HTML markup tags or entities.
     *
     * @param s String to test
     * @return String without HTML
     */
    public static boolean isHtml(String s) {
        boolean ret=false;
        if (s != null) {
            ret=htmlPattern.matcher(s).find();
        }
        return ret;
    }

    public static String removeHtml(String s) {
        String ret = "";
        if (s != null) {
        	if (s.toLowerCase().indexOf("<body") != -1) s = s.substring(s.toLowerCase().indexOf("<body"));
        	if (s.toLowerCase().indexOf("</body>") != -1) s = s.substring(0, s.toLowerCase().indexOf("</body>"));
            ret = s.replaceAll(tagStart, "").replaceAll(tagEnd, "").replaceAll(tagSelfClosing, "")
            		.replaceAll("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">", "")
            		.replaceAll("&amp;", "&").replaceAll("&nbsp;", "").replaceAll("\\n\\n","");
        }
        return ret;
    }

}