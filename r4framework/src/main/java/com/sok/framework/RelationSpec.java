package com.sok.framework;

import java.util.StringTokenizer;

import com.sok.framework.generation.NodeMap;


public class RelationSpec extends XMLSpec
{

	public static String NAME = "Relations";
	public static String _relation = "relation";
	public static String _relations = "relations";

	public static String _relationname = "relationname";
	public static String _fromtable = "fromtable";
	public static String _totable = "totable";
	public static String _fromkey = "fromkey";
	public static String _tokey = "tokey";
	public static String _relationtype = "relationtype";
	public static String _condition = "condition";
	public static String _priority = "priority";
   public static String _on = "on";
   public static String _keyvalue = "tokeyvalue";
   public static String _keyvaluename = "tokeyvaluename";
   
	public static char _hash = '#';
	public static char _underscore = '_';
	
    RelationSpecPart specificrelations;
    RelationSpecPart defaultrelations;
    
	public RelationSpec(String name)	
	{	
        defaultrelations = new RelationSpecPart(SpecManager.getFilePath(name, null, null), false);
        specificrelations = new RelationSpecPart(SpecManager.getFilePath(name, "specific/", null), true);       
        specificrelations.setIndexOffset(defaultrelations.getLength());
	}

	public synchronized void addRelation(String relationname, String fromtable, String totable, String fromkey, String tokey, String relationtype, String condition)
	{
        specificrelations.addRelation(relationname, fromtable, totable, fromkey, tokey, relationtype, condition);
	}

	public synchronized void resetCache()
	{
        defaultrelations.resetCache();
        specificrelations.resetCache();        
	}
	
	protected RelationSpecPart getRelationSpecPart(int i)
	{
      if(i < defaultrelations.getLength())
      {
         return(defaultrelations);
      }
      return(specificrelations);
	}
	
	public int getLength()
	{
	    return(defaultrelations.getLength() + specificrelations.getLength());
	}
	
	public boolean hasRelationshipWith(String fromtable, String totable)
	{
	    return(defaultrelations.hasRelationshipWith(fromtable, totable) ||
                specificrelations.hasRelationshipWith(fromtable, totable));
	}

    public static String escapeRelationName(String theString)
    {
       char aChar;
       int len = theString.length();
       if(len != 0 && theString.indexOf('=')>0 || theString.indexOf(_hash)>0)
       {
          StringBuffer outBuffer = new StringBuffer(len);

          for(int x = 0; x < len;)
          {
             aChar = theString.charAt(x++);
             if(aChar == _hash)
             {
                outBuffer.append(_underscore);
             } 
             else if(aChar == '(' && "ABS".equals(outBuffer.toString()))
             {
            	 // ( must be allowed only if its a case of trying to sort numerically
                 outBuffer.append('(');
             }
             else if(aChar == _underscore)
             {
                outBuffer.append(aChar);
             }
             else if(aChar >= '0' && aChar <= '9')
             {
                outBuffer.append(aChar);
             }             
             else if(aChar >= 'A' && aChar <= 'Z')
             {
                outBuffer.append(aChar);
             }
             else if(aChar >= 'a' && aChar <= 'z')
             {
                outBuffer.append(aChar);
             }
          }
          return outBuffer.toString();
       }
       else
       {
          return (theString);
       }
       
    }
    
    public static NodeMap getRelationParameters(String relationname)
    {
        if (relationname.indexOf('=')>0) {
           NodeMap node = new NodeMap();
            StringTokenizer iter = new StringTokenizer(relationname, "&#_");
            String param = null;
            String paramname = null;
            String paramvalue = null;
            int eqindex = 0;
            while (iter.hasMoreTokens()) {
               param = (String)iter.nextToken();
               eqindex = param.indexOf('=');
               if(eqindex>0){
                  paramname = param.substring(0,eqindex);
                  paramvalue = param.substring(eqindex+1,param.length());

                   if (paramname != null) {   
                      node.put(StringUtil.urlDecode(paramname), StringUtil.urlDecode(paramvalue));
                   }
               }
            }
            return(node);
         }
        return(null); 
    }     
    
	public int getIndex(String name)
	{
	    int index = defaultrelations.getIndex(name);
        if(index >=0)
        {
            return(index);
        }
        index = specificrelations.getIndex(name);
        if(index >=0)
        {
        	return(index);
        }
        return(index);
	}

	public boolean hasRelation(String name)
	{
        return(defaultrelations.hasRelation(name) ||
                specificrelations.hasRelation(name)); 
	}
	
	public String getName(int i)
	{
        return(getRelationSpecPart(i).getName(i));
	}

	public String getType(int i)
	{
        return(getRelationSpecPart(i).getType(i));
	}
	
	public String getToTableName(int i)
	{
        return(getRelationSpecPart(i).getToTableName(i));
	}
	
	public String getToTableSpecLocation(int i)
	{
        return(getRelationSpecPart(i).getToTableSpecLocation(i));
	}

	public String getFromTableName(int i)
	{
        return(getRelationSpecPart(i).getFromTableName(i));
	}

	public String getFromTableSpecLocation(int i)
	{
        return(getRelationSpecPart(i).getFromTableSpecLocation(i));
	}
	
	public String getToKey(int i)
	{
        return(getRelationSpecPart(i).getToKey(i));
	}
	
	public String getFromKey(int i)
	{
        return(getRelationSpecPart(i).getFromKey(i));
	}	

   public String getCondition(int i)
   {
        return(getRelationSpecPart(i).getCondition(i));
   }	
	
   /**
    * @deprecated
    */    
	public String getCondition(int i, int j)
	{
        return(getRelationSpecPart(i).getCondition(i, j));
	}

	public int getConditionLength(int i)
	{
        return(getRelationSpecPart(i).getConditionLength(i));
	}

   public String getOnToKey(int i, int j)
   {
      return(getRelationSpecPart(i).getOnToKey(i, j));
   }
   
   public String getOnFromKey(int i, int j)
   {
      return(getRelationSpecPart(i).getOnFromKey(i, j));
   }
   
   public String getOnKeyValue(int i, int j)
   {
      return(getRelationSpecPart(i).getOnKeyValue(i, j));
   }     

   public String getOnKeyValueName(int i, int j)
   {
      return(getRelationSpecPart(i).getOnKeyValueName(i, j));
   }   

   public int getOnLength(int i)
   {
      return(getRelationSpecPart(i).getOnLength(i));
   }
	
	public int getPriority(int i)
	{
        return(getRelationSpecPart(i).getPriority(i));
	}
}
