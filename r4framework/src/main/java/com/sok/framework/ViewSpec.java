package com.sok.framework;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ViewSpec extends XMLSpec
{ 	
	private static final Logger logger = LoggerFactory.getLogger(ViewSpec.class); 
	
   private Element[] fields;
   private Element[] foreignfields;
   private Element[] foreigncounts;
   private Element[] foreignsums;
   private Element[] counts;
   private String maintablename;
   private String condition;

	public static String _data = "data";
	public static String _foreigndata = "foreigndata";
	public static String _foreigncount = "foreigncount";
	public static String _foreignsum = "foreignsum";
	public static String _fieldname = "fieldname";
	public static String _formattype = "formattype";
	public static String _format = "format";
	public static String _asname = "asname";
	public static String _view = "view";
	public static String _viewname = "viewname";
	public static String _tablespec = "tablespec";
	public static String _relationship = "relationship";
	public static String _condition = "condition";
	public static String _valuerequired = "valuerequired";
 	public static String _count = "count";
 	public static String _on = "on";
 	public static String _label = "label";
	public static String _hidden = "hidden";
	
	public ViewSpec(String specname, String tablename)	
	{
		super();
		super.name = specname;
		maintablename = tablename;

		Element root = super.document.addElement(_view);
		root.addAttribute(_viewname, specname);
		root.addAttribute(_tablespec, tablename);
		root.addText(_n);
	}

	public ViewSpec(String path)	
	{	
		super(path);
        if(super.document!=null)
        {
    		Element root = super.document.getRootElement();
            if(root != null)
            {
            	if(!StringUtils.equalsIgnoreCase(root.getName(), "view")) {
                 	throw new IllegalArgumentException("Invalid View for path " + path);
                }
        		maintablename = root.attributeValue(_tablespec);
        		fields = super.getElementArray(root,_data);
        		
        		if(StringUtils.isBlank(maintablename)) {
        			throw new UnsupportedOperationException("Main table name must be defined"); 
        		}
        		// Pull the table for use in validation
        		final TableSpec table = SpecManager.getTableSpec(maintablename);
        		StringBuilder errors = null;
        		if(fields != null) { 
	        		for(Element e: fields) {
	        			String f = e.attributeValue(_fieldname);
	        			int ix = table.getFieldsIndex(f);
	        			if(ix == -1) {
	        				if(f.indexOf("*")!=-1 || f.indexOf("-")!=-1 || f.indexOf("(")!=-1) {
	        					// there are some cases where "fieldname" values are calculations 
	        					logger.warn("ViewSpec [{}] contains improper calculation field [{}]", path, f);
	        					continue;
	        				}
	        				if(errors == null) errors = new StringBuilder();
	        				errors.append("Field ").append(f).append(" missing from TableSpec\n");
	        			}
	        		}
        		}
        		foreignfields = super.getElementArray(root,_foreigndata);
        		if(foreignfields != null) {
        			RelationSpec rs = SpecManager.getRelationSpec(); 
        			for(Element e: foreignfields) {
        				String as = e.attributeValue(_asname);
        				String f = e.attributeValue(_fieldname);
        				String rel = e.attributeValue(_relationship);
        				
        				if(logger.isTraceEnabled()) logger.trace("Checking foreigndata asname={} fieldname={} relationship={}",new String[]{as, f, rel});
        				if(StringUtils.isBlank(as) || StringUtils.isBlank(f) || StringUtils.isBlank(rel)) {
        					if(errors == null) errors = new StringBuilder();
	        				errors.append("foreigndata asname=").append(as).append(" was invalid \n");
	        				continue;
        				}
        				String[] rels = rel.split("\\."); 
        				logger.trace("rels.length={}", rels.length);
        				TableSpec last = table, next = null;
        				for(int i=0; i<rels.length; i++) {
        					int ri = rs.getIndex(rels[i]);
        					logger.trace("check relationship={} hasIndex={}", rels[i], ri);
        					if(ri == -1) {
        						if(errors == null) errors = new StringBuilder();
    	        				errors.append("foreigndata asname=").append(as).append(" was invalid - invalid relation - ").append(rels[i]).append("\n");
    	        				continue;
        					}
        					String from = SpecManager.getTableSpec(rs.getFromTableSpecLocation(ri)).getTableName();
        					logger.trace("check table={} hasName={}", last.getTableName(), from);
        					if(!StringUtils.equals(last.getTableName(), from)) {
        						if(errors == null) errors = new StringBuilder();
    	        				errors.append("foreigndata asname=").append(as).append(" had invalid relation chain - ").append(rels[i]).append(" - does not match prev \n");
        					}
        					logger.trace("check table={} hasField={}", last.getTableName(), rs.getFromKey(ri));
        					if(!last.hasField(rs.getFromKey(ri))) {
        						if(errors == null) errors = new StringBuilder();
    	        				errors.append("foreigndata asname=").append(as).append(" had invalid relation - table did not contain from key - ").append(rels[i]).append("\n");
        					}
        					logger.trace("check table={} hasField={}", rs.getToTableSpecLocation(ri), rs.getToKey(ri));
        					next = SpecManager.getTableSpec(rs.getToTableSpecLocation(ri));
        					if(!next.hasField(rs.getToKey(ri))) {
        						if(errors == null) errors = new StringBuilder();
    	        				errors.append("foreigndata asname=").append(as).append(" had invalid relation - table did not contain to key - ").append(rels[i]).append("\n");
        					}
        					last = next; 
        				}
        				if(next != null) {
        					// will be null if the first relation is invalid
	        				logger.trace("check table={} hasField={}", next.getTableName(), f);
	        				if(!next.hasField(f)) {
	    						if(errors == null) errors = new StringBuilder();
		        				errors.append("foreigndata asname=").append(as).append(" had invalid relation - final table=").append(next.getTableName()).append(" did not contain field - ").append(f).append("\n");
	    					}
        				}
        			}
        		}
        		foreigncounts = super.getElementArray(root,_foreigncount);
        		foreignsums = super.getElementArray(root,_foreignsum);
        		counts = super.getElementArray(root,_count);
                condition = super.getFirstElementText(root,_condition);
                
                if(errors != null) {	// complete all validations, then throw exception with all errors.
                	logger.warn("ViewSpec({}) failed validation with errors {}", path, errors.toString());
                	//throw new UnsupportedOperationException(errors.toString());
        		}
            }
            else
            {
                throw new NullPointerException("Document has no root element - " + path);
            }
        }
        else
        {
            throw new NullPointerException("Document cannot be loaded - " + path);
        }
	}

	public synchronized void addData(String fieldname, String formatstring, String ftype, String alias)
	{
		addData(fieldname, formatstring, ftype, alias, null);
	}

	public synchronized void addData(String fieldname, String formatstring, String ftype, String alias, String required)
	{
		Element root = super.document.getRootElement();
		root.addText(_t);

		Element node = root.addElement(_data);
	
		node.addAttribute(_fieldname, fieldname);

		if(formatstring!=null && formatstring.length()!=0)
		{
			node.addAttribute(_format, formatstring);
		}

		if(ftype!=null && ftype.length()!=0)
		{
			node.addAttribute(_formattype, ftype);
		}

		if(alias!=null && alias.length()!=0)
		{
			node.addAttribute(_asname, alias);
		}

		if(required!=null && required.length()!=0)
		{
			node.addAttribute(_valuerequired, "");
		}

		root.addText(_n);

		if(fields==null)
		{
			fields = super.getElementArray(root,_data);
		}
	}

	public synchronized void addForeignData(String alias, String fieldname, String relation, String formatstring, String ftype)
	{
		Element root = super.document.getRootElement();
		root.addText(_t);
		
		Element node = root.addElement(_foreigndata);
	
		node.addAttribute(_asname, alias);
		node.addAttribute(_fieldname, fieldname);
		node.addAttribute(_relationship, relation);

		if(formatstring!=null && formatstring.length()!=0)
		{
			node.addAttribute(_format, formatstring);
		}

		if(ftype!=null && ftype.length()!=0)
		{
			node.addAttribute(_formattype, ftype);
		}

		root.addText(_n);
		
		if(foreignfields==null)
		{
			foreignfields = super.getElementArray(root,_foreigndata);
		}
	}

	public synchronized void addForeignCount(String alias,  String relation, String condition)
	{
		Element root = super.document.getRootElement();
		root.addText(_t);

		Element node = root.addElement(_foreigncount);
		
		node.addAttribute(_asname, alias);
		node.addAttribute(_relationship, relation);

		if(condition!=null && condition.length()!=0)
		{
			node.addText(_ntt);
			Element cnode = node.addElement(_condition);
			cnode.addText(_nttt);
			cnode.addText(condition);
			cnode.addText(_ntt);
			node.addText(_nt);
		}

		root.addText(_n);
		
		if(foreigncounts==null)
		{
			foreigncounts = super.getElementArray(root,_foreigncount);
		}
	}

	public synchronized void addForeignSum(String alias,  String relation, String condition)
	{
		Element root = super.document.getRootElement();
		root.addText(_t);

		Element node = root.addElement(_foreignsum);
		
		node.addAttribute(_asname, alias);
		node.addAttribute(_relationship, relation);

		if(condition!=null && condition.length()!=0)
		{
			node.addText(_ntt);
			Element cnode = node.addElement(_condition);
			cnode.addText(_nttt);
			cnode.addText(condition);
			cnode.addText(_ntt);
			node.addText(_nt);
		}

		root.addText(_n);
		
		if(foreignsums==null)
		{
			foreignsums = super.getElementArray(root,_foreignsum);
		}
	}

	
    public String getCondition() {
        return condition;
    }

	public int getLocalLength()
	{
		if(fields!=null)
		{
			return(fields.length);
		}
		return(0);
	}
	
	public String getMainTableName()
	{
		return(maintablename);	
	}
	
	public String getLocalItemFieldName(int i)
	{
		return(super.getAttribute(fields,i,_fieldname));	
	}

	public String getFormatTypeForName(String name)
	{
		for(int i=0; i<getLocalLength(); i++)
		{
			String alias = getLocalItemAsName(i);
			if(name.equals(getLocalItemFieldName(i)) || (alias!=null && name.equals(alias)))
			{
				return(getLocalItemFormatType(i));	
			}
		}
		for(int j=0; j<getRelatedLength(); j++)
		{
			if(name.equals(getRelatedItemName(j)))
			{
				return(getRelatedItemFormatType(j));	
			}
		}
		for(int j=0; j<getRelatedSumsLength(); j++)
		{
			if(name.equals(getRelatedSumItemName(j)))
			{
				return(getRelatedSumItemFormatType(j));	
			}
		}
		return(null);
	}

	public String getFormatForName(String name)
	{
		String alias = null;
		for(int i=0; i<getLocalLength(); i++)
		{
			alias = getLocalItemAsName(i);
			if(alias!=null && name.equals(alias))
			{
				return(getLocalItemFormat(i));
			}
			else if(name.equals(getLocalItemFieldName(i)) && alias==null)
			{
				return(getLocalItemFormat(i));
			}
		}
		for(int j=0; j<getRelatedLength(); j++)
		{
			if(name.equals(getRelatedItemName(j)))
			{
				return(getRelatedItemFormat(j));	
			}
		}
		for(int j=0; j<getRelatedSumsLength(); j++)
		{
			if(name.equals(getRelatedSumItemName(j)))
			{
				return(getRelatedSumItemFormat(j));	
			}
		}
		return(null);	
	}

	public int getDataIndex(String name)
	{
		String fieldname = null;
		String alias = null;

		for(int i=0; i<getLocalLength(); i++)
		{
			alias = getLocalItemAsName(i);
			fieldname = getLocalItemFieldName(i);
			if(alias!=null && name.equals(alias))
			{
				return(i);
			}
			else if(name.equals(fieldname) && alias==null)
			{
				return(i);
			}
		}
		return(-1);
	}

	public String getLocalItemFormatType(int i)
	{
		return(super.getAttribute(fields,i,_formattype));	
	}

	public String getLocalItemLabel(int i)
	{
		return(super.getAttribute(fields,i,_label));
	}

	public String getLocalItemFormat(int i)
	{
		return(super.getAttribute(fields,i,_format));
	}

	public String getLocalItemAsName(int i)
	{
		return(super.getAttribute(fields,i,_asname));
	}

	public String getLocalItemName(int i)
	{
		String value = super.getAttribute(fields,i,_asname);
		if(value==null)
		{
			value = getLocalItemFieldName(i);
		}
		return(value);
	}

	public boolean getValueRequired(int i)
	{
		String value = super.getAttribute(fields,i,_valuerequired);
		if(value!=null)
		{
			return(true);
		}
		return(false);
	}

	public int getCountsLength()
	{
		return(super.getCount(counts));
	}

	public String getCountItemName(int i)
	{
		return(super.getAttribute(counts,i,_on));
	}

	public String getCountItemAsName(int i)
	{
		return(super.getAttribute(counts,i,_asname));
	}

	public int getFieldLength()
	{
		return(super.getCount(fields));
	}
	
	public int getRelatedLength()
	{
		return(super.getCount(foreignfields));
	}
	
	public int getRelatedCountsLength()
	{
		return(super.getCount(foreigncounts));
	}	
	
	public int getRelatedSumsLength()
	{
		return(super.getCount(foreignsums));
	}	
	
	public String getRelatedItemName(int i)
	{
		return(super.getAttribute(foreignfields,i,_asname));
	}
	
	public String getRelatedItemFieldName(int i)
	{
		return(super.getAttribute(foreignfields,i,_fieldname));
	}

	public String getRelatedItemFormat(int i)
	{
		return(super.getAttribute(foreignfields,i,_format));
	}
	
	public String getRelatedSumItemFormat(int i)
	{
		return(super.getAttribute(foreignsums,i,_format));
	}
	
	public String getRelatedItemLabel(int i)
	{
		return(super.getAttribute(foreignfields,i,_label));
	}

	public String getRelatedItemFormatType(int i)
	{
		return(super.getAttribute(foreignfields,i,_formattype));
	}	

	public String getRelatedSumItemFormatType(int i)
	{
		return(super.getAttribute(foreignsums,i,_formattype));
	}	
	
	public String getRelatedCountItemName(int i)
	{
		return(super.getAttribute(foreigncounts,i,_asname));
	}	
	
	public String getRelatedSumItemName(int i)
	{
		return(super.getAttribute(foreignsums,i,_asname));
	}

	public boolean getRelatedCountHidden(int i)
	{
		String value = super.getAttribute(foreigncounts,i,_hidden);
		if(value!=null)
		{
			return(true);
		}
		return(false);
	}
	public boolean getRelatedSumHidden(int i)
	{
		String value = super.getAttribute(foreignsums,i,_hidden);
		if(value!=null)
		{
			return(true);
		}
		return(false);
	}

	public String getRelatedCountCondition(int i, int j)
	{
		return(super.getFirstElementText(foreigncounts[i],_condition));
	}

	public String getRelatedSumCondition(int i, int j)
	{
		return(super.getFirstElementText(foreignsums[i],_condition));
	}

	public int getRelatedCountConditionLength(int i)
	{
		if(super.firstElementHasText(foreigncounts[i],_condition))
		{
			return(1);
		}
		return(0);
	}

	public int getRelatedSumConditionLength(int i)
	{
		if(super.firstElementHasText(foreignsums[i],_condition))
		{
			return(1);
		}
		return(0);
	}
	
	public String getRelationshipNameForRelatedItem(int i)
	{
		return(super.getAttribute(foreignfields,i,_relationship));
	}

	public String getRelationshipNameForRelatedCountItem(int i)
	{
		return(super.getAttribute(foreigncounts,i,_relationship));
	}

	public String getRelationshipNameForRelatedSumItem(int i)
	{
		return(super.getAttribute(foreignsums,i,_relationship));
	}
	public String getFieldNameForRelatedSumItem(int i)
	{
		return(super.getAttribute(foreignsums,i,_fieldname));
	}

}
