package com.sok.framework.https;

import javax.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate; 

public class X509TrustAllManager implements X509TrustManager
{
    // a trust manager that does not validate certificate chains

    public X509Certificate[] getAcceptedIssuers()
    {
        return null;
    }
    
    public void checkClientTrusted(X509Certificate[] certs, String authType)
    {
    }
    
    public void checkServerTrusted(X509Certificate[] certs, String authType)
    {
    }
}