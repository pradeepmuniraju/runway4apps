package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;

public class DeleteSetNode extends StatementHeadNode
{
    public DeleteSetNode()
    {
        super(sql_delete);
        super.skipifempty = false;
    }       
    
    public DeleteSetNode(String name)
    {
        super(name);
        super.skipifempty = false;
    }   
    
    public UpdateNode setUpdate(String fieldname, Object fieldvalue, int datatype, String format)
    {
        UpdateNode node = new UpdateNode(fieldname, fieldvalue, datatype, format);
        addNode(node);
        return(node);
    }   
    
    protected void appendOut(RootNode root, StringBuffer buffer)
    {
        buffer.append(sql_delete);
        buffer.append(sql_from);
        buffer.append(name);
        buffer.append(rn);
    }
    
    protected void appendEnd(RootNode root, StringBuffer buffer)
    {
        if(fromsetnode!=null)
        {
            fromsetnode.generate(root, buffer);
        }
    }

    protected void appendSeparator(RootNode root, StringBuffer buffer)
    {
        buffer.append(sql_comma);
    }
    
    public int getNodeType()
    {
        return(NodeType.type_DeleteSet);
    }      
}