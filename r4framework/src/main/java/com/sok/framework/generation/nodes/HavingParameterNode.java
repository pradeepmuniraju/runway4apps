package com.sok.framework.generation.nodes;

import com.sok.framework.generation.*;

public class HavingParameterNode extends AbstractParameterNode implements GenerationKeys
{
    int priority = 0;
    String condition;
    
    public HavingParameterNode(String name, Object value)
    {
        super(name, value);
        initName();
    }
    
    public int getNodeType()
    {
        return(type_HavingParameter);
    }       
    
    protected void initName()
    {
        try{
            priority = Integer.parseInt(name.substring(_groupby_prefix.length()+1, name.length()));
        }catch(Exception e){}
    }
    
    public void setName(String name)
    {
        super.setName(name);
        initName();
    }
    
    public String getCondition()
    {
        return(value.toString());
    } 
    
    public int getPriority()
    {
        return(priority);
    }
}