package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;

public class HavingSetNode extends AbstractPrioritisedGeneratorSetNode
{
    public HavingSetNode()
    {
        super(sql_having);    
    }   
    
    public HavingSetNode(String name)
    {
        super(name);
    }
    
    public HavingNode setHaving(String condition, int num)
    {
        HavingNode node = new HavingNode(condition, num);
        addPrioritisedNode(node);
        return(node);
    }       
    
    protected void appendOut(RootNode root, StringBuffer buffer)
    {
        buffer.append(rn);        
        buffer.append(sql_having);
    }
    
    protected void appendEnd(RootNode root, StringBuffer buffer)
    {

    }

    protected void appendSeparator(RootNode root, StringBuffer buffer)
    {
        buffer.append(sql_comma);
    } 
 
    public int getNodeType()
    {
        return(NodeType.type_HavingSet);
    }      
}