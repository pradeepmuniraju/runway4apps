package com.sok.framework.generation;

import java.sql.*;

public class Rows extends Row implements GenerationKeys
{
    ResultSet result = null;
    ResultSetMetaData meta = null;

    Statement stmt = null;

    boolean scrollSensitive = false;
    int rowindex = 0;
    int rowcount = 0;
    
    public void setStatement(Statement stmt)
    {
        this.stmt = stmt;
    }

    public void setResultSet(ResultSet current)
    {
        result = current;
    }

    public int getRowIndex()
    {
        return(rowindex);
    }

    public int getSize()
    {
        return(rowcount);
    }

    public int getColumnCount()
    {
        try{
            return(meta.getColumnCount());
        }catch(Exception e){}
        return(0);
    }

    public String getColumnName(int i)
    {
        try{
            //TODO: return(meta.getColumnLabel(i));
           return(meta.getColumnName(i));
        }catch(Exception e){}
        return(null);
    }

    public boolean getResults(boolean scrollSensitive)
    {
    	this.success = false;
        this.scrollSensitive = scrollSensitive;
        rowindex = 0;
        rowcount = 0;
        if(statement==null)
        {
            if(getAction().length()==0)
            {
                setAction(SEARCH);
            }
            doAction();
        }
        if(statement!=null)
        {
            try
            {
                DatabaseFactory.retrieveResultSet(this, scrollSensitive);
                meta = result.getMetaData();
                if(scrollSensitive)
                {
                    result.last();
                    rowcount = result.getRow();
                    result.beforeFirst();
                }
                this.success = true;
                return(this.success);
            }
            catch(Exception e)
            {
            	this.success = false;
                throw new DatabaseException(e, "Database Error");
            }
        }
        else
        {
        	this.success = false;
            throw new DatabaseException("SQL Statement not found");
        }
    }

    public boolean getResults()
    {
        return getResults(false);
    }

    public boolean getNext()
    {
    	this.success = false;
        if(result!=null)
        {
            clear(type_DataNode);
            try
            {
                if(DatabaseFactory.initResultSet(result,this))
                {
                    rowindex++;
                    if(!this.scrollSensitive)
                    {
                       rowcount++;
                    }
                    this.success = true;
                    return(this.success);
                }
            }
            catch(RuntimeException e)
            {
            	this.success = false;
            	this.close();
                errors.append(e.toString());
                errors.append(_rn);
                throw(e);
            }
        }
        return(this.success);
    }

    public void close()
    {
        if(stmt!=null)
        {
            try{
                stmt.close();
            }catch(Exception e){}
        }
        if(result!=null)
        {
            try{
                result.close();
            }catch(Exception e){}
        }
        meta = null;
        super.close();
    }
 
}
