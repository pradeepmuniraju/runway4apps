package com.sok.framework.generation.nodes.sql;

//import com.sok.framework.generation.nodes.*;

public class WhereTokenSetNode extends WhereSetNode
{
	public WhereTokenSetNode()
	{
		super();
	}
	
	public WhereTokenSetNode(String name)
	{
		super(name);
	}

	public WhereTokenNode setWhereToken(String name, Object value, String tablename, int datatype)
	{
		WhereTokenNode node = new WhereTokenNode(name, value, tablename, datatype);
		addNode(node);
		return(node);
	}		
}