package com.sok.framework.generation.nodes;
//import java.util.*;
public class NodeList extends DefaultNode
{
	protected Node headNode = null;
	protected Node tailNode = null;
	protected int size = 0;
	
	public NodeList(String name)
	{
		super(name);
	}
	
	public int size()
	{
		return(size);
	}
	
   public int size(int type)
   {
      int i = 0;
      Node node = headNode;
      
      while(node!=null)
      {
          if(node.getNodeType() == type)
          {
              i++;
          }
          node = node.getNextNode();
      }
      return(i);
   }
	
 	public boolean isEmpty()
 	{
 		return(headNode == null);
 	}
	
	public Node getHeadNode()
	{
		return(headNode);
	}
	
	public Node getNode(String name)
	{
		Node node = headNode;
		while(node!=null)
		{
			if(node.getName().equals(name))
			{
				return(node);
			}
			node = node.getNextNode();
		}
		return(null);
	}			
	
    public Node getNode(String name, int type)
    {
        Node node = headNode;
        while(node!=null)
        {
        	String nodeName = node.getName();
        	if (nodeName.indexOf("^") > 0) nodeName = nodeName.substring(0,nodeName.indexOf("^"));
        	if (nodeName.indexOf("|") > 0) nodeName = nodeName.substring(0,nodeName.indexOf("|"));
            if(nodeName.equals(name) && node.getNodeType() == type)
            {
                return(node);
            }
            node = node.getNextNode();
        }
        return(null);
    }    
    
    public Node getNode(String name, int startrange, int endrange)
    {
        Node node = headNode;
        while(node!=null)
        {
            if(node.getName().equals(name) 
                    && node.getNodeType() >=  startrange
                    && node.getNodeType() <= endrange)
            {
                return(node);
            }
            node = node.getNextNode();
        }
        return(null);
    }      
    
	public void setNode(Node node)
	{
		Node old = getNode(node.getName());
		if(old!=null)
		{
			node.setNextNode(old.getNextNode());
			node.setPrevNode(old.getPrevNode());
			if(node.getNextNode()!=null)
			{
				node.getNextNode().setPrevNode(node);			
			}
			else
			{
				tailNode = node;
			}
			if(node.getPrevNode()!=null)
			{			
				node.getPrevNode().setNextNode(node);
			}
			else
			{
				headNode = node;
			}
			
			old.setNextNode(null);
			old.setPrevNode(null);
		}
		else
		{
			size++;
			appendNode(node);
		}
	}
	
	public void addNode(Node node)
	{
	   appendNode(node);
	}

	public void appendNode(Node node)
	{
      if(headNode == null)
      {
         headNode = node;
         tailNode = node;
      }
      else
      {
         tailNode.setNextNode(node);
         node.setPrevNode(tailNode);
         node.setNextNode(null);
         tailNode = node;
      }
      size++;
	}
	
	public void pushNode(Node node)
	{
		if(headNode == null)
		{
			headNode = node;
			tailNode = node;
		}
		else
		{
			headNode.setPrevNode(node);
			node.setNextNode(headNode);
			node.setPrevNode(null);
			headNode = node;
		}
		size++;
	}

	public void insertBeforeNode(Node node, Node position)
	{
		if(position.getPrevNode() == null)
		{
			pushNode(node);
		}
		else
		{
			insertNode(node, position.getPrevNode(), position);
		}
	}	

	public void insertAfterNode(Node node, Node position)
	{
		if(position.getNextNode() == null)
		{
		   appendNode(node);			
		}
		else
		{
			insertNode(node, position, position.getNextNode());
		}
	}	
	
	public void replaceNode(Node n, Node o)
	{
		if(headNode == o)
		{
			headNode = n;
		}
		else if(tailNode == o)
		{
			tailNode = n;
		}
		
		Node prev = o.getPrevNode();
		Node next = o.getNextNode();
		
		if(prev!=null)
		{
			prev.setNextNode(n);
			n.setPrevNode(prev);
		}
		
		if(next!=null)
		{
			prev.setPrevNode(n);
			n.setNextNode(next);
		}
	}
	
	protected void insertNode(Node n, Node a, Node b)
	{
		a.setNextNode(n);
		n.setPrevNode(a);
		n.setNextNode(b);
		b.setPrevNode(n);
		size++;
	}
	
	public void removeNode(Node node)
	{
		Node prev = node.getPrevNode();
		Node next = node.getNextNode();

		if(prev != null && next != null)
		{
			next.setPrevNode(prev);
			prev.setNextNode(next);
		}
		else if(next != null && prev == null)
		{
			headNode = next;
			next.setPrevNode(null);
		}		
		else if(prev != null && next == null)
		{
			tailNode = prev;
			prev.setNextNode(null);
		}		
		else if(prev == null && next == null)
		{
			tailNode = null;
			headNode = null;
		}
		node.setNextNode(null);
		node.setPrevNode(null);
		size--;
	}
	
	public Node removeNode(String name)
	{
		Node child = getNode(name);
		if(child != null)
		{
			removeNode(child);
		}
		return(child);
	}
	
	public Node find(String name)
	{
		if(getName().equals(name))
		{
			return(this);
		}
		Node node = null;		

		Node child = getHeadNode();
		while(child!=null)
		{
			node = child.find(name);
			if(node!=null)
			{
				return(node);
			}
			child = child.getNextNode();
		}

		return(node);
	}	
}