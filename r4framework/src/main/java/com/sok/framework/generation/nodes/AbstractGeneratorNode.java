package com.sok.framework.generation.nodes;

import com.sok.framework.generation.SQLKeys;

//import java.util.*;

public abstract class AbstractGeneratorNode extends DefaultNode implements GeneratorNode, SQLKeys
{
	public AbstractGeneratorNode(String name)
	{
		super(name);
	}
	
	abstract public void generate(RootNode root, StringBuffer buffer);
	
	public boolean hasOutPut()
	{
	   return(true);
	}
}