package com.sok.framework.generation.nodes;

import com.sok.framework.generation.SQLKeys;

//import com.sok.framework.generation.nodes.sql.*;
//import java.util.*;

public abstract class AbstractGeneratorSetNode extends BranchNode implements GeneratorNode, SQLKeys
{
	protected boolean skipifempty = true;
	
	public AbstractGeneratorSetNode(String name)
	{
		super(name);
	}		
	
	public void setSkipIfEmpty(boolean b)
	{
		skipifempty = b;
	}
	
	abstract protected void appendOut(RootNode root, StringBuffer buffer);
    
    protected void appendOut(RootNode root, GeneratorNode node, StringBuffer buffer)
    {
        appendOut(root, buffer);
        if(node!=null)
        {
            node.generate(root, buffer);
        }  
    }
    
	abstract protected void appendEnd(RootNode root, StringBuffer buffer);

	abstract protected void appendSeparator(RootNode root, StringBuffer buffer);	

    public void generate(RootNode root, GeneratorNode afnode, StringBuffer buffer)
    {
        if(!isEmpty() || !skipifempty)
        {       
            appendOut(root, afnode, buffer);
            generateChildren(root, buffer);
            appendEnd(root, buffer);
        } 
    }
    
    public void generateChildren(RootNode root, StringBuffer buffer)
    {
        Node node = getHeadNode();
        if(node!=null)
        {
            boolean addsep = false;
            GeneratorNode child = null;
            while(node!=null)
            {
                if(node instanceof GeneratorNode)
                {
                    child = ((GeneratorNode)node);
                    if(child.hasOutPut())
                    {
                       if(addsep)
                       {
                           appendSeparator(root, buffer);
                       }
                       child.generate(root, buffer);
                       addsep = true;
                    }
                }
                node = node.getNextNode();
            }
        }
    }
    
	public void generate(RootNode root, StringBuffer buffer)
	{
        generate(root, null, buffer);
	}
	
	public boolean childrenHasOutPut()
	{
      Node node = getHeadNode();
      if(node!=null)
      {
          GeneratorNode child = null;
          while(node!=null)
          {
              if(node instanceof GeneratorNode)
              {
                  child = ((GeneratorNode)node);
                  if(child.hasOutPut())
                  {
                       return(true);
                  }
              }
              node = node.getNextNode();
          }
      }
      return(false);
	}
	
   public boolean hasOutPut()
   {
      return(true);
   }	
}