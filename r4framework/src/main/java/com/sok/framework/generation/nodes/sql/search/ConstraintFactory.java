package com.sok.framework.generation.nodes.sql.search;

import com.sok.framework.generation.nodes.sql.*;
import java.sql.Types;
public class ConstraintFactory
{   
    private static final Constraint stringcontraint = new StringConstraint();
    private static final Constraint datetimecontraint = new DateTimeConstraint();
    private static final Constraint floatcontraint = new FloatConstraint();
    private static final Constraint integercontraint = new IntegerConstraint();
    
    public static final char singlequote = '\'';
    public static final char backslash = '\\'; 
    
    public static Constraint getConstraint(WhereNode node)
    {
        int datatype = node.getDataType();
        if(datatype == Types.TIMESTAMP)
        {
            return(datetimecontraint);
        }
        else if(datatype == Types.INTEGER)
        {
            return(integercontraint);
        }   
        else if(datatype == Types.FLOAT)
        {
            return(floatcontraint); 
        }         
        return(stringcontraint);
    }
    
    public static String escapeSQL(String s)
    {
       int quoteindex = s.indexOf(singlequote);
       int slashindex = s.indexOf(backslash);       
       if(quoteindex >= 0 || slashindex >= 0)
       {
          StringBuffer escaped = new StringBuffer();
          for(int i = 0; i < s.length(); i++)
          {
             if(s.charAt(i) == singlequote)
             {
                escaped.append(s.charAt(i));
             }
             else if(s.charAt(i) == backslash)
             {
                escaped.append(s.charAt(i));
             }             
             escaped.append(s.charAt(i));
          }
          return (escaped.toString());
       }
       return (s);
    } 
}