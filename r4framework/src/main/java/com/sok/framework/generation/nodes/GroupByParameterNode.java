package com.sok.framework.generation.nodes;

import com.sok.framework.generation.*;

public class GroupByParameterNode extends AbstractParameterNode implements GenerationKeys
{
	int priority = 0;
	String fieldname;
	String tablename;
	
	public GroupByParameterNode(String name, Object value)
	{
		super(name, value);
		initName();
		initValue();
	}
	
    public int getNodeType()
    {
        return(type_GroupByParameter);
    }       
    
	protected void initValue()
	{
		fieldname = value.toString();
		int index = fieldname.indexOf('.');
		if(index>0)
		{
			tablename = fieldname.substring(0,index);
			fieldname = fieldname.substring(index+1,fieldname.length());
		}
	}
	
	protected void initName()
	{
		try{
			priority = Integer.parseInt(name.substring(_groupby_prefix.length()+1, name.length()));
		}catch(Exception e){}
	}
	
	public void setName(String name)
	{
		super.setName(name);
		initName();
	}
	
	public Object setValue(Object value)
	{
		Object obj = super.setValue(value);
		initValue();
		return(obj);
	}	
	
	public String getFieldName()
	{
		return(fieldname);
	}

	public void setTableName(String tablename)
	{
		this.tablename = tablename;
	}		
	
	public String getTableName()
	{
		return(tablename);
	}	
	
	public int getPriority()
	{
		return(priority);
	}
}