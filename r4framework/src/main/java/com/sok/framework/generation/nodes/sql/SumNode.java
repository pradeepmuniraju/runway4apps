package com.sok.framework.generation.nodes.sql;

import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.RootNode;

public class SumNode extends SelectNode
{
	public SumNode(String tablename, String fieldname, String fieldalias)
	{
		super(tablename, fieldname, fieldalias);
	}

	public void generate(RootNode root, StringBuffer out)
	{
		out.append(sql_sum);
		out.append(sql_bracketbegin);		
		if(tablename!=null)
		{
			out.append(RelationSpec.escapeRelationName(tablename));
			out.append(sql_dot);
		}
		out.append(fieldname);
		out.append(sql_bracketend);			
		if(fieldalias != null)
		{
			out.append(sql_as);
			out.append(fieldalias);
		}
    }
    
    public int getNodeType()
    {
        return(NodeType.type_Sum);
    }  
}