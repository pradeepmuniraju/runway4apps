package com.sok.framework.generation;

public class IllegalConfigurationException extends TracedException
{
    public IllegalConfigurationException(String message)
    {
        super(message);
    }
    
    public IllegalConfigurationException(Exception ex)
    {
        super(ex);
    }    
    
    public IllegalConfigurationException(String message, Exception ex)
    {
        super(message, ex);
    }       
}