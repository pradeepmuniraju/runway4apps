package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;

public class SelectSetNode extends StatementHeadNode
{
	protected boolean distinct = false;
	
	public SelectSetNode()
	{
		super(sql_select);
		super.skipifempty = false;
	}		
	
	public SelectSetNode(String name)
	{
		super(name);
		super.skipifempty = false;
	}	
	
	public void setDistinct(boolean b)
	{
		this.distinct = b;
	}
	
	public SelectNode setSelect(String tablename, String fieldname, String fieldalias)
	{
		SelectNode node = new SelectNode(tablename, fieldname, fieldalias);
		addNode(node);
		return(node);
	}	
	
	protected void appendOut(RootNode root, StringBuffer buffer)
	{
		buffer.append(sql_select);
		if(distinct)
		{
			buffer.append(sql_distinct);
		}
	}
	
	protected void appendEnd(RootNode root, StringBuffer buffer)
	{
		if(fromsetnode!=null)
		{
			fromsetnode.generate(root, buffer);
		}
	}

	protected void appendSeparator(RootNode root, StringBuffer buffer)
	{
		buffer.append(sql_comma);
	}
    
    public int getNodeType()
    {
        return(NodeType.type_SelectSet);
    }     
}