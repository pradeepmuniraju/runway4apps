package com.sok.framework.generation.util;

import java.util.*;

public interface CalendarKeys
{
   public static final String NOW = "NOW";   
   
   public static final String TODAY = "TODAY";
   public static final String THISWEEK = "THISWEEK";
   public static final String THISMONTH = "THISMONTH";
   public static final String FUTURE = "FUTURE";
   public static final String OVERDUE = "OVERDUE";
   public static final String CURRENT = "CURRENT";
   public static final String CURRENTQUARTER = "CURRENTQUARTER";
   public static final String CURRENTMONTH = "CURRENTMONTH";
   public static final String CURRENTYEAR = "CURRENTYEAR";
   public static final String CURRENTFINYEAR = "CURRENTFINYEAR";
   public static final String CURRENTWEEK = "CURRENTWEEK";
   public static final String LASTWEEK = "LASTWEEK";
   public static final String LASTMONTH = "LASTMONTH";
   public static final String LASTYEAR = "LASTYEAR";
   public static final String LASTFINYEAR = "LASTFINYEAR";
   public static final String LAST = "LAST";
   public static final String NEXT = "NEXT";
   public static final String EXCEPT = "EXCEPT";
   public static final String WEEKS = "WEEKS";
   public static final String DAYS = "DAYS";
   public static final String MONTHS = "MONTHS";
   public static final String QUARTERS = "QUARTERS";
   public static final String MINUTES = "MINUTES";	// only supported by "AGO" syntax for search at present.
   public static final String HOURS = "HOURS";
   public static final String HOUR = "HOUR";
   public static final String YEARS = "YEARS";
   public static final String FINYEARS = "FINYEARS";    
   
   public static final String MONDAY = "MONDAY";
   public static final String TUESDAY = "TUESDAY";
   public static final String WEDNESDAY = "WEDNESDAY";
   public static final String THURSDAY = "THURSDAY";
   public static final String FRIDAY = "FRIDAY";
   public static final String SATURDAY = "SATURDAY";
   public static final String SUNDAY = "SUNDAY";   
   
   public static final String WITHTODAY = "WITHTODAY";
   public static final String AGO = "AGO";
   
   public static final String MON = "MON";
   public static final String TUE = "TUE";
   public static final String WED = "WED";
   public static final String THU = "THU";
   public static final String FRI = "FRI";
   public static final String SAT = "SAT";
   public static final String SUN = "SUN";
   
   public static final String JAN = "JAN";
   public static final String FEB = "FEB";
   public static final String MAR = "MAR";
   public static final String APR = "APR";
   public static final String MAY = "MAY";
   public static final String JUN = "JUN";
   public static final String JUL = "JUL";   
   public static final String AUG = "AUG"; 
   public static final String SEP = "SEP"; 
   public static final String OCT = "OCT"; 
   public static final String NOV = "NOV"; 
   public static final String DEC = "DEC"; 
   
   public static final String[] _weekdays = {SUN, MON, TUE, WED, THU, FRI, SAT};
   public static final int[] _gweekdays = {
      Calendar.SUNDAY,
      Calendar.MONDAY,
      Calendar.TUESDAY,
      Calendar.WEDNESDAY,
      Calendar.THURSDAY,
      Calendar.FRIDAY,
      Calendar.SATURDAY
   };
   public static final String[] _months = {JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC};   
      
}