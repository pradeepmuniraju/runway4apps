package com.sok.framework.generation.util;
import com.sok.framework.generation.nodes.*;
public class History extends NodeList
{
	boolean uniqueNames = false;
	boolean ignoreCase = false;
	boolean keepOrder = false;
	int maxSize = 0;

	protected static final LeafNode NULLNODE = new LeafNode("NULL",null);
	LeafNode current = NULLNODE;
	
	public History(int max)
	{
		super(null);
		maxSize = max;
	}

	public History(int maxsize, boolean uniquenames, boolean ignorecase, boolean keeporder)
	{
		super(null);
		maxSize = maxsize;
		uniqueNames = uniquenames;
		ignoreCase = ignorecase;
		keepOrder = keeporder;
	}
	
	public boolean append(String name)
	{
		if(name != null && name.length() != 0)
		{
			LeafNode newnode = new LeafNode(name, null);
			return (uniqueAppend(newnode));
		}
		return (false);
	}

	public boolean append(String name, Object value)
	{
		if(name != null && name.length() != 0)
		{
			LeafNode newnode = new LeafNode(name, value);
			return (uniqueAppend(newnode));
		}
		return (false);
	}

	public boolean push(String name)
	{
		if(name != null && name.length() != 0)
		{
			LeafNode newnode = new LeafNode(name, null);
			return (uniquePush(newnode));
		}
		return (false);
	}

	public boolean push(String name, Object value)
	{
		if(name != null && name.length() != 0)
		{
			LeafNode newnode = new LeafNode(name, value);
			return (uniquePush(newnode));
		}
		return (false);
	}

	public boolean uniquePush(LeafNode node)
	{
		if(!uniqueNames)
		{
			push(node);
			return (true);
		}
		else if(unique(node.getName()))
		{
			push(node);
			return (true);
		}
		return (false);
	}

	public void appendObject(Object value)
	{
		LeafNode newnode = new LeafNode(null, value);
		addNode(newnode);
	}

	public void pushObject(Object value)
	{
		LeafNode newnode = new LeafNode(null, value);
		pushNode(newnode);
	}

	boolean uniqueAppend(LeafNode node)
	{
		if(!uniqueNames)
		{
			append(node);
			return (true);
		}
		else if(unique(node.getName()))
		{
			append(node);
			return (true);
		}
		return (false);
	}

	void removeFirst()
	{
		removeNode(headNode);
	}

	void removeLast()
	{
		removeNode(tailNode);
	}

	boolean unique(String name)
	{
		LeafNode temp = (LeafNode)headNode;
		while(temp != null)
		{
			if(equals(temp, name))
			{
				if(!keepOrder)
				{
					moveToFirst(temp);
				}
				return (false);
			}
			temp = (LeafNode)temp.getNextNode();
		}
		return (true);
	}

	boolean equals(LeafNode ln, String n)
	{
		String temp = ln.getName();
		if(temp == null)
		{
			return (false);
		}
		else if(ignoreCase)
		{
			return (n.equalsIgnoreCase(temp));
		}
		return (n.equals(temp));
	}

	boolean equals(LeafNode ln1, LeafNode ln2)
	{
		String temp = ln2.getName();
		if(temp != null)
		{
			return (equals(ln1, temp));
		}
		return (false);
	}

	void push(LeafNode n)
	{
		if(size == maxSize)
		{
			removeLast();
		}
		pushNode(n);
	}

	void append(LeafNode n)
	{
		if(size == maxSize)
		{
			removeFirst();
		}
		addNode(n);
	}

	public void remove(String name)
	{
		Node node = getNode(name);
		if(node != null)
		{
			removeNode(node);
		}
	}
	
	public void remove()
	{
		removeNode(current);
	}

	public void remove(int index)
	{
		if(index<size)
		{
			setCurrent(index);
			remove();
		}
	}	
	
	public void moveCurrentToLast()
	{
		moveToLast(current);
	}

	public void moveCurrentToFirst()
	{
		moveToFirst(current);
	}

	public void moveToLast(LeafNode node)
	{
		if(equals((LeafNode)tailNode, node))
		{
			return;
		}
		removeNode(node);
		append(node);
	}

	public void moveToFirst(LeafNode node)
	{
		if(equals((LeafNode)headNode, node))
		{
			return;
		}
		removeNode(node);
		push(node);
	}

	public void resetCurrent()
	{
		current = NULLNODE;
	}

	protected boolean atStart()
	{
	   return(current==NULLNODE);
	}
	
	public boolean next()
	{
	   if(current != null)
      {
   		if(atStart())
   		{
   			current = (LeafNode)headNode;
   		}
   		else
   		{
   			current = (LeafNode)current.getNextNode();
   		}
   	   if(current != null)
   	   {
   	      return (true);
   	   }
		}
		return (false);
	}

	public boolean prev()
	{
		if(atStart())
		{
			current = (LeafNode)tailNode;
		}
		else if(current!=null)
		{
			current = (LeafNode)current.getPrevNode();
		}
		if(current!=null)
		{
			return (true);
		}
		return (false);
	}
	
   public boolean first()
   {
      if(current != null)
      {
         current = (LeafNode)headNode;

         if(current != null)
         {
            return (true);
         }
      }
      return (false);
   }

   public boolean last()
   {
      current = (LeafNode)tailNode;

      if(current!=null)
      {
         return (true);
      }
      return (false);
   }	

	public String getName()
	{
      if(current!=null)
      {	   
         return (current.getName());
      }
      return(null);		
	}

	public Object getValue()
	{
	   if(current!=null)
	   {
	      return (current.getValue());
	   }
	   return(null);
	}
	
	public void setCurrent(int index)
	{
		resetCurrent();
		for(int i=0; i<index+1; i++)
		{
			next();
		}
	}
	
	public void setCurrent(String name)
	{
		resetCurrent();
		String temp = null;
		while(!name.equals(temp) && next())
		{
			temp = getName();
		}
	}	
	
	public String getName(int index)
	{
		if(index<size)
		{
			setCurrent(index);
			return (getName());
		}
		return(null);
	}

	public Object getValue(int index)
	{
		if(index<size)
		{
			setCurrent(index);
			return (getValue());
		}
		return(null);
	}	

	public Object getValue(String name)
	{
		setCurrent(name);
		return(getValue());
	}
	
	public void truncateAfter(String name)
	{
		setCurrent(name);
		truncate();
	}
	
	public void truncate()
	{
		if(tailNode != current)
		{
			tailNode.setPrevNode(null);
			tailNode = current;
			current.setNextNode(null);
		}
	}
	
	public void setNames(String[] array)
	{
		for(int i=0; i<array.length; i++)
		{
			append(array[i]);
		}
	}
	
	public String[] getNames()
	{
		if(size > 0)
		{
			String[] values = new String[size];
			Node node = headNode;

			for(int i = 0; i < size; i++)
			{
				values[i] = node.getName();
				node = node.getNextNode();
			}
			return (values);
		}
		return (null);
	}

	public Object[] getValues()
	{
		if(size > 0)
		{
			Object[] values = new Object[size];
			LeafNode node = (LeafNode)headNode;
			for(int i = 0; i < size; i++)
			{
				values[i] = node.getValue();
				node = (LeafNode)node.getNextNode();
			}
			return (values);
		}
		return (null);
	}

}