package com.sok.framework.generation.nodes.sql;

//import com.sok.framework.ActionBean;
import com.sok.framework.generation.nodes.RootNode;
import com.sok.framework.generation.nodes.sql.join.*;

public class SubSelectSetNode extends SelectSetNode implements Joinable
{
   WhereSetNode wheresetnode;
   String vscript;
   
   JoinOnSetNode joinons = null;     
   
   public SubSelectSetNode(String name)
   {
      super(name);
   }
   
   public void setJoinOnSetNode(JoinOnSetNode joinons)
   {
      this.joinons = joinons;
   }     
   
   public void setWhereSetNode(WhereSetNode wheresetnode)
   {
      this.wheresetnode = wheresetnode;
   }
   
   public void setScript(String vscript)
   {
      this.vscript = vscript;
   }
   
   public String getScript()
   {
      return(vscript);
   }
   
   protected void appendOut(RootNode root, StringBuffer buffer)
   {
      buffer.append(rn);
      buffer.append(sql_bracketbegin);
      buffer.append(sql_select);
   }
   
   protected void appendSeparator(RootNode root, StringBuffer buffer)
   {
      buffer.append(sql_comma);
   }
   
   protected void appendEnd(RootNode root, StringBuffer buffer)
   {
      if(fromsetnode!=null)
      {
         fromsetnode.generate(root, buffer);
      }
      if(wheresetnode!=null)
      {
         wheresetnode.generate(root, buffer);
      }
      if(joinons!=null)
      {
         joinons.generate(root, buffer);
      }      
      if(vscript!=null)
      {
         root.getDatabase().appendConditions(vscript, buffer);
      }
      buffer.append(sql_bracketend);
      if(name!=null)
      {
         buffer.append(sql_as);
         buffer.append(name);
      }
   }
   
   public void generate(RootNode root, StringBuffer buffer)
   {
      super.generate(root, buffer);
      
   }
   
   public int getNodeType()
   {
      return(NodeType.type_SubSelectSet);
   }
}