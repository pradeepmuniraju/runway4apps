package com.sok.framework.generation.nodes.sql;

import java.sql.PreparedStatement;

import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.*;
import java.sql.SQLException;
//import com.sok.framework.generation.*;

public abstract class AbstractSetterGeneratorSetNode
    extends AbstractGeneratorSetNode
    implements SetterSetNode
{
    public AbstractSetterGeneratorSetNode(String name)
    {
        super(name);
    }
    
    public int setStatementValues(PreparedStatement updatestm, Row row, int index)
    {
        Node node = getHeadNode();
        if(node!=null)
        {
            while(node!=null)
            {
                if(node instanceof SetterSetNode)
                {
                    index = ((SetterSetNode)node).setStatementValues(updatestm, row, index);
                }
                else if(node instanceof SetterNode)
                {
                    try
                    {
                        index = ((SetterNode)node).setValue(updatestm, row, index);
                    }
                    catch(SQLException e)
                    {
                        StringBuffer error = new StringBuffer();
                        error.append("Error setting value on Prepered statement at Node: ");
                        error.append(node.toString());
                        throw new DatabaseException(e, error.toString());
                    }
                }
                node = node.getNextNode();
            }
        }
        return(index);
    }
      
}