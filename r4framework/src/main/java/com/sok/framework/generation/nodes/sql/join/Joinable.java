package com.sok.framework.generation.nodes.sql.join;

public interface Joinable
{
   public void setJoinOnSetNode(JoinOnSetNode joinons);
}