package com.sok.framework.generation.nodes.sql.search;
import java.sql.Types;

import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.nodes.sql.*;
import com.sok.framework.sql.*;

public class DateTimeConstraint extends AbstractConstraint implements GenerationKeys
{
   public DateTimeConstraint()
   {
      super();
   }
   
   protected void appendSingleConstraint(RootNode root, WhereNode node, StringBuffer out, String value)
   {
      SqlDatabase database = root.getDatabase();
      if(value.indexOf(value_NULL)>=0)
      {
         super.appendSingleConstraint(root, node, out, value);
      }
      else if(database.getDateRangeIndex(value) > 0)
      {
         database.appendFieldRangeValue(out, node.getFullName(), value, Types.TIMESTAMP, root.getDateConversionsType(), false);
      }
      else
      {
         database.appendFieldOpTimestampValue(out, node.getFullName(),
               value, root);
      }
   }
/*
    protected void appendSingleConstraint(RootNode root, WhereNode node, StringBuffer out, String value)
    {
        SqlDatabase database = root.getDatabase();
        int index = database.getDateRangeIndex(value);
        String name = node.getFullName();
 
        boolean omit = node.isOmit();
        if(value.indexOf('!')==0)
        {
            value =  value.substring(1, value.length());
            omit = !omit;
        }
 
        if(index > 0)
        {
            String gtvalue = value.substring(0, index);
            String ltvalue = value.substring(index + 1, value.length());
            database.appendDateRangeStmt(out, name, gtvalue, ltvalue, node.getInputFormat(), omit);
        }
        else if(value.equals(ActionBean.TODAY))
        {
            database.appendTodayStmt(out, name, omit);
        }
        else if(value.equals(ActionBean.THISWEEK))
        {
            database.appendThisWeekStmt(out, name, omit);
        }
        else if(value.equals(ActionBean.THISMONTH))
        {
            database.appendThisMonthStmt(out, name, omit);
        }
        else if(value.equals(ActionBean.FUTURE))
        {
            database.appendFutureStmt(out, name, omit);
        }
        else if(value.equals(ActionBean.OVERDUE))
        {
            database.appendOverdueStmt(out, name, omit);
        }
        else if(value.equals(ActionBean.CURRENTWEEK))
        {
            database.appendCurrentWeekStmt(out, name, omit);
        }
        else if(value.equals(ActionBean.CURRENTMONTH))
        {
            database.appendCurrentMonthStmt(out, name, omit);
        }
        else if(value.equals(ActionBean.CURRENTYEAR))
        {
            database.appendCurrentYearStmt(out, name, omit);
        }
        else if(value.equals(ActionBean.LASTWEEK))
        {
            database.appendLastWeekStmt(out, name, omit);
        }
        else if(value.equals(ActionBean.LASTMONTH))
        {
            database.appendLastMonthStmt(out, name, omit);
        }
        else if(value.equals(ActionBean.LASTYEAR))
        {
            database.appendLastYearStmt(out, name, omit);
        }
        else if(value.indexOf(ActionBean.LAST) == 0
                && value.indexOf(ActionBean.WEEKS) > 0)
        {
            database.appendLastRangeStmt(out, name,
                    ActionBean.getLastNumWeeksCal(value),
                    ActionBean.getLastNumWeeksInitCal(value),
                    omit);
        }
        else if(value.indexOf(ActionBean.LAST) == 0
                && value.indexOf(ActionBean.DAYS) > 0)
        {
            database.appendLastRangeStmt(out, name,
                    ActionBean.getLastNumDaysCal(value),
                    ActionBean.getYesterdayCal(),
                    omit);
        }
        else if(value.indexOf(ActionBean.LAST) == 0
                && value.indexOf(ActionBean.MONTHS) > 0)
        {
            database.appendLastRangeStmt(out, name,
                    ActionBean.getLastNumMonthsCal(value),
                    ActionBean.getLastNumMonthsInitCal(value),
                    omit);
        }
        else if(value.indexOf(ActionBean.NEXT) == 0
                && value.indexOf(ActionBean.WEEKS) > 0)
        {
            database.appendNextRangeStmt(out, name,
                    ActionBean.getNextNumWeeksCal(value),
                    ActionBean.getNextNumWeeksInitCal(value),
                    omit);
        }
        else if(value.indexOf(ActionBean.NEXT) == 0
                && value.indexOf(ActionBean.DAYS) > 0)
        {
            database.appendNextRangeStmt(out, name,
                    ActionBean.getNextNumDaysCal(value),
                    new GregorianCalendar(),
                    omit);
        }
        else if(value.indexOf(ActionBean.NEXT) == 0
                && value.indexOf(ActionBean.MONTHS) > 0)
        {
            database.appendNextRangeStmt(out, name,
                    ActionBean.getNextNumMonthsCal(value),
                    ActionBean.getNextNumMonthsInitCal(value),
                    omit);
        }
        else if(value.indexOf(ActionBean.HOUR) == 0)
        {
            database.appendHourStmt(out, name, ActionBean
                    .getHourString(value), omit);
        }
        else
        {
                appendProcessedDatatimeValue(root, node, out, value);
        }
    }
 
    protected void appendProcessedDatatimeValue(RootNode root, WhereNode node, StringBuffer out, String value)
    {
        //this won't do !dd/MM/yyyy searches
 
        boolean omit = node.isOmit();
        String valuestr[] = setOp(node.getName(), value, omit);
        SqlDatabase database = root.getDatabase();
        if(valuestr[valueix].equals(value_NULL))
        {
            out.append(node.getFullName());
            out.append(valuestr[opix]);
            if(valuestr[valueix].indexOf('!')==0)
            {
                valuestr[valueix] =  valuestr[valueix].substring(1, value.length());
                omit = !omit;
            }
            out.append(value_NULL);
        }
        else if(valuestr[valueix].equals(ActionBean.NOW))
        {
            out.append(node.getFullName());
            out.append(valuestr[opix]);
            if(valuestr[valueix].indexOf('!')==0)
            {
                valuestr[valueix] =  valuestr[valueix].substring(1, value.length());
                omit = !omit;
            }
            out.append(database.getCurrentDateMethod());
        }
        else
        {
 
            if(valuestr[valueix].indexOf('>')==0)
            {
 
            }
            else if(valuestr[valueix].indexOf('<')==0)
            {
 
            }
                database.appendDateRangeStmt(out, node.getFullName(), valuestr[valueix], valuestr[valueix], root.getDateConversionsType(), omit);
        }
    }
 
 
 */
   protected void appendProcessedValue(StringBuffer out, String valuestr, RootNode node)
   {
      out.append(valuestr);
   }
}