package com.sok.framework.generation.nodes.sql;

import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.SqlDatabase;

public class GroupByNode extends AbstractGeneratorNode implements PrioritisedNode
{
	String tablename;
	int num = 0;
	
	public GroupByNode(String tablename, String fieldname, int num)
	{
		super(fieldname);
		this.tablename = tablename;
		this.num = num;
	}
	
	public String getTableName()
	{
		return(tablename);
	}

	public String getFieldName()
	{
		return(name);
	}	
	
	public int getPriority()
	{
		return(num);
	}		
	
	public void generate(RootNode root, StringBuffer out)
	{
	   SqlDatabase database = root.getDatabase();
		if(tablename!=null)
		{
		   database.appendWithQuote(out,RelationSpec.escapeRelationName(tablename));
			out.append(sql_dot);
		}
		database.appendWithQuote(out,name);
	}
    
    public int getNodeType()
    {
        return(NodeType.type_GroupBy);
    }  	
}