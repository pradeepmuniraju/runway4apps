package com.sok.framework.generation.nodes;

public abstract class AbstractGeneratorSetPrioritisedNode extends AbstractGeneratorSetNode
    implements PrioritisedNode
{
    protected int priority = 0;
    
    public AbstractGeneratorSetPrioritisedNode(String name)
    {
        super(name);
    }
    
    public int getPriority()
    {
        return(priority);
    }
}