package com.sok.framework.generation.util;

import com.sok.framework.generation.nodes.RequestNode;

public class RelatedFieldName
   {
	   public int lastindex = -1;
	   public String relations = null;
	   public String fieldname = null;
	   public String fullname = null;

	   public RelatedFieldName(String name)
	   {
		   fullname = name;
   			lastindex = name.lastIndexOf('.');
	    	if(lastindex > -1)
	    	{
		        relations = name.substring(0, lastindex);
		        fieldname = name.substring(lastindex+1, name.length());
	    	}
	    	else
	    	{
	    		fieldname = name;
	    	}
	   }
	   
	   protected void setRelations(String s)
	   {
		   relations = s;
		   fullname = new StringBuffer(s).append('.').append(fieldname).toString();
	   }
	   
	   public String getLastRelation()
	   {
		   return(relations.substring(relations.lastIndexOf('.')+1, relations.length()));
	   }
	   
	   public boolean hasSubRelations()
	   {
		   return(relations.indexOf('.') > -1);
	   }
	   
	   public boolean hasRelations()
	   {
		   return(lastindex > -1);
	   }	   
	   
	   public String toString()
	   {
		   return(fullname);
	   }
	   
	   public boolean hasRelationSuffix()
	   {
		   return(relations.indexOf('#')>0);
	   }
	   
	   public void appendRelationSuffix(String suffix, int index)
	   {
		   StringBuffer buffer = new StringBuffer(relations);
		   if(!hasRelationSuffix())
		   {
			   buffer.append('#');
		   }
		   buffer.append(suffix);
		   if(index>-1)
		   {
			   buffer.append(String.valueOf(index));
		   } 
		   setRelations(buffer.toString());
	   }
}