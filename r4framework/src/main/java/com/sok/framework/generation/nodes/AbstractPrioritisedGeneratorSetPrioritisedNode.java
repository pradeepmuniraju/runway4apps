
package com.sok.framework.generation.nodes;

public abstract class AbstractPrioritisedGeneratorSetPrioritisedNode
    extends AbstractPrioritisedGeneratorSetNode
    implements PrioritisedNode
{
    protected int priority = 0;
    
    public AbstractPrioritisedGeneratorSetPrioritisedNode(String name)
    {
        super(name);
    }
    
    public int getPriority()
    {
        return(priority);
    }
}