package com.sok.framework.generation;
public interface GenerationKeys
{
	public static final String _viewspec = "-viewSpec";
	public static final String _tablespec = "-tableSpec";
	public static final String _actionspec = "-actionSpec";	
	public static final String _jndiname = "-sqlJndiName";
	public static final String _sort_prefix = "-sort";
	public static final String _order_prefix = "-order";
	public static final String _groupby_prefix = "-groupby";
    public static final String _having_prefix = "-having";    
	public static final String _limit_results = "-top";
	public static final String _action = "-action";
	public static final String _select = "-select";  //adds select items in no viewspec mode
    public static final String _idonly = "-idonly";	
    public static final String _join_prefix = "-join";
	//public static final String _updatestmt = "~updatestmt";
	public static final String at = "@";
	
	public static final String _true = "true";
	
	public static final String UPDATE = "update";
	public static final String UPDATEALL = "updateall";
	public static final String INSERT = "insert";
	public static final String DELETE = "delete";
	public static final String DELETEALL = "deleteall";
	public static final String SELECT = "select";
    public static final String SELECTFIRST = "selectfirst";  
	public static final String SEARCH = "search";
	public static final String EXECUTE = "execute";
	public static final String UPDATEON = "ON-";
	
	static final String INNER_JOIN = " = ";
	static final String OUTER_LEFT_JOIN = " *= ";
	static final String OUTER_RIGHT_JOIN = " =* ";	
    
    static final String TYPE_DATETIME = "datetime";
    static final String TYPE_DATE = "date";
    static final String TYPE_TIME = "time";
    static final String TYPE_NUMBER = "number";
    static final String TYPE_LONG = "long"; 
    static final String TYPE_PERCENT = "percent";
    static final String TYPE_CURRENCY = "currency";
    static final String TYPE_DECIMAL = "decimal";
    static final String TYPE_BOOLEAN = "boolean";    
    
    public static final String _blank = ""; 
    public static final String _rn = "\r\n"; 
    
    public static final String value_omit = "omit";  
    public static final String value_NULL = "NULL";      
    public static final String value_EMPTY = "EMPTY";
    public static final String value_CURRENTUSER = "CURRENTUSER";
    public static final String key_CURRENTUSERID = "CURRENTUSERID";    
}