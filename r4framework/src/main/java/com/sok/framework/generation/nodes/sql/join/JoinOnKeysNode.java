package com.sok.framework.generation.nodes.sql.join;

import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.AbstractGeneratorNode;
import com.sok.framework.generation.nodes.RootNode;

public class JoinOnKeysNode extends JoinOnNode
{
   protected String totablename = null;   
   protected String fromtablename = null;   
   protected String fromkeyname = null;
   protected String tokeyname = null;
   
   public JoinOnKeysNode(String totablename, String tokeyname, String fromtablename, String fromkeyname)
   {
      super(tokeyname);
      this.totablename = totablename;    
      this.fromtablename = fromtablename;
      this.fromkeyname = fromkeyname;
   }
   
   public void generate(RootNode node, StringBuffer out)
   {
         out.append(RelationSpec.escapeRelationName(fromtablename));
         out.append(sql_dot);
         out.append(fromkeyname);       
       
         out.append(sql_equals);
       
         out.append(RelationSpec.escapeRelationName(totablename));
         out.append(sql_dot);
         out.append(name);    
   }   
}