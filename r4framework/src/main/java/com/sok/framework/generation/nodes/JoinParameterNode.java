package com.sok.framework.generation.nodes;

import com.sok.framework.generation.*;

public class JoinParameterNode extends AbstractParameterNode implements SQLKeys
{
    String relations;
    
    
    public JoinParameterNode(String name, Object value)
    {    
        super(name, value);
        super.priority = 10;
        initValue();
    }
    
    public int getNodeType()
    {
        return(type_JoinParameter);
    }    
    
    public Object setValue(Object value)
    {
        Object obj = super.setValue(value);
        initValue();
        return(obj);
    }       
    
    protected void initValue()
    {
        relations = value.toString();
    }   
    
    public String getRelations()
    {
        return(relations);
    }
          
}