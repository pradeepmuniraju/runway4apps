package com.sok.framework.generation.nodes.sql;

import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.*;

public class SelectNode extends AbstractGeneratorNode
{
	String tablename, fieldname, fieldalias;
	
	public SelectNode(String tablename, String fieldname, String fieldalias)
	{
		super(fieldalias!=null?fieldalias:fieldname);
		this.fieldname = fieldname;
		this.tablename = tablename;
		this.fieldalias = fieldalias;		
	}
	
	public String getTableName()
	{
		return(tablename);
	}

	public String getFieldName()
	{
		return(fieldname);
	}	

	public String getFieldAlias()
	{
		return(fieldalias);
	}	
	
	public void generate(RootNode root, StringBuffer out)
	{
      SqlDatabase database = root.getDatabase();
      if(fieldname.indexOf('(') < 0)
      {
   		if(tablename!=null)
   		{
   			database.appendWithQuote(out,RelationSpec.escapeRelationName(tablename));
   			out.append(sql_dot);
   		}
   	   database.appendWithQuote(out,fieldname);
      }
      else
      {
         out.append(fieldname);
      }

		if(fieldalias != null)
		{
			out.append(sql_as);
			/* use appendAlias here as we may want to quote the alias */
			database.appendAliasWithQuote(out,fieldalias);
		}
	}
    
    public int getNodeType()
    {
        return(NodeType.type_Select);
    }  
}