package com.sok.framework.generation;

import java.sql.*;

public interface SQLKeys
{   
	static final String sql_comma = ", ";
	static final String sql_dot = ".";
	static final String sql_asterisk = "*";
	static final String sql_equals = " = ";
	static final String sql_like = " LIKE ";
	static final String sql_not_like = " NOT LIKE ";	
	static final String sql_not_equals = " <> ";		
	static final String sql_greater_than = " < ";
	static final String sql_less_than = " > ";
	static final String sql_greater_than_or_equals = " <= ";
	static final String sql_less_than_or_equals = " >= ";	
	static final String sql_as = " AS ";		
	static final String sql_and = " AND ";
    static final String sql_not = " NOT ";    
	static final String sql_or = " OR ";	
	static final String sql_on = " ON ";
    static final String sql_is = " IS ";    
    static final String sql_is_not = " IS NOT ";     
    static final String sql_update = "UPDATE ";
    static final String sql_set = " SET ";  
    static final String sql_questionmark = "?";    
    static final String sql_insert = "INSERT ";      
    static final String sql_into = "INTO ";
    static final String sql_values = " VALUES ";  
    static final String sql_delete = "DELETE ";     
	static final String sql_select = "SELECT ";
	static final String sql_from = " FROM ";
	static final String sql_sum = " SUM";
	static final String sql_count = " COUNT";	
	static final String sql_union = " UNION ";
    static final String sql_having = " HAVING ";    
	static final String sql_distinct = " DISTINCT ";		
	static final String sql_where = " WHERE ";
	static final String sql_exists = " EXISTS ";
	static final String sql_groupby = " GROUP BY ";
	static final String sql_orderby = " ORDER BY ";	
	static final String sql_asc = " ASC ";
	static final String sql_desc = " DESC ";
	static final String sql_top = " TOP ";
	static final String sql_limit = " LIMIT ";		
	static final String sql_bracketbegin = "(";
	static final String sql_bracketend = ")";
	static final String sql_inner_join = " INNER JOIN ";
	static final String sql_outer_left_join = " LEFT OUTER JOIN ";
	static final String sql_outer_right_join = " RIGHT OUTER JOIN ";	
	static final String rn = "\r\n";
	static final String space = " ";		
    
    static final int jdbctype_int = Types.INTEGER;
    static final int jdbctype_string = Types.VARCHAR;
    static final int jdbctype_datetime = Types.TIMESTAMP;
    static final int jdbctype_float = Types.FLOAT;     
     
}