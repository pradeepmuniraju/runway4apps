package com.sok.framework.generation;

public class IDNotFoundException extends TracedException
{
	StringBuffer error = new StringBuffer();
	
	public IDNotFoundException(String idname, String action)
	{
		super();
		error.append("Expected primary key parameter ");
		if(idname!=null)
		{
			error.append(idname);
		}
		error.append(" not found");
		if(action!=null)
		{
			error.append(" for action ");
			error.append(action);
		}
	}
	
	public String getMessage()
	{
		return(error.toString());
	}
	
	public String toString()
	{
		return(getMessage());
	}
}