package com.sok.framework.generation.nodes.sql.join;

import java.util.StringTokenizer;

import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.*;
import com.sok.framework.*;
import com.sok.framework.generation.nodes.sql.*;
/*
 *  <relation priority="100" relationname="ProductAnswer" fromtable="Products" totable="Answers" fromkey="ProductID" tokey="ProductID" relationtype=" *= ">
 *      <on tokey="QuestionID" tokeyvaluename="ProductAnswerQuestionID" />
 *  </relation>
 *  
 *  tokeyvalue attribute replaces the value with a parameter from the aliased relation name. e.g genrow.setParameter("ProductAnswer#ProductAnswerQuestionID=SOMEID.Answer","some answers2");
 *  ...
 *  WHERE
 *   EXISTS (SELECT ProductAnswer_ProductAnswerQuestionIDSOMEID.ProductID FROM Answers AS ProductAnswer_ProductAnswerQuestionIDSOMEID WHERE ProductAnswer_ProductAnswerQuestionIDSOMEID.ProductID = Products.ProductID AND ProductAnswer_ProductAnswerQuestionIDSOMEID.QuestionID = 'SOMEID' AND ProductAnswer_ProductAnswerQuestionIDHELLOWORLD.Answer LIKE 'some answers2')
 *  
 *  
 *  Other uses -
 *  <on tokey="QuestionID" tokeyvalue="SOMESTATICVALUE" />
 *  
 *  and
 *  
 *  <on tokey="QuestionID" fromkey="QuestionID" />
 *  
 *  works like the ones in relation tag.
 *  
 *  
 *  limitations -
 *  
 *  you cannot currently do
 *  <on fromkey="QuestionID" fromkeyvaluename="ProductAnswerQuestionID" /> 
 * 
 */
public class JoinOnSetNode extends AbstractGeneratorSetNode implements SQLKeys
{
   NodeMap params = null;
   
   public JoinOnSetNode(String relationname)
   {
      super(relationname);
      params = RelationSpec.getRelationParameters(relationname);
   }     
   
   public NodeMap getParameters()
   {
      return(params);
   }
   
   public int getNodeType()
   {
      return(NodeType.type_JoinOnSet);
   }   
   
   public JoinOnNode setJoingOn(String totable, String fromtable, String tokey, String fromkey, String keyvalue, String keyvaluename)
   {
      JoinOnNode onnode = null;
      if(keyvaluename!=null && keyvaluename.length()!=0)
      {
         onnode = new JoinOnNamedValueNode(totable, tokey, keyvaluename, params);
      }
      else if(keyvalue!=null && keyvalue.length()!=0)
      {
         onnode = new JoinOnValueNode(totable, tokey, keyvalue);
      }      
      else if(fromkey!=null && fromkey.length()!=0)
      {
         onnode = new JoinOnKeysNode(totable, tokey, fromtable, fromkey);
      }    
      else
      {
         throw new IllegalConfigurationException("No suitable join on node type found");
      }
      addNode(onnode);
      return(onnode);
   }  
   
   public void generate(RootNode root, GeneratorNode afnode, StringBuffer buffer)
   {
         super.generate(root, afnode, buffer);
   }
   
   protected void appendOut(RootNode root, StringBuffer buffer)
   {
      if(this.childrenHasOutPut())
      {
         buffer.append(sql_and);
      }
   }   
   
   protected void appendSeparator(RootNode root, StringBuffer buffer)
   {
      buffer.append(sql_and);
   }
   
   protected void appendEnd(RootNode root, StringBuffer buffer)
   {
      //buffer.append("***join set end***");
   }
   
}