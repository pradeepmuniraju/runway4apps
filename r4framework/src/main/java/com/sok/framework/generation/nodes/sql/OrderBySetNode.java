package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;

public class OrderBySetNode extends AbstractPrioritisedGeneratorSetNode
{
	public OrderBySetNode()
	{
		super(sql_orderby);    
	}	
	
	public OrderBySetNode(String name)
	{
		super(name);
	}
	
	public OrderByNode setOrderBy(String tablename, String fieldname, String order, int num)
	{
		OrderByNode node = new OrderByNode(tablename, fieldname, order, num);
		addPrioritisedNode(node);
		return(node);
	}		
	
	protected void appendOut(RootNode root, StringBuffer buffer)
	{
        buffer.append(rn);        
		buffer.append(sql_orderby);
	}
	
	protected void appendEnd(RootNode root, StringBuffer buffer)
	{

	}

	protected void appendSeparator(RootNode root, StringBuffer buffer)
	{
		buffer.append(sql_comma);
	}
    
    public int getNodeType()
    {
        return(NodeType.type_OrderBySet);
    }      
}