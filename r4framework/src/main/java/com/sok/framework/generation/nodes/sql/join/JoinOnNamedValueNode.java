package com.sok.framework.generation.nodes.sql.join;

import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.AbstractGeneratorNode;
import com.sok.framework.generation.nodes.RootNode;
import com.sok.framework.generation.NodeMap;

public class JoinOnNamedValueNode extends JoinOnValueNode
{  
   NodeMap params = null;
   
   public JoinOnNamedValueNode(String tablename, String name, String valuename, NodeMap params)
   {
      super(tablename, name, valuename);
      this.params = params;
   }
   
   public boolean hasOutPut()
   {
      return(params!=null);
   }
   
   public void generate(RootNode node, StringBuffer out)
   {
         out.append(RelationSpec.escapeRelationName(tablename));
         out.append(sql_dot);
         out.append(name);       
       
         out.append(sql_equals);
       
          out.append('\'');
          out.append(params.getString(value));
          out.append('\'');
   }   
}