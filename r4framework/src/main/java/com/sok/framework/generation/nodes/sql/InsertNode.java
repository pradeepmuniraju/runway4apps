package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.*;

public class InsertNode extends AbstractSetterGeneratorNode
{
    Object fieldvalue;
    int datatype = jdbctype_string;
    String format;
    int setindex = -1;
    
    public InsertNode(String fieldname, Object fieldvalue, int datatype, String format)
    {
        super(fieldname);
        this.fieldvalue = fieldvalue;
        this.datatype = datatype;
        this.format = format;
    }  
    
    public int getSetIndex()
    {
        return(setindex);
    }
    
    public Object getValue()
    {
        return(fieldvalue);
    }
    
    public String getString()
    {
        return(fieldvalue.toString());
    }
    
    public String getFormat()
    {
        return(format);
    }
    
    public int getDataType()
    {
        return(datatype);
    }
    
    public void generate(RootNode root, StringBuffer out)
    {
       SqlDatabase database = root.getDatabase();
        database.appendWithQuote(out,name);
    }  
    
    public int getNodeType()
    {
        return(NodeType.type_Insert);
    }     
    
}