package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.*;

public class LimitNode extends AbstractGeneratorNode
{
	int limit = 0;
	int skip = 0;

    public LimitNode(String name, int limit)
    {
        super(name);
        this.limit = limit;
    }    
    
	public LimitNode(String name, int skip, int limit)
	{
		super(name);
        this.limit = limit;
        this.skip = skip;
	}
	
	public void generate(RootNode root, StringBuffer out)
	{
	    SqlDatabase database = root.getDatabase();
        if(limit != 0)
        {
            database.appendTopStatement(out, limit);
        }
	}
    
    public int getNodeType()
    {
        return(NodeType.type_Limit);
    }      
}