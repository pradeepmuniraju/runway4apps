package com.sok.framework.generation;

public class TableSpecNotFoundException extends TracedException
{
	StringBuffer error = new StringBuffer();
	
	public TableSpecNotFoundException(String name)
	{
		super();
		error.append("Expected TableSpec ");
		if(name!=null)
		{
			error.append(name);
		}
		error.append(" not found");
	}
	
	public String getMessage()
	{
		return(error.toString());
	}
	
	public String toString()
	{
		return(getMessage());
	}
}