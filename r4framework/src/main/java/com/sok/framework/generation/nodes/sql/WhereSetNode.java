package com.sok.framework.generation.nodes.sql;

//import com.sok.framework.ActionBean;
import com.sok.framework.generation.nodes.*;

public class WhereSetNode extends AbstractPrioritisedGeneratorSetPrioritisedNode
{
    
	public WhereSetNode()
	{
		super(sql_where);   
	}		
	
	public WhereSetNode(String name)
	{
		super(name);
	}	
	
	public WhereNode setWhere(String name, Object value, String tablename, int datatype)
	{
		WhereNode node = new WhereNode(name, value, tablename, datatype);
		addPrioritisedNode(node);
		return(node);
	}	

	public WhereNode setWhere(String fromtable, String fromfield,
            String totable, String tofield,
            String vscript)
	{
		WhereNode node = new JoinWhereNode(fromtable, fromfield, totable, tofield);
        if(vscript!=null)
        {
            node.setScript(vscript);
        }        
        addPrioritisedNode(node);
		return(node);
	}
	
	public void setWhere(WhereNode node)
	{
	   addPrioritisedNode(node);
	}	
    
	protected void appendOut(RootNode root, StringBuffer buffer)
	{
		buffer.append(rn);
		buffer.append(sql_where);
	}
	
	protected void appendEnd(RootNode root, StringBuffer buffer)
	{

	}

	protected void appendSeparator(RootNode root, StringBuffer buffer)
	{
		buffer.append(sql_and);
	}

    public int getNodeType()
    {
        return(NodeType.type_WhereSet);
    }  
    
    public void toggleOmit()
    {
        Node node = getHeadNode();
        int nodetype = 0;
        while(node!=null)
        {
            nodetype = node.getNodeType();
            if(nodetype == NodeType.type_Where)
            {
                ((WhereNode)node).toggleOmit();
            }
            else if(nodetype == NodeType.type_WhereSet || nodetype == NodeType.type_WhereGroupSet)
            {
                ((WhereSetNode)node).toggleOmit();
            }
                
            node = node.getNextNode();
        }
    }    
    
    public boolean hasOutPut()
    {
       if(this.size()!=0)
       {
          return(true);
       }
       return(false);
    }
}