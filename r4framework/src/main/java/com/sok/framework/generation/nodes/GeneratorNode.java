package com.sok.framework.generation.nodes;

public interface GeneratorNode extends Node
{
	public void generate(RootNode root, StringBuffer buffer);
	
	public boolean hasOutPut();
}