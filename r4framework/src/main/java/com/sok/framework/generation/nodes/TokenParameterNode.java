package com.sok.framework.generation.nodes;

//import com.sok.framework.*;

public class TokenParameterNode extends AbstractParameterNode
{
    public TokenParameterNode(String name, Object value)
    {
        super(name, value);

    }
    
    public int getNodeType()
    {
        return(type_TokenParameter);
    }    
}