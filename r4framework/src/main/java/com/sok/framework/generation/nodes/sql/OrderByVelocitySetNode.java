package com.sok.framework.generation.nodes.sql;

public class OrderByVelocitySetNode extends OrderBySetNode
{
	public OrderByVelocitySetNode()
	{
		super();    
	}	
	
	public OrderByVelocitySetNode(String name)
	{
		super(name);
	}
	
	public OrderByVelocityNode setOrderBy(String fieldname, String order, int num)
	{
		OrderByVelocityNode node = new OrderByVelocityNode(fieldname, order, num);
		addPrioritisedNode(node);
		return(node);
	}	
}