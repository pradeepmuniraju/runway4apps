package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;
import com.sok.framework.*;

public class JoinWhereNode extends WhereNode
{
    protected String fromtable;
    protected String totable;
    protected String tofield;
    
    public JoinWhereNode(String fromtable, String fromfield, String totable, String tofield)
    {
        super(fromfield);
        this.tofield = tofield;
        this.fromtable = fromtable;
        this.totable = totable;
    }   
    
    public void generate(RootNode node, StringBuffer out)
    {
        out.append(RelationSpec.escapeRelationName(fromtable));
        out.append(sql_dot);
        out.append(name);
        
        out.append(sql_equals);
        
        out.append(RelationSpec.escapeRelationName(totable));
        out.append(sql_dot);
        out.append(tofield); 
        
        if(super.vscript!=null)
        {
            node.getDatabase().appendConditions(node, vscript, out);
        }        
    }
     
}