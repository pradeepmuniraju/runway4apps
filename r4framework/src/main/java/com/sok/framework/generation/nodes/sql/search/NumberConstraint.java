package com.sok.framework.generation.nodes.sql.search;
import java.sql.Types;
import java.util.GregorianCalendar;

import com.sok.framework.ActionBean;
import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.nodes.sql.*;
import com.sok.framework.sql.*;

public class NumberConstraint extends AbstractConstraint implements GenerationKeys
{
    public NumberConstraint()
    {
        super();
    }
    
    protected void appendSingleConstraint(RootNode root, WhereNode node, StringBuffer out, String value)
    {
        SqlDatabase database = root.getDatabase();
		int index = database.getRangeIndex(value);

        if(index > 0)
        {
            String gtvalue = value.substring(0, index);
            String ltvalue = value.substring(index + 1, value.length());
            database.appendNumberRangeStmt(out, node.getFullName(), gtvalue, ltvalue, node.getInputFormat(), node.isOmit());
        }      
        else
        {
            super.appendSingleConstraint(root, node, out, value);
        }
    }
       
    
    protected void appendProcessedValue(StringBuffer out, String value, RootNode root)
    {
        if(value.equals(value_NULL))
        {
            out.append(value_NULL);
        }        
        else
        {
            appendValue(out, value);
        }
    }
    
    protected String[] setOp(String name, String value, boolean omit)
    {
        String[] returnvalue = super.setOp(name, value, omit);
        if(returnvalue[opix].equals(sql_like))
        {
            returnvalue[opix] = sql_equals;
        }
        else if(returnvalue[opix].equals(sql_not_like))
        {
            returnvalue[opix] = sql_not_equals;
        }
        
        return(returnvalue);
    }       
}