package com.sok.framework.generation.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.sok.framework.sql.SqlDatabase;

public class CalendarFactory implements CalendarKeys
{
   public static final String defaulttimezone = "Australia/Melbourne"; 
   
   public static String getCurrentDate(String format)
   {
      return (CalendarFactory.getCurrentDate(format, defaulttimezone));
   }   
   
   public static String getCurrentDate(String format, String timezone)
   {
      DateFormat dt = new SimpleDateFormat(format);
      dt.setTimeZone(TimeZone.getTimeZone(timezone));
      return (dt.format(new java.util.Date()));
   }
   
   public static GregorianCalendar getEndOfLastMonthCal()
   {
      GregorianCalendar month = new GregorianCalendar();
      month.add(Calendar.MONTH, -1);
      month.set(Calendar.DATE, month.getActualMaximum(Calendar.DAY_OF_MONTH));
      return (month);
   }
   
   public static GregorianCalendar getStartOfCurrentFinancialYear() {
      GregorianCalendar month = new GregorianCalendar();
      int startMonth = month.get(Calendar.MONTH);
      int startYear = month.get(Calendar.YEAR);
      if(startMonth > 5) {        
         month.set(startYear, 6, 1);
         return  month;
      } else {
         month.set(startYear-1, 6, 1);
         return month;
      }      
   }
   
   public static GregorianCalendar getEndOfCurrentFinancialYear() {
      GregorianCalendar month = new GregorianCalendar();      
      int startMonth = month.get(Calendar.MONTH);
      int startYear = month.get(Calendar.YEAR);
      if(startMonth > 5) {        
         month.set(startYear+1, 5, 30);
         return  month;
      } else {
         month.set(startYear, 5, 30);
         return month;
      }            
   }
   
   public static GregorianCalendar getStartOfLastMonthCal()
   {
      GregorianCalendar month = new GregorianCalendar();
      month.add(Calendar.MONTH, -1);
      month.set(Calendar.DATE, 1);
      return (month);
   }   
   
   public static GregorianCalendar getEndOfLastWeekCal()
   {
      GregorianCalendar week = new GregorianCalendar();
      
      if(week.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
      {
         week.add(Calendar.DATE, -1);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
      {
         week.add(Calendar.DATE, -2);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
      {
         week.add(Calendar.DATE, -3);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
      {
         week.add(Calendar.DATE, -4);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
      {
         week.add(Calendar.DATE, -5);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
      {
         week.add(Calendar.DATE, -6);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
      {
         //week.add(Calendar.DATE, 7);
      }
      return (week);
   }   
   
   public static GregorianCalendar getStartOfLastWeekCal()
   {
      GregorianCalendar week = new GregorianCalendar();
      
      if(week.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
      {
         week.add(Calendar.DATE, -8);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
      {
         week.add(Calendar.DATE, -9);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
      {
         week.add(Calendar.DATE, -10);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
      {
         week.add(Calendar.DATE, -11);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
      {
         week.add(Calendar.DATE, -12);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
      {
         week.add(Calendar.DATE, -13);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
      {
         week.add(Calendar.DATE, -7);
      }
      return (week);
   }   
   
   public static GregorianCalendar getEndOfWeekCal()
   {
      GregorianCalendar week = new GregorianCalendar();
      
      if(week.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
      {
         week.add(Calendar.DATE, 6);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
      {
         week.add(Calendar.DATE, 5);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
      {
         week.add(Calendar.DATE, 4);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
      {
         week.add(Calendar.DATE, 3);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
      {
         week.add(Calendar.DATE, 2);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
      {
         week.add(Calendar.DATE, 1);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
      {
         //week.add(Calendar.DATE, 7);
      }
      return (week);
   }   
   
   public static GregorianCalendar getStartOfWeekCal()
   {
      GregorianCalendar week = new GregorianCalendar();
      
      if(week.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
      {
         week.add(Calendar.DATE, -1);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
      {
         week.add(Calendar.DATE, -2);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
      {
         week.add(Calendar.DATE, -3);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
      {
         week.add(Calendar.DATE, -4);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
      {
         week.add(Calendar.DATE, -5);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
      {
         week.add(Calendar.DATE, -6);
      }
      else if(week.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
      {
         //week.add(Calendar.DATE, -7);
      }
      return (week);
   }   
   
   public static GregorianCalendar getNextNumHoursCal(String field)
   {
      int hours = 0;
      try
      {
         hours = Integer.parseInt(getNextLastHoursString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      day.add(Calendar.HOUR_OF_DAY, hours);
      return (day);
   }
   
   public static GregorianCalendar getNextNumHoursInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int daynum = getInitDayOfWeek(field);
      if(daynum!=-1)
      {
         setNextWeeksInit(cal, daynum);
      }
      return (cal);
   }   
   
   public static GregorianCalendar getNextNumWeeksCal(String field)
   {
      int weeks = 0;
      try
      {
         weeks = Integer.parseInt(getNextLastWeeksString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      int daynum = getInitDayOfWeek(field);
      if(daynum!=-1)
      {
         setNextWeeksInit(day, daynum);
      }
      day.add(Calendar.DATE, (weeks * 7));
      day.add(Calendar.DATE, -1);
      return (day);
   }
   
   public static GregorianCalendar getNextNumWeeksInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int daynum = getInitDayOfWeek(field);
      if(daynum!=-1)
      {
         setNextWeeksInit(cal, daynum);
      }
      return (cal);
   }
   
   protected static void setNextWeeksInit(Calendar cal, int daynum)
   {
      int dayofweek = _gweekdays[daynum];
      int cdayofweek = cal.get(Calendar.DAY_OF_WEEK);
      if(dayofweek > cdayofweek)
      {
         cal.add(Calendar.DATE,  dayofweek - cdayofweek);
      }
      else if(dayofweek == cdayofweek)
      {
         cal.add(Calendar.DATE, 7);
      }
      else
      {
         cal.add(Calendar.DATE, 7 + cdayofweek - dayofweek);
      }
   }
   
   public static GregorianCalendar getNextNumDaysCal(String field)
   {
      int days = 0;
      try
      {
         days = Integer.parseInt(getNextLastDaysString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      day.add(Calendar.DATE, days);
      return (day);
   }
   
   public static GregorianCalendar getNextNumMonthsCal(String field)
   {
      int months = 0;
      try
      {
         months = Integer.parseInt(getNextLastMonthsString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      int daynum = getInitDayOfMonth(field);
      day.add(Calendar.MONTH, months);
      setNextMonthsInit(day, daynum);
      day.add(Calendar.DAY_OF_MONTH, -1);
      return (day);
   }
   
   public static GregorianCalendar getNextNumMonthsInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int daynum = getInitDayOfMonth(field);
      setNextMonthsInit(cal, daynum);
      return (cal);
   }
   
   private static void setNextMonthsInit(Calendar cal, int daynum)
   {
      if(daynum!=-1)
      {
         int currentday = cal.get(Calendar.DAY_OF_MONTH);
         if(daynum != currentday)
         {
            int daysinmonth = cal.getMaximum(Calendar.DAY_OF_MONTH);
            if(daynum > daysinmonth)
            {
               cal.set(Calendar.DAY_OF_MONTH, daysinmonth);
            }
            else
            {
               cal.set(Calendar.DAY_OF_MONTH, daynum);
            }
         }
         if(daynum <= currentday)
         {
            cal.add(Calendar.MONTH, 1);
         }
      }
   }

   public static Calendar getNextNumFinYearsCal(String field)
   {
	   System.out.println(field);
	   Calendar d = getNextNumYearsCal(field);
	   System.out.println(d.getTime().toString());
	   Calendar c = getEndOfFinYearCal(d);
	   System.out.println(c.getTime().toString());
	   c.add(Calendar.YEAR, -1);
	   System.out.println(c.getTime().toString());
	   return c;
   }
   
   public static GregorianCalendar getNextNumYearsCal(String field)
   {
      int years = 0;
      try
      {
         years = Integer.parseInt(getNextLastYearsString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      int monthnum = getInitMonthOfYear(field);
      day.add(Calendar.YEAR, years);
      setNextLastYearsInit(day, monthnum);
      day.add(Calendar.DAY_OF_MONTH, -1);
      return (day);
   }
   
   public static GregorianCalendar getNextNumYearsInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int monthnum = getInitMonthOfYear(field);
      setNextLastYearsInit(cal, monthnum);
      return (cal);
   }
   
   public static GregorianCalendar getCurrNumYearsInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int monthnum = getInitMonthOfYear(field);
      setNextCurrYearsInit(cal, monthnum);
      return (cal);
   }

   private static void setNextLastYearsInit(Calendar cal, int monthnum)
   {
      if(monthnum!=-1)
      {
         cal.set(Calendar.DAY_OF_MONTH, 1);
         cal.set(Calendar.MONTH, monthnum);
      }
   }
   
   private static void setNextCurrYearsInit(Calendar cal, int monthnum)
   {
      if(monthnum!=-1)
      {
      	if(monthnum <= cal.get(Calendar.MONTH))
      	{
      		cal.add(Calendar.YEAR, 1);
      	}
      	
      	cal.set(Calendar.MONTH, monthnum);
      	cal.add(Calendar.MONTH, -1);
         cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
      }
   } 
   
   public static GregorianCalendar getLastNumHoursCal(String field)
   {
      int hours = 0;
      try
      {
         hours = Integer.parseInt(getNextLastHoursString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      day.add(Calendar.HOUR_OF_DAY, 0 - hours);
      return (day);
   }   
   
   public static GregorianCalendar getLastNumWeeksCal(String field)
   {
      int weeks = 0;
      try
      {
         weeks = Integer.parseInt(getNextLastWeeksString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      int daynum = getInitDayOfWeek(field);
      if(daynum!=-1)
      {
         setLastWeeksInit(day, daynum);
      } else if(weeks == 0) {
    	  //ALLOWANCE FOR LAST0WEEKS to be current week
    	  weeks = 1;
      }
      day.add(Calendar.DATE, 0 - (weeks * 7));
      return (day);
   }
   
   /** from a given string, return a tuple of the calendar field and number applicable */
   public static class CalendarPeriod {
	   public final int num;
	   public final int field; 
	   public CalendarPeriod(String key) {
		   if(MINUTES.equals(key)) {
			   num = 1; 
			   field = Calendar.MINUTE;
		   } else if(HOURS.equals(key)) { 
			   num = 1;
			   field = Calendar.HOUR_OF_DAY; 
		   } else if(DAYS.equals(key)) {
			   num = 1;
			   field = Calendar.DAY_OF_YEAR; 
		   } else if(WEEKS.equals(key)) {
			   num = 1;
			   field =Calendar.WEEK_OF_YEAR; 
		   } else if(QUARTERS.equals(key)) {
			   num = 3;
			   field = Calendar.MONTH;
		   } else if(MONTHS.equals(key)) {
			   num = 1;
			   field = Calendar.MONTH; 
		   } else if(YEARS.equals(key)) {
			   num = 1;
			   field = Calendar.YEAR; 
		   } else { 
			   throw new IllegalArgumentException("Key: [" + key + "], was not one of MINUTES HOURS DAYS WEEKS MONTHS QUARTERS YEARS");
		   }
	   }
   }
   
   public static GregorianCalendar getLastNumWeeksInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int daynum = getInitDayOfWeek(field), weeks = 0;
      
      try {
         weeks = Integer.parseInt(getNextLastWeeksString(field));
      } catch(Exception e) { }
      
      if(daynum!=-1 && weeks != 0)
      {
         setLastWeeksInit(cal, daynum);
      }
      if(!field.endsWith(WITHTODAY)) { 
    	  cal.add(Calendar.DATE,  -1);  
      }
      return (cal);
   }
   
   protected static void setLastWeeksInit(Calendar cal, int daynum)
   {
      int dayofweek = _gweekdays[daynum];
      int cdayofweek = cal.get(Calendar.DAY_OF_WEEK);
      if(dayofweek > cdayofweek)
      {
         cal.add(Calendar.DATE, dayofweek - cdayofweek - 7);
      }
      else if(dayofweek < cdayofweek)
      {
         cal.add(Calendar.DATE,  dayofweek - cdayofweek);
      }
   }
   
   public static GregorianCalendar getLastNumDaysCal(String field)
   {
      int days = 0;
      try
      {
         days = Integer.parseInt(getNextLastDaysString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      day.add(Calendar.DATE, 0 - days - getInitDay(field));
      return (day);
   }
   
   public static GregorianCalendar getLastNumDaysInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int daynum = getInitDay(field);
      cal.add(Calendar.DATE,  0 - daynum - 1);
      return (cal);
   }
   
   public static GregorianCalendar getLastNumMonthsCal(String field)
   {
      int months = 0;
      try
      {
         months = Integer.parseInt(getNextLastMonthsString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      day.add(Calendar.MONTH, 0 - months);
      int daynum = getInitDayOfMonth(field);
      setLastMonthsInit(day, daynum);
      return (day);
   }
   
   public static GregorianCalendar getLastNumMonthsInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int daynum = getInitDayOfMonth(field);
      setLastMonthsInit(cal, daynum);
      if(!field.endsWith(WITHTODAY)) { 
    	  cal.add(Calendar.DATE,  -1);  
      }
      return (cal);
   }
   
   private static void setLastMonthsInit(Calendar cal, int daynum)
   {
      if(daynum!=-1)
      {
         int currentday = cal.get(Calendar.DAY_OF_MONTH);
         if(daynum != currentday)
         {
            int daysinmonth = cal.getMaximum(Calendar.DAY_OF_MONTH);
            if(daynum > daysinmonth)
            {
               cal.set(Calendar.DAY_OF_MONTH, daysinmonth);
            }
            else
            {
               cal.set(Calendar.DAY_OF_MONTH, daynum);
            }
         }
         if(daynum >= currentday)
         {
            cal.add(Calendar.MONTH, -1);
         }
      }
   }   
   
   public static Calendar getLastNumFinYearsCal(String field)
   {
      int years = 0;
      try
      {
         years = Integer.parseInt(getNextLastYearsString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      day.add(Calendar.YEAR, 1 - years);
      
      return getStartOfFinYearCal(day);
   }

   public static GregorianCalendar getLastNumYearsCal(String field)
   {
      int years = 0;
      try
      {
         years = Integer.parseInt(getNextLastYearsString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      day.add(Calendar.YEAR, 0 - years);
      int monthnum = getInitMonthOfYear(field);
      setNextLastYearsInit(day, monthnum);
      return (day);
   }
   
   public static boolean isFYear(String field) { 
	   return field != null && field.indexOf("FIN")>-1;
   }
   
   public static GregorianCalendar getCurrNumYearsCal(String field)
   {
      int years = 0;
      try
      {
         years = Integer.parseInt(getNextCurrYearsString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      int monthnum = getInitMonthOfYear(field);
      if(monthnum > day.get(Calendar.MONTH)) // go back an extra year if start month > curr month
      	years += 1;
      day.add(Calendar.YEAR, 1 - years);
      setNextLastYearsInit(day, monthnum);
      return (day);
   }

   public static GregorianCalendar getLastNumYearsInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int monthnum = getInitMonthOfYear(field);
      setNextLastYearsInit(cal, monthnum);
      cal.add(Calendar.DATE,  -1);
      return (cal);
   }   
   
   public static Calendar getYesterdayCal()
   {
      GregorianCalendar day = new GregorianCalendar();
      return (getYesterdayCal(day));
   }
   
   public static Calendar getYesterdayCal(Calendar cal)
   {
      cal.add(Calendar.DATE, -1);
      return (cal);
   }
   
   public static Calendar getTomorrowCal(Calendar cal)
   {
      cal.add(Calendar.DATE, 1);
      return (cal);
   }
   
   public static Calendar getTomorrowCal()
   {
      GregorianCalendar cal = new GregorianCalendar();
      return (getTomorrowCal(cal));
   }
   
   public static GregorianCalendar getEndOfMonthCal()
   {
      GregorianCalendar month = new GregorianCalendar();
      month.set(Calendar.DATE, month.getActualMaximum(Calendar.DAY_OF_MONTH));
      return (month);
   }
   
   public static GregorianCalendar getStartOfMonthCal()
   {
      GregorianCalendar month = new GregorianCalendar();
      month.set(Calendar.DATE, 1);
      return (month);
   }
   
   public static GregorianCalendar getEndOfYearCal()
   {
      GregorianCalendar month = new GregorianCalendar();
      month.set(Calendar.MONTH, 11);
      month.set(Calendar.DATE, month.getActualMaximum(Calendar.DAY_OF_MONTH));
      return (month);
   }
   
   public static GregorianCalendar getStartOfYearCal()
   {
      GregorianCalendar month = new GregorianCalendar();
      month.set(Calendar.MONTH, 0);
      month.set(Calendar.DATE, 1);
      return (month);
   }
   
   public static GregorianCalendar getEndOfLastYearCal()
   {
      GregorianCalendar month = new GregorianCalendar();
      month.add(Calendar.YEAR, -1);
      month.set(Calendar.MONTH, 11);
      month.set(Calendar.DATE, month.getActualMaximum(Calendar.DAY_OF_MONTH));
      return (month);
   }
   
   public static GregorianCalendar getStartOfLastYearCal()
   {
      GregorianCalendar month = new GregorianCalendar();
      month.add(Calendar.YEAR, -1);
      month.set(Calendar.MONTH, 0);
      month.set(Calendar.DATE, 1);
      return (month);
   }   
   
   public static Calendar getStartOfFinYearCal() {
	   return getStartOfFinYearCal(null);
	}
   
   public static Calendar getStartOfFinYearCal(Calendar c)
   {
   	  GregorianCalendar month = new GregorianCalendar();
   	  if(c != null) { 
   		  month.setTime(c.getTime());
   	  }
      if(month.get(Calendar.MONTH)<6) { 
    	  month.add(Calendar.YEAR, -1);
      }
      month.set(Calendar.MONTH, 6);
      month.set(Calendar.DATE, 1);
      return (month);
   }
   public static Calendar getEndOfFinYearCal()
   {
	   return getEndOfFinYearCal(null);
   }
   public static Calendar getEndOfFinYearCal(Calendar c)
   {
	   GregorianCalendar month = new GregorianCalendar();
   	  if(c != null) { 
   		  month.setTime(c.getTime());
   	  }
      if(month.get(Calendar.MONTH)>=6) { 
    	  month.add(Calendar.YEAR, 1);
      }
      month.set(Calendar.MONTH, 6);
      month.set(Calendar.DAY_OF_MONTH,1);
      month.add(Calendar.DAY_OF_MONTH,-1);
      return (month);
   }
   
   public static Calendar getStartOfLastFinYearCal()
   {
   	 	Calendar c = getStartOfFinYearCal(); 
   	 	c.add(Calendar.YEAR, -1);
   	 	return c;
   }   
   public static Calendar getEndOfLastFinYearCal()
   {
	   Calendar c = getEndOfFinYearCal(); 
  	 	c.add(Calendar.YEAR, -1);
  	 	return c;
   }
   
   public static String getNextLastHoursString(String s)
   {
      return (s.substring(LAST.length(), s.indexOf(HOURS)));
   }   
   
   public static String getNextLastDaysString(String s)
   {
      return (s.substring(LAST.length(), s.indexOf(DAYS)));
   }
   
   public static String getNextLastWeeksString(String s)
   {
      return (s.substring(LAST.length(), s.indexOf(WEEKS)));
   }
   
   public static String getNextLastMonthsString(String s)
   {
      return (s.substring(LAST.length(), s.indexOf(MONTHS)));
   }
   
   public static String getNextLastYearsString(String s)
   {
	   if(s.indexOf(FINYEARS)>-1) { 
		   return (s.substring(LAST.length(), s.indexOf(FINYEARS)));
	   }
      return (s.substring(LAST.length(), s.indexOf(YEARS)));
   }  
   
   public static String getNextCurrYearsString(String s)
   {
      return (s.substring(CURRENT.length(), s.indexOf(YEARS)));
   }
   
   public static String getNextLastMonthsInitString(String s)
   {
      return (s.substring(s.indexOf(MONTHS) + MONTHS.length(), s.length()));
   }
   
   public static String getNextLastDaysInitString(String s)
   {
      return (s.substring(s.indexOf(DAYS) + DAYS.length(), s.length()));
   }
   
   public static int getInitDay(String field)
   {
      int day = 0;
      try
      {
         String numstring = getNextLastDaysInitString(field);
         if(numstring!=null && numstring.length()>0)
         {
            day = Integer.parseInt(numstring);
         }
      }
      catch(Exception e)
      {}
      return(day);
   }

   public static int getInitMonthOfYear(String field)
   {
      for(int i=0; i<_months.length; i++)
      {
         if(field.indexOf(_months[i])>0)
         {
            return(i);
         }
      }
      return(-1);
   }   
   
   public static int getInitDayOfMonth(String field)
   {
      int day = -1;
      try
      {
         String numstring = getNextLastMonthsInitString(field);
         if(numstring!=null && numstring.length()>0)
         {
            day = Integer.parseInt(numstring);
         }
      }
      catch(Exception e)
      {}
      return(day);
   }
   
   public static int getInitDayOfWeek(String field)
   {
      for(int i=0; i<_weekdays.length; i++)
      {
         if(field.indexOf(_weekdays[i])>0)
         {
            return(i);
         }
      }
      return(-1);
   }
   
   public static String getHourString(String s)
   {
      return (s.substring(HOUR.length(), s.length()));
   }   
   
   /* Quarter functions adapted from the Month versions */ 
   
   public static int getInitDayOfQuarter(String field)
   {
      int day = -1;
      try
      {
         String numstring = getNextLastQuartersInitString(field);
         if(numstring!=null && numstring.length()>0)
         {
            day = Integer.parseInt(numstring);
         }
      }
      catch(Exception e)
      {}
      return(day);
   }
   
   public static GregorianCalendar getLastNumQuartersCal(String field)
   {
      int quarters = 0;
      try
      {
    	 /* how many quarters do we go back? */
         quarters = Integer.parseInt(getNextLastQuartersString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      day.add(Calendar.MONTH, 0 - (quarters*3));
      int daynum = getInitDayOfQuarter(field);
      setLastQuartersInit(day, daynum);
      return (day);
   }
   
   public static GregorianCalendar getLastNumQuartersInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int daynum = getInitDayOfQuarter(field);
      setLastQuartersInit(cal, daynum);
      cal.add(Calendar.DATE,  -1);
      return (cal);
   }
   
   private static void setLastQuartersInit(Calendar cal, int daynum)
   {
      if(daynum!=-1)
      {
         int currentday = cal.get(Calendar.DAY_OF_MONTH);
         if(daynum != currentday)
         {
            int daysinquarter = cal.getMaximum(Calendar.DAY_OF_MONTH);
            if(daynum > daysinquarter)
            {
               cal.set(Calendar.DAY_OF_MONTH, daysinquarter);
            }
            else
            {
               cal.set(Calendar.DAY_OF_MONTH, daynum);
            }
         }
         if(daynum >= currentday)
         {
            cal.add(Calendar.MONTH, -3);
         }
      }
   }   

   public static GregorianCalendar getNextNumQuartersCal(String field)
   {
      int quarters = 0;
      try
      {
         quarters = Integer.parseInt(getNextLastQuartersString(field));
      }
      catch(Exception e)
      {
      }
      GregorianCalendar day = new GregorianCalendar();
      int daynum = getInitDayOfQuarter(field);
      day.add(Calendar.MONTH, quarters*3);
      setNextQuartersInit(day, daynum);
      day.add(Calendar.DAY_OF_MONTH, -1);
      return (day);
   }
   
   public static GregorianCalendar getNextNumQuartersInitCal(String field)
   {
      GregorianCalendar cal = new GregorianCalendar();
      int daynum = getInitDayOfQuarter(field);
      setNextQuartersInit(cal, daynum);
      return (cal);
   }
   
   private static void setNextQuartersInit(Calendar cal, int daynum)
   {
      if(daynum!=-1)
      {
         int currentday = cal.get(Calendar.DAY_OF_MONTH);
         cal.add(Calendar.MONTH, 2);
         if(daynum != currentday)
         {
            int daysinquarter = cal.getMaximum(Calendar.DAY_OF_MONTH);
            if(daynum > daysinquarter)
            {
               cal.set(Calendar.DAY_OF_MONTH, daysinquarter);
            }
            else
            {
               cal.set(Calendar.DAY_OF_MONTH, daynum);
            }
         }
         if(daynum <= currentday)
         {
            cal.add(Calendar.MONTH, 1);
         }
      }
   }

   public static GregorianCalendar getEndOfQuarterCal()
   {
      GregorianCalendar quarter = new GregorianCalendar();
      quarter.set(Calendar.DATE, quarter.getActualMaximum(Calendar.DAY_OF_MONTH));
      return (quarter);
   }
   
   public static GregorianCalendar getStartOfQuarterCal()
   {
      GregorianCalendar quarter = new GregorianCalendar();
      quarter.set(Calendar.DATE, 1);
      quarter.add(Calendar.MONTH, -2);
      return (quarter);
   }
   
   public static GregorianCalendar getEndOfCurrentQuarterCal()
   {
      GregorianCalendar quarter = new GregorianCalendar();
      
      int month = quarter.get(Calendar.MONTH) + 1;
		
      if (month > 9) {
			quarter.set(Calendar.MONTH, 11);
      } else if (month > 6) {
			quarter.set(Calendar.MONTH, 8);
      } else if (month > 3) {
			quarter.set(Calendar.MONTH, 5);
      } else {
			quarter.set(Calendar.MONTH, 2);
      }
		
      quarter.set(Calendar.DATE, quarter.getActualMaximum(Calendar.DAY_OF_MONTH));
      return (quarter);
   }
   
   public static GregorianCalendar getStartOfCurrentQuarterCal()
   {
		GregorianCalendar quarter = new GregorianCalendar();
		int month = quarter.get(Calendar.MONTH) + 1;
		
		if (month > 9) {
			quarter.set(Calendar.MONTH, 9);
		} else if (month > 6) {
			quarter.set(Calendar.MONTH, 6);
		} else if (month > 3) {
			quarter.set(Calendar.MONTH, 3);
		} else {
			quarter.set(Calendar.MONTH, 0);
		}
		
		quarter.set(Calendar.DATE, 1);
		
		return (quarter);
	}

   public static String getNextLastQuartersString(String s)
   {
      return (s.substring(LAST.length(), s.indexOf(QUARTERS)));
   }
   public static String getNextLastQuartersInitString(String s)
   {
      return (s.substring(s.indexOf(QUARTERS) + QUARTERS.length(), s.length()));
   }
   
   
}