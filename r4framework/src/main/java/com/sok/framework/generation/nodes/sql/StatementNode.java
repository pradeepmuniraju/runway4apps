package com.sok.framework.generation.nodes.sql;

import java.sql.PreparedStatement;

import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.SqlDatabase;
//import javax.servlet.http.HttpServletRequest;

public class StatementNode extends AbstractGeneratorNode implements SetterSetNode
{
	StatementHeadNode statementhead;
	WhereSetNode wheresetnode;
	OrderByVelocitySetNode orderbysetnode;
	GroupBySetNode groupbysetnode;
    HavingSetNode havingsetnode;
    LimitNode limitnode;
    String vscript;
    
	public StatementNode(String name)
	{
		super(name);
	}
	
    public void setScript(String vscript)
    {
        this.vscript = vscript;
    }    
    
    public String getScript()
    {
        return(vscript);
    }      
    
	public void setStatementHeadNode(StatementHeadNode statementhead)
	{
		this.statementhead = statementhead;
	} 
    
	public StatementHeadNode getStatementHeadNode()
	{
		return(statementhead);
	}	
	
	public void setWhereSetNode(WhereSetNode wheresetnode)
	{
		this.wheresetnode = wheresetnode;
	}	

    public WhereSetNode getWhereSetNode()
    {
        return(wheresetnode);
    }       
    
    public void setLimitNode(LimitNode limitnode)
    {
        this.limitnode = limitnode;
    }    
    
	public LimitNode getLimitNode()
	{
		return(limitnode);
	}		    
    
	public SelectNode setSelect(SelectParameterNode sparamnode)
	{
		if(statementhead == null)
		{
			statementhead = new SelectSetNode();
		}		
		SelectNode node = new SelectNode(sparamnode.getTableName(), sparamnode.getFieldName(), sparamnode.getFieldAlias());
		statementhead.addNode(node);
		return(node);
	}	
	
	public OrderByNode setOrderBy(OrderByParameterNode pnode)
	{
		if(orderbysetnode == null)
		{
			orderbysetnode = new OrderByVelocitySetNode();
		}
		OrderByNode node = null;
		if(pnode.isScript())
		{
			node = orderbysetnode.setOrderBy(pnode.getFieldName(), pnode.getDirection(), pnode.getPriority());
		}
		else
		{
			node = orderbysetnode.setOrderBy(pnode.getTableName(), pnode.getFieldName(), pnode.getDirection(), pnode.getPriority());
		}
		return(node);
	}	
	
    public OrderBySetNode getOrderBySetNode()
    {
        return(orderbysetnode);
    }       
    
	public GroupByNode setGroupBy(GroupByParameterNode pnode)
	{
		if(groupbysetnode == null)
		{
			groupbysetnode = new GroupBySetNode();
		}		
		GroupByNode node = groupbysetnode.setGroupBy(pnode.getTableName(), pnode.getFieldName(), pnode.getPriority());
		return(node);
	}	

    public HavingSetNode getHavingSetNode()
    {
        return(havingsetnode);
    }      
    
    public HavingNode setHaving(HavingParameterNode pnode)
    {
        if(havingsetnode == null)
        {
            havingsetnode = new HavingSetNode();
        }       
        HavingNode node = havingsetnode.setHaving(pnode.getCondition(), pnode.getPriority());
        return(node);
    }       
    
    public GroupBySetNode getGroupBySetNode()
    {
        return(groupbysetnode);
    }       
    
	public boolean hasJoinWith(String relationname)
	{
		return(statementhead.hasJoinWith(relationname));
	}
    
	public void generate(RootNode root, StringBuffer out)
	{        
        SqlDatabase database = root.getDatabase();
        database.generate(root, out);
	}
    
    public int getNodeType()
    {
        return(NodeType.type_Statement);
    }      
    
    public int setStatementValues(PreparedStatement updatestm, Row row, int index)
    {
        index = statementhead.setStatementValues(updatestm, row, index);
        return(index);
    }
}