package com.sok.framework.generation;

import java.util.*;

import com.sok.framework.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.nodes.sql.*;
import com.sok.framework.generation.nodes.sql.join.*;
import com.sok.framework.generation.util.*;

public class GeneratorFactory implements GenerationKeys, SQLKeys
{
   
   public static void setConfiguration(RequestNode node)
   {
      String action = node.getString(_action);
      if(action.equals(SEARCH) || action.equals(SELECTFIRST) || action.equals(SELECT))
      {
         setSearchConfiguration(node);
      }
      else if(action.indexOf(UPDATE)==0)
      {
         setUpdateConfiguration(node);
      }
      else if(action.indexOf(DELETE)==0)
      {
         setDeleteConfiguration(node);
      }
      else if(action.indexOf(INSERT)==0)
      {
         setInsertConfiguration(node);
      }
   }
   
   public static void setSearchConfiguration(RequestNode node)
   {
      setSelectStatement(node);
   }
   
   public static void setSelectConfiguration(RequestNode node)
   {
      setSelectStatement(node);
   }
   
   public static void setDeleteConfiguration(RequestNode node)
   {
      TableSpec tspec = node.getTableSpec();
      if(tspec == null)
      {
         throw new TableSpecNotFoundException(_tablespec);
      }
      StatementNode statement = new StatementNode(tspec.getTableName());
      
      DeleteSetNode deleteset = new DeleteSetNode(tspec.getTableName());
      statement.setStatementHeadNode(deleteset);
      
      WhereTokenSetNode whereset = new WhereTokenSetNode();
      statement.setWhereSetNode(whereset);
      
      boolean issingleaction = node.getString(_action).equals(DELETE);
      Node head = node.getHeadNode();
      WhereParameterNode wpnode;
      String fieldname;
      Object fieldvalue;
      
      while(head != null)
      {
         if(head.getNodeType() == Parameter.type_WhereParameter)
         {
            wpnode = (WhereParameterNode)head;
            fieldname = wpnode.getFieldName();
            fieldvalue = wpnode.getValue();
            WhereTokenNode wherenode = new WhereTokenNode(
                  fieldname,
                  fieldvalue,
                  tspec.getTableName(), -1);
            setWherePart(whereset, wherenode, sql_and, node, statement);
         }
         head = head.getNextNode();
      }
      
      if(issingleaction)
      {
         setWhereOnPrimaryKey(node, whereset, tspec, statement);
      }
      
      if(whereset.isEmpty())
      {
         StringBuffer stmt = new StringBuffer("Delete with no constraints :");
         statement.generate(node, stmt);
         throw new IllegalConfigurationException(stmt.toString());
      }
      node.setStatementNode(statement);
   }
   
   public static void setUpdateConfiguration(RequestNode node)
   {
      //String tablespecname = node.getString(_tablespec);
      TableSpec tspec = node.getTableSpec();
      if(tspec == null)
      {
         throw new TableSpecNotFoundException(_tablespec);
      }
      StatementNode statement = new StatementNode(tspec.getTableName());
      UpdateSetNode updateset = new UpdateSetNode(tspec.getTableName());
      statement.setStatementHeadNode(updateset);
      WhereTokenSetNode whereset = new WhereTokenSetNode();
      statement.setWhereSetNode(whereset);
      
      boolean issingleaction = node.getString(_action).equals(UPDATE);
      Node head = node.getHeadNode();
      WhereParameterNode wpnode;
      String pkey = tspec.getPrimaryKeyName();
      String fieldname;
      Object fieldvalue;
      int findex;
      while(head != null)
      {
         if(head.getNodeType() == Parameter.type_WhereParameter)
         {
            wpnode = (WhereParameterNode)head;
            fieldname = wpnode.getFieldName();
            fieldvalue = wpnode.getValue();
            
            WhereTokenNode wherenode = new WhereTokenNode(
                  fieldname,
                  fieldvalue,
                  tspec.getTableName(), -1);
            setWherePart(whereset, wherenode, sql_and, node, statement);
         }
         else if(head.getNodeType() == Parameter.type_RequestParameter)
         {
            fieldname = head.getName();
            if(fieldname.indexOf('.')<0 && !pkey.equals(fieldname))
            {
               fieldvalue = ((LeafNode)head).getValue();
               findex = tspec.getFieldsIndex(fieldname);
               if(findex>=0)
               {
                  updateset.setUpdate(fieldname, fieldvalue,
                        tspec.getFieldType(findex),
                        tspec.getFormat(findex));
               }
            }
         }
         head = head.getNextNode();
      }
      
      if(issingleaction)
      {
         setWhereOnPrimaryKey(node, whereset, tspec, statement);
      }
      
      if(whereset.isEmpty())
      {
         StringBuffer stmt = new StringBuffer("Update with no constraints :");
         statement.generate(node, stmt);
         throw new IllegalConfigurationException(stmt.toString());
      }
      node.setStatementNode(statement);
   }
   
   public static void setInsertConfiguration(RequestNode node)
   {
      //String tablespecname = node.getString(_tablespec);
      TableSpec tspec = node.getTableSpec();
      if(tspec == null)
      {
         throw new TableSpecNotFoundException(_tablespec);
      }
      StatementNode statement = new StatementNode(tspec.getTableName());
      InsertSetNode insertset = new InsertSetNode(tspec.getTableName());
      statement.setStatementHeadNode(insertset);
      //We don't do conditional inserts, so this isn't required.
      //WhereTokenSetNode whereset = new WhereTokenSetNode();
      //statement.setWhereSetNode(whereset);
      
      Node head = node.getHeadNode();
      
      String fieldname;
      Object fieldvalue;
      int findex;
      while(head != null)
      {
         if(head.getNodeType() == Parameter.type_RequestParameter)
         {
            fieldname = head.getName();
            if(fieldname.indexOf('.')<0)
            {
               fieldvalue = ((LeafNode)head).getValue();
               findex = tspec.getFieldsIndex(fieldname);
               if(findex>=0)
               {
                  insertset.setInsert(fieldname, fieldvalue,
                        tspec.getFieldType(findex),
                        tspec.getFormat(findex));
               }
            }
         }
         head = head.getNextNode();
      }
      node.setStatementNode(statement);
   }
   
   protected static void setSelectStatement(RequestNode node)
   {
      String viewspecname = node.getString(_viewspec);
      String tablespecname = node.getString(_tablespec);
      
      SelectSetNode select = new SelectSetNode();
      FromSetNode from = null;
      ViewSpec vspec = null;
      TableSpec tspec = null;
      String tablename = null;
      String fieldname = null;
      String fieldalias = null;
      
      StatementNode statement = new StatementNode(sql_select);
      statement.setStatementHeadNode(select);
      vspec = node.getViewSpec();
      if(vspec != null)
      {
         boolean idonly = node.getString(_idonly).equals(_true);
         statement.setScript(vspec.getCondition());
         tspec = node.getTableSpec();
         if(tspec == null)
         {
            throw new TableSpecNotFoundException(tablename);
         }
         tablename = tspec.getTableName();
         from = new FromSetNode(tablename, null);
         
         if(idonly)
         {
            select.setSelect(tablename, tspec.getPrimaryKeyName(), null);
         }
         else
         {
            for(int i = 0; i < vspec.getLocalLength(); i++)
            {
               fieldname = vspec.getLocalItemFieldName(i);
               fieldalias = vspec.getLocalItemAsName(i);
               select.setSelect(tablename, fieldname, fieldalias);
            }
         }
  
         for(int j = 0; j < vspec.getCountsLength(); j++)
         {
            fieldname = vspec.getCountItemName(j);
            fieldalias = vspec.getCountItemAsName(j);
            CountNode countnode = new CountNode(tablename, fieldname, fieldalias);
            select.addNode(countnode);
         }
         
         String relationname = null;
         for(int j = 0; j < vspec.getRelatedLength(); j++)
         {
            relationname = setFromPart(from, vspec.getRelationshipNameForRelatedItem(j), node, null);
            fieldname = vspec.getRelatedItemFieldName(j);
            fieldalias = vspec.getRelatedItemName(j);
            if(!idonly)
            {
               select.setSelect(relationname, fieldname, fieldalias);
            }
         }
         
         for(int k = 0; k < vspec.getRelatedCountsLength(); k++)
         {
            if(!vspec.getRelatedCountHidden(k) && !idonly)
            {
               fieldalias = vspec.getRelatedCountItemName(k);
               SubSelectSetNode subselect = new SubSelectSetNode(fieldalias);
               subselect.setScript(vspec.getRelatedCountCondition(k,0));
               subselect.addNode(new CountNode(null, sql_asterisk, null));
               select.addNode(subselect);
               setSubSelectSetPart(subselect, vspec.getRelationshipNameForRelatedCountItem(k), node);
            }
         }
         
         for(int k = 0; k < vspec.getRelatedSumsLength(); k++)
         {
            if(!vspec.getRelatedSumHidden(k) && !idonly)
            {
               fieldalias = vspec.getRelatedSumItemName(k);
               SubSelectSetNode subselect = new SubSelectSetNode(fieldalias);
               subselect.setScript(vspec.getRelatedSumCondition(k,0));
               subselect.addNode(new SumNode(null, vspec.getFieldNameForRelatedSumItem(k), null));
               select.addNode(subselect);
               setSubSelectSetPart(subselect, vspec.getRelationshipNameForRelatedSumItem(k), node);
            }
         }
    
      }
      else if(vspec == null && viewspecname.length()!=0)
      {
         throw new ViewSpecNotFoundException(viewspecname);
      }
      else
      {
         tspec = node.getTableSpec();
         if(tspec == null)
         {
            throw new TableSpecNotFoundException(tablespecname);
         }
         tablename = tspec.getTableName();
         from = new FromSetNode(tablename, null);
      }
      select.setFromSetNode(from);
      WhereTokenSetNode where = new WhereTokenSetNode();
      statement.setWhereSetNode(where);
      
      boolean issingleaction = node.getString(_action).equals(SELECT);
      
      LeafNode head = (LeafNode)node.getHeadNode();
      SelectParameterNode sparam;
      JoinParameterNode jparam;
      OrderByParameterNode obparam;
      GroupByParameterNode gbparam;
      HavingParameterNode hparam;
      
      int paramtype = 0;
      
      while(head != null)
      {
         if(notEmpty(head.getValue()))
         {
            paramtype = head.getNodeType();
            if(paramtype == Parameter.type_SelectParameter)
            {
               sparam = (SelectParameterNode)head;
               
               if(sparam.getTableName() == null && tspec.hasField(sparam.getFieldName()))
               {
                  sparam.setTableName(tablename);
               }
               else if(sparam.getTableName() != null && !sparam.getTableName().equals(tablename))
               {
                  sparam.setTableName(setFromPart(from, sparam.getTableName(), node, null));
               }
               statement.setSelect(sparam);
            }
            else if(paramtype == Parameter.type_JoinParameter)
            {
               jparam = (JoinParameterNode)head;
               if(jparam.getRelations() != null)
               {
                  setFromPart(from, jparam.getRelations(), node, null);
               }
            }
            else if(!issingleaction && paramtype == Parameter.type_OrderByParameter)
            {
               obparam = (OrderByParameterNode)head;
               if(!obparam.isScript() && obparam.getTableName() == null && tspec.hasField(obparam.getFieldName()))
               {
                  obparam.setTableName(tablename);
               }
               statement.setOrderBy(obparam);
            }
            else if(!issingleaction && paramtype == Parameter.type_GroupByParameter)
            {
               gbparam = (GroupByParameterNode)head;
               if(gbparam.getTableName() == null && tspec.hasField(gbparam.getFieldName()))
               {
                  gbparam.setTableName(tablename);
               }
               statement.setGroupBy(gbparam);
            }
            else if(!issingleaction && paramtype == Parameter.type_LimitParameter)
            {
               LimitNode limitnode = new LimitNode(
                     head.getName(),
                     ((LimitParameterNode)head).getIntValue());
               statement.setLimitNode(limitnode);
            }
            else if(!issingleaction && paramtype == Parameter.type_HavingParameter)
            {
               hparam = (HavingParameterNode)head;
               statement.setHaving(hparam);
            }
            else if(!issingleaction && paramtype == Parameter.type_RequestParameter)
            {
               WhereTokenNode wherenode = new WhereTokenNode(
                     head.getName(),
                     head.getValue(),
                     tablename, -1);
               setWherePart(where, wherenode, sql_and, node, statement);
            }
         }
         head = (LeafNode)head.getNextNode();
      }
      
      if(issingleaction)
      {
         setWhereOnPrimaryKey(node, where, tspec, statement);
      }
      
      node.setStatementNode(statement);
   }
   
   public static boolean notEmpty(Object obj)
   {
      if(obj!=null )
      {
         if(obj instanceof String[])
         {
            String[] strings = (String[])obj;
            for(int i=0; i<strings.length; i++)
            {
               if(strings[i]!=null && strings[i].length()!=0)
               {
                  return(true);
               }
            }
         }
         else
         {
            return(obj.toString().length()!=0);
         }
      }
      return(false);
   }
   
   /**
   protected static void setWhereOnPrimaryKey(RootNode node, WhereSetNode whereset, TableSpec tspec)
   {
	   setWhereOnPrimaryKey(node, whereset, tspec, node.getStatementNode());
   }
   */
 
   protected static void setWhereOnPrimaryKey(RootNode node, WhereSetNode whereset, TableSpec tspec, StatementNode statement)
   {
      String primarykeyname = tspec.getPrimaryKeyName();
      String tablename = tspec.getTableName();
      String primarykeyvalue = node.getString(primarykeyname);
      if(primarykeyvalue.length()==0)
      {
         throw new IDNotFoundException(primarykeyname, node.getString(_action));
      }
      else
      {
         WhereNode where = new WhereNode(
               primarykeyname,
               primarykeyvalue,
               tablename, -1);
         setWherePart(whereset, where, sql_and, node, statement);
      }
   }
   
   protected static void setSubSelectSetPart(SubSelectSetNode subselect, String relations, RootNode node)
   {
      RelationSpec rspec = node.getRelationSpec();
      
      WhereSetNode whereset = new WhereSetNode();
      subselect.setWhereSetNode(whereset);
      
      StringTokenizer st = new StringTokenizer(relations,".");
      int rindex = -1;
      FromSetNode fromset = null;
      String relation = null;
      String lastrelation = null;
      String fromtablename = null;
      String totablename = null;
      String tofieldname = null;
      String fromfieldname = null;
      String vscript = null;
      int priority = 0;
      boolean isfirst = true;
      
      while(st.hasMoreTokens())
      {
         relation = st.nextToken();
         rindex = rspec.getIndex(relation);

         if(rindex != -1)
         {
            totablename = rspec.getToTableName(rindex);
            if(lastrelation == null)
            {
               fromtablename = rspec.getFromTableName(rindex);
            }
            else
            {
               fromtablename = lastrelation;
            }
            tofieldname = rspec.getToKey(rindex);
            fromfieldname = rspec.getFromKey(rindex);
            vscript = rspec.getCondition(rindex);
            priority = rspec.getPriority(rindex);
            if(isfirst)
            {
               fromset = new FromSetNode(totablename, relation);
               subselect.setFromSetNode(fromset);
               isfirst = false;
            }
            else
            {
               fromset.setFrom(relation,
                     fromtablename,
                     totablename,
                     fromfieldname,
                     tofieldname,
                     sql_comma,
                     priority);
            }
            lastrelation = relation;
            setAdditionalJoinParameters(rspec, rindex, relation, fromtablename, subselect);
            whereset.setWhere(fromtablename, fromfieldname,
                  relation, tofieldname, vscript);
         }
      }
   }
   
   protected static String setFromPart(FromSetNode from, String relationset, RootNode node, String overridejointype)
   {
      RelationSpec rspec = node.getRelationSpec();
      StringTokenizer st = new StringTokenizer(relationset,".");
      String relation = null;
      String lastrelation = null;
      String fromtablename = null;
      int rindex = -1;
      String jointype;
      FromNode fromnode = null;
      while(st.hasMoreTokens())
      {
         relation = st.nextToken();
         rindex = rspec.getIndex(relation);
         
         if(rindex != -1)
         {
            if(lastrelation == null)
            {
               fromtablename = rspec.getFromTableName(rindex);
            }
            else
            {
               fromtablename = lastrelation;
            }
            
            if(overridejointype!=null)
            {
               jointype = overridejointype;
            }
            else
            {
               jointype = getJoinType(rspec.getType(rindex));
            }
            
            fromnode = from.setFrom(relation,
                  fromtablename,
                  rspec.getToTableName(rindex),
                  rspec.getFromKey(rindex),
                  rspec.getToKey(rindex),
                  jointype,
                  rspec.getPriority(rindex),
                  rspec.getCondition(rindex));
            lastrelation = relation;
            
            setAdditionalJoinParameters(rspec, rindex, relation, fromtablename, fromnode);
         }
         else
         {
            throw new IllegalConfigurationException("Relation "+relation +" not found in relation specification");
         }
      }
      return(relation);
   }
   
   protected static void setAdditionalJoinParameters(RelationSpec rspec, int rindex, String totablename, String fromtablename, Joinable parent)
   {
      int onlength = rspec.getOnLength(rindex);
      if(onlength>0)
      {
         JoinOnSetNode onset = new JoinOnSetNode(totablename);
         for(int j=0; j<onlength; j++)
         {
            onset.setJoingOn(totablename, fromtablename, rspec.getOnToKey(rindex, j), rspec.getOnFromKey(rindex, j), rspec.getOnKeyValue(rindex, j), rspec.getOnKeyValueName(rindex, j));
         }
         parent.setJoinOnSetNode(onset);
      }
   }
   
   protected static boolean isGrouped(String s)
   {
	   return(s.indexOf('|') > -1 || s.indexOf('^') >- 1);
   }
   
   protected static boolean isGrouped(String[] values)
   {
       for(int i=0; i<values.length; i++)
       {
    	   if(isGrouped(values[i]))
    	   {
    		   return(true);
    	   }
       }
       return(false);
   }   
   
   protected static WhereNode getNewWhereNodeWithValue(WhereNode n, Object value)
   {
	   WhereNode node = new WhereNode(n.getName(), value, n.getTableName(), n.getDataType());
	   return(node);
   }
   
   /**
   protected static void setWhere(WhereSetNode where, WhereNode node, RootNode root, String op)
   {
	   setWhere(where, node, root, root.getStatementNode(), op);
   }
   */
   protected static void setWhere(WhereSetNode where, WhereNode node, RootNode root, StatementNode statement, String op)
   {
	   Object obj = node.getValue();
	   if(obj instanceof String[])
	   {
		   String[] values = (String[])obj;
		   if(isGrouped(values))
		   {
			   for(int i=0; i<values.length; i++)
			   {
				   setWhereGroupPart(where, node, root, values[i], op, statement);
			   }

		   }
		   else
		   {
		       where.setWhere(node);
		   }
	   }
	   else if(obj instanceof String)
	   {
		   String value = (String)obj;
		   if(isGrouped(value))
		   {
			   setWhereGroupPart(where, node, root, value, op, statement);
		   }
		   else
		   {
			   where.setWhere(node);
		   }
	   }
	   else
	   {
		   where.setWhere(node);
	   }
   }
   
   /**
   protected static void setWherePart(WhereSetNode where, WhereNode node, String separator, RootNode root) {
	   
	   setWherePart(where, node, separator, root, root.getStatementNode());
   }
   */
   
   //separator is the operator in search groups set by ^ or |
   protected static void setWherePart(WhereSetNode where, WhereNode node, String separator, RootNode root, StatementNode statement)
   {
      RelatedFieldName name = new RelatedFieldName(node.getName());
      if(isGrouped(name.fullname))
      {
         //System.out.println("isGrouped "+name.fullname);
         setWhereGroupPart(where, node, root, statement, null);
      }
      else if(name.hasRelations())
      {
         //String relations = name.relations;
         if(statement.hasJoinWith(name.relations))
         {
            //String fieldname = name.fieldname;
            node.setName(name.fieldname);
            
            if(name.hasSubRelations())
            {
               node.setTableName(name.getLastRelation());
            }
            else
            {
               node.setTableName(name.relations);
            }
            node.setIsRealTableName(false);
            FieldMeta fm = getDataMeta(node, root);
            node.setDataType(fm.type);
            node.setInputFormat(fm.format);
            if(node.getDataType()!=-1)
            {
               if(checkForOmit(root, node, name))
               {
                  setWhereGroupPart(where, node, root, statement, sql_and);
               }
               else
               {
            	   setWhere(where, node, root, statement, null);
               }
            }
            
         }
         else
         {
            setWhereExistsPart(where, node, separator, root, statement, name);
         }
      }
      else
      {
         FieldMeta fm = getDataMeta(node, root);
         node.setDataType(fm.type);
         node.setInputFormat(fm.format);
         
         int rcindex = getRelatedCountIndex(node, root);
         int rsindex = getRelatedSumIndex(node, root);
         if(node.getDataType()!=-1)
         {
        	 setWhere(where, node, root, statement, null);
         }
         else if(rcindex>=0)
         {
            setCountSubSelectSeach(where, node, root, rcindex);
         }
         else if(rsindex>=0)
         {
            setSumSubSelectSeach(where, node, root, rsindex);
         }         
      }
   }

   protected static void setCountSubSelectSeach(WhereSetNode where, WhereNode node, RootNode root, int rcindex)
   {
               ViewSpec vspec = root.getViewSpec();
               String fieldalias = vspec.getRelatedCountItemName(rcindex);
               SubSelectSetNode subselect = new SubSelectSetNode(null);
               subselect.setScript(vspec.getRelatedCountCondition(rcindex,0));
               subselect.addNode(new CountNode(null, sql_asterisk, fieldalias));
               WhereSubSelectNode wheresubselect = new WhereSubSelectNode(node);
               wheresubselect.setSubSelectSetNode(subselect);
               wheresubselect.setDataType(jdbctype_int);
               where.addPrioritisedNode(wheresubselect);
               setSubSelectSetPart(subselect, vspec.getRelationshipNameForRelatedCountItem(rcindex), root);    
   }
   
   protected static void setSumSubSelectSeach(WhereSetNode where, WhereNode node, RootNode root, int rsindex)
   {
               ViewSpec vspec = root.getViewSpec();
               String fieldalias = vspec.getRelatedSumItemName(rsindex);
               SubSelectSetNode subselect = new SubSelectSetNode(null);
               subselect.setScript(vspec.getRelatedSumCondition(rsindex,0));
               subselect.addNode(new SumNode(null, vspec.getFieldNameForRelatedSumItem(rsindex), fieldalias));
               //subselect.addNode(new SumNode(null, sql_asterisk, fieldalias));
               WhereSubSelectNode wheresubselect = new WhereSubSelectNode(node);
               wheresubselect.setSubSelectSetNode(subselect);
               wheresubselect.setDataType(jdbctype_float);
               where.addPrioritisedNode(wheresubselect);
               setSubSelectSetPart(subselect, vspec.getRelationshipNameForRelatedSumItem(rsindex), root);     
   }
   
   protected static int getRelatedCountIndex(WhereNode node, RootNode root)
   {
      ViewSpec vspec = root.getViewSpec();
      if(vspec!=null)
      {
         for(int i=0; i<vspec.getRelatedCountsLength(); i++)
         {
            if(vspec.getRelatedCountItemName(i).equals(node.getName()))
            {
               return(i);
            }
         }
      }
      return(-1);
   }
   
   protected static int getRelatedSumIndex(WhereNode node, RootNode root)
   {
      ViewSpec vspec = root.getViewSpec();
      if(vspec!=null)
      {
         for(int i=0; i<vspec.getRelatedSumsLength(); i++)
         {
            if(vspec.getRelatedSumItemName(i).equals(node.getName()))
            {
               return(i);
            }
         }
      }
      return(-1);
   }   
   
   public static boolean hasOmit(RootNode root, String relations)
   {
	      Node onode = root.getNode("-"+relations, Parameter.type_OmitParameter);
	      if (onode == null && relations.indexOf("#") > 0) {
	    	  onode = root.getNode("-"+relations.replaceAll("#", "_"), Parameter.type_OmitParameter);
	      }
    	  //System.out.println("looking for omit " + "-"+relations + " found: " + String.valueOf(onode!=null));
	      return(onode!=null);
   }
   
   public static boolean hasOmit(RootNode root, String relations, String relationswithgroups)
   {
         boolean hasomit = hasOmit(root, relations);
         if(!hasomit && relationswithgroups!=null)
         {
            hasomit = hasOmit(root, relationswithgroups);
         }
         //System.out.println("looking for group omit: " + "-"+relations + "groups: " +relationswithgroups + " found: " + String.valueOf(hasomit));
         return(hasomit);
   }
   
   protected static boolean checkForOmit(RootNode root, WhereNode node, RelatedFieldName relations)
   {
      boolean isomit = false;
      if(hasOmit(root, relations.relations, node.getOmitAltSearch()))
      {
         isomit = true;
         node.setOmit(isomit);
         node.setFlipOp(isomit);
      }
      return(isomit);
   }
   
   protected static FieldMeta getDataMeta(WhereNode node, RootNode root)
   {
      TableSpec tspec = null;
      
      if(node.isRealTableName())
      {
         tspec = root.getTableSpec();
      }
      else
      {
         RelationSpec rspec = root.getRelationSpec();
         int rindex = rspec.getIndex(node.getTableName());
         tspec = SpecManager.getTableSpec(rspec.getToTableSpecLocation(rindex));
      }
      int findex = tspec.getFieldsIndex(node.getName());
      FieldMeta fm = new FieldMeta();
      fm.type = tspec.getFieldType(findex);
      fm.format = tspec.getFormat(findex);
      return(fm);
   }
   
   /**
   protected static void setWhereExistsPart(WhereSetNode where, WhereNode node, String separator, RootNode root, RelatedFieldName relatedfieldname)
   {
	   setWhereExistsPart(where, node, separator, root, root.getStatementNode(), relatedfieldname); 
   }
   */
   protected static void setWhereExistsPart(WhereSetNode where, WhereNode node, String separator, RootNode root, StatementNode statement, RelatedFieldName relatedfieldname)
   {
      String name = node.getName();
      int dotindex = name.indexOf('.');
      String relationname = name.substring(0, dotindex);

      //occurs when reparsing a toString().. which you should be able to do, else how do saved searches work?
      if("javax".equals(relationname)) {
    	  return;
      }
      String stripedname = name.substring(dotindex+1, name.length());
      
      SimpleWhereExistsSetNode exists = null;
      Node rnode = where.getNode(relationname, NodeType.type_SimpleWhereExistsSet);
      
      if(rnode!=null)
      {
         exists = (SimpleWhereExistsSetNode)rnode;
      }
      else
      {
         RelationSpec rspec = root.getRelationSpec();
         int rindex = rspec.getIndex(relationname);
         if(rindex<0)
         {
            throw new IllegalConfigurationException("Relation not found: "+relationname);
         }
         String tablename = node.getTableName();
         String fromtablename;
         if(tablename == null)
         {
            fromtablename = rspec.getFromTableName(rindex);
         }
         else
         {
            fromtablename = tablename;
         }
         
         exists = new SimpleWhereExistsSetNode(relationname,
               fromtablename,
               rspec.getToTableName(rindex),
               rspec.getFromKey(rindex),
               rspec.getToKey(rindex),
               separator,
               rspec.getPriority(rindex));
         exists.setScript(rspec.getCondition(rindex));
         
         //if(hasOmit(root, relationname, node.getOmitAltSearch()))
         if(hasOmit(root, relationname))
         {
            exists.setOmit(true);
         }

         setAdditionalJoinParameters(rspec, rindex, relationname, fromtablename, exists);
         where.addPrioritisedNode(exists);
      }
      node.setName(stripedname);
      node.setTableName(relationname);
      node.setIsRealTableName(false);
      setWherePart(exists, node, separator, root, statement);
   }
   
   /**
   protected static WhereGroupSetNode setWhereGroupPart(WhereSetNode where, WhereNode node, RootNode root, String value, String op)
   {
	   return setWhereGroupPart(where, node, root, value, op, root.getStatementNode());
   } */
   
   protected static WhereGroupSetNode setWhereGroupPart(WhereSetNode where, WhereNode node, RootNode root, String value, String op, StatementNode statement)
   {	   
      int orindex = value.lastIndexOf('|');
      int andindex = value.lastIndexOf('^');
      String groupname = null;
      String stripedvalue = null;
      String separator = null;
      if(orindex > andindex)
      {
         groupname = value.substring(orindex, value.length());
         stripedvalue = value.substring(0, orindex);
         separator = sql_or;
      }
      else if(andindex > orindex)
      {
         groupname = value.substring(andindex, value.length());
         stripedvalue = value.substring(0, andindex);
         separator =  sql_and;
      }
      else
      {
    	 stripedvalue = value;
         groupname = node.getName();
      }
      WhereGroupSetNode group = null;
      Node gnode = where.getNode(groupname, NodeType.type_WhereGroupSet);
      if(gnode!=null)
      {
         group = (WhereGroupSetNode)gnode;
         //System.out.println("1found group "+ groupname);
      }
      else
      {
         if(op == null)
         {
            group = new WhereGroupSetNode(groupname, separator);
            //System.out.println("1new group "+ groupname + " separator " + separator + " parent:" + where.getName());
         }
         else
         {
            group = new WhereGroupSetNode(groupname, op);
            //System.out.println("1new group "+ groupname + " op " + op + " parent:" + where.getName());
         }
         where.addPrioritisedNode(group);
      }
	  WhereNode wnode = getNewWhereNodeWithValue(node, stripedvalue);  

      setWhere(group, wnode, root, statement, op);
      
      return(group);
   }   

   /**
   protected static WhereGroupSetNode setWhereGroupPart(WhereSetNode where, WhereNode node, RootNode root, String op) {
   	    return setWhereGroupPart(where, node, root, root.getStatementNode(), op);
   }
      */
   protected static WhereGroupSetNode setWhereGroupPart(WhereSetNode where, WhereNode node,  RootNode root, StatementNode statement, String op)
   {
	   String name = node.getName();
      int orindex = name.lastIndexOf('|');
      int andindex = name.lastIndexOf('^');
      String groupname = null;
      String stripedname = null;
      String separator = null;
      if(orindex > andindex)
      {
         groupname = name.substring(orindex, name.length());
         stripedname = name.substring(0, orindex);
         separator = sql_or;
      }
      else if(andindex > orindex)
      {
         groupname = name.substring(andindex, name.length());
         stripedname = name.substring(0, andindex);
         separator =  sql_and;
      }
      else
      {
         groupname = node.getName();
      }
      WhereGroupSetNode group = null;
      Node gnode = where.getNode(groupname, NodeType.type_WhereGroupSet);
      if(gnode!=null)
      {
         group = (WhereGroupSetNode)gnode;
         //System.out.println("2found group "+ groupname);
      }
      else
      {
         if(op == null)
         {
            group = new WhereGroupSetNode(groupname, separator);
            //System.out.println("2new group "+ groupname + " separator " + separator + " parent:" + where.getName());
         }
         else
         {
            group = new WhereGroupSetNode(groupname, op);
            //System.out.println("2new group "+ groupname + " op " + op + " parent:" + where.getName());
         }
         where.addPrioritisedNode(group);
      }
      if(stripedname!=null)
      {
         node.setName(stripedname);
      }

      setWherePart(group, node, separator, root, statement);
      
      return(group);
   }
   
   protected static String getJoinType(String type)
   {
      if(type.equals(OUTER_LEFT_JOIN))
      {
         return(sql_outer_left_join);
      }
      else if(type.equals(INNER_JOIN))
      {
         return(sql_inner_join);
      }
      else if(type.equals(OUTER_RIGHT_JOIN))
      {
         return(sql_outer_right_join);
      }
      return(null);
   }  
   
   public static void main(String[] arg)
   {
      /*
      StringBuffer qry = new StringBuffer();
      
      qry.append("(select count(*) from ");
      qry.append("ContactStatus");
      qry.append(" where ");
      qry.append(" StatusID = StatusList.StatusID) AS Num ");      
      
      ActionBean.setDatabase(com.sok.framework.sql.SqlDatabase.mysql);
      GenRow row = null;
      row = new GenRow();
      row.setTableSpec("StatusList");
      row.setParameter("-select1","StatusID");
      row.setParameter("-select2","Status");
      row.setParameter("-select3","StatusValue");
      row.setParameter("-select4","MainPage");
      row.setParameter("-select5",qry.toString());
      row.setParameter("ListName","ContactStatus");
      row.setParameter("Inactive","NULL+Y");

      row.doAction("search");
      System.out.print(row.getStatement()); // Incorrect Statement - ORDER BY Contacts.NoteCount
      */
      
      ActionBean.setDatabase(com.sok.framework.sql.SqlDatabase.mysql);
   
   //   GenRow row = null;
    //  row = new GenRow();
//row.parseRequest("-searchkey=051L2Z5L1Q8J0I0I4O9R156J6V5H&-resultcount=12&-action=search&-viewSpec=ContactListView&CURRENTUSERID%5EM153=0U1N1X487H3F1P9R329C4P898A4M&FirstName%5EM95=test&FirstName%5EM95=%25test%25&ContactContactGroups.ContactGroupGroup.SectionCode%7CVC%5EM84=AA%25&LastName=%25contact%25");
      //row.setViewSpec("ContactView");
      //row.setParameter("Email|1^2", "%email%");
     // row.setParameter("Mobile|1^2", "%email"); 
     // row.setParameter("Title^2", "%email"); 
/*
      row.setParameter("ContactCompany.CompanyCompanyStatus.StatusID","%");
      row.setParameter("-join1","ContactCompany.CompanyCompanyStatus");  
      row.setParameter("ContactCompany.CompanyCompanyStatus.CompanyID","NOW");  
  */  
      //row.setParameter("ContactNotes.ModifiedDate|1",">NOW");
      //row.setParameter("ContactNotes.RepUserID","myid");
   //   row.doAction("search");
     // System.out.print(row.getStatement());
      //row.setStatement(stmt.toString());
       /*
      row.setTableSpec("Contacts");
      row.setParameter("-select1", "ContactID");
      row.setParameter("-select2", "RepUserID");
      row.setParameter("-select3", "GroupID");



      row.setParameter("-ContactNotes","omit");
      row.setParameter("ContactNotes.NoteID","messageid");
      row.setParameter("-join1","ContactNotes");
      row.setAction("selectfirst");

      GenRow results = new GenRow();
      results.setTableSpec("Contacts");
      results.setParameter("-select1", "ContactID");
      //results.setParameter("-select2","count(*) as Votes");  
      //results.setParameter("FirstName^3|1^2","a");
      //results.setParameter("LastName^3|1^2","b");
      results.setParameter("ContactCompany#CS1.Company^5P5B6B3O0D906W8V960M|5I5G6S3S0X9Y6Y87930E","_%");
      results.setParameter("-ContactCompany#CS1^5P5B6B3O0D906W8V960M|5I5G6S3S0X9Y6Y87930E","omit");
      //results.setParameter("Street|1^2","c");
      //results.setParameter("Street2|1^2","d");      
      //results.doAction("search");
      results.generateSQLStatement();

      System.out.print(results.getStatement());
      
*/
/*
      RowBean bean = null;
      bean = new RowBean();
      bean.setViewSpec("ContactListView");
      bean.setParameter("ModifiedDate|1",">NOW");
      bean.setParameter("ContactNotes.ModifiedDate|1",">NOW");
      bean.setParameter("ContactNotes.RepUserID","myid");
      bean.doAction("search");
*/
      //row.setStatement(stmt.toString());

      
      //         c   
      
      //System.out.print(KeyMaker.getTimeInHex());
 
/*
      GenRow row1 = new GenRow();
      GenRow row2 = new GenRow();
      GenRow row3 = new GenRow();
      row1.parseRequest("-sort0=Contacts.LastName&-sort1=Contacts.FirstName&-searchkey=0Z192Y572D3K0N532L39509F0U2D&-resultcount=22&LastName=%25contact%25&-quicksearchNav=test+contact&ContactContactGroups.ContactGroupGroup.SectionCode%7CVC=AA%25&FirstName=%25test%25&-action=search&CURRENTUSERID=0U1N1X487H3F1P9R329C4P898A4M&-viewSpec=ContactListView");
      row2.parseRequest("-sort1=Contacts.FirstName&-searchkey=0S1V2G5T2P3Z0V5G2Q4C1P370L5O&-resultcount=86&ContactContactGroups.ContactGroupGroup.SectionCode%7CVC=AA%25&-order1=ASC&Title=Mr&-action=search&CURRENTUSERID=0U1N1X487H3F1P9R329C4P898A4M&-viewSpec=ContactListView");
      row3.parseRequest("-sort1=Contacts.FirstName&-searchkey=0F1Q265G213Y055E26531W8O1Z6G&-resultcount=6&ContactContactGroups.ContactGroupGroup.SectionCode%7CVC=AA%25&-order1=ASC&Title=Mrs&-action=search&CURRENTUSERID=0U1N1X487H3F1P9R329C4P898A4M&-viewSpec=ContactListView");

      GenRow row4 = DatabaseFactory.refineRequests(row2, row1);
      row4 = DatabaseFactory.refineRequests(row4, row3);
      row4.setViewSpec("ContactListView");
      //row4.doAction("search");
     // System.out.print(row4.getStatement());
*/
      /*
      java.util.Locale defaultlocale =  Locale.getDefault();
      System.out.print(defaultlocale.getDisplayName());
      

      java.util.Locale.setDefault(java.util.Locale.UK); 
      defaultlocale =  Locale.getDefault();
      System.out.print(defaultlocale.getDisplayName());

*/
      String[] test = {"","",""};
      GenRow bean = new GenRow();
      //bean.setTableSpec("Users");
      //bean.setParameter("DefaultGroupID", "togroupid");
      //bean.setParameter("ON-DefaultGroupID", "fromgroupid");  
      //bean.setAction("updateall");
      //GeneratorFactory.setConfiguration(bean);
      //System.out.println(bean.getStatement());
     // bean.setParameter("test", test);
     // System.out.print(bean.getString("test"));
/*
      GenRow bean = null;
      bean = new GenRow();
      bean.setViewSpec("NoteListView");
      bean.setParameter("NoteOrdersWithin24Hours.Name","test");
      bean.doAction("search");      
      System.out.print(bean.getStatement());   

      GenRow bean = new GenRow();
      bean.setViewSpec("groups/ContactCountView");   
      bean.setParameter("ContactContactGroups.GroupID", "groupid");
      bean.doAction("search"); 
      System.out.print(bean.getStatement()); 
      //bean.doAction("search");      
      System.out.print(bean.toString());    
                */  
      /*
      GenRow row = new GenRow();
      row.setViewSpec("ContactListView");   
      row.setParameter("ContactDepdupLastName.LastName","!EMPTY");
      //row.setParameter("ContactDepdupLastName.GroupID","????");
      row.setParameter("GroupID","????");
      //AND ContactDepdupLastNameEmail.GroupID = '$GroupID'
      row.doAction("search"); 
      //row.generateSQLStatement();
      System.out.print(row.getSearchStatement()); 
      */
      /*
      ActionBean.setDatabase(com.sok.framework.sql.SqlDatabase.mysql);
      GenRow row = null;
      row = new GenRow();
      row.setViewSpec("ContactListView");
      row.parseRequest("-top=200&ContactGroup.SectionCode%7CVC=AA%25&ContactGroup.SectionCode=AA%25&-action=search&-viewSpec=ContactListView&CURRENTUSERID=0D1F0V3S6W01237V8X8F5V4W3K7C");
      row.setParameter("ContactAnswer#ContactAnswerQuestionID=someid.QuestionID","0N11086T0K7D3R6C2Y0Q5");
      //row.setParameter("ContactAnswerQuestionID","someqid");
      //row.setParameter("-join1", "ContactAnswer");
      row.doAction("search");
      
      System.out.print(row.getStatement());      
      */
      
      GenRow row = new GenRow();
//      row.setRequest(request);
      row.setViewSpec("ContactView");
     // row.setParameter("-join", "ContactNotes.NoteEmail");
      row.setParameter("ContactNotes.NoteEmail.TemplateUsed","TemplateUsed");
      row.setParameter("-ContactNotes","omit");
      row.doAction("search");      System.out.print(row.getStatement());  
   }
   
}