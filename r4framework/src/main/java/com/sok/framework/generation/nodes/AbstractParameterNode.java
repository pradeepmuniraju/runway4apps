package com.sok.framework.generation.nodes;

import com.sok.framework.*;
import com.sok.framework.generation.*;

public abstract class AbstractParameterNode extends LeafNode implements ParameterNode, PrioritisedNode
{
    static final String _hiddeninput1 = "<input type=\"hidden\" name=\"";
    static final String _hiddeninput2 = "\" value=\"";
    static final String _hiddeninput3 = "\">\r\n";
       
    protected int priority = 100;
    
	public AbstractParameterNode(String name, Object value)
	{
		super(name, value);
	}
	
   public int getPriority()
   {
       return(priority);
   }	
	
    public abstract int getNodeType();
    
    public boolean isEmpty()
    {
    	return(!GeneratorFactory.notEmpty(value));
    }
    
	public void appendQuery(StringBuffer out)
	{
		if(value!=null)
		{
			if(value instanceof String)
			{
			   appendQuery(out, name, value.toString());
			}
			else if(value instanceof String[])
			{
				String[] values = (String[])value;
				for(int i=0; i<values.length; i++)
				{
					appendQuery(out, name, values[i]);
				}
			}
		}
	}
	
    public void appendHiddenInput(StringBuffer out)
    {
        if(value!=null)
        {
            if(value instanceof String)
            {
                appendHiddenInput(out, name, value.toString());
            }
            else if(value instanceof String[])
            {
                String[] values = (String[])value;
                for(int i=0; i<values.length; i++)
                {
                    appendHiddenInput(out, name, values[i]);
                }
            }
        }
    }    
    
    public static void appendHiddenInput(StringBuffer out, String name, String value)
    {
        out.append(_hiddeninput1);
        out.append(name);
        out.append(_hiddeninput2);
        out.append(value);
        out.append(_hiddeninput3);
    }
    
    public static void appendQuery(StringBuffer out, String name, String value)
    {
       if(out.length()!=0)
       {
          out.append('&');
       }
       out.append(StringUtil.urlEncode(name));
       out.append('=');
       out.append(StringUtil.urlEncode(value));
    }    
}