package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;

public interface NodeType
{     
    public static final int type_SelectSet = Node.type_Branch + 1; 
    public static final int type_Select = Node.type_Node + 1;        
    
    public static final int type_WhereSet = Node.type_Branch + 2;
    public static final int type_Where = Node.type_Node + 2;    
    
    public static final int type_FromSet = Node.type_Branch + 3;
    public static final int type_From = Node.type_Node + 3;
    
    public static final int type_GroupBySet = Node.type_Branch + 4;    
    public static final int type_GroupBy = Node.type_Node + 4;

    public static final int type_HavingSet = Node.type_Branch + 5;
    public static final int type_Having = Node.type_Node + 5;

    public static final int type_InsertSet = Node.type_Branch + 6;
    public static final int type_Insert = Node.type_Node + 6;

    public static final int type_OrderBySet = Node.type_Branch + 7;     
    public static final int type_OrderBy = Node.type_Node + 7;
    
    public static final int type_SubSelectSet = Node.type_Branch + 8;     
    public static final int type_SubSelect = Node.type_Node + 8;
    
    public static final int type_Sum = Node.type_Node + 9;
    
    public static final int type_Count = Node.type_Node + 10;
    
    public static final int type_Limit = Node.type_Node + 11; 

    public static final int type_SimpleWhereExistsSet = Node.type_Branch + 12;
     
    public static final int type_DeleteSet = Node.type_Branch + 13;   
    
    public static final int type_Statement = Node.type_Node + 14;      
    public static final int type_StatementHead = Node.type_Branch + 14;
    
    public static final int type_UpdateSet = Node.type_Branch + 15;
    public static final int type_Update = Node.type_Node + 15; 
    
    public static final int type_WhereGroupSet = Node.type_Branch + 16;      
    
    public static final int type_JoinOnSet = Node.type_Branch + 17;
    public static final int type_JoinOn = Node.type_Node + 17;     
}