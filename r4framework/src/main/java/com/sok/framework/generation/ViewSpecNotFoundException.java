package com.sok.framework.generation;

public class ViewSpecNotFoundException extends RuntimeException
{
	StringBuffer error = new StringBuffer();
	
	public ViewSpecNotFoundException(String name)
	{
		super();
		error.append("Expected ViewSpec ");
		if(name!=null)
		{
			error.append(name);
		}
		error.append(" not found");
	}
	
	public String getMessage()
	{
		return(error.toString());
	}
	
	public String toString()
	{
		return(getMessage());
	}
}