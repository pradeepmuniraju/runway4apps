package com.sok.framework.generation;

import com.sok.framework.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.AbstractDatabase;
import com.sok.framework.sql.MSSQL;
import com.sok.framework.sql.MySQL;
import com.sok.framework.sql.SqlDatabase;
import com.sok.framework.sql.VelocityDatabase;

public class NodeMap extends BranchNode implements org.apache.velocity.context.Context, java.util.Map
{
   protected SqlDatabase database;   
   
   public NodeMap()
   {
      super(null);
   }  
   
   public NodeMap(String name)
   {
      super(name);
   }     
   
   public Object[] getKeys()
   {
      Object[] keys = new Object[size()];
      Node head = getHeadNode();
      for(int i=0; i<keys.length && head!=null; i++)
      {
         keys[i] = head.getName();
         head = head.getNextNode();
      }
      return(keys);
   }  
   
   public String getString(String name)
   {
      Node node = getNode(name);
      if(node!=null && node instanceof LeafNode)
      {
         Object value = ((LeafNode)node).getValue();
         if(value!=null)
         {
            return(value.toString());
         }
      }
      return("");
   }  

   public boolean isSet(String name) 
   {
	   return getString(name).length()>0;
   }
   
    public String getString(String name, int type)
    {
      Object value = get(name, type);
      if(value!=null)
      {
          return(value.toString());
      }
        return("");
    }    
    
    public Object get(String name, int type)
    {
        Node node = getNode(name, type);
        if(node!=null)
        {
            Object value = ((LeafNode)node).getValue();
            return(value);
        }
        return(null);
    }    
        
    
   public Object get(String name)
   {  
      return(super.get(name));
   }
   
   public Object put(String name, Object value)
   {  
      return(super.put(name, value));
   }     
   
   public void setDatabase(SqlDatabase sd)
   {
       database = sd;
   }
   
  public void setDatabase(String s)
  {
     if(s!=null)
     {
        database = AbstractDatabase.getDatabase(s);
     }
     else
     {
        database = new MSSQL();
     }
  }  
  
  public SqlDatabase getDatabase()
  {
     if(database == null)
     {
        database = ActionBean.getDatabase();
     }
     if(database == null)
     {
        database = new MySQL();
     }     
     return(database);
  }
     
   
   public void useVelocityDatabase()
   {
      if(get(VelocityDatabase.DATABASE) == null)
      {
         this.addNode(new InternalTokenParameterNode(VelocityDatabase.DATABASE, new VelocityDatabase(this.getDatabase())));
      }
   }     
}