package com.sok.framework.generation.nodes.sql.search;
import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.nodes.sql.*;

public class StringConstraint extends AbstractConstraint implements GenerationKeys
{
    public StringConstraint()
    {
        super();
    }
    
    public void appendProcessedValue(StringBuffer out, String value, RootNode root)
    {
        if(value.equals(value_NULL))
        {
            out.append(value_NULL);
        }
        else if(value.equals(value_EMPTY))
        {
            appendValue(out, _blank);
        }  
        else if(value.equals(value_CURRENTUSER))
        {
            appendValue(out, root.getString(key_CURRENTUSERID));
        }         
        else
        {
            appendValue(out, value);
        }
    }
}