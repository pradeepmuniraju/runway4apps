package com.sok.framework.generation.nodes.sql;

import java.util.StringTokenizer;

import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.*;

public class FromSetNode extends AbstractGeneratorSetNode implements SQLKeys
{	
	protected String alias = null;
	
	public FromSetNode(String name, String alias)
	{
		super(name);
		this.alias = alias;
		super.skipifempty = false;
	}	
	
   public FromNode setFrom(String totablealias, String fromtablename, String totablename,
         String fromfield, String tofield, String jointype, int priority, String script)
 {
     //System.out.println("Empty? "+String.valueOf(isEmpty()));
     FromNode fromnode = null;
     String name = totablealias!=null?totablealias:totablename;
     Node node = getNode(name);
     if(node != null && node instanceof FromNode)
     {
         //System.out.println("found node "+name);
         fromnode = (FromNode)node;
     }
     else
     {
         //System.out.println("new node "+name);
         fromnode = new FromNode(name, totablealias, fromtablename, totablename, fromfield, tofield, jointype, priority);
         addNode(fromnode);
         if(script!=null && script.length()!=0)
         {
            fromnode.setScript(script);
         }
     }
     //System.out.println("jointype "+jointype);
     return(fromnode);
 } 
	
    public FromNode setFrom(String totablealias, String fromtablename, String totablename,
            String fromfield, String tofield, String jointype, int priority)
    {
        return(setFrom(totablealias, fromtablename, totablename, fromfield, tofield, jointype, priority, null));
    }    
    
	public boolean hasJoin(String relationset)
	{	
		StringTokenizer st = new StringTokenizer(relationset, ".");
		if(!st.hasMoreTokens())
		{
			return(false);
		}
		while(st.hasMoreTokens())
		{
			if(getNode(st.nextToken())==null)
			{
				return(false);
			}
		}
		return(true);
	}
	
	protected void appendOut(RootNode root, StringBuffer buffer)
	{
           SqlDatabase database = root.getDatabase();
		buffer.append(rn);
		buffer.append(sql_from);
		database.appendWithQuote(buffer,name);
		if(alias!=null)
		{
			buffer.append(sql_as);
			database.appendWithQuote(buffer,alias);
		}
	}
	
	protected void appendEnd(RootNode root, StringBuffer buffer)
	{
		
	}

	protected void appendSeparator(RootNode root, StringBuffer buffer)
	{

	}

    public int getNodeType()
    {
        return(NodeType.type_FromSet);
    }      
}