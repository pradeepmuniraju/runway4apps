package com.sok.framework.generation;

import java.io.*;

public class TracedException extends RuntimeException
{
   public TracedException()
   {
      super();
   }

   public TracedException(String message)
   {
      super(message);
   }

   public TracedException(Exception ex)
   {
      super(ex);
   }

   public TracedException(String message, Exception ex)
   {
      super(message, ex);
   }

   public static String getStackTraceAsString(Throwable e)
   {
      CharArrayWriter c = new CharArrayWriter();
      e.printStackTrace(new PrintWriter(c));
      return(c.toString());
   }

   public String getStackTraceAsString()
   {
      if(this.getCause() != null)
      {
         return(getStackTraceAsString(this.getCause()));
      }
      else
      {
         return(getStackTraceAsString(this));
      }
   }
}