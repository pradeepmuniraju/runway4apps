package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;

public class GroupBySetNode extends AbstractPrioritisedGeneratorSetNode
{
	public GroupBySetNode()
	{
		super(sql_groupby);    
	}	
	
	public GroupBySetNode(String name)
	{
		super(name);
	}
	
	public GroupByNode setGroupBy(String tablename, String fieldname, int num)
	{
		GroupByNode node = new GroupByNode(tablename, fieldname, num);
		addPrioritisedNode(node);
		return(node);
	}		
	
	protected void appendOut(RootNode root, StringBuffer buffer)
	{
        buffer.append(rn);        
		buffer.append(sql_groupby);
	}
	
	protected void appendEnd(RootNode root, StringBuffer buffer)
	{

	}

	protected void appendSeparator(RootNode root, StringBuffer buffer)
	{
		buffer.append(sql_comma);
	}	

    public int getNodeType()
    {
        return(NodeType.type_GroupBySet);
    }      
}