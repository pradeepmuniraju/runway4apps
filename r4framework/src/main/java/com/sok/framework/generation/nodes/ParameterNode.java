package com.sok.framework.generation.nodes;

import java.util.Map;

public interface ParameterNode extends Node, Parameter,  Map.Entry
{ 
    public boolean isEmpty();
}