package com.sok.framework.generation.nodes;

import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.sql.StatementNode;
import com.sok.framework.sql.*;

public class RootNode extends NodeMap implements MapData, GenerationKeys
{
	protected StringBuffer errors = new StringBuffer();	
	protected int updatedCount = 0;
	protected StatementNode statementnode = null;
	
    private ViewSpec vspec = null;
    private TableSpec tspec = null;
    private RelationSpec rspec = null;    
    
    private String convert = null;
    
	public RootNode(String name)
	{
		super(name);
	}	

    public String getDateConversionsType()
    {
        return(convert);
    }
    
    public void setDateConversionsType(String s)
    {
        convert = s;
    }    
    
    public void setViewSpec(ViewSpec vs)
    {
        vspec = vs;
        if(vs!=null)
        {
           tspec = SpecManager.getTableSpec(vspec.getMainTableName());
           rspec = SpecManager.getRelationSpec();
           put(_viewspec, vs.getName());
        }
        else
        {
           put(_viewspec, null);
        }
    }

    public void setTableSpec(TableSpec ts)
    {
        tspec = ts;
        rspec = SpecManager.getRelationSpec();
        put(_tablespec, ts.getName());
    }

    public void setViewSpec(String vs)
    {
        if(vs!=null)
        {
           vspec = SpecManager.getViewSpec(vs);
           tspec = SpecManager.getTableSpec(vspec.getMainTableName());
           rspec = SpecManager.getRelationSpec();
        }
        else
        {
           vspec = null;
        }
        put(_viewspec, vs);
    }
    
    public void setTableSpec(String ts)
    {
        tspec = SpecManager.getTableSpec(ts);
        rspec = SpecManager.getRelationSpec();
        put(_tablespec, ts);
 
    }
    
    public TableSpec getTableSpec()
    {
        if(tspec==null)
        {
            setTableSpec(getString(_tablespec));
        }
        return(tspec);
    }

    public ViewSpec getViewSpec()
    {
        if(vspec==null)
        {
            String vs = getString(_viewspec);
            if(vs.length()!=0)
            {
                setViewSpec(vs);
            }
        }        
        return(vspec);
    }

    public RelationSpec getRelationSpec()
    {
        if(rspec==null)
        {
            rspec = SpecManager.getRelationSpec();
        }
        return(rspec);
    }
    
	public void setStatementNode(StatementNode node)
	{
		statementnode = node;
	}
	
	public StatementNode getStatementNode()
	{
		return(statementnode);
	}		


	public void appendError(String msg)
	{
		errors.append(msg);
		errors.append("\r\n");
	}
	
	public String getErrors()
	{
		return(errors.toString());
	}
	
    public void setUpdatedCount(int c)
    {
        updatedCount = c;
    }    
    
	public int getUpdatedCount()
	{
		return(updatedCount);
	}
	
	public String generate()
	{
		StringBuffer buffer = new StringBuffer();
		statementnode.generate(this, buffer);
		return(buffer.toString());
	}   
    
    public String getStatementCriteria()
    {
        StringBuffer buffer = new StringBuffer();
        statementnode.getWhereSetNode().generateChildren(this, buffer);
        return(buffer.toString());
    }
    
    public void addNode(Node node)
    {
       if(node instanceof PrioritisedNode)
       {
          addPrioritisedNode((PrioritisedNode)node);
       }
       else
       {
          super.addNode(node);
       }
    }
        
}