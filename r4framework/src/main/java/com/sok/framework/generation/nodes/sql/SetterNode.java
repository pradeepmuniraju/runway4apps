package com.sok.framework.generation.nodes.sql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.*;

public interface SetterNode extends Node
{
    public int setValue(PreparedStatement updatestm, Row row, int index)throws SQLException;
    
    public String getFormat();
    
    public int getDataType();
    
    public Object getValue();
    
}