package com.sok.framework.generation.nodes.sql;

import java.sql.PreparedStatement;

import com.sok.framework.generation.DatabaseFactory;
import com.sok.framework.generation.Row;
import com.sok.framework.generation.nodes.*;
import java.sql.SQLException;
//import com.sok.framework.generation.*;

public abstract class AbstractSetterGeneratorNode
    extends AbstractGeneratorNode
    implements SetterNode
{
    public AbstractSetterGeneratorNode(String name)
    {
        super(name);
    }
    
    public int setValue(PreparedStatement pstmt, Row row, int index)throws SQLException
    {
        return(DatabaseFactory.setValue(pstmt, this, row, index));
    }
    
    public abstract String getFormat();
    
    public abstract int getDataType();  
}