package com.sok.framework.generation.nodes;

//import com.sok.framework.*;

public class DataNode extends LeafNode
{
    String format;
    String type;
    
    public DataNode(String name, Object value, String type, String format)
    {
        super(name, value);
        this.format = format;
        this.type = type;
    }
    
    public int getNodeType()
    {
        return(type_DataNode);
    }

    public void setFormat(String type, String format)
    {
        this.format = format;
        this.type = type;
    }
    
    public String getFormat()
    {
        return(format);
    }
    
    public String getType()
    {
        return(type);
    }    
}