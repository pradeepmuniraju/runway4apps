package com.sok.framework.generation.nodes;

//import com.sok.framework.*;

public class RequestParameterNode extends AbstractParameterNode
{
    public RequestParameterNode(String name, Object value)
    {
        super(name, value);

    }
    
    public int getNodeType()
    {
        return(type_RequestParameter);
    }    
}