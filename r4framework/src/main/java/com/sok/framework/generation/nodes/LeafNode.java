package com.sok.framework.generation.nodes;

import java.util.Map;

public class LeafNode extends DefaultNode implements Map.Entry
{
	protected Object value = null;
	protected RootNode rootnode = null;
	
	public LeafNode(String name, Object value)
	{
		super(name);
		this.value = value;
	}
	
	// Returns the value corresponding to this entry.
	public Object getValue()
	{
		return(value);
	}

	public Object setValue(Object value)
	{
		Object oldvalue = this.value;
		this.value = value;
		return(oldvalue);
	}
    
	//Returns the key corresponding to this entry.
	public Object getKey()
	{
		return(name);
	}	
	
}