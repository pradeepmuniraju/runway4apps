package com.sok.framework.generation.nodes.sql;

import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.SqlDatabase;

public class OrderByNode extends AbstractGeneratorNode implements PrioritisedNode
{
	String tablename, order;
	int num = 0;
	
	public OrderByNode(String tablename, String fieldname, String order, int num)
	{
		super(fieldname);
		this.tablename = tablename;
		this.order = order;
		this.num = num;
	}
	
	public String getTableName()
	{
		return(tablename);
	}

	public String getFieldName()
	{
		return(name);
	}

	public int getPriority()
	{
		return(num);
	}	
	
	public void generate(RootNode root, StringBuffer out)
	{
      SqlDatabase database = root.getDatabase();
		if(tablename!=null)
		{
			database.appendWithQuote(out,RelationSpec.escapeRelationName(tablename));
			out.append(sql_dot);
		}
		database.appendWithQuote(out,name);
		if(order != null)
		{
			out.append(space);
			out.append(order);
		}
	}
    
    public int getNodeType()
    {
        return(NodeType.type_OrderBy);
    }  
}