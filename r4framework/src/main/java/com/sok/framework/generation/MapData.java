package com.sok.framework.generation;

import com.sok.framework.sql.SqlDatabase;
import com.sok.framework.*;

public interface MapData extends org.apache.velocity.context.Context, java.util.Map
{    
	public String getString(String name);
    
    public void setViewSpec(ViewSpec vs);

    public void setTableSpec(TableSpec ts);

    public void setViewSpec(String vs);  
    
    public void setTableSpec(String ts);
    
    public TableSpec getTableSpec();

    public ViewSpec getViewSpec();

    public RelationSpec getRelationSpec(); 

    public SqlDatabase getDatabase();
    
    public void setDatabase(SqlDatabase database);  
    
    public String getDateConversionsType();
    
    public void setDateConversionsType(String type);    
}