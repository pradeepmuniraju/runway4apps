package com.sok.framework.generation.nodes;

//import com.sok.framework.*;

public class InternalTokenParameterNode extends TokenParameterNode
{
    public InternalTokenParameterNode(String name, Object value)
    {
        super(name, value);

    }
    
    public int getNodeType()
    {
        return(type_InternalTokenParameter);
    }    
    
    public void appendQuery(StringBuffer out){}
      
}