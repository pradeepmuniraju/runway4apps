package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.*;

public class UpdateNode extends AbstractSetterGeneratorNode
{
    Object fieldvalue;
    int datatype = jdbctype_string;
    String format;
    int setindex = -1;
    
    public UpdateNode(String fieldname, Object fieldvalue, int datatype, String format)
    {
        super(fieldname);
        this.fieldvalue = fieldvalue;
        this.datatype = datatype;
        this.format = format;
    }  
    
    public Object getValue()
    {
        return(fieldvalue);
    }
    
    public int getSetIndex()
    {
        return(setindex);
    }
    
    public String getString()
    {
        return(fieldvalue.toString());
    }
    
    public String getFormat()
    {
        return(format);
    }
    
    public int getDataType()
    {
        return(datatype);
    }
    
    public void generate(RootNode root, StringBuffer out)
    {
       SqlDatabase database = root.getDatabase();
        if(name.indexOf('(') < 0)
        {
            database.appendWithQuote(out,name);
            out.append(sql_equals);
            out.append(sql_questionmark);
        }
    }
    
    public int getNodeType()
    {
        return(NodeType.type_Update);
    }   
}