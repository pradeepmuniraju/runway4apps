package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.*;

public class UpdateSetNode extends StatementHeadNode
{
    public UpdateSetNode()
    {
        super(sql_update);
        super.skipifempty = false;
    }       
    
    public UpdateSetNode(String name)
    {
        super(name);
        super.skipifempty = false;
    }   
    
    public UpdateNode setUpdate(String fieldname, Object fieldvalue, int datatype, String format)
    {
        UpdateNode node = new UpdateNode(fieldname, fieldvalue, datatype, format);
        addNode(node);
        return(node);
    }   
    
    protected void appendOut(RootNode root, StringBuffer buffer)
    {
       SqlDatabase database = root.getDatabase();
        buffer.append(sql_update);
        database.appendWithQuote(buffer,name);
        buffer.append(sql_set);
        buffer.append(rn);
    }
    
    protected void appendEnd(RootNode root, StringBuffer buffer)
    {
        if(fromsetnode!=null)
        {
            fromsetnode.generate(root, buffer);
        }
    }

    protected void appendSeparator(RootNode root, StringBuffer buffer)
    {
        buffer.append(sql_comma);
    }
    
    public int getNodeType()
    {
        return(NodeType.type_UpdateSet);
    }      
}