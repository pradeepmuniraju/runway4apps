package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.*;

public class InsertSetNode extends StatementHeadNode
{
    public InsertSetNode()
    {
        super(sql_insert);
        super.skipifempty = false;
    }       
    
    public InsertSetNode(String name)
    {
        super(name);
        super.skipifempty = false;
    }   
    
    public InsertNode setInsert(String fieldname, Object fieldvalue, int datatype, String format)
    {
        InsertNode node = new InsertNode(fieldname, fieldvalue, datatype, format);
        addNode(node);
        return(node);
    }   
    
    protected void appendOut(RootNode root, StringBuffer buffer)
    {
       SqlDatabase database = root.getDatabase();
        buffer.append(sql_insert);
        buffer.append(sql_into);
        database.appendWithQuote(buffer,name);        
        buffer.append(rn);
        buffer.append(sql_bracketbegin);
    }
    
    protected void appendEnd(RootNode root, StringBuffer buffer)
    {
        buffer.append(sql_bracketend);
        buffer.append(sql_values);
        buffer.append(sql_bracketbegin);
        for(int i=0; i<size(); i++)
        {
            if(i!=0)
            {
                buffer.append(sql_comma);
            }
            buffer.append(sql_questionmark);
        }
        buffer.append(sql_bracketend);
        if(fromsetnode!=null)
        {
            fromsetnode.generate(root, buffer);
        }
    }

    protected void appendSeparator(RootNode root, StringBuffer buffer)
    {
        buffer.append(sql_comma);
    }
    
    public int getNodeType()
    {
        return(NodeType.type_InsertSet);
    }      
}