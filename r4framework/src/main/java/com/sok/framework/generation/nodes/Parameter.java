package com.sok.framework.generation.nodes;

public interface Parameter
{
    public static final int type_Parameter = Node.type_ParameterNode;  
    public static final int type_MinParameter = Node.type_ParameterNode;    
    public static final int type_MaxSearchParameter = type_Parameter + 20;  
    public static final int type_MaxParameter = Node.type_ParameterNode + 99;     
   
    public static final int type_RequestParameter = type_Parameter + 1;
    public static final int type_OmitParameter = type_Parameter + 2;
    public static final int type_JoinParameter = type_Parameter + 3;

    public static final int type_WhereParameter = type_MaxSearchParameter + 1;

    public static final int type_SelectParameter = type_MaxSearchParameter + 3;
    public static final int type_TokenParameter = type_MaxSearchParameter + 4;

    public static final int type_OrderByParameter = type_MaxSearchParameter + 5;
    public static final int type_GroupByParameter = type_MaxSearchParameter + 6;
    public static final int type_HavingParameter = type_MaxSearchParameter + 7;
    public static final int type_LimitParameter = type_MaxSearchParameter + 8;
    public static final int type_InternalTokenParameter = type_MaxSearchParameter + 9;
    

    
	public void appendQuery(StringBuffer buffer);
   
    public void appendHiddenInput(StringBuffer buffer); 
    
}