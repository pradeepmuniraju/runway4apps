package com.sok.framework.generation.nodes.sql.join;

import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.AbstractGeneratorNode;
import com.sok.framework.generation.nodes.RootNode;

public class JoinOnValueNode extends JoinOnNode
{
   protected String tablename = null;   
   protected String value = null;

   public JoinOnValueNode(String tablename, String name, String value)
   {
      super(name);
      this.tablename = tablename;    
      this.value = value;
   }
   
   public void generate(RootNode node, StringBuffer out)
   {
      if(value!=null && value.length()!=0)
      {
         out.append(RelationSpec.escapeRelationName(tablename));
         out.append(sql_dot);
         out.append(name);       
       
         out.append(sql_equals);
       
         out.append('\'');
         out.append(value);
         out.append('\'');
       }
   }   
}