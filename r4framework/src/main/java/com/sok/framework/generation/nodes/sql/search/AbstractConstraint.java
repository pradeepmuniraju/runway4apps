package com.sok.framework.generation.nodes.sql.search;
import java.util.*;

import com.sok.framework.ActionBean;
import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.nodes.sql.*;
import com.sok.framework.sql.*;

public abstract class AbstractConstraint implements Constraint, SQLKeys, GenerationKeys
{
   protected static final int opix = 1;
   protected static final int valueix = 0;
   protected static final String _ID = "ID";
   protected static final char _semicolon = ';';
   protected static final char _plus = '+';
   protected static final String _functionstart = "((";
   public AbstractConstraint()
   {
      
   }
   
   public void appendConstraint(RootNode root, WhereNode node, StringBuffer out, String value)
   {
      if(value.indexOf(_plus) > 0 || value.indexOf(_semicolon) > 0)
      {
         String[] tokens = split(value);

         int opindex = 0;
         char opchar = 0;
         String token = null;
         
         out.append(sql_bracketbegin);         
         
         for(int i=0; i<tokens.length; i++)
         {
            token = tokens[i];
            if(token.length()!=0)
            {
               //System.out.println(token);
               opindex = opindex + token.length();
               opchar = opindex < value.length() ? value.charAt(opindex) : 0;
            
               out.append(sql_bracketbegin);
               
               appendSingleConstraint(root, node, out, tokens[i]);
               opindex++;
               
               out.append(sql_bracketend);
               
               if(i<(tokens.length - 1))
               {
                  if(opindex !=0 && opchar == _plus)
                  {
                     if(node.isFlipOp())
                     {
                        out.append(sql_and);
                     }
                     else
                     {
                        out.append(sql_or);
                     }
                  }
                  else
                  {
                     if(node.isFlipOp())
                     {
                        out.append(sql_or);
                     }
                     else
                     {  
                        out.append(sql_and);
                     }      
                  }
               }            
            }
 
         }
         out.append(sql_bracketend);
      }
      else
      {
         appendSingleConstraint(root, node, out, value);
      }
   }  
   
   protected static String[] split(String value)
   {
      ArrayList tokens = new ArrayList();
      StringBuffer token = new StringBuffer();
      char tempchar = 0;
      for(int i=0; i<value.length(); i++)
      {
         tempchar = value.charAt(i);
         if(i!=0 && isOp(tempchar))
         {
            if(i+1 < value.length() && tempchar == _plus && value.charAt(i+1) == _plus)
            {
               token.append(tempchar);
               i++;
            }
            else if(i+1 < value.length() && tempchar == _semicolon && value.charAt(i+1) == _semicolon)
            {
               token.append(tempchar);
               i++;
            } 
            else if(i+1 == value.length())
            {
               token.append(tempchar);
            }
            else
            {
               tokens.add(token.toString());
               //System.out.println("added "+token);
               token = new StringBuffer();
            }
         }
         else
         {
            token.append(tempchar);
         }
      }
      tokens.add(token.toString());

      String[] stringtokens = new String[tokens.size()];
      tokens.toArray(stringtokens);
      return(stringtokens);
   }
   
   protected static boolean isOp(char c)
   {
      return(c == _plus || c == _semicolon);
   }
   
   protected void appendEscapedOp(RootNode root, WhereNode node, StringBuffer out, String token, char op)
   {
      if(op == _plus)
      {
         appendSingleConstraint(root, node, out, token + _plus);
      }
      else if(op == _semicolon)
      {
         appendSingleConstraint(root, node, out, token + _semicolon);
      }
   }
   
   protected void appendSingleConstraint(RootNode root, WhereNode node, StringBuffer out, String value)
   {
      SqlDatabase database = root.getDatabase();
      node.appendFullName(out, database);
      boolean omit = node.isOmit();
      if(value.indexOf('!')==0)
      {
         value =  value.substring(1, value.length());
         omit = !omit;
      }
      
      String valuestr[] = setOp(node.getName(), value, omit);
      
      out.append(valuestr[opix]);
      
      appendProcessedValue(out, valuestr[valueix], root);
   }
   
   protected String[] setOp(String name, String value, boolean omit)
   {
      String[] returnvalue = new String[2];
      returnvalue[valueix] = value;
      if(value.equals(value_NULL))
      {
         returnvalue[opix] = sql_is;
      }
      else if(name.endsWith(_ID ))
      {
         returnvalue[opix] = sql_equals;
      }
      else if(value.indexOf('<')==0 && value.indexOf('=')==1)
      {
         returnvalue[opix] = sql_greater_than_or_equals;
         returnvalue[valueix] = value.substring(2, value.length());
      }
      else if(value.indexOf('>')==0 && value.indexOf('=')==1)
      {
         returnvalue[opix] = sql_less_than_or_equals;
         returnvalue[valueix] = value.substring(2, value.length());
      }
      else if(value.indexOf('<')==0)
      {
         returnvalue[opix] = sql_greater_than;
         returnvalue[valueix] = value.substring(1, value.length());
      }
      else if(value.indexOf('>')==0)
      {
         returnvalue[opix] = sql_less_than;
         returnvalue[valueix] = value.substring(1, value.length());
      }
      else if (value.indexOf("%") == -1)
      {
         returnvalue[opix] = sql_equals;
      }
      else
      {
         returnvalue[opix] = sql_like;
      }
      
      if(omit)
      {
         returnvalue[opix] = getOmitOp(returnvalue[opix]);
      }
      return(returnvalue);
   }
   
   protected String getOmitOp(String opstr)
   {
      String omitop = null;
      if(opstr.equals(sql_like))
      {
         omitop = sql_not_like;
      }
      else if(opstr.equals(sql_is))
      {
         omitop = sql_is_not;
      }
      else if(opstr.equals(sql_not_like))
      {
         omitop = sql_like;
      }
      else if(opstr.equals(sql_equals))
      {
         omitop = sql_not_equals;
      }
      else if(opstr.equals(sql_not_equals))
      {
         omitop = sql_equals;
      }
      else if(opstr.equals(sql_greater_than))
      {
         omitop = sql_less_than_or_equals;
      }
      else if(opstr.equals(sql_less_than))
      {
         omitop = sql_greater_than_or_equals;
      }
      else if(opstr.equals(sql_less_than_or_equals))
      {
         omitop = sql_greater_than;
      }
      else if(opstr.equals(sql_greater_than_or_equals))
      {
         omitop = sql_less_than;
      }
      if(omitop == null)
      {
         throw new IllegalConfigurationException("omit op not found for "+ opstr);
      }
      return(omitop);
   }
   
   protected abstract void appendProcessedValue(StringBuffer out, String valuestr, RootNode node);
   
   protected void appendValue(StringBuffer out, String valuestr)
   {
      boolean isvalue = valuestr.indexOf(_functionstart)<0;
      if(isvalue){
         out.append('\'');
      }
      out.append(ConstraintFactory.escapeSQL(valuestr));
      if(isvalue){
         out.append('\'');
      }
   }
}