package com.sok.framework.generation.util;
import javax.servlet.http.HttpServletRequest;
import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.LeafNode;
import com.sok.framework.generation.nodes.Parameter;
public class TableDataHistorySet extends HistorySet
{   
   protected boolean usingNextSet = false;
   
   protected TableData maindata = null;
   protected String mainname = null;
   protected GenRow selection = new GenRow(); 
   
   public TableDataHistorySet()
   {
      super(9, true, false, true);
      appendTableDataHistory();
   }
   
   public TableDataHistorySet(int maxsize)
   {
      super(maxsize, true, false, true);
      appendTableDataHistory();
   }
   
   public TableDataHistorySet(int maxsize, boolean uniquenames, boolean ignorecase, boolean keeporder)
   {
      super(maxsize, uniquenames, ignorecase, keeporder);
      appendTableDataHistory();
   }
   
   public void parseSelection(HttpServletRequest request)
   {
      selection.clear();
      selection.parseRequest(request);
   }
   
   public boolean isSelected(String historykey, String datakey)
   {
       return(datakey.equals(selection.getString(historykey)));
   }
   
   public void setSelected(String historykey, String datakey)
   {
      selection.put(historykey, datakey);
   }
   
   public GenRow getSelection()
   {
      return(selection);
   }
   
   public int getSelectionCount()
   {
      return(selection.size(Parameter.type_RequestParameter));
   }
/*
   public GenRow mergeSelected()
   {
      TableDataHistory history = null;
      GenRow lasttdata = null;
      GenRow tdata = null;
      
      Object[] keys = selection.getKeys();
      String datakey, setkey = null;
      for(int i=0; i<keys.length; i++)
      {
         setkey = keys[i].toString();
         history = getTableDataHistory(setkey);
         if(history != null)
         {
            datakey = selection.getString(setkey);
            if(lasttdata == null)
            {
               lasttdata = (GenRow)history.getTableData(datakey);
            }
            else
            {
               tdata = DatabaseFactory.mergeRequests(lasttdata, (GenRow)history.getTableData(datakey), true, i);
               lasttdata = tdata;
            }
         }
      }
      if(tdata!=null)
      {
         setMainTableData(KeyMaker.generate(), tdata);
      }
      return(tdata);
   }
*/   
   public GenRow mergeSelected()
   {
      GenRow tdata = null;
      if(getSelectionCount()>0)
      {
         TableDataHistory history = null;
         String datakey, setkey = null;
         Object[] keys = selection.getKeys();
         GenRow[] rows = new GenRow[keys.length];
   
         for(int i=0; i<keys.length; i++)
         {
            setkey = keys[i].toString();
            history = getTableDataHistory(setkey);
            if(history != null)
            {
               datakey = selection.getString(setkey);
               rows[i] = (GenRow)history.getTableData(datakey);
            }
         }
         tdata = DatabaseFactory.mergeRequests(rows);
         if(tdata!=null)
         {
            setMainTableData(KeyMaker.generate(), tdata);
         }
      }
      return(tdata);
   }   

   public void setDefaultSelection()
   {
      this.resetCurrentAll();
      TableDataHistory history = null;
      while(next())
      {
         history = getTableDataHistory();
         if(selection.getString(getName()).length()==0)
         {
            if(!history.isEmpty())
            {
               selection.put(getName(), history.getName(history.size() - 1));
            }
         }
      }
   }
   
   public void setMainTableData(String name, TableData data)
   {
      this.mainname = name;
      this.maindata = data;
   }
   
   public TableData getMainTableData()
   {
      return(this.maindata);
   }
   
   public String getMainTableDataName()
   {
      return(this.mainname);
   }   
   
   public String appendTableDataHistory(TableDataHistory history)
   {
      String key = KeyMaker.generate();
      append(key, history);
      setCurrent(key);
      return(key);
   }
   
   public String appendTableDataHistory()
   {
      String tdhkey = appendHistory(getNewTableDataHistory());
      setCurrent(tdhkey);
      return(tdhkey);
   }   
   
   protected TableDataHistory getNewTableDataHistory()
   {
      return(new TableDataHistory(this.maxSize, this.uniqueNames, this.ignoreCase, this.keepOrder));
   }
   
   public TableDataHistory getTableDataHistory(String key)
   {   
      return((TableDataHistory)getValue(key));
   }

   public TableDataHistory getLastTableDataHistory()
   {
      if(last())
      {
         return(getTableDataHistory());
      }
      return(null);
   }
   
   public TableDataHistory getTableDataHistory()
   {
      return((TableDataHistory)getValue());
   }
   
   public TableData getTableData(String historykey, String datakey)
   {
      TableDataHistory temp = getTableDataHistory(historykey);
      return(temp.getTableData(datakey));
   }
   
   public TableData getTableData()
   {
      TableDataHistory history = getTableDataHistory(); 
      if(history!=null)
      {
         return(history.getTableData());
      }
      return(null);
   }
   
   public TableData findTableData(String datakey)
   {
      if(maindata!=null && mainname!=null)
      {
         if(mainname.equals(datakey))
         {
            return(maindata);
         }
      }
      TableData temp = null;
      for(int i = 0; i < size(); i++)
      {
         setCurrent(i);
         temp = getTableDataHistory().getTableData(datakey);
         if(temp!=null)
         {
            return(temp);
         }
      }
      return(null);
   }
   
   public int getTableDataSize(String historykey, String datakey)
   {
      TableDataHistory temp = getTableDataHistory(historykey);
      return(temp.size());
   }   
   
   public String appendTableData(String historykey, TableData data)
   {
      TableDataHistory temp = getTableDataHistory(historykey);
      return(temp.appendTableData(data));
   }
   
   public void truncateTableDataAfter(String historykey, String datakey)
   {
      TableDataHistory temp = getTableDataHistory(historykey);
      temp.truncateAfter(datakey);
   }
   
   public boolean usingNextSet()
   {
      return(usingNextSet);
   }
   
   public boolean nextTableData()
   {
      return(nextTableData(false));
   }
   
   protected boolean nextTableData(boolean nextset)
   {

      if(atStart()) //at beginning of set
      {
         if(!next())
         {
            //System.out.println("next at start false");
            return(false);
         }
         //System.out.println("next at start true");
      }
      
      TableDataHistory tdh = getTableDataHistory();
      if(tdh!=null)
      {
         boolean hasmore = tdh.next();
         //System.out.println("next tabledata "+String.valueOf(hasmore));
         if(hasmore)
         {
            usingNextSet = nextset;
            return(true);
         }
         
         if(next()) //at end of set, trying next set
         {
            //System.out.println("next at end true");
            return(nextTableData(true));
         } 
         //System.out.println("next at end false");
      }

      return(false);      
   }
   
   public boolean nextLastTableData()
   {
      if(next())
      {
         TableDataHistory tdh = getTableDataHistory();
         return(tdh.last());
      }
      return(false);      
   }   
   
   public void resetCurrentAll()
   {   
      for(int i=0; i<size(); i++)
      {
         this.setCurrent(i);
         getTableDataHistory().resetCurrent();
      }
      super.resetCurrent();      
   }
   
   public int tableDataSize()
   {
      int count = 0;
      for(int i=0; i<size(); i++)
      {
         this.setCurrent(i);
         count = count + getTableDataHistory().size();
      }
      return(count);
   }
   
   public static void main(String[] args)
   {
      ActionBean.setDatabase(com.sok.framework.sql.SqlDatabase.mysql);      
      
      GenRow row1 = new GenRow();
      row1.setViewSpec("ContactListView");
      row1.setParameter("FirstName", "test");
      row1.setParameter("LastName", "contact");
      
      GenRow row2 = new GenRow();
      row2.setViewSpec("ContactListView");
      row2.setParameter("FirstName", "a%");
      row2.setParameter("ContactCurrentStatus.StatusID", "40848170A4424749256580E11154F302");
      
      TableDataHistorySet hs = new TableDataHistorySet();
      String hkey1 = hs.appendTableDataHistory();
      String hkey2 = hs.appendTableDataHistory();

      String key1 = hs.appendTableData(hkey1, row1);
      String key2 = hs.appendTableData(hkey2, row2);
      
      hs.setSelected(hkey1, key1);
      hs.setSelected(hkey2, key2);
     
      hs.getSelection().setParameter("-token", "value");
      
      System.out.println(hs.getSelectionCount());
     
      
      GenRow tdata = hs.mergeSelected();
      if(tdata!=null)
      {
         tdata.setViewSpec("ContactListView");
         tdata.doAction("search");
         System.out.print(tdata.getStatement());
      }
   }
}