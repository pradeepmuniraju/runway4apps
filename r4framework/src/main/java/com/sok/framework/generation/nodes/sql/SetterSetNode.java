package com.sok.framework.generation.nodes.sql;

import java.sql.PreparedStatement;
import com.sok.framework.generation.*;

public interface SetterSetNode
{
    public int setStatementValues(PreparedStatement updatestm, Row row, int index);
}