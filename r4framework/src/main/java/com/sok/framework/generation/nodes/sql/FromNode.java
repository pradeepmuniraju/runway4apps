package com.sok.framework.generation.nodes.sql;

import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.*;
import com.sok.framework.generation.nodes.sql.join.*;
//import com.sok.framework.*;

public class FromNode extends AbstractGeneratorNode implements Joinable
{
   String totablealias,
         fromtablename, totablename,
         fromfield,  tofield,
         jointype, vscript;;
   
   int priority = 0;
   
   JoinOnSetNode joinons = null;
   
   public FromNode(String name, String totablealias, String fromtablename, String totablename, String fromfield, String tofield, String jointype, int priority)
   {
      super(name);
      this.totablealias = totablealias;
      this.fromtablename = fromtablename;
      this.totablename = totablename;
      this.fromfield = fromfield;
      this.tofield = tofield;
      this.jointype = jointype;
      this.priority = priority;
   }
   
   public void setJoinOnSetNode(JoinOnSetNode joinons)
   {
      this.joinons = joinons;
   }
   
   public void setScript(String vscript)
   {
      this.vscript = vscript;
   }
   
   public String getScript()
   {
      return(vscript);
   }   
   
   public int getPriority()
   {
      return(priority);
   }
   
   public String getToTable()
   {
      return(totablename);
   }
   
   public String getFromTable()
   {
      return(fromtablename);
   }
   
   public String getToTableAlias()
   {
      return(totablealias);
   }
   
   public String getToField()
   {
      return(tofield);
   }
   
   public String getFromField()
   {
      return(fromfield);
   }
   
   public String getJoinType()
   {
      return(jointype);
   }
   
   public void generate(RootNode root, StringBuffer out)
   {
      SqlDatabase database = root.getDatabase();
      out.append(rn);
      out.append(jointype);
      database.appendWithQuote(out,totablename);
      if(totablealias!=null)
      {
         out.append(sql_as);
         database.appendWithQuote(out,RelationSpec.escapeRelationName(totablealias));
      }
      if(!jointype.equals(sql_comma))
      {
         out.append(sql_on);
         database.appendWithQuote(out,RelationSpec.escapeRelationName(fromtablename));
         out.append(sql_dot);
         database.appendWithQuote(out,fromfield);
         out.append(sql_equals);
         database.appendWithQuote(out,RelationSpec.escapeRelationName(name));
         out.append(sql_dot);
         database.appendWithQuote(out,tofield);
      }
      if(joinons!=null)
      {
         joinons.generate(root, out);
      }
      if(vscript!=null)
      {
         root.getDatabase().appendConditions(root, vscript, out);
      }      
   }
   
   public int getNodeType()
   {
      return(NodeType.type_From);
   }
   
}