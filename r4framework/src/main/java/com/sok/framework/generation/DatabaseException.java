package com.sok.framework.generation;

public class DatabaseException extends TracedException
{
    String statement = null;
    
    public DatabaseException(String message)
    {
        super(message);
    }
    
    public DatabaseException(Exception ex)
    {
        super(ex);
    }    
    
    public DatabaseException(Exception ex, String statement)
    {
        super(ex);
        this.statement = statement;        
    }        
    
    public DatabaseException(String message, Exception ex)
    {
        super(message, ex);
    }     
    
    public String toString()
    {
        return(new StringBuffer(super.toString()).append("\r\n").append(statement).toString());
    }
}