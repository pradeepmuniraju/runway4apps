package com.sok.framework.generation.nodes;

import com.sok.framework.generation.*;

public class SelectParameterNode extends AbstractParameterNode implements
      SQLKeys
{
   String tablename, fieldname, alias;
   boolean iscustom = false;

   public SelectParameterNode(String name, Object value)
   {
      super(name, value);
      initValue();
   }

   public int getNodeType()
   {
      return(type_SelectParameter);
   }

   public Object setValue(Object value)
   {
      Object obj = super.setValue(value);
      initValue();
      return(obj);
   }

   protected void initValue()
   {
      fieldname = value.toString();

      iscustom = fieldname.indexOf('(') >= 0;

      int index = fieldname.lastIndexOf('.');
      if(index > 0 && !iscustom)
      {
         tablename = fieldname.substring(0, index);
         fieldname = fieldname.substring(index + 1, fieldname.length());
      }

      int asindex = fieldname.indexOf(sql_as);
      if(asindex > 0)
      {
         alias = fieldname.substring(asindex + sql_as.length(), fieldname
               .length());
         fieldname = fieldname.substring(0, asindex);
      }
   }

   public String getFieldName()
   {
      return(fieldname);
   }

   public String getFieldAlias()
   {
      return(alias);
   }

   public String getTableName()
   {
      return(tablename);
   }

   public void setTableName(String tablename)
   {
      this.tablename = tablename;
   }
}