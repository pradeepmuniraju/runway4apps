package com.sok.framework.generation.nodes.sql;

import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.sql.*;

public class WhereTokenNode extends WhereNode
{
   
	public WhereTokenNode(String name, Object value, String tablename, int datatype)
	{
		super(name, value, tablename, datatype);
	}    
    
	public void generate(RootNode root, StringBuffer out, String name, String value)
	{
		if(((RequestNode)root).getUseTokens())
		{
			SqlDatabase database = root.getDatabase();
			StringBuffer namebuffer = new StringBuffer();
			if(tablename!=null)
			{
				namebuffer.append(RelationSpec.escapeRelationName(tablename));
				namebuffer.append(sql_dot);
			}
			namebuffer.append(name);
			database.appendField(out, namebuffer.toString(), value, datatype, root);
		}
		else
		{
			super.generate(root, out, value);
		}
	}
		
}