package com.sok.framework.generation.nodes;

//import com.sok.framework.*;

public class OmitParameterNode extends AbstractParameterNode
{
    public OmitParameterNode(String name, Object value)
    {
        super(name, value);
    }
    
    public int getNodeType()
    {
        return(type_OmitParameter);
    }    
    
    public String getRelationshipName()
    {
        return(name.substring(1, name.length()));
    }
}