package com.sok.framework.generation.nodes;

import java.util.*;

public class BranchNode extends NodeList implements Map
{

   public BranchNode(String name)
   {
      super(name);
   }

   public void clear(int type)
   {
      Node node = headNode;
      Node temp = null;
      while(node != null)
      {
         if(node.getNodeType() == type)
         {
            temp = node;
            node = node.getNextNode();
            removeNode(temp);
         }
         else
         {
            node = node.getNextNode();
         }
      }
   }

   // Removes all mappings from this map (optional operation).
   public void clear()
   {
      headNode = null;
      tailNode = null;
      size = 0;
   }

   // Returns true if this map contains a mapping for the specified key.
   public boolean containsKey(Object key)
   {
      if(key instanceof String)
      {
         return(containsKey((String) key));
      }
      return(false);
   }

   // Returns true if this map contains a mapping for the specified key.
   public boolean containsKey(String name)
   {
      Node node = getNode(name);
      if(node == null)
      {
         return(false);
      }
      return(true);
   }

   // Returns true if this map maps one or more keys to the specified value.
   public boolean containsValue(Object value)
   {
      Node node = headNode;
      LeafNode lnode;
      while(node != null)
      {
         if(node instanceof LeafNode)
         {
            lnode = (LeafNode) node;
            if(value instanceof String
                  && lnode.getValue().toString().equals(value.toString()))
            {
               return(true);
            }
            else if(lnode.getValue() == value)
            {
               return(true);
            }
         }
         node = node.getNextNode();
      }
      return(false);
   }

   // Returns a set view of the mappings contained in this map.
   public Set<LeafNode> entrySet()
   {
      Node node = headNode;
      Set<LeafNode> list = new HashSet<LeafNode>();
      while(node != null)
      {
         if(node instanceof LeafNode)
         {
            list.add((LeafNode)node);
         }
         node = node.getNextNode();
      }
      return(list);
   }

   public Set<Node> entrySet(int type)
   {
      Node node = headNode;
      Set<Node> list = new HashSet<Node>();
      while(node != null)
      {
         if(node.getNodeType() == type)
         {
            list.add(node);
         }
         node = node.getNextNode();
      }
      return(list);
   }

   // Returns the value to which this map maps the specified key.
   public Object get(Object key)
   {
      Node node = getNode(key.toString());
      if(node != null && node instanceof LeafNode)
      {
         Object value = ((LeafNode) node).getValue();
         if(value != null)
         {
            return(value);
         }
      }
      return(null);
   }

   public Set keySet(int typerangestart, int typerangeend)
   {
      Node node = headNode;
      HashSet list = new HashSet();
      while(node != null)
      {
         if(node.getNodeType() >= typerangestart
               && node.getNodeType() <= typerangeend)
         {
            list.add(node.getName());
         }
         node = node.getNextNode();
      }
      return(list);
   }

   public Set keySet(int type)
   {
      return(keySet(type, type));
   }

   // Returns a set view of the keys contained in this map.
   public Set keySet()
   {
      return(keySet(type_minLeaf, type_maxLeaf));
   }

   // Associates the specified value with the specified key in this map
   // (optional operation).
   public Object put(Object name, Object value)
   {
      LeafNode paramnode = null;
      Node node = getNode(name.toString());
      Object lastvalue = null;

      if(node != null && node instanceof LeafNode)
      {
         paramnode = (LeafNode) node;
         lastvalue = paramnode.getValue();
         paramnode.setValue(value);
      }
      else
      {
         paramnode = new LeafNode(name.toString(), value);
         addNode(paramnode);
      }
 
      return(lastvalue);
   }

   // Copies all of the mappings from the specified map to this map (optional
   // operation).
   public void putAll(Map t)
   {
      Iterator keyit = t.keySet().iterator();
      Object key = null;
      while(keyit.hasNext())
      {
         key = keyit.next();
         put(key, t.get(key));
      }
   }

   // Removes the mapping for this key from this map if it is present (optional
   // operation).
   public Object remove(Object key)
   {
      if(key instanceof String)
      {
         return(remove((String) key));
      }
      return(null);
   }

   // Removes the mapping for this key from this map if it is present (optional
   // operation).
   public Object remove(String name)
   {
      Node node = getNode(name);
      Object lastvalue = null;
      if(node != null)
      {
         if(node instanceof LeafNode)
         {
            lastvalue = ((LeafNode) node).getValue();
         }
         removeNode(node);
      }
      return(lastvalue);
   }

   // Returns a collection view of the values contained in this map.
   public Collection values()
   {
      Node node = headNode;
      LeafNode lnode;
      ArrayList list = new ArrayList();
      while(node != null)
      {
         if(node instanceof LeafNode)
         {
            lnode = (LeafNode) node;
            list.add(lnode.getValue());
         }
         node = node.getNextNode();
      }
      return(list);
   }

   public int getType()
   {
      return(Node.type_Branch);
   }
   
   public void addPrioritisedNode(PrioritisedNode node)
   {
      if(headNode == null)
      {
         headNode = node;
         tailNode = node;
         size++;
      }
      else
      {
         Node child = headNode;
         PrioritisedNode pnode = null;
            boolean notadded = true;
         while(child!=null && notadded)
         {
            if(child instanceof PrioritisedNode)
            {
               pnode = (PrioritisedNode)child;
               if(pnode.getPriority() >= node.getPriority())
               {
                  this.insertBeforeNode(node, pnode);
                        notadded = false;
               }
               else if(pnode.getNextNode() == null)
               {
                  this.insertAfterNode(node, pnode);
                        notadded = false;
               }
            }
            else if(child.getNextNode() == null)
            {
               if(node.getPriority() > 0)
               {
                  pushNode(node);
                        notadded = false;
               }
               else
               {
                  appendNode(node);
                        notadded = false;
               }
            }
            child = child.getNextNode();
         }        
      }
   }   
}