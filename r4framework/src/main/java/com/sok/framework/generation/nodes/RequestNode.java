package com.sok.framework.generation.nodes;

import java.util.Iterator;
import java.util.Map;

import com.sok.framework.generation.*;

public class RequestNode extends RootNode implements GenerationKeys
{
   //the tokens path is deprecated
   protected boolean usetokens = false;
   
   public RequestNode(String name)
   {
      super(name);
   }
   
   protected Object put(String name, Object value, boolean stack)
   {
      String strvalue = null;
      if(value!=null)
      {
         strvalue = value.toString();
      }
      
      if(name.indexOf(_select)==0)
      {
         return(putSelect(name, value));
      }
      else if(name.indexOf(_join_prefix)==0)
      {
         return(putJoin(name, value));
      }
      else if(name.indexOf(_sort_prefix)==0)
      {
         return(putOrderBy(name, value));
      }
      else if(name.indexOf(_order_prefix)==0)
      {
         try {
            return(putOrderByDirection(name, value));
         }
         catch(Exception e) {
            return(putToken(name, value));
         }
      }
      else if(name.indexOf(_groupby_prefix)==0)
      {
         return(putGroupBy(name, value));
      }
      else if(name.indexOf(_limit_results)==0)
      {
         return(putLimit(value));
      }
      else if(name.indexOf(UPDATEON)==0)
      {
         return(putWhere(name, value));
      }
      else if(name.indexOf(_having_prefix)==0)
      {
         return(putHaving(name, value));
      }
      else if(name.indexOf('-')==0 && strvalue!=null && strvalue.equals(value_omit))
      {
         return(putOmit(name, value));
      }
      else if(name.indexOf('-')==0)
      {
         return(putToken(name, value));
      }
      else if(name.equals(key_CURRENTUSERID))
      {
         return(putToken(name, value));
      }
      else
      {
         if(value instanceof String && stack)
         {
            return(addRequest(name, strvalue));
         }
         else
         {
            return(putRequest(name, value));
         }
      }
   }   
   
   public Object put(String name, Object value)
   {
      return(put(name, value, false));
   }
   
   public Object add(String name, Object value)
   {
      return(put(name, value, true));
   }   
   
   public Object putToken(String name, Object value)
   {
      TokenParameterNode pnode = null;
      Node node = getNode(name, Parameter.type_TokenParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         pnode = (TokenParameterNode)node;
         lastvalue = pnode.getValue();
         pnode.setValue(value);
      }
      else
      {
         pnode = new TokenParameterNode(name, value);
         addNode(pnode);
      }
      return(lastvalue);
   }
   
   public Object putInternalToken(String name, Object value)
   {
      InternalTokenParameterNode pnode = null;
      Node node = getNode(name, Parameter.type_InternalTokenParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         pnode = (InternalTokenParameterNode)node;
         lastvalue = pnode.getValue();
         pnode.setValue(value);
      }
      else
      {
         pnode = new InternalTokenParameterNode(name, value);
         addNode(pnode);
      }
      return(lastvalue);
   }
   
   public Object getRequest(String name)
   {
      Node node = getNode(name, Parameter.type_RequestParameter);
      if(node != null)
      {
         RequestParameterNode pnode = (RequestParameterNode)node;
         return(pnode.getValue());
      }
      return(null);      
   }
   
   public Object putRequest(String name, Object value)
   {
      RequestParameterNode pnode = null;
      Node node = getNode(name, Parameter.type_RequestParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         pnode = (RequestParameterNode)node;
         lastvalue = pnode.getValue();
         pnode.setValue(value);
      }
      else
      {
         pnode = new RequestParameterNode(name, value);
         addNode(pnode);
      }
      return(lastvalue);
   }
   
   public Object addRequest(String name, String value)
   {
      Object exsistingvalue = getRequest(name);
      if(exsistingvalue != null)
      {
         if((exsistingvalue instanceof String[]))
         {
            String[] strarr = (String[])exsistingvalue;
            String[] newstrarr = new String[strarr.length+1];
            System.arraycopy(strarr, 0, newstrarr, 0, strarr.length);
            newstrarr[strarr.length] = value;
            return(putRequest(name, newstrarr));
         }
         else if(exsistingvalue instanceof String)
         {
            if(exsistingvalue.toString().length()!=0)
            {
               String[] newstrarr = new String[2];
               newstrarr[0] = (String)exsistingvalue;
               newstrarr[1] = value;
               return(putRequest(name, newstrarr));
            }
         }
         else
         {
            return(putRequest(name, value));
         }
      }
      return(putRequest(name, value));
   }
   
   public Object putLimit(Object value)
   {
      LimitParameterNode limitnode = null;
      Node node = getNode(_limit_results, Parameter.type_LimitParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         limitnode = (LimitParameterNode)node;
         lastvalue = limitnode.getValue();
         limitnode.setValue(value);
      }
      else
      {
         limitnode = new LimitParameterNode(value);
         addNode(limitnode);
      }
      return(lastvalue);
   }
   
   public Object putSelect(String name, Object value)
   {
      SelectParameterNode selectnode = null;
      Node node = getNode(name, Parameter.type_SelectParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         selectnode = (SelectParameterNode)node;
         lastvalue = selectnode.getValue();
         selectnode.setValue(value);
      }
      else
      {
         selectnode = new SelectParameterNode(name, value);
         addNode(selectnode);
      }
      return(lastvalue);
   }
   
   public Object putJoin(String name, Object value)
   {
      JoinParameterNode joinnode = null;
      Node node = getNode(name, Parameter.type_JoinParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         joinnode = (JoinParameterNode)node;
         lastvalue = joinnode.getValue();
         joinnode.setValue(value);
      }
      else
      {
         joinnode = new JoinParameterNode(name, value);
         addNode(joinnode);
      }
      return(lastvalue);
   }
   
   public Object putOmit(String name, Object value)
   {
      OmitParameterNode selectnode = null;
      Node node = getNode(name, Parameter.type_OmitParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         selectnode = (OmitParameterNode)node;
         lastvalue = selectnode.getValue();
         selectnode.setValue(value);
      }
      else
      {
         selectnode = new OmitParameterNode(name, value);
         addNode(selectnode);
      }
      return(lastvalue);
   }
   
   public Object putOrderByDirection(String name, Object value)
   {
      OrderByParameterNode orderbynode = null;
      String orderbyname = _sort_prefix + name.substring(_order_prefix.length(), name.length());
      Node node = getNode(orderbyname, Parameter.type_OrderByParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         orderbynode = (OrderByParameterNode)node;
         lastvalue = orderbynode.getDirection();
      }
      else
      {
         orderbynode = new OrderByParameterNode(orderbyname, null);
         addNode(orderbynode);
      }
      orderbynode.setDirection(value.toString());
      putToken(name, value);
      return(lastvalue);
   }
   
   public Object putOrderBy(String name, Object value, String direction)
   {
      OrderByParameterNode orderbynode = null;
      Node node = getNode(name, Parameter.type_OrderByParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         orderbynode = (OrderByParameterNode)node;
         lastvalue = orderbynode.getValue();
         orderbynode.setValue(value);
         if(direction!=null)
         {
            orderbynode.setDirection(direction);
         }
      }
      else
      {
         try{ //ignore -sort like parameters that dont have a number suffix
            orderbynode = new OrderByParameterNode(name, value, direction);
            addNode(orderbynode);
         }catch(Exception e){}
      }
      return(lastvalue);
   }
   
   public Object putOrderBy(String value, int priority)
   {
      return(putOrderBy(_sort_prefix+String.valueOf(priority), value));
   }
   
   public Object putOrderByDirection(String value, int priority)
   {
      return(putOrderByDirection(_order_prefix+String.valueOf(priority), value));
   }
   
   public Object putOrderBy(String value, String direction, int priority)
   {
      return(putOrderBy(_sort_prefix+String.valueOf(priority), value, direction));
   }
   
   public Object putOrderBy(String name, Object value)
   {
      return(putOrderBy(name, value, null));
   }
   
   public Object putGroupBy(String name, Object value)
   {
      GroupByParameterNode groupbynode = null;
      Node node = getNode(name, Parameter.type_GroupByParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         groupbynode = (GroupByParameterNode)node;
         lastvalue = groupbynode.getValue();
         groupbynode.setValue(value);
      }
      else
      {
         groupbynode = new GroupByParameterNode(name, value);
         addNode(groupbynode);
      }
      return(lastvalue);
   }
   
   public Object putHaving(String name, Object value)
   {
      HavingParameterNode pnode = null;
      Node node = getNode(name, Parameter.type_HavingParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         pnode = (HavingParameterNode)node;
         lastvalue = pnode.getValue();
         pnode.setValue(value);
      }
      else
      {
         pnode = new HavingParameterNode(name, value);
         addNode(pnode);
      }
      return(lastvalue);
   }
   
   public Object putWhere(String name, Object value)
   {
      WhereParameterNode pnode = null;
      Node node = getNode(name, Parameter.type_WhereParameter);
      Object lastvalue = null;
      
      if(node != null)
      {
         pnode = (WhereParameterNode)node;
         lastvalue = pnode.getValue();
         pnode.setValue(value);
      }
      else
      {
         pnode = new WhereParameterNode(name, value);
         addNode(pnode);
      }
      return(lastvalue);
   }
   
/*
        public void setUsePreparedStatement(boolean b)
        {
        usePreparedStatement = b;
        }
 
        public boolean getUsePreparedStatement()
        {
                return(usePreparedStatement);
    }
 */
   public void setUseTokens(boolean b)
   {
      usetokens = b;
   }
   
   public boolean getUseTokens()
   {
      return(usetokens);
   }
   
   
   public String toString()
   {
      return(this.toString(true,true));
   }
   
   public String toString(boolean showall)
   {
      return(this.toString(showall,true));
   }
   
   public String toString(boolean showall, boolean showtokens)
   {
      StringBuffer st = new StringBuffer();
      Node head = this.headNode;
      ParameterNode pnode;
      int nodetype = -1;
      while(head != null)
      {
         nodetype = head.getNodeType();
         if(nodetype >= Parameter.type_MinParameter && nodetype < Parameter.type_MaxParameter)
         {
	         if(showtokens && nodetype != Parameter.type_RequestParameter)
	         {
	            pnode = (ParameterNode)head;
	            if(showall || !pnode.isEmpty())
	            {
	               pnode.appendQuery(st);
	            }
	         }
	         else if(nodetype >= Parameter.type_RequestParameter && nodetype < Parameter.type_MaxSearchParameter)
	         {
	            pnode = (ParameterNode)head;
	            if(showall || !pnode.isEmpty())
	            {
	               pnode.appendQuery(st);
	            }
	         }
         }
         head = head.getNextNode();
      }
      return(st.toString());
   }
   
   public String toHiddenFormInputs(boolean outputempty)
   {   
      StringBuffer st = new StringBuffer();
      Node head = this.headNode;
      ParameterNode pnode;
      while(head != null)
      {
         if(head instanceof ParameterNode)
         {
            pnode = (ParameterNode)head;
            if(!pnode.isEmpty() || outputempty)
            {
               pnode.appendHiddenInput(st);
            }
         }
         head = head.getNextNode();
      }
      return(st.toString());
   }
   
   public String toHiddenFormInputs()
   {
      return(toHiddenFormInputs(true));
   }
   
	public void putAll(Map t)
 	{
		if (t != null && !t.isEmpty()) {
			for(Object key: t.keySet()) {
				if (key != null) put(key.toString(), t.get(key));
			}
		}
 	}
}