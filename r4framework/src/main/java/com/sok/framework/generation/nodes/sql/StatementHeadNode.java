package com.sok.framework.generation.nodes.sql;

import java.sql.PreparedStatement;

import com.sok.framework.generation.nodes.*;

public abstract class StatementHeadNode extends AbstractSetterGeneratorSetNode
{
	FromSetNode fromsetnode;
	
	public StatementHeadNode(String name)
	{
		super(name);
	}		

	public void setFromSetNode(FromSetNode fromsetnode)
	{
		this.fromsetnode = fromsetnode;		
	}	
	
	public boolean hasJoinWith(String relationname)
	{
        if(fromsetnode!=null)
        {
            return(fromsetnode.hasJoin(relationname));
        }
        return(false);
	}	
	
	public boolean hasFromSet()
	{
		return(fromsetnode!=null);
	}
    
    public int getNodeType()
    {
        return(NodeType.type_StatementHead);
    }  	
    
}