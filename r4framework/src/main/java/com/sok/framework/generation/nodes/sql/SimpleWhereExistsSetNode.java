package com.sok.framework.generation.nodes.sql;

import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.nodes.sql.join.JoinOnSetNode;
import com.sok.framework.generation.nodes.sql.join.Joinable;
import com.sok.framework.sql.*;

public class SimpleWhereExistsSetNode extends WhereSetNode implements Joinable
{
   String fromtablename, totablename, fromfield, tofield, separator, vscript;

   boolean omit = false;

   JoinOnSetNode joinons = null;

   public SimpleWhereExistsSetNode(String name, String fromtablename,
         String totablename, String fromfield, String tofield,
         String separator, int priority)
   {
      super(name);
      this.fromtablename = fromtablename;
      this.totablename = totablename;
      this.fromfield = fromfield;
      this.tofield = tofield;
      this.separator = separator;
      this.priority = priority;
      //System.out.println("SimpleWhereExistsSetNode "+name);
   }

   public void setJoinOnSetNode(JoinOnSetNode joinons)
   {
      this.joinons = joinons;
   }

   public void setScript(String vscript)
   {
      this.vscript = vscript;
   }

   public String getScript()
   {
      return(vscript);
   }

   public void setOmit(boolean omit)
   {
      this.omit = omit;
   }

   protected void appendOut(RootNode root, StringBuffer buffer)
   {
      SqlDatabase database = root.getDatabase();
      buffer.append(rn);
      if(omit)
      {
         buffer.append(sql_not);
      }
      buffer.append(sql_exists);
      buffer.append(sql_bracketbegin);
      buffer.append(sql_select);
      database.appendWithQuote(buffer, RelationSpec.escapeRelationName(name));
      buffer.append(sql_dot);
      database.appendWithQuote(buffer, tofield);
      buffer.append(sql_from);
      database.appendWithQuote(buffer, totablename);
      buffer.append(sql_as);
      database.appendWithQuote(buffer, RelationSpec.escapeRelationName(name));
      buffer.append(sql_where);
      if(name.length()!=0 && fromfield.length()!=0)
      {
         database.appendWithQuote(buffer, RelationSpec.escapeRelationName(name));
         buffer.append(sql_dot);
         database.appendWithQuote(buffer, tofield);
         buffer.append(sql_equals);
         database.appendWithQuote(buffer, RelationSpec
               .escapeRelationName(fromtablename));
         buffer.append(sql_dot);
         database.appendWithQuote(buffer, fromfield);
      }
      if(joinons != null)
      {
         joinons.generate(root, buffer);
         //buffer.append(String.valueOf(joinons.size()));
      }
      if(!isEmpty() && name.length()!=0 && fromfield.length()!=0)
      {
         buffer.append(sql_and);
      }
      buffer.append(sql_bracketbegin);
   }

   protected void appendEnd(RootNode root, StringBuffer buffer)
   {
	  buffer.append(sql_bracketend);
      if(vscript != null)
      {
         root.getDatabase().appendConditions(root, vscript, buffer);
         //root.getDatabase().appendConditions(vscript, buffer);
         //buffer.append("/* script */");
      }
      buffer.append(sql_bracketend);
   }

   protected void appendSeparator(RootNode root, StringBuffer buffer)
   {
      if(separator == null)
      {
         buffer.append(sql_and);
      }
      else
      {
         buffer.append(separator);
      }
   }

   public int getNodeType()
   {
      return(NodeType.type_SimpleWhereExistsSet);
   }
}