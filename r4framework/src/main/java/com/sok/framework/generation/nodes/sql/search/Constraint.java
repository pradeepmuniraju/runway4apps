package com.sok.framework.generation.nodes.sql.search;

import com.sok.framework.generation.nodes.RootNode;
import com.sok.framework.generation.nodes.sql.WhereNode;

public interface Constraint
{
    public abstract void appendConstraint(RootNode root, WhereNode node, StringBuffer out, String value);
}