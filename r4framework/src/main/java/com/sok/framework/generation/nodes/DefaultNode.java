package com.sok.framework.generation.nodes;

public class DefaultNode implements Node
{
	protected String name = null;

	protected Node prevNode;
	protected Node nextNode;	
	
	public DefaultNode(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return(name);
	}

	public void setName(String name)
	{
		this.name = name;
	}	
	
	public void setNextNode(Node node)
	{
		this.nextNode = node;
	}

	public void setPrevNode(Node node)
	{
		this.prevNode = node;
	}	
	
	public Node getNextNode()
	{
		return(nextNode);
	}
	
	public Node getPrevNode()
	{
		return(prevNode);
	}
	
	public boolean hasNextNode()
	{
		return(nextNode!=null);
	}
	
	public boolean hasPrevNode()
	{
		return(prevNode!=null);
	}
	
	public String toString()
	{
		return(name);
	}
	
	public boolean hasChildren()
	{
		return(false);
	}	
	
	public Node find(String name)
	{
		if(this.name.equals(name))
		{
			return(this);
		}
		return(null);
	}
    
    public int getNodeType()
    {
        return(type_Node);
    }    
}