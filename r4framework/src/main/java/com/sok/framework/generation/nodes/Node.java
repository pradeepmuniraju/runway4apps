package com.sok.framework.generation.nodes;

public interface Node
{       
    public static final int type_Node = 1000;  
    public static final int type_Leaf = 2000;      
    public static final int type_Branch = 3000; 
    
    public static final int type_DataNode = 1100;  
    public static final int type_ParameterNode = 1200; 
    
    public static final int type_maxLeaf = 2999;     
    public static final int type_minLeaf = 1100; 
    
	public String getName();

	public void setName(String name);
	
	public void setNextNode(Node node);

	public void setPrevNode(Node node);

	public Node getNextNode();

	public Node getPrevNode();

	public boolean hasNextNode();

	public boolean hasPrevNode();
	
	public String toString();
	
	public boolean hasChildren();
	
	public Node find(String name);
    
    public int getNodeType();    
}