package com.sok.framework.generation;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

public interface RowData extends TableSource, MapData
{
    public void setAction(String s);

    public void doAction();
    
    public void doAction(String s);
    
    public String getAction(); 

    public void setJndiName(String t);
    
    public void processRequest(HttpServletRequest request);

    public void parseRequest(HttpServletRequest request);

    public void setRequest(HttpServletRequest request);   
    
    public void parseRequest(HttpServletRequest request, String prefix);
    
    public void parseRequest(String query); 
    
    public void setUpdatedCount(int c);

    public int getUpdatedCount();
    
    public void setLocale(Locale l);

    public void setTimeZone(String s);

    public Locale getLocale();

    public TimeZone getTimeZone();
    
    public void setUserTimeZone(String s);
    
    public TimeZone getUserTimeZoneObject();
    
    public void close();
    
    public String getError(); 
    
    public String getStatement();    
    
}