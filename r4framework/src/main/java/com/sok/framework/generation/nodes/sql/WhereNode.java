package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.sql.search.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.*;
import com.sok.framework.sql.*;

public class WhereNode extends AbstractGeneratorNode implements PrioritisedNode
{
   protected Object value;
   protected String tablename;
   
   protected int datatype = -1;
   protected String inputformat = null;
   protected boolean isrealtablename = true;
   
   protected String vscript;
   protected boolean omit = false;
   protected boolean flipop = false;
   
   protected int priority = 0;      
   
   protected String rawname = null;
   
   public WhereNode(String name, Object value, String tablename, int datatype)
   {
      super(name);
      this.rawname = name;
      this.tablename = tablename;
      this.datatype = datatype;
      setValue(value);
   }
   
   public WhereNode(String name)
   {
      super(name);
      this.rawname = name;
   }
   
   public String getRawName()
   {
       return(rawname);
   }       
   
   public int getPriority()
   {
       return(priority);
   }    
   
   public boolean isRealTableName()
   {
      return(isrealtablename);
   }
   
   public void setIsRealTableName(boolean isrealtablename)
   {
      this.isrealtablename = isrealtablename;
   }
   
   public void setDataType(int datatype)
   {
      this.datatype = datatype;
   }
   
   public int getDataType()
   {
      return(datatype);
   }
   
   public void setInputFormat(String format)
   {
      this.inputformat = format;
   }
   
   public String getInputFormat()
   {
      return(inputformat);
   }
   
   public void setScript(String vscript)
   {
      this.vscript = vscript;
   }
   
   public String getScript()
   {
      return(vscript);
   }
   
   public void setTableName(String tablename)
   {
      this.tablename = tablename;
   }
   
   public String getTableName()
   {
      return(tablename);
   }
   
   public void setValue(Object value)
   {
      this.value = value;
      if(value!=null && value.toString().indexOf('+')>0)
      {
         priority++;
      }      
   }
   
   public Object getValue()
   {
      return(value);
   }
   
   public void toggleOmit()
   {
      omit = !omit;
   }
   
   public String getOmitAltSearch()
   {
      //System.out.println("getOmitAltSearch:"+ rawname);
      
      StringBuffer sb = new StringBuffer();
      int dotindex = rawname.indexOf('.');
      int orindex = rawname.indexOf('|');
      int andindex = rawname.indexOf('^');
      if(dotindex>0)
      {
         sb.append(rawname.substring(0, dotindex));
         if(orindex>0 || andindex>0)
         {
            if(orindex > 0 && (orindex < andindex || andindex < 0))
            {
               sb.append(rawname.substring(orindex, rawname.length()));
            }
            else if(andindex > 0 && (andindex < orindex || orindex < 0))
            {
               sb.append(rawname.substring(andindex, rawname.length()));
            }
            //System.out.println("getOmitAltSearch 2: "+ sb.toString());
         }
      }
      else
      {
         sb.append(rawname);
      }
      return(sb.toString());
   }
   
   public void setOmit(boolean b)
   {
      omit = b;
   }
   
   public boolean isOmit()
   {
      return(omit);
   }
   
   public boolean isFlipOp()
   {
      return(flipop);
   }
   
   public void setFlipOp(boolean b)
   {
      flipop = b;
   }
   
   public String getFullName()
   {
      if(tablename!=null && tablename.length()!=0)
      {
         return(new StringBuffer(RelationSpec.escapeRelationName(tablename)).append('.').append(name).toString());
      }
      return(name);
   }
   
   public void appendFullName(StringBuffer buffer, SqlDatabase database)
   {
      if(tablename!=null && tablename.length()!=0)
      {
         database.appendWithQuote(buffer,RelationSpec.escapeRelationName(tablename));
         buffer.append('.');
      }
      database.appendWithQuote(buffer,name);
   }
   
   public void generate(RootNode node, StringBuffer out)
   {
      if(value instanceof String[])
      {
         String[] values =  (String[])value;
         out.append(sql_bracketbegin);
         generate(node, out, values);
         out.append(sql_bracketend);
      }
      else
      {
         String valuestr = value.toString();
         generate(node, out, valuestr);
      }
      if(vscript!=null)
      {
         node.getDatabase().appendConditions(vscript, out);
      }
   }
   
   protected void generate(RootNode node, StringBuffer out, String[] values)
   {
       boolean notfirst = false;
       for(int i=0; i<values.length; i++)
       {
          if(values[i]!=null && values[i].length()!=0)
          {	   
		       if(notfirst)
		       {
		          if(isFlipOp())
		          {
		             out.append(sql_and);
		          }
		          else
		          {
		             out.append(sql_or);
		          }
		       }

		       generate(node, out, values[i]);
		       notfirst = true;
          }
       }       
   }
   
   public void generate(RootNode node, StringBuffer out, String value)
   {
      Constraint constraint = ConstraintFactory.getConstraint(this);
      constraint.appendConstraint(node, this, out, value);
   }
   
   public int getNodeType()
   {
      return(NodeType.type_Where);
   }
   
   public boolean hasOutPut()
   {
      if(name!=null && name.length()!=0)
      {
         return(true);
      }
      return(false);
   }
}