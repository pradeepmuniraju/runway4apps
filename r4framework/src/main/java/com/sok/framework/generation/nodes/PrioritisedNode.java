package com.sok.framework.generation.nodes;

public interface PrioritisedNode extends Node
{
	public int getPriority();
}