package com.sok.framework.generation.util;
import com.sok.framework.*;
public class TableDataHistory extends History
{   
   public TableDataHistory(int max)
   {
      super(max);
   }

   public TableDataHistory(int maxsize, boolean uniquenames, boolean ignorecase, boolean keeporder)
   {
      super(maxsize, uniquenames, ignorecase, keeporder);
   }
   
   public String appendTableData(TableData data)
   {
      String key = KeyMaker.generate();
      append(key, data);
      return(key);
   }
   
   public TableData getTableData(String key)
   {
      return((TableData)getValue(key));
   }
   
   public TableData getTableData(int i)
   {
      return((TableData)getValue(i));
   }   

   public TableData getTableData()
   {
      return((TableData)getValue());
   }   
}