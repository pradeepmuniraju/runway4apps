package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;

public class WhereGroupSetNode extends WhereSetNode
{
   protected String op = sql_and;

   public WhereGroupSetNode(String name)
   {
      super(name);
      super.priority++;
      
      //System.out.println("Group: "+name + " default op: "+ op);
   }

   public WhereGroupSetNode(String name, String op)
   {
      super(name);
      this.op = op;
      super.priority++;
      
      //System.out.println("Group: "+name + " op: "+ op);
   }

   public String getSeparator()
   {
      return(op);
   }

   public void setSeparator(String op)
   {
      this.op = op;
   }

   protected void appendOut(RootNode root, StringBuffer buffer)
   {
      buffer.append(sql_bracketbegin);
   }

   protected void appendEnd(RootNode root, StringBuffer buffer)
   {
      buffer.append(sql_bracketend);
   }

   protected void appendSeparator(RootNode root, StringBuffer buffer)
   {
      buffer.append(op);
      //buffer.append("<!-- "+ name +" -->");
   }

   public int getNodeType()
   {
      return(NodeType.type_WhereGroupSet);
   }
}