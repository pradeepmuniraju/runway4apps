package com.sok.framework.generation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Map;

import javax.servlet.ServletRequest;

import com.sok.framework.generation.nodes.*;
import com.sok.framework.*;
import com.sok.runway.UserBean;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
public class Row extends RequestNode implements RowData
{
    protected boolean closeconnection = true;
    protected Connection con = null;
    protected Locale thislocale = null;
    protected TimeZone usertimezone = null;
    protected TimeZone thistimezone = null;
    protected String statement = null;
    protected String jndiname = null;
    protected boolean success = false;
    protected MetaData[] metadata = null; //Temporary storage
    
    public Row()
    {
        super(null);
    }
    
    public boolean isSuccessful()
    {
        return(success);
    }
    
    public void setSuccess(boolean success)
    {
        this.success = success;
    }
    
    public void setRequest(HttpServletRequest request)
    {
    	if (request != null) {
			ActionBean.setRequest(request, this);
	
			UserBean currentUser = (UserBean) request.getSession().getAttribute("currentuser");
			if (currentUser != null && StringUtils.isNotBlank(currentUser.getString("TimeZone"))) {
				setUserTimeZone(currentUser.getString("TimeZone"));
			}
    	}
	}
    
    public void processRequest(HttpServletRequest request)
    {
        this.processRequest(request, null);
    }
    
    public void processRequest(HttpServletRequest request, String prefix)
    {
        ActionBean.setRequest(request, this);
        DatabaseFactory.processRequest(request, this, prefix);
    }
    
    public void parseRequest(HttpServletRequest request)
    {
        this.parseRequest(request, null);
    }    
    
    public void parseRequest(HttpServletRequest request, String prefix)
    {
        ActionBean.setRequest(request, this);
        DatabaseFactory.parseRequest(request, this, prefix);
    }    
    
    public void parseRequest(String query)
    {
        this.parseRequest(query, null);
    }    
    
    public void parseRequest(String query, String prefix)
    {
        DatabaseFactory.parseRequest(query, this, prefix);
    }     
    
    public void parseRequest(Map map)
    {
        DatabaseFactory.parseRequest(map, this, null);
    }    
    
    public void doAction(String action)
    {
        setAction(action);
        doAction();
    }
    
    public void doAction()
    {
        DatabaseFactory.doAction(this);
    }     
    
    public void setAction(String action)
    {
        putToken(_action, action);
    }
    
    public String getAction()
    {
        return(getString(_action, Parameter.type_TokenParameter));
    }
    
    public void connect()
    {
        ActionBean.connect(this);
    }
    
    public void setJndiName(String t)
    {
        jndiname = t;
        putToken(_jndiname, t);
    }
    
    public String getStatement() {
    	return getStatement(false);
    }
    
    public String getStatement(boolean html)
    {
        if(statement == null && this.statementnode!=null)
        {   
            statement = generate();
        }
        return html ? StringUtil.replace(statement, "\n", "<br/>\n") : (statement);
    }
    
    public void setStatement(String s)
    {
        statement = s;
    }      
    
    public String getJndiName()
    {
        if(jndiname == null)
        {
            Node node = getNode(_jndiname, Parameter.type_TokenParameter);
            if(node != null)
            {
                jndiname = ((TokenParameterNode)node).getValue().toString();
            }
        }
        return(jndiname);
    }
    
    public void setCloseConnection(boolean b)
    {
        closeconnection = b;
    }

    public boolean getCloseConnection()
    {
        return(closeconnection);
    }

    public void setConnection(Object con)
    {
        setConnection((Connection)con);
    }

    public void setConnection(Connection con, boolean close)
    {
    	if (con != null) {
	        this.con = con;
	        closeconnection = close;
    	}
    }

    public void setConnection(Connection con)
    {
    	if (con == null) 
    		setConnection(con, true);
    	else
    		setConnection(con, false);
    }
    public void setConnection(ServletRequest request)
    {
        Connection conn = ActionBean.getConnection(request);
        if(conn!=null)
        {
            con = conn;
            closeconnection = false;
        } 
    }
    
    public Connection getConnection()
    {
        return(con);
    }
    
    public void setLocale(Locale l)
    {
        thislocale = l;
    }
    
    public Locale getLocale()
    {
        return(thislocale);
    }    
    
    public TimeZone getTimeZone()
    {
		if (thistimezone == null) {
			
			if (StringUtils.isNotBlank(InitServlet.getSystemParam("ServerTimeZone")))
				thistimezone = TimeZone.getTimeZone(InitServlet.getSystemParam("ServerTimeZone"));
			else
				thistimezone = TimeZone.getTimeZone(ActionBean.defaulttimezone);
		}
		
		return (thistimezone);
	}

    public void setTimeZone(String zonestring)
    {
        thistimezone = TimeZone.getTimeZone(zonestring);
    }

    public void setTimeZone(TimeZone zone)
    {
        thistimezone = zone;
    }
    
    
    public String getUserTimeZone()
    {		
		return getUserTimeZoneObject().getDisplayName();
	}
    
    public TimeZone getUserTimeZoneObject()
    {
		if (usertimezone == null) {
			usertimezone = TimeZone.getTimeZone(RunwayUtil.getGlobalTimeZone());
		}

		return (usertimezone);
	}
    
    public void setUserTimeZone(String zonestring)
    {
        usertimezone = TimeZone.getTimeZone(zonestring);
    }

    public void setUserTimeZone(TimeZone zone)
    {
    	usertimezone = zone;
    }    
    
    public void setError(String s)
    {
        appendError(s);
    }
    
    public String getError()
    {
        return(getErrors());
    }
    
    public DataNode getDataNode(String name)
    {
        return((DataNode)getNode(name, type_DataNode));
    }
    
    public Object putData(String name, Object value, String type, String format)
    {
        DataNode dnode = null;
        Node node = getNode(name, type_DataNode);
        Object lastvalue = null;
        
        if(node != null)
        {
            dnode = (DataNode)node;
            lastvalue = dnode.getValue();
            dnode.setValue(value);
            dnode.setFormat(type, format);
        }
        else
        {
            dnode = new DataNode(name, value, type, format);
            addNode(dnode);
        }
        return(lastvalue);
    }    
    
    protected String[] getNames(int type)
    {
       ArrayList<String> list = new ArrayList<String>();
       Node head = getHeadNode();
       for(int i=0; i<size() && head!=null; i++)
       {
          if(head.getNodeType() == type)
          {
             list.add(head.getName());
          }
          head = head.getNextNode();
       }
       return(list.toArray(new String[list.size()]));
    }         
    
    public void close()
    {
        metadata = null;
        if(con!=null)
        {
            if(closeconnection)
            {
                try{
                    con.close();
                }catch(Exception e){}
            }
            con = null;
        }
    }
    
    public void setStatementValues(PreparedStatement updatestm)
    {
        statementnode.setStatementValues(updatestm, this, 1);
    }
    
    public void clearStatement()
    {
        statement = null;
    }
    
    public void clear()
    {
        statement = null;
        metadata = null;
        this.success = false;
        super.clear();
        if(jndiname!=null)
        {
        	putToken(_jndiname, jndiname);
        }
    }
}