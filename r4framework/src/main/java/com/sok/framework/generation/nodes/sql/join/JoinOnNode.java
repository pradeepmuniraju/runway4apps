package com.sok.framework.generation.nodes.sql.join;
import com.sok.framework.generation.nodes.sql.*;
import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.AbstractGeneratorNode;
import com.sok.framework.generation.nodes.RootNode;

public abstract class JoinOnNode extends AbstractGeneratorNode
{
   public JoinOnNode(String name)
   {
      super(name);
   }
   
   public int getNodeType()
   {
      return(NodeType.type_JoinOn);
   }
}