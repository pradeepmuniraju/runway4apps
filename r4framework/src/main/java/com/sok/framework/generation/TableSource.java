package com.sok.framework.generation;

import java.sql.*;
import javax.servlet.*;

public interface TableSource
{
	public void setCloseConnection(boolean b);

	public boolean getCloseConnection();

	public void setConnection(Object con);

	public void setConnection(Connection con);

	public void setConnection(ServletRequest request);	
	
	public Connection getConnection();
    
    public String getJndiName();  
    
    public void setError(String s);    
	
}