/*
 * WhereSubSelectNode.java
 *
 * Created on March 20, 2007, 5:24 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.sok.framework.generation.nodes.sql;
import com.sok.framework.generation.nodes.*;
/**
 *
 * @author root
 */
public class WhereSubSelectNode extends WhereNode
{
   protected SubSelectSetNode subselectsetnode = null;
   
   public WhereSubSelectNode(WhereNode node)
   {
      super(node.getName(), node.getValue(), null, node.getDataType());
   }
   
   public WhereSubSelectNode(String name, Object value, String tablename, int datatype)
   {
      super(name, value, tablename, datatype);
      //System.out.println("WhereSubSelectNode "+name);
   }
   
   public void setSubSelectSetNode(SubSelectSetNode node)
   {
      subselectsetnode = node;
   }
   
   public void generate(RootNode node, StringBuffer out, String value)
   {
      StringBuffer name = new StringBuffer();
      subselectsetnode.generate(node, name);
      this.name = name.toString();
      super.generate(node, out, value);
   }
}
