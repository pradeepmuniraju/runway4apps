package com.sok.framework.generation.util;
import com.sok.framework.*;
public class HistorySet extends History
{   
   public HistorySet(int max)
   {
      super(max);
   }

   public HistorySet(int maxsize, boolean uniquenames, boolean ignorecase, boolean keeporder)
   {
      super(maxsize, uniquenames, ignorecase, keeporder);
   }
   
   public String appendHistory(History history)
   {
      String key = KeyMaker.generate();
      append(key, history);
      return(key);
   }
   
   public String appendHistory()
   {
      return(appendHistory(new History(this.maxSize, this.uniqueNames, this.ignoreCase, this.keepOrder)));
   }   
   
   public History getHistory(String key)
   {
      return((History)getValue(key));
   }

   public History getHistory()
   {
      return((History)getValue());
   }   
}