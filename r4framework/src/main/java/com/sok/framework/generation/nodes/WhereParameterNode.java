package com.sok.framework.generation.nodes;

import com.sok.framework.generation.*;

public class WhereParameterNode extends AbstractParameterNode implements GenerationKeys
{
    String fieldname;
    
    public WhereParameterNode(String name, Object value)
    {
        super(name, value);
        initName();
    }
    
    public int getNodeType()
    {
        return(type_WhereParameter);
    }    
    
    protected void initName()
    {
        fieldname = name;
        if(fieldname.indexOf(UPDATEON) == 0)
        {
            fieldname = fieldname.substring(UPDATEON.length(), fieldname.length());
        }
    }
    
    public void setName(String name)
    {
        super.setName(name);
        initName();
    }

    public String getFieldName()
    {
        return(fieldname);
    }
    
}