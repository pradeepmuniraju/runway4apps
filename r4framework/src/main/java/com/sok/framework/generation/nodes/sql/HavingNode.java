package com.sok.framework.generation.nodes.sql;

import com.sok.framework.generation.nodes.*;

public class HavingNode extends AbstractGeneratorNode implements PrioritisedNode
{
    int num = 0;
    
    public HavingNode(String condition, int num)
    {
        super(condition);
        this.num = num;
    }
    
    public String getCondition()
    {
        return(name);
    }
    
    public int getPriority()
    {
        return(num);
    }       
    
    public void generate(RootNode root, StringBuffer out)
    {
        out.append(name);
    }
    
    public int getNodeType()
    {
        return(NodeType.type_Having);
    }    
}