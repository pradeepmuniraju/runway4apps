package com.sok.framework.generation.nodes;

import com.sok.framework.generation.*;

public class OrderByParameterNode extends AbstractParameterNode implements GenerationKeys
{
	int priority = 0;
	String fieldname;
	String tablename;
	String direction;
	boolean isscript = false;
	
	public OrderByParameterNode(String name, Object value)
	{
		super(name, value);
		initName();
		initValue();
	} 
    
    public OrderByParameterNode(String name, Object value, String direction)
    {
        super(name, value);
        initName();
        initValue();
        this.direction = direction;
    }    
    
    public int getNodeType()
    {
        return(type_OrderByParameter);
    }    
    
    public boolean isScript()
    {
    	return(isscript);
    }
    
	protected void initValue()
	{
		if(value!=null)
		{
			fieldname = value.toString();
			isscript = fieldname.indexOf('$')>=0;
			if(!isscript)
			{
				int index = fieldname.indexOf('.');
				if(index>0)
				{
					tablename = fieldname.substring(0,index);
					fieldname = fieldname.substring(index+1,fieldname.length());
				}
				else
				{
				   tablename = null;
				}
			}
		}
	}
	
	protected void initName()
	{
		try{
			priority = Integer.parseInt(name.substring(_sort_prefix.length(), name.length()));
		}catch(Exception e){
		    throw new IllegalConfigurationException("number missing in sort parameter", e);
        }
	}
	
	public void setName(String name)
	{
		super.setName(name);
		initName();
	}
	
	public Object setValue(Object value)
	{
		Object obj = super.setValue(value);
		initValue();
		return(obj);
	}	
	
	public String getFieldName()
	{
		return(fieldname);
	}
	
	public String getTableName()
	{
		return(tablename);
	}	

	public void setTableName(String tablename)
	{
		this.tablename = tablename;
	}		
	
	public String getDirection()
	{
		return(direction);
	}	
	
	public void setDirection(String direction)
	{
		this.direction = direction;
	}	
	
	public int getPriority()
	{
		return(priority);
	}
}