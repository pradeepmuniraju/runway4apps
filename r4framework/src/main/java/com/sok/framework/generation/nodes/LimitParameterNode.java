package com.sok.framework.generation.nodes;

import com.sok.framework.generation.*;

public class LimitParameterNode extends AbstractParameterNode implements GenerationKeys
{
    int limit = 0;

    public LimitParameterNode(Object value)
    {
        super(_limit_results, value);
        try{
            limit = Integer.parseInt(value.toString());
        }catch(Exception e){}
    }  
    
    public int getNodeType()
    {
        return(type_LimitParameter);
    }       
    
    public int getIntValue()
    {
        return(limit);
    }

    @Override
    public Object setValue(Object value)
    {
    	Object oldvalue = this.value;
    	this.value = value;
        try{
            limit = Integer.parseInt(value.toString());
        }catch(Exception e){}
        return(oldvalue);
    }
}