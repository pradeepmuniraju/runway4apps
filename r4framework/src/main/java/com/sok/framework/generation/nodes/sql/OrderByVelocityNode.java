package com.sok.framework.generation.nodes.sql;
import com.sok.framework.ActionBean;
import com.sok.framework.sql.VelocityDatabase;
import com.sok.framework.RelationSpec;
import com.sok.framework.generation.nodes.RootNode;

public class OrderByVelocityNode extends OrderByNode
{
	public OrderByVelocityNode(String fieldname, String order, int num)
	{
		super(null, fieldname, order, num);
	}
	
	public void generate(RootNode root, StringBuffer out)
	{
		root.useVelocityDatabase();
		out.append(ActionBean.getSubstituted(this.name, root, "orderby"));
		if(order != null)
		{
			out.append(space);
			out.append(order);
		}
	}
}