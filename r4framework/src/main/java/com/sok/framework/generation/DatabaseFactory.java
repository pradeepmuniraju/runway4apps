package com.sok.framework.generation;

import com.sok.framework.*;
import com.sok.framework.generation.nodes.Parameter;
import com.sok.framework.generation.nodes.ParameterNode;
import com.sok.framework.generation.nodes.RequestNode;
import com.sok.framework.generation.nodes.sql.SetterNode;
import com.sok.framework.generation.util.RelatedFieldName;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.util.*;

/**
 * TODO: Clean up code, remove dead commented code
 */
public class DatabaseFactory implements GenerationKeys
{
    public static void processRequest(HttpServletRequest request, Row node, String prefix)
    {
        parseRequest(request, node, prefix);
        doAction(node);
    }

    public static void doAction(Row node)
    {
        node.setSuccess(false);
        GeneratorFactory.setConfiguration(node);

        String action = node.getString(_action);
        if (action.equals(SEARCH))
        {
            node.setStatement(null);
            //don't auto get row on search action
            node.getStatement();
        }
        else if (action.equals(SELECT) || action.equals(SELECTFIRST))
        {
            node.setStatement(null);
            executeSelectStatement(node);
        }
        else if (action.indexOf(UPDATE) == 0)
        {
            node.setStatement(null);
            executeUpdateStatement(node);
        }
        else if (action.indexOf(DELETE) == 0)
        {
            node.setStatement(null);
            executeUpdateStatement(node);
        }
        else if (action.indexOf(INSERT) == 0)
        {
            node.setStatement(null);
            executeUpdateStatement(node);
        }
        else if (action.indexOf(EXECUTE) == 0)
        {
            executeUpdateStatement(node, false);
        }
        else
        {
            throw new IllegalConfigurationException("No action specified");
        }
    }

    public static void executeUpdateStatement(Row row)
    {
        executeUpdateStatement(row, true);
    }

    public static void executeUpdateStatement(Row row, boolean setvalues)
    {
        row.setUpdatedCount(0);
        Connection con;
        PreparedStatement updatestm = null;

        try
        {
            con = row.getConnection();
            if (con == null)
            {
                con = ActionBean.connect(row);
            }

            updatestm = con.prepareStatement(row.getStatement());
            if (setvalues)
            {
                row.setStatementValues(updatestm);
            }
            int r = updatestm.executeUpdate();
            row.setUpdatedCount(r);
            if (r > 0)
            {
                row.setSuccess(true);
            }

        }
        catch (Exception ex)
        {
            throw new DatabaseException(ex, updatestm.toString());
        }
        finally
        {
            if (updatestm != null)
            {
                try
                {
                    updatestm.close();
                }
                catch (SQLException ex)
                {
                    ex.printStackTrace();
                }
            }
            if (row.getCloseConnection())
            {
                row.close();
            }
        }
    }

    public static int setValue(PreparedStatement updatestm, SetterNode node, Row root, int index) throws SQLException
    {
        Object value = node.getValue();
        if (value == null)
        {
            index = setNullValue(updatestm, node, root, index);
        }
        if (value instanceof String)
        {
            index = setStringValue(updatestm, node, root, index);
        }
        else if (value instanceof String[])
        {
            index = setStringArrayValue(updatestm, node, root, index);
        }
        else if (value instanceof java.util.Date)
        {
            index = setDateValue(updatestm, node, root, index);
        }
        else if (value instanceof Integer)
        {
            index = setNumberValue(updatestm, node, root, index);
        }
        else if (value instanceof Double)
        {
            index = setNumberValue(updatestm, node, root, index);
        }
        else if (value instanceof Long)
        {
            index = setNumberValue(updatestm, node, root, index);
        }
        else if (value instanceof Float)
        {
            index = setNumberValue(updatestm, node, root, index);
        }
        else if (value instanceof Boolean)
        {
            index = setNumberValue(updatestm, node, root, index);
        }
        return (index);
    }

    protected static int setNullValue(PreparedStatement updatestm, SetterNode node, Row root, int index) throws SQLException
    {
        updatestm.setNull(index, node.getDataType());
        index++;
        return (index);
    }

    protected static int setNumberValue(PreparedStatement updatestm, SetterNode node, Row root, int index) throws SQLException
    {
        updatestm.setString(index, node.getValue().toString());
        index++;
        return (index);
    }

    protected static int setDateValue(PreparedStatement updatestm, SetterNode node, Row root, int index) throws SQLException
    {
        int fieldtype = node.getDataType();
        java.util.Date tsd = (java.util.Date) node.getValue();

        if (fieldtype == Types.TIMESTAMP)
        {
            Timestamp ts = new Timestamp(tsd.getTime());
            updatestm.setTimestamp(index, ts);
        }
        else
        {
            String valuestr = ActionBean.formatValue(node.getName(), node.getFormat(),
                    null, node.getValue(),
                    root.getLocale(), root.getTimeZone());
            updatestm.setString(index, valuestr);
        }

        index++;
        return (index);
    }


    protected static int setBooleanValue(PreparedStatement updatestm, SetterNode node, Row root, int index) throws SQLException
    {
        int fieldtype = node.getDataType();
        Object value = node.getValue();
        String valuestr = value.toString();

        if (Types.BOOLEAN == fieldtype)
        {
            updatestm.setObject(index, new Boolean(valuestr));
        }
        else
        {
            updatestm.setString(index, valuestr);
        }

        index++;
        return (index);
    }

    protected static int setStringArrayValue(PreparedStatement updatestm, SetterNode node, Row root, int index) throws SQLException
    {
        Object value = node.getValue();
        String valuestr = MultiField.compress((String[]) value);
        updatestm.setString(index, valuestr);
        index++;
        return (index);
    }

    protected static int setStringValue(PreparedStatement updatestm, SetterNode node, Row root, int index) throws SQLException
    {
        int fieldtype = node.getDataType();
        Object value = node.getValue();
        String valuestr = value.toString();
        if (fieldtype != Types.VARCHAR)
        {
            if (value == null || valuestr.length() == 0 || valuestr.equals(value_NULL)
                    || valuestr.equals(value_EMPTY))
            {
                updatestm.setNull(index, fieldtype);
            }
            else if (fieldtype == Types.TIMESTAMP)
            {
                java.util.Date tsd = ActionBean.getDate(valuestr, node.getFormat(), root.getLocale(),
                        root.getTimeZone());
                Timestamp ts = new Timestamp(tsd.getTime());
                updatestm.setTimestamp(index, ts);
            }
            else if (Types.BOOLEAN == fieldtype)
            {
                updatestm.setBoolean(index, new Boolean(valuestr).booleanValue());
            }
            else
            {
                updatestm.setString(index, valuestr);
            }
        }
        else
        {
            if (value == null || valuestr.length() == 0 || valuestr.equals(value_EMPTY))
            {
                updatestm.setString(index, _blank);
            }
            else if (valuestr.equals(value_CURRENTUSER))
            {
                updatestm.setString(index, root.getString(key_CURRENTUSERID));
            }
            else if (valuestr.equals(value_NULL))
            {
                updatestm.setNull(index, fieldtype);
            }
            else
            {
                updatestm.setString(index, valuestr);
            }
        }
        index++;
        return (index);
    }

    public static void executeSelectStatement(Row row)
    {
        Connection con;
        ResultSet current = null;
        Statement stmt = null;

        try
        {
            con = row.getConnection();
            if (con == null)
            {
                con = ActionBean.connect(row);
                row.setCloseConnection(true);
            }
            stmt = con.createStatement();
            
            if(SELECTFIRST.equals(row.getString(_action))) {
            	StringBuffer sb = new StringBuffer(row.getStatement());
                if(sb.indexOf("limit") == -1) {
                	row.getDatabase().appendTopStatement(sb, 1);
                }
                current = stmt.executeQuery(sb.toString());
            } else {
            	current = stmt.executeQuery(row.getStatement());
            }
            row.setSuccess(initResultSet(current, row));
        }
        catch (Exception ex)
        {
            throw new DatabaseException(ex, row.getStatement());
        }
        finally
        {
            if (stmt != null)
            {
                try
                {
                    stmt.close();
                }
                catch (SQLException ex)
                {
                    ex.printStackTrace();
                }
            }
            if (current != null)
            {
                try
                {
                    current.close();
                }
                catch (SQLException ex)
                {
                    ex.printStackTrace();
                }
            }
            try {
	            if (row.getCloseConnection())
	            {
	                row.close();
	            }
            } catch (Exception e) {
            }
        }
    }

    public static boolean initResultSet(ResultSet rs, Row row)
    {
        if (rs != null)
        {
            Object obj;

            try
            {
                if (rs.next())
                {
                    if (row.metadata == null)
                    {
                        row.metadata = getMetaData(rs, row);
                    }

                    for (int i = 0; i < row.metadata.length; i++)
                    {
                        obj = getResultObject(rs, i, row.metadata[i].type);
                        row.putData(row.metadata[i].name, obj, row.metadata[i].type, row.metadata[i].format);
                    }
                    return (true);
                }
            }
            catch (SQLException e)
            {
                throw new IllegalConfigurationException(e);
            }
        }

        return (false);
    }

    public static MetaData[] getMetaData(ResultSet rs, Row row) throws SQLException, IllegalConfigurationException
    {
        ViewSpec vspec = row.getViewSpec();
        ResultSetMetaData rsmeta = rs.getMetaData();
        int columncount = rsmeta.getColumnCount();
        MetaData[] metadata = new MetaData[columncount];

        for (int i = 0; i < columncount; i++)
        {
            metadata[i] = new MetaData();
            metadata[i].name = rsmeta.getColumnLabel(i + 1);
            if (vspec != null)
            {
                try
                {
                    metadata[i].type = vspec.getFormatTypeForName(metadata[i].name);
                    metadata[i].format = vspec.getFormatForName(metadata[i].name);
                }
                catch (Exception e)
                {
                    throw new IllegalConfigurationException(metadata[i].name + "column has no type or format in viewspec " + vspec.getName(), e);
                }
            }
            else
            {
                metadata[i].type = getTypeFromResultSetMeta(rsmeta.getColumnType(i + 1));
            }
        }

        return (metadata);
    }

    public static String getTypeFromResultSetMeta(int type)
    {
        if (type == Types.DATE)
        {
            return (TYPE_DATE);
        }
        else if (type == Types.TIME)
        {
            return (TYPE_TIME);
        }
        else if (type == Types.TIMESTAMP)
        {
            return (TYPE_DATETIME);
        }
        else if (type == Types.BIGINT)
        {
            return (TYPE_LONG);
        }
        else if(type == Types.INTEGER || type == Types.TINYINT) 
        {
        	return TYPE_NUMBER;
        }
        else if (type == Types.DOUBLE || type == Types.FLOAT || type == Types.DECIMAL)
        {
            return (TYPE_DECIMAL);
        }
        else if (type == Types.BOOLEAN)
        {
            return (TYPE_BOOLEAN);
        }
        return (null);
    }

    public static void retrieveResultSet(Rows row, boolean scrollSensitive)
    {
        String stmtstring = row.getStatement();
        Connection con = row.getConnection();
        ResultSet current = null;
        Statement stmt = null;
        try
        {
            if (con == null)
            {
                con = ActionBean.connect(row);
            }

            if (scrollSensitive)
            {
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
            }
            else
            {
                stmt = con.createStatement();
            }
            current = stmt.executeQuery(stmtstring);
            row.setStatement(stmt);
            row.setResultSet(current);
        }
        catch (Exception ex)
        {
            throw new DatabaseException(stmtstring, ex);
        }
    }

    static Object getResultObject(ResultSet rs, int i, String type)
    {
        Object value;
        try
        {
            if (type == null)
            {
                value = rs.getString(i + 1);
            }
            else if (type.equals(TYPE_BOOLEAN))
            {
                value = rs.getBoolean(i + 1);
            }
            else if (type.equals(TYPE_DATETIME))
            {
                value = rs.getTimestamp(i + 1);
            }
            else if (type.equals(TYPE_DATE))
            {
            	// this date format is bad for our system, so return it as a string
            	if ("0000-00-00 00:00:00".equals(rs.getString(i + 1))) value = null;
                value = rs.getDate(i + 1);
            }
            else if (type.equals(TYPE_TIME))
            {
                value = rs.getTime(i + 1);
            }
            else if (type.equals(TYPE_NUMBER))
            {
                value = rs.getInt(i + 1);
            }
            else if(type.equals(TYPE_LONG)) 
            {
            	value = rs.getLong(i + 1);
            }
            else if (type.equals(TYPE_PERCENT))
            {
                value = rs.getDouble(i + 1);
            }
            else if (type.equals(TYPE_CURRENCY))
            {
                value = rs.getDouble(i + 1);
            }
            else if (type.equals(TYPE_DECIMAL))
            {
                value = rs.getDouble(i + 1);
            }
            else
            {
                value = rs.getString(i + 1);
            }
        }
        catch (Exception e)
        {
            throw new IllegalConfigurationException("invalid type " + type + " specified for column " + String.valueOf(i + 1), e);
        }
        return (value);
    }

    public static void parseRequest(Map map, RequestNode node, String prefix)
    {
        if (map != null)
        {
            Iterator iter = map.keySet().iterator();

            String paramname = null;
            String key = null;
            while (iter.hasNext())
            {
                paramname = (String) iter.next();
                //TODO must what to to if we still want to do it this way
                if (prefix != null)
                {
                    if (paramname.startsWith(prefix + at))
                    {
                        key = paramname.substring(prefix.length() + at.length());
                    }
                    else
                    {
                        key = null;
                    }
                }
                else
                {
                    key = paramname;
                }

                if (key != null)
                {
                    if (map.get(paramname) instanceof Object[])
                    {
                        Object[] obj = (Object[]) map.get(paramname);
                        if (obj.length == 1)
                        {
                            node.put(key, obj[0]);
                        }
                        else
                        {
                            node.put(key, obj);
                        }
                    }
                    else
                    {
                        node.put(key, map.get(paramname));
                    }
                }
            }
        }
    }

    public static void parseRequest(String query, RequestNode node, String prefix)
    {
        if (query != null)
        {
            StringTokenizer iter = new StringTokenizer(query, "&");
            String param;
            String paramname;
            String paramvalue;
            int eqindex;
            while (iter.hasMoreTokens())
            {
                param = iter.nextToken();
                eqindex = param.indexOf('=');
                if (eqindex > 0)
                {
                    paramname = param.substring(0, eqindex);
                    paramvalue = param.substring(eqindex + 1, param.length());

                    if (prefix != null)
                    {
                        if (paramname.startsWith(prefix + at))
                        {
                            paramname = paramname.substring(prefix.length() + at.length());
                        }
                    }

                    if (paramname != null)
                    {
                        node.add(StringUtil.urlDecode(paramname), StringUtil.urlDecode(paramvalue));
                    }
                }
            }
        }
    }


    @SuppressWarnings("unchecked")
    public static GenRow refineRequests(RequestNode req1, RequestNode req2)
    {
        GenRow row = new GenRow();

        String mergetoken1 = KeyMaker.generate(10);
        String mergetoken2 = KeyMaker.generate(10);

        Set keys = new HashSet();
        keys.addAll(req2.keySet());
        keys.addAll(req1.keySet());
        Iterator iter = keys.iterator();
        RelatedFieldName paramname = null;

        ParameterNode node1, node2;
        boolean hasrelationomit1, hasrelationomit2 = false;
        int i = 0;

        while (iter.hasNext())
        {
            i++;
            hasrelationomit1 = false;
            hasrelationomit2 = false;

            paramname = new RelatedFieldName((String) iter.next());
            node1 = (ParameterNode) req1.getNode(paramname.fullname, Parameter.type_RequestParameter, Parameter.type_MaxSearchParameter);
            node2 = (ParameterNode) req2.getNode(paramname.fullname, Parameter.type_RequestParameter, Parameter.type_MaxSearchParameter);
            Object nodevalue1, nodevalue2;

            if (node1 != null && node2 != null && !node1.isEmpty() && !node2.isEmpty())
            {
                nodevalue1 = node1.getValue();
                if (nodevalue1 != null)
                {
                    hasrelationomit1 = (paramname.hasRelations() && GeneratorFactory.hasOmit(req1, paramname.relations));

                    setMergeParameter(row, paramname.fullname, nodevalue1, mergetoken1);
                }
                nodevalue2 = node2.getValue();
                if (nodevalue2 != null && !equals(nodevalue2, nodevalue1))
                {
                    hasrelationomit2 = (paramname.hasRelations() && GeneratorFactory.hasOmit(req2, paramname.relations));
                    if ((hasrelationomit2 && !hasrelationomit1) || (!hasrelationomit2 && hasrelationomit1))
                    {
                        paramname.appendRelationSuffix("M", i);
                    }
                    setMergeParameter(row, paramname.fullname, nodevalue2, mergetoken2);
                }
            }
            else if (node2 != null && !node2.isEmpty())
            {
                //row.put(setMergeSuffix(node2.getName(), false, null, mergetoken2), node2.getValue());
                setMergeParameter(row, paramname.fullname, node2.getValue(), mergetoken2);
            }
            else if (node1 != null && !node1.isEmpty())
            {
                //row.put(setMergeSuffix(node1.getName(), false, null, mergetoken1), node1.getValue());
                setMergeParameter(row, paramname.fullname, node1.getValue(), mergetoken1);
            }
        }

        return (row);
    }


    public static GenRow mergeRequests(RequestNode[] reqs)
    {
        GenRow row = new GenRow();
        Set keys = new HashSet();

        for (int i = 0; i < reqs.length; i++)
        {
            if (reqs[i] != null)
            {
                reqs[i].putToken("-mergetoken", KeyMaker.generate(10));
                keys.addAll(reqs[i].keySet());
            }
        }

        Iterator iter = keys.iterator();
        RelatedFieldName paramname = null;

        ParameterNode node;

        String orsuffix = KeyMaker.generate(10);
        while (iter.hasNext())
        {
            paramname = new RelatedFieldName((String) iter.next());
            for (int i = 0; i < reqs.length; i++)
            {
                if (reqs[i] != null)
                {
                    node = (ParameterNode) reqs[i].getNode(paramname.fullname, Parameter.type_MinParameter, Parameter.type_MaxSearchParameter);
                    if (node != null && !node.isEmpty())
                    {
                        row.put(setMergeSuffix(setMergeSuffix(node.getName(), false, null, reqs[i].getString("-mergetoken")), true, orsuffix, null), node.getValue());
                    }
                }
            }
        }

        return (row);
    }

    protected static void setMergeParameter(GenRow row, String nodename, Object nodevalue, String mergetoken)
    {
        if (nodevalue instanceof String)
        {

            //row.add(nodename, setMergeSuffix(nodevalue.toString(), i, or, orsuffix, null));
            row.add(setMergeSuffix(nodename, false, null, mergetoken), nodevalue.toString()); //this one works better when merging profile searches.
            //row.add(nodename, nodevalue.toString());

        }
        else if (nodevalue instanceof String[])
        {

            //add(row, nodename, setMergeSuffix(setMergeSuffix((String[])nodevalue, i, true, orsuffix, null), i, or, orsuffix, null));
            add(row, setMergeSuffix(nodename, false, null, mergetoken), (String[])nodevalue);
            //add(row, nodename, (String[]) nodevalue);

        }
    }

    protected static boolean equals(Object a, Object b)
    {
        if (a instanceof String && b instanceof String)
        {
            return (a.equals(b));
        }
        else if (a instanceof String[] && b instanceof String[])
        {
            return (equals((String[]) a, (String[]) b));
        }
        return (false);
    }

    protected static boolean equals(String[] a, String[] b)
    {
        if (a != null && b != null)
        {
            if (a.length == b.length)
            {

                String as, bs = null;
                for (int i = 0; i < a.length; i++)
                {
                    if (!getValueWithoutGroup(a[i]).equals(getValueWithoutGroup(b[i])))
                    {
                        return (false);
                    }
                }
                return (true);
            }
        }
        return (false);
    }

    protected static String getValueWithoutGroup(String s)
    {
        int gi = getGroupIndex(s);
        if (gi > 0)
        {
            return (s.substring(0, gi));
        }
        return (s);
    }

    protected static int getGroupIndex(String s)
    {
        int i = s.indexOf('|');
        int j = s.indexOf('^');
        if (i > 0 && j > 0) //want the smallest >0 index
        {
            return (i < j ? i : j);
        }
        return (i > j ? i : j);
    }

    protected static void add(RequestNode req, String name, String[] values)
    {
        for (int i = 0; i < values.length; i++)
        {
            if (values[i].length() != 0)
            {
                req.add(name, values[i]);
            }
        }
    }

    protected static String[] setMergeSuffix(String[] s, boolean or, String orsuffix, String andsuffix)
    {
        String[] sc = new String[s.length];
        for (int j = 0; j < s.length; j++)
        {
            sc[j] = setMergeSuffix(s[j], or, orsuffix, andsuffix);
        }
        return (sc);
    }

    protected static String setMergeSuffix(String s, boolean or, String orsuffix, String andsuffix)
    {
        if (s != null && s.length() != 0)
        {
            StringBuffer temp = new StringBuffer();
            if (or)
            {
                if (orsuffix != null)
                {
                    temp.append("|").append(orsuffix);
                }
            }
            else
            {
                if (andsuffix != null)
                {
                    temp.append("^").append(andsuffix);
                }
            }

            if (!s.endsWith(temp.toString()))
            {
                temp.insert(0, s);
                return (temp.toString());
            }
        }
        return (s);
    }

    public static void parseRequest(HttpServletRequest request, RequestNode node, String prefix)
    {
        parseRequest(request.getParameterMap(), node, prefix);
        /*Enumeration params = request.getParameterNames();
        String paramname = null;
        String paramvalue = null;
        String[] paramvalues = null;

        while(params.hasMoreElements())
        {
            paramname = (String) params.nextElement();
            paramvalue = request.getParameter(paramname);
            paramvalues = request.getParameterValues(paramname);
            if(paramvalues.length == 1)
            {
                node.put(paramname, paramvalue);
            }
            else
            {
                node.put(paramname, paramvalues);
            }
        }*/
    }
}