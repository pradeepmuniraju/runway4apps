package com.sok.validation;

import com.sok.framework.ActionBean;
import org.apache.commons.lang.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProcessValidator {

    public static final String VALIDATION_NAME = "ValidateMandatoryQuestions";

//    private static final Logger LOGGER = Logger.getLogger(ProcessValidator.class);

    public boolean canChangeStage(String previousStageID, String opportunityId) {

        if (previousStageID==null || StringUtils.isBlank(previousStageID)) {
            return true;
        }

        Connection conn = ActionBean.connect();
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {

            pStmt = conn.prepareStatement("select Value from SystemParameters where Name=?");
            pStmt.setString(1, VALIDATION_NAME);
            rs = pStmt.executeQuery();
            if (!rs.next()) { //If there is no result then no validation required
                return true;
            } else if (!Boolean.parseBoolean(rs.getString(1))) {
                return true;
            } else {

                pStmt = conn.prepareStatement("select ls.SurveyID from LinkedSurveys ls join Surveys s on ls.SurveyID=s.SurveyID " +
                        "where StageID=? and lower(s.Name) not like '%checklist'");
                pStmt.setString(1, previousStageID);
                rs = pStmt.executeQuery();

                List<String> surveyIds = null;
                if (!rs.next()) {
                    // No survey for this stagename so cool can change
                    return true;
                } else {
                    //There may be more than one survey per stage
                    surveyIds = new ArrayList<>();
                    rs.beforeFirst();
                    while (rs.next())
                        surveyIds.add(rs.getString(1));
                }

                for (String surveyId : surveyIds) {
                    pStmt = conn.prepareStatement("select QuestionID from SurveyQuestions where Mandatory='Y' and SurveyID=?");
                    pStmt.setString(1, surveyId);
                    rs = pStmt.executeQuery();
                    int numQuestions;

                    if (!rs.next()) {
                        //no mandatory questions continue to next surveyId
                        continue;
                    } else {
                        numQuestions = 1;
                        while (rs.next()) {
                            numQuestions++;
                        }
                    }

                    pStmt = conn.prepareStatement("select AnswerID from OpportunityAnswers oa join " +
                            "SurveyQuestions sq on oa.QuestionID=sq.QuestionID where sq.SurveyID=? and sq.Mandatory='Y' and oa.OpportunityID=?");

                    pStmt.setString(1, surveyId);
                    pStmt.setString(2, opportunityId);
                    rs = pStmt.executeQuery();
                    if (!rs.next()) {
                        //no answers
                        return false;
                    } else {
                        int numAnswers = 1;
                        while (rs.next()) {
                            numAnswers++;
                        }
                        if (numQuestions > numAnswers) {
                            return false;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            //should log something here
//            LOGGER.error("Failed to execute query " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (rs!=null)
                try { rs.close(); } catch (SQLException e) {}
            if (pStmt!=null)
                try { pStmt.close(); } catch (SQLException e) {}
            if (conn!=null)
                try { conn.close(); } catch (SQLException e) {}
        }

        return true;
    }



    public boolean isCheckListDone(String stageId, String opportunityID) {
    	if (StringUtils.isBlank(stageId) || StringUtils.isBlank(opportunityID)) return true;

        //Get the number of questions for the checklist
        final String sql = "select QuestionID from SurveyQuestions where SurveyID=(select SurveyID from Stages where StageID=?)";

        //Get the number of answers for the checklist
        final String sql2 = "select AnswerID from OpportunityStageAnswers osa join OpportunityStages os on osa.OpportunityStageID=os.OpportunityStageID " +
                                "where os.OpportunityID=? and QuestionID=? and osa.Answer='Yes'";

        Connection conn = ActionBean.connect();
        PreparedStatement pStmt = null;
        ResultSet rs = null;

        try {
            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, stageId);
            rs = pStmt.executeQuery();
            List<String> questionIds = new ArrayList<>();
            while (rs.next()) {
                questionIds.add(rs.getString(1));
            }
            if (questionIds.size()==0)
                return true; // no questions can return true

            pStmt = conn.prepareStatement(sql2);
            int answerCount = 0;

            for (String questionId : questionIds) {
                pStmt.setString(1, opportunityID);
                pStmt.setString(2, questionId);
                rs = pStmt.executeQuery();
                while (rs.next()) {
                    answerCount++;
                }
            }
            return questionIds.size()<=answerCount;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs!=null)
                try { rs.close(); } catch (SQLException e) {}
            if (pStmt!=null)
                try { pStmt.close(); } catch (SQLException e) {}
            if (conn!=null)
                try { conn.close(); } catch (SQLException e) {}
        }


        return false;
    }

}