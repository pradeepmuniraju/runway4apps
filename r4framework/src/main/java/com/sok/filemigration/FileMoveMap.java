package com.sok.filemigration;

import com.sok.framework.ActionBean;
import com.sok.framework.InitServlet;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.sql.*;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by pradeeprunway on 20/04/2017.
 */
public class FileMoveMap {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileMoveMap.class);

    private static FileMoveMap INSTANCE;

    private Properties properties;
    private String propertiesPath;
    private HashMap<String,String> movedMap = new HashMap<>();

    public static FileMoveMap getInstance(String propertiesPath) {
        if (INSTANCE==null)
            INSTANCE = new FileMoveMap(propertiesPath);
        return INSTANCE;
    }

    private FileMoveMap(String propertiesPath) {
        try {
            properties = new Properties();
            LOGGER.info(propertiesPath);
            this.propertiesPath = propertiesPath;

            properties.load(new FileReader(propertiesPath));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static FileMoveMap getInstance() {
        if (INSTANCE==null)
            throw new RuntimeException("Cannot provide instance of FileMoveMap as none of the required properties " +
                    "are set. Ensure the AppListener has been loaded.");
        return INSTANCE;
    }

    public void clearMovedMap() {
        movedMap.clear();
    }

    public void addMovedMap(String key, String value) {
        if (StringUtils.isBlank(key)) return;

        if (StringUtils.isNotBlank(value))
            movedMap.put(key, value);
        else
            movedMap.remove(key);
    }

    public void removeMovedMap(String key) {
        if (StringUtils.isNotBlank(key)) movedMap.remove(key);
    }

    public String getDocumentURLByFilePath(String urlPath, String fileName, String documentID) {
        // this should speed things up a bit
        if (StringUtils.isNotBlank(movedMap.get(documentID + urlPath))) return movedMap.get(documentID + urlPath);
        //get the file name from the db first
        String sql = "select dest from MigratedResources where source like ?";
        Connection conn = ActionBean.connect();
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        String s3Dest = null;
        try {
            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, "%" + urlPath);
            rs = pStmt.executeQuery();
            if (rs.next()) {
                s3Dest = rs.getString(1);
                // the file needs to be exact as the same file name can be in different places
                //} else {
                //	sql = "select dest from MigratedResources where source like ?";
                //  pStmt = conn.prepareStatement(sql);
                //  pStmt.setString(1, "%" + fileName);
                //  rs = pStmt.executeQuery();
                //  if (rs.next())
                //      s3Dest = rs.getString(1);
            }

        } catch (SQLException e) {
            LOGGER.error("Failed to fetch s3 url name from db ERROR:=" + e.getMessage());
        } finally {
            cleanup(conn, pStmt, rs);
        }

        // need to replace the + with %2B or it will not resolve
        if (StringUtils.isNotBlank(s3Dest) && !"null".equals(s3Dest)) {
            s3Dest = s3Dest.replaceAll("\\+", "%2B");
            movedMap.put(documentID + urlPath, s3Dest);
        } else {
            //Not found in Migrated Resouces table; default to original path
            s3Dest = urlPath;

            if (StringUtils.isNotBlank(s3Dest) && !s3Dest.startsWith("http")) s3Dest = InitServlet.getSystemParam("URLHome") + "/" + s3Dest;

            //Below code not needed.... S3 Destination should be evaluated  only from Migrated resources table.
/*        	s3Dest = fileMover.getResource(documentID, fileName, fileMover.authenticate());
            if (StringUtils.isNotBlank(s3Dest) && !"null".equals(s3Dest)) {
            	s3Dest = s3Dest.replaceAll("\\+", "%2B");
            } else {
            	s3Dest = urlPath;
            }*/
            movedMap.put(documentID + urlPath, s3Dest);
        }

        return s3Dest;
    }

    public void deleteResource(String filePath, String fileName, String documentID) {
        final String sql = "delete from MigratedResources where resourceId=? or source like ?";
        Connection conn = ActionBean.connect();
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, documentID);
            pStmt.setString(2, "%" + filePath);
            pStmt.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error("Failed to fetch s3 url name from db ERROR:=" + e.getMessage());
        } finally {
            cleanup(conn, pStmt, null);
        }
        //This should be handled as part of R4 - P2 Sync process.
        //And using archiving rules written on AWS S3 bucket..
//        fileMover.deactivateResource(documentID, fileName, fileMover.authenticate());
        movedMap.remove(documentID + fileName);
    }

    private static void cleanup(Connection conn, Statement stmt, ResultSet rs) {
        if (rs!=null) try { rs.close(); } catch (SQLException e) {}
        if (stmt!=null) try { stmt.close(); } catch (SQLException e) {}
        if (conn!=null) try { conn.close(); } catch (SQLException e) {}
    }
}
