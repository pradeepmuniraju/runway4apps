/*
 * User: Behrang
 * Date: Jun 8, 2010
 * Time: 3:00:16 PM
 */
package com.sok.gmaps;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class CityGeocode implements Serializable {

    private String id;
    private String country;
    private String state;
    private String city;
    private String latitude;
    private String longitude;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{\n");
        json.append("\"id\": \"").append(escape(id)).append("\",\n");
        json.append("\"country\": \"").append(escape(country)).append("\",\n");
        json.append("\"state\": \"").append(escape(state)).append("\",\n");
        json.append("\"city\": \"").append(escape(city)).append("\",\n");
        json.append("\"lat\": \"").append(escape(latitude)).append("\",\n");
        json.append("\"lng\": \"").append(escape(longitude)).append("\"\n");
        json.append("}");

        return json.toString();
    }

    private String escape(String text) {
        return StringEscapeUtils.escapeHtml(text);               
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CityGeocode that = (CityGeocode) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
