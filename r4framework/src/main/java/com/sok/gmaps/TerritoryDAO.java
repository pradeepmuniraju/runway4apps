/*
 * User: Behrang
 * Date: Jun 8, 2010
 * Time: 3:13:32 PM 
 */
package com.sok.gmaps;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class TerritoryDAO {

    public Territory findById(HttpServletRequest request, String id) {
        GenRow terRow = new GenRow();
        terRow.setRequest(request);
        terRow.setTableSpec("Territories");
        terRow.setParameter("TerritoryID", id);
        terRow.setParameter("-select1", "TerritoryID");
        terRow.setParameter("-select2", "Name");
        terRow.setParameter("-select3", "Description");
        terRow.setParameter("-select4", "Country");
        terRow.setParameter("-select5", "State");
        terRow.setParameter("-select6", "Type");
        terRow.setParameter("-select8", "GroupID");
        terRow.doAction(GenerationKeys.SELECT);        
        if (!terRow.isSuccessful()) {
            return null;
        }

        Territory ter = new Territory();
        ter.setId(terRow.getString("TerritoryID"));
        ter.setName(terRow.getString("Name"));
        ter.setDescription(terRow.getString("Description"));
        ter.setCountry(terRow.getString("Country"));
        ter.setState(terRow.getString("State"));
        ter.setType(terRow.getString("Type"));
        ter.setGroupId(terRow.getString("GroupID"));

        GenRow terRepRow = new GenRow();
        terRepRow.setRequest(request);
        terRepRow.setTableSpec("TerritorySalesReps");
        terRepRow.setParameter("-select1", "TerritorySalesRepID");
        terRepRow.setParameter("-select2", "TerritoryID");
        terRepRow.setParameter("-select3", "SalesRepID");
        terRepRow.setParameter("TerritoryID", id);
        terRepRow.doAction(GenerationKeys.SEARCH);
        terRepRow.getResults(true);

        List<String> salesRepIds = new ArrayList<String>();
        while (terRepRow.getNext()) {
            salesRepIds.add(terRepRow.getString("SalesRepID"));
        }
        terRepRow.close();

        for (String salesRepId : salesRepIds) {
            if (salesRepId == null || salesRepId.trim().length() == 0) { continue; }
            GenRow repRow = new GenRow();
            repRow.setTableSpec("Users");
            repRow.setParameter("UserID", salesRepId);
            repRow.setParameter("-select1", "UserID");
            repRow.setParameter("-select2", "FirstName");
            repRow.setParameter("-select3", "LastName");
            repRow.doAction(GenerationKeys.SELECT);
            if (repRow.isSuccessful()) {
                Territory.SalesRep salesRep = new Territory.SalesRep();
                salesRep.setId(repRow.getString("UserID"));
                salesRep.setFirstName(repRow.getString("FirstName"));
                salesRep.setLastName(repRow.getString("LastName"));
                ter.getSalesReps().add(salesRep);
            }

        }

        if (ter.getGroupId() != null && ter.getGroupId().trim().length() != 0) {
            GenRow groupRow = new GenRow();
            groupRow.setTableSpec("Groups");
            groupRow.setParameter("GroupID", ter.getGroupId());
            groupRow.setParameter("-select1", "GroupID");
            groupRow.setParameter("-select2", "Name");
            groupRow.doAction(GenerationKeys.SELECT);

            if (groupRow.isSuccessful()) {
                ter.setGroupName(groupRow.getString("Name"));
            }
        }

        return ter;
    }

    public List<Territory> findByAddress(HttpServletRequest request, String city, String state, String country) {
        GenRow terRow = new GenRow();
        terRow.setRequest(request);
        terRow.setTableSpec("Territories");
        terRow.setParameter("-select1", "TerritoryID");
        terRow.setParameter("-select2", "Name");
        terRow.setParameter("-select3", "Description");
        terRow.setParameter("-select4", "Country");
        terRow.setParameter("-select5", "State");
        terRow.setParameter("-select6", "Type");
        terRow.setParameter("-select8", "GroupID");
        terRow.setParameter("Country", country);
        terRow.setParameter("State", state);
        terRow.doAction(GenerationKeys.SEARCH);
        terRow.getResults(true);
        if (terRow.getSize() == 0) {
            return null;
        }

        List<Territory> territories = new ArrayList<Territory>();

        while (terRow.getNext()) {
            GenRow terCities = new GenRow();
            terCities.setRequest(request);
            terCities.setTableSpec("TerritoryCities");
            terCities.setParameter("TerritoryID", terRow.getString("TerritoryID"));
            terCities.setParameter("-select1", "CityID");
            terCities.doAction(GenerationKeys.SEARCH);
            terCities.getResults(true);

            while (terCities.getNext()) {
                GenRow cityGeocode = new GenRow();
                cityGeocode.setRequest(request);
                cityGeocode.setTableSpec("CityGeocodes");
                cityGeocode.setParameter("CityGeocodeID", terCities.getString("CityID"));
                cityGeocode.setParameter("-select1", "CityGeocodeID");
                cityGeocode.setParameter("-select2", "City");
                cityGeocode.doAction(GenerationKeys.SEARCH);
                cityGeocode.getResults(true);
                while (cityGeocode.getNext()) {
                    String cityName = cityGeocode.getString("City");
                    if (cityName.equalsIgnoreCase(city)) {
                        territories.add(findById(request, terRow.getString("TerritoryID")));
                        break;
                    }
                }
            }
            terCities.close();
        }

        return territories;        
    }

    public List<Territory> findAll(HttpServletRequest request) {

        List<Territory> terList = new ArrayList<Territory>();
        GenRow terRow = new GenRow();
        terRow.setRequest(request);
        terRow.setTableSpec("Territories");
        terRow.setParameter("-select1", "TerritoryID");
        terRow.setParameter("-select2", "Name");
        terRow.setParameter("-select3", "Description");
        terRow.setParameter("-select4", "Country");
        terRow.setParameter("-select5", "State");   
        terRow.setParameter("-select6", "Type");
        terRow.setParameter("-select8", "GroupID");
        terRow.sortBy("Name", 0);
        terRow.doAction(GenerationKeys.SEARCH);
        terRow.getResults(true);

        while (terRow.getNext()) {
            Territory ter = new Territory();
            ter.setId(terRow.getString("TerritoryID"));
            ter.setName(terRow.getString("Name"));
            ter.setDescription(terRow.getString("Description"));
            ter.setCountry(terRow.getString("Country"));
            ter.setState(terRow.getString("State"));
            ter.setType(terRow.getString("Type"));
            ter.setGroupId(terRow.getString("GroupID"));
            terList.add(ter);

            GenRow terRepRow = new GenRow();
            terRepRow.setRequest(request);
            terRepRow.setTableSpec("TerritorySalesReps");
            terRepRow.setParameter("-select1", "TerritorySalesRepID");
            terRepRow.setParameter("-select2", "TerritoryID");
            terRepRow.setParameter("-select3", "SalesRepID");
            terRepRow.setParameter("TerritoryID", ter.getId());
            terRepRow.doAction(GenerationKeys.SEARCH);
            terRepRow.getResults(true);

            List<String> salesRepIds = new ArrayList<String>();
            while (terRepRow.getNext()) {
                salesRepIds.add(terRepRow.getString("SalesRepID"));
            }
            terRepRow.close();

            for (String salesRepId : salesRepIds) {
                if (salesRepId == null || salesRepId.trim().length() == 0) continue;
                GenRow repRow = new GenRow();
                repRow.setTableSpec("Users");
                repRow.setParameter("UserID", salesRepId);
                repRow.setParameter("-select1", "UserID");
                repRow.setParameter("-select2", "FirstName");
                repRow.setParameter("-select3", "LastName");
                repRow.doAction(GenerationKeys.SELECT);
                if (repRow.isSuccessful()) {
                    Territory.SalesRep salesRep = new Territory.SalesRep();
                    salesRep.setId(repRow.getString("UserID"));
                    salesRep.setFirstName(repRow.getString("FirstName"));
                    salesRep.setLastName(repRow.getString("LastName"));
                    ter.getSalesReps().add(salesRep);
                }
            }
            
            if (ter.getGroupId() != null && ter.getGroupId().trim().length() != 0) {
                GenRow groupRow = new GenRow();
                groupRow.setTableSpec("Groups");
                groupRow.setParameter("GroupID", ter.getGroupId());
                groupRow.setParameter("-select1", "GroupID");
                groupRow.setParameter("-select2", "Name");                
                groupRow.doAction(GenerationKeys.SELECT);

                if (groupRow.isSuccessful()) {
                    ter.setGroupName(groupRow.getString("Name"));
                }
            }
        }

        terRow.close();

        for (Territory ter : terList) {
            GenRow terCityRow = new GenRow();
            terCityRow.setRequest(request);
            terCityRow.setViewSpec("TerritoryCitiesView");
            terCityRow.setParameter("ON-TerritoryID", ter.getId());
            terCityRow.setParameter("TerritoryID", ter.getId());
            terCityRow.sortBy("City", 0);
            terCityRow.doAction(GenerationKeys.SEARCH);
            terCityRow.getResults(true);
            while (terCityRow.getNext()) {
                CityGeocode cg = new CityGeocode();
                cg.setCountry(terCityRow.getString("Country"));
                cg.setState(terCityRow.getString("State"));
                cg.setCity(terCityRow.getString("City"));
                cg.setId(terCityRow.getString("CityGeocodeID"));
                cg.setLatitude(terCityRow.getString("CityLatitude"));
                cg.setLongitude(terCityRow.getString("CityLongitude"));
                ter.addCity(cg);
            }
        }

        return terList;
    }



}
