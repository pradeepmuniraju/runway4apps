/*
 * User: Behrang
 * Date: Jun 8, 2010
 * Time: 3:03:03 PM 
 */
package com.sok.gmaps;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Territory implements Serializable {

    private String id;
    private String name;
    private String description;
    private String state;
    private String type;
    private String country;
    private String groupId;
    private String groupName;

    private List<SalesRep> salesReps;
    private List<CityGeocode> cities;

    public Territory() {
        cities = new ArrayList<CityGeocode>();
        salesReps = new ArrayList<SalesRep>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<SalesRep> getSalesReps() {
        return salesReps;
    }

    public void setSalesReps(List<SalesRep> salesReps) {
        this.salesReps = salesReps;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<CityGeocode> getCities() {
        return cities;
    }

    public void setCities(List<CityGeocode> cities) {
        this.cities = cities;
    }    

    public void addCity(CityGeocode cg) {
        cities.add(cg);
    }

    public boolean removeCity(CityGeocode cg) {
        return cities.remove(cg);
    }

    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{\n");
        json.append("id: \"").append(escape(id)).append("\",\n");
        json.append("name: \"").append(escape(name)).append("\",\n");
        json.append("country: \"").append(escape(country)).append("\",\n");
        json.append("state: \"").append(escape(state)).append("\",\n");
        json.append("type: \"").append(escape(type)).append("\",\n");
        json.append("groupId: \"").append(escape(groupId)).append("\",\n");
        json.append("groupName: \"").append(escape(groupName)).append("\",\n");
        json.append("reps: [\n");
        for (SalesRep sr : salesReps) {
            json.append(sr.toJson()).append(",\n");
        }

        if (salesReps.size() > 0) {
            json.setLength(json.length() - 2);
        }

        json.append("\n],\n");

        json.append("cities: [\n");

        for (CityGeocode cg : cities) {
            json.append(cg.toJson()).append(",\n");
        }

        if (cities.size() > 0) {
            json.setLength(json.length() - 2);
        }

        json.append("\n]}");

        return json.toString();        
    }

    private String escape(String text) {
        return StringEscapeUtils.escapeHtml(text);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Territory territory = (Territory) o;

        if (id != null ? !id.equals(territory.id) : territory.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public static class SalesRep {

        String id;
        String firstName;
        String lastName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String toJson() {
            StringBuilder json = new StringBuilder();
            json.append("{\n");
            json.append("id: \"").append(escape(id)).append("\",\n");
            json.append("firstName: \"").append(escape(firstName)).append("\",\n");
            json.append("lastName: \"").append(escape(lastName)).append("\"\n}");
            return json.toString();
        }

        private String escape(String text) {
            return StringEscapeUtils.escapeHtml(text);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SalesRep salesRep = (SalesRep) o;

            if (id != null ? !id.equals(salesRep.id) : salesRep.id != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }

        
    }
}
