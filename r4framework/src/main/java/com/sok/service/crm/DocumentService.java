package com.sok.service.crm;

import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.Document;
import com.sok.runway.externalInterface.beans.LinkedDocument;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFImageWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.util.*;

//import com.sok.runway.crm.LinkedDocument;

public class DocumentService {

	private static final DocumentService service = new DocumentService();
	/* server logs */
	private static final Logger logger = LoggerFactory.getLogger(DocumentService.class);
	/* logs user actions for auditing */
	private static final ActivityLogger activityLogger = ActivityLogger.getActivityLogger();
	private static final UserService us = UserService.getInstance(); 
	public static DocumentService getInstance() {
		return service;
	}

	public GenRow getDocumentRow(Connection con, String documentID) {
		GenRow d = new GenRow();
		d.setConnection(con);
		d.setViewSpec("DocumentView");
		d.setParameter("DocumentID", documentID);
		if(!d.isSet("DocumentID")) {
			throw new IllegalArgumentException("DocumentID cannot be null or empty");
		}
		d.doAction(GenerationKeys.SELECT);
		if(!d.isSuccessful()) {
			throw new NotFoundException("Could not find document with id " + documentID);
		}
		return d;
	}

	public Document getDocument(final HttpServletRequest request, final String documentID) {
		logger.trace("getDocument(Request, {})", documentID);
		return getDocument(ActionBean.getConnection(request), us.getCurrentUser(request), documentID);
	}

	public Document getDocument(final Connection con, final UserBean user, final String documentID){
		logger.trace("getDocument(Connection, {},{})", user != null ? user.getName() : "null user", documentID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Documents")) {
			throw new AccessException("You do not have permission to access the document module");
		}
		GenRow doc = getDocumentRow(con, documentID);
		if(!user.canView(doc)) {
			//throw new AccessException("You do not have permission to view this document");
		}
		return BeanFactory.getDocumentFromTableData(doc);
	}


	/**
	 * Accept a document object with initial details, and save the file specified as a Runway document
	 */
	public Document uploadDocument(final HttpServletRequest request, File f, Document d) {
		return uploadDocument(ActionBean.getConnection(request), us.getCurrentUser(request), f, d);
	}

	/**
	 * Accept a document object with initial details, and save the file specified as a Runway document
	 */
	public Document uploadDocument(final Connection con, final UserBean user, File f, Document d) {

		final GenRow doc = StringUtils.isNotBlank(d.getDocumentID()) ? getDocumentRow(con, d.getDocumentID()) : new GenRow();
		if(doc.isSuccessful()) {
			doc.setAction(GenerationKeys.UPDATE);
			if(!user.canEdit(doc)) {
				throw new AccessException("You do not have permission to update this document");
			}
		} else {
			if(!user.canCreate("Documents")) {
				throw new AccessException("You do not have permission to create documents");
			}
			doc.setConnection(con);
			doc.setViewSpec("DocumentView");
			doc.setAction(GenerationKeys.INSERT);
			d.setDocumentID(KeyMaker.generate());
		}
		BeanFactory.setTableDataFromDocument(d, doc);
		if(!doc.isSet("GroupID")) {
			doc.setParameter("GroupID", user.getString("DefaultGroupID"));
		}
		String newFileName = StringUtil.removeNonFileSystemCharacters(f.getName());
		File folder = null;
		if (newFileName != null && (newFileName.toLowerCase().endsWith(".jpg") || newFileName.toLowerCase().endsWith(".png") || newFileName.toLowerCase().endsWith(".gif"))) {
			folder = f.getParentFile().getParentFile(); 
		} else {
			folder = new File(InitServlet.getRealPath("/documents/"));
		}
		
		File userfolder = new File(folder,user.getUserID());
		if(!userfolder.exists())
		{
			userfolder.mkdir();
		}
		File contactfolder = new File(userfolder,d.getDocumentID());
		contactfolder.mkdir();
		File dest = new File(contactfolder,newFileName);
		//files can not be moved across file systems.
		//f.renameTo(dest);
		
		String filePath = f.getAbsolutePath().substring(InitServlet.getRealPath("").length());
		try {
			copyFile(f, dest);
			filePath = dest.getAbsolutePath().substring(InitServlet.getRealPath("").length());
			f.delete();
		} catch (Exception e) {
			filePath = f.getAbsolutePath().substring(InitServlet.getRealPath("").length());
		}

		if (File.separatorChar != '/') {
			filePath = StringUtil.replace(filePath,File.separatorChar,'/');
		}
		if (filePath.startsWith("/")) {
			filePath = filePath.substring(1);
		}
		doc.setParameter("FilePath",filePath);
		doc.setParameter("FileName",dest.getName());
		doc.setParameter("FileSize",dest.length());

		doc.doAction();

		if(doc.isSuccessful()) {
			activityLogger.logActivity(user.getUser(), con, "Document", d.getDocumentID(), doc.getAction());
		} else {
			throw new RuntimeException("Failed to save document with error : " + doc.getError());
		}
		return getDocument(con, user, d.getDocumentID());
	}

	public List<Document> getDocuments(final HttpServletRequest request, final String type, final String subType, final String keyword) { 
		return getDocuments(ActionBean.getConnection(request), us.getCurrentUser(request), type, subType, keyword);
	}

	/**
	 * Return a list of documents, optionally searching by type, subtype or keyword(s)
	 * Will return a maximum of 100 records. 
	 */
	public List<Document> getDocuments(final Connection con, final UserBean user, final String type, final String subType, final String keyword) { 
		GenRow d = new GenRow();
		d.setViewSpec("DocumentView");
		user.setConstraint(d, "DocumentGroup");
		if(StringUtils.isNotBlank(type)) {
			d.setParameter("DocumentType^search", type);
		}
		if(StringUtils.isNotBlank(subType)) {
			d.setParameter("DocumentSubType^search", subType);
		}
		if(StringUtils.isNotBlank(keyword)) {
			if(keyword.indexOf(" ") > 0) {
				String[] kws = keyword.split(" ");
				StringBuilder query = new StringBuilder(keyword.length() + (kws.length * 2));

				for(String k: kws) {
					if(query.length() != 0) 
						query.append("+");
					query.append("%").append(k).append("%");
				}
				d.setParameter("Description|1", query.toString());
				d.setParameter("FileName|1", query.toString());
				d.setParameter("Name|1", query.toString());
			} else {
				String q = new StringBuilder(keyword.length()+2).append("%").append(keyword).append("%").toString();
				d.setParameter("Description|1", q);
				d.setParameter("FileName|1", q);
				d.setParameter("Name|1", q);
			}
		}
		//arbitrary number for max results
		d.setTop(100);
		return retrieveDocuments(con, d);
	}

	public List<Document> getProductLinkedDocuments(final HttpServletRequest request, final String productID) {
		return getProductLinkedDocuments(ActionBean.getConnection(request), us.getCurrentUser(request), productID);
	}

	public List<Document> getProductLinkedDocuments(final Connection con, final UserBean user, final String productID) {
		if(StringUtils.isBlank(productID)) {
			throw new IllegalArgumentException("ProductID must be supplied");
		}
		GenRow d = new GenRow();
		d.setViewSpec("LinkedDocumentView");
		user.setConstraint(d, "LinkedDocumentDocument.DocumentGroup");
		d.setParameter("ProductID", productID);

		return retrieveDocuments(con, d);
	}

	private List<Document> retrieveDocuments(Connection con, GenRow d) {
		try {
			d.setConnection(con);
			if(logger.isTraceEnabled()) {
				d.doAction(GenerationKeys.SEARCH);
				logger.trace(d.getStatement());
			}
			d.setParameter("-sort0", "DocumentType");
			d.setParameter("-sort1", "DocumentSubType");
			d.setParameter("-sort2", "Description");
			d.getResults(true);

			if(d.getNext()) {
				List<Document> list = new ArrayList<Document>(d.getSize());
				do {
					list.add(BeanFactory.getDocumentFromTableData(d));
				} while(d.getNext());
				return list;
			}
			return Collections.emptyList();
		}finally {
			d.close();
		}	
	}

	public Map<Integer, String> getDocumentPages(final HttpServletRequest request, final String documentID, final int start, final int end, final String password) {

		return getDocumentPages(ActionBean.getConnection(request), us.getCurrentUser(request), documentID, start, end, password);
	}

	public Map<Integer, String> getDocumentPages(final Connection con, final UserBean user, final String documentID, final int start, final int end, final String password) {
		logger.trace("getDocumentPages");
		/* takes care of auth, etc */
		Document d = getDocument(con, user, documentID);
		if(!"PDF".equals(d.getDocumentType())) {
			throw new UnsupportedOperationException("Get Document Pages is only supported for pdf documents");
		}
		File f = new File(InitServlet.getRealPath(d.getFilePath()));
		if(!f.exists()) {
			logger.error("File did not exist to get document pages " + f.getAbsolutePath());
		}
		File directory = f.getParentFile();
		boolean notIndexed = false;
		if(!notIndexed) {

			PDDocument document = null;
			try {
				document = PDDocument.load( f.getAbsolutePath() );     
				//document.print();

				if( document.isEncrypted() )
				{
					try
					{
						document.decrypt( password );
					}
					catch( InvalidPasswordException e )
					{
						if( StringUtils.isNotBlank(password) )//they supplied the wrong password
						{
							throw new RuntimeException("The supplied password for this document is incorrect", e);
						}
						else
						{
							throw new RuntimeException("This document is encrypted and you have not supplied a password", e);
						}
					} catch (CryptographyException e) {
						throw new RuntimeException("Crypto Error", e);
					}
				}

				PDFImageWriter imageWriter = new PDFImageWriter();
				boolean success = imageWriter.writeImage(document, "png", "", start, end, directory.getAbsolutePath() + "/page_", BufferedImage.TYPE_INT_RGB, 240);

				if(!success) {
					throw new RuntimeException("This failed for an unknown reason");
				}

				Map<Integer, String> ret = new HashMap<Integer, String>();
				String partialPath = d.getFilePath().substring(0, d.getFilePath().lastIndexOf("/")) + "/page_%1$d.png";
				//pdfbox doesn't throw an exception for too many pages requested, so we must handle this.
				for(int i=start; i<= end && i<= document.getNumberOfPages(); i++) {
					ret.put(new Integer(i), String.format(partialPath, i));
				}
				return ret;
			} catch (IOException ioe) {
				logger.error("An IO Error occurred, probably whilst saving the output images", ioe);
				throw new RuntimeException("An IO Error occurred, probably whilst saving the output images", ioe);
			} finally {
				if(document != null)
					try { document.close(); } catch (IOException e) { logger.error("Error on close", e); }
			}
		}
		throw new UnsupportedOperationException("not yet implemented");
	}

	public LinkedDocument createLinkedDocument(HttpServletRequest request, LinkedDocument linkedDocument) {
		return createLinkedDocument(ActionBean.getConnection(request), us.getCurrentUser(request), linkedDocument);
	}

	public LinkedDocument createLinkedDocument(Connection con, UserBean user, LinkedDocument linkedDocument) {

		GenRow ld = new GenRow();
		ld.setConnection(con);
		ld.setTableSpec("LinkedDocuments");
		ld.setParameter("DocumentID", linkedDocument.getDocumentID());
		ld.setParameter("ProductID", linkedDocument.getProductID());


		//only supported at the moment 
		if(!ld.isSet("DocumentID") || !ld.isSet("ProductID")) {
			throw new IllegalArgumentException("not supported at this time");
		}
		ld.setParameter("-select1","LinkedDocumentID");
		ld.doAction(GenerationKeys.SELECTFIRST);

		if(ld.isSuccessful()) {
			//update, not yet implemented 
			linkedDocument.setLinkedDocumentID(ld.getData("LinkedDocumentID"));
		} else {
			ld.setToNewID("LinkedDocumentID");
			ld.setParameter("CreatedBy", user.getUserID());
			ld.setParameter("CreatedDate", "NOW");
			if (StringUtils.isNotBlank(linkedDocument.getGalleyID())) ld.setParameter("GalleryID", linkedDocument.getGalleyID());

			ld.doAction(GenerationKeys.INSERT);

			if(!ld.isSuccessful()) 
				throw new RuntimeException("Error saving linked document " + ld.getError());

			linkedDocument.setLinkedDocumentID(ld.getString("LinkedDocumentID"));
		}
		
		com.sok.runway.crm.Product.touch(linkedDocument.getProductID(), user.getUserID());

		return linkedDocument;
	}


	public LinkedDocument createLinkedDocumentForPipeline(HttpServletRequest request, LinkedDocument linkedDocument) {
		System.out.println("createLinkedDocument is: " + request.getParameter("ProductID"));
		return createLinkedDocumentForPipeline(ActionBean.getConnection(request), us.getCurrentUser(request), linkedDocument);
	}

	public LinkedDocument createLinkedDocumentForPipeline(Connection con, UserBean user, LinkedDocument linkedDocument) {

		GenRow ld = new GenRow();
		ld.setConnection(con);
		ld.setTableSpec("LinkedDocuments");
		ld.setParameter("DocumentID", linkedDocument.getDocumentID());
		ld.setParameter("ProductID", linkedDocument.getProductID());

		//only supported at the moment 
		if(!ld.isSet("DocumentID") || !ld.isSet("ProductID")) {
			throw new IllegalArgumentException("not supported at this time");
		}
		ld.setParameter("-select1","LinkedDocumentID");
		ld.doAction(GenerationKeys.SELECTFIRST);

		if(ld.isSuccessful()) {
			//update, not yet implemented 
			linkedDocument.setLinkedDocumentID(ld.getData("LinkedDocumentID"));
		} else {
			ld.setToNewID("LinkedDocumentID");
			ld.setParameter("CreatedBy", user.getUserID());
			ld.setParameter("CreatedDate", "NOW");

			ld.setParameter("ShowOnPortal", "Y");

			ld.doAction(GenerationKeys.INSERT);

			if(!ld.isSuccessful()) 
				throw new RuntimeException("Error saving linked document " + ld.getError());

			linkedDocument.setLinkedDocumentID(ld.getString("LinkedDocumentID"));
		}
		return linkedDocument;
	}	

	public Document updateDocument(HttpServletRequest request, Document document) {
		return updateDocument(ActionBean.getConnection(request), us.getCurrentUser(request), document); 
	}

	public Document updateDocument(Connection con, UserBean user, Document document) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Documents")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		GenRow doc = getDocumentRow(con, document.getDocumentID());
		if(!user.canEdit(doc)) {
			throw new AccessException("You do not have permission to edit this document");
		}
		BeanFactory.setTableDataFromDocument(document, doc);

		if ("directlink".equals(doc.getString("DocumentSubType").toLowerCase()) || "direct link".equals(doc.getString("DocumentSubType").toLowerCase()) || 
				"youtube".equals(doc.getString("DocumentSubType").toLowerCase()) || "you tube".equals(doc.getString("DocumentSubType").toLowerCase()) || 
				"vimeo".equals(doc.getString("DocumentSubType").toLowerCase())) doc.setParameter("DocumentType", "Video");

		doc.setParameter("ModifiedDate", new Date());
		doc.setParameter("ModifiedBy", user.getUserID());
		doc.doAction(GenerationKeys.UPDATE);

		return getDocument(con, user, document.getDocumentID());
	}

	public void deleteDocument(HttpServletRequest request, String documentID) {
		deleteDocument(ActionBean.getConnection(request), us.getCurrentUser(request), documentID); 
	}

	public void deleteDocument(Connection con, UserBean user, String documentID) {
		com.sok.runway.crm.Document doc = new com.sok.runway.crm.Document(con, documentID);
		doc.setCurrentUser(user.getUser());
		if(!doc.canDelete()) {
			logger.warn("You do not have permission to delete this document");
		}
		doc.delete();
	}

	public static void copyFile(File sourceFile, File destFile) throws Exception {
	    if(!destFile.exists()) {
			String dirPath = destFile.getAbsolutePath().substring(0, destFile.getAbsolutePath().lastIndexOf("/"));
			File newDir = new File(dirPath);
			if (!newDir.exists()) newDir.mkdirs();
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;

	    try {
	        source = new FileInputStream(sourceFile.getCanonicalFile()).getChannel();
	        destination = new FileOutputStream(destFile.getCanonicalFile()).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    } catch (Exception e) {
	    	if (!sourceFile.exists()) throw e;
	    	sourceFile.getCanonicalFile().renameTo(destFile.getCanonicalFile());
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
	}
}
