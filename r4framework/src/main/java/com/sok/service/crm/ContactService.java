package com.sok.service.crm;

import com.sok.framework.*;
import com.sok.framework.generation.DatabaseException;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.crm.factories.StatusUpdater;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.Contact;
import com.sok.runway.externalInterface.beans.shared.ContactProfile;
import com.sok.runway.externalInterface.beans.shared.ContactQuestion;
import com.sok.runway.externalInterface.resources.ContactResource;
import com.sok.runway.externalInterface.resources.UtilityResource.ContactLocation;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.sql.Connection;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactService extends AbstractService {

	private static final ContactService service = new ContactService();
	/* server logs */
	private static final Logger logger = LoggerFactory.getLogger(ContactService.class);
	public Logger getLogger() {
		return logger;
	}
	/* logs user actions for auditing */
	private static final ActivityLogger activityLogger = ActivityLogger.getActivityLogger();
	private static final UserService us = UserService.getInstance(); 
	public static ContactService getInstance() {
		return service;
	}
	
    private static final Pattern levelNumberPattern = Pattern.compile("^[a-z]++\\s*+\\d++\\s*+[-/]?+");
    //Matches all two digit Area Codes enclosed in brackets: (XX),
    //and any other lingering characters like brackets, dashes, etc 
    private static final Pattern stripPhoneNumberPattern = Pattern.compile("(\\(\\d\\d\\))|([^0-9&&[^\\s]])");
    //Street Number Pattern (Assume only 1 street number, after level number has been removed).
    private static final Pattern streetNumberPattern = Pattern.compile("[0-9]*+");
    //Remove Terms
    private static final Pattern removeTermsPattern = Pattern.compile("(road)|(rd)|(street)|(st)|(avenue)|(terrace)|(level)|(lvl)|(ground)|(gnd)|(flat)");
	
    /**
     * Adapted from crm/quicknote/actions/getquicknotecontactmatches.jsp
     * @param request
     * @return
     */
	public List<Contact> getQuicknoteContactMatches(HttpServletRequest request) {
		final UserBean user = us.getCurrentUser(request);
		
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Contacts")) {
			throw new AccessException("You do not have permission to access the contacts module");
		}
		
	    GenRow contacts = null;
	    GenRow contactGroupView = null;
	    try {
	        String firstName = getDecodedParameter("-FirstName", request, "First Name");
	        String lastName = getDecodedParameter("-LastName", request,"Last Name");
	        String company = getDecodedParameter("-Company", request, "Company");
	        String email = getDecodedParameter("-Email", request, "Email");
	        String companyID = getDecodedParameter("-companyid", request, "");
	    	
	     // We count the number of non-empty optional parmaters because if
	        // all of these parameters are empty, we do not want to go off
	        // and do an expensive search for all companies.
	        int numberNonEmptyOptParams = 0;

	        // Search for contact matches
	        contacts = new GenRow();
	        contacts.setTableSpec("Contacts");
	        contacts.parseRequest(request);

	        contacts.setParameter("-select0", "ContactID");
	        contacts.setParameter("-select1", "FirstName");
	        contacts.setParameter("-select2", "LastName");
	        contacts.setParameter("-select3", "DISTINCT Email");
	        
	        if (company.length() > 0) {
	            contacts.addParameter("Company^1", "%" + company + "%");
	            numberNonEmptyOptParams++;
	            
	        }

	        if (firstName.length() > 0) {
	            contacts.setParameter("FirstName^2^1", "%" + firstName + "%");
	            numberNonEmptyOptParams += 1;

	        }

	        if (lastName.length() > 0) {
	            contacts.setParameter("LastName^1", "%" + lastName + "%");
	            numberNonEmptyOptParams += 1;

	        }
	        
	        if (email.length() > 0) {
	            contacts.setParameter("Email^1", "%" + email + "%");
	            numberNonEmptyOptParams += 1;

	        }

	        if (numberNonEmptyOptParams == 0 && companyID.length() > 0) {
	            contacts.setParameter("CompanyID^1", companyID);
	            numberNonEmptyOptParams += 1;
	        } else if (companyID.length() > 0) {
	        	numberNonEmptyOptParams += 1;
	        }

	        // Only do the search if we have at least one non-empty
	        // optional parameter.
	        if (numberNonEmptyOptParams > 0) {
	        	user.setConstraint(contacts, "ContactContactGroups.ContactGroupGroup");
	        	contacts.sortBy("FirstName",0);
	        	contacts.sortBy("LastName",1);
	            contacts.setAction(GenerationKeys.SEARCH);
	            contacts.getResults(true);
	            
	            logger.debug("QuickMatch {} - {}", contacts.getSize(), contacts.getStatement());
	        }

	        if(contacts == null || contacts.getSize() == 0) {
            	return Collections.emptyList();
	        }
	        contactGroupView = newContactGroupView(request);
            List<Contact> list = new ArrayList<Contact>();
            while(contacts.getNext()) {
                String contactID = contacts.getData("ContactID");
                loadContactGroupViewForContact(contactGroupView, contactID);
                list.add(BeanFactory.getContactFromTableData(contactGroupView));  
            }
            return list;
	    } finally {
	    	logger.trace("closing contact beans");
	    	if(contacts != null) contacts.close();
	    	if(contactGroupView != null) contactGroupView.close();
	    }
	}

    /**
     * Pre-condition: homeStreet and homeStreet2 fields have no leading or trailing whitespace.
     */
    private String[] extractAddress(String homeStreet, String homeStreet2) {
        // Address comprises of Flat Number, Street Number, Street Name, and Suburb
        // address[0] - Level/Flat Number
        // address[1] - Street Number
        // address[2] - Street Name and Suburb
        String[] addressData = new String[3];

        // We get address information from the "homeStreet" and "homeStreet2" fields.
        String address = (homeStreet + " " + homeStreet2).toLowerCase();

        // Level/flat Number
        String levelNumber = "";
        Matcher levelNumberMatcher = levelNumberPattern.matcher(address);

        if (levelNumberMatcher.find()) {
            // Note that we keep any dashes or slashes to contrict the search.
            // We want to avoid matching level/flat numbers against street numbers if we can.
            levelNumber = levelNumberMatcher.group().trim();
            // Strip out Level Number
            address = levelNumberMatcher.replaceAll("").trim();
        }

        // Street Number
        String streetNumber = "";

        Matcher streetNumberMatcher = streetNumberPattern.matcher(address);

        if (streetNumberMatcher.find()) {
            streetNumber = streetNumberMatcher.group();
            address = streetNumberMatcher.replaceAll("").trim();
        }
        // Street Name and Suburb (whatever is remaining in address)
        // Excluding superfluous terms (e.g. road, street, level, etc);
        Matcher removeTermsMatcher = removeTermsPattern.matcher(address);
        address = removeTermsMatcher.replaceAll("");

        String streetNameAndSuburb = address;
        addressData[0] = levelNumber;
        addressData[1] = streetNumber;
        addressData[2] = streetNameAndSuburb;

        return addressData;
    }

    private GenRow newContactGroupView(HttpServletRequest request) {
        GenRow contactGroupView = new GenRow();
        contactGroupView.setRequest(request);
        contactGroupView.setViewSpec("ContactView");
        
        return contactGroupView;
    }

    private void loadContactGroupViewForContact(GenRow contactGroupView, String contactID) {
        contactGroupView.clear();
        contactGroupView.setParameter("ContactID", contactID);
        contactGroupView.doAction(GenerationKeys.SELECT);
        /*
        contactGroupView.doAction(GenerationKeys.SEARCH);
        contactGroupView.getResults(true);
        */
    }
    
    public Contact getContact(final HttpServletRequest request, final String contactID) {
    	logger.trace("getContact(Request, {})", contactID);
    	return getContact(ActionBean.getConnection(request), us.getCurrentUser(request), contactID);
    }
    
    public Contact getContact(final Connection conn, final UserBean user, final String contactID) {
    	logger.trace("getContact(Connection, {}, {})", user != null ? user.getName() : "null user", contactID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Contacts")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		if(StringUtils.isBlank(contactID)) {
			throw new IllegalArgumentException("ContactID must be supplied");
		}
		com.sok.runway.crm.Contact c = new com.sok.runway.crm.Contact(conn, contactID);
		if(!c.isLoaded()) {
			throw new NotFoundException("The contact could not be found for id " + contactID);
		}
		if(!user.getUser().canView(c)) {
			throw new AccessException("You do not have permission to view the contact with id " + contactID);
		} 
    	return BeanFactory.getContactFromTableData(c.getRecord());
    }

	public List<ContactProfile> updateProfiles(final HttpServletRequest request, final String contactID, final List<ContactProfile> profiles) {
		logger.trace("updateProfiles(Request, {}, {})", contactID, profiles);
		updateProfiles(ActionBean.getConnection(request), us.getCurrentUser(request), contactID, profiles);
		//not sure the right way to do this. 
		return ContactResource.getInstance().getContactProfile(request, contactID);
	}
	
	private void updateProfiles(final Connection conn, final UserBean user, final String contactID, final List<ContactProfile> profiles) {
		logger.trace("updateProfiles(Connection, {}, {}, Profiles)", user != null ? user.getName() : "null user", contactID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Contacts")) {
			throw new AccessException("You do not have permission to access the contact module");
		}
		
		if(StringUtils.isBlank(contactID)) {
			throw new IllegalArgumentException("ContactID must be supplied");
		}
		if(profiles == null) {
			logger.error("updateProfiles with empty profile list - returning []");
			return;
		}
		com.sok.runway.crm.Contact c = new com.sok.runway.crm.Contact(conn, contactID);
		if(!c.isLoaded()) {
			throw new NotFoundException("The contact could not be found for id " + contactID);
		}
		if(!user.canEdit(c.getRecord())) {
			throw new AccessException("You do not have permission to edit the contact with id " + contactID);
		} 
		
		for(ContactProfile cp: profiles) {
			for(ContactQuestion cq : cp.getQuestions()) {
				//i guess? 
				c.setProfileField(cq.getQuestionID(), cq.getAnswer().getAnswer());
			}
		}
	}	
	
	public List<Contact> getLinkedContacts(final HttpServletRequest request, final String contactID) { 
		logger.trace("getLinkedContacts(Request, {}", contactID);
		return getLinkedContacts(ActionBean.getConnection(request), us.getCurrentUser(request), contactID);
	}
	
	public List<Contact> getLinkedContacts(final Connection con, final UserBean user, final String contactID) {
		logger.trace("getContactLocations(Conn, {}, {}", user != null ? user.getName() : "null user", contactID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Contacts")) {
			throw new AccessException("You do not have permission to access the contacts module");
		}
		if(StringUtils.isBlank(contactID)) {
			throw new IllegalArgumentException("A contactID must be supplied");
		}
		com.sok.runway.crm.Contact c = new com.sok.runway.crm.Contact(con, contactID);
		if(!c.isLoaded()) {
			throw new NotFoundException("The contact was not found " + contactID);
		}
		if(!user.canView(c.getRecord()) && !user.canViewAssigned(c.getRecord())) {
			throw new AccessException("You do not have permission to view this contact");
		}
		
		GenRow links = new GenRow();
		try {
			links.setConnection(con);
			links.setViewSpec("ContactListView");
			user.setConstraint(links);
			links.setParameter("-join1", "ContactLinkee");
			links.setParameter("-join2", "ContactLinkee.ContactLinkType");
			links.setParameter("ContactLinkee.LinkerID", contactID);
			links.setParameter("-select0", "ContactLinkee.LinkID");
			links.setParameter("-select1","ContactLinkee.ContactLinkType.LinkType");
			/*
			String[] fields = new String[]{"ContactID","FirstName","LastName","Email"};
			int a = 2, max = fields.length -1;
			
			for(int i=2; ; i++) {
				links.setParameter("-select" + (i+a), "LinkeeContact." + fields[i]);
				if(i == max) 
					break;
			}
			*/
			if(logger.isDebugEnabled()) {
				links.doAction(GenerationKeys.SEARCH);
				logger.debug(links.getStatement());
			}
			
			links.getResults(true);
			
			if(links.getNext()) {
				List<Contact> list = new java.util.ArrayList<Contact>(links.getSize());
				do {
					LinkedContact lc = new LinkedContact();
					lc.setLinkType(links.getString("LinkType"));
					
					list.add(BeanFactory.setContactFromTableData(lc, links));
				} while(links.getNext());
				return list;
			}
			return java.util.Collections.emptyList();
		} finally {
			links.close();
		}	
	}
	
	@XmlAccessorType(XmlAccessType.NONE)
	public static class LinkedContact extends Contact {
		
		private String linkType;

		@JsonProperty("LinkType")
		public String getLinkType() {
			return linkType;
		}

		@JsonProperty("LinkType")
		public void setLinkType(String linkType) {
			this.linkType = linkType;
		}
	}
	
	
	public List<ContactLocation> getContactLocations(final HttpServletRequest request, final String contactID) { 
		logger.trace("getContactLocations(Request, {}", contactID);
		return getContactLocations(ActionBean.getConnection(request), us.getCurrentUser(request), contactID);
	}
	
	public List<ContactLocation> getContactLocations(final Connection con, final UserBean user, final String contactID) {
		logger.trace("getContactLocations(Conn, {}, {}", user != null ? user.getName() : "null user", contactID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Contacts")) {
			throw new AccessException("You do not have permission to access the contacts module");
		}
		if(StringUtils.isBlank(contactID)) {
			throw new IllegalArgumentException("A contactID must be supplied");
		}
		com.sok.runway.crm.Contact c = new com.sok.runway.crm.Contact(con, contactID);
		if(!c.isLoaded()) {
			throw new NotFoundException("The contact was not found " + contactID);
		}
		c.setCurrentUser(user.getUser());
		if(!c.canView()) {
			throw new AccessException("You do not have permission to view this contact");
		}
		GenRow locations = new GenRow();
		try {
			locations.setConnection(con);
			locations.setViewSpec("ContactLocationView");
			locations.setParameter("ContactID", contactID);
			locations.getResults(true);
			
			if(locations.getNext()) {
				List<ContactLocation> list = new java.util.ArrayList<ContactLocation>(locations.getSize());
				do {
					list.add(new ContactLocation(locations));
				} while(locations.getNext());
				return list;
			}
			return java.util.Collections.emptyList();
		} finally {
			locations.close();
		}	
	}
	
	public List<Contact> getDuplicateContactMatches(final HttpServletRequest request) {
		GenRow req = new GenRow();
		req.parseRequest(request);
		return getDuplicateContactMatches(ActionBean.getConnection(request), us.getCurrentUser(request), req);
	}
	
	public List<Contact> getDuplicateContactMatches(final Connection con, final UserBean user, final GenRow req) {

		//Set<String> dupcontacts = new HashSet<String>();
		
		Map<String, DuplicateContact> dupMap = new HashMap<String, DuplicateContact>();

		final String email = StringUtils.trimToNull(req.getString("Email"));
		final String mobile = StringUtils.trimToNull(req.getString("Mobile"));
		//boolean detailscheckok = true;
		//boolean mobilecheckok = true;
		//boolean emailcheckok = true;
		
		GenRow contacts = new GenRow();
		contacts.setViewSpec("ContactListView");
		
		final int max = 11;
		
		if(email != null) { 
			try { 
				contacts.setConnection(con);
				contacts.setParameter("Email|1", "%" + email + "%");
				contacts.setTop(11);
				contacts.getResults();
				if(contacts.getNext()) {
					do { 
						//contacts.setParameter("Email", contacts.getData("Email"));
						DuplicateContact dc = new DuplicateContact();
						BeanFactory.setContactFromTableData(dc, contacts);
						dc.setEmailMatched(true);
						dupMap.put(contacts.getData("ContactID"), dc);
					} while (contacts.getNext() && dupMap.size() != max);
				}
			} finally {
				contacts.close();
			}
			contacts.clear();
		}
		if(mobile != null) {
			try { 
				contacts.setConnection(con);
				contacts.setParameter("Mobile|1", getMobileSearch(mobile));
				contacts.setTop(11);
				contacts.getResults();
				if(contacts.getNext()) {
					do { 
						//contacts.setParameter("Mobile", contacts.getData("Mobile"));
						if(dupMap.containsKey(contacts.getData("ContactID"))) {
							dupMap.get(contacts.getData("ContactID")).setMobileMatched(true);
						} else {
							DuplicateContact dc = new DuplicateContact();
							BeanFactory.setContactFromTableData(dc, contacts);
							dc.setMobileMatched(true);
							dupMap.put(contacts.getData("ContactID"), dc);
						}
					} while (contacts.getNext() && dupMap.size() != max);
				}
			} finally {
				contacts.close();
			}
		}
		if(dupMap.size() == 0) {
			return Collections.emptyList();
		}
		return new ArrayList<Contact>(dupMap.values());
	}
	
	@XmlAccessorType(XmlAccessType.NONE)
	public static class DuplicateContact extends Contact {
		
		private boolean mobileMatched;
		private boolean emailMatched;
		@JsonProperty("MobileMatched")
		public boolean isMobileMatched() {
			return mobileMatched;
		}
		@JsonProperty("MobileMatched")
		public void setMobileMatched(boolean mobileMatched) {
			this.mobileMatched = mobileMatched;
		}
		@JsonProperty("EmailMatched")
		public boolean isEmailMatched() {
			return emailMatched;
		}
		@JsonProperty("EmailMatched")
		public void setEmailMatched(boolean emailMatched) {
			this.emailMatched = emailMatched;
		}
	}
	
	
	/* pull from contactvalidateduplicate */
	
	String getMobileSearch(String mobile){
		   String temp = StringUtil.toDecimal(mobile);
		   if(temp.length()!=0){
		      StringBuffer searchstr = new StringBuffer();

			   char[] nums = temp.toCharArray();
				int count = 0;
			   for(int i=nums.length -1; i>=0; i--){

			      if(i!=0 || nums[i]!='0'){
			         if(count==0 || count==3 || count==6 || count==9){
				      	searchstr.insert(0, '%');
			         }
				      searchstr.insert(0, nums[i]);

			      }
			      count++;
			   }
			   if('%' != searchstr.charAt(0)){
				   searchstr.insert(0, '%');
			   }
			   return(searchstr.toString());
		   
		   }
		   return("RETURNNOTHING");
		}
	
	/* pull from contactgroupaction */
	
	public void createGroupStatus(Connection con, TableData row, UserBean currentuser)
	{
		GenRow groupStatus = new GenRow();
		groupStatus.setConnection(con);
		groupStatus.setViewSpec("ContactGroupStatusView");
		groupStatus.setToNewID("GroupStatusID");
		groupStatus.setParameter("ContactGroupID",row.getString("ContactGroupID"));
		groupStatus.setParameter("StatusID",row.getString("StatusID"));
		
        setAlerts(con, groupStatus, row);
        groupStatus.setParameter("CreatedDate","NOW");
        groupStatus.setParameter("CreatedBy",currentuser.getString("UserID"));
		groupStatus.setAction(ActionBean.INSERT);
		groupStatus.doAction();
		row.setColumn("ContactGroupStatusID", groupStatus.getString("GroupStatusID"));
		
		if(row.getAction().equals("update")){
		   StatusUpdater.updateContactQualifiedDetails(con, row, currentuser.getCurrentUserID());
		}
	}

	public void setAlerts(Connection con, GenRow status, TableData row)
	{
		StatusUpdater.setAlerts(con, status, row.getString("StatusID"), row.getString("GroupID"));
	}

	public GenRow createContactGroup(Connection con, UserBean currentuser, String contactID, String groupID, String repUserID, String statusID) {
		return createContactGroup(con, currentuser, contactID, groupID, repUserID, statusID, true);
	}
	
	public GenRow createContactGroup(Connection con, UserBean currentuser, String contactID, String groupID, String repUserID, String statusID, boolean allowUpdate) {
		  
		  boolean allowSame = "true".equals(InitServlet.getSystemParam("ContactAllowSameGroupSelection")); 
		
		  GenRow row = new GenRow();
		  row.setViewSpec("ContactGroupView");
		  row.setConnection(con);
		  row.setParameter("ContactID", contactID);
		  row.setParameter("GroupID", groupID);
		  if(allowSame) row.setParameter("RepUserID", repUserID);
		  row.doAction(GenerationKeys.SELECTFIRST);
		  
		  if(!row.isSuccessful()) {
			  //group did not exist 
			  if(!allowSame) row.setParameter("RepUserID", repUserID); 
			  row.setParameter("CreatedDate","NOW");
			  row.setParameter("CreatedBy", currentuser.getString("UserID"));
			  row.setToNewID("ContactGroupID");
			  row.setAction(GenerationKeys.INSERT);
			  
			  row.setParameter("StatusID", statusID);
			  createGroupStatus(con, row, currentuser);
		  } else if (allowUpdate) {
			  if(!StringUtils.equals(row.getData("RepUserID"), repUserID)) {
				  if(!allowSame) row.setParameter("RepUserID", repUserID); 
				  row.setAction(GenerationKeys.UPDATE);
			  }
			  if(row.isSet("ContactGroupStatusID")) {
				  GenRow check = new GenRow(); 
				  check.setConnection(con);
				  check.setViewSpec("GroupStatusView");
				  check.setParameter("GroupStatusID", row.getData("ContactGroupStatusID")); 
				  check.doAction(GenerationKeys.SELECTFIRST);
				  
				  if(!check.isSuccessful() || (StringUtils.isNotBlank(statusID) && !StringUtils.equals(check.getData("StatusID"), statusID))) {
					  row.setParameter("StatusID", statusID);
					  createGroupStatus(con, row, currentuser);
					  row.setAction(GenerationKeys.UPDATE);
				  }
			  }
		  }
		  if(!GenerationKeys.SELECTFIRST.equals(row.getAction())) { //ie if there is an update or insert
			  try {
				  row.doAction(); 
			  } catch (DatabaseException de) {
				  Throwable cause = de.getCause();
				  if(cause != null && cause instanceof SQLIntegrityConstraintViolationException) {
					  row.clear();
					  row.setParameter("-select1","ContactGroupID");
					  row.setParameter("ContactID", contactID);
					  row.setParameter("GroupID", groupID);
					  row.setParameter("RepUserID", repUserID);
					  row.doAction(GenerationKeys.SELECTFIRST);
					  return row;
				  }
				  throw de;
			  }
		  }
		  return row;
	  }
	  
	  public void checkContactShortlist(Connection con, UserBean user, final String contactID, final String productID) {
		  checkContactShortlist(con, user, contactID, productID, false);
	  }
	  /**
	   * @deprecated - string as boolean value
	   */
	  public void checkContactShortlist(Connection con, UserBean user, final String contactID, final String productID, String isWebShortListRequest) {
		  checkContactShortlist(con, user, contactID, productID, "Yes".equalsIgnoreCase(isWebShortListRequest));
	  }
	  
	  public void checkContactShortlist(Connection con, UserBean user, final String contactID, final String productID, String isWebShortListRequest, String regionID) {
		  checkContactShortlist(con, user, contactID, productID, "Yes".equalsIgnoreCase(isWebShortListRequest), regionID);
	  }
	  
	  public void checkContactShortlist(Connection con, UserBean user, final String contactID, final String productID, boolean isWebShortListRequest, String regionID)
	  {
		  GenRow product = new GenRow();
		  product.setConnection(con);
		  product.setTableSpec("Products"); 
		  product.setParameter("-select1","ProductType");
		  product.setParameter("ProductID", productID);
		  product.doAction("selectfirst");
		  
		  logger.debug("checkContactShortlist(Con, {},)", contactID, productID);
		  logger.debug("checkContactShortlist(Con, {}, {}, {})", isWebShortListRequest, regionID);
		  if(StringUtils.isNotBlank(contactID) && StringUtils.isNotBlank(productID)) {
				GenRow worker = new GenRow();
				worker.setConnection(con);
				worker.setTableSpec("ContactProducts"); 
				worker.setParameter("-select1","ContactProductID");
				worker.setParameter("ContactID", contactID);
				worker.setParameter("ProductID", productID);
				if(StringUtils.isNotBlank(regionID) && !"null".equalsIgnoreCase(regionID) && !"House and Land".equals(product.getString("ProductType")))
					worker.setParameter("RegionID", regionID);
				worker.setParameter("RepUserID", user.getUserID());
				worker.doAction(GenerationKeys.SELECTFIRST);
				if(!worker.isSuccessful()) { 
					logger.debug("Creating contact link");
					worker.createNewID();
					
					if (isWebShortListRequest)
					{
						worker.setParameter("LinkType", "WebShortList");
					}
					else
					{
						worker.setParameter("LinkType", "shortlist");
					}	
					
					worker.setParameter("CreatedDate", "NOW");
					worker.setParameter("CreatedBy", user.getUserID());
					try {
						worker.doAction(GenerationKeys.INSERT);
					} catch (DatabaseException de) {
						Throwable cause = de.getCause();
						if(cause != null && cause instanceof SQLIntegrityConstraintViolationException) {
							  /* this is fine, if there already is one we can lose this exception */
						} else {
							throw de;
						}
					}
				}
				
				worker = new GenRow();
				worker.setConnection(con);
				worker.setTableSpec("ContactGroups"); 
				worker.setParameter("-select1","ContactGroupID");
				worker.setParameter("ContactID", contactID);
				// We want to check if the user is a rep for this contact, we don't care which group it is.
				// This could be changed in future to be a group attached to a product, perhaps
				//worker.setParameter("GroupID", user.getString("DefaultGroupID"));
				worker.setParameter("RepUserID", user.getUserID());
				worker.doAction(GenerationKeys.SELECTFIRST);
				if(!worker.isSuccessful()) { 
					if("true".equals(InitServlet.getSystemParam("CRM-IncludeRepGroup"))) {
						createContactGroup(con, user, contactID, user.getString("DefaultGroupID"), user.getUserID(), InitServlet.getSystemParam("ContactRepDefaultStatusID"), false);
					}
				}
			}
	  }
	  
	  public void checkContactShortlist(Connection con, UserBean user, final String contactID, final String productID, boolean isWebShortListRequest) {
		  checkContactShortlist(con, user, contactID, productID, isWebShortListRequest, null);
	  }
	  
	  private void createContactGroups(Connection con, UserBean currentuser, String contactID, String[] groupIDs, String[] repUserIDs, String[] statusIDs) {
		    
		    if(groupIDs != null && repUserIDs != null && statusIDs != null) {
		       
		      GenRow contactGroup = new GenRow();
		      contactGroup.setTableSpec("ContactGroups");
		      contactGroup.setConnection(con);
		      contactGroup.setParameter("ContactID", contactID);
		       
		      for(int i = 0; i < groupIDs.length; i++) {
		        final String groupID = (i < groupIDs.length) ? groupIDs[i] : "";
		        final String repUserID = (i < repUserIDs.length) ? repUserIDs[i] : "";
		        final String statusID = (i < statusIDs.length) ? statusIDs[i] : "";
		        
		        contactGroup.setParameter("GroupID", groupID);
		        contactGroup.setParameter("RepUserID", repUserID);  
		        contactGroup.setParameter("StatusID", statusID);
				contactGroup.setParameter("CreatedDate","NOW");
				contactGroup.setParameter("CreatedBy", currentuser.getString("UserID"));
				contactGroup.setToNewID("ContactGroupID");
				createGroupStatus(con, contactGroup, currentuser);
				contactGroup.setAction(GenerationKeys.INSERT);
				contactGroup.doAction(); 
		      }
		      contactGroup.close();
		    }
		  }
}
