package com.sok.service.crm.cms.properties;

import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.beans.PropertyOption;
import com.sok.runway.externalInterface.beans.PropertyOptionCategory;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class PropertyOptionPDHService {

	private static final PropertyOptionPDHService service = new PropertyOptionPDHService();
	private static final Logger logger = LoggerFactory.getLogger(PropertyOptionPDHService.class);
	private static final UserService us = UserService.getInstance();
	public static PropertyOptionPDHService getInstance() {
		return service;
	}

	public List<PropertyOptionCategory> getPropertyOptionCategories(Connection conn, UserBean user) {
		logger.debug("getPropertyOptionCategories(Connection, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		final GenRow worker = getPropertyOptionCategoryGenRow();
		try { 
			if(conn != null) {
				worker.setConnection(conn);
			} else {
				ActionBean.connect(worker);
			}
			worker.setParameter("-sort1","ProductGroups.Description");
			if(logger.isTraceEnabled()) { 
				worker.doAction(GenerationKeys.SEARCH);
				logger.trace(worker.getStatement());
			}
			worker.getResults(true);
			if(worker.getNext()) {
				final List<PropertyOptionCategory> list = new ArrayList<PropertyOptionCategory>(worker.size());
				do {
					list.add(new PropertyOptionCategory(worker));
				} while(worker.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			worker.close();
		}
	}

	public List<PropertyOption> getPropertyOptions(Connection connection, UserBean user, String productGroupId, String search) {
		return getPropertyOptions(connection, user, productGroupId, search, null);
	}

	public List<PropertyOption> getPropertyOptions(Connection connection, UserBean user, String productGroupId, String search, String orderId) {		
		logger.debug("getPropertyOptions(Connection, {}, {})", user != null ? user.getName() : "null user", productGroupId);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		if(StringUtils.isBlank(productGroupId)) {
			throw new IllegalArgumentException("No product group supplied");
		}

		HashSet<String> keys = new HashSet<String>();
		String productKey = "";
		String groupID = ""; 
		if (StringUtils.isNotBlank(orderId)) {
			String productNum = getOrderProductNum(orderId);
			groupID = getOrderGroupID(orderId);
			if (StringUtils.isNotBlank(productNum)) {
				if (productNum.indexOf("-") >= 0) productNum = productNum.substring(0,productNum.indexOf("-"));
				GenRow row = new GenRow();
				row.setTableSpec("ListItems");
				row.setConnection(connection);
				row.setParameter("-select0", "ItemLabel");
				row.setParameter("-select1", "ItemValue");
				row.setParameter("ListID", "0P1G2B9P804B384L9O5T828Q4N8P123");
				row.doAction("search");

				row.getResults();

				while (row.getNext()) {
					if (row.isSet("ItemLabel") && row.isSet("ItemValue")) {
						keys.add(row.getString("ItemLabel").substring(0,3).toUpperCase());
						if (row.getString("ItemValue").equalsIgnoreCase(productNum)) productKey = row.getString("ItemLabel");
					}
				}

				row.close();
			}
		}

		GenRow worker = getPropertyOptionGenRow();
		try {
			worker.setConnection(connection);
			if (StringUtils.isNotBlank(groupID)) worker.setParameter("GroupID", groupID + "+NULL+EMPTY"); 
			boolean proceed = false;
			if(!"ALL".equals(productGroupId)) {
				worker.setParameter("ProductGroupID", productGroupId);
				proceed = true;
			}
			if(StringUtils.isNotBlank(search)) {
				if(search.trim().indexOf(" ") != -1) { 
					String[] st = search.trim().split(" ");
					int m = st.length - 1;
					if(m != -1) {
						for(int i=0; ; i++) {
							worker.setParameter("Name^11|1", "%" + st[i] + "%");
							worker.setParameter("Description^22|1", "%" + st[i] + "%");
							if(i == m) break;
						}
					}
				} else { 
					worker.setParameter("Name|1", "%" + search + "%");
					worker.setParameter("Description|1", "%" + search + "%");
				}
				proceed = true;
			}
			String regionID = "";
			if (StringUtils.isNotBlank(orderId)) {
				regionID = getOrderRegion(orderId);
			}
			if(logger.isTraceEnabled()) {
				worker.doAction(GenerationKeys.SEARCH);
				logger.trace(worker.getStatement());
			}

			if(proceed) 
				worker.getResults(true);
			if(worker.getNext()) {
				final List<PropertyOption> list = new ArrayList<PropertyOption>(worker.size());
				do {
					boolean isCustom = worker.getData("Name") != null && keys.contains(worker.getData("Name").toUpperCase().substring(0,3)) && !worker.getData("Name").toUpperCase().startsWith(productKey.toUpperCase());
					PropertyOption po = new PropertyOption(worker, isCustom);
					if (StringUtils.isNotBlank(regionID)) {
						double tc = getRegionPrice(po.ProductID, regionID);
						if (tc != 0) po.setTotalCost(tc);
					}
					list.add(po);
				} while(worker.getNext());
				return list;
			}
		} finally {
			worker.close();
		}
		return Collections.emptyList();
	}

	private double getRegionPrice(String productID, String regionID) {
		if (StringUtil.isBlankOrEmpty(productID) || StringUtil.isBlankOrEmpty(regionID)) return 0;
		GenRow row = new GenRow();
		row.setTableSpec("ProductRegionPrice");
		row.setParameter("-select0", "TotalCost");
		row.setParameter("ProductID", productID);
		row.setParameter("RegionID", regionID);
		row.doAction("selectfirst");

		return row.getDouble("TotalCost");
	}

	private String getOrderProductNum(String orderId) {
		if (StringUtil.isBlankOrEmpty(orderId)) return "";
		GenRow row = new GenRow();
		row.setTableSpec("Orders");
		row.setParameter("-select0", "OrderProduct.ProductNum AS ProductNum");
		row.setParameter("OrderID", orderId);
		row.doAction("selectfirst");

		return row.getString("ProductNum");
	}

	private String getOrderGroupID(String orderId) {
		if (StringUtil.isBlankOrEmpty(orderId)) return "";
		GenRow row = new GenRow();
		row.setTableSpec("Orders");
		row.setParameter("-select0", "GroupID");
		row.setParameter("OrderID", orderId);
		row.doAction("selectfirst");

		return row.getString("GroupID");
	}

	private String getOrderRegion(String orderId) {
		if (StringUtil.isBlankOrEmpty(orderId)) return "";
		GenRow row = new GenRow();
		row.setTableSpec("Orders");
		row.setParameter("-select0", "RegionID");
		row.setParameter("OrderID", orderId);
		row.doAction("selectfirst");

		return row.getString("RegionID");
	}

	public PropertyOptionCategory getPropertyOptionCategory(Connection conn, UserBean user, String category, String categoryId) {
		if(logger.isDebugEnabled()) logger.debug("getPropertyOptionCategory(Connection, {}, {},{})", new String[]{user != null ? user.getName() :"null user", category, categoryId});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		if(StringUtils.isBlank(category) || StringUtils.isBlank(categoryId)) {
			logger.debug("getPropertyOptionCategory({},{}) returning null", category, categoryId);
			throw new RuntimeException("Property cat or categoryId were null");
		}
		final GenRow worker = getPropertyOptionCategoryGenRow();
		try { 
			if(conn != null) {
				worker.setConnection(conn);
			} else {
				ActionBean.connect(worker);
			}
			worker.setParameter("Description", category);
			worker.doAction(GenerationKeys.SELECTFIRST);

			if(!worker.isSuccessful()) {
				worker.setToNewID("ProductGroupID");
				worker.setParameter("GroupName", category);
				worker.setParameter("GroupID", InitServlet.getSystemParam("DefaultGroupID"));

				worker.setParameter("ModifiedBy", user.getUserID());
				worker.setParameter("CreatedBy", user.getUserID());
				worker.setParameter("ModifiedDate", "NOW");
				worker.setParameter("CreatedDate", "NOW");

				worker.doAction(GenerationKeys.INSERT);
				if(!worker.isSuccessful()) {
					throw new RuntimeException("Failed to insert PropertyOptionCategory " + worker.toString());
				}
				//load up categoryid, name, num 
				worker.doAction(GenerationKeys.SELECT);
			}
			if(!worker.getString("GroupName").equals(category)) {
				logger.error("Inconsistent data for categoryID {} found {} expected {}", new String[]{categoryId, worker.getString("GroupName"), category});
				throw new RuntimeException("Inconsistent data for categoryId : " + categoryId);
			}
			return new PropertyOptionCategory(worker);
		} finally {
			worker.close();
		}
	}

	private GenRow getPropertyOptionCategoryGenRow() {
		final GenRow worker = new GenRow();
		worker.setTableSpec("ProductGroups");
		worker.setParameter("-select0","ProductGroups.ProductGroupID as CategoryID");
		worker.setParameter("-select1","ProductGroups.GroupName as CategoryName");
		worker.setParameter("-select2","ProductGroups.Description as CategoryNum");
		worker.setParameter("Active","Y");
		return worker;
	}

	private GenRow getPropertyOptionGenRow() {
		final GenRow displayOptions = new GenRow();
		displayOptions.setTableSpec("Products");
		displayOptions.setParameter("Active","Y");
		displayOptions.setParameter("-join1", "ProductImage");
		displayOptions.setParameter("-join2", "ProductProductGroup");
		displayOptions.setParameter("-join3","LinkedProductProductDisplayOptions");
		displayOptions.setParameter("ProductType", "Property Option"); // ??
		displayOptions.setParameter("ProductProductGroup.Active","Y");

		displayOptions.setParameter("-select1","Products.ProductID");
		displayOptions.setParameter("-select2","Products.ProductNum");
		displayOptions.setParameter("-select3","Products.Name");
		displayOptions.setParameter("-select4","ProductImage.FilePath as ImagePath");
		displayOptions.setParameter("-select5","Products.Cost as Cost");
		displayOptions.setParameter("-select6","Products.GST as GST");
		displayOptions.setParameter("-select7","Products.TotalCost as TotalCost");
		displayOptions.setParameter("-select8","ProductProductGroup.ProductGroupID as CategoryID");
		displayOptions.setParameter("-select9","ProductProductGroup.GroupName as CategoryName");
		displayOptions.setParameter("-select10","ProductProductGroup.Description as CategoryNum");
		displayOptions.setParameter("-select11","Products.Description as Description");

		//relation for quantity
		displayOptions.setParameter("-select12","LinkedProductProductDisplayOptions.Quantity as Quantity");

		//pricing type 
		displayOptions.setParameter("-select13","Products.PricingType as PricingType");

		return displayOptions;
	}

	public List<PropertyOption> getDisplayPropertyOptions(HttpServletRequest request, String displayProductID) {
		return getDisplayPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), displayProductID, null, null, null, null);
	}

	public List<PropertyOption> getDisplayPropertyOptions(HttpServletRequest request, String displayProductID, String categoryId, String search) {
		return getDisplayPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), displayProductID, categoryId, search, null, null);
	}

	public List<PropertyOption> getDisplayPropertyOptions(HttpServletRequest request, String displayProductID, String categoryId, String search, String type) {
		return getDisplayPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), displayProductID, categoryId, search, type, null);
	}

	public List<PropertyOption> getDisplayPropertyOptions(Connection con, UserBean user, String displayProductID, String categoryId, String search, String type, String orderId) {
		if(logger.isTraceEnabled()) logger.trace("getDisplayPropertyOptions(Connection, {}, {},{}, {})", new String[]{user != null ? user.getName() :"null user", displayProductID, categoryId, search});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		final GenRow displayOptions = getPropertyOptionGenRow();
		
		try { 
			displayOptions.setConnection(con);

			displayOptions.setParameter("-select14", "LinkedProductProductDisplayOptions.SortOrder AS OptionSortOrder");
			displayOptions.setParameter("-select15", "LinkedProductProductDisplayOptions.Type AS Type");

			if("display".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type", "O+D");
			} else if("estate".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type", "E");
			} else if("option".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type","O");
			} else if("decor".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type","D");
			}

			displayOptions.sortBy("OptionSortOrder",0);
			//displayOptions.setParameter("-join4", "ProductLinkedProducts");
			displayOptions.setParameter("LinkedProductProductDisplayOptions.ProductID", displayProductID);

			if(StringUtils.isNotBlank(categoryId) && !"ALL".equals(categoryId)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.ProductDisplayOptionLinkedProducts.ProductGroupID", categoryId);
			}
			if(StringUtils.isNotBlank(search)) {
				displayOptions.setParameter("Name|1", "%" + search + "%");
				displayOptions.setParameter("Description|1", "%" + search + "%");
			}

			if(logger.isTraceEnabled()) {
				displayOptions.doAction("search");
				logger.trace(displayOptions.getStatement());
			}

			displayOptions.getResults(true);
			if(displayOptions.getNext()) {
				HashSet<String> keys = new HashSet<String>();
				String productKey = "";
				if (StringUtils.isNotBlank(orderId)) {
					String productNum = getOrderProductNum(orderId);
					if (StringUtils.isNotBlank(productNum)) {
						GenRow row = new GenRow();
						row.setTableSpec("ListItems");
						row.setConnection(con);
						row.setParameter("-select0", "ItemLabel");
						row.setParameter("-select1", "ItemValue");
						row.setParameter("ListID", "0P1G2B9P804B384L9O5T828Q4N8P123");
						row.doAction("search");

						row.getResults();

						while (row.getNext()) {
							if (row.isSet("ItemLabel") && row.isSet("ItemValue")) {
								keys.add(row.getString("ItemLabel").substring(0,3).toUpperCase());
								if (row.getString("ItemValue").equalsIgnoreCase(productNum)) productKey = row.getString("ItemLabel");
							}
						}

						row.close();
					}
				}

				final List<PropertyOption> list = new ArrayList<PropertyOption>(displayOptions.size());
				do {
					boolean isCustom = displayOptions.getData("Name") != null && keys.contains(displayOptions.getData("Name").toUpperCase().substring(0,3)) && !displayOptions.getData("Name").toUpperCase().startsWith(productKey.toUpperCase()); 
					list.add(new PropertyOption(displayOptions, isCustom));
				}while(displayOptions.getNext());
				return list;
			} else {
				return getDisplayPropertyOptionsByAddress(con, user, displayProductID, categoryId, search, type, orderId);
			}
		} finally {
			displayOptions.close();
		}
		//return java.util.Collections.emptyList();
	}

	public List<PropertyOption> getDisplayPropertyOptionsByAddress(Connection con, UserBean user, String address, String categoryId, String search, String type) {
		return getDisplayPropertyOptionsByAddress(con, user, address, categoryId, search, type, null);
	}
	
	public List<PropertyOption> getDisplayPropertyOptionsByAddress(Connection con, UserBean user, String address, String categoryId, String search, String type, String orderId) {
		if(logger.isTraceEnabled()) logger.trace("getDisplayPropertyOptions(Connection, {}, {},{}, {})", new String[]{user != null ? user.getName() :"null user", address, categoryId, search});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		final GenRow displayOptions = getPropertyOptionGenRow();
		try { 
			displayOptions.setConnection(con);
			displayOptions.setParameter("-select14", "LinkedProductProductDisplayOptions.SortOrder AS OptionSortOrder");
			displayOptions.setParameter("-select15", "LinkedProductProductDisplayOptions.Type AS Type");

			displayOptions.sortBy("OptionSortOrder",0);
			//displayOptions.setParameter("-join4", "ProductLinkedProducts");
			displayOptions.setParameter("LinkedProductProductDisplayOptions.Address", address);
			if(StringUtils.isNotBlank(search)) {
				displayOptions.setParameter("Name|1", "%" + search + "%");
				displayOptions.setParameter("Description|1", "%" + search + "%");
			}
			if(StringUtils.isNotBlank(categoryId) && !"ALL".equals(categoryId)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.ProductDisplayOptionLinkedProducts.ProductGroupID", categoryId);
			}

			if("display".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type", "O+D");
			} else if("estate".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type", "E");
			} else if("option".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type","O");
			} else if("decor".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type","D");
			}


			HashSet<String> keys = new HashSet<String>();
			String productKey = "";
			if (StringUtils.isNotBlank(orderId)) {
				String productNum = getOrderProductNum(orderId);
				if (StringUtils.isNotBlank(productNum)) {
					GenRow row = new GenRow();
					row.setTableSpec("ListItems");
					row.setConnection(con);
					row.setParameter("-select0", "ItemLabel");
					row.setParameter("-select1", "ItemValue");
					row.setParameter("ListID", "0P1G2B9P804B384L9O5T828Q4N8P123");
					row.doAction("search");

					row.getResults();

					while (row.getNext()) {
						if (row.isSet("ItemLabel") && row.isSet("ItemValue")) {
							keys.add(row.getString("ItemLabel").substring(0,3).toUpperCase());
							if (row.getString("ItemValue").equalsIgnoreCase(productNum)) productKey = row.getString("ItemLabel");
						}
					}

					row.close();
				}
			}

			displayOptions.getResults(true);
			if(displayOptions.getNext()) {
				final List<PropertyOption> list = new ArrayList<PropertyOption>(displayOptions.size());
				do {
					boolean isCustom = displayOptions.getData("Name") != null && keys.contains(displayOptions.getData("Name").toUpperCase().substring(0,3)) && !displayOptions.getData("Name").toUpperCase().startsWith(productKey.toUpperCase()); 
					list.add(new PropertyOption(displayOptions, isCustom));
				}while(displayOptions.getNext());
				return list;
			} 
		} finally {
			displayOptions.close();
		}
		return java.util.Collections.emptyList();
	}

	public PropertyOption getPropertyOption(Connection conn, UserBean user, String category, String categoryId, String name, double cost) {
		if(logger.isTraceEnabled()) logger.trace("getDisplayPropertyOptions(Connection, {}, {},{},{},{})", new String[]{user != null ? user.getName() :"null user", category, categoryId, name, String.valueOf(cost)});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		PropertyOptionCategory poc = getPropertyOptionCategory(conn, user, category, categoryId);
		final GenRow worker = getPropertyOptionGenRow();
		try { 
			if(conn != null) {
				worker.setConnection(conn);
			} else {
				ActionBean.connect(worker);
			}
			worker.setParameter("ProductGroupID", poc.CategoryID);
			worker.setParameter("Name", name);
			worker.doAction(GenerationKeys.SELECTFIRST);

			if(!worker.isSuccessful()) {
				worker.setToNewID("ProductID");
				worker.setParameter("Cost", cost);
				worker.setParameter("GroupID", InitServlet.getSystemParam("DefaultGroupID"));
				worker.setParameter("CreatedBy", user.getUserID());
				worker.setParameter("CreatedDate", "NOW");
				worker.setParameter("ModifiedBy", user.getUserID());
				worker.setParameter("ModifiedDate", "NOW");

				worker.doAction(GenerationKeys.INSERT);
				if(!worker.isSuccessful()) {
					throw new RuntimeException("Failed to insert PropertyOptionCategory " + worker.toString());
				}

				worker.doAction(GenerationKeys.SELECT);
			} else {
				worker.setParameter("ModifiedBy", user.getUserID());
				worker.setParameter("ModifiedDate", "NOW");
				worker.setParameter("Cost", cost);
				worker.doAction(GenerationKeys.UPDATE);
			}
			return new PropertyOption(worker);
		} finally {
			worker.close();
		}
	}

	public List<PropertyOption> getDisplayPropertyOptions(
			HttpServletRequest request, String displayProductID, String categoryId,
			String search, String type, String orderId) {
		// TODO Auto-generated method stub
		return getDisplayPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), displayProductID, categoryId, search, type, orderId);
	}
}
