package com.sok.service.crm.cms.properties;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.ErrorMap;
import com.sok.runway.UserBean;
import com.sok.runway.crm.Product;
import com.sok.runway.crm.cms.properties.Land;
import com.sok.runway.crm.cms.properties.entities.BuilderEntity;
import com.sok.service.crm.UserService;

import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.cms.properties.Builder;
import com.sok.runway.externalInterface.beans.cms.properties.HomeDetails;
import com.sok.runway.externalInterface.beans.cms.properties.LegacyLot;
import com.sok.runway.externalInterface.beans.cms.properties.Lot;
import com.sok.runway.externalInterface.beans.cms.properties.Stage;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.offline.rpmManager.RPMtask;

public class BuilderService {

	private static final BuilderService service = new BuilderService();
	private static final Logger logger = LoggerFactory.getLogger(BuilderService.class);
	/* TODO - private */protected static final UserService us = UserService.getInstance();
	public static BuilderService getInstance() {
		return service;
	}
	
	public List<Builder> getBuilders(final HttpServletRequest request) {
		return getBuilders(request, null, true);
	}
	
	/* request methods */
	public List<Builder> getBuilders(final HttpServletRequest request, Date modifiedSince, boolean active) {
		logger.trace("getBuilders(Request, modifiedSince={})", String.valueOf(modifiedSince));
		return getBuilders(ActionBean.getConnection(request), us.getCurrentUser(request), modifiedSince, active);
	}

	public Builder getBuilder(final HttpServletRequest request, final String builderID) {
		logger.trace("getBuilder(Request, {})");
		return getBuilder(ActionBean.getConnection(request), us.getCurrentUser(request), builderID);
	}
	
	/* internal methods  */
	// TODO getBuilderEntities ? 
	public List<Builder> getBuilders(final Connection con, final UserBean user, final Date modifiedSince, final boolean active) {
		logger.trace("getBuilders(Conn, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow builderList = new GenRow();
		builderList.setTableSpec("Products");
		builderList.setParameter("ProductType", "Builder");
		if(modifiedSince != null) { 
			builderList.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		builderList.setParameter("-sort0", "Name");
		builderList.setParameter("-select1","ProductID");
		if(active) {
			builderList.setParameter("Active","Y");
		}

		try {
			builderList.setConnection(con);
			builderList.getResults(true);

			if(builderList.getNext()) {
				List<Builder> list = new java.util.ArrayList<Builder>(builderList.getSize());
				do {
					//check for can view ? 
					list.add(BeanFactory.external(new BuilderEntity(builderList.getConnection(), builderList.getData("ProductID"))));
				}while(builderList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			builderList.close();
		}
	}

	
	public Builder getBuilder(final Connection conn, final UserBean user, final String builderID) {
		logger.trace("getBuilder(Conn, {}, {})", user != null ? user.getName() : "null user", builderID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(builderID)) {
			throw new IllegalArgumentException("You must supply a builderID to use this method");
		}
		BuilderEntity e = new BuilderEntity(conn);
		e.setCurrentUser(user.getUser());
		e.load(builderID);
		if(!e.isLoaded()) {
			throw new NotFoundException("The Builder you requested could not be found: " + builderID);
		}
		if(!e.canView()) {
			throw new AccessException("You do not have permission to view the builder with id " + builderID);
		}
		return BeanFactory.external(e); 
	}	
}
