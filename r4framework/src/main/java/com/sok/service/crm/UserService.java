package com.sok.service.crm;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.LockManager;
import com.sok.runway.SessionKeys;
import com.sok.runway.SessionManager;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.User;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserService extends AbstractService {
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	public Logger getLogger() {
		return logger;
	}
	private static final UserService service = new UserService();
	public static UserService getInstance() {
		return service;
	}
	
	public User getUser(HttpServletRequest request, String userId) {
		logger.debug("UserService.getUser({})", userId);
		return getUser(ActionBean.getConnection(request), getCurrentUser(request), userId);
	}
	
	public String login(HttpServletRequest request, String username, String password) {
		String remoteIP = request.getRemoteAddr();
		if (remoteIP.startsWith("150.70.") || remoteIP.startsWith("216.104.")) {
			logger.info("Login from banned IP {}");
			throw new NotAuthenticatedException("You are not authorised to use this system.");
		} else {
			final UserBean currentuser = new UserBean();
			currentuser.setViewSpec("UserView");
			currentuser.setRequest(request);
			currentuser.setParameter("Username", username);
			currentuser.setParameter("Password", password);

			final String login = currentuser.getLogin(true);

			if(UserBean.authenticated.equals(login)) {
				int timeout = 1800;
				try {
					timeout = Integer.parseInt(InitServlet.getSystemParam("RunwayTimeOut"));	
				}
				catch (NumberFormatException e) { }

				if(currentuser.getString("HomePage").indexOf("campaignmode.jsp")>0){
					currentuser.setColumn("-campaignmode", "true");
				}
				else{
					currentuser.setColumn("-campaignmode", "false");
				}         

				if(request.getParameter("-locationidhidden") != null && (request.getParameter("-locationidhidden")).length() > 0 ){
					currentuser.setColumn("-usercurrentlocation", request.getParameter("-locationidhidden"));
				}else if(request.getParameter("-locationid") != null && (request.getParameter("-locationid")).length() > 0){
					currentuser.setColumn("-usercurrentlocation", request.getParameter("-locationid"));
				}
				HttpSession thissession = request.getSession(true);
				thissession.setAttribute("currentuser",currentuser);
				thissession.setMaxInactiveInterval(timeout);

				/* Added to 3.6.6 to make sure no current locks exist on login */
				LockManager.unlockAll(currentuser);

				SessionManager sessionManager = (SessionManager)InitServlet.getConfig().getServletContext().getAttribute("sessionManager");
				if (sessionManager == null) {
					// if the session manager is not there due to an exception, lets create one and set it
					sessionManager = new SessionManager();
					InitServlet.getConfig().getServletContext().setAttribute("sessionManager",sessionManager);
				}
				sessionManager.setUser(thissession, currentuser);
			}
			return login;
		}
	}
	
	public UserBean getCurrentUser(HttpServletRequest request) { 
		logger.debug("UserResource.getCurrentUser()");
		UserBean currentuser = (UserBean)request.getSession().getAttribute(SessionKeys.CURRENT_USER);
		if(currentuser == null) {	// UserBean will be in request scope if using oauth tokens
			currentuser = (UserBean)request.getAttribute(SessionKeys.CURRENT_USER);
		}
		if(currentuser == null && "true".equals(InitServlet.getSystemParam("DeveloperMode"))) {
			final String sysUserID = com.sok.framework.InitServlet.getSystemParam("SystemUserID");
			logger.debug("using dev mode with user ({})", sysUserID);
			currentuser = new UserBean();
			currentuser.setRequest(request);
			currentuser.setViewSpec("UserView");
			currentuser.setParameter("UserID", com.sok.framework.InitServlet.getSystemParam("SystemUserID"));
			currentuser.doAction(ActionBean.SELECT);
		}
		return currentuser; 
	}
	
	public UserBean getSimpleUser(final Connection con) {
		logger.debug("UserService.getUser({})");
		final String sysUserID = com.sok.framework.InitServlet.getSystemParam("SystemUserID");
		

	 return	getSimpleUser(con, sysUserID);


	}
	
	public UserBean getSimpleUser(final Connection con, String userId) {
		return getSimpleUser(con, userId, false);
	}
	
	public UserBean getSimpleUser(final Connection con, String userId, boolean checkActive) {
		logger.debug("UserService.getUser({})", userId);
		if(StringUtils.isNotBlank(userId)) { 
			UserBean user = new UserBean();
			user.setConnection(con);
			user.setViewSpec("UserView");
			user.setParameter("UserID", userId);
			if (checkActive) user.setParameter("Status", "Active");
			user.doAction(ActionBean.SELECT);
			if(user.isSet("Username")) {
		        return user;
			}
		}
		return null;
	}

	public User getUser(final Connection con, final UserBean currentuser, String userId) {
		if(logger.isDebugEnabled()) logger.debug("UserService.getUser(con, currentuser={}, userID={})", currentuser != null ? currentuser.getName() : "null user", userId);
		if(currentuser == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(StringUtils.isBlank(userId)) {
			throw new IllegalArgumentException("UserID must be supplied");
		}
		boolean isCurrentUser = currentuser.getUserID().equals(userId); 
		if(isCurrentUser) {
			if(currentuser.get("-userVG") != null) {
				return (User)currentuser.get("-userVG");
			}
		}
		if(StringUtils.isNotBlank(userId)) { 
			UserBean user = new UserBean();
			user.setConnection(con);
			user.setViewSpec("UserView");
			user.setParameter("UserID", userId);
			user.doAction(ActionBean.SELECT);
			if(user.isSet("Username")) {
				UserVG userObj = new UserVG();
				BeanFactory.setUserFromTableData(userObj, user);
				userObj.setVisibleGroups(getVisibleGroups(con, currentuser, userId));
				if(isCurrentUser) {
					currentuser.put("-userVG", userObj);
				}
		        return userObj;
			}
		}
		return null;
	}
	
	public User getUser(final Connection con, final UserBean currentuser) {
		return getUser(con, currentuser, (UserBean)null);
	}
	
	public User getUser(final Connection con, final UserBean currentuser, UserBean user) {
		if(currentuser == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(user == null) {
			user = currentuser;
		}
		if(user.isSet("Username")) {
			UserVG userObj = new UserVG();
			BeanFactory.setUserFromTableData(userObj, user);
			userObj.setVisibleGroups(getVisibleGroups(con, currentuser, user.getUserID()));
	        return userObj;
		}
		return null;
	}
	
	private class UserVG extends User {
		
		private List<VisibleGroup> vgList;
		
		@JsonProperty("VisibleGroups")
		public List<VisibleGroup> getVisibleGroups() {
			return vgList;
		}
		@JsonProperty("VisibleGroups")
		public void setVisibleGroups(List<VisibleGroup> vgList) {
			this.vgList = vgList;
		}
	}
	
	//Not for external use - yet 
	private List<VisibleGroup> getVisibleGroups(final Connection con, final UserBean currentuser, final String userId) {
		
		logger.trace("getVisibleGroups(Con, {}, {})", currentuser != null ? currentuser.getName() : "null user", userId); 
		GenRow visibleGroups = new GenRow();
        try { 
	        visibleGroups.setViewSpec("GroupView");
	        visibleGroups.setConnection(con);
	        currentuser.setConstraint(visibleGroups);
	        visibleGroups.sortBy("Groups.Name",0);
	        visibleGroups.sortBy("Groups.SectionCode",1);
	        visibleGroups.setParameter("Applicable" , "Y");
	        visibleGroups.doAction("search");
        	visibleGroups.getResults(true);
        	if(visibleGroups.getNext()) {
        		List<VisibleGroup> groupList = new ArrayList<VisibleGroup>(visibleGroups.getSize());
        		do {
        			groupList.add(new VisibleGroup(visibleGroups, getGroupStatusList(con, currentuser, visibleGroups.getData("GroupID"), GroupStatusType.Contacts)));
        		} while(visibleGroups.getNext());
        		return groupList;
        	}
        	return Collections.emptyList();
        } finally {
        	visibleGroups.close();
        }
	}
	
	private class VisibleGroup {
		public String GroupID;
		public String Name;
		public List<GroupStatus> StatusList;
		
		public VisibleGroup() {}
		public VisibleGroup(GenRow groupView, List<GroupStatus> statusList) {
			this.GroupID = groupView.getData("GroupID");
			this.Name = groupView.getData("Name");
			this.StatusList = statusList;
		}
	}
	
	//Not for external use - yet 	
	private enum GroupStatusType { Contacts, Companies, Process }
	
	//Not for external use - yet 
	private List<GroupStatus> getGroupStatusList(final Connection con, final UserBean currentuser, final String groupId, final GroupStatusType groupStatusType) {
		/* adapted from linked status options */
		GenRow list = new GenRow();
		switch(groupStatusType) {
			case Contacts: 
				list.setParameter("LinkedStatusStatus.ForContacts", "Y");
				list.setParameter("LinkedStatusStatus.ListName","Group%");
				list.setParameter("ListName","Group%");
				break;
			case Companies: 
			    list.setParameter("LinkedStatusStatus.ForCompanies", "Y");
				list.setParameter("LinkedStatusStatus.ListName","Group%");
				list.setParameter("ListName","Group%");
				break;
			case Process: 
				//list.setParameter("StageID","!EMPTY");
				list.setParameter("ListName","Process");
				list.setParameter("LinkedStatusStatus.ListName","Process");
				break;
			default: 
				return Collections.emptyList();
		}
		try {	
			list.setConnection(con);
			list.setViewSpec("LinkedStatusView"); 
			list.setParameter("LinkedStatusStatus.Inactive","NULL+!Y");
			list.setParameter("ListName","Group%");
			list.setParameter("GroupID", groupId);
			//currentuser.getUser().setStatusConstraint(list);
			list.setParameter("-sort1","SortNumber"); 
			list.setParameter("-sort2","Status");
			list.setParameter("-groupby0","StatusID");
			//list.setAction(GenerationKeys.SEARCH);
			list.getResults(true);
			if(list.getNext()) {
				List<GroupStatus> gsl = new ArrayList<GroupStatus>();
				do {
					gsl.add(new GroupStatus(list));
				} while(list.getNext());
				return gsl;
			}
			return Collections.emptyList();
		} finally {
			list.close();
		}
	}
	
	private class GroupStatus {
		public String StatusID;
		public String Status;
		
		public GroupStatus() {}
		public GroupStatus(GenRow linkedStatusView) {
			this.Status = linkedStatusView.getData("Status");
			this.StatusID = linkedStatusView.getData("StatusID");
		}
	}

	public List<User> getQuicknoteUserMatches(HttpServletRequest request) {
		final UserBean user = getCurrentUser(request);
		
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Users")) {
			throw new AccessException("You do not have permission to access the users module");
		}
		
		GenRow users = null;
	    try {
	        String firstName = getDecodedParameter("-FirstName", request, "First Name");
	        String lastName = getDecodedParameter("-LastName", request,"Last Name");
	        String company = getDecodedParameter("-Company", request, "Company");
	        String email = getDecodedParameter("-Email", request, "Email");
	        String companyID = getDecodedParameter("-companyid", request, "");
	    	
	     // We count the number of non-empty optional parmaters because if
	        // all of these parameters are empty, we do not want to go off
	        // and do an expensive search for all companies.
	        int numberNonEmptyOptParams = 0;

	        // Search for contact matches
	        users = new GenRow();
	        users.setRequest(request);
	        users.setTableSpec("Users");

	        users.setParameter("-select0", "UserID");
	        users.setParameter("-select1", "FirstName");
	        users.setParameter("-select2", "LastName");
	        users.setParameter("-select3", "City");
	        users.setParameter("-select4", "State");
	        users.setParameter("-select5", "DISTINCT Email");
	        
	        if (company.length() > 0) {
	            users.addParameter("Company^1", "%" + company + "%");
	            numberNonEmptyOptParams++;
	            
	        }

	        if (firstName.length() > 0) {
	            users.setParameter("FirstName^2^1", "%" + firstName + "%");
	            numberNonEmptyOptParams += 1;

	        }

	        if (lastName.length() > 0) {
	            users.setParameter("LastName^1", "%" + lastName + "%");
	            numberNonEmptyOptParams += 1;

	        }
	        
	        if (email.length() > 0) {
	            users.setParameter("Email^1", "%" + email + "%");
	            numberNonEmptyOptParams += 1;

	        }

	        if (numberNonEmptyOptParams == 0 && companyID.length() > 0) {
	            users.setParameter("CompanyID^1", companyID);
	            numberNonEmptyOptParams += 1;
	        } else if (companyID.length() > 0) {
	        	numberNonEmptyOptParams += 1;
	        }

	        // Only do the search if we have at least one non-empty
	        // optional parameter.
	        if (numberNonEmptyOptParams > 0) {
	        	users.sortBy("FirstName",0);
	        	users.sortBy("LastName",1);
	            users.setAction(GenerationKeys.SEARCH);
	            users.getResults(true);
	            
	            logger.debug("QuickMatch {} - {}", users.getSize(), users.getStatement());
	        }

	        if(users == null || users.getSize() == 0) {
            	return Collections.emptyList();
	        }
            List<User> list = new ArrayList<User>(users.getSize());
            while(users.getNext()) {
                list.add(BeanFactory.getUserFromTableData(users));  
            }
            return list;
	    } finally {
	    	logger.trace("closing user beans");
	    	if(users != null) users.close();
	    }	
	}
}
