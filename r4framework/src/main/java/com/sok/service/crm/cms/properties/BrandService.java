package com.sok.service.crm.cms.properties;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.cms.properties.Brand;
import com.sok.runway.crm.cms.properties.entities.BrandEntity;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class BrandService {
	
	private static final Logger logger = LoggerFactory.getLogger(BrandService.class);
	private static final UserService userService = UserService.getInstance(); 
	private static final BrandService service = new BrandService();
	private BrandService() { }
	public static BrandService getInstance() {
		return service;
	}
	
	public List<Brand> getBrandsExternal(HttpServletRequest request, Date modifiedSince, boolean active) {

		return getBrandsExternal(ActionBean.getConnection(request), userService.getCurrentUser(request), "", modifiedSince, active);
	}

	public List<Brand> getBrandsExternal(HttpServletRequest request, String builderID, Date modifiedSince, boolean active) {

		return getBrandsExternal(ActionBean.getConnection(request), userService.getCurrentUser(request), builderID,  modifiedSince, active);
	}

	private List<Brand> getBrandsExternal(final Connection con, final UserBean user, final String builderID, final Date modifiedSince, final boolean active) {
		logger.trace("getBrandsExternal(Conn, {}, {}, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.Brand brandList = new com.sok.runway.crm.cms.properties.Brand(con);
		if(modifiedSince != null) { 
			brandList.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		if (StringUtils.isNotBlank(builderID)) brandList.setParameter("ProductParentBuilderBrand.ProductVariationBuilderBrand.ProductID", builderID);
		brandList.setParameter("-sort0", "Name");
		brandList.setParameter("-select1","ProductID");
		if(active) {
			brandList.setParameter("Active","Y");
		}
		try {
			brandList.setConnection(con);
			brandList.getResults(true);
			
			if(brandList.getNext()) {
				List<Brand> list = new ArrayList<Brand>(brandList.getSize());
				do {
					list.add(BeanFactory.external(brandList));
				}while(brandList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			brandList.close();
		}
	}
	
	public Brand getBrandExternal(HttpServletRequest request, String productId) {
		logger.debug("getBrand(Request, {})", productId);
		return BeanFactory.external(getBrand(ActionBean.getConnection(request), userService.getCurrentUser(request), productId));
	}
	
	private BrandEntity getBrand(final Connection conn, final UserBean user, final String productId) {
		if(logger.isDebugEnabled()) logger.debug("getPlan(Conn,{}, {})", user != null ? user.getName() : "null user",  productId);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		BrandEntity p = new BrandEntity(conn, productId);

		if(!p.isLoaded()) {
			throw new NotFoundException("The requested brand could not be found");
		}
		p.setCurrentUser(user.getUser());
		if(!p.canView()) {
			throw new AccessException("You do not have permission to view this brand");
		}
		return p;
	}
	

}
