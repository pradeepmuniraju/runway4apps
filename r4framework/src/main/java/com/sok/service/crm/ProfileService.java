package com.sok.service.crm;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.AnswerBean;
import com.sok.runway.UserBean;
import com.sok.runway.crm.Opportunity;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.ItemList;
import com.sok.runway.externalInterface.beans.shared.*;
import com.sok.runway.view.ValueList;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import com.sok.runway.ItemLists;
//import com.sok.runway.SingleItemList;

public class ProfileService {

	protected static final ProfileService service = new ProfileService();
	private static final Logger logger = LoggerFactory.getLogger(ProfileService.class);
	protected static final UserService us = UserService.getInstance(); 
	public static ProfileService getInstance() {
		return service;
	}
	//Use ValueList instead as uses a static map of lists, this would never be updated in case of list change.
	//private final ItemLists itemLists = new ItemLists();
	
	public enum ProfileType { CONTACT, ORDER, OPPORTUNITY, CONTACTPORTAL, ORDERPORTAL, OPPORTUNITYPORTAL  }
	
	public List<Profile> getProfiles(final HttpServletRequest request) {
		logger.trace("getProfiles(Request)");
		//we're only supporting contact profiles for now.
		return getProfiles(ActionBean.getConnection(request), us.getCurrentUser(request), ProfileType.CONTACT);
	}
	
	/**
	 * 
	 * 
	 * TODO - make this profileservice abstract, and have other profile services which extend to provide specific profile service functionality. 
	 * 
	 * 
	 * 
	 */
	
	public List<Profile> getProfiles(final HttpServletRequest request, String recordID) {
		logger.trace("getProfiles(Request, {})", recordID);
		return getProfiles(ActionBean.getConnection(request), us.getCurrentUser(request), ProfileType.CONTACT, recordID);
	}
	
	public List<Profile> getProfiles(final HttpServletRequest request, ProfileType type, String recordID) {
		logger.trace("getProfiles(Request, {},{})", type.name(), recordID);
		return getProfiles(ActionBean.getConnection(request), us.getCurrentUser(request), type, recordID);
	}
	
	public List<Profile> getProfiles(final HttpServletRequest request, ProfileType type, String recordID, String profileID) {
		logger.trace("getProfiles(Request, {},{})", type.name(), recordID);
		return getProfiles(ActionBean.getConnection(request), us.getCurrentUser(request), type, recordID, profileID);
	}
	
	public List<Profile> getProfiles(Connection conn, UserBean user, ProfileType type) {
		if(logger.isTraceEnabled()) logger.trace("getProfiles(Conn, {}, {})", new String[]{user != null ? user.getName() : "null user", type.name()});
		return getProfiles(conn, user, type, null);
	}
	public List<Profile> getProfiles(Connection conn, UserBean user, ProfileType type, String recordID) {
		if(logger.isTraceEnabled()) logger.trace("getProfiles(Conn, {}, {}, {})", new String[]{user != null ? user.getName() : "null user", type.name(), recordID});
		return getProfiles(conn, user, type, recordID, null);
	}

	public List<Profile> getProfiles(Connection conn, UserBean user, ProfileType type, String recordID, String profileID) {
		if(logger.isTraceEnabled()) logger.trace("getProfiles(Conn, {}, {}, {} {})", new String[]{user != null ? user.getName() : "null user", type.name(), recordID, profileID});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Questions")) {
			throw new AccessException("You do not have permission to access the profiles module");
		}
		
		GenRow g = new GenRow();
		GenRow q = new GenRow();
		try {
			g.setConnection(conn);
			g.setViewSpec("SurveyView");
			
			q.setConnection(conn);
			
			if(StringUtils.isBlank(recordID)) {
				q.setViewSpec("SurveyQuestionView");
			}
			
			if (StringUtils.isNotBlank(profileID)) {
				g.setParameter("SurveyID", profileID);
			}

			switch(type) {
				case ORDER:
				case ORDERPORTAL:
					g.setParameter("ProfileType", "OrderProfile");
					if(StringUtils.isNotBlank(recordID)) {
						q.setViewSpec("SurveyQuestionOrderAnswerView");
					}
					break;
				case CONTACT: 
				case CONTACTPORTAL: 
					g.setParameter("ProfileType", "ContactProfile");
					if(StringUtils.isNotBlank(recordID)) {
						q.setViewSpec("SurveyQuestionContactAnswerView");
					}
					break;
				case OPPORTUNITY:
				case OPPORTUNITYPORTAL:
					g.setParameter("ProfileType", "OpportunityProfile");
					if(StringUtils.isNotBlank(recordID)) {
						q.setViewSpec("SurveyQuestionOpportunityAnswerView");
					}
					break;
				default:
					throw new UnsupportedOperationException("Profile Type not yet implemented : " + type.name());
			}
			//user.setConstraint(g,"SurveyGroup");
			if (type == ProfileType.ORDER || type == ProfileType.CONTACT || type == ProfileType.OPPORTUNITY) {
				user.setConstraint(g,"SurveyLinkedSurvey#security.LinkedSurveyGroup#security|SC");
				g.setParameter("SurveyLinkedSurvey#security.GroupID|SC","NULL+EMPTY");
			}
			g.setParameter("-sort0","Name");
			
			if(logger.isDebugEnabled()) { 
				g.doAction("search");
				logger.debug(g.toString());
				logger.debug(g.getStatement());
			} 
			
			g.getResults(true);
			
			if(g.getNext()) {
				List<Profile> list = new ArrayList<Profile>(g.getSize());
				do {
					switch(type) {
						case ORDER:
						case ORDERPORTAL:
							OrderProfile op = BeanFactory.getOrderProfileFromTableData(g);
							list.add(op);
							q.clear();
							q.setParameter("SurveyID", op.getSurveyID());
							q.setParameter("Disabled", "NULL+!Y");
							q.setParameter("-sort0", "SortOrder");
							if(StringUtils.isNotBlank(recordID)) {
								q.setParameter("OrderID", recordID);
							} 
							if(logger.isDebugEnabled()) { 
								q.doAction(GenerationKeys.SEARCH);
								logger.debug(q.toString());
								logger.debug(q.getStatement());
							}
							if(q.isSet("SurveyID")) {
								q.getResults(true);
							}
							if(q.getNext()) {
								OrderQuestion[] qs = new OrderQuestion[q.getSize()];
								op.setQuestions(qs);
								do {
									OrderQuestion cq = BeanFactory.getOrderQuestionAnswerFromTableData(q);
									if(StringUtils.isNotBlank(cq.getValueList())) {
										
										ValueList vl = ValueList.getValueList(cq.getValueList());
										//SingleItemList sil = itemLists.getList(cq.getValueList());
										if(vl != null) {
											cq.setList(BeanFactory.getItemList(vl));
										}
									}
									qs[q.getRowIndex()-1] = cq; 
								} while (q.getNext());
							}
							break; 
						case CONTACT:
						case CONTACTPORTAL:
							ContactProfile cp = BeanFactory.getContactProfileFromTableData(g);
							list.add(cp);
							q.clear();
							q.setParameter("SurveyID", cp.getSurveyID());
							q.setParameter("Disabled", "NULL+!Y");
							q.setParameter("-sort0", "SortOrder");
							if(StringUtils.isNotBlank(recordID)) {
								q.setParameter("ContactID", recordID);
							} 
							if(logger.isDebugEnabled()) { 
								q.doAction(GenerationKeys.SEARCH);
								logger.debug(q.toString());
								logger.debug(q.getStatement());
							}
							if(q.isSet("SurveyID")) {
								q.getResults(true);
							}
							if(q.getNext()) {
								ContactQuestion[] qs = new ContactQuestion[q.getSize()];
								cp.setQuestions(qs);
								do {
									ContactQuestion cq = BeanFactory.getContactQuestionAnswerFromTableData(q);
									if(StringUtils.isNotBlank(cq.getValueList())) {
										
										ValueList vl = ValueList.getValueList(cq.getValueList());
										//SingleItemList sil = itemLists.getList(cq.getValueList());
										if(vl != null) {
											cq.setList(BeanFactory.getItemList(vl));
										}
									}
									qs[q.getRowIndex()-1] = cq; 
								} while (q.getNext());
							}
							break; 
						case OPPORTUNITY:
						case OPPORTUNITYPORTAL:
							OpportunityProfile opp = BeanFactory.getOpportunityProfileFromTableData(g);
							list.add(opp);
							q.clear();
							q.setParameter("SurveyID", opp.getSurveyID());
							q.setParameter("Disabled", "NULL+!Y");
							q.setParameter("-sort0", "SortOrder");
							if(StringUtils.isNotBlank(recordID)) {
								q.setParameter("OpportunityID", recordID);
							} 
							if(logger.isDebugEnabled()) { 
								q.doAction(GenerationKeys.SEARCH);
								logger.debug(q.toString());
								logger.debug(q.getStatement());
							}
							if(q.isSet("SurveyID")) {
								q.getResults(true);
							}
							if(q.getNext()) {
								OpportunityQuestion[] qs = new OpportunityQuestion[q.getSize()];
								opp.setQuestions(qs);
								do {
									OpportunityQuestion cq = BeanFactory.getOpportunityQuestionAnswerFromTableData(q);
									if(StringUtils.isNotBlank(cq.getValueList())) {
										
										ValueList vl = ValueList.getValueList(cq.getValueList());
										//SingleItemList sil = itemLists.getList(cq.getValueList());
										if(vl != null) {
											cq.setList(BeanFactory.getItemList(vl));
										}
									}
									qs[q.getRowIndex()-1] = cq; 
								} while (q.getNext());
							}
							break; 
					}
				} while(g.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			g.close();
			q.close();
		}
	}

	public OrderProfile saveOrderProfile(final HttpServletRequest request, final String orderID, final OrderProfile orderProfile) {
		logger.trace("saveOrderProfile(Request, {}, {})", orderID, orderProfile);
		return saveOrderProfile(ActionBean.getConnection(request), us.getCurrentUser(request), orderID, orderProfile);
	}
	
	public static OrderAnswer getOrderProfileAnswerByQuestionName(String name, List<OrderProfile> profiles) {
		if(profiles == null || profiles.isEmpty() || StringUtils.isBlank(name)) {
			return null;
		}
		for(OrderProfile p: profiles) {
			OrderQuestion[] oqs = p.getQuestions();
			if(oqs == null || oqs.length == 0)
				continue;
			for(OrderQuestion oq: oqs) {
				if(name.equals(oq.getLabel())) {
					return oq.getAnswer();
				}
			}
		}
		return null;
	}
	
	
	public OrderProfile saveOrderProfile(Connection conn, UserBean user, final String orderID, final OrderProfile orderProfile) {
		if(logger.isTraceEnabled()) logger.trace("saveOrderProfile(Request, {}, {}, {})", new String[]{user != null ? user.getName() : "null user", orderID, orderProfile.toString()});
		if(orderProfile.getQuestions() == null) {
			logger.debug("no questions, nothing to save");
			return orderProfile;
		}
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		final GenRow orderRow = OrderService.getInstance().getOrderRow(conn, orderID);
		if(!user.canView(orderRow)) {
			throw new AccessException("You do not have permission to view this order");
		}
		if(!user.canEdit(orderRow)) { 
			throw new AccessException("You do not have permission to edit this order");
		}
		
		GenRow worker = new GenRow();
		worker.setConnection(conn);
		worker.setViewSpec("answers/OrderAnswerView");
		
		AnswerBean ab = new AnswerBean();
		ab.setConnection(conn);
		ab.setCurrentUser(user);
		
		for(OrderQuestion oq: orderProfile.getQuestions()) {
			if(oq.getAnswer() == null) {
				logger.trace("not saving answer for [{}] as it was null", oq.getQuestionID());
				//not supporting null answer object. 
				continue;
			}			
			OrderAnswer oa = oq.getAnswer();
			
			if(logger.isTraceEnabled()) logger.trace("Saving Answer [{}] to Order [{}] and QuestionID [{}]", new String[]{oa.getAnswer(), orderID, oq.getQuestionID()});
			ab.saveAnswer(orderID, oq.getQuestionID(), "OrderID", new String[]{oa.getAnswer()}, 0, true);
			worker.clear();
			worker.setParameter("QuestionID", oq.getQuestionID());
			worker.setParameter("OrderID", orderID);
			
			if(logger.isDebugEnabled() && StringUtils.isNotBlank(oa.getAnswer())) { 
				worker.doAction(GenerationKeys.SEARCH);
				logger.debug(worker.getStatement());
			}
			
			worker.doAction(GenerationKeys.SELECTFIRST);
			
			oq.setAnswer(BeanFactory.getOrderAnswerFromTableData(worker));
		}
		return orderProfile;
	}
	
	public OpportunityProfile saveOpportunityProfile(final HttpServletRequest request, final String opportunityID, final OpportunityProfile opportunityProfile) {
		logger.trace("saveOpportunityProfile(Request, {}, {})", opportunityID, opportunityProfile);
		return saveOpportunityProfile(ActionBean.getConnection(request), us.getCurrentUser(request), opportunityID, opportunityProfile);
	}
	
	public static OpportunityAnswer getOpportunityProfileAnswerByQuestionName(String name, List<OpportunityProfile> profiles) {
		if(profiles == null || profiles.isEmpty() || StringUtils.isBlank(name)) {
			return null;
		}
		for(OpportunityProfile p: profiles) {
			OpportunityQuestion[] oqs = p.getQuestions();
			if(oqs == null || oqs.length == 0)
				continue;
			for(OpportunityQuestion oq: oqs) {
				if(name.equals(oq.getLabel())) {
					return oq.getAnswer();
				}
			}
		}
		return null;
	}
	
	
	public OpportunityProfile saveOpportunityProfile(Connection conn, UserBean user, final String opportunityID, final OpportunityProfile opportunityProfile) {
		if(logger.isTraceEnabled()) logger.trace("saveOpportunityProfile(Request, {}, {}, {})", new String[]{user != null ? user.getName() : "null user", opportunityID, opportunityProfile.toString()});
		if(opportunityProfile.getQuestions() == null) {
			logger.debug("no questions, nothing to save");
			return opportunityProfile;
		}
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Opportunities")) {
			throw new AccessException("You do not have permission to access the opportunity module");
		}
		final Opportunity opp = new Opportunity(conn, opportunityID); 
		opp.setCurrentUser(user.getUser()); 
		
		if(!opp.canView()) {
			throw new AccessException("You do not have permission to view this opportunity");
		}
		if(!opp.canEdit()) {
			throw new AccessException("You do not have permission to edit this opportunity");
		}
		GenRow worker = new GenRow();
		worker.setConnection(conn);
		worker.setViewSpec("answers/OpportunityAnswerView");
		
		AnswerBean ab = new AnswerBean();
		ab.setConnection(conn);
		ab.setCurrentUser(user);
		
		for(OpportunityQuestion oq: opportunityProfile.getQuestions()) {
			if(oq.getAnswer() == null) {
				logger.trace("not saving answer for [{}] as it was null", oq.getQuestionID());
				//not supporting null answer object. 
				continue;
			}			
			OpportunityAnswer oa = oq.getAnswer();
			
			if(logger.isTraceEnabled()) logger.trace("Saving Answer [{}] to Opportunity [{}] and QuestionID [{}]", new String[]{oa.getAnswer(), opportunityID, oq.getQuestionID()});
			ab.saveAnswer(opportunityID, oq.getQuestionID(), "OpportunityID", new String[]{oa.getAnswer()}, 0, true);
			worker.clear();
			worker.setParameter("QuestionID", oq.getQuestionID());
			worker.setParameter("OpportunityID", opportunityID);
			
			if(logger.isDebugEnabled() && StringUtils.isNotBlank(oa.getAnswer())) { 
				worker.doAction(GenerationKeys.SEARCH);
				logger.debug(worker.getStatement());
			}
			
			worker.doAction(GenerationKeys.SELECTFIRST);
			
			oq.setAnswer(BeanFactory.getOpportunityAnswerFromTableData(worker));
		}
		return opportunityProfile;
	}
	public ContactProfile saveContactProfile(final HttpServletRequest request, final String contactID, final ContactProfile contactProfile) {
		logger.trace("contactProfile(Request, {}, {})", contactID, contactProfile);
		return contactProfile(ActionBean.getConnection(request), us.getCurrentUser(request), contactID, contactProfile);
	}
	
	public ContactProfile contactProfile(Connection conn, UserBean user, final String contactID, final ContactProfile contactProfile) {
		if(logger.isTraceEnabled()) logger.trace("contactProfile(Request, {}, {}, {})", new String[]{user != null ? user.getName() : "null user", contactID, contactProfile.toString()});
		if(contactProfile.getQuestions() == null) {
			logger.debug("no questions, nothing to save");
			return contactProfile;
		}
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Contacts")) {
			throw new AccessException("You do not have permission to access the contacts module");
		}
		com.sok.runway.crm.Contact contact = new com.sok.runway.crm.Contact(conn, contactID);
		if(!contact.isLoaded()) { 
			throw new NotFoundException("The contact could not be found with id " + contactID);
		}
		contact.setCurrentUser(user.getUser());
		if(!contact.canView()) {
			throw new AccessException("You do not have permission to view this contact");
		}
		if(!contact.canEdit()) {  
			throw new AccessException("You do not have permission to edit this contact");
		}
		GenRow worker = new GenRow();
		worker.setConnection(conn);
		worker.setViewSpec("answers/ContactAnswerView");
		
		AnswerBean ab = new AnswerBean();
		ab.setConnection(conn);
		ab.setCurrentUser(user);
		
		for(ContactQuestion cq: contactProfile.getQuestions()) {
			if(cq.getAnswer() == null) {
				//not supporting null answer object. 
				continue;
			}			
			ContactAnswer ca = cq.getAnswer();
			ab.saveAnswer(contactID, cq.getQuestionID(), "ContactID", new String[]{ca.getAnswer()}, 0, true);
			
			//need to do this to reload it.. ugh.
			worker.clear();
			worker.setParameter("QuestionID", cq.getQuestionID());
			worker.setParameter("ContactID", contactID);
			worker.doAction(GenerationKeys.SELECTFIRST);
			cq.setAnswer(BeanFactory.getContactAnswerFromTableData(worker));
		}
		return contactProfile;
	}
	
	public ItemList getItemListByName(final String name) {
		if(StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("The name was null or empty");
		}
		ValueList vl = ValueList.getValueList(name);
		if(vl == null) {
			throw new NotFoundException("Could not find item list with name " + name);
		}
		return BeanFactory.getItemList(vl);
		/*
		SingleItemList sil = itemLists.getList(name);
		if(sil == null) {
			throw new NotFoundException("Could not find item list with name " + name);
		}
		return BeanFactory.getItemList(sil);
		*/
	}
	
	public ItemList getCostItemListByName(final String name) {
		if(StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("The name was null or empty");
		}
		ValueList vl = ValueList.getValueList(name);
		if(vl == null) {
			throw new NotFoundException("Could not find item list with name " + name);
		}
		return BeanFactory.getItemList(vl);
		/*
		SingleItemList sil = itemLists.getList(name);
		if(sil == null) {
			throw new NotFoundException("Could not find item list with name " + name);
		}
		return BeanFactory.getItemList(sil);
		*/
	}
	
	/*
	public GenRow getContactQuestionsRow(Connection conn, UserBean user, String surveyID) {
		
		
		
		
		throw new UnsupportedOperationException("not yet implemented");
	}
	
	*/
	
}
