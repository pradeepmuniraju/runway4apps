package com.sok.service.crm.cms.properties;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.cms.properties.Estate;
import com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage;
import com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackageCheck;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartmentCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPackageCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPackages;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexStatusHistory;
import com.sok.runway.externalInterface.beans.cms.properties.SearchKey;
import com.sok.runway.crm.cms.properties.Facade;
import com.sok.runway.crm.cms.properties.PackageCost;
import com.sok.runway.crm.cms.properties.entities.EstateEntity;
import com.sok.runway.crm.cms.properties.entities.HouseLandPackageEntity;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.PlanService.HLPackage;
import com.sok.service.crm.cms.properties.PlanService.HLPackageCost;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.ListIterator;

public class PackageService {
	
	private static final Logger logger = LoggerFactory.getLogger(PackageService.class);
	private static final UserService userService = UserService.getInstance(); 
	private static final PackageService service = new PackageService();
	private static final UserService us = UserService.getInstance(); 
	private PackageService() { }
	public static PackageService getInstance() {
		return service;
	}

	public List<HouseLandPackage> getPackagesExternal(HttpServletRequest request, final Date modifiedSince, final boolean active) {
		GenRow search = new GenRow(); 
		if(modifiedSince != null) { 
			search.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		if(active) {
			search.setParameter("Active","Y");
		}
		return getPackagesExternal(ActionBean.getConnection(request), userService.getCurrentUser(request), search);
	} 
	
	public List<HouseLandPackageCheck> getPackagesExternalCheck(HttpServletRequest request, GenRow search) {
		return getPackagesExternalCheck(ActionBean.getConnection(request), userService.getCurrentUser(request), search);
	}
	
	private List<HouseLandPackageCheck> getPackagesExternalCheck(final Connection con, final UserBean user, GenRow search) {
		logger.trace("getPackagesExternalCheck(Conn, user={}, search={})", user != null ? user.getName() : "null user", search != null ? search.toString() : "null search");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.HouseLandPackage packageList = new com.sok.runway.crm.cms.properties.HouseLandPackage(con);
		
		if(search != null)
			packageList.putAll(search);
		else
			packageList.setParameter("Active","Y");
		
		packageList.setParameter("-select1","ProductID");

		try {
			// we need a know order for later
			packageList.sortBy("ProductID", 0);
			packageList.setConnection(con);
			packageList.getResults(true);
			
			if(logger.isTraceEnabled()) {
				packageList.doAction(GenerationKeys.SEARCH);
				logger.trace(packageList.getStatement());
			}
			
			if(packageList.getNext()) {
				List<HouseLandPackageCheck> list = new ArrayList<HouseLandPackageCheck>(packageList.getSize());
				do {
					list.add(BeanFactory.externalCheck(packageList));
				}while(packageList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			packageList.close();
		}
	}
	
	public List<HouseLandPackage> getPackagesExternal(HttpServletRequest request, GenRow search) {
		return getPackagesExternal(ActionBean.getConnection(request), userService.getCurrentUser(request), search);
	}
	
	private List<HouseLandPackage> getPackagesExternal(final Connection con, final UserBean user, GenRow search) {
		logger.trace("getPackagesExternal(Conn, user={}, search={})", user != null ? user.getName() : "null user", search != null ? search.toString() : "null search");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.HouseLandPackage packageList = new com.sok.runway.crm.cms.properties.HouseLandPackage(con);
		
		if(search != null)
			packageList.putAll(search);
		else
			packageList.setParameter("Active","Y");
		
		packageList.setParameter("-select1","ProductID");

		try {
			packageList.setViewSpec("properties/ProductHouseAndLandView");
			// we need a know order for later
			packageList.sortBy("ProductID", 0);
			packageList.setConnection(con);
			packageList.getResults(true);
			
			if(logger.isTraceEnabled()) {
				packageList.doAction(GenerationKeys.SEARCH);
				logger.trace(packageList.getStatement());
			}
			
			if(packageList.getNext()) {
				List<HouseLandPackage> list = new ArrayList<HouseLandPackage>(packageList.getSize());
				do {
					list.add(BeanFactory.external(packageList));
				}while(packageList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			packageList.close();
		}
	}
	
	public static class HouseLandPackageList implements List<HouseLandPackage> {

		private final List<com.sok.runway.crm.cms.properties.HouseLandPackage> idList; 
		
		public HouseLandPackageList(List<com.sok.runway.crm.cms.properties.HouseLandPackage> wrappedList) {
			idList = wrappedList; 
		}
		
		@Override
		public boolean add(HouseLandPackage arg0) {
			return false; 
		}

		@Override
		public void add(int arg0, HouseLandPackage arg1) {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public boolean addAll(Collection<? extends HouseLandPackage> arg0) {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public boolean addAll(int arg0, Collection<? extends HouseLandPackage> arg1) {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public void clear() {
			idList.clear();
		}

		@Override
		public boolean contains(Object arg0) {
			if(arg0 == null) return false;
			if(arg0 instanceof com.sok.runway.crm.cms.properties.HouseLandPackage) return idList.contains(arg0);
			if(arg0 instanceof HouseLandPackage) {
				for(com.sok.runway.crm.cms.properties.HouseLandPackage hlp: idList) {
					if(hlp.getString("ProductID").equals(((HouseLandPackage)arg0).getPackageID())) {
						return true; 
					}
				}
			}
			return false;
		}

		@Override
		public boolean containsAll(Collection<?> arg0) {
			if(arg0 == null) return false;
			for(Object o: arg0) {
				if(!contains(o)) return false;
			}
			return false;
		}

		@Override
		public HouseLandPackage get(int arg0) {
			return BeanFactory.external(idList.get(arg0));
		}

		@Override
		public int indexOf(Object arg0) {
			if(arg0 == null) return -1;
			if(arg0 instanceof com.sok.runway.crm.cms.properties.HouseLandPackage) return idList.indexOf(arg0);
			int ix=0; 
			if(arg0 instanceof HouseLandPackage) {
				for(com.sok.runway.crm.cms.properties.HouseLandPackage hlp: idList) {
					if(hlp.getString("ProductID").equals(((HouseLandPackage)arg0).getPackageID())) {
						return ix; 
					}
					ix++;
				}
			}
			return -1;
		}

		@Override
		public boolean isEmpty() {
			return idList.isEmpty();
		}

		@Override
		public Iterator<HouseLandPackage> iterator() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int lastIndexOf(Object arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public ListIterator<HouseLandPackage> listIterator() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ListIterator<HouseLandPackage> listIterator(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean remove(Object arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public HouseLandPackage remove(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean removeAll(Collection<?> arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean retainAll(Collection<?> arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public HouseLandPackage set(int arg0, HouseLandPackage arg1) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int size() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public List<HouseLandPackage> subList(int arg0, int arg1) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object[] toArray() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <T> T[] toArray(T[] arg0) {
			// TODO Auto-generated method stub
			return null;
		} 
		
		
		
		
	}
	
	
	/* this does work, but not in use.
	private HouseLandPackageEntityList getPackages(final Connection con, final UserBean user, final Date modifiedSince, final boolean active) {
		
		logger.trace("getPackages(Conn, {}, {}, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow packageList = new GenRow();
		packageList.setTableSpec("Products");
		packageList.setParameter("ProductType", "House and Land");
		if(modifiedSince != null) { 
			packageList.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		packageList.setParameter("-sort0", "Name");
		packageList.setParameter("-select1","ProductID");
		if(active) {
			packageList.setParameter("Active","Y");
		}
		//security constraint ?
		try {
			packageList.setConnection(con);
			packageList.getResults(true);
			
			if(packageList.getNext()) {
				HouseLandPackageEntityList list = new HouseLandPackageEntityList(packageList.getSize());
				do {
					//check for can view ? 
					list.add(new HouseLandPackageEntity(packageList.getConnection(), packageList.getData("ProductID")));
				}while(packageList.getNext());
				return list;
			}
			return EMPTY_LIST;
		} finally {
			packageList.close();
		}
	}
	
	private static final HouseLandPackageEntityList EMPTY_LIST = new HouseLandPackageEntityList(0);
	public static class HouseLandPackageEntityList extends ArrayList<HouseLandPackageEntity> {
		private static final long serialVersionUID = 1L;
		public HouseLandPackageEntityList(int size) {
			super(size);
		}
		public List<HouseLandPackage> external() {
			if(isEmpty()) return Collections.emptyList();
			List<HouseLandPackage> list = new ArrayList<HouseLandPackage>(this.size());
			for(HouseLandPackageEntity hlpe: this) {
				list.add(hlpe.external());
			}
			return list;
		}
	}
	*/
	public HouseLandPackage getPackageExternal(HttpServletRequest request, String productId, BeanRepresentation repr) {
		logger.debug("getPackage(Request, {})", productId);
		return BeanFactory.external(getPackage(ActionBean.getConnection(request), userService.getCurrentUser(request), productId), repr);
	}
	
	private HouseLandPackageEntity getPackage(final Connection conn, final UserBean user, final String productId) {
		if(logger.isDebugEnabled()) logger.debug("getPlan(Conn,{}, {})", user != null ? user.getName() : "null user",  productId);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		HouseLandPackageEntity p = new HouseLandPackageEntity(conn, productId);

		if(!p.isLoaded()) {
			throw new NotFoundException("The requested package could not be found");
		}
		p.setCurrentUser(user.getUser());
		if(!p.canView()) {
			throw new AccessException("You do not have permission to view this package");
		}
		
		
		
		
		/* TODO FUTURE 
		PackageCost pc = new PackageCost(conn);
		HLPackage pkg = new HLPackage(p, getQuoteViewID(p));
		try {
			pc.setParameter("-join1","ProductsLinkedProductProduct");	//inner join 
			pc.setParameter("ProductsLinkedProductProduct.ProductID", p.getString("ProductID"));
			pc.setParameter("ProductsLinkedProductProduct.Active", "Y");
			if(logger.isDebugEnabled()) { 
				pc.doAction(GenerationKeys.SEARCH);
				logger.debug(pc.getStatement());
			}
			pc.getResults(true);
			if(pc.getNext()) {
				List<HLPackageCost> costlist = new ArrayList<HLPackageCost>(pc.getSize());
				do {
					costlist.add(new HLPackageCost(pc));
				} while(pc.getNext());
				pkg.PackageCosts = costlist;
			}
		} finally {
			pc.close();
		}
		Facade f = new Facade(conn);
		f.setParameter("ProductID", p.getString("ProductID"));
		if(logger.isDebugEnabled()) { 
			f.doAction(GenerationKeys.SEARCH);
			logger.debug(f.getStatement());
		}
		f.doAction(GenerationKeys.SELECTFIRST);
		if(f.isSuccessful()) {
			pkg.FacadeID = f.getData("LinkedProductID");
		}
		*/
		return p;
	}
	
	public SearchKey getMinMax(HttpServletRequest request, String searchKey) {
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		row.setRequest(request);
		row.setParameter("ProductType", "House and Land");
		row.setParameter("-select0", "MIN(" + searchKey + ") AS Minimum");
		row.setParameter("-select1", "MAX(" + searchKey + ") AS Maximum");
		row.setParameter(searchKey, ">0");
		row.doAction("selectfirst");
		
		double min = 0, max = 0;
		
		try {
			min = Double.parseDouble(row.getString("Minimum"));
		} catch (Exception e) {
			System.out.println("Error Min " +  row.getString("Minimum"));
		}

		try {
			max = Double.parseDouble(row.getString("Maximum"));
		} catch (Exception e) {
			System.out.println("Error Min " +  row.getString("Minimum"));
		}

		return new SearchKey(searchKey, min, max);
	}

	public List<QuickIndexPackages> getPackageSearch(final HttpServletRequest request, String search, boolean filtered) {
		logger.trace("getLotsSearch(Request, {}, {})", search, filtered);
		return getPackageSearch(ActionBean.getConnection(request), us.getCurrentUser(request), search, filtered);
	}
	
	public List<QuickIndexPackages> getPackageSearch(final Connection conn, final UserBean user, String search, boolean filtered) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow packageRow = getPlanSearch(conn, user, search, filtered);
		try {
			if(logger.isTraceEnabled()) {
				packageRow.doAction(GenerationKeys.SEARCH);
				logger.trace(packageRow.getStatement());
			}
			packageRow.getResults(true);
			
			System.out.println(packageRow.getStatement());
			
			if(packageRow.getNext()) {
				List<QuickIndexPackages> list = new ArrayList<QuickIndexPackages>(packageRow.getSize());
				do {
					QuickIndexPackages qip = new QuickIndexPackages(packageRow);
					if (packageRow.isSet("ThumbDocumentID")) qip.setThumbnailImage(getImageDocument(conn, packageRow.getString("ThumbDocumentID")));
					if (packageRow.isSet("ImageDocumentID")) qip.setMainImage(getImageDocument(conn, packageRow.getString("ImageDocumentID")));
					qip.setStatusHistory(getStatus(packageRow));
					list.add(qip);
				} while (packageRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			packageRow.close();
		}	
	}
	
	private QuickIndexStatusHistory getStatus(GenRow packageRow) {
		QuickIndexStatusHistory qish = new QuickIndexStatusHistory();
		
		qish.setProductID(packageRow.getString("ProductID"));
		qish.setProductStatusID(packageRow.getString("ProductStatusID"));
		qish.setStatus(packageRow.getString("CurrentStatus"));
		qish.setStatusID(packageRow.getString("CurrentStatusID"));
		
		return qish;
	}
	private SimpleDocument getImageDocument(Connection conn, String documentID) {
		GenRow row = new GenRow();
		row.setTableSpec("Documents");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("DocumentID", documentID);
		row.doAction("selectfirst");
		
		if (row.isSet("FilePath")) {
			SimpleDocument sd = new SimpleDocument();
			sd.setDocumentID(documentID);
			sd.setFileName(row.getString("FileName"));
			sd.setFilePath(row.getString("FilePath"));

			/*
			sd.setCreatedDate(row.getDate("CreatedDate"));
			sd.setCreatedBy(row.getString("CreatedBy"));
			sd.setCreatedByName(new PersonName(row.getString("OwnFirstName"), row.getString("OwnLastName")));
			*/
			if (row.isSet("DocumentCaption")) sd.setDescription(row.getString("DocumentCaption"));
			else sd.setDescription(row.getString("Description"));

			sd.setType(row.getString("DocumentType"));
			sd.setSubType(row.getString("DocumentSubType"));
			
			return sd;
		}
		
		return null;
	}
	
	public GenRow getPlanSearch(final Connection conn, final UserBean user, final String search, boolean filter)
	{
		GenRow packageRow = new GenRow();
		packageRow.setViewSpec("properties/ProductQuickIndexDescriptionView");
		packageRow.setConnection(conn);
		packageRow.parseRequest(search);
		packageRow.setParameter("-groupby0", "ProductID");
		packageRow.setParameter("ProductType", "House and Land");
		if (!packageRow.isSet("DesignID")) packageRow.setParameter("DesignID", "!NULL;!EMPTY");
		if (!packageRow.isSet("RangeID")) packageRow.setParameter("RangeID", "!NULL;!EMPTY");
		if (!packageRow.isSet("StageID")) packageRow.setParameter("StageID", "!NULL;!EMPTY");
		if (!packageRow.isSet("EstateID")) packageRow.setParameter("EstateID", "!NULL;!EMPTY");
		
		boolean params = search != null && search.length() > 0;
		if(filter) {
			packageRow.setParameter("Active", "Y");
			String availableID = InitServlet.getSystemParam("ProductAvailableStatus");
			String availableIDs = InitServlet.getSystemParam("ProductAvailableAditionalStatuses");
			if (StringUtils.isBlank(availableIDs)) availableIDs = availableID;
			if (StringUtils.isNotBlank(availableIDs)) {
				packageRow.setParameter("QuickIndexCurrentStatus.StatusID", availableIDs);
				packageRow.setParameter("AvailableDate", "<=NOW+NULL");
				packageRow.setParameter("ExpiryDate", ">=NOW+NULL");
			}
			boolean publishFilter = "true".equals(InitServlet.getSystemParam("API-SearchPublishWebsite"));
			if (publishFilter) packageRow.setParameter("PublishedWebsite", "Y");
		}
		if(!params) {
			throw new IllegalArgumentException("Please provide the correct parameters for this API.");
		} else {
			if (packageRow.getString("PackageID").length() > 0) {
				packageRow.setParameter("ProductID", packageRow.getString("PackageID"));
			}
			if (packageRow.getString("DisplayFlag").length() == 0) {
				packageRow.setParameter("DisplayFlag", "!Y+NULL");
			}
			if (packageRow.getString("MarketingStatus").length() > 0) {
				packageRow.setParameter("LotExclusivity", packageRow.getString("MarketingStatus"));
			}
			if (packageRow.getString("LotName").length() > 0) {
				String lotName = packageRow.getString("LotName");
				packageRow.remove("LotName");
				packageRow.setParameter("Name|l", lotName);
				packageRow.setParameter("Name|l", "Lot " + lotName);
			}
			if (packageRow.getString("StageName").length() > 0) {
				String stageName = packageRow.getString("StageName");
				packageRow.remove("StageName");
				packageRow.setParameter("StageName|s", stageName);
				packageRow.setParameter("StageName|s", "Stage " + stageName);
			}
			if (packageRow.getString("PackageName").length() > 0) {
				packageRow.setParameter("Name|l", packageRow.getString("PackageName"));
			}
			if (packageRow.getString("BrandID").length() > 0) {
				packageRow.setParameter("BuilderID", packageRow.getString("BrandID"));
			}
			if (packageRow.getString("LotWidth").length() > 0) {
				packageRow.setParameter("LotWidth", packageRow.getString("LotWidth"));
			}
			if (packageRow.getString("LotDepth").length() > 0) {
				packageRow.setParameter("LotDepth", packageRow.getString("LotDepth"));
			}
			if (packageRow.getString("LotArea").length() > 0) {
				packageRow.setParameter("LotArea", packageRow.getString("LotArea"));
			}
			if (packageRow.getString("LotPrice").length() > 0) {
				packageRow.setParameter("LotPrice", packageRow.getString("LotPrice"));
			}
			if (packageRow.getString("HomeWidth").length() > 0) {
				packageRow.setParameter("HomeWidth", packageRow.getString("HomeWidth"));
			}
			if (packageRow.getString("HomeDepth").length() > 0) {
				packageRow.setParameter("HomeDepth", packageRow.getString("HomeDepth"));
			}
			if (packageRow.getString("HomeArea").length() > 0) {
				packageRow.setParameter("HomeArea", packageRow.getString("HomeArea"));
			}
			if (packageRow.getString("HomePrice").length() > 0) {
				packageRow.setParameter("HomePrice", packageRow.getString("HomePrice"));
			}
			if (packageRow.getString("BuildWidth").length() > 0) {
				packageRow.setParameter("BuildWidth", packageRow.getString("BuildWidth"));
			}
			if (packageRow.getString("BuildDepth").length() > 0) {
				packageRow.setParameter("BuildDepth", packageRow.getString("BuildDepth"));
			}
			if (packageRow.getString("CanFitOnDepth").length() > 0) {
				packageRow.setParameter("CanFitOnDepth", packageRow.getString("CanFitOnDepth"));
			}
			if (packageRow.getString("CanFitOnWidth").length() > 0) {
				packageRow.setParameter("CanFitOnWidth", packageRow.getString("CanFitOnWidth"));
			}
			if (packageRow.getString("Storeys").length() > 0) {
				packageRow.setParameter("Storeys", packageRow.getString("Storeys"));
			}
			if (packageRow.getString("Bedrooms").length() > 0) {
				packageRow.setParameter("Bedrooms", packageRow.getString("Bedrooms"));
			}
			if (packageRow.getString("Bathrooms").length() > 0) {
				packageRow.setParameter("Bathrooms", packageRow.getString("Bathrooms"));
			}
			if (packageRow.getString("CarParks").length() > 0) {
				packageRow.setParameter("CarParks", packageRow.getString("CarParks"));
			}
			if (packageRow.getString("Study").length() > 0) {
				packageRow.setParameter("Study", packageRow.getString("Study") + "+NULL+EMPTY");
			}
			if (packageRow.getString("Garage").length() > 0) {
				packageRow.setParameter("Garage", packageRow.getString("Garage") + "+NULL+EMPTY");
			}
			if (packageRow.getString("LocationName").length() > 0) {
				packageRow.setParameter("QuickIndexDisplayEntity.Name", packageRow.getString("LocationName"));
				packageRow.remove("LocationName");
			}
			if (packageRow.getString("LocationNum").length() > 0) {
				packageRow.setParameter("QuickIndexDisplayEntity.DisplayEntityProduct.ProductNum", packageRow.getString("LocationNum"));
				packageRow.remove("LocationNum");
			}
			if (packageRow.getString("KeyWord").length() > 0) {
				String keyWord = "%" + packageRow.getString("KeyWord").replaceAll("\\\\+", "%+%").replaceAll(" ", "%+%").replaceAll(";", "%;%") + "%";
				packageRow.remove("KeyWord");
				packageRow.setParameter("ProductQuickIndexProductDescriptionWebsite.HeadLine|k", keyWord);
				packageRow.setParameter("ProductQuickIndexProductDescriptionWebsite.Description|k", keyWord);
			}
		}
		
		//packageRow.setParameter("-sort0","RangeName");
		//packageRow.setParameter("-sort1","DesignName");
		if (packageRow.getString("-sort0").length() == 0 ) packageRow.setParameter("-sort0","Name");
		
		packageRow.setParameter("-groupby0", "ProductID");
		
		return packageRow;
	}

	public List<QuickIndexPackageCollections> getPackageCollectionSearch(final HttpServletRequest request, String search, String collection) {
		logger.trace("getApartmentsSearch(Request, {}, {})", search, collection);
		return getPackageCollectionSearch(ActionBean.getConnection(request), us.getCurrentUser(request), search, collection);
	}
	
	public List<QuickIndexPackageCollections> getPackageCollectionSearch(final Connection conn, final UserBean user, String search, String collection) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow planRow = getPlanSearch(conn, user, search, true);
		try {
			String[] parts = null;
			if (StringUtils.isNotBlank(collection)) {
				parts = collection.split("\\+");
				if (parts != null && parts.length > 0) {
					for (int p = 0; p < parts.length; ++p) {
						if ("MarketingStatus".equals(parts[p])) parts[p] = "LotExclusivity";
						if ("OriginalPlanID".equals(parts[p])) parts[p] = "CopiedFromID";
						planRow.setParameter("-groupby" + p, parts[p]);
					}
				}
			}
			if(logger.isTraceEnabled()) {
				planRow.doAction(GenerationKeys.SEARCH);
				logger.trace(planRow.getStatement());
			}
			planRow.getResults(true);
			
			System.out.println(planRow.getStatement());
			
			if(planRow.getNext()) {
				List<QuickIndexPackageCollections> list = new ArrayList<QuickIndexPackageCollections>(planRow.getSize());
				do {
					QuickIndexPackageCollections qip = new QuickIndexPackageCollections(planRow);
					//if (planRow.isSet("ThumbDocumentID")) qip.setThumbnailImage(getImageDocument(conn, planRow.getString("ThumbDocumentID")));
					//if (planRow.isSet("ImageDocumentID")) qip.setMainImage(getImageDocument(conn, planRow.getString("ImageDocumentID")));
					//qip.setStatusHistory(getStatus(planRow));
					String local = search;
					if (parts != null && parts.length > 0) {
						for (int p = 0; p < parts.length; ++p) {
							if (local.length() > 0) local += "&";
							if (planRow.isSet(parts[p]))
								local += parts[p] + "=" + planRow.getData(parts[p]);
							else
								local += parts[p] + "=EMPTY%2BNULL";
						}
					}
					qip.setPackageList(getPackageSearch(conn, user, local, true));
					list.add(qip);
				} while (planRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			planRow.close();
		}	
	}
	
}
