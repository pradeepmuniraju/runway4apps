package com.sok.service.crm.cms.properties;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.Land;
import com.sok.runway.crm.cms.properties.entities.BuildingEntity;
import com.sok.runway.crm.cms.properties.entities.BuildingStageEntity;
import com.sok.runway.crm.cms.properties.entities.HouseLandPackageEntity;
import com.sok.runway.crm.cms.properties.entities.ApartmentEntity;
import com.sok.runway.crm.cms.properties.entities.StageEntity;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.cms.properties.Building;
import com.sok.runway.externalInterface.beans.cms.properties.Apartment;
import com.sok.runway.externalInterface.beans.cms.properties.ApartmentCheck;
import com.sok.runway.externalInterface.beans.cms.properties.Apartment;
import com.sok.runway.externalInterface.beans.cms.properties.ApartmentCheck;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartmentCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartments;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPlans;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexStatusHistory;
import com.sok.runway.externalInterface.beans.cms.properties.SearchKey;
import com.sok.runway.externalInterface.beans.cms.properties.BuildingStage;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

public class BuildingService {

	private static final BuildingService service = new BuildingService();
	private static final Logger logger = LoggerFactory.getLogger(BuildingService.class);
	/* TODO - private */protected static final UserService us = UserService.getInstance();
	public static BuildingService getInstance() {
		return service;
	}
	
	public List<Building> getBuildings(final HttpServletRequest request) {
		return getBuildings(request, null, true);
	}
	
	/* request methods */
	public List<Building> getBuildings(final HttpServletRequest request, Date modifiedSince, boolean active) {
		logger.trace("getBuildings(Request, modifiedSince={})", String.valueOf(modifiedSince));
		return getBuildings(ActionBean.getConnection(request), us.getCurrentUser(request), modifiedSince, active);
	}

	public Building getBuilding(final HttpServletRequest request, final String builderID) {
		logger.trace("getBuilding(Request, {})");
		return getBuilding(ActionBean.getConnection(request), us.getCurrentUser(request), builderID);
	}
	
	/* internal methods  */
	// TODO getBuildingEntities ? 
	public List<Building> getBuildings(final Connection con, final UserBean user, final Date modifiedSince, final boolean active) {
		logger.trace("getBuildings(Conn, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow buildingList = new GenRow();
		buildingList.setTableSpec("Products");
		buildingList.setParameter("ProductType", "Building");
		if(modifiedSince != null) { 
			buildingList.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		buildingList.setParameter("-sort0", "Name");
		buildingList.setParameter("-select1","ProductID");
		if(active) {
			buildingList.setParameter("Active","Y");
		}

		try {
			buildingList.setConnection(con);
			buildingList.getResults(true);

			if(buildingList.getNext()) {
				List<Building> list = new ArrayList<Building>(buildingList.getSize());
				do { 
					list.add(BeanFactory.external(new BuildingEntity(buildingList.getConnection(), buildingList.getData("ProductID"))));
				}while(buildingList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			buildingList.close();
		}
	}
	
	public Building getBuilding(final Connection conn, final UserBean user, final String builderID) {
		logger.trace("getBuilding(Conn, {}, {})", user != null ? user.getName() : "null user", builderID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(builderID)) {
			throw new IllegalArgumentException("You must supply a builderID to use this method");
		}
		BuildingEntity e = new BuildingEntity(conn);
		e.setCurrentUser(user.getUser());
		e.load(builderID);
		if(!e.isLoaded()) {
			throw new NotFoundException("The Builder you requested could not be found: " + builderID);
		}
		if(!e.canView()) {
			throw new AccessException("You do not have permission to view the builder with id " + builderID);
		}
		return BeanFactory.external(e); 
	}	
	
	public List<QuickIndexApartments> getApartmentsSearch(final HttpServletRequest request, String search, boolean filtered) {
		logger.trace("getApartmentsSearch(Request, {}, {})", search, filtered);
		return getApartmentsSearch(ActionBean.getConnection(request), us.getCurrentUser(request), search, filtered);
	}
	
	public List<QuickIndexApartments> getApartmentsSearch(final Connection conn, final UserBean user, String search, boolean filtered) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow planRow = getApartmentSearch(conn, user, search, filtered);
		try {
			if(logger.isTraceEnabled()) {
				planRow.doAction(GenerationKeys.SEARCH);
				logger.trace(planRow.getStatement());
			}
			planRow.getResults(true);
			
			System.out.println(planRow.getStatement());
			
			if(planRow.getNext()) {
				List<QuickIndexApartments> list = new ArrayList<QuickIndexApartments>(planRow.getSize());
				do {
					QuickIndexApartments qip = new QuickIndexApartments(planRow);
					if (planRow.isSet("ThumbDocumentID")) qip.setThumbnailImage(getImageDocument(conn, planRow.getString("ThumbDocumentID")));
					if (planRow.isSet("ImageDocumentID")) qip.setMainImage(getImageDocument(conn, planRow.getString("ImageDocumentID")));
					qip.setStatusHistory(getStatus(planRow));
					list.add(qip);
				} while (planRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			planRow.close();
		}	
	}
	
	public List<QuickIndexApartmentCollections> getApartmentsCollectionSearch(final HttpServletRequest request, String search, String collection) {
		logger.trace("getApartmentsSearch(Request, {}, {})", search, collection);
		return getApartmentsCollectionSearch(ActionBean.getConnection(request), us.getCurrentUser(request), search, collection);
	}
	
	public List<QuickIndexApartmentCollections> getApartmentsCollectionSearch(final Connection conn, final UserBean user, String search, String collection) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow planRow = getApartmentSearch(conn, user, search, true);
		try {
			String[] parts = null;
			if (StringUtils.isNotBlank(collection)) {
				parts = collection.split("\\+");
				if (parts != null && parts.length > 0) {
					for (int p = 0; p < parts.length; ++p) {
						if ("MarketingStatus".equals(parts[p])) parts[p] = "LotExclusivity";
						planRow.setParameter("-groupby" + p, parts[p]);
					}
				}
			}
			if(logger.isTraceEnabled()) {
				planRow.doAction(GenerationKeys.SEARCH);
				logger.trace(planRow.getStatement());
			}
			planRow.getResults(true);
			
			System.out.println(planRow.getStatement());
			
			if(planRow.getNext()) {
				List<QuickIndexApartmentCollections> list = new ArrayList<QuickIndexApartmentCollections>(planRow.getSize());
				do {
					QuickIndexApartmentCollections qip = new QuickIndexApartmentCollections(planRow);
					//if (planRow.isSet("ThumbDocumentID")) qip.setThumbnailImage(getImageDocument(conn, planRow.getString("ThumbDocumentID")));
					//if (planRow.isSet("ImageDocumentID")) qip.setMainImage(getImageDocument(conn, planRow.getString("ImageDocumentID")));
					//qip.setStatusHistory(getStatus(planRow));
					String local = search;
					if (parts != null && parts.length > 0) {
						for (int p = 0; p < parts.length; ++p) {
							if (local.length() > 0) local += "&";
							if (planRow.isSet(parts[p]))
								local += parts[p] + "=" + planRow.getData(parts[p]);
							else
								local += parts[p] + "=EMPTY%2BNULL";
						}
					}
					qip.setApartmentList(getApartmentsSearch(conn, user, local, true));
					list.add(qip);
				} while (planRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			planRow.close();
		}	
	}
	
	private QuickIndexStatusHistory getStatus(GenRow packageRow) {
		QuickIndexStatusHistory qish = new QuickIndexStatusHistory();
		
		qish.setProductID(packageRow.getString("ProductID"));
		qish.setProductStatusID(packageRow.getString("ProductStatusID"));
		qish.setStatus(packageRow.getString("CurrentStatus"));
		qish.setStatusID(packageRow.getString("CurrentStatusID"));
		
		return qish;
	}
	private SimpleDocument getImageDocument(Connection conn, String documentID) {
		GenRow row = new GenRow();
		row.setTableSpec("Documents");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("DocumentID", documentID);
		row.doAction("selectfirst");
		
		if (row.isSet("FilePath")) {
			SimpleDocument sd = new SimpleDocument();
			sd.setDocumentID(documentID);
			sd.setFileName(row.getString("FileName"));
			sd.setFilePath(row.getString("FilePath"));

			/*
			sd.setCreatedDate(row.getDate("CreatedDate"));
			sd.setCreatedBy(row.getString("CreatedBy"));
			sd.setCreatedByName(new PersonName(row.getString("OwnFirstName"), row.getString("OwnLastName")));
			*/
			if (row.isSet("DocumentCaption")) sd.setDescription(row.getString("DocumentCaption"));
			else sd.setDescription(row.getString("Description"));
			
			sd.setType(row.getString("DcoumentType"));
			sd.setSubType(row.getString("DocumentSubType"));
			
			return sd;
		}
		
		return null;
	}
	
	private GenRow getApartmentSearch(final Connection conn, final UserBean user, final String search, boolean filter)
	{
		GenRow planRow = new GenRow();
		planRow.setViewSpec("properties/ProductQuickIndexApartmentDescriptionView");
		planRow.setConnection(conn);
		planRow.parseRequest(search);
		//planRow.setParameter("-select0", "*");
		planRow.setParameter("-groupby0", "ProductID");
		planRow.setParameter("ProductType", "Apartment");
		if (!planRow.isSet("StageID")) planRow.setParameter("StageID", "!NULL;!EMPTY");
		if (!planRow.isSet("EstateID")) planRow.setParameter("EstateID", "!NULL;!EMPTY");
		
		
		boolean params = search != null && search.length() > 0;
		if(filter) {
			planRow.setParameter("Active", "Y");
			String availableID = InitServlet.getSystemParam("ProductAvailableStatus");
			String availableIDs = InitServlet.getSystemParam("ProductAvailableAditionalStatuses");
			if (StringUtils.isBlank(availableIDs)) availableIDs = availableID;
			if (StringUtils.isNotBlank(availableIDs)) {
				planRow.setParameter("QuickIndexCurrentStatus.StatusID", availableIDs);
				planRow.setParameter("AvailableDate", "<=NOW+NULL");
				planRow.setParameter("ExpiryDate", ">=NOW+NULL");
			}
			boolean publishFilter = "true".equals(InitServlet.getSystemParam("API-SearchPublishWebsite"));
			if (publishFilter) planRow.setParameter("PublishedWebsite", "Y");
		}
		if(!params) {
			throw new IllegalArgumentException("Please provide the correct parameters for this API.");
		} else {
			if (planRow.getString("ApartmentName").length() > 0) {
				String apartmentName = planRow.getString("ApartmentName");
				planRow.remove("ApartmentName");
				planRow.setParameter("Name|l", apartmentName);
				planRow.setParameter("Name|l", "Apartment " + apartmentName);
			}
			if (planRow.getString("BuildingStageName").length() > 0) {
				String stageName = planRow.getString("BuildingStageName");
				planRow.remove("BuildingStageName");
				planRow.setParameter("StageName|s", stageName);
				planRow.setParameter("StageName|s", "Stage " + stageName);
				planRow.setParameter("StageName|s", "Level " + stageName);
			}
			if (planRow.getString("MarketingStatus").length() > 0) {
				planRow.setParameter("LotExclusivity", planRow.getString("MarketingStatus"));
			}
			if (planRow.getString("BuildingName").length() > 0) {
				planRow.setParameter("EstateName", planRow.getString("BuildingName"));
			}
			if (planRow.getString("BuildingID").length() > 0) {
				planRow.setParameter("EstateID", planRow.getString("BuildingID"));
			}
			if (planRow.getString("BuildingStageID").length() > 0) {
				planRow.setParameter("StageID", planRow.getString("BuildingStageID"));
			}
			if (planRow.getString("ApartmentWidth").length() > 0) {
				planRow.setParameter("HomeWidth", planRow.getString("ApartmentWidth"));
			}
			if (planRow.getString("ApartmentDepth").length() > 0) {
				planRow.setParameter("HomeDepth", planRow.getString("ApartmentDepth"));
			}
			if (planRow.getString("ApartmentArea").length() > 0) {
				planRow.setParameter("HomeArea", planRow.getString("ApartmentArea"));
			}
			if (planRow.getString("ApartmentPrice").length() > 0) {
				planRow.setParameter("HomePrice", planRow.getString("ApartmentPrice"));
			}
			if (planRow.getString("BuildWidth").length() > 0) {
				planRow.setParameter("BuildWidth", planRow.getString("BuildWidth"));
			}
			if (planRow.getString("BuildDepth").length() > 0) {
				planRow.setParameter("BuildDepth", planRow.getString("BuildDepth"));
			}
			if (planRow.getString("CanFitOnDepth").length() > 0) {
				planRow.setParameter("CanFitOnDepth", planRow.getString("CanFitOnDepth"));
			}
			if (planRow.getString("CanFitOnWidth").length() > 0) {
				planRow.setParameter("CanFitOnWidth", planRow.getString("CanFitOnWidth"));
			}
			if (planRow.getString("Storeys").length() > 0) {
				planRow.setParameter("Storeys", planRow.getString("Storeys"));
			}
			if (planRow.getString("Bedrooms").length() > 0) {
				planRow.setParameter("Bedrooms", planRow.getString("Bedrooms"));
			}
			if (planRow.getString("Bathrooms").length() > 0) {
				planRow.setParameter("Bathrooms", planRow.getString("Bathrooms"));
			}
			if (planRow.getString("CarParks").length() > 0) {
				planRow.setParameter("CarParks", planRow.getString("CarParks"));
			}
			if (planRow.getString("Study").length() > 0) {
				planRow.setParameter("Study", planRow.getString("Study") );
			}
			if (planRow.getString("Garage").length() > 0) {
				planRow.setParameter("Garage", planRow.getString("Garage"));
			}
			
			if (planRow.getString("LocationName").length() > 0) {
				planRow.setParameter("QuickIndexDisplayEntity.Name", planRow.getString("LocationName"));
				planRow.remove("LocationName");
			}
			if (planRow.getString("LocationNum").length() > 0) {
				planRow.setParameter("QuickIndexDisplayEntity.DisplayEntityProduct.ProductNum", planRow.getString("LocationNum"));
				planRow.remove("LocationNum");
			}
			if (planRow.getString("KeyWord").length() > 0) {
				String keyWord = "%" + planRow.getString("KeyWord").replaceAll("\\\\+", "%+%").replaceAll(" ", "%+%").replaceAll(";", "%;%") + "%";
				planRow.remove("KeyWord");
				planRow.setParameter("ProductQuickIndexProductDescriptionWebsite.HeadLine|k", keyWord);
				planRow.setParameter("ProductQuickIndexProductDescriptionWebsite.Description|k", keyWord);
			}
		}
		
		if (!planRow.isSet("-sort0")) {
			planRow.setParameter("-sort0","BuildingName");
			//planRow.setParameter("-sort1","BuildingStageName");
			//planRow.setParameter("-sort2","Name");
			planRow.setParameter("-sort1", "ABS(SUBSTRING(BuildingStageName,11))");
			planRow.sortBy("ABS(SUBSTRING(ProductQuickIndex.Name,11))", 2);
			planRow.sortBy("ABS(ProductQuickIndex.Name)",3);
			
		} else {
			for (int s = 0; s < 10; ++s) {
				if ("MarketingStatus".equals(planRow.getString("-sort" + s))) planRow.sortBy("LotExclusivity", s);
			}
		}
		
		planRow.setParameter("-groupby0", "ProductID");
		
		return planRow;
	}
	
	public SearchKey getMinMax(HttpServletRequest request, String searchKey) {
		String transKey = searchKey;
		if (transKey != null) transKey = transKey.replaceAll("Apartment", "Apartment");
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		row.setRequest(request);
		row.setParameter("ProductType", "Apartment");
		row.setParameter("-select0", "MIN(" + transKey + ") AS Minimum");
		row.setParameter("-select1", "MAX(" + transKey + ") AS Maximum");
		row.setParameter(transKey, ">0");
		row.doAction("selectfirst");
		
		double min = 0, max = 0;
		
		try {
			min = Double.parseDouble(row.getString("Minimum"));
		} catch (Exception e) {
			System.out.println("Error Min " +  row.getString("Minimum"));
		}

		try {
			max = Double.parseDouble(row.getString("Maximum"));
		} catch (Exception e) {
			System.out.println("Error Min " +  row.getString("Minimum"));
		}

		return new SearchKey(searchKey, min, max);
	}
	
	public List<ApartmentCheck> getApartmentsExternalCheck(final HttpServletRequest request, String estateID, String stageID, Date modifiedSince) {
		logger.trace("getApartments(Request, {}, {})", estateID, stageID);
		return getApartmentsExternalCheck(ActionBean.getConnection(request), us.getCurrentUser(request), estateID, stageID, modifiedSince);
	}
	
	public List<ApartmentCheck> getApartmentsExternalCheck(final Connection conn, final UserBean user, final String estateID, final String stageID, final Date modifiedSince) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.Apartment apartmentRow = getApartmentSearch(conn, user, estateID, stageID, modifiedSince);
		try {
			if(logger.isTraceEnabled()) {
				apartmentRow.doAction(GenerationKeys.SEARCH);
				logger.trace(apartmentRow.getStatement());
			}
			apartmentRow.getResults(true);
			if(apartmentRow.getNext()) {
				List<ApartmentCheck> list = new ArrayList<ApartmentCheck>(apartmentRow.getSize());
				do {
					list.add(new ApartmentCheck(apartmentRow));
				} while (apartmentRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			apartmentRow.close();
		}	
	}
	
	public List<Apartment> getApartmentsExternal(HttpServletRequest request, GenRow search) {
		return getApartmentsExternal(ActionBean.getConnection(request), us.getCurrentUser(request), search);
	}
	
	private List<Apartment> getApartmentsExternal(final Connection con, final UserBean user, GenRow search) {
		logger.trace("getApartmentsExternal(Conn, user={}, search={})", user != null ? user.getName() : "null user", search != null ? search.toString() : "null search");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.Apartment apartmentlist = new com.sok.runway.crm.cms.properties.Apartment(con);
		
		if(search != null)
			apartmentlist.putAll(search);
		else
			apartmentlist.setParameter("Active","Y");
		
		apartmentlist.setParameter("-select1","ProductID");

		try {
			// we need a know order for later
			apartmentlist.sortBy("ProductID", 0);
			apartmentlist.setConnection(con);
			apartmentlist.getResults(true);
			
			if(logger.isTraceEnabled()) {
				apartmentlist.doAction(GenerationKeys.SEARCH);
				logger.trace(apartmentlist.getStatement());
			}
			
			if(apartmentlist.getNext()) {
				List<Apartment> list = new ArrayList<Apartment>(apartmentlist.getSize());
				do {
					list.add(BeanFactory.external(apartmentlist));
				}while(apartmentlist.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			apartmentlist.close();
		}
	}

	public List<Apartment> getApartmentsExternal(final HttpServletRequest request, String buildingID, String stageID, Date modifiedSince, boolean active) {
		logger.trace("getApartments(Request, {}, {})", buildingID, stageID);
		return getApartmentsExternal(ActionBean.getConnection(request), us.getCurrentUser(request), buildingID, stageID, modifiedSince, active);
	}
	
	public List<Apartment> getApartmentsExternal(final Connection conn, final UserBean user, final String buildingID, final String stageID, final Date modifiedSince, boolean active) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.Apartment apartmentRow = getApartmentSearch(conn, user, buildingID, stageID, modifiedSince);
		try {
			if (active) apartmentRow.setParameter("Active", "Y");
			if(logger.isTraceEnabled()) {
				apartmentRow.doAction(GenerationKeys.SEARCH);
				logger.trace(apartmentRow.getStatement());
			}
			apartmentRow.getResults(true);
			if(apartmentRow.getNext()) {
				List<Apartment> list = new ArrayList<Apartment>(apartmentRow.getSize());
				do {
					list.add(new Apartment(apartmentRow));
				} while (apartmentRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			apartmentRow.close();
		}	
	}
	
	private com.sok.runway.crm.cms.properties.Apartment getApartmentSearch(final Connection conn, final UserBean user, final String estateID, final String stageID, final Date modifiedSince)
	{
		com.sok.runway.crm.cms.properties.Apartment apartmentRow = new com.sok.runway.crm.cms.properties.Apartment(conn);
		boolean params = false;
		if(StringUtils.isNotBlank(estateID)) {
			apartmentRow.setParameter("ProductParentBuildingStageApartment.ProductVariationBuildingStageApartment.ProductParentBuildingBuildingStage.ProductVariationBuildingBuildingStage.ProductID^1", estateID);
			params = true;
		}
		if(StringUtils.isNotBlank(stageID)) {
			apartmentRow.setParameter("ProductParentBuildingStageApartment.ProductVariationBuildingStageApartment.ProductID^1", stageID);
			params = true;
		}
		if(modifiedSince != null) {
			apartmentRow.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
			params = true;
		}
		if(!params) {
			throw new IllegalArgumentException("ModifiedSince must be supplied");
		}			
		
		//TODO - not sure if this is correct. Dion ?  
		//currentuser.setConstraint(bean, "ProductProductSecurityGroups.ProductSecurityGroupGroup");	
		// only active ? 
		apartmentRow.setParameter("-sort0","ProductNum");
		apartmentRow.setParameter("-sort1","Name");
		
		return apartmentRow;
	}

	public ApartmentEntity getApartment(final HttpServletRequest request, final String apartmentID) {
		logger.trace("getApartment(Request, {})");
		return getApartment(ActionBean.getConnection(request), us.getCurrentUser(request), apartmentID);
	}

	public ApartmentEntity getApartment(final Connection conn, final UserBean user, final String apartmentID) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(apartmentID)) {
			throw new IllegalArgumentException("You must supply a apartmentID to use this method");
		}
		ApartmentEntity product = new ApartmentEntity(conn, apartmentID);
		if(!product.isLoaded()) {
			throw new NotFoundException("The Apartment you requested could not be found: " + apartmentID);
		}
		product.setCurrentUser(user.getUser());
		if(!product.canView()) {
			throw new AccessException("You do not have permission to view the apartment with id " + apartmentID);
		}
		return product;
	}

	public List<BuildingStage> getBuildingStages(HttpServletRequest request, String estateID, Date mod, boolean active) {
		// TODO Auto-generated method stub
		return getBuildingStages(ActionBean.getConnection(request), us.getCurrentUser(request), estateID, mod, active);
	}
	
	public List<BuildingStage> getBuildingStages(final Connection con, final UserBean user, String estateID, Date modifiedSince, boolean active) {
		logger.trace("getBuildingStages(Conn, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		//if(StringUtils.isBlank(estateID)) {
		//	throw new IllegalArgumentException("EstateID must be provided");
		//}
		
		GenRow stageList = new GenRow();
		stageList.setTableSpec("Products");
		stageList.setParameter("ProductType", "Building Stage");
		if(estateID != null) {
			stageList.setParameter("ProductParentBuildingBuildingStage.ProductVariationBuildingBuildingStage.ProductID", estateID);
		}
		if(modifiedSince != null) { 
			stageList.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		stageList.setParameter("-sort0", "Name");
		stageList.setParameter("-select1","ProductID");
		if(active) {
			stageList.setParameter("Active","Y");
		}
		//security constraint ?
		try {
			stageList.setConnection(con);
			stageList.getResults(true);
			
			if(stageList.getNext()) {
				List<BuildingStage> list = new java.util.ArrayList<BuildingStage>(stageList.getSize());
				do {
					//check for can view ? 
					list.add(new BuildingStageEntity(stageList.getConnection(), stageList.getData("ProductID")).external());
				}while(stageList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			stageList.close();
		}
	}
	
	public BuildingStage getBuildingStage(final HttpServletRequest request, final String stageID) {
		logger.trace("getBuildingStage(Request, {})");
		return getBuildingStage(ActionBean.getConnection(request), us.getCurrentUser(request), stageID);
	}
	
	public BuildingStage getBuildingStage(final Connection conn, final UserBean user, final String stageID) {
		logger.trace("getBuildingStage(Conn, {}, {})", user != null ? user.getName() : "null user", stageID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(stageID)) {
			throw new IllegalArgumentException("You must supply a stageID to use this method");
		}
		BuildingStageEntity e = new BuildingStageEntity(conn);
		e.setCurrentUser(user.getUser());
		e.load(stageID);
		if(!e.isLoaded()) {
			throw new NotFoundException("The BuildingStage you requested could not be found: " + stageID);
		}
		if(!e.canView()) {
			throw new AccessException("You do not have permission to view the stage with id " + stageID);
		}
		return e.external();
	}

}
