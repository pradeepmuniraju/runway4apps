package com.sok.service.crm.cms.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.beans.Display;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class DisplayService {

	private static final DisplayService service = new DisplayService();
	private static final Logger logger = LoggerFactory.getLogger(DisplayService.class);
	private static final UserService us = UserService.getInstance();
	public static DisplayService getInstance() {
		return service;
	}
	
	public GenRow getDisplayGenRow() {
		final GenRow row = new GenRow();
		row.setTableSpec("ProductDisplayOptions");
		row.setParameter("-select0","Address");
		row.setParameter("-groupby0", "Address");
		return row;
	}
	
	
	public List<Display> getDisplays(final HttpServletRequest request, final String type) {
		logger.debug("getDisplays(Req, {})",type);
		return getDisplays(ActionBean.getConnection(request), us.getCurrentUser(request), type);
	}
	
	public List<Display> getDisplays(Connection conn, UserBean user, final String type) {
		logger.debug("getDisplays(Con, {}, {})", user != null ? user.getName() : "null user", type);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You cannot access the products module");
		}
		GenRow row = getDisplayGenRow();
		try {
			if("display".equals(type)) {
				row.setParameter("Type", "O+D");
			} else if("estate".equals(type)) {
				row.setParameter("Type", "E");
			} else if("option".equals(type)) {
				row.setParameter("Type","O");
			} else if("decor".equals(type)) {
				row.setParameter("Type","D");
			}
			row.setConnection(conn);
			if(logger.isTraceEnabled()) {
				row.doAction(GenerationKeys.SEARCH);
				logger.trace(row.getSearchStatement());
			}
			row.getResults(true);
			
			if(row.getNext()) {
				List<Display> list = new ArrayList<Display>(row.size());
				do {
					
					list.add(new Display(row.getString("Address"), row.getString("Address")));
				} while(row.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			row.close();
		}
	}
}
