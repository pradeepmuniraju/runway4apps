package com.sok.service.crm.cms.properties;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.ErrorMap;
import com.sok.runway.UserBean;
import com.sok.runway.crm.Product;
import com.sok.runway.crm.cms.properties.Land;
import com.sok.runway.crm.cms.properties.entities.EstateEntity;
import com.sok.runway.crm.cms.properties.entities.LotEntity;
import com.sok.runway.crm.cms.properties.entities.StageEntity;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.cms.properties.Estate;
import com.sok.runway.externalInterface.beans.cms.properties.Home;
import com.sok.runway.externalInterface.beans.cms.properties.HomeDetails;
import com.sok.runway.externalInterface.beans.cms.properties.LegacyLot;
import com.sok.runway.externalInterface.beans.cms.properties.Lot;
import com.sok.runway.externalInterface.beans.cms.properties.LotCheck;
import com.sok.runway.externalInterface.beans.cms.properties.Plan;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartmentCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexLot;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexLotCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexStatusHistory;
import com.sok.runway.externalInterface.beans.cms.properties.SearchKey;
import com.sok.runway.externalInterface.beans.cms.properties.Stage;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.houseandland.CreateHouseAndLand;
import com.sok.runway.offline.rpmManager.RPMtask;

public class EstateService {

	private static final EstateService service = new EstateService();
	private static final Logger logger = LoggerFactory.getLogger(EstateService.class);
	/* TODO - private */protected static final UserService us = UserService.getInstance();
	public static EstateService getInstance() {
		return service;
	}
	
	public List<Estate> getEstates(final HttpServletRequest request) {
		return getEstates(request, null, true);
	}
	
	/* request methods */
	public List<Estate> getEstates(final HttpServletRequest request, Date modifiedSince, boolean active) {
		logger.trace("getEstates(Request, modifiedSince={})", String.valueOf(modifiedSince));
		return getEstates(ActionBean.getConnection(request), us.getCurrentUser(request), modifiedSince, active);
	}

	public Estate getEstate(final HttpServletRequest request, final String estateID) {
		logger.trace("getEstate(Request, {})");
		return getEstate(ActionBean.getConnection(request), us.getCurrentUser(request), estateID);
	}
	
	/* internal methods  */
	// TODO getEstateEntities ? 
	public List<Estate> getEstates(final Connection con, final UserBean user, final Date modifiedSince, final boolean active) {
		logger.trace("getEstates(Conn, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow estateList = new GenRow();
		estateList.setTableSpec("Products");
		estateList.setParameter("ProductType", "Estate");
		if(modifiedSince != null) { 
			estateList.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		estateList.setParameter("-sort0", "Name");
		estateList.setParameter("-select1","ProductID");
		if(active) {
			estateList.setParameter("Active","Y");
		}
		//security constraint ?
		try {
			estateList.setConnection(con);
			estateList.getResults(true);

			if(estateList.getNext()) {
				List<Estate> list = new java.util.ArrayList<Estate>(estateList.getSize());
				do {
					//check for can view ? 
					list.add(BeanFactory.external(new EstateEntity(estateList.getConnection(), estateList.getData("ProductID"))));
				}while(estateList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			estateList.close();
		}
	}


	public List<Stage> getStages(HttpServletRequest request, String estateID, Date mod, boolean active) {
		// TODO Auto-generated method stub
		return getStages(ActionBean.getConnection(request), us.getCurrentUser(request), estateID, mod, active);
	}
	
	public List<Stage> getStages(final Connection con, final UserBean user, String estateID, Date modifiedSince, boolean active) {
		logger.trace("getStages(Conn, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		//if(StringUtils.isBlank(estateID)) {
		//	throw new IllegalArgumentException("EstateID must be provided");
		//}
		
		GenRow stageList = new GenRow();
		stageList.setTableSpec("Products");
		stageList.setParameter("ProductType", "Stage");
		if(estateID != null) {
			stageList.setParameter("ProductParentEstateStage.ProductVariationEstateStage.ProductID", estateID);
		}
		if(modifiedSince != null) { 
			stageList.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		stageList.setParameter("-sort0", "Name");
		stageList.setParameter("-select1","ProductID");
		if(active) {
			stageList.setParameter("Active","Y");
		}
		//security constraint ?
		try {
			stageList.setConnection(con);
			stageList.getResults(true);
			
			if(stageList.getNext()) {
				List<Stage> list = new java.util.ArrayList<Stage>(stageList.getSize());
				do {
					//check for can view ? 
					list.add(new StageEntity(stageList.getConnection(), stageList.getData("ProductID")).external());
				}while(stageList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			stageList.close();
		}
	}
	
	public Estate getEstate(final Connection conn, final UserBean user, final String estateID) {
		logger.trace("getEstate(Conn, {}, {})", user != null ? user.getName() : "null user", estateID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(estateID)) {
			throw new IllegalArgumentException("You must supply a estateID to use this method");
		}
		EstateEntity e = new EstateEntity(conn);
		e.setCurrentUser(user.getUser());
		e.load(estateID);
		if(!e.isLoaded()) {
			throw new NotFoundException("The Estate you requested could not be found: " + estateID);
		}
		if(!e.canView()) {
			throw new AccessException("You do not have permission to view the estate with id " + estateID);
		}
		return BeanFactory.external(e); 
	}
	
	public Stage getStage(final HttpServletRequest request, final String stageID) {
		logger.trace("getStage(Request, {})");
		return getStage(ActionBean.getConnection(request), us.getCurrentUser(request), stageID);
	}
	
	public Stage getStage(final Connection conn, final UserBean user, final String stageID) {
		logger.trace("getStage(Conn, {}, {})", user != null ? user.getName() : "null user", stageID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(stageID)) {
			throw new IllegalArgumentException("You must supply a stageID to use this method");
		}
		StageEntity e = new StageEntity(conn);
		e.setCurrentUser(user.getUser());
		e.load(stageID);
		if(!e.isLoaded()) {
			throw new NotFoundException("The Stage you requested could not be found: " + stageID);
		}
		if(!e.canView()) {
			throw new AccessException("You do not have permission to view the stage with id " + stageID);
		}
		return e.external();
	}

	public LotEntity getLot(final HttpServletRequest request, final String lotID) {
		logger.trace("getLot(Request, {})");
		return getLot(ActionBean.getConnection(request), us.getCurrentUser(request), lotID);
	}
	
	public List<LegacyLot> getLotsExternalLegacy(final HttpServletRequest request, String estateID, String stageID, Date modifiedSince) {
		logger.trace("getLots(Request, {}, {})", estateID, stageID);
		return getLotsExternalLegacy(ActionBean.getConnection(request), us.getCurrentUser(request), estateID, stageID, modifiedSince);
	}
	
	
	// Deprecated, sort of. We could have done the conversion to the legacy lot type in the resource / endpoint, but it's more efficient this way.
	public List<LegacyLot> getLotsExternalLegacy(final Connection conn, final UserBean user, final String estateID, final String stageID, final Date modifiedSince) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		Land landRow = getLotSearch(conn, user, estateID, stageID, modifiedSince);
		try {
			if(logger.isTraceEnabled()) {
				landRow.doAction(GenerationKeys.SEARCH);
				logger.trace(landRow.getStatement());
			}
			landRow.getResults(true);
			if(landRow.getNext()) {
				List<LegacyLot> list = new ArrayList<LegacyLot>(landRow.getSize());
				do {
					list.add(new LegacyLot(landRow));
				} while (landRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			landRow.close();
		}	
	}
	
	public List<Lot> getLotsExternal(final HttpServletRequest request, String estateID, String stageID, Date modifiedSince, boolean active) {
		logger.trace("getLots(Request, {}, {})", estateID, stageID);
		return getLotsExternal(ActionBean.getConnection(request), us.getCurrentUser(request), estateID, stageID, modifiedSince, active);
	}
	
	public List<Lot> getLotsExternal(final Connection conn, final UserBean user, final String estateID, final String stageID, final Date modifiedSince, boolean active) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		Land landRow = getLotSearch(conn, user, estateID, stageID, modifiedSince);
		if (active) landRow.setParameter("Active", "Y");
		try {
			if(logger.isTraceEnabled()) {
				landRow.doAction(GenerationKeys.SEARCH);
				logger.trace(landRow.getStatement());
			}
			landRow.getResults(true);
			if(landRow.getNext()) {
				List<Lot> list = new ArrayList<Lot>(landRow.getSize());
				do {
					list.add(new Lot(landRow));
				} while (landRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			landRow.close();
		}	
	}
	
	public List<QuickIndexLot> getLotsSearch(final HttpServletRequest request, String search, boolean filtered) {
		logger.trace("getLotsSearch(Request, {}, {})", search, filtered);
		return getLotsSearch(ActionBean.getConnection(request), us.getCurrentUser(request), search, filtered);
	}
	
	public List<QuickIndexLot> getLotsSearch(final Connection conn, final UserBean user, String search, boolean filtered) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow landRow = getLotSearch(conn, user, search, filtered);
		try {
			if(logger.isTraceEnabled()) {
				landRow.doAction(GenerationKeys.SEARCH);
				logger.trace(landRow.getStatement());
			}
			landRow.getResults(true);
			
			if(landRow.getNext()) {
				List<QuickIndexLot> list = new ArrayList<QuickIndexLot>(landRow.getSize());
				do {
					QuickIndexLot qil = new QuickIndexLot(landRow);
					if (landRow.isSet("ThumbDocumentID")) qil.setThumbnailImage(getImageDocument(conn, landRow.getString("ThumbDocumentID")));
					if (landRow.isSet("ImageDocumentID")) qil.setMainImage(getImageDocument(conn, landRow.getString("ImageDocumentID")));
					qil.setPackageCount(getPackageCount(conn, landRow.getString("ProductID")));
					qil.setStatusHistory(getStatus(landRow));
					list.add(qil);
				} while (landRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			landRow.close();
		}	
	}
	
	public List<LotCheck> getLotsExternalCheck(final HttpServletRequest request, String estateID, String stageID, Date modifiedSince) {
		logger.trace("getLots(Request, {}, {})", estateID, stageID);
		return getLotsExternalCheck(ActionBean.getConnection(request), us.getCurrentUser(request), estateID, stageID, modifiedSince);
	}
	
	public List<LotCheck> getLotsExternalCheck(final Connection conn, final UserBean user, final String estateID, final String stageID, final Date modifiedSince) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		Land landRow = getLotSearch(conn, user, estateID, stageID, modifiedSince);
		try {
			if(logger.isTraceEnabled()) {
				landRow.doAction(GenerationKeys.SEARCH);
				logger.trace(landRow.getStatement());
			}
			landRow.getResults(true);
			if(landRow.getNext()) {
				List<LotCheck> list = new ArrayList<LotCheck>(landRow.getSize());
				do {
					list.add(new LotCheck(landRow));
				} while (landRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			landRow.close();
		}	
	}
	
	private Land getLotSearch(final Connection conn, final UserBean user, final String estateID, final String stageID, final Date modifiedSince)
	{
		Land landRow = new Land(conn);
		boolean params = false;
		if(StringUtils.isNotBlank(estateID)) {
			landRow.setParameter("ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ProductID^1", estateID);
			params = true;
		}
		if(StringUtils.isNotBlank(stageID)) {
			landRow.setParameter("ProductParentStageLot.ProductVariationStageLot.ProductID^1", stageID);
			params = true;
		}
		if(modifiedSince != null) {
			landRow.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
			params = true;
		}
		if(!params) {
			throw new IllegalArgumentException("ModifiedSince must be supplied");
		}			
		
		//TODO - not sure if this is correct. Dion ?  
		//currentuser.setConstraint(bean, "ProductProductSecurityGroups.ProductSecurityGroupGroup");	
		// only active ? 
		landRow.setParameter("-sort0","ProductNum");
		landRow.setParameter("-sort1","Name");
		
		return landRow;
	}
	
	private GenRow getLotSearch(final Connection conn, final UserBean user, final String search, boolean filter)
	{
		GenRow landRow = new GenRow();
		landRow.setViewSpec("properties/ProductQuickIndexDescriptionView");
		landRow.setConnection(conn);
		landRow.parseRequest(search);
		landRow.setParameter("-groupby0", "ProductID");
		landRow.setParameter("ProductType", "Land");
		if (!landRow.isSet("StageID")) landRow.setParameter("StageID", "!NULL;!EMPTY");
		if (!landRow.isSet("EstateID")) landRow.setParameter("EstateID", "!NULL;!EMPTY");
		
		boolean params = search != null && search.length() > 0;
		if(filter) {
			landRow.setParameter("Active", "Y");
			String availableID = InitServlet.getSystemParam("ProductAvailableStatus");
			String availableIDs = InitServlet.getSystemParam("ProductAvailableAditionalStatuses");
			if (StringUtils.isBlank(availableIDs)) availableIDs = availableID;
			if (StringUtils.isNotBlank(availableIDs)) {
				landRow.setParameter("QuickIndexCurrentStatus.StatusID", availableIDs);
				landRow.setParameter("AvailableDate", "<=NOW+NULL");
				landRow.setParameter("ExpiryDate", ">=NOW+NULL");
			}
			boolean publishFilter = "true".equals(InitServlet.getSystemParam("API-SearchPublishWebsite"));
			if (publishFilter) landRow.setParameter("PublishedWebsite", "Y");
		}
		if(!params) {
			throw new IllegalArgumentException("Please provide the correct parameters for this API.");
		} else {
			if (landRow.getString("MarketingStatus").length() > 0) {
				landRow.setParameter("LotExclusivity", landRow.getString("MarketingStatus"));
			}
			if (landRow.getString("LotName").length() > 0) {
				String lotName = landRow.getString("LotName");
				landRow.remove("LotName");
				landRow.setParameter("Name|l", lotName);
				landRow.setParameter("Name|l", "Lot " + lotName);
			}
			if (landRow.getString("StageName").length() > 0) {
				String stageName = landRow.getString("StageName");
				landRow.remove("StageName");
				landRow.setParameter("StageName|s", stageName);
				landRow.setParameter("StageName|s", "Stage " + stageName);
			}
			if (landRow.getString("LotWidth").length() > 0) {
				landRow.setParameter("LotWidth", landRow.getString("LotWidth"));
			}
			if (landRow.getString("LotDepth").length() > 0) {
				landRow.setParameter("LotDepth", landRow.getString("LotDepth"));
			}
			if (landRow.getString("LotArea").length() > 0) {
				landRow.setParameter("LotArea", landRow.getString("LotArea"));
			}
			if (landRow.getString("LotPrice").length() > 0) {
				landRow.setParameter("LotPrice", landRow.getString("LotPrice"));
			}
			if (landRow.getString("LocationName").length() > 0) {
				landRow.setParameter("QuickIndexDisplayEntity.Name", landRow.getString("LocationName"));
				landRow.remove("LocationName");
			}
			if (landRow.getString("LocationNum").length() > 0) {
				landRow.setParameter("QuickIndexDisplayEntity.DisplayEntityProduct.ProductNum", landRow.getString("LocationNum"));
				landRow.remove("LocationNum");
			}
			if (landRow.getString("KeyWord").length() > 0) {
				String keyWord = "%" + landRow.getString("KeyWord").replaceAll("\\\\+", "%+%").replaceAll(" ", "%+%").replaceAll(";", "%;%") + "%";
				landRow.remove("KeyWord");
				landRow.setParameter("ProductQuickIndexProductDescriptionWebsite.HeadLine|k", keyWord);
				landRow.setParameter("ProductQuickIndexProductDescriptionWebsite.Description|k", keyWord);
			}
		}
		
		//TODO - not sure if this is correct. Dion ?  
		//currentuser.setConstraint(bean, "ProductProductSecurityGroups.ProductSecurityGroupGroup");	
		// only active ? 
		if (landRow.getString("-sort0").length() == 0) {
			landRow.setParameter("-sort0","EstateName");
			//landRow.setParameter("-sort1","StageName");
			//landRow.setParameter("-sort2","Name");
			landRow.setParameter("-sort1", "ABS(SUBSTRING(StageName,11))");
			landRow.sortBy("ABS(SUBSTRING(ProductQuickIndex.Name,11))", 2);
			landRow.sortBy("ABS(ProductQuickIndex.Name)",3);
			
		} else {
			for (int s = 0; s < 10; ++s) {
				if ("MarketingStatus".equals(landRow.getString("-sort" + s))) landRow.sortBy("LotExclusivity", s);
			}
		}
		
		landRow.setParameter("-groupby0", "ProductID");
		
		return landRow;
	}
	
	public LotEntity getLot(final Connection conn, final UserBean user, final String lotID) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(lotID)) {
			throw new IllegalArgumentException("You must supply a lotID to use this method");
		}
		LotEntity product = new LotEntity(conn, lotID);
		if(!product.isLoaded()) {
			throw new NotFoundException("The Lot you requested could not be found: " + lotID);
		}
		product.setCurrentUser(user.getUser());
		if(!product.canView()) {
			throw new AccessException("You do not have permission to view the lot with id " + lotID);
		}
		return product;
	}

	public LegacyLot updateLot(HttpServletRequest request, LegacyLot lot) {
		logger.debug("updateLot(request, legacyLot)");
		return updateLot(ActionBean.getConnection(request), us.getCurrentUser(request), lot);
	}

	public LegacyLot updateLot(Connection con, UserBean user, LegacyLot externalLot) {
		logger.debug("updateLot(con, user, legacyLot)");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		LotEntity lot = new LotEntity(con, externalLot); 
		lot.setCurrentUser(user.getUser());
		
		return new LegacyLot(BeanFactory.external(updateLot(lot)));
	}
	
	/* 
	 * Notes on conversion - this is also being used by the soap endpoint at ws/PropertyAPI.jws
	 * Changes will need to be confirmed working there also (suspect bean.Lot will wrap or be set from entity.Lot)
	 */
	private LotEntity updateLot(LotEntity lot) {
		logger.debug("updateLot(Entity)");
		boolean isNew = true;
		if(StringUtils.isNotBlank(lot.getPrimaryKey())) {	
			logger.debug("lot had primary key, doing update with id [{}], name={} string[{}]", new String[]{ lot.getPrimaryKey(), lot.getPrimaryKeyName(), lot.getRecord().getString("ProductID")});
			LotEntity lookup = new LotEntity(lot.getConnection());
			lookup.load(lot.getPrimaryKey()); 
			if(!lookup.isLoaded()) {
				throw new NotFoundException("The Lot you requested could not be found, new lots should not be submitted with an ID : " + lot.getPrimaryKey());
			}
			lookup.setCurrentUser(lot.getCurrentUser());
			if(!lookup.canEdit()) {
				throw new AccessException("You do not have permission to edit the lot with id " + lot.getPrimaryKey());
			}
			isNew = false;
		} else if(StringUtils.isNotBlank(lot.getField("Name")) && StringUtils.isNotBlank(lot.getField("StageProductID"))) { 
			// lookup lot based on stage and name
			logger.debug("lookup of estate/stage based on StageProductID={} and Name={}", lot.getField("StageProductID"), lot.getField("Name"));
			LotEntity lookup = new LotEntity(lot.getConnection());
			lookup.loadFromField(new String[]{"Name", "ProductParentStageLot.ProductVariationStageLot.ProductID"}, new String[]{lot.getField("Name"), lot.getField("StageProductID")});
			if(lookup.isLoaded()) {
				logger.debug("lookup successful, setting primary key: [{}]", lookup.getPrimaryKey());
				logger.trace(lookup.getStatement());
				lookup.setCurrentUser(lot.getCurrentUser());
				isNew = false;
				if(!lookup.canEdit()) {
					throw new AccessException("This lot already exists and you do not have permission to edit it. It's ID was:" + lookup.getPrimaryKey());
				}
				lot.setPrimaryKey(lookup.getPrimaryKey());
			}
		}
		if (isNew && !lot.canCreate()) {
			throw new AccessException("You do not have permission to create new lots");
		}
		logger.debug( isNew  ? "doing insert " : "doing update");
		logger.trace(lot.toString());
		ErrorMap em = isNew ? lot.insert() : lot.update();	
		if(em.isEmpty()) {
			logger.debug("update success, refresh index");
			RPMtask.getInstance().refreshData(RPMtask.type.land);
			RPMtask.getInstance().refreshIndex("update",lot.getPrimaryKey(),"Land");
			logger.debug("load lot");
			lot.load(lot.getPrimaryKey());
			logger.debug("return lot");
			return lot;
		} else {
			logger.error(em.toString());
			throw new RuntimeException("Error in persistence " + em.toString());
		}
	}
	
	
	public Stage updateStage(Connection con, UserBean user, Stage stage) {
		logger.debug("updateStage(con, user, stage)");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		StageEntity se = new StageEntity(con, stage); 
		se.setCurrentUser(user.getUser());
		
		return BeanFactory.external(updateStage(con, user, se));
	}

	public StageEntity updateStage(Connection con, UserBean user, StageEntity se) {
		
		logger.debug("updateStage(con, user, stage)");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(!user.canCreate("Products")) {
			throw new AccessException("You do not have permission to create new stages");
		}
		
		boolean isNew = true; 
		if(StringUtils.isNotBlank(se.getPrimaryKey())) {	
			logger.debug("stage had primary key, doing update with id [{}], name={} string[{}]", new String[]{ se.getPrimaryKey(), se.getPrimaryKeyName(), se.getRecord().getString("ProductID")});
			StageEntity lookup = new StageEntity(se.getConnection());
			lookup.load(se.getPrimaryKey()); 
			if(!lookup.isLoaded()) {
				throw new NotFoundException("The Lot you requested could not be found, new lots should not be submitted with an ID : " + se.getPrimaryKey());
			}
			lookup.setCurrentUser(se.getCurrentUser());
			if(!lookup.canEdit()) {
				throw new AccessException("You do not have permission to edit the lot with id " + se.getPrimaryKey());
			}
			isNew = false;
		} else if(StringUtils.isNotBlank(se.getField("Name")) && StringUtils.isNotBlank(se.getField("EstateProductID"))) { 
			// lookup lot based on stage and name
			logger.debug("lookup of estate/stage based on EstateProductID={} and Name={}", se.getField("EstateProductID"), se.getField("Name"));
			StageEntity lookup = new StageEntity(se.getConnection());
			lookup.loadFromField(new String[]{"Name", "ProductParentEstateStage.ProductVariationEstateStage.ProductID"}, new String[]{se.getField("Name"), se.getField("EstateProductID")});
			if(lookup.isLoaded()) {
				logger.debug("lookup successful, setting primary key: [{}]", lookup.getPrimaryKey());
				logger.trace(lookup.getStatement());
				lookup.setCurrentUser(se.getCurrentUser());
				isNew = false;
				if(!lookup.canEdit()) {
					throw new AccessException("This stage already exists and you do not have permission to edit it. It's ID was:" + lookup.getPrimaryKey());
				}
				se.setPrimaryKey(lookup.getPrimaryKey());
			}
		}
		
		// 		logger.debug( isNew  ? "doing insert " : "doing update");
		logger.trace(se.toString());
		ErrorMap em = isNew ? se.insert() : se.update();	
		if(em.isEmpty()) {
			logger.debug("update success, refresh index");
			logger.debug("load stage");
			se.load(se.getPrimaryKey());
			logger.debug("return lot");
			return se;
		} else {
			logger.error(em.toString());
			throw new RuntimeException("Error in persistence " + em.toString());
		}
	}
	
	public List<Lot> getLotsExternal(HttpServletRequest request, GenRow search) {
		return getLotsExternal(ActionBean.getConnection(request), us.getCurrentUser(request), search);
	}
	
	private List<Lot> getLotsExternal(final Connection con, final UserBean user, GenRow search) {
		logger.trace("getLotsExternal(Conn, user={}, search={})", user != null ? user.getName() : "null user", search != null ? search.toString() : "null search");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.Land landList = new com.sok.runway.crm.cms.properties.Land(con);
		
		if(search != null)
			landList.putAll(search);
		else
			landList.setParameter("Active","Y");
		
		landList.setParameter("-select1","ProductID");

		try {
			// we need a know order for later
			landList.sortBy("ProductID", 0);
			landList.setConnection(con);
			landList.getResults(true);
			
			if(logger.isTraceEnabled()) {
				landList.doAction(GenerationKeys.SEARCH);
				logger.trace(landList.getStatement());
			}
			
			if(landList.getNext()) {
				List<Lot> list = new ArrayList<Lot>(landList.getSize());
				do {
					list.add(BeanFactory.external(landList));
				}while(landList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			landList.close();
		}
	}

	private QuickIndexStatusHistory getStatus(GenRow packageRow) {
		QuickIndexStatusHistory qish = new QuickIndexStatusHistory();
		
		qish.setProductID(packageRow.getString("ProductID"));
		qish.setProductStatusID(packageRow.getString("ProductStatusID"));
		qish.setStatus(packageRow.getString("CurrentStatus"));
		qish.setStatusID(packageRow.getString("CurrentStatusID"));
		
		return qish;
	}
	private SimpleDocument getImageDocument(Connection conn, String documentID) {
		GenRow row = new GenRow();
		row.setTableSpec("Documents");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("DocumentID", documentID);
		row.doAction("selectfirst");
		
		if (row.isSet("FilePath")) {
			SimpleDocument sd = new SimpleDocument();
			sd.setDocumentID(documentID);
			sd.setFileName(row.getString("FileName"));
			sd.setFilePath(row.getString("FilePath"));

			/*
			sd.setCreatedDate(row.getDate("CreatedDate"));
			sd.setCreatedBy(row.getString("CreatedBy"));
			sd.setCreatedByName(new PersonName(row.getString("OwnFirstName"), row.getString("OwnLastName")));
			*/
			if (row.isSet("DocumentCaption")) sd.setDescription(row.getString("DocumentCaption"));
			else sd.setDescription(row.getString("Description"));
			
			sd.setType(row.getString("DocumentType"));
			sd.setSubType(row.getString("DocumentSubType"));
			
			return sd;
		}
		
		return null;
	}
	
	public SearchKey getMinMax(HttpServletRequest request, String searchKey) {
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		row.setRequest(request);
		row.setParameter("ProductType", "Land");
		row.setParameter("-select0", "MIN(" + searchKey + ") AS Minimum");
		row.setParameter("-select1", "MAX(" + searchKey + ") AS Maximum");
		row.setParameter(searchKey, ">0");
		row.doAction("selectfirst");
		
		double min = 0, max = 0;
		
		try {
			min = Double.parseDouble(row.getString("Minimum"));
		} catch (Exception e) {
			System.out.println("Error Min " +  row.getString("Minimum"));
		}

		try {
			max = Double.parseDouble(row.getString("Maximum"));
		} catch (Exception e) {
			System.out.println("Error Min " +  row.getString("Minimum"));
		}

		return new SearchKey(searchKey, min, max);
	}

	public List<Home> findLand(HttpServletRequest request, String lotID) {
		return findLand(ActionBean.getConnection(request), us.getCurrentUser(request), lotID);
	}
	
	public List<Home> findLand(Connection con, final UserBean user, String lotID) {
		logger.trace("getLotsExternal(Conn, user={}, lotID={})", user != null ? user.getName() : "null user", lotID != null ? lotID.toString() : "null lotID");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}

		com.sok.runway.crm.cms.properties.Home homeList = new com.sok.runway.crm.cms.properties.Home(con);
		
		homeList.setParameter("Active","Y");
		
		homeList.setParameter("-select1","ProductID");

		try {
			CreateHouseAndLand hal = new CreateHouseAndLand(homeList);
			// we need a know order for later
			homeList.sortBy("ProductID", 0);
			homeList.setConnection(con);
			homeList.getResults();
			
			if(logger.isTraceEnabled()) {
				homeList.doAction(GenerationKeys.SEARCH);
				logger.trace(homeList.getStatement());
			}
			
			if(homeList.getNext()) {
				List<Home> list = new ArrayList<Home>(homeList.getSize());
				do {
					Home h = BeanFactory.external(homeList);
					
					List<Plan> ps = h.getPlans();
					
					if (ps != null && ps.size() > 0) {
						for (int x = 0; x < ps.size(); ++x) {
							Plan p = ps.get(x);
							
							boolean b = hal.checkRules(lotID, p.getPlanID(),"");
							if (!b) {
								ps.remove(x);
								--x;
							}
						}
					
						if (ps.size() > 0) {
							h.setPlans(ps);
							list.add(h);
						}
					}
				}while(homeList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			homeList.close();
		}
	}
	
	public List<QuickIndexLotCollections> getLandCollectionSearch(final HttpServletRequest request, String search, String collection) {
		logger.trace("getApartmentsSearch(Request, {}, {})", search, collection);
		return getLandCollectionSearch(ActionBean.getConnection(request), us.getCurrentUser(request), search, collection);
	}
	
	public List<QuickIndexLotCollections> getLandCollectionSearch(final Connection conn, final UserBean user, String search, String collection) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow planRow = getLotSearch(conn, user, search, true);
		try {
			String[] parts = null;
			if (StringUtils.isNotBlank(collection)) {
				parts = collection.split("\\+");
				if (parts != null && parts.length > 0) {
					for (int p = 0; p < parts.length; ++p) {
						if ("MarketingStatus".equals(parts[p])) parts[p] = "LotExclusivity";
						planRow.setParameter("-groupby" + p, parts[p]);
					}
				}
			}
			if(logger.isTraceEnabled()) {
				planRow.doAction(GenerationKeys.SEARCH);
				logger.trace(planRow.getStatement());
			}
			planRow.getResults(true);
			
			System.out.println(planRow.getStatement());
			
			if(planRow.getNext()) {
				List<QuickIndexLotCollections> list = new ArrayList<QuickIndexLotCollections>(planRow.getSize());
				do {
					QuickIndexLotCollections qip = new QuickIndexLotCollections(planRow);
					//if (planRow.isSet("ThumbDocumentID")) qip.setThumbnailImage(getImageDocument(conn, planRow.getString("ThumbDocumentID")));
					//if (planRow.isSet("ImageDocumentID")) qip.setMainImage(getImageDocument(conn, planRow.getString("ImageDocumentID")));
					//qip.setStatusHistory(getStatus(planRow));
					String local = search;
					if (parts != null && parts.length > 0) {
						for (int p = 0; p < parts.length; ++p) {
							if (local.length() > 0) local += "&";
							if (planRow.isSet(parts[p]))
								local += parts[p] + "=" + planRow.getData(parts[p]);
							else
								local += parts[p] + "=EMPTY+NULL";
						}
					}
					qip.setLotList(getLotsSearch(conn, user, local, true));
					list.add(qip);
				} while (planRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			planRow.close();
		}	
	}
	
	public static String getPackageCount(final Connection conn, String productID) {
		String c = "0";
		if(StringUtils.isNotBlank(productID)) {
			GenRow row = PackageService.getInstance().getPlanSearch(conn, null, "LotID=" + productID, true);
			row.doAction(GenerationKeys.SEARCH);
			row.getResults(true);
			
			logger.debug("getPackageCount {} ", row.getStatement());
			
			if(row.isSuccessful()) {
				c = Integer.toString(row.getSize());
			} 
			
			row.close();
		}
		return c;
	}
}
