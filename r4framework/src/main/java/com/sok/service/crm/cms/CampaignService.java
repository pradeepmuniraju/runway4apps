package com.sok.service.crm.cms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.sql.Connection;

import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.campaigns.Campaign;
import com.sok.runway.externalInterface.resources.UtilityResource;
import com.sok.runway.externalInterface.resources.UtilityResource.Progress;
import com.sok.runway.houseandland.CreateHouseAndLand;
import com.sok.runway.offline.ThreadManager;
import com.sok.runway.offline.rpmManager.RPMtask;


public class CampaignService {
	private static final CampaignService service = new CampaignService();
	private static final Logger logger = LoggerFactory.getLogger(CampaignService.class);
	private static final UserService us = UserService.getInstance(); 
	public static CampaignService getInstance() {
		return service;
	}
	private CampaignService() {}
	
	public List<Campaign> getCampaigns(HttpServletRequest request, String type) {
		return getCampaigns(ActionBean.getConnection(request), us.getCurrentUser(request), type);
	}
	
	public List<Campaign> getCampaigns(Connection con, UserBean user, String type) {
		
		GenRow ct = new GenRow();
		ct.setConnection(con);
		try {
			ct.setTableSpec("Campaigns");
			ct.setParameter("-select1","CampaignID");

			if(StringUtils.isNotBlank(type)) {
				ct.setParameter("Type", type);
			}
			user.setConstraint(ct, "CampaignLinkedCampaign.LinkedCampaignGroup");
			if(logger.isTraceEnabled()) { 
				ct.doAction(GenerationKeys.SEARCH);
				logger.trace(ct.getStatement());
			}
			ct.getResults(true);
			
			if(ct.getNext()) {
				List<Campaign> list = new ArrayList<Campaign>(ct.getSize());
				do {
					list.add(new Campaign(con, ct.getData("CampaignID")));
				} while(ct.getNext());
				return list;
			} 
			return Collections.emptyList();
		} finally {
			ct.close();
		}
	}
	
	public Campaign getCampaign(Connection con, UserBean user, String campaignID) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(StringUtils.isBlank(campaignID)) {
			throw new IllegalArgumentException("CampaignID must be supplied");
		}
		final Campaign c = new Campaign(con, campaignID);
		if(!c.isLoaded()) {
			throw new NotFoundException("Campaign could not be found with id " + campaignID);
		}
		c.setCurrentUser(user.getUser());		
		if(c.canView()) {
			return c;
		}
		throw new AccessException("You do not have permission to view this campaign");
	}
	
	private static final UtilityResource utilResource = UtilityResource.getInstance();
	
	public String applyCampaignDrips(HttpServletRequest request, final String campaignID) {
		return applyCampaignDrips(request, (HttpServletResponse)null, campaignID);
	}
	
	public String applyCampaignDrips(HttpServletRequest request, HttpServletResponse response, final String campaignID) {
		final String progressId = "ApplyCampaignDrips-" + campaignID;
		/* is task currently running ? */
		Progress ep = utilResource.getProgressItem(request, response, progressId);
		if(ep == null || ep.complete) {
			//create a new process
			utilResource.createProgress(request, progressId, "Apply Campaign Drips");
			final UserBean user = us.getCurrentUser(request);
			ThreadManager.startThread(new Runnable() {
				public void run() {
					GenRow r = new GenRow();
					try { 
						ActionBean.connect(r);
						r.setCloseConnection(true);
						applyCampaignDrips(r.getConnection(), user, campaignID);
					} finally {
						utilResource.completeProgressItem(user, progressId);
						r.close();
					}
				}
			});
		} else {
			logger.debug("previous progress continues, allegedly");
		}
		return progressId;
	}
	
	public void applyCampaignDrips(Connection con, UserBean user, String campaignID) {
		logger.debug("applyCampaignDrips(con, user={}, campaignID={})", user != null ? user.getName() : "null user", campaignID);
		Campaign c = getCampaign(con, user, campaignID);
		Set<String> dripIDs = getCampaignDrips(con, c.getCampaignID());
		if(dripIDs.isEmpty()) {
			logger.warn("Did not find any campaign drips to apply for campaign {}", campaignID);
			return;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Found drips [{}] to apply to campaign {}", StringUtils.join(dripIDs,","), c.getCampaignID());
		}
		final CreateHouseAndLand chnl = new CreateHouseAndLand(null, null, InitServlet.getConfig().getServletContext());
		GenRow productlist = new GenRow();
		productlist.setViewSpec("CampaignProductView");
		try { 
			productlist.setConnection(con); 
			productlist.setColumn("CampaignID",campaignID);
			user.setConstraint(productlist, "CampaignProductProduct.ProductGroup");
			
			productlist.doAction("search");
			logger.debug(productlist.getStatement());
			productlist.getResults(true);			
			
			Double tot = new Double(productlist.getSize());
			String progressId = tot.intValue() != 0 ? "ApplyCampaignDrips-" + campaignID : null;
			UtilityResource.getInstance().updateProgressItem(user, progressId, 0.01, "Apply Campaign - Phase 1/2", 0, tot.intValue());
			
			logger.debug("before while");
			
			Set<String> idList = new HashSet<String>();

			while(productlist.getNext()) {
				if("House and Land".equals(productlist.getData("ProductType"))) {
					idList.add(productlist.getData("ProductID"));
				} else if("Land".equals(productlist.getData("ProductType"))) {
					idList.addAll(applyHNL(con, user, productlist.getData("ProductID"), c.getCampaignID()));
				} else {
					logger.warn("Apply Campaign drips skipped for product type {}", productlist.getData("ProductType"));
				}
				
				if(tot.intValue() != productlist.getRowIndex()) // don't complete the progress on first phase.
					UtilityResource.getInstance().updateProgressItem(user, progressId, productlist.getRowIndex() / tot.doubleValue(), null, productlist.getRowIndex(), -1);
			}
			
			tot = new Double(idList.size());
			int ix = 0;
			UtilityResource.getInstance().updateProgressItem(user, progressId, 0.01, "Apply Campaign - Phase 2/2", 0, tot.intValue());
			for(String hnlID: idList) {
				logger.debug("Apply Drips to HNL product with ID {}", hnlID);
				removeCampaignDrips(con, user, dripIDs, campaignID, hnlID);
				for(String dripID: dripIDs) {
					chnl.copyDripsFromRootDrip(dripID, hnlID, "Campaign", user.getUserID());
				}	
				RPMtask.getInstance().calculateSinglePackageDrips(con, hnlID);
				UtilityResource.getInstance().updateProgressItem(user, progressId, ++ix / tot.doubleValue(), null, ix, -1);
			}
			/*
			while(productlist.getNext()) {
				logger.debug("in while");
				if("House and Land".equals(productlist.getData("ProductType"))) {
					logger.debug("Apply Drips to HNL product with ID {}", productlist.getData("ProductID"));
					removeCampaignDrips(con, user, dripIDs, campaignID, productlist.getData("ProductID"));
					for(String dripID: dripIDs) {
						chnl.copyDripsFromRootDrip(dripID, productlist.getData("ProductID"), "Campaign", user.getUserID());
					}
					RPMtask.getInstance().calculateSinglePackageDrips(con, productlist.getData("ProductID"));
				} else if("Land".equals(productlist.getData("ProductType"))) {
					for(String hnlID: applyHNL(con, user, productlist.getData("ProductID"), c.getCampaignID())) {
						removeCampaignDrips(con, user, dripIDs, campaignID, hnlID);
						for(String dripID: dripIDs) {
							chnl.copyDripsFromRootDrip(dripID, hnlID, "Campaign", user.getUserID());
						}	
						RPMtask.getInstance().calculateSinglePackageDrips(con, hnlID);
					}
					//TODO copy drips to land also? 
				} else {
					logger.warn("Apply Campaign drips skipped for product type {}", productlist.getData("ProductType"));
				}
				UtilityResource.getInstance().updateProgressItem(user, progressId, productlist.getRowIndex() / tot);
			}
			*/
		} finally {
			productlist.close();
		}
		logger.debug("applyCampaignDrips complete");
	}
	
	public void applyCampaignDrips(HttpServletRequest request, String campaignID, String productID) {		
		applyCampaignDrips(ActionBean.getConnection(request), us.getCurrentUser(request), campaignID, productID);
	}
	
	public void applyCampaignDrips(HttpServletRequest request, HttpServletResponse response, String campaignID, String productID) {		
		//TODO - is this supposed to do something ?? 
		applyCampaignDrips(ActionBean.getConnection(request), us.getCurrentUser(request), campaignID, productID);
	}
	
	public void applyCampaignDrips(Connection con, UserBean user, String campaignID, String productID) {
		logger.debug("applyCampaignDrips(con, user={}, campaignID={})", user != null ? user.getName() : "null user", campaignID);
		Campaign c = getCampaign(con, user, campaignID);
		Set<String> dripIDs = getCampaignDrips(con, c.getCampaignID());
		if(dripIDs.isEmpty()) {
			logger.warn("Did not find any campaign drips to apply for campaign {}", campaignID);
			return;
		}
		final String progressId = "ApplyCampaignDrips-" + campaignID + productID;
		if(logger.isDebugEnabled()) {
			logger.debug("Found drips [{}] to apply to campaign {}", StringUtils.join(dripIDs,","), c.getCampaignID());
		}
		GenRow p = new GenRow();
		p.setConnection(con);
		p.setTableSpec("Products");
		p.setParameter("-select1","ProductID");
		p.setParameter("-select2","ProductType");
		p.setParameter("ProductID", productID);
		if(p.isSet("ProductID")) {
			p.doAction("select");
		}
		if(p.isSuccessful()) {
			Progress prog = utilResource.getProgressItem(user, progressId);
			final CreateHouseAndLand chnl = new CreateHouseAndLand(null, null, InitServlet.getConfig().getServletContext());
			chnl.setConnection(con);
			
			if("House and Land".equals(p.getData("ProductType"))) {
				prog.total = 1;
				prog.count = 0;
				logger.debug("Apply Drips to HNL product with ID {}", p.getData("ProductID"));
				removeCampaignDrips(con, user, dripIDs, campaignID, p.getData("ProductID"));
				for(String dripID: dripIDs) {
					chnl.copyDripsFromRootDrip(dripID, p.getData("ProductID"), "Campaign", user.getUserID());
				}
				RPMtask.getInstance().calculateSinglePackageDrips(con, p.getData("ProductID"));
				prog.count = 1;
				prog.complete = true;
				prog.p = 100;
			} else if("Land".equals(p.getData("ProductType"))) {
				logger.debug("Apply Drips to Land product with ID {}", p.getData("ProductID"));
				List<String> hnlIDs = applyHNL(con, user, p.getData("ProductID"), c.getCampaignID());
				prog.count = 0;
				prog.total = hnlIDs.size();
				for(String hnlID: hnlIDs) {
					logger.debug("applying campaign drips to hnl product [{}]", hnlID);
					removeCampaignDrips(con, user, dripIDs, campaignID, hnlID);
					for(String dripID: dripIDs) {
						chnl.copyDripsFromRootDrip(dripID, hnlID, "Campaign", user.getUserID());
					}	
					logger.debug("calculating single package drips for product [{}]", hnlID);
					RPMtask.getInstance().calculateSinglePackageDrips(con, hnlID);
					prog.count++;
					prog.p = (new Double(prog.count).doubleValue() / new Double(prog.total).doubleValue()) * 100; 
				}
				prog.p = 100;
				prog.complete = true;
				//TODO copy drips to land also? 
			} else {
				logger.warn("Apply Campaign drips skipped for product type {}", p.getData("ProductType"));
			}
		} else {
			logger.warn("Could not find product to apply drips to");
		}
	}
	
	public void updateCampaignDripDates(HttpServletRequest request, String campaignID, Date startDate, Date endDate) {
		updateCampaignDripDates(ActionBean.getConnection(request), us.getCurrentUser(request), campaignID, startDate, endDate);
	}
	
	public void updateCampaignDripDates(Connection con, UserBean user, String campaignID, Date startDate, Date endDate) {
		if(logger.isDebugEnabled()) logger.debug("updateCampaignDripDates(con, user={}, campaignID={}, start={}, end={})", new String[]{user != null ? user.getName() : "null user", campaignID, String.valueOf(startDate), String.valueOf(endDate)});
		Campaign c = getCampaign(con, user, campaignID);
		Set<String> dripIDs = getCampaignDrips(con, c.getCampaignID(), false);
		if(dripIDs.isEmpty()) {
			logger.warn("Did not find any campaign drips to apply for campaign {}", campaignID);
			return;
		}
		GenRow r = new GenRow();
		r.setTableSpec("Products");
		r.setConnection(con);
		r.setParameter("AvailableDate", startDate);
		r.setParameter("ExpiryDate", endDate);
		r.setParameter("ModifiedDate", "NOW");
		r.setParameter("ModifiedBy", user.getUserID());
		r.setAction(GenerationKeys.UPDATE);
		
		for(String s: dripIDs) {
			r.setParameter("ProductID", s);
			r.doAction();
		}
	}
	
	private void applyCampaign(Connection con, UserBean user, String productID, String campaignID) {
		if(StringUtils.isBlank(productID) || StringUtils.isBlank(campaignID)) {
			logger.warn("applyCampaign(con, {}, {}) is not valid", productID, campaignID);
			return;
		}
        GenRow r = new GenRow();
        r.setConnection(con);
        r.setViewSpec("CampaignProductView");
        r.setParameter("CampaignID", campaignID);
        r.setParameter("ProductID", productID);
		r.doAction(GenerationKeys.SELECTFIRST);
		
		if(logger.isTraceEnabled()) logger.trace(r.getStatement());
		
		if(r.isSuccessful() && r.getData("CampaignID").length() > 0) {
			//if(!r.getData("CampaignID").equals(campaignID)) {
			//	removeCampaignDrips(con, user, r.getData("CampaignID"), productID);
			//}
			r.setParameter("Actioned","Y");		// Technically this hasn't been actioned yet, it will be
			r.setParameter("CampaignID", campaignID);
			r.	doAction(GenerationKeys.UPDATE);
		} else {
			r.setToNewID("CampaignProductID");
			r.setParameter("Actioned","Y");
			r.setParameter("CampaignID", campaignID);
			r.doAction(GenerationKeys.INSERT);
		}
	}
	
	private List<String> applyHNL(Connection con, UserBean user, String landProductID, String campaignID) {
		
		GenRow qi = new GenRow();
		qi.setViewSpec("properties/ProductQuickIndexView");
		try { 
			qi.setConnection(con); 
			qi.setParameter("Active","Y");
			qi.setParameter("LotID", landProductID);
			qi.setParameter("QuickIndexCurrentStatus.ProductStatusList.Status","Available");
			
			if(logger.isTraceEnabled()) {
				qi.doAction(GenerationKeys.SEARCH);
				logger.trace(qi.getStatement());
			}
			qi.getResults(true); 
			
			if(qi.getNext()) {
				List<String> hnlIds = new ArrayList<String>(qi.getSize());
				do {
					applyCampaign(con, user, qi.getData("ProductID"), campaignID);
					hnlIds.add(qi.getData("ProductID"));
				} while(qi.getNext());
				return hnlIds;
			}
			return Collections.emptyList();
		} finally {
			qi.close();
		}
	}
	
	public void removeCampaignDrips(HttpServletRequest request, String campaignID) {
		removeCampaignDrips(ActionBean.getConnection(request), us.getCurrentUser(request), campaignID);
	}
	
	//TODO move to drip service ?
	private String findRootCostings(Connection conn, String productID) {
		//logger.debug("findRootCostings(conn, productID={})", productID);
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(conn);
		row.setParameter("-select0", "PackageCostProductID");
		row.setParameter("-select1","ProductID"); 	// I think the idea is that if the SelectFirst fails then it will return an empty string.
		row.setParameter("ProductID", productID);
		row.doAction(GenerationKeys.SELECTFIRST);
		
		if (row.getString("PackageCostProductID").length() > 0) {
			String tmpID = findRootCostings(conn,row.getString("PackageCostProductID"));
			if (StringUtils.isNotBlank(tmpID)) {
				return tmpID;
			}
		}
		
		return row.getData("ProductID");
	}
	
	public void removeCampaignDrips(Connection con, UserBean user, String campaignID) {
		logger.debug("removeCampaignDrips(con, user={}, campaignID={})", user != null ? user.getName() : "null user", campaignID);
		Campaign c = getCampaign(con, user, campaignID);
		Set<String> dripIDs = getCampaignDrips(con, c.getCampaignID());	//add results of list into set as we need to check against these.
		if(dripIDs.isEmpty()) {
			logger.warn("Did not find any campaign drips to apply for campaign {}", campaignID);
			return;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Found drips [{}] to remove from campaign {} products", StringUtils.join(dripIDs,","), c.getCampaignID());
		}
		GenRow check = new GenRow();
		check.setViewSpec("ProductLinkedProductView");
		check.setConnection(con);
		
		GenRow productlist = new GenRow();
		productlist.setViewSpec("CampaignProductView");
		
		GenRow remPP = new GenRow();
		remPP.setConnection(con);
		remPP.setTableSpec("ProductProducts");
		remPP.setAction(GenerationKeys.DELETE);
		
		GenRow remProduct = new GenRow();
		remProduct.setConnection(con);
		remProduct.setTableSpec("Products");
		remProduct.setAction(GenerationKeys.DELETE);
		
		try { 
			productlist.setConnection(con); 
			productlist.setColumn("CampaignID",campaignID);
			user.setConstraint(productlist, "CampaignProductProduct.ProductGroup");
			productlist.getResults(); 
			
			while(productlist.getNext()) {				
				removeCampaignDrips(con, user, dripIDs, productlist.getData("ProductID"), check, remPP, remProduct);
			}
		
		} finally {
			productlist.close();
		}
		logger.debug("removeCampaignDrips complete");
	}
	
	public void removeCampaignDrips(HttpServletRequest request, String campaignID, String fromProductID) {
		removeCampaignDrips(ActionBean.getConnection(request), us.getCurrentUser(request),  campaignID, fromProductID);
	}
	
	public void removeCampaignDrips(Connection con, UserBean user, String campaignID, String fromProductID) {
		Campaign c = getCampaign(con, user, campaignID);
		
		Set<String> dripIDs = getCampaignDrips(con, c.getCampaignID());	//add results of list into set as we need to check against these.
		if(dripIDs.isEmpty()) {
			logger.warn("Did not find any campaign drips to apply for campaign {}", campaignID);
			return;
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Found drips [{}] to remove from product={} in campaign={}", new String[]{StringUtils.join(dripIDs,","), fromProductID, c.getCampaignID()});
		}
		removeCampaignDrips(con, user, dripIDs, campaignID, fromProductID);
	}
	
	private void removeCampaignDrips(Connection con, UserBean user, Set<String> dripIDs, String campaignID, String fromProductID) {
		if(logger.isDebugEnabled()) logger.debug("removeCampaignDrips(con, user={}, campaignID={}, fromProductID={})", new String[]{ user != null ? user.getName() : "null user", campaignID, fromProductID});

		GenRow check = new GenRow();
		check.setViewSpec("ProductLinkedProductView");
		check.setConnection(con);
		
		//GenRow productlist = new GenRow();
		//productlist.setViewSpec("CampaignProductView");
		
		GenRow remPP = new GenRow();
		remPP.setConnection(con);
		remPP.setTableSpec("ProductProducts");
		remPP.setAction(GenerationKeys.DELETE);
		
		GenRow remProduct = new GenRow();
		remProduct.setConnection(con);
		remProduct.setTableSpec("Products");
		remProduct.setAction(GenerationKeys.DELETE);
		
		removeCampaignDrips(con, user, dripIDs, fromProductID, check, remPP, remProduct);
	}
	
	private void removeCampaignDrips(Connection con, UserBean user, Set<String> dripIDs, String fromProductID, GenRow check, GenRow remPP, GenRow remProduct) {
		logger.debug("removeCampaignDrips(con, user, dripIDs, fromProductID={}", fromProductID);
		check.clear();
		check.setParameter("ProductID", fromProductID);
		check.setParameter("Category", "Discount+Rebate+Inclusion+Promotion");
		check.setParameter("ProductProductsLinkedProduct.ProductType", "%Drip%");
		try { 
			if(check.isSet("ProductID")) {
				check.doAction(GenerationKeys.SEARCH);
				check.getResults(); 
			}
			while(check.getNext()) {
				String checkID = findRootCostings(con, check.getString("PackageCostProductID"));
				if(dripIDs.contains(checkID)) {
					//remove pp
					remPP.setParameter("ProductProductID", check.getData("ProductProductID"));
					remPP.doAction();
					
					//remove product 
					remProduct.setParameter("ProductID", check.getData("LinkedProductID"));
					remProduct.doAction();
				}
			}
		} finally { 
			check.close();
		}
		RPMtask.getInstance().calculateSinglePackageDrips(con, fromProductID);
	}
	
	public void addCampaignDrips(Connection con, UserBean user, String toProductID, String campaignID) {
		logger.debug("addCampaignDrips(con, user={}, campaignID={})", user != null ? user.getName() : "null user", campaignID);
		Campaign c = getCampaign(con, user, campaignID);
		Set<String> dripIDs = getCampaignDrips(con, c.getCampaignID());
		if(dripIDs.isEmpty()) {
			logger.warn("Did not find any campaign drips to apply for campaign {}", campaignID);
			return;
		}
		final CreateHouseAndLand chnl = new CreateHouseAndLand(null, null, InitServlet.getConfig().getServletContext());
		
		GenRow check = new GenRow();
		check.setViewSpec("ProductLinkedProductView");
		
		GenRow clone = new GenRow();
		clone.setTableSpec("ProductProducts");
		
		addCampaignDrips(con, user, toProductID, dripIDs, chnl, check, clone);
	}
	
	private void addCampaignDrips(Connection con, UserBean user, String toProductID, Set<String> dripIDs, CreateHouseAndLand chnl, GenRow check, GenRow clone) {
		check.clear();
		Set<String> rootCostingIDs = Collections.emptySet();
		try { 
			check.setConnection(con);
			check.setParameter("ProductID", toProductID);
			check.setParameter("Category", "Discount+Rebate+Inclusion+Promotion");
			//check.setParameter("Product Type", "%Drip%");
			check.setParameter("ProductProductsLinkedProduct.ProductType", "%Drip%");
			check.getResults(true);
			
			rootCostingIDs = new HashSet<String>(check.getSize());
			while (check.getNext()) {
				String foundProductID = findRootCostings(con, check.getString("PackageCostProductID"));
				rootCostingIDs.add(foundProductID);
			}
		} finally { 
			check.close();
		}
		for(String dripID: dripIDs) {
			if(!rootCostingIDs.contains(dripID)) {
				/*
				String newproductid = KeyMaker.generate();
				chnl.copyplan("Products", "ProductID",dripID, newproductid, "Campaign",user.getUserID());
				clone.setParameter("LinkedProductID", newproductid);
				
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedBy", user.getUserID());
				clone.setParameter("ModifiedBy", user.getUserID());
				clone.setParameter("ProductID", toProductID);
				clone.createNewID();
				clone.doAction("insert");
				
				*/
				chnl.copyDripsFromRootDrip(dripID, toProductID, "Campaign", user.getUserID());
			}
		}
	}
	
	private Set<String> getCampaignDrips(Connection con, String campaignID) {
		return getCampaignDrips(con, campaignID, true);
	}
	
	private Set<String> getCampaignDrips(Connection con, String campaignID, boolean rootDripsOnly) {
		GenRow dripList = new GenRow();
		try {
			dripList.setConnection(con);
			dripList.setSearchStatement(new StringBuilder().append("SELECT ParentProductID as ProductID from PackageCostings WHERE ApplyToID = '").append(campaignID).append("' Group By ProductID;").toString());
			dripList.getResults(true);
			
			if(dripList.getNext()) {
				Set<String> dripIDs = new HashSet<String>(dripList.getSize());
				do {
					if(rootDripsOnly) { 
						String root = findRootCostings(con, dripList.getData("ProductID"));
						if(StringUtils.isNotBlank(root))	//this is returning "" sometimes.
							dripIDs.add(root);
					} else {
						dripIDs.add(dripList.getData("ProductID"));
					}
				} while(dripList.getNext());
				return dripIDs;
			} else {
				logger.debug("getCampaignDrips found no drips to apply, returning");
				return Collections.emptySet();
			}
		} finally {
			dripList.close();
		}
	}
}
