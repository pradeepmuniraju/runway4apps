package com.sok.service.crm.cms.properties;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.beans.cms.properties.EmailTemplate;
import com.sok.service.crm.UserService;
import com.sok.service.exception.NotAuthenticatedException;

public class EmailTemplateService {

	private static final EmailTemplateService service = new EmailTemplateService();
	private static final Logger logger = LoggerFactory.getLogger(EmailTemplateService.class);
	private static final UserService us = UserService.getInstance();

	public static EmailTemplateService getInstance() {
		return service;
	}

	public GenRow getTemplateGenRow() {
		final GenRow row = new GenRow();
		row.setViewSpec("TemplateView");
		return row;
	}

	public List<EmailTemplate> geEmailTemplates(final HttpServletRequest request, final String groupID, final String masterTemplateID, final String active) {
		logger.debug("geEmailTemplates(groupID {}, masterTemplateID {})", groupID, masterTemplateID);
		return geEmailTemplates(ActionBean.getConnection(request), us.getCurrentUser(request), groupID, masterTemplateID, active);
	}

	public List<EmailTemplate> geEmailTemplates(Connection conn, UserBean user, final String groupID, final String masterTemplateID, final String active) {
		logger.debug("geEmailTemplates(Con, {}, {})", user != null ? user.getName() : "null user", active);
		logger.debug("geEmailTemplates({}, {})", groupID, masterTemplateID);
		if (user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		/*
		 * if (!user.canAccess("Products")) { throw new AccessException("You cannot access the products module"); }
		 */
		GenRow row = getTemplateGenRow();
		try {
			row.setConnection(conn);
			row.setParameter("Active", active);
			if (StringUtils.isNotBlank(groupID)) {
				row.setParameter("GroupID", groupID); // needs to be revisited to check if the group id is fine
			}
			if (StringUtils.isNotBlank(masterTemplateID)) {
				row.setParameter("MasterTemplateID", masterTemplateID);
			}
			row.doAction(GenerationKeys.SEARCH);
			logger.trace(row.getSearchStatement());
			row.getResults(true);

			if (row.getNext()) {
				List<EmailTemplate> list = new ArrayList<EmailTemplate>(row.size());
				do {
					EmailTemplate emailTemplate = new EmailTemplate(row);
					emailTemplate.setTemplateBody(getTemplateBody(conn, emailTemplate.getTemplateID()));
					list.add(emailTemplate);
				} while (row.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			row.close();
		}
	}

	public EmailTemplate geEmailTemplate(final HttpServletRequest request, final String templateID, final String contactID, final String groupID, final String authorUserID) {
		logger.debug("geEmailTemplates(templateID {})", templateID);
		return geEmailTemplate(ActionBean.getConnection(request), us.getCurrentUser(request), templateID, contactID, groupID, authorUserID);
	}

	public EmailTemplate geEmailTemplate(Connection conn, UserBean user, final String templateID, final String contactID, final String groupID, final String authorUserID) {
		logger.debug("geEmailTemplates(Con, {}, {})", user != null ? user.getName() : "null user", templateID);
		if (user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		/*
		 * if (!user.canAccess("Products")) { throw new AccessException("You cannot access the products module"); }
		 */
		GenRow row = getTemplateGenRow();
		try {
			row.setConnection(conn);
			row.setParameter("TemplateID", templateID);
			row.doAction(GenerationKeys.SELECTFIRST);
			logger.trace(row.getStatement());
			EmailTemplate emailTemplate = new EmailTemplate(row);
			emailTemplate.setTemplateBody(getTemplateBody(conn, emailTemplate.getTemplateID()));
			return emailTemplate;
		} finally {
			row.close();
		}
	}

	/**
	 * @param con
	 * @param templateID
	 * @return
	 */
	private String getTemplateBody(Connection con, String templateID) {
		// Select Body from Pages where PageID in (Select PageID from PageLinks where ParentID in
		// ( Select PageID from PageLinks where CMSID in (Select CMSID from CMS where TemplateID = '021R008V0Y1W7E3C832D3G0Q243X') and ParentID is null));

		GenRow row = new GenRow();
		row.setConnection(con);
		row.setViewSpec("PageView");
		row.setParameter("PageLinkCMS.TemplateID", templateID);
		row.setParameter("ParentID", "!NULL");
		row.doAction(GenerationKeys.SELECTFIRST); // do select first as at the moment only one body is supported
		logger.trace(row.getStatement());
		return row.getString("Body");

	}
}
