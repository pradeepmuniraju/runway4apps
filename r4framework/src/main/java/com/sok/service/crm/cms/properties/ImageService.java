package com.sok.service.crm.cms.properties;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.sql.Connection;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.cms.properties.Album;
import com.sok.runway.externalInterface.beans.cms.properties.Image;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

public class ImageService {

	private static final ImageService service = new ImageService();
	private static final Logger logger = LoggerFactory.getLogger(ImageService.class);
	private static final UserService us = UserService.getInstance();
	public static ImageService getInstance() {
		return service;
	}
	
	public List<Image> getImages(final HttpServletRequest request, String productID, String galleryID) {
		logger.trace("getImages(Request, {})", productID);
		return getImages(ActionBean.getConnection(request), us.getCurrentUser(request), productID, galleryID, "");
	}

	public List<Image> getImages(final HttpServletRequest request, String productID, String galleryID, String subType) {
		logger.trace("getImages(Request, {})", productID);
		return getImages(ActionBean.getConnection(request), us.getCurrentUser(request), productID, galleryID, subType);
	}

	public List<Image> getImages(final Connection con, final UserBean user, String productID, String galleryID) {
		return getImages(con, user, productID, galleryID, "");
	}

	public List<Image> getImages(final Connection con, final UserBean user, String productID, String galleryID, String subType) {
		logger.trace("getImages(Conn,{}, {})", user != null ? user.getName() : "null user",  productID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(productID == null || productID.length() == 0) {
			throw new NotFoundException("The requested product could not be found");
		}
		GenRow row = new GenRow();
		try {
			List<Image> images = new ArrayList<Image>();
			
			row.setViewSpec("LinkedDocumentView");
			row.setParameter("ProductID", productID);
			row.setParameter("GalleryID", galleryID);
			if (StringUtils.isNotBlank(subType)) row.setParameter("LinkedDocumentDocument.DocumentSubType", subType);
			row.sortBy("SortOrder", 0);
			
			row.doAction("search");
			row.getResults();
			
			while (row.getNext()) {
				images.add(BeanFactory.externalImage(row));
			}
			
			return images;
		} finally {
			if(row != null) row.close();
		}
	}
	
	public List<Album> getAlbums(final HttpServletRequest request, String productID) {
		logger.trace("getImages(Request, {})", productID);
		return getAlbums(ActionBean.getConnection(request), us.getCurrentUser(request), productID, null, null, "");
	}

	public List<Album> getAlbums(final HttpServletRequest request, String productID, String albumType, String publish) {
		logger.trace("getImages(Request, {} {} {})", new String[] {productID, albumType, publish});
		return getAlbums(ActionBean.getConnection(request), us.getCurrentUser(request), productID, albumType, publish, "");
	}

	public List<Album> getAlbums(final HttpServletRequest request, String productID, String albumType, String publish, String subType) {
		logger.trace("getImages(Request, {} {} {})", new String[] {productID, albumType, publish});
		return getAlbums(ActionBean.getConnection(request), us.getCurrentUser(request), productID, albumType, publish, subType);
	}

	public List<Album> getAlbums(final Connection con, final UserBean user, String productID, String albumType, String publish, String subType) {
		logger.trace("getImages(Conn,{}, {})", user != null ? user.getName() : "null user",  productID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(productID == null || productID.length() == 0) {
			throw new NotFoundException("The requested product could not be found");
		}
		
		GenRow row = new GenRow();

		try {
			List<Album> albums = new ArrayList<Album>();
			
			if (!StringUtils.isNotBlank(albumType) && !StringUtils.isNotBlank(publish)) {
				Album album = new Album();
				album.setImages(getImages(con, user, productID, "NULL+EMPTY", subType));
				
				if (album.getImages().size() > 0) albums.add(album);
				
				publish = "Website";
			}
			
			row.setViewSpec("GalleryView");
			row.setParameter("ProductID", productID);
			
			if (StringUtils.isNotBlank(albumType)) row.setParameter("AlbumType", albumType);
			if (StringUtils.isNotBlank(publish)) row.setParameter("Publisher", "%" + publish + "%");
			
			row.setParameter("-groupby0","GalleryID");
			
			row.sortBy("GalleryName", 0);
			
			row.doAction("search");
			
			row.getResults();
			
			while (row.getNext()) {
				Album a = BeanFactory.externalAlbum(row);
				a.setImages(getImages(con, user, "NULL+" + productID, row.getString("GalleryID"), subType));
				albums.add(a);
			}

			return albums;
		} finally {
			if(row != null) row.close();
		}
	}

}
