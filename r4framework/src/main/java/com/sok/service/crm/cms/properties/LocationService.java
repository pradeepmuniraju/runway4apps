package com.sok.service.crm.cms.properties;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.runway.externalInterface.beans.cms.properties.Home;
import com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage;
import com.sok.runway.externalInterface.beans.cms.properties.Location;
import com.sok.runway.externalInterface.beans.cms.properties.Lot;
import com.sok.runway.externalInterface.beans.cms.properties.Staff;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.LegacyLocation;
import com.sok.service.crm.UserService;

public class LocationService {
	
	private static final Logger logger = LoggerFactory.getLogger(LocationService.class);
	private static final UserService userService = UserService.getInstance(); 
	private static final LocationService service = new LocationService();
	private static final String VIEWSPEC = "DisplayEntityView";
	
	private LocationService() { }
	public static LocationService getInstance() {
		return service;
	}
	
	public Location getRootLocation(HttpServletRequest request) {
		return getRootLocation(request, false);
	}
	
	public Location getRootLocation(HttpServletRequest request, boolean populate) {
		GenRow row = new GenRow();
		row.setViewSpec(VIEWSPEC);
		if (request != null) row.setRequest(request);
		row.setParameter("ParentDisplayEntityID", "NULL");
		row.doAction("selectfirst");
		
		if (row.isSuccessful() || row.isSet("DisplayEntityID")) {
			Location location = populateLocation(row);
			if (populate) location.setLocations(getLocations(request, row.getString("DisplayEntityID"), populate));
			
			return location;
		}
		
		return null;
	}
	
	public Location getLocation(HttpServletRequest request, String locationID) {
		return getLocation(request, locationID, false);
	}
	
	public Location getLocation(HttpServletRequest request, String locationID, boolean populate) {
		GenRow row = new GenRow();
		row.setViewSpec(VIEWSPEC);
		if (request != null) row.setRequest(request);
		row.setParameter("DisplayEntityID", locationID);
		row.doAction("selectfirst");
		
		if (row.isSuccessful() || row.isSet("DisplayEntityID")) {
			Location location = populateLocation(row);
			if (populate) location.setLocations(getLocations(request, row.getString("DisplayEntityID"), populate));
			
			return location;
		}
		
		return null;
	}
	
	public List<Location> getLocations(HttpServletRequest request, String locationID) {
		return getLocations(request, locationID, false);
	}

	public List<Location> getLocations(HttpServletRequest request, String locationID, boolean populate) {
		if (locationID == null || locationID.length() == 0) return null;
		
		GenRow row = new GenRow();
		row.setViewSpec(VIEWSPEC);
		if (request != null) row.setRequest(request);
		row.setParameter("ParentDisplayEntityID", locationID);
		row.doAction("search");
		
		row.getResults();

		List<Location> locations = new ArrayList<Location>();

		while (row.getNext()) {
			Location location = populateLocation(row);
			if (populate) location.setLocations(getLocations(request, row.getString("DisplayEntityID"), populate));
			locations.add(location);
		}
		
		row.close();
		
		return locations;
	}
	
	public Location populateLocation(GenRow data) {
		Location location = new Location();
		
		location.setActive(data.getString("Active"));
		location.setCreatedBy(data.getString("CreatedBy"));
		location.setWebsite(data.getString("Web"));
		
		try {
			location.setCreatedDate(data.getDate("CreatedDate"));
		} catch (Exception e) {}
		location.setDescription(data.getString("Description"));
		location.setLocationID(data.getString("DisplayEntityID"));
		location.setModifiedBy(data.getString("ModifiedBy"));
		try {
			location.setModifiedDate(data.getDate("ModifiedDate"));
		} catch (Exception e) {}

		location.setName(data.getString("Name"));
		location.setProductNum(data.getString("ProductNum"));
		location.setType(data.getString("Type"));
		
		location.setProductID(data.getString("ProductID"));
		location.setCompanyID(data.getString("ProductID"));
		
		if (data.isSet("AddressID")) {
			Address address = new Address();
			address.setAddressID(data.getString("AddressID"));
			address.setStreetNumber(data.getString("StreetNumber"));
			address.setStreet(data.getString("Street"));
			address.setStreet2(data.getString("Street2"));
			address.setCity(data.getString("City"));
			address.setState(data.getString("State"));
			address.setPostcode(data.getString("Postcode"));
			address.setCountry(data.getString("Country"));
			address.setLatitude(data.getString("Latitude"));
			address.setLongitude(data.getString("Longitude"));
			
			location.setAddress(address);
		}
		
		return location;
	}
	public List<Staff> getStaff(HttpServletRequest request, String locationID,
			boolean populate) {
		
		Location location = getLocation(request, locationID, populate);
		
		List<Staff> staff = new ArrayList<Staff>();
		
		if (location != null) { 
			processStaff(request, staff, location, populate);
		}
		return staff;
	}
	
	private void processStaff(HttpServletRequest request, List<Staff> staff, Location location,
			boolean populate) {
		
		GenRow companyLinkedContacts = new GenRow();
		companyLinkedContacts.setViewSpec("CompanyContactLinkView");
		if (request != null) companyLinkedContacts.setRequest(request);
		if(location.getCompanyID() != null && location.getCompanyID().length() > 0){
			companyLinkedContacts.setParameter("CompanyID",location.getCompanyID());
			companyLinkedContacts.doAction("search");
			companyLinkedContacts.getResults();
			
			while (companyLinkedContacts.getNext()) {
				Staff staffRec = new Staff();
				staffRec.setContactID(companyLinkedContacts.getString("ContactID"));
				staffRec.setFirstName(companyLinkedContacts.getString("FirstName"));
				staffRec.setLastName(companyLinkedContacts.getString("LastName"));
				staffRec.setName((companyLinkedContacts.getString("FirstName") + " " + companyLinkedContacts.getString("LastName")).trim());
				staffRec.setLocationID(location.getLocationID());
				staffRec.setLocationName(location.getName());
				staffRec.setPhone(companyLinkedContacts.getString("Phone"));
				staffRec.setMobile(companyLinkedContacts.getString("Mobile"));
				staffRec.setEmail(companyLinkedContacts.getString("Email"));
				
				staff.add(staffRec);
			}
			
			if (populate) {
				for (Location loc : location.getLocations()) {
					processStaff(request, staff, loc, populate);
				}
			}
			
			companyLinkedContacts.close();
			
		}
	}

	public List<Home> getHomes(HttpServletRequest request, String locationID,
			boolean populate) {
		
		Location location = getLocation(request, locationID, populate);
		
		List<Home> homes = new ArrayList<Home>();
		
		if (location != null) { 
			processHomes(request, homes, location, populate);
		}
		return homes;
	}
	
	private void processHomes(HttpServletRequest request, List<Home> homes, Location location,
			boolean populate) {
		
		GenRow search = new GenRow();
		search.setParameter("LocationID|1", location.getLocationID());
		search.setParameter("RegionID|1", location.getLocationID());
		search.setParameter("-groupby1", "ProductID");
		
		if(request.getHeader("Referer") != null && request.getHeader("Referer").indexOf("api-docs") != -1) {
			if (populate)
				search.setParameter("-top", 5);
			else
				search.setParameter("-top", 20);
		}
		
		List<Home> homesLoc = HomeService.getInstance().getHomesExternal(request, search);
		
		homes.addAll(homesLoc);
		
		if (populate) {
			for (Location loc : location.getLocations()) {
				processHomes(request, homes, loc, populate);
			}
		}
	}

	public List<Lot> getLand(HttpServletRequest request, String locationID,
			boolean populate) {
		
		Location location = getLocation(request, locationID, populate);
		
		List<Lot> land = new ArrayList<Lot>();
		
		if (location != null) { 
			processLand(request, land, location, populate);
		}
		return land;
	}
	
	private void processLand(HttpServletRequest request, List<Lot> land, Location location,
			boolean populate) {
		
		GenRow search = new GenRow();
		search.setParameter("LocationID|1", location.getLocationID());
		search.setParameter("RegionID|1", location.getLocationID());
		search.setParameter("-groupby1", "ProductID");
		
		if(request.getHeader("Referer") != null && request.getHeader("Referer").indexOf("api-docs") != -1) {
			if (populate)
				search.setParameter("-top", 5);
			else
				search.setParameter("-top", 20);
		}
		
		List<Lot>landLoc = EstateService.getInstance().getLotsExternal(request, search);
		
		land.addAll(landLoc);
		
		if (populate) {
			for (Location loc : location.getLocations()) {
				processLand(request, land, loc, populate);
			}
		}
	}

	public List<HouseLandPackage> getHouseAndLand(HttpServletRequest request, String locationID,
			boolean populate) {
		
		Location location = getLocation(request, locationID, populate);
		
		List<HouseLandPackage> houseAndLand = new ArrayList<HouseLandPackage>();
		
		if (location != null) { 
			processHouseAndLand(request, houseAndLand, location, populate);
		}
		return houseAndLand;
	}
	
	private void processHouseAndLand(HttpServletRequest request, List<HouseLandPackage> houseAndLand, Location location,
			boolean populate) {
		
		GenRow search = new GenRow();
		search.setParameter("LocationID|1", location.getLocationID());
		search.setParameter("RegionID|1", location.getLocationID());
		search.setParameter("-groupby1", "ProductID");
		
		if(request.getHeader("Referer") != null && request.getHeader("Referer").indexOf("api-docs") != -1) {
			if (populate)
				search.setParameter("-top", 5);
			else
				search.setParameter("-top", 20);
		}
		
		List<HouseLandPackage> houseAndLandLoc = PackageService.getInstance().getPackagesExternal(request, search);
		
		houseAndLand.addAll(houseAndLandLoc);
		
		if (populate) {
			for (Location loc : location.getLocations()) {
				processHouseAndLand(request, houseAndLand, loc, populate);
			}
		}
	}

	public List<HouseLandPackage> getHouseAndLandDisplay(HttpServletRequest request, String locationID,
			boolean populate) {
		
		Location location = getLocation(request, locationID, populate);
		
		List<HouseLandPackage> houseAndLand = new ArrayList<HouseLandPackage>();
		
		if (location != null) { 
			processHouseAndLandDisplay(request, houseAndLand, location, populate);
		}
		return houseAndLand;
	}
	
	private void processHouseAndLandDisplay(HttpServletRequest request, List<HouseLandPackage> houseAndLand, Location location,
			boolean populate) {
		
		GenRow search = new GenRow();
		search.setParameter("LocationID|1", location.getLocationID());
		search.setParameter("RegionID|1", location.getLocationID());
		search.setParameter("DisplayFlag", "Y");
		search.setParameter("-groupby1", "ProductID");
		
		if(request.getHeader("Referer") != null && request.getHeader("Referer").indexOf("api-docs") != -1) {
			if (populate)
				search.setParameter("-top", 5);
			else
				search.setParameter("-top", 20);
		}
		
		List<HouseLandPackage> houseAndLandLoc = PackageService.getInstance().getPackagesExternal(request, search);
		
		houseAndLand.addAll(houseAndLandLoc);
		
		if (populate) {
			for (Location loc : location.getLocations()) {
				processHouseAndLandDisplay(request, houseAndLand, loc, populate);
			}
		}
	}
	public LegacyLocation getLegacyLocation(HttpServletRequest request, String locationID) {
		return getLegacyLocation(request, locationID, false);
	}
	
	public LegacyLocation getLegacyLocation(HttpServletRequest request, String locationID, boolean populate) {
		GenRow row = new GenRow();
		row.setViewSpec(VIEWSPEC);
		if (request != null) row.setRequest(request);
		row.setParameter("DisplayEntityID", locationID);
		row.doAction("selectfirst");
		
		if (row.isSuccessful() || row.isSet("DisplayEntityID")) {
			LegacyLocation location = populateLegacyLocation(row);
			//if (populate) location.setLocations(getLocations(request, row.getString("DisplayEntityID"), populate));
			
			return location;
		}
		
		return null;
	}
	public LegacyLocation populateLegacyLocation(GenRow data) {
		LegacyLocation location = new LegacyLocation();
		
		/*
		location.setActive(data.getString("Active"));
		location.setCreatedBy(data.getString("CreatedBy"));
		try {
			location.setCreatedDate(data.getDate("CreatedDate"));
		} catch (Exception e) {}
		location.setDescription(data.getString("Description"));
		location.setLocationID(data.getString("DisplayEntityID"));
		location.setModifiedBy(data.getString("ModifiedBy"));
		try {
			location.setModifiedDate(data.getDate("ModifiedDate"));
		} catch (Exception e) {}
		*/
		location.setName(data.getString("Name"));
		location.setLocationID(data.getData("DisplayEntityID"));
		//location.setProductNum(data.getString("ProductNum"));
		location.setType(data.getString("Type"));
		
		//location.setProductID(data.getString("ProductID"));
		//location.setCompanyID(data.getString("ProductID"));
		
		return location;
	}
	
}
