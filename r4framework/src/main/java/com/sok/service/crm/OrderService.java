package com.sok.service.crm;

import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.ErrorMap;
import com.sok.runway.UserBean;
import com.sok.runway.crm.Contact;
import com.sok.runway.crm.Opportunity;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.crm.cms.properties.entities.PlanEntity;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.Order;
import com.sok.runway.externalInterface.beans.QuoteView;
import com.sok.runway.externalInterface.beans.QuoteViewPage;
import com.sok.runway.externalInterface.beans.cms.properties.Lot;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.OrderContact;
import com.sok.runway.externalInterface.beans.shared.OrderItem;
import com.sok.runway.externalInterface.beans.shared.PackageCost;
import com.sok.runway.externalInterface.resources.OrderResource;
import com.sok.runway.externalInterface.resources.PropertyResource;
import com.sok.runway.externalInterface.resources.UtilityResource.ContactLocation;
import com.sok.runway.security.Group;
import com.sok.runway.view.ValueList;
import com.sok.service.crm.cms.properties.DripService;
import com.sok.service.crm.cms.properties.DripService.DripCollection;
import com.sok.service.crm.cms.properties.DripService.DripContent;
import com.sok.service.crm.cms.properties.DripService.DripProduct;
import com.sok.service.crm.cms.properties.EstateService;
import com.sok.service.crm.cms.properties.PlanService;
import com.sok.service.crm.cms.properties.PlanService.HLPackage;
import com.sok.service.crm.cms.properties.PlanService.HLPackageCost;
import com.sok.service.crm.cms.properties.PlanService.Plan;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.text.ParseException;
import java.util.*;

public class OrderService {

	private static final OrderService service = new OrderService();
	private static final DripService dripService = DripService.getInstance();
	private static final PlanService planService = PlanService.getInstance();
	/* server logs */
	final static boolean sameGroups = "true".equals(InitServlet.getSystemParam("ContactAllowSameGroupSelection"));

	private static final Logger logger = LoggerFactory.getLogger(OrderService.class);
	/* logs user actions for auditing */
	private static final ActivityLogger activityLogger = ActivityLogger.getActivityLogger();
	private static final ContactService cs = ContactService.getInstance();
	private static final UserService us = UserService.getInstance(); 
	public static OrderService getInstance() {
		return service;
	}
	
	/**
	 * return true if the promo lock date is specified and is after the date specified. 
	 * @param d
	 * @return
	 */
	public boolean isPromotionLocked(Date dateOfSale) {
		if(dateOfSale == null) {
			return true;
		}
		String priceUpdate = StringUtils.trimToNull(InitServlet.getSystemParam("LastPriceUpdateDate"));
		if(priceUpdate == null) {
			logger.debug("we do not have a last price update date, so are returning false");
			//ie, the sysparam is not in use.
			return false;
		}
		try { 
			logger.debug("Parsing {} for last price update", priceUpdate);
			Date priceUpdateDate = new java.text.SimpleDateFormat("yyyy-MM-dd").parse(priceUpdate);
			logger.debug("Using {} for price update", priceUpdateDate.toString());
			return priceUpdateDate.after(dateOfSale);
		} catch (ParseException pe) {
			logger.error("Error parsing date from sysparam", pe);
			return false;
		}
	}
	
	public List<Order> getOrders(final HttpServletRequest request) throws NotAuthenticatedException, AccessException {
		logger.trace("getOrders(Request)");
		return getOrders(ActionBean.getConnection(request), us.getCurrentUser(request));
	}
	public List<Order> getOrders(final Connection conn, final UserBean user) throws NotAuthenticatedException, AccessException {
		logger.trace("getOrders(Connection, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		final GenRow orders = new GenRow();
		try { 
			orders.setViewSpec("OrderView");
			orders.setConnection(conn);
			user.setConstraint(orders,"OrderGroup");
			orders.setParameter("-sort1", "OrderNum");
			orders.setParameter("-order1","DESC");
			orders.getResults(true);
			
			if(orders.getNext()) { 
				List<Order> list = new ArrayList<Order>(orders.getSize());
				do {
					list.add(BeanFactory.getOrderFromTableData(orders));
				} while(orders.getNext());
				return list;
			} 
			return Collections.emptyList();
		} finally {
			orders.close();
		}
	}
	
	GenRow getOrderRow(Connection conn, String orderID) {
		final GenRow template = new GenRow();
		template.setConnection(conn);
		template.setViewSpec("OrderView");
		template.setParameter("OrderID", orderID);
		
		if(!template.isSet("OrderID")) {
			throw new IllegalArgumentException("No OrderID given");
		}
		template.doAction(GenerationKeys.SELECT);

		if(!template.isSuccessful()) {
			throw new NotFoundException("The order was not found : " + orderID);
		}
		return template;
	}
	
	/**
	 * @deprecated - pass quote view id to ensure meta data is generated for content
	 */
	public void addPackageIncludedDripItems(final HttpServletRequest request, String orderID, String packageID) {
		addPackageIncludedDripItems(request, orderID, packageID, null);
	}
	
	public void addPackageIncludedDripItems(final HttpServletRequest request, String orderID, String packageID, String quoteViewID) {
		logger.debug("addPackageIncludedDripItems(request, {}, {})", orderID, packageID);
		DripCollection dc = PropertyResource.getInstance().getPackageDrips(request, packageID, "Quotes",  quoteViewID);
		addDripItems(request, orderID, dc);
	}
	
	/**
	 * @deprecated - pass quote view id to ensure meta data is generated for content
	 */
	public void addPlanIncludedDripItems(final HttpServletRequest request, String orderID, String planID) {
		addPlanIncludedDripItems(request, orderID, planID, null);
	}
	
	public void addPlanIncludedDripItems(final HttpServletRequest request, String orderID, String planID, String quoteViewID) {
		logger.debug("addPlanIncludedDripItems(request, {}, {})", orderID, planID);
		DripCollection dc = PropertyResource.getInstance().getPlanDrips(request, planID, "Quotes",  quoteViewID);
		addDripItems(request, orderID, dc);
	} 
	
	// category cache, internal use only 
	private class CatCache {
		String cat; 
		boolean summary, total; 
		int sort; 
		private CatCache(String cat, boolean summary, boolean total, int sort) {
			this.cat = cat; 
			this.summary = summary; 
			this.total = total; 
			this.sort = sort; 
		}
	}
	
	public void addPackageCosts(final HttpServletRequest request, final String orderID, final String quoteViewID, String packageID) {
		logger.debug("addPackageCosts(request, orderID={}, quoteViewID={}, packageID={})", new String[]{orderID, quoteViewID, packageID});
		final ValueList packageCostTypes = ValueList.getValueList("PackageCostCategory"); 
		Map<String, CatCache> packageCostTypeMapping = new HashMap<String, CatCache>();
		
		List<OrderItem> orderItems = getOrderItems(request, orderID);
		
		GenRow packageCost = new GenRow();
		packageCost.setTableSpec("OrderItems");
		packageCost.setParameter("OrderID", orderID);
		packageCost.setRequest(request); 
		
		packageCost.setParameter("ItemCost", 0);
		packageCost.setParameter("Cost", 0);
		packageCost.setParameter("GST", 0);
		packageCost.setParameter("TotalCost", 0);
		packageCost.setParameter("Quantity", 1);
		
		packageCost.setParameter("ItemLocked","Y");
		packageCost.setParameter("ItemGroupLocked","Y");
		packageCost.setParameter("QuantityLocked","Y");
		packageCost.setParameter("CostLocked","Y");
		packageCost.setParameter("IncludePrice","Y");
		
		int ix = 0; 
		
		List<QuoteViewPage> pages = QuoteViewService.getInstance().getQuoteViewPages(request, quoteViewID);
		final int pcMax = packageCostTypes != null ? (packageCostTypes.size() - 1)  : -1;
		logger.debug("have Package Cost Types: {}", pcMax + 1);		
		if(pcMax != -1) {
			PlanService.HLPackage hlp = PlanService.getInstance().getPackage(request, packageID); 
			if(hlp != null && hlp.PackageCosts != null) { 
				List<PlanService.HLPackageCost> pcl = hlp.PackageCosts;
				Collections.sort(pcl, new Comparator<PlanService.HLPackageCost>() {
					@Override
					public int compare(HLPackageCost arg0, HLPackageCost arg1) {
						int ix0 = -1; 
						int ix1 = -1; 
						for(int i=0; ; i++) {
							if(StringUtils.equals(packageCostTypes.getValue(i), arg0.Category)) {
								ix0 = i;
							}
							if(StringUtils.equals(packageCostTypes.getValue(i), arg1.Category)) {
								ix1 = i; 
							}
							if(i==pcMax) break;
						}
						return new Integer(ix0).compareTo(new Integer(ix1));
					}
				});
				for(PlanService.HLPackageCost pc: hlp.PackageCosts) {
					if(!pc.Active) continue;
					boolean contains = false;
					// make sure we don't double up.
					for(OrderItem oi: orderItems) {
						if(pc.ProductID.equals(oi.getProductID())) {
							contains = true;
							break;
						}
					}
					if(contains) continue;
					ix++;
					CatCache cc = packageCostTypeMapping.get(pc.Category); 
					if(cc == null) {
						for(QuoteViewPage qvp: pages) {
							if(!qvp.PageTypeName.equals("Custom")) {
								continue;
							}					
							if(qvp.SelectedOptions != null && qvp.SelectedOptions.PageOptions != null && qvp.SelectedOptions.PageOptions.containsKey(pc.Category) && qvp.SelectedOptions.PageOptions.get(pc.Category) == Boolean.TRUE) {
								String cat = "Custom-" + qvp.QuoteViewPageID; 
								OrderItem exItem = null;
								int maxSort = -1; 
								for(OrderItem oi: orderItems) {
									if(cat.equals(oi.getCategory())) {
										if(oi.getItemGroupSort() > maxSort) maxSort = oi.getItemGroupSort(); 
										if(pc.Category.equals(oi.getItemGroup())) {
											exItem = oi; 
											break; 
										}
									}
								}
								if(maxSort == -1) maxSort = 999; 
								cc = new CatCache(cat, qvp.SelectedOptions.PageOptions.get(pc.Category + "Summary") == Boolean.TRUE, qvp.SelectedOptions.PageOptions.get(pc.Category + "GroupTotal") == Boolean.TRUE, exItem != null ? exItem.getItemGroupSort() : maxSort + 1);
								packageCostTypeMapping.put(pc.Category, cc);
								break;
							}
						}
					} 
					if(cc != null) { 
						packageCost.setToNewID("OrderItemID");
						packageCost.setParameter("ProductID", pc.ProductID);
						packageCost.setParameter("Name", pc.Name);
						packageCost.setParameter("Description", StringUtils.isBlank(pc.ProductDetails) ? pc.Name : pc.ProductDetails); 
						packageCost.setParameter("Category", cc.cat); 
						packageCost.setParameter("ItemGroup", pc.Category); 
						packageCost.setParameter("ItemGroupSort", cc.sort);
						packageCost.setParameter("IncludeSummary", cc.summary ? "Y" : "N");
						packageCost.setParameter("ItemGroupTotal", cc.total ? "Y" : "N");
						packageCost.setParameter("SortNumber",ix);
						packageCost.doAction(GenerationKeys.INSERT);
						
						if(logger.isTraceEnabled()) {
							logger.trace(packageCost.toString());
						}
					}
				}
			}
		}
	}
	
	public void addDripItems(final HttpServletRequest request, String orderID, DripCollection dc) {
		logger.debug("addDripItems(request, orderID={}, dc-all={})", orderID, dc.allDrips.size());
		GenRow item = new GenRow();
		item.setRequest(request);
		item.setTableSpec("OrderItems");
		item.setParameter("Category","DRIP");
		item.setParameter("IncludePrice","Y");
		item.setParameter("IncludeSummary","Y");
		
		
		GenRow promoItem = new GenRow();
		promoItem.setRequest(request); 
		promoItem.setTableSpec("OrderItems");
		promoItem.setParameter("Category","DRIP");
		promoItem.setParameter("IncludePrice","N");
		promoItem.setParameter("IncludeSummary","N");
		promoItem.setParameter("ItemCost", 0);
		promoItem.setParameter("Cost", 0);
		promoItem.setParameter("GST", 0);
		promoItem.setParameter("TotalCost",0);
		promoItem.setParameter("Quantity", 1);
		promoItem.setParameter("MetaData","{}");
		promoItem.setParameter("OrderID", orderID);
		
		for(DripProduct dp : dc.allDrips) {
			if("Included".equals(dp.Type) && "Y".equals(dp.Active) && !"Not Shown".equals(dp.PricingType)) {
				item.setToNewID("OrderItemID");
				item.setParameter("OrderID", orderID);
				item.setParameter("ProductID", dp.ProductID);
				item.setParameter("Name", dp.Name);
				item.setParameter("ItemGroup", dp.Category);
				item.setParameter("ItemCost", dp.Cost);
				item.setParameter("Cost", dp.Cost);
				item.setParameter("GST", dp.GST);
				item.setParameter("PricingType", dp.PricingType);
				
				if("Post Net".equals(dp.PricingType)) { 
					item.setParameter("TotalCost", "0");
					item.setParameter("Quantity","0");
				} else { 
					item.setParameter("TotalCost", dp.TotalCost);
					item.setParameter("Quantity","1");
				} 
				
				/* need to add content here to item.MetaData */
				if(dp.DripInfo != null && dp.DripInfo.ContentList != null && !dp.DripInfo.ContentList.isEmpty()) {
					JSONObject meta = new JSONObject();
					List<String> items = new ArrayList<String>();
					meta.put("Items", items);
					for(DripContent dripContent: dp.DripInfo.ContentList) {
						if(dripContent.ContentType == DripService.DripContentType.Text) {
							if("Value".equals(dripContent.Placement)) {
								items.add(dripContent.Description);
								if(dripContent.Selectable != true) { // ie should be added automatically.
									promoItem.setToNewID("OrderItemID");
									promoItem.setParameter("ProductID", dp.ProductID);
									promoItem.setParameter("Name", dripContent.Description.length() < 100 ? dripContent.Description : dripContent.Description.substring(0,99));
									promoItem.setParameter("Description", dripContent.Description);
									promoItem.setParameter("ItemGroup", dp.Category);
									promoItem.doAction(GenerationKeys.INSERT);
								}
							} else if ("Heading".equals(dripContent.Placement)) {
								meta.put("Heading",dripContent.Description);
							} else if ("Description".equals(dripContent.Placement)) {
								meta.put("Description",dripContent.Description);
							} else if("Cover".equals(dripContent.Placement)) {
								meta.put("Cover",dripContent.Description);
							}
						} else if(dripContent.ContentType == DripService.DripContentType.Image && "Image".equals(dripContent.Placement)){
							logger.debug("\nShould be setting image path {}", dripContent.ImagePath);
							meta.put("ImagePath", dripContent.ImagePath);
						}	
					}
					item.setParameter("MetaData", meta.toJSONString());
				} else {
					item.setParameter("MetaData","{}");
				}
				item.doAction(GenerationKeys.INSERT);
			}
		}
	}
	
	public Order getOrder(final HttpServletRequest request, String orderID) throws NotAuthenticatedException, AccessException {
		logger.trace("getOrder(Request, {})", orderID);
		return getOrder(ActionBean.getConnection(request), us.getCurrentUser(request), orderID);
	}
	
	public Order getOrder(final Connection conn, final UserBean user, final String orderID) throws NotAuthenticatedException, AccessException {
		logger.trace("getOrder(Conn, {})", orderID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		final GenRow orderRow = getOrderRow(conn, orderID);
		if(!user.canView(orderRow)) {
			throw new AccessException("You do not have permission to view this order");
		}
		//this could load the order items as well. 
		Order order = BeanFactory.getOrderFromTableData(orderRow);
		order.setLinkedContacts(getOrderContacts(conn, orderRow));
		return order;
	}

	public Order saveOrder(HttpServletRequest request, Order order) throws NotAuthenticatedException, AccessException {
		logger.trace("saveOrder(Request, {})", order);
		return saveOrder(ActionBean.getConnection(request), us.getCurrentUser(request) /* this should be pulled from a user service */, order);
	}
	
	public Order saveOrder(Connection conn, UserBean user, Order order) throws NotAuthenticatedException, AccessException {
		logger.trace("saveOrder(Conn, {}, {})", user != null ? user.getName() : "null user", order);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders") || !user.canCreate("Orders")) {
			throw new AccessException("You do not have permission to create orders");
		}
		com.sok.runway.crm.Order o = new com.sok.runway.crm.Order(conn);
		o.setCurrentUser(user.getUser());
		BeanFactory.setTableDataFromOrder(order, o.getRecord());
		/* N.B. if GroupID is not set then one will be selected via SecuredEntity beforeSave */
		// The above will result in the order being different from the product, we need them the same
		if (StringUtils.isBlank(o.getGroupID()) && StringUtils.isNotBlank(o.getField("ProductID"))) {
			GenRow row = new GenRow();
			row.setTableSpec("Products");
			row.setConnection(conn);
			row.setParameter("-select0", "GroupID");
			row.setParameter("ProductID", o.getField("ProductID"));
			row.doAction("selectfirst");
			
			Collection<Group> groups = user.getUser().getGroups();

			o.setGroupID(user.getString("DefaultGroupID"));
			for (Group group : groups) {
				if (row.getString("GroupID").equals(group.getGroupID())) {
					o.setGroupID(row.getString("GroupID"));
				}
			}
		}
		o.insert();
		order.setOrderID(o.getPrimaryKey());
		checkContactShortlist(o, user);
		
		if(order.getOrderItems() != null) {
			//all these will be inserts (howver usually not populated)
			GenRow orderItem = new GenRow();
			orderItem.setConnection(conn);
			orderItem.setViewSpec("OrderItemsView");
			orderItem.setAction(ActionBean.INSERT);
			
			for(OrderItem oi: order.getOrderItems()) {
				orderItem.clear();
				oi.setOrderID(order.getOrderID());
				BeanFactory.setTableDataFromOrderItem(oi, orderItem);
				orderItem.setToNewID("OrderItemID");
				orderItem.doAction(GenerationKeys.INSERT);
				if(!orderItem.isSuccessful()) {
					throw new RuntimeException("Failed inserting order item : " + orderItem.getError());
				}
				oi.setOrderItemID(orderItem.getString("OrderItemID"));
			}
		}
		return order;		
	}
	
	public Order updateOrder(HttpServletRequest request, Order order) throws NotAuthenticatedException, AccessException {
		logger.trace("updateOrder(Request, {})", order);
		return updateOrder(ActionBean.getConnection(request), us.getCurrentUser(request) /* this should be pulled from a user service */, order);
	}
	
	public Order updateOrder(Connection conn, UserBean user, Order order) throws NotAuthenticatedException, AccessException {
		logger.trace("updateOrder(Conn, {}, {})", user != null ? user.getName() : "null user", order);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access orders");
		}
		com.sok.runway.crm.Order o = new com.sok.runway.crm.Order(conn);
		o.setCurrentUser(user.getUser());
		o.load(order.getOrderID());
		if(!o.isLoaded()) {
			throw new UnsupportedOperationException("The order could not be found");
		}
		if(!user.canEdit(o.getRecord())) {
			throw new AccessException("You do not have permission to edit this order");
		}
		BeanFactory.setTableDataFromOrder(order, o.getRecord());
		o.update();
		/* the framework won't have objects at this stage, so we need to reload it. sheesh! */
		o.load(o.getPrimaryKey());
		
		checkContactShortlist(o, user);
		checkDocuments(o, user);
		return BeanFactory.getOrderFromTableData(o.getRecord());
	}
	
	public void checkDocuments(com.sok.runway.crm.Order o, UserBean user) {
		if(StringUtils.isNotBlank(o.getField("OpportunityID"))) {
			Opportunity opp = new Opportunity(o.getConnection(), o.getField("OpportunityID"));
			opp.setCurrentUser(user.getUser());
			for(String s: o.getDocumentIDs()) {
				opp.linkDocument(s);
			}
		}
		
	}

	public void checkContactShortlist(com.sok.runway.crm.Order o, UserBean user) {
		if(StringUtils.isBlank(o.getField("RepUserID"))) {
			return;
		}
		if(!o.getField("RepUserID").equals(user.getUserID())) { 
			user = us.getSimpleUser(o.getConnection(), o.getField("RepUserID"));
		}
		checkOrderContactShortlist(o.getConnection(), o.getField("ContactID"), o.getField("ProductID"), user, o.getField("RegionID"));
	}
	

	public void checkOrderContactShortlist(Connection con, String contactID, String orderProductID, UserBean user) {
		checkOrderContactShortlist(con, contactID, orderProductID, user, null);
	}
	/**
	 * Called when an order is updated, this method will check that the appopriate product is linked to the contact 
	 * record, add the rep to the contact groups list if neccessary and re-order the groups so that this rep / group is at the top of the list. 
	 */
	public void checkOrderContactShortlist(Connection con, String contactID, String orderProductID, UserBean user, String regionID) {
		
		GenRow prod = new GenRow();
		prod.setConnection(con);
		prod.setTableSpec("Products");
		prod.setParameter("ProductID", orderProductID);
		prod.setParameter("-select1","ProductID");
		prod.setParameter("-select2","CopiedFromProductID");
		prod.setParameter("-select3","ProductType");
		prod.doAction("select");
		
		final String productID = "Home Plan".equals(prod.getData("ProductType")) && prod.isSet("CopiedFromProductID") ? prod.getData("CopiedFromProductID") : prod.getData("ProductID");
		cs.checkContactShortlist(con, user, contactID, productID, false, regionID);
		reorderGroups(con, contactID, user.getString("DefaultGroupID"), user.getUserID());
	}
	
	public void deleteOrder(HttpServletRequest request, String orderID) throws NotAuthenticatedException, AccessException {
		logger.trace("deleteOrder(Request, {})", orderID);
		deleteOrder(ActionBean.getConnection(request), us.getCurrentUser(request) /* this should be pulled from a user service */, orderID);
	}
	
	public void deleteOrder(Connection conn, UserBean user, String orderID) throws NotAuthenticatedException, AccessException {
		logger.trace("deleteOrder(Conn, {}, {})", user != null ? user.getName() : "null user", orderID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access orders");
		}
		if(StringUtils.isBlank(orderID)) {
			throw new IllegalArgumentException("OrderID cannot be null or empty");
		}
		com.sok.runway.crm.Order o = new com.sok.runway.crm.Order(conn);
		o.setCurrentUser(user.getUser());
		o.load(orderID);
		if(!o.isLoaded()) {
			/* if the order has already been deleted we do not throw an exception */
			return;
		}
		if(!o.canDelete()) {
			//not working for some reason, and secured entity will be checking this anyway.
			//throw new AccessException("You do not have permission to delete this order");
		}
		if(!o.delete()) {
			throw new RuntimeException("An internal error has occurred - " + o.getRecord().getError());
		}
	}
	
	public List<OrderItem> getOrderItems(HttpServletRequest request, String orderID) { 
		return getOrderItems(ActionBean.getConnection(request), us.getCurrentUser(request), orderID);
	}
	
	public OrderItem getOrderItem(HttpServletRequest request, String orderID, String orderItemID) {
		return getOrderItem(ActionBean.getConnection(request), us.getCurrentUser(request), orderID, orderItemID);
	}
	
	public OrderItem saveOrderItem(HttpServletRequest request, String orderID, OrderItem orderItem) {
		return saveOrderItem(ActionBean.getConnection(request), us.getCurrentUser(request), orderID, orderItem);
	}
	
	public OrderItem updateOrderItem(HttpServletRequest request, String orderID, String orderItemID, OrderItem orderItem) {
		return updateOrderItem(ActionBean.getConnection(request), us.getCurrentUser(request), orderID, orderItemID, orderItem);
	}
	
	public void deleteOrderItem(HttpServletRequest request, String orderID, String orderItemID) {
		deleteOrderItem(ActionBean.getConnection(request), us.getCurrentUser(request), orderID, orderItemID);
	}
	
	public List<OrderItem> getOrderItems(Connection conn, UserBean user, String orderID) {
		logger.trace("getOrderItems(Conn, {}, {}", user != null ? user.getName() : "null user", orderID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		//handles all the view restrictions, non-existing records, etc.
		Order order = getOrder(conn, user, orderID);
		if ("Home Plan".equals(order.getProductType())) {
			Plan plan = planService.getPlan(conn, user, order.getProductID());
			dripService.getAllDrips(conn, user, new String[]{ plan.RangeProductID, plan.HomeProductID, plan.ProductID, orderID, order.getQuoteViewID()}, null);
		} else {
			dripService.getAllDrips(conn, user, new String[]{ order.getProductID(), order.getProductID(), order.getProductID(), orderID, order.getQuoteViewID()}, null);
		}
		return getOrderItems(conn, order);
	}
	
	/**
	 * Marked private as we assume that security has already been actioned by this point. 
	 * If you want this method to be public, implement user based security for canAccess && can View for the order
	 * in question by adding an extra UserBean parameter and updating the calling methods to pass that in.
	 */
	private List<OrderItem> getOrderItems(Connection conn, Order order) {
		logger.trace("getOrderItems(Conn, {})", order);
		final GenRow orderItems = new GenRow();
		try { 
			orderItems.setViewSpec("OrderItemsView");
			orderItems.setConnection(conn);
			orderItems.setParameter("OrderID", order.getOrderID());
			orderItems.setParameter("-sort1", "SortNumber");
			orderItems.setParameter("-order1","ASC");
 
			if(logger.isDebugEnabled()) { 
				orderItems.doAction("search");
				logger.debug(orderItems.getStatement());
			} 
			orderItems.getResults(true);
			
			if(orderItems.getNext()) { 
				List<OrderItem> list = new ArrayList<OrderItem>(orderItems.getSize());
				do {
					list.add(BeanFactory.getOrderItemFromTableData(orderItems));
				} while(orderItems.getNext());
				return list;
			} 
			return Collections.emptyList();
		} finally {
			orderItems.close();
		}
	}
	
	public OrderItem getOrderItem(Connection conn, UserBean user, String orderID, String orderItemID) {
		Order order = getOrder(conn, user, orderID);
		com.sok.runway.crm.OrderItem oi = new com.sok.runway.crm.OrderItem(conn);
		oi.setCurrentUser(user.getUser());
		oi.load(orderItemID);
		if(!oi.isLoaded()) {
			throw new NotFoundException("The order item was not found");
		}
		if(!order.getOrderID().equals(oi.getField("OrderID"))) {
			throw new IllegalArgumentException("The order item did not belong to the order given");
		}
		return BeanFactory.getOrderItemFromTableData(oi.getRecord());
	}
	
	public OrderItem saveOrderItem(Connection conn, UserBean user, String orderID, OrderItem orderItem) {
		Order order = getOrder(conn, user, orderID);
		if(!order.getOrderID().equals(orderItem.getOrderID())) {
			//Validation Exception ?
			throw new IllegalArgumentException("The order item did not belong to the order given");
		}

		if (StringUtils.isBlank(orderItem.getCopiedProductID())) orderItem.setCopiedProductID(findRootCostings(conn, orderItem.getProductID()));

		com.sok.runway.crm.OrderItem oi = new com.sok.runway.crm.OrderItem(conn);
		oi.setCurrentUser(user.getUser());
		//Drips will now have appropriate costs, we don't need to update this.
		//if("DRIP".equals(orderItem.getCategory())) {
		//	orderItem.setTotalCost(orderItem.getCost());
		//	orderItem.setGST(0);
		//}
		BeanFactory.setTableDataFromOrderItem(orderItem, oi.getRecord());
		/* do something to handle sorting ? */
		oi.insert();
		oi.load(oi.getPrimaryKey());
		
		if ("Package".equals(orderItem.getCategory()) || "Plan".equals(orderItem.getCategory())) {
			if (!orderItem.getProductID().equals(order.getProductID())) {
				order.setProductID(orderItem.getProductID());
				updateOrderProduct(conn, user, orderID, orderItem.getProductID());
			}
		}
		
		logger.debug("orderItem.getProductID() is >> " + orderItem.getProductID());
		
		if (orderItem.getTotalcost() != 0 && orderItem.getCategory() != null && orderItem.getCategory().startsWith("Custom-") 
				&& StringUtils.isNotBlank(orderItem.getProductID())) {
			// we need to update the base costs of the product as it includes these values.
			if (orderItem.getMetaData() == null || orderItem.getMetaData().indexOf("special") == -1) {
				com.sok.runway.crm.OrderItem pi = new com.sok.runway.crm.OrderItem(conn);
				pi.loadFromField(new String[]{"OrderID", "ProductID"}, new String[]{order.getOrderID(),order.getProductID()});
				if (pi.isLoaded()) {
					try {
						double total = pi.getDouble("TotalCost");
						double itemTotal = orderItem.getTotalcost();
						total -= itemTotal;
						GenRow pItem = new GenRow();
						pItem.setTableSpec("OrderItems");
						pItem.setConnection(conn);
						pItem.setParameter("TotalCost","" + total);
						pItem.setParameter("Cost","" + Math.ceil((total / 1.1 ) * 100) / 100);
						pItem.setParameter("ItemCost","" + Math.ceil((total / 1.1 ) * 100) / 100);
						pItem.setParameter("GST","" + (total - (Math.ceil((total / 1.1 ) * 100) / 100)));
						pItem.setParameter("OrderItemID", pi.getPrimaryKey());
						pItem.doAction("update");
					} catch (Exception e) {}
				}
			}
		} else if ("Plan".equals(orderItem.getCategory()) && StringUtils.isNotBlank(order.getRegionID())) {
			// If the Order Item is a plan then use the region pricing

			com.sok.runway.crm.OrderItem pi = new com.sok.runway.crm.OrderItem(conn);
			pi.loadFromField(new String[] { "OrderID", "ProductID" }, new String[] { order.getOrderID(), order.getProductID() });
			if (pi.isLoaded()) {
				try {
					double total = RunwayUtil.getRegionalPrice(order.getProductID(), order.getRegionID());

					GenRow pItem = new GenRow();
					pItem.setTableSpec("OrderItems");
					pItem.setConnection(conn);
					pItem.setParameter("TotalCost", "" + total);
					pItem.setParameter("Cost", "" + Math.ceil((total / 1.1) * 100) / 100);
					pItem.setParameter("ItemCost", "" + Math.ceil((total / 1.1) * 100) / 100);
					pItem.setParameter("GST", "" + (total - (Math.ceil((total / 1.1) * 100) / 100)));
					pItem.setParameter("OrderItemID", pi.getPrimaryKey());
					pItem.doAction("update");
				} catch (Exception e) {
				}
			}

		} else if ("DRIP".equals(orderItem.getCategory())) {
			
			addDripVPB(conn, user, orderID, orderItem.getCopiedProductID(), orderItem.getName());
		}
		
		/* update the costs in the parent record */
		updateOrder(conn, user, orderID);
		
		if ("Facade".equals(orderItem.getCategory())) {
			// need to link this image to the order;
			setOrderImage(conn, orderID, order.getProductID(), orderItem.getProductID());
		}
		
		OrderItem ni = BeanFactory.getOrderItemFromTableData(oi.getRecord());
		
		if (ni.getDescription().length() == 0) {
			ni.setPackageCosts(loadPackageCosts(conn, ni.getItemGroup()));
	
			if (orderItem.isPackageCost() || ni.getPackageCosts() != null) {
				ni.setPackageCost(true);
				ni.setItemLocked(false);
			}
		}

		return ni;
	}
	
	private String findRootCostings(Connection conn, String productID) {
		if (StringUtils.isBlank(productID))	return null;

		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");

		if (row.getString("PackageCostProductID").length() > 0) {
			String tmpID = findRootCostings(conn,row.getString("PackageCostProductID"));
			if (StringUtils.isNotBlank(tmpID)) {
				return tmpID;
			}
		}

		return row.getData("ProductID");
	}

	private void addDripVPB(Connection conn, UserBean user, String orderID, String productID, String itemGroup) {
		if (StringUtils.isBlank(orderID) || StringUtils.isBlank(productID)) return;
		
		GenRow pp = new GenRow();
		pp.setViewSpec("ProductLinkedProductView");
		pp.setConnection(conn);
		pp.setParameter("ProductID", productID);
		pp.doAction("search");
		
		pp.getResults();
		
		while (pp.getNext()) {
			OrderItem oi = new OrderItem();
			oi.setCategory("VPB");
			oi.setDescription(pp.getString("Description"));
			oi.setItemGroup(itemGroup);
			oi.setName(pp.getName());
			oi.setOrderID(orderID);
			oi.setProductID(pp.getString("LinkedProductID"));
			oi.setProductNum(pp.getString("ProductNum"));
			oi.setQuantity(pp.getDouble("Quantity"));
			oi.setCost(pp.getDouble("Cost"));
			oi.setGST(pp.getDouble("GST"));
			oi.setTotalCost(pp.getDouble("TotalCost"));
			
			saveOrderItem(conn, user, orderID, oi);
		}
		
		pp.close();
	}

	private PackageCost[] loadPackageCosts(Connection conn, String category) {
		if (category == null || category.length() == 0) return null;
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("ProductType", "Package Cost Library");
		row.setParameter("Category", category);
		row.setParameter("Active", "Y");
		row.sortBy("Name", 0);
		row.doAction("search");
		
		row.getResults(true);
		
		if (row.getSize() == 0) return null;
		
		PackageCost[] pcost = new PackageCost[row.getSize()];
		
		int p = 0;
		
		while (row.getNext()) {
			PackageCost pc = new PackageCost();
			pc.setName(row.getString("Name"));
			if (row.isSet("ProductDetails")) pc.setDescription(row.getString("ProductDetails"));
			else pc.setDescription(row.getString("Name"));
			pc.setCategory(row.getData("Category"));
			pc.setProductID(row.getString("ProductID"));
			pc.setQuantity(1);
			if (!row.isSet("PackageCostCalcField") && row.getDouble("TotalCost") != 0) {
				pc.setCost((Math.round((row.getDouble("TotalCost") / 1.1) * 100) / 100));
				pc.setItemCost((Math.round((row.getDouble("TotalCost") / 1.1) * 100) / 100));
				pc.setGST(row.getDouble("TotalCost") - (Math.round((row.getDouble("TotalCost") / 1.1) * 100) / 100));
				pc.setTotalCost(row.getDouble("TotalCost"));
			} else {
				pc.setCost(0);
				pc.setItemCost(0);
				pc.setGST(0);
				pc.setTotalCost(0);
			}
			pcost[p++] = pc;
		}
		
		return pcost;
	}

	private void updateOrderProduct(Connection conn, UserBean user, String orderID, String productID) {
		RowBean order = new RowBean();
		order.setViewSpec("OrderView");
		order.setConnection(conn);
		order.setColumn("OrderID", orderID);
		order.setColumn("ProductID", productID);
		order.setAction("update");
		order.doAction();
	}

	public OrderItem updateOrderItem(Connection conn, UserBean user, String orderID, String orderItemID, OrderItem orderItem) {
		Order order = getOrder(conn, user, orderID);
		com.sok.runway.crm.OrderItem oi = new com.sok.runway.crm.OrderItem(conn);
		oi.setCurrentUser(user.getUser());
		oi.load(orderItemID);
		if(!oi.isLoaded()) {
			throw new NotFoundException("The order item was not found");
		}
		if(!order.getOrderID().equals(oi.getField("OrderID"))) {
			throw new IllegalArgumentException("The order item did not belong to the order given");
		}
		
		BeanFactory.setTableDataFromOrderItem(orderItem, oi.getRecord());
		oi.update();
		
		updateOrder(conn, user, orderID);
		
		oi.load(oi.getPrimaryKey());
		
		if ("Facade".equals(orderItem.getCategory())) {
			// need to link this image to the order;
			setOrderImage(conn, orderID, order.getProductID(), orderItem.getProductID());
		}
		return BeanFactory.getOrderItemFromTableData(oi.getRecord());
	}
	
	// TODO Mikey this may need cleaning up
	public void setOrderImage(Connection conn, String orderID, String productID, String facadeID) {
		GenRow prod = new GenRow();
		prod.setConnection(conn);
		prod.setTableSpec("Products");
		prod.setParameter("ProductID", facadeID);
		prod.setParameter("-select1","Image");
		prod.setParameter("-select2","ThumbnailImage");
		prod.doAction("selectfirst");
		
		GenRow link = new GenRow();
		link.setConnection(conn);
		link.setTableSpec("LinkedDocuments");
		link.setParameter("-select0", "*");
		link.setParameter("OrderID", orderID);
		link.doAction("selectfirst");
		
		if (link.isSuccessful() || link.getString("LinkedDocumentID").length() > 0) {
			String linkedDocID = link.getString("LinkedDocumentID");
			link.clear();
			link.setParameter("LinkedDocumentID", linkedDocID);
			link.setParameter("CreatedDate","NOW");
			link.setParameter("ProductID", productID);
			link.setParameter("OrderID", orderID);
			if (prod.getString("Image").length() > 0) link.setParameter("DocumentID", prod.getString("Image"));
			else if (prod.getString("Image").length() > 0) link.setParameter("DocumentID", prod.getString("Image"));
			
			if (link.getString("DocumentID").length() > 0) link.doAction("update");
		} else {
			link.clear();
			link.setParameter("LinkedDocumentID", KeyMaker.generate());
			link.setParameter("CreatedDate","NOW");
			link.setParameter("CreatedBy", InitServlet.getSystemParams().getProperty("SystemUserID"));
			link.setParameter("OrderID", orderID);
			link.setParameter("ProductID", productID);
			if (prod.getString("Image").length() > 0) link.setParameter("DocumentID", prod.getString("Image"));
			else if (prod.getString("Image").length() > 0) link.setParameter("DocumentID", prod.getString("Image"));
			
			if (link.getString("DocumentID").length() > 0) link.doAction("insert");
		}

	}
	
	public void deleteOrderItem(Connection conn, UserBean user, String orderID, String orderItemID) {
		
		com.sok.runway.crm.OrderItem oi = new com.sok.runway.crm.OrderItem(conn);
		oi.setCurrentUser(user.getUser());
		oi.load(orderItemID);
		if(!oi.isLoaded()) {
			return;
		}
		if(!StringUtils.equals(orderID, oi.getField("OrderID"))) {
			throw new IllegalArgumentException("The order item did not belong to the order given");
		}
		if(!oi.delete()) {
			throw new RuntimeException("An internal error has occurred - " + oi.getRecord().getError());
		}
		updateOrder(conn, user, orderID);
	}
	
	//taken from crm/orders/actions/includes/updateorder.jsp
	public String runwayUpdateOrder(HttpServletRequest request, String orderID) {
		StringBuilder upd = new StringBuilder();
		try {
			runwayUpdateOrder(ActionBean.getConnection(request), us.getCurrentUser(request), orderID, upd);
		}catch(Exception e){System.out.println(e); e.printStackTrace(); }
		return upd.toString();
	}
	
	public void updateOrder(Connection conn, UserBean user, String orderID) {
		runwayUpdateOrder(conn, user, orderID, null);
	}
	
	//taken from crm/orders/actions/includes/updateorder.jsp
	public void runwayUpdateOrder(Connection conn, UserBean currentuser, String orderID, StringBuilder out) {
		
		RowBean order = new RowBean();
		order.setViewSpec("OrderView");
		order.setConnection(conn);
		order.setColumn("OrderID", orderID);
		order.setAction("select");
		order.doAction();
		
		String discountstr = order.getString("Discount");
		float discount = 0.0f;
		if(discountstr.length()!=0)
		{
			discount = Float.parseFloat(discountstr);
		}
		
		//BaseCost
		//Margin

		StringBuffer stmt = new StringBuffer();
		stmt.append("Select");
		//stmt.append(" (SELECT SUM(GST) FROM OrderItems WHERE OrderItems.OrderID = Orders.OrderID) as GST, ");
		//stmt.append(" (SELECT SUM(Cost) FROM OrderItems WHERE OrderItems.OrderID = Orders.OrderID) as Cost, ");
		//stmt.append(" (SELECT SUM(TotalCost) FROM OrderItems WHERE OrderItems.OrderID = Orders.OrderID) as TotalCost, ");
		stmt.append(" (SELECT SUM(Quantity) FROM OrderItems WHERE OrderItems.OrderID = Orders.OrderID) as Quantity, ");
		
		stmt.append(" (SELECT SUM(OrderItems.Quantity*Products.BaseCost) FROM OrderItems INNER JOIN Products ON OrderItems.ProductID = Products.ProductID WHERE OrderItems.OrderID = Orders.OrderID) as BaseCost, ");
		stmt.append(" (SELECT 1 - (SUM(OrderItems.Quantity*Products.BaseCost) / SUM(OrderItems.Cost)) FROM OrderItems INNER JOIN Products ON OrderItems.ProductID = Products.ProductID WHERE OrderItems.OrderID = Orders.OrderID) as Margin ");
		
		
		stmt.append("FROM Orders WHERE (OrderID = '");
		stmt.append(orderID);
		stmt.append("')");
		
		
		GenRow items = new GenRow();
		items.setConnection(conn);
		items.setViewSpec("OrderItemsView");
		items.setParameter("OrderID", orderID);
		items.getResults(); 
		
		//float baseCost = 0;
		float cost = 0;
		float gst = 0;
		//float qty = 0;
		float totalcost = 0;
		
		while(items.getNext()) {
			//baseCost += items.getFloat("BaseCost");
			cost += items.getFloat("Cost");
			if(!"DRIP".equals(items.getData("Category"))) {
				gst += items.getFloat("GST");
			}
			totalcost += items.getFloat("TotalCost");
		}
		
		RowBean bean = new RowBean();
		bean.setConnection(conn);
		bean.setSearchStatement(stmt.toString());
		bean.retrieveDataOnStmt();
		
		//System.out.print(stmt); 
		order.clear();
		order.setConnection(conn);
		order.setColumn("OrderID", orderID);
		order.setAction("update");

		order.setColumn("Cost", String.valueOf(cost));
		order.setColumn("Quantity", bean.getString("Quantity"));
		order.setColumn("BaseCost", bean.getString("BaseCost"));
		
		float discountamount = 0.0f;
		//float gst = Float.parseFloat(StringUtils.defaultIfEmpty(bean.getString("GST"), "0.0f"));
		//float totalcost = Float.parseFloat(StringUtils.defaultIfEmpty(bean.getString("TotalCost"), "0.0f"));
		float totalcostwithdiscount = totalcost;
		float margin = Float.parseFloat(StringUtils.defaultIfEmpty(bean.getString("Margin"), "0.0f"));

		if(discount == 0)
		{	
			order.setColumn("DiscountAmount", String.valueOf(discountamount));	
			order.setColumn("GST", String.valueOf(gst));	
			order.setColumn("TotalCost", String.valueOf(totalcost));
			order.setColumn("Margin", String.valueOf(margin));
		}
		else
		{
		  	discountamount = 0.0f;
			gst = 0.0f;
			totalcost = 0.0f;
			totalcostwithdiscount = 0.0f;
			
			//float cost = 0.0f;
			float baseCost = 0.0f;
			//float gstRate = Float.parseFloat(InitServlet.getSystemParam("SalesTaxRate")); 
			//if(bean.getString("Cost").length()!=0) {
			//	cost = Float.parseFloat(bean.getString("Cost"));
			//}
			if(bean.getString("BaseCost").length()!=0) {
				baseCost = Float.parseFloat(bean.getString("BaseCost"));
			}
			
			discountamount = cost * discount / 100;
			totalcostwithdiscount = cost - discountamount;
			//gst =  totalcostwithdiscount * gstRate;		//not entirely convinced this is correct, perhaps the discount should be applied on a per-item basis to make this work. this shouldn't matter unless some discounted items
					// are gst free 
			totalcost = totalcostwithdiscount + gst;
			
			margin = 1 - (baseCost / totalcostwithdiscount);
			
			order.setColumn("DiscountAmount", String.valueOf(discountamount));	
			order.setColumn("Margin", String.valueOf(margin));	
			order.setColumn("GST", String.valueOf(gst));	
			order.setColumn("TotalCost", String.valueOf(totalcost));
			
			if(out != null) {
				out.append(gst).append("<br>\n")
				.append(discountamount).append("<br>\n")
				.append(totalcost).append("<br>\n")
				.append(totalcostwithdiscount).append("<br>\n");
			}
		}	
		order.doAction();	
		order.close();
		
	  // If attached to an opportunity, add to its total
	  GenRow opportunityOrder = new GenRow();
	  opportunityOrder.setConnection(conn);
	  opportunityOrder.setTableSpec("Orders");
	  opportunityOrder.setParameter("-join1", "OrderOpportunity");
	  opportunityOrder.setParameter("OrderID", orderID);
	  opportunityOrder.setParameter("-select1", "OpportunityID");
	  //currentuser.setConstraint(opportunityOrder, "OrderGroup");
	  opportunityOrder.setAction(GenerationKeys.SELECTFIRST);
	  opportunityOrder.doAction();
	  String opportunityID = opportunityOrder.getData("OpportunityID");
	  if(opportunityID != null && opportunityID.length() > 0) {
	     opportunityOrder.clear();
	     opportunityOrder.setTableSpec("Opportunities");
	     opportunityOrder.setParameter("OpportunityID", opportunityID);
	     opportunityOrder.setParameter("QuoteAmount", totalcostwithdiscount);
	     opportunityOrder.setParameter("TotalQuoteAmount", totalcost);
	     //currentuser.setConstraint(opportunityOrder, "OpportunityGroup");
	     opportunityOrder.setAction(GenerationKeys.UPDATE);
	     opportunityOrder.doAction();     
	  }
	  opportunityOrder.close();
	  
	  if(out != null) out.append(order.getError());
	}

	public void deleteOrderTemplate(Connection conn, UserBean user, String orderTemplateID) {
		//TODO - user security? 
		GenRow templates = new GenRow();
		templates.setConnection(conn);
		templates.setViewSpec("OrderTemplateView");
		templates.setParameter("ON-OrderTemplateID", orderTemplateID);
		if (orderTemplateID != null && orderTemplateID.length() > 0) {
		templates.doAction(GenerationKeys.DELETEALL);				
		} else {
			throw new NotFoundException("The order templte id was not found");
		}
		activityLogger.logActivity(user.getUser(), templates.getConnection(), "OrderTemplates", orderTemplateID,"delete");
		
		templates.clear();
		templates.setViewSpec("OrderTemplateItemsView");
		templates.setParameter("ON-OrderTemplateID", orderTemplateID);
		if (orderTemplateID != null && orderTemplateID.length() > 0) {
		templates.doAction(GenerationKeys.DELETEALL);
			logger.error("deleteOrderTemplate row {}",templates.getStatement());
		} else {
			throw new NotFoundException("The order templte id was not found");
		}
		
		this.setCachedTemplate(orderTemplateID, null);
	}
	
	public List<OrderContact> getOrderContacts(HttpServletRequest request, String orderID) {
		logger.debug("getOrderContacts(Request, {})", orderID);
		return getOrderContacts(ActionBean.getConnection(request), us.getCurrentUser(request), orderID);
	}
	
	public OrderContact createOrderContact(HttpServletRequest request, String orderID, OrderContact orderContact)
	{
		if(logger.isDebugEnabled()) logger.debug("createOrderContact(Request, {}, {})", orderID, orderContact != null ? orderContact.getName() : "null OrderContact");
		return createOrderContact(ActionBean.getConnection(request), us.getCurrentUser(request), orderID, orderContact);
	}
	
	public OrderContact updateOrderContact(HttpServletRequest request, String orderID, String orderContactID, OrderContact orderContact)
	{
		if(logger.isDebugEnabled()) logger.debug("updateOrderContact(Request, {}, {}, {})", new String[]{ orderID, orderContactID, orderContact != null ? orderContact.getName() : "null OrderContact" });
		return updateOrderContact(ActionBean.getConnection(request), us.getCurrentUser(request), orderID, orderContactID, orderContact);
	}
	
	public void deleteOrderContact(HttpServletRequest request, String orderID, String orderContactID) { 
		deleteOrderContact(ActionBean.getConnection(request), us.getCurrentUser(request), orderID, orderContactID);
	}
	
	public List<OrderContact> getOrderContacts(Connection conn, UserBean user, String orderID) {
		if(logger.isDebugEnabled()) logger.debug("getOrderContacts(Conn, {}, {}, {})", new String[]{user != null ? user.getName() : "null user", orderID});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		final GenRow orderRow = getOrderRow(conn, orderID);
		if(!user.canView(orderRow)) {
			throw new AccessException("You do not have permission to view this order");
		}
		return getOrderContacts(conn, orderRow);
	}
	
	private List<OrderContact> getOrderContacts(Connection conn, GenRow orderRow) {
		
		final GenRow linkedContacts = new GenRow();
		try { 
			linkedContacts.setConnection(conn);
			linkedContacts.setViewSpec("OrderContactListView");
			linkedContacts.setParameter("OrderID", orderRow.getString("OrderID"));
			
			if(logger.isDebugEnabled()) {
				linkedContacts.doAction(GenerationKeys.SEARCH);
				logger.debug(linkedContacts.getStatement());
			}
			if(linkedContacts.isSet("OrderID")) {
				linkedContacts.getResults(true);
			}
			if(linkedContacts.getNext()) {
				List<OrderContact> linkedContactArray = new ArrayList<OrderContact>(linkedContacts.getSize());
				do {
					linkedContactArray.add(BeanFactory.getOrderContactFromTableData(linkedContacts)); 
				} while(linkedContacts.getNext());
				return linkedContactArray;
			}
		} finally {
			linkedContacts.close();
		}
		return Collections.emptyList();
	}
	
	
	public OrderContact createOrderContact(Connection conn, UserBean user, String orderID, OrderContact orderContact)
	{
		if(logger.isTraceEnabled()) logger.trace("createOrderContact(Conn, {}, {}, {})", new String[]{user != null ? user.getName() : "null user", orderID, orderContact != null ? orderContact.getName() : "null OrderContact"});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		final GenRow orderRow = getOrderRow(conn, orderID);
		if(!user.canEdit(orderRow)) {
			throw new AccessException("You do not have permission to edit this order");
		}
		
		GenRow oc = new GenRow();
		oc.setConnection(conn);
		oc.setTableSpec("OrderContacts");
		oc.setParameter("-select1", "OrderContactID");
		oc.setParameter("OrderID", orderID);
		oc.setParameter("ContactID", orderContact.getContactID());
		
		if(!oc.isSet("OrderID")) {
			throw new IllegalArgumentException("OrderID must be provided");
		}
		if(!oc.isSet("ContactID")) {
			/* create a contact id and set it in the order */
			Contact c = new Contact(conn);
			c.setCurrentUser(user.getUser());
			BeanFactory.setTableDataFromContact(orderContact, c.getRecord());
			 
			c.setGroupID(orderContact.getGroupID());
			c.setParameter("ContactStatusID", InitServlet.getSystemParam("ContactLinkedDefaultStatus"));
			
			ErrorMap errors = c.insert();
			if(!errors.isEmpty()) {
				logger.error("failed to insert");
				logger.error(errors.toString());
				throw new RuntimeException("failed to insert contact");
			}
			oc.setParameter("ContactID", c.getPrimaryKey());
			if("secondary".equals(orderContact.getRole())) {
				orderContact.setGroupStatusID(InitServlet.getSystemParam("ContactLinkedDefaultStatus"));
			}
			ContactService.getInstance().createContactGroup(conn, user, c.getPrimaryKey(), orderContact.getGroupID(),orderRow.getString("RepUserID"), orderContact.getGroupStatusID());
		}
		if(!oc.isSet("ContactID")) {
			throw new IllegalArgumentException("ContactID must be provided");
		}
		oc.doAction(GenerationKeys.SELECTFIRST);
		
		if(!oc.isSuccessful()) {
			oc.setToNewID("OrderContactID");
			oc.setAction(GenerationKeys.INSERT);
		} else {
			/* we want to update the existing if one exists */
			oc.setAction(GenerationKeys.UPDATE);
		}
		BeanFactory.setTableDataFromOrderContact(orderContact, oc);
		oc.doAction();
		
		if(!oc.isSuccessful()) {
			throw new RuntimeException("An unexpected error occurred: " + oc.getError());
		}
		oc.setViewSpec("OrderContactListView");
		oc.doAction(GenerationKeys.SELECT); // loads up the contact fields 
		
		return BeanFactory.getOrderContactFromTableData(oc);
	}
	
	public OrderContact updateOrderContact(Connection conn, UserBean user, String orderID, String orderContactID, OrderContact orderContact)
	{
		if(logger.isTraceEnabled()) logger.trace("createOrderContact(Request, {}, {}, {}, {})", new String[]{user != null ? user.getName() : "null user", orderID, orderContactID, orderContact != null ? orderContact.getName() : "null OrderContact" });
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		final GenRow orderRow = getOrderRow(conn, orderID);
		if(!user.canEdit(orderRow)) {
			throw new AccessException("You do not have permission to edit this order");
		}
		if(!orderContactID.equals(orderContact.getOrderContactID())) {
			throw new IllegalArgumentException("OrderContactID from path did not match orderContact object");
		}
		if(!orderID.equals(orderContact.getOrderID())) {
			throw new IllegalArgumentException("OrderID from path did not match orderContact object");
		}
		GenRow oc = this.getOrderContactRow(conn, orderContactID);
		if(!orderID.equals(oc.getString("OrderID"))) {
			throw new IllegalArgumentException("OrderID and OrderID from OrderContact did not match");
		}

		Contact c = new Contact(conn);
		c.setCurrentUser(user.getUser());
		c.load(orderContact.getContactID());
		
		if(!c.isLoaded()) {
			throw new NotFoundException("Could not find contact with id " +  orderContact.getContactID() + " for update");
		}
		if(!user.getUser().canEdit(c)) {
			throw new NotFoundException("You do not have permission to edit this contact:  " +  orderContact.getContactID());
		}
		BeanFactory.setTableDataFromContact(orderContact, c.getRecord());
		c.update();
		
		BeanFactory.setTableDataFromOrderContact(orderContact, oc);
		oc.doAction(GenerationKeys.UPDATE);
		
		if(!oc.isSuccessful()) {
			throw new RuntimeException("An unexpected error occurred: " + oc.getError());
		}
		oc.setViewSpec("OrderContactListView");
		oc.doAction(GenerationKeys.SELECT); // loads up the contact fields 
		
		return BeanFactory.getOrderContactFromTableData(oc);
	}
	
	public void deleteOrderContact(Connection conn, UserBean user, String orderID, String orderContactID) {
		if(logger.isTraceEnabled()) logger.trace("deleteOrderContact(Conn, {},{},{})", new String[]{user != null ? user.getName() : "null user", orderID, orderContactID });
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		final GenRow orderRow = getOrderRow(conn, orderID);
		if(!user.canEdit(orderRow)) {
			throw new AccessException("You do not have permission to edit this order");
		}
		try {
			final GenRow contactRow = getOrderContactRow(conn, orderContactID);
			if(!orderID.equals(contactRow.getString("OrderID"))) {
				throw new IllegalArgumentException("The order contact did not belong to the order given");
			}
			contactRow.doAction(GenerationKeys.DELETE);
			if(!contactRow.isSuccessful()) {
				throw new RuntimeException("An unexpected error occurred: " + contactRow.getError());
			}
		} catch (NotFoundException nfe) {
			//we don't care about this on delete
		}
	}

	/**
	 * This code will reorder the groups of a contact to set the current user as the top group, but only if that user is in the list of groups as a rep
	 * 
	 * @param conn: a database connection
	 * @param contactID: the id of the contact to be reordered
	 * @param groupID: the group to be reordered, this is optional, it can be null or ""
	 * @param repUserID: this is the UserID of the current User
	 * 
	 *  TODO this method should be called on any update or insert of orders, including order items
	 */
	protected void reorderGroups(Connection conn, String contactID, String groupID, String repUserID) {
		  if (contactID == null || contactID.length() == 0) return;
		  GenRow repGroup = new GenRow();
		  repGroup.setViewSpec("ContactGroupView");
		  repGroup.setConnection(conn);
		  repGroup.setParameter("ContactID", contactID);
		  repGroup.setParameter("GroupID", groupID);
		  repGroup.setParameter("RepUserID", repUserID);
		  repGroup.doAction("selectfirst");
		  
		  if (!repGroup.isSuccessful() || repGroup.getString("ContactGroupID").length() == 0) {
			  repGroup.remove("GroupID");
			  repGroup.doAction("selectfirst");
		  }
		  
		  if (!"Y".equals(repGroup.getString("MoveToTop"))) return;

		  if (repGroup.isSuccessful() && repGroup.getString("ContactGroupID").length() > 0) {
			  GenRow group = new GenRow();
			  group.setViewSpec("ContactGroupView");
			  group.setConnection(conn);
			  group.setParameter("ContactID", contactID);
			  group.setParameter("ContactGroupID", "!" + repGroup.getString("ContactGroupID"));
			  group.sortBy("SortOrder", 0);
			  group.doAction("search");
			  
			  GenRow row = new GenRow();
			  row.setTableSpec("ContactGroups");
			  row.setConnection(conn);
			  row.setParameter("ContactGroupID", repGroup.getString("ContactGroupID"));
			  row.setParameter("SortOrder", "0");
			  row.doAction("update");
			  
			  group.getResults();
			  
			  int so = 1;
			  
			  while (group.getNext()) {
				  row.clear();
				  row.setParameter("ContactGroupID", group.getData("ContactGroupID"));
				  row.setParameter("SortOrder", "" + so);
				  row.doAction("update");
				  ++so;
			  }
			  group.close();
		  }
	}
	
	/**
	 * If a price change has occurred many items in the order will not be valid, not just the drips.
	 * 
	 * We are allowing duplication of original pricing so long as as a price change has not occurred.
	 * 
	 */
	public Order duplicateOrder(HttpServletRequest request, final String orderID) {
		Order order = this.getOrder(request,  orderID);
		final String userID = UserService.getInstance().getCurrentUser(request).getUserID(); 
		boolean priceChange = isPromotionLocked(order.getDateOfSale());
		boolean quoteUpgrade = "true".equals(request.getParameter("-upgradeQuote"));
		logger.debug("duplicateOrder(request, {}) [priceChange={}]", orderID, priceChange);
		if(priceChange && !quoteUpgrade) {
			throw new RuntimeException("Order cannot be duplicated as a price change has occurred");
		}
		order.setOrderID(null);
		order.setOrderNum(null);
		order.setDateOfSale(new java.util.Date());
		order.setCreatedDate(order.getDateOfSale());
		order.setCreatedBy(userID); 
		order.setModifiedDate(order.getDateOfSale());
		order.setModifiedBy(userID); 
		
		order.setOpportunityID(null);
		order = this.saveOrder(request, order);
		
		List<OrderContact> list = this.getOrderContacts(request, orderID);
		for(OrderContact oc : list) {
			oc.setOrderContactID(null);
			oc.setOrderID(order.getOrderID());
			this.createOrderContact(request, order.getOrderID(), oc);
		}
		
		String packageID = null; 
		
		for(OrderItem oi: this.getOrderItems(request, orderID)) {
			/* 
			 * We're not doing this anymore, we will throw an exception. 
			 * If reverted this can be used as a basis for duplicating after a price change.
			 * VPB and Custom items may need to be updated in this case.
			if(priceChange) {
				if("DRIP".equals(oi.getCategory())) {
					logger.debug("Promotion was locked, skipping drip item {}", oi.getName());
					continue;
				} else if("Package".equals(oi.getCategory())) {
					addPackageIncludedDripItems(request, orderID, oi.getProductID());
				} else if("Plan".equals(oi.getCategory())) {
					addPlanIncludedDripItems(request, orderID, oi.getProductID());
				}
			}
			*/
			// if we're duplicating post update, add in the relevant package costs
			boolean addItem = false;
		    if(!quoteUpgrade || "VPB".equals(oi.getCategory()) || "Facade".equals(oi.getCategory())) {
		    	addItem = true; 
		    } else if (StringUtils.isNotBlank(oi.getCategory()) && oi.getCategory().startsWith("Custom-") && (!oi.isQuantityLocked() || !oi.isCostLocked())) {
		    	addItem = true;
		    }
			if("Package".equals(oi.getCategory())) {
				packageID = oi.getProductID(); 
				if(quoteUpgrade) {
					HLPackage hlp = planService.getPackage(request, oi.getProductID()); 
					if("true".equals(InitServlet.getSystemParam("QuotePackageWithoutLand"))) { 
						Lot l = BeanFactory.external(EstateService.getInstance().getLot(request, hlp.LotProductID)); 
						oi.setItemCost(hlp.Cost - l.getCost());
						oi.setCost(hlp.Cost - l.getCost());
						oi.setGST(hlp.GST - l.getGST());
						oi.setTotalCost(hlp.TotalCost - l.getTotalCost());
					} else {
						oi.setItemCost(hlp.Cost);
						oi.setCost(hlp.Cost);
						oi.setGST(hlp.GST);
						oi.setTotalCost(hlp.TotalCost); 
					}
					
					String quoteViewID = hlp.QuoteViewID; // planService.getPackageQuoteViewID(request, oi.getProductID());
					if(StringUtils.isNotBlank(quoteViewID) && !StringUtils.equals(quoteViewID, order.getQuoteViewID())) {
						order.setQuoteViewID(quoteViewID);
						saveOrder(request, order); 
					}
					addPackageIncludedDripItems(request, order.getOrderID(), oi.getProductID(), order.getQuoteViewID());
				}
				addItem = true; 
			} else if("Plan".equals(oi.getCategory())) {
				if(quoteUpgrade) { 
					String planID = oi.getProductID();
					
					PlanEntity pe = new PlanEntity(request);
					pe.load(planID);
					
					if(pe.isSet("CopiedFromProductID")) {
						planID = PlanService.getInstance().duplicatePlan(request, pe.getField("CopiedFromProductID"),userID);
					} else {
						// If it wasn't copied before, should it be? 
						// planID = PlanService.getInstance().duplicatePlan(request, planID, UserService.getInstance().getCurrentUser(request).getUserID());
					}
					
					PlanService.Plan p = planService.getPlan(request, planID);
					oi.setProductID(planID);
					oi.setItemCost(p.Cost);
					oi.setCost(p.Cost);
					oi.setGST(p.GST);
					oi.setTotalCost(p.TotalCost);
					
					String quoteViewID = p.QuoteViewID; //planService.getPlanQuoteViewID(request, oi.getProductID());
					if(StringUtils.isNotBlank(quoteViewID) && !StringUtils.equals(quoteViewID, order.getQuoteViewID())) {
						order.setQuoteViewID(quoteViewID);
						saveOrder(request, order); 
					}
					addPlanIncludedDripItems(request, order.getOrderID(), oi.getProductID(), order.getQuoteViewID());
				}
				addItem = true;
			} 
			if(addItem) {
				oi.setOrderItemID(null);
				oi.setOrderID(order.getOrderID());
				this.saveOrderItem(request, order.getOrderID(), oi);
			}
		}
		if(false && quoteUpgrade) {
			QuoteView qv = QuoteViewService.getInstance().getQuoteView(request, order.getQuoteViewID());
			if(qv != null && qv.OrderTemplate != null && qv.OrderTemplate.getOrderItems() != null) {
				GenRow orderItem = new GenRow();
				orderItem.setRequest(request); 
				orderItem.setViewSpec("OrderItemsView");
				orderItem.setAction(ActionBean.INSERT);				
				for(OrderItem oi: qv.OrderTemplate.getOrderItems()) { 
				//all these will be inserts 
					orderItem.clear();
					oi.setOrderID(order.getOrderID());
					BeanFactory.setTableDataFromOrderItem(oi, orderItem);
					orderItem.setToNewID("OrderItemID");
					orderItem.doAction(GenerationKeys.INSERT);
					if(!orderItem.isSuccessful()) {
						throw new RuntimeException("Failed inserting order item : " + orderItem.getError());
					}
					oi.setOrderItemID(orderItem.getString("OrderItemID"));
				} 
			}
		}
		if(packageID != null) {
			addPackageCosts(request, order.getOrderID(), order.getQuoteViewID(), packageID);
		}
		
		/* add location */
		List<ContactLocation> locList = this.getOrderLocations(request, orderID);
		if(!locList.isEmpty()) {
			GenRow loc = new GenRow();
			loc.setRequest(request);
			loc.setViewSpec("ContactLocationView");
			loc.setParameter("OrderID", order.getOrderID());
			loc.setParameter("Name", "Temporary Location");
			loc.setParameter("CreatedBy", userID);
			loc.setParameter("CreatedDate", "NOW");
			
			for(ContactLocation cl: locList) {
				loc.setToNewID("ContactLocationID");
				try {
				loc.doAction(GenerationKeys.INSERT);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				cl.OrderID = order.getOrderID();
				cl.AddressID = null;
				cl.LocationID = null;
				cl.ContactID = null;
				cl.ContactLocationID = loc.getParameter("ContactLocationID");
				OrderResource.getInstance().updateOrderLocation(request, order.getOrderID(), cl.ContactLocationID, cl);
			}
		}
		
		/* add profiles */
		GenRow profile = new GenRow();
		try { 
			profile.setRequest(request);
			profile.setViewSpec("answers/OrderAnswerView");
			profile.setParameter("OrderID", orderID);
			
			profile.doAction("search");
			logger.debug(profile.getStatement());
			
			profile.getResults();
			
			if(profile.getNext()) {
				GenRow worker = new GenRow();
				worker.setRequest(request);
				worker.setViewSpec("answers/OrderAnswerView");
				do {
					worker.clear();
					worker.putAllDataAsParameters(profile);
					//logger.debug(profile.getData("QuestionID"));
					logger.error(worker.toString());
					worker.setParameter("QuestionID", profile.getData("QuestionID"));
					
					worker.setParameter("OrderID", order.getOrderID());
					worker.setToNewID("AnswerID");
					worker.setParameter("CreatedDate", "NOW");
					worker.setParameter("ModifiedDate", "NOW");
					worker.setParameter("CreatedBy", order.getCreatedBy());
					worker.setParameter("ModifiedBy", order.getCreatedBy());
					
					worker.doAction(GenerationKeys.INSERT);
				} while(profile.getNext());
			}
		} finally {
			profile.close();
		}
		
		return order;
	}
	

	GenRow getOrderContactRow(Connection conn, String orderContactID) {
		final GenRow template = new GenRow();
		template.setConnection(conn);
		template.setViewSpec("OrderContactListView");
		template.setParameter("OrderContactID", orderContactID);
		
		if(!template.isSet("OrderContactID")) {
			throw new IllegalArgumentException("No OrderContactID given");
		}
		template.doAction(GenerationKeys.SELECT);
		
		if(!template.isSuccessful()) {
			throw new NotFoundException("The order contact was not found : " + orderContactID);
		}
		return template;
	}
	
	private Map<String, Order> orderTemplateCache = Collections.synchronizedMap(new HashMap<String, Order>());
	public Order getCachedTemplate(String orderTemplateID) {
		// * Disabled as we're making database-side changes in development. 
		if(orderTemplateCache.containsKey(orderTemplateID)) {
			logger.trace("cache hit {}", orderTemplateID);
			return orderTemplateCache.get(orderTemplateID);
		}
		logger.trace("cache miss {}", orderTemplateID);
		
		return null;
	}
	
	public void clearTemplateCache() {
		logger.debug("clearTemplateCache()");
		orderTemplateCache.clear();
	}
	
	public void setCachedTemplate(String orderTemplateID, Order order) {
		if(order == null) {
			logger.debug("templateCache.remove({})", orderTemplateID);
			orderTemplateCache.remove(orderTemplateID);
		} else {
			logger.debug("templateCache.put({}, order)", orderTemplateID);
			orderTemplateCache.put(orderTemplateID, order);
		} 
	}
	
	public Order getOrderTemplate(final Connection conn, final UserBean user, String orderTemplateID) {
		logger.trace("getOrderTemplate(Conn, {}, {})", user != null ? user.getName() : "null user", orderTemplateID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		if(StringUtils.isBlank(orderTemplateID)) {
			throw new IllegalArgumentException("the order template id was blank");
		}
		Order order = getCachedTemplate(orderTemplateID);
		if(order == null) {
			order = getOrderTemplate(conn, orderTemplateID);
			setCachedTemplate(orderTemplateID, order);
		}
		return order;
	}
	
	private Order getOrderTemplate(Connection conn, String orderTemplateID) {
		final GenRow template = getOrderTemplateRow(conn, orderTemplateID);
		final Order order = BeanFactory.getOrderFromTableData(template);
		final GenRow items = new GenRow();
		items.setConnection(conn);
		items.setViewSpec("OrderTemplateItemsView");
		items.setParameter("OrderTemplateID", orderTemplateID);

		try {
			items.doAction("search");
			logger.debug(items.getStatement());
			
			items.getResults(true);
			
			if(items.getNext()) {
				OrderItem[] orderItems = new OrderItem[items.getSize()];
				do {
					logger.debug(items.getData("Category"));
					
					// rowIndex is 1 based
					orderItems[items.getRowIndex()-1] = BeanFactory.getOrderItemFromTableData(items);
				} while (items.getNext());
				order.setOrderItems(orderItems);
			}
		} finally {
			items.close();	
		}
		return order; 
	}
	

	public Order persistOrderTemplate(final HttpServletRequest request, Order orderTemplate) {
		logger.trace("saveOrderTemplate(req, {})", orderTemplate);
		return persistOrderTemplate(ActionBean.getConnection(request), us.getCurrentUser(request), orderTemplate);
	}
	
	/**
	 * Save or Update an Order Template based on the Order given. 
	 * @return Order with OrderTemplateID set, if it wasn't already. 
	 */
	public Order persistOrderTemplate(final Connection conn, final UserBean user, Order order) {
		if(logger.isTraceEnabled()) {
			logger.trace("saveOrderTemplate(conn, {}, {})", user != null ? user.getName() : "null user", order);
		}
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		if(!user.canAdmin()) {
			throw new AccessException("You must be an admin to update order templates");
		}
		final boolean update = StringUtils.isNotBlank(order.getOrderTemplateID()); 
		final GenRow g = update ? getOrderTemplateRow(conn, order.getOrderTemplateID()) : new GenRow(); 
		
		if(update) {
			g.setAction(GenerationKeys.UPDATE);
		} else {
			g.setConnection(conn);
			g.setViewSpec("OrderTemplateView");
			order.setOrderTemplateID(KeyMaker.generate());
			g.setAction(GenerationKeys.INSERT);
			
			g.setParameter("TemplateName","Generated Order Template");
			g.setParameter("CreatedDate", "NOW");
			g.setParameter("CreatedBy", user.getUserID());
			g.setParameter("GroupID", user.getString("DefaultGroupID"));
		}

		BeanFactory.setTableDataFromOrder(order, g);
		g.setParameter("ModifiedDate", "NOW");
		g.setParameter("ModifiedBy", user.getUserID());
		
		activityLogger.logActivity(user.getUser(), conn, "OrderTemplates", order.getOrderTemplateID(),g.getAction());
		g.doAction();
		
		if(order.getOrderItems() != null) {
			GenRow deleteRow = new GenRow();
			deleteRow.setConnection(conn);
			deleteRow.setViewSpec("OrderTemplateItemsView");
			deleteRow.setParameter("ON-OrderTemplateID", order.getOrderTemplateID());
			int i = 0;
			for(OrderItem oi: order.getOrderItems()) {
				g.clear();
				g.setViewSpec("OrderTemplateItemsView");
				g.setParameter("OrderTemplateID", order.getOrderTemplateID());
				
				if(StringUtils.isBlank(oi.getOrderTemplateItemID())) {
					oi.setOrderTemplateItemID(KeyMaker.generate());
					g.setAction(GenerationKeys.INSERT);
				} else {
					g.setAction(GenerationKeys.UPDATE);
				}
				deleteRow.setParameter("ON-OrderTemplateItemID|"+(++i), "!" + oi.getOrderTemplateItemID());
				// this will set OrderTemplateItemID
				BeanFactory.setTableDataFromOrderItem(oi, g);
				g.doAction();
			}
			//delete's all items that were not found in the update.
			if (order.getOrderTemplateID() != null && order.getOrderTemplateID().length() > 0) deleteRow.doAction(GenerationKeys.DELETEALL);
			logger.error("persistOrderTemplate row {}",deleteRow.getStatement());
		} 
		//set the template in cache.
		this.setCachedTemplate(order.getOrderTemplateID(), order);
		return order;
	}
	
	public Order getOrderTemplate(final HttpServletRequest request, String orderTemplateID) {
		logger.trace("getOrderTemplate(Request, {})", orderTemplateID);
		return getOrderTemplate(ActionBean.getConnection(request), us.getCurrentUser(request), orderTemplateID);
	}
	
	private GenRow getOrderTemplateRow(Connection conn, String orderTemplateID) {
		final GenRow template = new GenRow();
		template.setConnection(conn);
		template.setViewSpec("OrderTemplateView");
		template.setParameter("OrderTemplateID", orderTemplateID);
		template.doAction(GenerationKeys.SELECT);
		
		if(!template.isSuccessful()) {
			logger.error(template.toString());
			logger.error(template.getError());
			template.doAction("search");
			logger.error(template.getStatement());
			
			throw new NotFoundException("The quote template was not found : " + orderTemplateID);
		}
		return template;
	}
	
	public List<ContactLocation> getOrderLocations(Connection con, UserBean user, String orderID) {
		
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		GenRow orderRow = this.getOrderRow(con, orderID);
		if(!user.canView(orderRow)) {
			throw new AccessException("You do not have permission to view this order");
		}
		GenRow locations = new GenRow();
		try {
			locations.setConnection(con);
			locations.setViewSpec("ContactLocationView");
			locations.setParameter("OrderID", orderID);
			locations.getResults(true);
			
			if(locations.getNext()) {
				List<ContactLocation> list = new java.util.ArrayList<ContactLocation>(locations.getSize());
				do {
					ContactLocation orderLocation = new ContactLocation(locations);
					
					logger.debug("AddressID {}", orderLocation.AddressID);
					logger.debug("StreetNumber {}", orderLocation.StreetNumber);
					logger.debug("Street {}", orderLocation.Street);
					logger.debug("City {}", orderLocation.City);
					logger.debug("Postcode {}", orderLocation.Postcode);
					logger.debug("State {}", orderLocation.State);
					logger.debug("Country {}", orderLocation.Country);
					
					list.add(orderLocation);
				} while(locations.getNext());
				return list;
			}
			return java.util.Collections.emptyList();
			
		} finally {
			locations.close();
		}
	}
	
	public List<ContactLocation> getOrderLocations(HttpServletRequest request, String orderID) {
		return getOrderLocations(ActionBean.getConnection(request), us.getCurrentUser(request), orderID);
	}

	public List<com.sok.runway.externalInterface.beans.Contact> getLinkedContacts(String contactID) {
		
		List<com.sok.runway.externalInterface.beans.Contact> contactsList = new java.util.ArrayList<com.sok.runway.externalInterface.beans.Contact>();
		GenRow linkedContacts = new GenRow();
		try {
	        linkedContacts.setViewSpec("LinkeeContactListView");
	        linkedContacts.setColumn("LinkerID",contactID);
	        linkedContacts.sortBy("LinkeeContact.FirstName",0);
	        linkedContacts.sortBy("LinkeeContact.LastName",1);
	        linkedContacts.getResults();
	        
	        while(linkedContacts.getNext())
	        {
	        	com.sok.runway.externalInterface.beans.Contact contact = new com.sok.runway.externalInterface.beans.Contact();
	        	contact.setFirstName(linkedContacts.getString("FirstName"));
	        	contact.setLastName(linkedContacts.getString("LastName"));
	        	contact.setType(linkedContacts.getString("LinkType"));
	        	contact.setCompany(linkedContacts.getString("Company"));
	        	contact.setPhone(linkedContacts.getString("Phone"));
	        	contact.setEmail(linkedContacts.getString("Email"));
	        	
	        	Address address = new Address();
	        	address.setStreet(linkedContacts.getString("Street"));
	        	address.setStreet2(linkedContacts.getString("Street2"));
	        	address.setCity(linkedContacts.getString("City"));	        	
	        	address.setState(linkedContacts.getString("State"));
	        	address.setPostcode(linkedContacts.getString("Postcode"));
	        	
	        	contact.setPhysicalAddress(address);
	        	contactsList.add(contact);
	        }
		} 
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return contactsList;
	}

}
