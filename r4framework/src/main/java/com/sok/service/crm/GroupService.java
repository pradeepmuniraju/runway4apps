package com.sok.service.crm;

import com.sok.framework.GenRow;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class GroupService {
	private static final Logger logger = LoggerFactory.getLogger(GroupService.class);
	private static final GroupService service = new GroupService();
	private static final UserService us =  UserService.getInstance();
	
	public static GroupService getInstance() {
		return service;
	}
	
	
	public List<User> getGroupUsers(HttpServletRequest request, String GroupID) {
		logger.debug("GroupService.getGroupUsers({})", GroupID);
		
		List<User> groupUsers =  new ArrayList<User>();
		
		GenRow linkeduserlist = new GenRow();
	   	linkeduserlist.setViewSpec("UserSelectView");
	   	linkeduserlist.setRequest(request);
	    linkeduserlist.setParameter("UserLinkedUser.LinkedGroupID|1",GroupID);
	    linkeduserlist.setParameter("Status","Active");	    
	   	linkeduserlist.setParameter("-sort1","FirstName");
	   	linkeduserlist.setParameter("-sort2","LastName");
	   	us.getCurrentUser(request).setConstraint(linkeduserlist,"LinkedUserUser.UserGroups.UserGroup");
	   	linkeduserlist.doAction("search");
	   	linkeduserlist.getResults();
	   	while(linkeduserlist.getNext()){ 
	   		User u = new User();	   		
	   		BeanFactory.setUserFromTableData(u, linkeduserlist);
	   		groupUsers.add(u);
	   	}
	   	
		linkeduserlist.close();
		
		if(logger.isDebugEnabled())
			logger.debug(linkeduserlist.getStatement());
		
		return groupUsers;
	}
	
}
