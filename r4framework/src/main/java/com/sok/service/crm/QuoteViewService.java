package com.sok.service.crm;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.externalInterface.beans.Order;
import com.sok.runway.externalInterface.beans.QuoteView;
import com.sok.runway.externalInterface.beans.QuoteViewPage;
import com.sok.runway.externalInterface.beans.shared.OrderItem;
import com.sok.runway.offline.ThreadManager;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuoteViewService {
	private static final QuoteViewService service = new QuoteViewService();
	private static final OrderService os = new OrderService();
	private static final Logger logger = LoggerFactory.getLogger(QuoteViewService.class);
	private static final UserService us = UserService.getInstance(); 
	private static final ActivityLogger activityLogger = ActivityLogger.getActivityLogger();
	public static QuoteViewService getInstance() {
		return service;
	}
	
	/* I should note that the id's aren't tied into Runway anywhere, it just helps in keeping quote templates portable if they're the same */
	private final String[][] types = {
			{"0F1C3T3M508X8Z8P1A3K3E7V491Y",	"Cover Page"},
			{"0M153C3P518O8K8N173Y387S4123",	"Document"},
			{"011A353M5N8E8A8B1A3Z3Q7L4222",	"DRIP"},
			{"0Z1N3B3N598D868M1Q353C7Q443K",	"Product"},
			{"061J383I52888A8J1L373F7F4W3F",	"Profile"},
			{"0M15333M5S8L8W881S3K3Y7H464E",	"VPB"},
			{"0U1T3Q375U8B86831S3R3E7F4F4C",	"Custom"},
			{"0P1D3F305U8O8A8G1W35317A4J4F",	"Open"},
			{"0E1W394W7F9H288V8Y959X7D710M",	"Floorplan"},
			{"0B1R3K5K511Q9N3S1H782O0L955Y",    "Summary"}
	};
	
	public QuoteViewService() {
		//TODO - tie this into the TableSpecs
		ThreadManager.startThread(new Runnable() {
			public void run() {
				logger.info("Checking for new QuoteViewPageTypes");
				GenRow check = new GenRow();
				String sysUserID = InitServlet.getSystemParam("SystemUserID", "F180D2D08404F1241723606192C82747");
				try { 
					ActionBean.connect(check);
					check.setViewSpec("quoteviews/QuoteViewPageTypeView");
					for(String[] t: types) {
						check.setParameter("QuoteViewPageTypeID", t[0]);	
						check.doAction(GenerationKeys.SELECT);
						
						if(!check.isSuccessful()) {
							check.remove("QuoteViewPageTypeID");
							check.setParameter("TypeName", t[1]);
							check.doAction(GenerationKeys.SELECTFIRST);
							
							if(!check.isSuccessful()) {
								check.setParameter("QuoteViewPageTypeID", t[0]);	
								check.setParameter("ModifiedDate","NOW");
								check.setParameter("CreatedDate","NOW");
								check.setParameter("ModifiedBy",sysUserID);
								check.setParameter("CreatedBy",sysUserID);
								check.setParameter("DefaultOptions","[]");	//TODO, we never did use these. Perhaps they should go..
								check.setParameter("PreviewOptions","[]");
								check.doAction(GenerationKeys.INSERT);
								
								logger.info("Added new QuoteViewPageType for {}", t[1]);
							}
						}
					}
				} catch (Exception e) {
					logger.error("Error creating / updating default data for QuoteViews", e);
				} finally {
					check.close();
				}
			}
		}, "QuoteViewDataCheck");
	}
	
	public QuoteView getQuoteView(final HttpServletRequest request, String quoteViewID) {
		logger.debug("getQuoteView(Request, {})", quoteViewID);
		return getQuoteView(ActionBean.getConnection(request), us.getCurrentUser(request), quoteViewID);
	} 
	
	public QuoteView getQuoteView(final Connection conn, final UserBean user, String quoteViewID) {
		logger.debug("getQuoteView(Conn, {}, {})", user != null ? user.getName() : "null user", quoteViewID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		if(StringUtils.isBlank(quoteViewID)) {
			throw new IllegalArgumentException("QuoteViewID was null or empty");
		}
		final GenRow templates = new GenRow();
		templates.setConnection(conn);
		templates.setViewSpec("quoteviews/QuoteViewView");
		templates.setParameter("QuoteViewID", quoteViewID);
		templates.setParameter("-sort1", "Active");
		templates.setParameter("-sort2","ViewName");
		templates.doAction(GenerationKeys.SELECT);
		
		if(!templates.isSuccessful()) {
			throw new NotFoundException("The Quote Template could not be found : " + quoteViewID);
		}
		final QuoteView qv = new QuoteView(templates);
		if(StringUtils.isNotBlank(templates.getString("OrderTemplateID"))) {
			qv.OrderTemplate = os.getOrderTemplate(conn, user, templates.getString("OrderTemplateID"));
		}
		return qv;
	}
	
	public List<QuoteView> getQuoteViews(final HttpServletRequest request) {
		logger.debug("getQuoteViews(request)");
		return getQuoteViews(ActionBean.getConnection(request), us.getCurrentUser(request));
	} 
	
	public List<QuoteView> getQuoteViews(final Connection conn, final UserBean user) {
		logger.debug("getQuoteViews(Conn, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		final GenRow templates = new GenRow();
		try { 
			templates.setConnection(conn);
			templates.setViewSpec("quoteviews/QuoteViewView");
			templates.setParameter("-sort1", "Active");
			templates.setParameter("-sort2","ViewName");
			templates.getResults(true);
			if(templates.getNext()) {
				final List<QuoteView> list = new ArrayList<QuoteView>();
				do 
				{
					QuoteView qv = new QuoteView(templates);
					if(StringUtils.isNotBlank(templates.getString("OrderTemplateID"))) {
						qv.OrderTemplate = os.getOrderTemplate(conn, user, templates.getString("OrderTemplateID"));
					}
					list.add(qv);
				} while(templates.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			templates.close();
		}	
	}
	
	public List<QuoteViewPage> getQuoteViewPages(final HttpServletRequest request, final String quoteViewID) {
		logger.debug("getQuoteViewPages(request, {})", quoteViewID);
		return getQuoteViewPages(ActionBean.getConnection(request), us.getCurrentUser(request), quoteViewID);
	} 
	
	public List<QuoteViewPage> getQuoteViewPages(final Connection conn, final UserBean user, final String quoteViewID) {
		logger.debug("getQuoteViewPages(Conn, {}, {})", user != null ? user.getName() : "null user", quoteViewID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		final GenRow g = new GenRow();
		try {
			g.setConnection(conn);
			g.setViewSpec("quoteviews/QuoteViewPageView");
			g.setParameter("QuoteViewID", quoteViewID);
			g.setParameter("-sort1", "SortOrder");
			g.getResults(true);
			if(g.getNext()) {
				final List<QuoteViewPage> list = new ArrayList<QuoteViewPage>(g.getSize());
				do 
				{
					list.add(new QuoteViewPage(g));
				} while(g.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			g.close();
		}
	}
	
	public QuoteViewPage saveQuoteViewPage(HttpServletRequest request, QuoteViewPage quoteViewPage) {
		return saveQuoteViewPage(ActionBean.getConnection(request), us.getCurrentUser(request), quoteViewPage);
	}
	
	public QuoteViewPage saveQuoteViewPage(Connection con, UserBean user, QuoteViewPage quoteViewPage) {
		
		final GenRow templates = new GenRow();
		final GenRow g = new GenRow();
		try { 
			templates.setConnection(con);
			templates.setViewSpec("quoteviews/QuoteViewView");
			templates.setParameter("QuoteViewID", quoteViewPage.QuoteViewID);
			templates.doAction(GenerationKeys.SELECT);
			
			if(!templates.isSuccessful()) {
				throw new NotFoundException("QuoteView not found, could not insert page. ID: " + quoteViewPage.QuoteViewID);
			}
			g.setViewSpec("quoteviews/QuoteViewPageView");
			g.setToNewID("QuoteViewPageID");
			g.setParameter("QuoteViewID", quoteViewPage.QuoteViewID);
			g.setParameter("QuoteViewPageTypeID", quoteViewPage.QuoteViewPageTypeID);
			g.setParameter("PageName", quoteViewPage.PageName);
			g.setParameter("SortOrder", quoteViewPage.SortOrder);
			g.setParameter("Layout", quoteViewPage.Layout);
			g.setParameter("Active", quoteViewPage.Active ? "Y" : "N");
			g.setParameter("DisplayScreen", quoteViewPage.DisplayScreen ? "Y" : "N");
			g.setParameter("DisplayPDF", quoteViewPage.DisplayPDF ? "Y" : "N");
			g.setParameter("DisplayKDR", quoteViewPage.DisplayKDR ? "Y" : "N");
			g.setParameter("DisplayOJ", quoteViewPage.DisplayOJ ? "Y" : "N");
			g.setParameter("DisplayHNL", quoteViewPage.DisplayHNL ? "Y" : "N");
			
			g.setParameter("Conditions", quoteViewPage.Conditions);
			
			g.doAction(GenerationKeys.INSERT);
			if(!g.isSuccessful()) {
				throw new RuntimeException("Failed to insert data " + g.getError());
			}
			quoteViewPage.QuoteViewPageID = g.getString("QuoteViewPageID");
			activityLogger.logActivity(user.getUser(), con, "QuoteView", quoteViewPage.QuoteViewID, ActivityLogger.ActionType.Updated.name());
			activityLogger.logActivity(user.getUser(), con, "QuoteViewPage", quoteViewPage.QuoteViewPageID, ActivityLogger.ActionType.Inserted.name());
			
			return quoteViewPage;		
		} finally {
			templates.close();
			g.close();
		}
	}
	
	public QuoteViewPage updateQuoteViewPage(HttpServletRequest request, QuoteViewPage quoteViewPage) {
		return updateQuoteViewPage(ActionBean.getConnection(request), us.getCurrentUser(request), quoteViewPage);
	}
	
	public QuoteViewPage updateQuoteViewPage(Connection con, UserBean user, QuoteViewPage quoteViewPage) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		if(!user.canAdmin()) {
			throw new AccessException("You do not have persmission to modify quote views");
		}
		if(quoteViewPage == null) {
			throw new IllegalArgumentException("QuoteViewPage not provided for action update");
		}
		if(StringUtils.isBlank(quoteViewPage.QuoteViewPageID)) {
			throw new IllegalArgumentException("QuoteViewPageID not populated for action update");
		}
		
		final GenRow templates = new GenRow();
		try { 
		
			templates.setConnection(con);
			templates.setViewSpec("quoteviews/QuoteViewPageView");
			templates.setParameter("QuoteViewID", quoteViewPage.QuoteViewID);
			templates.setParameter("QuoteViewPageID", quoteViewPage.QuoteViewPageID);
			templates.doAction(GenerationKeys.SELECT);
			if(templates.isSuccessful()) {
				templates.setParameter("PageName", quoteViewPage.PageName);
				templates.setParameter("SortOrder",quoteViewPage.SortOrder);
				templates.setParameter("Layout",quoteViewPage.Layout);
				templates.setParameter("Active",quoteViewPage.Active ? "Y" : "N");
				
				templates.setParameter("DisplayScreen",quoteViewPage.DisplayScreen ? "Y" : "N");
				templates.setParameter("DisplayPDF",quoteViewPage.DisplayPDF ? "Y" : "N");
				templates.setParameter("DisplayKDR", quoteViewPage.DisplayKDR ? "Y" : "N");
				templates.setParameter("DisplayOJ", quoteViewPage.DisplayOJ ? "Y" : "N");
				templates.setParameter("DisplayHNL", quoteViewPage.DisplayHNL ? "Y" : "N");
				templates.setParameter("SelectedOptions", quoteViewPage.SelectedOptions.toString());

				templates.setParameter("Conditions", quoteViewPage.Conditions);

				templates.setParameter("ModifiedDate", "NOW");
				templates.setParameter("ModifiedBy", user.getUserID());
				
				templates.doAction(GenerationKeys.UPDATE);
				
				if(!templates.isSuccessful()) {
					throw new RuntimeException("failed to save data");
				}
				activityLogger.logActivity(user.getUser(), con, "QuoteView", quoteViewPage.QuoteViewID, ActivityLogger.ActionType.Updated.name());
				activityLogger.logActivity(user.getUser(), con, "QuoteViewPage", quoteViewPage.QuoteViewPageID, ActivityLogger.ActionType.Updated.name());
				
				return quoteViewPage;
			} else {
				throw new NotFoundException("QuoteViewPage not found for ID: " + quoteViewPage.QuoteViewPageID);
			}
		} finally {
			templates.close();
		}
	} 
	
	/* 
	 * create order from quote view and product - like is done on the client side. 
	public Order createOrder(final HttpServletRequest request, String quoteViewID, String productID) {
		
		
		
		return null;
	}
	
	public Order createOrder(final Connection conn, final UserBean user, final quoteViewID, final String productID) {
		
		
		
		return null;
	}
	*/
	
	public QuoteView updateQuoteView(final HttpServletRequest request, QuoteView view) {
		return updateQuoteView(ActionBean.getConnection(request), us.getCurrentUser(request), view);
	}
	
	public QuoteView updateQuoteView(final Connection conn, final UserBean user, final QuoteView view) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		if(!user.canAdmin()) {
			throw new AccessException("You do not have persmission to modify quote views");
		}
		if(StringUtils.isBlank(view.QuoteViewID)) {
			throw new IllegalArgumentException("QuoteViewID not provided for action update");
		}
		final GenRow templates = getQuoteViewRow(conn, view.QuoteViewID);
		
		logger.debug("saving...");
		logger.debug(view.toString());
		
		if(view.OrderTemplate != null) {
			os.persistOrderTemplate(conn, user, view.OrderTemplate);
			//whatever it's state, lets keep things simple.
			if(view.OrderTemplate.getOrderTemplateID() != null) {
				templates.setParameter("OrderTemplateID", view.OrderTemplate.getOrderTemplateID());
			}
		}
		templates.setParameter("ModifiedBy", user.getUserID());
		templates.setParameter("ModifiedDate","NOW");
		templates.setParameter("Active", view.Active ? "Y" : "N");
		templates.setParameter("ViewName",view.ViewName);
		templates.setParameter("Description", view.Description);
		templates.setParameter("Expiry", view.Expiry);
		templates.doAction(GenerationKeys.UPDATE);
		
		return view;
	}
	
	public QuoteView duplicateQuoteView(final HttpServletRequest request, String quoteViewID) {
		final QuoteView qv = getQuoteView(request, quoteViewID);
		qv.QuoteViewID = null;
		Order o = qv.OrderTemplate;
		qv.ViewName = "Copy of " + qv.ViewName;
		o.setOrderID(null);
		o.setOrderTemplateID(null);
		if(o.getOrderItems() != null) {
			for(OrderItem oi: o.getOrderItems()) {
				oi.setOrderID(null);
				oi.setOrderTemplateItemID(null);
			}
		}
		
		logger.debug("order template id to be updated should be null {}", qv.OrderTemplate.getOrderTemplateID());
		final QuoteView ret = saveQuoteView(request, qv);
		List<QuoteViewPage> qvpList = this.getQuoteViewPages(request, quoteViewID);
		for(QuoteViewPage qvp: qvpList) {
			qvp.QuoteViewID = ret.QuoteViewID;
			qvp.QuoteViewPageID = null;
			saveQuoteViewPage(request, qvp);	
		}
		return ret;
	}
	
	private GenRow getQuoteViewRow(final Connection conn, final String quoteViewID) {
		final GenRow templates = new GenRow();
		templates.setConnection(conn);
		templates.setViewSpec("quoteviews/QuoteViewView");
		templates.setParameter("QuoteViewID", quoteViewID);
		templates.setParameter("-sort1", "Active");
		templates.setParameter("-sort2","ViewName");
		templates.doAction(GenerationKeys.SELECT);
		
		if(!templates.isSuccessful()) {
			throw new NotFoundException("Could not find QuoteView with id: " + quoteViewID);
		}
		return templates;
	}
	
	public QuoteView saveQuoteView(final HttpServletRequest request, QuoteView view) {
		return saveQuoteView(ActionBean.getConnection(request), us.getCurrentUser(request), view);
	}
	
	public QuoteView saveQuoteView(final Connection conn, final UserBean user, final QuoteView view) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		if(!user.canAdmin()) {
			throw new AccessException("You do not have permission to modify quote views");
		}
		final GenRow templates = new GenRow();
		
		templates.setConnection(conn);
		templates.setViewSpec("quoteviews/QuoteViewView");
		templates.setToNewID("QuoteViewID");
		templates.setParameter("Active", view.Active ? "Y" : "N");
		templates.setParameter("ViewName",view.ViewName);
		templates.setParameter("Description", view.Description);
		templates.setParameter("Expiry", view.Expiry);
		templates.setParameter("ModifiedBy", user.getUserID());
		templates.setParameter("ModifiedDate","NOW");
		templates.setParameter("CreatedBy", user.getUserID());
		templates.setParameter("CreatedDate","NOW");
		
		if(view.OrderTemplate == null) {
			/* if there isn't one, then create one */
			view.OrderTemplate = new Order();
			view.OrderTemplate.setName("Generated Order Template");
		}
		os.persistOrderTemplate(conn, user, view.OrderTemplate);
		//whatever it's state, lets keep things simple.
		if(view.OrderTemplate.getOrderTemplateID() != null) {
			templates.setParameter("OrderTemplateID", view.OrderTemplate.getOrderTemplateID());
		}
		
		templates.doAction(GenerationKeys.INSERT);
		if(!templates.isSuccessful()) {
			logger.debug("wasn't able to insert " + templates.getError());
			throw new RuntimeException(templates.getError());
		}
		activityLogger.logActivity(user.getUser(), templates.getConnection(), "QuoteView", templates.getString("QuoteViewID"),templates.getAction());
		view.QuoteViewID = templates.getString("QuoteViewID");
			
		return view;
	}
	
	public void deleteQuoteView(final HttpServletRequest request, String quoteViewID) {
		deleteQuoteView(ActionBean.getConnection(request), us.getCurrentUser(request), quoteViewID);
	}
	
	public void deleteQuoteView(final Connection conn, final UserBean user, final String quoteViewID) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Orders")) {
			throw new AccessException("You do not have permission to access the order module");
		}
		if(!user.canAdmin()) {
			throw new AccessException("You do not have permission to modify quote views");
		}
		try {
			GenRow templates = getQuoteViewRow(conn, quoteViewID);
			final String orderTemplateID = templates.isSet("OrderTemplateID") ? templates.getString("OrderTemplateID") : null;
			templates.doAction(GenerationKeys.DELETE);
			logger.debug("Deleted quote view {}", quoteViewID);
			activityLogger.logActivity(user.getUser(), templates.getConnection(), "QuoteView", quoteViewID,templates.getAction());
			templates.clear();
			templates.setViewSpec("quoteviews/QuoteViewPageView");
			templates.setParameter("ON-QuoteViewID", quoteViewID);
			templates.doAction(GenerationKeys.DELETEALL);
			
			if(orderTemplateID != null) {
				os.deleteOrderTemplate(conn, user, orderTemplateID);
			}
		} catch (NotFoundException nfe) {
			//this is fine, if it's already gone do not throw an exception
			logger.trace("Attempted to delete already deleted QuoteView {}", quoteViewID);
		}
	}
}
