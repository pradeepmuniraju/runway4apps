package com.sok.service.crm.cms.properties;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.cms.properties.Range;
import com.sok.runway.crm.cms.properties.entities.RangeEntity;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class RangeService {
	
	private static final Logger logger = LoggerFactory.getLogger(RangeService.class);
	private static final UserService userService = UserService.getInstance(); 
	private static final RangeService service = new RangeService();
	private RangeService() { }
	public static RangeService getInstance() {
		return service;
	}
	
	public List<Range> getRangesExternal(HttpServletRequest request, Date modifiedSince, boolean active) {

		return getRangesExternal(ActionBean.getConnection(request), userService.getCurrentUser(request), "", modifiedSince, active);
	}

	public List<Range> getRangesExternal(HttpServletRequest request, String brandID, Date modifiedSince, boolean active) {

		return getRangesExternal(ActionBean.getConnection(request), userService.getCurrentUser(request), brandID, modifiedSince, active);
	}

	private List<Range> getRangesExternal(final Connection con, final UserBean user, final String brandID, final Date modifiedSince, final boolean active) {
		logger.trace("getRangesExternal(Conn, {}, {}, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.Range rangeList = new com.sok.runway.crm.cms.properties.Range(con);
		if(modifiedSince != null) { 
			rangeList.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		if (StringUtils.isNotBlank(brandID)) rangeList.setParameter("ProductParentBrandRange.ProductVariationBrandRange.ProductID", brandID);
		
		rangeList.setParameter("-sort0", "Name");
		rangeList.setParameter("-select1","ProductID");
		if(active) {
			rangeList.setParameter("Active","Y");
		}
		try {
			rangeList.setConnection(con);
			rangeList.getResults(true);
			
			if(rangeList.getNext()) {
				List<Range> list = new ArrayList<Range>(rangeList.getSize());
				do {
					list.add(BeanFactory.external(rangeList));
				}while(rangeList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			rangeList.close();
		}
	}
	
	public Range getRangeExternal(HttpServletRequest request, String productId) {
		logger.debug("getRange(Request, {})", productId);
		return BeanFactory.external(getRange(ActionBean.getConnection(request), userService.getCurrentUser(request), productId));
	}
	
	private RangeEntity getRange(final Connection conn, final UserBean user, final String productId) {
		if(logger.isDebugEnabled()) logger.debug("getPlan(Conn,{}, {})", user != null ? user.getName() : "null user",  productId);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		RangeEntity p = new RangeEntity(conn, productId);

		if(!p.isLoaded()) {
			throw new NotFoundException("The requested range could not be found");
		}
		p.setCurrentUser(user.getUser());
		if(!p.canView()) {
			throw new AccessException("You do not have permission to view this range");
		}
		return p;
	}
	

}
