package com.sok.service.crm.cms.properties;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.Home;
import com.sok.runway.externalInterface.beans.cms.properties.HomeCheck;
import com.sok.runway.crm.cms.properties.Facade;
import com.sok.runway.crm.cms.properties.entities.HomeEntity;
import com.sok.runway.crm.cms.properties.entities.PlanEntity;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.ListIterator;

public class HomeService {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeService.class);
	private static final UserService userService = UserService.getInstance(); 
	private static final HomeService service = new HomeService();
	private HomeService() { }
	public static HomeService getInstance() {
		return service;
	}

	public List<Home> getHomesExternal(HttpServletRequest request, final Date modifiedSince, final boolean active) {
		GenRow search = new GenRow(); 
		search.setRequest(request);
		if(modifiedSince != null) { 
			search.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		if(active) {
			search.setParameter("Active","Y");
			search.setParameter("-active","true");
		}
		return getHomesExternal(ActionBean.getConnection(request), userService.getCurrentUser(request), search);
	} 
	
	public List<Home> getHomesExternal(HttpServletRequest request, GenRow search) {
		return getHomesExternal(ActionBean.getConnection(request), userService.getCurrentUser(request), search);
	}
	
	private List<Home> getHomesExternal(final Connection con, final UserBean user, GenRow search) {
		logger.trace("getHomesExternal(Conn, user={}, search={})", user != null ? user.getName() : "null user", search != null ? search.toString() : "null search");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.Home homeList = new com.sok.runway.crm.cms.properties.Home(con);
		
		if(search != null)
			homeList.putAll(search);
		else
			homeList.setParameter("Active","Y");
		
		homeList.setParameter("-select1","ProductID");

		try {
			// we need a know order for later
			homeList.setParameter("-groupby0", "ProductID");
			homeList.setConnection(con);
			homeList.getResults(true);
			
			if(logger.isTraceEnabled()) {
				homeList.doAction(GenerationKeys.SEARCH);
				logger.trace(homeList.getStatement());
			}
			
			if(homeList.getNext()) {
				List<Home> list = new ArrayList<Home>(homeList.getSize());
				do {
					list.add(BeanFactory.external(homeList));
				}while(homeList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			homeList.close();
		}
	}
	
	public List<HomeCheck> getHomesExternalCheck(HttpServletRequest request, GenRow search) {
		return getHomesExternalCheck(ActionBean.getConnection(request), userService.getCurrentUser(request), search);
	}
	
	private List<HomeCheck> getHomesExternalCheck(final Connection con, final UserBean user, GenRow search) {
		logger.trace("getHomesExternal(Conn, user={}, search={})", user != null ? user.getName() : "null user", search != null ? search.toString() : "null search");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.Home homeList = new com.sok.runway.crm.cms.properties.Home(con);
		
		if(search != null)
			homeList.putAll(search);
		else
			homeList.setParameter("Active","Y");
		
		homeList.setParameter("-select1","ProductID");

		try {
			// we need a know order for later
			homeList.setParameter("-groupby0", "ProductID");
			homeList.setConnection(con);
			homeList.getResults(true);
			
			if(logger.isTraceEnabled()) {
				homeList.doAction(GenerationKeys.SEARCH);
				logger.trace(homeList.getStatement());
			}
			
			if(homeList.getNext()) {
				List<HomeCheck> list = new ArrayList<HomeCheck>(homeList.getSize());
				do {
					list.add(BeanFactory.externalCheck(homeList));
				}while(homeList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			homeList.close();
		}
	}
	
	public static class HomeList implements List<Home> {

		private final List<com.sok.runway.crm.cms.properties.Home> idList; 
		
		public HomeList(List<com.sok.runway.crm.cms.properties.Home> wrappedList) {
			idList = wrappedList; 
		}
		
		@Override
		public boolean add(Home arg0) {
			return false; 
		}

		@Override
		public void add(int arg0, Home arg1) {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public boolean addAll(Collection<? extends Home> arg0) {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public boolean addAll(int arg0, Collection<? extends Home> arg1) {
			throw new UnsupportedOperationException("Not implemented");
		}

		@Override
		public void clear() {
			idList.clear();
		}

		@Override
		public boolean contains(Object arg0) {
			if(arg0 == null) return false;
			if(arg0 instanceof com.sok.runway.crm.cms.properties.HouseLandPackage) return idList.contains(arg0);
			if(arg0 instanceof Home) {
				for(com.sok.runway.crm.cms.properties.Home hlp: idList) {
					if(hlp.getString("ProductID").equals(((Home)arg0).getHomeID())) {
						return true; 
					}
				}
			}
			return false;
		}

		@Override
		public boolean containsAll(Collection<?> arg0) {
			if(arg0 == null) return false;
			for(Object o: arg0) {
				if(!contains(o)) return false;
			}
			return false;
		}

		@Override
		public Home get(int arg0) {
			return BeanFactory.external(idList.get(arg0));
		}

		@Override
		public int indexOf(Object arg0) {
			if(arg0 == null) return -1;
			if(arg0 instanceof com.sok.runway.crm.cms.properties.HouseLandPackage) return idList.indexOf(arg0);
			int ix=0; 
			if(arg0 instanceof Home) {
				for(com.sok.runway.crm.cms.properties.Home hlp: idList) {
					if(hlp.getString("ProductID").equals(((Home)arg0).getHomeID())) {
						return ix; 
					}
					ix++;
				}
			}
			return -1;
		}

		@Override
		public boolean isEmpty() {
			return idList.isEmpty();
		}

		@Override
		public Iterator<Home> iterator() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int lastIndexOf(Object arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public ListIterator<Home> listIterator() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ListIterator<Home> listIterator(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean remove(Object arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public Home remove(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean removeAll(Collection<?> arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean retainAll(Collection<?> arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public Home set(int arg0, Home arg1) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int size() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public List<Home> subList(int arg0, int arg1) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object[] toArray() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <T> T[] toArray(T[] arg0) {
			// TODO Auto-generated method stub
			return null;
		} 
		
		
		
		
	}
	
	
	/* this does work, but not in use.
	private HouseLandPackageEntityList getHomes(final Connection con, final UserBean user, final Date modifiedSince, final boolean active) {
		
		logger.trace("getHomes(Conn, {}, {}, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in or authenticated");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow homeList = new GenRow();
		homeList.setTableSpec("Products");
		homeList.setParameter("ProductType", "House and Land");
		if(modifiedSince != null) { 
			homeList.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		homeList.setParameter("-sort0", "Name");
		homeList.setParameter("-select1","ProductID");
		if(active) {
			homeList.setParameter("Active","Y");
		}
		//security constraint ?
		try {
			homeList.setConnection(con);
			homeList.getResults(true);
			
			if(homeList.getNext()) {
				HouseLandPackageEntityList list = new HouseLandPackageEntityList(homeList.getSize());
				do {
					//check for can view ? 
					list.add(new HouseLandPackageEntity(homeList.getConnection(), homeList.getData("ProductID")));
				}while(homeList.getNext());
				return list;
			}
			return EMPTY_LIST;
		} finally {
			homeList.close();
		}
	}
	
	private static final HouseLandPackageEntityList EMPTY_LIST = new HouseLandPackageEntityList(0);
	public static class HouseLandPackageEntityList extends ArrayList<HouseLandPackageEntity> {
		private static final long serialVersionUID = 1L;
		public HouseLandPackageEntityList(int size) {
			super(size);
		}
		public List<HouseLandPackage> external() {
			if(isEmpty()) return Collections.emptyList();
			List<HouseLandPackage> list = new ArrayList<HouseLandPackage>(this.size());
			for(HouseLandPackageEntity hlpe: this) {
				list.add(hlpe.external());
			}
			return list;
		}
	}
	*/
	public Home getHomesExternal(HttpServletRequest request, String productId, BeanRepresentation repr) {
		logger.debug("getPackage(Request, {})", productId);
		return BeanFactory.external(getHome(ActionBean.getConnection(request), userService.getCurrentUser(request), productId), repr);
	}
	
	private HomeEntity getHome(final Connection conn, final UserBean user, final String productId) {
		if(logger.isDebugEnabled()) logger.debug("getPlan(Conn,{}, {})", user != null ? user.getName() : "null user",  productId);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		HomeEntity p = new HomeEntity(conn, productId);

		if(!p.isLoaded()) {
			throw new NotFoundException("The requested package could not be found");
		}
		p.setCurrentUser(user.getUser());
		if(!p.canView()) {
			throw new AccessException("You do not have permission to view this package");
		}
		
		
		
		
		/* TODO FUTURE 
		PackageCost pc = new PackageCost(conn);
		HLPackage pkg = new HLPackage(p, getQuoteViewID(p));
		try {
			pc.setParameter("-join1","ProductsLinkedProductProduct");	//inner join 
			pc.setParameter("ProductsLinkedProductProduct.ProductID", p.getString("ProductID"));
			pc.setParameter("ProductsLinkedProductProduct.Active", "Y");
			if(logger.isDebugEnabled()) { 
				pc.doAction(GenerationKeys.SEARCH);
				logger.debug(pc.getStatement());
			}
			pc.getResults(true);
			if(pc.getNext()) {
				List<HLPackageCost> costlist = new ArrayList<HLPackageCost>(pc.getSize());
				do {
					costlist.add(new HLPackageCost(pc));
				} while(pc.getNext());
				pkg.PackageCosts = costlist;
			}
		} finally {
			pc.close();
		}
		Facade f = new Facade(conn);
		f.setParameter("ProductID", p.getString("ProductID"));
		if(logger.isDebugEnabled()) { 
			f.doAction(GenerationKeys.SEARCH);
			logger.debug(f.getStatement());
		}
		f.doAction(GenerationKeys.SELECTFIRST);
		if(f.isSuccessful()) {
			pkg.FacadeID = f.getData("LinkedProductID");
		}
		*/
		return p;
	}
	public Home getPlanExternal(HttpServletRequest request, String productId, BeanRepresentation repr) {
		logger.debug("getPackage(Request, {})", productId);
		return BeanFactory.external(getPlan(ActionBean.getConnection(request), userService.getCurrentUser(request), productId), repr);
	}
	
	private HomeEntity getPlan(final Connection conn, final UserBean user, final String productId) {
		if(logger.isDebugEnabled()) logger.debug("getPlan(Conn,{}, {})", user != null ? user.getName() : "null user",  productId);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		PlanEntity p = new PlanEntity(conn, productId);

		if(!p.isLoaded()) {
			throw new NotFoundException("The requested plan could not be found");
		}
		p.setCurrentUser(user.getUser());
		if(!p.canView()) {
			throw new AccessException("You do not have permission to view this plan");
		}
		
		HomeEntity h = new HomeEntity(conn, p.getField("HomeProductID"));
		
		if(!h.isLoaded()) {
			throw new NotFoundException("The requested home for plan could not be found");
		}
		h.setCurrentUser(user.getUser());
		if(!h.canView()) {
			throw new AccessException("You do not have permission to view this home for plan");
		}
		
		h.setPlanEntity(p);
		
		return h;
	}
}
