package com.sok.service.crm.cms.properties;

import com.sok.framework.*;
import com.sok.framework.generation.DatabaseException;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.Product;
import com.sok.runway.crm.cms.properties.Brand;
import com.sok.runway.crm.cms.properties.Facade;
import com.sok.runway.crm.cms.properties.Home;
import com.sok.runway.crm.cms.properties.*;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.entities.RealEstateEntity;
import com.sok.runway.crm.cms.properties.publishing.PublishConstants;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.Contact;
import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.cms.properties.*;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.runway.externalInterface.beans.shared.ProductSecurityGroup;
import com.sok.runway.offline.rpmManager.RPMtask;
import com.sok.service.crm.ContactService;
import com.sok.service.crm.DocumentService;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Connection;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.*;

public class PlanService {
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class ExistingProperty {
		public String Name, ProductID, ProductType, ProductSubType, ProductNum, Description, PricingType, Image, ImagePath, ThumbnailImage, ThumbnailImagePath, Summary, PublishDescription, MarketingStatus, PublishingStatus, PublishingStatusIcon,
				Vendor, VendorDisplay, VendorPhone, VendorEmail, VendorType, SellingAgent, SellingAgentDisplay, SellingAgentPhone, SellingAgentEmail, PlanProductID, LotProductID, CurrentStatus, CurrentStatusID,
				SellingAgentContactID, SellingAgentContact, Developer, DeveloperCompany, Project, Stage, GroupID, LotNumber, HomeDesignName, UpdateByProcess, FloorPlan, RegionName, LocationName, RegionID, LocationID;
		
		public String Lot_ProductSubType, Lot_MarketingStatus, REAListingID;
		
		public double PlanTotalCost, LotTotalCost, TotalCost, DripCost, DripResult;
		
		public boolean Active, DisplayFlag;
		
		public long CreatedDate, ModifiedDate;
		public PersonName CreatedBy, ModifiedBy;
		
		public List<ProductDetail> ProductDetails = Collections.emptyList();
		public List<ProductSecurityGroup> ProductSecurityGroups = Collections.emptyList();
		public Address Address = null;
	}
	
	public static class Address {
		
		public String AddressID, StreetNumber, Street, Street2, City, State, Postcode, MapRef;
		
	}
	
	private static final PlanService service = new PlanService();
	private static final DocumentService documentService = DocumentService.getInstance();
	private static final Logger logger = LoggerFactory.getLogger(PlanService.class);
	private static final UserService us = UserService.getInstance(); 
	public static PlanService getInstance() {
		return service;
	}
	
	//do some validation on the service? 
	private final static String[] usedViews = {"properties/ProductExistingPropertyView", "properties/HomeDetails"};
	static {
		for(String s: usedViews) {
			try { 
				SpecManager.getViewSpec(s);
			} catch(IllegalArgumentException e) {
				SpecManager.getTableSpec(s);
			} catch(NullPointerException e) {
				logger.warn("ViewSpec {} did not exist, some functions will not work", s);
			}
		}
	}
	
	public Plan getPlan(final HttpServletRequest request, String planProductID) {
		logger.debug("getPlan(Request, {})", planProductID);
		return getPlan(ActionBean.getConnection(request), us.getCurrentUser(request), planProductID);
	}
	
	public Plan getPlan(final Connection con, final UserBean user, String planProductID) {
		if(logger.isDebugEnabled()) logger.debug("getPlan(Conn,{}, {})", user != null ? user.getName() : "null user",  planProductID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		
		final boolean random = "RANDOM".equals(planProductID); 
		final HomePlan p = random ? new HomePlan(con) : new HomePlan(con, planProductID);
		if(random) {
			p.setParameter("Active","Y");
			user.setConstraint(p, "ProductProductSecurityGroups");
			p.doAction(GenerationKeys.SELECTFIRST);
		}
		if(!p.isSuccessful()) {
			throw new NotFoundException(random ? "No home plans could be found, some must be created before using this feature" : "The requested plan could not be found");
		}
		if(!user.canView(p) && !user.canView(p, "GroupProductSecurityGroup")) {
			throw new AccessException("You do not have permission to view this home plan");
		}
		
		//need to retrieve potential floor plans 
		
		return new Plan(p, getQuoteViewID(p), getFacadeImageID(p));
	}
	
	private String getFacadeImageID(HomePlan hp) {
		GenRow facade = new GenRow();
		facade.setConnection(hp.getConnection());
		facade.setViewSpec("ProductLinkedProductView");
		facade.setColumn("ProductID", hp.getString("HomeProductID"));
		facade.setColumn("ProductProductsLinkedProduct.ProductType", "Home Facade");
		facade.setColumn("IsExclusive", "Y");
		facade.sortBy("ProductProductsLinkedProduct.Name", 0);
		
		// 1. Default facade image
		facade.doAction("selectfirst");
		
		// 2. First facade image in case no default is chosen
		if (!facade.isSuccessful()) {
			facade.remove("IsExclusive");
			facade.doAction("selectfirst");
		}
		
		//3. Hero image of the home, if there are no facades specified
		if (!facade.isSuccessful()) {
			facade = new GenRow();
			facade.setConnection(hp.getConnection());
			facade.setViewSpec("ProductView");
			facade.setColumn("ProductID", hp.getString("HomeProductID"));
			facade.doAction("selectfirst");
		}

		return facade.getString("Image");
	}
	
	public String getPlanQuoteViewID(HttpServletRequest request, String productID) {
		return getQuoteViewID(new HomePlan(request, productID));
	}
	
	public String getQuoteViewID(HomePlan hp) {
		
		final GenRow quoteViews = new GenRow();
		try {
			quoteViews.setConnection(hp.getConnection());
			quoteViews.setViewSpec("quoteviews/ProductQuoteViewView");
			quoteViews.setParameter("-sort1","Expiry");
			quoteViews.setParameter("ProductID", hp.getString("ProductID"));
			quoteViews.setParameter("Active","Y");
			quoteViews.setParameter("Expiry",">NOW");
			
			if(quoteViews.isSet("ProductID")) {
				quoteViews.doAction(GenerationKeys.SELECTFIRST);
				if(quoteViews.isSuccessful()) {
					return quoteViews.getData("QuoteViewID");
				}
			}
			Home h = hp.getHome();
			if(h == null) {
				return null;
			}
			quoteViews.setParameter("ProductID", h.getString("ProductID"));
			if(quoteViews.isSet("ProductID")) {
				quoteViews.doAction(GenerationKeys.SELECTFIRST);
				if(quoteViews.isSuccessful()) {
					return quoteViews.getData("QuoteViewID");
				}
			}
			HomeRange hr = h.getHomeRange();
			if(hr == null) {
				return null;
			}
			quoteViews.setParameter("ProductID", hr.getString("ProductID"));
			if(quoteViews.isSet("ProductID")) {
				quoteViews.doAction(GenerationKeys.SELECTFIRST);
				if(quoteViews.isSuccessful()) {
					return quoteViews.getData("QuoteViewID");
				}
			}
			
			Brand b = h.getHomeBrand();
			if(b == null) {
				return null;
			}
			quoteViews.setParameter("ProductID", b.getString("ProductID"));
			if(quoteViews.isSet("ProductID")) {
				quoteViews.doAction(GenerationKeys.SELECTFIRST);
				if(quoteViews.isSuccessful()) {
					return quoteViews.getData("QuoteViewID");
				}
			}
			
		} finally {
			quoteViews.close();
		}
		
		return null;
	}
	
	public String getPackageQuoteViewID(HttpServletRequest request, String productID) {
		return getQuoteViewID(new HouseLandPackage(request, productID));
	}
	
	public String getQuoteViewID(HouseLandPackage hlp) {
		
		final GenRow quoteViews = new GenRow();
		try {
			quoteViews.setConnection(hlp.getConnection());
			quoteViews.setViewSpec("quoteviews/ProductQuoteViewView");
			quoteViews.setParameter("-sort1","Expiry");
			quoteViews.setParameter("ProductID", hlp.getString("ProductID"));
			quoteViews.setParameter("Active","Y");
			quoteViews.setParameter("Expiry",">NOW");
			
			if(quoteViews.isSet("ProductID")) {
				quoteViews.doAction(GenerationKeys.SELECTFIRST);
				if(quoteViews.isSuccessful()) {
					return quoteViews.getData("QuoteViewID");
				}
			}
			
			HomePlan hp = hlp.getHomePlan();
			quoteViews.setParameter("ProductID", hp.getString("ProductID"));
			if(quoteViews.isSet("ProductID")) {
				quoteViews.doAction(GenerationKeys.SELECTFIRST);
				if(quoteViews.isSuccessful()) {
					return quoteViews.getData("QuoteViewID");
				}
			}
			
			Home h = hp.getHome();
			if(h == null) {
				return null;
			}
			quoteViews.setParameter("ProductID", h.getString("ProductID"));
			if(quoteViews.isSet("ProductID")) {
				quoteViews.doAction(GenerationKeys.SELECTFIRST);
				if(quoteViews.isSuccessful()) {
					return quoteViews.getData("QuoteViewID");
				}
			}
			HomeRange hr = h.getHomeRange();
			if(hr == null) {
				return null;
			}
			quoteViews.setParameter("ProductID", hr.getString("ProductID"));
			if(quoteViews.isSet("ProductID")) {
				quoteViews.doAction(GenerationKeys.SELECTFIRST);
				if(quoteViews.isSuccessful()) {
					return quoteViews.getData("QuoteViewID");
				}
			}
			
			Brand b = h.getHomeBrand();
			if(b == null) {
				return null;
			}
			quoteViews.setParameter("ProductID", b.getString("ProductID"));
			if(quoteViews.isSet("ProductID")) {
				quoteViews.doAction(GenerationKeys.SELECTFIRST);
				if(quoteViews.isSuccessful()) {
					return quoteViews.getData("QuoteViewID");
				}
			}
			
		} finally {
			quoteViews.close();
		}
		
		return null;
	}
	
	/* temporary - move to external interface */
	@XmlAccessorType(XmlAccessType.FIELD)
	@JsonIgnoreProperties({ "PlanID" })
	public static class Plan {
		public String ProductID;
		public String CopyProductID;
		public String HomeProductID;
		public String Name;
		public String ImagePath;
		public String ThumbnailPath;
		public String ProductNum; 
		public String Summary;
		public String PublishDescription; 
		public String DefaultFacadeHeroImageID;
		public String HomeName, HomeImagePath;
		public String RangeName;
		public String RangeProductID;
		public String QuoteViewID;
		public String BrandName;
		public String BrandProductID;
		
		public double Cost;
		public double GST;
		public double TotalCost;
		
		public Plan() {}
		public Plan(HomePlan p, String QuoteViewID, String defaultFacadeHeroImageID) {
			this.ProductID = p.getData("ProductID");
			this.CopyProductID = p.getData("CopiedFromProductID");
			this.HomeProductID = p.getData("HomeProductID");
			
			this.Name = p.getData("Name");
			this.ImagePath = p.getData("ImagePath");
			this.ThumbnailPath = p.getData("ThumbnailImagePath");
			this.Cost = p.getDouble("Cost");
			this.GST = p.getDouble("GST");
			this.TotalCost = p.getDouble("TotalCost");
			this.ProductNum = p.getString("ProductNum");
			this.Summary = p.getString("Summary");
			this.PublishDescription = p.getString("PublishDescription");
			this.DefaultFacadeHeroImageID = defaultFacadeHeroImageID;
			this.HomeName = p.getData("HomeName");
			this.HomeImagePath = p.getData("HomeImagePath");
			
			this.RangeProductID = p.getData("RangeProductID");
			this.RangeName = p.getData("RangeName");
			this.QuoteViewID = QuoteViewID;
			this.BrandName = p.getData("BrandName");
			this.BrandProductID = p.getData("BrandProductID");
		}
	}
	public HLPackage getPackage(HttpServletRequest request, String productId) {
		logger.debug("getPackage(Request, {})", productId);
		return getPackage(ActionBean.getConnection(request), us.getCurrentUser(request), productId);
	}
	
	@XmlAccessorType(XmlAccessType.FIELD)
	@JsonIgnoreProperties({ "PackageID" })
	public static class HLPackage extends Plan {
		
		public String CopyProductID;
		public String PlanProductID;
		public String PlanProductNum;
		public String PlanName;
		public double PlanCost;
		public double TotalPlanCost;
		
		public String LotName;
		public String LotProductID;
		public String LotProductNum; 
		public double LotCost;
		public double TotalLotCost;
		public String EstateProductID;
		public String EstateName;
		public String StageName;
		public String StageProductID;
		public String PackageProductNum;
		
		public String RegionName;
		
		public String FacadeID;
		public String titleDate;
		
		public String CurrentStatusID, CurrentStatus;
		public List<HLPackageCost> PackageCosts;
		
		public HLPackage() {}
		public HLPackage(HouseLandPackage p, String QuoteViewID) {
			this.ProductID = p.getData("ProductID");
			this.HomeProductID = p.getData("HomeProductID");
			this.Name = p.getData("Name");
			this.ImagePath = p.getData("ImagePath");
			this.ThumbnailPath = p.getData("ThumbnailImagePath");
			this.Cost = p.getDouble("Cost");
			this.GST = p.getDouble("GST");
			this.TotalCost = p.getDouble("TotalCost");
			
			this.CurrentStatusID = p.getData("CurrentStatusID");
			this.CurrentStatus = p.getData("CurrentStatus");
			
			this.HomeName = p.getData("HomeName");
			this.RangeName = p.getData("RangeName");
			
			this.LotProductID = p.getData("LotProductID");
			if(p.getLand() != null) { 
				this.LotName = p.getLotName();
				this.LotCost = p.getLotCost();
				this.TotalLotCost = p.getLotTotalCost();
				this.titleDate = p.getLand().getString("LotTitleMonth") + " " + p.getLand().getString("LotTitleYear");
			}
			
			this.StageName = p.getStageName();
			this.StageProductID = p.getData("StageProductID");
			
			this.EstateName = p.getEstateName();
			this.EstateProductID = p.getData("EstateProductID");
			
			this.CopyProductID = p.getData("CopyPlanID");
			this.PlanProductID = p.getData("PlanProductID");
			this.PlanName = p.getPlanName();
			this.PlanCost = p.getPlanCost();
			this.TotalPlanCost = p.getPlanTotalCost();
			
			this.RegionName = p.getData("RegionName");
			
			this.RangeName = p.getRangeName();
			this.HomeName = p.getHomeName();
			this.QuoteViewID = QuoteViewID;
			
			this.LotProductNum = p.getLand().getData("ProductNum");
			this.PlanProductNum = p.getHomePlan().getData("ProductNum");
			this.PackageProductNum = p.getData("ProductNum");
		}
	}
	
	@XmlRootElement(name="PackageCost")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class HLPackageCost {
		
		public String ProductID, Name, Category, Description, ProductDetails;
		public double Cost, GST, TotalCost;
		public boolean Active;
		public HLPackageCost() {}
		public HLPackageCost(PackageCost p) {
			this.ProductID = p.getString("ProductID");
			this.Name = p.getString("Name");
			this.Description = p.getString("Description");
			this.ProductDetails = p.getString("ProductDetails");
			this.Category = p.getString("Category");
			this.Cost = p.getDouble("Cost");
			this.GST = p.getDouble("GST");
			this.TotalCost = p.getDouble("TotalCost");
			this.Active = "Y".equals(p.getString("Active"));
		}
	}
	
	/**
	 * API V1 
	 */
	public HLPackage getPackage(final Connection conn, final UserBean user, final String productId) {
		if(logger.isDebugEnabled()) logger.debug("getPlan(Conn,{}, {})", user != null ? user.getName() : "null user",  productId);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		HouseLandPackage p = new HouseLandPackage(conn, productId);
		
		p.setParameter("-groupby0", "ProductID");
		p.doAction("search");
		logger.trace(p.getStatement());
	
		p.doAction("select");
		
		if(!p.isSuccessful()) {
			throw new NotFoundException("The requested plan could not be found");
		}
		if(!user.canView(p) && !user.canView(p, "GroupProductSecurityGroup")) {
			throw new AccessException("You do not have permission to view this home plan");
		}
		PackageCost pc = new PackageCost(conn);
		HLPackage pkg = new HLPackage(p, getQuoteViewID(p));
		try {
			pc.setParameter("-join1","ProductsLinkedProductProduct");	//inner join 
			pc.setParameter("ProductsLinkedProductProduct.ProductID", p.getString("ProductID"));
			if(logger.isDebugEnabled()) { 
				pc.doAction(GenerationKeys.SEARCH);
				logger.debug(pc.getStatement());
			}
			pc.getResults(true);
			if(pc.getNext()) {
				List<HLPackageCost> costlist = new ArrayList<HLPackageCost>(pc.getSize());
				do {
					costlist.add(new HLPackageCost(pc));
				} while(pc.getNext());
				pkg.PackageCosts = costlist;
			}
		} finally {
			pc.close();
		}
		Facade f = new Facade(conn);
		f.setParameter("ProductID", p.getString("ProductID"));
		if(logger.isDebugEnabled()) { 
			f.doAction(GenerationKeys.SEARCH);
			logger.debug(f.getStatement());
		}
		f.doAction(GenerationKeys.SELECTFIRST);
		if(f.isSuccessful()) {
			pkg.FacadeID = f.getData("LinkedProductID");
		}
		return pkg;
	}


	public HLRange getHome(final HttpServletRequest request, String homeID) {
		logger.debug("getHome(Request)");
		return getHome(ActionBean.getConnection(request), us.getCurrentUser(request), homeID);
	}
	
	public HLRange getHome(final Connection con, final UserBean user, String homeID) {
		logger.debug("getHome(Conn,{},homeID={})", user != null ? user.getName() : "null user", homeID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
	
		Home h = new Home(con, homeID);
		
		if(h.isSuccessful()) return new HLRange(h);

		throw new NotFoundException("The home could not be found " + homeID);
	}
	
	public List<HLRange> getHomes(final HttpServletRequest request, String rangeID) {
		logger.debug("getHomes(Request)");
		return getHomes(ActionBean.getConnection(request), us.getCurrentUser(request), rangeID);
	}
	
	public List<HLRange> getHomes(final Connection con, final UserBean user, String rangeID) {
		logger.debug("getHomes(Conn,{})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
	
		Home h = new Home(con);
		
		if(StringUtils.isNotBlank(rangeID)) {
			h.setParameter("ProductParentHomeRange.ProductVariationHomeRange.ProductID", rangeID);
		}
		
		try {
			h.setParameter("-sort0", "Name");
			h.setParameter("-groupby0", "ProductID");
			if(logger.isTraceEnabled()) {
				h.doAction(GenerationKeys.SEARCH);
				logger.trace(h.getSearchStatement());
			}
			h.getResults(true);;
			if(h.getNext()) {
				List<HLRange> list = new ArrayList<HLRange>(h.getSize());
				do {
					list.add(new HLRange(h));
				} while(h.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			h.close();
		}
	}
	
	public static class HLRange {
		
		public String ProductID, Name, Category; 
		public HLRange() {}
		public HLRange(HomeRange p) {
			this.ProductID = p.getData("ProductID");
			this.Name = p.getData("Name");
			this.Category = p.getData("Category");
		}
		//TODO make a real object
		public HLRange(Home p) {
			this.ProductID = p.getData("ProductID");
			this.Name = p.getData("Name");
			this.Category = p.getData("Category");
		}
	}
	
	public HLRange getRange(final HttpServletRequest request, String rangeID) {
		logger.debug("getRange(Request, rangeID={})", rangeID);
		return getRange(ActionBean.getConnection(request), us.getCurrentUser(request), rangeID);
	}
	
	public HLRange getRange(final Connection conn, final UserBean user, String rangeID) {
		logger.debug("getRanges(Conn,{})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		HomeRange hr = new HomeRange(conn, rangeID);
		if(hr.isSuccessful()) return new HLRange(hr);
		throw new NotFoundException("The range could not be found " + rangeID);
	}
	
	
	public List<HLRange> getRanges(final HttpServletRequest request) {
		logger.debug("getRanges(Request)");
		return getRanges(ActionBean.getConnection(request), us.getCurrentUser(request));
	}
	
	public List<HLRange> getRanges(final Connection conn, final UserBean user) {
		logger.debug("getRanges(Conn,{})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		HomeRange hr = new HomeRange(conn);
		try {
			hr.setParameter("-sort0", "Name");
			hr.setParameter("-groupby0", "ProductID");
			if(logger.isTraceEnabled()) {
				hr.doAction(GenerationKeys.SEARCH);
				logger.trace(hr.getSearchStatement());
			}
			hr.getResults(true);
			if(hr.getNext()) {
				List<HLRange> list = new ArrayList<HLRange>(hr.getSize());
				do {
					list.add(new HLRange(hr));
				} while(hr.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			hr.close();
		}
	}
	
	public ExistingProperty saveExistingProperty(final HttpServletRequest request, ExistingProperty p) {
		logger.debug("saveExistingProperty(request, property(ProductSubType={}))", p.ProductSubType);
		return saveExistingProperty(ActionBean.getConnection(request), us.getCurrentUser(request), p);
	}
	
	public ExistingProperty saveExistingProperty(final Connection con, final UserBean user, ExistingProperty p) {
		logger.debug("saveExistingProperty(con, user={}, property(ProductSubType={}))", user != null ? user.getName() : "null user", p.ProductSubType);
		boolean update = false;
		GenRow r = new GenRow();
		r.setConnection(con);
		r.setViewSpec("properties/ProductExistingPropertyView");
		
		if(StringUtils.isNotBlank(p.ProductID)) {
			r.setParameter("ProductID", p.ProductID);
			r.doAction(GenerationKeys.SELECT);
			
			if(!r.isSuccessful()) {
				r.doAction(GenerationKeys.SEARCH);
				logger.error(r.getStatement());
				
				throw new NotFoundException("Property was not found for update with id " + p.ProductID);
			} else if(!user.canEdit(r)) {
				//TODO, obvs
				//throw new AccessException("You do not have permission to edit this property");
			}
			update = true;
			r.setAction(GenerationKeys.UPDATE);
		} else {
			r.setToNewID("ProductID");
			r.setParameter("GroupID", user.getString("DefaultGroupID"));
			r.setAction(GenerationKeys.INSERT);
			
			r.setParameter("CreatedDate","NOW");
			r.setParameter("CreatedBy", user.getUserID());
			p.ProductID = r.getString("ProductID");
		}
		
		r.setParameter("ProductType",p.ProductType);		//allowing to set here to re-use this interface.
		r.setParameter("ProductSubType", p.ProductSubType);
		r.setParameter("PricingType", p.PricingType);
		r.setParameter("ProductNum", p.ProductNum);
		r.setParameter("Description", p.Description);
		r.setParameter("Image", p.Image);
		r.setParameter("ThumbnailImage", p.ThumbnailImage);
		
		r.setParameter("Summary", p.Summary);
		r.setParameter("PublishDescription", p.PublishDescription);
		r.setParameter("LocationID", p.LocationID);
		r.setParameter("RegionID", p.RegionID);
		r.setParameter("UpdateByProcess", p.UpdateByProcess);
	
		
		boolean foundplan = false, foundlot = false, updateAddressFromLot = false;
		//only do this on update, we won't have any records yet on insert
		if(GenerationKeys.UPDATE.equals(r.getAction())) { 
			GenRow pp = new GenRow();
			try { 
				pp.setConnection(con); 
				pp.setViewSpec("ProductProductLinkedView");
				pp.setParameter("ProductID", p.ProductID);
				
				pp.getResults(); 
				while(pp.getNext()) {
					if("Land".equals(pp.getData("ProductType"))) {
						updateProductProduct(con, user, p.ProductID, pp.getData("LinkedProductID"), p.LotProductID, pp.getData("ProductType"));	
						foundlot = true;
						updateAddressFromLot = !StringUtils.equals(pp.getData("LinkedProductID"), p.LotProductID);
					} else if ("Home Plan".equals(pp.getData("ProductType"))) {
						updateProductProduct(con, user, p.ProductID, pp.getData("LinkedProductID"), p.PlanProductID, pp.getData("ProductType"));
						foundplan = true;
					}
				}
			} finally { 
				pp.close();
			}
		}
		
		if(!foundplan) 
			updateProductProduct(con, user, p.ProductID, null, p.PlanProductID, "Home Plan");
		if(!foundlot)  {
			updateProductProduct(con, user, p.ProductID, null, p.LotProductID, "Land");
			updateAddressFromLot = StringUtils.isNotBlank(p.LotProductID);
		}
		if(!StringUtils.equals(r.getString("CurrentStatusID"), p.CurrentStatusID) && StringUtils.isNotBlank(p.CurrentStatusID)) {
			//add a new status 
			String key = KeyMaker.generate();
			r.setParameter("ProductStatusID", key);
	         GenRow statusBean = new GenRow();
	         statusBean.setConnection(con);
	         statusBean.setViewSpec("ProductStatusView");
	         statusBean.setParameter("ProductStatusID", key);
	         statusBean.setParameter("ProductID", p.ProductID);
	         statusBean.setParameter("StatusID",p.CurrentStatusID);
	         statusBean.setParameter("CreatedDate","NOW");
	         statusBean.setParameter("CreatedBy",user.getUserID());
	         statusBean.doAction(ActionBean.INSERT);
		}
		
		r.setParameter("Vendor", p.Vendor);
		r.setParameter("SellingAgent", p.SellingAgent);
		r.setParameter("SellingAgentContactID", p.SellingAgentContactID);
		r.setParameter("Developer", p.Developer);
		r.setParameter("Project", p.Project);
		r.setParameter("Stage", p.Stage);
		
		r.setParameter("Active", p.Active ? "Y" : "N");
		r.setParameter("DisplayFlag", p.DisplayFlag ? "Y" : "N");
		r.setParameter("ModifiedDate","NOW");
		r.setParameter("ModifiedBy", user.getUserID());
		r.setParameter("Name", p.Name);
		if(p.Address != null || updateAddressFromLot) {
			Address nameAdd = p.Address;		
			String addr = "";
			if(!updateAddressFromLot && nameAdd != null) { 
				addr = nameAdd.Street;
				if(nameAdd.Street2 != null && (nameAdd.Street2).length()>0){
					addr = addr + " " + nameAdd.Street2;
				}
				if ("true".equals(InitServlet.getSystemParam("RPM-REuseLongName"))) {
					if(nameAdd.City != null && (nameAdd.City).length()>0){
						addr = addr + ", " + nameAdd.City;
					}
					if(nameAdd.State != null && (nameAdd.State).length()>0){
						addr = addr + " " + nameAdd.State;
					}
					if(nameAdd.Postcode != null && (nameAdd.Postcode).length()>0){
						addr = addr + " " + nameAdd.Postcode;
					}
				}
			}else{
				GenRow l = new GenRow();
				l.setTableSpec("Addresses");
				l.setParameter("-select1","*");
				l.setParameter("ProductID", p.LotProductID);
				if(l.isSet("ProductID")) { 
					l.doAction(GenerationKeys.SELECTFIRST);
				}
				if(l.isSuccessful()) {
					
					addr = l.getData("Street");
					if(l.getData("Street2") != null && (l.getData("Street2")).length()>0){
						addr = addr + " " + l.getData("Street2");
					}
					if ("true".equals(InitServlet.getSystemParam("RPM-REuseLongName"))) {
						if(l.getData("City") != null && (l.getData("City")).length()>0){
							addr = addr + ", " + l.getData("City");
						}
						if(l.getData("State") != null && (l.getData("State")).length()>0){
							addr = addr + " " + l.getData("State");
						}
						if(l.getData("Postcode") != null && (l.getData("Postcode")).length()>0){
							addr = addr + " " + l.getData("Postcode");
						}
					}
					
					if(logger.isDebugEnabled()) logger.debug(addr);
				} else { 
					logger.debug("unable to find address from lot product with id {}", p.LotProductID);
					updateAddressFromLot = false;
				}
			}
			r.setParameter("Name", addr);
		}else{
			r.setParameter("Name", p.Name);
		}
		
		if(!update || r.getDouble("TotalCost") != p.TotalCost) { 
			double cost = p.TotalCost / 1.1;
			r.setParameter("GST", p.TotalCost - cost);
			r.setParameter("Cost", cost);
			r.setParameter("TotalCost", p.TotalCost);
			
			r.setParameter("DripTotal", 0);
			r.setParameter("DripResult", p.TotalCost);
		}
		
		r.doAction();
		if(!r.isSuccessful()) {
			throw new RuntimeException("Save/Insert failed with error " + r.getError());
		}
		
		if(GenerationKeys.INSERT.equals(r.getAction())) { 
			GenRow pg = new GenRow();
			pg.setViewSpec("ProductSecurityGroupView");
			pg.setConnection(con);
			pg.setToNewID("ProductSecurityGroupID");
			pg.setParameter("ProductID", p.ProductID);
			pg.setParameter("GroupID", user.getString("DefaultGroupID"));
			pg.setParameter("RepUserID", user.getUserID());
			pg.setParameter("CreatedDate", "NOW");
			pg.setParameter("CreatedBy", user.getUserID());
			pg.doAction(GenerationKeys.INSERT);
		}
		
		r.doAction(GenerationKeys.SELECT);
		
		if(!r.isSuccessful()) {
			throw new RuntimeException("Select failed with error " + r.getError());
		}
		
		p.ImagePath = r.getData("ImagePath");
		p.ThumbnailImagePath = r.getData("ThumbnailImagePath");
		
		
		if(p.Address != null || updateAddressFromLot) {
			Address a = p.Address;
			
			GenRow addresses = new GenRow();
			addresses.setTableSpec("Addresses");
			addresses.setParameter("-select0","AddressID");
			addresses.setParameter("ProductID", p.ProductID);
			if(addresses.isSet("ProductID")) { 
				addresses.doAction(GenerationKeys.SELECTFIRST);
			}
			
			r.clear();
			r.setViewSpec((com.sok.framework.ViewSpec)null);
			r.setTableSpec("Addresses");
			
			if( (addresses.getString("AddressID") != null && (addresses.getString("AddressID")).length() > 0) || (a != null && StringUtils.isNotBlank(a.AddressID))) {
				if(addresses.getString("AddressID") != null && (addresses.getString("AddressID")).length() > 0){
					r.setParameter("AddressID", addresses.getString("AddressID"));
				}else{
					r.setParameter("AddressID", a.AddressID);
				}
				r.setAction(GenerationKeys.UPDATE);
				
			} else {
				r.setToNewID("AddressID");
				r.setAction(GenerationKeys.INSERT);
				//we're going to reload these.
				//a.AddressID = r.getString("AddressID");
				r.setParameter("ProductID", p.ProductID);
			}
			addresses.close();
			if(updateAddressFromLot) {
				/* pull the address attached to the lot product, and update or insert as appropriate */
				GenRow l = new GenRow();
				l.setTableSpec("Addresses");
				l.setParameter("-select1","*");
				l.setParameter("ProductID", p.LotProductID);
				if(l.isSet("ProductID")) { 
					l.doAction(GenerationKeys.SELECTFIRST);
				}
				if(l.isSuccessful()) {
					for(String s: new String[]{"LocationID","Street2","City","State","Postcode","Region","Country","Latitude","Longitude","MapRef"}) {
						r.setParameter(s, l.getData(s));
					}
					//lets be honest, this is a kludge.
					r.setParameter("Street", l.getData("StreetNumber"));
					r.setParameter("Street2", StringUtils.trim(new StringBuilder().append(l.getData("Street")).append(" ").append(l.getData("Street2")).toString()));
					
					if(logger.isDebugEnabled()) logger.debug(r.toString());
				} else { 
					logger.debug("unable to find address from lot product with id {}", p.LotProductID);
					updateAddressFromLot = false;
				}
			} 
			if(!updateAddressFromLot && a != null) { 
				r.setParameter("Street",a.Street);
				r.setParameter("Street2",a.Street2);
				r.setParameter("City", a.City);
				r.setParameter("State", a.State);
				r.setParameter("Postcode", a.Postcode);
				r.setParameter("MapRef", a.MapRef);
			}
			logger.debug("Perform address action a!=null=[{}], updateFromLot=[{}]", a != null, updateAddressFromLot);
			if(a != null || updateAddressFromLot) {
				r.doAction();
			}
			if(!r.isSuccessful()) {
				if(GenerationKeys.UPDATE.equals(r.getAction())) {
					logger.error("{} Save/Insert failed with error {}", r.toString(), r.getError());
					r.setAction(GenerationKeys.INSERT); 
					r.doAction();
				}
				if(!r.isSuccessful())
					throw new RuntimeException(r.toString() + " Save/Insert failed with error " + r.getError());
			}
		}
		
		updateProductFeedAgentDetails(con, p.REAListingID, p.ProductID, user.getUserID());
		
		GenRow products = new GenRow();
		products.setTableSpec("Products");
		products.setParameter("-select0","*");
		products.setParameter("ProductID",p.ProductID);
		products.doAction("selectfirst");
		
		RPMtask.getInstance().refreshData(RPMtask.type.existingproperty);
		RPMtask.getInstance().calculateEPDrips(null, new String[] {products.getString("ProductID"), products.getString("TotalCost"), products.getString("DripResult")});
		RPMtask.getInstance().doIndexRefreash("update", p.ProductID, "Existing Property");
		
		return getExistingProperty(con, user, p.ProductID);
	}

	public void updateProductProduct(Connection con, UserBean user, String productID, String oldProductID, String newProductID, String productType) {
		if(StringUtils.equals(oldProductID, newProductID)) {
			return;
		}
		
		String oldHomeProductID = null, newHomeProductID = null;		
		
		oldProductID = StringUtils.trimToNull(oldProductID);
		newProductID = StringUtils.trimToNull(newProductID);
		
		if("Home Plan".equals(productType)) {
			if(oldProductID != null) {
				PropertyEntity entity = PropertyFactory.getPropertyEntity(con, oldProductID);
				HomePlan plan = (HomePlan) entity;
				oldHomeProductID = plan.getHome().getString("ProductID");
			}
			
			if(newProductID != null) {
				PropertyEntity entity = PropertyFactory.getPropertyEntity(con, newProductID);
				HomePlan plan = (HomePlan) entity;
				newHomeProductID = plan.getHome().getString("ProductID");
			}
		}
		
		Product newProduct = null;
		if(newProductID != null) {
			newProduct = new Product(con);
			newProduct.load(newProductID);
			
			if(!newProduct.isLoaded()) 
				newProductID = null;
		}

		GenRow pr = new GenRow();
		pr.setConnection(con);
		pr.setTableSpec("properties/ProductDetails");
		
		// Reset Product Details
		if(oldProductID != null) {
			//pr.setParameter("ON-CopiedFromProductID", oldProductID); 
			pr.setParameter("ON-ProductID", productID);
			pr.setParameter("ON-ProductType", productType.replaceAll(" ","") + "%");			
			pr.doAction(GenerationKeys.DELETEALL);
			logger.debug("Deleted product details. Query {}", pr.getStatement());
		}
		if(newProductID != null) {
			List<ProductDetail> planDetails = getProductDetails(con, user, newProductID);
			for(ProductDetail pd: planDetails) {
				pr.clear();
				pr.setParameter("-select1","ProductDetailsID");
				pr.setParameter("ProductID", productID);
				pr.setParameter("ProductType", newProduct.getProductType().replaceAll(" ","") + "%");
				pr.setParameter("DetailsType", pd.DetailsType);
				if (pr.getString("ProductID").length() > 0) pr.doAction(GenerationKeys.SELECTFIRST);
				
				pd.populateGenRow(pr);
				pr.setParameter("ProductType", newProduct.getProductType().replaceAll(" ","") + "Details");
				pr.setParameter("CopiedFromProductID", newProduct.getPrimaryKey());
				
				if(pr.isSuccessful()) {
					pr.setAction(GenerationKeys.UPDATE);
					//continue here if we do not want to override any existing values.
				} else {
					pr.setToNewID("ProductDetailsID");
					pr.setAction(GenerationKeys.INSERT);
				}
				pr.doAction(); 
			}
		}
		
		// Reset Linked Documents
		GenRow ld = new GenRow();
		ld.setConnection(con);
		ld.setViewSpec("LinkedDocumentView");
		
		GenRow ldw = new GenRow();
		ldw.setConnection(con);
		ldw.setViewSpec("LinkedDocumentView");
		
		if(oldProductID != null) {
			try {
				deleteLinkedDocuments(con, user, oldProductID, productID);
				if(StringUtils.isNotBlank(oldHomeProductID)) {
					deleteLinkedDocuments(con, user, oldHomeProductID, productID);
				}
			} finally {
				ld.close();
			}
		} else {
			logger.debug("old plan productid was empty");
		}
			
		if(newProductID != null) { 
			try {	
				addLinkedDocuments(con, user, newProductID, productID);
				if(StringUtils.isNotBlank(newHomeProductID)) {
					addLinkedDocuments(con, user, newHomeProductID, productID);
				}
				
			} finally {
				ld.close();
			}
		}

		pr.clear();
		pr.setTableSpec("ProductProducts");
		pr.setParameter("ProductID", productID);
		
		if(oldProductID != null) {
			pr.setParameter("-select1", "ProductProductID");
			pr.setParameter("LinkedProductID", oldProductID);
			pr.doAction(GenerationKeys.SELECTFIRST);
			if(pr.isSuccessful()) {
				pr.setAction(GenerationKeys.UPDATE);
			} else {
				pr.setParameter("CreatedDate","NOW");
				pr.setParameter("CreatedBy", user.getUserID());
				pr.setToNewID("ProductProductID");
				pr.setAction(GenerationKeys.INSERT);
			}
		} else {
			pr.setAction(GenerationKeys.INSERT);
			pr.setParameter("CreatedDate","NOW");
			pr.setParameter("CreatedBy", user.getUserID());
			pr.setToNewID("ProductProductID");
		}
		if(newProductID != null) { 
			pr.setParameter("LinkActive","Y");
			pr.setParameter("LinkedProductID", newProductID);
			pr.setParameter("ModifiedDate","NOW");
			pr.setParameter("ModifiedBy", user.getUserID());
			pr.doAction();
			
			if(logger.isDebugEnabled()) logger.debug("updating product products {}, \n{}", pr.getStatement(), pr.toString());
			
		} else if(GenerationKeys.UPDATE.equals(pr.getAction())) {
			pr.doAction(GenerationKeys.DELETE);
		}
	}

	public void deleteLinkedDocuments(Connection con, UserBean user, String originalProductID, String productID) {
		// Add Linked Documents
		GenRow ld = new GenRow();
		ld.setConnection(con);
		ld.setViewSpec("LinkedDocumentView");
		
		GenRow ldw = new GenRow();
		ldw.setConnection(con);
		ldw.setViewSpec("LinkedDocumentView");
		
		ld.setParameter("ProductID", originalProductID);
		ld.getResults();
		while(ld.getNext()) {
			ldw.setParameter("ON-DocumentID", ld.getData("DocumentID"));
			ldw.setParameter("ON-ProductID", productID);
			ldw.doAction(GenerationKeys.DELETEALL);
		}
	}
	
	public void addLinkedDocuments(Connection con, UserBean user, String originalProductID, String productID) {
		// Add Linked Documents
		GenRow ld = new GenRow();
		ld.setConnection(con);
		ld.setViewSpec("LinkedDocumentView");
		
		GenRow ldw = new GenRow();
		ldw.setConnection(con);
		ldw.setViewSpec("LinkedDocumentView");
		
		ld.clear();
		ld.setParameter("ProductID", originalProductID);
		ld.doAction(GenerationKeys.SEARCH);
		ld.getResults(); 
		
		ldw.clear();
		ldw.setParameter("ProductID", productID);
		ldw.setParameter("CreatedDate","NOW");
		ldw.setParameter("CreatedBy", user.getUserID());
		while(ld.getNext()) {
			ldw.setToNewID("LinkedDocumentID");
			ldw.setParameter("DocumentID", ld.getData("DocumentID"));
			ldw.setParameter("OrderID", ld.getData("OrderID"));
			try {
				ldw.doAction(GenerationKeys.INSERT);
			} catch (DatabaseException ve) {	//NB, constraint doesn't exist.. yet
				if(ve.getCause() != null && ve.getCause() instanceof SQLIntegrityConstraintViolationException) { 
					logger.debug("did not insert as already existed {}", ldw.toString());
				} else {
					throw ve;
				}
			}
		}
	}
	
	public ExistingProperty getExistingProperty(final HttpServletRequest request, String productId) {
		return getExistingProperty(ActionBean.getConnection(request), us.getCurrentUser(request), productId);
	}
	
	public ExistingProperty getExistingProperty(final Connection con, final UserBean user, String productId) {
		logger.debug("getProperty(Conn,{})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		
		GenRow r = new GenRow(); 
		r.setViewSpec("properties/ProductExistingPropertyView");
		r.setConnection(con);
		r.setParameter("ProductID", productId);
		if(logger.isTraceEnabled()) {
			r.doAction(GenerationKeys.SEARCH);
			logger.trace(r.getStatement());
		}
		
		r.doAction(GenerationKeys.SELECT);
		
		
		if(!r.isSuccessful()) {
			throw new NotFoundException("Property was not found with id " + productId);
		}
		
		ExistingProperty p = new ExistingProperty();
		p.ProductID = r.getData("ProductID");
		p.Name = r.getData("Name");
		p.ProductType = r.getData("ProductType");
		p.PricingType = r.getData("PricingType");
		p.ProductNum = r.getData("ProductNum");
		p.Description = r.getData("Description");
		p.ProductSubType = r.getData("ProductSubType");
		p.Summary = r.getData("Summary");
		p.PublishDescription = r.getData("PublishDescription");
		p.PublishingStatus = r.getData("PublishingStatus");
		p.MarketingStatus = r.getData("MarketingStatus");
		p.GroupID = r.getData("GroupID"); //used for uploaded docs

		p.PublishingStatusIcon = PublishConstants.getPublishOptionClass(r.getData("PublishingStatus"));
		p.UpdateByProcess = "Y";
		p.LocationName = r.getData("LocationName");
		p.RegionName = r.getData("RegionName");
		p.LocationID = r.getData("LocationID");
		p.RegionID = r.getData("RegionID");
		
		GenRow pp = new GenRow();
		GenRow planProduct = new GenRow();
		try { 
			pp.setConnection(con); 
			pp.setViewSpec("ProductProductLinkedView");
			pp.setParameter("-select1", "ProductProductsLinkedProduct.ProductSubType");
			pp.setParameter("-select2", "ProductProductsLinkedProduct.MarketingStatus");
			pp.setParameter("ProductID", r.getData("ProductID"));
			pp.getResults(); 
			while(pp.getNext()) {
				
				if("Land".equals(pp.getData("ProductType"))) {
					p.LotProductID = pp.getData("LinkedProductID");
					p.LotTotalCost = pp.getDouble("TotalCost");
					p.LotNumber = pp.getString("Name");
					p.Lot_ProductSubType = pp.getString("ProductSubType");
					p.Lot_MarketingStatus = pp.getString("MarketingStatus");
				} else if ("Home Plan".equals(pp.getData("ProductType"))) {
					planProduct = new GenRow();
					planProduct.setConnection(con); 
					planProduct.setViewSpec("properties/ProductPlanView");
					planProduct.setParameter("ProductID", pp.getData("LinkedProductID"));
					planProduct.doAction(GenerationKeys.SELECTFIRST);
					
					p.HomeDesignName =  new StringBuilder().append(planProduct.getString("HomeName")).append(" ").append(planProduct.getString("Name")).toString();
					p.PlanProductID = pp.getData("LinkedProductID");
					p.PlanTotalCost = pp.getDouble("TotalCost");
				}
			}
		} finally {
			planProduct.close();
			pp.close();
		}
		
		
		
		p.TotalCost = r.getDouble("TotalCost");
		p.DripCost = r.getDouble("DripCost");
		p.DripResult = r.getDouble("DripResult") == 0 ? r.getDouble("TotalCost") : r.getDouble("DripResult");
		
		p.Vendor = r.getData("Vendor");
		if(StringUtils.isNotBlank(p.Vendor)){
			try { 
				Contact c = ContactService.getInstance().getContact(con, user, p.Vendor);
				p.VendorDisplay = c.getName();
				if(StringUtils.isNotBlank(c.getMobile())) {
					p.VendorPhone = c.getMobile();
				} else {
					p.VendorPhone = c.getPhone();
				}
				p.VendorEmail = c.getEmail();
				p.VendorType = "Contact";
			} catch (NotFoundException nfe) {
				UserBean u = UserService.getInstance().getSimpleUser(con, p.Vendor);
				if(u != null) {
					p.VendorDisplay = u.getName();
					if(StringUtils.isNotBlank(u.getString("Mobile"))) {
						p.VendorPhone = u.getString("Mobile");
					} else { 
						p.VendorPhone = u.getString("Phone");
					}
					p.VendorEmail = u.getEmail();
					p.VendorType = "User";
				}
			}
		}
		p.SellingAgent = r.getData("SellingAgent");
		p.SellingAgentDisplay = r.getData("SellingAgentCompany");
		p.SellingAgentContactID = r.getData("SellingAgentContactID");
		p.Developer = r.getData("Developer");
		p.DeveloperCompany = r.getData("DeveloperCompany");
		p.Project = r.getData("Project");
		p.Stage = r.getData("Stage");		
		
		if(StringUtils.isNotBlank(r.getData("SellingAgentContactID"))) {
			
			p.SellingAgentContact = new StringBuilder().append(r.getData("SellingAgentContactFirstName")).append(" ").append(r.getData("SellingAgentContactLastName")).toString(); 
			
			p.SellingAgentEmail = r.getData("SellingAgentContactEmail");
			
			if(StringUtils.isNotBlank(r.getData("SellingAgentContactMobile"))) {
				p.SellingAgentPhone = r.getData("SellingAgentContactMobile");
			} else { 
				p.SellingAgentPhone = r.getData("SellingAgentContactPhone");
			}
		} else {
			p.SellingAgentEmail = r.getData("SellingAgentEmail");
			p.SellingAgentPhone = r.getData("SellingAgentPhone");
		}
		
		p.Image = r.getData("Image");
		p.ImagePath = r.getData("ImagePath");
		p.ThumbnailImage = r.getData("ThumbnailImage");
		p.ThumbnailImagePath = r.getData("ThumbnailImagePath");
		
		p.Active = "Y".equals(r.getData("Active"));
		p.DisplayFlag = "Y".equals(r.getData("DisplayFlag"));
		p.CurrentStatus = r.getData("CurrentStatus");
		p.CurrentStatusID = r.getData("CurrentStatusID");
		
		if(r.getDate("CreatedDate") != null) 
			p.CreatedDate = r.getDate("CreatedDate").getTime();
		if(r.getDate("ModifiedDate") != null) 
			p.ModifiedDate = r.getDate("ModifiedDate").getTime();
		
		p.CreatedBy = new PersonName(r.getData("OwnFirstName"), r.getData("OwnLastName"));
		p.ModifiedBy = new PersonName(r.getData("ModFirstName"), r.getData("ModLastName"));
		
		Address a = new Address();
		a.AddressID = r.getData("AddressID");
		a.Street = r.getData("Street");
		a.Street2 = r.getData("Street2");
		a.City = r.getData("City");
		a.State = r.getData("State");
		a.Postcode = r.getData("Postcode");
		a.MapRef = r.getData("MapRef");
		
		p.Address = a;
		p.ProductDetails = getProductDetails(con, user, p.ProductID);
		if(logger.isDebugEnabled()) logger.debug("Size of details array: {}", p.ProductDetails.size());
		
		p.ProductSecurityGroups = getProductSecurityGroups(con, user, p.ProductID);
		if(logger.isDebugEnabled()) logger.debug("Size of groups array: {}", p.ProductSecurityGroups.size());
		
		GenRow productFeedDetails = RunwayUtil.getProductFeedDetails(con, productId);
		p.REAListingID = productFeedDetails.getString("REAListingID");
		return p;
	}
	
	
	
	
	
	public List<ProductDetail> getProductDetails(HttpServletRequest request, String productId) {
		return getProductDetails(ActionBean.getConnection(request), us.getCurrentUser(request), productId);
	}
	
	public List<ProductDetail> getProductDetails(final Connection con, final UserBean user,String productId) {
		return getProductDetails(con, user, productId, null);
	}
	public List<ProductDetail> getProductDetails(final Connection con, final UserBean user,String productID, String type) {
		logger.debug("getProductDetails(con, user, productID={}, type={}", productID, type);
		GenRow r = new GenRow();
		try { 
			r.setTableSpec("properties/ProductDetails");
			r.setConnection(con);
			r.setParameter("-select","*");
			r.setParameter("ProductID", productID);
			if(type != null) {
				r.setParameter("ProductType", type);
			}
			r.setParameter("-sort1","SortOrder");
			if(logger.isTraceEnabled()) {
				r.doAction(GenerationKeys.SEARCH);
				logger.trace(r.getStatement());
			}
			if(r.isSet("ProductID")) { 
				r.getResults(true);
			}
			if(r.getNext()) {
				List<ProductDetail> list = new ArrayList<ProductDetail>(r.getSize());
				do {
					list.add(new ProductDetail(r));
				} while(r.getNext());
				return list;
			}
			if(logger.isDebugEnabled()) { 
				logger.debug(r.getError());
				logger.debug(r.getErrors());
			}
			return Collections.emptyList();
		} finally { 
			r.close();
		}
	}
	
	public List<ProductSecurityGroup> getProductSecurityGroups(HttpServletRequest request, String productId) {
		return getProductSecurityGroups(ActionBean.getConnection(request), us.getCurrentUser(request), productId);
	}
	public List<ProductSecurityGroup> getProductSecurityGroups(final Connection con, final UserBean user,String productId) {
		GenRow r = new GenRow();
		try { 
			r.setViewSpec("ProductSecurityGroupView");
			r.setConnection(con);
			r.setParameter("ProductID", productId);
			if(logger.isTraceEnabled()) {
				r.doAction(GenerationKeys.SEARCH);
				logger.trace(r.getStatement());
			}
			r.getResults(true);
			if(r.getNext()) {
				List<ProductSecurityGroup> list = new ArrayList<ProductSecurityGroup>(r.getSize());
				do {
					list.add(new ProductSecurityGroup(r));
				} while(r.getNext());
				return list;
			} else {
				logger.debug("r.getNext() was false");
			}
			logger.debug(r.getError());
			logger.debug(r.getErrors());
			return Collections.emptyList();
		} finally { 
			r.close();
		}
	}
	
	public ProductSecurityGroup getProductSecurityGroup(final Connection con, String ProductSecurityGroupID) {
		ProductSecurityGroup psg = null;
		
		GenRow r = new GenRow();
		try { 
			r.setViewSpec("ProductSecurityGroupView");
			r.setConnection(con);
			r.setParameter("ProductSecurityGroupID", ProductSecurityGroupID);
			if(logger.isTraceEnabled()) {
				r.doAction(GenerationKeys.SELECTFIRST);
				logger.trace(r.getStatement());
			}
			r.doAction(GenerationKeys.SELECTFIRST);
			if(r.isSuccessful()) {
				psg = new ProductSecurityGroup(r);
			} else {
				logger.debug("r.getNext() was false");
			}
			logger.debug(r.getError());
			logger.debug(r.getErrors());
			return psg;
		} finally { 
			r.close();
		}
	}
	
	public ProductDetail getProductDetail(HttpServletRequest request, String productId, String productDetailID) {
		return getProductDetail(ActionBean.getConnection(request), us.getCurrentUser(request), productId, productDetailID);
	}
	public ProductDetail getProductDetail(final Connection con, final UserBean user,String productId, String productDetailID) {
		GenRow r = new GenRow();
		r.setTableSpec("properties/ProductDetails");
		r.setConnection(con);
		r.setParameter("-select","*");
		r.setParameter("ProductDetailsID", productDetailID);
		r.doAction(GenerationKeys.SELECT);
		if(!r.isSuccessful()) {
			throw new NotFoundException("ProductDetail not found with id " + productDetailID);
		}
		if(!StringUtils.equals(productId, r.getData("ProductID"))) {
			throw new RuntimeException("Attempted to get product detail with id " + productDetailID + " which matched a different product than requested " + productId);
		}
		return new ProductDetail(r);
	}

	public List<Plan> getPlans(HttpServletRequest request, String homeProductID) {
		logger.debug("getPlans(Request, {})", homeProductID);
		return getPlans(ActionBean.getConnection(request), us.getCurrentUser(request), homeProductID);
	}
	
	public List<Plan> getPlans(final Connection conn, final UserBean user, String homeProductID) {
		if(logger.isDebugEnabled()) logger.debug("getPlan(Conn,{}, {})", user != null ? user.getName() : "null user",  homeProductID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		final HomePlan p = new HomePlan(conn);
		try { 
			p.setParameter("Active","Y");
			p.setParameter("Category", "NULL+EMPTY");
			user.setConstraint(p, "ProductProductSecurityGroups");
			if(StringUtils.isNotBlank(homeProductID)) {
				p.setParameter("ProductParentHomeDesign.ProductVariationHomeDesign.ProductID", homeProductID);
			}
			p.setParameter("-groupby0", "ProductID");
			p.getResults(true);
			
			if(p.getNext()) {
				List<Plan> list = new ArrayList<Plan>(p.getSize());
				do {
					list.add(new Plan(p, null, getFacadeImageID(p)));
				} while (p.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			p.close();
		}
	}

	public List<com.sok.runway.externalInterface.beans.cms.properties.Plan> getHomePlans(final Connection conn, String homeProductID, boolean active) {
		if(logger.isDebugEnabled()) logger.debug("getPlan(Conn,{}, {})", "null user",  homeProductID);
		final HomePlan p = new HomePlan(conn);
		try { 
			if (active) p.setParameter("Active","Y");
			p.setParameter("Category", "NULL");
			if(StringUtils.isNotBlank(homeProductID)) {
				p.setParameter("ProductParentHomeDesign.ProductVariationHomeDesign.ProductID", homeProductID);
			}
			p.setParameter("-groupby0", "ProductID");
			p.getResults(true);
			
			if(p.getNext()) {
				List<com.sok.runway.externalInterface.beans.cms.properties.Plan> list = new ArrayList<com.sok.runway.externalInterface.beans.cms.properties.Plan>(p.getSize());
				do {
					list.add(BeanFactory.external(p));
				} while (p.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			p.close();
		}
	}

	public List<com.sok.runway.externalInterface.beans.cms.properties.PlanCheck> getHomePlansCheck(final Connection conn, String homeProductID, boolean active) {
		if(logger.isDebugEnabled()) logger.debug("getPlan(Conn,{}, {})", "null user",  homeProductID);
		final HomePlan p = new HomePlan(conn);
		try { 
			if (active) p.setParameter("Active","Y");
			p.setParameter("Category", "NULL");
			if(StringUtils.isNotBlank(homeProductID)) {
				p.setParameter("ProductParentHomeDesign.ProductVariationHomeDesign.ProductID", homeProductID);
			}
			p.setParameter("-groupby0", "ProductID");
			p.getResults(true);
			
			if(p.getNext()) {
				List<com.sok.runway.externalInterface.beans.cms.properties.PlanCheck> list = new ArrayList<com.sok.runway.externalInterface.beans.cms.properties.PlanCheck>(p.getSize());
				do {
					list.add(BeanFactory.externalCheck(p));
				} while (p.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			p.close();
		}
	}

	public ProductDetail saveProductDetail(HttpServletRequest request, ProductDetail productDetail) {
		return saveProductDetail(ActionBean.getConnection(request), us.getCurrentUser(request), productDetail);
	}	
	public ProductDetail saveProductDetail(final Connection con, final UserBean user, ProductDetail productDetail) {
		
		GenRow pr = new GenRow();
		pr.setConnection(con);
		pr.setTableSpec("properties/ProductDetails");
		pr.setParameter("-select1","ProductDetailsID");
		pr.setParameter("ProductID", productDetail.ProductID);
		pr.setParameter("DetailsType", productDetail.DetailsType);
		if (pr.getString("ProductID").length() > 0) pr.doAction(GenerationKeys.SELECTFIRST);
		
		if(pr.isSuccessful()) {
			productDetail.ProductDetailsID = pr.getData("ProductDetailsID");
			pr.setParameter("ProductType", productDetail.ProductType);
			pr.setAction(GenerationKeys.UPDATE);
		} else {
			pr.setToNewID("ProductDetailsID");
			pr.setParameter("ProductType", productDetail.ProductType);
			productDetail.ProductDetailsID = pr.getString("ProductDetailsID");
			pr.setAction(GenerationKeys.INSERT);
		}
		productDetail.populateGenRow(pr);
		pr.doAction();
		
		
		if(!pr.isSuccessful())
			throw new RuntimeException(pr.toString() + " Save/Insert failed with error " + pr.getError());
		
		processProductDetails(pr, productDetail.ProductID, getDetailsMap(productDetail.ProductType));
		
		RPMtask.getInstance().doIndexRefreash("update", productDetail.ProductID);
		
		return getProductDetail(con, user, productDetail.ProductID, productDetail.ProductDetailsID);
	}
	
	
	public ProductSecurityGroup saveProductSecurityGroup(HttpServletRequest request, ProductSecurityGroup productSecurityGroup) {
		return saveProductSecurityGroup(ActionBean.getConnection(request), us.getCurrentUser(request), productSecurityGroup);
	}	

	public ProductSecurityGroup saveProductSecurityGroup(final Connection con, final UserBean user, ProductSecurityGroup productSecurityGroup) {

		GenRow pr = new GenRow();
		pr.setConnection(con);
		pr.setTableSpec("ProductSecurityGroups");
		pr.setParameter("-select1", "ProductSecurityGroupID");
		pr.setParameter("ProductID", productSecurityGroup.ProductID);
		pr.doAction(GenerationKeys.SELECTFIRST);

		if (!isUserBelongsToGroup(con, user, productSecurityGroup)) {
			productSecurityGroup.RepUserID = null;
		}
		productSecurityGroup.populateGenRow(pr);
		// Assuming that there is a one-to-one mapping between Products and ProductSecurityGroups
		if (pr.isSet("ProductID") && pr.isSuccessful()) {
			productSecurityGroup.ProductSecurityGroupID = pr.getData("ProductSecurityGroupID");
			if(pr.isSet("GroupID") || (pr.isSet("RepUserID")))
				pr.setAction(GenerationKeys.UPDATE);
		} else {
			pr.setToNewID("ProductSecurityGroupID");
			productSecurityGroup.ProductSecurityGroupID = pr.getString("ProductSecurityGroupID");
			pr.setParameter("CreatedDate", "now()");
			pr.setParameter("CreatedBy", user.getUserID());
			pr.setAction(GenerationKeys.INSERT);
		}
		
		pr.doAction();

		if (!pr.isSuccessful())
			throw new RuntimeException(pr.toString() + " Save/Insert failed with error " + pr.getError());

		return getProductSecurityGroup(con, productSecurityGroup.ProductSecurityGroupID);
	}
	
	private boolean isUserBelongsToGroup(final Connection con, final UserBean user, ProductSecurityGroup productSecurityGroup){
		boolean isUserValid = false;
		
		logger.debug("checkIfUserBelongsToGroup(Conn,{})", productSecurityGroup.GroupID);
		
		GenRow linkeduserlist = new GenRow();
	   	linkeduserlist.setViewSpec("UserSelectView");
	   	linkeduserlist.setConnection(con);
	    linkeduserlist.setParameter("UserID",productSecurityGroup.RepUserID);
	    linkeduserlist.setParameter("UserLinkedUser.LinkedGroupID|1",productSecurityGroup.GroupID);
	    linkeduserlist.setParameter("Status","Active");	    
	   	linkeduserlist.setParameter("-sort1","FirstName");
	   	linkeduserlist.setParameter("-sort2","LastName");
	   	user.setConstraint(linkeduserlist,"LinkedUserUser.UserGroups.UserGroup");
	   	linkeduserlist.doAction("selectfirst");

	   	if(linkeduserlist.isSuccessful())
	   		isUserValid = true;
	   	
		linkeduserlist.close();
		
		if(logger.isDebugEnabled())
			logger.debug(linkeduserlist.getStatement());
		
		return isUserValid;
	
	}
	
	public List<RealEstate> getRealEstateExternal(final HttpServletRequest request, final Date modifiedSince, final boolean active) {
		logger.debug("getRealEstateExternal(Request, modifiedSince={})", modifiedSince);
		return getRealEstateExternal(ActionBean.getConnection(request), us.getCurrentUser(request), modifiedSince, active);		
	}
	
	public List<RealEstate> getRealEstateExternal(final Connection con, final UserBean user, final Date modifiedSince, final boolean active) {
		
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.ExistingProperty packageList = new com.sok.runway.crm.cms.properties.ExistingProperty(con);
		if(modifiedSince != null) { 
			packageList.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(modifiedSince));
		}
		packageList.setParameter("-sort0", "Name");
		if(active) {
			packageList.setParameter("Active","Y");
		}
		try {
			//packageList.setConnection(con);
			packageList.setParameter("-groupby0", "ProductID");
			packageList.getResults(true);
			
			if(packageList.getNext()) {
				List<RealEstate> list = new ArrayList<RealEstate>(packageList.getSize());
				do {
					list.add(BeanFactory.external(packageList, BeanRepresentation.Simple));
				}while(packageList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			packageList.close();
		}
	}

	public RealEstateEntity getRealEstate(HttpServletRequest request, String realEstateID) {
		logger.trace("getLot(Request, {})");
		return getRealEstate(ActionBean.getConnection(request), us.getCurrentUser(request), realEstateID);
	}

	private RealEstateEntity getRealEstate(Connection con, UserBean user, String realEstateID) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(realEstateID)) {
			throw new IllegalArgumentException("You must supply a realEstateID to use this method");
		}
		RealEstateEntity product = new RealEstateEntity(con, realEstateID);
		if(!product.isLoaded()) {
			throw new NotFoundException("The real estate you requested could not be found: " + realEstateID);
		}
		product.setCurrentUser(user.getUser());
		if(!product.canView()) {
			throw new AccessException("You do not have permission to view the real estate with id " + realEstateID);
		}
		return product;
	}
	
	/*
	 * might be useful for updating other product types etc.
	 * 
	HomeDetails pd = p.Details;
	r.clear();
	r.setViewSpec((com.sok.framework.ViewSpec)null);
	r.setTableSpec("properties/HomeDetails");
	r.setParameter("ProductID", p.ProductID);
	
	if(update) {
		r.setAction(GenerationKeys.UPDATE);
	} else {
		r.setAction(GenerationKeys.INSERT);
	}
	
	r.setParameter("Area", pd.Area); 
	r.setParameter("Design", pd.Design);
	r.setParameter("Bedrooms", pd.Bedrooms);
	r.setParameter("Bathrooms", pd.Bathrooms);
	r.setParameter("CarParks", pd.CarParks);
	r.setParameter("Facade", pd.Facade);
	r.doAction();
	
	if(!r.isSuccessful()) {
		if(GenerationKeys.UPDATE.equals(r.getAction())) {
			logger.error("{} Save/Insert failed with error {}", r.toString(), r.getError());
			r.setAction(GenerationKeys.INSERT);
			r.doAction();
		}
		if(!r.isSuccessful())
			throw new RuntimeException(r.toString() + " Save/Insert failed with error " + r.getError());
	}
	*/

	private String copy(HttpServletRequest request, String tablespecname, String productidfieldname, String fromproductid, String toproductid, String userid)
	{

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);
		row.setRequest(request);
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		GenRow clone = new GenRow();
		try { 
			row.getResults();
			while (row.getNext()) {
				clone.clear();
				clone.setTableSpec(tablespecname);
				clone.setRequest(request);
				clone.putAllDataAsParameters(row);
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				if ("Products".endsWith(tablespecname) && "Home Plan".equals(row.getString("ProductType"))) {
					clone.setParameter("CopiedFromProductID", fromproductid);
					clone.setParameter("Category", "PlanSale");
					clone.setParameter("UpdateByProcess", "Y");
				}
				clone.remove(row.getTableSpec().getPrimaryKeyName());
				clone.createNewID();
				clone.setParameter(productidfieldname, toproductid);
				clone.doAction("insert");
			}
		} finally { 
			row.close();
		}
		return(clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	private void insertProductVariation(HttpServletRequest request, String parentproductid, String childproductid, String type){
		GenRow productvariation = new GenRow();
		productvariation.setRequest(request);
		productvariation.setTableSpec("ProductVariations");
		productvariation.setParameter("ParentProductID", parentproductid);
		productvariation.setParameter("ChildProductID", childproductid);
		productvariation.setParameter("Type", type);
		productvariation.createNewID();
		productvariation.doAction("insert");
	}

	public String duplicatePlan(HttpServletRequest request, String oldProductID, String userID){
		GenRow existingProduct = new GenRow();
		existingProduct.setRequest(request);
		existingProduct.setViewSpec("properties/ProductPlanView");
		existingProduct.setParameter("ProductID",oldProductID);
		existingProduct.doAction("selectfirst");

		String newproductid = KeyMaker.generate();

		copy(request, "Products", "ProductID", existingProduct.getString("ProductID"), newproductid, userID);
		GenRow duplicateProduct = new GenRow();
		duplicateProduct.setRequest(request);
		duplicateProduct.setTableSpec("Products");
		duplicateProduct.setParameter("ProductID",newproductid);
		duplicateProduct.setParameter("CopiedFromProductID",existingProduct.getString("ProductID"));
		if ("home Plan".equals(existingProduct.getString("ProductType"))) duplicateProduct.setParameter("Category","PlanSale");
		duplicateProduct.setParameter("UpdateByProcess","Y");
		duplicateProduct.doAction("update");

		copy(request, "ProductSecurityGroups", "ProductID", existingProduct.getString("ProductID"), newproductid , userID);
		copy(request, "LinkedDocuments", "ProductID", existingProduct.getString("ProductID"), newproductid, userID);
		insertProductVariation(request, existingProduct.getString("HomeProductID"), newproductid, "Home");
		copy(request, "properties/HomeDetails", "ProductID", existingProduct.getString("ProductID"), newproductid, userID);
		copy(request, "properties/ProductDetails", "ProductID", existingProduct.getString("ProductID"), newproductid, userID);
		copy(request, "ProductProducts", "ProductID", existingProduct.getString("ProductID"), newproductid, userID);
		copy(request, "ProductCommissions", "ProductID", existingProduct.getString("ProductID"), newproductid, userID); 

		return newproductid;
	}

	public SearchKey getMinMax(HttpServletRequest request, String searchKey) {
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		row.setRequest(request);
		row.setParameter("ProductType", "Home Plan");
		row.setParameter("-select0", "MIN(" + searchKey + ") AS Minimum");
		row.setParameter("-select1", "MAX(" + searchKey + ") AS Maximum");
		row.setParameter(searchKey, ">0");
		row.doAction("selectfirst");
		
		double min = 0, max = 0;
		
		try {
			min = Double.parseDouble(row.getString("Minimum"));
		} catch (Exception e) {
			System.out.println("Error Min " +  row.getString("Minimum"));
		}

		try {
			max = Double.parseDouble(row.getString("Maximum"));
		} catch (Exception e) {
			System.out.println("Error Min " +  row.getString("Minimum"));
		}

		return new SearchKey(searchKey, min, max);
	}

	public SearchKey getREMinMax(HttpServletRequest request, String searchKey) {
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		row.setRequest(request);
		row.setParameter("ProductType", "Existing Property");
		row.setParameter("-select0", "MIN(" + searchKey + ") AS Minimum");
		row.setParameter("-select1", "MAX(" + searchKey + ") AS Maximum");
		row.setParameter(searchKey, ">0");
		row.doAction("selectfirst");
		
		double min = 0, max = 0;
		
		try {
			min = Double.parseDouble(row.getString("Minimum"));
		} catch (Exception e) {
			System.out.println("Error Min " +  row.getString("Minimum"));
		}

		try {
			max = Double.parseDouble(row.getString("Maximum"));
		} catch (Exception e) {
			System.out.println("Error Min " +  row.getString("Minimum"));
		}

		return new SearchKey(searchKey, min, max);
	}

	public List<QuickIndexPlans> getPlansSearch(final HttpServletRequest request, String search, boolean filtered) {
		logger.trace("getLotsSearch(Request, {}, {})", search, filtered);
		return getPlansSearch(ActionBean.getConnection(request), us.getCurrentUser(request), search, filtered);
	}
	
	public List<QuickIndexPlans> getPlansSearch(final Connection conn, final UserBean user, String search, boolean filtered) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow planRow = getPlanSearch(conn, user, search, filtered);
		try {
			if(logger.isTraceEnabled()) {
				planRow.doAction(GenerationKeys.SEARCH);
				logger.trace(planRow.getStatement());
			}
			planRow.getResults(true);
			
			//System.out.println(planRow.getStatement());
			
			if(planRow.getNext()) {
				List<QuickIndexPlans> list = new ArrayList<QuickIndexPlans>(planRow.getSize());
				do {
					QuickIndexPlans qip = new QuickIndexPlans(planRow);
					if (planRow.isSet("ThumbDocumentID")) qip.setThumbnailImage(getImageDocument(conn, planRow.getString("ThumbDocumentID")));
					if (planRow.isSet("ImageDocumentID")) qip.setMainImage(getImageDocument(conn, planRow.getString("ImageDocumentID")));
					if (planRow.isSet("DesignThumbDocumentID")) qip.setDesignThumbnailImage(getImageDocument(conn, planRow.getString("DesignThumbDocumentID")));
					if (planRow.isSet("DesignImageDocumentID")) qip.setDesignMainImage(getImageDocument(conn, planRow.getString("DesignImageDocumentID")));
					qip.setStatusHistory(getStatus(planRow));
					list.add(qip);
				} while (planRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			planRow.close();
		}	
	}
	
	public List<QuickIndexRealEstate> getRealEstateSearch(final HttpServletRequest request, String search, boolean filtered) {
		logger.trace("getLotsSearch(Request, {}, {})", search, filtered);
		return getRealEstateSearch(ActionBean.getConnection(request), us.getCurrentUser(request), search, filtered);
	}
	
	public List<QuickIndexRealEstate> getRealEstateSearch(final Connection conn, final UserBean user, String search, boolean filtered) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow planRow = getRealEstatesSearch(conn, user, search, filtered);
		try {
			if(logger.isTraceEnabled()) {
				planRow.doAction(GenerationKeys.SEARCH);
				logger.trace(planRow.getStatement());
			}
			planRow.getResults(true);
			
			//System.out.println(planRow.getStatement());
			
			if(planRow.getNext()) {
				List<QuickIndexRealEstate> list = new ArrayList<QuickIndexRealEstate>(planRow.getSize());
				do {
					QuickIndexRealEstate qip = new QuickIndexRealEstate(planRow);
					if (planRow.isSet("ThumbDocumentID")) qip.setThumbnailImage(getImageDocument(conn, planRow.getString("ThumbDocumentID")));
					if (planRow.isSet("ImageDocumentID")) qip.setMainImage(getImageDocument(conn, planRow.getString("ImageDocumentID")));
					qip.setStatusHistory(getStatus(planRow));
					list.add(qip);
				} while (planRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			planRow.close();
		}	
	}
	
	private QuickIndexStatusHistory getStatus(GenRow packageRow) {
		QuickIndexStatusHistory qish = new QuickIndexStatusHistory();
		
		qish.setProductID(packageRow.getString("ProductID"));
		qish.setProductStatusID(packageRow.getString("ProductStatusID"));
		qish.setStatus(packageRow.getString("CurrentStatus"));
		qish.setStatusID(packageRow.getString("CurrentStatusID"));
		
		return qish;
	}
	private SimpleDocument getImageDocument(Connection conn, String documentID) {
		GenRow row = new GenRow();
		row.setTableSpec("Documents");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("DocumentID", documentID);
		row.doAction("selectfirst");
		
		if (row.isSet("FilePath")) {
			SimpleDocument sd = new SimpleDocument();
			sd.setDocumentID(documentID);
			sd.setFileName(row.getString("FileName"));
			sd.setFilePath(row.getString("FilePath"));
			
			/*
			sd.setCreatedDate(row.getDate("CreatedDate"));
			sd.setCreatedBy(row.getString("CreatedBy"));
			sd.setCreatedByName(new PersonName(row.getString("OwnFirstName"), row.getString("OwnLastName")));
			*/
			if (row.isSet("DocumentCaption")) sd.setDescription(row.getString("DocumentCaption"));
			else sd.setDescription(row.getString("Description"));

			sd.setType(row.getString("DocumentType"));
			sd.setSubType(row.getString("DocumentSubType"));
			
			return sd;
		}
		
		return null;
	}
	
	private GenRow getPlanSearch(final Connection conn, final UserBean user, final String search, boolean filter)
	{
		GenRow planRow = new GenRow();
		planRow.setViewSpec("properties/ProductQuickIndexDescriptionView");
		planRow.setConnection(conn);
		planRow.parseRequest(search);
		planRow.setParameter("-groupby0", "ProductID");
		planRow.setParameter("ProductType", "Home Plan");
		if (!planRow.isSet("DesignID")) planRow.setParameter("DesignID", "!NULL;!EMPTY");
		if (!planRow.isSet("RangeID")) planRow.setParameter("RangeID", "!NULL;!EMPTY");
		
		boolean params = search != null && search.length() > 0;
		if(filter) {
			planRow.setParameter("Active", "Y");
			String availableID = InitServlet.getSystemParam("ProductAvailableStatus");
			String availableIDs = InitServlet.getSystemParam("ProductAvailableAditionalStatuses");
			if (StringUtils.isBlank(availableIDs)) availableIDs = availableID;
			if (StringUtils.isNotBlank(availableIDs)) {
				planRow.setParameter("QuickIndexCurrentStatus.StatusID", availableIDs);
				planRow.setParameter("AvailableDate", "<=NOW+NULL");
				planRow.setParameter("ExpiryDate", ">=NOW+NULL");
			}
			boolean publishFilter = "true".equals(InitServlet.getSystemParam("API-SearchPublishWebsite"));
			if (publishFilter) planRow.setParameter("PublishedWebsite", "Y");
		}
		if(!params) {
			throw new IllegalArgumentException("Please provide the correct parameters for this API.");
		} else {
			if (planRow.getString("BrandID").length() > 0) {
				planRow.setParameter("BuilderID", planRow.getString("BrandID"));
			}
			if (planRow.getString("MarketingStatus").length() > 0) {
				planRow.setParameter("LotExclusivity", planRow.getString("MarketingStatus"));
			}
			if (planRow.getString("HomeWidth").length() > 0) {
				planRow.setParameter("HomeWidth", planRow.getString("HomeWidth"));
			}
			if (planRow.getString("HomeDepth").length() > 0) {
				planRow.setParameter("HomeDepth", planRow.getString("HomeDepth"));
			}
			if (planRow.getString("HomeArea").length() > 0) {
				planRow.setParameter("HomeArea", planRow.getString("HomeArea"));
			}
			if (planRow.getString("HomePrice").length() > 0) {
				planRow.setParameter("HomePrice", planRow.getString("HomePrice"));
			}
			if (planRow.getString("BuildWidth").length() > 0) {
				planRow.setParameter("BuildWidth", planRow.getString("BuildWidth"));
			}
			if (planRow.getString("BuildDepth").length() > 0) {
				planRow.setParameter("BuildDepth", planRow.getString("BuildDepth"));
			}
			if (planRow.getString("CanFitOnDepth").length() > 0) {
				planRow.setParameter("CanFitOnDepth", planRow.getString("CanFitOnDepth"));
			}
			if (planRow.getString("CanFitOnWidth").length() > 0) {
				planRow.setParameter("CanFitOnWidth", planRow.getString("CanFitOnWidth"));
			}
			if (planRow.getString("Storeys").length() > 0) {
				planRow.setParameter("Storeys", planRow.getString("Storeys"));
			}
			if (planRow.getString("Bedrooms").length() > 0) {
				planRow.setParameter("Bedrooms", planRow.getString("Bedrooms"));
			}
			if (planRow.getString("Bathrooms").length() > 0) {
				planRow.setParameter("Bathrooms", planRow.getString("Bathrooms"));
			}
			if (planRow.getString("CarParks").length() > 0) {
				planRow.setParameter("CarParks", planRow.getString("CarParks"));
			}
			if (planRow.getString("Study").length() > 0) {
				planRow.setParameter("Study", planRow.getString("Study") + "+NULL+EMPTY");
			}
			if (planRow.getString("Garage").length() > 0) {
				planRow.setParameter("Garage", planRow.getString("Garage") + "+NULL+EMPTY");
			}
			if (planRow.getString("LocationName").length() > 0) {
				planRow.setParameter("QuickIndexDisplayEntity.Name", planRow.getString("LocationName"));
				planRow.remove("LocationName");
			}
			if (planRow.getString("LocationNum").length() > 0) {
				planRow.setParameter("QuickIndexDisplayEntity.DisplayEntityProduct.ProductNum", planRow.getString("LocationNum"));
				planRow.remove("LocationNum");
			}
			if (planRow.getString("KeyWord").length() > 0) {
				String keyWord = "%" + planRow.getString("KeyWord").replaceAll("\\\\+", "%+%").replaceAll(" ", "%+%").replaceAll(";", "%;%") + "%";
				planRow.remove("KeyWord");
				planRow.setParameter("ProductQuickIndexProductDescriptionWebsite.HeadLine|k", keyWord);
				planRow.setParameter("ProductQuickIndexProductDescriptionWebsite.Description|k", keyWord);
			}
		}
		
		if (planRow.getString("-sort0").length() == 0 ) {
			planRow.setParameter("-sort0","Name");
		} else {
			for (int s = 0; s < 10; ++s) {
				if ("MarketingStatus".equals(planRow.getString("-sort" + s))) planRow.sortBy("LotExclusivity", s);
			}
		}
		
		planRow.setParameter("-groupby0", "ProductID");
		
		return planRow;
	}
	
	private GenRow getRealEstatesSearch(final Connection conn, final UserBean user, final String search, boolean filter)
	{
		GenRow packageRow = new GenRow();
		packageRow.setViewSpec("properties/ProductQuickIndexDescriptionView");
		packageRow.setConnection(conn);
		packageRow.parseRequest(search);
		//packageRow.setParameter("-select0", "*");
		packageRow.setParameter("ProductType", "Existing Property");
		
		boolean params = search != null && search.length() > 0;
		if(filter) {
			packageRow.setParameter("Active", "Y");
			String availableID = InitServlet.getSystemParam("ProductAvailableStatus");
			String availableIDs = InitServlet.getSystemParam("ProductAvailableAditionalStatuses");
			if (StringUtils.isBlank(availableIDs)) availableIDs = availableID;
			if (StringUtils.isNotBlank(availableIDs)) {
				packageRow.setParameter("QuickIndexCurrentStatus.StatusID", availableIDs);
				packageRow.setParameter("AvailableDate", "<=NOW+NULL");
				packageRow.setParameter("ExpiryDate", ">=NOW+NULL");
			}
			boolean publishFilter = "true".equals(InitServlet.getSystemParam("API-SearchPublishWebsite"));
			if (publishFilter) packageRow.setParameter("PublishedWebsite", "Y");
		}
		if(!params) {
			throw new IllegalArgumentException("Please provide the correct parameters for this API.");
		} else {
			if (packageRow.getString("LotName").length() > 0) {
				String lotName = packageRow.getString("LotName");
				packageRow.remove("LotName");
				packageRow.setParameter("Name|l", lotName);
				packageRow.setParameter("Name|l", "Lot " + lotName);
			}
			if (packageRow.getString("StageName").length() > 0) {
				String stageName = packageRow.getString("StageName");
				packageRow.remove("StageName");
				packageRow.setParameter("StageName|s", stageName);
				packageRow.setParameter("StageName|s", "Stage " + stageName);
			}
			if (packageRow.getString("MarketingStatus").length() > 0) {
				packageRow.setParameter("LotExclusivity", packageRow.getString("MarketingStatus"));
			}
			if (packageRow.getString("LotWidth").length() > 0) {
				packageRow.setParameter("LotWidth", packageRow.getString("LotWidth"));
			}
			if (packageRow.getString("LotDepth").length() > 0) {
				packageRow.setParameter("LotDepth", packageRow.getString("LotDepth"));
			}
			if (packageRow.getString("LotArea").length() > 0) {
				packageRow.setParameter("LotArea", packageRow.getString("LotArea"));
			}
			if (packageRow.getString("LotPrice").length() > 0) {
				packageRow.setParameter("LotPrice", packageRow.getString("LotPrice"));
			}
			if (packageRow.getString("HomeWidth").length() > 0) {
				packageRow.setParameter("HomeWidth", packageRow.getString("HomeWidth"));
			}
			if (packageRow.getString("HomeDepth").length() > 0) {
				packageRow.setParameter("HomeDepth", packageRow.getString("HomeDepth"));
			}
			if (packageRow.getString("HomeArea").length() > 0) {
				packageRow.setParameter("HomeArea", packageRow.getString("HomeArea"));
			}
			if (packageRow.getString("HomePrice").length() > 0) {
				packageRow.setParameter("HomePrice", packageRow.getString("HomePrice"));
			}
			if (packageRow.getString("BuildWidth").length() > 0) {
				packageRow.setParameter("BuildWidth", packageRow.getString("BuildWidth"));
			}
			if (packageRow.getString("BuildDepth").length() > 0) {
				packageRow.setParameter("BuildDepth", packageRow.getString("BuildDepth"));
			}
			if (packageRow.getString("CanFitOnDepth").length() > 0) {
				packageRow.setParameter("CanFitOnDepth", packageRow.getString("CanFitOnDepth"));
			}
			if (packageRow.getString("CanFitOnWidth").length() > 0) {
				packageRow.setParameter("CanFitOnWidth", packageRow.getString("CanFitOnWidth"));
			}
			if (packageRow.getString("Storeys").length() > 0) {
				packageRow.setParameter("Storeys", packageRow.getString("Storeys"));
			}
			if (packageRow.getString("Bedrooms").length() > 0) {
				packageRow.setParameter("Bedrooms", packageRow.getString("Bedrooms"));
			}
			if (packageRow.getString("Bathrooms").length() > 0) {
				packageRow.setParameter("Bathrooms", packageRow.getString("Bathrooms"));
			}
			if (packageRow.getString("CarParks").length() > 0) {
				packageRow.setParameter("CarParks", packageRow.getString("CarParks"));
			}
			if (packageRow.getString("Study").length() > 0) {
				packageRow.setParameter("Study", packageRow.getString("Study"));
			}
			if (packageRow.getString("Garage").length() > 0) {
				packageRow.setParameter("Garage", packageRow.getString("Garage"));
			}
			if (packageRow.getString("LocationName").length() > 0) {
				packageRow.setParameter("QuickIndexDisplayEntity.Name", packageRow.getString("LocationName"));
				packageRow.remove("LocationName");
			}
			if (packageRow.getString("LocationNum").length() > 0) {
				packageRow.setParameter("QuickIndexDisplayEntity.DisplayEntityProduct.ProductNum", packageRow.getString("LocationNum"));
				packageRow.remove("LocationNum");
			}
			if (packageRow.getString("KeyWord").length() > 0) {
				String keyWord = "%" + packageRow.getString("KeyWord").replaceAll("\\\\+", "%+%").replaceAll(" ", "%+%").replaceAll(";", "%;%") + "%";
				packageRow.remove("KeyWord");
				packageRow.setParameter("ProductQuickIndexProductDescriptionWebsite.HeadLine|k", keyWord);
				packageRow.setParameter("ProductQuickIndexProductDescriptionWebsite.Description|k", keyWord);
			}
		}
		
		//packageRow.setParameter("-sort0","RangeName");
		//packageRow.setParameter("-sort1","DesignName");
		if (packageRow.getString("-sort0").length() == 0 ) {
			packageRow.setParameter("-sort0","Name");
		} else {
			for (int s = 0; s < 10; ++s) {
				if ("MarketingStatus".equals(packageRow.getString("-sort" + s))) packageRow.sortBy("LotExclusivity", s);
			}
		}
		
		packageRow.setParameter("-groupby0", "ProductID");
		return packageRow;
	}

	public List<QuickIndexPlanCollections> getPlanCollectionSearch(final HttpServletRequest request, String search, String collection) {
		logger.trace("getApartmentsSearch(Request, {}, {})", search, collection);
		return getPlanCollectionSearch(ActionBean.getConnection(request), us.getCurrentUser(request), search, collection);
	}
	
	public List<QuickIndexPlanCollections> getPlanCollectionSearch(final Connection conn, final UserBean user, String search, String collection) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow planRow = getPlanSearch(conn, user, search, true);
		try {
			String[] parts = null;
			if (StringUtils.isNotBlank(collection)) {
				parts = collection.split("\\+");
				if (parts != null && parts.length > 0) {
					for (int p = 0; p < parts.length; ++p) {
						if ("MarketingStatus".equals(parts[p])) parts[p] = "LotExclusivity";
						if ("OriginalPlanID".equals(parts[p])) parts[p] = "CopiedFromID";
						planRow.setParameter("-groupby" + p, parts[p]);
					}
				}
			}
			if(logger.isTraceEnabled()) {
				planRow.doAction(GenerationKeys.SEARCH);
				logger.trace(planRow.getStatement());
			}
			planRow.getResults();
			
			System.out.println(planRow.getStatement());
			
			if(planRow.getNext()) {
				List<QuickIndexPlanCollections> list = new ArrayList<QuickIndexPlanCollections>(planRow.getSize());
				do {
					QuickIndexPlanCollections qip = new QuickIndexPlanCollections(planRow);
					//if (planRow.isSet("ThumbDocumentID")) qip.setThumbnailImage(getImageDocument(conn, planRow.getString("ThumbDocumentID")));
					//if (planRow.isSet("ImageDocumentID")) qip.setMainImage(getImageDocument(conn, planRow.getString("ImageDocumentID")));
					//qip.setStatusHistory(getStatus(planRow));
					String local = search;
					if (parts != null && parts.length > 0) {
						for (int p = 0; p < parts.length; ++p) {
							if (local.length() > 0) local += "&";
							if (planRow.isSet(parts[p]))
								local += parts[p] + "=" + planRow.getData(parts[p]);
							else
								local += parts[p] + "=EMPTY%2BNULL";
						}
					}
					qip.setPlanList(getPlansSearch(conn, user, local, true));
					list.add(qip);
				} while (planRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			planRow.close();
		}	
	}
	
	public List<QuickIndexRealEstateCollections> getRealEstateCollectionSearch(final HttpServletRequest request, String search, String collection) {
		logger.trace("getApartmentsSearch(Request, {}, {})", search, collection);
		return getRealEstateCollectionSearch(ActionBean.getConnection(request), us.getCurrentUser(request), search, collection);
	}
	
	public List<QuickIndexRealEstateCollections> getRealEstateCollectionSearch(final Connection conn, final UserBean user, String search, String collection) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow planRow = getRealEstatesSearch(conn, user, search, true);
		try {
			String[] parts = null;
			if (StringUtils.isNotBlank(collection)) {
				parts = collection.split("\\+");
				if (parts != null && parts.length > 0) {
					for (int p = 0; p < parts.length; ++p) {
						if ("MarketingStatus".equals(parts[p])) parts[p] = "LotExclusivity";
						planRow.setParameter("-groupby" + p, parts[p]);
					}
				}
			}
			if(logger.isTraceEnabled()) {
				planRow.doAction(GenerationKeys.SEARCH);
				logger.trace(planRow.getStatement());
			}
			planRow.getResults(true);
			
			System.out.println(planRow.getStatement());
			
			if(planRow.getNext()) {
				List<QuickIndexRealEstateCollections> list = new ArrayList<QuickIndexRealEstateCollections>(planRow.getSize());
				do {
					QuickIndexRealEstateCollections qip = new QuickIndexRealEstateCollections(planRow);
					//if (planRow.isSet("ThumbDocumentID")) qip.setThumbnailImage(getImageDocument(conn, planRow.getString("ThumbDocumentID")));
					//if (planRow.isSet("ImageDocumentID")) qip.setMainImage(getImageDocument(conn, planRow.getString("ImageDocumentID")));
					//qip.setStatusHistory(getStatus(planRow));
					String local = search;
					if (parts != null && parts.length > 0) {
						for (int p = 0; p < parts.length; ++p) {
							if (local.length() > 0) local += "&";
							if (planRow.isSet(parts[p]))
								local += parts[p] + "=" + planRow.getData(parts[p]);
							else
								local += parts[p] + "=EMPTY%2BNULL";
						}
					}
					qip.setRealEstateList(getRealEstateSearch(conn, user, local, true));
					list.add(qip);
				} while (planRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			planRow.close();
		}	
	}
	
	private static void processProductDetails(GenRow row, String productID, Map<String,String> map) {
		TableSpec detailsSpec = SpecManager.getTableSpec("properties/ProductDetails");
		String ignorelist = "ProductID,CopiedFromProductID,SortOrder";

		GenRow home = new GenRow(); 
		home.setTableSpec("properties/HomeDetails");
		home.setParameter("ProductID",productID);
		
		boolean homeSet = false;
		String typeID = row.getString("DetailsType");
		String productType = row.getString("ProductType").replaceAll("Details", "");
		for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
			if (ignorelist.indexOf(detailsSpec.getFieldName(cs)) == -1) {
				if (map.containsKey(productType + "." +  typeID + "." + detailsSpec.getFieldName(cs))) {
					home.setColumn(map.get(productType + "." +  typeID + "." + detailsSpec.getFieldName(cs)), row.getString(detailsSpec.getFieldName(cs)));
					homeSet = true;
				}
			}
		}
		if (homeSet) {
			try {
				home.doAction("insert");
			} catch (Exception e) {
				home.doAction("update");
			}
		}
	}
	
	public Map<String, String> getDetailsMap(String productType) {
		Map<String,String> map = new HashMap<String,String>();
		
		String f = "Product" + productType.replaceAll(" ", "").replaceFirst("Details", "") + "ViewDisplay";
		if ("Land".equals(productType) || "LandDetails".equals(productType)) f = "ProductLotViewDisplay";
		if ("HomePlan".equals(productType.replaceAll(" ", "")) || "HomePlanDetails".equals(productType.replaceAll(" ", ""))) f = "ProductPlanViewDisplay";
		
	    DisplaySpec dspecLot = SpecManager.getDisplaySpec(f);

	    String[] names = dspecLot.getItemNames();
		for (int d = 0; d < names.length; ++d) {
			map.put(dspecLot.getLocalItemFieldName(names[d]), names[d]);
			
		}
		
		return map;
	}

	public List<QuickIndex> getProductsSearch(HttpServletRequest request,
			String query, boolean filtered) {
		logger.trace("getLotsSearch(Request, {}, {})", query, filtered);
		return getProductsSearch(ActionBean.getConnection(request), us.getCurrentUser(request), query, filtered);
	}

	private List<QuickIndex> getProductsSearch(Connection conn,
			UserBean user, String query, boolean filtered) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		GenRow planRow = getProductSearch(conn, user, query, filtered);
		try {
			if(logger.isTraceEnabled()) {
				planRow.doAction(GenerationKeys.SEARCH);
				logger.trace(planRow.getStatement());
			}
			planRow.getResults(true);
			
			//System.out.println(planRow.getStatement());
			
			if(planRow.getNext()) {
				List<QuickIndex> list = new ArrayList<QuickIndex>(planRow.getSize());
				do {
					QuickIndex qip = null;
					if ("Home Plan".equals(planRow.getData("ProductType"))) {
						qip = new QuickIndexPlans(planRow);
						if (planRow.isSet("DesignThumbDocumentID")) ((QuickIndexPlans) qip).setDesignThumbnailImage(getImageDocument(conn, planRow.getString("DesignThumbDocumentID")));
						if (planRow.isSet("DesignImageDocumentID")) ((QuickIndexPlans) qip).setDesignMainImage(getImageDocument(conn, planRow.getString("DesignImageDocumentID")));
					} else if ("House and Land".equals(planRow.getData("ProductType"))) {
						qip = new QuickIndexPackages(planRow);
						if (planRow.isSet("DesignThumbDocumentID")) ((QuickIndexPackages) qip).setDesignThumbnailImage(getImageDocument(conn, planRow.getString("DesignThumbDocumentID")));
						if (planRow.isSet("DesignImageDocumentID")) ((QuickIndexPackages) qip).setDesignMainImage(getImageDocument(conn, planRow.getString("DesignImageDocumentID")));
					} else if ("Existing Property".equals(planRow.getData("ProductType"))) {
						qip = new QuickIndexRealEstate(planRow);
						if (planRow.isSet("DesignThumbDocumentID")) ((QuickIndexRealEstate) qip).setDesignThumbnailImage(getImageDocument(conn, planRow.getString("DesignThumbDocumentID")));
						if (planRow.isSet("DesignImageDocumentID")) ((QuickIndexRealEstate) qip).setDesignMainImage(getImageDocument(conn, planRow.getString("DesignImageDocumentID")));
					} else if ("Apartment".equals(planRow.getData("ProductType"))) {
						GenRow row = new GenRow();
						row.setViewSpec("properties/ProductQuickIndexApartmentDescriptionView");
						row.setConnection(conn);
						row.setParameter("ProductID", planRow.getData("ProductID"));
						row.setParameter("ProductType", "Apartment");
						row.doAction("selectfirst");
						qip = new QuickIndexApartments(row);
					} else if ("Land".equals(planRow.getData("ProductType"))) {
						qip = new QuickIndexLot(planRow);
					}
					if (planRow.isSet("ThumbDocumentID")) qip.setThumbnailImage(getImageDocument(conn, planRow.getString("ThumbDocumentID")));
					if (planRow.isSet("ImageDocumentID")) qip.setMainImage(getImageDocument(conn, planRow.getString("ImageDocumentID")));
					qip.setStatusHistory(getStatus(planRow));
					list.add(qip);
				} while (planRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			planRow.close();
		}	
	}

	private GenRow getProductSearch(final Connection conn, final UserBean user, final String search, boolean filter)
	{
		GenRow planRow = new GenRow();
		planRow.setViewSpec("properties/ProductQuickIndexDescriptionView");
		planRow.setConnection(conn);
		planRow.parseRequest(search);
		planRow.setParameter("-groupby0", "ProductID");
		planRow.setParameter("ProductType", "Home Plan+House and Land+Existing Property+Apartment+Land");
		
		boolean params = search != null && search.length() > 0;
		if(filter) {
			planRow.setParameter("Active", "Y");
			String availableID = InitServlet.getSystemParam("ProductAvailableStatus");
			String availableIDs = InitServlet.getSystemParam("ProductAvailableAditionalStatuses");
			if (StringUtils.isBlank(availableIDs)) availableIDs = availableID;
			if (StringUtils.isNotBlank(availableIDs)) {
				planRow.setParameter("QuickIndexCurrentStatus.StatusID", availableIDs);
				planRow.setParameter("AvailableDate", "<=NOW+NULL");
				planRow.setParameter("ExpiryDate", ">=NOW+NULL");
			}
			boolean publishFilter = "true".equals(InitServlet.getSystemParam("API-SearchPublishWebsite"));
			if (publishFilter) planRow.setParameter("PublishedWebsite", "Y");
		}
		if(!params) {
			throw new IllegalArgumentException("Please provide the correct parameters for this API.");
		} else {
			if (planRow.getString("BrandID").length() > 0) {
				planRow.setParameter("BuilderID", planRow.getString("BrandID"));
			}
			if (planRow.getString("MarketingStatus").length() > 0) {
				planRow.setParameter("LotExclusivity", planRow.getString("MarketingStatus"));
			}
			if (planRow.getString("HomeWidth").length() > 0) {
				planRow.setParameter("HomeWidth", planRow.getString("HomeWidth"));
			}
			if (planRow.getString("HomeDepth").length() > 0) {
				planRow.setParameter("HomeDepth", planRow.getString("HomeDepth"));
			}
			if (planRow.getString("HomeArea").length() > 0) {
				planRow.setParameter("HomeArea", planRow.getString("HomeArea"));
			}
			if (planRow.getString("HomePrice").length() > 0) {
				planRow.setParameter("HomePrice", planRow.getString("HomePrice"));
			}
			if (planRow.getString("BuildWidth").length() > 0) {
				planRow.setParameter("BuildWidth", planRow.getString("BuildWidth"));
			}
			if (planRow.getString("BuildDepth").length() > 0) {
				planRow.setParameter("BuildDepth", planRow.getString("BuildDepth"));
			}
			if (planRow.getString("CanFitOnDepth").length() > 0) {
				planRow.setParameter("CanFitOnDepth", planRow.getString("CanFitOnDepth"));
			}
			if (planRow.getString("CanFitOnWidth").length() > 0) {
				planRow.setParameter("CanFitOnWidth", planRow.getString("CanFitOnWidth"));
			}
			if (planRow.getString("Storeys").length() > 0) {
				planRow.setParameter("Storeys", planRow.getString("Storeys"));
			}
			if (planRow.getString("Bedrooms").length() > 0) {
				planRow.setParameter("Bedrooms", planRow.getString("Bedrooms"));
			}
			if (planRow.getString("Bathrooms").length() > 0) {
				planRow.setParameter("Bathrooms", planRow.getString("Bathrooms"));
			}
			if (planRow.getString("CarParks").length() > 0) {
				planRow.setParameter("CarParks", planRow.getString("CarParks"));
			}
			if (planRow.getString("Study").length() > 0) {
				planRow.setParameter("Study", planRow.getString("Study") + "+NULL+EMPTY");
			}
			if (planRow.getString("Garage").length() > 0) {
				planRow.setParameter("Garage", planRow.getString("Garage") + "+NULL+EMPTY");
			}
			if (planRow.getString("LocationName").length() > 0) {
				planRow.setParameter("QuickIndexDisplayEntity.Name", planRow.getString("LocationName"));
				planRow.remove("LocationName");
			}
			if (planRow.getString("LotName").length() > 0) {
				String lotName = planRow.getString("LotName");
				planRow.remove("LotName");
				planRow.setParameter("Name|l", lotName);
				planRow.setParameter("Name|l", "Lot " + lotName);
			}
			if (planRow.getString("StageName").length() > 0) {
				String stageName = planRow.getString("StageName");
				planRow.remove("StageName");
				planRow.setParameter("StageName|s", stageName);
				planRow.setParameter("StageName|s", "Stage " + stageName);
			}
			if (planRow.getString("LotWidth").length() > 0) {
				planRow.setParameter("LotWidth", planRow.getString("LotWidth"));
			}
			if (planRow.getString("LotDepth").length() > 0) {
				planRow.setParameter("LotDepth", planRow.getString("LotDepth"));
			}
			if (planRow.getString("LotArea").length() > 0) {
				planRow.setParameter("LotArea", planRow.getString("LotArea"));
			}
			if (planRow.getString("LotPrice").length() > 0) {
				planRow.setParameter("LotPrice", planRow.getString("LotPrice"));
			}
			if (planRow.getString("LocationName").length() > 0) {
				planRow.setParameter("QuickIndexDisplayEntity.Name", planRow.getString("LocationName"));
				planRow.remove("LocationName");
			}
			if (planRow.getString("LocationNum").length() > 0) {
				planRow.setParameter("QuickIndexDisplayEntity.DisplayEntityProduct.ProductNum", planRow.getString("LocationNum"));
				planRow.remove("LocationNum");
			}
			if (planRow.getString("KeyWord").length() > 0) {
				String keyWord = "%" + planRow.getString("KeyWord").replaceAll("\\\\+", "%+%").replaceAll(" ", "%+%").replaceAll(";", "%;%") + "%";
				planRow.remove("KeyWord");
				planRow.setParameter("ProductQuickIndexProductDescriptionWebsite.HeadLine|k", keyWord);
				planRow.setParameter("ProductQuickIndexProductDescriptionWebsite.Description|k", keyWord);
			}
		}
		
		if (planRow.getString("-sort0").length() == 0 ) {
			planRow.setParameter("-sort0","Name");
		} else {
			for (int s = 0; s < 10; ++s) {
				if ("MarketingStatus".equals(planRow.getString("-sort" + s))) planRow.sortBy("LotExclusivity", s);
			}
		}
		
		planRow.setParameter("-groupby0", "ProductID");
		
		return planRow;
	}
	
	public static void updateProductFeedAgentDetails(final Connection conn, String REAListingID, String productID, String userID) {
			try {
				GenRow productFeedDetails = new GenRow();
				productFeedDetails.setConnection(conn);
				productFeedDetails.setViewSpec("properties/ProductFeedAgentDetailsView");
				productFeedDetails.setParameter("ProductID", productID);
				productFeedDetails.setParameter("REAListingID", REAListingID);			
				productFeedDetails.setParameter("ModifiedBy", userID);
				productFeedDetails.setParameter("ModifiedDate", "NOW");
				productFeedDetails.doAction("update");
	
				if (!productFeedDetails.isSuccessful()) {
					productFeedDetails.setParameter("CreatedBy", userID);
					productFeedDetails.setParameter("CreatedDate", "NOW");
					productFeedDetails.doAction("insert");
				}
			} catch (Exception e) {
				logger.error("Error updating ProductFeedAgentDetails" + e);
				e.printStackTrace();
			}
	}

	
}