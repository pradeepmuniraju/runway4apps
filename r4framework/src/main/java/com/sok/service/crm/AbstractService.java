package com.sok.service.crm;

import com.sok.framework.ActionBean;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractService {
	
	private static final Pattern stripPhoneNumberPattern = Pattern.compile("(\\(\\d\\d\\))|([^0-9&&[^\\s]])");
	public abstract Logger getLogger();
	
	protected String getDecodedParameter(String fieldName, HttpServletRequest request, String label) {
        final String fieldValue = fieldName != null ? 
        		StringUtils.trimToNull(request.getParameter(fieldName))
        	: null; 
        if (fieldValue == null || fieldValue.equals("empty") || fieldValue.equals(label)) {
            return ActionBean._blank;
        }
        try {
            return URLDecoder.decode(fieldValue, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        	getLogger().error("Not sure how this occurred - via displaymatches", e);
        }
        return fieldValue;
    }

    protected String stripPhoneNumber(String phoneNumber) {
        String strippedPhoneNumber = new String(phoneNumber);

        // Remove all spaces, two digit Area Codes enclosed in brackets: (XX),
        // and any other lingering characters like brackets, dashes, etc
        Matcher stripPhoneNumberMatcher = stripPhoneNumberPattern.matcher(phoneNumber);
        strippedPhoneNumber = stripPhoneNumberMatcher.replaceAll("");

        return strippedPhoneNumber;
    }

}
