package com.sok.service.crm.cms.properties;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.APLRecord;

import com.sok.framework.ActionBean;
import com.sok.framework.Conditions;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.RunwayUtil;
import com.sok.framework.SpecManager;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.TableSpec;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.URLFilter;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.HomePlan;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.templates.model.Package;
import com.sok.runway.externalInterface.beans.Order;
import com.sok.service.crm.OrderService;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.DripService.DripProduct;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

/**
 * Drip Service 
 * 
 * A service to return drip libraries or drips in a consistent format.
 * TODO - future: Update to allow create / update / delete of drips / drip libraries.
 * 
 * @author Michael Dekmetzian
 * @version 0.1
 * @date 22 May 2012
 */
public class DripService {

	private static final DripService service = new DripService();
	private static final Logger logger = LoggerFactory.getLogger(DripService.class);

	private final NumberFormat currency = NumberFormat.getCurrencyInstance();
	private final NumberFormat number = NumberFormat.getNumberInstance();

	private TableSpec detailsSpec = SpecManager.getTableSpec("properties/ProductDetails");

	private List<DripProduct> postDrips = new ArrayList<DripService.DripProduct>();

	private DripCollection dripCollection = null;

	/* TODO - private */
	protected static final UserService us = UserService.getInstance();

	private boolean estateDone = false;

	public static DripService getInstance() {
		return service;
	}

	/* request methods */
	public List<DripLibrary> getDripLibraries(final HttpServletRequest request) {
		logger.debug("getDripLibraries(Request)");
		return getDripLibraries(ActionBean.getConnection(request), us.getCurrentUser(request));
	}

	public DripLibrary getDripLibrary(final HttpServletRequest request, final String productLibraryID) {
		logger.debug("getDripLibrary(Request, {})");
		return getDripLibrary(ActionBean.getConnection(request), us.getCurrentUser(request), productLibraryID);
	}

	/**
	 * A convenience method to return a list of the currently available drips, for use in search screens and similar.
	 * @param request
	 * @param activeOnly
	 * @return	a list of drip products
	 */
	public List<DripProduct> getDripProducts(HttpServletRequest request, boolean activeOnly) {
		return getDripProducts(ActionBean.getConnection(request), us.getCurrentUser(request), activeOnly);
	}

	/**
	 * @deprecated 	- this is out of date and should not be used.
	 */
	public List<DripProduct> getDripProducts(final HttpServletRequest request, final String productID, final String productLibraryID) {
		logger.debug("getDripProducts(Request, {})", productLibraryID);
		return getDripProducts(ActionBean.getConnection(request), us.getCurrentUser(request), productID, productLibraryID);
	}

	/**
	 * @deprecated - this was never implemented and is out of date.
	 */
	public DripProduct getDripProduct(final HttpServletRequest request, final String productLibraryID, final String dripProductID) {
		logger.debug("getDripLibrary(Request, {})");
		return getDripProduct(ActionBean.getConnection(request), us.getCurrentUser(request), productLibraryID, dripProductID);
	}

	/** Returns all the drips associated with the productIDs
	 * @param request
	 * @param productIDs
	 * @return
	 */
	public DripCollection getAllDrips(final HttpServletRequest request, final String[] productIDs, final Date date) {
		logger.debug("getDripLibrary(Request, {})");
		return getAllDrips(ActionBean.getConnection(request), us.getCurrentUser(request), productIDs, date);
	}

	public List<GenRow> getDripList(final HttpServletRequest request) {
		logger.debug("getDripList(Request)");
		return getDripList(ActionBean.getConnection(request), us.getCurrentUser(request));
	}

	public List<GenRow> getArchiveAndAvailableDripList(final HttpServletRequest request) {
		logger.debug("getDripList(Request)");
		return getArchiveAndAvailableDripList(ActionBean.getConnection(request), us.getCurrentUser(request));
	}

	public String getDripImage(final HttpServletRequest request, final String[] productIDs, final String type, final String template, final String placement) {
		logger.debug("getDripImage(Request, {})");
		return getDripImage(ActionBean.getConnection(request), us.getCurrentUser(request), productIDs, type, template, placement);
	}

	public String getDripText(final HttpServletRequest request, final String[] productIDs, final String type, final String template, final String placement) {
		logger.debug("getDripText(Request, {})");
		return getDripText(ActionBean.getConnection(request), us.getCurrentUser(request), productIDs, type, template, placement);
	}

	public List<String> getDripImages(final HttpServletRequest request, final String[] productIDs, final String type, final String template, final String placement) {
		logger.debug("getPromoImage(Request, {})");
		return getDripImages(ActionBean.getConnection(request), us.getCurrentUser(request), productIDs, type, template, placement);
	}

	public List<String> getDripTexts(final HttpServletRequest request, final String[] productIDs, final String type, final String template, final String placement) {
		logger.debug("getPromoImage(Request, {})");
		return getDripTexts(ActionBean.getConnection(request), us.getCurrentUser(request), productIDs, type, template, placement);
	}


	public HashMap<String, String> getDripMapForTemplates(final HttpServletRequest request, final String[] productIDs, final String subType, final String template) {
		if(logger.isDebugEnabled()) logger.debug("getDripMapForTemplates(Request, [{}], {}, {})", new String[]{StringUtils.join(productIDs, ","), subType, template} );
		//URLFilter.exceptionLogger(this, request, null);
		return getDripMapForTemplates(ActionBean.getConnection(request), us.getCurrentUser(request), productIDs, subType, template, request.getParameter("DRIP"));
	}

	/* internal methods  */ 
	public List<DripLibrary> getDripLibraries(final Connection conn, final UserBean user) {
		logger.debug("getDripLibraries(Conn, {})", user != null ? user.getName() : "null user");
		//if(user == null) {
		//	throw new NotAuthenticatedException("You are not logged in");
		//}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}

		GenRow productLibraries = getLibraryGenRow();
		try {
			productLibraries.setConnection(conn);
			productLibraries.sortBy("Name",0);
			if(logger.isTraceEnabled()) { 
				productLibraries.doAction("search");
				logger.trace(productLibraries.getStatement());
			}
			productLibraries.getResults(true);

			if(productLibraries.getNext()) {
				List<DripLibrary> list = new ArrayList<DripLibrary>(productLibraries.getSize());
				do {
					list.add(new DripLibrary(productLibraries));
				} while(productLibraries.getNext());

				return list;
			}
			return Collections.emptyList();
		} finally { 
			productLibraries.close();
		}
	}

	private GenRow getLibraryGenRow() {

		GenRow productLibraries = new GenRow();
		productLibraries.setTableSpec("ProductLibraries");
		productLibraries.setParameter("-select0","Name");
		productLibraries.setParameter("-select1","Description");
		productLibraries.setParameter("-select2","ProductLibraryID");
		productLibraries.setParameter("-select3","Constraints");
		productLibraries.setParameter("-select4", "ProductIDs"); 
		productLibraries.setParameter("Type", "Drip Library");

		return productLibraries;
	}
	public DripLibrary getDripLibrary(final Connection conn, final String userId, final String productLibraryID) {
		UserBean user = us.getSimpleUser(conn, userId);
		return getDripLibrary(conn, user, productLibraryID);
	}

	public DripLibrary getDripLibrary(final Connection conn, final UserBean user, final String productLibraryID) {
		logger.debug("getDripLibrary(Conn, {}, {})", user != null ? user.getName() : "null user", productLibraryID);
		//if(user == null) {
		//	throw new NotAuthenticatedException("You are not logged in");
		//}
		if(user != null && !user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(productLibraryID)) { 
			throw new IllegalArgumentException("product library id must be supplied");
		}
		GenRow productLibraries = getLibraryGenRow();
		productLibraries.setConnection(conn);
		productLibraries.setParameter("ProductLibraryID", productLibraryID);
		productLibraries.doAction(GenerationKeys.SELECT);
		if(!productLibraries.isSuccessful()) { 
			throw new NotFoundException("The drip library could not be found : " + productLibraryID);
		} 
		return new DripLibrary(productLibraries);
	}

	private GenRow getDripListGenRow(final String productID, final String dripIDs) {
		GenRow drips = new GenRow();
		drips.setViewSpec("ProductLinkedProductView");
		drips.setColumn("ProductID", productID);
		drips.setColumn("LinkedProductID", dripIDs);
		drips.setColumn("ProductType", "Drip+Drip Library");
		drips.setColumn("ProductProductsLinkedProduct.ProductType", "Drip+Drip Library");
		drips.setColumn("ProductProductsLinkedProduct.ProductPackageCosts.Active", "Y");
		drips.setColumn("ProductProductsLinkedProduct.Active", "Y");
		//
		//drips.setColumn("ProductProductsLinkedProduct.ProductPackageCosts.AvailableDate", "<=NOW+NULL");
		//drips.setColumn("ProductProductsLinkedProduct.ProductPackageCosts.ExpiryDate", ">=NOW+NULL");
		drips.sortBy("ProductProductsLinkedProduct.Category", 0);
		drips.setParameter("-groupby0", "PackageCostProductID");
		if(logger.isDebugEnabled()) {
			drips.doAction(GenerationKeys.SEARCH);
		}
		return drips;
	}

	private static GenRow getCurrentDripListGenRow() {
		GenRow drips = new GenRow();
		drips.setViewSpec("ProductLinkedProductView");
		drips.setColumn("ProductType", "Drip+Drip Library");
		drips.setColumn("ProductProductsLinkedProduct.ProductType", "Drip+Drip Library");
		drips.sortBy("AvailableDate", 0);
		drips.sortBy("ExpiryDate", 1);
		drips.sortBy("ProductProductsLinkedProduct.Name", 3);
		drips.sortOrder("DESC", 1);
		//drips.setColumn("ProductProductsLinkedProduct.ProductPackageCosts.Active", "Y");
		//drips.setColumn("ProductProductsLinkedProduct.Active", "Y");
		return drips;
	}



	private List<DripProduct> getDripProducts(Connection con, UserBean user, boolean showDisabled) {
		if(logger.isDebugEnabled()) logger.debug("getDripProducts(Conn, {}, {})", new String[] {user != null ? user.getName() : "null user", String.valueOf(showDisabled) });
		//if(user == null) {
		//	throw new NotAuthenticatedException("You are not logged in");
		//}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		} 
		GenRow lib = new GenRow();
		try {
			lib.setConnection(con);
			lib.setViewSpec("ProductView");
			if(!showDisabled) {
				lib.setParameter("Active","Y");
			}
			lib.setParameter("ProductType","Drip+Drip Library");
			lib.setParameter("PackageCostProductID","NULL+EMPTY");	//we want the original drips, not any copied ones.
			lib.setParameter("-sort1","Name");

			if(logger.isTraceEnabled()) {
				lib.doAction(GenerationKeys.SEARCH);
				logger.trace(lib.getStatement());
			}	
			lib.getResults(true);

			if(lib.getNext()) {
				List<DripProduct> list = new ArrayList<DripProduct>(lib.getSize());
				do { 
					list.add(new DripProduct(lib));
				} while(lib.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			lib.close();
		}
	}


	/**
	 * @deprecated 	- this is out of date and should not be used.
	 */
	public List<DripProduct> getDripProducts(final Connection conn, final UserBean user, final String productID, final String productLibraryID) {
		if(logger.isDebugEnabled()) logger.debug("getDripProducts(Conn, {}, {}, {})", new String[] {user != null ? user.getName() : "null user", productLibraryID, productID });
		//if(user == null) {
		//	throw new NotAuthenticatedException("You are not logged in");
		//}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(productID)) { 
			//throw new IllegalArgumentException("product id must be supplied");
		}
		if(StringUtils.isBlank(productLibraryID)) { 
			throw new IllegalArgumentException("product library id must be supplied");
		}
		GenRow lib = getLibraryGenRow();
		lib.setParameter("-select4", "ProductIDs");
		lib.setParameter("ProductLibraryID", productLibraryID);
		lib.doAction(GenerationKeys.SELECT);
		if(!lib.isSuccessful()) { 
			throw new NotFoundException("The drip library could not be found : " + productLibraryID);
		} 

		GenRow dripList = getDripListGenRow(productID, lib.getString("ProductIDs"));
		GenRow images = new GenRow();
		images.setViewSpec("LinkedDocumentView");
		try {
			dripList.setConnection(conn);
			if(logger.isDebugEnabled()) {
				dripList.doAction(GenerationKeys.SEARCH);
				logger.debug(dripList.getStatement());
			}

			dripList.getResults(true);
			images.setConnection(conn);

			if(dripList.getNext()) {
				List<DripProduct> dripProducts = new ArrayList<DripProduct>(dripList.getSize());
				do {
					DripProduct dp = new DripProduct(dripList);
					dripProducts.add(dp);
					images.setParameter("ProductID", dripList.getString("PackageCostProductID"));
					images.setParameter("DocumentSubType", "Quick Quote");
					images.setParameter("LinkedDocumentDocument.DocumentSubType", "Quick Quote");
					images.setParameter("DocumentType", "Image");
					/*
					if (placement != null) {
						images.setParameter("LinkedDocumentDocument.Placement", placement);
					}
					if (templateName != null) {
						images.setParameter("LinkedDocumentDocument.TemplateName", templateName);
					}
					 */
					images.setParameter("DocumentType", "Image");
					images.sortBy("Name", 0);
					images.doAction("search");
					images.getResults();
					if(images.getNext()) {
						// supposedly the first is fine.
						dp.ImagePath = images.getString("FilePath");
					}
					images.close();
				} while(dripList.getNext());
				return dripProducts;
			}
			return Collections.emptyList();
		}finally {
			dripList.close();
			images.close();
		}
	}

	/** 
	 * @deprecated - never implemented fully
	 */
	public DripProduct getDripProduct(final Connection conn, final UserBean user, final String productLibraryID, final String dripProductID) {
		if(logger.isTraceEnabled()) logger.trace("getDripProduct(Conn, {}, {}, {})", new String[]{user != null ? user.getName() : "null user", productLibraryID, dripProductID});
		//if(user == null) {
		//	throw new NotAuthenticatedException("You are not logged in");
		//}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		if(StringUtils.isBlank(productLibraryID)) { 
			throw new IllegalArgumentException("product library id must be supplied");
		}
		GenRow lib = getLibraryGenRow();
		lib.setParameter("-select4", "ProductIDs");
		lib.setParameter("ProductLibraryID", productLibraryID);
		lib.doAction(GenerationKeys.SELECT);
		if(!lib.isSuccessful()) { 
			throw new NotFoundException("The drip library could not be found : " + productLibraryID);
		} 

		/* 
		 * if(!row.isSuccessful()) { 
		 * 		throw new NotFoundException("The drip product could not be found : " + dripProductID);
		 * } 
		 */
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/**
	 * The is so we can do single drip price difference in feeds
	 * 
	 * @param request
	 * @param dripProductID
	 * @return
	 */
	public DripProduct getSingleDripProduct(final HttpServletRequest request, final String dripProductID) {
		return getSingleDripProduct(ActionBean.getConnection(request), us.getCurrentUser(request), dripProductID);

	}

	/**
	 * This is so we can do single drip price differences in feeds
	 * 
	 * @param conn
	 * @param user
	 * @param dripProductID
	 * @return
	 */
	public DripProduct getSingleDripProduct(final Connection conn, final UserBean user, final String dripProductID) {
		if(logger.isTraceEnabled()) logger.trace("getSingleDripProduct(Conn, {}, {}, {})", new String[]{user != null ? user.getName() : "null user", dripProductID});
		//if(user == null) {
		//	throw new NotAuthenticatedException("You are not logged in");
		//}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(conn);
		row.setColumn("ProductType", "Drip+Drip Library");
		row.setColumn("ProductProductsLinkedProduct.ProductType", "Drip+Drip Library");
		row.setColumn("LinkedProductID", dripProductID);
		row.doAction("selectfirst");


		if(!row.isSuccessful()) { 
			throw new NotFoundException("The drip product could not be found : " + dripProductID);
		} 

		return new DripProduct(row);
	}

	public DripProduct getDripFromProduct(GenRow dripProduct) {
		if (dripProduct == null) return null;
		return new DripProduct(dripProduct);
	}
	/** Returns a lit with 2 genrows
	 *  1. Included Drips
	 *  2. Optional Drips
	 * @param conn
	 * @param user
	 * @return
	 */
	public List<GenRow> getDripList(final Connection conn, final UserBean user) {
		GenRow includedList = new GenRow();
		includedList.setConnection(conn);
		includedList.clear();
		includedList.setTableSpec("Products");
		includedList.setParameter("-select0","*");
		includedList.setParameter("ProductType","Drip Library");
		includedList.setParameter("ProductSubType","Included");
		includedList.sortBy("Name",0);
		includedList.doAction("search");
		includedList.getResults();

		GenRow optionalList = new GenRow();
		optionalList.setConnection(conn);
		optionalList.clear();
		optionalList.setTableSpec("Products");
		optionalList.setParameter("-select0","*");
		optionalList.setParameter("ProductType","Drip Library");
		optionalList.setParameter("ProductSubType","Optional");
		optionalList.sortBy("Name",0);
		optionalList.doAction("search");
		optionalList.getResults();


		List<GenRow> result = new ArrayList<GenRow>(2);
		result.add(includedList);
		result.add(optionalList);

		return result;
	}

	/** Returns a lit with 4 genrows
	 *  1. Included Drips
	 *  2. Optional Drips
	 *  3. Archive Included Drips
	 *  4. Archive Optional Drips
	 *  DISPLAYFLAG column is used as ARCHIVEFLAG column
	 * @param conn
	 * @param user
	 * @return
	 */
	public List<GenRow> getArchiveAndAvailableDripList(final Connection conn, final UserBean user) {
		GenRow includedList = new GenRow();
		includedList.setConnection(conn);
		includedList.clear();
		includedList.setTableSpec("Products");
		includedList.setParameter("-select0","*");
		includedList.setParameter("ProductType","Drip Library");
		includedList.setParameter("ProductSubType","Included");
		includedList.setParameter("DisplayFlag","NULL+EMPTY+N");
		includedList.sortBy("Name",0);
		includedList.doAction("search");
		includedList.getResults(true);

		GenRow archiveIncludeList = new GenRow();
		archiveIncludeList.setConnection(conn);
		archiveIncludeList.clear();
		archiveIncludeList.setTableSpec("Products");
		archiveIncludeList.setParameter("-select0","*");
		archiveIncludeList.setParameter("ProductType","Drip Library");
		archiveIncludeList.setParameter("ProductSubType","Included");
		archiveIncludeList.setParameter("DisplayFlag","Y");
		archiveIncludeList.sortBy("Name",0);
		archiveIncludeList.doAction("search");
		archiveIncludeList.getResults(true);

		GenRow optionalList = new GenRow();
		optionalList.setConnection(conn);
		optionalList.clear();
		optionalList.setTableSpec("Products");
		optionalList.setParameter("-select0","*");
		optionalList.setParameter("ProductType","Drip Library");
		optionalList.setParameter("ProductSubType","Optional");
		optionalList.setParameter("DisplayFlag","NULL+EMPTY+N");
		optionalList.sortBy("Name",0);
		optionalList.doAction("search");
		optionalList.getResults(true);

		GenRow archiveOptionalList = new GenRow();
		archiveOptionalList.setConnection(conn);
		archiveOptionalList.clear();
		archiveOptionalList.setTableSpec("Products");
		archiveOptionalList.setParameter("-select0","*");
		archiveOptionalList.setParameter("ProductType","Drip Library");
		archiveOptionalList.setParameter("ProductSubType","Optional");
		archiveOptionalList.setParameter("DisplayFlag","Y");
		archiveOptionalList.sortBy("Name",0);
		archiveOptionalList.doAction("search");
		archiveOptionalList.getResults(true);


		List<GenRow> result = new ArrayList<GenRow>(4);
		result.add(includedList);
		result.add(optionalList);
		result.add(archiveIncludeList);
		result.add(archiveOptionalList);

		return result;
	}

	public DripCollection getAllDrips(final Connection conn, final UserBean user, final String[] productIDs, final Date date) {
		return getAllDrips(conn, user, productIDs, date, null, -1); // New Method. Contain logic to cope with the regional pricing change
	}


	/**
	 * This is the Overloaded method. This is required to cope with the Regional Pricing requirement
	 */
	public DripCollection getAllDrips(final Connection conn, final UserBean user, final String[] productIDs, final Date date, TableData requestrow, int regionIndex) {
		if (logger.isDebugEnabled())
			logger.debug("getAllDrips(Conn, {}, {}, {}, {})", new String[] { user != null ? user.getName() : "null user", productIDs != null ? StringUtils.join(productIDs,","): "null productIds",  (requestrow!=null && requestrow.getString("RegionID")!=null) ? requestrow.getString("RegionID") : "null RegionID" });

		if (requestrow != null) {
			logger.debug("getAllDrips with requestrow RegionID parameter is called. RegionID is: " + requestrow.getString("RegionID") + "and Index :" + regionIndex);	
		}

		postDrips = new ArrayList<DripService.DripProduct>();
		//if (user == null) {
		// Method getAllDrips is also called when user is viewing product PDF templates for which they need not log in   
		//throw new NotAuthenticatedException("You are not logged in");
		//}
		// Method getAllDrips is also called when user is viewing product PDF templates for which they need not log in
		//if (!user.canAccess("Products")) {			
		//throw new AccessException("You do not have permission to access the properties module");
		//}

		if (productIDs == null || productIDs.length == 0) {
			throw new IllegalArgumentException("productIDs must be supplied");
		}
		
		HashMap<String,DripProduct> currentDrips = new HashMap<String,DripProduct>();
		
		GenRow drips = getCurrentDripListGenRow();

		String dateStr = sformat.format(new Date());

		if (date != null) {
			dateStr = sformat.format(date);
			//drips.setColumn("ProductProductsLinkedProduct.ProductPackageCosts.AvailableDate", "<=" + dateStr + "+NULL");
			//drips.setColumn("ProductProductsLinkedProduct.ProductPackageCosts.ExpiryDate", ">=" + dateStr + "+NULL");
			// Issue: https://runway.aceproject.com/Tab.asp?Tab=2&TASK_ID=43&PROJECT_ID=14&cantOpen=0&noNextPrevious=1
			// Above was causing issues when the DRIP from say Home was present in H&L but the DRIP was actually removed from Home for some reason and H&L package not created as yet.
			// There is no need to go to parent DRIP to fetch the date range, relying on the DRIP attached to the Product itself

			drips.setColumn("ProductProductsLinkedProduct.AvailableDate", "<=" + dateStr + "+NULL");
			drips.setColumn("ProductProductsLinkedProduct.ExpiryDate", ">=" + dateStr + "+NULL");
		}

		try {
			drips.setConnection(conn);

			DripCollection collection = new DripCollection();

			// Used for H&L only
			String[] productTypeLabels = new String[]{"House", "Land", "!House;!Land+NULL+EMPTY"};

			List<DripProduct> netDripsForRecalculation = new ArrayList<DripProduct>();
			GenRow mainProduct = PropertyFactory.getPropertyEntity(conn, productIDs[Math.min(productIDs.length, 3) - 1]);

			Set<String> dripList = new HashSet<String>();
			
			String orderID = "";
			String quoteViewID = "";
			String contactID = "";
			
			if (productIDs.length >= 5) quoteViewID = productIDs[4];

			if (productIDs.length >= 4) {
				orderID = productIDs[3];
				if (StringUtils.isNotBlank(orderID)) {
					Order o = OrderService.getInstance().getOrder(conn, user, orderID);
					contactID = o.getContactID();
					if (StringUtils.isBlank(quoteViewID)) quoteViewID = o.getQuoteViewID();
				}
			}

			for (int i = 0; i < Math.min(productIDs.length, 3); i++) {
				String productID = productIDs[i];

				if (productID == null || productID.length() == 0) continue;

				String origin = mainProduct.getString("ProductType");

				HashMap<String, DripLibrary> includedLibMap = new HashMap<String, DripLibrary>();
				HashMap<String, DripLibrary> optionalLibMap = new HashMap<String, DripLibrary>();

				drips.setColumn("ProductID", productID);

				if("House and Land".equals(origin))
					drips.setColumn("ProductProductsLinkedProduct.Summary", productTypeLabels[i]);

				drips.doAction(GenerationKeys.SEARCH);
				if (logger.isTraceEnabled()) {
					logger.trace("=========================================================================================================");
					logger.trace(drips.getStatement());
				}				

				drips.getResults();
				DripProduct dp;

				while (drips.getNext()) {
					if(dripList.contains(drips.getData("LinkedProductID"))) {
						continue;
					}
					dripList.add(drips.getData("LinkedProductID"));

					dp = new DripProduct(drips);
					collection.allDrips.add(dp);
					
					dp.ProductID = drips.getData("LinkedProductID");
					// we need this for later
					currentDrips.put(dp.ProductID, dp);
					if (StringUtils.isNotBlank(dp.CopiedFromID)) currentDrips.put(dp.CopiedFromID, dp);
					
					dp.loadContraints(conn);

					if ("House and Land".equals(origin)) {
						// It could be a drip actually copied from House/Land when the package was
						// created.
						if (StringUtils.isNotBlank(drips.getString("Summary")))
							origin = drips.getString("Summary");
					}

					// If the drip is attached to the primary product id, editing is allowed
					if (productID.equals(mainProduct.getString("ProductID")))
						dp.Editable = true;

					if ("Included".equals(dp.Type)) {
						collection.includedDripCount++;
						groupDrips(conn, user, dp, includedLibMap);
						if (!"Post Net".equals(dp.PricingType)) {
							processDRIPvalue(conn, mainProduct, dp, date, netDripsForRecalculation, quoteViewID, orderID, contactID);
							collection.includedDripAmount = collection.includedDripAmount + dp.Value;
							if ("Post Net".equals(dp.PricingType)) collection.includedPostDripAmount = collection.includedPostDripAmount + dp.Value;
							if (dp.isOrderDrip && StringUtils.isNotBlank(orderID)) updateOrder(conn, dp, orderID);
						} else {
							postDrips.add(dp);
						}
					} else {
						collection.optionalDripCount++;
						groupDrips(conn, user, dp, optionalLibMap);
						if (!"Post Net".equals(dp.PricingType)) {
							processDRIPvalue(conn, mainProduct, dp, date, netDripsForRecalculation, quoteViewID, orderID, contactID);
							collection.optionalDripAmount = collection.optionalDripAmount + dp.Value;
							if ("Post Net".equals(dp.PricingType)) collection.optionalPostDripAmount = collection.optionalPostDripAmount + dp.Value;
						} else {
							postDrips.add(dp);
						}
					}

				}

				collection.includedProductList.add(includedLibMap);
				collection.optionalProductList.add(optionalLibMap);
			}
			
			if (productIDs.length >= 4 && StringUtils.isNotBlank(productIDs[3])) {
				GenRow items = new GenRow();
				items.setTableSpec("OrderItems");
				items.setConnection(conn);
				items.setParameter("-select0", "OrderItemID");
				items.setParameter("-select1", "ProductID");
				items.setParameter("-select2", "CopiedProductID");
				items.setParameter("OrderID", productIDs[3]);
				items.setParameter("Category", "DRIP");
				items.doAction("search");
				
				items.getResults();
				
				HashMap<String, DripLibrary> orderLibMap = new HashMap<String, DripLibrary>();
				
				System.out.println(items.getStatement());
				
				while (items.getNext()) {
					DripProduct dp = null;
					if (StringUtils.isNotBlank(items.getData("CopiedProductID"))) dp = currentDrips.get(items.getData("CopiedProductID"));
					if (dp != null && !items.getData("ProductID").equals(dp.ProductID)) {
						dp.Active = "Y";

						GenRow update = new GenRow();
						update.setTableSpec("OrderItems");
						update.setConnection(conn);
						update.setParameter("OrderItemID", items.getString("OrderItemID"));
						update.setParameter("ProductID", dp.ProductID);
						update.doAction("update");
					} else if (dp == null) {
						if (StringUtils.isNotBlank(items.getData("ProductID"))) dp = currentDrips.get(items.getData("ProductID"));
						if (dp != null && !items.getData("CopiedProductID").equals(dp.CopiedFromID)) {
							dp.Active = "Y";
							
							GenRow update = new GenRow();
							update.setTableSpec("OrderItems");
							update.setConnection(conn);
							update.setParameter("OrderItemID", items.getString("OrderItemID"));
							update.setParameter("CopiedProductID", dp.CopiedFromID);
							update.doAction("update");
						}
					}
					if (dp == null) {
						String prod = items.getString("CopiedProductID");
						if (StringUtils.isNotBlank(prod)) prod += "+";
						prod += items.getString("ProductID");
						
						GenRow ds = new GenRow();
						ds.setViewSpec("ProductView");
						ds.setConnection(conn);
						ds.setParameter("ProductID", prod);
						if (StringUtils.isNotBlank(prod)) ds.doAction("selectfirst");
						
						System.out.println(ds.getStatement());
						
						if (StringUtils.isNotBlank(ds.getData("ProductID"))) {
							dp = new DripProduct(ds);
							collection.allDrips.add(dp);
							
							if (StringUtils.isBlank(dp.CopiedFromID) && items.getString("CopiedProductID").equals(dp.ProductID)) {
								dp.CopiedFromID = dp.ProductID;
								dp.PackageCostProductID = dp.ProductID;
							}
							
							// we need this for later
							currentDrips.put(dp.ProductID, dp);
							if (StringUtils.isNotBlank(dp.CopiedFromID)) currentDrips.put(dp.CopiedFromID, dp);
	
							dp.loadContraints(conn);

							dp.Active = "Y";
							dp.Type = "Order";

							groupDrips(conn, user, dp, orderLibMap);
							
							if (!"Post Net".equals(dp.PricingType)) {
								processDRIPvalue(conn, mainProduct, dp, date, netDripsForRecalculation);
								//collection.includedDripAmount = collection.includedDripAmount + dp.Value;
								//if ("Post Net".equals(dp.PricingType)) collection.includedPostDripAmount = collection.includedPostDripAmount + dp.Value;
							} else {
								postDrips.add(dp);
							}

							GenRow update = new GenRow();
							update.setTableSpec("OrderItems");
							update.setConnection(conn);
							update.setParameter("OrderItemID", items.getString("OrderItemID"));
							update.setParameter("ProductID", dp.ProductID);
							update.setParameter("CopiedProductID", dp.CopiedFromID);
							update.doAction("update");
						} else {
							GenRow update = new GenRow();
							update.setTableSpec("OrderItems");
							update.setConnection(conn);
							update.setParameter("OrderItemID", items.getString("OrderItemID"));
							update.doAction("delete");
						}
					}
				}
				
				collection.orderProductList.add(orderLibMap);
			}

			double totalCost = 0;
			boolean regionPricing = "true".equals(InitServlet.getSystemParam("RegionPricingEnabled"));
			if(regionPricing && requestrow != null && StringUtils.isNotBlank(requestrow.getString("RegionID")) && regionIndex != -1) {
				// Logic for the Regional Pricing
				String regionIDsInRequest = requestrow.getString("RegionID");

				ArrayList<String> regionIDs = RunwayUtil.getRegionIDList(regionIDsInRequest);
				String usedRegionID = regionIDs.get(regionIndex-1);

				logger.debug("Getting Regional Price for ProductID: " + mainProduct.getString("ProductID") + "  ,  usedRegionID: " + usedRegionID + "  , regionIDsInRequest: " + regionIDsInRequest);
				totalCost = RunwayUtil.getRegionalPrice(mainProduct.getString("ProductID"), usedRegionID);

				//Kavi : Commenting this code as products inactive in a certain region will return 0 
				/*if (totalCost <= 0) {
					totalCost = mainProduct.getDouble("TotalCost");
				}*/
			} else {
				totalCost = mainProduct.getDouble("TotalCost");
			}

			if(totalCost != 0)
				collection.NetAmount = StringUtil.getRoundedValue(totalCost + collection.includedDripAmount);

			for (DripProduct dp : netDripsForRecalculation){
				dp.Value = stringToDouble(dp.Margin) * collection.NetAmount;
				collection.optionalDripAmount = collection.optionalDripAmount + dp.Value; 
			}

			dripCollection = collection;
			// do not use iterators on global attributes as it will have a concurrency exception
			for (int x = 0; x < postDrips.size(); ++x) {
				try {
					DripProduct dp = postDrips.get(x);
					if ("Included".equals(dp.Type)) {
						processDRIPvalue(conn, mainProduct, dp, date, netDripsForRecalculation);
						collection.includedDripAmount = collection.includedDripAmount + dp.Value;
						if ("Post Net".equals(dp.PricingType)) collection.includedPostDripAmount = collection.includedPostDripAmount + dp.Value;
					} else {
						processDRIPvalue(conn, mainProduct, dp, date, netDripsForRecalculation);
						collection.optionalDripAmount = collection.optionalDripAmount + dp.Value;
						if ("Post Net".equals(dp.PricingType)) collection.optionalPostDripAmount = collection.optionalPostDripAmount + dp.Value;
					}
				} catch (Exception e) {
					break;
				}
			}

			dripCollection = null;

			return collection;
		} finally {
			drips.close();
		}
	}


	private void updateOrder(Connection conn, DripProduct dp, String orderID) {
		if (!dp.isOrderDrip || StringUtils.isBlank(orderID)) return;
		
		GenRow row = new GenRow();
		row.setTableSpec("OrderItems");
		row.setConnection(conn);
		row.setParameter("ON-OrderID", orderID);
		row.setParameter("ON-ProductID", dp.ProductID);
		row.setParameter("GST", dp.GST);
		row.setParameter("Cost", dp.Cost);
		row.setParameter("ItemCost", dp.Cost);
		row.setParameter("TotalCost", dp.TotalCost);
		
		row.doAction("updateall");
		
		OrderService.getInstance().updateOrder(conn, null, orderID);
	}

	private void groupDrips(final Connection conn, final UserBean user, DripProduct dp, HashMap<String, DripLibrary> dripMap) {
		DripLibrary library = null;
		if (StringUtils.isNotBlank(dp.ProductLibraryID)) {
			library = dripMap.get(dp.ProductLibraryID);
			if (library == null) {
				try {
					library = getDripLibrary(conn, user, dp.ProductLibraryID);
					dripMap.put(dp.ProductLibraryID, library);
				} catch (Exception e) {
					// we must not have a drastic falure here, this may happen if you move a drip from a library, the drip stays in the product
					logger.warn("Missing Drip Lbrary {} for ProductLibraryID={}", e.getMessage(), dp.ProductLibraryID);
					library = dripMap.get("Others");
					if (library == null) {
						library = new DripLibrary();
						library.Name = "Others";
						library.ProductLibraryID = "Others";
						dripMap.put("Others", library);
					}
				}
			}
		} else {
			// Put it in Others
			library = dripMap.get("Others");
			if (library == null) {
				library = new DripLibrary();
				library.Name = "Others";
				library.ProductLibraryID = "Others";
				dripMap.put("Others", library);
			}

		}
		if(library.products == null)
			library.products = new ArrayList<DripProduct>();

		library.products.add(dp);

	}

	public void processDRIPvalue(final Connection conn, GenRow mainProduct, DripProduct drip, Date evalDate, List<DripProduct> netDripsForRecalculation) {		
		processDRIPvalue(conn, mainProduct, drip, evalDate, netDripsForRecalculation, "", "", "");
	}
	
	public void processDRIPvalue(final Connection conn, GenRow mainProduct, DripProduct drip, Date evalDate, List<DripProduct> netDripsForRecalculation, String quoteViewID, String orderID, String contactID) {		
		double value = 0;
		String active = "N";
		String mainPID = mainProduct.getString("ProductID");
		double tCost = drip.TotalCost;

		Date start = null;
		Date end = null;

		if (evalDate == null) evalDate = new Date();

		try {			
			evalDate = sformat.parse(sformat.format(evalDate));
		}catch (Exception e) {
			// TODO: handle exception
		}

		// If the drip is attached to the H&L, toggling is allowed
		if ("House And Land".equalsIgnoreCase(mainProduct.getString("ProductType"))) {
			drip.Editable = false;
			drip.ToggleActive = true;			
		}

		if (drip.AvailableDate.length() > 0)
			start = dateString(drip.AvailableDate);
		if (drip.ExpiryDate.length() > 0)
			end = dateString(drip.ExpiryDate);
		
		if ("Advantage - The Dream Life - Modern Render Finish".equals(drip.Name)) {
			System.out.println("Here");
		}

		String rootProductID = mainPID;
		if ("Y".equals(drip.Active)) {
			drip.PackageCostCalcString= ""; 
			if ((start == null || start.before(evalDate) || start.equals(evalDate)) && (end == null || end.after(evalDate) || end.equals(evalDate))) {
				if ("House and Land".equalsIgnoreCase(mainProduct.getString("ProductType"))) {
					mainProduct.clear();
					mainProduct.setViewSpec("properties/ProductHouseAndLandView");
					mainProduct.setParameter("ProductID", mainPID);
					mainProduct.doAction("selectfirst");
				} else if ("Home Plan".equalsIgnoreCase(mainProduct.getString("ProductType"))) {
					mainProduct.clear();
					mainProduct.setViewSpec("properties/ProductPlanView");
					mainProduct.setParameter("ProductID", mainPID);
					mainProduct.doAction("selectfirst");
					if (mainProduct.getString("CopiedFromProductID").length() > 0) {
						rootProductID = findRootPlan(conn,mainProduct.getString("CopiedFromProductID")).getData("ProductID");
						if (rootProductID == null || rootProductID.length() == 0);
					}
				} else if ("Land".equalsIgnoreCase(mainProduct.getString("ProductType"))) {
					mainProduct.clear();
					mainProduct.setViewSpec("properties/ProductLotView");
					mainProduct.setParameter("ProductID", mainPID);
					mainProduct.doAction("selectfirst");
				} else if ("Apartment".equalsIgnoreCase(mainProduct.getString("ProductType"))) {
					mainProduct.clear();
					mainProduct.setViewSpec("properties/ProductApartmentView");
					mainProduct.setParameter("ProductID", mainPID);
					mainProduct.doAction("selectfirst");
				}
				active = "Y";

				mainProduct.setParameter("CampaignID", getCampaignID(mainPID));

				String costCalcField = drip.PackageCostCalcField;
				String costCalcProduct = drip.PackageCostProductDetails;
				String cost = "0";

				if ("MultiCosting".equals(costCalcField) || "FreeCosting".equals(costCalcField)) {
					GenRow costings = findCostings(conn, drip.ProductID, "NULL+EMPTY");
					if (costings != null && costings.getSize() > 0) {
						active = "N";
						if (mainProduct.getString("ProductType").length() > 0) {
							cost = calculate(String.valueOf(drip.Cost));
							String a = "N";
							boolean done = false;
							estateDone = false;
							while (costings.getNext()) {
								done = true;
								if ("Y".equals(doDripProcessing(conn, drip, costings, mainProduct, quoteViewID, orderID, contactID))) a = "Y";
							}
							if (done) active = a;
							value = drip.TotalCost;
						}
					}
					if (costings != null)
						costings.close();
					if (drip.PackageCostCalcString == null || drip.PackageCostCalcString.length() == 0) {
						drip.PackageCostCalcString = "Not Applied";
						active = "N";
					}
				} else {
					if (costCalcProduct.length() == 0 && (drip.Margin.replaceAll("[0\\.]", "").length() == 0)) {
						value = drip.TotalCost;
					} else if ("Package Cost".equals(costCalcProduct)) {
						GenRow check = new GenRow();
						check.setViewSpec("ProductLinkedProductView");
						check.setConnection(conn);
						check.setParameter("ProductID", mainPID);
						check.setParameter("PackageCostProductID",drip.PackageCostCalcField);
						check.setParameter("ProductProductsLinkedProduct.PackageCostProductID",drip.PackageCostCalcField);
						check.doAction("selectfirst");

						if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
							value = drip.TotalCost;
						} else {
							value = stringToDouble(drip.Margin) * check.getDouble("TotalCost");
						}
					} else if ("Land".equals(mainProduct.getString("ProductType"))) {
						if ("Land".equals(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("Cost");
								}
							} catch (Exception e) {
							}
						} else if ("Land Total".equals(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("TotalCost");
								}
							} catch (Exception e) {
							}
						}
					} else if ("Home Plan".equals(mainProduct.getString("ProductType"))) {
						if ("Home Plan".equals(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("Cost");
								}
							} catch (Exception e) {
							}
						} else if ("Home Plan Total".equals(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("TotalCost");
								}
							} catch (Exception e) {
							}
						}
					} else if ("Existing Property".equals(mainProduct.getString("ProductType"))) {
						if ("Real Estate Total".equalsIgnoreCase(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("TotalCost");
								}
							} catch (Exception e) {
							}
						}
					} else if ("House and Land".equalsIgnoreCase(mainProduct.getString("ProductType"))) {
						if ("Land".equals(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("LotCost");
								}
							} catch (Exception e) {
							}
						} else if ("Land Total".equals(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("LotTotalCost");
								}
							} catch (Exception e) {
							}
						}
						if ("Home Plan".equals(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("PlanCost");
								}
							} catch (Exception e) {
							}
						} else if ("Home Plan Total".equals(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("PlanTotalCost");
								}
							} catch (Exception e) {
							}
						}
						if ("House And Land".equalsIgnoreCase(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("Cost");
								}
							} catch (Exception e) {
							}
						} else if ("House And Land Total".equalsIgnoreCase(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									value = stringToDouble(drip.Margin) * mainProduct.getDouble("TotalCost");
								}
							} catch (Exception e) {
							}
						} else if ("Total Less House".equalsIgnoreCase(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									if (dripCollection == null)
										value = stringToDouble(drip.Margin) * (mainProduct.getDouble("TotalCost") - mainProduct.getDouble("PlanTotalCost"));
									else
										value = stringToDouble(drip.Margin) * ((mainProduct.getDouble("TotalCost") + (dripCollection.includedDripAmount - dripCollection.includedPostDripAmount)) - mainProduct.getDouble("PlanTotalCost"));
								}
							} catch (Exception e) {
							}
						} else if ("Total Less Land".equalsIgnoreCase(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									if (dripCollection == null)
										value = stringToDouble(drip.Margin) * (mainProduct.getDouble("TotalCost") - mainProduct.getDouble("LotTotalCost"));
									else
										value = stringToDouble(drip.Margin) * ((mainProduct.getDouble("TotalCost") + (dripCollection.includedDripAmount - dripCollection.includedPostDripAmount)) - mainProduct.getDouble("LotTotalCost"));
								}
							} catch (Exception e) {
							}
						} else if ("Net".equalsIgnoreCase(drip.PackageCostProductDetails)) {
							try {
								if (drip.Margin.replaceAll("[0\\.%]", "").length() == 0) {
									value = drip.TotalCost;
								} else {
									netDripsForRecalculation.add(drip);
									value = 0;
								}
							} catch (Exception e) {
							}
						}
					} 
				}
			}
		}
		
		if ("Y".equals(active) && value != tCost && StringUtils.isNotBlank(drip.ProductID)) {
			GenRow update = new GenRow();
			update.setTableSpec("Products");
			update.setParameter("ProductID", drip.ProductID);
			update.setParameter("TotalCost", "" + value);
			update.doAction("update");
		}

		drip.Active = active;
		drip.Value = value;
		drip.Cost = (double) Math.floor((value / 1.1) * 100) / 100;
		drip.TotalCost = value;
		drip.GST = drip.TotalCost - drip.Cost;
	}

	private Object getCampaignID(String mainPID) {
		StringBuilder campaignIDs = new StringBuilder();

		GenRow row = new GenRow();
		row.setTableSpec("CampaignProducts");
		row.setParameter("-select0", "CampaignID");
		row.setParameter("ProductID",mainPID);
		row.doAction("search");

		row.getResults();

		while (row.getNext()) {
			if (campaignIDs.length() > 0) campaignIDs.append("+");
			campaignIDs.append(row.getString("CampaignID"));
		}
		row.close();

		return campaignIDs.toString();
	}

	private String doDripProcessing(Connection conn, DripProduct drip, GenRow costings, GenRow mainProduct) {
		return doDripProcessing(conn, drip, costings, mainProduct, "", "", "");
	}
	private String doDripProcessing(Connection conn, DripProduct drip, GenRow costings, GenRow mainProduct, String quoteViewID, String orderID, String contactID) {
		String costCalcField = drip.PackageCostCalcField;
		String costCalcProduct = drip.PackageCostProductDetails;
		String cost = "0";

		String active = "N";

		String rootProductID = mainProduct.getString("ProductID");
		String condition = drip.PackageCostCalcString;
		//if (condition == null) condition = "";
		String applyTo = costings.getString("ApplyTo");
		String applyToID = costings.getString("ApplyToID");
		boolean theCondition = true;
		if ("FreeForm".equals(costings.getString("IfDimension"))) {
			theCondition = checkConditionFree(conn, costings,
					orderID, contactID, mainProduct);
		} else {
			theCondition = checkCondition(conn, costings,
					rootProductID);
		}
		
		if ("Order".equals(applyTo)) drip.isOrderDrip = true;


		logger.trace(costCalcField + " cost {} ",
				costings.getString("Cost"));

		//System.out.println(mainProduct.getStatement());

		//drip.PackageCostProductDetails =  "";
		//drip.PackageCostCalcField = "";
		String dbCost = costings.getString("Cost");
		String packageCostCalcField = costings
				.getString("PackageCostCalcField");
		String PackageCostProductDetails = costings
				.getString("PackageCostProductDetails");
		/*
		if (theCondition
				&& costings.getString("Cost").replaceAll("[0\\.]", "").length() == 0 && (costings.getData("ParentID") == null || costings.getData("ParentID").length() == 0)) {
			GenRow child = findCostings(conn, drip.ProductID, costings.getString("CostingID"));
			if (child != null) {
				while (child.getNext()) {
					/*
					if ("FreeCosting".equals(costCalcField)) {
						theCondition = checkConditionFree(conn,
								child, rootProductID);
					} else {
						theCondition = checkCondition(conn, child,
								rootProductID);
					}
					if (theCondition) {
						dbCost = child.getString("Cost");
						packageCostCalcField = child
								.getString("PackageCostCalcField");
						PackageCostProductDetails = child
								.getString("PackageCostProductDetails");
						String calcStr = drip.PackageCostCalcString;
						if (calcStr.length() > 0)
							calcStr += " +<br />";
						calcStr += getCondition(conn, child,
								mainProduct.getString("ProductID"));

						drip.PackageCostCalcString = calcStr;
						break;
					}
					active = doDripProcessing(conn, drip, child, mainProduct);
				}
				child.close();
			}
		}
		 */
		boolean processed = false;

		if (dbCost.length() == 0)
			dbCost = "0";
		if ("".equals(applyTo) && theCondition) {
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField =	packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.addCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField)) {
				condition += costings.getString("IfDimValue");
			} else {
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			}
			if (StringUtils.isBlank(packageCostCalcField) && StringUtils.isBlank(PackageCostProductDetails)) {
				if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
					condition += ",<br>";
				condition += currency.format(stringToDouble(cost));
				drip.TotalCost += stringToDouble(cost);
			}
			active = "Y";
			drip.PackageCostCalcString = condition;
			processed = true;
		} else if ("Order".equals(applyTo)
				&& applyToID.equals(quoteViewID)
				&& theCondition) {
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField = packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.setCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField))
				condition += costings.getString("IfDimValue");
			else
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			active = "Y";
		} else if ("Brand".equals(applyTo)
				&& applyToID.equals(mainProduct.getString("BrandProductID"))
				&& theCondition) {
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField = packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.setCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField))
				condition += costings.getString("IfDimValue");
			else
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			active = "Y";
		} else if ("Home Range".equals(applyTo)
				&& applyToID.equals(mainProduct
						.getString("RangeProductID"))
						&& theCondition) {
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField = packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.setCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField))
				condition += costings.getString("IfDimValue");
			else
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			active = "Y";
		} else if ("Home".equals(applyTo)
				&& applyToID.equals(mainProduct.getString("HomeProductID"))
				&& theCondition) {
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField = packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.setCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField))
				condition += costings.getString("IfDimValue");
			else
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			active = "Y";
		} else if ("Home Plan".equals(applyTo)
				&& (applyToID.equals(mainProduct.getString("CopyPlanID"))
						|| applyToID.equals(mainProduct.getString("PlanID"))
						|| applyToID.equals(mainProduct.getString("CopiedFromProductID"))
						|| applyToID.equals(mainProduct.getString("ProductID")))
						&& theCondition) {
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField =	packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.setCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField))
				condition += costings.getString("IfDimValue");
			else
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			active = "Y";
		} else if ("Location".equals(applyTo)
				&& (applyToID.equals(
						mainProduct.getString("LocationID")) || applyToID.equals(
								mainProduct.getString("RegionID"))) 
						&& theCondition) {
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField = packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.setCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField))
				condition += costings.getString("IfDimValue");
			else
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			active = "Y";
		} else if ("Estate".equals(applyTo)
				&& (applyToID.equals(mainProduct.getString("EstateProductID")) || ("UseAllOthers".equals(applyToID) && !estateDone))
				&& theCondition) {
			estateDone = true;
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField = packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.setCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField))
				condition += costings.getString("IfDimValue");
			else
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			active = "Y";
		} else if ("Building".equals(applyTo)
				&& applyToID.equals(mainProduct
						.getString("BuildingProductID"))
						&& theCondition) {
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField = packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.setCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField))
				condition += costings.getString("IfDimValue");
			else
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			active = "Y";
		} else if ("Campaign".equals(applyTo)
				&& mainProduct
				.getString("CampaignID").indexOf(applyToID) != -1
				&& theCondition) {
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField = packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.setCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField))
				condition += costings.getString("IfDimValue");
			else
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			active = "Y";
		} else if (applyTo.length() == 0
				&& applyToID.length() == 0 && theCondition) {
			if (packageCostCalcField != null && packageCostCalcField.length() > 0) drip.PackageCostCalcField = packageCostCalcField;
			if (PackageCostProductDetails.length() > 0) {
				drip.PackageCostProductDetails =
						applyTo.replaceAll(" ", "")
						+ "Details-"
						+ PackageCostProductDetails;
			}
			drip.setCost(dbCost);
			cost = calculate(dbCost);
			if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
				condition += ",<br>";
			if ("FreeCosting".equals(costCalcField))
				condition += costings.getString("IfDimValue");
			else
				condition += getCondition(conn, costings,
						mainProduct.getString("ProductID"));
			active = "Y";
		} else {
			cost = "0";
		}
		costCalcField = packageCostCalcField;
		costCalcProduct = PackageCostProductDetails;

		if (costCalcField.indexOf("-") >= 0)
			costCalcField = costCalcField
			.substring(costCalcField.indexOf("-") + 1);

		if (theCondition && costCalcField.length() == 0) {
			if (costCalcProduct != null && costCalcProduct.length() > 0) {
				if (costCalcProduct.indexOf("-") >= 0)
					costCalcProduct = costCalcProduct.substring(costCalcProduct.indexOf("-") + 1);

				double value = drip.TotalCost;
				if ("Land".equals(mainProduct.getString("ProductType"))) {
					if ("Land".equals(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								value += stringToDouble(cost) * mainProduct.getDouble("Cost");
							}
							processed = true;
						} catch (Exception e) {
						}
					} else if ("Land Total".equals(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								value += stringToDouble(cost) * mainProduct.getDouble("TotalCost");
							}
							processed = true;
						} catch (Exception e) {
						}
					}
				} else if ("Home Plan".equals(mainProduct.getString("ProductType"))) {
					if ("Home Plan".equals(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								value += stringToDouble(cost) * mainProduct.getDouble("Cost");
							}
							processed = true;
						} catch (Exception e) {
						}
					} else if ("Home Plan Total".equals(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								value += stringToDouble(cost) * mainProduct.getDouble("TotalCost");
							}
							processed = true;
						} catch (Exception e) {
						}
					}
				} else if ("House and Land".equalsIgnoreCase(mainProduct.getString("ProductType"))) {
					if ("Land".equals(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								value += stringToDouble(cost) * mainProduct.getDouble("LotCost");
							}
							processed = true;
						} catch (Exception e) {
						}
					} else if ("Land Total".equals(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								value += stringToDouble(cost) * mainProduct.getDouble("LotTotalCost");
							}
							processed = true;
						} catch (Exception e) {
						}
					} else if ("Home Plan".equals(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								value += stringToDouble(cost) * mainProduct.getDouble("PlanCost");
							}
							processed = true;
						} catch (Exception e) {
						}
					} else if ("Home Plan Total".equals(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								value += stringToDouble(cost) * mainProduct.getDouble("PlanTotalCost");
							}
							processed = true;
						} catch (Exception e) {
						}
					}
					if ("House And Land".equalsIgnoreCase(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								value += stringToDouble(cost) * mainProduct.getDouble("Cost");
							}
							processed = true;
						} catch (Exception e) {
						}
					} else if ("House And Land Total".equalsIgnoreCase(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								value += stringToDouble(cost) * mainProduct.getDouble("TotalCost");
							}
							processed = true;
						} catch (Exception e) {
						}
					} else if ("Total Less House".equalsIgnoreCase(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								if (dripCollection == null)
									value = stringToDouble(cost) * (mainProduct.getDouble("TotalCost") - mainProduct.getDouble("PlanTotalCost"));
								else
									value = stringToDouble(cost) * ((mainProduct.getDouble("TotalCost") + (dripCollection.includedDripAmount - dripCollection.includedPostDripAmount)) - mainProduct.getDouble("PlanTotalCost"));
							}
							processed = true;
						} catch (Exception e) {
						}
					} else if ("Total Less Land".equalsIgnoreCase(costCalcProduct)) {
						try {
							if (cost.replaceAll("[0\\.%]", "").length() == 0) {
								value = drip.TotalCost;
							} else {
								if (dripCollection == null)
									value = stringToDouble(cost) * (mainProduct.getDouble("TotalCost") - mainProduct.getDouble("LotTotalCost"));
								else
									value = stringToDouble(cost) * ((mainProduct.getDouble("TotalCost") + (dripCollection.includedDripAmount - dripCollection.includedPostDripAmount)) - mainProduct.getDouble("LotTotalCost"));
							}
							processed = true;
						} catch (Exception e) {
						}
					}		
				}

				if (processed) {
					drip.TotalCost = value;
					String calcStr = drip.PackageCostCalcString;
					if (condition.length() > 0 && condition.indexOf(calcStr) == -1) {
						if (calcStr.length() > 0)
							calcStr += " +<br />";
						if (!condition.toLowerCase().endsWith("<br>"))
							condition += ",<br>";
						calcStr += condition
								+ costCalcProduct + " (" + currency.format(value / stringToDouble(cost)) + ") X " + cost;
					} else {
						if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
							condition += ",<br>";
						calcStr += condition
								+costCalcProduct + " (" + currency.format(value / stringToDouble(cost)) + ") X " + cost;
					}
					drip.PackageCostCalcString = calcStr;
				}
			//} else{
			//	processed = false;
			}
			if (!processed) {
				double tCost = 0;
				try {
					tCost = drip.TotalCost + Double.parseDouble(cost);
				} catch (Exception e) {
				}
				drip.TotalCost = tCost;
				if (!"-".equals(cost) && !"0%".equals(cost) && !"%".equals(cost) && stringToDouble(cost) != 0) {
					String calcStr = drip.PackageCostCalcString;
					if (condition.length() > 0 && condition.indexOf(calcStr) == -1) {
						if (calcStr.length() > 0)
							calcStr += " +<br />";
						if (!condition.toLowerCase().endsWith("<br>"))
							condition += ",<br>";
						calcStr += condition + " "
								+ currency.format(Double
										.parseDouble(cost));
					} else {
						if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
							condition += ",<br>";
						if (!cost.endsWith("%")) {
							calcStr = condition + " "
									+ currency.format(Double
											.parseDouble(cost));
						} else {
							calcStr = condition + " "
									+ cost;
						}
					}
					drip.PackageCostCalcString = calcStr;
				} else if ("-".equals(cost) || "0%".equals(cost) || "%".equals(cost)) {
					String calcStr = drip.PackageCostCalcString;
					if (condition.length() > 0 && condition.indexOf(calcStr) == -1) {
						if (calcStr.length() > 0)
							calcStr += " +<br />";
						if (!condition.toLowerCase().endsWith("<br>"))
							condition += ",<br>";
						calcStr += condition
								+ " $0.00";
					} else {
						if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
							condition += ",<br>";
						calcStr = condition
								+ " $0.00";
					}
					drip.PackageCostCalcString = calcStr;
				} else if ("0".equals(cost) && "Y".equals(active)) {
					drip.PackageCostCalcString = "Information Only";
				}
			}
		} else if (costCalcProduct != null && costCalcProduct.indexOf("Package Cost") >= 0 && costCalcField != null && costCalcField.startsWith("All")) {
			costCalcField = costCalcField.substring(3);
			if (costCalcField.startsWith(":")) costCalcField = costCalcField.substring(1);

			calculatePackageCost(conn, rootProductID, drip, costCalcField, cost);
		} else if (costCalcProduct != null && costCalcProduct.indexOf("Package Cost") >= 0) {
			GenRow row = getRowForPackageCost(conn, rootProductID, costCalcField);

			if(logger.isTraceEnabled()) logger.trace(row.getStatement());

			try {
				double total = stringToDouble(cost) * row.getDouble("TotalCost");
				/*
				if (cost.trim().endsWith("%")) {
					String c = cost.substring(0,cost.length() - 1).trim();
					total = (Double.parseDouble(c) / 100)
				 * row.getDouble("TotalCost");
				} else { 
					total = Double.parseDouble(cost)

							row.getDouble("TotalCost");
				}
				 */
				if (total != 0) {
					String calcStr = drip.PackageCostCalcString;
					if (calcStr.length() > 0 && calcStr.equalsIgnoreCase(condition)) calcStr = "";
					if (condition.length() > 0 && condition.indexOf(calcStr) == -1) {
						if (calcStr.length() > 0)
							calcStr += " +<br />";
						if (!condition.toLowerCase().endsWith("<br>"))
							condition += ",<br>";
						calcStr += condition + " ("
								+ row.getString("Name") + " ";
					} else {
						if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
							condition += ",<br>";
						calcStr = condition + " ("
								+ row.getString("Name") + " ";
					}
					String calc = row
							.getString("TotalCost");
					try {
						calc = currency.format(Double
								.parseDouble(calc));
					} catch (Exception e) {
					}
					calcStr += calc
							+ " X ";
					if (cost.trim().endsWith("%")) {
						calcStr += cost;
					} else {
						calcStr += currency.format(Double.parseDouble(cost));
					}
					calcStr += ") "
							+ currency.format(total);
					drip.PackageCostCalcString = calcStr;
					total += drip.TotalCost;
					drip.TotalCost = total;
				}
			} catch (Exception e) {

			}

		} else if (detailsSpec.hasField(costCalcField)) {
			if (costCalcProduct.indexOf("-") >= 0) {
				costCalcProduct = costCalcProduct
						.substring(costCalcProduct
								.indexOf("-") + 1);
				// yes we have to remove it twice
				if (costCalcProduct.indexOf("-") >= 0)
					costCalcProduct = costCalcProduct
					.substring(costCalcProduct
							.indexOf("-") + 1);

				GenRow details  = new GenRow();
				details.setTableSpec("properties/ProductDetails");
				details.setConnection(conn);
				details.setParameter("-select", costCalcField);
				details.setColumn("ProductID",
						rootProductID);
				details.setColumn("DetailsType",
						costCalcProduct);
				try {
					if (details.getString("ProductID").length() > 0) details.doAction("selectfirst");
				} catch (Exception e) {}

				try {
					double total = stringToDouble(cost) * details
							.getDouble(costCalcField);
					/*
					if (cost.trim().endsWith("%")) {
						String c = cost.substring(0,cost.length() - 1);
						total = (Double.parseDouble(c) / 100)
					 * details
								.getDouble(costCalcField);
					} else { 
						total = Double.parseDouble(cost)
					 * details
								.getDouble(costCalcField);
					}
					 */
					if (total != 0) {
						String calcStr = drip.PackageCostCalcString;
						if (condition.length() > 0 && condition.indexOf(calcStr) == -1) {
							if (calcStr.length() > 0)
								calcStr += " +<br>";
							if (!condition.toLowerCase().endsWith("<br>"))
								condition += ",<br>";
							calcStr += condition + " ("
									+ costCalcProduct + ":"
									+ costCalcField + " ";
						} else {
							if (condition.length() > 0 && !condition.toLowerCase().endsWith("<br>"))
								condition += ",<br>";
							calcStr = condition + " ("
									+ costCalcProduct + ":"
									+ costCalcField + " ";
						}
						String calc = details
								.getString(costCalcField);
						try {
							calc = number.format(Double
									.parseDouble(calc));
						} catch (Exception e) {
						}
						calcStr += calc
								+ " X "
								+ currency.format(Double
										.parseDouble(cost))
										+ ") "
										+ currency.format(total);
						drip.PackageCostCalcString = calcStr;
						total += drip.TotalCost;
						drip.TotalCost = total;
					}
				} catch (Exception e) {

				}
			}
		}
		if (theCondition && "Y".equals(active)
				&& costings.getString("Cost").replaceAll("[0\\.]", "").length() == 0 && (costings.getData("ParentID") == null || costings.getData("ParentID").length() == 0)) {
			GenRow child = findCostings(conn, drip.ProductID, costings.getString("CostingID"));
			if (child != null) {
				if ("Information Only".equals(drip.PackageCostCalcString)) drip.PackageCostCalcString = "";
				String a = "N";
				boolean done = false;
				while (child.getNext()) {
					done = true;
					if ("Y".equals(doDripProcessing(conn, drip, child, mainProduct, quoteViewID, orderID, contactID))) a = "Y";
				}
				child.close();
				if (done) active = a;
			}
		}

		return active;
	}

	private void calculatePackageCost(Connection conn, String productID, DripProduct drip, String category, String cost) {
		String condition = "";
		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(conn);
		row.setParameter("ProductType", "Package Cost+Package Cost Library");
		row.setParameter("ProductProductsLinkedProduct.ProductType", "Package Cost+Package Cost Library");
		row.setParameter("ProductID", productID);
		if (category.length() > 0) {
			row.setParameter("Category",category);
			row.setParameter("ProductProductsLinkedProduct.Category",category);
		}
		row.doAction("search");

		row.getResults();

		while (row.getNext()) {
			try {
				double total = stringToDouble(cost) * row.getDouble("TotalCost");
				/*
				if (cost.trim().endsWith("%")) {
					String c = cost.substring(0,cost.length() - 1);
					total = (Double.parseDouble(c) / 100)
				 * row.getDouble("TotalCost");
				} else { 
					total = Double.parseDouble(cost)
				 * row.getDouble("TotalCost");
				}
				 */
				if (total != 0) {
					String calcStr = drip.PackageCostCalcString;
					if (calcStr.length() > 0)
						calcStr += " +<br />";
					if (condition.length() > 0)
						condition += "";
					calcStr += condition + " ("
							+ row.getString("Name") + " ";
					String calc = row
							.getString("TotalCost");
					try {
						calc = currency.format(Double
								.parseDouble(calc));
					} catch (Exception e) {
					}
					calcStr += calc
							+ " X ";
					if (cost.trim().endsWith("%")) {
						calcStr += cost;
					} else {
						calcStr += currency.format(Double.parseDouble(cost));
					}
					calcStr += ") "
							+ currency.format(total);
					drip.PackageCostCalcString = calcStr;
					total += drip.TotalCost;
					drip.TotalCost = total;
				}
			} catch (Exception e) {

			}

		}
		row.close();

	}

	private Date dateString(String date) {
		if (date == null || date.length() == 0)
			return null;

		if (date.indexOf(".") > 0)
			date = date.substring(0, date.indexOf("."));
		try {
			return dformat.parse(date);
		} catch (Exception e) {
			if (date.indexOf(" ") > 0) {
				try {
					return sformat.parse(date.substring(0, date.indexOf(" ")));
				} catch (Exception e2) {
				}
			}
		}

		return null;
	}

	private double stringToDouble(String input){
		double d = 0;
		try{
			input = input.trim();
			if (input.endsWith("%")) {
				input = input.substring(0,input.length() - 1);
				d = Double.parseDouble(input) / 100;
			} else {
				d = Double.parseDouble(input);
			}
		}catch (Exception e) {
		}
		return d;
	}

	private GenRow findCostings(Connection conn, String productID, String parentID) {

		GenRow costings = new GenRow();
		costings.setTableSpec("PackageCostings");
		costings.setConnection(conn);
		costings.setParameter("-select0", "*");
		costings.setColumn("ParentProductID", productID);
		costings.setColumn("ParentID", parentID);
		costings.sortBy("SortOrder", 0);
		costings.sortOrder("ASC", 0);
		costings.doAction("search");

		costings.getResults(true);

		if (costings.isSuccessful() && costings.getSize() > 0) {
			return costings;
		}
		costings.close();

		GenRow product = new GenRow();
		product.setTableSpec("Products");
		product.setConnection(conn);
		product.setParameter("-select", "PackageCostProductID");
		product.setColumn("ProductID", productID);
		product.doAction("selectfirst");

		if (product.isSuccessful()
				&& product.getString("PackageCostProductID").length() > 0) {
			return findCostings(conn, product.getString("PackageCostProductID"),
					parentID);
		}

		return null;
	}

	// we need to cache everything we can to speed this up a lot
	private GenRow getCachedTabledProduct(Connection conn, String tableSpec, String key,
			String value) {
		//GenRow row = productMap.get(value + tableSpec);

		//if (row != null)
		//return row;

		GenRow row = new GenRow();
		row.setTableSpec(tableSpec);
		row.setConnection(conn);
		row.setParameter("-select", "*");
		row.setColumn(key, value);

		row.doAction("selectfirst");

		//productMap.put(value + tableSpec, row);

		return row;
	}

	protected boolean checkCondition(Connection conn, GenRow costings, String houselandid) {
		if (costings.getString("IfDimension").length() == 0
				|| costings.getString("IfDimValue").length() == 0
				|| "FreeForm".equals(costings.getString("IfDimension")))
			return true;

		String ifDim = costings.getString("IfDimension");
		if (ifDim.indexOf("-") >= 0)
			ifDim = ifDim.substring(ifDim.indexOf("-") + 1);

		if ("Package Cost".equals(ifDim)) return checkPackageCondition(conn, costings, houselandid);

		String ifDimValue = costings.getString("IfDimValue");
		if (ifDimValue.indexOf("-") >= 0)
			ifDimValue = ifDimValue.substring(ifDim.indexOf("-") + 1);


		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductDetails");
		row.setConnection(conn);
		row.setParameter("-select", ifDimValue);
		row.setParameter("DetailsType", ifDim);
		row.setParameter("ProductID", houselandid);
		if (row.getString("ProductID").length() > 0) row.doAction("selectfirst");

		if(logger.isTraceEnabled()) logger.trace(row.getStatement());

		try {
			double value = Double.parseDouble(costings.getString("IfValue"));

			if (row.isSuccessful() && row.getString(ifDimValue).length() > 0) {
				if (costings.getString("IfCalc").indexOf("-") > 0) {
					try {
						String range = costings.getString("IfCalc");
						String[] parts = range.split("-");
						double from = Double.parseDouble(parts[0].trim());
						double to = Double.parseDouble(parts[1].trim());
						return row.getDouble(ifDimValue) >= from
								&& row.getDouble(ifDimValue) <= to;
					} catch (Exception e) {
					}
				} else if ("<".equals(costings.getString("IfCalc"))) {
					return row.getDouble(ifDimValue) < value;
				} else if (">".equals(costings.getString("IfCalc"))) {
					return row.getDouble(ifDimValue) > value;
				} else {
					return row.getDouble(ifDimValue) == value;
				}
			}
		} catch (Exception e) {
			String value = costings.getString("IfValue");

			if (row.isSuccessful() && row.getString(ifDimValue).length() > 0) {
				if (costings.getString("IfCalc").indexOf("-") > 0) {
					try {
						String range = costings.getString("IfCalc");
						String[] parts = range.split("-");
						double from = Double.parseDouble(parts[0].trim());
						double to = Double.parseDouble(parts[1].trim());
						return row.getDouble(ifDimValue) >= from
								&& row.getDouble(ifDimValue) <= to;
					} catch (Exception e1) {
					}
				} else if ("<".equals(costings.getString("IfCalc"))) {
					return row.getString(ifDimValue).compareToIgnoreCase(value) < 0;
				} else if (">".equals(costings.getString("IfCalc"))) {
					return row.getString(ifDimValue).compareToIgnoreCase(value) > 0;
				} else {
					return row.getString(ifDimValue).compareToIgnoreCase(value) == 0;
				}
			} if ("!".equals(value) || "=!".equals(value)) {
				return true;
			}
		}

		return false;
	}

	private boolean checkPackageCondition(Connection conn, GenRow costings,	String houselandid) {
		String ifDimValue = costings.getString("IfDimValue");

		GenRow cost = null;

		cost = getRowForPackageCost(conn, houselandid, ifDimValue);

		if (cost != null) {
			GenRow row = cost;
			try {
				double value = Double.parseDouble(costings.getString("IfValue"));

				if (row.isSuccessful() && row.getString("TotalCost").length() > 0) {
					if (costings.getString("IfCalc").indexOf("-") > 0) {
						try {
							String range = costings.getString("IfCalc");
							String[] parts = range.split("-");
							double from = Double.parseDouble(parts[0].trim());
							double to = Double.parseDouble(parts[1].trim());
							return row.getDouble("TotalCost") >= from
									&& row.getDouble("TotalCost") <= to;
						} catch (Exception e) {
						}
					} else if ("<".equals(costings.getString("IfCalc"))) {
						return row.getDouble("TotalCost") < value;
					} else if (">".equals(costings.getString("IfCalc"))) {
						return row.getDouble("TotalCost") > value;
					} else {
						return row.getDouble("TotalCost") == value;
					}
				}
			} catch (Exception e) {
				String value = costings.getString("IfValue");

				if (row.isSuccessful() && row.getString("TotalCost").length() > 0) {
					if (costings.getString("IfCalc").indexOf("-") > 0) {
						try {
							String range = costings.getString("IfCalc");
							String[] parts = range.split("-");
							double from = Double.parseDouble(parts[0].trim());
							double to = Double.parseDouble(parts[1].trim());
							return row.getDouble("TotalCost") >= from
									&& row.getDouble("TotalCost") <= to;
						} catch (Exception e1) {
						}
					} else if ("<".equals(costings.getString("IfCalc"))) {
						return row.getString("TotalCost").compareToIgnoreCase(value) < 0;
					} else if (">".equals(costings.getString("IfCalc"))) {
						return row.getString("TotalCost").compareToIgnoreCase(value) > 0;
					} else {
						return row.getString("TotalCost").compareToIgnoreCase(value) == 0;
					}
				}
			}
		}
		return false;
	}

	private GenRow getRowForPackageCost(Connection conn, String productID, String packageCostID) {
		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(conn);
		row.setParameter("ProductType", "Package Cost+Package Cost Library");
		row.setParameter("ProductProductsLinkedProduct.ProductType", "Package Cost+Package Cost Library");
		row.setParameter("ProductID", productID);
		row.doAction("search");

		row.getResults();

		GenRow r = new GenRow();
		r.setViewSpec("ProductView");
		r.setConnection(conn);

		GenRow cost = null;
		while (row.getNext()) {
			if (row.getString("PackageCostProductID").length() > 0) {
				cost = findRootCostings(conn, row.isSet("PackageCostProductID")? row.getString("PackageCostProductID") : row.getString("ProductID"));
				if (cost.getData("ProductID").equals(packageCostID)) {
					break;
				} else {
					cost = null;
				}
			} else if (row.getData("LinkedProductID").equals(packageCostID)) {
				cost = row;
				break;
			}
		}


		if (cost != null && cost.getData("ProductID").equals(packageCostID)) {
			r.setParameter("ProductID", row.getString("LinkedProductID"));
			r.doAction("selectfirst");
		}

		row.close();

		return r;
	}
	protected static GenRow findRootCostings(Connection conn, String productID) {
		if(logger.isTraceEnabled()) logger.trace("findRootCosting(Con, {})", productID);
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");
		if(logger.isTraceEnabled()) logger.trace("have {}, {}", row.getString("ProductID"), row.getString("ProductType"));
		if (row.getString("PackageCostProductID").length() > 0) {
			GenRow tmp = findRootCostings(conn,row.getString("PackageCostProductID"));
			if (StringUtils.isNotBlank(tmp.getData("ProductID"))) {
				return tmp;
			}
		}
		if(logger.isTraceEnabled()) logger.trace("returing {}, {}", row.getString("ProductID"), row.getString("ProductType"));
		return row;
	}

	protected static GenRow findRootPlan(Connection conn, String productID) {
		if(logger.isTraceEnabled()) logger.trace("findRootPlan(Con, {})", productID);
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");
		if(logger.isTraceEnabled()) logger.trace("have {}, {}", row.getString("ProductID"), row.getString("ProductType"));
		if (row.getString("PackageCostProductID").length() > 0) return findRootCostings(conn,  row.isSet("PackageCostProductID")? row.getString("PackageCostProductID") : row.getString("ProductID"));
		if(logger.isTraceEnabled()) logger.trace("returing {}, {}", row.getString("ProductID"), row.getString("ProductType"));
		return row;
	}

	protected String calculate(String string) {
		if (string == null || string.length() == 0)
			return "";

		if ("-".equals(string)) return string;

		int count = 0;
		String result = string.replaceAll("  ", " ").replaceAll("  ", " ");
		// do this until there is no more maths or 20 attempts (so we don't get
		// into an endless loop due to bad maths
		while (result.length() != result.replaceAll("[\\+\\-\\*\\/]", "")
				.length() && ++count < 20) {
			// tokenize it, make sure all double spaces become single space
			StringTokenizer st = new StringTokenizer(result, " ");
			while (st.hasMoreTokens()) {
				String token1 = st.nextToken();
				if (token1.indexOf("=") >= 0 || token1.indexOf("<") >= 0
						|| token1.indexOf(">") >= 0)
					continue;
				if (token1.indexOf("+") >= 0 || token1.indexOf("-") >= 0
						|| token1.indexOf("*") >= 0 || token1.indexOf("/") >= 0)
					break;
				if (st.hasMoreTokens()) {
					String maths = st.nextToken();
					if (maths.indexOf("=") >= 0 || maths.indexOf("<") >= 0
							|| maths.indexOf(">") >= 0)
						continue;
					if (st.hasMoreTokens()) {
						String token2 = st.nextToken();
						if (token2.indexOf("=") >= 0
								|| token2.indexOf("<") >= 0
								|| token2.indexOf(">") >= 0)
							continue;
						double a = 0;
						try {
							double d1 = Double.parseDouble(token1);
							double d2 = Double.parseDouble(token2);
							if ("+".equals(maths))
								a = d1 + d2;
							else if ("-".equals(maths))
								a = d1 - d2;
							else if ("*".equals(maths))
								a = d1 * d2;
							else if ("/".equals(maths))
								a = d1 / d2;
							a = (double) Math.floor((double) a * 1000) / 1000;
						} catch (Exception e) {
						}
						StringBuilder sb = (new StringBuilder(token1))
								.append(" \\").append(maths).append(" ")
								.append(token2);
						if (!result.equals(result.replaceFirst(sb.toString(),
								String.valueOf(a)))) {
							result = result.replaceFirst(sb.toString(),
									String.valueOf(a));
						} else {
							sb = (new StringBuilder(token1)).append(" \\")
									.append(maths).append(" ").append(token2);
							if (!result.equals(result.replaceFirst(
									sb.toString(), String.valueOf(a)))) {
								result = result.replaceFirst(sb.toString(),
										String.valueOf(a));
							} else {
								sb = (new StringBuilder(token1)).append(maths)
										.append(token2);
								if (!result.equals(result.replaceFirst(
										sb.toString(), String.valueOf(a)))) {
									result = result.replaceFirst(sb.toString(),
											String.valueOf(a));
								} else {
									sb = (new StringBuilder(token1))
											.append("\\").append(maths)
											.append(token2);
									if (!result.equals(result.replaceFirst(
											sb.toString(), String.valueOf(a)))) {
										result = result.replaceFirst(
												sb.toString(),
												String.valueOf(a));
									}

								}

							}
						}
					}
				}
			}
		}

		return result;
	}

	protected boolean checkConditionFree(Connection conn, GenRow costings, String orderID, String contactID, GenRow mainProduct) {
		if (costings.getString("IfDimension").length() == 0
				|| costings.getString("IfDimValue").length() == 0
				|| !"FreeForm".equals(costings.getString("IfDimension")))
			return true;

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductDetails");

		String ifDim = costings.getString("IfDimValue");
		String[] partss = ifDim.split("[\\+\\-\\*\\/\\<\\=\\!\\>]");
		for (int p = 0; p < partss.length; ++p) {
			String tag = partss[p].trim();
			if (tag.length() == tag.replaceAll("[0-9]", "").length()) {
				String[] fields = tag.split("\\.");
				
				if (fields.length == 3 && "Profile".equals(fields[0])) {
					
					GenRow answer = new GenRow();
					if (StringUtils.isNotBlank(orderID)) {
						answer.setViewSpec("answers/OrderAnswerView");
						answer.setParameter("OrderAnswerQuestion.Label|1", fields[1]);
						answer.setParameter("OrderAnswerQuestion.ShortLabel|1", fields[1]);
						answer.setParameter("OrderID", orderID);
						answer.doAction("selectfirst");
					}
					if (!answer.isSet("AnswerID") && StringUtils.isNotBlank(contactID)) {
						answer.setViewSpec("answers/ContactAnswerView");
						answer.setParameter("ContactAnswerQuestion.Label|1", fields[1]);
						answer.setParameter("ContactAnswerQuestion.ShortLabel|1", fields[1]);
						answer.setParameter("ContactID", contactID);
						answer.doAction("selectfirst");
					}
					if (!answer.isSet("AnswerID") && StringUtils.isNotBlank(mainProduct.getData("ProductID"))) {
						answer.setViewSpec("answers/ProductAnswerView");
						answer.setParameter("OrderAnswerQuestion.Label|1", fields[1]);
						answer.setParameter("OrderAnswerQuestion.ShortLabel|1", fields[1]);
						answer.setParameter("ProductID", mainProduct.getData("ProductID"));
						answer.doAction("selectfirst");
					}
					if (answer.isSet("AnswerID") && answer.isSet(fields[2])) {
						ifDim = ifDim.replaceAll(tag.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]").replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)"),
								" " + answer.getString(fields[2]).replaceAll(" ", "_") + " ");
					} else {
						ifDim = ifDim.replaceAll(tag.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]").replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)"),
								" -missing- ");
					}
				} else if (fields.length == 3) {
					row.clear();
					row.setConnection(conn);
					row.setParameter("-select", fields[2]);
					row.setParameter("DetailsType", fields[1]);
					row.setParameter("ProductID", mainProduct.getData("ProductID"));
					if (row.getString("ProductID").length() > 0) row.doAction("selectfirst");

					if (row.isSuccessful()) {
						ifDim = ifDim.replaceAll(tag.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]").replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)"),
								" " + row.getString(fields[2]) + " ");
					}
				} else if (fields.length == 2) {
					if ("TitleDate".equalsIgnoreCase(fields[0])) {
						GenRow homeDetails = getCachedTabledProduct(conn, "properties/HomeDetails", "ProductID", mainProduct.getData("ProductID"));
						if (homeDetails.isSuccessful()) {
							if ("Month".equalsIgnoreCase(fields[1])) {
								String month = homeDetails.getString("LotTitleMonth");
								if (month.length() > 3) month = month.substring(0,3);
								ifDim = ifDim.replaceAll(tag.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]").replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)"),
										" " + month + " ");
							} else if ("MonthYear".equalsIgnoreCase(fields[1])) {
								String month = homeDetails.getString("LotTitleMonth");
								if (month.length() > 3) month = month.substring(0,3);
								ifDim = ifDim.replaceAll(tag.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]").replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)"),
										" " + month + homeDetails.getString("LotTitleYear") + " ");
							} else if ("MonthsToTitle".equalsIgnoreCase(fields[1])) {
								String month = getMonthsToTitle(homeDetails.getString("LotTitleMonth"),homeDetails.getString("LotTitleYear"));
								ifDim = ifDim.replaceAll(tag.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]").replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)"),
										" " + month + " ");
							}
						}
					} else if ("Package".equalsIgnoreCase(fields[0])) {
						// put NO_DATA, if not conditions like Package.MarketingStatus != Custom will be formatted to " != Custom" which does not pass through condition evaluation properly
						String dbValue = StringUtils.defaultIfEmpty(mainProduct.getString(fields[1]).replaceAll(" ", "_") , "NO_DATA");								
						ifDim = ifDim.replaceAll(tag.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]").replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)")," " + dbValue + " ");
					}
				}
			}
		}

		if (ifDim.length() == ifDim.replaceAll("[a-zA-Z]", "").length()) {
			ifDim = calculate(ifDim);

			boolean ret = true;
			StringTokenizer st = new StringTokenizer(ifDim, " ");
			if (st.hasMoreTokens()) {
				String token1 = st.nextToken();
				while (st.hasMoreTokens()) {
					if (token1.indexOf("=") >= 0 || token1.indexOf("<") >= 0
							|| token1.indexOf(">") >= 0)
						return false;
					if (st.hasMoreTokens()) {
						String maths = st.nextToken();
						if (maths.indexOf("=") == -1
								&& maths.indexOf("<") == -1
								&& maths.indexOf(">") == -1)
							return false;
						if (st.hasMoreTokens()) {
							String token2 = st.nextToken();
							if (token2.indexOf("=") >= 0
									|| token2.indexOf("<") >= 0
									|| token2.indexOf(">") >= 0)
								return false;
							try {
								double d1 = Double.parseDouble(token1);
								double d2 = Double.parseDouble(token2);
								if ("=".equals(maths) || "==".equals(maths))
									ret = d1 == d2;
								else if ("<".equals(maths))
									ret = d1 < d2;
								else if ("<=".equals(maths))
									ret = d1 <= d2;
								else if (">".equals(maths))
									ret = d1 > d2;
									else if (">=".equals(maths) || "=>".equals(maths))
										ret = d1 >= d2;
										else if ("!=".equals(maths))
											ret = d1 != d2;
							} catch (Exception e) {
								try {
									if ("=".equals(maths) || "==".equals(maths))
										ret = token1.trim().equalsIgnoreCase(token2.trim());
									else if ("<".equals(maths))
										ret = token1.trim().compareToIgnoreCase(token2.trim()) < 0;
									else if ("<=".equals(maths))
										ret = token1.trim().compareToIgnoreCase(token2.trim()) <= 0;
									else if (">".equals(maths))
										ret = token1.trim().compareToIgnoreCase(token2.trim()) > 0;
										else if (">=".equals(maths) || "=>".equals(maths))
											ret = token1.trim().compareToIgnoreCase(token2.trim()) >= 0;
											else if ("!=".equals(maths))
												ret = !token1.trim().equalsIgnoreCase(token2.trim());
								} catch (Exception e2) {
									return false;
								}
							}
							token1 = token2;
						}
					}
					if (!ret)
						break;
				}
			}
			return ret;
		} else {
			boolean ret = true;
			StringTokenizer st = new StringTokenizer(ifDim, " ");
			if (st.hasMoreTokens()) {
				String token1 = st.nextToken();
				while (st.hasMoreTokens()) {
					if (token1.indexOf("!=") >= 0 || token1.indexOf("=") >= 0 || token1.indexOf("<") >= 0
							|| token1.indexOf(">") >= 0)
						return false;
					if (st.hasMoreTokens()) {
						String maths = st.nextToken();
						if (maths.indexOf("=") == -1
								&& maths.indexOf("<") == -1
								&& maths.indexOf(">") == -1)
							return false;
						if (st.hasMoreTokens()) {
							String token2 = st.nextToken();
							if (token2.indexOf("=") >= 0
									|| token2.indexOf("<") >= 0
									|| token2.indexOf(">") >= 0)
								return false;
							try {
								double d1 = Double.parseDouble(token1);
								double d2 = Double.parseDouble(token2);
								if ("=".equals(maths) || "==".equals(maths))
									ret = d1 == d2;
								else if ("<".equals(maths))
									ret = d1 < d2;
								else if ("<=".equals(maths))
									ret = d1 <= d2;
								else if (">".equals(maths))
									ret = d1 > d2;
									else if (">=".equals(maths) || "=>".equals(maths))
										ret = d1 >= d2;
										else if ("!=".equals(maths))
											ret = d1 != d2;
							} catch (Exception e) {
								try {
									if ("=".equals(maths) || "==".equals(maths))
										ret = token1.trim().equalsIgnoreCase(token2.trim());
									else if ("<".equals(maths))
										ret = token1.trim().compareToIgnoreCase(token2.trim()) < 0;
									else if ("<=".equals(maths))
										ret = token1.trim().compareToIgnoreCase(token2.trim()) <= 0;
									else if (">".equals(maths))
										ret = token1.trim().compareToIgnoreCase(token2.trim()) > 0;
										else if (">=".equals(maths) || "=>".equals(maths))
											ret = token1.trim().compareToIgnoreCase(token2.trim()) >= 0;
											else if ("!=".equals(maths))
												ret = !token1.trim().equalsIgnoreCase(token2.trim());
								} catch (Exception e2) {
									return false;
								}
							}
							token1 = token2;
						}
					}
					if (!ret)
						break;
				}
			}
			return ret;
		}

		//return false;
	}

	private String getMonthsToTitle(String month, String year) {
		Calendar now = Calendar.getInstance();
		Calendar title = Calendar.getInstance();
		
		// we need to set it to the first of the month so we don't shift 1 month on by mistake.
		now.set(Calendar.DAY_OF_MONTH, 1);
		title.set(Calendar.DAY_OF_MONTH, 1);

		String[] months = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		int m = 0;
		int y = 0;
		if (month.length() > 3) month = month.substring(0, 3);

		try {
			y = Integer.parseInt(year);
			for (m = 0; m < months.length; ++m) {
				if (month.equalsIgnoreCase(months[m])) break;
			}
			title.set(Calendar.MONTH,m);
			title.set(Calendar.YEAR, y);
			if (now.before(title)) {
				int c = 0;
				while (now.before(title)) {
					now.add(Calendar.MONTH,1);
					++c;
				}
				return String.valueOf(c);
			} else if (now.after(title)) {
				int c = 0;
				while (now.after(title)) {
					now.add(Calendar.MONTH,-1);
					--c;
				}
				return String.valueOf(c);
			}
		} catch (Exception e) {
			return "0";
		}
		return "0";
	}

	protected String getCondition(Connection conn, GenRow costings, String houselandid) {
		if (costings.getString("IfDimension").length() == 0
				|| costings.getString("IfDimValue").length() == 0)
			return "";

		String ifDim = costings.getString("IfDimension");
		if (ifDim.indexOf("-") >= 0)
			ifDim = ifDim.substring(ifDim.indexOf("-") + 1);

		if ("Package Cost".equals(ifDim)) return getPackageCondition(conn, costings, houselandid);

		String ifDimValue = costings.getString("IfDimValue");
		if (ifDimValue.indexOf("-") >= 0)
			ifDimValue = ifDimValue.substring(ifDim.indexOf("-") + 1);

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductDetails");
		row.setConnection(conn);
		row.setParameter("-select", ifDimValue);
		row.setParameter("DetailsType", ifDim);
		row.setParameter("ProductID", houselandid);
		if (row.getString("ProductID").length() > 0) row.doAction("selectfirst");

		String value = costings.getString("IfValue");

		if (row.isSuccessful() && row.getString(ifDimValue).length() > 0) {
			if (costings.getString("IfCalc").indexOf("-") > 0) {
				try {
					return ifDim + ":" + ifDimValue + " "
							+ costings.getString("IfCalc");
				} catch (Exception e) {
				}
			} else if ("<".equals(costings.getString("IfCalc"))) {
				return ifDim + ":" + ifDimValue + " < " + value;
			} else if (">".equals(costings.getString("IfCalc"))) {
				return ifDim + ":" + ifDimValue + " > " + value;
			} else {
				return ifDim + ":" + ifDimValue + " = " + value;
			}
		}

		return "";
	}

	protected String getPackageCondition(Connection conn, GenRow costings, String houselandid) {
		if (costings.getString("IfDimension").length() == 0
				|| costings.getString("IfDimValue").length() == 0)
			return "";

		String ifDimValue = costings.getString("IfDimValue");

		GenRow cost = getRowForPackageCost(conn, houselandid, ifDimValue);

		if (cost != null) {
			String value = costings.getString("IfValue");

			GenRow row = cost;

			if (row.isSuccessful() && row.getString("TotalCost").length() > 0) {
				if (costings.getString("IfCalc").indexOf("-") > 0) {
					try {
						return row.getString("Name") + " "
								+ costings.getString("IfCalc");
					} catch (Exception e) {
					}
				} else if ("<".equals(costings.getString("IfCalc"))) {
					return row.getString("Name") + " < " + value;
				} else if (">".equals(costings.getString("IfCalc"))) {
					return row.getString("Name") + " > " + value;
				} else {
					return row.getString("Name") + " = " + value;
				}
			}
		}

		return "";
	}

	public String getDripImage(final Connection conn, final UserBean user, final String[] productIDs, final String type, final String template, final String placement) {
		if (logger.isTraceEnabled())
			logger.trace("getDripImage(Conn, {}, {}, {})", new String[] { user != null ? user.getName() : "null user", productIDs != null ? productIDs.toString() : "null productIDs" });

		List<String> dripImages = getDripImages(conn, user, productIDs, type, template, placement);

		if (dripImages != null && (dripImages.size() > 0)) {
			return dripImages.get(0);
		}

		return null;
	}

	public String getDripText(final Connection conn, final UserBean user, final String[] productIDs, final String type, final String template, final String placement) {
		if (logger.isTraceEnabled())
			logger.trace("getDripText(Conn, {}, {}, {})", new String[] { user != null ? user.getName() : "null user", productIDs != null ? productIDs.toString() : "null productIDs" });

		List<String> dripTexts = getDripTexts(conn, user, productIDs, type, template, placement);

		if (dripTexts != null && (dripTexts.size() > 0)) {
			return dripTexts.get(0);
		}

		return null;
	}

	public List<String> getDripTexts(final Connection conn, final UserBean user, final String[] productIDs, final String type, final String template, final String placement) {
		if (logger.isTraceEnabled())
			logger.trace("getDripTexts(Conn, {}, {}, {})", new String[] { user != null ? user.getName() : "null user", productIDs != null ? productIDs.toString() : "null productIDs" });

		return getDripInfoForTemplates(conn, user, productIDs, "Text", type, template, placement);
	}

	public List<String> getDripImages(final Connection conn, final UserBean user, final String[] productIDs, final String type, final String template, final String placement) {
		logger.trace("getDripImages(Request, {})");

		return getDripInfoForTemplates(conn, user, productIDs, "Image", type, template, placement);
	}

	/** Returns All the dripText or dripImage attached to the products at a placement for a template
	 * @param conn
	 * @param user
	 * @param productIDs : All ProductIDs for which the Drips needs to retrieved, with Primary Product ID being the last one 
	 * @param type : Text/Image
	 * @param subType : PDF/Quotes/Email
	 * @param templateName : TemplateName or TemplateID
	 * @param placement : position 1/2/3/4/5 for subtype PDF , Value/LineItem for Subtype Quotes
	 * @return
	 */

	private List<String> getDripInfoForTemplates(final Connection conn, final UserBean user, String[] productIDs, String type, String subType, String templateName, String placement) {
		DripCollection collection = getAllDrips(conn, user, productIDs, new Date());
		PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(conn, productIDs[productIDs.length - 1]);

		List<String> list = new ArrayList<String>();
		for (DripProduct dp : collection.allDrips) {
			// escape $(Regex metacharacter)
			// 22-June-2012 NI: String value = "\\" + currency.format(Math.ceil(dp.Value));
			String value = "\\" + currency.format(Math.floor(dp.Value));
			if (value.indexOf(".") > 0) {
				value = value.substring(0, value.indexOf("."));
			}
			if (value.startsWith("\\-")) {
				value = "-\\$" + value.substring(3);
			}

			// 22-June-2012 NI: String result = "\\" + currency.format(Math.ceil(collection.NetAmount + dp.Value));
			String result = "\\" + currency.format(Math.floor(collection.NetAmount + Math.floor(dp.Value)));
			if (result.indexOf(".") > 0) {
				result = result.substring(0, result.indexOf("."));
			}

			String exclude = "\\" + currency.format(Math.floor(collection.NetAmount - Math.floor(dp.Value)));
			if (exclude.indexOf(".") > 0) {
				exclude = exclude.substring(0, exclude.indexOf("."));
			}

			GenRow documents = new GenRow();
			try { 
				documents.setConnection(conn);
				documents.setViewSpec("LinkedDocumentView");
				if ("Global".equals(dp.Category)) 
					documents.setParameter("ProductID", dp.ProductID);
				else
					documents.setParameter("ProductID", findRootCostings(conn, StringUtils.isNotBlank(dp.PackageCostProductID)? dp.PackageCostProductID : dp.ProductID).getString("ProductID"));
				documents.setParameter("DocumentSubType", subType);
				documents.setParameter("LinkedDocumentDocument.DocumentSubType", subType);
				documents.setParameter("DocumentType", type);
				if (placement != null) {
					documents.setParameter("LinkedDocumentDocument.Placement", placement);
				}
				if (templateName != null) {
					documents.setParameter("LinkedDocumentDocument.TemplateName", templateName);
				}
				documents.sortBy("Name", 0);
				documents.doAction("search");
				documents.getResults();

				while (documents.getNext()) {
					if ("Image".equals(type))
						list.add(documents.getString("FilePath"));
					else if ("Text".equals(type)) {
						String description = replaceTokens(documents.getString("Description"), value, result, exclude, mainProduct);
						if ("15".equals(documents.getString("Placement")) || "16".equals(documents.getString("Placement"))) {
							Conditions conditions = new Conditions(new HashMap<String,String>());
							if (description.startsWith("$ ")) {
								description = conditions.calculate(description.substring(2).replaceAll("[^0-9\\.\\-\\+\\*\\/ ]", ""));
								try {
									description = "" + Math.ceil(Double.parseDouble(description.replaceAll("[^0-9\\.\\-]", "")));
								} catch (Exception e) {}
								description = "$" + description.replaceFirst("\\.0","");
							} else {
								description = conditions.calculate(description);
							}
						}
						list.add(description);
					}
				}
			} finally { 
				documents.close();
			}
			if(logger.isDebugEnabled()) { 
				logger.debug(dp.toString());
			} 
		}
		return list;
	}
	
	public DripCollection getAllDripsInfo(final HttpServletRequest request, String[] productIDs) {
		return getAllDripsInfo(ActionBean.getConnection(request), us.getCurrentUser(request), productIDs);
	}

	/** Returns All the dripText or dripImage attached to the products at a placement for a template
	 * @param conn
	 * @param user
	 * @param productIDs : All ProductIDs for which the Drips needs to retrieved, with Primary Product ID being the last one 
	 * @param type : Text/Image
	 * @param subType : PDF/Quotes/Email
	 * @param templateName : TemplateName or TemplateID
	 * @param placement : position 1/2/3/4/5 for subtype PDF , Value/LineItem for Subtype Quotes
	 * @return
	 */

	public DripCollection getAllDripsInfo(final Connection conn, final UserBean user, String[] productIDs) {
		DripCollection collection = getAllDrips(conn, user, productIDs, new Date());
		PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(conn, productIDs[productIDs.length - 1]);

		for (DripProduct dp : collection.allDrips) {
			List<DripContent> list = new ArrayList<DripContent>();
			// escape $(Regex metacharacter)
			// 22-June-2012 NI: String value = "\\" + currency.format(Math.ceil(dp.Value));
			String value = "\\" + currency.format(Math.floor(dp.Value));
			if (value.indexOf(".") > 0) {
				value = value.substring(0, value.indexOf("."));
			}
			if (value.startsWith("\\-")) {
				value = "-\\$" + value.substring(3);
			}

			// 22-June-2012 NI: String result = "\\" + currency.format(Math.ceil(collection.NetAmount + dp.Value));
			String result = "\\" + currency.format(Math.floor(collection.NetAmount + Math.floor(dp.Value)));
			if (result.indexOf(".") > 0) {
				result = result.substring(0, result.indexOf("."));
			}

			String exclude = "\\" + currency.format(Math.floor(collection.NetAmount - Math.floor(dp.Value)));
			if (exclude.indexOf(".") > 0) {
				exclude = exclude.substring(0, exclude.indexOf("."));
			}

			GenRow documents = new GenRow();
			try { 
				documents.setConnection(conn);
				documents.setViewSpec("LinkedDocumentView");
				if ("Global".equals(dp.Category)) 
					documents.setParameter("ProductID", dp.ProductID);
				else
					documents.setParameter("ProductID", findRootCostings(conn, StringUtils.isNotBlank(dp.PackageCostProductID)? dp.PackageCostProductID : dp.ProductID).getString("ProductID"));
				documents.sortBy("DocumentSubType", 0);
				documents.sortBy("Placement", 1);
				documents.getResults(true );

				while (documents.getNext()) {
					String description = "";
					if ("Text".equals(documents.getString("DocumentType"))) description = replaceTokens(documents.getString("Description"), value, result, exclude, mainProduct);
					if ("15".equals(documents.getString("Placement")) || "16".equals(documents.getString("Placement"))) {
						Conditions conditions = new Conditions(new HashMap<String,String>());
						if (description.startsWith("$ ")) {
							description = conditions.calculate(description.substring(2).replaceAll("[^0-9\\.\\-\\+\\*\\/ ]", ""));
							try {
								description = "" + Math.ceil(Double.parseDouble(description.replaceAll("[^0-9\\.\\-]", "")));
							} catch (Exception e) {}
							description = "$" + description.replaceFirst("\\.0","");
						} else {
							description = conditions.calculate(description);
						}
					}
					list.add(new DripContent(documents, description));
				}
			} finally { 
				documents.close();
			}
			if(logger.isDebugEnabled()) { 
				logger.debug(dp.toString());
			}
			dp.DripContent = list;
		}
		return collection;
	}

	/** Returns All the dripText or dripImage attached to the products at a placement for a template
	 * @param conn
	 * @param user
	 * @param productIDs : All ProductIDs for which the Drips needs to retrieved, with Primary Product ID being the last one 
	 * @param type : Text/Image
	 * @param subType : PDF/Quotes/Email
	 * @param templateName : TemplateName or TemplateID
	 * @param placement : position 1/2/3/4/5 for subtype PDF , Value/LineItem for Subtype Quotes
	 * @return
	 */

	public HashMap<String, String> getDripMapForTemplates(final Connection conn, final UserBean user, String[] productIDs, String subType, String template) {
		return getDripMapForTemplates(conn, user, productIDs, subType, template, null, -1, null); // New Overloaded Method. Added to cope with the Regional Pricing changes
	}

	public HashMap<String, String> getDripMapForTemplates(final Connection conn, final UserBean user, String[] productIDs, String subType, String template, String dripList) {
		return getDripMapForTemplates(conn, user, productIDs, subType, template, null, -1, dripList); // New Overloaded Method. Added to cope with the Regional Pricing changes
	}



	/** This is an Overloaded method to accomodate the RegionID and any other request Param. 
	 * This change is initiated with PDH Regional Pricing.
	 * @return
	 */
	public HashMap<String, String> getDripMapForTemplates(final Connection conn, final UserBean user, String[] productIDs, String subType, String template, TableData requestrow, int regionIndex) {
		return getDripMapForTemplates(conn, user, productIDs, subType, template, requestrow, regionIndex, null);
	}
	public HashMap<String, String> getDripMapForTemplates(final Connection conn, final UserBean user, String[] productIDs, String subType, String template, TableData requestrow, int regionIndex, String dripList) {
		if(logger.isDebugEnabled()) logger.debug("getDripMapForTemplates(Conn, {}, [{}], {}, {}, {})", new String[]{user != null ? user.getName() : "null user", StringUtils.join(productIDs, ","), subType, template, (requestrow != null && requestrow.getString("RegionID") != null) ? requestrow.getString("RegionID") : "null region"} );
		DripCollection collection = getAllDrips(conn, user, productIDs, new Date(), requestrow, regionIndex);
		PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(conn, productIDs[productIDs.length - 1]);

		// Map with [DocumentType+Placement] as the key and corresponding the drip products as value
		HashMap<String, List<DripDocument>> allDripMap = new HashMap<String, List<DripDocument>>();

		// token for drip cost
		String netAmount = currency.format(Math.floor(collection.NetAmount));
		if (netAmount.indexOf(".") > 0) {
			netAmount = netAmount.substring(0, netAmount.indexOf("."));

			netAmount = netAmount.replaceAll("\\\\", "");
		}

		for (DripProduct dp : collection.allDrips) {
			if (StringUtils.isNotBlank(dripList) && dripList.indexOf(dp.ProductID) == -1) continue;
			if ("Y".equals(dp.Active)) {
				// escape $(Regex metacharacter)
				// 22-Jun-2012 NI: String value = "\\" + currency.format(Math.ceil(dp.Value));
				String value = "\\" + currency.format(Math.floor(dp.Value));
				if (value.indexOf(".") > 0) {
					value = value.substring(0, value.indexOf("."));
				}
				if (value.startsWith("\\-")) {
					value = "-\\$" + value.substring(3);
				}

				// 22-Jun-2012 NI: String result = "\\" + currency.format(Math.ceil(collection.NetAmount + dp.Value));
				String result = "\\" + currency.format(Math.floor(collection.NetAmount + Math.floor(dp.Value)));
				if (result.indexOf(".") > 0) {
					result = result.substring(0, result.indexOf("."));
				}

				String exclude = "\\" + currency.format(Math.floor(collection.NetAmount - Math.floor(dp.Value)));
				if (exclude.indexOf(".") > 0) {
					exclude = exclude.substring(0, exclude.indexOf("."));
				}



				GenRow documents = new GenRow();
				try {
					documents.setConnection(conn);
					documents.setViewSpec("LinkedDocumentView");
					//documents.setParameter("-select1", "LinkedDocumentDocument.TemplateName AS TemplateName");
					if ("Global".equals(dp.Category)) 
						documents.setParameter("ProductID", dp.ProductID);
					else
						documents.setParameter("ProductID", findRootCostings(conn, StringUtils.isNotBlank(dp.PackageCostProductID)? dp.PackageCostProductID : dp.ProductID).getString("ProductID"));
					documents.setParameter("DocumentSubType", subType);
					documents.setParameter("LinkedDocumentDocument.DocumentSubType", subType);
					//if (template != null) {
					//	documents.setParameter("TemplateName", template);
					//	documents.setParameter("LinkedDocumentDocument.TemplateName", template);
					//}
					documents.sortBy("Name", 0);

					if(logger.isTraceEnabled()) {
						documents.doAction(GenerationKeys.SEARCH);
						logger.trace(documents.getStatement());
					}

					documents.getResults(true);

					//System.out.println(documents.getStatement());

					while (documents.getNext()) {
						String infoText = "";
						if ("Image".equals(documents.getString("DocumentType")))
							infoText = documents.getString("FilePath");
						else if ("Text".equals(documents.getString("DocumentType"))) {
							infoText = replaceTokens(documents.getString("Description"), value, result, exclude, mainProduct);
							if ("15".equals(documents.getString("Placement")) || "16".equals(documents.getString("Placement"))) {
								Conditions conditions = new Conditions(new HashMap<String,String>());
								if (infoText.startsWith("$ ")) {
									infoText = conditions.calculate(infoText.substring(2).replaceAll("[^0-9\\.\\-\\+\\*\\/ ]", ""));
									try {
										infoText = "" + Math.ceil(Double.parseDouble(infoText.replaceAll("[^0-9\\.\\-]", "")));
									} catch (Exception e) {}
									infoText = "$" + infoText.replaceFirst("\\.0", "");
								} else {
									infoText = conditions.calculate(infoText);
								}
							}
						}

						List<DripDocument> dripDocs = (List<DripDocument>) allDripMap.get(documents.getString("DocumentType") + documents.getString("TemplateName") + documents.getString("Placement"));

						// 19-June-2012 NI: Added to get the Total Cost - All Included DRIPs cost

						if (dripDocs == null) {
							dripDocs = new ArrayList<DripDocument>();
							if (documents.getString("TemplateName").equals(template)) allDripMap.put(documents.getString("DocumentType") + documents.getString("Placement"), dripDocs);
							allDripMap.put(documents.getString("DocumentType") + documents.getString("TemplateName") + documents.getString("Placement"), dripDocs);



							if(logger.isDebugEnabled()) {
								logger.debug("Type: {}, subType: {}, netAmount: {} , includedDripAmount: {} ,  optionalDripAmount: {} , includedDripCount: {},  optionalDripCount: {},  TotalAmount: {}",
										new String[]{documents.getString("DocumentType") ,subType ,netAmount, String.valueOf(collection.includedDripAmount),String.valueOf(collection.optionalDripAmount)
										, String.valueOf(collection.includedDripCount), String.valueOf(collection.optionalDripCount), String.valueOf(collection.TotalAmount)});
							}
							List<DripDocument> netAmountDripDocList = new ArrayList<DripService.DripDocument>();
							netAmountDripDocList.add(new DripDocument(netAmount, null, null));
							if (documents.getString("TemplateName").equals(template)) allDripMap.put((documents.getString("DocumentType")+"NetAmount"), netAmountDripDocList);
							allDripMap.put((documents.getString("DocumentType") + documents.getString("TemplateName") + "NetAmount"), netAmountDripDocList);
						}

						Date start = null;
						Date end = null;

						if (dp.AvailableDate.length() > 0)
							start = dateString(dp.AvailableDate);

						if (dp.ExpiryDate.length() > 0)
							end = dateString(dp.ExpiryDate);

						dripDocs.add(new DripDocument(infoText, start, end, "Campaign".equals(dp.Summary)));
					}
				} finally {
					documents.close();
				}
			}
			if(logger.isDebugEnabled()) { 
				logger.debug(dp.toString());
			}
		}

		// docMap will contain all drips available for particular slot, it is necessary to pick only the latest text
		HashMap<String, String> latestDripMap = new HashMap<String, String>();

		Iterator<Map.Entry<String, List<DripDocument>>> it = allDripMap.entrySet().iterator();

		while (it.hasNext()) {
			Entry pairs = (Entry)it.next();
			it.remove(); // avoids a ConcurrentModificationException

			latestDripMap.put((String)pairs.getKey(), getLatestDrip((List<DripDocument>) pairs.getValue()));
		}

		latestDripMap.put("DripResult", netAmount);
		latestDripMap.put("DripResultRaw", add(mainProduct.getString("TotalCost") , collection.includedDripAmount));

		return latestDripMap;
	}

	
	private String add(String one, double two) {
		if (StringUtils.isBlank(one)) return currency.format(two);

		double dNet = Double.parseDouble(one.replaceAll("[^0-9\\-\\.]", ""));
		return currency.format(dNet + two);
	}
	
	private String getLatestDrip(List<DripDocument> dripDocs) {

		if(dripDocs == null || dripDocs.size() == 0)
			return "";

		else if(dripDocs.size() == 1)
			return dripDocs.get(0).InfoText;

		for(DripDocument dd: dripDocs) {
			if(dd.Campaign) 
				return dd.InfoText;
		}

		// To begin with assume first one is the latest
		DripDocument latestDripDoc = dripDocs.get(0);

		for (int i = 1; i < dripDocs.size(); i++) {
			DripDocument dripDoc = dripDocs.get(i);

			if(dripDoc.AvailableDate != null && latestDripDoc.AvailableDate != null && dripDoc.AvailableDate.after(latestDripDoc.AvailableDate))
				latestDripDoc = dripDoc;
			else if(dripDoc.AvailableDate != null && latestDripDoc.AvailableDate == null)
				latestDripDoc = dripDoc;
		}

		return latestDripDoc.InfoText;
	}

	public void populateDripInfo(Connection con, DripCollection collection, PropertyEntity mainProduct, String subType, String templateName, String placement) {

		NumberFormat currency = NumberFormat.getCurrencyInstance();
		currency.setMaximumFractionDigits(0);

		for (DripProduct dp : collection.allDrips) {
			String value = StringUtil.replace(StringUtil.replace(StringUtil.replace(currency.format(dp.Value), "$", "\\$"), ")", "\\)"), "(", "\\(");
			//if (value.indexOf(".") > 0) {
			//	value = value.substring(0, value.indexOf("."));
			//}

			String result = StringUtil.replace(StringUtil.replace(StringUtil.replace(currency.format(Math.floor(Math.floor(collection.NetAmount) + dp.Value)), "$", "\\$"), ")", "\\)"), "(", "\\(");

			String exclude = StringUtil.replace(StringUtil.replace(StringUtil.replace(currency.format(Math.floor(Math.floor(collection.NetAmount) - dp.Value)), "$", "\\$"), ")", "\\)"), "(", "\\(");

			//String result = "\\" + currency.format(Math.ceil(collection.NetAmount + dp.Value));
			//if (result.indexOf(".") > 0) {
			//	result = result.substring(0, result.indexOf("."));
			//}
			GenRow documents = new GenRow();
			try { 
				documents.setConnection(con);
				documents.setViewSpec("LinkedDocumentView");

				GenRow rootDrip = this.findRootPlan(con, StringUtils.isNotBlank(dp.PackageCostProductID)? dp.PackageCostProductID : dp.ProductID);
				logger.debug("have root drip {}", rootDrip.getString("ProductID"));
				documents.setParameter("ProductID", rootDrip.getString("ProductID"));
				documents.setParameter("DocumentSubType", subType);
				documents.setParameter("LinkedDocumentDocument.DocumentSubType", subType);
				documents.setParameter("DocumentType", "Image+Text");
				if (placement != null) {
					documents.setParameter("LinkedDocumentDocument.Placement", placement);
				}
				if (templateName != null) {
					documents.setParameter("LinkedDocumentDocument.TemplateName", templateName);
				}
				documents.setParameter("-select1","LinkedDocumentDocument.Selectable");
				documents.sortBy("SortOrder", 0);
				documents.sortBy("Name", 1);
				if(logger.isTraceEnabled()) {
					documents.doAction("search");
					logger.trace(documents.getStatement());
				} 
				documents.getResults();

				if(documents.getNext()) {
					//@deprecated
					List<String[]> textList = new ArrayList<String[]>();
					//@deprecated
					List<String[]> imageList = new ArrayList<String[]>();

					List<DripContent> contentList = new ArrayList<DripContent>();
					do { 
						String description = replaceTokens(documents.getString("Description"), value, result, exclude, mainProduct);
						if ("Image".equals(documents.getData("DocumentType")))
							imageList.add(new String[]{documents.getString("FilePath"), documents.getString("Placement"), documents.getParameter("Selectable"), description });
						else if ("Text".equals(documents.getData("DocumentType"))) {
							if ("15".equals(documents.getString("Placement")) || "16".equals(documents.getString("Placement"))) {
								Conditions conditions = new Conditions(new HashMap<String,String>());
								if (description.startsWith("$ ")) {
									description = conditions.calculate(description.substring(2).replaceAll("[^0-9\\.\\-\\+\\*\\/ ]", ""));
									try {
										description = "" + Math.ceil(Double.parseDouble(description.replaceAll("[^0-9\\.\\-]", "")));
									} catch (Exception e) {}
									description = "$" + description.replaceFirst("\\.0","");
								} else {
									description = conditions.calculate(description);
								}
							}
							textList.add(new String[]{description, documents.getString("Placement"), documents.getParameter("Selectable")});
						}
						contentList.add(new DripContent(documents, description));
					} while (documents.getNext());
					dp.DripInfo = new DripInfo(textList, imageList);
					dp.DripInfo.ContentList = contentList;
				}
			} finally { 
				documents.close();
			}
			if(logger.isDebugEnabled()) { 
				logger.debug(dp.toString());
			}
		}
	}

	private String getDimension(String productID, String type, String detail, String value) {
		if (productID == null || type == null || detail == null || value == null ||
				productID.length() == 0 || type.length() == 0 || detail.length() == 0 || value.length() == 0) return "";

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductDetails");
		row.setParameter("ProductType", type + "%");
		row.setParameter("DetailsType", detail);
		row.setParameter("-select0",value);
		row.doAction("selectfirst");

		return row.getString(value);
	}

	private String replaceTokens(String description, String value, String result, String exclude, PropertyEntity mainProduct) {
		if(logger.isDebugEnabled()) logger.debug("replaceTokens({}, {}, {}, {}, {}", new String[]{description, value, result, exclude, mainProduct.getString("ProductID")});
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMaximumFractionDigits(0);

		if (description.indexOf("$(") > -1) {
			logger.trace("Replacing Token fields");
			if (value != null) description = description.replaceAll("\\$\\(DRIP_VALUE\\)", value);
			if (result != null) description = description.replaceAll("\\$\\(DRIP_RESULT\\)", result);
			if (exclude != null) description = description.replaceAll("\\$\\(DRIP_EXCLUDE\\)", exclude);
			
			description = description.replaceAll("\\$\\(CURRENT_DATE\\)", RunwayUtil.getCurDateInServerTimeZone(RunwayUtil.ddmmyyyy));
			description = description.replaceAll("\\$\\(CURRENT_TIME\\)", RunwayUtil.getCurDateInServerTimeZone(new SimpleDateFormat("HH:mm:ss")));
			description = description.replaceAll("\\$\\(CURRENT_DATETIME\\)", RunwayUtil.getCurDateInServerTimeZone(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")));
			
			if (description.indexOf("$(LOAN_AMOUNT)") != -1 || description.indexOf("$(LOAN_AMOUNT_SHORT)") != -1 || description.indexOf("$(LOAN_INTEREST_RATE)") != -1 || description.indexOf("$(LOAN_INTEREST_RATE_FIXED)") != -1 ||
					description.indexOf("$(LOAN_INTEREST_RATE_COMPARISON)") != -1 || description.indexOf("$(LOAN_PER_WEEK)") != -1 || description.indexOf("$(LOAN_PER_MONTH)") != -1 ||
					description.indexOf("$(LOAN_PER_WEEK_SHORT)") != -1 || description.indexOf("$(LOAN_PER_MONTH_SHORT)") != -1 ||
					description.indexOf("$(LOAN_YEARS)") != -1 || description.indexOf("$(LOAN_YEARS_FIXED)") != -1 || description.indexOf("$(LOAN_INTEREST_DATE)") != -1 ||
					description.indexOf("$(LOAN_PER_WEEK_FIXED_SHORT)") != -1 || description.indexOf("$(LOAN_PER_MONTH_FIXED_SHORT)") != -1 ||
					description.indexOf("$(LOAN_PER_WEEK_FIXED)") != -1 || description.indexOf("$(LOAN_PER_MONTH_FIXED)") != -1) {

				LoanCalculations lc = new LoanCalculations(mainProduct);

				description = description.replaceAll("\\$\\(LOAN_AMOUNT\\)", lc.getLoanAmount());
				description = description.replaceAll("\\$\\(LOAN_AMOUNT_SHORT\\)", lc.getLoanAmountShort());

				description = description.replaceAll("\\$\\(LOAN_INTEREST_RATE\\)", lc.getIntrestRate());
				description = description.replaceAll("\\$\\(LOAN_INTEREST_RATE_FIXED\\)", lc.getInterestrateFixed());
				description = description.replaceAll("\\$\\(LOAN_INTEREST_RATE_COMPARISON\\)", lc.getInterestRateComparison());

				description = description.replaceAll("\\$\\(LOAN_PER_WEEK\\)", lc.getPerWeekAmount());
				description = description.replaceAll("\\$\\(LOAN_PER_WEEK_SHORT\\)", lc.getPerWeekAmountShort());
				description = description.replaceAll("\\$\\(LOAN_PER_MONTH\\)", lc.getPerMonthAmount());
				description = description.replaceAll("\\$\\(LOAN_PER_MONTH_SHORT\\)", lc.getPerMonthAmountShort());

				description = description.replaceAll("\\$\\(LOAN_PER_WEEK_FIXED\\)", lc.getPerWeekAmountFixed());
				description = description.replaceAll("\\$\\(LOAN_PER_WEEK_FIXED_SHORT\\)", lc.getPerWeekAmountFixedShort());
				description = description.replaceAll("\\$\\(LOAN_PER_MONTH_FIXED\\)", lc.getPerMonthAmountFixed());
				description = description.replaceAll("\\$\\(LOAN_PER_MONTH_FIXED_SHORT\\)", lc.getPerMonthAmountFixedShort());

				description = description.replaceAll("\\$\\(LOAN_YEARS\\)", lc.getLoanYears());
				description = description.replaceAll("\\$\\(LOAN_YEARS_FIXED\\)", lc.getLoanYearsFixed());
				description = description.replaceAll("\\$\\(LOAN_INTEREST_DATE\\)", lc.getLoanDate());
			}

			if (mainProduct.isPropertyType(PropertyType.HouseLandPackage)) {
				logger.trace("Replacing Package fields");
				HouseLandPackage product = (HouseLandPackage) mainProduct;
				description = description.replaceAll("\\$\\(RANGE_NAME\\)", product.getRangeName());
				description = description.replaceAll("\\$\\(HOME_NAME\\)", product.getHomeName());
				description = description.replaceAll("\\$\\(PLAN_NAME\\)", product.getPlanName());
				//description = description.replaceAll("\\$\\(PLAN_PRICE\\)", nf.format(product.getPlanCost()));
				description = StringUtil.replace(description, "$(PLAN_PRICE)", nf.format(product.getPlanTotalCost()));
				description = description.replaceAll("\\$\\(ESTATE_NAME\\)", product.getEstateName());
				description = description.replaceAll("\\$\\(STAGE_NAME\\)", product.getStageName());
				description = description.replaceAll("\\$\\(LOT_NAME\\)", product.getLotName());
				//description = description.replaceAll("\\$\\(LOT_PRICE\\)", nf.format(product.getLotTotalCost()));

				description = StringUtil.replace(description, "$(LOT_PRICE)", nf.format(product.getLotTotalCost()));

				description = StringUtil.replace(description, "$(LOT_TITLE_DATE)", (product.getLand().getString("TitleMonth") + " " + product.getLand().getString("TitleYear")).trim());

				description = StringUtil.replace(description, "$(PACKAGE_TITLE_DATE)", (product.getString("TitleMonth") + " " + product.getString("TitleYear")).trim());

				description = description.replaceAll("\\$\\(FACADE_NAME\\)", product.getFacadeName());
				//description = description.replaceAll("\\$\\(FACADE_DESCRIPTION\\)", product.getFacadeDescription().replaceAll("\\$", "\\\\$"));
				description = StringUtil.replace(description, "$(FACADE_DESCRIPTION)", product.getFacadeDescription());
			} else if (mainProduct.isPropertyType(PropertyType.HomePlan)) {
				logger.trace("Replacing HomePlan fields");
				HomePlan product = (HomePlan) mainProduct;
				description = description.replaceAll("\\$\\(RANGE_NAME\\)", product.getRangeName());
				description = description.replaceAll("\\$\\(HOME_NAME\\)", product.getHomeName());
				description = description.replaceAll("\\$\\(PLAN_NAME\\)", product.getPlanName());
				//description = description.replaceAll("\\$\\(PLAN_PRICE\\)", nf.format(product.getDouble("TotalCost")));
				description = StringUtil.replace(description, "$(PLAN_PRICE)", nf.format(product.getDouble("TotalCost")));
				description = description.replaceAll("\\$\\(FACADE_NAME\\)", product.getFacadeName());
				//description = description.replaceAll("\\$\\(FACADE_DESCRIPTION\\)", product.getFacadeDescription().replaceAll("\\$", "\\\\$"));
				description = StringUtil.replace(description, "$(FACADE_DESCRIPTION)", product.getFacadeDescription());
			} else if(logger.isTraceEnabled()) {
				logger.trace("No specific fields replaced, product was a {}", mainProduct.getPropertyType().name());
			}
		}

		int start = 0;
		while ((start = description.indexOf("$(", start)) > -1) {
			int end = description.indexOf(")", start);
			if (end == -1) break;
			if (description.indexOf("$(", start + 1) < end) break;
			if (end > start + 2) {
				String text = description.substring(start + 2, end);
				//System.out.println(text);
				int dot1 = text.indexOf(".");
				if (dot1 != -1 && dot1 + 1 < end && text.charAt(dot1 + 1) != ' ') {
					int dot2 = text.indexOf(".",dot1 + 1);
					if (dot2 != -1 && dot2 + 1 < end && text.charAt(dot2 + 1) != ' ') {
						String type = text.substring(0,dot1);
						String details = text.substring(dot1 + 1,dot2);
						String val = text.substring(dot2 + 1);

						Object r = getDimension(mainProduct.getString("ProductID"), type, details, val);
						if (r == null) r = getDimension(mainProduct.getString("ProductID"), "", details, val);
						if (r == null) r = "";
						description = description.replaceAll("\\$\\(" + text + "\\)", r.toString());
					}
				}
			} else {
				break;
			}
			start = end;
		}
		if(logger.isTraceEnabled()) logger.trace(description);
		return description;
	}

	public static enum DripContentType { Text, Image };
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class DripContent {
		public DripContentType ContentType;
		public boolean Selectable;
		public String Placement;
		public String ImagePath;
		public String Description;
		public String ContentSubType;
		public String Template;
		public String URL;

		public DripContent() {}
		public DripContent(GenRow document, String description /* needs to be replaced */) {
			this.ContentType = DripContentType.valueOf(document.getData("DocumentType"));
			this.ContentSubType = document.getData("DocumentSubType");
			this.Selectable = "Y".equals(document.getData("Selectable"));
			this.Placement = document.getData("Placement");
			this.Template = document.getData("TemplateName");
			this.Description = this.ContentType == DripContentType.Text ? description : null;
			this.ImagePath =  this.ContentType == DripContentType.Image ? document.getData("FilePath") : null;
			this.URL =  this.ContentType == DripContentType.Image ? InitServlet.getSystemParam("URLHome") + "/" + document.getData("FilePath") : null;
			
		}
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class DripInfo {

		public List<DripContent> ContentList = Collections.emptyList();

		public List<String[]> TextInfo = Collections.emptyList();
		public List<String[]> ImageInfo = Collections.emptyList();

		public DripInfo(List<String[]> text, List<String[]> image) {
			this.TextInfo = text; 
			this.ImageInfo = image;
		}

		@Override
		public String toString() {
			StringBuilder s  = new StringBuilder();
			s.append("DripInfo(");
			appendList(s, TextInfo);
			s.append(",");
			appendList(s, ImageInfo);
			s.append(")");
			return s.toString();
		}


		private void appendList(StringBuilder s, List<String[]> list) {
			s.append("List(");
			for(String[] str: list) {
				appendArray(s, str);
				s.append(",");
			}
			s.append(");");
		}

		private void appendArray(StringBuilder s, String[] arr) {
			s.append("[");
			for(String ts: arr) {
				s.append(ts).append(",");
			}
			s.append("]");

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class DripLibrary {
		public String ProductLibraryID;
		public String Name;
		public String Description;
		public String Constraints;
		public String ProductIDs;


		private List<DripProduct> products;
		public DripLibraryType Type = DripLibraryType.MULTIPLE;

		public DripLibrary() { } 
		public DripLibrary(GenRow g) {
			ProductLibraryID = g.getString("ProductLibraryID");
			Name = g.getString("Name");
			Description = g.getString("Description");
			Constraints = g.getString("Constraints");
			ProductIDs = g.getString("ProductIDs");
		}

		@JsonProperty("Products")
		public List<DripProduct> getProducts() {
			if(StringUtils.isNotBlank(ProductIDs)) {
				Collections.sort(products, new Comparator<DripProduct>() {
					public int compare(DripProduct dp, DripProduct dp2) {
						int i = ProductIDs.indexOf(dp.CopiedFromID), i2 = ProductIDs.indexOf(dp2.CopiedFromID);
						if(i == i2) return 0;
						if(i == -1) return -1;
						if(i2 == -1) return 1;
						return i - i2;
					}
				});
			}
			return products;
		}
	}
	/* not sure what these are in reality, feel free to change. */
	public static enum DripLibraryType { SINGLE, MULTIPLE }

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class DripProduct {
		public String ProductID;
		public String ProductLibraryID;
		public String Name;
		public String Description, Summary;
		public double Cost;
		public double GST;
		public double TotalCost;
		public String ImagePath;
		public String Type;
		public String Category;
		public String Active;
		public String AvailableDate;
		public String ExpiryDate;
		public String PackageCostCalcField;
		public String Margin;
		public String PackageCostProductDetails;
		public String PackageCostProductID;
		public String CopiedFromID;
		public double Value;
		public String PackageCostCalcString;
		public String Constraint = "N";
		public int	  NumberOf;
		public String ParentProductID;
		public String ParentProductLibraryProductID;
		public String ProductLibraryProductID;
		public String PricingType;
		public List<DripContent> DripContent;
		public boolean isOrderDrip = false;

		// For UI Purpose
		public boolean Editable;
		public boolean ToggleActive;
		public String Origin;
		public DripInfo DripInfo;

		public DripProduct() {} 

		public void setCost(String cost) {
			try {
				Cost = Double.parseDouble(cost);
			} catch (Exception e) {}
		}

		public void addCost(String cost) {
			try {
				Cost += Double.parseDouble(cost);
			} catch (Exception e) {}
		}

		public DripProduct(GenRow g) {
			ProductID = g.getData("ProductID");
			Name = g.getString("Name");
			Description = g.getString("Description");
			ProductLibraryID = g.getString("ParentItemID");
			Cost = g.getDouble("Cost");
			GST = g.getDouble("GST");
			TotalCost = g.getDouble("TotalCost");
			ImagePath = g.getString("ImagePath");
			Type = g.getString("ProductSubType");
			Category = g.getString("Category");
			Active = g.getString("Active");
			// if(StringUtils.isNotBlank(g.getString("AvailableDate")))
			AvailableDate = g.getString("AvailableDate");
			ExpiryDate = g.getString("ExpiryDate");
			PackageCostCalcField = g.getString("PackageCostCalcField");
			Margin = g.getString("Margin");
			PackageCostProductDetails = g.getString("PackageCostProductDetails");
			PackageCostProductID = g.getString("PackageCostProductID");
			CopiedFromID = g.getString("PackageCostProductID");
			PackageCostCalcString = g.getString("PackageCostCalcString");
			Summary = g.getString("Summary");
			if(g.isSet("PricingType"))
				PricingType = g.getString("PricingType");
			else
				PricingType = "Included".equals(Type) ? "Inc Base" : "Inc Net";
			
			if (Cost == 0 && TotalCost != 0) TotalCost = 0;

			loadContraints(g.getConnection());
		}

		public void loadContraints(Connection conn) {
			String productID = ProductID;
			if (productID != null && productID.length() > 0) {
				GenRow product = findRootCostings(conn, productID);
				productID = product.getString("ProductID");
			}
			if (productID == null || productID.length() == 0) productID = ProductID;
			/*
			if (CopiedFromID != null && CopiedFromID.length() > 0 &&
					ProductLibraryID != null && ProductLibraryID.length() > 0) {

				logger.debug("loading constraints from copied id {} and library {}", CopiedFromID, ProductLibraryID);

				GenRow row = new GenRow();
				row.setConnection(conn);
				row.setTableSpec("ProductLibraryProducts");
				row.setParameter("-select", "*");
				row.setParameter("ProductID", CopiedFromID);
				row.setParameter("ProductLibraryID", ProductLibraryID);
				row.doAction("selectfirst");

				if (row.isSuccessful() && row.getString("ProductLibraryProductID").length() > 0) {
					Constraint = row.getString("Constraints");
					NumberOf = row.getInt("NumberOf");

					ParentProductID = row.getString("ParentProductID");
					ParentProductLibraryProductID = row.getString("ParentProductLibraryProductID");
					ProductLibraryProductID = row.getString("ProductLibraryProductID");
				}
			} else */
			if (productID != null && productID.length() > 0 &&
					ProductLibraryID != null && ProductLibraryID.length() > 0) {
				if(logger.isTraceEnabled()) logger.trace("loading constraints from product id {} and library {}", ProductID, ProductLibraryID);
				GenRow row = new GenRow();
				row.setConnection(conn);
				row.setTableSpec("ProductLibraryProducts");
				row.setParameter("-select", "*");
				row.setParameter("ProductID", productID);
				row.setParameter("ProductLibraryID", ProductLibraryID);
				row.doAction("selectfirst");

				if (row.isSuccessful() && row.getString("ProductLibraryProductID").length() > 0) {
					Constraint = row.getString("Constraints");
					NumberOf = row.getInt("NumberOf");

					ParentProductID = row.getString("ParentProductID");
					ParentProductLibraryProductID = row.getString("ParentProductLibraryProductID");
					ProductLibraryProductID = row.getString("ProductLibraryProductID");
				}
			}
		}
		public String toString() {
			StringBuilder sb = new StringBuilder("DRIP: ").append(Name).append(" ").append(PackageCostCalcString).append(" ").append(Active);
			sb.append(" $").append(TotalCost);
			return sb.toString();
		}
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class DripCollection {
		@JsonProperty("IncludedProductList")
		public List<HashMap<String, DripLibrary>> includedProductList = new ArrayList<HashMap<String, DripLibrary>>();
		@JsonProperty("OptionalProductList")
		public List<HashMap<String, DripLibrary>> optionalProductList = new ArrayList<HashMap<String, DripLibrary>>();

		@JsonProperty("OrderProductList")
		public List<HashMap<String, DripLibrary>> orderProductList = new ArrayList<HashMap<String, DripLibrary>>();

		@JsonProperty("AllDrips")
		public List<DripProduct> allDrips = new ArrayList<DripService.DripProduct>();

		@JsonProperty("IncludedDripAmount")
		public double includedDripAmount;

		@JsonProperty("IncludedPostDripAmount")
		public double includedPostDripAmount;

		@JsonProperty("OptionalDripAmount")
		public double optionalDripAmount;

		@JsonProperty("OptionalPostDripAmount")
		public double optionalPostDripAmount;

		@JsonProperty("IncludedDripCount")
		public int includedDripCount;

		@JsonProperty("OptionalDripCount")
		public int optionalDripCount;

		@JsonProperty("TotalAmount")
		public double TotalAmount; //  Total Cost of the product

		@JsonProperty("NetAmount")
		public double NetAmount; // = Total cost  + includedDripAmount
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class DripDocument {
		public String InfoText;
		public Date AvailableDate;
		public Date ExpiryDate;
		boolean Campaign = false;

		public DripDocument(String infoText, Date availableDate, Date expiryDate) {
			AvailableDate = availableDate; 
			ExpiryDate = expiryDate;
			InfoText = infoText;
		}
		public DripDocument(String infoText, Date availableDate, Date expiryDate, boolean campaign) {
			this(infoText, availableDate, expiryDate);
			this.Campaign = campaign;
		}
	}

	private final SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private final SimpleDateFormat sformat = new SimpleDateFormat("dd/MM/yyyy");

	public static final int BEFORE = -1;
	public static final int EQUAL = 0;
	public static final int AFTER = 1;

	public static class HNLDripComparator implements Comparator<DripProduct> {
		public int compare(DripProduct d1, DripProduct d2) {
			if (d1 == d2)
				return EQUAL;

			// based on origin
			if (StringUtils.isBlank(d1.Origin) || StringUtils.isBlank(d2.Origin)) {
				return EQUAL;
			}

			if (d1.Origin.equals(d2.Origin)) {
				return EQUAL;
			}

			if ("House".equals(d1.Origin))
				return BEFORE;

			if (("Land".equals(d1.Origin)) && "House and Land".equals(d2.Origin))
				return BEFORE;

			return AFTER;
		}
	}

	public class LoanCalculations {

		private double loanYears = 30;
		private double loanFixYears = 1;
		private double loanRate = 0;
		private double loanRateFixed = 0;
		private double loanRateComparison = 0;
		private double loanAmount = 95;
		private double loanAmountValue = 0;
		private double loanValue = 0;
		
		private double weeks = 365.25 / 7;

		private double monthlyPaymentFixed = 0;
		private double monthlyPayment = 0;
		
		private double loanDischarge = 0;
		private double loanFee = 0;

		private String loanDate = "";

		private double[] payments;
		
		private final NumberFormat currencyShort = NumberFormat.getCurrencyInstance();

		public LoanCalculations(PropertyEntity pe) {
			loanYears = getDoubleValue(InitServlet.getSystemParam("Loan-NumberOfYears"));
			loanFixYears = getDoubleValue(InitServlet.getSystemParam("Loan-FixedTermYears"));
			loanRate = getDoubleValue(InitServlet.getSystemParam("Loan-FullRate"));
			loanRateFixed = getDoubleValue(InitServlet.getSystemParam("Loan-FixedTermRate"));

			loanAmount = getDoubleValue(InitServlet.getSystemParam("Loan-PercentageOfTotal"));

			loanFee = getDoubleValue(InitServlet.getSystemParam("Loan-Fee"));
			loanDischarge = getDoubleValue(InitServlet.getSystemParam("Loan-DischargeAmount"));
			loanDate = InitServlet.getSystemParam("Loan-Date");

			loanValue = pe.getDouble("DripResult");
			if (loanValue == 0) loanValue = pe.getDouble("TotalCost");
			loanValue = StringUtil.getRoundedValue(loanValue);
			
			currencyShort.setMaximumFractionDigits(0);

			if (loanYears > 0) { 
				payments = new double[(int) (loanYears * 12) + 1];
				

				if (loanAmount > 0)
					loanAmountValue = loanValue * (loanAmount / 100);
				else
					loanAmountValue = loanValue;

				double fv = loanAmountValue;

				payments[0] = -fv + loanFee;
				
				int count = 0;
				
				double rate = 0;

				if (loanFixYears > 0 && loanRateFixed > 0 && loanYears > 0) {
					monthlyPaymentFixed = pmt(fv, loanRateFixed, loanYears);
					monthlyPaymentFixed = ((double) Math.round(monthlyPaymentFixed * 100)) / 100;

					for (int i = 1; i <= loanFixYears * 12; ++i) {
						payments[i] = monthlyPaymentFixed;
					}

					count = (int) loanFixYears * 12;

					fv = -fv(loanRateFixed, loanFixYears, -monthlyPaymentFixed,  fv, 0);
					
					rate = loanRateFixed;
				}

				if (loanYears > 0 && loanRate > 0) {
					monthlyPayment = pmt(fv, loanRate, loanYears - loanFixYears);
					monthlyPayment = ((double) Math.round(monthlyPayment * 100)) / 100;
					
					for (int i = count + 1; i <= loanYears * 12; ++i) {
						payments[i] = monthlyPayment;
					}
					
					count = (int) (loanYears * 12) + 1;
					
					payments[(int) (loanYears * 12)] = monthlyPayment + loanDischarge;
					
					if (rate == 0) rate = loanRate;
				}
				
				loanRateComparison = irr(payments, (rate / 12) / 100) * 12 * 100;
				loanRateComparison = ((double) Math.round(loanRateComparison * 100)) / 100;
			}
		}

		public String getLoanDate() {
			// TODO Auto-generated method stub
			return loanDate;
		}

		public String getLoanYearsFixed() {
			// TODO Auto-generated method stub
			return ("" + loanFixYears).replaceFirst("\\.0", "");
		}

		public String getPerMonthAmount() {
			// TODO Auto-generated method stub
			return "\\" + currency.format(monthlyPayment);
		}

		public String getPerMonthAmountShort() {
			// TODO Auto-generated method stub
			return "\\" + currencyShort.format(monthlyPayment);
		}

		public String getPerWeekAmount() {
			// TODO Auto-generated method stub
			return "\\" + currency.format((monthlyPayment * 12) / weeks);
		}

		public String getPerWeekAmountShort() {
			// TODO Auto-generated method stub
			return "\\" + currencyShort.format((monthlyPayment * 12) / weeks);
		}

		public String getLoanYears() {
			// TODO Auto-generated method stub
			return ("" + loanYears).replaceFirst("\\.0", "");
		}

		public String getPerMonthAmountFixed() {
			// TODO Auto-generated method stub
			return "\\" + currency.format(monthlyPaymentFixed);
		}

		public String getPerMonthAmountFixedShort() {
			// TODO Auto-generated method stub
			return "\\" + currencyShort.format(monthlyPaymentFixed);
		}

		public String getPerWeekAmountFixed() {
			// TODO Auto-generated method stub
			return "\\" + currency.format((monthlyPaymentFixed * 12) / weeks);
		}

		public String getPerWeekAmountFixedShort() {
			// TODO Auto-generated method stub
			return "\\" + currencyShort.format((monthlyPaymentFixed * 12) / weeks);
		}

		public String getInterestRateComparison() {
			// TODO Auto-generated method stub
			return "" + loanRateComparison + "%";
		}

		public String getInterestrateFixed() {
			// TODO Auto-generated method stub
			return "" + loanRateFixed + "%";
		}

		public String getIntrestRate() {
			// TODO Auto-generated method stub
			return "" + loanRate + "%";
		}

		public String getLoanAmount() {
			// TODO Auto-generated method stub
			return "\\" + currency.format(loanAmountValue);
		}
		
		public String getLoanAmountShort() {
			// TODO Auto-generated method stub
			return "\\" + currencyShort.format(loanAmountValue);
		}
		
		/**
		 * Emulates Excel/Calc's FV(interest_rate, number_payments, payment, PV,
		 * Type) function, which calculates future value or principal at period N.
		 * 
		 * @param r
		 *            - periodic interest rate represented as a decimal.
		 * @param nper
		 *            - number of total payments / periods.
		 * @param pmt
		 *            - periodic payment amount.
		 * @param pv
		 *            - present value -- borrowed or invested principal.
		 * @param type
		 *            - when payment is made: beginning of period is 1; end, 0.
		 * @return <code>double</code> representing future principal value.
		 */
		//http://en.wikipedia.org/wiki/Future_value
		public double fv(double r, double nper, double pmt, double pv, int type) {
			double ir = (r / 12) / 100;
			double np = nper * 12;

			return -(pv * pow(1 + ir, np) + pmt * (1 + ir * type) * (pow(1 + ir, np) - 1) / ir);
		}

		/**
		 * Overloaded fv() call omitting type, which defaults to 0.
		 * 
		 * @see #fv(double, int, double, double, int)
		 */
		public double fv(double r, double nper, double c, double pv) {
			return fv(r, nper, c, pv, 0);
		}

		public double pmt(double value, double rate, double term) {
			double ir = (rate / 12) / 100;
			double np = term * 12;

			return (ir / (1 - Math.pow(1 + ir, -np))) * value;
		}

		public double pow(double a, double b) {
			double c = a;
			for (int x = 1; x < b; ++x) {
				c = c * a;
			}

			return c;
		}

		private static final double MINDIF=.0000001;
		/** Returns an Internal Rate of Return for a series of cash-flows. */
		public double irr(double[] list, double guess) {
			double irr=0;
			/*
			 * sanity check
			 */
			if (list[0]==0) return Double.NaN;
			int ineg=0;
			int ipos=0;
			int i;
			for (i=0;i<list.length;i++) {
				irr+=list[ i ];
				if (list[ i ]>0) ipos++;
				else if (list[ i ]<0) ineg++;
			}
			if (ineg==0 || ipos==0) return Double.NaN;

			double a1=list[0];
			if (guess<=0) guess=.5;
			if (irr<0) irr=-guess; else irr=guess;
			boolean was_hi=false;
			double a3;
			for (int iter=0;iter<=50;iter++) {
				a3=a1;
				int j=1;
				for (i=1;i<list.length;i++) {
					a3+=list[ i ]/Math.pow(1.0+irr,j);
					j++;
				}
				if (Math.abs(a3)<.01) return irr;
				if (a3>0) {
					if (was_hi) guess/=2;
					irr+=guess;
					if (was_hi) {
						guess-=MINDIF;
						was_hi=false;
					}
				} else {
					guess/=2;
					irr-=guess;
					was_hi=true;
				}
				if (guess<=MINDIF) return irr;
			}
			return Double.NaN;
		}

		private int getIntValue(String s) {
			if (StringUtils.isNotBlank(s)) {
				try {
					return Integer.parseInt(s);
				} catch (Exception e) {

				}
			}

			return 0;
		}

		private double getDoubleValue(String s) {
			if (StringUtils.isNotBlank(s)) {
				try {
					return Double.parseDouble(s);
				} catch (Exception e) {

				}
			}

			return 0.0;
		}
	}
	public static class EstateComparator implements Comparator {
		public int compare(Object p1, Object p2) {
			String p1Name = ((Package) p1).estateName;
			String p2Name = ((Package) p2).estateName;
			return p1Name.compareTo(p2Name);
		}
	}
	public static ArrayList<String> getDripArray(String drip, String token, int maxItemList) {
		ArrayList<String> al = new ArrayList<String>();
		if(StringUtils.isNotBlank(drip)) {
			StringTokenizer str = new StringTokenizer(drip, token);
			int count = 0;
			while (str.hasMoreElements() && count < maxItemList) {
				String inclusionStr = str.nextElement().toString();
				al.add(inclusionStr);
				count++;
			}
		}
		return al;
	}
	
}
