package com.sok.service.crm;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.Company;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CompanyService extends AbstractService {

	private static final Logger logger = LoggerFactory.getLogger(CompanyService.class);
	public Logger getLogger() {
		return logger;		
	}
	private static final ActivityLogger activityLogger = ActivityLogger.getActivityLogger();
	private static final UserService us = UserService.getInstance(); 
	
	private static final CompanyService service = new CompanyService();
	public static CompanyService getInstance() {
		return service;
	}
	
	public List<Company> getQuicknoteCompanyMatches(HttpServletRequest request) {
		final UserBean user = us.getCurrentUser(request);
		
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Companies") && request.getParameter("Type") == null) {
			throw new AccessException("You do not have permission to access the companies module");
		}
		
	    GenRow companies = null;
	    GenRow companyGroupView = null;
	    try {
	        String department = getDecodedParameter("-Department", request,"Department");
	        String company = getDecodedParameter("-Company", request, "Company");
	        String email = getDecodedParameter("-Email", request, "Email");
	        String companyID = getDecodedParameter("-companyid", request, "");
	    	
	     // We count the number of non-empty optional parmaters because if
	        // all of these parameters are empty, we do not want to go off
	        // and do an expensive search for all companies.
	        int numberNonEmptyOptParams = 0;

	        // Search for contact matches
	        companies = new GenRow();
	        companies.setTableSpec("Companies");
	        companies.parseRequest(request);

	        companies.setParameter("-select0", "CompanyID");
	        companies.setParameter("-select1", "Department");
	        //contacts.setParameter("-select2", "LastName");
	        companies.setParameter("-select2", "DISTINCT Email");
	        
	        if (company.length() > 0) {
	            companies.addParameter("Company^1", "%" + company + "%");
	            numberNonEmptyOptParams++;
	            
	        }

	        if (department.length() > 0) {
	            companies.setParameter("Department^1", "%" + department + "%");
	            numberNonEmptyOptParams += 1;

	        }
	        
	        if (email.length() > 0) {
	            companies.setParameter("Email^1", "%" + email + "%");
	            numberNonEmptyOptParams += 1;

	        }

	        if (numberNonEmptyOptParams == 0 && companyID.length() > 0) {
	            companies.setParameter("CompanyID^1", companyID);
	            numberNonEmptyOptParams += 1;
	        } else if (companyID.length() > 0) {
	        	numberNonEmptyOptParams += 1;
	        }
	        
	        if(companies.isSet("Type")) {
	        	numberNonEmptyOptParams += 1;
	        }

	        // Only do the search if we have at least one non-empty
	        // optional parameter.
	        if (numberNonEmptyOptParams > 0) {
	        	if(!companies.isSet("Type")) user.setConstraint(companies, "CompanyCompanyGroups.CompanyGroup");
	        	companies.sortBy("Company",0);
	        	companies.sortBy("department",1);
	            companies.setAction(GenerationKeys.SEARCH);
	            companies.getResults(true);
	            
	            if(logger.isDebugEnabled()) logger.debug("QuickMatch {} - {}", companies.getSize(), companies.getStatement());
	        }

	        if(companies == null || companies.getSize() == 0) {
            	return Collections.emptyList();
	        }
	        companyGroupView = newCompanyGroupView(request);
            List<Company> list = new ArrayList<Company>();
            while(companies.getNext()) {
                String contactID = companies.getData("CompanyID");
                loadCompanyGroupViewForCompany(companyGroupView, contactID);
                list.add(BeanFactory.getCompanyFromTableData(companyGroupView));  
            }
            return list;
	    } finally {
	    	logger.trace("closing company beans");
	    	if(companies != null) companies.close();
	    	if(companyGroupView != null) companyGroupView.close();
	    }
	}
	
    private GenRow newCompanyGroupView(HttpServletRequest request) {
        GenRow companyGroupView = new GenRow();
        companyGroupView.setRequest(request);
        companyGroupView.setViewSpec("CompanyView");
        return companyGroupView;
    }
    
    private void loadCompanyGroupViewForCompany(GenRow contactGroupView, String contactID) {
        contactGroupView.clear();
        contactGroupView.setParameter("CompanyID", contactID);
        contactGroupView.doAction(GenerationKeys.SELECT);
        /*
        contactGroupView.doAction(GenerationKeys.SEARCH);
        contactGroupView.getResults(true);
        */
    }
    
	
}
