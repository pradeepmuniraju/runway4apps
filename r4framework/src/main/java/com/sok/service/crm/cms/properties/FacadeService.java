package com.sok.service.crm.cms.properties;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.sql.Connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.Facade;
import com.sok.runway.crm.cms.properties.Home;
import com.sok.runway.crm.cms.properties.HomePlan;
import com.sok.runway.externalInterface.beans.cms.properties.Plan;
import com.sok.runway.externalInterface.beans.cms.properties.ProductDetail;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

public class FacadeService {

	private static final FacadeService service = new FacadeService();
	private static final Logger logger = LoggerFactory.getLogger(FacadeService.class);
	private static final UserService us = UserService.getInstance();
	public static FacadeService getInstance() {
		return service;
	}
	
	public List<Facade> getFacades(final HttpServletRequest request, String homeProductID) {
		logger.trace("getFacades(Request, {})", homeProductID);
		return getFacades(ActionBean.getConnection(request), us.getCurrentUser(request), homeProductID);
	}

	public List<Facade> getFacades(final Connection con, final UserBean user, String homeProductID) {
		logger.trace("getFacades(Conn,{}, {})", user != null ? user.getName() : "null user",  homeProductID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		Home h = "ALL".equals(homeProductID) ? null : new Home(con, homeProductID);
		if(h != null && !h.isSuccessful()) {
			throw new NotFoundException("The requested facade could not be found");
		}
		com.sok.runway.crm.cms.properties.Facade facadesRow = null;
		try { 
			if(h == null) {
				facadesRow = new com.sok.runway.crm.cms.properties.Facade(con);
				facadesRow.setParameter("-join1","ProductProductsProduct");
				facadesRow.setParameter("-select1","ProductProductsProduct.Name as ParentProductName");
				facadesRow.setParameter("Active", "Y");
				facadesRow.setParameter("ProductProductsLinkedProduct.Active", "Y");
				facadesRow.setParameter("CopiedFromProductID","NULL+EMPTY");
				facadesRow.setParameter("ProductProductsProduct.CopiedFromProductID","NULL+EMPTY");
				facadesRow.setParameter("-sort0","ProductProductsProduct.Name");
				facadesRow.setParameter("-sort1","ProductProductsLinkedProduct.Name");
				facadesRow.getResults();
			} else {
				facadesRow = h.getFacades();
			}
			
			if(facadesRow.getNext()) {
				List<Facade> list = new ArrayList<Facade>(10);
				do {
					//fails as is looking at productproducts..hrm.
					//if(user.canView(facadesRow)) { 
						list.add(new Facade(facadesRow));
					//} else {
					//	logger.debug("User {} could not access facade {}", user.getUserID(), facadesRow.getData("ProductID"));
					//}
				} while (facadesRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			if(facadesRow != null) facadesRow.close();
		}
	}
	
	public List<Facade> getPlanFacades(final HttpServletRequest request, String planProductID) {
		logger.trace("getFacades(Request, {})", planProductID);
		return getPlanFacades(ActionBean.getConnection(request), us.getCurrentUser(request), planProductID);
	}

	public List<Facade> getPlanFacades(final Connection con, final UserBean user, String planProductID) {
		logger.trace("getFacades(Conn,{}, {})", user != null ? user.getName() : "null user",  planProductID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		HomePlan h = "ALL".equals(planProductID) ? null : new HomePlan(con, planProductID);
		if(h != null && !h.isSuccessful()) {
			throw new NotFoundException("The requested facade could not be found");
		}
		com.sok.runway.crm.cms.properties.Facade facadesRow = null;
		try { 
			if(h == null) {
				facadesRow = new com.sok.runway.crm.cms.properties.Facade(con);
				facadesRow.setParameter("-join1","ProductProductsProduct");
				facadesRow.setParameter("-select1","ProductProductsProduct.Name as ParentProductName");
				facadesRow.setParameter("Active", "Y");
				facadesRow.setParameter("ProductProductsLinkedProduct.Active", "Y");
				facadesRow.setParameter("CopiedFromProductID","NULL+EMPTY");
				facadesRow.setParameter("ProductProductsProduct.CopiedFromProductID","NULL+EMPTY");
				facadesRow.setParameter("-sort0","ProductProductsProduct.Name");
				facadesRow.setParameter("-sort1","ProductProductsLinkedProduct.Name");
				facadesRow.getResults();
			} else {
				facadesRow = h.getFacades();
			}
			
			if(facadesRow.getNext()) {
				List<Facade> list = new ArrayList<Facade>(10);
				do {
					//fails as is looking at productproducts..hrm.
					//if(user.canView(facadesRow)) { 
						list.add(new Facade(facadesRow));
					//} else {
					//	logger.debug("User {} could not access facade {}", user.getUserID(), facadesRow.getData("ProductID"));
					//}
				} while (facadesRow.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			if(facadesRow != null) facadesRow.close();
		}
	}
	
	/* used in order pdfs */
	public Facade getFacade(final HttpServletRequest request, final String facadeProductID) {
		logger.trace("getFacade(Request, {})", facadeProductID);
		return getFacade(ActionBean.getConnection(request), us.getCurrentUser(request), facadeProductID);
	}
	public Facade getFacade(final Connection conn, final UserBean user, final String facadeProductID) {
		logger.trace("getFacade(Conn, {}, {})", user != null ? user.getName() : "null user", facadeProductID);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the properties module");
		}
		com.sok.runway.crm.cms.properties.Facade fr = new com.sok.runway.crm.cms.properties.Facade(conn, facadeProductID); 
		
		if(fr.isSuccessful()) {
			return new Facade(fr);
		}
		throw new NotFoundException("No facade was found for id " + facadeProductID);
	}
	
	public List<ProductDetail> getProductDetails(HttpServletRequest request, String productId) {
		return getProductDetails(ActionBean.getConnection(request), us.getCurrentUser(request), productId);
	}
	
	public List<ProductDetail> getProductDetails(final Connection con, final UserBean user,String productId) {
		return getProductDetails(con, user, productId, null);
	}
	public List<ProductDetail> getProductDetails(final Connection con, final UserBean user,String productID, String type) {
		logger.debug("getProductDetails(con, user, productID={}, type={}", productID, type);
		GenRow r = new GenRow();
		try { 
			r.setTableSpec("properties/ProductDetails");
			r.setConnection(con);
			r.setParameter("-select","*");
			r.setParameter("ProductID", productID);
			if(type != null) {
				r.setParameter("ProductType", type);
			}
			r.setParameter("-sort1","SortOrder");
			if(logger.isTraceEnabled()) {
				r.doAction(GenerationKeys.SEARCH);
				logger.trace(r.getStatement());
			}
			if(r.isSet("ProductID")) { 
				r.getResults(true);
			}
			if(r.getNext()) {
				List<ProductDetail> list = new ArrayList<ProductDetail>(r.getSize());
				do {
					list.add(new ProductDetail(r));
				} while(r.getNext());
				return list;
			}
			if(logger.isDebugEnabled()) { 
				logger.debug(r.getError());
				logger.debug(r.getErrors());
			}
			return Collections.emptyList();
		} finally { 
			r.close();
		}
	}
	
	/* temporary - move to external interface */
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Facade {
		public String ProductID;
		public String Name, ParentProductName;
		public String ImageName;
		public String ImagePath;
		public String ThumbnailImageName;
		public String ThumbnailImagePath;
		public double Cost;
		public double GST;
		public double TotalCost;
		public boolean DefaultFacade = false;
		
		public Facade() {}
		public Facade(com.sok.runway.crm.cms.properties.Facade facade) {
			this.ProductID = facade.getData("LinkedProductID");
			this.Name = facade.getData("Name");
			this.ParentProductName = facade.getData("ParentProductName");	//not always used for performance reasons, but reqd for search.
			this.ImageName = facade.getData("ImageName");
			this.ImagePath = facade.getData("ImagePath");
			this.ThumbnailImageName = facade.getData("ThumbnailImageName");
			this.ThumbnailImagePath = facade.getData("ThumbnailImagePath");
			this.Cost = facade.getDouble("Cost");
			this.GST = facade.getDouble("GST");
			this.TotalCost = facade.getDouble("TotalCost");
			this.DefaultFacade = "Y".equals(facade.getString("IsExclusive"));
		}
	}
}