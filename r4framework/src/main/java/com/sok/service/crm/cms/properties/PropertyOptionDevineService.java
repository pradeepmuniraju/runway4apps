package com.sok.service.crm.cms.properties;

import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.beans.PropertyOption;
import com.sok.runway.externalInterface.beans.PropertyOptionCategory;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class PropertyOptionDevineService {

	private static final PropertyOptionDevineService service = new PropertyOptionDevineService();
	private static final Logger logger = LoggerFactory.getLogger(PropertyOptionDevineService.class);
	private static final UserService us = UserService.getInstance();
	public static PropertyOptionDevineService getInstance() {
		return service;
	}
	
	public List<PropertyOptionCategory> getPropertyOptionCategories(Connection conn, UserBean user) {
		logger.debug("getPropertyOptionCategories(Connection, {})", user != null ? user.getName() : "null user");
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		final GenRow worker = getPropertyOptionCategoryGenRow();
		try { 
			if(conn != null) {
				worker.setConnection(conn);
			} else {
				ActionBean.connect(worker);
			}
			worker.setParameter("-sort1","ProductGroups.Description");
			if(logger.isTraceEnabled()) { 
				worker.doAction(GenerationKeys.SEARCH);
				logger.trace(worker.getStatement());
			}
			worker.getResults(true);
			if(worker.getNext()) {
				final List<PropertyOptionCategory> list = new ArrayList<PropertyOptionCategory>(worker.size());
				do {
					list.add(new PropertyOptionCategory(worker));
				} while(worker.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			worker.close();
		}
	}

	public List<PropertyOption> getPropertyOptions(Connection connection, UserBean user, String productGroupId, String search) {
		return getPropertyOptions(connection, user, productGroupId, search, null);
	}
	
	public List<PropertyOption> getPropertyOptions(Connection connection, UserBean user, String productGroupId, String search, String orderId) {		
		logger.debug("getPropertyOptions(Connection, {}, {})", user != null ? user.getName() : "null user", productGroupId);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		if(StringUtils.isBlank(productGroupId)) {
			throw new IllegalArgumentException("No product group supplied");
		}
		
		GenRow worker = getPropertyOptionGenRow();
		
		HashMap<String,String> map = new HashMap<String,String>();
		
		if (StringUtils.isNotBlank(orderId)) {
			String productID = getOrderProductID(orderId);
			if (StringUtils.isNotBlank(productID)) {
				GenRow row = new GenRow();
				row.setTableSpec("ProductAnswers");
				row.setParameter("-select0", "QuestionID");
				row.setParameter("-select1", "Answer");
				row.setConnection(connection);
				row.setParameter("ProductID", productID);
				row.setParameter("QuestionShortLabel", "Criteria%");
				row.setParameter("ProductAnswerQuestion.ShortLabel", "Criteria%");
				row.setParameter("ProductID", productID);
				row.doAction("search");
				
				row.getResults();
				
				while (row.getNext()) {
					if (row.isSet("Answer")) {
						map.put(row.getData("QuestionID"), row.getString("Answer"));
					}
				}
				
				row.close();
				
				GenRow product = getOrderProduct(productID);
				if (product.isSet("EstateName")) map.put(findProductQuestion("CriteriaEstate"), product.getString("EstateName"));;
				if (product.isSet("ProductNum")) map.put(findProductQuestion("CriteriaProduct"), product.getString("ProductNum"));;
			}
		}
		
		try {
			worker.setConnection(connection);
			boolean proceed = false;
			if(!"ALL".equals(productGroupId)) {
				worker.setParameter("ProductGroupID", productGroupId);
				proceed = true;
			}
			if(StringUtils.isNotBlank(search)) {
				if(search.trim().indexOf(" ") != -1) { 
					String[] st = search.trim().split(" ");
					int m = st.length - 1;
					if(m != -1) {
						for(int i=0; ; i++) {
					        worker.setParameter("Name^11|1", "%" + st[i] + "%");
							worker.setParameter("Description^22|1", "%" + st[i] + "%");
							if(i == m) break;
						}
					}
				} else { 
					worker.setParameter("Name|1", "%" + search + "%");
					worker.setParameter("Description|1", "%" + search + "%");
				}
				proceed = true;
			}
			if(logger.isTraceEnabled()) {
				worker.doAction(GenerationKeys.SEARCH);
				logger.trace(worker.getStatement());
			}
			
			if(proceed) 
				worker.getResults(true);
			System.out.println(worker.getStatement());
			if(worker.getNext()) {
				final List<PropertyOption> list = new ArrayList<PropertyOption>(worker.size());
				do {
					Iterator<String> i = map.keySet().iterator();
					boolean inc = true;
					while (i.hasNext()) {
						String questionID = i.next();
						System.out.print(worker.getString("ProductNum") + "   ");
						inc = checkAnswer(connection, worker.getString("ProductID"), questionID, map.get(questionID));
						if (!inc) break;
					}
					if (inc) {
						worker.setParameter("Quantity", getProductAnswer(connection, worker.getString("ProductID"), findProductQuestion("VariationQty")));
						worker.setParameter("Minimum", getProductAnswer(connection, worker.getString("ProductID"), findProductQuestion("VariationMinQty")));
						worker.setParameter("Maximum", getProductAnswer(connection, worker.getString("ProductID"), findProductQuestion("VariationMaxQty")));
						list.add(new PropertyOption(worker));
					}
				} while(worker.getNext());
				return list;
			}
		} finally {
			worker.close();
		}
		return Collections.emptyList();
	}
	
	private String findProductQuestion(String question) {
		GenRow row = new GenRow();
		row.setTableSpec("Questions");
		row.setParameter("-select0", "QuestionID");
		row.setParameter("ShortLabel", question);
		row.setParameter("ProfileType", "ProductProfile");
		row.doAction("selectfirst");

		return row.getString("QuestionID");
	}
	
	private boolean checkAnswer(Connection connection, String productID, String questionID, String answer) {
		
		String answerText = getProductAnswer(connection, productID, questionID);
		
		System.out.println(questionID + " - " + answer + " = " + answer);
		
		if (answerText.length() == 0 || answerText.equalsIgnoreCase(answer)) return true;
		
		return false;
	}
	
	private String getProductAnswer(Connection connection, String productID, String questionID) {
		if (productID == null | productID.length() == 0 || questionID == null || questionID.length() == 0) return "";
		GenRow row = new GenRow();
		row.setTableSpec("ProductAnswers");
		row.setConnection(connection);
		row.setParameter("-select0", "Answer");
		row.setParameter("ProductID", productID);
		row.setParameter("QuestionID", questionID);
		row.doAction("selectfirst");
		
		return row.getString("Answer");
		
	}
	
	
	private String getOrderProductNum(String orderId) {
		if (StringUtil.isBlankOrEmpty(orderId)) return "";
		GenRow row = new GenRow();
		row.setViewSpec("OrderView");
		row.setParameter("OrderID", orderId);
		row.doAction("selectfirst");
		
		return row.getString("ProductNum");
	}

	private String getOrderProductID(String orderId) {
		if (StringUtil.isBlankOrEmpty(orderId)) return "";
		GenRow row = new GenRow();
		row.setViewSpec("OrderView");
		row.setParameter("OrderID", orderId);
		row.doAction("selectfirst");
		
		return row.getString("ProductID");
	}

	private GenRow getOrderProduct(String productId) {
		if (StringUtil.isBlankOrEmpty(productId)) return new GenRow();
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHouseAndLandView");
		row.setParameter("ProductID", productId);
		row.setParameter("ProductType", "House and Land");
		row.doAction("selectfirst");
		
		if (!row.isSuccessful() && !row.isSet("ProductNum")) {
			row.setViewSpec("properties/ProductHomePlanView");
			row.setParameter("ProductID", productId);
			row.setParameter("ProductType", "Home Plan");
			row.doAction("selectfirst");
		}
		
		return row;
	}

	public PropertyOptionCategory getPropertyOptionCategory(Connection conn, UserBean user, String category, String categoryId) {
		if(logger.isDebugEnabled()) logger.debug("getPropertyOptionCategory(Connection, {}, {},{})", new String[]{user != null ? user.getName() :"null user", category, categoryId});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		if(StringUtils.isBlank(category) || StringUtils.isBlank(categoryId)) {
			logger.debug("getPropertyOptionCategory({},{}) returning null", category, categoryId);
			throw new RuntimeException("Property cat or categoryId were null");
		}
		final GenRow worker = getPropertyOptionCategoryGenRow();
		try { 
			if(conn != null) {
				worker.setConnection(conn);
			} else {
				ActionBean.connect(worker);
			}
			worker.setParameter("Description", category);
			worker.doAction(GenerationKeys.SELECTFIRST);
			
			if(!worker.isSuccessful()) {
				worker.setToNewID("ProductGroupID");
				worker.setParameter("GroupName", category);
				worker.setParameter("GroupID", InitServlet.getSystemParam("DefaultGroupID"));
				
				worker.setParameter("ModifiedBy", user.getUserID());
				worker.setParameter("CreatedBy", user.getUserID());
				worker.setParameter("ModifiedDate", "NOW");
				worker.setParameter("CreatedDate", "NOW");

				worker.doAction(GenerationKeys.INSERT);
				if(!worker.isSuccessful()) {
					throw new RuntimeException("Failed to insert PropertyOptionCategory " + worker.toString());
				}
				//load up categoryid, name, num 
				worker.doAction(GenerationKeys.SELECT);
			}
			if(!worker.getString("GroupName").equals(category)) {
				logger.error("Inconsistent data for categoryID {} found {} expected {}", new String[]{categoryId, worker.getString("GroupName"), category});
				throw new RuntimeException("Inconsistent data for categoryId : " + categoryId);
			}
			return new PropertyOptionCategory(worker);
		} finally {
			worker.close();
		}
	}
	
	private GenRow getPropertyOptionCategoryGenRow() {
		final GenRow worker = new GenRow();
		worker.setTableSpec("ProductGroups");
		worker.setParameter("-select0","ProductGroups.ProductGroupID as CategoryID");
		worker.setParameter("-select1","ProductGroups.GroupName as CategoryName");
		worker.setParameter("-select2","ProductGroups.Description as CategoryNum");
		worker.setParameter("Active","Y");
		return worker;
	}
	
	private GenRow getPropertyOptionGenRow() {
		final GenRow displayOptions = new GenRow();
		displayOptions.setTableSpec("Products");
		displayOptions.setParameter("Active","Y");
		displayOptions.setParameter("-join1", "ProductImage");
		displayOptions.setParameter("-join2", "ProductProductGroup");
		displayOptions.setParameter("-join3","LinkedProductProductDisplayOptions");
		displayOptions.setParameter("ProductType", "Property Option"); // ??
		displayOptions.setParameter("ProductProductGroup.Active","Y");
		
		displayOptions.setParameter("-select1","Products.ProductID");
		displayOptions.setParameter("-select2","Products.ProductNum");
		displayOptions.setParameter("-select3","Products.Name");
		displayOptions.setParameter("-select4","ProductImage.FilePath as ImagePath");
		displayOptions.setParameter("-select5","Products.Cost as Cost");
		displayOptions.setParameter("-select6","Products.GST as GST");
		displayOptions.setParameter("-select7","Products.TotalCost as TotalCost");
		displayOptions.setParameter("-select8","ProductProductGroup.ProductGroupID as CategoryID");
		displayOptions.setParameter("-select9","ProductProductGroup.GroupName as CategoryName");
		displayOptions.setParameter("-select10","ProductProductGroup.Description as CategoryNum");
		displayOptions.setParameter("-select11","Products.Description as Description");
		
		//relation for quantity
		displayOptions.setParameter("-select12","LinkedProductProductDisplayOptions.Quantity as Quantity");
		
		//pricing type 
		displayOptions.setParameter("-select13","Products.PricingType as PricingType");
		
		return displayOptions;
	}
	
	public List<PropertyOption> getDisplayPropertyOptions(HttpServletRequest request, String displayProductID) {
		return getDisplayPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), displayProductID, null, null, null);
	}
	
	public List<PropertyOption> getDisplayPropertyOptions(HttpServletRequest request, String displayProductID, String categoryId, String search) {
		return getDisplayPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), displayProductID, categoryId, search, null);
	}
	
	public List<PropertyOption> getDisplayPropertyOptions(HttpServletRequest request, String displayProductID, String categoryId, String search, String type) {
		return getDisplayPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), displayProductID, categoryId, search, type);
	}
	
	public List<PropertyOption> getDisplayPropertyOptions(Connection con, UserBean user, String displayProductID, String categoryId, String search, String type) {
		if(logger.isTraceEnabled()) logger.trace("getDisplayPropertyOptions(Connection, {}, {},{}, {})", new String[]{user != null ? user.getName() :"null user", displayProductID, categoryId, search});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		final GenRow displayOptions = getPropertyOptionGenRow();
		try { 
			displayOptions.setConnection(con);
			
			displayOptions.setParameter("-select14", "LinkedProductProductDisplayOptions.SortOrder AS OptionSortOrder");
			displayOptions.setParameter("-select15", "LinkedProductProductDisplayOptions.Type AS Type");
			
			if("display".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type", "O+D");
			} else if("estate".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type", "E");
			} else if("option".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type","O");
			} else if("decor".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type","D");
			}
			
			displayOptions.sortBy("OptionSortOrder",0);
			//displayOptions.setParameter("-join4", "ProductLinkedProducts");
			displayOptions.setParameter("LinkedProductProductDisplayOptions.ProductID", displayProductID);
			
			if(StringUtils.isNotBlank(categoryId) && !"ALL".equals(categoryId)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.ProductDisplayOptionLinkedProducts.ProductGroupID", categoryId);
			}
			if(StringUtils.isNotBlank(search)) {
				displayOptions.setParameter("Name|1", "%" + search + "%");
				displayOptions.setParameter("Description|1", "%" + search + "%");
			}
			
			if(logger.isTraceEnabled()) {
				displayOptions.doAction("search");
				logger.trace(displayOptions.getStatement());
			}
			
			displayOptions.getResults(true);
			if(displayOptions.getNext()) {
				final List<PropertyOption> list = new ArrayList<PropertyOption>(displayOptions.size());
				do {
					list.add(new PropertyOption(displayOptions));
				}while(displayOptions.getNext());
				return list;
			} else {
				return getDisplayPropertyOptionsByAddress(con, user, displayProductID, categoryId, search, type);
			}
		} finally {
			displayOptions.close();
		}
		//return java.util.Collections.emptyList();
	}
	
	public List<PropertyOption> getDisplayPropertyOptionsByAddress(Connection con, UserBean user, String address, String categoryId, String search, String type) {
		if(logger.isTraceEnabled()) logger.trace("getDisplayPropertyOptions(Connection, {}, {},{}, {})", new String[]{user != null ? user.getName() :"null user", address, categoryId, search});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		final GenRow displayOptions = getPropertyOptionGenRow();
		try { 
			displayOptions.setConnection(con);
			displayOptions.setParameter("-select14", "LinkedProductProductDisplayOptions.SortOrder AS OptionSortOrder");
			displayOptions.setParameter("-select15", "LinkedProductProductDisplayOptions.Type AS Type");
			
			displayOptions.sortBy("OptionSortOrder",0);
			//displayOptions.setParameter("-join4", "ProductLinkedProducts");
			displayOptions.setParameter("LinkedProductProductDisplayOptions.Address", address);
			if(StringUtils.isNotBlank(search)) {
				displayOptions.setParameter("Name|1", "%" + search + "%");
				displayOptions.setParameter("Description|1", "%" + search + "%");
			}
			if(StringUtils.isNotBlank(categoryId) && !"ALL".equals(categoryId)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.ProductDisplayOptionLinkedProducts.ProductGroupID", categoryId);
			}
			
			if("display".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type", "O+D");
			} else if("estate".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type", "E");
			} else if("option".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type","O");
			} else if("decor".equals(type)) {
				displayOptions.setParameter("LinkedProductProductDisplayOptions.Type","D");
			}
			
			
			displayOptions.getResults(true);
			if(displayOptions.getNext()) {
				final List<PropertyOption> list = new ArrayList<PropertyOption>(displayOptions.size());
				do {
					list.add(new PropertyOption(displayOptions));
				}while(displayOptions.getNext());
				return list;
			} 
		} finally {
			displayOptions.close();
		}
		return java.util.Collections.emptyList();
	}
	
	public PropertyOption getPropertyOption(Connection conn, UserBean user, String category, String categoryId, String name, double cost) {
		if(logger.isTraceEnabled()) logger.trace("getDisplayPropertyOptions(Connection, {}, {},{},{},{})", new String[]{user != null ? user.getName() :"null user", category, categoryId, name, String.valueOf(cost)});
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Products")) {
			throw new AccessException("You do not have permission to access the products module");
		}
		PropertyOptionCategory poc = getPropertyOptionCategory(conn, user, category, categoryId);
		final GenRow worker = getPropertyOptionGenRow();
		try { 
			if(conn != null) {
				worker.setConnection(conn);
			} else {
				ActionBean.connect(worker);
			}
			worker.setParameter("ProductGroupID", poc.CategoryID);
			worker.setParameter("Name", name);
			worker.doAction(GenerationKeys.SELECTFIRST);
			
			if(!worker.isSuccessful()) {
				worker.setToNewID("ProductID");
				worker.setParameter("Cost", cost);
				worker.setParameter("GroupID", InitServlet.getSystemParam("DefaultGroupID"));
				worker.setParameter("CreatedBy", user.getUserID());
				worker.setParameter("CreatedDate", "NOW");
				worker.setParameter("ModifiedBy", user.getUserID());
				worker.setParameter("ModifiedDate", "NOW");
				
				worker.doAction(GenerationKeys.INSERT);
				if(!worker.isSuccessful()) {
					throw new RuntimeException("Failed to insert PropertyOptionCategory " + worker.toString());
				}
				
				worker.doAction(GenerationKeys.SELECT);
			} else {
				worker.setParameter("ModifiedBy", user.getUserID());
				worker.setParameter("ModifiedDate", "NOW");
				worker.setParameter("Cost", cost);
				worker.doAction(GenerationKeys.UPDATE);
			}
			return new PropertyOption(worker);
		} finally {
			worker.close();
		}
	}
}
