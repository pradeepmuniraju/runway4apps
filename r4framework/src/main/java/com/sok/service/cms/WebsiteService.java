package com.sok.service.cms;


import java.sql.Connection;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.DatabaseException;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.generation.IllegalConfigurationException;

import com.sok.runway.ErrorMap;
import com.sok.runway.URLFilter;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.Page;
import com.sok.runway.crm.cms.PageContent;
import com.sok.runway.crm.cms.PageLink;
import com.sok.runway.crm.cms.PageMeta;
import  com.sok.runway.externalInterface.beans.cms.CMSDomain;
import  com.sok.runway.externalInterface.beans.cms.CMSSite;
import  com.sok.runway.externalInterface.beans.cms.CMSUser;
import com.sok.runway.security.User;


import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.DataException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

public class WebsiteService {
	
	private static final Logger logger = LoggerFactory.getLogger(WebsiteService.class);
	private static final UserService userService = UserService.getInstance();
	private static final WebsiteService service = new WebsiteService();
	public static WebsiteService getInstance() {
		return service;
	}
	private WebsiteService() {}
	
	public PageLink getPageLink(HttpServletRequest request, String cmsID, String linkID) {
		logger.debug("getPageLink(request, cmsID={}, linkID={}", cmsID, linkID);
		return getPageLink(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, linkID);
	}
	
	public PageLink getPageLink(Connection con, UserBean user, String cmsID, String linkID) {
		PageLink p = new PageLink(con, linkID);
		if(!p.isLoaded()) {
			throw new NotFoundException("PageLink could not be found with linkID " + linkID);
		}
		if(!StringUtils.equals(cmsID, p.getField("CMSID"))) {
			throw new IllegalArgumentException("CMSID's did not match");
		}
		if(user != null) p.setCurrentUser(user.getUser());
		return p;
	}
	
	public PageLink updatePageLink(HttpServletRequest request, String cmsID, String linkID, PageLink pageLink) {
		return updatePageLink(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, linkID, pageLink);
	}
	
	public PageLink updatePageLink(Connection con, UserBean user, String cmsID, String linkID, PageLink pageLink) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		PageLink check = getPageLink(con, user, cmsID, linkID);
		if(!StringUtils.equals(check.getLinkID(), pageLink.getLinkID())) {
			throw new IllegalArgumentException("PageLinkID's did not match");
		}
		if(!StringUtils.equals(check.getCMSID(), pageLink.getCMSID())) {
			throw new IllegalArgumentException("CMSID's did not match");
		}
		if(!check.canEdit()) {
			throw new AccessException("You must be either a Editor or Publisher of this website to make changes");
		}
		pageLink.setConnection(con);
		pageLink.setCurrentUser(user.getUser());
		ErrorMap em = pageLink.update();
		
		if(em.isEmpty()) {
			check.load(linkID);
			return check;
		} else {
			logger.error(em.toString());
			throw new RuntimeException("Error in updating");
		}
	}
	
	public Page getPage(HttpServletRequest request, String cmsID, String pageID) {
		return getPage(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, pageID);
	}
	public Page getPage(Connection con, UserBean user, String cmsID, String pageID) {
		logger.debug("getPage(request, cmsID={}, pageID={}", new String[]{cmsID, pageID});
		
		/* 
		 * right now in runway, there is no LinkID in pages, but we'll want to add a linkId to the pages 
		 * so that they know which link they belong to (because there will be more than one of them)
		 * 
		 * unsure how this might work in relation to linking content to another part of the website, but suspect
		 * that'll work with pagecontent and not pages anymore. 
		 */
		Page p = new Page(con);
		p.load(pageID, cmsID);
		if(!p.isLoaded()) {
			throw new NotFoundException("Page could not be found with pageID " + pageID);
		}
		if(!StringUtils.equals(cmsID, p.getField("CMSID"))) {
			logger.error("Incoming CMSID={}, loaded={}", cmsID, p.getField("CMSID"));
			throw new IllegalArgumentException("CMSID's did not match");
		}
		if(user != null) p.setCurrentUser(user.getUser());
		return p;
	}
	
	/**
	 * While this isn't strictly correct in the current structure, it'll need to be able to be updated
	 * independenly in the future. 
	 * @param request
	 * @param cmsID
	 * @param linkID
	 * @param pageID
	 * @param page
	 * @return
	 */
	public Page updatePage(HttpServletRequest request, String cmsID, String linkID, String pageID, Page page) {
		return updatePage(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, linkID, pageID, page);
	} 
	
	public Page updatePage(Connection con, UserBean user, String cmsID, String linkID, String pageID, Page page) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(StringUtils.isEmpty(pageID) || !StringUtils.equals(pageID, page.getPageID())) { 
			throw new IllegalArgumentException("PageID must be non-empty and matching the object");
		}
		Page check = getPage(con, user, cmsID, pageID);
		if(!check.canEdit()) {
			throw new AccessException("You must be either a Editor or Publisher of this website to make changes");
		}
		if(StringUtils.isEmpty(cmsID) || !StringUtils.equals(cmsID, check.getField("CMSID"))) {
			throw new IllegalArgumentException("CMSID must be non-empty and matching the found data");
		}
		page.setConnection(con);
		page.setCurrentUser(user.getUser());
		/* do some check to see if the user can edit this page ? */
		ErrorMap em = page.update();
		if(em.isEmpty()) {
			check.load(pageID);
			return check;
		} else {
			logger.error(em.toString());
			throw new RuntimeException("Error in updating");
		}
	}
	
	public PageMeta getPageMeta(HttpServletRequest request, String cmsID, String linkID, String pageID, String pageMetaID) {
		return getPageMeta(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, linkID, pageID, pageMetaID);
	}
	public PageMeta getPageMeta(Connection con, UserBean user, String cmsID, String linkID, String pageID, String pageMetaID) {
		logger.debug("getPageMeta(request, cmsID={}, linkID={}, pageID={}, pageMetaID={}", new String[]{cmsID, linkID, pageID, pageMetaID});
		
		/* 
		 * right now in runway, there is no LinkID in pages, but we'll want to add a linkId to the pages 
		 * so that they know which link they belong to (because there will be more than one of them)
		 * 
		 * unsure how this might work in relation to linking content to another part of the website, but suspect
		 * that'll work with pagecontent and not pages anymore. 
		 */
		PageMeta p = new PageMeta(con, pageMetaID);
		if(!p.isLoaded()) {
			throw new NotFoundException("PageMeta could not be found with pageMetaID " + pageMetaID);
		}
		if(user != null) p.setCurrentUser(user.getUser());
		return p;
	}
	
	/**
	 * While this isn't strictly correct in the current structure, it'll need to be able to be updated
	 * independenly in the future. 
	 * @param request
	 * @param cmsID
	 * @param linkID
	 * @param pageMetaID
	 * @param page
	 * @return
	 */
	public PageMeta updatePageMeta(HttpServletRequest request, String cmsID, String linkID, String pageID, String pageMetaID, PageMeta page) {
		return updatePageMeta(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, linkID, pageID, pageMetaID, page);
	} 
	
	public PageMeta updatePageMeta(Connection con, UserBean user, String cmsID, String linkID, String pageID, String pageMetaID, PageMeta page) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(StringUtils.isEmpty(pageMetaID) || !StringUtils.equals(pageMetaID, page.getPageMetaID())) { 
			throw new IllegalArgumentException("PageMetaID must be non-empty and matching the object");
		}
		PageMeta check = getPageMeta(con, user, cmsID, linkID, pageID, pageMetaID);
		Page p = getPage(con, user, cmsID, pageID);
		if(!p.canEdit()) {
			throw new AccessException("You must be either a Editor or Publisher of this website to make changes");
		}
		if(StringUtils.isEmpty(cmsID) || !StringUtils.equals(cmsID, p.getField("CMSID"))) {
			throw new IllegalArgumentException("CMSID must be non-empty and matching the found data");
		}
		if(!StringUtils.equals(pageMetaID, p.getField("PageMetaID"))) {
			throw new IllegalArgumentException("PageMeta did not belong to this page");
		}
		page.setConnection(con);
		page.setCurrentUser(user.getUser());
		/* do some check to see if the user can edit this page ? */
		ErrorMap em = page.update();
		if(em.isEmpty()) {
			check.load(pageMetaID);
			return check;
		} else {
			logger.error(em.toString());
			throw new RuntimeException("Error in updating");
		}
	}
	public PageMeta createPageMeta(HttpServletRequest request, String cmsID, String linkID, String pageID, PageMeta page) {
		return createPageMeta(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, linkID, pageID, page);
	} 
	
	public PageMeta createPageMeta(Connection con, UserBean user, String cmsID, String linkID, String pageID, PageMeta page) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		Page p = getPage(con, user, cmsID, pageID);
		if(!p.canEdit()) {
			throw new AccessException("You must be either a Editor or Publisher of this website to make changes");
		}
		if(StringUtils.isEmpty(cmsID) || !StringUtils.equals(cmsID, p.getField("CMSID"))) {
			throw new IllegalArgumentException("CMSID must be non-empty and matching the found data");
		}
		page.setConnection(con);
		page.setCurrentUser(user.getUser());
		/* do some check to see if the user can edit this page ? */
		ErrorMap em = page.insert();
		if(em.isEmpty()) {
			p.setField("PageMetaID", page.getPageMetaID());
			em = p.update(); 
			if(em.isEmpty()) {
				page.load(page.getPageMetaID());
				return page;
			}
		} 
		logger.error(em.toString());
		throw new RuntimeException("Error in updating");
	}
	
	public CMSUser updateCMSUser(HttpServletRequest request, CMSUser user) {
		
		GenRow cu = new GenRow();
		cu.setRequest(request);
		cu.setViewSpec("cms3/CMSUserView");
		cu.setParameter("CMSUserID", user.getCMSUserID());
		cu.doAction(GenerationKeys.SELECT);
		
		if(!cu.isSuccessful()) {
			throw new NotFoundException("CMSUser record was not found for id : " + user.getCMSUserID());
		}
		
		cu.setParameter("CMSID", user.getCMSID());
		cu.setParameter("UserID", user.getUserID());
		cu.setParameter("Role", user.getRole());
		cu.doAction(GenerationKeys.UPDATE);
		
		if(cu.isSuccessful()) {
			return user;
		} else {
			logger.error("Failed to update CMSUser record with {} ", cu.getError());
			throw new RuntimeException("Failed to update CMSUser record with error " + cu.getError());
		}
	}
	public void deleteCMSUser(HttpServletRequest request, String CMSID, String CMSUserID) {
		GenRow cu = new GenRow();
		cu.setRequest(request);
		cu.setViewSpec("cms3/CMSUserView");
		cu.setParameter("CMSUserID", CMSUserID);
		cu.doAction(GenerationKeys.SELECT);
		
		if(!cu.isSuccessful()) {
			//already deleted, this is fine.
			return;
		}
		if(!cu.getData("CMSID").equals(CMSID)) {
			throw new RuntimeException("CMSID " + CMSID + " did not match CMSUser " + CMSUserID);
		}
		cu.doAction(GenerationKeys.DELETE);
		
		if(!cu.isSuccessful())
			throw new RuntimeException(cu.getError());
	}

	public boolean canEditWebsite(Connection con, User user, String CMSID) {
		
		//TODO - this should cache results so that userID - CMSID should be returned without db access
		if(StringUtils.isBlank(CMSID) || user == null) {
			return false;
		}
		GenRow cu = new GenRow();
		if(con == null) 
			ActionBean.connect(cu);
		else
			cu.setConnection(con);
		cu.setViewSpec("cms3/CMSUserView");
		cu.setParameter("CMSID", CMSID);
		cu.setParameter("UserID", user.getUserID());
		cu.doAction(GenerationKeys.SELECTFIRST);
		if(cu.isSuccessful() && ("Editor".equals(cu.getData("Role")) || "Publisher".equals(cu.getData("Role")))) {
			return true;
		} 
		return false;
	}
	
	public List<CMSUser> getCMSUsers(HttpServletRequest request, String CMSID) {
		GenRow cu = new GenRow();
		try {
			cu.setRequest(request);
			cu.setViewSpec("cms3/CMSUserView");
			cu.setParameter("CMSID", CMSID);
			cu.getResults(true);
			if(cu.getNext()) {
				List<CMSUser> list = new ArrayList<CMSUser>();
				do {
					list.add(new CMSUser(cu));
				} while(cu.getNext());
				return list;
			} else {
				return Collections.emptyList();
			}
		} finally {
			cu.close();
		}
	}

	public void deleteCMSDomain(HttpServletRequest request, String CMSID, String CMSDomainID) {
		GenRow cu = new GenRow();
		cu.setRequest(request);
		cu.setViewSpec("cms3/CMSDomainView");
		cu.setParameter("CMSDomainID", CMSDomainID);
		cu.doAction(GenerationKeys.SELECT);
		
		if(!cu.isSuccessful()) {
			//already deleted, this is fine.
			return;
		}
		if(!cu.getData("CMSID").equals(CMSID)) {
			throw new RuntimeException("CMSID " + CMSID + " did not match CMSDomain " + CMSDomainID);
		}
		cu.doAction(GenerationKeys.DELETE);
		
		if(!cu.isSuccessful())
			throw new RuntimeException(cu.getError());	
		
		URLFilter.loadShortURL(true);
	}
	
	
	public List<CMSDomain> getCMSDomains(HttpServletRequest request, String CMSID) {
		GenRow cms = new GenRow();
		try {
			cms.setRequest(request);
			cms.setViewSpec("cms3/CMSDomainView");
			cms.setParameter("CMSID", CMSID);
			if(cms.isSet("CMSID")) {
				cms.setParameter("-sort1","Domain");
				cms.getResults(true);
				if(cms.getNext()) { 
					List<CMSDomain> list = new ArrayList<CMSDomain>(cms.getSize());
					do {
						list.add(new CMSDomain(cms));
					} while(cms.getNext());
					return list;
				}
			}
			return Collections.emptyList();
		} finally { 
			cms.close();
		}
	}
	
	public CMSSite getCMS(HttpServletRequest request, String cmsID) {
		return getCMS(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID);
	} 
	
	public CMSSite getCMS(Connection con, UserBean user, String cmsID) {
		GenRow cms = new GenRow();
		cms.setConnection(con);
		cms.setViewSpec("CMSView");
		cms.setParameter("CMSID", cmsID);
		cms.doAction(GenerationKeys.SELECT);
		
		if(cms.isSuccessful()) {
			//if(user !user.canView(cms) && !canEditWebsite(con, user.getUser(), cmsID)){
			//	throw new AccessException("You do not have permission to access this cms site");
			//}
			return new CMSSite(cms);
		}
		throw new NotFoundException("Could not find CMS record with ID " + cmsID);
	}
	public CMSDomain createCMSDomain(HttpServletRequest request, CMSDomain domain) {
		//Values are trimmed in the object 
		if(domain.getCMSID() == null) {
			throw new IllegalArgumentException("CMSDomain CMSID was null or empty");
		} else if(domain.getDomain() == null) {
			throw new IllegalArgumentException("CMSDomain Domain was null or empty");
		}
		GenRow cms = new GenRow();
		cms.setViewSpec("cms3/CMSDomainView");
		cms.setParameter("CMSID", domain.getCMSID());
		cms.setParameter("Domain", domain.getDomain());
		cms.setToNewID("CMSDomainID");
		try {
			cms.doAction(GenerationKeys.INSERT);
		} catch(DatabaseException de) {
			Throwable cause = de.getCause();
			if(cause != null && cause instanceof SQLIntegrityConstraintViolationException) {
				//domain already existed
				cms.remove("CMSDomainID");
				cms.doAction(GenerationKeys.SELECTFIRST);
				if(cms.isSuccessful()) {	//which it should be as the integrity constraint is on CMSID and Domain
					domain.setCMSDomainID(cms.getData("CMSDomainID"));
					return domain;
				} else {
					throw new RuntimeException("Failed to insert or retrieve data " + cms.getError());
				}
			} else { 
				throw de;
			}
		}
		if(cms.isSuccessful()) {
			domain.setCMSDomainID(cms.getString("CMSDomainID"));
			
			URLFilter.loadShortURL(true);
			return domain;
		} else {
			throw new RuntimeException("Failed to insert data " + cms.getError());
		}
	}
	
	public PageContent getPageContent(HttpServletRequest request, String cmsID, String pageID, String pageContentID) {
		return getPageContent(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, pageID, pageContentID);
	}
	
	public PageContent getPageContent(Connection con, UserBean user, String cmsID, String pageID, String pageContentID) {
		
		PageContent pc = new PageContent(con, pageContentID);
		if(!StringUtils.equals(pc.getPageID(), pageID)) { 
			throw new RuntimeException("PageIDs did not match");
		}
		return pc; 
	}
	
	public PageContent updatePageContent(HttpServletRequest request, String cmsID, String pageID, String pageMetaID, PageContent page) {
		return updatePageContent(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, pageID, pageMetaID, page);
	} 
	
	public PageContent updatePageContent(Connection con, UserBean user, String cmsID, String pageID, String pageContentID, PageContent page) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(StringUtils.isEmpty(pageContentID) || !StringUtils.equals(pageContentID, page.getPageContentID())) { 
			throw new IllegalArgumentException("PageContentID must be non-empty and matching the object");
		}
		PageContent check = getPageContent(con, user, cmsID, pageID, pageContentID);
		if(StringUtils.isEmpty(cmsID)) {
			throw new IllegalArgumentException("CMSID must be non-empty and matching the found data");
		}
		Page p = new Page(con, pageID);
		if(!p.getField("PageID").equals(p.getField("MasterPageID"))) {
			p.load(p.getField("MasterPageID"));		// this one will be linked to cms, sort of.
		}
		if(StringUtils.isEmpty(cmsID) || !StringUtils.equals(cmsID, p.getField("CMSID"))) {
			throw new IllegalArgumentException("CMSID must be non-empty and matching the found data");
		}
		if(!p.canEdit()) {
			throw new AccessException("You must be either a Editor or Publisher of this website to make changes");
		}
		page.setConnection(con);
		page.setCurrentUser(user.getUser());
		/* do some check to see if the user can edit this page ? */
		ErrorMap em = page.update();
		if(em.isEmpty()) {
			check.load(pageContentID);
			//TODO UPDATE CACHE 
			return check;
		} else {
			logger.error(em.toString());
			throw new RuntimeException("Error in updating");
		}
	}
	public PageContent createPageContent(HttpServletRequest request, String cmsID, String pageID, PageContent page) {
		return createPageContent(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, pageID, page);
	} 
	
	public PageContent createPageContent(Connection con, UserBean user, String cmsID, String pageID, PageContent page) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(StringUtils.isEmpty(cmsID)) {
			throw new IllegalArgumentException("CMSID must be non-empty and matching the found data");
		}
		Page p = new Page(con, pageID);
		if(!p.getField("PageID").equals(p.getField("PageMasterID"))) {
			p.load(p.getField("PageMasterID"));		// this one will be linked to cms, sort of.
		}
		if(StringUtils.isEmpty(cmsID) || !StringUtils.equals(cmsID, p.getField("CMSID"))) {
			throw new IllegalArgumentException("CMSID must be non-empty and matching the found data");
		}
		if(!p.canEdit()) {
			throw new AccessException("You must be either a Editor or Publisher of this website to make changes");
		}
		page.setConnection(con);
		page.setCurrentUser(user.getUser());
		/* do some check to see if the user can edit this page ? */
		ErrorMap em = page.insert();
		if(em.isEmpty()) {
			p.setField("PageContentID", page.getPageContentID());
			em = p.update(); 
			if(em.isEmpty()) {
				page.load(page.getPageContentID());
				//TODO UPDATE CACHE 
				return page;
			}
		} 
		logger.error(em.toString());
		throw new RuntimeException("Error in updating");
	}
	
	public List<PageContent> getPageContents(HttpServletRequest request, String cmsID, String pageID) {
		if(logger.isDebugEnabled()) logger.debug("getPageContents(request, cmsID={}, pageID={})", new String[]{cmsID, pageID});
		return getPageContents(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, pageID);
	}
	
	public List<PageContent> getPageContents(Connection con, UserBean user, String cmsID, String pageID) {
		if(logger.isDebugEnabled()) logger.debug("getPageContents(con, user={}, cmsID={}, pageID={})", new String[]{user!=null?user.getName():"null user",cmsID, pageID});
		
		//TODO CHECK CACHE 
		
		/* nb, cmsID and linkID may be null, maybe don't bother? */
		GenRow content = new GenRow();
		try {
			if(con != null)
				content.setConnection(con);
			content.setTableSpec("cms3/PageContent");
			content.setParameter("-select1","PageContentID");
			content.setParameter("PageID", pageID);
			if(content.isSet("PageID")) {
				if(logger.isTraceEnabled()) {
					content.doAction(GenerationKeys.SEARCH);
					logger.trace(content.getStatement());
				}
				content.getResults(true);
				if(content.getNext()) {
					List<PageContent> list = new ArrayList<PageContent>(content.getSize());
					do {
						list.add(new PageContent(content.getConnection(), content.getData("PageContentID")));
					}  while(content.getNext());
					
					//TODO UPDATE CACHE 
					return list;
				}
			}
			return Collections.emptyList();
		} finally {
			content.close();
		}
	}
	
	public List<PageLink> getPageLinks(HttpServletRequest request, String cmsID) {
		return getPageLinks(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID);
	}
	public List<PageLink> getPageLinks(Connection con, UserBean user, String cmsID) {
		GenRow content = new GenRow();
		try {
			content.setConnection(con);
			content.setTableSpec("PageLinks");
			content.setParameter("-select1","LinkID");
			content.setParameter("CMSID", cmsID);
			if(content.isSet("CMSID")) {
				content.getResults(true);
				if(content.getNext()) {
					List<PageLink> list = new ArrayList<PageLink>(content.getSize());
					do {
						list.add(new PageLink(con, content.getData("LinkID")));
					}  while(content.getNext());
					return list;
				}
			}
			return Collections.emptyList();
		} finally {
			content.close();
		}
	}
	
	public Page createPage(HttpServletRequest request, String cmsID, Page page) {
	
		return createPage(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, page);
	}
	public Page createPage(Connection con, UserBean user, String cmsID, Page page) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!canEditWebsite(con, user.getUser(), cmsID)) {
			throw new AccessException("You do not have permission to create pages");
		}
		page.setConnection(con);
		page.setCurrentUser(user.getUser());
		ErrorMap em = page.insert();
		if(em.isEmpty()) {
			return page;
		} 
		throw new DataException("Error saving Page", em);
	}
	
	public PageLink createPageLink(HttpServletRequest request, String cmsID, PageLink pageLink) {

		return createPageLink(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, pageLink);
	}
	public PageLink createPageLink(Connection con, UserBean user, String cmsID, PageLink pageLink) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!StringUtils.equals(cmsID, pageLink.getField("CMSID"))) {
			throw new IllegalArgumentException("CMSID's did not match");
		}
		
		pageLink.setConnection(con);
		pageLink.setCurrentUser(user.getUser());
		if(!pageLink.canCreate()) {
			throw new AccessException("You do not have permission to create pages");
		}
		if(StringUtils.isBlank(pageLink.getShortURL())) {
			throw new IllegalArgumentException("A short url must be supplied");
		}
		GenRow c = new GenRow();
		c.setConnection(con);
		c.setTableSpec("PageLinks");
		c.setParameter("-select1","LinkID");
		c.setParameter("CMSID", cmsID);
		c.setParameter("ShortURL", pageLink.getShortURL());
		c.doAction(GenerationKeys.SELECTFIRST);
		if(c.isSuccessful()) {
			throw new RuntimeException("That short url is in use, please select another");
		}
		
		c.clear();
		c.setParameter("-select1","Path");
		c.setParameter("PageID", pageLink.getParentID());
		c.setParameter("CMSID", cmsID);
		if(c.isSet("PageID")) 
			c.doAction(GenerationKeys.SELECTFIRST);
		
		String genPath = KeyMaker.generate(5);
		StringBuilder path = new StringBuilder();
		if(c.isSuccessful()) {
			path.append(c.getData("Path"));
		} else {
			CMSSite cms = getCMS(con, user, cmsID);
			path.append('/').append(cms.Path);
		}
		path.append('/').append(genPath);
		
		pageLink.setField("Path", path.toString());
		pageLink.setField("ExternalPath", path.toString());
		pageLink.setField("Type", "Default"); 		//linking pages should be done elsewhere 
		
		ErrorMap em = pageLink.insert();
		if(em.isEmpty()) {
			URLFilter.addShortURL(cmsID, pageLink.getShortURL(), pageLink.getPath());
			return getPageLink(con, user, cmsID, pageLink.getLinkID());
		} 
		throw new DataException("Error saving PageLink", em);
	}
	public void deletePageLink(HttpServletRequest request, String cmsID, String linkID) {

		deletePageLink(ActionBean.getConnection(request), userService.getCurrentUser(request), cmsID, linkID);
	}

	public void deletePageLink(Connection con, UserBean user, String cmsID, String linkID) {
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		PageLink pageLink = new PageLink(con, linkID);
		if(!pageLink.isLoaded()) 
			return;
		
		if(!StringUtils.equals(cmsID, pageLink.getField("CMSID"))) {
			throw new IllegalArgumentException("CMSID's did not match");
		}
		pageLink.setCurrentUser(user.getUser());
		if(!pageLink.canDelete()) {
			throw new AccessException("You do not have permission to delete pages");
		}
		//this actually should never be true, if it's loaded then we have a pk 
		if(!pageLink.delete()) {
			throw new RuntimeException("An internal error occurred");
		}
	}
}