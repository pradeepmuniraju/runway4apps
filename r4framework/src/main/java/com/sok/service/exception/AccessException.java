package com.sok.service.exception;

public class AccessException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	public AccessException(String string) {
		super(string);
	}
}
