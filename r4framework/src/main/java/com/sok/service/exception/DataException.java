package com.sok.service.exception;
import com.sok.runway.ErrorMap;

public class DataException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private final ErrorMap er; 
	public DataException(String string, ErrorMap er) {
		super(string);
		this.er = er;
	}
	public ErrorMap getErrorMap() {
		return er;
	}
}
