package com.sok.service.exception;

public class NotAuthenticatedException extends AccessException {
	private static final long serialVersionUID = 1L;
	
	public NotAuthenticatedException() {
		super("You are not logged in");
	}
	
	public NotAuthenticatedException(String message) {
		super(message);
	}
}
