package com.sok.service.exception;

/**
 * An exception to signify that this record could not be found. 
 * @author Michael Dekmetzian
 * @date 14 May 2012
 */
public class NotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	public NotFoundException(String message) {
		super(message);
	}
}
