package com.sok;

import java.io.CharArrayWriter;
import java.io.PrintWriter;

import javax.servlet.ServletException;

public class Debugger {
   
   private static Debugger debugger = null;
   
   private boolean debug = false;
   
   private StringBuffer messages = null;
   
   public Debugger() {
      messages = new StringBuffer();
   }
   
   public static Debugger getDebugger() {
      if (debugger == null) {
         debugger = new Debugger();
      }
      return debugger;
   }
   
   public void debug(String message) {
      if (debug) {
      	 if(messages.length() > 2000000) { 
      	 	int rem = messages.length() - 2000000; 
      	 	messages.delete(0, rem); 
      	 } 
      	 
         messages.append(message);
         messages.append("<br/>");
      }
   }
   
   public void debug(Exception e) {
      if (debug && e != null) {
         messages.append(trace(e));
         messages.append("<br/>");
      }
   }
   
   public String getMessages() {
      return messages.toString();
   }
   
   public void setDebug(boolean debug) {
      this.debug = debug;
   }
   
   public boolean isDebugging() {
      return debug;
   }
   
   public void clear() {
      messages = new StringBuffer();
   }
   
   public static String trace(Exception e) {
		
		StringBuffer msg = new StringBuffer(); 

		CharArrayWriter c = new CharArrayWriter();
		PrintWriter p = new PrintWriter(c);

		CharArrayWriter cl = new CharArrayWriter();
		PrintWriter pl = new PrintWriter(cl);

		p.write(e.toString()); 
		e.printStackTrace(pl);
		
		Throwable cause = e.getCause();
		
		if (cause != null) {
			p.write("<br/>Caused By: ");
			p.write(cause.toString()); 

			pl.write("<br/><br/>Caused By:<br/><br/>");
			cause.printStackTrace(pl);
		}
		
		if(e instanceof ServletException) { 
			ServletException se = (ServletException)e; 

			cause = se.getRootCause(); 

			if (cause != null) {
			   p.write("<br/><br/>Root Cause: ");
			   p.write(cause.toString()); 

			   pl.write("<br/><br/>Root Cause:<br/><br/>");
			   cause.printStackTrace(pl);
			}
		} 

		msg.append("<br/>Exception : ").append(c.toString()); 
		msg.append("<br/><br/>Stack Trace: <br/>").append(cl.toString()); 

		return msg.toString(); 
	}
}