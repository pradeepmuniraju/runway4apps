package com.sok.runway;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import com.sok.framework.*;
public class ExcelServlet extends HttpServlet
{
	private static String templatehome = "excel/";

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		doGet(req, res);
	}

	TableBean getBean(HttpServletRequest request)
	{
		String beanname = request.getParameter("bean");
		String viewname = request.getParameter("view");
		TableBean bean = null;

			HttpSession session = request.getSession();
			bean = (TableBean)session.getAttribute(beanname);
			bean.setViewSpec(SpecManager.getViewSpec(viewname));
			bean.generateSQLStatment();

		return(bean);
	}

	void writeExcel(OutputStream out, ResultSet current, String template, TableBean bean)throws ServletException
	{
		try
		{
			File file = new File(template);
			FileInputStream fis = new FileInputStream(file);
			POIFSFileSystem fs = new POIFSFileSystem(fis);
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheetAt(0);

			ResultSetMetaData meta = current.getMetaData();
			int colsize = meta.getColumnCount();

			int rowindex = 0;

			HSSFRow row = getRow(sheet,rowindex);
			HSSFCell cell = null;

			String name = null;
			for(int j=0; j<colsize; j++)
			{
				name = meta.getColumnName(j+1);
			   cell = getCell(row,j);
			   cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			   cell.setCellValue(name);
			}

			rowindex++;

			while(current.next())
			{
			   row = getRow(sheet,rowindex);
				for(int i=0; i< colsize; i++)
				{
					name = meta.getColumnName(i+1);
			    	cell = getCell(row,i);
			   	cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			    	cell.setCellValue(ActionBean.getFormatedString(name, i, current, bean));
				}
				rowindex++;
			}
		   wb.write(out);
		}
		catch(Exception e)
		{
			throw(new ServletException(e));
		}

	}

	public HSSFCell getCell(HSSFRow row, int i)
	{
		HSSFCell cell =  null;//row.getCell((short)i); //from 0
		if (cell == null)
		{
			cell = row.createCell(i);
		}
		return(cell);
	}

	public HSSFRow getRow(HSSFSheet sheet, int rowindex)
	{
		HSSFRow row = sheet.getRow(rowindex); //from 0
		if(row == null)
		{
			row = sheet.createRow((short)rowindex);
		}
		return(row);
	}

   public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
		String debugparam = request.getParameter("-debug");
		boolean debug = false;
		if(debugparam!=null)
		{
			debug = true;
		}
		
		if(debug)
		{
			response.setContentType("text/plain");
		}
		else
		{
			response.setContentType("application/vnd.ms-excel");
		}
		OutputStream out = response.getOutputStream();
		TableBean bean = getBean(request);
		String stmtstring = bean.getSearchStatement();
		String template = request.getParameter("template");
		StringBuffer templatepath  = new StringBuffer();
		String contextPath = getServletContext().getRealPath("");
		templatepath.append(contextPath);
			char end = contextPath.charAt(contextPath.length()-1);
			if(end != '/' && end !='\\')
			{
				templatepath.append("/");
			}
		templatepath.append(templatehome);
		templatepath.append(template);

		String dbconn = getServletContext().getInitParameter("sqlJndiName");
		Connection con = null;
		Context ctx = null;
		ResultSet current = null;
		try
		{
			ctx = new InitialContext();

				DataSource ds = null;
				try{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource)envContext.lookup(dbconn);
				}catch(Exception e){}
				if(ds == null)
				{
					ds = (DataSource)ctx.lookup(dbconn);
				}
			con = ds.getConnection();
			Statement stmt = con.createStatement();
			current = stmt.executeQuery(stmtstring);
			writeExcel(out, current, templatepath.toString(), bean);
		}
		catch(SQLException sqlex)
		{
			throw(new ServletException(sqlex));
		}
		catch(NamingException nex)
		{
			throw(new ServletException(nex));
		}
		finally
		{
    		if (current != null) 
    		{
				try{
            	current.close();
        		}catch (Exception ex){}
    		}
    		if (con != null) 
    		{
				try{
            	con.close();
        		}catch (Exception ex){}
    		}
    		if (ctx != null) 
    		{
        		try{       
            	ctx.close();
        		}catch (Exception ex){}
    		}
    		if (out != null) 
    		{
        		try{
            	out.close();
        		}catch (Exception ex){}
    		}
		}
	}
}
