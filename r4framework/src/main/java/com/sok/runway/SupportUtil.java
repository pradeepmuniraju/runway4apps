package com.sok.runway;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

public class SupportUtil {

	public static final String  TEMPLATE_NAME_APPROVAL_NOTIFICATION = "Approval Notification";
	public static final String  TEMPLATE_NAME_STATUS_CHANGE_NOTIFICATION = "Approval Notification";
	
	
	public static double getTotalSpentHours(HttpServletRequest request, String orderID) throws Exception {
		return getTotalSpentHours(ActionBean.getConnection(request), orderID);
	}

	public static double getTotalApprovedHours(HttpServletRequest request, String orderID) throws Exception {
		return getTotalApprovedHours(ActionBean.getConnection(request), orderID);
	}
	
	public static double getRemainingHours(HttpServletRequest request, String orderID) throws Exception {
		return getTotalApprovedHours(request, orderID) - getTotalSpentHours(request, orderID);
	}
	
	
	

	public static double getTotalSpentHours(Connection con, String orderID) throws Exception {
		return getTotalSpentHours(con, null, orderID );
	}
	
	public static double getTotalApprovedHours(Connection con, String orderID) throws Exception {
		return getTotalApprovedHours(con, null, orderID );
	}
	
	public static double getRemainingHours(Connection con, String orderID) throws Exception {
		return getTotalApprovedHours(con, orderID) - getTotalSpentHours(con, orderID);
	}
	
	
	
	private static double getTotalSpentHours(Connection con, String jndiName, String orderID) throws Exception {
		double totalTime = 0;

		GenRow slips = new GenRow();
		slips.setViewSpec("SlipsView");
		
		if(con != null && !con.isClosed())
			slips.setConnection(con);
		else if(StringUtils.isNotBlank(jndiName))
			slips.setJndiName(jndiName);
		
		slips.setParameter("SlipsOrderItems.OrderItemOrder.OrderID", orderID);
		slips.setParameter("-select0", "SUM(TimeTaken) as TotalTimeTaken");
		slips.doAction(GenerationKeys.SELECTFIRST);

		if (slips.isSuccessful() && StringUtils.isNotBlank(slips.getData("TotalTimeTaken")))
			totalTime = slips.getDouble("TotalTimeTaken");

		slips.close();
		return totalTime;
	}

	private static double getTotalApprovedHours(Connection con, String jndiName, String orderID) throws Exception {
		double totalTime = 0;

		GenRow approvals = new GenRow();
		approvals.setViewSpec("OrderApprovalsView");
		
		if(con != null && !con.isClosed())
			approvals.setConnection(con);
		else if(StringUtils.isNotBlank(jndiName))
			approvals.setJndiName(jndiName);
		
		approvals.setParameter("OrderID", orderID);
		approvals.setParameter("-select0", "SUM(ApprovedHours) as TotalApprovedHours");
		approvals.doAction(GenerationKeys.SELECTFIRST);

		if (approvals.isSuccessful() && StringUtils.isNotBlank(approvals.getData("TotalApprovedHours")))
			totalTime = approvals.getDouble("TotalApprovedHours");

		approvals.close();
		return totalTime;
	}
	
	public static double getRemainingHours(Connection con, String jndiName, String orderID) throws Exception {
		return getTotalApprovedHours(con, jndiName, orderID) - getTotalSpentHours(con, jndiName, orderID);
	}

}
