package com.sok.runway;

import org.apache.commons.lang.StringUtils;

public class SalesRepBean {

private String repName; 
	
	private String repMobile; 
	
	private String repPhone; 
	
	private String repEmail; 
	
	private String repUserID;
	
	private String address;
	
	public SalesRepBean(){};
	
	public SalesRepBean(String repName, String repMobile, String repPhone, String repEmail, String repUserID){
		this.repName = repName;
		this.repMobile = repMobile;
		this.repPhone = repPhone;
		this.repEmail = repEmail;
		this.repUserID = repUserID;
	}
	
	/**
	 * @return the repName
	 */
	public String getRepName() {
		if(StringUtils.isNotBlank(repName))
		return repName;
		return "";
	}
	/**
	 * @param repName the repName to set
	 */
	public void setRepName(String repName) {
		this.repName = repName;
	}
	/**
	 * @return the repMobile
	 */
	public String getRepMobile() {
		if(StringUtils.isNotBlank(repMobile))
		return repMobile;
		return "";
	}
	/**
	 * @param repMobile the repMobile to set
	 */
	public void setRepMobile(String repMobile) {
		this.repMobile = repMobile;
	}
	/**
	 * @return the repPhone
	 */
	public String getRepPhone() {
		if(StringUtils.isNotBlank(repPhone))
		return repPhone;
		return "";
	}
	/**
	 * @param repPhone the repPhone to set
	 */
	public void setRepPhone(String repPhone) {
		this.repPhone = repPhone;
	}
	/**
	 * @return the repEmail
	 */
	public String getRepEmail() {
		if(StringUtils.isNotBlank(repEmail))
		return repEmail;
		return "";
	}
	/**
	 * @param repEmail the repEmail to set
	 */
	public void setRepEmail(String repEmail) {
		this.repEmail = repEmail;
	}
	/**
	 * @return the repUserID
	 */
	public String getRepUserID() {
		if(StringUtils.isNotBlank(repUserID))
		return repUserID;
		return "";
	}
	/**
	 * @param repUserID the repUserID to set
	 */
	public void setRepUserID(String repUserID) {
		this.repUserID = repUserID;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
}
