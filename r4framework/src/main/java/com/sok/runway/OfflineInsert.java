package com.sok.runway;
import javax.servlet.*;     
import javax.servlet.http.*;

import java.util.*;
import java.sql.*;
import java.text.*;
import com.sok.framework.*;

public class OfflineInsert extends OfflineUpdate {
   
	public static final String INSERT_SPEC = "-insertspec";
	
	TableSpec insertTableSpec = null;
   boolean includeCompanyID = false;
   
   ArrayList foreignset = new ArrayList(500);
   
	public OfflineInsert(HttpServletRequest request, ServletContext context) {
		super(request, context, false);
        initProcess(request, context);
        start();
	}
   
    public void initProcess(HttpServletRequest request, ServletContext context)
    {
        super.initProcess(request, context);
        insertTableSpec = SpecManager.getTableSpec(requestbean.getString(INSERT_SPEC));
        tspec = search.getTableSpec();
                
        if (getTargetTableName().equals("Notes") && getSourceTableName().equals("Contacts")) {
          includeCompanyID = true;
        }
        
        requestbean.put("CreatedDate","NOW");
        requestbean.put("CreatedBy",super.user.getCurrentUserID());
    }
    
	void getStatementString()
	{
		StringBuffer statement = new StringBuffer();
		
		statement.append("insert into ");
		statement.append(insertTableSpec.getTableName());
		statement.append(" ( ");
		
		StringBuffer values = new StringBuffer();
		
		statement.append(insertTableSpec.getPrimaryKeyName());
		values.append(ActionBean.getDatabase().getNewIDMethod());
		
		int n = 0;
		for(int i=0; i< insertTableSpec.getFieldsLength(); i++) {
			String fieldname = insertTableSpec.getFieldName(i);
			if(requestbean.containsKey(fieldname)) {
				statement.append(_comma);
				values.append(_comma);

				statement.append(fieldname);
				values.append("?");
				n++;
			}
		}
		
		if(insertcontactstatus) {
			statement.append(_comma);
			values.append(_comma);

			statement.append("ContactStatusID");
			values.append("?");
		}
		else if(insertcompanystatus) {
			if(n!=0)
			{
				statement.append(_comma);
				values.append(_comma);
			}
			statement.append("CompanyStatusID");
			values.append("?");
		}
		
		statement.append(_comma);
		statement.append(tspec.getPrimaryKeyName());
		values.append(_comma);
		values.append("?");
		
		
		if (includeCompanyID) {
		   statement.append(_comma);
			values.append(_comma);
			
   		statement.append("CompanyID");
   		values.append("?");
		}
		
		
      statement.append(" ) values ( ");
      statement.append(values);
      statement.append(" ) ");
      
		statementstring = statement.toString();
		if(debug)
		{
			errors.append("\r\n");
			errors.append(statementstring);
			errors.append("\r\n");
		}
	}


	void setParameters(int index)throws Exception
	{
		int n = 0;
		String fieldname = null;
		String value = null;
		String recordid = (String)foundset.get(index);
		for(int j=0; j<insertTableSpec.getFieldsLength(); j++)
		{
			fieldname = insertTableSpec.getFieldName(j);
			int fieldtype = insertTableSpec.getFieldType(j);
			if(requestbean.containsKey(fieldname))
			{
				value = (String)requestbean.get(fieldname);
				n++;
				if(fieldtype==Types.TIMESTAMP)
				{
					if(value==null || value.length()==0)
					{
						updatestm.setNull(n, Types.TIMESTAMP);
					}
					else
					{
						Timestamp ts = new Timestamp(getDate(j,value).getTime());
						updatestm.setTimestamp(n, ts);
					}
				}					
				else
				{
					if(fieldtype!=Types.VARCHAR)
					{
						if(value==null || value.length()==0)
						{
							updatestm.setNull(n, fieldtype);
						}
						else
						{
							updatestm.setString(n, value);
						}
					}
					else
					{
						if(value==null || value.length()==0)
						{						
							updatestm.setString(n, _blank);
						}
						else
						{
							updatestm.setString(n, value);
						}
					}
				}
			}
		}

		n++;
		updatestm.setString(n++, recordid);
		
		if (includeCompanyID) {
		   updatestm.setString(n, (String)foreignset.get(index));
		}
	}
	
	
	protected java.util.Date getDate(int specindex , String value)
	{
		if(value!=null && !value.equals(NOW))
		{
		   return ActionBean.getDate( value, insertTableSpec.getFormat(specindex), getLocale());
		   
			/*SimpleDateFormat sdf = new SimpleDateFormat(insertTableSpec.getFormat(specindex), getLocale());
			try{
				return(sdf.parse(value));
			}catch(Exception e){}*/	
		}
		return(new java.util.Date());
	}
	
	void setEmailHeader()
	{
		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, getLocale());
		status.append("Runway offline process - Insert ");
		status.append(getTargetTableName());
		status.append("\r\n");
		status.append("Initiated ");
		status.append(dt.format(new java.util.Date()));
		status.append("\r\n");
	}
	
	protected String getTargetTableName() {
	   return insertTableSpec.getTableName();
	}
	
   /*protected String getRetrieveQuery() {
		String idname = tspec.getPrimaryKeyName();
		String tablename = getSourceTableName();
		
		com.sok.framework.sql.SqlDatabase database = ActionBean.getDatabase();
		
		StringBuffer searchstmt = new StringBuffer();
		searchstmt.append("select ");
		if (database instanceof com.sok.framework.sql.MSSQL) {
         database.appendTopStatement(searchstmt, search.getTop());
      }
		searchstmt.append(tablename);
		searchstmt.append(".");
		searchstmt.append(idname);
		if (includeCompanyID) {
   		searchstmt.append(", ");
   		searchstmt.append(tablename);
   		searchstmt.append(".");
   		searchstmt.append("CompanyID");
		}
		searchstmt.append(" from ");
		searchstmt.append(tablename);
		searchstmt.append(" where ");
		searchstmt.append(search.getSearchCriteria());
      if (database instanceof com.sok.framework.sql.MySQL) {
         database.appendTopStatement(searchstmt, search.getTop());
      }
		
		return searchstmt.toString();
   }*/
  
   public String getStatusMailSubject()
   {
	   return(contextname + " Runway offline process - Insert "+getTargetTableName());
   }  
   
   protected void processExtraFields(TableData current) throws SQLException {
      if (includeCompanyID) {
	      foreignset.add(current.getString("CompanyID"));
      }
   }
}
