package com.sok.runway.view.crm.admin;

import com.sok.runway.crm.*;
import com.sok.runway.crm.admin.*;
import com.sok.runway.view.*;
import com.sok.runway.security.AuditEntity;
import com.sok.framework.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class StageView extends Stage
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("GroupDisplay");

   public StageView(Connection con)
   {
      super(con);
   }

   public StageView(ServletRequest request)
   {
      super(request);

      request.setAttribute("stage",this);
   }

   public StageView(Connection con, String ProfileID)
   {
      super(con, ProfileID);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }

}