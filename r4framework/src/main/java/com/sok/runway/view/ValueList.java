package com.sok.runway.view;


import com.sok.framework.*;
import com.sok.framework.generation.*;
import java.util.*;

public class ValueList {
   
   private static HashMap<String, ValueList> valueLists = new HashMap<String, ValueList>();
   
   public static ValueList getValueList(String listName) {
      
      if (listName != null) {
         ValueList list = valueLists.get(listName);
         
         if (list == null) {
            list = load(listName);         
         }
         
         return list;
      }
      else {
         return null;
      }
   }
   
   private static synchronized ValueList load(String listName){
      
      ValueList list = (ValueList)valueLists.get(listName);
      
      if (list == null) {      
         list = new ValueList(listName);
         valueLists.put(listName, list);
      }
      return(list);
   }
   
   public static void reload(String listName) {
      valueLists.remove(listName);
   }
   
   private ArrayList<String> labels = null;
   private ArrayList<String> values = null;
   private ArrayList<Integer> quantifiers = null;
   private ArrayList<String> displayFields = null;
   private ArrayList<String> displayNames = null;
   
   private String listName = null;
   private String defaultValue = null;
   
   public ValueList(String listName) {
      
      this.listName = listName;
      
      labels = new ArrayList<String>();
      values = new ArrayList<String>();
      quantifiers = new ArrayList<Integer>();
      displayFields = new ArrayList<String>();
      displayNames = new ArrayList<String>();
      
      GenRow listItems = new GenRow();
      listItems.setJndiName(ActionBean.getJndiName());
      listItems.setViewSpec("ItemListView");
      listItems.setParameter("ListName", listName);
      listItems.doAction(GenerationKeys.SELECTFIRST);
      
      if(listItems.isSuccessful()) { 
	      String listID = listItems.getData("ListID");
	      this.defaultValue = listItems.getData("DefaultValue");
	      
	      listItems = new GenRow();
	      listItems.setJndiName(ActionBean.getJndiName());
	      listItems.setTableSpec("ListItems");
	      listItems.setParameter("-select#1", "ItemLabel");
	      listItems.setParameter("-select#2", "ItemValue");
	      listItems.setParameter("-select#3", "Quantifier");
	      listItems.setParameter("-select#4", "DisplayField");
	      listItems.setParameter("-select#5", "DisplayName");
	      
	      listItems.setParameter("-sort0", "SortNumber");
	      
	      listItems.setParameter("ListID", listID);
	      
	      listItems.doAction(ActionBean.SEARCH);
	      listItems.getResults();
	      
	      while (listItems.getNext()) {
	         values.add(listItems.getData("ItemValue"));
	         
	         String label = listItems.getData("ItemLabel");
	         if (label == null || label.length() == 0) {
	            labels.add(listItems.getData("ItemValue"));
	         }
	         else {
	            labels.add(listItems.getData("ItemLabel"));
	         }
	         quantifiers.add(listItems.getInt("Quantifier"));
	         
	         displayFields.add(listItems.getData("DisplayField"));        
	         displayNames.add(listItems.getData("DisplayName"));
	      }
	      listItems.close();      
      } // else create new list ? 
   }
   
   public String getListName()
   {
      return this.listName;
   }
   
   public String getDefaultValue() {
      return defaultValue;
   }
   
   public int size() {
      return values.size();
   }
   
   public String getLabel(int index) {
      return labels.get(index);
   }
   
   public String getValue(int index) {
      return values.get(index);
   }
   
   public int getQuantifier(int index) {
      return quantifiers.get(index);
   }
   
   public String getDisplayField(int index) {
	      return displayFields.get(index);
	   }
   
   public String getDisplayName(int index) {
	      return displayNames.get(index);
	   }
   
   public boolean containsValue(String value) {
      return values.contains(value);
   }
}
