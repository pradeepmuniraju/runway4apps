package com.sok.runway.view.crm;

import com.sok.runway.crm.Product;
import com.sok.runway.view.FormFactory;
import com.sok.framework.SpecManager;
import com.sok.framework.DisplaySpec;

import javax.servlet.ServletRequest;
import java.sql.Connection;

public class ProductView extends Product implements ProfiledEntityView
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("ProductDisplay");
   
   public ProductView(Connection con)
   {
      super(con);
   }

   public ProductView(ServletRequest request)
   {
      super(request);
      
      request.setAttribute("product",this);
      }

   public ProductView(Connection con, String productID)
   {
      super(con, productID);
   }  
   
   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }
   
   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }
   
   public String getProfileFieldDisplay(String questionID) {
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildProfileDisplayValue(this, questionID);
   }
   
   public String getProfileFormInput(String questionID) {
      
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildProfileFormInput(this, questionID);
   }


   public String getFormInput(String field) {

      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildFormInput(this, field);
   }
}