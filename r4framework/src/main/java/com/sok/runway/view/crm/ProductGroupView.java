package com.sok.runway.view.crm;

import com.sok.runway.crm.ProductGroup;
import com.sok.runway.view.FormFactory;
import com.sok.runway.view.RunwayEntityView;
import com.sok.framework.SpecManager;
import com.sok.framework.DisplaySpec;

import javax.servlet.ServletRequest;
import java.sql.Connection;

public class ProductGroupView extends ProductGroup implements RunwayEntityView
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("ProductGroupDisplay");
   
   /** 
    * Instantiate without loading specific Product Group.
    * @param con
    */
   public ProductGroupView(Connection con)
   {
      super(con);
   }
   
   /** 
    * Instantiate and load ProductGroupID within HTTP Request parameter.
    * @param request
    */
   public ProductGroupView(ServletRequest request)
   {
      super(request);
      
      request.setAttribute("productgroup",this);
      }
   
   /** 
    * Instantiate and load specific ProductGroupID.
    * @param con
    * @param productGroupID
    */
   public ProductGroupView(Connection con, String productGroupID)
   {
      super(con, productGroupID);
   }  
   
   /** 
    * Check display spec whether the specified field is used. Returns false
    * only if the matching field has notused=true.
    * @param field
    */
   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }
   
   /**
    * Retrieves the label of the specified field from display spec. Returns empty
    * string if not found.
    * @param field
    */
   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }
   
   /**
    * Build and returns HTML input element for a given field as specified in FormFactory.java
    * @param field
    */
   public String getFormInput(String field) {

      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildFormInput(this, field);
   }
}