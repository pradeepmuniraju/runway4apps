package com.sok.runway.view.crm;

import com.sok.runway.*;
import com.sok.runway.crm.*;
import com.sok.runway.view.*;
import com.sok.runway.security.AuditEntity;
import com.sok.framework.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class DocumentView extends Document
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("DocumentDisplay");

   public DocumentView(Connection con)
   {
      super(con);
   }

   public DocumentView(ServletRequest request)
   {
      super(request);

      request.setAttribute("document",this);
   }

   public DocumentView(Connection con, String DocumentID)
   {
      super(con, DocumentID);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }

}