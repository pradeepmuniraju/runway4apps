package com.sok.runway.view.crm;

import com.sok.runway.crm.*;
import com.sok.runway.crm.profile.*;
import com.sok.runway.view.*;
import com.sok.runway.security.AuditEntity;
import com.sok.framework.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class SurveyView extends Survey
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("SurveyDisplay");

   public SurveyView(Connection con)
   {
      super(con);
   }

   public SurveyView(ServletRequest request)
   {
      super(request);

      request.setAttribute("surveys",this);
   }

   public SurveyView(Connection con, String ProfileID)
   {
      super(con, ProfileID);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }

}