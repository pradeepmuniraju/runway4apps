package com.sok.runway.view.crm.cms;

import com.sok.runway.TemplateURLs;
import com.sok.runway.crm.cms.FieldType;
import com.sok.runway.crm.cms.Page;
import com.sok.runway.crm.cms.PageContent;
import com.sok.service.cms.WebsiteService;
import com.sok.framework.DisplaySpec;
import com.sok.framework.InitServlet;
import com.sok.framework.SpecManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A PageView is a view of a single page or page version, consistent with Views in other places within Runway.
 * 
 * This can be used to render a particular version, if required.
 * @author mike
 *
 */
public class PageView extends Page
{
	protected DisplaySpec dspec = SpecManager.getDisplaySpec("GroupDisplay");
	protected TemplateURLs urllinks = new TemplateURLs();
	protected boolean editing = false, gallery = false;
	private static final Logger logger = LoggerFactory.getLogger(PageView.class);

	public PageView() { 
		super();
	}

	public PageView(Connection con)
	{
		super(con);
	}

	public PageView(HttpServletRequest request)
	{
		super(request);
		setRequest(request);
	}

	public PageView(Connection con, String pageID)
	{
		super(con, pageID);
	}

	private String displayVersionID = null;

	protected Map<String, PageContent> pageContent = null;
	public Map<String, PageContent> getPageContent() { 
		if(pageContent != null) {
			return pageContent;
		}
		return Collections.emptyMap();
	}

	public void setRequest(HttpServletRequest request) {
		request.setAttribute("pageView",this);	//this wont be appropriate if we're talking about a sub page.
		request.setAttribute("page", this); // there is existing code which uses this, tho it wont work for useBean
		checkEditing(request);
	}

	public TemplateURLs getLinks() {
		return urllinks;
	}

	public boolean hasGallery() {
		return this.gallery == true;
	}
	public boolean isEditing() {
		return this.editing == true;
	}
	public void setEditing(boolean editing) {
		this.editing = editing; 
	}


	public void checkEditing(HttpServletRequest request) {
		if("false".equals(request.getParameter("edit"))) {
			request.getSession().removeAttribute("editing");
		} else if(request.getParameter("edit") != null) {
			editing = true;
			request.getSession().setAttribute("editing", Boolean.TRUE);
		} else if(Boolean.TRUE == request.getSession().getAttribute("editing")) {
			editing = true;
		} 
		/* we will check for null when deciding which id to use */
		displayVersionID = StringUtils.trimToNull(request.getParameter("PageVersionID"));
	}

	public void renderField(String fieldName, FieldType fieldType, JspWriter out) throws IOException {
		renderField(fieldName, fieldType, out, editing);
	}

	public void renderField(String fieldName, FieldType fieldType, JspWriter out, boolean edit) throws IOException {
		if(edit) {
			//the editor will add an overlay to this element to enable clicks, or install the editor.
			out.print("<div style=\"display: inline-block; width: 100%; height: 100%;\" class=\"page-content\" data-content-type=\""); 
			out.print(fieldType.name());
			out.print("\" data-page-id=\"");
			out.print(getPageID());
			out.print("\" data-page-version-id=\"");
			out.print(getPageVersionID());
			out.print("\" data-content-field=\"");
			out.print(fieldName);
			out.print("\">");
		}
		if(fieldType == FieldType.HTMLField) {
			if(logger.isTraceEnabled()) logger.trace(getField(fieldName));
			out.print(urllinks.getHTML(getField(fieldName), getRecord()));
		} else { 
			PageContent pc = pageContent != null && pageContent.containsKey(fieldName) ? pageContent.get(fieldName) : null;
			if(pc != null) { 
				if(!fieldType.name().equals(pc.getContentType())) {
					out.print("The saved content for this field did not match the type specified ["); out.print(fieldName); out.print("]. It should be removed or updated.");
				} else if(fieldType == FieldType.Gallery) {
					logger.trace(pc.getContent());
					out.print(pc.getContent());
				} else { 
					logger.trace(pc.getContent());
					out.print(urllinks.getHTML(pc.getContent(), getRecord()));
				}
			} else {
				logger.debug("no field content found for fieldname=[{}], keysAvail=[{}]", fieldName, pageContent != null ? StringUtils.join(pageContent.keySet(), ",") : "[]");
			}
		}
		if(edit) {
			out.print("</div>");
		}
		if(FieldType.Gallery == fieldType) gallery = true; 
	}

	@Override
	public void postLoad() {
		super.postLoad();
		if(isLoaded() && getField("CMSID").length()>0) {
			if(urllinks == null) {
				logger.warn("urllinks was somehow null ? ");
				urllinks = new TemplateURLs();
			}
			gallery = false;
			urllinks.getCMSLinks(getField("CMSID"),InitServlet.getConfig().getServletContext());
			List<PageContent> pcl = WebsiteService.getInstance().getPageContents(getConnection(), null, null, displayVersionID != null ? displayVersionID : getPageID());
			if(!pcl.isEmpty()) { 
				if(pageContent == null) {
					pageContent = new HashMap<String, PageContent>(pcl.size());
				} else {
					pageContent.clear();
				}
				for(PageContent pc: pcl) {
					logger.trace("pageContent.put({},{})", pc.getContentField(), pc.getPageContentID());
					pageContent.put(pc.getContentField(), pc);
					if("Gallery".equals(pc.getContentType())) {
						gallery = true;
					}
				}
			} else{ 
				logger.warn("got no page content from websiteService");
				if(pageContent != null) { 
					pageContent.clear();
					pageContent = null;
				}
			}
		} else {
			logger.warn("PageView({}) was not loaded", getPrimaryKey());
		}
	}

	public boolean isFieldUsed(String field)
	{
		return(dspec.isItemUsed(field));
	}

	public String getFieldLabel(String field)
	{
		return(dspec.getItemLabel(field));
	}

	public String getFieldSource(String field)
	{
		return (dspec.getItemSource(field));
	}

	public String getFieldValidationType(String field, int num)
	{
		return(dspec.getItemValidationType(field, num));
	}

	public String getFieldValidationMessage(String field, int num)
	{
		return(dspec.getItemValidationMessage(field, num));
	}

}