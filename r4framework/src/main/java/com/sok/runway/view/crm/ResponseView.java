package com.sok.runway.view.crm;

import com.sok.runway.*;
import com.sok.runway.crm.*;
import com.sok.runway.view.*;
import com.sok.runway.security.AuditEntity;
import com.sok.framework.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class ResponseView extends Response
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("ResponseDisplay");

   public ResponseView(Connection con)
   {
      super(con);
   }

   public ResponseView(ServletRequest request)
   {
      super(request);

      request.setAttribute("response",this);
   }

   public ResponseView(Connection con, String ResponseID)
   {
      super(con, ResponseID);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }

}