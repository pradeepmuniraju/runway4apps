package com.sok.runway.view.crm;

import com.sok.runway.campaigns.*;
import com.sok.framework.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class CampaignView extends Campaign
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("CampaignDisplay");

   public CampaignView(Connection con)
   {
      super(con);
   }

   public CampaignView(ServletRequest request)
   {
      super(request);

      request.setAttribute("campaign",this);
   }

   public CampaignView(Connection con, String CampaignID)
   {
      super(con, CampaignID);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }

}