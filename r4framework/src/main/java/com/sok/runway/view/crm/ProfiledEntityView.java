package com.sok.runway.view.crm;

import com.sok.runway.view.*;

public interface ProfiledEntityView extends RunwayEntityView {
   
   public String getProfileInputType(String questionID);
   
   public String getProfileLabel(String questionID);
   
   public String getProfileValueListName(String questionID);
   
   public String getProfileField(String questionID);  
   
   public String getProfileField(String questionID, int displayType);  
   
   public String getProfileFieldDisplay(String questionID);  
   
   public int getProfileQuantifier(String questionID);
   
   public String getProfileFormInput(String questionID);
   
}