package com.sok.runway.view.crm;

import com.sok.runway.crm.Contact;
import com.sok.runway.crm.interfaces.Dialable;
import com.sok.runway.crm.factories.DialerFactory; 
import com.sok.runway.view.FormFactory;
import com.sok.runway.ItemLists;
import com.sok.runway.SingleItemList;

import com.sok.framework.ActionBean;
import com.sok.framework.DisplaySpec;
import com.sok.framework.InitServlet; 
import com.sok.framework.SpecManager;
import com.sok.runway.crm.Company;

import javax.servlet.ServletRequest;
import java.sql.Connection;
import java.util.Map;

/* deprecated */ 
import com.sok.runway.crm.ProfiledEntity;
import java.util.Collection;


public class ContactView extends Contact implements ProfiledEntityView, Dialable
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("ContactDisplay");
   
   public ContactView(Connection con)
   {
      super(con);
   }

   public ContactView(ServletRequest request)
   {
      super(request);
      
      request.setAttribute("contact",this);
   }

   public ContactView(Connection con, String contactID)
   {
      super(con, contactID);
   }
   
   /*
    *  Created By : Puja Shah
    *  Created Date : 30th July 2008
    *  Purpose : Copy Values from the CompanyView to the ContactView 
    */
   public ContactView(ServletRequest request,boolean copycompanyparameters)
   {
      super(request);
      copyCompanyValues(request);
      request.setAttribute("contact",this);
   }
   
   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }
   
   /*
    *  Created By : Puja Shah
    *  Created Date : 30th July 2008
    *  Purpose : To check whether the Field passed is to be displayed for B2B Client type or not
    */
   
   public boolean isB2BFieldUsed(String field)
   {
       boolean isFeildUsed = dspec.isItemUsed(field);
       if(isFeildUsed){
    	   isFeildUsed = "B2B".equals(InitServlet.getSystemParams().getProperty("ClientType"));
       }
	   return(isFeildUsed);
   }
   
   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }
   
   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }
   
   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }
   
   public boolean hasFieldValidation(String field)
   {
	   return (dspec.hasItemValidation(field));
   }
   
   public int getFieldValidationCount(String field)
   {
	   return (dspec.getItemValidationCount(field));
   }
   
   public String getProfileFieldDisplay(String questionID) {
      return FormFactory.getFormFactory().buildProfileDisplayValue(this, questionID);
   }
   
   public String getProfileFormInput(String questionID) {   
      return getProfileFormInput(questionID, null);
   }
   
   public String getProfileFormInput(String questionID, Map attributes) {
      return FormFactory.getFormFactory().buildProfileFormInput(this, questionID, attributes);
   }
   
   public String getFormInput(String field) {
      return FormFactory.getFormFactory().buildFormInput(this, field, null);
   }
   
   public String getFormInput(String field, Map attributes) {
      return FormFactory.getFormFactory().buildFormInput(this, field, attributes);
   }
   
   /**
    * @deprecated - shouldn't need to use this.
    */
   public Collection<String> getProfileCheckboxInputs(String QuestionID){
	   
	   String inputName = ProfiledEntity.PROFILE_PREFIX + QuestionID;
	   String inputValue = getProfileField(QuestionID);
	   String valueListName = getProfileValueListName(QuestionID);
	   
	   FormFactory ff = FormFactory.getFormFactory();
	   return ff.buildCheckboxInputs(inputName, inputValue, valueListName);
   }
   
   public String getRecordURL(int channel) {
	   return getRecordURL(null, channel); 
   }
   
   public String getRecordURL(int channel, String custom) {
	   return getRecordURL(null, channel, custom); 
   }
   
   public String getRecordURL(String field, int channel) {
	   return getRecordURL(field, channel, ActionBean._blank); 
   }
   
   public String getRecordURL(String field, int channel, String custom) {
	
		return DialerFactory.getRecordURL(this, field, channel, custom); 
	}
   
   public String getPhoneURL(String field, int channel){
	   return getPhoneURL(field, channel, null); 
   }
   
   public String getPhoneURL(String field, int channel,String custom){
	   return DialerFactory.getPhoneURL(this, field, channel, custom); 
   }
	
	public String getName() {
		StringBuilder name = new StringBuilder(getField("FirstName")).append("+").append(getField("LastName")); 
		return name.toString(); 
	}

	public String getDialURL(String field) {
		return DialerFactory.getDialURL(this, field); 
	}
	
	/*
	    *  Created By : Puja Shah
	    *  Created Date : 30th July 2008
	    *  Purpose : To get the Client Type System Parameter value.
	    */
	public String getClientType()
	{
	      return(InitServlet.getSystemParams().getProperty("ClientType"));
	}
	
	/*
	    *  Created By : Puja Shah
	    *  Created Date : 30th July 2008
	    *  Purpose : To get the Title of the Contact window.
	    */
	public String getTitle() {
		StringBuilder name = new StringBuilder(this.getField("FirstName")).append(" ").append(this.getField("LastName")); 
		return name.toString(); 
	}
	
	/*
    *  Created By : Puja Shah
    *  Created Date : 30th July 2008
    *  Purpose : Check whether the CTIEnabled system parameter is true. 
    *  			 If its value is true displays the link on the field which is passed as a parameter
    *  			 and call getDialURL method.
    */
	public String checkDialURL(String field){
		StringBuffer finalLabel = new StringBuffer();
		boolean ctiEnabled = "true".equals(InitServlet.getSystemParams().getProperty("CTIEnabled"));
			if(ctiEnabled && this.getField(field).length() > 0){
				finalLabel.append("<a href=\"");
				finalLabel.append(this.getDialURL(field));
				finalLabel.append("\">");
				finalLabel.append(this.getFieldLabel(field));
				finalLabel.append("</a>");
			}else{
				finalLabel.append(this.getFieldLabel(field));
			}
		return(finalLabel.toString());
	}
	
	/*
	    *  Created By : Puja Shah
	    *  Updated By : Dion Chapman
	    *  Created Date : 30th July 2008
	    *  Purpose : Check whether the CTIEnabled system parameter is true. 
	    *  			 If its value is true displays the link on the field which is passed as a parameter
	    *  			 and call getDialURL method using the imgTag as the link.
	    */
	public String checkDialURL(String field, String imgTag){
		StringBuffer finalLabel = new StringBuffer();
		boolean ctiEnabled = "true".equals(InitServlet.getSystemParams().getProperty("CTIEnabled"));
			if(ctiEnabled && this.getField(field).length() > 0){
				finalLabel.append("<a href=\"");
				finalLabel.append(this.getDialURL(field));
				finalLabel.append("\">");
				finalLabel.append(imgTag);
				finalLabel.append("</a>");
			}else{
				finalLabel.append("");
			}
		return(finalLabel.toString());
	}
	
	/*
	    *  Created By : Puja Shah
	    *  Updated By : Dion Chapman
	    *  Created Date : 30th July 2008
	    *  Purpose : Check whether the CTIEnabled system parameter is true. 
	    *  			 If its value is true displays the link on the field which is passed as a parameter
	    *  			 and call getDialURL method using the imgTag as the link.
	    */
	public String checkDialNumber(String field){
		StringBuffer finalLabel = new StringBuffer();
		boolean ctiEnabled = "true".equals(InitServlet.getSystemParams().getProperty("CTIEnabled"));
			if(ctiEnabled && this.getField(field).length() > 0){
				finalLabel.append("<a href=\"");
				finalLabel.append(this.getDialURL(field));
				finalLabel.append("\">");
				finalLabel.append(this.getField(field));
				finalLabel.append("</a>");
			}else{
				finalLabel.append(this.getField(field));
			}
		return(finalLabel.toString());
	}
	
	public String checkPhoneURL(String field,int channel,String custom){
		StringBuffer finalLabel = new StringBuffer();
		boolean ctiEnabled = "true".equals(InitServlet.getSystemParams().getProperty("CTIEnabled"));
			if(ctiEnabled && this.getField(field).length() > 0){
				finalLabel.append("<a href=\"");
				finalLabel.append(this.getPhoneURL(field,channel,custom));
				finalLabel.append("\">");
				finalLabel.append(this.getFieldLabel(field));
				finalLabel.append("</a>");
			}else{
				finalLabel.append(this.getFieldLabel(field));
			}
		return(finalLabel.toString());
	}
	
	public String checkPhoneURL(String field,int channel){
		return checkPhoneURL(field,channel,null);
	}
	
	private void copyCompanyValues(ServletRequest request){

		Company company = new Company(request);
		if(company.isLoaded())
		{
	       this.setField("Company", company.getField("Company"));
	       this.setField("Industry", company.getField("Industry"));
	       
		    ItemLists itemlists = new ItemLists();
		    itemlists.setConnection(this.getConnection());
		    SingleItemList types = itemlists.getList("ContactAddressType");
		    String type1 = "";
		    if (types.size() > 0) type1 = types.getValue(0);
		    String type2 = "";
		    if (types.size() > 1) type2 = types.getValue(1);

		    SingleItemList companytypes = itemlists.getList("CompanyAddressType");
		    String companytype1 = "";
		    if (companytypes.size() > 0) companytype1 = companytypes.getValue(0);
		    String companytype2 = "";
		    if (companytypes.size() > 1) companytype2 = companytypes.getValue(1);
		    

			if (type1.equals(companytype1) || (type1.equals("Work") && companytype1.equals("Office"))) {
	
            this.setField("Street", company.getField("Street"));
			this.setField("Street2", company.getField("Street2"));
			this.setField("City", company.getField("City"));
			this.setField("State", company.getField("State"));
			this.setField("Postcode", company.getField("Postcode"));
			this.setField("Country", company.getField("Country"));
	         this.setField("Phone", company.getField("Phone"));
	         this.setField("Fax", company.getField("Fax"));
			}	
         if (type2.equals(companytype2) || (type2.equals("Work") && companytype2.equals("Office"))) {
            
            this.setField("HomeStreet", company.getField("PostalStreet"));
            this.setField("HomeStreet2", company.getField("PostalStreet2"));
            this.setField("HomeCity", company.getField("PostalCity"));
            this.setField("HomeState", company.getField("PostalState"));
            this.setField("HomePostcode", company.getField("PostalPostcode"));
            this.setField("HomeCountry", company.getField("PostalCountry"));
	        this.setField("HomePhone", company.getField("Phone"));
            this.setField("Fax2", company.getField("Fax2"));
         }  
         if (type1.equals(companytype2) || (type1.equals("Work") && companytype2.equals("Office"))) {
            
            this.setField("Street", company.getField("PostalStreet"));
            this.setField("Street2", company.getField("PostalStreet2"));
            this.setField("City", company.getField("PostalCity"));
            this.setField("State", company.getField("PostalState"));
            this.setField("Postcode", company.getField("PostalPostcode"));
            this.setField("Country", company.getField("PostalCountry"));
	        this.setField("Phone", company.getField("Phone2"));
            this.setField("Fax", company.getField("Fax2"));
         }  
         if (type2.equals(companytype1) || (type2.equals("Work") && companytype1.equals("Office"))) {
            
            this.setField("HomeStreet", company.getField("Street"));
            this.setField("HomeStreet2", company.getField("Street2"));
            this.setField("HomeCity", company.getField("City"));
            this.setField("HomeState", company.getField("State"));
            this.setField("HomePostcode", company.getField("Postcode"));
            this.setField("HomeCountry", company.getField("Country"));
	        this.setField("HomePhone", company.getField("Phone2"));
            this.setField("Fax2", company.getField("Fax"));
         }         
		}
	}
}