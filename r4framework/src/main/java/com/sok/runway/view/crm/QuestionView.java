package com.sok.runway.view.crm;

import com.sok.runway.*;
import com.sok.runway.crm.*;
import com.sok.runway.crm.profile.*;
import com.sok.runway.view.*;
import com.sok.runway.security.AuditEntity;
import com.sok.framework.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class QuestionView extends Question
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("QuestionDisplay");

   public QuestionView(Connection con)
   {
      super(con);
   }

   public QuestionView(ServletRequest request)
   {
      super(request);

      request.setAttribute("question",this);
   }

   public QuestionView(Connection con, String QuestionID)
   {
      super(con, QuestionID);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }

}