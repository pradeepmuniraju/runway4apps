package com.sok.runway.view.crm;

import com.sok.runway.crm.*;
import com.sok.runway.crm.interfaces.Recordable;
import com.sok.runway.crm.factories.DialerFactory; 
import com.sok.runway.view.*;
import com.sok.framework.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class NoteView extends Note implements ProfiledEntityView, Recordable
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("NoteDisplay");

   public NoteView(Connection con)
   {
      super(con);
   }

   public NoteView(ServletRequest request)
   {
      super(request);

      request.setAttribute("note",this);
   }

   public NoteView(Connection con, String NoteID)
   {
      super(con, NoteID);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }
   
   public String getProfileFieldDisplay(String questionID) {
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildProfileDisplayValue(this, questionID);
   }

   public String getProfileFormInput(String questionID) {

      FormFactory ff = FormFactory.getFormFactory();

      return ff.buildProfileFormInput(this, questionID);
   }

   public String getFormInput(String field) {

      FormFactory ff = FormFactory.getFormFactory();

      return ff.buildFormInput(this, field);
   }
	
	public String getRecordURL(int channel) {
		return getRecordURL(channel, ActionBean._blank);
	}
	
	public String getRecordURL(int channel, String custom) {
		return DialerFactory.getRecordURL(this, null, channel, custom);
	}

}