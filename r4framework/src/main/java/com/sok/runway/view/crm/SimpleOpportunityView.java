package com.sok.runway.view.crm;

import java.sql.Connection;

import javax.servlet.ServletRequest;

import com.sok.framework.DisplaySpec;
import com.sok.framework.RowSetBean;
import com.sok.framework.SpecManager;
import com.sok.framework.TableData;
import com.sok.framework.generation.DatabaseException;
import com.sok.runway.crm.SimpleOpportunity;
import com.sok.runway.view.FormFactory;

public class SimpleOpportunityView extends SimpleOpportunity implements ProfiledEntityView
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("SimpleOpportunityDisplay");
   protected String modestr = null;

   protected boolean unlocked = false;
   protected boolean isselected = false;
    
   public SimpleOpportunityView(Connection con)
   {
      super(con);
   }

   public SimpleOpportunityView(ServletRequest request)
   {
      super(request);
      loadView();
      request.setAttribute("simpleopportunity",this);
   }
   
   public SimpleOpportunityView(ServletRequest request, String viewspec)
   {
      super(request, viewspec);
      loadView();
      request.setAttribute("simpleopportunity",this);
   }   

   public SimpleOpportunityView(Connection con, String opportunityID)
   {
      super(con, opportunityID);
      loadView();
   }

   protected void loadView()
   {
      if(super.isLoaded())
      {
         initMode();
      }
   }
   
   protected void initMode()
   {
      modestr = "details";
      
      if(getField("ProbabilityName") == null || getField("ProbabilityName").length() == 0)
      {
         if(getField("Probability")!=null)
         {
            bean.replaceData("ProbabilityName", getField("Probability")+"%");
         }
      }

      if(getParameter("-mode")!=null && getParameter("-mode").length()!=0)
      {
         modestr = getParameter("-mode");
      }
      
   }
   
   public String getMode()
   {
      return(modestr);
   }   
   
   public boolean isMode(String mode)
   {
      return(modestr.equals(mode));
   }
   
   public int isModeType(String mode)
   {
      if(modestr.indexOf(mode)>-1)
      {
         return(1);
      }
      return(0);
   }   
   
   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }
   
   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
     return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }
   
   public String getProfileFieldDisplay(String questionID) {
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildProfileDisplayValue(this, questionID);
   }
   
   public String getProfileFormInput(String questionID) {
      
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildProfileFormInput(this, questionID);
   }
   
   public String getFormInput(String field) {
      
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildFormInput(this, field);
   }
}
