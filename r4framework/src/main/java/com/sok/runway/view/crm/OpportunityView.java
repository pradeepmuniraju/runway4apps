package com.sok.runway.view.crm;

import com.sok.runway.*;
import com.sok.runway.crm.*;
import com.sok.runway.view.*;
import com.sok.runway.security.AuditEntity;
import com.sok.framework.*;
import com.sok.framework.generation.*;
import javax.servlet.ServletRequest;
import java.sql.*;

public class OpportunityView extends Opportunity implements ProfiledEntityView
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("OpportunityDisplay");
   protected String modestr = null;
   protected StringBuffer includepath = null;
   
   protected int currentstage = 0;
   protected int stagecount = 0;      
   protected int selectedstage = 0;   
   
   protected RowSetBean stages = new RowSetBean();         //TODO use genrow when framework issue is resolved
   protected String cstyle = null;

   protected boolean unlocked = false;
   protected boolean conditions = false;
   protected boolean isselected = false;
   protected boolean prevLocked = false;
   protected boolean hasCheckList = true;
   
   protected String sstagestyle = "background-color: #FFFFFF; border-width: 1px;";
   protected String stagestyle = "border-width: 0px 1px 1px 1px; border-style: solid; border-color: #FFFFFF; background-image: url(../images/tabui/opportunities/process_stage_bg.png); background-repeat: repeat-x;";
   
   protected String includepathcontext = "/crm/tabui/opportunities/includes/";
   
   public OpportunityView(Connection con)
   {
      super(con);
   }

   public OpportunityView(ServletRequest request)
   {
      super(request);
      loadView();
      request.setAttribute("opportunity",this);
   }
   
   public OpportunityView(ServletRequest request, String viewspec)
   {
      super(request, viewspec);
      loadView();
      request.setAttribute("opportunity",this);
   }   

   public OpportunityView(Connection con, String opportunityID)
   {
      super(con, opportunityID);
      loadView();
   }

   protected void loadView()
   {
      if(super.isLoaded())
      {
         initMode();
         initStages();
      }
   }
   
   protected void initMode()
   {
      modestr = (getParameter("DefaultTab").length() > 0)? getParameter("DefaultTab") : "details";
      
      if(getField("ProbabilityName") == null || getField("ProbabilityName").length() == 0)
      {
         if(getField("Probability")!=null)
         {
            bean.replaceData("ProbabilityName", getField("Probability")+"%");
         }
      }

      if(getParameter("-mode")!=null && getParameter("-mode").length()!=0)
      {
         modestr = getParameter("-mode");
      }
      else if(getParameter("ProcessStageID")!=null && getParameter("ProcessStageID").equals("Completed"))
      {
         setMode("view");
      }
      else if(getParameter("-stage")!=null && getParameter("-stage").length()==0)
      {
          setMode("view");
      }
      

   }
   
   protected void setSelectedStage()
   {
      stagecount = 0;  
      
      currentstage = getField("ProcessStageID").length()==0?1:1000;
      selectedstage = getParameter("-stage").length()!=0?Integer.parseInt(getParameter("-stage")):1000;        
      
      stages.setViewSpec("OpportunityStageStageView");
      stages.setConnection(getConnection());
      stages.setParameter("OpportunityID", getParameter("OpportunityID"));
      stages.sortBy("OpportunityStageProcessStage.SortNumber",0);
      //stages.doAction("search");
      stages.generateSQLStatement();
      stages.getResults();
      
      //System.out.println("setSelectedStage " + stages.getStatement());

      while(stages.getNext())
      {
         ++stagecount;
         
         if(currentstage == 1000)
         {
            if(getField("ProcessStageID").equals(stages.getData("ProcessStageID")))
            {
               currentstage = stagecount;
               if(selectedstage == 1000)
               {
                  selectedstage = currentstage;
                  setParameter("-stage", String.valueOf(selectedstage));
               }
            }
         }         
         
         if(stagecount == selectedstage)
         {
            setParameter("-selectedProcessStageID", stages.getString("ProcessStageID"));
            setParameter("-selectedOpportunityStageID", stages.getString("OpportunityStageID"));
            setParameter("-selectedStageID", stages.getString("StageID"));
         }    
      }
      stages.close();
      stages.clear();
      stagecount = 0;
   }
   
   protected void setNextStage()
   {
      
      if(getField("-selectedProcessStageID")!=null && getField("-selectedProcessStageID").length()!=0)
      {
         stages.setViewSpec("OpportunityStageStageView");
         stages.setConnection(getConnection());
         stages.setParameter("OpportunityStageProcessStage.ProcessStageStage.StageChildren.ParentStages.StageProcessStage.ProcessStageOpportunityStage.ProcessStageID", getField("-selectedProcessStageID"));
         stages.setParameter("OpportunityStageProcessStage.ProcessStageStage.StageChildren.ParentStages.StageProcessStage.ProcessStageOpportunityStage.OpportunityID", getField("OpportunityID"));
         stages.setParameter("OpportunityID",getParameter("OpportunityID"));
         stages.sortBy("OpportunityStageProcessStage.SortNumber",0);
         //stages.doAction("search");
         stages.generateSQLStatement();
         stages.getResults();
         //System.out.println("setNextStage " + stages.getStatement());
         
         if(stages.getError().length()!=0)
         {
            throw new DatabaseException(stages.getError());
         }
         if(stages.getNext())
         {
             setParameter("-nextProcessStageID", stages.getString("ProcessStageID"));
             setParameter("-nextStageID", stages.getString("StageID"));
            //out.print(stages.getString("ProcessStageID"));
         }
         stages.close();
         stages.clear();
      }
   }
   
   protected void initStages()
   {
      setSelectedStage();
      
      setNextStage();
      
      stagecount = 0;
      stages.setViewSpec("OpportunityStageStageView");
      stages.setConnection(getConnection());
      stages.setParameter("OpportunityID", getParameter("OpportunityID"));
      stages.sortBy("OpportunityStageProcessStage.SortNumber",0);
      //stages.doAction("search");
      stages.generateSQLStatement();
      stages.getResults();
      
      //System.out.println("initStage " + stages.getStatement());
      
      if(stages.getError().length()!=0)
      {
         throw new DatabaseException(stages.getError());
      }
   }

   public void setIncludePathContext(String p)
   {
      includepathcontext = p;
   }
   
   public void setSelectedStageStyle(String s)
   {
      sstagestyle = s;
   }
   
   public void setStageStyle(String s)
   {
      stagestyle = s;
   }
   
   public String getStageStyle()
   {
      return(cstyle);
   }
   
   public int getStageCount()
   {
      return(stagecount);
   }
   
   public int getSelectedStage()
   {
      return(selectedstage);
   }
   
   protected boolean isUnlocked(TableData stage)
   {
	  //System.out.println("isUnlocked " + stage.getString("StageID") + " " + stage.getString("Active") + " " + stage.getString("IncompleteParents"));
      return(stage.getString("Active").equals("Yes") || stage.getString("IncompleteParents").equals("0"));
   }
   
   protected boolean isPrevLocked(TableData stage)
   {
	  //System.out.println("isPrevLocked " + stage.getString("StageID") + " " + stage.getString("Active") + " " + stage.getString("CheckList") + " " + stage.getString("Parents") + " " + stage.getString("IncompleteParents"));
      return(!stage.getString("Active").equals("Yes") && stage.getString("CheckList").length() > 0 && !stage.getString("Parents").equals("0"));
   }
   
   private boolean checkList(RowSetBean stage) {
	   return (stage.getString("CheckList").length() > 0 && !isCheckListDone(stage)) || hasCheckList;
   }
   
   private boolean isCheckListDone(RowSetBean stage) {
	   if (stage.getString("SurveyID").length() > 0 && stage.getString("CheckList").length() > 0) {
		   GenRow row = new GenRow();
		   row.setViewSpec("SurveyQuestionOpportunityStageAnswerView");
		   row.setConnection(getConnection());
		   row.setParameter("-select0", "COUNT(*) AS Count");
		   row.setParameter("OpportunityStageID", stage.getString("OpportunityStageID"));
		   row.setParameter("SurveyID", stage.getString("SurveyID"));
		   row.setParameter("Answer", "!Yes");
		   row.doAction("selectfirst");
		   
		   return "0".equals(row.getString("Count"));
	   
	   }
	   return true;
   }
   
   protected boolean isConditions(TableData stage)
   {
      return(!"0".equals(stage.getString("IncompleteConditions")));
   }

   public boolean getNextStage()
   {
      if(stages.getNext())
      {
         ++stagecount;
                    
         if(stagecount == selectedstage)
         {
            isselected = true;
            cstyle = sstagestyle;

         }
         else
         {
            isselected = false;
            cstyle = stagestyle;
         }     
         
         if (stages.getString("Parents").equals("0")) hasCheckList = false;
         
         unlocked = !hasCheckList && isUnlocked(stages) && !("Y".equals(stages.getString("ConditionsBlock")) && conditions);
         
         if(getParameter("-nextProcessStageID").length()==0 && unlocked && stages.getString("CompletionDate").length()==0 && selectedstage<stagecount)
         {
            setParameter("-nextProcessStageID", stages.getString("ProcessStageID"));
            setParameter("-nextStageID", stages.getString("StageID"));
         }
         
         if (!conditions) conditions = isConditions(stages);
         
         //prevLocked = hasCheckList;
         hasCheckList = checkList(stages);
         
         return(true);
      }
      return(false);
   }
    

   public boolean isCurrentStageNext()
   {
      return(getParameter("-nextProcessStageID").equals(getCurrentStageField("ProcessStageID")));
   }
   
   public boolean isCurrentStageSelected()
   {
      return(isselected);
   }   
   
   public boolean isCurrentStageLocked()
   {
      return(!unlocked);
   }
   
   public String getCurrentStageField(String name)
   {
      return(stages.getData(name));
   }
   
   public void closeStages()
   {
      stages.close();
      stages = null;
      conditions = false;
   }
   
   public void setMode(String mode)
   {
      modestr = mode;
      setParameter("-mode", modestr);
   }      
   
   public String getMode()
   {
      return(modestr);
   }   
   
   public boolean isMode(String mode)
   {
      return(modestr.equals(mode));
   }
   
   public int isModeType(String mode)
   {
      if(modestr.indexOf(mode)>-1)
      {
         return(1);
      }
      return(0);
   }   
   
   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }
   
   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }
   
   public String getIncludePath()
   {
      includepath = new StringBuffer();
      includepath.append(includepathcontext)
      .append(modestr)
      .append(".jsp");
      return(includepath.toString());
   }
   
   public String getProfileFieldDisplay(String questionID) {
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildProfileDisplayValue(this, questionID);
   }

   
   public String getProfileFormInput(String questionID) {
      
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildProfileFormInput(this, questionID);
   }
   
   public String getFormInput(String field) {
      
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildFormInput(this, field);
   }
}