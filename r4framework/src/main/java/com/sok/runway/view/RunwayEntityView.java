package com.sok.runway.view;

public interface RunwayEntityView {
   
   public String getTableName();  
   
   public String getField(String field);  
   
   public String getFormInput(String field);
   
   /* Display Spec methods */
   public boolean isFieldUsed(String field);
   
   public String getFieldLabel(String field);
   
   public String getFieldSource(String field);

   public String getFieldValidationType(String field, int num);

   public String getFieldValidationMessage(String field, int num);
}