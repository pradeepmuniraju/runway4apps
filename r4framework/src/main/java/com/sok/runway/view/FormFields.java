package com.sok.runway.view;

public interface FormFields {
     
   
	public static final String FIELD_TEXT = "Text";
	public static final String FIELD_TEXT_TINY = "Tiny";
	public static final String FIELD_TEXT_SHORT = "Short";
	public static final String FIELD_TEXT_MEDIUM = "Medium";
	public static final String FIELD_TEXT_LONG = "Long";
	public static final String FIELD_TEXTAREA = "Multi-line";
	
	public static final String FIELD_SELECT = "Drop Down";
	public static final String FIELD_LIST = "List";
	public static final String FIELD_RADIO = "Radio Button";
	public static final String FIELD_CHECKBOX = "Checkbox";
	
	public static final String FIELD_DATE = "Date";
	public static final String FIELD_NUMBER = "Number";
	public static final String FIELD_LARGE_NUMBER = "Large Number";
	public static final String FIELD_PERCENTAGE = "Percentage";
	public static final String FIELD_CURRENCY = "Currency";
	public static final String FIELD_YESNO = "YesNo";
	public static final String FIELD_HIDDEN = "Hidden";
}