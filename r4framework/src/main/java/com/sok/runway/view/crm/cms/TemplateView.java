package com.sok.runway.view.crm.cms;

import com.sok.runway.crm.cms.*;
import com.sok.runway.view.*;
import com.sok.framework.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class TemplateView extends Template
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("GroupDisplay");

   public TemplateView(Connection con)
   {
      super(con);
   }

   public TemplateView(ServletRequest request)
   {
      super(request);

      request.setAttribute("template",this);
   }

   public TemplateView(Connection con, String templateID)
   {
      super(con, templateID);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }

}