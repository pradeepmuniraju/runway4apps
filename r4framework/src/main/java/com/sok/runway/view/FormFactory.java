package com.sok.runway.view;

import com.sok.runway.*;
import com.sok.runway.crm.*;
import com.sok.runway.view.crm.*;
import com.sok.framework.*;
import java.util.*;

public class FormFactory implements FormFields {
   
   private static FormFactory formFactory = null;
   
   public static FormFactory getFormFactory() {
      if (formFactory == null) {
         loadFactory();
      }
      return formFactory;
      
   }
   
   public static void setFormFactory(FormFactory formFactory) {
      FormFactory.formFactory = formFactory;
   }
   
   private static synchronized void loadFactory() {
            
      if (formFactory == null) {      
         formFactory = new FormFactory();
      }
   }
   
   /* These are private so we can implement them better later */
   private HashMap<String, HashMap> entityFieldTypes = null;
   private HashMap<String, HashMap> entityValueLists = null;
   
   public FormFactory() {
      FormFactory.formFactory = this;
      
      entityFieldTypes = new HashMap<String, HashMap>();
      entityValueLists = new HashMap<String, HashMap>();

      //Contact Field Types
      HashMap contactFieldTypes = new HashMap();
      contactFieldTypes.put("Title", FIELD_SELECT);
      contactFieldTypes.put("FirstName", FIELD_TEXT_MEDIUM);
      contactFieldTypes.put("LastName", FIELD_TEXT_MEDIUM);
      contactFieldTypes.put("Street", FIELD_TEXT_LONG);
      contactFieldTypes.put("Street2", FIELD_TEXT_LONG);
      contactFieldTypes.put("City", FIELD_TEXT_MEDIUM);
      contactFieldTypes.put("State", FIELD_SELECT);
      contactFieldTypes.put("Postcode", FIELD_TEXT_MEDIUM);
      contactFieldTypes.put("Country", FIELD_SELECT);
      contactFieldTypes.put("HomeStreet", FIELD_TEXT_LONG);
      contactFieldTypes.put("HomeStreet2", FIELD_TEXT_LONG);
      contactFieldTypes.put("HomeCity", FIELD_TEXT_MEDIUM);
      contactFieldTypes.put("HomeState", FIELD_SELECT);
      contactFieldTypes.put("HomePostcode", FIELD_TEXT_MEDIUM);
      contactFieldTypes.put("HomeCountry", FIELD_SELECT);
      contactFieldTypes.put("Phone", FIELD_TEXT_MEDIUM);
      contactFieldTypes.put("HomePhone", FIELD_TEXT_MEDIUM);
      contactFieldTypes.put("Mobile", FIELD_TEXT_MEDIUM);
      contactFieldTypes.put("Email", FIELD_TEXT_LONG);
      contactFieldTypes.put("FollowUpDate", FIELD_DATE);

      // Contact Value Lists
      HashMap contactValueLists = new HashMap();
      contactValueLists.put("Title", "Title");
      contactValueLists.put("Source", "Source");
      contactValueLists.put("State", "State");
      contactValueLists.put("HomeState", "State");
      contactValueLists.put("Country", "Country");
      contactValueLists.put("HomeCountry", "Country");

      entityFieldTypes.put("Contacts" ,contactFieldTypes);
      entityValueLists.put("Contacts" ,contactValueLists);
      
      // Product Field Types
      HashMap productFieldTypes = new HashMap();
      productFieldTypes.put("Active", FIELD_CHECKBOX);
      entityFieldTypes.put("Products" ,productFieldTypes);
      
      // Product Group Field Types
      HashMap productGroupFieldTypes = new HashMap();
      productGroupFieldTypes.put("GroupName", FIELD_TEXT_MEDIUM);
      productGroupFieldTypes.put("Description", FIELD_TEXT_LONG);
      productGroupFieldTypes.put("SortOrder", FIELD_TEXT_TINY);
      productGroupFieldTypes.put("Active", FIELD_CHECKBOX);
      entityFieldTypes.put("ProductGroups" ,productGroupFieldTypes);
   }
   
   /* These are private so we can implement them better later */
   private String getFieldType(String table, String field) {
      String fieldType = null;
      if (table != null && field != null) {
         HashMap tableMap = entityFieldTypes.get(table);
         if (tableMap != null) {
            fieldType = (String)tableMap.get(field);
         }
      }
      if (fieldType == null) {
         return FIELD_TEXT;
      }
      else {
         return fieldType;
      }
   }
   /* These are private so we can implement them better later */
   private String getValueList(String table, String field) {
      String valuelist = null;
      if (table != null && field != null) {
         HashMap tableMap = entityValueLists.get(table);
         if (tableMap != null) {
            valuelist = (String)tableMap.get(field);
         }
      }
      return valuelist;
   }
   
   public String buildFormInput(RunwayEntityView runwayEntity, String field) { 
      
      String type = getFieldType(runwayEntity.getTableName(), field);
      String value = runwayEntity.getField(field);
      String valueListName = getValueList(runwayEntity.getTableName(), field);
       
      return buildInput(field, value, type, valueListName);
   }
   
   public String buildFormInput(RunwayEntityView runwayEntity, String field, Map attributes) { 
      
      String type = getFieldType(runwayEntity.getTableName(), field);
      String value = runwayEntity.getField(field);
      String valueListName = getValueList(runwayEntity.getTableName(), field);
       
      return buildInput(field, value, type, valueListName, attributes);
   }
   
   private String buildInput(String inputName, String value, String type, String valueListName) {
   	return buildInput(inputName, value, type, valueListName, null);
   }
   
	/**
	 *    Builds appropriate form input based on the type.
	 *
	 *    @type type of input to be produced
	 *    @returns String inputs
	 */
   private String buildInput(String inputName, String value, String type, String valueListName, Map attributes) {  
      
      if (FIELD_TEXT_TINY.equals(type)
            || FIELD_NUMBER.equals(type)
            || FIELD_CURRENCY.equals(type)) {
         return buildTextInput(inputName, value, 5, attributes).toString();
      }
      else if (FIELD_PERCENTAGE.equals(type)) {
         return buildTextInput(inputName, value, 5, attributes).append(ActionBean._percent).toString();
      }
      else if (FIELD_TEXT_SHORT.equals(type)) {
         return buildTextInput(inputName, value, 10, attributes).toString();
      }
      else if (FIELD_TEXT.equals(type)) {
         return buildTextInput(inputName, value, -1, attributes).toString();
      }
      else if (FIELD_TEXT_MEDIUM.equals(type)) {
         return buildTextInput(inputName, value, 20, attributes).toString();
      }
      else if (FIELD_TEXT_LONG.equals(type) || FIELD_LARGE_NUMBER.equals(type)) {
         return buildTextInput(inputName, value, 30, attributes).toString();
      }
      else if (FIELD_TEXTAREA.equals(type)) {
         return buildTextAreaInput(inputName, value, attributes).toString();
      }
      else if (FIELD_SELECT.equals(type)) {
         return buildSelectInput(inputName, value, valueListName, attributes).toString();
      }
      else if (FIELD_LIST.equals(type)) {
         return buildMulitSelectInput(inputName, value, valueListName, attributes).toString();
      }
      else if (FIELD_RADIO.equals(type)) {
         
         Collection<String> inputs =  buildRadioInputs(inputName, value, valueListName, attributes);
         
         return buildInputTable(inputs, 10);
      }
      else if (FIELD_CHECKBOX.equals(type)) {
         
         if (valueListName == null || valueListName.length() == 0) {
            return buildSingleCheckboxInput(inputName, value, attributes).toString();
         }
         else {
            Collection<String> inputs =  buildCheckboxInputs(inputName, value, valueListName, attributes);
            
            return buildInputTable(inputs, 10);
         }
      }
      else if(FIELD_DATE.equals(type)){
    	  return buildTextInput(inputName, value, 10, attributes).toString();
      }
      else if(FIELD_YESNO.equals(type)){
    	  return buildYesNoInput(inputName, value, attributes).toString();
      }
      else {
         return ActionBean._blank;
      }
   }
   
	/**
	 *    Builds appropriate question representation based on the type of question.
	 *
	 *    @returns String formatted value
	 */
   public String buildProfileDisplayValue(ProfiledEntityView profiledEntity, String questionID) {
      
      String type = profiledEntity.getProfileInputType(questionID);
      
      if (FIELD_NUMBER.equals(type)
            || FIELD_LARGE_NUMBER.equals(type)) {
         return profiledEntity.getProfileField(questionID, ProfiledEntity.FIELD_TYPE_NUMBER);
      }
      else if (FIELD_CURRENCY.equals(type)) {
         return profiledEntity.getProfileField(questionID, ProfiledEntity.FIELD_TYPE_CURRENCY);
      }
      else if (FIELD_PERCENTAGE.equals(type)) {
         return profiledEntity.getProfileField(questionID, ProfiledEntity.FIELD_TYPE_STRING) + ActionBean._percent;
      }
      else if(FIELD_DATE.equals(type)){
    	  return profiledEntity.getProfileField(questionID, ProfiledEntity.FIELD_TYPE_DATE);
      }
      else if (FIELD_TEXT_TINY.equals(type)
               || FIELD_TEXT_SHORT.equals(type)
               || FIELD_TEXT.equals(type)
               || FIELD_TEXT_MEDIUM.equals(type)
               || FIELD_TEXT_LONG.equals(type)
               || FIELD_YESNO.equals(type)
               || FIELD_SELECT.equals(type)
               || FIELD_RADIO.equals(type)) {
         return profiledEntity.getProfileField(questionID, ProfiledEntity.FIELD_TYPE_STRING);
      }
      else if (FIELD_TEXTAREA.equals(type)) {
         return profiledEntity.getProfileField(questionID, ProfiledEntity.FIELD_TYPE_STRING).replaceAll("\\r", "<br/>");
      }
      else if (FIELD_LIST.equals(type)|| FIELD_CHECKBOX.equals(type)) {
                  
         String[] splitValues = profiledEntity.getProfileField(questionID, ProfiledEntity.FIELD_TYPE_STRING).split("\\+");
         
         Vector<String> values = new Vector<String>(splitValues.length);
          for(int i=0; i<splitValues.length; i++) {
        	  values.add(i, splitValues[i]);
          }
         return buildInputTable(values, 10);
      }
      else {
         return ActionBean._blank;
      }
   }
   
   /**
	 *    Builds appropriate form input based on the type of question
	 *
	 *    @returns String inputs
	 */
   public String buildProfileFormInput(ProfiledEntityView profiledEntity, String questionID) {
   	
      return buildProfileFormInput(profiledEntity, questionID, null);
   }
   
   /**
	 *    Builds appropriate form input based on the type of question, with custom profile prefix (for older survey forms)
	 *    and additional attributes
	 *
	 *    @returns String inputs
	 */
   public String buildProfileFormInput(ProfiledEntityView profiledEntity, String questionID, Map attributes) {
      
      String type = profiledEntity.getProfileInputType(questionID);
      String inputName = ProfiledEntity.PROFILE_PREFIX + questionID;
      String value = profiledEntity.getProfileField(questionID);
      String valueListName = profiledEntity.getProfileValueListName(questionID);
      
      return buildInput(inputName, value, type, valueListName, attributes);
   }
   
	/**
	 *    Builds a &lt;table&gt; containing the supplied inputs .
	 *
	 *    @colHeight number of inputs per column
	 *    @returns StringBuffer table of inputs
	 */
   public String buildInputTable(Collection<String> inputs, int colHeight) {
      StringBuffer table  = new StringBuffer();
         
      table.append("<table>\r\n<tr>");
      
      boolean first = true;
      int counter = 0;
      for ( String input : inputs) {
         if (counter == 0) {
            if (!first) {
               table.append("\r\n</td>");
            }
            first = false;
            table.append("\r\n<td>\r\n");
         }
         counter++;
         table.append(input);
         table.append("<br/>\r\n");
         if (counter == colHeight) {
            counter = 0;
         }
      }
      if (!first) {
         table.append("\r\n</td>");
      }
      table.append("\r\n</tr>\r\n</table>");
      
      return table.toString();
   }
   
	/**
	 *    Builds a &lt;textarea&gt; input.
	 *
	 *    @returns StringBuffer input
	 */
   public StringBuffer buildTextAreaInput(String inputName, String inputValue, Map attributes) {
      StringBuffer input = new StringBuffer();
      
      input.append("<textarea name=\"");
      input.append(inputName);
      input.append("\" rows=\"5\"");
      if(attributes != null)
      {
      	Iterator itr = attributes.entrySet().iterator();
      	while (itr.hasNext()) 
      	{
      		Map.Entry pairs = (Map.Entry)itr.next();
      		input.append(" " + pairs.getKey() + "=\"" + pairs.getValue() + "\"");
      	}
      }
      input.append(" >");
      input.append(StringUtil.encodeHTML(inputValue));
      input.append("</textarea>");      
      
      return input;
   }
    
	/**
	 *    Builds a &lt;input type="text" &gt; input no size.
	 *
	 *    @returns StringBuffer input
	 */
   public StringBuffer buildTextInput(String inputName, String inputValue) {
      return buildTextInput(inputName, inputValue, -1, null);
   }
   
   public StringBuffer buildTextInput(String inputName, String inputValue, int size) {
      return buildTextInput(inputName, inputValue, size, null);
   }
      
	/**
	 *    Builds a &lt;input type="text" size="${size}" &gt; input specified size.
	 *
	 *    @returns StringBuffer input
	 */
   public StringBuffer buildTextInput(String inputName, String inputValue, int size, Map attributes) {
      StringBuffer input = new StringBuffer();
      
      input.append("<input type=\"text\" name=\"");
      input.append(inputName);
      input.append("\" value=\"");
      input.append(StringUtil.toHTML(inputValue));
      input.append("\" ");
      if (size != -1) {
         input.append("size=\"");
         input.append(size);
         input.append("\"");
      }
      if(attributes != null)
      {
      	Iterator itr = attributes.entrySet().iterator();
      	while (itr.hasNext()) 
      	{
      		Map.Entry pairs = (Map.Entry)itr.next();
      		input.append(" " + pairs.getKey() + "=\"" + pairs.getValue() + "\"");
      	}
      }
      input.append("/>");
      
      return input;
   }
   
   public StringBuffer buildMulitSelectInput(String inputName, String inputValue, String valueListName, Map attributes) {
      return buildSelectInput(inputName, inputValue, valueListName, false, 4, attributes);
   }
   
   public StringBuffer buildSelectInput(String inputName, String inputValue, String valueListName) {
      return buildSelectInput(inputName, inputValue, valueListName, false, -1, null);
   }
   
   public StringBuffer buildSelectInput(String inputName, String inputValue, String valueListName, Map attributes) {
      return buildSelectInput(inputName, inputValue, valueListName, false, -1, attributes);
   }
   
   public StringBuffer buildSearchSelectInput(String inputName, String inputValue, String valueListName) {
      return buildSelectInput(inputName, inputValue, valueListName, false, -1, null);
   }
      
	/**
	 *    Builds a &lt;select&gt; input with options defined by valueListName.
	 *
	 *    @returns StringBuffer input
	 */
   private StringBuffer buildSelectInput(String inputName, String inputValue, String valueListName, boolean useWildcards, int multiSize, Map attributes) {
      StringBuffer input = new StringBuffer();
      
      input.append("<select name=\"");
      input.append(inputName);
      input.append("\"");
      if (multiSize > 1) {
         input.append(" size=\"");
         input.append(multiSize);
         input.append("\" multiple=\"true\"");
      }
      if(attributes != null)
      {
      	Iterator itr = attributes.entrySet().iterator();
      	while (itr.hasNext()) 
      	{
      		Map.Entry pairs = (Map.Entry)itr.next();
      		input.append(" " + pairs.getKey() + "=\"" + pairs.getValue() + "\"");
      	}
      }
      input.append(">\r\n");
      input.append("<option value=\"\">&nbsp;</option>\r\n");
      input.append("");
      
      ValueList list = ValueList.getValueList(valueListName);
      
      MultiField mf = new MultiField(inputValue);
      boolean optionSelected = false;
		for (int i=0; i<list.size(); i++) {
		   
		   String label = list.getLabel(i);
		   String value = list.getValue(i);
			int quantifier = list.getQuantifier(i);
			
			if (useWildcards) {
			   value = ActionBean._percent + value + ActionBean._percent;
			   optionSelected = mf.contains(value);
			}
			else if (quantifier > 0) {
			   optionSelected = mf.contains(value);
			   value = value + ProfiledEntity.QUANTIFIER_SEPERATOR + quantifier;
   		}
   		else {
			   optionSelected = mf.contains(value);
   		}
			
			input.append("<option value=\"");
			input.append(StringUtil.toHTML(value));
			input.append("\" ");
			
			if(optionSelected) {
				input.append("selected=\"selected\" ");
			}
			input.append(">");
			input.append(StringUtil.toHTML(label));
			input.append("</option>\r\n");
		}
		if (!useWildcards) {
   	   for (int j=0; j < mf.length(); j++) {
   	      String selectedValue = mf.get(j);
   	      
   	      if (selectedValue.length() > 0 && !list.containsValue(selectedValue)) {
   	         
      			input.append("<option value=\"");
      			input.append(StringUtil.toHTML(selectedValue));
      			/*if (quantifier > 0) {
      			   
      			   input.append(ProfiledEntity.QUANTIFIER_SEPERATOR);
      			   input.append("0");
      			}*/
   				input.append("\" selected=\"selected\" >");
   				input.append(StringUtil.toHTML(selectedValue));
   				input.append("</option>\r\n");
   	      }
   	   }
		}
		
      input.append("</select>");
      return input;
   }
   
	/**
	 *    Builds a Yes No input radio buttons.
	 *
	 *    @returns StringBuffer input
	 */
   public StringBuffer buildYesNoInput(String inputName, String value, Map attributes) {
      StringBuffer input = new StringBuffer();
      boolean checked = false;
      
      input.append("<input type=\"radio\" name=\"");
      input.append(inputName);
      input.append("\" value=\"Yes\" ");
      
      if (value != null && value.length() > 0) {
         checked = value.charAt(0) == 'Y' || value.charAt(0) == 'y';
      }
      if (checked) {
         input.append("checked=\"checked\" ");
      }
      if(attributes != null)
      {
      	Iterator itr = attributes.entrySet().iterator();
      	while (itr.hasNext()) 
      	{
      		Map.Entry pairs = (Map.Entry)itr.next();
      		input.append(" " + pairs.getKey() + "=\"" + pairs.getValue() + "\"");
      	}
      }
      input.append(" />&nbsp;Yes");
      
      input.append("&nbsp;<input type=\"radio\" name=\"");
      input.append(inputName);
      input.append("\" value=\"No\" ");
      
      if (value != null && value.length() > 0) {
         checked = value.charAt(0) == 'N' || value.charAt(0) == 'n';
      }
      if (checked) {
         input.append("checked=\"checked\" ");
      }
      if(attributes != null)
      {
      	Iterator itr = attributes.entrySet().iterator();
      	while (itr.hasNext()) 
      	{
      		Map.Entry pairs = (Map.Entry)itr.next();
      		input.append(" " + pairs.getKey() + "=\"" + pairs.getValue() + "\"");
      	}
      }
      input.append(" />&nbsp;No");
      
      return input;
   }
   
	/**
	 *    Builds a Collection of radio button inputs with labels.
	 *
	 *    @returns Collection of StringBuffer inputs
	 */
	public Collection<String> buildRadioInputs(String inputName, String inputValue, String valueListName, Map attributes) {//, boolean search, boolean includeQt, int printAmount, int startAt) {
	   return buildInputs("radio", inputName, inputValue, valueListName, attributes);
	}
   
	/**
	 *    Builds a Collection of checkbox inputs with labels and additional attributes
	 *
	 *    @returns Collection of StringBuffer inputs
	 */
	public Collection<String> buildCheckboxInputs(String inputName, String inputValue, String valueListName, Map attributes) {//, boolean search, boolean includeQt, int printAmount, int startAt) {
	   return buildInputs("checkbox", inputName, inputValue, valueListName, attributes);
	}
	
	/**
	 *    Builds a Collection of checkbox inputs with labels without additional attributes.
	 *
	 *    @returns Collection of StringBuffer inputs
	 */
	public Collection<String> buildCheckboxInputs(String inputName, String inputValue, String valueListName) {//, boolean search, boolean includeQt, int printAmount, int startAt) {
	   return buildInputs("checkbox", inputName, inputValue, valueListName, null);
	}
	
	public StringBuffer buildSingleCheckboxInput(String inputName, String value, Map attributes) {
	   StringBuffer input = new StringBuffer();
	         
	   input.append("<input type=\"checkbox\" name=\"-");
	   input.append(inputName);
      input.append("\" value=\"\" ");
      
      boolean checked = false;
      if (value != null && value.length() > 0) {
         checked = value.charAt(0) == 'Y' || value.charAt(0) == 'y';
      }
      if (checked) {
         input.append("checked=\"checked\" ");
      }
	   String id = attributes == null ? null : (String)attributes.get("id");
	   if (id == null) {
      	id = KeyMaker.generate();
      	if(attributes == null) attributes = new HashMap();
      	attributes.put("id", id);
      }
      input.append("onClick=\"document.getElementById('");
      input.append(id);
      input.append("').value = (this.checked?'Y':'N');\"");
      if(attributes != null)
      {
      	Iterator itr = attributes.entrySet().iterator();
      	while (itr.hasNext()) 
      	{
      		Map.Entry pairs = (Map.Entry)itr.next();
      		input.append(" " + pairs.getKey() + "=\"");
      		if(((String)pairs.getKey()).equals("id"))
      			input.append("-");
      		input.append(pairs.getValue() + "\"");
      	}
      }
      input.append(" />");

      input.append("<input type=\"hidden\" name=\"");
      input.append(inputName);
      input.append("\" value=\"");
      input.append(checked?"Y":"N");
      input.append("\"");
      input.append(" id=\"" + id + "\"");
      input.append(" />");
      
      return input;
	}
	
	/**
	 *    Builds a Collection of inputs with labels.
	 *
	 *    @type radio or checkbox
	 *    @returns Collection of StringBuffer inputs
	 */
	private Collection<String> buildInputs(String type, String inputName, String inputValue, String valueListName, Map attributes) {//, boolean search, boolean includeQt, int printAmount, int startAt) {
	
	   ValueList list = ValueList.getValueList(valueListName);
	   
	   ArrayList<String> inputs = new ArrayList<String>(list.size());
	   
	   StringBuffer input = null;
	   
	   MultiField mf = new MultiField(inputValue);
	   
		for (int i=0; i<list.size(); i++) {
		   input = new StringBuffer();
		   
		   String label = list.getLabel(i);
		   String value = list.getValue(i);
         int quantifier = list.getQuantifier(i);
         if(i == 0) {
            input.append("<input type=\"hidden\" name=\"");
            input.append(inputName);
			   input.append("\" value=\"\" /> ");
         }
		   input.append("<input type=\"");
         input.append(type);
         input.append("\" name=\"");
         input.append(inputName);
         input.append("\" value=\"");
         input.append(StringUtil.toHTML(value));
         input.append("\" ");
         if (mf.contains(value)) {
            input.append("checked=\"checked\" ");
         }
         if(attributes != null) {
         	Iterator itr = attributes.entrySet().iterator();
         	while (itr.hasNext()) {
         		Map.Entry pairs = (Map.Entry)itr.next();
         		input.append(" " + pairs.getKey() + "=\"" + pairs.getValue() + "\"");
         	}
         }
         input.append("/>");
         input.append("&nbsp;");
         input.append(label);
         inputs.add(input.toString());
		}
	   for (int j=0; j < mf.length(); j++) {
	      String selectedValue = mf.get(j);
	      
	      if (selectedValue.length() > 0 && !list.containsValue(selectedValue)) {
	         
            input.append(selectedValue);
            input.append("&nbsp;");
            input.append("<input type=\"");
            input.append(type);
            input.append("\" name=\"");
            input.append(inputName);
            input.append("\" value=\"");
            input.append(StringUtil.toHTML(selectedValue));
            input.append("\" ");
				input.append(" checked=\"checked\" >");
				if(attributes != null) {
	         	Iterator itr = attributes.entrySet().iterator();
	         	while (itr.hasNext()) {
	         		Map.Entry pairs = (Map.Entry)itr.next();
	         		input.append(" " + pairs.getKey() + "=\"" + pairs.getValue() + "\"");
	         	}
	         }
            input.append("/>");
            inputs.add(input.toString());
	      }
	   }
	   
		return inputs;
	}
   
   
}