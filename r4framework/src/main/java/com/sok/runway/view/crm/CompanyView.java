package com.sok.runway.view.crm;

import com.sok.runway.crm.Company;
import com.sok.runway.crm.factories.DialerFactory;
import com.sok.runway.crm.interfaces.Dialable;
import com.sok.runway.view.FormFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.DisplaySpec;
import com.sok.framework.InitServlet; 
import com.sok.framework.SpecManager;
import com.sok.framework.StringUtil;

import javax.servlet.ServletRequest;
import java.sql.Connection;
import java.util.Map;

public class CompanyView extends Company implements ProfiledEntityView, Dialable
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("CompanyDisplay");

   public CompanyView(Connection con)
   {
      super(con);
   }

   public CompanyView(ServletRequest request)
   {
      super(request);

      request.setAttribute("company",this);
   }

   public CompanyView(Connection con, String CompanyID)
   {
      super(con, CompanyID);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }
   
   public String getProfileFieldDisplay(String questionID) {
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildProfileDisplayValue(this, questionID);
   }
   
   public String getProfileFormInput(String questionID) {
      
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildProfileFormInput(this, questionID);
   }
   
   public String getFormInput(String field) {
      
      FormFactory ff = FormFactory.getFormFactory();
      
      return ff.buildFormInput(this, field);
   }
   
   public String getRecordURL(int channel) {
	   return getRecordURL(null, channel); 
   }
   
   public String getRecordURL(int channel, String custom) {
	   return getRecordURL(null, channel, custom); 
   }
   
   public String getRecordURL(String field, int channel) {
	   return getRecordURL(field, channel, ActionBean._blank); 
   }
   
   public String getRecordURL(String field, int channel, String custom) {
	   return DialerFactory.getRecordURL(this, field, channel, custom); 
   }
   
   public String getPhoneURL(String field, int channel){
	   return getPhoneURL(field, channel,null); 
   }
   
   public String getPhoneURL(String field, int channel,String custom){
	   return DialerFactory.getPhoneURL(this, field, channel, custom); 
   }
   
   public String getDialURL(String field) {
	   return DialerFactory.getDialURL(this, field); 
   }
   
   /*
    *  Created By : Puja Shah
    *  Created Date : 30th July 2008
    *  Purpose : Check whether the CTIEnabled system parameter is true. 
    *           If its value is true displays the link on the field which is passed as a parameter
    *           and call getDialURL method.
    */
   public String checkDialURL(String field){
      StringBuffer finalLabel = new StringBuffer();
      boolean ctiEnabled = "true".equals(InitServlet.getSystemParams().getProperty("CTIEnabled"));
         if(ctiEnabled && this.getField(field).length() > 0){
            finalLabel.append("<a href=\"");
            finalLabel.append(this.getDialURL(field));
            finalLabel.append("\">");
            finalLabel.append(this.getFieldLabel(field));
            finalLabel.append("</a>");
         }else{
            finalLabel.append(this.getFieldLabel(field));
         }
      return(finalLabel.toString());
   }
   
   /*
       *  Created By : Puja Shah
       *  Updated By : Dion Chapman
       *  Created Date : 30th July 2008
       *  Purpose : Check whether the CTIEnabled system parameter is true. 
       *           If its value is true displays the link on the field which is passed as a parameter
       *           and call getDialURL method using the imgTag as the link.
       */
   public String checkDialURL(String field, String imgTag){
      StringBuffer finalLabel = new StringBuffer();
      boolean ctiEnabled = "true".equals(InitServlet.getSystemParams().getProperty("CTIEnabled"));
         if(ctiEnabled && this.getField(field).length() > 0){
            finalLabel.append("<a href=\"");
            finalLabel.append(this.getDialURL(field));
            finalLabel.append("\">");
            finalLabel.append(imgTag);
            finalLabel.append("</a>");
         }else{
            finalLabel.append("");
         }
      return(finalLabel.toString());
   }
   
   /*
       *  Created By : Puja Shah
       *  Updated By : Dion Chapman
       *  Created Date : 30th July 2008
       *  Purpose : Check whether the CTIEnabled system parameter is true. 
       *           If its value is true displays the link on the field which is passed as a parameter
       *           and call getDialURL method using the imgTag as the link.
       */
   public String checkDialNumber(String field){
      StringBuffer finalLabel = new StringBuffer();
      boolean ctiEnabled = "true".equals(InitServlet.getSystemParams().getProperty("CTIEnabled"));
         if(ctiEnabled && this.getField(field).length() > 0){
            finalLabel.append("<a href=\"");
            finalLabel.append(this.getDialURL(field));
            finalLabel.append("\">");
            finalLabel.append(this.getField(field));
            finalLabel.append("</a>");
         }else{
            finalLabel.append(this.getField(field));
         }
      return(finalLabel.toString());
   }
   
   public String checkPhoneURL(String field,int channel,String custom){
      StringBuffer finalLabel = new StringBuffer();
      boolean ctiEnabled = "true".equals(InitServlet.getSystemParams().getProperty("CTIEnabled"));
         if(ctiEnabled && this.getField(field).length() > 0){
            finalLabel.append("<a href=\"");
            finalLabel.append(this.getPhoneURL(field,channel,custom));
            finalLabel.append("\">");
            finalLabel.append(this.getFieldLabel(field));
            finalLabel.append("</a>");
         }else{
            finalLabel.append(this.getFieldLabel(field));
         }
      return(finalLabel.toString());
   }
   
   public String checkPhoneURL(String field,int channel){
      return checkPhoneURL(field,channel,null);
   }   
}