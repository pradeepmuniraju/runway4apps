package com.sok.runway.view.crm.cms;

import com.sok.runway.crm.cms.*;
import com.sok.runway.view.*;
import com.sok.framework.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class MasterTemplateView extends MasterTemplate
{
   protected DisplaySpec dspec = SpecManager.getDisplaySpec("GroupDisplay");

   public MasterTemplateView(Connection con)
   {
      super(con);
   }

   public MasterTemplateView(ServletRequest request)
   {
      super(request);

      request.setAttribute("masterTemplate",this);
   }

   public MasterTemplateView(Connection con, String masterTemplateID)
   {
      super(con, masterTemplateID);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }

}