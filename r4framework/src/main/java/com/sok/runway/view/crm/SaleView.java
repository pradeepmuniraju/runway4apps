package com.sok.runway.view.crm;

import com.sok.runway.*;
import com.sok.runway.crm.*;
import com.sok.runway.view.*;
import com.sok.runway.security.AuditEntity;
import com.sok.framework.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class SaleView extends Sale implements ProfiledEntityView
{
   protected DisplaySpec dspec;

   public SaleView(Connection con)
   {
      super(con);
      dspec = SpecManager.getDisplaySpec("SaleDisplay");
   }

   public SaleView(ServletRequest request)
   {
      super(request);
      dspec = SpecManager.getDisplaySpec("SaleDisplay");
      request.setAttribute("sale",this);
   }

   public boolean isFieldUsed(String field)
   {
      return(dspec.isItemUsed(field));
   }

   public String getFieldLabel(String field)
   {
      return(dspec.getItemLabel(field));
   }

   public String getFieldSource(String field)
   {
	  return (dspec.getItemSource(field));
   }

   public String getFieldValidationType(String field, int num)
   {
      return(dspec.getItemValidationType(field, num));
   }

   public String getFieldValidationMessage(String field, int num)
   {
      return(dspec.getItemValidationMessage(field, num));
   }

   public String getProfileFieldDisplay(String questionID) {
      FormFactory ff = FormFactory.getFormFactory();

      return ff.buildProfileDisplayValue(this, questionID);
   }

   public String getProfileFormInput(String questionID) {

      FormFactory ff = FormFactory.getFormFactory();

      return ff.buildProfileFormInput(this, questionID);
   }

   public String getFormInput(String field) {

      FormFactory ff = FormFactory.getFormFactory();

      return ff.buildFormInput(this, field);
   }
}