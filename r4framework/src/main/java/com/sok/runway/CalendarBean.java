package com.sok.runway;

import com.sok.framework.*;
import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.util.*;
import com.sok.runway.UserCalendar.Appointment;

import java.util.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;
import java.text.*;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

public class CalendarBean extends GenRow
{
   public GenRow appointments = new GenRow();

   GregorianCalendar gCal = null;
   GregorianCalendar appointmentdate = null;

   int[][] dates = new int[6][7]; // weeks in month, days in week
   boolean[][] dateinmonth = new boolean[6][7]; // weeks in month, days in week

   public static int MON = 0;
   public static int TUE = 1;
   public static int WED = 2;
   public static int THU = 3;
   public static int FRI = 4;
   public static int SAT = 5;
   public static int SUN = 6;

   public static final String[] daysofweek = { "MON", "TUE", "WED", "THU",
         "FRI", "SAT", "SUN" };
   
   public static final String[] daysofweekfullname = { "Monday", "Tuesday", "Wednesday", "Thursday",
       "Friday", "Saturday", "Sunday" };

   public static final String[] duration = { "0:30", "1:00", "1:30", "2:00",
         "2:30", "3:00", "3:30", "4:00", "4:30", "5:00", "5:30", "6:00",
         "6:30", "7:00", "7:30", "8:00" };

   public static final String[] times = { "6:00AM", "6:30AM", "7:00AM",
         "7:30AM", "8:00AM", "8:30AM", "9:00AM", "9:30AM", "10:00AM",
         "10:30AM", "11:00AM", "11:30AM", "12:00PM", "12:30PM", "1:00PM",
         "1:30PM", "2:00PM", "2:30PM", "3:00PM", "3:30PM", "4:00PM", "4:30PM",
         "5:00PM", "5:30PM", "6:00PM", "6:30PM", "7:00PM", "7:30PM", "8:00PM",
         "8:30PM", "9:00PM", "9:30PM", "10:00PM", "10:30PM", "11:00PM",
         "11:30PM", "12:00AM", "12:30AM", "1:00AM", "1:30AM", "2:00AM",
         "2:30AM", "3:00AM", "3:30AM", "4:00AM", "4:30AM", "5:00AM", "5:30AM" };

   public static final String[] displaytimes = { "6:00<br>AM", null, "7:00",
         null, "8:00", null, "9:00<br>AM", null, "10:00", null, "11:00", null,
         "12:00<br>PM", null, "1:00", null, "2:00", null, "3:00<br>PM", null, "4:00",
         null, "5:00", null, "6:00<br>PM", null, "7:00", null, "8:00", null, "9:00<br>PM",
         null, "10:00", null, "11:00", null, "12:00<br>AM", null, "1:00", null,
         "2:00", null, "3:00<br>AM", null, "4:00", null, "5:00", null };
   
   // Constants for Day Planner
   public static final String[] dayPlannertimes = { "12:00AM", "12:30AM", "1:00AM", "1:30AM", 
	   "2:00AM", "2:30AM", "3:00AM", "3:30AM", "4:00AM", "4:30AM", "5:00AM", "5:30AM", "6:00AM", 
	   "6:30AM", "7:00AM", "7:30AM", "8:00AM", "8:30AM", "9:00AM", "9:30AM", "10:00AM", "10:30AM", 
	   "11:00AM", "11:30AM", "12:00PM", "12:30PM", "1:00PM", "1:30PM", "2:00PM", "2:30PM", "3:00PM", 
	   "3:30PM", "4:00PM", "4:30PM", "5:00PM", "5:30PM", "6:00PM", "6:30PM", "7:00PM", "7:30PM", "8:00PM", 
	   "8:30PM", "9:00PM", "9:30PM", "10:00PM", "10:30PM", "11:00PM", "11:30PM" };
   
   public static final String[][] minutesReplace = {{"01","02","03","04","05","06","07","08","09","10",
	   												 "11","12","13","14","15","16","17","18","19"},
	   												{"20","21","22","23","24","25","26","27","28","29",
	   											     "31","32","33","34","35","36","37","38","39","40",
		   										     "41","42","43","44","45","46","47","48","49"},
		   										    {"50","51","52","53","54","55","56","57","58","59"}};
   
   public static final String[] minutesReplacement = {":00",":30","1:00"};
   
   // Constants for Day Planner
   public static final String[] dayPlannerDisplaytimes = {"12:00<br>AM", null, "1:00", null, "2:00", null, "3:00<br>AM", null, "4:00", 
	   null, "5:00", null, "6:00<br>AM", null, "7:00", null, "8:00", null, "9:00<br>AM", null, 
	   "10:00", null, "11:00", null, "12:00<br>PM", null, "1:00", null, "2:00", null, "3:00<br>PM", 
	   null, "4:00", null, "5:00", null, "6:00<br>PM", null, "7:00", null, "8:00", null, "9:00<br>PM",
       null, "10:00", null, "11:00", null};

   public static String[] months = { "January", "February", "March", "April",
         "May", "June", "July", "August", "September", "October", "November",
         "December" };

   static final String blank = "";
   String contextpath = null;
   String datasource = null;
   StringBuffer errors = null;
   StringBuffer statement = null;
   String thisday = null;


   // today
   int today = 0;
   int thismonth = 0;
   int thisyear = 0;

   // currently displayed
   int currentday = 0;
   int currentdayofweek = 0;
   int currentweek = 0;
   int currentmonth = 0;
   int currentyear = 0;
   int lastweekinmonth = 0;

   //static int maxchars = 20;
   static int mmaxchars = 250;

   public static String daymode = "day";
   public static String weekmode = "week";
   public static String monthmode = "month";
   static String MODE = "-mode";
   static String USERID = "filterUserID";
   
   boolean debug = true;
   
   boolean sevenDays = false;

   private boolean replaceCarriageReturns = false;

   public CalendarBean()
   {
      super();
      setViewSpec("CalendarView");

      gCal = new GregorianCalendar();
      gCal.setFirstDayOfWeek(Calendar.MONDAY);
      today = gCal.get(Calendar.DAY_OF_MONTH);

      appointmentdate = new GregorianCalendar();
      appointmentdate.setFirstDayOfWeek(Calendar.MONDAY);

      thismonth = gCal.get(Calendar.MONTH);
      thisyear = gCal.get(Calendar.YEAR);
      DateFormat dayformat = new SimpleDateFormat("dd/MM/yyyy");
      thisday = dayformat.format(gCal.getTime());

      currentmonth = thismonth;
      int dayofweek = gCal.get(Calendar.DAY_OF_WEEK);
      currentdayofweek = getDay(dayofweek);
      currentweek = getWeekOfMonth(today, dayofweek) - 1;
      currentday = today;
      currentyear = thisyear;
   }

   public void setRequest(HttpServletRequest request)
   {
      super.setRequest(request);
      appointments.setRequest(request);
   }

   public void parseRequest(HttpServletRequest request)
   {
      clear();
      super.parseRequest(request);
      appointments.setRequest(request);
      if(getMode().length()==0)
      {
         put(MODE,weekmode);
      }
      
      if(getUserID().length() == 0)
      {
         put(USERID,getString(ActionBean.CURRENTUSERID));
      }      
      /*Get the User-Agent to find out if the browser is Firefox*/
      String useragent = request.getHeader("User-Agent");
      boolean isFirefoxBrowser = useragent.indexOf("Firefox") >= 0;  
      setSingleLineTitleText(isFirefoxBrowser);      
   }

   public void clear()
   {
      super.clear();
      appointments.clear();
   }
   
   public GenRow getAppointments()
   {
      return(appointments);
   }
   
   public void setAppointments(GenRow row)
   {
      appointments = row;
   }   

   public boolean isModeSelected(String mode)
   {
      return(getMode().equals(mode));
   }

   public String getUserID()
   {
      return(getString(USERID));
   }

   public void setAppointmentParameters(String begin, String end)
   {
      setAppointmentParameters(appointments);
      appointments.setParameter("NoteDate", begin+"-"+end);
      appointments.doAction("search");
   }
   
   public void setAppointmentParameters(TableData rows)
   {
      String filteruserid = getUserID();      
      rows.setViewSpec("CalendarView");
      rows.setParameter("Type", "Appointment");
      rows.setParameter("RepUserID|1", filteruserid);
      rows.setParameter("NoteAttendee.UserID|1", filteruserid);
      rows.setParameter("-sort1","Notes.NoteDate");
      rows.setParameter("-sort2","Notes.NoteTime");
      rows.setParameter("-sort3","Notes.Duration");      
   }

   // sun's gCal.get(Calendar.WEEK_OF_MONTH) is boken
   // it ignores gCal.setFirstDayOfWeek(Calendar.MONDAY)
   // this method assumes monday as FirstDayOfWeek
   int getWeekOfMonth(int today, int dayofweek)
   {
      int weeks = 0;
      int days = today;
      if(dayofweek != Calendar.SUNDAY)
      {
         weeks = weeks + 1;
         days = days - dayofweek + 1;
      }
      if(days > 0)
      {
         weeks = weeks + (days / 7);
         int re = days % 7;
         if(re != 0)
         {
            weeks = weeks + 1;
         }
      }
      return(weeks);
   }

   public String getMode()
   {
      return(getString(MODE));
   }

   public void setDebug(boolean b)
   {
      debug = b;
   }

   public String getErrors()
   {
      return(errors.toString());
   }

   protected void initArray()
   {
      for (int i = 0; i < dateinmonth.length; i++)
      {
         for (int j = 0; j < dateinmonth[i].length; j++)
         {
            dateinmonth[i][j] = false;
         }
      }
   }

   public void setContextPath(String t)
   {
      contextpath = t;
   }

   public String getStatement()
   {
      return(statement.toString());
   }

   void clearDates()
   {
      for (int i = 0; i < dates.length; i++) // weeks
      {
         for (int j = 0; j < dates[i].length; j++) // days
         {
            dates[i][j] = 0;
         }
      }
   }

   public void setDay()
   {
      setMonth();
   }

   public void setMonth()
   {
      int daysinmonth = getDaysInMonth();
      int tempdate = gCal.get(Calendar.DAY_OF_MONTH);
      int tempmonth = gCal.get(Calendar.MONTH);
      int tempyear = gCal.get(Calendar.YEAR);

      clearDates();
      gCal.set(Calendar.DAY_OF_MONTH, 1);
      adjustToWeekStart();

      StringBuffer start = new StringBuffer();
      start.append(String.valueOf(gCal.get(Calendar.DAY_OF_MONTH)));
      start.append('/');
      start.append(String.valueOf(gCal.get(Calendar.MONTH) + 1));
      start.append('/');
      start.append(String.valueOf(gCal.get(Calendar.YEAR)));

      boolean reachedfirst = false;
      boolean pastmonth = false;
      boolean notreachedendofweek = true;

      int tempdayofmonth = 0;
      int tempdayofweek = 0;

      for (int i = 0; i < dates.length && notreachedendofweek; i++) // week of
                                                                     // month
      {
         for (int j = 0; j < dates[i].length && notreachedendofweek; j++) // day
                                                                           // of
                                                                           // week
         {
            tempdayofmonth = gCal.get(Calendar.DAY_OF_MONTH);
            tempdayofweek = gCal.get(Calendar.DAY_OF_WEEK);

            if(tempdayofmonth == 1)
            {
               reachedfirst = true;
            }

            if(reachedfirst && !pastmonth)
            {
               dateinmonth[i][j] = true;
            }
            else
            {
               dateinmonth[i][j] = false;
            }

            if(reachedfirst && tempdayofmonth == daysinmonth)
            {
               pastmonth = true;
            }

            if(tempdayofweek == Calendar.MONDAY && j == MON)
            {
               dates[i][j] = tempdayofmonth;
               if(notreachedendofweek)
               {
                  gCal.add(Calendar.DATE, 1);
               }
            }
            else if(tempdayofweek == Calendar.TUESDAY && j == TUE)
            {
               dates[i][j] = tempdayofmonth;
               if(notreachedendofweek)
               {
                  gCal.add(Calendar.DATE, 1);
               }
            }
            else if(tempdayofweek == Calendar.WEDNESDAY && j == WED)
            {
               dates[i][j] = tempdayofmonth;
               if(notreachedendofweek)
               {
                  gCal.add(Calendar.DATE, 1);
               }
            }
            else if(tempdayofweek == Calendar.THURSDAY && j == THU)
            {
               dates[i][j] = tempdayofmonth;
               if(notreachedendofweek)
               {
                  gCal.add(Calendar.DATE, 1);
               }
            }
            else if(tempdayofweek == Calendar.FRIDAY && j == FRI)
            {
               dates[i][j] = tempdayofmonth;
               if(notreachedendofweek)
               {
                  gCal.add(Calendar.DATE, 1);
               }
            }
            else if(tempdayofweek == Calendar.SATURDAY && j == SAT)
            {
               dates[i][j] = tempdayofmonth;
               if(notreachedendofweek)
               {
                  gCal.add(Calendar.DATE, 1);
               }
            }
            else if(tempdayofweek == Calendar.SUNDAY && j == SUN)
            {
               dates[i][j] = tempdayofmonth;
               if(pastmonth)
               {
                  notreachedendofweek = false;
                  lastweekinmonth = i;
               }

               if(notreachedendofweek)
               {
                  gCal.add(Calendar.DATE, 1);
               }
            }

         }
      }

      gCal.add(Calendar.DAY_OF_MONTH, 1);

      StringBuffer end = new StringBuffer();
      end.append(String.valueOf(gCal.get(Calendar.DAY_OF_MONTH)));
      end.append('/');
      end.append(String.valueOf(gCal.get(Calendar.MONTH) + 1));
      end.append('/');
      end.append(String.valueOf(gCal.get(Calendar.YEAR)));

      setAppointmentParameters(start.toString(), end.toString());

      gCal.set(Calendar.YEAR, tempyear);
      gCal.set(Calendar.MONTH, tempmonth);
      gCal.set(Calendar.DAY_OF_MONTH, tempdate);
   }

   public int getWeekNumber()
   {
      return(currentweek);
   }

   void adjustToWeekStart()
   {
      if(gCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
      {
         gCal.add(Calendar.DATE, -1);
      }
      else if(gCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
      {
         gCal.add(Calendar.DATE, -2);
      }
      else if(gCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
      {
         gCal.add(Calendar.DATE, -3);
      }
      else if(gCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
      {
         gCal.add(Calendar.DATE, -4);
      }
      else if(gCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
      {
         gCal.add(Calendar.DATE, -5);
      }
      else if(gCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
      {
         gCal.add(Calendar.DATE, -6);
      }
   }

   public String getCellClass(int t)
   {
      return(getCellClass(currentweek, currentdayofweek, t));
   }

   public String getCellClass(int d, int t)
   {
      return(getCellClass(currentweek, d, t));
   }

   public String getCellClass(int w, int d, int t)
   {
      if(t > 3 && t < 26)
      {
         return("borderbr");
      }
      else
      {
         return("borderbrdark");
      }
   }
   
   public String getDayPlannerCellClass(int t)
   {
      return(getDayPlannerCellClass(currentweek, currentdayofweek, t));
   }

   public String getDayPlannerCellClass(int d, int t)
   {
      return(getDayPlannerCellClass(currentweek, d, t));
   }

   public String getDayPlannerCellClass(int w, int d, int t)
   {
      if(t > 13 && t < 32)
      {
         return("borderbr");
      }
      else
      {
         return("borderbrdark");
      }
   }
   
   public String getCellID(int d, int t)
   {
      StringBuffer id = new StringBuffer();
      id.append(String.valueOf(d));
      id.append("/");
      id.append(String.valueOf(currentdayofweek));
      return(id.toString());
   }

   public String getDateSuffix(int d)
   {
      if(d != 11 && d != 12 && d != 13)
      {
         int r = d % 10;

         if(r == 1)
         {
            return("<sup><small>st</small></sup>");
         }
         else if(r == 2)
         {
            return("<sup><small>nd</small></sup>");
         }
         else if(r == 3)
         {
            return("<sup><small>rd</small></sup>");
         }
      }
      return("<sup><small>th</small></sup>");
   }

   public String getTimeHeader(int i)
   {
      if(displaytimes[i] != null)
      {
         StringBuffer temp = new StringBuffer();
         temp.append("<td rowspan=\"2\" align=\"center\" class=\"timecell\">");
         temp.append(displaytimes[i]);
         temp.append("</td>");
         return(temp.toString());
      }
      return(blank);
   }
   
   public String getTimeHeaderForDayPlanner(int i)
   {
      if(dayPlannerDisplaytimes[i] != null)
      {
    	 if(i <= 17 || i >= 36) 
    	 {
	         StringBuffer temp = new StringBuffer();
	         temp.append("<td rowspan=\"2\" align=\"center\" class=\"timecell_dp_offline_hours\">");
	         temp.append(dayPlannerDisplaytimes[i]);
	         temp.append("</td>");
	         return(temp.toString());
    	 }
    	 else
    	 {
             StringBuffer temp = new StringBuffer();
             temp.append("<td rowspan=\"2\" align=\"center\" class=\"timecell_dp\">");
             temp.append(dayPlannerDisplaytimes[i]);
             temp.append("</td>");
             return(temp.toString());
    	 }
      }
      return(blank);
   }

   public String getTime(int i)
   {
      if(times[i] != null)
      {
         return(times[i]);
      }
      return(blank);
   }
   
   public String getDayPlannerTime(int i)
   {
      if(dayPlannertimes[i] != null)
      {
         return(dayPlannertimes[i]);
      }
      return(blank);
   }

   public String getDayOfMonthString(int w, int i)
   {
      if(dates[w][i] != 0)
      {
         return(String.valueOf(dates[w][i]));
      }
      return(blank);
   }

   public String getDayOfMonthString(int i)
   {
      return(getDayOfMonthString(currentweek, i));
   }

   public String getDayOfMonthString()
   {
      return(getDayOfMonthString(currentweek, currentdayofweek));
   }

   public boolean isDayInMonth(int w, int i)
   {
      return(dateinmonth[w][i]);
   }

   public boolean isDayInMonth(int i)
   {
      return(isDayInMonth(currentweek, i));
   }

   public boolean isDayInMonth()
   {
      return(isDayInMonth(currentweek, currentdayofweek));
   }

   public String getDayOfMonth(int w, int i)
   {
      if(dates[w][i] != 0)
      {
         StringBuffer temp = new StringBuffer();
         temp.append(String.valueOf(dates[w][i]));
         temp.append(getDateSuffix(dates[w][i]));
         return(temp.toString());
      }
      return(blank);
   }

   public String getDayOfMonth(int i)
   {
      return(getDayOfMonth(currentweek, i));
   }

   public String getDayOfMonth()
   {
      return(getDayOfMonth(currentweek, currentdayofweek));
   }

   public String getDayOfWeek(int i)
   {
      return(daysofweek[i]);
   }

   public String getDayOfWeek()
   {
      return(daysofweek[currentdayofweek]);
   }
   
   public String getDayOfWeekFullName(int i)
   {
      return(daysofweekfullname[i]);
   }

   public String getDayOfWeekFullName()
   {
      return(daysofweekfullname[currentdayofweek]);
   }

   public void setSingleLineTitleText(boolean replaceCarriageReturns)
   {
      this.replaceCarriageReturns = replaceCarriageReturns;
   }

   public boolean isSingleLineTitleText()
   {
      return replaceCarriageReturns;
   }

   int getWeek(int week, int month)
   {
      if(month == currentmonth)
      {
         return(week - 1);
      }
      else if(month < currentmonth)
      {
         return(0);
      }
      else
      {
         return(lastweekinmonth);
      }
   }

   protected String getString(ResultSet rs, int index)
   {
      String str = null;
      try
      {
         str = rs.getString(index);
      }
      catch(Exception e)
      {
      }

      if(str == null)
      {
         str = "";
      }
      return(str);
   }

   public static String getShortText(String inputtext, int size)
   {
      if(inputtext.length() > size)
      {
         inputtext = inputtext.substring(0, size);
         inputtext = inputtext + "...";
      }
      return(inputtext);
   }

   static int getTime(String time)
   {
      int t = 0;
      if(time != null)
      {
         for (int i = 0; i < times.length; i++)
         {
            if(time.equals(times[i]))
            {
               t = i;
               break;
            }
         }
      }
      if (t > 35) t = 0;
      return(t);
   }
   
   static int getDayPlannerTime(String time)
   {
      int t = 0;
      if(time != null && time.length() >= 6 )
      {
         for (int i = 0; i < dayPlannertimes.length; i++)
         {
            if(time.equals(dayPlannertimes[i]))
            {
               t = i;
               break;
            }
         }
      }
      return(t);
   }
   
   static String replaceTime(String time) {
	   String t = "";
	   for (int a = 0; a < 3; ++a) {
		   String p1[] = minutesReplace[a];
		   String p2 = minutesReplacement[a];
		   
		   for (int b = 0; b < p1.length; ++b) {
			   String r = ":" + p1[b];
			   if (time.indexOf(r) > 0) {
				   if (!p2.startsWith("1:")) {
					   time = time.replaceAll(r,p2);
				   } else if (!time.startsWith("24:")){
					   time = time.replaceAll(r,p2);
				       for (int i = 0; i < dayPlannertimes.length; i++)
				       {
				           if(time.substring(0, 2).equals(dayPlannertimes[i].substring(0, 2)))
				           {
				        	   time = dayPlannertimes[i + 2].substring(0, 2) + time.substring(3);
				        	   break;
				           }
				       }
				   } else {
					   time = time.replaceAll(r,minutesReplacement[a - 1]);
				   }
				   break;
			   }
		   }
	   }
	   
	   return time;
   }

   static int getDurationBlocks(String blocks)
   {
	  if (blocks == null || blocks.length() == 0 || "0:00".equals(blocks)) return 0;
	  String[] parts = blocks.split(":") ;
	  
	  if (parts.length == 2) {
		  try {
			  double b = (Double.parseDouble(parts[0]) * 60) + Double.parseDouble(parts[1]);
			  b = Math.ceil(b / 30);
			  if (b > 36) b = 36;
			  return (int) b;
		  } catch (Exception e) {
			  
		  }
	  }
	  return 1;
	  /* this old method only worked for :00 and :30 values, Exchange would give different numbers
      for (int i = 0; i < duration.length; i++)
      {
         if(duration[i].equals(blocks))
         {
            return(i + 1);
         }
      }
      // we should have a minimum of 1 for anything
      if (blocks.startsWith("0")) return 1
      return(16); */
   }
   
   public int getAppointmentStartBlock()
   {
      return(getTime(replaceTime(appointments.getData("NoteTime"))));
   }
   
   public int getDayPlannerAppointmentStartBlock()
   {
      return(getDayPlannerTime(replaceTime(appointments.getData("NoteTime"))));
   }

   public int getAppointmentBlocks()
   {
      return(getDurationBlocks(appointments.getData("Duration")));
   }
   
   static int getDurationMinutes(String blocks)
   {
      return(getDurationBlocks(blocks) * 30);
   }

   public static String getEndDate(String notedate, String blocks)
   {
      try
      {
         String format = "dd/MM/yyyy hh:mma";
         java.util.Date date = ActionBean.getDate(notedate, format, null, null);
         GregorianCalendar cal = new GregorianCalendar();
         cal.setTime(date);
         cal.add(Calendar.MINUTE, getDurationMinutes(blocks));
         DateFormat dt = new SimpleDateFormat(format);
         return(dt.format(cal.getTime()));
      }
      catch(Exception e)
      {
      }

      return(null);
   }

   /**
    * @return String of today's date
    */
   public String getTodayString()
   {
      return thisday;
   }

   protected int getDay(int calday)
   {
      int day = 0;
      if(calday == Calendar.MONDAY)
      {
         day = MON;
      }
      else if(calday == Calendar.TUESDAY)
      {
         day = TUE;
      }
      else if(calday == Calendar.WEDNESDAY)
      {
         day = WED;
      }
      else if(calday == Calendar.THURSDAY)
      {
         day = THU;
      }
      else if(calday == Calendar.FRIDAY)
      {
         day = FRI;
      }
      else if(calday == Calendar.SATURDAY)
      {
         day = SAT;
      }
      else if(calday == Calendar.SUNDAY)
      {
         day = SUN;
      }
      return(day);
   }


   public void doAction()
   {
      boolean adjustoweek = false;
      String action = getAction();
      if(action != null && action.length() != 0)
      {
         int templastmonth = gCal.get(Calendar.MONTH);

         if(action.equals("nextday"))
         {
            gCal.add(Calendar.DATE, 1);
         }
         else if(action.equals("priorday"))
         {
            gCal.add(Calendar.DATE, -1);
         }
         else if(action.equals("nextweek"))
         {
            adjustoweek = true;
            gCal.add(Calendar.DATE, 7);
         }
         else if(action.equals("priorweek"))
         {
            gCal.add(Calendar.DATE, -7);
         }
         else if(action.equals("priormonth"))
         {
            adjustoweek = true;
            gCal.add(Calendar.MONTH, -1);
            gCal.set(Calendar.DAY_OF_MONTH, 1);
         }
         else if(action.equals("priormonthend"))
         {
            adjustoweek = true;
            gCal.add(Calendar.MONTH, -1);
            gCal.set(Calendar.DAY_OF_MONTH, gCal
                  .getActualMaximum(Calendar.DAY_OF_MONTH));
         }
         else if(action.equals("nextmonth"))
         {
            adjustoweek = true;
            gCal.add(Calendar.MONTH, 1);
            gCal.set(Calendar.DAY_OF_MONTH, 1);
         }
         else if(action.equalsIgnoreCase("today"))
         {
            gCal.set(Calendar.YEAR, thisyear);
            gCal.set(Calendar.MONTH, thismonth);
            gCal.set(Calendar.DAY_OF_MONTH, today);
         }
         else if(action.indexOf("/") > 0)
         {
            String[] gotodate = action.split("/");
            if(dates.length > 0)
            {
               try
               {
                  int day = Integer.parseInt(gotodate[0]);
                  gCal.set(Calendar.DAY_OF_MONTH, day);
               }
               catch(Exception e)
               {
               }
            }
            if(dates.length > 1)
            {
               try
               {
                  int month = Integer.parseInt(gotodate[1]);
                  gCal.set(Calendar.MONTH, month - 1);
               }
               catch(Exception e)
               {
               }
            }
            if(dates.length > 2)
            {
               try
               {
                  int year = Integer.parseInt(gotodate[2]);
                  gCal.set(Calendar.YEAR, year);
               }
               catch(Exception e)
               {
               }
            }
         }
         else
         {
            try
            {
               int day = Integer.parseInt(action);
               gCal.set(Calendar.DAY_OF_MONTH, day);
            }
            catch(Exception e)
            {
            }
         }

         int tempthismonth = gCal.get(Calendar.MONTH);

      }
      else
      {
         adjustoweek = true;
      }

      if(getMode().equals(weekmode) && adjustoweek)
      {
         if(gCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
         {
            gCal.add(Calendar.DATE, 2);
         }
         else if(gCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
         {
            gCal.add(Calendar.DATE, 1);
         }
      }

      currentmonth = gCal.get(Calendar.MONTH);
      int dayofweek = gCal.get(Calendar.DAY_OF_WEEK);
      // currentweek = gCal.get(Calendar.WEEK_OF_MONTH)-1;
      currentdayofweek = getDay(dayofweek);
      currentday = gCal.get(Calendar.DAY_OF_MONTH);
      currentyear = gCal.get(Calendar.YEAR);
      currentweek = getWeekOfMonth(currentday, dayofweek) - 1;

      initArray();
      setMonth();
  
   }

   public String getMonth()
   {
      return(months[gCal.get(Calendar.MONTH)]);
   }

   public String getYear()
   {
      return(String.valueOf(gCal.get(Calendar.YEAR)));
   }

   public int getDaysInMonth()
   {
      return(gCal.getActualMaximum(Calendar.DAY_OF_MONTH));
   }

   public String renderMonth()
   {
      StringBuffer sb = new StringBuffer();
      java.util.Date current = gCal.getTime();
      gCal.set(Calendar.DAY_OF_MONTH, 1);
      int rowcount = 0;
      int daysinmonth = getDaysInMonth();
      boolean pad = false;
      for (int i = 0; i < daysinmonth; i++)
      {
         int day = gCal.get(Calendar.DAY_OF_WEEK);
         if(i == 0)
         {
            if(day == Calendar.SUNDAY)
            {
               rowcount = rowcount + 1;
               pad = true;
            }
            if(rowcount == currentweek && getMode().equals(weekmode))
            {
               sb.append("<tr class=\"weekhighlight\">\r\n");
            }
            else
            {
               sb.append("<tr>\r\n");
            }
            rowcount = rowcount + 1;
            if(day == Calendar.MONDAY)
            {
               sb.append("<td></td>");
            }
            if(day == Calendar.TUESDAY)
            {
               sb.append("<td></td><td></td>");
            }
            else if(day == Calendar.WEDNESDAY)
            {
               sb.append("<td></td><td></td><td></td>");
            }
            else if(day == Calendar.THURSDAY)
            {
               sb.append("<td></td><td></td><td></td><td></td>");
            }
            else if(day == Calendar.FRIDAY)
            {
               sb.append("<td></td><td></td><td></td><td></td><td></td>");
            }
            else if(day == Calendar.SATURDAY)
            {
               sb
                     .append("<td></td><td></td><td></td><td></td><td></td><td></td>");
            }
         }

         sb.append("<td class=\"cell");

         if(currentday == i + 1)
         {
            sb.append("highlight");
         }
         sb.append("\" id=\"DATE_");
         sb.append(getCalString());         
         sb.append("\">");

         if(day != Calendar.SUNDAY && day != Calendar.SATURDAY)
         {
            sb.append("<span class=\"");
            sb.append("daycell");
            if(today == i + 1 && thismonth == currentmonth)
            {
               sb.append("bold");
            }
            sb.append("\">");

            sb.append("<a href=\"");
            if(contextpath != null)
            {
               sb.append(contextpath);
               sb.append("/runway/");
            }
            sb.append("calendar.view?");
            sb.append("-action=");
            sb.append(String.valueOf(i + 1));
            AbstractParameterNode.appendQuery(sb, USERID, getUserID());
            AbstractParameterNode.appendQuery(sb, MODE, getMode());           
            appendExQuery(sb);
 
            sb.append("\">");

            sb.append(String.valueOf(i + 1));
            sb.append("</a>");
            sb.append("</span>");
         }
         else
         {
            sb.append("<span class=\"");
            sb.append("weekendcell");
            if(today == i + 1 && thismonth == currentmonth)
            {
               sb.append("bold");
            }
            sb.append("\">");
            
            sb.append("<a href=\"");
            if(contextpath != null)
            {
               sb.append(contextpath);
               sb.append("/runway/");
            }
            sb.append("calendar.view?");
            sb.append("-action=");
            sb.append(String.valueOf(i + 1));
            AbstractParameterNode.appendQuery(sb, USERID, getUserID());
            AbstractParameterNode.appendQuery(sb, MODE, getMode());           
            appendExQuery(sb);
 
            sb.append("\">");
            
            sb.append(String.valueOf(i + 1));
            sb.append("</a>");
            sb.append("</span>");
         }

         sb.append("</td>");
         if(day == Calendar.SATURDAY)
         {
            sb.append("</tr>");
            if(i + 1 != daysinmonth)
            {
               if(rowcount == currentweek && getMode().equals(weekmode))
               {
                  sb.append("<tr class=\"weekhighlight\">\r\n");
               }
               else
               {
                  sb.append("<tr>\r\n");
               }
               rowcount = rowcount + 1;
            }
         }
         gCal.add(Calendar.DATE, 1);
      }
      sb.append("</tr>\r\n");
      if(rowcount < 6 || pad)
      {
         sb.append("<tr>\r\n<td><br><!--");
         sb.append(String.valueOf(rowcount));
         sb.append("--></td>\r\n</tr>\r\n");
      }

      gCal.setTime(current);
      return(sb.toString());
   }
   
   
	/* Render month method to populate Day Planner on Assistant widget
	 * page once a date is selected.
	 * 
	 * HTML link hard coded for this page.
	 */ 
  public String renderMonthForDayPlannerOnAssistant(String outerDiv, String userID)
  {
     StringBuffer sb = new StringBuffer();
     java.util.Date current = gCal.getTime();
     gCal.set(Calendar.DAY_OF_MONTH, 1);
     int rowcount = 0;
     int daysinmonth = getDaysInMonth();
     boolean pad = false;
     for (int i = 0; i < daysinmonth; i++)
     {
        int day = gCal.get(Calendar.DAY_OF_WEEK);
        if(i == 0)
        {
           if(day == Calendar.SUNDAY)
           {
              rowcount = rowcount + 1;
              pad = true;
           }
           if(rowcount == currentweek && getMode().equals(weekmode))
           {
              sb.append("<tr class=\"weekhighlight\">\r\n");
           }
           else
           {
              sb.append("<tr>\r\n");
           }
           rowcount = rowcount + 1;
           if(day == Calendar.MONDAY)
           {
              sb.append("<td>&nbsp;</td>");
           }
           if(day == Calendar.TUESDAY)
           {
              sb.append("<td>&nbsp;</td><td>&nbsp;</td>");
           }
           else if(day == Calendar.WEDNESDAY)
           {
              sb.append("<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>");
           }
           else if(day == Calendar.THURSDAY)
           {
              sb.append("<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>");
           }
           else if(day == Calendar.FRIDAY)
           {
              sb.append("<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>");
           }
           else if(day == Calendar.SATURDAY)
           {
              sb
                    .append("<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>");
           }
        }

        sb.append("<td class=\"cell");

        if(currentday == i + 1)
        {
           sb.append("highlight");
        }
        sb.append("\" id=\"DATE_");
        sb.append(getCalString());         
        sb.append("\">");

        if(sevenDays || (day != Calendar.SUNDAY && day != Calendar.SATURDAY))
        {
           sb.append("<span class=\"");
           sb.append("daycell");
           if(today == i + 1 && thismonth == currentmonth)
           {
              sb.append("bold");
           }
           sb.append("\">");

           sb.append("<a href=\"");

           sb.append("javascript:modifyCalendar('");           
           sb.append(String.valueOf(i + 1));
           sb.append("', '");
           if(contextpath != null)
           {
              sb.append(contextpath);
           }
           sb.append("', '");
           sb.append(outerDiv);
           sb.append("', '");
           sb.append(userID);
           sb.append("')\"");
           sb.append(">");

           sb.append(String.valueOf(i + 1));
           sb.append("</a>");
           sb.append("</span>");
        }
        else
        {
           sb.append("<span class=\"");
           sb.append("weekendcell");
           if(today == i + 1 && thismonth == currentmonth)
           {
              sb.append("bold");
           }
           sb.append("\">");
           
           sb.append("<a href=\"");

           sb.append("javascript:modifyCalendar('");           
           sb.append(String.valueOf(i + 1));
           sb.append("', '");
           if(contextpath != null)
           {
              sb.append(contextpath);
           }
           sb.append("', '");
           sb.append(outerDiv);
           sb.append("', '");
           sb.append(userID);
           sb.append("')\"");
           sb.append(">");
           
           sb.append(String.valueOf(i + 1));
           sb.append("</a>");
           sb.append("</span>");
        }

        sb.append("</td>");
        if(day == Calendar.SATURDAY)
        {
           sb.append("</tr>");
           if(i + 1 != daysinmonth)
           {
              if(rowcount == currentweek && getMode().equals(weekmode))
              {
                 sb.append("<tr class=\"weekhighlight\">\r\n");
              }
              else
              {
                 sb.append("<tr>\r\n");
              }
              rowcount = rowcount + 1;
           }
        }
        gCal.add(Calendar.DATE, 1);
     }
     sb.append("</tr>\r\n");
     /*if(rowcount < 6 || pad)
     {
        sb.append("<tr>\r\n<td><br><!--");
        sb.append(String.valueOf(rowcount));
        sb.append("--></td>\r\n</tr>\r\n");
     }*/

     gCal.setTime(current);
     return(sb.toString());
  }   

   public void setSevenDays(boolean b) {
	   sevenDays = b;
   }
   
   public String renderPrintMonth(int add)
   {
      StringBuffer sb = new StringBuffer();
      java.util.Date current = gCal.getTime();
      gCal.set(Calendar.DAY_OF_MONTH, 1);
      gCal.add(Calendar.MONTH, add);
      int rowcount = 0;
      int daysinmonth = getDaysInMonth();
      boolean pad = false;
      sb.append("<tr><td colspan=\"7\" class=\"headerCell\">");
      sb.append(getMonth());
      sb.append(" ");
      sb.append(getYear());
      sb.append("</td></tr>");
      for (int i = 0; i < daysinmonth; i++)
      {
         int day = gCal.get(Calendar.DAY_OF_WEEK);
         if(i == 0)
         {
            if(day == Calendar.SUNDAY)
            {
               rowcount = rowcount + 1;
               pad = true;
            }

            sb.append("<tr>\r\n");

            rowcount = rowcount + 1;
            if(day == Calendar.MONDAY)
            {
               sb.append("<td></td>");
            }
            if(day == Calendar.TUESDAY)
            {
               sb.append("<td></td><td></td>");
            }
            else if(day == Calendar.WEDNESDAY)
            {
               sb.append("<td></td><td></td><td></td>");
            }
            else if(day == Calendar.THURSDAY)
            {
               sb.append("<td></td><td></td><td></td><td></td>");
            }
            else if(day == Calendar.FRIDAY)
            {
               sb.append("<td></td><td></td><td></td><td></td><td></td>");
            }
            else if(day == Calendar.SATURDAY)
            {
               sb
                     .append("<td></td><td></td><td></td><td></td><td></td><td></td>");
            }
         }

         if(day != Calendar.SUNDAY && day != Calendar.SATURDAY)
         {
            sb.append("<td class=\"");
            sb.append("daycell");
            sb.append("\">");
            sb.append(String.valueOf(i + 1));
            sb.append("</td>");
         }
         else
         {
            sb.append("<td class=\"");
            sb.append("weekendcell");
            sb.append("\">");
            sb.append(String.valueOf(i + 1));
            sb.append("</td>");
         }

         if(day == Calendar.SATURDAY)
         {
            sb.append("</tr>");
            if(i + 1 != daysinmonth)
            {
               sb.append("<tr>\r\n");
               rowcount = rowcount + 1;
            }
         }
         gCal.add(Calendar.DATE, 1);
      }
      sb.append("</tr>\r\n");
      if(rowcount < 6 || pad)
      {
         sb.append("<tr>\r\n<td><br></td>\r\n</tr>\r\n");
      }

      gCal.setTime(current);
      return(sb.toString());
   }

   public String getCalString()
   {
      int month = gCal.get(Calendar.MONTH) + 1;
      int date = gCal.get(Calendar.DAY_OF_MONTH);
      StringBuffer datestring = new StringBuffer();
      if(date<10)
      {
         datestring.append("0");
      }      
      datestring.append(String.valueOf(date));
      datestring.append("/");
      if(month<10)
      {
         datestring.append("0");
      }
      datestring.append(String.valueOf(month));
      datestring.append("/");
      datestring.append(String.valueOf(gCal.get(Calendar.YEAR)));
      return(datestring.toString());
   }   
   
   public String getDateString()
   {
      return(getDateString(currentweek, currentdayofweek));
   }

   public String getDateString(int day)
   {
      return(getDateString(currentweek, day));
   }

   public String getDateString(int week, int day)
   {
      StringBuffer datestring = new StringBuffer();
      int monthvalue = currentmonth + 1;
      int dayvalue = dates[week][day];
      if(week > 3 && dayvalue < 7)
      {
         monthvalue = monthvalue + 1;
      }
      if(week < 2 && dayvalue > 20)
      {
         monthvalue = monthvalue - 1;
      }
      if(dates[week][day] < 10)
      {
         datestring.append("0");
      }
      datestring.append(String.valueOf(dayvalue));
      datestring.append("/");
      if(monthvalue < 10)
      {
         datestring.append("0");
      }
      datestring.append(String.valueOf(monthvalue));
      datestring.append("/");
      datestring.append(String.valueOf(currentyear));
      return(datestring.toString());
   }

   public String getToolTip()
   {
      StringBuffer temp = new StringBuffer();
      String contactname = appointments.getString("FirstName") + " " + appointments.getString("LastName");
      String contactcompany = appointments.getString("Company");
      if(contactname.length()>1)
      {
         temp.append(contactname);
      }
      if(contactcompany.length()!=0)
      {
         if(temp.length()!=0)
         {
            temp.append(" - ");
         }
            temp.append(contactcompany);
      }
      if(temp.length()!=0)
      {
         temp.append("\r\n");
      }
      temp.append(getText());

      String notestr =   StringUtil.replace(StringUtil.replace(StringUtil.replace(temp.toString(),"\"","&quot;"),"<","&lt;"),">","&gt;");
      
      if(replaceCarriageReturns)
      {
         notestr = StringUtil.replace(notestr,"\r\n","");
         notestr = StringUtil.replace(notestr,"\n","");
      }      
      
      return(notestr.toString());
   }
   
   public String getContactName()
   {
      StringBuffer temp = new StringBuffer();
      if(appointments.getString("FirstName").length()!=0)
      {
         temp.append(appointments.getString("FirstName")).append(" ").append(appointments.getString("LastName"));
      }
      return(temp.toString());
   }
   
   public String getSubject()
   {
      String temp = appointments.getString("Subject").length()!=0?appointments.getString("Subject"):getShortText(appointments.getString("Body"),mmaxchars);
      return(StringUtil.toHTML(temp));
   }   
   
   protected String getText()
   {
      return(appointments.getString("Subject").length()!=0?getShortText(appointments.getString("Subject"),mmaxchars):getShortText(appointments.getString("Body").length() > 0? appointments.getString("Body") : "-no subject-",mmaxchars));
   }
   
   public String getLongText()
   {
      return(appointments.getString("Subject").length()!=0?appointments.getString("Subject"):appointments.getString("Body"));
   }   
   
   protected String getAddress(TableData rs)
   {
      String state = rs.getString("State");
      String postcode = rs.getString("Postcode");
      
      StringBuffer sb = new StringBuffer();
      appendLine(sb, rs.getString("Street"));
      appendLine(sb, rs.getString("Street2"));
      appendLine(sb, rs.getString("City"));
      if(state.length()!=0 || postcode.length()!=0)
      {
         if(sb.length()!=0)
         {
            sb.append("<br>");
         }
         sb.append(state);
         sb.append(" ");
         sb.append(postcode);
      }
      appendLine(sb, rs.getString("Phone"));
      appendLine(sb, rs.getString("Mobile"));
      
      return(sb.toString());
   }   
   
   protected void appendLine(StringBuffer sb, String text)
   {
      if(text.length()!=0)
      {
         if(sb.length()!=0)
         {
            sb.append("<br>");
         }
         sb.append(text);
      }
   }
   
   public String getAddress()
   {
      StringBuffer temp = new StringBuffer();
      String contactname = getContactName();
      String company = appointments.getString("Company");
      String address = getAddress(appointments);
      if(contactname.length()!=0)
      {
         temp.append("<div class=\"appointmentTitleCell\">");
         temp.append(contactname);
         temp.append("</div>");
      }
      if(company.length()!=0)
      {
         temp.append("<div class=\"appointmentTextCell\">");
         temp.append(company);
         temp.append("</div>");
      }
      if(address.length()!=0)
      {
         temp.append("<div class=\"appointmentTextCell\">");
         temp.append(address);
         temp.append("</div>");
      }

      return(temp.toString());   
   }   
   
   public String getExQuery()
   {
      StringBuffer st = new StringBuffer();
      appendExQuery(st);
      if(st.length()>0)
      {
         st.insert(0,'&');
      }
      return(st.toString());
   }
   
   public void appendExQuery(StringBuffer st)
   {
      Node head = this.headNode;
      ParameterNode pnode;
      int nodetype = -1;
      while(head != null)
      {
         nodetype = head.getNodeType();
         if(nodetype >= Parameter.type_MinParameter && nodetype < Parameter.type_MaxParameter)
         {
            if(isExParameter(head.getName()))
            {
               pnode = (ParameterNode)head;
               if(!pnode.isEmpty())
               {
                  pnode.appendQuery(st);
               }
            }
         }
         head = head.getNextNode();
      }
   }
   
   public String getExHiddenInputs()
   {
      StringBuffer st = new StringBuffer();      
      Node head = this.headNode;
      ParameterNode pnode;
      int nodetype = -1;
      while(head != null)
      {
         nodetype = head.getNodeType();
         if(nodetype >= Parameter.type_MinParameter && nodetype < Parameter.type_MaxParameter)
         {
            if(isExParameter(head.getName()))
            {
               pnode = (ParameterNode)head;
               if(!pnode.isEmpty())
               {
                  pnode.appendHiddenInput(st);
               }
            }
         }
         head = head.getNextNode();
      }
      return(st.toString());
   }   
   
   protected boolean isExParameter(String name)
   {
      if(ActionBean.ACTION.equals(name) || ActionBean.CURRENTUSERID.equals(name) || USERID.equals(name) || MODE.equals(name))
      {
         return(false);
      }
      return(true);
   }  
   
   public static void main(String[] args)
   {

   }
}