package com.sok.runway;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import com.sok.framework.TableData;
import com.sok.runway.validationRule.CreditCardValidationRule;
import com.sok.runway.validationRule.DateTimeFormatValidationRule;
import com.sok.runway.validationRule.InRangeValidationRule;
import com.sok.runway.validationRule.MandatoryValidationRule;
import com.sok.runway.validationRule.MutuallyExclusiveValidationRule;
import com.sok.runway.validationRule.NumericValidationRule;
import com.sok.runway.validationRule.RegExpValidationRule;
import com.sok.runway.validationRule.RestrictedValidationRule;
import com.sok.runway.validationRule.URLValidationRule;
import com.sok.runway.validationRule.ValidationRule;

/**
 * The Validator class represents the server side validation.
 * It supports the following:
 * - Mandatory Validation.
 * - In Range Validation (min and max values).
 * - Mutually Exclusive Fields Validation.
 * - Restricted Values Validation.
 * - Date / Time format validation
 * - Email Address validation
 * - Phone Number validation
 * - Regular Expression validation
 * - URL validation (http/s, ftp/s, file://)
 * - Number validation
 * - Credit Card validation (Number, Expiry Date, and Type)
 * 
 * @author raymond.dinata
 */
public class Validator 
{
	private static HashMap validators = new HashMap();
   
	/* List of validation rules where the class iterates and executes the validate method. */
	private ArrayList<ValidationRule> ruleList = null; 

	/**
	 * Constructor - initialise the list of validation rules.
	 */
	public Validator() 
	{
		ruleList = new ArrayList<ValidationRule>();
	}
   
	public static synchronized Validator getValidator(String name) {
		return (Validator)validators.get(name);
	}

	public static synchronized void addValidator(String name, Validator validator) {
		validators.put(name, validator);
	}   

	// TODO: Delete this if unused
	public class RangeValues { 
		String field; 
		Double minValue; 
		Double maxValue; 
		String msg; 

		public RangeValues(String field, Double minValue, Double maxValue, String msg) {

			this.field = field;
			this.minValue = minValue; 
			this.maxValue = maxValue; 
			this.msg = msg; 
		}
	}
   
   /**
    * Adds the mandatory validation rule for a field
    * @param field the field name of the field to be validated
    * @param errorMsg the error message to be displayed when validation fails
    */
   public void addMandatoryField(String fieldName, String errorMsg) 
   {
	   MandatoryValidationRule rule = new MandatoryValidationRule(fieldName, errorMsg);
	   addValidatorRule(rule); 
   }

   /**
    * Adds the in range double validation rule for a field 
    * @param field the field name of the field to be validated
    * @param minValue the minimum limit of the field value
    * @param maxValue the maximum limit of the field value
    * @param errorMsg the error message to be displayed when validation fails
    */
   public void addRangeField(String fieldName, double minValue, double maxValue, String errorMsg) 
   { 
	   InRangeValidationRule<Double> rule = new InRangeValidationRule<Double>(fieldName, minValue, maxValue, errorMsg);
	   addValidatorRule(rule); 
   }
   
   /**
    * Adds the in range integer validation rule for a field 
    * @param field the field name of the field to be validated
    * @param minValue the minimum limit of the field value
    * @param maxValue the maximum limit of the field value
    * @param errorMsg the error message to be displayed when validation fails
    */
   public void addRangeField(String fieldName, int minValue, int maxValue, String errorMsg) 
   { 
	   InRangeValidationRule<Integer> rule = new InRangeValidationRule<Integer>(fieldName, minValue, maxValue, errorMsg);
	   addValidatorRule(rule); 
   }

   /**
    * Adds the restricted validation rule for a field  
    * @param fieldName the field name of the field to be validated
    * @param allowedValues list of allowed values for the field
    */
   public void addRestrictedValueField(String fieldName, Set<String> allowedValues) 
   {
	   RestrictedValidationRule  rule = new RestrictedValidationRule(fieldName, allowedValues);
	   addValidatorRule(rule); 
   }
   
   /**
    * Adds the mutually exclusive validation rules for a list of fields
    * @param fieldNames list of field names to be validated
    */
   public void addMutexFields(Set<String> fieldNames) 
   {
	   MutuallyExclusiveValidationRule  rule = new MutuallyExclusiveValidationRule(fieldNames);
	   addValidatorRule(rule); 
   }

   /**
    * Adds the date /time format validation rule for a field
    * @param fieldName the date / time field name of the field to be validated 
    * @param errorMsg the error message to be displayed when the validation fails
    * @param pattern the date format pattern
    */
   public void addDateTimeFormatField(String fieldName, String errorMsg, String pattern)
   {
	   DateTimeFormatValidationRule rule = new DateTimeFormatValidationRule(fieldName, errorMsg, pattern);
	   addValidatorRule(rule);
   }
   
   /**
    * Adds the email address validation rule for a field
    * @param fieldName the field name to be validated
    * @param errorMsg the error message to be displayed when the validation fails
    */
   public void addEmailAddressField(String fieldName, String errorMsg)
   {
	   RegExpValidationRule rule = new RegExpValidationRule(fieldName, errorMsg, RegExpValidationRule.EMAIL_REG_EX_PATTERN);
	   addValidatorRule(rule);
   }
   
   /**
    * Adds the phone number validation rule for a field
    * @param fieldName the field name to be validated
    * @param errorMsg the error message to be displayed when the validation fails
    */
   public void addPhoneNumberField(String fieldName, String errorMsg)
   {
	   RegExpValidationRule rule = new RegExpValidationRule(fieldName, errorMsg, RegExpValidationRule.PHONE_NUMBER_REG_EX_PATTERN);
	   addValidatorRule(rule);
   }
   
   /**
    * Adds the regular expression validation rule for a field
    * @param fieldName the field name to be validated
    * @param errorMsg the error message to be displayed when the validation fails
    * @param regExp the regular expression to be used for validation
    */
   public void addRegExpField(String fieldName, String errorMsg, String regExp)
   {
	   RegExpValidationRule rule = new RegExpValidationRule(fieldName, errorMsg, regExp);
	   addValidatorRule(rule);
   }
   
   /**
    * Adds the URL validation rule for a field
    * @param fieldName the field name to be validated
    * @param errorMsg the error message to be displayed when the validation fails
    */
   public void addURLField(String fieldName, String errorMsg)
   {
	   URLValidationRule rule = new URLValidationRule(fieldName, errorMsg);
	   addValidatorRule(rule);
   }
   
   /**
    * Adds the numeric validation rule for a field
    * @param fieldName the field name to be validated
    * @param errorMsg the error message to be displayed when the validation fails
    */
   public void addNumericField(String fieldName, String errorMsg)
   {
	   NumericValidationRule rule = new NumericValidationRule(fieldName, errorMsg);
	   addValidatorRule(rule);
   }

   /**
    * Adds the credit card validation rule for a field to validate the credit card number
    * @param fieldName the field name to be validated
    * @param errorMsg the error message to be displayed when the validation fails
    * @param cardType credit card type info needed to figure out whether the credit card number is valid or not
    */
   public void addCreditCardNumberField(String fieldName, String errorMsg, String cardType)
   {
	   CreditCardValidationRule rule = new CreditCardValidationRule(fieldName, errorMsg, CreditCardValidationRule.CREDIT_CARD_NUMBER, cardType);
	   addValidatorRule(rule);
   }
   
   /**
    * Adds the credit card validation rule for a field to validate the credit card expiry date
    * @param fieldName the field name to be validated
    * @param errorMsg the error message to be displayed when the validation fails
    */
   public void addCreditCardExpiryField(String fieldName, String errorMsg)
   {
	   CreditCardValidationRule rule = new CreditCardValidationRule(fieldName, errorMsg, CreditCardValidationRule.CREDIT_CARD_EXPIRY_DATE);
	   addValidatorRule(rule);
   }

   /**
    * Adds the credit card validation rule for a field to validate the credit card type
    * @param fieldName the field name to be validated
    * @param errorMsg the error message to be displayed when the validation fails
    */
   public void addCreditCardTypeField(String fieldName, String errorMsg)
   {
	   CreditCardValidationRule rule = new CreditCardValidationRule(fieldName, errorMsg, CreditCardValidationRule.CREDIT_CARD_TYPE);
	   addValidatorRule(rule);
   }
   
   /**
    * Runs the validation in the validation rules
    * @param parameters data which holds a list of fields and their values.
    * @return map of the field names and their error message, empty map if there is no error.
    */
   public synchronized ErrorMap validate(TableData parameters) 
   {
	   // The result (ErrorMap) needs synchronized 
	   ErrorMap result = new ErrorMap();
	   
	   if (ruleList == null)
		   throw new RuntimeException("The list of rules is null.");
	   
	   // TODO: Fix the warning. The ErrorMap should be changed so that it extends HashMap<String, String) instead of raw type.
	   for (ValidationRule rule: ruleList)
	   {
		   boolean validateResult = rule.validate(parameters);
		   if (!validateResult)
			   result.put(rule.fieldName, rule.errorMsg);
	   }
	   return result;
   }
   
   /**
    * Runs the validation in the validation rules. This can return multiple error messages for one field
    * @param parameters data which holds a list of fields and their values.
    * @return map of the field names and their list of error messages, empty map if there is no error.
    */
   public synchronized HashMap<String, ArrayList<String>> validateWithMultipleErrors(TableData parameters) 
   {
	   // The result needs synchronized 
	   HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
	   
	   if (ruleList == null)
		   throw new RuntimeException("The list of rules is null.");
	   
	   for (ValidationRule rule: ruleList)
	   {
		   boolean validateResult = rule.validate(parameters);
		   if (!validateResult)
		   {
			   ArrayList<String> errorMessages = result.get(rule.fieldName);
			   if (errorMessages == null)
				   errorMessages = new ArrayList<String>();
			   errorMessages.add(rule.errorMsg);
			   result.put(rule.fieldName, errorMessages);
		   }
	   }
	   return result;
   }
   
   /**
    * Adds a validation rule into the validation rule list.
    * @param validationRule the validation rule to be added
    */
   public void addValidatorRule(ValidationRule validationRule)
   {
	   if (ruleList == null)
	   {
		   throw new RuntimeException("The list of rules is null.");
	   }
	   ruleList.add(validationRule);
   }
}