package com.sok.runway;

import javax.servlet.*;
import javax.servlet.http.*;

import java.util.*;
import java.io.*;

import com.sok.framework.*;
import com.sok.runway.offline.*;

public abstract class OfflineProcess implements OfflineNotifier
{
	protected StringBuffer errors = null;
	
   protected ServletContext context;
	/*private String smtpHost = null;
	private String smtpUsername = null;
	private String smtpPassword = null;
   private String smtpPort = null;
   */
   
   protected String dbconn;
   protected Thread thisthread;
   protected Locale thislocale;
   protected RowBean requestbean;
   protected String contextpath;
   
   protected String processID = null;
   protected String processKiller = null;
   protected String lastError = null;
   
   protected String contextname = null;
   
   protected boolean debug = false;	
   protected boolean started = false;	
	
	public OfflineProcess()
	{
		
	}
	public OfflineProcess(HttpServletRequest request, ServletContext context)
	{
		setVar(request, context);
		errors = new StringBuffer();		
		requestbean = new RowBean();
		requestbean.parseRequest(request);
	}

	public OfflineProcess(String query, HttpServletRequest request, ServletContext context)
	{
		setVar(request, context);
		errors = new StringBuffer();		
		requestbean = new RowBean();
		requestbean.parseRequest(query);
	}
	
	public OfflineProcess(RowBean bean, HttpServletRequest request, ServletContext context)
	{
		setVar(request, context);
		errors = new StringBuffer();		
		requestbean = bean;
	}	
	
	public boolean sendEmail() {
	   return true;
	}
	

	protected void start() {
	   
	   if (!started) {
   		started = true;
   	   processID = KeyMaker.generate();
   	   
   	   OfflineProcessMonitor.getOfflineProcessMonitor().addOfflineProcess(this);
   		OfflineProcessor op = new OfflineProcessor(this);
   		thisthread = new Thread(op,
   				new StringBuilder(20)
   						.append(this.getClass().getName())
   						.append("-OfflineProcessThread-")
   						.append(ThreadManager.getNextThreadId()).toString());
   		try {
   		   thisthread.setPriority(Thread.MIN_PRIORITY);
   		}
   		catch (Exception e) {
   		} 
   		ThreadManager.startThread(thisthread);
   		//thisthread.start();	
   		}
	}
	
	protected void setVar(HttpServletRequest request, ServletContext context)
	{
		this.context = context;
		contextname = context.getServletContextName();
		contextpath = request.getContextPath();
		/*smtpHost = (String)context.getInitParameter("SMTPHost");
		smtpUsername = (String)context.getInitParameter("SMTPUsername");
		smtpPassword = (String)context.getInitParameter("SMTPPassword");
        smtpPort = (String)context.getInitParameter("SMTPPort");
        */
		dbconn = (String)context.getInitParameter("sqlJndiName");
		thislocale = request.getLocale();
		//String timezonestring = (String)context.getInitParameter("timeZone");
	}


	public void runProcess() {
	   execute();
	   OfflineProcessMonitor.getOfflineProcessMonitor().removeOfflineProcess(this);
	}
	
	public void kill(String processKiller) {
	   this.processKiller = processKiller;
	}
	
	public boolean isKilled() {
	   return processKiller != null;
	}
	
	public String getKiller() {
	   return processKiller;
	}
		
	public abstract void execute();

   /**
    *  
    
	public String getStatusMailHost() {
		return getSMTPHost();
	}*/
	
	/*public String getSMTPHost() {
		return(smtpHost);
	}
	public String getSMTPUsername() {
	   if (smtpUsername == null) {
		   return "";
	   }
	   else {
		   return(smtpUsername);
		}
	}
	public String getSMTPPassword() {
	   if (smtpPassword == null) {
		   return "";
	   }
	   else {
		   return(smtpPassword);
		}
	}    
    
    public String getSMTPPort()
    {
        return smtpPort;
    }*/

	public abstract String getStatusMailBody();
	
	public String getStatusMailHtmlBody() {
		return null;
	}

	public abstract String getStatusMailSubject();

	public abstract String getStatusMailRecipient();

    public Locale getLocale()
	{
		return(thislocale);
	}


   public String getProcessID() {
      return processID;
   }
   
   /*public abstract int getProcessSize();
   
   public abstract int getProcessedCount();
   
   public abstract int getErrorCount();*/
   
   public String getLastError() {
      return lastError;
   }
   
   public void setLastError(String lastError) {
      this.lastError = lastError;
   }
   
   public File getStatusAttachment() {
	   return(null);
   }
   
   public void appendError(String msg, String id)
   {
      //String error = msg + " - " + id + "\r\n";
	   /*errors.append(msg);
	   errors.append(" - ");
	   errors.append(id);
	   errors.append("\r\n");*/
	   setLastError(msg + " - " + id + "\r\n");	   
   }
   
   public void appendError(String error)
   {
	   //errors.append(error);
	   //errors.append("\r\n");
	   setLastError(error + "\r\n");
   } 
   
   public String getError()
   {
	   return(errors.toString());
   }
   
   public boolean isDebug()
   {
	   return(debug);
   }
}
