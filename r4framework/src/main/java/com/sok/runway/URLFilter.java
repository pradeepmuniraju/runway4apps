package com.sok.runway;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

import com.sok.framework.ActionBean;
import com.sok.framework.DBConnectionFilter;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.DatabaseException;
import com.sok.framework.sql.DatabaseSynchronizationListener;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

public class URLFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(URLFilter.class);
	public static String suffix = ".sok";
	public static String linksuffix = ".link";
	private static Map<String, String> shortURLMap = null;
	private static Map<String, String> specialHostMap = null;
	String mainpage = null;
	String linkResponsePage = null;
	ServletContext servletContext = null;

	@SuppressWarnings("unchecked")
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws ServletException, IOException {
		HttpServletRequest httprequest = (HttpServletRequest) request;
		HttpServletResponse httpresponse = (HttpServletResponse) response;		
		String requesturl = httprequest.getRequestURI();
		/* set the original url as an attribute so that it can be grabbed by 404 pages and such. */
		request.setAttribute("-originalUrl", requesturl);

		String contextPath = httprequest.getContextPath();

		//you want it, turn on trace logging.
		logger.trace("[{}?{}]", httprequest.getRequestURI(), httprequest.getQueryString());

		if (shortURLMap == null) {
			shortURLMap = loadShortURL();
		}
		if (specialHostMap == null) {
			specialHostMap = loadSpecialHost();
		}
		String url = requesturl.substring(contextPath.length() + 1);
		String domainurl = request.getServerName() + url, shorturl = null;
		
		if (url.toLowerCase().indexOf(".pdf") >= 0) {
			((HttpServletResponse) response).setHeader("x-robots-tag","noindex, nofollow" );
		}
		
		if (url.toLowerCase().indexOf(".jsp") >= 0 || url.toLowerCase().indexOf(".pdf") >= 0) {
			if(!request.isSecure()){
				//no cache breaks downloading pdf in ie over https
				((HttpServletResponse) response).setHeader("Expires","Mon, 26 Jul 1997 05:00:00 GMT" );  // disable IE caching
				((HttpServletResponse) response).setHeader("Cache-Control","no-cache, must-revalidate" );
				((HttpServletResponse) response).setHeader("Pragma","no-cache" );
				((HttpServletResponse) response).setDateHeader ("Expires", 0);
			} else {
				((HttpServletResponse) response).setHeader("Expires","Mon, 26 Jul 1997 05:00:00 GMT" );  // disable IE caching
				((HttpServletResponse) response).setHeader("Pragma","no-cache" );
			}
		} else if (url.toLowerCase().indexOf(".jpg") >= 0 || url.toLowerCase().indexOf(".png") >= 0 || url.toLowerCase().indexOf(".gif") >= 0 || url.toLowerCase().indexOf(".css") >= 0) {
			if(!request.isSecure()){
				//no cache breaks downloading pdf in ie over https
				Date d = new Date();
				d.setTime(d.getTime() + (14 * 24 * 60 * 60 * 1000));
				((HttpServletResponse) response).setHeader("Expires",d.toString());  // disable IE caching
				((HttpServletResponse) response).setDateHeader ("Expires", d.getTime());
				((HttpServletResponse) response).setHeader("Cache-Control","cache, max-age" );
				((HttpServletResponse) response).setHeader("Pragma","cache" );
			}
		} else if (url.toLowerCase().indexOf(".js") >= 0) {
			if(!request.isSecure()){
				//no cache breaks downloading pdf in ie over https
				Date d = new Date();
				d.setTime(d.getTime() + (7 * 24 * 60 * 60 * 1000));
				((HttpServletResponse) response).setHeader("Expires",d.toString());  // disable IE caching
				((HttpServletResponse) response).setDateHeader ("Expires", d.getTime());
				((HttpServletResponse) response).setHeader("Cache-Control","cache, max-age" );
				((HttpServletResponse) response).setHeader("Pragma","cache" );
			}
		}

		if(shortURLMap.containsKey(domainurl)) {
			shorturl = shortURLMap.get(domainurl);
		} else if(shortURLMap.containsKey(url)) {
			shorturl = shortURLMap.get(url);
		}
		HttpSession session = (HttpSession) httprequest.getSession();
		if (session != null) {
			UserBean currentuser = (UserBean) session.getAttribute("currentuser");
			if (currentuser != null && "Y".equals(currentuser.getString("LogAllRequests"))) exceptionLogger(this, httprequest, null);
			else if ("true".equals(InitServlet.getSystemParam("LogAllRequests"))) exceptionLogger(this, httprequest, null);
		}


		if(shorturl != null) {
			requesturl = shorturl;
			/* Suspect this does a similar thing to the above, not sure tho */
			if (requesturl.startsWith("/")) {
				if (requesturl.indexOf("/", 1) > 1) {
					String subpath = requesturl.substring(1, requesturl.indexOf("/", 1));
					logger.trace("SubPath {}", subpath);  
					if (specialHostMap.containsKey(subpath)) {
						String Host = specialHostMap.get(subpath);
						//httprequest.setAttribute("Host", Host );
						logger.trace("Host {} -> {}", httprequest.getServerName(), Host);
						if (Host.toLowerCase().indexOf(httprequest.getServerName()) == -1) {
							logger.trace("Redirect to  {}{}", Host, url);	
							httpresponse.sendRedirect(Host + url);
							return;
						}
						logger.trace("Skipping to  {}{}", Host, url);
					}
				}
			}
			httprequest.setAttribute("Path", requesturl);
			RequestDispatcher dispatcher = servletContext.getRequestDispatcher(mainpage);
			/* we don't know the url path, so we can't map the dbconnection filter to them - so we must add and close the request connections */
			DBConnectionFilter.setConnection(request);
			try { 
				dispatcher.forward(httprequest, response);
			} finally { 
				DBConnectionFilter.closeConnection(request);
			}

		} else if (requesturl.endsWith(suffix)) {
			requesturl = httprequest.getRequestURI()
					.substring(contextPath.length(),
							requesturl.length() - suffix.length());
			httprequest.setAttribute("Path", requesturl);
			RequestDispatcher dispatcher = servletContext
					.getRequestDispatcher(mainpage);
			dispatcher.forward(httprequest, response);
		} else if (requesturl.endsWith(linksuffix)) {
			requesturl = httprequest.getRequestURI().substring(
					contextPath.length() + 1,
					requesturl.length() - linksuffix.length());
			httprequest.setAttribute("TemplateURLID", requesturl);
			RequestDispatcher dispatcher = servletContext.getRequestDispatcher(linkResponsePage);
			dispatcher.forward(httprequest, response);
		} else if(requesturl.indexOf("/specs/") != -1) {
			httpresponse.setStatus(403);
		} else if(requesturl.endsWith("/runway") || requesturl.endsWith("/runway/")) {
			if (!requesturl.endsWith("/")) requesturl += "/";
			requesturl = "/crm/main.jsp";
			httpresponse.setStatus(301);
			RequestDispatcher dispatcher = servletContext.getRequestDispatcher(requesturl);
			dispatcher.forward(httprequest, httpresponse);
		} else {
			try {
				if(!DatabaseSynchronizationListener.hasStartupErrors()) {
					chain.doFilter(request, response);
				} else { 
					boolean found = false; 
					for(String s: new String[]{"jpg","js","png","gif","jpeg","css","html"}) {
						if(requesturl.endsWith(s)) {
							chain.doFilter(request, response);
							found = true; 
							break;
						}
					}
					if(!found) {
						// Set the attribute so that it can be picked up correctly by a jsp error page.
						request.setAttribute(PageContext.EXCEPTION /* JEE6 RequestDispatcher.ERROR_EXCEPTION */, new RuntimeException("Error in database synchronization : \n" + StringUtils.join(DatabaseSynchronizationListener.getStartupErrors(), "\n")));
						RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/errors/ioerror.jsp");
						dispatcher.forward(httprequest, response);
					}
				} 
			} catch (Exception e) {
				logger.error("Exception in URLFilter path [{}?{}]", httprequest.getRequestURI(), httprequest.getQueryString()); 
				if ((e.getMessage() != null && e.getMessage().indexOf("not found") != -1) || e instanceof FileNotFoundException) {
					try {
						exceptionLogger(this, httprequest, e);
					} catch (Exception e1) {}
					httpresponse.setStatus(404);
				} else {
					try {
						exceptionLogger(this, httprequest, e);
					} catch (Exception e1) {}
					httpresponse.setStatus(500);
				}
				return; 
			}
		}
	}

	/**
	 * This method will print up a nice list of parameters for debugging purposes
	 * 
	 * @param c The class calling this method
	 * @param request A HTTP request
	 * @param e The Exception
	 */
	@SuppressWarnings("unchecked")
	public static void exceptionLogger(Object c, HttpServletRequest request, Exception e) {
		try {
			String key = KeyMaker.generate(3);
			Enumeration<String> param = null;
			if(JSPLogger.isErrorEnabled(c)) {
				if (e == null)
					JSPLogger.error(c,"--------------- Start User Log -------------------------------");
				else
					JSPLogger.error(c,"--------------- Start Exception -------------------------------");
				if (request != null) {
					HttpSession session = (HttpSession) request.getSession();
					if (session != null) {
						UserBean currentuser = (UserBean) session.getAttribute("currentuser");
						if (currentuser != null) JSPLogger.error(c,key + " UserID = {} {} {}", new String[]{currentuser.getUserID(), currentuser.getFirstName(), currentuser.getLastName()});
					}
					JSPLogger.error(c,key + " Remote = {}", request.getRemoteAddr());

					try {
						param = request.getParameterNames();
						while (param != null && param.hasMoreElements()) {
							String s = (String) param.nextElement();
							JSPLogger.error(c,key + " Param {} = {}", s, request.getParameter(s));
						}
					} catch (Exception e1) {}
					try {
						param = request.getAttributeNames();
						while (param != null && param.hasMoreElements()) {
							String s = (String) param.nextElement();
							JSPLogger.error(c,key + " Attrib {} = {}", s, request.getAttribute(s));
						}
					} catch (Exception e1) {}
					try {
						param = request.getHeaderNames();
						while (param != null && param.hasMoreElements()) {
							String s = (String) param.nextElement();
							JSPLogger.error(c,key + " Header {} = {}", s, request.getHeader(s));
						}
					} catch (Exception e1) {}
					try {
						if (session != null) {
							param = session.getAttributeNames();
							while (param != null && param.hasMoreElements()) {
								String s = (String) param.nextElement();
								JSPLogger.error(c,key + " Session {} = {}", s, session.getAttribute(s));
							}
						}
					} catch (Exception e1) {}
				}
			}
			if (e != null) {
				JSPLogger.error(c,key + " Exception is ", e);
				JSPLogger.error(c,"--------------- End Exception -------------------------------");
			} else {
				JSPLogger.error(c,"--------------- End User Log -------------------------------");
			}
		} catch (Exception e1) {}
	}

	public static void exceptionLogger(HttpServletRequest request, Exception e) {
		exceptionLogger(new Object(), request, e);
	}

	public void init(FilterConfig config) throws ServletException {
		mainpage = config.getInitParameter("mainPage");
		linkResponsePage = config.getInitParameter("linkResponsePage");
		servletContext = config.getServletContext();
	}


	public static synchronized Map<String, String> loadShortURL() {
		return loadShortURL(false);
	}

	/**
	 * Method to do the initial load of shortURLs.
	 */
	public static synchronized Map<String, String> loadShortURL(boolean reload) {
		if (shortURLMap == null || reload) {
			GenRow shortURLQuery = new GenRow();
			try {
				HashMap<String, String>shortURLMap = new HashMap<String, String>();
				String query = "SELECT distinct ShortURL, Path, CMSDomains.Domain from PageLinks left outer join CMSDomains on PageLinks.CMSID = CMSDomains.CMSID where ShortURL <> ''";
				shortURLQuery.setStatement(query);
				ActionBean.connect(shortURLQuery);
				shortURLQuery.getResults();
				while (shortURLQuery.getNext()) {
					String shortURL = shortURLQuery.getString("ShortURL");
					String Path = shortURLQuery.getString("Path");
					String domain = shortURLQuery.getString("Domain");
					if(StringUtils.isBlank(domain)) {
						shortURLMap.put(shortURL, Path);
					} else {
						shortURLMap.put(domain + shortURL, Path);
					}
				}
				URLFilter.shortURLMap = shortURLMap;
			} catch (DatabaseException de) {
				logger.error("Database connection issues loading shortURL ", de); 
				shortURLMap = Collections.emptyMap();
			} finally {
				shortURLQuery.close();
			}
		}
		return shortURLMap;
	}

	/**
	 * Method to do the initial load of shortURLs.
	 * @deprecated - use CMSDomains
	 */
	public static synchronized Map<String, String> loadSpecialHost() {
		if (specialHostMap == null) {
			GenRow specialHostQuery = new GenRow();
			try {
				ActionBean.connect(specialHostQuery);
				specialHostMap = new HashMap<String, String>();
				String query = "SELECT distinct Host, ShortPath from specialHost";

				specialHostQuery.setStatement(query);
				specialHostQuery.getResults();
				while (specialHostQuery.getNext()) {
					String Host = specialHostQuery.getString("Host");
					String Path = specialHostQuery.getString("ShortPath");
					specialHostMap.put(Path, Host);
					logger.trace("Setting specialHostPath {} = {}", Path, Host);
				}
			} catch (DatabaseException de) {
				logger.error("Database connection issues loading specialHost - {}", de.getMessage()); 
				specialHostMap = Collections.emptyMap();
			} finally {
				specialHostQuery.close();
			}
		}
		return specialHostMap;
	}

	/**
	 * Method to reload ShortURLs, which would be called when a shortURL is
	 * updated via Runway. Method added for backwards compatibility.
	 * 
	 * @param pageID
	 */
	public static synchronized void reloadHashMap(String pageID) {
		reloadHashMap();
		specialHostMap = null;
		loadSpecialHost();
	}

	/**
	 * added on 06/07/2009, it is used in
	 * \jsp\crm\cms\sites\action\pageaction.jsp Method to reload ShortURLs,
	 * which would be called when a shortURL is updated via Runway. Method added
	 * for backwards compatibility.
	 * 
	 * @param pageID
	 *            @
	 */
	public static synchronized void reloadHashMap(HttpServletRequest request,
			String pageID) {
		reloadHashMap();
		specialHostMap = null;
		loadSpecialHost();
	}

	/**
	 * Method to reload ShortURLs, which would be called when a shortURL is
	 * updated via Runway.
	 */
	public static synchronized void reloadHashMap() {

		shortURLMap = new HashMap<String, String>();
		GenRow shortURLQuery = new GenRow();
		try {
			String query = "SELECT distinct ShortURL,Path from PageLinks where ShortURL <> ''";
			shortURLQuery.setStatement(query);
			shortURLQuery.getResults();
			while (shortURLQuery.getNext()) {
				String shortURL = shortURLQuery.getString("ShortURL");
				String Path = shortURLQuery.getString("Path");
				shortURLMap.put(shortURL, Path);
				logger.trace("Setting shortURLPath {} = {}", shortURL, Path);
			}
		} catch (DatabaseException de) {
			logger.error("Database connection issues reloading shortURL ", de);
		} finally {
			shortURLQuery.close();
		}
	}

	public void destroy() {
		if(specialHostMap != null) {
			specialHostMap.clear(); 
			specialHostMap = null;
		}
		if(shortURLMap != null) { 
			shortURLMap.clear();
			shortURLMap = null;
		}
	}

	public static void addShortURL(String cmsID, String shortURL, String path) {
		//TODO - load specific short url - add domain / url combinations to map 
		loadShortURL(true);
	}
}
