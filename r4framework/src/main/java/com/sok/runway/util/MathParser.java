/**
 * 
 */
package com.sok.runway.util;

import java.util.Map;

/**
 * @author dion
 * 
 */
public class MathParser {

	/**
	 * 
	 */
	public MathParser() {
		// TODO Auto-generated constructor stub
	}

	public double eval(String expression) {
		int lastLP = expression.lastIndexOf("(");
		while (lastLP >= 0) {
			int firstRP = expression.indexOf(")", lastLP);
			// if we don't have a match break out
			if (firstRP == -1)
				break;
			String sub = expression.substring(lastLP + 1, firstRP);
			double d = eval(sub);
			String D = "" + d;
			if (lastLP > 0 && firstRP < expression.length() - 1) {
				expression = expression.substring(0, lastLP - 1) + D
						+ expression.substring(firstRP + 1);
			} else if (lastLP == 0 && firstRP < expression.length()) {
				expression = D + expression.substring(firstRP + 1);
			} else if (lastLP > 0 && firstRP >= expression.length() - 1) {
				expression = expression.substring(0, lastLP - 1) + D;
			} else {
				expression = D;
			}
			lastLP = expression.lastIndexOf("(");
		}

		int firstTimes = expression.indexOf("*");
		int firstDiv = expression.indexOf("/");
		int first = -1;
		if (firstTimes >= 0 && firstDiv == -1) {
			first = firstTimes;
		} else if (firstTimes == -1 && firstDiv >= 0) {
			first = firstDiv;
		} else if (firstTimes < firstDiv) {
			first = firstTimes;
		} else if (firstTimes > firstDiv) {
			first = firstDiv;
		}
		while (first >= 0) {

		}
		System.out.println(expression);

		return 0;
	}

	public double eval(String expression, Map values) {

		return 0;
	}

}
