/**
 * User: Behrang
 * Date: Jul 7, 2010
 * Time: 1:52:04 PM 
 * 
 * Updated: May 18, 2011
 * Features: Ability to re-use link builder with different params.
 */
package com.sok.runway.util;
import java.util.HashMap;
import java.util.Map;
import com.sok.framework.ActionBean;

/**
 * A helper class for building URLs. 
 */
public class LinkBuilder {

	private String base = ActionBean._blank;
	private final Map<String, String> params = new HashMap<String, String>();
	private static final char q = '?';
	private static final char e = '=';
	private static final char a = '&';

    /**
     * Sets the base path of the URL ie contextPath / whatever.
     * @param base
     * @return this
     */
    public LinkBuilder base(String base) {
    	this.base = base;
        return this;
    }
    
    /**
     * Sets a link parameter
     * @param name
     * @param value
     * @return this
     */
    public LinkBuilder param(String name, String value) {
        params.put(name, value);
        return this;
    }
    
    /**
     * removes a link parameter
     * @param name
     * @return this
     */
    public LinkBuilder clearParam(String name) { 
    	params.remove(name);
    	return this;
    }
    
    /**
     * Clears all currently set values for the object.
     * @return this
     */
    public LinkBuilder clear()
    {
    	base = ActionBean._blank;
    	params.clear();
    	return this;
    }
    
    /**
     * Returns a link representation of the base url and all currently set params.
     * i.e. base ? param.name = param.value & param2.name = param2.value etc
     * @return link as String.
     */
    public String toString() {
    	final StringBuilder link = new StringBuilder(base).append(q);
    	for(Map.Entry<String, String> p: params.entrySet()) 
    	{
    		link.append(p.getKey()).append(e).append(p.getValue()).append(a);
    	}
        if (link.charAt(link.length() - 1) == a) {
            link.setLength(link.length() - 1);
        }
        return link.toString();
    }
}
