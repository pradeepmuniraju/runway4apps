package com.sok.runway.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.InitServlet;
import com.sok.framework.Scalr;
import com.sok.framework.Scalr.Mode;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.cms.properties.PropertyEntity.ResizeOption;


public class ImageResizeUtil {

		private static final Logger logger = LoggerFactory.getLogger(ImageResizeUtil.class);
		
		
		/**
		 * This is utility method to resize the image by keeping the current heightXwidth ratio. If the newWidth and newHeight 
		 * break the current ratio of the image then the Scalr.resize method will return the best possible smallest image. 
		 * Scalr is a thirparty java class used to resize the images in Runway. Don't assume that the returned image from this
		 * method is of size newHeight X newWidth. The Scalr.resize method will decide the best possible smallest image for the
		 * supplied image.
		 * 
		 * @param inPath
		 * @param newWidth
		 * @param newHeight
		 * @param option - is getting ignored in the method. 
		 * @param desiredOutputExtension
		 * @return the newly generated file location.
		 */
		public static String imageResizeByHeightWidth (String inPath, int newWidth, int newHeight, ResizeOption option, String desiredOutputExtension)
		{
			// Sample HomePath: "http://localhost:8080/Runway4"
			// Sample inFilePath: "C:\Program Files\Apache Software Foundation\Tomcat 5.5\webapps\Runway4\files\F180D2D08404F1241723606192C82747\0R1Q3X5Z8U1Z2A792C578U8A9B33\VERVE24_RH_WEB.gif"
			// Sample outFilePath: "C:\Program Files\Apache Software Foundation\Tomcat 5.5\webapps\Runway4\files\F180D2D08404F1241723606192C82747\0R1Q3X5Z8U1Z2A792C578U8A9B33\VERVE24_RH_WEB_Pad_800x600.gif"
				
			String resizedFileNamePostFix = "_" +  option.name() + "_" + newWidth + "x" + newHeight + "." + desiredOutputExtension; 
			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			
			String inFilePath = inPath;
			String outFilePath = new String(inFilePath.getBytes());
			outFilePath = outFilePath.replaceAll(" ", "_");
			String imgExt = outFilePath.substring(outFilePath.lastIndexOf("."));
			outFilePath = outFilePath.replaceAll(imgExt, resizedFileNamePostFix);
			
			String finalOutPutFilePath = homePath + "/" + outFilePath;
			
			boolean isConversionNeeded =  !imgExt.contains(desiredOutputExtension);
					
			String imgStr = outFilePath;
			//imgStr = imgStr.replace(" ", "%20");
			if (!StringUtil.isBlankOrEmpty(imgStr))
			{	
				if(imgStr.indexOf(homePath) == 0) {
					imgStr = imgStr.substring(homePath.length());
				}
				if(imgStr.startsWith("/")) {
					imgStr = imgStr.substring(1);
				}
			}
			outFilePath = InitServlet.getRealPath("/" + imgStr);


			if (!StringUtil.isBlankOrEmpty(inFilePath))
			{	
				if(inFilePath.indexOf(homePath) == 0) {
					inFilePath = inFilePath.substring(homePath.length());
				}
				if(inFilePath.startsWith("/")) {
					inFilePath = inFilePath.substring(1);
				}
			}
			inFilePath = InitServlet.getRealPath("/" + inFilePath);
			//inFilePath = homePath + "/" + inPath;
			
			System.out.println("homePath is: " + homePath);
			System.out.println("inFilePath: " + inFilePath);
			System.out.println("outFilePath: " + outFilePath);
			System.out.println("finalOutPutFilePath: " + finalOutPutFilePath);
			
			
			logger.trace("homePath: ", homePath);
			logger.trace("inFilePath: ", inFilePath);
			logger.trace("outFilePath: ", outFilePath);
			logger.trace("finalOutPutFilePath: ", finalOutPutFilePath);
			

			File outFile = null;
			File inFile = null;
			try
			{
				outFile = new File(outFilePath);
				if(outFile.exists())
				{
					inFile = new File(inFilePath);
					long inLastModificationDate = inFile.lastModified();
					long outLastModificationDate = outFile.lastModified();
					
					if (inLastModificationDate > outLastModificationDate)
					{
//						finalOutPutFilePath = PropertyEntity.resizeImage(inPath, newWidth, newHeight, option, true, desiredOutputExtension);


						BufferedImage i = ImageIO.read(inFile);
						
						BufferedImage resizedImage = Scalr.resize(i, Mode.AUTOMATIC, newWidth, newHeight);
						
						if(isConversionNeeded)
							convertFormat(resizedImage, outFile, desiredOutputExtension, newWidth, newHeight, imgExt);
						else						
							ImageIO.write(resizedImage, desiredOutputExtension, new FileOutputStream(outFile));
						
						System.out.println("Moved as inLastModificationDate is greater. newResizedPath 01: " + finalOutPutFilePath);
						logger.trace("Moved as inLastModificationDate is greater. finalOutPutFilePath 01:  ", finalOutPutFilePath);
						return finalOutPutFilePath;
					}	
				}
				else
				{
//						finalOutPutFilePath = PropertyEntity.resizeImage(inPath, newWidth, newHeight, option, true, desiredOutputExtension);
					inFile = new File(inFilePath);
					BufferedImage i = ImageIO.read(inFile);
					BufferedImage resizedImage = Scalr.resize(i, Mode.AUTOMATIC, newWidth, newHeight);
					
					if(isConversionNeeded) {						
						convertFormat(resizedImage, outFile, desiredOutputExtension, newWidth, newHeight, imgExt);
					}
					else						
						ImageIO.write(resizedImage, desiredOutputExtension, new FileOutputStream(outFile));
					
					System.out.println("Moved First-time. newResizedPath 02: " + finalOutPutFilePath);
					logger.trace("Moved First-time. newResizedPath 02: " + finalOutPutFilePath);
					return finalOutPutFilePath;
				}
				
			}
			catch(Exception ee)
			{
				System.out.println("Exception occurred. Message is: " + ee.getMessage());
				ee.printStackTrace();
			}
			finally
			{
				if (outFile != null)
				{
					outFile = null;
				}	
				
				if (inFile != null)
				{
					inFile = null;
				}
			}
			
			return finalOutPutFilePath;
		}
		
		
		
		
		
		
		
		
		/*
		 *  This will return the file path like below.
		 *  outFilePath: C:\Program Files\Apache Software Foundation\Tomcat 5.5\webapps\Runway4\files\0H1S2H8M4M6G0B9N9P2N1A652F5I\0J1S3P0B7Y6G884G0O5L6Q1K9F5M\A5_Aero_Chandler_Facade_Crop_800x600.jpg 
		 */
		public static String imageResizeByHeightWidthAbsPathOutput (String inPath, int newWidth, int newHeight, ResizeOption option, String desiredOutputExtension)
		{
			// Sample HomePath: "http://localhost:8080/Runway4"
			// Sample inFilePath: "C:\Program Files\Apache Software Foundation\Tomcat 5.5\webapps\Runway4\files\F180D2D08404F1241723606192C82747\0R1Q3X5Z8U1Z2A792C578U8A9B33\VERVE24_RH_WEB.gif"
			// Sample outFilePath: "C:\Program Files\Apache Software Foundation\Tomcat 5.5\webapps\Runway4\files\F180D2D08404F1241723606192C82747\0R1Q3X5Z8U1Z2A792C578U8A9B33\VERVE24_RH_WEB_Pad_800x600.gif"
				
			String resizedFileNamePostFix = "_" +  option.name() + "_" + newWidth + "x" + newHeight + "." + desiredOutputExtension; 
			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			
			String inFilePath = inPath;
			String outFilePath = new String(inFilePath.getBytes());
			outFilePath = outFilePath.replaceAll(" ", "_");
			String imgExt = outFilePath.substring(outFilePath.lastIndexOf("."));
			outFilePath = outFilePath.replaceAll(imgExt, resizedFileNamePostFix);
			
			String finalOutPutFilePath = homePath + "/" + outFilePath;
			

			String imgStr = outFilePath;
			//imgStr = imgStr.replace(" ", "%20");
			if (!StringUtil.isBlankOrEmpty(imgStr))
			{	
				if(imgStr.indexOf(homePath) == 0) {
					imgStr = imgStr.substring(homePath.length());
				}
				if(imgStr.startsWith("/")) {
					imgStr = imgStr.substring(1);
				}
			}
			outFilePath = InitServlet.getRealPath("/" + imgStr);


			if (!StringUtil.isBlankOrEmpty(inFilePath))
			{	
				if(inFilePath.indexOf(homePath) == 0) {
					inFilePath = inFilePath.substring(homePath.length());
				}
				if(inFilePath.startsWith("/")) {
					inFilePath = inFilePath.substring(1);
				}
			}
			inFilePath = InitServlet.getRealPath("/" + inFilePath);
			//inFilePath = homePath + "/" + inPath;
			
			System.out.println("homePath is: " + homePath);
			System.out.println("inFilePath: " + inFilePath);
			System.out.println("outFilePath: " + outFilePath);
			System.out.println("finalOutPutFilePath: " + finalOutPutFilePath);
			System.out.println("newWidth: " + newWidth + " , newHeight: " + newHeight);
			
			
			logger.trace("homePath: ", homePath);
			logger.trace("inFilePath: ", inFilePath);
			logger.trace("outFilePath: ", outFilePath);
			logger.trace("finalOutPutFilePath: ", finalOutPutFilePath);
			logger.trace("newWidth: " + newWidth + " , newHeight: " + newHeight);
			

			File outFile = null;
			File inFile = null;
			try
			{
				outFile = new File(outFilePath);
				if(outFile.exists())
				{
					inFile = new File(inFilePath);
					long inLastModificationDate = inFile.lastModified();
					long outLastModificationDate = outFile.lastModified();
					
					if (inLastModificationDate > outLastModificationDate)
					{
//						finalOutPutFilePath = PropertyEntity.resizeImage(inPath, newWidth, newHeight, option, true, desiredOutputExtension);


						BufferedImage i = ImageIO.read(inFile);
						ImageIO.write(Scalr.resize(i, Mode.AUTOMATIC, newWidth, newHeight), desiredOutputExtension, new FileOutputStream(outFile));
						System.out.println("Moved as inLastModificationDate is greater. newResizedPath 01: " + finalOutPutFilePath);
						logger.trace("Moved as inLastModificationDate is greater. finalOutPutFilePath 01:  ", finalOutPutFilePath);
						return outFilePath;
					}	
				}
				else
				{
//						finalOutPutFilePath = PropertyEntity.resizeImage(inPath, newWidth, newHeight, option, true, desiredOutputExtension);
					inFile = new File(inFilePath);
					BufferedImage i = ImageIO.read(inFile);
					ImageIO.write(Scalr.resize(i, Mode.AUTOMATIC, newWidth, newHeight), desiredOutputExtension, new FileOutputStream(outFile));
					System.out.println("Moved First-time. newResizedPath 02: " + finalOutPutFilePath);
					logger.trace("Moved First-time. newResizedPath 02: " + finalOutPutFilePath);
					return outFilePath;
				}
				
			}
			catch(Exception ee)
			{
				System.out.println("Exception occurred. Message is: " + ee.getMessage());
				ee.printStackTrace();
			}
			finally
			{
				if (outFile != null)
				{
					outFile = null;
				}	
				
				if (inFile != null)
				{
					inFile = null;
				}
			}
			
			return outFilePath;
		}
		
		public static void convertFormat(BufferedImage inputBuffImage, File outputFile, String formatName, int newWidth, int newHeight, String actualFormat) throws IOException {
			String tmp = outputFile.getAbsolutePath().replace(formatName, actualFormat.substring(1)); 
			
			
			FileOutputStream tmpOutputStream = new FileOutputStream(new File(tmp));			
			ImageIO.write(inputBuffImage, actualFormat.substring(1), tmpOutputStream);
			
			if(tmpOutputStream != null) {
				tmpOutputStream.flush(); 
				tmpOutputStream.close();
			}
			
			// Write the resized file  and read again as it does not work otherwise
			FileInputStream inputStream = new FileInputStream(tmp);
			BufferedImage inputImage = ImageIO.read(inputStream);
			
			ImageOutputStream ios = null;
			ImageWriter iw = null;

			try {
				// writer
				iw = ImageIO.getImageWritersByFormatName(formatName).next();

				// input
				BufferedImage tmpImg = new BufferedImage(newWidth, newHeight, inputImage.getType());
				Graphics2D g = tmpImg.createGraphics();
				g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				g.drawImage(inputImage, 0, 0, Color.white, null);
				g.dispose();

				inputImage = tmpImg;

				// output
				ios = ImageIO.createImageOutputStream(outputFile);

				if (ios == null) {
					throw new RuntimeException("Output Stream was null, probably because you don't have write access.");
				}

				iw.setOutput(ios);

				ImageWriteParam iwParam = iw.getDefaultWriteParam();
				if ("jpg".equals(formatName)) {
					// Set the compression quality to 0.9f.
					iwParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
					iwParam.setCompressionQuality(0.9f);
				}
				// Write image
				iw.write(null, new IIOImage(inputImage, null, null), iwParam);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					ios.flush();
					iw.dispose();
					ios.close();
				} catch (Exception e) {
				}
			}
		}
		
		
		
		
		
		
		public static void main (String any [])
		{
			String inFile = "/specific/pdf/images/RnD/CHELSEA-III.gif";
			
			String floorPlanImgPath = ImageResizeUtil.imageResizeByHeightWidth (inFile, 800, 600, ResizeOption.Pad, "gif");
			System.out.println("floorPlanImgPath: " + floorPlanImgPath);
		}
}
