package com.sok.runway.util;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class AdaptorCDATA extends XmlAdapter<String, String> {

    @Override
    public String marshal(String arg0) throws Exception {
        return "<![CDATA[" + arg0.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&quote;", "\"").replaceAll("&amp;", "&") + "]]>";
    }
    @Override
    public String unmarshal(String arg0) throws Exception {
        return arg0;
    }
}