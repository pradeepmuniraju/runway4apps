package com.sok.runway.campaigns;

import java.sql.Connection;
import java.util.*;
import javax.servlet.ServletRequest;
import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.crm.*;
import com.sok.runway.security.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.runway.crm.factories.*;

public class CampaignApplication extends AuditEntity implements StatusHistory {
	
	private CampaignEvent ce = null;
	
	public CampaignApplication(Connection con) {
	    super(con, "ContactCampaignView");
	}
	
	public CampaignApplication(ServletRequest request) {
	    super(request, "ContactCampaignView");
       populateFromRequest(request.getParameterMap());
	}
	   
	public CampaignApplication(Connection con, String contactCampaignID) {
		super(con, "ContactCampaignView");
		load(contactCampaignID);
	}

   public CampaignEvent getCampaignEvent() {
      if (ce == null) {
         ce = new CampaignEvent(getConnection());
      }
      else {
         ce.clear();
      }
      return ce;
   }

   public String getStatusHistoryViewName() {
      return "ContactCampaignStatusView";
   }

   public String getStatusHistoryID() {
      return getField("ContactCampaignStatusID");
   }

   public void setStatusHistoryID(String id) {
	   setParameter("ContactCampaignStatusID", id);
   }

   public String getStatusID() {
      return getField("StatusID");
   }

	public boolean loadContact(String campaignid, String contactid) {
	     
		if (campaignid.length()>0 && contactid.length()>0) { 
			String[] fields = {"CampaignID","ContactID"}; 
		    String[] values = {campaignid,contactid};
		     
		    loadFromField(fields,values); 
		}
		return isLoaded();
	}

	public boolean loadCompany(String campaignid, String companyID) {
	     
		if (campaignid.length()>0 && companyID.length()>0) { 
			String[] fields = {"CampaignID","CompanyID"}; 
		    String[] values = {campaignid,companyID};
		     
		    loadFromField(fields,values); 
		}
		return isLoaded();
	}
   
   public void applyAllCampaignEvents() {
      if (getPrimaryKey().length() !=0 && getField("CampaignID").length() !=0 ) {
         GenRow events = new GenRow();
         events.setConnection(getConnection());
         events.setViewSpec("EventView");
         events.setParameter("CampaignID", getField("CampaignID"));
         events.doAction(ActionBean.SEARCH);
         events.getResults();
         while(events.getNext()) {
            applyCampaignEvent(events.getData("EventID"));
         }
      }
   }
      
   public CampaignEvent applyCampaignEvent(String eventID) {
      CampaignEvent campaignEvent = null;
      
      if (eventID != null && eventID.length() != 0) { 
         campaignEvent = getCampaignEvent();
         campaignEvent.loadCampaignEvent(getPrimaryKey(), eventID);
         campaignEvent.setCurrentUser(getCurrentUser());
         
         if (!campaignEvent.isLoaded()) {        	         	 
            GenRow event = new GenRow();
            event.setConnection(getConnection());
            event.setViewSpec("EventView");
            event.setParameter("EventID", eventID);
            event.doAction(ActionBean.SELECT);
            
            campaignEvent.setField("RepUserID", event.getData("RepUserID"));
            campaignEvent.setField("EventDate", event.getDate("EventDate"));
            campaignEvent.setField("LeadTime", event.getData("LeadTime"));
            campaignEvent.setField("Status", event.getData("Status"));
            campaignEvent.setField("Subject", event.getData("Subject"));
            campaignEvent.setField("Body", event.getData("Body"));
            campaignEvent.setField("ContactCampaignID", getPrimaryKey());
            campaignEvent.setField("Attempts", "0");
            campaignEvent.setField("MasterEventID", event.getData("EventID"));
                     
            ErrorMap map = campaignEvent.insert();
            if (!map.isEmpty()) {
               campaignEvent = null;
            }
         } else {
        	 
        	 GenRow event = new GenRow();
             event.setConnection(getConnection());
             event.setViewSpec("EventView");
             event.setParameter("EventID", eventID);
             event.doAction(ActionBean.SELECT);
             
             campaignEvent.setField("RepUserID", event.getData("RepUserID"));
             campaignEvent.setField("EventDate", event.getDate("EventDate"));
             campaignEvent.setField("LeadTime", event.getData("LeadTime"));
             campaignEvent.setField("Status", event.getData("Status"));
             campaignEvent.setField("Subject", event.getData("Subject"));
             campaignEvent.setField("Body", event.getData("Body"));
             campaignEvent.setField("ContactCampaignID", getPrimaryKey());
             campaignEvent.setField("Attempts", "0");
             campaignEvent.setField("MasterEventID", event.getData("EventID"));
                      
             ErrorMap map = campaignEvent.update();
             if (!map.isEmpty()) {
                campaignEvent = null;
             } 
         }
      }
      return campaignEvent;
   }
   
   public ErrorMap update() {
      if (getParameter("StatusID") != null && getParameter("StatusID").length() != 0) {
         
         StatusUpdater su = StatusUpdater.getStatusUpdater();
         if (su.isNewStatus(this)) {
            setParameter("ContactCampaignStatusID",KeyMaker.generate());
            su.insertStatus(this);
         }
      }
      return super.update();
   }
   
   public ErrorMap insert() {
      boolean insertStatus = false;
      if (getParameter("StatusID").length() != 0) {
         if (getParameter("ContactCampaignStatusID").length() == 0) {
            setParameter("ContactCampaignStatusID",KeyMaker.generate());
         }
         insertStatus = true;
      }
      ErrorMap map = super.insert();
      
      if (map.isEmpty() && insertStatus) {
         StatusUpdater su = StatusUpdater.getStatusUpdater();
         su.insertStatus(this);
      }
      
      return map;
   }
	
	public Validator getValidator() {
	    Validator val = Validator.getValidator("CampaignApplication");
	    if (val == null) {
	       val = new Validator();
	       val.addMandatoryField("ContactCampaignID","SYSTEM ERROR - ContactCampaignID");
	       val.addMandatoryField("CampaignID","SYSTEM ERROR - CampaignID");
         val.addMandatoryField("ContactCampaignStatusID","ContactApplication must have a status selected");
	       Set set = new HashSet();
	       set.add("CompanyID");
	       set.add("ContactID");
	       val.addMutexFields(set);
	       val.addMandatoryField("Status","Status must be entered.");
	       
	       Validator.addValidator("CampaignApplication", val);
	    }
    
	    return val;
	}

}
