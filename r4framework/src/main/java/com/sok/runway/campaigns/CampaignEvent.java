package com.sok.runway.campaigns;

import java.sql.Connection;
import java.util.*;
import javax.servlet.ServletRequest;
import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.security.*;

public class CampaignEvent extends AuditEntity  {
	
	public CampaignEvent(Connection con) {
	    super(con, "ContactEventView");
	}
	
	public CampaignEvent(ServletRequest request) {
	    super(request, "ContactEventView");
       populateFromRequest(request.getParameterMap());
	}
	   
	public CampaignEvent(Connection con, String eventID) {
		super(con, "ContactEventView");
		load(eventID);
	}
	
	public void loadCampaignEvent(String contactCampaignID, String eventID) {
	     
		if (contactCampaignID.length()>0 && eventID.length()>0) { 
			String[] fields = {"ContactCampaignID","MasterEventID"}; 
         String[] values = {contactCampaignID,eventID};
		   
		   loadFromField(fields,values); 
		}
	}
	
	public Validator getValidator() {
	    Validator val = Validator.getValidator("CampaignEvent");
	    if (val == null) {
	       val = new Validator();
	       val.addMandatoryField("EventID","SYSTEM ERROR - EventID");
	       val.addMandatoryField("ContactCampaignID","SYSTEM ERROR - ContactCampaignID");
	       val.addMandatoryField("MasterEventID","SYSTEM ERROR - MasterEventID");
	       
	       Validator.addValidator("CampaignEvent", val);
	    }
    
	    return val;
	}

}
