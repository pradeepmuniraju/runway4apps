package com.sok.runway.campaigns;

import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.generation.Row;
import com.sok.runway.*;
import com.sok.runway.crm.*;
import com.sok.runway.crm.factories.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.runway.offline.ThreadManager;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import java.sql.Connection;
import java.util.*;

import org.codehaus.jackson.annotate.JsonProperty;

public class Campaign extends SecuredEntity implements DocumentLinkable {

	public DisplaySpec dspec = new DisplaySpec("ContactDisplay");

	public enum Mode {
		Email, SimpleEmail, Telemarketing, Product
	};

	public final static Map<String,String[][]> outcomeSearch = new LinkedHashMap<String, String[][]>();
	public final static Map<String,String[][]> outcomes = new LinkedHashMap<String, String[][]>();
	static { 
		/* searches must be mapped to outcomes */
		outcomeSearch.put("Failed",new String[][]{{"SendStatus","Opt-out or left company%+Invalid Addresses%+Sales Rep Inactive%"}});
		outcomeSearch.put("Out Of Office",new String[][]{{"SendStatus","Sent"},{"ContactEventEmail.NoteResponses.Description","Email Bounce: Out of Office"}});
		outcomeSearch.put("Bounced",new String[][]{{"SendStatus","Sent"},{"ContactEventResponses.Description","Email Bounce%"}});
		outcomeSearch.put("Opted Out",new String[][]{{"SendStatus","Sent+Out Of Office"},{"ContactEventEmail.NoteChildNote.NoteResponses.Description","Opt-Out Confirmed"}});
		outcomeSearch.put("Opened",new String[][]{{"SendStatus","Sent+Out Of Office"},{"SendOutcome","!Opted Out+NULL"},{"ContactEventResponses.Description","Viewed Email"}});
		
		/* outcomes can then be simply calculated */
		outcomes.put("Sent",new String[][]{{"SendOutcome","Sent"}}); 
		outcomes.put("Opened",new String[][]{{"SendOutcome","Opened"}});
		outcomes.put("Bounced",new String[][]{{"SendOutcome","Bounced"}});
		outcomes.put("Opted Out",new String[][]{{"SendOutcome","Opted Out"}});
		outcomes.put("Out of Office",new String[][]{{"SendOutcome","Out Of Office"}});
		outcomes.put("Failed to Send",new String[][]{{"SendOutcome","Failed"}});		
	}
	
	private static final String COMPANY_FK = "CompanyID";
	private static final String CONTACT_FK = "ContactID";
	public static final String ENTITY_NAME = "Campaign";

	private CampaignApplication ca = null;

	public Campaign() {
		super((Connection)null, "CampaignDetailView");
	}
	
	public Campaign(Connection con) {
		super(con, "CampaignDetailView");
	}

	public Campaign(ServletRequest request) {
		super(request, "CampaignDetailView");
		populateFromRequest(request.getParameterMap());
	}

	public Campaign(Connection con, String campaignID) {
		this(con);
		load(campaignID);
	}

	public String getEntityName() {
		return ENTITY_NAME;
	}

	public CampaignApplication getCampaignApplication() {
		if (ca == null) {
			ca = new CampaignApplication(getConnection());
		} else {
			ca.clear();
		}
		return ca;
	}

	public static Mode getMode(Row r) {
		/* hack for campaign event view */
		Mode campaignType = Mode.Telemarketing;
		if(r.getString("Type") != null && r.getString("Type").equals("Email")){
			campaignType = Mode.Email ;
		}else if(r.getString("Type").equals("SimpleEmail")){
			campaignType = Mode.SimpleEmail ;
		} else if("Product".equals(r.getString("Type"))) {
			campaignType = Mode.Product;
		}
		if (r.isSet("CampaignType")) {
			campaignType = Mode.Telemarketing;
			if(r.getString("CampaignType") != null && r.getString("CampaignType").equals("Email")){
				campaignType = Mode.Email ;
			}else if(r.getString("CampaignType") != null && r.getString("CampaignType").equals("SimpleEmail")){
				campaignType = Mode.SimpleEmail ;
			}			
			return campaignType;
		}
		return campaignType;
	}

	public static Mode getMode(String s) {
		/* hack for campaign event view */
		Mode campaignType = Mode.Telemarketing;
		if("Email".equals(s)){
			campaignType = Mode.Email ;
		}else if("SimpleEmail".equals(s)){
			campaignType = Mode.SimpleEmail ;
		} else if("Product".equals(s)) {
			campaignType = Mode.Product;
		}
		return campaignType;
	}

	public static Set<String> getTemplateResponses(HttpServletRequest request) { 

		String campaignID = request.getParameter("-campaignID");
		if(campaignID==null) 
			campaignID = request.getParameter("CampaignID");
		String eventID = request.getParameter("EventID");
		if(eventID == null) 
			eventID = (String)request.getAttribute("EventID");
		if(eventID == null) 
			eventID = "";
		
		/* create a unique list */
		Set<String> set = new HashSet<String>();

		GenRow events = new GenRow();
		events.setViewSpec("EventView");
		events.setRequest(request);
		events.setParameter("CampaignID", campaignID);
		if(eventID.length()>0) 
			events.setParameter("EventID", eventID);
		
		if(!events.isSet("CampaignID")) { 
			return Collections.emptySet();
		}
		
		events.doAction(GenerationKeys.SEARCH);
		events.getResults();

		GenRow urls = new GenRow();
		urls.setRequest(request);
		urls.setTableSpec("TemplateURLs");
		urls.setParameter("-select1", "Response");
		urls.setAction(GenerationKeys.SEARCH);

		GenRow pages = new GenRow();
		pages.setRequest(request);
		pages.setTableSpec("Pages");
		pages.setParameter("-select1", "TrackResponse");
		pages.setParameter("-select2", "PageID");
		pages.setAction(GenerationKeys.SEARCH);

		GenRow docs = null;
		
		while (events.getNext()) {
			if (events.isSet("NewsletterCMSID")) {
				urls.addParameter("CMSID|1", events.getData("NewsletterCMSID"));
				pages.addParameter("PageLink.CMSID|1",
						events.getData("NewsletterCMSID"));
			} else if (events.isSet("TemplateID")) {
				urls.addParameter("TemplateID|1", events.getData("TemplateID"));
				if (events.isSet("MasterTemplateID")) {
					urls.addParameter("MasterTemplateID|1",
							events.getData("MasterTemplateID"));
				}
			}
		}

		if (urls.isSet("CMSID|1") || urls.isSet("TemplateID|1")) {
			urls.getResults();
		}
		while (urls.getNext()) {
			set.add(urls.getData("Response"));
		}
		urls.close();
		if (pages.isSet("PageLink.CMSID|1")) {
			pages.getResults();
			
			docs = new GenRow();
			docs.setViewSpec("LinkedDocumentView"); 
			docs.setConnection(pages.getConnection()); 
		}
		while (pages.getNext()) {
			set.add(pages.getData("TrackResponse")); 
			docs.setParameter("PageID",pages.getData("PageID")); 
			docs.setParameter("-sort1","SortOrder");
			docs.doAction("search");
			
			docs.getResults(); 
			if(docs.getNext()) 
			{
				set.add("Viewed Screenshots for "+pages.getData("Title"));
				do{ 
					set.add("Viewed Screenshot: "+docs.getData("Description"));
				} while (docs.getNext()); 
			}
			docs.clear();
		}
		pages.close();
		if(docs != null) { 
			docs.close();
		}
		
		set.add("Viewed Email"); 
		set.add("Viewed HTML Email Online - html");  
		set.add("Viewed HTML Email Online - text");  
		//set.add("Opt-Out Confirmed");  
		set.add("Opt-Out Request");  
		
		return set; 
	}
	
	public static Map<String, Integer[]> getResponsesCount(HttpServletRequest request) {

		String campaignID = request.getParameter("-campaignID");
		if(campaignID==null) 
			campaignID = request.getParameter("CampaignID");
		
		String eventID = request.getParameter("EventID");
		if(eventID == null) 
			eventID = (String)request.getAttribute("EventID");
		if(eventID == null) 
			eventID = "";
		
		Set<String> set = getTemplateResponses(request); 
		
		GenRow resp = new GenRow(); 
		resp.setTableSpec("Responses"); 
		resp.setRequest(request);
		resp.setParameter("-select1","Description"); 
		resp.setParameter("-select2","ContactID");
		resp.setParameter("-select3","count(*) as count");
		resp.setParameter("-join1", "ResponseNote");
		resp.setParameter("ResponseNote.CampaignID",campaignID); 
		if(eventID.length()>0) 
			resp.setParameter("ResponseNote.EmailContactEvent.MasterEventID",eventID);
		resp.setParameter("-groupby1","Description");
		resp.setParameter("-groupby2","ContactID");
		resp.setParameter("-sort1","Description");
		resp.doAction(GenerationKeys.SEARCH); 
		resp.getResults();
		
		/* we need to create a contacts set to keep track of the total number of unique contacts */
		Set<String> contacts = new HashSet<String>(); 
		
		Map<String, Integer[]> results = new LinkedHashMap<String, Integer[]>();
		
		for(String s: set) { 
			results.put(s, new Integer[]{0,0});
		}
		int respcount = 0; 
		while(resp.getNext()) 
		{
			if(results.containsKey(resp.getData("Description"))) { 
				Integer[] v = results.get(resp.getData("Description")); 
				v[0]++; 
				v[1] += resp.getInt("count");
				results.put(resp.getData("Description"),v); 
			} else { 
				results.put(resp.getData("Description"),new Integer[]{1,resp.getInt("count")}); 
			} 
			respcount += resp.getInt("count");
			contacts.add(resp.getData("ContactID"));
		}
		results.put("-totals",new Integer[]{respcount, contacts.size()});
		return results; 
	}
	
	public static void updateStatusThread(final GenRow linkedStatus, final String bean, final String currentuserid)
	{
		Thread t = new Thread("Campaign-UpdateStatusThread-"+ThreadManager.getNextThreadId()) { 
			public void run() { 
				ActionBean.connect(linkedStatus);
				updateStatus(linkedStatus, bean, currentuserid);
			}
		}; 
		t.start();
	}
	
	public static int updateStatus(final GenRow linkedStatus, final String bean, final String currentuserid)
	{
		int updated = 0; 
		GenRow cc = new GenRow(); 
		cc.setViewSpec("ContactCampaignView"); 
		cc.setConnection(linkedStatus.getConnection()); 
		cc.setParameter("CampaignID", linkedStatus.getData("CampaignID"));
		cc.setParameter("-select1","(select LinkedGroupID from LinkedCampaigns where ContactCampaigns.CampaignID = LinkedCampaigns.CampaignID and exists (select GroupID from ContactGroups where ContactCampaigns.ContactID = ContactGroups.ContactID and LinkedCampaigns.LinkedGroupID = ContactGroups.GroupID ) order by LinkedCampaigns.SortOrder limit 1 ) as CCGroupID");
		cc.parseRequest(bean);
		/* fix for some contact beans */
		cc.setParameter("-sort0",""); 
		cc.setParameter("-sort1",""); 
		cc.setParameter("-sort2",""); 
		cc.doAction("search"); 
		//System.out.println(cc.getSearchStatement());
		
		cc.getResults(); 
		
		GenRow updater = new GenRow(); 
		updater.setTableSpec("ContactCampaigns"); 
		updater.setConnection(linkedStatus.getConnection());
		updater.setAction(GenerationKeys.UPDATE);
		
		GenRow newStatus = new GenRow(); 
		newStatus.setConnection(linkedStatus.getConnection());
		newStatus.setTableSpec("ContactCampaignStatus");
		newStatus.setParameter("StatusID",linkedStatus.getData("StatusID")); 
		newStatus.setParameter("CreatedDate","NOW"); 
		newStatus.setParameter("CreatedBy",currentuserid);
		newStatus.setAction(GenerationKeys.INSERT); 
		
		GenRow newContactStatus = new GenRow();
		newContactStatus.setConnection(linkedStatus.getConnection());
		newContactStatus.setTableSpec("GroupStatus");
		newContactStatus.setParameter("StatusID",linkedStatus.getData("ContactStatusID")); 
		newContactStatus.setParameter("CreatedDate","NOW"); 
		newContactStatus.setParameter("CreatedBy",currentuserid);
		newContactStatus.setAction(GenerationKeys.INSERT);
		
		GenRow cg = null;
		
		while(cc.getNext()) { 
			if(cc.getData("CCGroupID").equals(linkedStatus.getData("GroupID")))
			{
				if(!cc.getData("CurrentStatusID").equals(linkedStatus.getData("StatusID"))
					/* this would disable downgrading of status' && cc.getInt("CurrentStatusValue") < linkedStatus.getInt("StatusValue")*/) {
					
					//System.out.println("Updating cc"); 
					//System.out.println(cc.getData("CurrentStatusID") + " " + linkedStatus.getData("StatusID"));
					
					newStatus.setParameter("ContactCampaignID", cc.getData("ContactCampaignID")); 
					newStatus.setParameter("StatusID", linkedStatus.getData("StatusID"));
					newStatus.createNewID(); 
					newStatus.doAction();
					
					updater.setParameter("ContactCampaignID", cc.getData("ContactCampaignID"));
					updater.setParameter("ContactCampaignStatusID",newStatus.getParameter("ContactCampaignStatusID")); 
					updater.doAction(); 
					//System.out.println("updating cc"); 
					//System.out.println(updater.getSearchStatement());
					updated++; 
				}
				
				/* need to update contact status here too */
				if(cc.isSet("ContactID") && linkedStatus.isSet("ContactStatusID"))
				{
					cg = Contact.getContactGroup(linkedStatus.getConnection(), cc.getString("ContactID"), linkedStatus.getString("GroupID"));
					if(cg != null && !cg.getData("CurrentStatusID").equals(linkedStatus.getData("ContactStatusID"))
						/* this would disable downgrading of status' && cg.getInt("CurrentStatusValue") < linkedStatus.getInt("ContactStatusValue") */)
					{
						//create new contact group status
						newContactStatus.setParameter("ContactGroupID", cg.getData("ContactGroupID"));
						newContactStatus.setParameter("StatusID", linkedStatus.getData("ContactStatusID"));
						newContactStatus.createNewID(); 
						newContactStatus.doAction(); 
						
						cg.setParameter("ContactGroupID", cg.getData("ContactGroupID"));
						cg.setParameter("ContactGroupStatusID", newContactStatus.getParameter("GroupStatusID")); 
						cg.doAction(GenerationKeys.UPDATE);
						
						//System.out.println("updating cg"); 
						//System.out.println(cg.getSearchStatement());
						updated++; 
					}
				}
			}
		}
		return updated;
	}
	
	public static List<String> getResponses(HttpServletRequest request) {
		
		String campaignID = request.getParameter("-campaignID");
		if(campaignID==null) 
			campaignID = request.getParameter("CampaignID");
		String eventID = request.getParameter("EventID"); 
		if(eventID == null) 
			eventID = (String)request.getAttribute("EventID");
		if(eventID == null) 
			eventID = "";
		
		Set<String> set = getTemplateResponses(request); 
		
		GenRow resp = new GenRow();
		resp.setTableSpec("Responses");
		resp.setRequest(request);
		resp.setParameter("-select1", "Description");
		resp.setParameter("ResponseNote.CampaignID", campaignID);
		if(eventID.length()>0) 
			resp.setParameter("ResponseNote.EmailContactEvent.MasterEventID",eventID);
		
		if(resp.isSet("ResponseNote.CampaignID")) { 
			resp.setParameter("Description","!EMPTY");
			resp.setParameter("-groupby1", "Description");
			resp.setAction(GenerationKeys.SEARCH);
			resp.getResults();
			while (resp.getNext()) {
				set.add(resp.getString("Description"));
			}
			resp.close();
		}

		/* sort the list and return */
		List<String> list = new ArrayList<String>(set);
		list.remove("");

		Collections.sort(list);

		return list;
	}

	public CampaignApplication applyCompanyCampaign(String companyID,
			boolean allEvents) {
		return applyCampaignAndEvent(COMPANY_FK, companyID, null, allEvents);
	}

	public CampaignApplication applyCompanyCampaignAndEvent(String companyID,
			String eventID) {
		return applyCampaignAndEvent(COMPANY_FK, companyID, eventID, false);
	}

	public CampaignApplication applyContactCampaign(String contactID,
			boolean allEvents) {
		return applyCampaignAndEvent(CONTACT_FK, contactID, null, allEvents);
	}

	public CampaignApplication applyContactCampaignAndEvent(String contactID,
			String eventID) {
		return applyCampaignAndEvent(CONTACT_FK, contactID, eventID, false);
	}

	private CampaignApplication applyCampaignAndEvent(String fkField,
			String id, String eventID, boolean allEvents) {
		CampaignApplication campaignApplication = null;
		if (getPrimaryKey().length() != 0 && id.length() != 0) {
			campaignApplication = getCampaignApplication();
			campaignApplication.setCurrentUser(getCurrentUser());
			if (COMPANY_FK.equals(fkField)) {
				campaignApplication.loadCompany(getPrimaryKey(), id);
			} else {
				campaignApplication.loadContact(getPrimaryKey(), id);
			}

			if (!campaignApplication.isLoaded()) {
				campaignApplication.setField(fkField, id);
				campaignApplication.setField(getPrimaryKeyName(),
						getPrimaryKey());
				campaignApplication.setField("Status", "Active");
				campaignApplication.setField("StatusID",
						getField("DefaultStatusID"));

				ErrorMap map = campaignApplication.insert();
				if (!map.isEmpty()) {
					campaignApplication = null;
				}
			}

			if (campaignApplication != null) {
				if (allEvents) {
					campaignApplication.applyAllCampaignEvents();
				} else if (eventID != null) {
					campaignApplication.applyCampaignEvent(eventID);
				}
			}
		}
		return campaignApplication;

	}

	public Validator getValidator() {

		Validator val = Validator.getValidator("Campaign");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("CampaignID", "SYSTEM ERROR - CampaignID");
			val.addMandatoryField("Name", "Name must not be blank.");
			// val.addMandatoryField("Description","Description must not be blank.");
			val.addMandatoryField("DefaultStatusID",
					"Default Status must be selected.");
			// val.addMandatoryField("ExitStatusID","Exit Status must be selected.");
			val.addMandatoryField("GroupID",
					"The " + dspec.getItemLabel("Groups.Name")
							+ " must be selected");

			Validator.addValidator("Campaign", val);
		}

		return val;
	}

	@JsonProperty("CampaignID")
	public String getCampaignID() {
		return getField("CampaignID");
	}

	@JsonProperty("Name")
	public String getName() {
		return getField("Name");
	}

	@JsonProperty("Description")
	public String getDescription() {
		return getField("Description");
	
	}
	@JsonProperty("Type")
	public String getType() {
		return getField("Type");
	}

	@JsonProperty("CampaignID")
	public void setCampaignID(String campaignId) {
		setField("CampaignID", campaignId);
	}

	@JsonProperty("Name")
	public void setName(String name) {
		setField("Name", name);
	}

	@JsonProperty("Description")
	public void setDescription(String description) {
		setField("Description", description);	
	}
	@JsonProperty("Type")
	public void setType(String type) {
		setField("Type", type);
	}
	
	public String getGroupID() {
		return getField("GroupID");
	}

	public String getPrimarySort() {
		return getField("PrimarySort");
	}

	public String getDefaultStatusID() {
		return getField("DefaultStatusID");
	}

	public String getExitStatusID() {
		return getField("ExitStatusID");
	}

	public String getInboundCampaignPointID() {
		return getField("InboundCampaignPointID");
	}

	public String getStartDate() {
		return getField("StartDate");
	}

	public String EndDate() {
		return getField("EndDate");
	}

	/**
	 * Linkable Interface
	 */
	public String getLinkableTitle() {
		return getName();
	}

	/**
	 * DocumentLinkable Interface
	 */
	public Collection<String> getLinkableDocumentIDs() {
		return getLinkableDocumentIDs(null);
	}

	/**
	 * DocumentLinkable Interface
	 */
	public Collection<String> getLinkableDocumentIDs(
			Map<String, String> filterParameters) {
		return LinkableDocumentFactory.getLinkableDocumentIDs(this,
				filterParameters);
	}

	/**
	 * DocumentLinkable Interface
	 */
	public Collection<String> getDocumentIDs() {
		return getDocumentIDs(null);
	}

	/**
	 * DocumentLinkable Interface
	 */
	public Collection<String> getDocumentIDs(
			Map<String, String> filterParameters) {
		return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
	}

	/**
	 * DocumentLinkable Interface
	 */
	public LinkableDocument getLinkableDocument() {
		LinkableDocument doc = new LinkedDocument(getConnection());
		doc.setCurrentUser(getCurrentUser());
		return doc;
	}

	/**
	 * DocumentLinkable Interface
	 */
	public void linkDocument(String documentID) {
		LinkableDocumentFactory.linkDocument(this, documentID);
	}

	public static Map<String, String> getLinkedGroups(Connection connection, String campaignID) {
		GenRow groups = new GenRow();
		//groups.setConnection(connection);
		groups.setViewSpec("LinkedCampaignGroupView");
		groups.setParameter("CampaignID", campaignID);
		groups.setParameter("-sort1","SortOrder"); 
		
		if(groups.isSet("CampaignID")) { 
			groups.doAction("search");
			groups.getResults(true); /* need to know the size to create array */

			Map<String, String> groupMap = new LinkedHashMap<String, String>();  
			while(groups.getNext()) 
			{
				groups.put( groups.getData("LinkedGroupID"), groups.getData("RepUserID"));
			}
			groups.close();
			return groupMap;
		}
		return null;
	}
}
