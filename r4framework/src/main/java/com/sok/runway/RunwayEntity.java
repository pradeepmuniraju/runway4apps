package com.sok.runway;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.TableSpec;
import com.sok.framework.ViewSpec;
import com.sok.framework.generation.IllegalConfigurationException;
import com.sok.service.crm.UserService;

public abstract class RunwayEntity implements com.sok.framework.generation.GenerationKeys {
   private static final Logger logger = LoggerFactory.getLogger(RunwayEntity.class);
   protected GenRow bean = new GenRow();
   protected HashMap<String, RunwayEntity> specificEntities = null;
   
   private boolean loaded = false;
   private boolean persisted = false; 
   
   private static SimpleDateFormat dateFormater = null;
   
   protected GenRow execBean = null;
   
   //private StringBuffer debugString = new StringBuffer();
   
   /*public void debug(String s) {
      debugString.append(s);
      debugString.append("\r\n");
   }
   public String getDebug() {
      return debugString.toString();
   }*/
   
   private class SpecificEntity extends RunwayEntity {
      
      private RunwayEntity parent;
      
      public SpecificEntity(RunwayEntity parent, String vSpec) {
         super(parent.getConnection(), vSpec);
         this.parent = parent;
      }
      
      public Validator getValidator() {
         Validator val = new Validator();
         val.addMandatoryField(parent.getPrimaryKeyName(),"SYSTEM ERROR - PrimaryKey");
         return val;
      }
   }
   
   public RunwayEntity getSpecificEntity(String vSpec) {
      
      if (specificEntities == null) {
         specificEntities = new HashMap<String, RunwayEntity>();
      }
      
      RunwayEntity specific = specificEntities.get(vSpec);
      
      if (specific == null) {
         specific = new SpecificEntity(this, vSpec);
         specificEntities.put(vSpec, specific);
         specific.load(getPrimaryKey());
      }
      return specific;
   }
   
   public RunwayEntity() {
   }
   
   public RunwayEntity(Object con, String viewSpec) {
      this((Connection)con, viewSpec);
   }
   
   public RunwayEntity(ServletRequest request, String viewSpec) {
      this(request.getAttribute(ActionBean.CON), viewSpec);
      if(request instanceof HttpServletRequest) {
    	  try {
    	  bean.setUserTimeZone(UserService.getInstance().getCurrentUser((HttpServletRequest)request).getTimeZoneObject());
    	  } catch (Exception e) {
    		  System.out.println(">> Error " + e);
    		  e.printStackTrace();
    	  }
      }
   }

   public RunwayEntity(Connection con, String viewSpec) {
      bean.setViewSpec(viewSpec);
      if (con != null) {
         bean.setConnection(con);
      }
      else {
   		bean.setJndiName(ActionBean.getJndiName());
      }
   }
   
   public RunwayEntity(HttpServletRequest request){
      bean.setRequest(request);
   }
   
   public void setJndiConnection(String jndiName) {
      bean.setJndiName(jndiName);
      ActionBean.connect(bean);
      bean.setCloseConnection(false);
   }
   
   public void close() {
      bean.close();
   }
   
   public String getTableName() {
      return bean.getTableSpec().getTableName();
   }
   
   public String getPrimaryKeyName() {
      return bean.getTableSpec().getPrimaryKeyName();
   }
   
   public String getPrimaryKey() {
      return getField(getPrimaryKeyName());
   }
   
   public void setNewPrimaryKey() {
      setField(getPrimaryKeyName(), KeyMaker.generate());
      persisted = false; 
      loaded = false; 
   }
   
   public void setPrimaryKey(String pk) {
      setField(getPrimaryKeyName(), pk);
      persisted = false;
      loaded = false; 
   }
   
   public void setViewSpec(String viewSpec) {
      bean.setViewSpec(viewSpec);
   }
   
   public ViewSpec getViewSpec() {
      return bean.getViewSpec();
   }
   
   public Connection getConnection() {
      return bean.getConnection();
   }
   
   public void setConnection(Connection con) {
      bean.setConnection(con);
   }
   
   public void setConnection(ServletRequest request) {
      setConnection((Connection)request.getAttribute(ActionBean.CON));
   }
   
   public void setConnection() { 
	   ActionBean.connect(bean); 
   }
   
	public void setLocale(Locale locale) {
		bean.setLocale(locale);
	}
	
   public String getStatement() {
      return bean.getStatement();
   }
   
   public TableData getRecord() {
      return bean;
   }
   
   public void setLoaded(boolean loaded) {
      this.loaded = loaded;
   }
   
   public boolean load(String pk) {
	   clear();
	   return loadFinal(pk);
   }
   
   public boolean loadFinal(String pk) {
      if (pk != null && pk.length() > 0) {
         bean.setParameter(getPrimaryKeyName(), pk);
         bean.clearStatement();
         bean.doAction(SELECTFIRST);
         setLoaded(bean.isSuccessful());
         postLoad();
      }
      return isLoaded();
   }
   
   public void postLoad() {
	   if(isLoaded()) { 
		   persisted = true;
	   } 
   }
   
   public boolean reload() {
      return load(getPrimaryKey());
   }
   
   public boolean isLoaded() {
      return loaded;
   }
   public boolean isPersisted() {
      return persisted;
   }
   public boolean isSet(String field) {
	   return bean.isSet(field);
   }

   public boolean loadFromField(String[] names, String[] values)  {
		clear(); 
		
		if (names != null && values != null && names.length==values.length && names.length > 0) { 
			for (int i=0; i<names.length; i++) { 
				bean.setParameter(names[i],values[i]);
			}
         bean.clearStatement();
         bean.doAction(SELECTFIRST);
         setLoaded(bean.isSuccessful());
         postLoad();
		} 
      return isLoaded();
   }
   
   public boolean loadFromField(String name, String value) {
      
    	String[] names = {name};
    	String[] values = {value};
    	return loadFromField(names,values); 
   }
   
   public void clear() {
      bean.clear();
      if (specificEntities != null) {
		   specificEntities.clear();
		}
      setLoaded(false);
   }
    
   public String getField(String field) {
      return bean.getColumn(field);
   }
   
   public String getFieldNull(String field) {
	   return bean.isSet(field) ? bean.getColumn(field) : null;
   }
   
   public String getFieldHTML(String field) {
      return StringUtil.toHTML(getField(field));
   }
   
   public Object getObject(String field) {
      return bean.get(field);
   }
   
   public String getFormattedField(String field, String format) {
      return bean.getData(field, format);
   }
   
   public int getInt(String field) {
      return bean.getInt(field);
   }
   
   public long getLong(String field) {
      return bean.getLong(field);
   }
   
   public float getFloat(String field) {
      return bean.getFloat(field);
   }
   
   public double getDouble(String field) {
      return bean.getDouble(field);
   }   
   
   public Date getDate(String field) {
      try { 
    	  return bean.getDate(field);
      } catch (IllegalConfigurationException ice) {
    	  logger.warn("Error Retrieving date for table=[{}] with id=[{}] message=[{}]", new String[]{getTableName(), getPrimaryKey(), ice.getMessage()});
    	  return null;
      }
   }   
   
   public void setField(String parameter, java.util.Date value) {
      if (value != null) {
         bean.setParameter(parameter, new java.sql.Timestamp(value.getTime()));
      }
      else {
         bean.setParameter(parameter, null);
      }
   }
   
   public void setField(String parameter, Object value) {
      bean.setParameter(parameter, value);
   } 
   
   public void setParameter(String parameter, java.util.Date value) {
      if (value != null) {
         bean.setParameter(parameter, new java.sql.Timestamp(value.getTime()));
      }
      else {
         bean.setParameter(parameter, null);
      }
   }
   
   public void setParameter(String parameter, Object value) {
      bean.setParameter(parameter, value);
   } 
   
   public String getFormattedParameter(String parameter, String format) {
      return bean.getFormattedParameter(parameter, format);
   }
   
   public String getFormattedParameter(String parameter) {
      String s = bean.getFormattedParameter(parameter);
      if (s != null) {
         return s;
      }
      else {
         return ActionBean._blank;
      }
   }
   
   public boolean hasParameter(String parameter) {
	   return StringUtils.isNotBlank(bean.getParameter(parameter));
   }
   
   public String getParameter(String parameter) {
      String s = bean.getParameter(parameter);
      if (s != null) {
         return s;
      }
      else {
         return ActionBean._blank;
      }
   }
   
   public void populateFromRequest(Map map, String prefix) {
      if (prefix == null) {
         populateFromRequest(map);
      }
      else {
         String prefixToken = prefix + "@";
         if (map.containsKey(prefixToken + getPrimaryKeyName())) {
            load(((String[])map.get(prefixToken + getPrimaryKeyName()))[0]);
         }
            
         Iterator iter = map.keySet().iterator();
         String key = null;
         String[] values = null;
         while (iter.hasNext()) {
            key = (String)iter.next();
            
            if (key.startsWith(prefixToken)) {
               values = (String[])map.get(key);
               bean.setParameter(key.substring(prefixToken.length()), buildValueFromArray(values));
            }
         }
      }
   }
   
   public static String buildValueFromArray(String[] values) {
      if (values.length == 1) {
         return values[0];
      }
      else {
         StringBuffer val = new StringBuffer();
         for (int i=0; i < values.length; i++) {
            if (values[i].length() > 0) {
               if (val.length() > 0) {
                  val.append("+");
               }
               val.append(values[i]);
            }
         }
         return val.toString();
      }
   }
   
   /**
    * 26/06/2007 Parses the map that is passed in as a parameter, does not load.
    * @author Mikey
    * @param map
    */
   public void populate(Map map) {
	   populateFromRequest(map, false); 
   }
   
   /**
    * 24/09/2007 Parses the bean String that is passed in as a parameter, does not load.
    * @author Mitch
    * @param beanString
    *
    * Delegates to GenRow.parseRequest(String)
    */
   public void populate(String beanString) {
	   bean.parseRequest(beanString);
   }
   
   /**
    * 26/06/2007 added toString() method
    * @author Mikey 
    * @return the toString() method of the main bean. 
    */
   public String toString() { 
	   return bean.toString(); 
   }
   
   public void populateFromRequest(Map map) { 
      populateFromRequest(map, true);
   }
    
   public void populateFromRequest(Map map, boolean load) {      
      if (load && map.containsKey(getPrimaryKeyName())) {
         if (map.get(getPrimaryKeyName()) instanceof String[]) {
            String[] pk = (String[])map.get(getPrimaryKeyName());
            if (pk != null && pk.length == 1) {
               load(pk[0]);
            }
         }
         else {
            load((String)map.get(getPrimaryKeyName()));
         }
      }
      
      bean.parseRequest(map);
   }
   
   public ErrorMap validate() {
      return getValidator().validate(bean);
   }
   
   public void beforeSave() {
   }
   
   public void beforeUpdate() {
   }
   
   public void beforeInsert() { 
      if (bean.getParameter(getPrimaryKeyName()) == null || bean.getParameter(getPrimaryKeyName()).length() == 0) {
          bean.createNewID();
      }
   }
   
   public void postSave() {
	  persisted = true; 
      if (specificEntities != null) {
         for (RunwayEntity entity : specificEntities.values()) {
            if (entity.isLoaded() || entity.isPersisted()) {
               entity.update();
            }
            else {
               entity.insert();
            }
         }
      }
   }
   
   public void postDelete() { } 
   
   /**
    * 14/12/2007 added dupe() method
    * @author Roy 
    * @return void dupilcate objects
    */
   public void dupe() {
	   TableSpec ts = bean.getTableSpec();
	   String fieldName = null;
	   
	   for(int i=0; i<ts.getFieldsLength(); i++){
	      fieldName = ts.getFieldName(i);
	      
	      if (bean.getParameter(fieldName) == null) {
	         this.setField(fieldName, this.getObject(fieldName));
	      }
	   }
	   this.setNewPrimaryKey();
   }
   
   
   public ErrorMap update() {
      beforeSave();
      beforeUpdate();
      ErrorMap errors = validate();
      if (errors.isEmpty()) {
         bean.clearStatement();
         bean.doAction(UPDATE);
         if(bean.getError().length()==0) { 
        	 postSave();
         } else { 
        	 errors.put("update error", bean.getError()); 
         }
      }
      return errors;
   }
   
   public ErrorMap insert() {
      beforeSave();
      beforeInsert();
      /* this code should be removed if it does not produce any output */
      if (bean.getParameter(getPrimaryKeyName()) == null || bean.getParameter(getPrimaryKeyName()).length() == 0) {
          try { 
        	  throw new RuntimeException("Setting new pk for entity, should be set by beforeInsert()");
          } catch (RuntimeException re) {
        	  logger.warn(re.getMessage(), re);
          }
    	  bean.createNewID();
      }
      ErrorMap errors = validate();
      if (errors.isEmpty()) {
         bean.clearStatement();
         bean.doAction(INSERT);
         if(bean.getError().length()==0) { 
        	 postSave();
         } else { 
        	 errors.put("insert error", bean.getError()); 
         }
      }
      return errors;
   }
   
   public boolean delete() {
      if (getPrimaryKey().length() > 0) {
         bean.clearStatement();
         bean.doAction(DELETE);
         postDelete();
         return true;
      }
      logger.debug("PK was null {}", this.getClass().getName());
      return false;
   }
   
   public void executeStatement(String sql) {
      if (execBean == null) {
         execBean = new GenRow();
         execBean.setJndiName(ActionBean.getJndiName());
         execBean.setConnection(getConnection());
      }
      execBean.setStatement(sql);
      execBean.doAction(EXECUTE);
   }
   
   public void putAllDataAsParameters(Object obj){
	 if (obj instanceof RunwayEntity) {
		 RunwayEntity runwayentity = (RunwayEntity)obj;
		 GenRow g = (GenRow)runwayentity.getRecord();
		 bean.putAllDataAsParameters(g);
	 }
   }
   
   public abstract Validator getValidator();
   
   public synchronized static String formatDate(Date date, String pattern) {

      if (dateFormater == null) {
         dateFormater = new SimpleDateFormat(pattern);
      }
      else {
         dateFormater.applyPattern(pattern);
      }
      return dateFormater.format(date);
   }
}