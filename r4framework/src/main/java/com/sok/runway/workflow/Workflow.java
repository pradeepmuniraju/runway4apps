package com.sok.runway.workflow;

import com.sok.runway.security.*;
import com.sok.framework.*;
import com.sok.runway.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;
import java.util.*;

public class Workflow extends SecuredEntity {
	
	public Workflow(Connection con) {
	    super(con, "workflow/WorkflowView");
	}
	
	public Workflow(ServletRequest request) {
	    super(request, "workflow/WorkflowView");
	}
	   
	public Workflow(Connection con, String workflowID) {
		this(con);
		load(workflowID);
	}

   public WorkflowApplication addCompanyToWorkflow(String companyID) {
      if ((isLoaded() || isPersisted()) && companyID != null && companyID.length() > 0) {
         WorkflowApplication app = new WorkflowApplication(getConnection());
         app.loadCompany(getPrimaryKey(), companyID);
         if (!app.isLoaded()) {
            app.setCurrentUser(getCurrentUser());
            app.setField("WorkflowPointID", getField("WorkflowPointID"));
            app.setField("Active", "Y");
            app.insert();
         }
         return app;
      }
      return null;
   }
   
   public Collection getWorkflowPointIDs() {
      ArrayList ids = new ArrayList();
      
      if (getField("WorkflowID").length() > 0) {
         GenRow pointList = new GenRow();
         pointList.setConnection(getConnection());
         pointList.setViewSpec("workflow/WorkflowPointIDView");
         pointList.setParameter("WorkflowID", getField("WorkflowID"));
         pointList.setSort(0, "PointQuestion", "ASC");
         pointList.getResults();
         
         while (pointList.getNext()) {
            ids.add(pointList.getString("WorkflowPointID"));
         }
         pointList.close();
      }
      return ids;
   }
	
   public boolean delete() {
      Iterator iter = getWorkflowPointIDs().iterator();
      boolean success = true;
      WorkflowPoint point = new WorkflowPoint(getConnection());
      while (success && iter.hasNext()) {
         point.load((String)iter.next());
         if (!point.delete()) {
            success = false;
         }
      }
      return (success && super.delete());
   }

	public Validator getValidator() {
		
	    Validator val = Validator.getValidator("Workflow");
	    if (val == null) {
	       val = new Validator();
	       val.addMandatoryField("WorkflowID","SYSTEM ERROR - WorkflowID");
	       val.addMandatoryField("WorkflowPointID","SYSTEM ERROR - WorkflowPointID");
	       val.addMandatoryField("Name","Name must not be blank.");
	       val.addMandatoryField("Description","Description must not be blank.");
	       val.addMandatoryField("GroupID","Group must be selected");
	       
	       Validator.addValidator("Workflow", val);
	    }
    
	    return val;
	}
	
	public ErrorMap insert() {
	   setField("WorkflowPointID",KeyMaker.generate());
	   
	   ErrorMap map = super.insert();
	   
	   
	   if (map.isEmpty()) {
	      WorkflowPoint point = new WorkflowPoint(getConnection());
	      point.setCurrentUser(getCurrentUser());
	      point.setField("PointQuestion", getField("PointQuestion"));
	      if (point.getField("PointQuestion").length() == 0) {
	         point.setField("PointQuestion","< starting question >");
	      }
	      point.setField("WorkflowPointID", getField("WorkflowPointID"));
	      point.setField("WorkflowID", getPrimaryKey());
	      map.putAll(point.insert());
	   }
	   return map;
	}
	
	public String getWorkflowID() {
		return getField("WorkflowID");
	}
	public String getName() {
		return getField("Name");
	}
	public String getDescription() {
		return getField("Description");
	}
	public String getWorkflowPointID() {
		return getField("WorkflowPointID");
	}
	public boolean isActive() { 
		return "Y".equals(getField("Active"));
	} 
   
}
