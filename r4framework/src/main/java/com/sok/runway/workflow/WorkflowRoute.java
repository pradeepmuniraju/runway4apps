package com.sok.runway.workflow;

import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;
import java.util.*;

public class WorkflowRoute extends AuditEntity {
	
	public WorkflowRoute(Connection con) {
	    super(con, "workflow/WorkflowRouteView");
	}
	
	public WorkflowRoute(ServletRequest request) {
	    super(request, "workflow/WorkflowRouteView");
	}
	   
	public WorkflowRoute(Connection con, String workflowRouteID) {
		this(con);
		load(workflowRouteID);
	}
	
   public Collection getWorkflowRouteActionIDs() {
      ArrayList ids = new ArrayList();
      
      if (getField("WorkflowRouteID").length() > 0) {
         GenRow list = new GenRow();
         list.setConnection(getConnection());
         list.setViewSpec("workflow/WorkflowRouteActionIDView");
         list.setParameter("WorkflowRouteID", getField("WorkflowRouteID"));
         list.setSort(0, "SortNumber", "ASC");
         list.getResults();
         
         while (list.getNext()) {
            ids.add(list.getString("WorkflowRouteActionID"));
         }
         list.close();
      }
      return ids;
   }
	
   public boolean delete() {
      Iterator iter = getWorkflowRouteActionIDs().iterator();
      boolean success = true;
      WorkflowRouteAction action = new WorkflowRouteAction(getConnection());
      while (success && iter.hasNext()) {
         action.load((String)iter.next());
         if (!action.delete()) {
            success = false;
         }
      }
      return (success && super.delete());
   }
	
	public Validator getValidator() {
	    Validator val = Validator.getValidator("WorkflowRoute");
	    if (val == null) {
	       val = new Validator();
	       val.addMandatoryField("WorkflowRouteID","SYSTEM ERROR - WorkflowRouteID");
	       val.addMandatoryField("WorkflowPointID","SYSTEM ERROR - WorkflowPointID");
	       val.addMandatoryField("RouteAnswer","Answer must be specified");
	       
	       Validator.addValidator("WorkflowRoute", val);
	    }
    
	    return val;
	}

}
