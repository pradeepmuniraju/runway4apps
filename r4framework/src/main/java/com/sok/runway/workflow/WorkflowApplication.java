package com.sok.runway.workflow;

import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;
import java.util.*;

public class WorkflowApplication extends AuditEntity {
	
	public WorkflowApplication(Connection con) {
	    super(con, "workflow/WorkflowApplicationView");
	}
	
	public WorkflowApplication(ServletRequest request) {
	    super(request, "workflow/WorkflowApplicationView");
	}
	   
	public WorkflowApplication(Connection con, String workflowApplicationID) {
		this(con);
		load(workflowApplicationID);
	}

	public void loadContact(String workflowID, String contactID) {
	     
		if (workflowID.length()>0 && contactID.length()>0) { 
         String[] fields = {"WorkflowID","ContactID"}; 
         String[] values = {workflowID,contactID};
         
         loadFromField(fields,values); 
		}
	}

	public void loadCompany(String workflowID, String companyID) {
	     
		if (workflowID.length()>0 && companyID.length()>0) { 
         String[] fields = {"WorkflowID","CompanyID"}; 
         String[] values = {workflowID,companyID};
         
         loadFromField(fields,values); 
		}
	}
	
	public Validator getValidator() {
	    Validator val = Validator.getValidator("WorkflowApplication");
	    if (val == null) {
	       val = new Validator();
	       val.addMandatoryField("WorkflowApplicationID","SYSTEM ERROR - WorkflowApplicationID");
	       val.addMandatoryField("WorkflowID","SYSTEM ERROR - WorkflowID");
	       
	       Set set = new HashSet();
	       set.add("CompanyID");
	       set.add("ContactID");
	       val.addMutexFields(set);
	       
	       Validator.addValidator("WorkflowApplication", val);
	    }
    
	    return val;
	}

}
