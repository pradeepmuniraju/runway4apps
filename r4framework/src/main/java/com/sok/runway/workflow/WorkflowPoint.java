package com.sok.runway.workflow;

import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;
import java.util.*;

public class WorkflowPoint extends AuditEntity {
	
	public WorkflowPoint(Connection con) {
	    super(con, "workflow/WorkflowPointView");
	}
	
	public WorkflowPoint(ServletRequest request) {
	    super(request, "workflow/WorkflowPointView");
	}
	   
	public WorkflowPoint(Connection con, String workflowPointID) {
		this(con);
		load(workflowPointID);
	}
	
   public Collection getWorkflowRouteIDs() {
      ArrayList ids = new ArrayList();
      
      if (getField("WorkflowPointID").length() > 0) {
         GenRow list = new GenRow();
         list.setConnection(getConnection());
         list.setViewSpec("workflow/WorkflowRouteIDView");
         list.setParameter("WorkflowPointID", getField("WorkflowPointID"));
         list.setSort(0, "SortNumber", "ASC");
         list.getResults();
         
         while (list.getNext()) {
            ids.add(list.getString("WorkflowRouteID"));
         }
         list.close();
      }
      return ids;
   }
	
   public boolean delete() {
      Iterator iter = getWorkflowRouteIDs().iterator();
      boolean success = true;
      WorkflowRoute route = new WorkflowRoute(getConnection());
      while (success && iter.hasNext()) {
         route.load((String)iter.next());
         if (!route.delete()) {
            success = false;
         }
      }
      return (success && super.delete());
   }
   
	public Validator getValidator() {
	    Validator val = Validator.getValidator("WorkflowPoint");
	    if (val == null) {
	       val = new Validator();
	       val.addMandatoryField("WorkflowPointID","SYSTEM ERROR - WorkflowPointID");
	       val.addMandatoryField("WorkflowID","SYSTEM ERROR - WorkflowID");
	       val.addMandatoryField("PointQuestion","Question must be specified");
	       
	       Validator.addValidator("WorkflowPoint", val);
	    }
    
	    return val;
	}

}
