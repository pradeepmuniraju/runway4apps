package com.sok.runway.workflow;

import java.sql.Connection;
import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.crm.RunwayAction;
import javax.servlet.ServletRequest;

public class WorkflowRouteAction extends RunwayAction {
		
	public WorkflowRouteAction(Connection con) {
	    super(con, "workflow/WorkflowRouteActionView");
	}
   
   public WorkflowRouteAction(ServletRequest request) {
      super(request, "workflow/WorkflowRouteActionView");
   }
	   
	public WorkflowRouteAction(Connection con, String workflowRouteID) {
		this(con);
		load(workflowRouteID);
	}
	
	public boolean isMandatory() { 
		return "Y".equals(getField("Mandatory"));
	} 
	
	public boolean isHidden() { 
		return "Y".equals(getField("Hidden"));
	} 
	
	public boolean isAutomatic() { 
		return "Y".equals(getField("Automatic"));
	} 
	
	public boolean isActive() { 
		return "Y".equals(getField("Active"));
	} 
	
	public Validator getValidator() {
	    Validator val = Validator.getValidator("WorkflowRouteAction");
	    if (val == null) {
	       val = new Validator();
	       val.addMandatoryField("WorkflowRouteActionID","SYSTEM ERROR - WorkflowRouteActionID");
	       val.addMandatoryField("WorkflowRouteID","SYSTEM ERROR - WorkflowRouteID");
	       val.addMandatoryField("Name","Name must be specified");
	       val.addMandatoryField("ActionType","Action Type must be specified");
	       
	       Validator.addValidator("WorkflowRouteAction", val);
	    }
    
	    return val;
	}

}
