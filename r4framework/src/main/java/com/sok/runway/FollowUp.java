package com.sok.runway;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import java.util.*;
public class FollowUp
{
	public static final String future = "0";
	public static final String overdue = "3";
	public static final String today = "2";
	public static final String thisweek = "1";

	static boolean debug = false;

	public static String getCode(String fud)
	{
		try
		{
			if(fud==null || fud.length()==0)
			{
				return("");	
			}
			else                                                                                                                         
			{
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Date followupDate = sdf.parse(fud, new ParsePosition(0));
				return(getCode(followupDate));
			}
		}
		catch(Exception e)
		{
			if(debug)
			{
				return(e.toString());
			}
			else
			{
				return("");
			}
		}
	}

	public static String getCode(Date fud)
	{
		try
		{
			if(fud==null)
			{
				return("");	
			}
			else
			{
				Date followupDate = fud;
				GregorianCalendar followup = new GregorianCalendar();
		 		followup.setTime(followupDate);
		 		
				GregorianCalendar week = new GregorianCalendar();
				if(week.get(Calendar.DAY_OF_WEEK)==Calendar.TUESDAY)
				{
					week.add(Calendar.DATE, 5);
				}
				else if(week.get(Calendar.DAY_OF_WEEK)==Calendar.WEDNESDAY)
				{
					week.add(Calendar.DATE, 4);
				}
				else if(week.get(Calendar.DAY_OF_WEEK)==Calendar.THURSDAY)
				{
					week.add(Calendar.DATE, 3);
				}
				else if(week.get(Calendar.DAY_OF_WEEK)==Calendar.FRIDAY)
				{
					week.add(Calendar.DATE, 2);
				}
				else if(week.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY)
				{
					week.add(Calendar.DATE, 1);
				}
				else if(week.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY)
				{
					week.add(Calendar.DATE, 6);
				}
				
				GregorianCalendar todaycal = new GregorianCalendar();
				
				int todayint = getCSFormat(todaycal);
				int weekint = getCSFormat(week);
				int followupint = getCSFormat(followup);
		
				if(followupint>todayint)
				{
					if(followupint<weekint)
					{
						return(thisweek);
					}
					return(future);
				}
				else if(followupint==todayint)
				{
					return(today);	
				}
				return(overdue);
			}
		}
		catch(Exception e)
		{
			if(debug)
			{
				return(e.toString());
			}
			else
			{
				return("");
			}
		}
	}

	public static String getCode(Object fud)
	{
		if(fud instanceof Date)
		{
			Date followupDate = (Date)fud;
			return(getCode(followupDate));
		}
		else if(fud instanceof String)
		{
			String followupDate = (String)fud;
			return(getCode(followupDate));
		}
		return("");
	}
	
	public static int getCSFormat(Calendar cal)
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append(String.valueOf(cal.get(Calendar.YEAR)));
		String month = String.valueOf(cal.get(Calendar.MONTH));
		if(month.length()==1)
		{
			buffer.append("0");
		}
		buffer.append(month);
		String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
		if(day.length()==1)
		{
			buffer.append("0");
		}
		buffer.append(day);
		return(Integer.parseInt(buffer.toString()));
	}
	
}
