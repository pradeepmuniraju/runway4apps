package com.sok.runway;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.velocity.app.VelocityEngine;

import com.sok.framework.RowArrayBean;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;

public class TemplateURLs extends RowArrayBean
{
   public static final String LINK_TYPE_LINK = "T";
   public static final String LINK_TYPE_IMAGE = "I";
   
	String urlhome = null; //pageContext.getServletContext().getInitParameter("URLHome");
	Map<String, Integer> names = null; 
	public void setURLHome(String home)
	{
		urlhome = home;
	}
	
	public boolean getResults() { 
		if(names == null) { 
			names = new HashMap<String, Integer>();
		} else { 
			names.clear();
		}
		return super.getResults();
	}
   
	public void getMasterLinks(String masterTemplateID, ServletContext scontext)
	{
		setJndiName(scontext.getInitParameter("sqlJndiName"));
		setURLHome(scontext.getInitParameter("URLHome"));
		setViewSpec("TemplateURLView");
		setColumn("MasterTemplateID",masterTemplateID);
		if(masterTemplateID!=null && masterTemplateID.length()!=0)
		{
			getResults();
		}
	}
	
	public void getTemplateLinks(String templateid, ServletContext scontext)
	{
		setJndiName(scontext.getInitParameter("sqlJndiName"));
		setURLHome(scontext.getInitParameter("URLHome"));
		setViewSpec("TemplateURLView");
		setColumn("TemplateID",templateid);
		if(templateid!=null && templateid.length()!=0)
		{
			getResults();
		}
	}
	
	/*public void getLinks(String templateid, ServletContext scontext)
	{
	   getLinks(templateid, "", scontext);
   }*/

	public void getLinks(String templateid, String masterTemplateid, ServletContext scontext)
	{
		setJndiName(scontext.getInitParameter("sqlJndiName"));
		setURLHome(scontext.getInitParameter("URLHome"));
		setViewSpec("TemplateURLView");
		if(masterTemplateid!=null && masterTemplateid.length()!=0)
		{
			setColumn("TemplateID|1",templateid);
			setColumn("MasterTemplateID|1",masterTemplateid);
		}
		else
		{
			setColumn("TemplateID",templateid);
		}
		sortBy("TemplateID",0);
		if(templateid!=null && templateid.length()!=0)
		{
			getResults();
		}
	}

	public void getCMSLinks(String cmsid, ServletContext scontext)
	{
		setJndiName(scontext.getInitParameter("sqlJndiName"));
		setURLHome(scontext.getInitParameter("URLHome"));
		setViewSpec("TemplateURLView");
		setColumn("CMSID",cmsid);
		if(cmsid!=null && cmsid.length()!=0)
		{
			getResults();
		}
	}

   public void setTemplateLinks(TableData context)
   {
      StringBuffer tempurl = null;
      TableData tempurlbean = null;
      
      for(int i=0; i<this.getSize(); i++)
      {
         tempurlbean = this.get(i);
         tempurl = new StringBuffer();

         if(tempurlbean.getString("URL").indexOf("://")<0 && tempurlbean.getString("URL").indexOf("/")!=0)
         {
            tempurl.append(urlhome);
            tempurl.append("/");
         }
         tempurl.append(tempurlbean.getString("URL"));

         if(tempurlbean.getString("URL").indexOf('?')>0)
         {
            tempurl.append("&ID=");
            tempurl.append("${NoteID}");
         }
         else
         {
            tempurl.append("?ID=");
            tempurl.append("${NoteID}");
         }

         context.put(tempurlbean.getString("Name"), tempurl.toString());
      }
   }
	
	public void setTextLinks(TableData context)
	{
		StringBuffer tempurl = null;
		TableData tempurlbean = null;

		for(int i=0; i<this.getSize(); i++)
		{
			tempurlbean = this.get(i);
			tempurl = new StringBuffer();
			String linktype = tempurlbean.getString("LinkType");
			if(linktype!=null && linktype.equals(LINK_TYPE_LINK)){
				tempurl.append(tempurlbean.getString("LinkText"));
			}else{
				tempurl.append(tempurlbean.getString("AltImageText"));
			}
			tempurl.append(" - ");

			if(tempurlbean.getString("Response").length()!=0)
			{		
				tempurl.append(urlhome);
				tempurl.append("/");
				tempurl.append(tempurlbean.getString("TemplateURLID"));
				tempurl.append(".link");

				tempurl.append("?ID=");
				tempurl.append(context.getString("NoteID"));
			}
			else
			{
				if(tempurlbean.getString("URL").indexOf("://")<0 && tempurlbean.getString("URL").indexOf("/")!=0)
				{
					tempurl.append(urlhome);
					tempurl.append("/");
				}
				tempurl.append(tempurlbean.getString("URL"));

				if(tempurlbean.getString("URL").indexOf('?')>0)
				{
					tempurl.append("&ID=");
					tempurl.append(context.getString("NoteID"));
				}
				else
				{
					tempurl.append("?ID=");
					tempurl.append(context.getString("NoteID"));
				}
			}


			context.put(tempurlbean.getString("Name"), tempurl.toString());
		}
	}
	
	public TableData getRecord(String name) { 
		if(names.size()==0) { 
			for(int i=0; i<this.getSize(); i++)
			{
				names.put(this.get(i).getString("Name"),i); 
			}
		} 
		Integer i = names.get(name);
		if(i != null) { 
			return (TableData)this.get(i);
		}
		return (TableData)null; 
	}
	
	public void setHTMLLinks(TableData context)
	{
		StringBuffer tempurl = null;
		TableData tempurlbean = null;
		

		for(int i=0; i<this.getSize(); i++)
		{
			tempurlbean = this.get(i);
			tempurlbean.setColumn("URL",StringUtil.replace(StringUtil.replace(tempurlbean.getString("URL"),"$NoteID",(String) context.get("NoteID")),"${NoteID}",(String) context.get("NoteID")));
			tempurl = new StringBuffer();

			boolean useahref = tempurlbean.getString("LinkText").length()==0?false:true;

			if(useahref)
			{
				tempurl.append("<a href=\"");
			}

			if(tempurlbean.getString("Response").length()!=0 || !useahref)
			{		
				tempurl.append(urlhome);
				tempurl.append("/");
				tempurl.append(tempurlbean.getString("TemplateURLID"));
				tempurl.append(".link");

				tempurl.append("?ID=");
				tempurl.append((String) context.get("NoteID"));
			}
			else
			{
				if(tempurlbean.getString("URL").indexOf("://")<0 && tempurlbean.getString("URL").indexOf("/")!=0)
				{
					tempurl.append(urlhome);
					tempurl.append("/");
				}
				tempurl.append(tempurlbean.getString("URL"));

				if(tempurlbean.getString("URL").indexOf('?')>0)
				{
					tempurl.append("&ID=");
					tempurl.append((String) context.get("NoteID"));
				}
				else
				{
					tempurl.append("?ID=");
					tempurl.append((String) context.get("NoteID"));
				}
			}



			if(useahref)
			{
				tempurl.append("\"");
					
				if(tempurlbean.getString("Target").length()!=0)
				{
					tempurl.append(" target=\"");
					tempurl.append(tempurlbean.getString("Target"));
					tempurl.append("\"");
				}
				
				tempurl.append(">");
				String linktype = tempurlbean.getString("LinkType");
				String linktext = tempurlbean.getString("LinkText");
				
				if(linktype!=null && linktype.equals(LINK_TYPE_LINK)){
					tempurl.append(tempurlbean.getString("LinkText"));
				}else if(linktype!=null && linktype.equals(LINK_TYPE_IMAGE)){
					String altimagetext = tempurlbean.getString("AltImageText");
					if(linktext!=null && linktext.length()!=0){
						tempurl.append("<img src=\"");
						tempurl.append(linktext);
						tempurl.append("\" ");
						tempurl.append("border=\"0\" ");
						tempurl.append("alt=\"");
						tempurl.append(altimagetext);
						tempurl.append("\" ");
						tempurl.append(">");
					}else{
						tempurl.append("No Image Specified");
					}
				}else{
					tempurl.append(tempurlbean.getString("LinkText"));
				}
				tempurl.append("</a>");
			}
			context.put(tempurlbean.getString("Name"), tempurl.toString());
		}
	}

	
	public String getHTML(TableData context, String column)
	{
		return(getHTML(context, context, column));
	}
	
	public String getHTML(TableData context, String column, boolean replacelinebreaks)
	{
		return(getHTML(context, context, column, replacelinebreaks));
	}

	public String getText(TableData context, String column)
	{
		return(getText(context, context, column));
	}

	public String getHTML(TableData data, TableData context, String column)
	{
		return(getHTML( data,  context,  column, true));
	}

	public String getHTML(TableData data, TableData context, String column, boolean replacelinebreaks)
	{
		return getHTML(data.getString(column), context, replacelinebreaks);	
	}
	
	public String getHTML(String text, TableData context) {
		return getHTML(text, context, true);
	}
	
	public String getHTML(String text, TableData context, boolean replacelinebreaks) {
		setHTMLLinks(context);
		if(replacelinebreaks && StringUtil.needsHTMLLinefeedEncoding(text))
		{
			text = StringUtil.replace(text,"\n","<br/>");
		}
		return(generate(context, text));
	}
	
	public String getText(TableData data, TableData context, String column)
	{
		setTextLinks(context);
		return(generate(context, data.getString(column)));
	}

	String generate(TableData context, String column)
	{
		StringWriter text = new StringWriter();
		try{
			VelocityEngine ve = VelocityManager.getEngine();
			ve.evaluate( context, text, "TemplateURLS", column);
			text.flush();
			text.close();
		}catch(Exception e){}
		return(text.toString());
	}

}
