package com.sok.runway.externalInterface.beans.cms.properties;

import java.text.NumberFormat;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.GenRow;
import com.sok.framework.RunwayUtil;

public class QuickIndexRealEstateCollections extends QuickIndexRealEstate {

	private List<QuickIndexRealEstate> list = null;
	private double minTotalCost = -1, maxTotalCost = 0;
	private double minAdvCost = -1, maxAdvCost = 0;
	private double minBed = -1, maxBed = 0;
	private double minBath = -1, maxBath = 0;
	private double minCar = -1, maxCar = 0;
	private double minLevels = -1, maxLevels = 0;
	
	private final NumberFormat nf = NumberFormat.getInstance();
	/**
	 * 
	 */
	public QuickIndexRealEstateCollections() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param row
	 */
	public QuickIndexRealEstateCollections(GenRow row) {
		super(row);
		if ("ProductSubType".equals(row.getString("-groupby0"))) {
			GenRow item = new GenRow();
			item.setViewSpec("ListItemView");
			item.setConnection(row.getConnection());
			item.setParameter("ListName", this.getEstateName().replaceAll(" ", "") + "LotSubType");
			item.setParameter("ListItemItemList.ListName", this.getEstateName().replaceAll(" ", "") + "LotSubType");
			item.setParameter("ItemValue", this.getProductSubType());
			
			if (item.isSet("ItemValue")) {
				item.doAction("selectfirst");
				
				if (item.isSet("Description")) {
					this.setDescription(item.getString("Description"));
					this.setHeadline("");
				}
			}
		}
		nf.setMaximumFractionDigits(0);
	}

	@XmlElement(name="RealEstate")
	public List<QuickIndexRealEstate> getRealEstateList() {
		return list;
	}

	public void setRealEstateList(List<QuickIndexRealEstate> list) {
		this.list = list;
		
		String displayType = "";
		
		for (int l = 0; l < list.size(); ++l) {
			QuickIndexRealEstate qia = list.get(l);

			if (StringUtils.isBlank(displayType)) displayType = qia.getDisplayPriceType();
			
			double bed = qia.getBedrooms();
			if (bed > 0) {
				if (bed < minBed || minBed == -1) minBed = bed;
				if (bed > maxBed) maxBed = bed;
			}
			double bath = qia.getBathrooms();
			if (bath > 0) {
				if (bath < minBath || minBath == -1) minBath = bath;
				if (bath > maxBath) maxBath = bath;
			}
			double level = qia.getStoreys();
			if (level > 0) {
				if (level < minLevels || minLevels == -1) minLevels = level;
				if (level > maxLevels) maxLevels = level;
			}
			double car = qia.getCarparks();
			if (car > 0) {
				if (car < minCar || minCar == -1) minCar = car;
				if (car > maxCar) maxCar = car;
			}
			double totalCost = qia.getTotalCost();
			if (totalCost > 0) {
				if (totalCost < minTotalCost || minTotalCost == -1) minTotalCost = totalCost;
				if (totalCost > maxTotalCost) maxTotalCost = totalCost;
			}
			double advCost = qia.getAdvertisedCost();
			if (advCost > 0) {
				if (advCost < minAdvCost || minAdvCost == -1) minAdvCost = advCost;
				if (advCost > maxAdvCost) maxAdvCost = advCost;
			}
		}
		if (StringUtils.isBlank(displayType)) displayType = getEstateDisplayType(this.getEstateID());
		
		if (minAdvCost != 0) {
			if (minAdvCost != maxAdvCost)
				this.setDisplayPrice(RunwayUtil.displayPrice(displayType, minAdvCost, maxAdvCost));
			else 
				this.setDisplayPrice(RunwayUtil.displayPrice(displayType, minAdvCost));
		} else if (minTotalCost != 0) {
			if (minTotalCost != maxTotalCost)
				this.setDisplayPrice(RunwayUtil.displayPrice(displayType, minTotalCost, maxTotalCost));
			else
				this.setDisplayPrice(RunwayUtil.displayPrice(displayType, minTotalCost));
		} else if (list.size() > 0){
			this.setDisplayPrice("POA");
		}
	}
	
	private String getEstateDisplayType(String estateID) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setParameter("-select0", "DisplayPriceType");
		row.setParameter("ProductID", estateID);
		row.setParameter("ProductType", "Estate");
		if (row.isSet("ProductID")) row.doAction("selectfirst");
		
		return row.getString("DisplayPriceType");
	}

	@XmlElement(name="Bedrooms")
	public String getBedroomsVal() {
		if (minBed > maxBed || minBed == -1) minBed = maxBed;
		if (minBed == maxBed) return String.valueOf(maxBed).replaceAll("\\.0$","");
		
		return ("" + minBed).replaceAll("\\.0$","") + ("-" + maxBed).replaceAll("\\.0$","");
	}
	
	public void setBedroomsVal(String val) {
		
	}
	
	@XmlElement(name="Bathrooms")
	public String getBathroomsVal() {
		if (minBath > maxBath || minBath == -1) minBath = maxBath;
		if (minBath == maxBath) return String.valueOf(maxBath).replaceAll("\\.0$","");
		
		return ("" + minBath).replaceAll("\\.0$","") + ("-" + maxBath).replaceAll("\\.0$","");
	}
	
	public void setBathroomsVal(String val) {
		
	}
	
	@XmlElement(name="CarParks")
	public String getCarParksVal() {
		if (minCar > maxCar || minCar == -1) minCar = maxCar;
		if (minCar == maxCar) return String.valueOf(maxCar).replaceAll("\\.0$","");
		
		return ("" + minCar).replaceAll("\\.0$","") + ("-" + maxCar).replaceAll("\\.0$","");
	}
	
	public void setCarParksVal(String val) {
		
	}
	
	@XmlElement(name="Levels")
	public String getLevelsVal() {
		if (minLevels > maxLevels || minLevels == -1) minLevels = maxLevels;
		if (minLevels == maxLevels) return String.valueOf(maxLevels).replaceAll("\\.0$","");
		
		return ("" + minLevels).replaceAll("\\.0$","") + ("-" + maxLevels).replaceAll("\\.0$","");
	}
	
	@XmlElement(name="AdvertisedCost")
	public String getApartmentCostVal() {
		if (minAdvCost > maxAdvCost || minAdvCost == -1) minAdvCost = maxAdvCost;
		if (minAdvCost == maxAdvCost) return String.valueOf(maxAdvCost).replaceAll("\\.0$","").replaceAll("\\.00$","");
		
		return nf.format(minAdvCost) + "-" + nf.format(maxAdvCost);
	}
	
	@XmlElement(name="TotalCost")
	public String getTotalCostVal() {
		if (minTotalCost > maxTotalCost || minTotalCost == -1) minTotalCost = maxTotalCost;
		if (minTotalCost == maxTotalCost) return String.valueOf(maxTotalCost).replaceAll("\\.0$","").replaceAll("\\.00$","");
		
		return nf.format(minTotalCost) + "-" + nf.format(maxTotalCost);
	}
	
	public void setLevelsVal(String val) {
		
	}
	
	@XmlElement(name="Count")
	public int getCount() {
		if (list == null) return 0;
		
		return list.size();
	}
	
	public void setCount(int val) {
		
	}
}
