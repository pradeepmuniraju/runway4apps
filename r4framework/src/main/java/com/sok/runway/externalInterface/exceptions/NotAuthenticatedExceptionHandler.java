package com.sok.runway.externalInterface.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.sok.runway.externalInterface.DataApplication;
import com.sok.service.exception.NotAuthenticatedException;

@Provider
public class NotAuthenticatedExceptionHandler implements ExceptionMapper<NotAuthenticatedException> {

	public Response toResponse(NotAuthenticatedException exception){
	    return Response.status(Response.Status.UNAUTHORIZED)
	    .entity(new DataApplication.DataApplicationException(exception, Response.Status.UNAUTHORIZED.getStatusCode()))
	    .build();
	}
}
