package com.sok.runway.externalInterface.resources.cms;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.cms.Page;
import com.sok.service.cms.WebsiteService;

public class PageResource {
	
	private static final Logger logger = LoggerFactory.getLogger(PageResource.class);
	private static final WebsiteService websiteService = WebsiteService.getInstance();
 
	private final String cmsID, linkID;
	@Context HttpServletRequest request;
	
	public PageResource(String cmsID, HttpServletRequest request) {
		this.cmsID = cmsID;
		this.linkID = null;
		this.request = request; 
	}
	
	public PageResource(String cmsID, String linkID, HttpServletRequest request) {
		this.cmsID = cmsID;
		this.linkID = linkID;
		this.request = request;
	}
	
	@GET
	@Path("{PageID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Page getPage(@PathParam("PageID") String pageID) {
		if(logger.isDebugEnabled()) logger.debug("getPage(request, cmsID={}, pageID={})",cmsID, pageID);
		return websiteService.getPage(request, cmsID, pageID);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Page createPage(Page page) {
		logger.debug("createPage(request, cmsID={})",cmsID);
		return websiteService.createPage(request, cmsID, page);
	}
	
	@PUT
	@Path("{PageID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Page updatePage(@PathParam("PageID") String pageID, Page page) {
		logger.debug("updatePage(request, cmsID={}, pageID={}",cmsID, pageID);
		return websiteService.updatePage(request, cmsID, null, pageID, page);
	}
	
	@Path("{PageID}/meta")
	public PageMetaResource getPageMetaResource(@PathParam("PageID") String pageID) {
		return new PageMetaResource(cmsID, pageID, request);
	}
	
	@Path("{PageID}/content")
	public PageContentResource getPageContentResource(@PathParam("PageID") String pageID) {
		return new PageContentResource(cmsID, pageID, request);
	}
}
