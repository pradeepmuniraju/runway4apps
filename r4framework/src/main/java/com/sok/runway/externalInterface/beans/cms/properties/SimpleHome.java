package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Home")
@XmlType(propOrder={"homeID","name", "range" })
public abstract class SimpleHome {

	protected String homeID, name;
	protected SimpleRange range;
	
	@XmlElement(name="HomeID")
	public String getHomeID() {
		return homeID;
	}
	public void setHomeID(String homeID) {
		this.homeID = homeID;
	}
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name="Range")
	public SimpleRange getRange() {
		return range;
	}
	public void setRange(SimpleRange range) {
		this.range = range;
	}
}