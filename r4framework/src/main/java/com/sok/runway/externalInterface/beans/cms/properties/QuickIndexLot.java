package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.NONE)
@XmlSeeAlso(QuickIndex.class)
@XmlRootElement(name="QuickIndexLot")
public class QuickIndexLot extends QuickIndex {
	/*
	<field name="EstateID" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="EstateName" type="12" dtype="VARCHAR" dlen="200"/>
	<field name="StageID" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="StageName" type="12" dtype="VARCHAR" dlen="200"/>
	<field name="LotID" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="LotName" type="12" dtype="VARCHAR" dlen="200"/>
	<field name="LotType" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="LotExclusivity" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="LotPrice" type="6" dtype="DOUBLE"/>
	<field name="LotWidth" type="6" dtype="DOUBLE"/>
	<field name="LotDepth" type="6" dtype="DOUBLE"/>
	<field name="LotArea" type="6" dtype="DOUBLE"/>

	 */
	protected String estateID, estateName, stageID, stageName, lotID, lotName, lotType, lotExclusivity, packageCount;
	protected double lotPrice, lotWidth, lotDepth, lotArea;

	public QuickIndexLot() {
		// TODO Auto-generated constructor stub
	}

	public QuickIndexLot(GenRow row) {
		super(row);

		estateID = row.getData("EstateID");
		estateName = row.getData("EstateName");
		stageID = row.getData("StageID");
		stageName = row.getData("StageName");
		lotID = row.isSet("LotID")? row.getData("LotID") : row.getData("ProductID");
		lotName = row.isSet("LotName")? row.getData("LotName") : row.getData("Name");
		lotType = row.getData("LotType");
		lotExclusivity = row.getData("LotExclusivity");

		lotPrice = row.getDouble("LotPrice");
		lotArea = row.getDouble("LotArea");
		lotDepth = row.getDouble("LotDepth");
		lotWidth = row.getDouble("LotWidth");
	}

	@XmlElement(name="EstateID")
	public String getEstateID() {
		return estateID;
	}

	public void setEstateID(String estateID) {
		this.estateID = estateID;
	}

	@XmlElement(name="EstateName")
	public String getEstateName() {
		return estateName;
	}

	public void setEstateName(String estateName) {
		this.estateName = estateName;
	}

	@XmlElement(name="StageID")
	public String getStageID() {
		return stageID;
	}

	public void setStageID(String stageID) {
		this.stageID = stageID;
	}

	@XmlElement(name="StageName")
	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	@XmlElement(name="LotID")
	public String getLotID() {
		return lotID;
	}

	public void setLotID(String lotID) {
		this.lotID = lotID;
	}

	@XmlElement(name="LotName")
	public String getLotName() {
		return lotName;
	}

	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	@XmlElement(name="LotType")
	public String getLotType() {
		return lotType;
	}

	public void setLotType(String lotType) {
		this.lotType = lotType;
	}

	@XmlElement(name="LotExclusivity")
	public String getLotExclusivity() {
		return lotExclusivity;
	}

	public void setLotExclusivity(String lotExclusivity) {
		this.lotExclusivity = lotExclusivity;
	}

	@XmlElement(name="LotPrice")
	public double getLotPrice() {
		return lotPrice;
	}

	public void setLotPrice(double lotPrice) {
		this.lotPrice = lotPrice;
	}

	@XmlElement(name="LotWidth")
	public double getLotWidth() {
		return lotWidth;
	}

	public void setLotWidth(double lotWidth) {
		this.lotWidth = lotWidth;
	}

	@XmlElement(name="LotDepth")
	public double getLotDepth() {
		return lotDepth;
	}

	public void setLotDepth(double lotDepth) {
		this.lotDepth = lotDepth;
	}

	@XmlElement(name="LotArea")
	public double getLotArea() {
		return lotArea;
	}

	public void setLotArea(double lotArea) {
		this.lotArea = lotArea;
	}
	
	@XmlElement(name="PackageCount")
	public String getPackageCount() {
		return packageCount;
	}

	public void setPackageCount(String packageCount) {
		this.packageCount = packageCount;
	}

}
