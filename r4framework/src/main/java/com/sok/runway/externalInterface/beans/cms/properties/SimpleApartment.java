package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.NONE)
@XmlSeeAlso(Apartment.class)
@XmlRootElement(name="Apartment")
@XmlType(propOrder={"apartmentID","name","cost"})		//MarketingStatus AvailableDate
public abstract class SimpleApartment {

	protected String apartmentID, name;
	protected Double cost = null;
	
	public SimpleApartment() {}
	
	@XmlElement(name="ApartmentID")
	public String getApartmentID() {
		return apartmentID;
	}
	public void setApartmentID(String apartmentID) {
		this.apartmentID = apartmentID;
	}
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name="Cost")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
}
