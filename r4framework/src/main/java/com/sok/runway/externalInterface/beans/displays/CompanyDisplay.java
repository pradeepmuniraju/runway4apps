package com.sok.runway.externalInterface.beans.displays;

import com.sok.runway.externalInterface.beans.displays.shared.AddressDisplay;
import com.sok.runway.externalInterface.beans.displays.shared.GroupDisplay;


/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class CompanyDisplay
{
	private String companyLabel = null;
	private boolean companyNotUsed = false;
	private String departmentLabel = null;
	private boolean departmentNotUsed = false;

	private String phoneLabel = null;
	private boolean phoneNotUsed = false;
	private String faxLabel = null;
	private boolean faxNotUsed = false;
	private String webLabel = null;
	private boolean webNotUsed = false;
	private String emailLabel = null;
	private boolean emailNotUsed = false;

	private String repuseridLabel = null;
	private boolean repuseridNotUsed = false;
	private String mgruseridLabel = null;
	private boolean mgruseridNotUsed = false;
	private String sourceLabel = null;
	private boolean sourceNotUsed = false;
	private String createdbyLabel = null;
	private boolean createdbyNotUsed = false;
	private String typeLabel = null;
	private boolean typeNotUsed = false;
	private String industryLabel = null;
	private boolean industryNotUsed = false;

	private String qualifiedLabel = null;
	private boolean qualifiedNotUsed = false;
	
	private String followupdateLabel = null;
	private boolean followupdateNotUsed = false;
	private String phone2Label = null;
	private boolean phone2NotUsed = false;
	private String notesLabel = null;
	private boolean notesNotUsed = false;
	private String groupidLabel = null;
	private boolean groupidNotUsed = false;
	private String companystatusidLabel = null;
	private boolean companystatusidNotUsed = false;
	private String wholesalerLabel = null;
	private boolean wholesalerNotUsed = false;
	private String accountnoLabel = null;
	private boolean accountnoNotUsed = false;
	private String legalcompanynameLabel = null;
	private boolean legalcompanynameNotUsed = false;
	private String abnLabel = null;
	private boolean abnNotUsed = false;
	
	private AddressDisplay physicaladdressDisplay = null;
	private AddressDisplay postaladdressDisplay = null;
	
	public String getCompanyLabel()
	{
		return(companyLabel);
	}	
	
	public void setCompanyLabel(String companyLabel)
	{
		this.companyLabel = companyLabel;
	}	
	
	public boolean getCompanyNotUsed()
	{
		return(companyNotUsed);
	}	
	
	public void setCompanyNotUsed(boolean companyNotUsed)
	{
		this.companyNotUsed = companyNotUsed;
	}	
	
	public String getDepartmentLabel()
	{
		return(departmentLabel);
	}	
	
	public void setDepartmentLabel(String departmentLabel)
	{
		this.departmentLabel = departmentLabel;
	}	
	
	public boolean getDepartmentNotUsed()
	{
		return(departmentNotUsed);
	}	
	
	public void setDepartmentNotUsed(boolean departmentNotUsed)
	{
		this.departmentNotUsed = departmentNotUsed;
	}	
	
	public String getPhoneLabel()
	{
		return(phoneLabel);
	}	
	
	public void setPhoneLabel(String phoneLabel)
	{
		this.phoneLabel = phoneLabel;
	}	
	
	public boolean getPhoneNotUsed()
	{
		return(phoneNotUsed);
	}	
	
	public void setPhoneNotUsed(boolean phoneNotUsed)
	{
		this.phoneNotUsed = phoneNotUsed;
	}	
	
	public String getFaxLabel()
	{
		return(faxLabel);
	}	
	
	public void setFaxLabel(String faxLabel)
	{
		this.faxLabel = faxLabel;
	}	
	
	public boolean getFaxNotUsed()
	{
		return(faxNotUsed);
	}	
	
	public void setFaxNotUsed(boolean faxNotUsed)
	{
		this.faxNotUsed = faxNotUsed;
	}	
	
	public String getWebLabel()
	{
		return(webLabel);
	}	
	
	public void setWebLabel(String webLabel)
	{
		this.webLabel = webLabel;
	}	
	
	public boolean getWebNotUsed()
	{
		return(webNotUsed);
	}	
	
	public void setWebNotUsed(boolean webNotUsed)
	{
		this.webNotUsed = webNotUsed;
	}	
	
	public String getEmailLabel()
	{
		return(emailLabel);
	}	
	
	public void setEmailLabel(String emailLabel)
	{
		this.emailLabel = emailLabel;
	}	
	
	public boolean getEmailNotUsed()
	{
		return(emailNotUsed);
	}	
	
	public void setEmailNotUsed(boolean emailNotUsed)
	{
		this.emailNotUsed = emailNotUsed;
	}	
	
	public String getRepUserIDLabel()
	{
		return(repuseridLabel);
	}	
	
	public void setRepUserIDLabel(String repuseridLabel)
	{
		this.repuseridLabel = repuseridLabel;
	}	
	
	public boolean getRepUserIDNotUsed()
	{
		return(repuseridNotUsed);
	}	
	
	public void setRepUserIDNotUsed(boolean repuseridNotUsed)
	{
		this.repuseridNotUsed = repuseridNotUsed;
	}	
	
	public String getMgrUserIDLabel()
	{
		return(mgruseridLabel);
	}	
	
	public void setMgrUserIDLabel(String mgruseridLabel)
	{
		this.mgruseridLabel = mgruseridLabel;
	}	
	
	public boolean getMgrUserIDNotUsed()
	{
		return(mgruseridNotUsed);
	}	
	
	public void setMgrUserIDNotUsed(boolean mgruseridNotUsed)
	{
		this.mgruseridNotUsed = mgruseridNotUsed;
	}	
	
	public String getSourceLabel()
	{
		return(sourceLabel);
	}	
	
	public void setSourceLabel(String sourceLabel)
	{
		this.sourceLabel = sourceLabel;
	}	
	
	public boolean getSourceNotUsed()
	{
		return(sourceNotUsed);
	}	
	
	public void setSourceNotUsed(boolean sourceNotUsed)
	{
		this.sourceNotUsed = sourceNotUsed;
	}	
	
	public String getCreatedByLabel()
	{
		return(createdbyLabel);
	}	
	
	public void setCreatedByLabel(String createdbyLabel)
	{
		this.createdbyLabel = createdbyLabel;
	}	
	
	public boolean getCreatedByNotUsed()
	{
		return(createdbyNotUsed);
	}	
	
	public void setCreatedByNotUsed(boolean createdbyNotUsed)
	{
		this.createdbyNotUsed = createdbyNotUsed;
	}	
	
	public String getTypeLabel()
	{
		return(typeLabel);
	}	
	
	public void setTypeLabel(String typeLabel)
	{
		this.typeLabel = typeLabel;
	}	
	
	public boolean getTypeNotUsed()
	{
		return(typeNotUsed);
	}	
	
	public void setTypeNotUsed(boolean typeNotUsed)
	{
		this.typeNotUsed = typeNotUsed;
	}	
	
	public String getIndustryLabel()
	{
		return(industryLabel);
	}	
	
	public void setIndustryLabel(String industryLabel)
	{
		this.industryLabel = industryLabel;
	}	
	
	public boolean getIndustryNotUsed()
	{
		return(industryNotUsed);
	}	
	
	public void setIndustryNotUsed(boolean industryNotUsed)
	{
		this.industryNotUsed = industryNotUsed;
	}	
	
	public String getQualifiedLabel()
	{
		return(qualifiedLabel);
	}	
	
	public void setQualifiedByLabel(String qualifiedbyLabel)
	{
		this.qualifiedLabel = qualifiedbyLabel;
	}	
	
	public boolean getQualifiedNotUsed()
	{
		return(qualifiedNotUsed);
	}	
	
	public void setQualifiedNotUsed(boolean qualifiedbyNotUsed)
	{
		this.qualifiedNotUsed = qualifiedbyNotUsed;
	}	
	
	public String getFollowUpDateLabel()
	{
		return(followupdateLabel);
	}	
	
	public void setFollowUpDateLabel(String followupdateLabel)
	{
		this.followupdateLabel = followupdateLabel;
	}	
	
	public boolean getFollowUpDateNotUsed()
	{
		return(followupdateNotUsed);
	}	
	
	public void setFollowUpDateNotUsed(boolean followupdateNotUsed)
	{
		this.followupdateNotUsed = followupdateNotUsed;
	}	
	
	public String getPhone2Label()
	{
		return(phone2Label);
	}	
	
	public void setPhone2Label(String phone2Label)
	{
		this.phone2Label = phone2Label;
	}	
	
	public boolean getPhone2NotUsed()
	{
		return(phone2NotUsed);
	}	
	
	public void setPhone2NotUsed(boolean phone2NotUsed)
	{
		this.phone2NotUsed = phone2NotUsed;
	}	
	
	public String getNotesLabel()
	{
		return(notesLabel);
	}	
	
	public void setNotesLabel(String notesLabel)
	{
		this.notesLabel = notesLabel;
	}	
	
	public boolean getNotesNotUsed()
	{
		return(notesNotUsed);
	}	
	
	public void setNotesNotUsed(boolean notesNotUsed)
	{
		this.notesNotUsed = notesNotUsed;
	}	
	
	public String getGroupIDLabel()
	{
		return(groupidLabel);
	}	
	
	public void setGroupIDLabel(String groupidLabel)
	{
		this.groupidLabel = groupidLabel;
	}	
	
	public boolean getGroupIDNotUsed()
	{
		return(groupidNotUsed);
	}	
	
	public void setGroupIDNotUsed(boolean groupidNotUsed)
	{
		this.groupidNotUsed = groupidNotUsed;
	}	
	
	public String getCompanyStatusIDLabel()
	{
		return(companystatusidLabel);
	}	
	
	public void setCompanyStatusIDLabel(String companystatusidLabel)
	{
		this.companystatusidLabel = companystatusidLabel;
	}	
	
	public boolean getCompanyStatusIDNotUsed()
	{
		return(companystatusidNotUsed);
	}	
	
	public void setCompanyStatusIDNotUsed(boolean companystatusidNotUsed)
	{
		this.companystatusidNotUsed = companystatusidNotUsed;
	}	
	
	public String getWholesalerLabel()
	{
		return(wholesalerLabel);
	}	
	
	public void setWholesalerLabel(String wholesalerLabel)
	{
		this.wholesalerLabel = wholesalerLabel;
	}	
	
	public boolean getWholesalerNotUsed()
	{
		return(wholesalerNotUsed);
	}	
	
	public void setWholesalerNotUsed(boolean wholesalerNotUsed)
	{
		this.wholesalerNotUsed = wholesalerNotUsed;
	}	
	
	public String getAccountNoLabel()
	{
		return(accountnoLabel);
	}	
	
	public void setAccountNoLabel(String accountnoLabel)
	{
		this.accountnoLabel = accountnoLabel;
	}	
	
	public boolean getAccountNoNotUsed()
	{
		return(accountnoNotUsed);
	}	
	
	public void setAccountNoNotUsed(boolean accountnoNotUsed)
	{
		this.accountnoNotUsed = accountnoNotUsed;
	}	
	
	public String getLegalCompanyNameLabel()
	{
		return(legalcompanynameLabel);
	}	
	
	public void setLegalCompanyNameLabel(String legalcompanynameLabel)
	{
		this.legalcompanynameLabel = legalcompanynameLabel;
	}	
	
	public boolean getLegalCompanyNameNotUsed()
	{
		return(legalcompanynameNotUsed);
	}	
	
	public void setLegalCompanyNameNotUsed(boolean legalcompanynameNotUsed)
	{
		this.legalcompanynameNotUsed = legalcompanynameNotUsed;
	}	
	
	public String getABNLabel()
	{
		return(abnLabel);
	}	
	
	public void setABNLabel(String abnLabel)
	{
		this.abnLabel = abnLabel;
	}	
	
	public boolean getABNNotUsed()
	{
		return(abnNotUsed);
	}	
	
	public void setABNNotUsed(boolean abnNotUsed)
	{
		this.abnNotUsed = abnNotUsed;
	}	

	public AddressDisplay getPhysicalAddressDisplay()
	{
		return(physicaladdressDisplay);
	}
	
	public void setPhysicalAddress(AddressDisplay addressDisplay)
	{
		physicaladdressDisplay = addressDisplay;
	}
	
	public AddressDisplay getPostalAddress()
	{
		return(postaladdressDisplay);
	}
	
	public void setPostalAddress(AddressDisplay addressDisplay)
	{
		postaladdressDisplay = addressDisplay;
	}	
	
	private GroupDisplay groupdisplay = null;
	
	public GroupDisplay getGroupDisplay()
	{
		return(groupdisplay);
	}
	
	public void setGroupDisplay(GroupDisplay groupdisplay)
	{
		this.groupdisplay = groupdisplay;
	}	
}
