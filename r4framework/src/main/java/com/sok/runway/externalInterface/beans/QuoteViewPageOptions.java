package com.sok.runway.externalInterface.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
//requires jackson mapper jar, using to serialize/deserialize within this object.
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.apache.commons.lang.StringUtils;

@XmlAccessorType(XmlAccessType.FIELD)
/* some data will have properties which no longer exist */
@JsonIgnoreProperties(ignoreUnknown=true)
public class QuoteViewPageOptions {

	public QuoteViewPageOptions() { }
	
	private static final Option DEFAULT_OPTION = new Option();
	//set them all to default so we can deserialize from {} and get the defaults.
	
	public Option Baseprice = DEFAULT_OPTION;
	public Option Comments = DEFAULT_OPTION;
	//public Option Contact = null;
	public Option CustomVariations = DEFAULT_OPTION;
	public Option DRIPS = DEFAULT_OPTION;
	public Option DateExpiry = DEFAULT_OPTION;
	public Option EstateCovenants = DEFAULT_OPTION;
	public Option Facade = DEFAULT_OPTION;
	public Option Financials = DEFAULT_OPTION;
	public Option HouseDetails = DEFAULT_OPTION;
	//public Option LinkedContact = null;
	public Option LotDetails = DEFAULT_OPTION;
	public Option OHS = DEFAULT_OPTION;
	public Option Options = DEFAULT_OPTION;
	public Option SalesConsultant = DEFAULT_OPTION;
	public Option SiteCosts = DEFAULT_OPTION;
	public ExpiryDaysOption ExpiryDays = null;
	public BooleanOption SeparateLand = null;
	/* for profile page */
	public List<SurveyOption> SelectedSurveys = null;
	public List<DripLibraryOption> SelectedDripLibraries = null;
	public DocumentOption SelectedDocument = null;
	public VPBOption VPBOptions = null;
	public Map<String, Boolean> PageOptions = null;
	
	public static QuoteViewPageOptions fromString(String s) {
		//this can't be a constructor as we want this to return a value, not construct one. 
		//not sure if this can be used in a constructor or 
		if(StringUtils.isBlank(s)) {
			return new QuoteViewPageOptions(); //return default options
		}
		ObjectMapper om = new ObjectMapper();
		try {
			return om.readValue(s, QuoteViewPageOptions.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String toString() {		
		ObjectMapper om = new ObjectMapper();
		try {
			return om.writeValueAsString(this);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class DocumentOption {
		public String DocumentID;
		public String FileName;
		public String Pages;
		public Map<Integer, String> Images;
	}
	
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class DripLibraryOption {
		public String ProductLibraryID;
		public String Name;
		public String type = "drip";
	}
	
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class SurveyOption {
		public String SurveyID;
		public String Name;
		public String[] Questions; // the questions that are selected for use.
		public String type = "driplibrary";
	}
	
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Option {
		public boolean screen = true;
		public boolean pdf = true;
		public boolean included = false;
		public String type = "cover";
	}
	
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class ExpiryDaysOption {
		public int ExpiryDays;
	}
	
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class BooleanOption {
		public boolean BooleanValue = false;
	}
	
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class VPBOption {
		public boolean DisplayFilter = true;
		public boolean EstateFilter = true;
		public boolean MultiLineDescriptions = true;
		public boolean IncludeName;
		public boolean EstateCovenantsInBasePrice = true;
	}
}
