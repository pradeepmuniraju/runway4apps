package com.sok.runway.externalInterface.beans;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.amber.oauth2.common.token.OAuthToken;

import com.wordnik.swagger.annotations.ApiClass;

@XmlRootElement(name="OAuthToken")
@ApiClass(value="OAuthToken")
public class OAuthTokenImpl implements OAuthToken {
	private long expires, created;
	private String access, refresh, secret;
	public OAuthTokenImpl() { }
	
	public OAuthTokenImpl(String secret, String access, Date expiry, Date created, String refresh) {
		this.access = access;
		this.refresh = refresh;
		this.secret = secret;
		expires = expiry.getTime() - new Date().getTime();
		this.created = created.getTime();
	}
	
	@XmlElement(name="AccessToken")
	@Override
	public String getAccessToken() {
		return access;
	}

	@XmlElement(name="ExpiresIn")
	@Override
	public Long getExpiresIn() {
		return expires;
	}

	@XmlElement(name="RefreshToken")
	@Override
	public String getRefreshToken() {
		return refresh;
	}

	@XmlTransient
	@Override
	public String getScope() {
		return "API";
	}
}
