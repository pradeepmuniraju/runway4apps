package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Building")
@XmlSeeAlso(Builder.class)
@XmlType(propOrder={"buildingID","name" })		//MarketingStatus AvailableDate
public abstract class SimpleBuilding {

	protected String buildingID, name;
	
	//@JsonProperty("builderID")
	@XmlElement(name="BuildingID")
	public String getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(String builderID) {
		this.buildingID = builderID;
	}
	//@JsonProperty("Name")
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
