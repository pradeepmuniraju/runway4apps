package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.runway.externalInterface.beans.shared.AbstractStatusHistory;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="HouseLandPackageStatus")
@XmlType(propOrder={"packageStatusID","packageID"})
public class HouseLandPackageStatusHistory extends AbstractStatusHistory {
	private String packageStatusID, packageID;

	@XmlElement(name="PackageStatusID")
	public String getPackageStatusID() {
		return packageStatusID;
	}
	public void setPackageStatusID(String packageStatusID) {
		this.packageStatusID = packageStatusID;
	}
	
	@XmlElement(name="PackageID")
	public String getPackageID() {
		return packageID;
	}
	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}
}