package com.sok.runway.externalInterface.resources.cms;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.cms.PageLink;
import com.sok.service.cms.WebsiteService;

public class PageLinkResource {

	private static final Logger logger = LoggerFactory.getLogger(PageLinkResource.class);
	private static final WebsiteService websiteService = WebsiteService.getInstance();
	
	private final String cmsID;
	private final @Context HttpServletRequest request;
	public PageLinkResource(String cmsID,HttpServletRequest request) {
		this.cmsID = cmsID;
		this.request = request; 
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PageLink> getPageLinks() {
		logger.debug("getPageLink(request, cmsID={})", cmsID);
		return websiteService.getPageLinks(request, cmsID);
	}
	
	@GET
	@Path("{LinkID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PageLink getPageLink(@PathParam("LinkID") String linkID) {
		logger.debug("getPageLink(request, cmsID={}, linkID={})", cmsID, linkID);
		return websiteService.getPageLink(request, cmsID, linkID);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public PageLink createPageLink(PageLink pageLink) {
		logger.debug("createPageLink(request, cmsID={})", cmsID);
		return websiteService.createPageLink(request, cmsID, pageLink);
	}
	
	@PUT
	@Path("{LinkID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PageLink updatePageLink(@PathParam("LinkID") String linkID, PageLink pageLink) {
		logger.debug("updatePageLink(request, cmsID={}, linkID={})", cmsID, linkID);
		return websiteService.updatePageLink(request, cmsID, linkID, pageLink);
	}
	
	@DELETE
	@Path("{LinkID}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deletePageLink(@PathParam("LinkID") String linkID) {
		logger.debug("deletePageLink(request, cmsID={}, linkID={})", cmsID, linkID);
		websiteService.deletePageLink(request, cmsID, linkID);
	}
	
	@Path("{LinkID}/pages")
	public PageResource getCMSPages(@PathParam("LinkID") String linkID) {
		return new PageResource(cmsID, linkID, request);
	}
}
