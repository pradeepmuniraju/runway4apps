package com.sok.runway.externalInterface.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.sok.runway.externalInterface.DataApplication;
import com.sok.service.exception.DataException;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

@Provider
public class DataExceptionHandler implements ExceptionMapper<DataException> {
	
	private static final Logger logger = LoggerFactory.getLogger(DataExceptionHandler.class);
	public Response toResponse(DataException exception){
		logger.error("Unknown error", exception);
	    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
	    .entity(new DataApplication.DataApplicationException(exception, Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()))
	    .build();
	}
}
