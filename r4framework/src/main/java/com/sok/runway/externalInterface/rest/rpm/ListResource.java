package com.sok.runway.externalInterface.rest.rpm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.ListItem;
import com.sok.runway.externalInterface.beans.cms.properties.Lot;
import com.sok.runway.externalInterface.beans.cms.properties.LotCheck;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartmentCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartments;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexLot;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexLotCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPackageCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPackages;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPlanCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPlans;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexRealEstate;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexRealEstateCollections;
import com.sok.runway.externalInterface.beans.cms.properties.SearchKey;
import com.sok.runway.externalInterface.beans.cms.properties.Statuses;
import com.sok.service.crm.cms.properties.BuilderService;
import com.sok.service.crm.cms.properties.BuildingService;
import com.sok.service.crm.cms.properties.EstateService;
import com.sok.service.crm.cms.properties.PackageService;
import com.sok.service.crm.cms.properties.PlanService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.servlet.http.HttpServletRequest;

@Path("/lists")
@Api(value = "/lists", description = "Provides information about value lists.")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class ListResource {

	private static final Logger logger = LoggerFactory.getLogger(ListResource.class);
	
	@Context HttpServletRequest request;
	
	public ListResource() {
	}
	
	public ListResource(HttpServletRequest request) {
		this.request = request;
	}
	
	@GET
	@ApiOperation(value = "Item Lists", notes="<p>Returns a list of all Items Lists in Runway.</p>")
	public List<String> getLists() {
        GenRow itemLists = new GenRow();
        itemLists.setRequest(request);
        itemLists.setTableSpec("ItemLists");
        itemLists.setParameter("-select0", "ListName");
        itemLists.setParameter("ListName", "!%Stages;!%Regions");
        itemLists.setParameter("ListType", "!Question"); //do not expose system value lists, make the list not system instead if you need it here
        itemLists.sortBy("ListName", 0);
        itemLists.doAction(GenerationKeys.SEARCH);
        
        itemLists.getResults();
        
        ArrayList<String> list = new ArrayList<String>();
        
        while (itemLists.getNext()) {
        	if (itemLists.isSet("ListName")) list.add(itemLists.getData("ListName"));
        }
        
        itemLists.close();
		
		return list;
	}
	
	@GET
	@Path("/{listName}")
	@ApiOperation(value = "return a List", notes="<p>Returns all items in a given list. Sorted by SortNumber, Label and Value.</p>", responseClass="com.sok.runway.externalInterface.beans.cms.properties.ListItem", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<ListItem> getListItems(@ApiParam(value = "The list you want the items for", required = true) @PathParam("listName") String listName) {
		if(StringUtils.isNotBlank(listName)) {
			logger.debug("getListItems({} {})", listName, true);
		} else {
			return null;
		}
		
        GenRow itemLists = new GenRow();
        itemLists.setRequest(request);
        itemLists.setViewSpec("ListItemView");
        itemLists.setParameter("ListName", listName);
        itemLists.setParameter("ListItemItemList.ListName", listName);
        itemLists.sortBy("SortNumber", 0);
        itemLists.sortBy("ItemLabel", 1);
        itemLists.sortBy("ItemValue", 2);
        itemLists.doAction(GenerationKeys.SEARCH);
        
        itemLists.getResults();
        
        ArrayList<ListItem> list = new ArrayList<ListItem>();
        
        while (itemLists.getNext()) {
        	list.add(new ListItem(itemLists));
        }
        
        itemLists.close();
		
		return list;
	}
	
	@GET
	@Path("/statuses")
	@ApiOperation(value = "Stauses Lists", notes="<p>Returns Status Lists Names</p>", responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartmentCollections", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<String> getStatusList() {
		ArrayList<String> list = new ArrayList<String>();
		
		list.add("Group");
		list.add("Product");
		list.add("Process");
		list.add("Opportunity");
		list.add("Order");
		
		return list;
	}

	@GET
	@Path("/statuses/{listName}")
	@ApiOperation(value = "Status List", notes="Returns all items in a given list. Sorted by Status.")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public List<Statuses> getStatus(@ApiParam(value = "The list you want the items for", required = true) @PathParam("listName") String listName) {
		if(StringUtils.isNotBlank(listName)) {
			logger.debug("getListItems({} {})", listName, true);
		} else {
			return null;
		}
		
        GenRow itemLists = new GenRow();
        itemLists.setRequest(request);
        itemLists.setViewSpec("StatusListDocumentView");
        itemLists.setParameter("ListName", listName);
        itemLists.setParameter("Inactive", "!Y");
        itemLists.sortBy("Status", 0);
        itemLists.doAction(GenerationKeys.SEARCH);
        
        itemLists.getResults();
        
        ArrayList<Statuses> list = new ArrayList<Statuses>();
        
        while (itemLists.getNext()) {
        	Statuses s = new Statuses(itemLists);
        	if (itemLists.isSet("DocumentID")) s.setImage(BeanFactory.loadDocument(itemLists.getString("DocumentID")));
        	list.add(s);
        }
        
        itemLists.close();
		
		return list;
	}
}

