package com.sok.runway.externalInterface.beans.cms.properties;

import com.sok.framework.TableData;

public class HomeDetails {
	
	public HomeDetails() {}
	public HomeDetails(TableData data) {
		ProductID = data.getString("ProductID");
		Bedrooms = data.getString("Bedrooms");
		Bathrooms = data.getString("Bathrooms");
		HomeSize = data.getString("HomeSize");
		HomeSizeSq = data.getString("HomeSizeSq");
		LotWidth = data.getString("LotWidth");
		LotDepth = data.getString("LotDepth");
		Garage = data.getString("Garage");
		Study = data.getString("Study");
		Dimensions = data.getString("Dimensions");
		Area = data.getString("Area");
		CarParks = data.getString("CarParks");
		CanFitOnDepth = data.getString("CanFitOnDepth");
		CanFitOnFrontage = data.getString("CanFitOnFrontage");
		WillFitHouseSize = data.getString("WillFitHouseSize");
		LotTitleMonth = data.getString("LotTitleMonth");
		LotTitleYear = data.getString("LotTitleYear");
		IsLotTitled = data.getString("IsLotTitled");
		FitsArea = data.getString("FitsArea");
		FitsFrontage = data.getString("FitsFrontage");
		FitsDepth = data.getString("FitsDepth");
		FitsBuildWidth = data.getString("FitsBuildWidth");
		FitsBuildDepth = data.getString("FitsBuildDepth");
		Perimeter = data.getString("Perimeter");
		Storey = data.getString("Storey");
		GroundFloorArea = data.getString("GroundFloorArea");
		FirstFloorWindows = data.getString("FirstFloorWindows");
		EavesFacade = data.getString("EavesFacade");
		EavesGround = data.getString("EavesGround");
		EavesFirst = data.getString("EavesFirst");
		LeftSetback = data.getString("LeftSetback");
		RightSetback = data.getString("RightSetback");
		FrontSetback = data.getString("FrontSetback");
		RearSetback = data.getString("RearSetback");
		BuildWidth = data.getString("BuildWidth");
		BuildDepth = data.getString("BuildDepth");
	}

	public String ProductID, Bedrooms, Bathrooms, HomeSize, HomeSizeSq, LotWidth, LotDepth, Garage, Study, Dimensions, Area, CarParks, CanFitOnDepth, CanFitOnFrontage, WillFitHouseSize, LotTitleMonth, LotTitleYear, IsLotTitled, FitsArea, FitsFrontage, FitsDepth, FitsBuildWidth, FitsBuildDepth, Perimeter, Storey, GroundFloorArea, FirstFloorWindows, EavesFacade, EavesGround, EavesFirst, LeftSetback, RightSetback, FrontSetback, RearSetback, BuildWidth, BuildDepth, Facade;
}
