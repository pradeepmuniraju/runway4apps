package com.sok.runway.externalInterface.beans;

import java.util.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.runway.externalInterface.beans.shared.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
public class Contact
{
	private String contactid = null;
	private String title = null;
	private String firstname = null;
	private String lastname = null;
	private String salutation = null;
	private String position = null;
	private String company = null;
	private String department = null;

	private String phone = null;
	private String homephone = null;
	private String fax = null;
	private String mobile = null;
	private String web = null;
	private String email = null;
	private String repuserid = null;
	private String mgruserid = null;
	private Date createddate = null;
	private Date modifieddate = null;
	private String modifiedby = null;
	private String createdby = null;
	private String referrerid = null;
	private String companyid = null;
	private Date followupdate = null;
	private String emailtype = null;
	private String bandwidth = null;
	private String contact = null;
	private String keep = null;
	private String contactstatusid = null;
	private String gktitle = null;
	private String gkfirstname = null;
	private String gklastname = null;
	private String qualifieddate = null;
	private String qualifiedby = null;

	private String notes = null;
	private String tobequalified = null;
	private String email2 = null;
	private String emailstatus = null;
	private String groupid = null;
	private String type = null;
	private String source = null;
	private String accountno = null;
	private String industry = null;
	private String username = null;
	private String password = null;
	private String createdcampaignid = null;
	
	private Address physicaladdress = null;
	private Address postaladdress = null;	
	private Status currentstatus = null;
	private String groupStatusID;
	
	private ContactGroup[] groups = null;  
	
	private String error = null;
	
	public String getError()
	{
		return(error);
	}	
	
	public void setError(String error)
	{
		this.error = error;
	}
	
	@JsonProperty("ContactID")
	public String getContactID()
	{
		return(contactid);
	}	
	
	@JsonProperty("ContactID")
	public void setContactID(String contactid)
	{
		this.contactid = contactid;
	}	
	
	@JsonProperty("Title")
	public String getTitle()
	{
		return(title);
	}	
	
	@XmlTransient 
	public String getName() {
		StringBuilder name = new StringBuilder();
		if(firstname != null)
		{
			name.append(firstname).append(" ");
		}
		if(lastname != null) {
			name.append(lastname);
		}
		return name.toString();
	}
	
	@JsonProperty("Title")
	public void setTitle(String title)
	{
		this.title = title;
	}	
	
	@JsonProperty("FirstName")
	public String getFirstName()
	{
		return(firstname);
	}	
	
	@JsonProperty("FirstName")
	public void setFirstName(String firstname)
	{
		this.firstname = firstname;
	}	
	
	@JsonProperty("LastName")
	public String getLastName()
	{
		return(lastname);
	}	
	
	@JsonProperty("LastName")
	public void setLastName(String lastname)
	{
		this.lastname = lastname;
	}	
	
	public String getSalutation()
	{
		return(salutation);
	}	
	
	public void setSalutation(String salutation)
	{
		this.salutation = salutation;
	}	
	
	public String getPosition()
	{
		return(position);
	}	
	
	public void setPosition(String position)
	{
		this.position = position;
	}	
	
	@JsonProperty("Company")
	public String getCompany()
	{
		return(company);
	}	
	
	@JsonProperty("Company")
	public void setCompany(String company)
	{
		this.company = company;
	}	
	
	public String getDepartment()
	{
		return(department);
	}	
	
	public void setDepartment(String department)
	{
		this.department = department;
	}	
	
	@JsonProperty("Phone")
	public String getPhone()
	{
		return(phone);
	}	
	
	@JsonProperty("Phone")
	public void setPhone(String phone)
	{
		this.phone = phone;
	}	
	
	public String getFax()
	{
		return(fax);
	}	
	
	public void setFax(String fax)
	{
		this.fax = fax;
	}	
	
	@JsonProperty("Mobile")
	public String getMobile()
	{
		return(mobile);
	}	
	
	@JsonProperty("Mobile")
	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}	
	
	public String getWeb()
	{
		return(web);
	}	
	
	public void setWeb(String web)
	{
		this.web = web;
	}	
	
	@JsonProperty("Email")
	public String getEmail()
	{
		return(email);
	}	
	
	@JsonProperty("Email")
	public void setEmail(String email)
	{
		this.email = email;
	}	
	
	public String getRepUserID()
	{
		return(repuserid);
	}	
	
	public void setRepUserID(String repuserid)
	{
		this.repuserid = repuserid;
	}	
	
	public String getMgrUserID()
	{
		return(mgruserid);
	}	
	
	public void setMgrUserID(String mgruserid)
	{
		this.mgruserid = mgruserid;
	}	
	
	public Date getCreatedDate()
	{
		return(createddate);
	}	
	
	public void setCreatedDate(Date createddate)
	{
		this.createddate = createddate;
	}	
	
	public Date getModifiedDate()
	{
		return(modifieddate);
	}	
	
	public void setModifiedDate(Date modifieddate)
	{
		this.modifieddate = modifieddate;
	}	
	
	public String getModifiedBy()
	{
		return(modifiedby);
	}	
	
	public void setModifiedBy(String modifiedby)
	{
		this.modifiedby = modifiedby;
	}	
	
	public String getCreatedBy()
	{
		return(createdby);
	}	
	
	public void setCreatedBy(String createdby)
	{
		this.createdby = createdby;
	}	
	
	public String getReferrerID()
	{
		return(referrerid);
	}	
	
	public void setReferrerID(String referrerid)
	{
		this.referrerid = referrerid;
	}	
	
	public String getCompanyID()
	{
		return(companyid);
	}	
	
	public void setCompanyID(String companyid)
	{
		this.companyid = companyid;
	}	
	
	public Date getFollowUpDate()
	{
		return(followupdate);
	}	
	
	public void setFollowUpDate(Date followupdate)
	{
		this.followupdate = followupdate;
	}	
	
	public String getEmailType()
	{
		return(emailtype);
	}	
	
	public void setEmailType(String emailtype)
	{
		this.emailtype = emailtype;
	}	
	
	public String getBandwidth()
	{
		return(bandwidth);
	}	
	
	public void setBandwidth(String bandwidth)
	{
		this.bandwidth = bandwidth;
	}	
	
	public String getContact()
	{
		return(contact);
	}	
	
	public void setContact(String contact)
	{
		this.contact = contact;
	}	
	
	public String getKeep()
	{
		return(keep);
	}	
	
	public void setKeep(String keep)
	{
		this.keep = keep;
	}	
	
	public String getContactStatusID()
	{
		return(contactstatusid);
	}	
	
	public void setContactStatusID(String contactstatusid)
	{
		this.contactstatusid = contactstatusid;
	}	
	
	public String getGKTitle()
	{
		return(gktitle);
	}	
	
	public void setGKTitle(String gktitle)
	{
		this.gktitle = gktitle;
	}	
	
	public String getGKFirstName()
	{
		return(gkfirstname);
	}	
	
	public void setGKFirstName(String gkfirstname)
	{
		this.gkfirstname = gkfirstname;
	}	
	
	public String getGKLastName()
	{
		return(gklastname);
	}	
	
	public void setGKLastName(String gklastname)
	{
		this.gklastname = gklastname;
	}	
	
	public String getQualifiedDate()
	{
		return(qualifieddate);
	}	
	
	public void setQualifiedDate(String qualifieddate)
	{
		this.qualifieddate = qualifieddate;
	}	
	
	public String getQualifiedBy()
	{
		return(qualifiedby);
	}	
	
	public void setQualifiedBy(String qualifiedby)
	{
		this.qualifiedby = qualifiedby;
	}	
	
	public String getHomePhone()
	{
		return(homephone);
	}	
	
	public void setHomePhone(String homephone)
	{
		this.homephone = homephone;
	}	
	
	public String getNotes()
	{
		return(notes);
	}	
	
	public void setNotes(String notes)
	{
		this.notes = notes;
	}	
	
	public String getToBeQualified()
	{
		return(tobequalified);
	}	
	
	public void setToBeQualified(String tobequalified)
	{
		this.tobequalified = tobequalified;
	}	
	
	public String getEmail2()
	{
		return(email2);
	}	
	
	public void setEmail2(String email2)
	{
		this.email2 = email2;
	}	
	
	public String getEmailStatus()
	{
		return(emailstatus);
	}	
	
	public void setEmailStatus(String emailstatus)
	{
		this.emailstatus = emailstatus;
	}	
	
	@JsonProperty("GroupID")
	public String getGroupID()
	{
		return(groupid);
	}	
	
	@JsonProperty("GroupID")
	public void setGroupID(String groupid)
	{
		this.groupid = groupid;
	}	
	
	public String getType()
	{
		return(type);
	}	
	
	public void setType(String type)
	{
		this.type = type;
	}	
	
	public String getSource()
	{
		return(source);
	}	
	
	public void setSource(String source)
	{
		this.source = source;
	}	
	
	public String getAccountNo()
	{
		return(accountno);
	}	
	
	public void setAccountNo(String accountno)
	{
		this.accountno = accountno;
	}	
	
	public String getIndustry()
	{
		return(industry);
	}	
	
	public void setIndustry(String industry)
	{
		this.industry = industry;
	}	
	
	public String getUsername()
	{
		return(username);
	}	
	
	public void setUsername(String username)
	{
		this.username = username;
	}	
	
	public String getPassword()
	{
		return(password);
	}	
	
	public void setPassword(String password)
	{
		this.password = password;
	}	
	
	public String getCreatedCampaignID()
	{
		return(createdcampaignid);
	}	
	
	public void setCreatedCampaignID(String createdcampaignid)
	{
		this.createdcampaignid = createdcampaignid;
	}	

	@JsonProperty("PhysicalAddress")
	public Address getPhysicalAddress()
	{
		return(physicaladdress);
	}
	
	@JsonProperty("PhysicalAddress")
	public void setPhysicalAddress(Address address)
	{
		physicaladdress = address;
	}
	
	@JsonProperty("PostalAddress")
	public Address getPostalAddress()
	{
		return(postaladdress);
	}
	
	@JsonProperty("PostalAddress")
	public void setPostalAddress(Address address)
	{
		postaladdress = address;
	}	
	
	public Status getCurrentStatus()
	{
		return(currentstatus);
	}
	
	public void setCurrentStatus(Status status)
	{
		currentstatus = status;
	}	
	
	private PersonName modifiedbyname = null;
	private PersonName createdbyname = null;
	private PersonName qualifiedbyname = null;	
	private PersonName repname = null;	
	private PersonName mgrname = null;	
	
	public PersonName getModifiedByName()
	{
		return(modifiedbyname);
	}
	
	public void setModifiedByName(PersonName modifiedbyname)
	{
		this.modifiedbyname = modifiedbyname;
	}
	
	public PersonName getCreatedByName()
	{
		return(createdbyname);
	}
	
	public void setCreatedByName(PersonName createdbyname)
	{
		this.createdbyname = createdbyname;
	}	
	
	public PersonName getQualifiedByName()
	{
		return(qualifiedbyname);
	}
	
	public void setQualifiedByName(PersonName qualifiedbyname)
	{
		this.qualifiedbyname = qualifiedbyname;
	}	
	
	public PersonName getRepName()
	{
		return(repname);
	}
	
	public void setRepName(PersonName repname)
	{
		this.repname = repname;
	}
	
	public PersonName getMgrName()
	{
		return(mgrname);
	}
	
	public void setMgrName(PersonName mgrname)
	{
		this.mgrname = mgrname;
	}		
	
	//TEMP - move to list of group stauts'
	@JsonProperty("GroupStatusID")
	public String getGroupStatusID() {
		return groupStatusID;
	}
	@JsonProperty("GroupStatusID")
	public void setGroupStatusID(String groupStatusID) {
		this.groupStatusID = groupStatusID;
	}

	public ContactGroup[] getGroups() {
		return groups;
	}

	public void setGroups(ContactGroup[] groups) {
		this.groups = groups;
	}
}