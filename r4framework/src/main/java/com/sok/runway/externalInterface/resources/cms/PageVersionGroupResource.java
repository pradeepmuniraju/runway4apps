package com.sok.runway.externalInterface.resources.cms;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.cms.PageVersionGroup;
import com.sok.service.cms.WebsiteService;

public class PageVersionGroupResource {

	private static final Logger logger = LoggerFactory.getLogger(PageVersionGroupResource.class);
	private static final WebsiteService websiteService = WebsiteService.getInstance();
	
	@Context HttpServletRequest request;
	private final String cmsID;
	public PageVersionGroupResource(String cmsID, HttpServletRequest request) {
		this.cmsID = cmsID;
		this.request = request;
	}

	@GET
	@Path("{PageVersionGroupID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PageVersionGroup getPageVersionGroup(@PathParam("PageVersionGroupID") String PageVersionGroupID) {
		if(logger.isDebugEnabled()) logger.debug("getPageVersionGroup(request, cmsID={}, pageVersionGroupID={})",cmsID, PageVersionGroupID);
		return null;
	}
	
	@PUT
	@Path("{PageVersionGroupID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PageVersionGroup updatePageVersionGroup(@PathParam("PageVersionGroupID") String PageVersionGroupID, PageVersionGroup pageVersionGroup) {
		if(logger.isDebugEnabled()) logger.debug("updatePageVersionGroup(request, cmsID={}, pageVersionGroupID={})",cmsID, PageVersionGroupID);
		return null;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public PageVersionGroup updatePageVersionGroup(@PathParam("PageVersionGroupID") String PageVersionGroupID) {
		if(logger.isDebugEnabled()) logger.debug("updatePageVersionGroup(request, cmsID={}, pageVersionGroupID={})",cmsID, PageVersionGroupID);
		return null;
	}
	
}
