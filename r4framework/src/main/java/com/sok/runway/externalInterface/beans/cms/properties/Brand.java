package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.shared.PersonName;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Brand")
@XmlType(propOrder={/* "brandID", "name",*/"productNum", "description", "URL","thumbnailImage","createdBy","createdByName","createdDate","modifiedBy","modifiedByName","modifiedDate"})
public class Brand extends SimpleBrand {
	
	private String productNum, description, url, createdBy, modifiedBy;
	private Date createdDate, modifiedDate;
	private PersonName modifiedByName = null, createdByName = null;
	private SimpleDocument thumbnailImage;
	/*
	@Override
	@XmlElement(name="BrandID")
	public String getBrandID() {
		return brandID;
	}
	public void setBrandID(String brandID) {
		this.brandID = brandID;
	}
	@Override
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	*/
	@XmlElement(name="ProductNum")
	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}
	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@XmlElement(name="URL")
	public String getURL() {
		return url;
	}
	public void setURL(String url) {
		this.url = url;
	}
	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName(){
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedByName = modifiedbyname;
	}
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@XmlElement(name="ThumbnailImage")
	public SimpleDocument getThumbnailImage() {
		return thumbnailImage;
	}
	public void setThumbnailImage(SimpleDocument thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}
}
