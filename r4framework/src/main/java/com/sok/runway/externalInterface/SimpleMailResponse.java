package com.sok.runway.externalInterface;

import java.io.StringWriter;
import java.util.Date;

import javax.servlet.ServletConfig;

import org.apache.velocity.app.VelocityEngine;

import com.sok.framework.InitServlet;
import com.sok.framework.MailerBean;
import com.sok.framework.RowBean;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;

public class SimpleMailResponse {

	private String status = null;
	private String message = null;

	private static final String DEFAULT_VELOCITY_PATH = "/cms/emails/offlinealerts/";
	
	private final String defaultFrom = "dionc@runway.com.au";

	protected String alerttext = "generic/generic_text.vt";
	protected String alerthtml = "generic/generic.vt";

	public SimpleMailResponse() {}

	public SimpleMailResponse(String status, String message) {
		this.status = status;
		this.message = message;
	}

	public String getStatus() {
		return (status);
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return (message);
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private String generateEmail(TableData context, String script) {
		StringWriter text = new StringWriter();
		try {
			StringBuffer roottemplate = new StringBuffer();
			roottemplate.append("#parse(\"");
			roottemplate.append(DEFAULT_VELOCITY_PATH);
			roottemplate.append(script);
			roottemplate.append("\")");

			VelocityEngine ve = VelocityManager.getEngine();
			ve.evaluate(context, text, "StatusAlerts", roottemplate.toString());

			text.flush();
			text.close();

		} catch (Exception e) {
			//logger.error(e);
		}
		return (text.toString());
	}

	public String sendEmail(String to, String subject, String msg) {
		//logger.debug("sending email to " + to);
		try {
			RowBean context = new RowBean();
			context.put("URLHome", InitServlet.getSystemParam("URLHome"));
			context.put("alert", to);
			context.put("body", msg);
			context.put("subject", subject);
			context.put("date", new Date());

			String text = generateEmail(context, alerttext);
			String html = generateEmail(context, alerthtml);
			//String text = generateEmail(context, StringUtil.convertHtmlToText(msg));
			//String html = generateEmail(context, msg);

			ServletConfig sc = InitServlet.getConfig();
			if (sc != null && to.length() != 0) {
				MailerBean mailer = new MailerBean();
				mailer.setServletContext(sc.getServletContext());
				mailer.setFrom(defaultFrom);
				mailer.setTo(to);
				mailer.setSubject(subject);
				mailer.setTextbody(text);
				mailer.setHtmlbody(html);
				//if (config != null && config.getPretend()) {
					//debug("email to " + to + "\r\n " + html);
				//} else {
					return mailer.getResponse();
					//debug("email to " + to + " " + response);
				//}
			}

		} catch (Exception e) {
		//	error(e);
		}
		
		return "Failed";
	}

}
