package com.sok.runway.externalInterface.beans;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no bussiness logic.
 * 
 */
public class IDList extends Result
{
	private String[] ids = null;
	
	public String[] getIDs()
	{
		return(ids);
	}	
	
	public void setIDs(String[] ids)
	{
		this.ids = ids;
	}		
	
	private String error = null;
	
	public String getError()
	{
		return(error);
	}	
	
	public void setError(String error)
	{
		this.error = error;
	}	
}