package com.sok.runway.externalInterface.beans.shared;
import java.util.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.ItemList;
import com.sok.runway.view.ValueList;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
public abstract class Question
{
	private String questionid = null;
	private String label = null;
	private String exportlabel = null;
	private String inputtype = null;
	private String valuelist = null;
	private String parentquestionid = null;
	private Date createddate = null;
	private String createdby = null;
	private Date modifieddate = null;
	private String modifiedby = null;
	private String description = null;
	private String calculationtype = null;
	private String relatedquestionid = null;
	private String calculationquestionid = null;
	private String calculationperiod = null;
	private String productgroupid = null;
	private String groupid = null;
	private ItemList itemList = null;
	private String surveyquestionid = null;
	private String sortorder = null;
	private String indentlevel = null;
	private String divider = null;
	private String quantifier = null;
	private String disabled = null;
	private String mandatory = null;
	
	
	@JsonProperty("QuestionID")
	public String getQuestionID()
	{
		return(questionid);
	}	
	
	@JsonProperty("QuestionID")
	public void setQuestionID(String questionid)
	{
		this.questionid = questionid;
	}	
	
	@JsonProperty("Label")
	public String getLabel()
	{
		return(label);
	}	
	
	@JsonProperty("Label")
	public void setLabel(String label)
	{
		this.label = label;
	}	
	
	public String getExportLabel()
	{
		return(exportlabel);
	}	
	
	public void setExportLabel(String exportlabel)
	{
		this.exportlabel = exportlabel;
	}	
	
	@JsonProperty("InputType")
	public String getInputType()
	{
		return(inputtype);
	}	
	
	@JsonProperty("InputType")
	public void setInputType(String inputtype)
	{
		this.inputtype = inputtype;
	}	
	
	public String getValueList()
	{
		return(valuelist);
	}	
	
	public void setValueList(String valuelist)
	{
		this.valuelist = valuelist;
	}	
	
	public String getParentQuestionID()
	{
		return(parentquestionid);
	}	
	
	public void setParentQuestionID(String parentquestionid)
	{
		this.parentquestionid = parentquestionid;
	}	
	
	public Date getCreatedDate()
	{
		return(createddate);
	}	
	
	public void setCreatedDate(Date createddate)
	{
		this.createddate = createddate;
	}	
	
	public String getCreatedBy()
	{
		return(createdby);
	}	
	
	public void setCreatedBy(String createdby)
	{
		this.createdby = createdby;
	}	
	
	public Date getModifiedDate()
	{
		return(modifieddate);
	}	
	
	public void setModifiedDate(Date modifieddate)
	{
		this.modifieddate = modifieddate;
	}	
	
	public String getModifiedBy()
	{
		return(modifiedby);
	}	
	
	public void setModifiedBy(String modifiedby)
	{
		this.modifiedby = modifiedby;
	}	
	
	@JsonProperty("Description")
	public String getDescription()
	{
		return(description);
	}	
	
	@JsonProperty("Description")
	public void setDescription(String description)
	{
		this.description = description;
	}	
	
	public String getCalculationType()
	{
		return(calculationtype);
	}	
	
	public void setCalculationType(String calculationtype)
	{
		this.calculationtype = calculationtype;
	}	
	
	public String getRelatedQuestionID()
	{
		return(relatedquestionid);
	}	
	
	public void setRelatedQuestionID(String relatedquestionid)
	{
		this.relatedquestionid = relatedquestionid;
	}	
	
	public String getCalculationQuestionID()
	{
		return(calculationquestionid);
	}	
	
	public void setCalculationQuestionID(String calculationquestionid)
	{
		this.calculationquestionid = calculationquestionid;
	}	
	
	public String getCalculationPeriod()
	{
		return(calculationperiod);
	}	
	
	public void setCalculationPeriod(String calculationperiod)
	{
		this.calculationperiod = calculationperiod;
	}	
	
	public String getProductGroupID()
	{
		return(productgroupid);
	}	
	
	public void setProductGroupID(String productgroupid)
	{
		this.productgroupid = productgroupid;
	}	
	
	public String getGroupID()
	{
		return(groupid);
	}	
	
	public void setGroupID(String groupid)
	{
		this.groupid = groupid;
	}		

	public String getSurveyQuestionID()
	{
		return(surveyquestionid);
	}	
	
	public void setSurveyQuestionID(String surveyquestionid)
	{
		this.surveyquestionid = surveyquestionid;
	}	
	
	@JsonProperty("SortOrder")
	public String getSortOrder()
	{
		return(sortorder);
	}	
	
	@JsonProperty("SortOrder")
	public void setSortOrder(String sortorder)
	{
		this.sortorder = sortorder;
	}	
	
	public String getIndentLevel()
	{
		return(indentlevel);
	}	
	
	public void setIndentLevel(String indentlevel)
	{
		this.indentlevel = indentlevel;
	}	
	
	public String getDivider()
	{
		return(divider);
	}	
	
	public void setDivider(String divider)
	{
		this.divider = divider;
	}	
	
	public String getQuantifier()
	{
		return(quantifier);
	}	
	
	public void setQuantifier(String quantifier)
	{
		this.quantifier = quantifier;
	}	
	
	@JsonIgnore
	public String getDisabled()
	{
		return(disabled);
	}	
	
	@JsonIgnore
	public void setDisabled(String disabled)
	{
		this.disabled = disabled;
	}	
	
	@JsonProperty("Disabled")
	public void setDisabled(boolean disabled) {
		this.disabled = disabled ? "Y" : "N";
	}
	
	@JsonProperty("Disabled")
	public boolean isDisabled() {
		return "Y".equals(this.disabled);
	}
	
	@JsonIgnore
	public String getMandatory()
	{
		return(mandatory);
	}	
	
	@JsonIgnore
	public void setMandatory(String mandatory)
	{
		this.mandatory = mandatory;
	}		
	
	@JsonProperty("Mandatory")
	public boolean isMandatory() {
		return "Y".equals(this.mandatory);
	}
	
	@JsonProperty("Mandatory")
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory ? "Y" : "N";
	}
	
	@JsonProperty("List")
	public void setList(ItemList itemList) {
		this.itemList = itemList;
	}
	
	@JsonProperty("List")
	public ItemList getList() {
		return itemList;
	}

	/*
	 * 'Calculation' 'Checkbox' 'Currency' 'Date' 'Drop Down' 'Hidden' 'Large Number' 'List' 'Long'
	 * 'Medium' 'Multi-line' 'Number' 'Percentage' 'Radio Button' 'Short' 'Sum Act. Orders' 'Tiny' 'YesNo'
	 */
	public int calculateHeight() {
		if (this.inputtype == null || this.inputtype.length() == 0) return 0;
		if ("Checkbox".equalsIgnoreCase(this.inputtype) || "Radio Button".equalsIgnoreCase(this.inputtype)) {
			return (valuelist == null || valuelist.length() == 0)? 1 : getItemListCount();
		} else if ("Multi-line".equalsIgnoreCase(this.inputtype)) {
			return 4;
		} else if ("List".equalsIgnoreCase(this.inputtype)) {
			return (valuelist == null || valuelist.length() == 0)? 1 : Math.min(4, getItemListCount());
		} else if ("Hidden".equalsIgnoreCase(this.inputtype) || "Calculation".equalsIgnoreCase(this.inputtype) || "Sum Act. Orders".equalsIgnoreCase(this.inputtype)) {
			return 0;
		}
		
		return 1;
	}

	public int getItemListCount() {
		if (itemList == null) {
			itemList = loadItemList();
		}
		
		return itemList != null && itemList.getListItems() != null? itemList.getListItems().length : 0;
	}

	public ItemList loadItemList() {
		if (itemList == null && valuelist != null) {
			ValueList vl = ValueList.getValueList(getValueList());
			//SingleItemList sil = itemLists.getList(cq.getValueList());
			if(vl != null) {
				setList(BeanFactory.getItemList(vl));
			}
		}
		return itemList;
				
	}
	
}
