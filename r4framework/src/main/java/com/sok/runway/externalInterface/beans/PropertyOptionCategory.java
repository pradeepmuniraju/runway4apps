package com.sok.runway.externalInterface.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.FIELD)
public class PropertyOptionCategory {

	public String CategoryID;
	public String CategoryName;
	public String CategoryNum;
	
	public PropertyOptionCategory() {} 
	public PropertyOptionCategory(GenRow product) {
		this.CategoryID = product.getString("CategoryID");
		this.CategoryName = product.getString("CategoryName");
		this.CategoryNum = product.getString("CategoryNum");
	}
	
}
