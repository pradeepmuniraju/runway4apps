package com.sok.runway.externalInterface.oauth;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.amber.oauth2.as.request.OAuthAuthzRequest;
import org.apache.amber.oauth2.as.response.OAuthASResponse;
import org.apache.amber.oauth2.common.OAuth;
import org.apache.amber.oauth2.common.exception.OAuthSystemException;
import org.apache.amber.oauth2.common.message.OAuthResponse;
import org.apache.amber.oauth2.common.token.OAuthToken;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.beans.OAuthTokenImpl;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/oauth")
@Api(value = "/oauth", description = "OAuth Resource")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class OAuthResource {

	private static final Logger logger = LoggerFactory.getLogger(OAuthResource.class);
	private static final String prop = InitServlet.getSystemParam("URLHome") + "/oauth";
	@Context HttpServletRequest request;
	@Context HttpHeaders headers;
	
	@POST
	@Path("authorize")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response doAuthorize(@FormParam("client_id") String clientId, @FormParam("request_key") String requestKey) throws IOException, OAuthSystemException {
		logger.debug("doAuthorize(client_id={},request_key={})", clientId, requestKey);
		
		if(!request.isSecure() && !"127.0.0.1".equals(request.getRemoteAddr())) {
			throw new UnsupportedOperationException("https urls must be used at all times");
		}
		
		String referer = request.getHeader("referer");
		
		if(referer == null || !referer.startsWith(prop)) {
			logger.error("Expected path: {}", prop);
			logger.error("Got: {}", referer);
		}
		
		logger.debug(request.getParameterMap().toString());

		OAuthAuthzRequest oauthRequest = (OAuthAuthzRequest)request.getSession().getAttribute(requestKey);
		logger.debug("client_id={}", oauthRequest.getClientId());
		//logger.debug("client_secret={}", oauthRequest.getClientSecret());
		logger.debug("client_state={}", ((OAuthAuthzRequest) oauthRequest).getState());
		logger.debug("response_type={}", ((OAuthAuthzRequest) oauthRequest).getResponseType());
		
		boolean offline = "offline".equals(oauthRequest.getParam("access_type"));
		
		if(!StringUtils.equals(clientId, oauthRequest.getClientId())) {
			throw new IllegalStateException("Incorrect request validation");
		}
		
		UserBean user = UserService.getInstance().getCurrentUser(request);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in, or have logged out during the process");
		}
		
		if(StringUtils.isBlank(clientId)) {
			throw new IllegalArgumentException("client_id must be supplied");
		} else if (StringUtils.containsAny(clientId, discludeChars)) {
			throw new IllegalArgumentException("client_id was invalid");
		}
		
		GenRow app = new GenRow();
		app.setRequest(request);
		app.setViewSpec("oauth/ClientApplicationView");
		app.setParameter("ClientID", clientId);
		//app.setParameter("ClientSecret", oauthRequest.getClientSecret());
		app.doAction(GenerationKeys.SELECTFIRST);
		
		if(!app.isSuccessful()) {
			throw new UnsupportedOperationException("OAuth services are only available over https");
		}
		
		String userID = user.getUserID();

		if (StringUtils.isBlank(userID)) userID = app.getData("ClientID");
		
		
		GenRow token = new GenRow();
		token.setRequest(request);
		token.setViewSpec("oauth/OAuthTokenView");
		token.setParameter("OAuthTokenID", KeyMaker.generate());
		token.setParameter("AccessToken", KeyMaker.generate(45));
		if(offline) {
			token.setParameter("RefreshToken", KeyMaker.generate(45));
		}
		token.setParameter("AuthCode", KeyMaker.generate(30));
		token.setParameter("ClientAppID", app.getData("ClientAppID"));
		token.setParameter("CreatedBy", userID);
		token.setParameter("CreatedDate", "NOW");
		token.setParameter("RequestedFrom", request.getRemoteAddr());
		token.setParameter("UserID", userID);
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, 90);
		
		token.setParameter("ExpiryDate", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(c.getTime()));
		token.doAction(GenerationKeys.INSERT);
		
		if(!token.isSuccessful()) {
			throw new RuntimeException(token.getError());
		}
		
		OAuthResponse resp = OAuthASResponse
				.authorizationResponse(request, HttpServletResponse.SC_FOUND)
				.setCode(token.getString("AuthCode"))  
				.setParam(OAuth.OAUTH_STATE, oauthRequest.getState())	//this is a hack, setState should be public but isn't.
				.location(oauthRequest.getRedirectURI())
				.buildQueryMessage();

		try {
			return Response.temporaryRedirect(new URI(resp.getLocationUri())).build();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private char[] discludeChars = {'_','%'};
	
	@POST
	@Path("/token")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@ApiOperation(value = "Exchange oauth code or refresh token for new access token", responseClass="com.sok.runway.externalInterface.beans.OAuthTokenImpl")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid values"), @ApiError(code = 401, reason = "client not found"), @ApiError(code = 403, reason = "code/token not found") })
	public OAuthToken getToken(
			@ApiParam(value = "The type of request", allowableValues="authorization_code,refresh_token", required = true) @FormParam("grant_type") String grantType, 
			@ApiParam(value = "Your client id", required=true) @FormParam("client_id") String clientId, 
			@ApiParam(value = "Your client secret", required=true) @FormParam("client_secret") String clientSecret,
			@ApiParam(value = "A recently supplied access_code", required = false) @FormParam("code") String code, 
			@ApiParam(value = "A refresh token previously supplied", required = false) @FormParam("refresh_token") String refreshToken) {
		
		if(!request.isSecure() && !"127.0.0.1".equals(request.getRemoteAddr())) {
			throw new UnsupportedOperationException("OAuth services are only available over https");
		}
		
		if(StringUtils.isBlank(clientId) || StringUtils.isBlank(clientSecret)) {
			throw new IllegalArgumentException("client_id and client_secret must be supplied");
		} else if (StringUtils.containsAny(clientId, discludeChars) || StringUtils.containsAny(clientSecret, discludeChars)) {
			throw new IllegalArgumentException("client_id or client_secret was invalid");
		}
		GenRow app = new GenRow();
		app.setRequest(request);
		app.setViewSpec("oauth/ClientApplicationView");
		app.setParameter("ClientID", clientId);
		app.setParameter("ClientSecret", clientSecret);
		app.doAction(GenerationKeys.SELECTFIRST);
		
		if(!app.isSuccessful()) {
			throw new NotAuthenticatedException("client_id and client_secret did not match a currently registered application");
		}
		
		if("authorization_code".equals(grantType)) { 
			if(StringUtils.isBlank(code) || StringUtils.containsAny(code, discludeChars)) {
				throw new IllegalArgumentException("code was invalid or blank");
			}
			
			GenRow token = new GenRow();
			token.setRequest(request);
			token.setViewSpec("oauth/OAuthTokenView");
			token.setParameter("ExpiryDate",">NOW");
			token.setParameter("AuthCode", code);
			token.setParameter("ClientAppID", app.getData("ClientAppID"));
			
			if(logger.isTraceEnabled()) {
				token.doAction(GenerationKeys.SEARCH);
				logger.trace(token.getStatement());
			}
			
			token.doAction(GenerationKeys.SELECTFIRST);
			
			System.out.println(token.getStatement());
			
			if(!token.isSuccessful()) {
				throw new AccessException("code was invalid or has expired. code must be exchanged for token within 1 hour of being generated");
			}
			
			return new OAuthTokenImpl(clientSecret, token.getData("AccessToken"), token.getDate("ExpiryDate"), token.getDate("CreatedDate"), StringUtils.trimToNull(token.getData("RefreshToken")));
		} else if("refresh_token".equals(grantType)) { 
			
			//     grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA&client_id=s6BhdRkqt3&client_secret=7Fjfp0ZBr1KtDRbnfVdmIw
			
			if(StringUtils.isBlank(refreshToken) || StringUtils.containsAny(refreshToken, discludeChars)) {
				throw new IllegalArgumentException("refresh_token was invalid or blank");
			}
			GenRow refresh = new GenRow();
			refresh.setRequest(request);
			refresh.setViewSpec("oauth/OAuthTokenView");
			refresh.setParameter("RefreshToken", refreshToken);
			refresh.doAction(GenerationKeys.SELECTFIRST);
			
			if(!refresh.isSuccessful()) {
				logger.debug("Unknown refresh_token [{}]", refreshToken);
				throw new AccessException("refresh_token was invalid or has been revoked. if this is incorrect, a new access/refresh token may be requested");
			}

			String userID = refresh.getData("UserID");
			UserBean user = UserService.getInstance().getCurrentUser(request);

			if (user != null && StringUtils.isBlank(userID)) userID = user.getUserID();
			
			GenRow token = new GenRow();
			token.setRequest(request);
			token.setViewSpec("oauth/OAuthTokenView");
			token.setParameter("OAuthTokenID", KeyMaker.generate());
			token.setParameter("AccessToken", KeyMaker.generate(45));
			token.setParameter("ClientAppID", app.getData("ClientAppID"));
			token.setParameter("CreatedBy", "E3B3653517125741C947061350C510E4");		// system internal
			token.setParameter("CreatedDate","NOW");
			token.setParameter("UserID", userID);
			token.setParameter("RequestedFrom", request.getRemoteAddr());
			
			Calendar c = Calendar.getInstance();
			c.add(Calendar.MINUTE, 90);
			
			token.setParameter("ExpiryDate", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(c.getTime()));
			token.doAction(GenerationKeys.INSERT);
			
			if(!token.isSuccessful()) {
				throw new RuntimeException(token.getError());
			}
			
			String id = token.getParameter("OAuthTokenID");
			token.clear();
			token.setParameter("OAuthTokenID", id);
			token.doAction(GenerationKeys.SELECT);
			
			if(!token.isSuccessful()) {
				throw new RuntimeException(token.getError());
			}
			
			return new OAuthTokenImpl(clientSecret, token.getData("AccessToken"), token.getDate("ExpiryDate"), token.getDate("CreatedDate"), null);
		}
		throw new UnsupportedOperationException("Not yet implmented other than authorization_code grants");
	}
}
