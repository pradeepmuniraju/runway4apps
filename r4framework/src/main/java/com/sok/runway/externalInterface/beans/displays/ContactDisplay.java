package com.sok.runway.externalInterface.beans.displays;

import com.sok.runway.externalInterface.beans.displays.shared.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class ContactDisplay
{
	private String titleLabel = null;
	private boolean titleNotUsed = false;
	private String firstnameLabel = null;
	private boolean firstnameNotUsed = false;
	private String lastnameLabel = null;
	private boolean lastnameNotUsed = false;
	private String salutationLabel = null;
	private boolean salutationNotUsed = false;
	private String positionLabel = null;
	private boolean positionNotUsed = false;
	private String companyLabel = null;
	private boolean companyNotUsed = false;
	private String departmentLabel = null;
	private boolean departmentNotUsed = false;

	private String phoneLabel = null;
	private boolean phoneNotUsed = false;
	private String faxLabel = null;
	private boolean faxNotUsed = false;
	private String mobileLabel = null;
	private boolean mobileNotUsed = false;
	private String webLabel = null;
	private boolean webNotUsed = false;
	private String emailLabel = null;
	private boolean emailNotUsed = false;
	private String repuseridLabel = null;
	private boolean repuseridNotUsed = false;
	private String mgruseridLabel = null;
	private boolean mgruseridNotUsed = false;

	private String referreridLabel = null;
	private boolean referreridNotUsed = false;
	private String companyidLabel = null;
	private boolean companyidNotUsed = false;
	private String followupdateLabel = null;
	private boolean followupdateNotUsed = false;
	private String emailtypeLabel = null;
	private boolean emailtypeNotUsed = false;
	private String bandwidthLabel = null;
	private boolean bandwidthNotUsed = false;
	private String contactLabel = null;
	private boolean contactNotUsed = false;
	private String keepLabel = null;
	private boolean keepNotUsed = false;
	private String contactstatusidLabel = null;
	private boolean contactstatusidNotUsed = false;
	private String gktitleLabel = null;
	private boolean gktitleNotUsed = false;
	private String gkfirstnameLabel = null;
	private boolean gkfirstnameNotUsed = false;
	private String gklastnameLabel = null;
	private boolean gklastnameNotUsed = false;

	private String qualifiedLabel = null;
	private boolean qualifiedNotUsed = false;

	private String notesLabel = null;
	private boolean notesNotUsed = false;
	private String tobequalifiedLabel = null;
	private boolean tobequalifiedNotUsed = false;
	private String email2Label = null;
	private boolean email2NotUsed = false;
	private String emailstatusLabel = null;
	private boolean emailstatusNotUsed = false;
	private String groupidLabel = null;
	private boolean groupidNotUsed = false;
	private String typeLabel = null;
	private boolean typeNotUsed = false;
	private String sourceLabel = null;
	private boolean sourceNotUsed = false;
	private String accountnoLabel = null;
	private boolean accountnoNotUsed = false;
	private String industryLabel = null;
	private boolean industryNotUsed = false;
	private String usernameLabel = null;
	private boolean usernameNotUsed = false;
	private String passwordLabel = null;
	private boolean passwordNotUsed = false;
	
	public String getTitleLabel()
	{
		return(titleLabel);
	}	
	
	public void setTitleLabel(String titleLabel)
	{
		this.titleLabel = titleLabel;
	}	
	
	public boolean getTitleNotUsed()
	{
		return(titleNotUsed);
	}	
	
	public void setTitleNotUsed(boolean titleNotUsed)
	{
		this.titleNotUsed = titleNotUsed;
	}	
	
	public String getFirstNameLabel()
	{
		return(firstnameLabel);
	}	
	
	public void setFirstNameLabel(String firstnameLabel)
	{
		this.firstnameLabel = firstnameLabel;
	}	
	
	public boolean getFirstNameNotUsed()
	{
		return(firstnameNotUsed);
	}	
	
	public void setFirstNameNotUsed(boolean firstnameNotUsed)
	{
		this.firstnameNotUsed = firstnameNotUsed;
	}	
	
	public String getLastNameLabel()
	{
		return(lastnameLabel);
	}	
	
	public void setLastNameLabel(String lastnameLabel)
	{
		this.lastnameLabel = lastnameLabel;
	}	
	
	public boolean getLastNameNotUsed()
	{
		return(lastnameNotUsed);
	}	
	
	public void setLastNameNotUsed(boolean lastnameNotUsed)
	{
		this.lastnameNotUsed = lastnameNotUsed;
	}	
	
	public String getSalutationLabel()
	{
		return(salutationLabel);
	}	
	
	public void setSalutationLabel(String salutationLabel)
	{
		this.salutationLabel = salutationLabel;
	}	
	
	public boolean getSalutationNotUsed()
	{
		return(salutationNotUsed);
	}	
	
	public void setSalutationNotUsed(boolean salutationNotUsed)
	{
		this.salutationNotUsed = salutationNotUsed;
	}	
	
	public String getPositionLabel()
	{
		return(positionLabel);
	}	
	
	public void setPositionLabel(String positionLabel)
	{
		this.positionLabel = positionLabel;
	}	
	
	public boolean getPositionNotUsed()
	{
		return(positionNotUsed);
	}	
	
	public void setPositionNotUsed(boolean positionNotUsed)
	{
		this.positionNotUsed = positionNotUsed;
	}	
	
	public String getCompanyLabel()
	{
		return(companyLabel);
	}	
	
	public void setCompanyLabel(String companyLabel)
	{
		this.companyLabel = companyLabel;
	}	
	
	public boolean getCompanyNotUsed()
	{
		return(companyNotUsed);
	}	
	
	public void setCompanyNotUsed(boolean companyNotUsed)
	{
		this.companyNotUsed = companyNotUsed;
	}	
	
	public String getDepartmentLabel()
	{
		return(departmentLabel);
	}	
	
	public void setDepartmentLabel(String departmentLabel)
	{
		this.departmentLabel = departmentLabel;
	}	
	
	public boolean getDepartmentNotUsed()
	{
		return(departmentNotUsed);
	}	
	
	public void setDepartmentNotUsed(boolean departmentNotUsed)
	{
		this.departmentNotUsed = departmentNotUsed;
	}		
	
	public String getPhoneLabel()
	{
		return(phoneLabel);
	}	
	
	public void setPhoneLabel(String phoneLabel)
	{
		this.phoneLabel = phoneLabel;
	}	
	
	public boolean getPhoneNotUsed()
	{
		return(phoneNotUsed);
	}	
	
	public void setPhoneNotUsed(boolean phoneNotUsed)
	{
		this.phoneNotUsed = phoneNotUsed;
	}	
	
	public String getFaxLabel()
	{
		return(faxLabel);
	}	
	
	public void setFaxLabel(String faxLabel)
	{
		this.faxLabel = faxLabel;
	}	
	
	public boolean getFaxNotUsed()
	{
		return(faxNotUsed);
	}	
	
	public void setFaxNotUsed(boolean faxNotUsed)
	{
		this.faxNotUsed = faxNotUsed;
	}	
	
	public String getMobileLabel()
	{
		return(mobileLabel);
	}	
	
	public void setMobileLabel(String mobileLabel)
	{
		this.mobileLabel = mobileLabel;
	}	
	
	public boolean getMobileNotUsed()
	{
		return(mobileNotUsed);
	}	
	
	public void setMobileNotUsed(boolean mobileNotUsed)
	{
		this.mobileNotUsed = mobileNotUsed;
	}	
	
	public String getWebLabel()
	{
		return(webLabel);
	}	
	
	public void setWebLabel(String webLabel)
	{
		this.webLabel = webLabel;
	}	
	
	public boolean getWebNotUsed()
	{
		return(webNotUsed);
	}	
	
	public void setWebNotUsed(boolean webNotUsed)
	{
		this.webNotUsed = webNotUsed;
	}	
	
	public String getEmailLabel()
	{
		return(emailLabel);
	}	
	
	public void setEmailLabel(String emailLabel)
	{
		this.emailLabel = emailLabel;
	}	
	
	public boolean getEmailNotUsed()
	{
		return(emailNotUsed);
	}	
	
	public void setEmailNotUsed(boolean emailNotUsed)
	{
		this.emailNotUsed = emailNotUsed;
	}	
	
	public String getRepUserIDLabel()
	{
		return(repuseridLabel);
	}	
	
	public void setRepUserIDLabel(String repuseridLabel)
	{
		this.repuseridLabel = repuseridLabel;
	}	
	
	public boolean getRepUserIDNotUsed()
	{
		return(repuseridNotUsed);
	}	
	
	public void setRepUserIDNotUsed(boolean repuseridNotUsed)
	{
		this.repuseridNotUsed = repuseridNotUsed;
	}	
	
	public String getMgrUserIDLabel()
	{
		return(mgruseridLabel);
	}	
	
	public void setMgrUserIDLabel(String mgruseridLabel)
	{
		this.mgruseridLabel = mgruseridLabel;
	}	
	
	public boolean getMgrUserIDNotUsed()
	{
		return(mgruseridNotUsed);
	}	
	
	public void setMgrUserIDNotUsed(boolean mgruseridNotUsed)
	{
		this.mgruseridNotUsed = mgruseridNotUsed;
	}	
	
	public String getReferrerIDLabel()
	{
		return(referreridLabel);
	}	
	
	public void setReferrerIDLabel(String referreridLabel)
	{
		this.referreridLabel = referreridLabel;
	}	
	
	public boolean getReferrerIDNotUsed()
	{
		return(referreridNotUsed);
	}	
	
	public void setReferrerIDNotUsed(boolean referreridNotUsed)
	{
		this.referreridNotUsed = referreridNotUsed;
	}	
	
	public String getCompanyIDLabel()
	{
		return(companyidLabel);
	}	
	
	public void setCompanyIDLabel(String companyidLabel)
	{
		this.companyidLabel = companyidLabel;
	}	
	
	public boolean getCompanyIDNotUsed()
	{
		return(companyidNotUsed);
	}	
	
	public void setCompanyIDNotUsed(boolean companyidNotUsed)
	{
		this.companyidNotUsed = companyidNotUsed;
	}	
	
	public String getFollowUpDateLabel()
	{
		return(followupdateLabel);
	}	
	
	public void setFollowUpDateLabel(String followupdateLabel)
	{
		this.followupdateLabel = followupdateLabel;
	}	
	
	public boolean getFollowUpDateNotUsed()
	{
		return(followupdateNotUsed);
	}	
	
	public void setFollowUpDateNotUsed(boolean followupdateNotUsed)
	{
		this.followupdateNotUsed = followupdateNotUsed;
	}	
	
	public String getEmailTypeLabel()
	{
		return(emailtypeLabel);
	}	
	
	public void setEmailTypeLabel(String emailtypeLabel)
	{
		this.emailtypeLabel = emailtypeLabel;
	}	
	
	public boolean getEmailTypeNotUsed()
	{
		return(emailtypeNotUsed);
	}	
	
	public void setEmailTypeNotUsed(boolean emailtypeNotUsed)
	{
		this.emailtypeNotUsed = emailtypeNotUsed;
	}	
	
	public String getBandwidthLabel()
	{
		return(bandwidthLabel);
	}	
	
	public void setBandwidthLabel(String bandwidthLabel)
	{
		this.bandwidthLabel = bandwidthLabel;
	}	
	
	public boolean getBandwidthNotUsed()
	{
		return(bandwidthNotUsed);
	}	
	
	public void setBandwidthNotUsed(boolean bandwidthNotUsed)
	{
		this.bandwidthNotUsed = bandwidthNotUsed;
	}	
	
	public String getContactLabel()
	{
		return(contactLabel);
	}	
	
	public void setContactLabel(String contactLabel)
	{
		this.contactLabel = contactLabel;
	}	
	
	public boolean getContactNotUsed()
	{
		return(contactNotUsed);
	}	
	
	public void setContactNotUsed(boolean contactNotUsed)
	{
		this.contactNotUsed = contactNotUsed;
	}	
	
	public String getKeepLabel()
	{
		return(keepLabel);
	}	
	
	public void setKeepLabel(String keepLabel)
	{
		this.keepLabel = keepLabel;
	}	
	
	public boolean getKeepNotUsed()
	{
		return(keepNotUsed);
	}	
	
	public void setKeepNotUsed(boolean keepNotUsed)
	{
		this.keepNotUsed = keepNotUsed;
	}	
	
	public String getContactStatusIDLabel()
	{
		return(contactstatusidLabel);
	}	
	
	public void setContactStatusIDLabel(String contactstatusidLabel)
	{
		this.contactstatusidLabel = contactstatusidLabel;
	}	
	
	public boolean getContactStatusIDNotUsed()
	{
		return(contactstatusidNotUsed);
	}	
	
	public void setContactStatusIDNotUsed(boolean contactstatusidNotUsed)
	{
		this.contactstatusidNotUsed = contactstatusidNotUsed;
	}	
	
	public String getGKTitleLabel()
	{
		return(gktitleLabel);
	}	
	
	public void setGKTitleLabel(String gktitleLabel)
	{
		this.gktitleLabel = gktitleLabel;
	}	
	
	public boolean getGKTitleNotUsed()
	{
		return(gktitleNotUsed);
	}	
	
	public void setGKTitleNotUsed(boolean gktitleNotUsed)
	{
		this.gktitleNotUsed = gktitleNotUsed;
	}	
	
	public String getGKFirstNameLabel()
	{
		return(gkfirstnameLabel);
	}	
	
	public void setGKFirstNameLabel(String gkfirstnameLabel)
	{
		this.gkfirstnameLabel = gkfirstnameLabel;
	}	
	
	public boolean getGKFirstNameNotUsed()
	{
		return(gkfirstnameNotUsed);
	}	
	
	public void setGKFirstNameNotUsed(boolean gkfirstnameNotUsed)
	{
		this.gkfirstnameNotUsed = gkfirstnameNotUsed;
	}	
	
	public String getGKLastNameLabel()
	{
		return(gklastnameLabel);
	}	
	
	public void setGKLastNameLabel(String gklastnameLabel)
	{
		this.gklastnameLabel = gklastnameLabel;
	}	
	
	public boolean getGKLastNameNotUsed()
	{
		return(gklastnameNotUsed);
	}	
	
	public void setGKLastNameNotUsed(boolean gklastnameNotUsed)
	{
		this.gklastnameNotUsed = gklastnameNotUsed;
	}	
	
	public String getQualifiedLabel()
	{
		return(qualifiedLabel);
	}	
	
	public void setQualifiedLabel(String qualifiedbyLabel)
	{
		this.qualifiedLabel = qualifiedbyLabel;
	}	
	
	public boolean getQualifiedNotUsed()
	{
		return(qualifiedNotUsed);
	}	
	
	public void setQualifiedNotUsed(boolean qualifiedbyNotUsed)
	{
		this.qualifiedNotUsed = qualifiedbyNotUsed;
	}	
	
	public String getNotesLabel()
	{
		return(notesLabel);
	}	
	
	public void setNotesLabel(String notesLabel)
	{
		this.notesLabel = notesLabel;
	}	
	
	public boolean getNotesNotUsed()
	{
		return(notesNotUsed);
	}	
	
	public void setNotesNotUsed(boolean notesNotUsed)
	{
		this.notesNotUsed = notesNotUsed;
	}	
	
	public String getToBeQualifiedLabel()
	{
		return(tobequalifiedLabel);
	}	
	
	public void setToBeQualifiedLabel(String tobequalifiedLabel)
	{
		this.tobequalifiedLabel = tobequalifiedLabel;
	}	
	
	public boolean getToBeQualifiedNotUsed()
	{
		return(tobequalifiedNotUsed);
	}	
	
	public void setToBeQualifiedNotUsed(boolean tobequalifiedNotUsed)
	{
		this.tobequalifiedNotUsed = tobequalifiedNotUsed;
	}	
	
	public String getEmail2Label()
	{
		return(email2Label);
	}	
	
	public void setEmail2Label(String email2Label)
	{
		this.email2Label = email2Label;
	}	
	
	public boolean getEmail2NotUsed()
	{
		return(email2NotUsed);
	}	
	
	public void setEmail2NotUsed(boolean email2NotUsed)
	{
		this.email2NotUsed = email2NotUsed;
	}	
	
	public String getEmailStatusLabel()
	{
		return(emailstatusLabel);
	}	
	
	public void setEmailStatusLabel(String emailstatusLabel)
	{
		this.emailstatusLabel = emailstatusLabel;
	}	
	
	public boolean getEmailStatusNotUsed()
	{
		return(emailstatusNotUsed);
	}	
	
	public void setEmailStatusNotUsed(boolean emailstatusNotUsed)
	{
		this.emailstatusNotUsed = emailstatusNotUsed;
	}	
	
	public String getGroupIDLabel()
	{
		return(groupidLabel);
	}	
	
	public void setGroupIDLabel(String groupidLabel)
	{
		this.groupidLabel = groupidLabel;
	}	
	
	public boolean getGroupIDNotUsed()
	{
		return(groupidNotUsed);
	}	
	
	public void setGroupIDNotUsed(boolean groupidNotUsed)
	{
		this.groupidNotUsed = groupidNotUsed;
	}	
	
	public String getTypeLabel()
	{
		return(typeLabel);
	}	
	
	public void setTypeLabel(String typeLabel)
	{
		this.typeLabel = typeLabel;
	}	
	
	public boolean getTypeNotUsed()
	{
		return(typeNotUsed);
	}	
	
	public void setTypeNotUsed(boolean typeNotUsed)
	{
		this.typeNotUsed = typeNotUsed;
	}	
	
	public String getSourceLabel()
	{
		return(sourceLabel);
	}	
	
	public void setSourceLabel(String sourceLabel)
	{
		this.sourceLabel = sourceLabel;
	}	
	
	public boolean getSourceNotUsed()
	{
		return(sourceNotUsed);
	}	
	
	public void setSourceNotUsed(boolean sourceNotUsed)
	{
		this.sourceNotUsed = sourceNotUsed;
	}	
	
	public String getAccountNoLabel()
	{
		return(accountnoLabel);
	}	
	
	public void setAccountNoLabel(String accountnoLabel)
	{
		this.accountnoLabel = accountnoLabel;
	}	
	
	public boolean getAccountNoNotUsed()
	{
		return(accountnoNotUsed);
	}	
	
	public void setAccountNoNotUsed(boolean accountnoNotUsed)
	{
		this.accountnoNotUsed = accountnoNotUsed;
	}	
	
	public String getIndustryLabel()
	{
		return(industryLabel);
	}	
	
	public void setIndustryLabel(String industryLabel)
	{
		this.industryLabel = industryLabel;
	}	
	
	public boolean getIndustryNotUsed()
	{
		return(industryNotUsed);
	}	
	
	public void setIndustryNotUsed(boolean industryNotUsed)
	{
		this.industryNotUsed = industryNotUsed;
	}	
	
	public String getUsernameLabel()
	{
		return(usernameLabel);
	}	
	
	public void setUsernameLabel(String usernameLabel)
	{
		this.usernameLabel = usernameLabel;
	}	
	
	public boolean getUsernameNotUsed()
	{
		return(usernameNotUsed);
	}	
	
	public void setUsernameNotUsed(boolean usernameNotUsed)
	{
		this.usernameNotUsed = usernameNotUsed;
	}	
	
	public String getPasswordLabel()
	{
		return(passwordLabel);
	}	
	
	public void setPasswordLabel(String passwordLabel)
	{
		this.passwordLabel = passwordLabel;
	}	
	
	public boolean getPasswordNotUsed()
	{
		return(passwordNotUsed);
	}	
	
	public void setPasswordNotUsed(boolean passwordNotUsed)
	{
		this.passwordNotUsed = passwordNotUsed;
	}	

	private GroupDisplay groupdisplay = null;
	
	public GroupDisplay getGroupDisplay()
	{
		return(groupdisplay);
	}
	
	public void setGroupDisplay(GroupDisplay groupdisplay)
	{
		this.groupdisplay = groupdisplay;
	}
}
