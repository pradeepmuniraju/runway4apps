package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.runway.externalInterface.beans.shared.AbstractStatusHistory;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="QuickIndexStatus")
@XmlType(propOrder={"productStatusID","productID"})
public class QuickIndexStatusHistory extends AbstractStatusHistory {
	private String productStatusID, productID;

	@XmlElement(name="ProductStatusID")
	public String getProductStatusID() {
		return productStatusID;
	}
	public void setProductStatusID(String productStatusID) {
		this.productStatusID = productStatusID;
	}
	
	@XmlElement(name="ProductID")
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
}
