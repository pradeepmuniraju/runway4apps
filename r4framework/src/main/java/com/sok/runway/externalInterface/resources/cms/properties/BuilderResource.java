package com.sok.runway.externalInterface.resources.cms.properties;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.beans.cms.properties.Builder;
import com.sok.service.crm.cms.properties.BuilderService;

@Path("/builders")
@Produces({ MediaType.APPLICATION_JSON})
public class BuilderResource {

	private static final Logger logger = LoggerFactory.getLogger(BuilderResource.class);
	private static final BuilderService builderService = BuilderService.getInstance();
	
	private static final BuilderResource resource = new BuilderResource();
	public static BuilderResource getInstance() { return resource; }
	
	@GET
	public List<Builder> getBuilders(@Context HttpServletRequest request) {
		return builderService.getBuilders(request); 
	}
	
	@GET
	@Path("{builderID}")
	public Builder getBuilder(@Context HttpServletRequest request, @PathParam("builderID") final String builderID) {
		return builderService.getBuilder(request, builderID);
	}
	
	/*
	@Path("{builderID}/lots")
	public LotResource getLotResource(@Context HttpServletRequest request, @PathParam("builderID") final String builderID) {
		return new LotResource(request, builderID);
	}
	*/
}