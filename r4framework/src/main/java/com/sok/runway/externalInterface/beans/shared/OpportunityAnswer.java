package com.sok.runway.externalInterface.beans.shared;

import org.codehaus.jackson.annotate.JsonProperty;
/**
 * 
 * @author mike
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class OpportunityAnswer extends Answer
{
	private String opportunityID = null;
	
	@JsonProperty("OpportunityID")
	public String getOpportunityID()
	{
		return(opportunityID);
	}	
	
	@JsonProperty("OpportunityID")
	public void setOpportunityID(String opportunityID)
	{
		this.opportunityID = opportunityID;
	}		
}