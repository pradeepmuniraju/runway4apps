package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.NONE)
@XmlSeeAlso(SearchKey.class)
@XmlRootElement(name="SearchKey")
@XmlType(propOrder={"searchKey","min","max"})		//MarketingStatus AvailableDate
public class SearchKey {

	protected String searchKey;
	protected double min = 0, max = 0;
	
	public SearchKey() {}
	
	public SearchKey(String searchKey, double min, double max) {
		this.searchKey = searchKey;
		this.max = max;
		this.min = min;
	}
	
	@XmlElement(name="Key")
	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	@XmlElement(name="Minimum")
	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	@XmlElement(name="Maximum")
	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}
}
