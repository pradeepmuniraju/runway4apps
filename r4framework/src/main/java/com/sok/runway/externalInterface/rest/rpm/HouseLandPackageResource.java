package com.sok.runway.externalInterface.rest.rpm;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.Album;
import com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage;
import com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackageCheck;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPackages;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPlans;
import com.sok.runway.externalInterface.beans.cms.properties.SearchKey;
import com.sok.runway.security.SecurityFactory;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.DripService;
import com.sok.service.crm.cms.properties.DripService.DripCollection;
import com.sok.service.crm.cms.properties.ImageService;
//import com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackageSearch;
import com.sok.service.crm.cms.properties.PackageService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

@Path("/packages")
@Api(value = "/packages", description = "Retrieve Package Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class HouseLandPackageResource {
	
	private static final PackageService packageService = PackageService.getInstance();
	static final Logger logger = LoggerFactory.getLogger(HouseLandPackageResource.class);
	@Context HttpServletRequest request;
	
	@GET
	@ApiOperation(value = "List packages", notes="<p>Optionally filtering by modified date, this test will only do the first 20 packages. This API will return all packages when not used via api-docs.</p><p>In production it is recommended that the package feed be started after 4am. This allows for the DRIP calculation process which starts after midnight, otherwise prices maybe incorrect.</p><p>You should also set active to false in your request so you get all packages that have been modified and then remove those from your database that Active is not set to 'Y' and CurrentStatus is not set to 'Available'.<p><p>Any deleted packages will not show in this feed, please use <a onclick='Docs.toggleEndpointListForResource(\'activites\');' href='#!/activites/getActivities_get_0'>/activties</a> to list deleted packages</p>", responseClass="com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "HouseLandPackages")
	public Response getPackages(
			@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = true) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active packages", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try {  
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);		 
				} catch (ParseException pe) {	
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		/** Search option 2. Downside to this is the limit of the GET request.
		 * this is going to be used by the henley developer portal, i'm not 100% if this is how we want to do api-search or 
		 * not. Consider it a hack if you like ;) */
		GenRow search = new GenRow(); 
		
		if(request.getHeader("Referer") != null && request.getHeader("Referer").indexOf("api-docs") != -1) {
			search.setParameter("-top", 20);
		}
		
		search.parseRequest(request, "_");
		if(mod != null) { 
			search.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(mod));
		}
		if(active) {
			search.setParameter("Active","Y");
		}
		
		/* semi-hacky content-negotiation */
		String accept = StringUtils.trimToEmpty(request.getHeader("Accept"));
		int xml = accept.indexOf("text/xml"), json = accept.indexOf("application/json"); 
		MediaType mt = xml != -1 && (xml < json || json == -1) ? MediaType.TEXT_XML_TYPE : MediaType.APPLICATION_JSON_TYPE;
		
		if(!"true".equals(request.getParameter("-paged"))) {
			/*
			Response.ok(new StreamingOutput() {
				public void write(OutputStream output) throws IOException, WebApplicationException {
					// * TODO streamed package output  
					// * Maybe only do for JSON, as it's easier 
					org.codehaus.jackson.JsonFactory jf = new org.codehaus.jackson.JsonFactory();
					org.codehaus.jackson.JsonGenerator jg = jf.createJsonGenerator(output);
				}
			}).build();
			*/
			return Response.ok(mt == MediaType.TEXT_XML_TYPE ? new PackageList(packageService.getPackagesExternal(request, search)) : packageService.getPackagesExternal(request, search)).build();
		} else {
			
			// take the search params, make up a unique search identifier based on those params. 
			
			/* 
			 * Price EstateID StageID LotID HomeID Frontage Size 
			 */
			String pagingKey = StringUtils.defaultIfEmpty(request.getParameter("-pagingKey"), "-lastHLAPI2Search"); 
			int perPage = new Integer(StringUtils.defaultIfEmpty(request.getParameter("-perPage"), "50"));
			int page = new Integer(StringUtils.defaultIfEmpty(request.getParameter("-page"), "0"));
			
			logger.debug("using pagingKey={}, perPage={}, page={}", new String[]{pagingKey, String.valueOf(perPage), String.valueOf(page) });
			
			List<String> keys = new ArrayList<String>(search.keySet());
			Collections.sort(keys);
			StringBuilder params = new StringBuilder(); 
			boolean first = true;
			for(String key: keys) {
				if(first) first = false; else params.append("&"); 
				params.append(key).append("=").append(String.valueOf(search.get(key))); 
			}
			
			logger.debug(search.toString());
			logger.debug(params.toString());
			
			LastSearch ls = (LastSearch)request.getSession().getAttribute(pagingKey);
			String hash = SecurityFactory.hashPassword(params.toString());
			
			logger.debug("New hash: [{}] - old hash [{}]", hash, ls != null ? ls.hash : "ls was null"); 
			
			if(ls == null || !hash.equals(ls.hash)) {
				logger.debug("doing new search"); 
				ls = new LastSearch(packageService.getPackagesExternal(request, search), hash);
				request.getSession().setAttribute(pagingKey, ls);
			}
			
			int startCount = page * perPage;
			
			if(startCount > ls.total) {
				throw new IllegalArgumentException("Page requested is greater than the amount of available pages");
			}
			
			int resultSize = startCount + perPage > ls.total ? ls.total - startCount : perPage;
			int lastIndex = startCount + resultSize - 1; 
			
			List<HouseLandPackage> results = Collections.emptyList();
			if(resultSize != 0) { 
				results = new ArrayList<HouseLandPackage>(resultSize); 
				for(int i=startCount;;i++) {
					results.add(ls.list.get(i));
					if(i == lastIndex) {
						break;
					}
				}
			}
			return Response.ok(new PagedSearchResult(results, ls.total, page)).build();
		}
	}
	
	@GET
	@ApiOperation(value = "List package IDs", notes="<p>Optionally filtering by modified date, this test will only do the first 20 packages. This API will return all packages when not used via api-docs.</p><p>In production it is recommended that the package feed be started after 4am. This allows for the DRIP calculation process which starts after midnight, otherwise prices maybe incorrect.</p><p>You should also set active to false in your request so you get all packages that have been modified and then remove those from your database that Active is not set to 'Y' and CurrentStatus is not set to 'Available'.<p><p>Any deleted packages will not show in this feed, please use <a onclick='Docs.toggleEndpointListForResource(\'activites\');' href='#!/activites/getActivities_get_0'>/activties</a> to list deleted packages</p>", responseClass="com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage", multiValueResponse=true)
	@Path("/check")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "HouseLandPackages")
	public Response getPackagesCheck(
			@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = true) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active packages", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try {  
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);		 
				} catch (ParseException pe) {	
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		/** Search option 2. Downside to this is the limit of the GET request.
		 * this is going to be used by the henley developer portal, i'm not 100% if this is how we want to do api-search or 
		 * not. Consider it a hack if you like ;) */
		GenRow search = new GenRow(); 
		
		if(request.getHeader("Referer") != null && request.getHeader("Referer").indexOf("api-docs") != -1) {
			search.setParameter("-top", 20);
		}
		
		search.parseRequest(request, "_");
		if(mod != null) { 
			search.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(mod));
		}
		if(active) {
			search.setParameter("Active","Y");
		}
		
		/* semi-hacky content-negotiation */
		String accept = StringUtils.trimToEmpty(request.getHeader("Accept"));
		int xml = accept.indexOf("text/xml"), json = accept.indexOf("application/json"); 
		MediaType mt = xml != -1 && (xml < json || json == -1) ? MediaType.TEXT_XML_TYPE : MediaType.APPLICATION_JSON_TYPE;
		
		if(!"true".equals(request.getParameter("-paged"))) {
			/*
			Response.ok(new StreamingOutput() {
				public void write(OutputStream output) throws IOException, WebApplicationException {
					// * TODO streamed package output  
					// * Maybe only do for JSON, as it's easier 
					org.codehaus.jackson.JsonFactory jf = new org.codehaus.jackson.JsonFactory();
					org.codehaus.jackson.JsonGenerator jg = jf.createJsonGenerator(output);
				}
			}).build();
			*/
			return Response.ok(mt == MediaType.TEXT_XML_TYPE ? new PackageListCheck(packageService.getPackagesExternalCheck(request, search)) : packageService.getPackagesExternalCheck(request, search)).build();
		} else {
			
			// take the search params, make up a unique search identifier based on those params. 
			
			/* 
			 * Price EstateID StageID LotID HomeID Frontage Size 
			 */
			String pagingKey = StringUtils.defaultIfEmpty(request.getParameter("-pagingKey"), "-lastHLAPI2Search"); 
			int perPage = new Integer(StringUtils.defaultIfEmpty(request.getParameter("-perPage"), "50"));
			int page = new Integer(StringUtils.defaultIfEmpty(request.getParameter("-page"), "0"));
			
			logger.debug("using pagingKey={}, perPage={}, page={}", new String[]{pagingKey, String.valueOf(perPage), String.valueOf(page) });
			
			List<String> keys = new ArrayList<String>(search.keySet());
			Collections.sort(keys);
			StringBuilder params = new StringBuilder(); 
			boolean first = true;
			for(String key: keys) {
				if(first) first = false; else params.append("&"); 
				params.append(key).append("=").append(String.valueOf(search.get(key))); 
			}
			
			logger.debug(search.toString());
			logger.debug(params.toString());
			
			LastSearch ls = (LastSearch)request.getSession().getAttribute(pagingKey);
			String hash = SecurityFactory.hashPassword(params.toString());
			
			logger.debug("New hash: [{}] - old hash [{}]", hash, ls != null ? ls.hash : "ls was null"); 
			
			if(ls == null || !hash.equals(ls.hash)) {
				logger.debug("doing new search"); 
				ls = new LastSearch(packageService.getPackagesExternal(request, search), hash);
				request.getSession().setAttribute(pagingKey, ls);
			}
			
			int startCount = page * perPage;
			
			if(startCount > ls.total) {
				throw new IllegalArgumentException("Page requested is greater than the amount of available pages");
			}
			
			int resultSize = startCount + perPage > ls.total ? ls.total - startCount : perPage;
			int lastIndex = startCount + resultSize - 1; 
			
			List<HouseLandPackage> results = Collections.emptyList();
			if(resultSize != 0) { 
				results = new ArrayList<HouseLandPackage>(resultSize); 
				for(int i=startCount;;i++) {
					results.add(ls.list.get(i));
					if(i == lastIndex) {
						break;
					}
				}
			}
			return Response.ok(new PagedSearchResult(results, ls.total, page)).build();
		}
	}
	
	@XmlRootElement(name="Search")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class PagedSearchResult {
		public final List<HouseLandPackage> results; 
		public final int count;
		public final int page;  
		
		public PagedSearchResult(List<HouseLandPackage> results, int count, int page) {
			this.results = results; 
			this.count = count; 
			this.page = page; 
		}
	}
	
	@XmlAccessorType(XmlAccessType.PROPERTY)
	@XmlRootElement(name = "Packages")
	public static class PackageList {
		
		private final List<HouseLandPackage> list; 
		public PackageList() {
			list = Collections.emptyList(); 
		}
		public PackageList(List<HouseLandPackage> list) {
			this.list = list;
		}
		
		@XmlElement(name="Package")
		public List<HouseLandPackage> getPackages() {
			return list;
		}
	}
	
	@XmlAccessorType(XmlAccessType.PROPERTY)
	@XmlRootElement(name = "Packages")
	public static class PackageListCheck {
		
		private final List<HouseLandPackageCheck> list; 
		public PackageListCheck() {
			list = Collections.emptyList(); 
		}
		public PackageListCheck(List<HouseLandPackageCheck> list) {
			this.list = list;
		}
		
		@XmlElement(name="Package")
		public List<HouseLandPackageCheck> getPackages() {
			return list;
		}
	}
	
	private class LastSearch {
		private final List<HouseLandPackage> list; 
		private final String hash; 
		private final int total; 
		
		public LastSearch(List<HouseLandPackage> list, String hash) {
			this.list = list; 
			this.hash = hash; 
			this.total = list.size();
		}
	}
	
	///**
	// * Search Option 1, unknown format at this stage.
	// */
	//@POST
	//@Path("/search")
	//@ApiOperation(value="Create a package search", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackageSearch")
	//@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	//public Response getPackageSearch(Object search) throws java.net.URISyntaxException {
	//	String searchID = null;
	//	return Response.status(Status.TEMPORARY_REDIRECT).location(new URI(request.getContextPath()  + "/api/2/packages/?SearchID=" + searchID)).build();
	//}
	
	@ApiResource(value = "/publishing", resourceClass="com.sok.runway.externalInterface.rest.rpm.PublishingListResource")
	@Path("publishing")
	public PublishingListResource getPublishingResource() {
		return new PublishingListResource(request, PropertyType.HouseLandPackage);
	}
	
	@GET
	@Path("/{packageID}")
	@ApiOperation(value = "Return a particular package", notes="<p>In production it is recommended that the package feed be started after 4am. This allows for the DRIP calculation process which starts after midnight, otherwise prices may be incorrect.</p><p>You should remove those packages from your database that Active is not set to 'Y' and CurrentStatus is not set to 'Available'.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public HouseLandPackage getPackage(@Context HttpServletRequest request, 
			@PathParam("packageID") final String packageID,
			@ApiParam(value = "Retrieve extended package information", allowableValues="Simple,Extended", required=false, defaultValue="Simple") @DefaultValue("Simple") @QueryParam("View") BeanRepresentation extended
			) {
		return packageService.getPackageExternal(request, packageID, extended);
	}
	
	@GET
	@ApiResource(value = "/{packageID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Home Design", notes="<p>This call will return a list of Albums and their Images for a given Home Design. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/{packageID}/albums")
	public AlbumResource getPackageAlbumResource(@PathParam("packageID") final String packageID) {
		return new AlbumResource(request,  packageID);
	}

	@GET
	@ApiResource(value = "/{packageID}/drips", resourceClass="com.sok.service.crm.cms.properties.DripService.DripCollectionExternal")
	@ApiOperation(value = "Return a list of DRIPs from the package", notes="<p>This DRIP service has 2 parts to it, the first is IncludedDripList which includes all the default DRIPs, the OptionalDripList are DRIPs not on by default</p>"
	+ "<p>The DRIPs are listed as part of Product under Other, each DRIP has values and DripContent. The DripContent is what you need to populate your website</p>"
	+ "<p>DripContent consists of ContentType (Text or Image), ContentSubType (PDF, Email, Website, Quotes or Publishing), Placement, Template and ImagePath or Description</p>"
	+ "<p>It is best to use the Website or PDF content for you needs</p>", responseClass = "com.sok.service.crm.cms.properties.DripService.DripCollectionExternal")
	@Path("/{packageID}/drips")
	public DripCollectionExternal getPackageDripsResource(@PathParam("packageID") final String packageID) {
		DripService ds = new DripService();
		return new DripCollectionExternal(ds.getAllDripsInfo(request, new String[] {packageID,  packageID, packageID}));
		//return new AlbumResource(request,  packageID);
	}

	@GET
	@ApiResource(value = "/{packageID}/publishing", resourceClass="com.sok.runway.externalInterface.rest.rpm.PublishingResource")
	@Path("/{packageID}/publishing")
	public PublishingResource getPackagePublishingResource(@PathParam("packageID") final String packageID,@ApiParam(value = "A publish method", allowableValues="Website", required = true) @QueryParam("PublishMethod") String publishMethod) {
		return new PublishingResource(request, PropertyType.HouseLandPackage, packageID, publishMethod);
	}
	
}
