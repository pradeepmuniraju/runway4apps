/**
 * 
 */
package com.sok.runway.externalInterface.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.sok.framework.GenRow;

/**
 * @author Dion Chapman
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PropertyOptionExtended extends PropertyOption {

	public boolean HasExtraText = false;
	public boolean CostLocked = false;
	public boolean QuantityLocked = false;
	/**
	 * 
	 */
	public PropertyOptionExtended() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param product
	 */
	public PropertyOptionExtended(GenRow product) {
		super(product);
		HasExtraText = "Y".equals(product.getString("DisabledForHouse"));
		CostLocked = !"Y".equals(product.getString("DisabledForLot"));
		QuantityLocked = "N".equals(product.getString("DisabledForHousePayments"));
	}

	/**
	 * @param product
	 * @param isCustom
	 */
	public PropertyOptionExtended(GenRow product, boolean isCustom) {
		super(product, isCustom);
		HasExtraText = "Y".equals(product.getString("DisabledForHouse"));
		CostLocked = !"Y".equals(product.getString("DisabledForLot"));
		QuantityLocked = "N".equals(product.getString("DisabledForHousePayments"));
	}

}
