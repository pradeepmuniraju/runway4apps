package com.sok.runway.externalInterface.beans.shared;

import java.util.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class ProductGroup
{
   private String productgroupid = null;
   private String groupname = null;
   private String description = null;
   private String sortorder = null;
   private Date createddate = null;
   private Date modifieddate = null;
   private String createdby = null;
   private String modifiedby = null;
   private String groupid = null;
   private String active = null;
   
   public String getProductGroupID()
   {
      return(productgroupid);
   }  
   
   public void setProductGroupID(String productgroupid)
   {
      this.productgroupid = productgroupid;
   }  
   
   public String getGroupName()
   {
      return(groupname);
   }  
   
   public void setGroupName(String groupname)
   {
      this.groupname = groupname;
   }  
   
   public String getDescription()
   {
      return(description);
   }  
   
   public void setDescription(String description)
   {
      this.description = description;
   }  
   
   public String getSortOrder()
   {
      return(sortorder);
   }  
   
   public void setSortOrder(String sortorder)
   {
      this.sortorder = sortorder;
   }  
   
   public Date getCreatedDate()
   {
      return(createddate);
   }  
   
   public void setCreatedDate(Date createddate)
   {
      this.createddate = createddate;
   }  
   
   public Date getModifiedDate()
   {
      return(modifieddate);
   }  
   
   public void setModifiedDate(Date modifieddate)
   {
      this.modifieddate = modifieddate;
   }  
   
   public String getCreatedBy()
   {
      return(createdby);
   }  
   
   public void setCreatedBy(String createdby)
   {
      this.createdby = createdby;
   }  
   
   public String getModifiedBy()
   {
      return(modifiedby);
   }  
   
   public void setModifiedBy(String modifiedby)
   {
      this.modifiedby = modifiedby;
   }  
   
   public String getGroupID()
   {
      return(groupid);
   }  
   
   public void setGroupID(String groupid)
   {
      this.groupid = groupid;
   }  
   
   public String getActive()
   {
      return(active);
   }  
   
   public void setActive(String active)
   {
      this.active = active;
   }  

}
