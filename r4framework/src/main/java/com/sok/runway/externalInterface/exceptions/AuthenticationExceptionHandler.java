package com.sok.runway.externalInterface.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.sok.runway.externalInterface.DataApplication;
import com.sok.service.exception.AccessException;

@Provider
public class AuthenticationExceptionHandler implements ExceptionMapper<AccessException> {

	public Response toResponse(AccessException exception){
	    return Response.status(Response.Status.FORBIDDEN)
	    .entity(new DataApplication.DataApplicationException(exception, Response.Status.FORBIDDEN.getStatusCode()))
	    .build();
	}
}
