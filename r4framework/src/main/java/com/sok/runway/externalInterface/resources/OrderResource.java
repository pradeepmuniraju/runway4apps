package com.sok.runway.externalInterface.resources;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;


import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.beans.Order;
import com.sok.runway.externalInterface.beans.shared.OrderContact;
import com.sok.runway.externalInterface.beans.shared.OrderItem;
import com.sok.runway.externalInterface.beans.shared.OrderProfile;
import com.sok.runway.externalInterface.beans.shared.Profile;
import com.sok.runway.externalInterface.resources.UtilityResource.ContactLocation;
import com.sok.service.crm.OrderService;
import com.sok.service.crm.ProfileService;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.DripService;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;

import javax.servlet.http.HttpServletRequest; 

@Path("/orders")
public class OrderResource {
	private static final OrderResource resource = new OrderResource();
	private static final Logger logger = LoggerFactory.getLogger(OrderResource.class);
	private static final OrderService os = OrderService.getInstance();
	private static final DripService dripService = DripService.getInstance();
	private static final ProfileService ps = ProfileService.getInstance();
	private static final UserService us = UserService.getInstance();
	public static OrderResource getInstance() {
		return resource;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Order> getOrders(@Context HttpServletRequest request, @QueryParam("search") String search) {
		logger.trace("getOrders(Request, {})", search);
		return os.getOrders(request);
	}
	
	@GET
	@Path("{orderID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Order getOrder(@Context HttpServletRequest request, @PathParam("orderID") String orderID) {
		logger.trace("getOrder(Request, {})", orderID);
		return os.getOrder(request, orderID);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Order saveOrder(@Context HttpServletRequest request, Order order) {
		logger.trace("saveOrder(Request, {}", order);
		return os.saveOrder(request, order);
	}
	
	@PUT
	@Path("{orderID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Order updateOrder(@Context HttpServletRequest request, @PathParam("orderID") String orderID, Order order) {
		logger.trace("updateOrder(Request, {}, {})", orderID, order);
		if(orderID == null || !orderID.equals(order.getOrderID())) {
			throw new IllegalArgumentException("Path/Object OrderID's did not match");
		}
		return os.updateOrder(request, order);
	}
	
	@GET
	@Path("{orderID}/pdf")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, String> getOrderPDF(@Context HttpServletRequest request, @PathParam("orderID") String orderID, @QueryParam("QuoteViewPageID") String QuoteViewPageID) {
		StringBuilder url = new StringBuilder(100); 
		url.append("/order.pdf?OrderID=").append(orderID);
		if(StringUtils.isNotBlank(QuoteViewPageID)) {
			url.append("&QuoteViewPageID=").append(QuoteViewPageID);
		}
		UserBean user = us.getCurrentUser(request);
		
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		
		
		Map<String,String> resp = new HashMap<String, String>();
		String fileKey = KeyMaker.generate();
		try { 
			java.io.File f = doFileOutput(request,url.toString(), fileKey);
			
			com.sok.runway.crm.Order order = new com.sok.runway.crm.Order(request);
			order.setCurrentUser(user.getUser());
			order.load(orderID);
			
			com.sok.runway.crm.Document doc = new com.sok.runway.crm.Document(request);
			doc.setCurrentUser(user.getUser());
			doc.setPrimaryKey(fileKey);
			doc.setField("FileName", f.getName());
			doc.setField("GroupID", order.getField("GroupID"));
			doc.setField("FilePath", f.getPath());
			doc.setField("FileSize", f.length());
			doc.setField("DocumentType", "PDF");
			doc.insert();

			order.linkDocument(fileKey);
			os.checkDocuments(order, user);
			
			resp.put("status","success");
			resp.put("fileKey", fileKey);
			return resp;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private java.io.File doFileOutput(HttpServletRequest request, String urlPath, String fileKey) throws Exception {
	    com.sok.framework.servlet.ResponseOutputStream responseOS = null;
	    java.io.FileOutputStream fos = null;
	    try {
	    	
	    	
	    	java.io.File dir = new java.io.File(InitServlet.getRealPath("/files/orderpdf/"));
	    	dir.mkdirs();
	    	
	    	java.io.File file = new java.io.File(InitServlet.getRealPath("/files/orderpdf/" + fileKey + ".pdf"));
	    	
	    	logger.trace("File {} {}", file.getAbsoluteFile(), file.exists());
	       fos = new java.io.FileOutputStream(file);
	       responseOS = new com.sok.framework.servlet.ResponseOutputStream(request.getSession(), fos, urlPath);
	       responseOS.service(ActionBean.getConnection(request));
	       request.getSession().setAttribute(fileKey, file);
	       
	       return file;
	    } finally {
	       if (fos != null) {
	          try {
	             fos.flush();
	             fos.close();
	          } catch (Exception ex) {
	             ex.printStackTrace();
	          }
	       }
	       if (responseOS != null) {
	          responseOS.close();
	       }
	    }
	 }
	
	@GET
	@Path("{orderID}/profiles")
	@Produces(MediaType.APPLICATION_JSON)
	public List<OrderProfile> getOrderProfile(@Context HttpServletRequest request, @PathParam("orderID") String orderID) {
		logger.trace("getOrderProfile(Request,{})", orderID);
		// TODO - fix the types here to use generics etc. 
		final List<Profile> pl = ps.getProfiles(request, ProfileService.ProfileType.ORDER, orderID);
		final List<OrderProfile> cpl = new ArrayList<OrderProfile>(pl.size());
		for(Profile p: pl) {
			cpl.add((OrderProfile)p);
		}
		return cpl;
	}
	
	@PUT
	@Path("{orderID}/profiles/{surveyID}")
	@Produces(MediaType.APPLICATION_JSON)
	public OrderProfile updateOrderProfile(@Context HttpServletRequest request, @PathParam("orderID") String orderID, @PathParam("surveyID") String surveyID, OrderProfile orderProfile) {
		logger.trace("getOrderProfile(Request,{},{})", orderID, surveyID);
		if(StringUtils.isBlank(orderID)) {
			throw new IllegalArgumentException("OrderID blank");
		}
		if(StringUtils.isBlank(surveyID)) {
			throw new IllegalArgumentException("SurveyID blank");
		}		
		if(!surveyID.equals(orderProfile.getSurveyID())) {
			throw new IllegalArgumentException("SurveyID did not match survey id in object");
		}
		
		OrderProfile op = ps.saveOrderProfile(request, orderID, orderProfile);
		
		UserBean user = us.getCurrentUser(request);
		Connection conn = ActionBean.getConnection(request);
		Order order = os.getOrder(conn, user, orderID);
		dripService.getAllDrips(conn, user, new String[]{ order.getProductID(), order.getProductID(), order.getProductID(), orderID, order.getQuoteViewID()}, null);
		
		return op;
	}
	
	@POST
	@Path("{orderID}/duplicate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Order duplicateOrder(@Context HttpServletRequest request, @PathParam("orderID") String orderID) {
		logger.trace("duplicateOrder(Request, {}, {})", orderID);
		if(orderID == null) {
			throw new IllegalArgumentException("OrderID must be supplied for duplicate action");
		}
		return os.duplicateOrder(request, orderID);
	}
	
	@DELETE 
	@Path("{orderID}")
	public void deleteOrder(@Context HttpServletRequest request, @PathParam("orderID") String orderID) {
		logger.trace("deleteOrder(Request, {})", orderID);
		os.deleteOrder(request, orderID);
	}
	
	@GET
	@Path("{orderID}/locations")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ContactLocation> getOrderLocations(@Context HttpServletRequest request, @PathParam("orderID") String orderID) {
		logger.trace("getOrderLocations(Request, {})", orderID);
		
		List<ContactLocation> lcl = UtilityResource.getInstance().getOrderLocations(request, orderID); 
		if(lcl.isEmpty()) {
			
			GenRow loc = new GenRow();
			loc.setRequest(request);
			loc.setViewSpec("ContactLocationView");
			loc.setToNewID("ContactLocationID");
			loc.setParameter("OrderID", orderID);
			loc.setParameter("Name", "Temporary Location");
			loc.setParameter("CreatedDate","NOW");
			try { 
				loc.setParameter("CreatedBy", UserService.getInstance().getCurrentUser(request).getUserID());
			} catch (NullPointerException npe) { } 
			loc.doAction("insert");
			
			if(!loc.isSuccessful()) {
				throw new RuntimeException(loc.getError());
			}
			
			lcl = new java.util.ArrayList<ContactLocation>(1);
			lcl.add(new ContactLocation(loc));
			return lcl;
		}
		return lcl; 
	}
	
	@PUT
	@Path("{orderID}/locations/{contactLocationID}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContactLocation updateOrderLocation(@Context HttpServletRequest request, @PathParam("orderID") String orderID,  @PathParam("contactLocationID") String contactLocationID, ContactLocation cl) {
		logger.debug("ContactLocation(Request, {},{})", orderID, contactLocationID);
	
		GenRow loc = new GenRow();
		loc.setRequest(request);
		loc.setViewSpec("ContactLocationView");
		loc.setParameter("ContactLocationID", contactLocationID);
		loc.doAction("select");
		
		if(!loc.isSuccessful()) {
			throw new NotFoundException("ContactLocationID not found " + contactLocationID);
		}
		if(!orderID.equals(loc.get("OrderID"))) {
			throw new IllegalArgumentException("Loc did not match contact loc");
		}
		if(cl == null) {
			throw new IllegalArgumentException("no contact location in body");
		}
		GenRow address = new GenRow();
		address.setRequest(request);
		address.setTableSpec("Addresses");
		if(StringUtils.isNotBlank(cl.AddressID)) {
			address.setParameter("AddressID",cl.AddressID);
			address.setAction("update");
		} else {
			address.setToNewID("AddressID");
			address.setAction("insert");
			cl.AddressID = address.getString("AddressID");
		}
		address.setParameter("StreetNumber", cl.StreetNumber);
		address.setParameter("Street", cl.Street);
		address.setParameter("Street2", cl.Street2);
		address.setParameter("City", cl.City);
		address.setParameter("State", cl.State);
		address.setParameter("Postcode", cl.Postcode);
		address.setParameter("Country", cl.Country);
		address.doAction();
		if(logger.isTraceEnabled()) { 
			logger.trace(address.toString());
			logger.trace(address.getError());
		} 
		if(!address.isSuccessful()) {
			throw new RuntimeException(address.getError());
		}
		
		loc.setParameter("BuildAddressID", cl.AddressID);
		
		if(StringUtils.isNotBlank(cl.LocationID)) {
			GenRow reg = new GenRow();
			reg.setRequest(request);
			reg.setViewSpec("DisplayEntityView");
			reg.setParameter("DisplayEntityID", cl.LocationID);
			reg.doAction("select");
			if(reg.isSuccessful()) {
				cl.RegionID = reg.getData("ParentDisplayEntityID");
			}
		} else if (StringUtils.isNotBlank(cl.ProductLotID)) {
			
			com.sok.runway.crm.cms.properties.Land land = new com.sok.runway.crm.cms.properties.Land(request, cl.ProductLotID);
			String landLocationID = land.getLocationID();
			if(StringUtils.isNotBlank(landLocationID)) {
				cl.LocationID = land.getString("LocationID");
				GenRow reg = new GenRow();
				reg.setRequest(request);
				reg.setViewSpec("DisplayEntityView");
				reg.setParameter("DisplayEntityID", landLocationID);
				reg.doAction("select");
				if(reg.isSuccessful()) {
					cl.RegionID = reg.getData("ParentDisplayEntityID");
				}
			} 
		}
		loc.setParameter("ProductID", cl.ProductID);
		loc.setParameter("RegionID", cl.RegionID);
		loc.setParameter("LocationID", cl.LocationID);
		loc.doAction("update");
		if(!loc.isSuccessful()) {
			throw new RuntimeException(loc.getError());
		}
		loc.doAction("select");
		if(!loc.isSuccessful()) {
			throw new RuntimeException(loc.getError());
		}
		return new ContactLocation(loc);
	}
	
	@GET
	@Path("{orderID}/items")
	@Produces(MediaType.APPLICATION_JSON)
	public List<OrderItem> getOrderItems(@Context HttpServletRequest request, @PathParam("orderID") String orderID) {
		logger.trace("getOrderItems(Request, {})", orderID);
		return os.getOrderItems(request, orderID);
	}
	
	@GET
	@Path("{orderID}/items/{orderItemID}")
	@Produces(MediaType.APPLICATION_JSON)
	public OrderItem getOrderItem(@Context HttpServletRequest request, @PathParam("orderID") String orderID, @PathParam("orderItemID") String orderItemID) {
		logger.trace("getOrderItem(Request, {}, {})", orderID, orderItemID);
		return os.getOrderItem(request, orderID, orderItemID);
	}
	
	@POST
	@Path("{orderID}/items")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public OrderItem createOrderItem(@Context HttpServletRequest request, @PathParam("orderID") String orderID, OrderItem orderItem) {
		logger.trace("createOrderItem(Request, {}, {})", orderItem);
		if(orderID == null || !orderID.equals(orderItem.getOrderID())) {
			throw new IllegalArgumentException("Path/Object OrderID's did not match");
		}
		return os.saveOrderItem(request, orderID, orderItem);
	}
	
	@PUT
	@Path("{orderID}/items/{orderItemID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public OrderItem updateOrderItem(@Context HttpServletRequest request, @PathParam("orderID") String orderID, @PathParam("orderItemID") String orderItemID, OrderItem orderItem) {
		//we check is traceEnabled here as we are performing a calculation within the logger call. If trace is not enabled we do not wish this to be done. 
		if(logger.isTraceEnabled()) {
			logger.trace("updateOrderItem(Request, {}, {}, {})", new String[]{orderID, orderItemID, orderItem.toString()});
		}
		return os.updateOrderItem(request, orderID, orderItemID, orderItem);
	}
	
	@DELETE
	@Path("{orderID}/items/{orderItemID}")
	public void deleteOrderItem(@Context HttpServletRequest request, @PathParam("orderID") String orderID, @PathParam("orderItemID") String orderItemID) {
		logger.trace("deleteOrderItem(Request, {}, {})", orderID, orderItemID);
		os.deleteOrderItem(request, orderID, orderItemID);
	}
	
	@GET
	@Path("{orderID}/contacts")
	@Produces(MediaType.APPLICATION_JSON)
	public List<OrderContact> getOrderContacts(@Context HttpServletRequest request, @PathParam("orderID") String orderID) {
		logger.trace("getOrderContacts(Request, {})", orderID);
		return os.getOrderContacts(request, orderID);
	}
	
	@POST
	@Path("{orderID}/contacts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public OrderContact createOrderContact(@Context HttpServletRequest request, @PathParam("orderID") String orderID, OrderContact orderContact) {
		logger.trace("saveOrderContact(Request, {})", orderID);
		return os.createOrderContact(request, orderID, orderContact);
	}
	
	@PUT
	@Path("{orderID}/contacts/{orderContactID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public OrderContact updateOrderContact(@Context HttpServletRequest request, @PathParam("orderID") String orderID, @PathParam("orderContactID") String orderContactID, OrderContact orderContact) {
		logger.trace("updateOrderContact(Request, {}, {})", orderID, orderContactID);
		return os.updateOrderContact(request, orderID, orderContactID, orderContact);
	}
	
	@DELETE
	@Path("{orderID}/contacts/{orderContactID}")
	public void deleteOrderContact(@Context HttpServletRequest request, @PathParam("orderID") String orderID, @PathParam("orderContactID") String orderContactID) {
		logger.trace("deleteOrderContact(Request, {}, {})", orderID, orderContactID);
		os.deleteOrderContact(request, orderID, orderContactID);
	}
}
