package com.sok.runway.externalInterface.resources;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;


import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.SingleItemList;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.Contact;
import com.sok.runway.externalInterface.beans.ItemList;
import com.sok.runway.externalInterface.beans.shared.ContactProfile;
import com.sok.runway.externalInterface.beans.shared.Profile;
import com.sok.service.crm.ContactService;
import com.sok.service.crm.OrderService;
import com.sok.service.crm.ProfileService;
import com.sok.service.crm.UserService;
import com.sok.service.exception.NotAuthenticatedException;


/**
 * Short term hack resource as we're short of time. 
 * Sorry, in advance.  
 * 
 * Basically a holder for utility functions that don't warrant having their own services, etc.
 * 
 * @author mike
 *
 */
@Path("util")
public class UtilityResource {

	private static final UtilityResource resource = new UtilityResource();
	private static final Logger logger = LoggerFactory.getLogger(UtilityResource.class);
	private final ContactService cs = ContactService.getInstance();
	private final ProfileService ps = ProfileService.getInstance();
	private final UserService userService = UserService.getInstance();
	public static UtilityResource getInstance() {
		return resource;
	}

	@Context HttpServletRequest request;

	private List<ContactLinkType> contactLinkTypeList = null;
	private static final Map<String, List<Progress>> progressMap = Collections.synchronizedMap(new HashMap<String, List<Progress>>());

	@GET
	@Path("test2")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public com.sok.runway.crm.cms.properties.Facade getPackage() {
		//String pkgID = "0I1Y3T4Y0Y413M5D3P3U2G64333G";
		//com.sok.runway.crm.cms.properties.entities.HouseLandPackage pk = new com.sok.runway.crm.cms.properties.entities.HouseLandPackage(request);
		//pk.setCurrentUser("F180D2D08404F1241723606192C82747");
		//pk.load(pkgID);

		//logger.debug(pk.getRecord().getStatement());

		//return pk;

		String id = "0G1M313U4A2C737I3Y4N8U8U8D5U";		
		com.sok.runway.crm.cms.properties.Facade f = new com.sok.runway.crm.cms.properties.Facade(request, id);
		return f;
	}


	@GET
	@Path("progress")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Progress> getProgressItems(@Context HttpServletRequest request, @Context HttpServletResponse response) {
		if(!request.isSecure()){
			//no cache breaks downloading pdf in ie over https
			response.setHeader("Expires","Mon, 26 Jul 1997 05:00:00 GMT" );  // disable IE caching
			response.setHeader("Cache-Control","no-cache, must-revalidate" );
			response.setHeader("Pragma","no-cache" );
			response.setDateHeader ("Expires", 0);
		}
		return getProgressItems(userService.getCurrentUser(request));
	}

	public List<Progress> getProgressItems(UserBean user) {
		if(user == null || !progressMap.containsKey(user.getUserID())) {
			return Collections.emptyList();
		}
		return progressMap.get(user.getUserID());
	}

	/**
	 * Backwards compatible method for Runway jsp based calls.
	 */
	public Progress getProgressItem(HttpServletRequest request, String progressId) {
		return getProgressItem(request, null, progressId);
	}
	
	@GET
	@Path("progress/{progressId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Progress getProgressItem(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("progressId") String progressId) {
		if(!request.isSecure() && response != null){
			//no cache breaks downloading pdf in ie over https
			response.setHeader("Expires","Mon, 26 Jul 1997 05:00:00 GMT" );  // disable IE caching
			response.setHeader("Cache-Control","no-cache, must-revalidate" );
			response.setHeader("Pragma","no-cache" );
			response.setDateHeader ("Expires", 0);
		}
		return getProgressItem(userService.getCurrentUser(request), progressId);
	}

	public Progress getProgressItem(UserBean user, String progressId) {
		for(Progress p: getProgressItems(user)) {
			if(StringUtils.equals(p.id, progressId)) {
				return p;
			}
		}
		return null;
	}

	@PUT
	@Path("progress/{progressId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Progress updateProgressItem(@Context HttpServletRequest request, @PathParam("progressId") String progressId, @QueryParam("progress") double progress) {
		return updateProgressItem(userService.getCurrentUser(request), progressId, progress);
	}

	public Progress updateProgressItem(UserBean user, String progressId, double progress) {
		return updateProgressItem(user, progressId, progress, null, -1, -1);
	}
	
	public Progress updateProgressItem(UserBean user, String progressId, double progress, String title, int count, int total) {
		Progress p = getProgressItem(user, progressId);
		if(p != null) {
			p.p = progress * 100;
			if(p.p == 100)
				p.complete = true;
			else { 
				if(count != -1) 
					p.count = count;
				if(total != -1) 
					p.total = total;
			}
			if(StringUtils.isNotBlank(title)) p.title = title;
		}
		return p;
	}

	public Progress completeProgressItem(UserBean user, String progressId) {
		Progress p = getProgressItem(user, progressId);
		if(p != null) {
			p.complete = true;
		}
		return p;
	}


	@POST
	@Path("progress")
	public Progress createProgress(@Context HttpServletRequest request, String id, String title) {
		UserBean user = userService.getCurrentUser(request);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		return createProgress(user, id, title);
	}

	public Progress createProgress(UserBean user, String id, String title) {
		Progress e = getProgressItem(user, id);
		if(e != null) {
			if(!e.complete) {
				//throw exception, progess is continuing. 
			}
			e.title= title;
			e.p = 0;
			e.complete = false;
			return e;
		}

		Progress p = new Progress();
		p.id = id;
		p.title = title;
		p.p = 0;
		p.userID = user.getUserID();
		p.complete = false;

		if(!progressMap.containsKey(user.getUserID())) {
			progressMap.put(user.getUserID(), Collections.synchronizedList(new ArrayList<Progress>(1)));
		}
		progressMap.get(user.getUserID()).add(p);
		return p;
	}

	@DELETE
	@Path("progress/{progressId}")
	@Produces(MediaType.APPLICATION_JSON)
	public void removeProgressItem(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("progressId") String progressId) {
		UserBean user = userService.getCurrentUser(request);
		List<Progress> userList = getProgressItems(request, response);
		if(userList.isEmpty()) // returned as the user has no progress items 
			return;
		for(Progress p: userList) {
			if(StringUtils.equals(p.id, progressId)) {
				userList.remove(p);
				break;
			}
		}
		if(userList.isEmpty())	//we have removed one from a list of progress items, now there are none in it
			progressMap.remove(user.getUserID());
	}

	public static class Progress {
		public String id, title, userID;
		public int count = 0, total = 0;
		public double p = 0;
		public boolean complete = false;
	}

	@GET
	@Path("locations")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AddressLocation> searchLocations(@Context HttpServletRequest request, @QueryParam("search") String search) {
		logger.trace("searchLocations(Request, {})", search);
		if(StringUtils.isBlank(search)) {
			return Collections.emptyList();
		}
		GenRow lists = new GenRow();
		try { 
			lists.setTableSpec("Locations");
			lists.setRequest(request);
			lists.setParameter("-select","*");
			lists.setParameter("Pcode|1",search + "%");
			lists.setParameter("Locality|1", search + "%");
			lists.setParameter("-top","20");
			if(logger.isDebugEnabled()) {
				lists.doAction(GenerationKeys.SEARCH);
				logger.debug(lists.getStatement());
			}

			lists.getResults(true);

			if(lists.getNext()) {
				List<AddressLocation> list = new ArrayList<AddressLocation>(lists.getSize()); 
				do {
					list.add(new AddressLocation(lists));
				} while (lists.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			lists.close();
		}
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class AddressLocation {
		public String Locality, Postcode, State;
		public AddressLocation() {}
		public AddressLocation(GenRow r) {
			Locality = r.getString("Locality");
			Postcode = r.getString("Pcode");
			State = r.getString("State");
		}
		@Override
		public String toString() {
			return new StringBuilder()
			.append(Locality)
			.append("|")
			.append(Postcode)
			.append("|")
			.append(State)
			.toString();
		}
	}

	@GET
	@Path("contactlinktypes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ContactLinkType> getContactLinkTypes(@Context HttpServletRequest request) {

		if("clear".equals(request.getParameter("-clearCache"))) {
			contactLinkTypeList = null;
		}
		if(contactLinkTypeList != null) {
			return contactLinkTypeList;
		}

		GenRow r = new GenRow();
		try {
			r.setRequest(request);
			r.setViewSpec("ContactLinkTypeView");
			r.setParameter("-sort0","LinkType");
			r.getResults(true);

			if(r.getNext()) {
				List<ContactLinkType> list = new ArrayList<ContactLinkType>(r.getSize()); 
				do {
					list.add(new ContactLinkType(r));
				} while (r.getNext());
				contactLinkTypeList = list;
				return list;
			}
		} finally {
			r.close();
		}
		return Collections.emptyList();
	}

	@GET
	@Path("itemlists/{itemListName}")
	@Produces(MediaType.APPLICATION_JSON)
	public ItemList getItemList(@PathParam("itemListName") String itemListName) {
		return ps.getItemListByName(itemListName);
	}

	@GET
	@Path("costs/{itemListName}")
	@Produces(MediaType.APPLICATION_JSON)
	public ItemList getCostItemList(@PathParam("itemListName") String itemListName) {
		return ps.getCostItemListByName(itemListName);
	}

	@GET
	@Path("listheadings")
	@Produces(MediaType.APPLICATION_JSON)
	public List<HeadingsGroup> getListHeadings(@Context HttpServletRequest request, @QueryParam("module") String module) {
		if(StringUtils.isEmpty(module)) {
			throw new IllegalArgumentException("Module must be supplied");
		}
		GenRow list = new GenRow();
		try {
			list.setParameter("Module", module);
			list.setTableSpec("UserLayoutHeadingsGroup");
			list.setParameter("-select1","LayoutID");
			list.setParameter("-select2","LayoutName");
			list.getResults(true);

			if(list.getNext()) {
				List<HeadingsGroup> hgl = new ArrayList<HeadingsGroup>(list.getSize());
				do {
					hgl.add(new HeadingsGroup(list));
				} while(list.getNext());
				return hgl;
			}
			return Collections.emptyList();
		} finally {
			list.close();
		}
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class ContactLinkType {
		public String LinkTypeID, LinkType, ReverseLinkID;
		public ContactLinkType() {}
		public ContactLinkType(GenRow r) {
			LinkTypeID = r.getString("LinkTypeID");
			LinkType = r.getString("LinkType");
			ReverseLinkID = r.getString("ReverseLinkID");
		}
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class HeadingsGroup {
		public String LayoutID, LayoutName;
		public HeadingsGroup() {}
		public HeadingsGroup(GenRow r) {
			LayoutID = r.getString("LayoutID");
			LayoutName = r.getString("LayoutName");
		}
	}

	@GET
	@Path("regions")
	public List<Region> getRegions(@Context HttpServletRequest request) {
		GenRow row = new GenRow();
		try { 
			row.setRequest(request);
			row.setTableSpec("DisplayEntities");
			row.setParameter("-select0","DisplayEntityID");
			row.setParameter("-select1","Name");
			row.setParameter("-select2","Description");
			row.setParameter("-select3","Type");
			row.setParameter("-select4","ProductID");
			row.setParameter("-join1","DisplayEntityProduct");
			row.setParameter("-select5","DisplayEntityProduct.ProductNum");

			row.setParameter("Type","Region");
			row.sortBy("Name",0);

			if(logger.isDebugEnabled()) { 
				row.doAction("search");

			}

			row.getResults(true); 

			if(row.getNext()) {
				List<Region> list = new java.util.ArrayList<Region>(row.getSize());
				do {
					list.add(new Region(row));
				} while(row.getNext());
				return list;
			}
			return java.util.Collections.emptyList();
		} finally {
			row.close();
		}
	}


	@GET
	@Path("regions/{regionID}/locations")
	public List<Location> getLocations(@Context HttpServletRequest request, @PathParam("regionID") String regionID) {
		GenRow locations = new GenRow();
		try { 
			locations.setTableSpec("DisplayEntities");
			locations.setParameter("-select0","DisplayEntityID");
			locations.setParameter("-select1","Name");
			locations.setParameter("-select2","Description");
			locations.setParameter("-select3","Type");
			locations.setParameter("-select4","ProductID");
			locations.setParameter("ParentDisplayEntityID",regionID);
			locations.setParameter("Type","!Region"); 
			locations.sortBy("Name",0);
			locations.doAction("search");
			locations.getResults();

			if(locations.getNext()) {
				List<Location> list = new java.util.ArrayList<Location>(locations.getSize());
				do {
					list.add(new Location(locations));
				} while(locations.getNext());
				return list;
			}
			return java.util.Collections.emptyList();
		} finally {
			locations.close();
		}
	}

	/* aka, DisplayEntities */
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Region {
		public String RegionID, Name, Description, Type, ProductID, ProductNum;
		public Region(){}
		public Region(GenRow row) {
			RegionID = row.getString("DisplayEntityID");
			Name = row.getString("Name");
			Description = row.getString("Description");
			Type = row.getString("Type");
			ProductID = row.getString("ProductID");
			ProductNum = row.getString("ProductNum");
		}
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Location {
		public String LocationID, Name, Description, Type, ProductID, ProductNum; //LocationID, Pcode, Locality, State, Comments, Category;
		public Location(){} 
		public Location(GenRow row) {
			LocationID = row.getString("DisplayEntityID");
			Name = row.getString("Name");
			Description = row.getString("Description");
			Type = row.getString("Type");
			ProductID = row.getString("ProductID");
			ProductNum = row.getString("ProductNum");
			/*
			LocationID = row.getString("LocationID");
			Pcode = row.getString("Pcode");
			Locality = row.getString("Locality");
			State = row.getString("State");
			Comments = row.getString("Comments");
			Category = row.getString("Category");
			 */
		}
	}

	public static class ContactLocation { 
		public String ContactLocationID, ContactID, OrderID, RegionID, LocationID, Name,
		AddressID, StreetNumber, Street, Street2, City, State, Postcode, Country,
		ProductID /* display product id */, EstateProductID, ProductLotID;

		public ContactLocation() { } 
		public ContactLocation(GenRow g) { 
			ContactLocationID = g.getString("ContactLocationID");
			ContactID = g.getString("ContactID");
			OrderID = g.getString("OrderID");
			RegionID = g.getString("RegionID");
			LocationID = g.getString("LocationID");
			Name = g.getString("Name");

			AddressID = g.getString("AddressID");
			StreetNumber = g.getString("StreetNumber");
			Street = g.getString("Street");
			Street2 = g.getString("Street2");
			City = g.getString("City");
			State = g.getString("State");
			Postcode = g.getString("Postcode");
			Country = g.getString("Country");
			ProductLotID = g.getString("ProductLotID");
			ProductID = g.getString("ProductID");
			EstateProductID = g.getString("EstateProductID");
		}
	}

	public List<ContactLocation> getOrderLocations(HttpServletRequest request, String orderID) {
		return OrderService.getInstance().getOrderLocations(request, orderID);
	}

}
