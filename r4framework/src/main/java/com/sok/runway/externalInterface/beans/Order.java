package com.sok.runway.externalInterface.beans;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.runway.externalInterface.beans.shared.*;

/**
 * 
 * @author jong
 * 
 *         Java Bean for data transport only, contains no business logic.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
/* nb, if a property is ignored it is not mapped for read or write */
//@JsonIgnoreProperties({ "RepName", "RepEmail", "RepPhone", "OrderTemplate", "ProductType", "async" })
@JsonIgnoreProperties({ "RepName", "RepEmail", "RepPhone", "OrderTemplate", "async" })
public class Order {
	private String orderid = null;
	private String name = null;
	private String repuserid = null;
	private String productid = null;
	private String orderProductName = null;
	private double quantity = 0;
	private double cost = 0;
	private double gst = 0;
	private String timeofsale = null;
	private Date createddate = null;
	private Date modifieddate = null;
	private String modifiedby = null;
	private String contactid = null;
	private String companyid = null;
	private String invitation = null;
	private double totalcost = 0;
	private Date dateofsale = null;
	private String paymentmethod = null;
	private String contactphone = null;
	private String email = null;
	private String timeofcall = null;
	private String creditname = null;
	private String credittype = null;
	private String creditexpire = null;
	private String ordernum = null;
	private String creditnum = null;
	private String orderstatusid = null;
	private String websale = null;
	private String createdby = null;
	private String contactname = null;
	private String companyname = null;
	
	private String expiryAction = null;

	private String opportunityid = null;
	private String campaignid = null;
	private String notes = null;
	private String groupid = null;
	private int discount = 0;
	private float discountfloat = 0;
	private double discountamount = 0;
	private String processstageid = null;
	private String approval = null;
	private String hidetotals = null;
	private String documenturl = null;
	private int probability = 0;
	private Date outcomedate = null;
	private String ponumber = null;

	private Address deliveryaddress = null;
	private Status currentstatus = null;
	private String quoteViewID = null;
	private String orderTemplateID = null;
	private String error = null;
	private List<OrderContact> linkedContacts = null;
	private boolean active = false;
	private boolean promotionsLocked = false;
	
	private String regionID = null;
	private String regionName = null;
	private String productType = null;
	
	private OrderItem[] items = null;

	public String getError() {
		return (error);
	}

	public void setError(String error) {
		this.error = error;
	}

	@JsonProperty("OrderID")
	public String getOrderID() {
		return (orderid);
	}

	@JsonProperty("OrderID")
	public void setOrderID(String orderid) {
		this.orderid = orderid;
	}

	@JsonProperty("Name")
	public String getName() {
		return (name);
	}

	@JsonProperty("Name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("RepUserID")
	public String getRepUserID() {
		return (repuserid);
	}

	@JsonProperty("RepUserID")
	public void setRepUserID(String repuserid) {
		this.repuserid = repuserid;
	}

	@JsonProperty("ProductID")
	public String getProductID() {
		return (productid);
	}

	@JsonProperty("ProductID")
	public void setProductID(String productid) {
		this.productid = productid;
	}

	@JsonProperty("OrderProductName")
	public String getOrderProductName() {
		return (orderProductName);
	}

	@JsonProperty("OrderProductName")
	public void setOrderProductName(String orderProductName) {
		this.orderProductName = orderProductName;
	}

	@JsonProperty("Quantity")
	public double getQuantity() {
		return (quantity);
	}

	@JsonProperty("Quantity")
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	@JsonProperty("Cost")
	public double getCost() {
		return (cost);
	}

	@JsonProperty("Cost")
	public void setCost(double cost) {
		this.cost = cost;
	}

	@JsonProperty("GST")
	public double getGST() {
		return (gst);
	}

	@JsonProperty("GST")
	public void setGST(double gst) {
		this.gst = gst;
	}

	public String getTimeOfSale() {
		return (timeofsale);
	}

	public void setTimeOfSale(String timeofsale) {
		this.timeofsale = timeofsale;
	}

	public Date getCreatedDate() {
		return (createddate);
	}

	public void setCreatedDate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifiedDate() {
		return (modifieddate);
	}

	public void setModifiedDate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public String getModifiedBy() {
		return (modifiedby);
	}

	public void setModifiedBy(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	@JsonProperty("ContactID")
	public String getContactID() {
		return (contactid);
	}

	@JsonProperty("ContactID")
	public void setContactID(String contactid) {
		this.contactid = contactid;
	}

	@JsonProperty("CompanyID")
	public String getCompanyID() {
		return (companyid);
	}

	@JsonProperty("CompanyID")
	public void setCompanyID(String companyid) {
		this.companyid = companyid;
	}

	public String getInvitation() {
		return (invitation);
	}

	public void setInvitation(String invitation) {
		this.invitation = invitation;
	}

	@JsonProperty("TotalCost")
	public double getTotalCost() {
		return (totalcost);
	}

	@JsonProperty("TotalCost")
	public void setTotalCost(double totalcost) {
		this.totalcost = totalcost;
	}

	/*
	 * This is because the framework is returning a java.sql.Date, not a
	 * java.util.Date. java.sql.Date's are returned as strings in Jackson, and
	 * it is reccommended to not user java.sql.Date for any field being
	 * serialized.
	 * 
	 * http://wiki.fasterxml.com/JacksonFAQDateHandling
	 */
	@JsonProperty("DateOfSale")
	public Long getDateOfSaleLong() {
		if (dateofsale == null) {
			return null;
		}
		return new Long(dateofsale.getTime());
	}

	public Date getDateOfSale() {
		return (dateofsale);
	}

	@JsonProperty("DateOfSale")
	public void setDateOfSale(Date dateofsale) {
		this.dateofsale = dateofsale;
	}

	public String getPaymentMethod() {
		return (paymentmethod);
	}

	public void setPaymentMethod(String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}

	public String getContactPhone() {
		return (contactphone);
	}

	public void setContactPhone(String contactphone) {
		this.contactphone = contactphone;
	}

	public String getEmail() {
		return (email);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTimeOfCall() {
		return (timeofcall);
	}

	public void setTimeOfCall(String timeofcall) {
		this.timeofcall = timeofcall;
	}

	public String getCreditName() {
		return (creditname);
	}

	public void setCreditName(String creditname) {
		this.creditname = creditname;
	}

	public String getCreditType() {
		return (credittype);
	}

	public void setCreditType(String credittype) {
		this.credittype = credittype;
	}

	public String getCreditExpire() {
		return (creditexpire);
	}

	public void setCreditExpire(String creditexpire) {
		this.creditexpire = creditexpire;
	}

	@JsonProperty("OrderNum")
	public String getOrderNum() {
		return (ordernum);
	}

	@JsonProperty("OrderNum")
	public void setOrderNum(String ordernum) {
		this.ordernum = ordernum;
	}

	public String getCreditNum() {
		return (creditnum);
	}

	public void setCreditNum(String creditnum) {
		this.creditnum = creditnum;
	}

	public String getOrderStatusID() {
		return (orderstatusid);
	}

	public void setOrderStatusID(String orderstatusid) {
		this.orderstatusid = orderstatusid;
	}

	public String getWebSale() {
		return (websale);
	}

	public void setWebSale(String websale) {
		this.websale = websale;
	}

	public String getCreatedBy() {
		return (createdby);
	}

	public void setCreatedBy(String createdby) {
		this.createdby = createdby;
	}

	public String getContactName() {
		return (contactname);
	}

	public void setContactName(String contactname) {
		this.contactname = contactname;
	}

	public String getCompanyName() {
		return (companyname);
	}

	public void setCompanyName(String companyname) {
		this.companyname = companyname;
	}

	@JsonProperty("OpportunityID")
	public String getOpportunityID() {
		return (opportunityid);
	}

	@JsonProperty("OpportunityID")
	public void setOpportunityID(String opportunityid) {
		this.opportunityid = opportunityid;
	}

	public String getCampaignID() {
		return (campaignid);
	}

	public void setCampaignID(String campaignid) {
		this.campaignid = campaignid;
	}

	@JsonProperty("Notes")
	public String getNotes() {
		return (notes);
	}

	@JsonProperty("Notes")
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getGroupID() {
		return (groupid);
	}

	public void setGroupID(String groupid) {
		this.groupid = groupid;
	}

	public int getDiscount() {
		return (discount);
	}

	public float getDiscountFloat() {
		return (discount);
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public void setDiscountFloat(float discount) {
		this.discountfloat = discount;
	}

	public double getDiscountAmount() {
		return (discountamount);
	}

	public void setDiscountAmount(double discountamount) {
		this.discountamount = discountamount;
	}

	public String getProcessStageID() {
		return (processstageid);
	}

	public void setProcessStageID(String processstageid) {
		this.processstageid = processstageid;
	}

	@JsonProperty("Approval")
	public String getApproval() {
		return (approval);
	}

	@JsonProperty("Approval")
	public void setApproval(String approval) {
		this.approval = approval;
	}

	public String getHideTotals() {
		return (hidetotals);
	}

	public void setHideTotals(String hidetotals) {
		this.hidetotals = hidetotals;
	}

	public String getDocumentUrl() {
		return (documenturl);
	}

	public void setDocumentUrl(String documenturl) {
		this.documenturl = documenturl;
	}

	public int getProbability() {
		return (probability);
	}

	public void setProbability(int probability) {
		this.probability = probability;
	}

	@JsonProperty("OutcomeDate")
	public Long getOutcomeDateLong() {
		if (outcomedate == null || "Deposited".equalsIgnoreCase(expiryAction)) {
			return null;
		}
		return new Long(outcomedate.getTime());
	}
	
	public Date getOutcomeDate() {
		if (outcomedate == null || "Deposited".equalsIgnoreCase(expiryAction)) {
			return null;
		}
		return outcomedate;
	}

	@JsonProperty("OutcomeDate")
	public void setOutcomeDate(Date outcomedate) {
		this.outcomedate = outcomedate;
	}

	public String getPONumber() {
		return (ponumber);
	}

	public void setPONumber(String ponumber) {
		this.ponumber = ponumber;
	}

	private PersonName modifiedbyname = null;
	private PersonName createdbyname = null;
	private PersonName repname = null;

	public PersonName getModifiedByName() {
		return (modifiedbyname);
	}

	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedbyname = modifiedbyname;
	}

	public PersonName getRepName() {
		return (repname);
	}

	public void setRepName(PersonName repname) {
		this.repname = repname;
	}

	public PersonName getCreatedByName() {
		return (createdbyname);
	}

	public void setCreatedByName(PersonName createdbyname) {
		this.createdbyname = createdbyname;
	}

	public Address getDeliveryAddress() {
		return (deliveryaddress);
	}

	public void setDeliveryAddress(Address address) {
		deliveryaddress = address;
	}

	@JsonProperty("OrderItems")
	public OrderItem[] getOrderItems() {
		return (items);
	}

	@JsonProperty("OrderItems")
	public void setOrderItems(OrderItem[] items) {
		this.items = items;
	}

	public Status getCurrentStatus() {
		return (currentstatus);
	}

	public void setCurrentStatus(Status status) {
		currentstatus = status;
	}

	@JsonProperty("QuoteViewID")
	public String getQuoteViewID() {
		return quoteViewID;
	}

	@JsonProperty("QuoteViewID")
	public void setQuoteViewID(String quoteViewID) {
		this.quoteViewID = quoteViewID;
	}

	@JsonProperty("OrderTemplateID")
	public String getOrderTemplateID() {
		return orderTemplateID;
	}

	@JsonProperty("OrderTemplateID")
	public void setOrderTemplateID(String orderTemplateID) {
		this.orderTemplateID = orderTemplateID;
	}

	@JsonProperty("LinkedContacts")
	public List<OrderContact> getLinkedContacts() {
		return linkedContacts;
	}

	@JsonProperty("LinkedContacts")
	public void setLinkedContacts(List<OrderContact> linkedContacts) {
		this.linkedContacts = linkedContacts;
	}
	
	@JsonProperty("Active")
	public boolean isActive() {
		return active;
	}
	
	@JsonProperty("Active") 
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@JsonProperty("PromotionsLocked")
	public boolean isPromotionsLocked() {
		return promotionsLocked;
	}
	
	/**
	 * FYI - This is calculated, and is never persisted.
	 * We accept this in the return json as it will not hurt anything and will cause errors otherwise.
	 * @param promotionsLocked
	 */
	@JsonProperty("PromotionsLocked")
	public void setPromotionsLocked(boolean promotionsLocked) {
		this.promotionsLocked = promotionsLocked;
	}

	@JsonProperty("ExpiryAction")
	public String getExpiryAction() {
		return expiryAction;
	}

	public void setExpiryAction(String expiryAction) {
		this.expiryAction = expiryAction;
	}
	
	@JsonProperty("RegionID")
	public String getRegionID() {
		return regionID;
	}

	public void setRegionID(String regionID) {
		this.regionID = regionID;
	}
	
	@JsonProperty("RegionName")
	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	
	@JsonProperty("ProductType")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
}
