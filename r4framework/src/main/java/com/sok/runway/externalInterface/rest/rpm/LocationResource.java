package com.sok.runway.externalInterface.rest.rpm;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.Home;
import com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage;
import com.sok.runway.externalInterface.beans.cms.properties.Location;
import com.sok.runway.externalInterface.beans.cms.properties.Lot;
import com.sok.runway.externalInterface.beans.cms.properties.Staff;
import com.sok.runway.security.SecurityFactory;
//import com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackageSearch;
import com.sok.service.crm.cms.properties.HomeService;
import com.sok.service.crm.cms.properties.LocationService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

@Path("/location")
@Api(value = "/location", description = "Retrieve Location and Region Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class LocationResource {
	
	private static final LocationService locationService = LocationService.getInstance();
	static final Logger logger = LoggerFactory.getLogger(LocationResource.class);
	@Context HttpServletRequest request;
	
	@GET
	@ApiOperation(value = "List of all Locations and Regions", notes="<p>A location in Runway represents States, Regions, Display Centres, Land Centres and Offices. This is a tree structure where a State can have many Regions which in turn can have many Display Centres.</p><p>The /locations API will return all Locations from the root location onwards.</p>", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Location", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "Location")
	public Response getLocations() {
		GenRow search = new GenRow(); 
		
		search.parseRequest(request, "_");
		/* semi-hacky content-negotiation */
		String accept = StringUtils.trimToEmpty(request.getHeader("Accept"));
		int xml = accept.indexOf("text/xml"), json = accept.indexOf("application/json"); 
		MediaType mt = xml != -1 && (xml < json || json == -1) ? MediaType.TEXT_XML_TYPE : MediaType.APPLICATION_JSON_TYPE;
		
		return Response.ok(mt == MediaType.TEXT_XML_TYPE ? new LocationList(locationService.getRootLocation(request,true)) : locationService.getRootLocation(request,true)).build();
	}
	
	@XmlRootElement(name="Search")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class PagedSearchResult {
		public final List<Location> results; 
		public final int count;
		public final int page;  
		
		public PagedSearchResult(List<Location> results, int count, int page) {
			this.results = results; 
			this.count = count; 
			this.page = page; 
		}
	}
	
	@XmlAccessorType(XmlAccessType.PROPERTY)
	@XmlRootElement(name = "Location")
	public static class LocationList {
		
		private final Location location; 
		public LocationList() {
			location = null;
		}
		public LocationList(Location location) {
			this.location = location;
		}
		
		@XmlElement(name="Location")
		public Location getLocation() {
			return location;
		}
	}
	
	private class LastSearch {
		private final List<Location> list; 
		private final String hash; 
		private final int total; 
		
		public LastSearch(List<Location> list, String hash) {
			this.list = list; 
			this.hash = hash; 
			this.total = list.size();
		}
	}
	
	@GET
	@Path("/{locationID}")
	@ApiOperation(value = "Return a particular Location or Region", notes="<p>This API will start with the Location that is specified by locationID. If SubLocoations is true (default) then all locations linked to this location is also returned else only the specified location is returned.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Location")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public Location getLocation(@Context HttpServletRequest request, 
			@PathParam("locationID") final String locationID,
			@ApiParam(value = "Return all sub Locations", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("SubLocations") boolean populate
			) {
		return locationService.getLocation(request, locationID, populate);
	}
	
	@GET
	@ApiResource(value = "/{locationID}/staff", resourceClass="com.sok.runway.externalInterface.beans.cms.properties.Staff")
	@ApiOperation(value = "Return a list of Staff at a Location or Region", notes="<p>This API will return all staff and includes their name, phone, mobile and email for the specified location and SubLocations if selected.</p><p>In Runway Locations see the Staff tab.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Staff")
	@Path("/{locationID}/staff")
	public List<Staff> getLocationStaffResource(@PathParam("locationID") final String locationID,
			@ApiParam(value = "Return all staff from sub Locations", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("SubLocations") boolean populate) {
		
		return  locationService.getStaff(request, locationID, populate);
	}
	
	@GET
	@ApiResource(value = "/{locationID}/displays", resourceClass="com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage")
	@ApiOperation(value = "Return a list of Display Homes as Packages", notes="<p>This will return any display packages at the locations and SubLocations if selected. The information returned is simular as the /package/{packageID} simple form API.</p><p>A display package is a special form of the package that indicates it is a already built display home.</p><p>In Runway Locations see the Display tab.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage")
	@Path("/{locationID}/displays")
	public List<HouseLandPackage> getLocationDisplayResource(@PathParam("locationID") final String locationID,
			@ApiParam(value = "Return all sub Locations", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("SubLocations") boolean populate) {
		return locationService.getHouseAndLandDisplay(request, locationID, populate);
	}

	@GET
	@ApiResource(value = "/{locationID}/packages", resourceClass="com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage")
	@ApiOperation(value = "Return a list of Packages at a Location or Region", notes="<p>This will return any packages at the locations and SubLocations if selected. The information returned is simular as the /package/{packageID} simple form API.</p><p>In Runway Locations see the H&L Package tab.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackage")
	@Path("/{locationID}/packages")
	public List<HouseLandPackage> getLocationPackagesResource(@PathParam("locationID") final String locationID,
			@ApiParam(value = "Return all packages for all sub Locations", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("SubLocations") boolean populate) {
		return locationService.getHouseAndLand(request, locationID, populate);
	}

	@GET
	@ApiResource(value = "/{locationID}/homes", resourceClass="com.sok.runway.externalInterface.beans.cms.properties.Home")
	@ApiOperation(value = "Return a list of Homes at a Location or Region", notes="<p>This will return any Homes and Plans at the locations and SubLocations if selected. The information returned is simular as the /homes/{homeID} simple form API.</p><p>This is new functionality and is not available in Runway Locations.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Home")
	@Path("/{locationID}/homes")
	public List<Home> getLocationHomesResource(@PathParam("locationID") final String locationID,
			@ApiParam(value = "Return all Homes at all sub Locations", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("SubLocations") boolean populate) {
		return locationService.getHomes(request, locationID, populate);
	}

	@GET
	@ApiResource(value = "/{locationID}/lots", resourceClass="com.sok.runway.externalInterface.beans.cms.properties.Lot")
	@ApiOperation(value = "Return a list of Lots at a Location or Region", notes="<p>This will return any Lots (with Stages and Estates) at the locations and SubLocations if selected. The information returned is simular as the /lots/{lotID} simple form API.</p><p>In Runway Locations see the Lots tab.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Lot")
	@Path("/{locationID}/lots")
	public List<Lot> getLocationLandResource(@PathParam("locationID") final String locationID,
			@ApiParam(value = "Return all lots for all sub Locations", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("SubLocations") boolean populate) {
		return locationService.getLand(request, locationID, populate);
	}
}
