package com.sok.runway.externalInterface.beans.shared;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.FIELD)
public class ProductSecurityGroup {

	public String ProductSecurityGroupID, ProductID, GroupID, RepUserID, ProductSecurityGroupStatusID, GroupName, RepName, RepMobile, RepPhone, RepEmail;

	public ProductSecurityGroup() {
	}

	public ProductSecurityGroup(GenRow g) {
		ProductSecurityGroupID = g.getString("ProductSecurityGroupID");
		ProductID = g.getString("ProductID");
		GroupID = g.getString("GroupID");
		RepUserID = g.getString("RepUserID");
		ProductSecurityGroupStatusID = g.getString("ProductSecurityGroupStatusID");
		GroupName = g.getString("Name");
		RepName = g.getString("RepFirstName") + " " + g.getString("RepLastName");
		RepMobile = g.getString("RepMobile");
		RepPhone = g.getString("RepPhone");
		RepEmail = g.getString("RepEmail");
	}

	public void populateGenRow(GenRow g) {
		g.setParameter("ProductID", ProductID);
		g.setParameter("GroupID", GroupID);
		g.setParameter("RepUserID", RepUserID);
		g.setParameter("ProductSecurityGroupStatusID", ProductSecurityGroupStatusID);
	}
}
