package com.sok.runway.externalInterface.beans.shared;

import org.codehaus.jackson.annotate.JsonProperty;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class ContactAnswer extends Answer
{
	private String contactid = null;
	
	@JsonProperty("ContactID")
	public String getContactID()
	{
		return(contactid);
	}	
	
	@JsonProperty("ContactID")
	public void setContactID(String contactid)
	{
		this.contactid = contactid;
	}		
}