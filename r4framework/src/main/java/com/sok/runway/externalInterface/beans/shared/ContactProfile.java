package com.sok.runway.externalInterface.beans.shared;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class ContactProfile extends Profile
{
	private ContactQuestion[] questions = null;
	
	@JsonProperty("Questions")
	public ContactQuestion[] getQuestions()
	{
		return(questions);
	}	
	
	@JsonProperty("Questions")
	public void setQuestions(ContactQuestion[] questions)
	{
		this.questions = questions;
	}	
}