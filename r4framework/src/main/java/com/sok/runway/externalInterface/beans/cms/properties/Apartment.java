package com.sok.runway.externalInterface.beans.cms.properties;

import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

import com.wordnik.swagger.annotations.ApiClass;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.RunwayUtil;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.SimplePublishing;
import com.sok.runway.externalInterface.beans.cms.SimpleCampaign;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.runway.externalInterface.beans.shared.ProductSecurityGroup;

/**
 * v2 of the Apartment external bean. 
 * 
 * Rather than maintaining extra fields with backwards compatibility in the one object and forcing updates on xml services, we're using a new object. 
 *
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Apartment")
//@XmlType(propOrder={"productNum", "description","address","locationID", "location", "regionID", "region", "currentStatus", "active", "details", "titleMonth", "titleYear", "GST", "totalCost","interactiveMap", "createdBy","createdByName","createdDate","modifiedBy","modifiedByName","modifiedDate","campaign","error"})
//@ApiClass(description="Deprecated: ProductID, EstateProductID, StageProductID")
public class Apartment extends SimpleApartment {
	
	private String productNum, description, createdBy, modifiedBy, subType, marketingStatus;
	private Address address;
	private Date createdDate, modifiedDate;
	private Date availableDate, expiryDate;
	private String error; 
	private PersonName modifiedByName = null, createdByName = null;
	private String active;
	
	private String displayPrice = ""; 
	
	private String locationID, regionID;
	//private HomeDetails details;
	private List<ProductDetail> details = null;
	private String titleMonth, titleYear;
	private Double GST = null, totalCost = null; //, price = null;
	private SimpleCampaign campaign = null;
	private ApartmentStatusHistory currentStatus = null;
	private SimpleDocument image = null;
	private SimpleDocument thumbnail = null;
	
	private InteractiveMap interactiveMap;
	
	private Location location = null;
	private Location region = null;
	protected SimpleBuildingStage stage = null;
	private SimplePublishing publishing = null;
	private List<ProductSecurityGroup> groups;
	private String publishHeadline;
	private String publishDescription;
	
	public Apartment() {}
	public Apartment(com.sok.runway.crm.cms.properties.Apartment data) {
		this.setApartmentID(data.getData("ProductID"));
		this.setName(data.getString("Name"));
		this.setProductNum(data.getString("ProductNum"));
		this.setDescription(data.getString("Description"));
		this.setActive(data.getString("Active"));
		this.setSubType(data.getString("ProductSubType"));
		this.setMarketingStatus(data.getString("MarketingStatus"));
		
		String displayPriceType = data.getString("DisplayPriceType");
		
		if (data.isSet("DripResult")) {
			this.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("DripResult")));
		} else if (data.isSet("TotalCost")) {
			this.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("TotalCost")));
		}
			
		if (StringUtils.isBlank(this.getDisplayPrice()) && data.isSet("DisplayPrice")) {
			this.setDisplayPrice(data.getString("DisplayPrice"));
		}
		
		this.setAvailableDate(data.getDate("AvailableDate"));
		this.setExpiryDate(data.getDate("ExpiryDate"));
		
		this.setGroups(BeanFactory.getProductGroups(data.getConnection(), this.getApartmentID()));
		
		this.setBuildingStage(new BuildingStage());
		this.getBuildingStage().setBuildingStageID(data.getString("BuildingStageProductID"));
		this.getBuildingStage().setName(data.getString("BuildingStageName"));
		
		Building es = new Building();		
		es.setBuildingID(data.getString("BuildingProductID"));
		es.setName(data.getString("BuildingName"));
		this.getBuildingStage().setBuilding(es);
		
		this.setAddress(BeanFactory.getAddressFromTableData(data, ""));
		this.setCreatedDate(data.getDate("CreatedDate"));
		this.setModifiedDate(data.getDate("ModifiedDate"));
		this.setCreatedBy(data.getString("CreatedBy"));
	    this.setCreatedByName(BeanFactory.getPersonName(data.getString("OwnFirstName"), data.getString("OwnLastName")));
	    this.setModifiedBy(data.getString("ModifiedBy"));
	    this.setModifiedByName(BeanFactory.getPersonName(data.getString("ModFirstName"), data.getString("ModLastName")));
	    
	    this.setPublishing(BeanFactory.loadPublishing(data.getData("ProductID")));
		
		this.setRegionID(data.getString("RegionID"));
		this.setLocationID(data.getString("LocationID"));
		//this.setDetails(new HomeDetails(data));
		this.setCost(data.getDouble("Cost"));
		this.setGST(data.getDouble("GST"));
		this.setTotalCost(data.getDouble("TotalCost"));
		this.setTitleMonth(data.getString("TitleMonth"));
		this.setTitleYear(data.getString("TitleYear"));
		
		ApartmentStatusHistory s = new ApartmentStatusHistory();
		s.setStatus(data.getString("CurrentStatus"));
		s.setStatusID(data.getString("CurrentStatusID"));
		this.setCurrentStatus(s);
		
		if(StringUtils.isNotBlank(data.getData("Image"))) {
			this.setImage(BeanFactory.loadDocument(data.getData("Image")));
		} else if(StringUtils.isNotBlank(data.getData("EstateImage"))) {
			this.setImage(BeanFactory.loadDocument(data.getData("EstateImage")));
		}
		
		if(StringUtils.isNotBlank(data.getData("ThumbnailImage"))) {
			this.setThumbnail(BeanFactory.loadDocument(data.getData("ThumbnailImage")));
		} else if(StringUtils.isNotBlank(data.getData("EstateThumbnailImage"))) {
			this.setThumbnail(BeanFactory.loadDocument(data.getData("EstateThumbnailImage")));
		}

		GenRow maps = new GenRow();
		maps.setViewSpec("LinkedDocumentView");
		maps.setConnection(data.getConnection());
		maps.setParameter("ProductID",data.getString("ProductID"));
		maps.setParameter("DocumentSubType","MasterPlan");
		maps.setParameter("LinkedDocumentDocument.DocumentSubType","MasterPlan");
		
		if (maps.getString("ProductID").length() > 0) {
			maps.doAction("search");
			
			maps.getResults();
			
			if (maps.getNext()) {
				InteractiveMap im = new InteractiveMap();
				im.setMajorID(data.getString("EstateProductID"));
				im.setMinorID(data.getString("StageProductID"));
				im.setProductType("Land");
				
				do {
					if (maps.getString("FileName").toLowerCase().endsWith(".jpg") || maps.getString("FileName").toLowerCase().endsWith(".png")) {
						SimpleDocument sd = new SimpleDocument();
						sd.setDocumentID(maps.getString("DocumentID"));
						sd.setFileName(maps.getString("FileName"));
						sd.setFilePath(maps.getString("FilePath"));
						sd.setType(maps.getString("DocumentType"));
						sd.setSubType(maps.getString("DocumentSubType"));
						
						im.setMapImage(sd);
					} else if (maps.getString("FileName").toLowerCase().endsWith(".map")) {
						im.loadMapHTML(maps.getString("FilePath"));
					}
					
				} while (maps.getNext());
				
				this.setInteractiveMap(im);
			}
		}
		
		maps.close();
	}
	
	
	@XmlElement(name="ProductNum")
	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}
	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@XmlElement(name="PublishHeadline")
	public String getPublishHeadline() {
		return publishHeadline;
	}
	public void setPublishHeadline(String publishHeadline) {
		this.publishHeadline = publishHeadline;
	}
	
	@XmlElement(name="PublishDescription")
	public String getPublishDescription() {
		return publishDescription;
	}
	public void setPublishDescription(String publishDescription) {
		this.publishDescription = publishDescription;
	}
	
	@XmlElement(name="LocationID")
	public String getLocationID() {
		return locationID;
	}
	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}
	@XmlElement(name="RegionID")
	public String getRegionID() {
		return regionID;
	}
	public void setRegionID(String regionID) {
		this.regionID = regionID;
	}
	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName(){
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedByName = modifiedbyname;
	}
	
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@XmlElement(name="Address")
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	
	@XmlElement(name="TitleMonth")
	public String getTitleMonth() {
		return titleMonth;
	}
	public void setTitleMonth(String titleMonth) {
		this.titleMonth = titleMonth;
	}
	@XmlElement(name="TitleYear")
	public String getTitleYear() {
		return titleYear;
	}
	public void setTitleYear(String titleYear) {
		this.titleYear = titleYear;
	}

	@XmlElement(name="GST")
	public Double getGST() {
		return GST;
	}
	public void setGST(Double gST) {
		this.GST = gST;
	}
	@XmlElement(name="TotalCost")
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	/*
	@XmlElement(name="Price")
	public Double getPrice() {
		return price; 
	}
	public void setPrice(Double price) {
		this.price = price; 
	}
	*/
	@XmlElement(name="Error")
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	@XmlElement(name="Campaign")
	public SimpleCampaign getCampaign() {
		return campaign;
	}
	public void setCampaign(SimpleCampaign campaign) {
		this.campaign = campaign;
	}
	
	@XmlElementWrapper(name = "Details")
	@XmlElement(name="Detail")
	public List<ProductDetail> getDetails() {
		return details;
	}
	public void setDetails(List<ProductDetail> details) {
		this.details = details;
	}
	
	@XmlElement(name="CurrentStatus")
	public ApartmentStatusHistory getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(ApartmentStatusHistory currentStatus) {
		this.currentStatus = currentStatus; 
	}
	
	@XmlElement(name="Active")
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
	@XmlElement(name="Location")
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	
	@XmlElement(name="Region")
	public Location getRegion() {
		return region;
	}
	public void setRegion(Location region) {
		this.region = region;
	}

	@XmlElement(name="InteractiveMap")
	public InteractiveMap getInteractiveMap() {
		return interactiveMap;
	}
	public void setInteractiveMap(InteractiveMap interactiveMap) {
		this.interactiveMap = interactiveMap;
	}

	@XmlElement(name="Image")
	public SimpleDocument getImage() {
		return image;
	}
	public void setImage(SimpleDocument image) {
		this.image = image;
	}
	
	@XmlElement(name="Thumbnail")
	public SimpleDocument getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(SimpleDocument image) {
		this.thumbnail = image;
	}

	@XmlElement(name="SubType")
	public String getSubType() {
		return subType;
	}
	public void setSubType(String SubType) {
		this.subType = SubType;
	}
	@XmlElement(name="BuildingStage")
	public SimpleBuildingStage getBuildingStage() {
		return stage;
	}
	public void setBuildingStage(SimpleBuildingStage stage) {
		this.stage = stage;
	}
	@XmlElement(name="Publishing")
	public SimplePublishing getPublishing() {
		return publishing;
	}
	public void setPublishing(SimplePublishing publishing) {
		this.publishing = publishing;
	}
	
	@XmlElement(name="VisibilityStatus")
	public String getVisibilityStatus() {
		if (currentStatus == null || currentStatus.getStatusID() == null) return "Offline";
		
		if (!VisibilityStatus.checkVisibilityStatus(active, currentStatus.getStatusID(), availableDate, expiryDate)) return "Offline";
		
		if ("true".equals(InitServlet.getSystemParam("RPM-Publishing-Apartments"))) {
			if (publishing == null) return "Offline";
			if (!VisibilityStatus.checkPublishedStatus(publishing.getPublishedFlag(), publishing.getStatus(), publishing.getStartDate(), publishing.getEndDate())) return "Offline";
		}
		
		return "Online";
	}
	
	public void setVisibilityStatus(String status) {
		
	}
	@XmlElement(name="AvailableDate")
	public Date getAvailableDate() {
		return availableDate;
	}
	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}
	@XmlElement(name="ExpiryDate")
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	@XmlElement(name="DisplayPrice")
	public String getDisplayPrice() {
		return displayPrice;
	}
	public void setDisplayPrice(String displayPrice) {
		this.displayPrice = displayPrice;
	}
	@XmlElement(name="MarketingStatus")
	public String getMarketingStatus() {
		return marketingStatus;
	}
	public void setMarketingStatus(String marketingStatus) {
		this.marketingStatus = marketingStatus;
	}

	@XmlElementWrapper(name = "Groups")
	@XmlElement(name="Group")
	public List<ProductSecurityGroup> getGroups() {
		return groups;
	}
	public void setGroups(List<ProductSecurityGroup> groups) {
		this.groups = groups;
	}	
	
}
