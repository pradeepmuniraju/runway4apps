package com.sok.runway.externalInterface.beans.shared;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name="Person")
public class PersonName
{
	private String firstname = null;
	private String lastname = null;
	
	public PersonName() {} 
	public PersonName(String firstname, String lastname) {
		this.firstname = firstname;
		this.lastname = lastname;
	}
	
	@XmlElement(name="FirstName")
	public String getFirstName()
	{
		return(firstname);
	}	
	public void setFirstName(String firstname)
	{
		this.firstname = firstname;
	}	
	
	@XmlElement(name="LastName")
	public String getLastName()
	{
		return(lastname);
	}	
	public void setLastName(String lastname)
	{
		this.lastname = lastname;
	}
	
	@XmlElement(name="Name")
	public String getName() {
		return toString();
	}
	
	@Override
	public String toString() {
		StringBuilder name = new StringBuilder();
		if(firstname != null)
		{
			name.append(firstname).append(" ");
		}
		if(lastname != null) {
			name.append(lastname);
		}
		return name.toString();
	}
	
}