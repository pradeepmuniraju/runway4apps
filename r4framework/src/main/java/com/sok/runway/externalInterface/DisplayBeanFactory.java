package com.sok.runway.externalInterface;
import com.sok.runway.externalInterface.beans.displays.*;
import com.sok.runway.externalInterface.beans.displays.shared.*;
import com.sok.framework.*;

public class DisplayBeanFactory
{
	public static ContactDisplay getContactDisplay()
	{
	   DisplaySpec dspec = SpecManager.getDisplaySpec("ContactDisplay");
		ContactDisplay display = new ContactDisplay();
		
		display.setGroupDisplay(getGroupDisplay());
		return(display);
	}
	
	public static CompanyDisplay getCompanyDisplay()
	{
		CompanyDisplay display = new CompanyDisplay();
		display.setGroupDisplay(getGroupDisplay());
		return(display);
	}
	
	public static NoteDisplay getNoteDisplay()
	{
		NoteDisplay display = new NoteDisplay();
		display.setGroupDisplay(getGroupDisplay());
		return(display);
	}	
	
	protected static GroupDisplay getGroupDisplay()
	{
		GroupDisplay display = new GroupDisplay();
		return(display);
	}
}