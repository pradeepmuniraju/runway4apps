package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.framework.InitServlet;
import com.sok.runway.crm.cms.properties.Land;
import com.sok.runway.crm.cms.properties.entities.ApartmentEntity;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.SimplePublishing;
import com.sok.runway.externalInterface.beans.shared.Address;

@XmlAccessorType(XmlAccessType.FIELD)
public class LegacyApartment {

	private String ProductID;
	private String Name;
	private String ProductNum;
	private String BuildingProductID;
	private String BuildingName;
	private String StageProductID;
	private String StageName;
	//private String ApartmentName;
	private String LocationID, RegionID, Status, StatusID, SubType;
	private Address Address;
	private HomeDetails Details;
	private String TitleMonth, TitleYear;
	private double Cost, GST, TotalCost;
	private String Error; 
	private String active;
	private SimplePublishing publishing = null;
	private Date availableDate, expiryDate;
	
	public LegacyApartment() {}
	public LegacyApartment(String error) {
		this.Error = error;
	}
	public LegacyApartment(Land data) {
		this.ProductID = data.getString("ProductID");
		this.Name = data.getString("Name");
		this.ProductNum = data.getString("ProductNum");
		this.active = data.getData("Active");
		
		this.BuildingProductID = data.getString("BuildingProductID");
		this.BuildingName = data.getString("BuildingName");
		
		this.StageProductID = data.getString("StageProductID");
		this.StageName = data.getString("StageName");
		this.RegionID = data.getString("RegionID");
		this.LocationID = data.getString("LocationID");
		this.Address = BeanFactory.getAddressFromTableData(data, "");
		this.Details = new HomeDetails(data);
		
		this.Cost = data.getDouble("Cost");
		this.GST = data.getDouble("GST");
		this.TotalCost = data.getDouble("TotalCost");
		this.TitleMonth = data.getString("ApartmentTitleMonth");
		this.TitleYear = data.getString("ApartmentTitleYear");
		
		this.Status = data.getString("CurrentStatus");
		this.StatusID = data.getString("CurrentStatusID");
		this.SubType = data.getString("ProductSubType");

		this.setPublishing(BeanFactory.loadPublishing(data.getData("ProductID")));
	}
	
	public LegacyApartment(ApartmentEntity le) {
		this(BeanFactory.external(le));
        setDetails(le.getDetails());		
	}
	
	public LegacyApartment(Apartment apartment) {
		
		this.ProductID = apartment.getApartmentID();
		this.Name = apartment.getName();
		this.ProductNum = apartment.getProductNum();
		this.active = apartment.getActive();
		
		this.BuildingProductID = (apartment.getBuildingStage() != null && apartment.getBuildingStage().getBuilding() != null) ? apartment.getBuildingStage().getBuilding().getBuildingID() : null;
		this.BuildingName = (apartment.getBuildingStage() != null && apartment.getBuildingStage().getBuilding() != null) ? apartment.getBuildingStage().getBuilding().getName() : null;
		
		this.StageProductID = (apartment.getBuildingStage() != null) ? apartment.getBuildingStage().getBuildingStageID() : null;
		this.StageName = (apartment.getBuildingStage() != null) ? apartment.getBuildingStage().getName() : null;
		this.RegionID = apartment.getRegionID();
		this.LocationID = apartment.getLocationID();
		this.Address = apartment.getAddress();
		
		this.Cost = apartment.getCost() != null ? apartment.getCost() : 0d;
		this.GST = apartment.getGST() != null ? apartment.getGST() : 0d;
		this.TotalCost = apartment.getTotalCost() != null ? apartment.getTotalCost() : 0d;
		this.TitleMonth = apartment.getTitleMonth();
		this.TitleYear = apartment.getTitleYear();
		
		this.Status = apartment.getCurrentStatus() != null ? apartment.getCurrentStatus().getStatus() : null;
		this.StatusID = apartment.getCurrentStatus() != null ? apartment.getCurrentStatus().getStatusID() : null;
		this.SubType = apartment.getSubType();
	}
	
	@JsonProperty("ProductID")
	public String getProductID() {
		return ProductID;
	}
	@JsonProperty("ProductID")
	public void setProductID(String productID) {
		ProductID = productID;
	}
	@JsonProperty("Name")
	public String getName() {
		return Name;
	}
	@JsonProperty("Name")
	public void setName(String name) {
		Name = name;
	}
	@JsonProperty("ProductNum")
	public String getProductNum() {
		return ProductNum;
	}
	@JsonProperty("ProductNum")
	public void setProductNum(String productNum) {
		ProductNum = productNum;
	}
	@JsonProperty("BuildingProductID")
	public String getBuildingProductID() {
		return BuildingProductID;
	}
	@JsonProperty("BuildingProductID")
	public void setBuildingProductID(String estateProductID) {
		BuildingProductID = estateProductID;
	}
	@JsonProperty("BuildingName")
	public String getBuildingName() {
		return BuildingName;
	}
	@JsonProperty("BuildingName")
	public void setBuildingName(String estateName) {
		BuildingName = estateName;
	}
	@JsonProperty("StageProductID")
	public String getStageProductID() {
		return StageProductID;
	}
	@JsonProperty("StageProductID")
	public void setStageProductID(String stageProductID) {
		StageProductID = stageProductID;
	}
	@JsonProperty("StageName")
	public String getStageName() {
		return StageName;
	}
	@JsonProperty("StageName")
	public void setStageName(String stageName) {
		StageName = stageName;
	}
	@JsonProperty("ApartmentName")
	public String getApartmentName() {
		return Name;
	}
	@JsonProperty("ApartmentName")
	public void setApartmentName(String apartmentName) {
		this.Name = apartmentName;
	}
	@JsonProperty("LocationID")
	public String getLocationID() {
		return LocationID;
	}
	@JsonProperty("LocationID")
	public void setLocationID(String locationID) {
		LocationID = locationID;
	}
	@JsonProperty("RegionID")
	public String getRegionID() {
		return RegionID;
	}
	@JsonProperty("RegionID")
	public void setRegionID(String regionID) {
		RegionID = regionID;
	}
	@JsonProperty("Address")
	public Address getAddress() {
		return Address;
	}
	@JsonProperty("Address")
	public void setAddress(Address address) {
		Address = address;
	}
	@JsonProperty("TitleMonth")
	public String getTitleMonth() {
		return TitleMonth;
	}
	@JsonProperty("TitleMonth")
	public void setTitleMonth(String titleMonth) {
		TitleMonth = titleMonth;
	}
	@JsonProperty("TitleYear")
	public String getTitleYear() {
		return TitleYear;
	}
	@JsonProperty("TitleYear")
	public void setTitleYear(String titleYear) {
		TitleYear = titleYear;
	}
	@JsonProperty("StatusID")
	public String getStatusID() {
		return StatusID;
	}
	@JsonProperty("StatusID")
	public void setStatusID(String StatusID) {
		this.StatusID = StatusID;
	}
	@JsonProperty("Status")
	public String getStatus() {
		return Status;
	}
	@JsonProperty("Status")
	public void setStatus(String Status) {
		this.Status = Status;
	}
	@JsonProperty("SubType")
	public String getSubType() {
		return SubType;
	}
	@JsonProperty("SubType")
	public void setSubType(String SubType) {
		this.SubType = SubType;
	}
	
	@JsonProperty("Cost")
	public double getCost() {
		return Cost;
	}
	@JsonProperty("Cost")
	public void setCost(double cost) {
		Cost = cost;
	}
	@JsonProperty("GST")
	public double getGST() {
		return GST;
	}
	@JsonProperty("GST")
	public void setGST(double gST) {
		GST = gST;
	}
	@JsonProperty("TotalCost")
	public double getTotalCost() {
		return TotalCost;
	}
	@JsonProperty("TotalCost")
	public void setTotalCost(double totalCost) {
		TotalCost = totalCost;
	}
	@JsonProperty("Detail")
	public HomeDetails getDetails() {
		return Details;
	}
	@JsonProperty("Detail")
	public void setDetails(HomeDetails Details) {
		this.Details = Details;
	}
	@JsonProperty("Error")
	public String getError() {
		return Error;
	}
	@JsonProperty("Error")
	public void setError(String error) {
		Error = error;
	}
	@XmlElement(name="Publishing")
	public SimplePublishing getPublishing() {
		return publishing;
	}
	public void setPublishing(SimplePublishing publishing) {
		this.publishing = publishing;
	}
	@XmlElement(name="VisibilityStatus")
	public String getVisibilityStatus() {
		if (StatusID == null) return "Offline";
		
		if (!VisibilityStatus.checkVisibilityStatus(active, StatusID, availableDate, expiryDate)) return "Offline";
		
		if ("true".equals(InitServlet.getSystemParam("RPM-Publishing-Apartments"))) {
			if (publishing == null) return "Offline";
			if (!VisibilityStatus.checkPublishedStatus(publishing.getPublishedFlag(), publishing.getStatus(), publishing.getStartDate(), publishing.getEndDate())) return "Offline";
		}
		
		return "Online";
	}
	
	public void setVisibilityStatus(String status) {
		
	}
	@XmlElement(name="AvailableDate")
	public Date getAavalableDate() {
		return availableDate;
	}
	public void setAavalableDate(Date avalableDate) {
		this.availableDate = avalableDate;
	}
	@XmlElement(name="ExpiryDate")
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
}