package com.sok.runway.externalInterface.beans.shared;

import java.util.Date;

public class AbstractGroup {

	private String groupID, groupName, repUserID, createdBy;
	private Date createdDate; 
	private PersonName createdByName, repUserName;
	//private GroupStatusHistory currentStatus; 
	
	public String getGroupID() {
		return groupID;
	}
	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getRepUserID() {
		return repUserID;
	}
	public void setRepUserID(String repUserID) {
		this.repUserID = repUserID;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public PersonName getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}
	public PersonName getRepUserName() {
		return repUserName;
	}
	public void setRepUserName(PersonName repUserName) {
		this.repUserName = repUserName;
	}
}
