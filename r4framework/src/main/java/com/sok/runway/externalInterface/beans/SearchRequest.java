package com.sok.runway.externalInterface.beans;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class SearchRequest
{
	SearchParameter[] parameters = null;
	
	public void setParameters(SearchParameter[] parameters)
	{
		this.parameters = parameters;
	}

	public SearchParameter[] getParameters()
	{
		return(parameters);
	}
}