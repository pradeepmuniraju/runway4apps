package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.*;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.sok.framework.InitServlet;
import com.sok.runway.JSPLogger;
import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.SimplePublishing;
import com.sok.runway.externalInterface.beans.cms.SimpleCampaign;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.LegacyLocation;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.runway.externalInterface.beans.shared.ProductSecurityGroup;
import com.sok.service.crm.cms.properties.PlanService.HLPackageCost;
import com.wordnik.swagger.annotations.ApiClass;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiProperty;

@XmlAccessorType(XmlAccessType.NONE)
//@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement(name = "Package")
@ApiClass(description="Deprecated: ProductID")
//@XmlType(propOrder={"packageID", "name","productNum","description"
//		,"lot","plan","image","facade","address","basePrice","dripPrice","campaignDripPrice","priceExCampaignDripPrice","price","campaigns","location","region","marketingStatus","currentStatus","active","displayFlag","packageCosts"
//		,"publishHeadline","publishDescription","detailPdfUrl","preAvailableDate","availableDate","expiryDate"
//		,"createdBy","createdByName","createdDate","modifiedBy","modifiedByName","modifiedDate","error"})
public class HouseLandPackage {

	private String packageID, name, productNum, description, createdBy, modifiedBy, publishHeadline, publishDescription, detailPDFURL, active, displayFlag, marketingStatus;
	private Address address;
	private Date createdDate, modifiedDate, preAvailableDate, availableDate, expiryDate;
	private String error; 
	private PersonName modifiedByName = null, createdByName = null;
	private HouseLandPackageStatusHistory currentStatus; 
	private String displayPrice = "";
	
	//<Price><%= Math.floor(price) %></Price>
	//<WasPrice><%= Math.floor(searchBean.getDouble("TotalCost")) %></WasPrice>
	//<FirstHomeBuyersPrice><%= Math.floor(price) %></FirstHomeBuyersPrice>
	//<Featured><%=String.valueOf(featured) %></Featured>
	//<Campaign><![CDATA[<%=featured ? searchBean.getString("CampaignName") : "" %>]]></Campaign>
	//<Status><%=status %></Status>
	
	Double basePrice = null, dripPrice = null, campaignDripPrice = null, price = null;
	List<SimpleCampaign> campaigns = Collections.emptyList();
	LegacyLocation location, region;
	
	/* this could potentially include the lot or plan information */
	private SimpleLot lot; 
	private SimpleDocument image;
	private SimplePlan plan;
	private SimpleFacade facade;
	private List<HLPackageCost> packageCosts;
	private SimplePublishing publishing = null;
	private List<ProductSecurityGroup> groups;

	@XmlElement(name="PackageID")
	public String getPackageID() {
		return packageID;
	}
	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name="ProductNum")
	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}
	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@XmlElement(name="Lot")
	public SimpleLot getLot() {
		return lot;
	}
	public void setLot(SimpleLot lot) {
		this.lot = lot;
	}
	@XmlElement(name="Plan")
	public SimplePlan getPlan() {
		return plan;
	}
	public void setPlan(SimplePlan plan) {
		this.plan = plan;
	}
	
	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName(){
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedByName = modifiedbyname;
	}
	
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@XmlElement(name="PreAvailableDate")
	public Date getPreAvailableDate() {
		return preAvailableDate;
	}
	public void setPreAvailableDate(Date preAvailableDate) {
		this.preAvailableDate = preAvailableDate;
	}
	
	@XmlElement(name="Address")
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	@XmlElement(name="Error")
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	@XmlElement(name="Image")
	public SimpleDocument getImage() {
		return image;
	}
	public void setImage(SimpleDocument image) {
		this.image = image;
	}
	
	@XmlElement(name="CurrentStatus")
	public HouseLandPackageStatusHistory getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(HouseLandPackageStatusHistory currentStatus) {
		this.currentStatus = currentStatus; 
	}
	
	@ApiProperty(allowableValues="Y|N")
	@XmlElement(name="Active")
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active; 
	}
	
	@ApiProperty(allowableValues="Y|N")
	@XmlElement(name="DisplayFlag")
	public String getDisplayFlag() {
		return displayFlag;
	}
	public void setDisplayFlag(String displayFlag) {
		this.displayFlag = displayFlag;
	}
	
	@ApiProperty(allowableValues="See LotMarketingStatus value list")
	@XmlElement(name="MarketingStatus")
	public String getMarketingStatus() {
		return marketingStatus;
	}
	public void setMarketingStatus(String marketingStatus) {
		this.marketingStatus = marketingStatus; 
	}
	
	@XmlElement(name="BasePrice")
	public Double getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(Double basePrice) {
		this.basePrice = basePrice;
	}
	
	@XmlElement(name="DripPrice")
	public Double getDripPrice() {
		return dripPrice;
	}
	public void setDripPrice(Double dripPrice) {
		this.dripPrice = dripPrice;
	}
	@XmlElement(name="CampaignDripPrice")
	public Double getCampaignDripPrice() {
		return campaignDripPrice;
	}
	public void setCampaignDripPrice(Double campaignDripPrice) {
		this.campaignDripPrice = campaignDripPrice;
	}
	
	/**
	 * @deprecated	- This is here specifically for Visual Jazz. Do not use this ever.
	 */
	@XmlElement(name="PriceExCampaignDripPrice")
	public Double getPriceExCampaignDripPrice() {
		return (price != null ? price - (campaignDripPrice != null ? campaignDripPrice.doubleValue() : 0) : null);
	}
	// fake method to keep jackson happy
	public void setPriceExCampaignDripPrice(Double price) { } 
	
	@XmlElement(name="Price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@XmlElementWrapper(name = "Campaigns")
	@XmlElement(name="Campaign")
	public List<SimpleCampaign> getCampaigns() {
		return campaigns;
	}
	public void setCampaigns(List<SimpleCampaign> campaigns) {
		this.campaigns = campaigns;
	}
	@XmlElement(name="Facade")
	public SimpleFacade getFacade() {
		return facade;
	}
	public void setFacade(SimpleFacade facade) {
		this.facade = facade;
	}
	
	@XmlElementWrapper(name = "PackageCosts")
	@XmlElement(name="PackageCost")
	public List<HLPackageCost> getPackageCosts() {
		return packageCosts;
	}
	public void setPackageCosts(List<HLPackageCost> packageCosts) {
		this.packageCosts = packageCosts;
	}
	
	@XmlElement(name="PublishHeadline")
	public String getPublishHeadline() {
		return publishHeadline;
	}
	public void setPublishHeadline(String publishHeadline) {
		this.publishHeadline = publishHeadline;
	}
	
	@XmlElement(name="PublishDescription")
	public String getPublishDescription() {
		return publishDescription;
	}
	public void setPublishDescription(String publishDescription) {
		this.publishDescription = publishDescription;
	}
	
	@XmlElement(name="PropertyPDFURL")
	public String getDetailPdfUrl() {
		return detailPDFURL; 
	}
	public void setDetailPdfUrl(String detailPDFURL) {
		this.detailPDFURL = detailPDFURL; 
	}
	
	@XmlElement(name="Location")
	public LegacyLocation getLocation() {
		return location;
	}
	public void setLocation(LegacyLocation location) {
		this.location = location;
	}

	@XmlElement(name="Region")
	public LegacyLocation getRegion() {
		return region;
	}
	public void setRegion(LegacyLocation region) {
		this.region = region;
	}
	@XmlElement(name="Publishing")
	public SimplePublishing getPublishing() {
		return publishing;
	}
	public void setPublishing(SimplePublishing publishing) {
		this.publishing = publishing;
	}
	@XmlElement(name="VisibilityStatus")
	public String getVisibilityStatus() {
		if (currentStatus == null || currentStatus.getStatusID() == null) return "Offline";
		
		if (!VisibilityStatus.checkVisibilityStatus(active, currentStatus.getStatusID(), availableDate, expiryDate)) return "Offline";
		
		if ("true".equals(InitServlet.getSystemParam("RPM-Publishing-HouseAndLand"))) {
			if (publishing == null) return "Offline";
			if (!VisibilityStatus.checkPublishedStatus(publishing.getPublishedFlag(), publishing.getStatus(), publishing.getStartDate(), publishing.getEndDate())) return "Offline";
		}
		
		return "Online";
	}
	
	public void setVisibilityStatus(String status) {
		
	}
	@XmlElement(name="AvailableDate")
	public Date getAavalableDate() {
		return availableDate;
	}
	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}
	@XmlElement(name="ExpiryDate")
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	@XmlElement(name="DisplayPrice")
	public String getDisplayPrice() {
		return displayPrice;
	}
	public void setDisplayPrice(String displayPrice) {
		this.displayPrice = displayPrice;
	}
	
	@XmlElementWrapper(name = "Groups")
	@XmlElement(name="Group")
	public List<ProductSecurityGroup> getGroups() {
		return groups;
	}
	public void setGroups(List<ProductSecurityGroup> groups) {
		this.groups = groups;
	}	
	
}
