package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Range")
@XmlType(propOrder={"rangeID","name","brand"})
public abstract class SimpleRange {

	protected String rangeID, name;
	protected SimpleBrand brand; 
	
	@XmlElement(name="RangeID")
	public String getRangeID() {
		return rangeID;
	}
	public void setRangeID(String rangeID) {
		this.rangeID = rangeID;
	}
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name="Brand")
	public SimpleBrand getBrand() {
		return brand;
	}
	public void setBrand(SimpleBrand brand) {
		this.brand = brand;
	}
}