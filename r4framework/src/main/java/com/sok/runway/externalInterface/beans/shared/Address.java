package com.sok.runway.externalInterface.beans.shared;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no bussiness logic.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement(name="Address")
@XmlType(propOrder={"addressID","streetNumber","street","street2","city","state","postcode","country","mapRef","useMap","latitude","longitude"})
public class Address
{	
	private String addressID = null;
	private String streetNumber = null;
	private String street = null;
	private String street2 = null;
	private String city = null;
	private String postcode = null;
	private String state = null;
	private String country = null;
	private String mapRef = null;
	private String latitude = null, longitude = null;
	private boolean useMap = false;
	
	@XmlElement(name="AddressID")
	public String getAddressID() {
		return this.addressID;
	}
	public void setAddressID(String addressID) {
		this.addressID = addressID;
	}	
	
	@XmlElement(name="StreetNumber")
	public String getStreetNumber() {
		return streetNumber;
	}
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	@XmlElement(name="Street")
	public String getStreet()
	{
		return(street);
	}	
	public void setStreet(String street)
	{
		this.street = street;
	}	

	@XmlElement(name="Street2")
	public String getStreet2()
	{
		return(street2);
	}
	public void setStreet2(String street2)
	{
		this.street2 = street2;
	}	
	
	@XmlElement(name="City")
	public String getCity()
	{
		return(city);
	}
	public void setCity(String city)
	{
		this.city = city;
	}		
	
	@XmlElement(name="State")
	public String getState()
	{
		return(state);
	}	
	public void setState(String state)
	{
		this.state = state;
	}	
	
	@XmlElement(name="Postcode")
	public String getPostcode()
	{
		return(postcode);
	}
	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}	
	
	@XmlElement(name="Country")
	public String getCountry()
	{
		return(country);
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	
	@XmlElement(name="UseMap")
	public boolean isUseMap() {
		return this.useMap == true;
	}
	public void setUseMap(Boolean useMap) {
		this.useMap = useMap != null ? useMap.booleanValue() : false;
	}
	
	@XmlElement(name="MapRef")
	public String getMapRef()
	{
		return(mapRef);
	}
	public void setMapRef(String mapRef)
	{
		this.mapRef = mapRef;
	}
	
	@XmlElement(name="Latitude")
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	@XmlElement(name="Longitude")
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}