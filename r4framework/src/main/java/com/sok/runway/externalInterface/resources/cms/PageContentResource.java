package com.sok.runway.externalInterface.resources.cms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.cms.PageContent;
import com.sok.service.cms.WebsiteService;

public class PageContentResource {

	private static final Logger logger = LoggerFactory.getLogger(PageContentResource.class);
	private static final WebsiteService websiteService = WebsiteService.getInstance();
	private final String cmsID, pageID;
	@Context HttpServletRequest request;
	
	public PageContentResource(String cmsID, String pageID, HttpServletRequest request) {
		this.cmsID = cmsID; 
		this.pageID = pageID;
		this.request = request; 
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PageContent> getPageContents(@Context HttpServletRequest request) {
		return websiteService.getPageContents(request, cmsID, pageID);
	}
	
	@GET
	@Path("{PageContentID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PageContent getPageContent(@PathParam("PageContentID") String pageContentID) {
		return websiteService.getPageContent(request, cmsID, pageID, pageContentID);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public PageContent insertPageContent(PageContent pageContent) {
		return websiteService.createPageContent(request, cmsID, pageID, pageContent);
	}
	
	@PUT
	@Path("{PageContentID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PageContent updatePageContent(@PathParam("PageContentID") String pageContentID, PageContent pageContent) {
		return websiteService.updatePageContent(request, cmsID, pageID, pageContentID, pageContent);
	}
}
