package com.sok.runway.externalInterface;

import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.generation.IllegalConfigurationException;
import com.sok.runway.SingleItemList;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.Facade;
import com.sok.runway.crm.cms.properties.HomePlan;
import com.sok.runway.crm.cms.properties.entities.*;
import com.sok.runway.externalInterface.beans.*;
import com.sok.runway.externalInterface.beans.cms.SimpleCampaign;
import com.sok.runway.externalInterface.beans.cms.properties.*;
import com.sok.runway.externalInterface.beans.cms.properties.Range;
import com.sok.runway.externalInterface.beans.shared.*;
import com.sok.runway.externalInterface.beans.shared.Answer;
import com.sok.runway.externalInterface.beans.shared.ListItem;
import com.sok.runway.externalInterface.beans.shared.Profile;
import com.sok.runway.view.ValueList;
import com.sok.service.crm.OrderService;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.FacadeService;
import com.sok.service.crm.cms.properties.LocationService;
import com.sok.service.crm.cms.properties.PackageService;
import com.sok.service.crm.cms.properties.PlanService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BeanFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(BeanFactory.class);

    private static final SimpleDateFormat API_LAST_MODIFIED_TIME_FORMATTER = new SimpleDateFormat("yyyyMMdd-HH-mm-ss");
    private static final String DEFAULT_CLIENT_BUCKET_NAME = "default-client-bucket";

    private static final String SYS_PARAM_PDF_DOWNLOAD_URL_HOME = "PdfDownloadUrlHome";
    private static final String SYS_PARAM_R6_AWSS3_BUCKET_NAME = "R6-AWSS3BucketName";
    private static final String SYS_PARAM_URL_HOME = "URLHome";
    private static final String SYS_PARAM_PDF_AUTO_CACHING_ENABLED = "PDFAutoCachingEnabled";

    public static Contact getErrorContact(String error) {
        Contact bean = new Contact();
        bean.setError(error);
        return (bean);
    }

    public static Contact getContactFromTableData(TableData data) {
        Contact bean = new Contact();
        return setContactFromTableData(bean, data);
    }

    public static OrderContact getOrderContactFromTableData(TableData data) {

        OrderContact bean = new OrderContact();
        bean.setOrderContactID(data.getString("OrderContactID"));
        bean.setOrderID(data.getString("OrderID"));
        bean.setRole(data.getString("Role"));
        bean.setRecipientType(data.getString("RecipientType"));

        return (OrderContact) setContactFromTableData(bean, data);
    }

    public static Address getAddressFromTableData(TableData data, String prefix) {
        if (prefix == null) {
            prefix = "";
        }
        Address address = new Address();
        address.setAddressID(StringUtils.trimToNull(data.getString(prefix + "AddressID")));
        address.setStreetNumber(StringUtils.trimToNull(data.getString(prefix + "StreetNumber")));
        address.setStreet(StringUtils.trimToNull(data.getString(prefix + "Street")));
        address.setStreet2(StringUtils.trimToNull(data.getString(prefix + "Street2")));
        address.setCity(StringUtils.trimToNull(data.getString(prefix + "City")));
        address.setState(StringUtils.trimToNull(data.getString(prefix + "State")));
        address.setPostcode(StringUtils.trimToNull(data.getString(prefix + "Postcode")));
        address.setCountry(StringUtils.trimToNull(data.getString(prefix + "Country")));
        address.setUseMap("Y".equals(data.getString(prefix + "UseMap")));
        address.setLatitude(StringUtils.trimToNull(data.getString(prefix + "Latitude")));
        address.setLongitude(StringUtils.trimToNull(data.getString(prefix + "Longitude")));
        address.setMapRef(StringUtils.trimToNull(data.getString(prefix + "MapRef")));
        return address;
    }

    public static void setTableDataFromAddress(Address address, TableData data, String prefix) {
        if (address == null) {
            return;
        }
        data.setParameter(prefix + "AddressID", address.getAddressID());
        data.setParameter(prefix + "StreetNumber", address.getStreetNumber());
        data.setParameter(prefix + "Street", address.getStreet());
        data.setParameter(prefix + "Street2", address.getStreet2());
        data.setParameter(prefix + "City", address.getCity());
        data.setParameter(prefix + "State", address.getState());
        data.setParameter(prefix + "Postcode", address.getPostcode());
        data.setParameter(prefix + "Country", address.getCountry());
        data.setParameter(prefix + "UseMap", address.isUseMap() ? "Y" : "N");
        data.setParameter(prefix + "MapRef", address.getMapRef());
        data.setParameter(prefix + "Latitude", address.getLatitude());
        data.setParameter(prefix + "Longitude", address.getLongitude());
    }

    public static Contact setContactFromTableData(Contact bean, TableData data) {
        bean.setContactID(data.getString("ContactID"));
        bean.setTitle(data.getString("Title"));
        bean.setFirstName(data.getString("FirstName"));
        bean.setLastName(data.getString("LastName"));
        bean.setSalutation(data.getString("Salutation"));
        bean.setPosition(data.getString("Position"));
        bean.setCompany(data.getString("Company"));
        bean.setDepartment(data.getString("Department"));

        bean.setPhone(data.getString("Phone"));
        bean.setFax(data.getString("Fax"));
        bean.setMobile(data.getString("Mobile"));
        bean.setWeb(data.getString("Web"));
        bean.setEmail(data.getString("Email"));
        bean.setRepUserID(data.getString("RepUserID"));
        bean.setMgrUserID(data.getString("MgrUserID"));
        bean.setCreatedDate((Date) data.getObject("CreatedDate"));
        bean.setModifiedDate((Date) data.getObject("ModifiedDate"));
        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setCreatedBy(data.getString("CreatedBy"));
        bean.setReferrerID(data.getString("ReferrerID"));
        bean.setCompanyID(data.getString("CompanyID"));
        bean.setFollowUpDate((Date) data.getObject("FollowUpDate"));
        bean.setEmailType(data.getString("EmailType"));
        bean.setBandwidth(data.getString("Bandwidth"));
        bean.setContact(data.getString("Contact"));
        bean.setKeep(data.getString("Keep"));
        bean.setContactStatusID(data.getString("ContactStatusID"));
        bean.setGKTitle(data.getString("GKTitle"));
        bean.setGKFirstName(data.getString("GKFirstName"));
        bean.setGKLastName(data.getString("GKLastName"));
        bean.setQualifiedDate(data.getString("QualifiedDate"));
        bean.setQualifiedBy(data.getString("QualifiedBy"));

        bean.setHomePhone(data.getString("HomePhone"));
        bean.setNotes(data.getString("Notes"));
        bean.setToBeQualified(data.getString("ToBeQualified"));
        bean.setEmail2(data.getString("Email2"));
        bean.setEmailStatus(data.getString("EmailStatus"));
        bean.setGroupID(data.getString("GroupID"));
        bean.setType(data.getString("Type"));
        bean.setSource(data.getString("Source"));
        bean.setAccountNo(data.getString("AccountNo"));
        bean.setIndustry(data.getString("Industry"));
        bean.setUsername(data.getString("Username"));
        bean.setPassword(data.getString("Password"));
        bean.setCreatedCampaignID(data.getString("CreatedCampaignID"));

        Address physical = new Address();
        Address postal = new Address();

        postal.setStreet(data.getString("HomeStreet"));
        postal.setStreet2(data.getString("HomeStreet2"));
        postal.setCity(data.getString("HomeCity"));
        postal.setState(data.getString("HomeState"));
        postal.setPostcode(data.getString("HomePostcode"));
        postal.setCountry(data.getString("HomeCountry"));

        physical.setStreet(data.getString("Street"));
        physical.setStreet2(data.getString("Street2"));
        physical.setCity(data.getString("City"));
        physical.setState(data.getString("State"));
        physical.setPostcode(data.getString("Postcode"));
        physical.setCountry(data.getString("Country"));

        bean.setPhysicalAddress(physical);
        bean.setPostalAddress(postal);

        Status status = new Status();
        status.setStatusID(data.getString("CurrentStatusID"));
        status.setStatus(data.getString("CurrentStatus"));
        status.setValue(data.getString("CurrentStatusValue"));
        bean.setCurrentStatus(status);

        bean.setError(data.getError());

        bean.setCreatedByName(getPersonName(data.getString("OwnFirstName"), data
                .getString("OwnLastName")));
        bean.setModifiedByName(getPersonName(data.getString("ModFirstName"), data
                .getString("ModLastName")));
        bean.setRepName(getPersonName(data.getString("RepFirstName"), data
                .getString("RepLastName")));
        bean.setMgrName(getPersonName(data.getString("MgrFirstName"), data
                .getString("MgrLastName")));
        bean.setQualifiedByName(getPersonName(data.getString("QuaFirstName"),
                data.getString("QuaLastName")));


        if ("true".equals(data.getString("-loadGroups"))) { // set in soap endpoint, poss future default

            GenRow contactGroups = new GenRow();
            contactGroups.setViewSpec("ContactGroupView");
            contactGroups.setParameter("ContactID", data.getString("ContactID"));
            contactGroups.setParameter("-sort1", "SortOrder");

            if (contactGroups.isSet("ContactID")) {
                if (LOGGER.isTraceEnabled()) {
                    contactGroups.doAction(GenerationKeys.SEARCH);
                    LOGGER.trace(contactGroups.getStatement());
                }

                try {
                    contactGroups.setConnection(data.getConnection());
                    contactGroups.getResults(true);

                    if (contactGroups.getNext()) {
                        ContactGroup[] groups = new ContactGroup[contactGroups.getSize()];
                        int ix = 0;
                        ContactGroup cg = null;
                        do {
                            cg = new ContactGroup();
                            cg.setGroupID(contactGroups.getData("GroupID"));
                            cg.setGroupName(contactGroups.getData("Name"));
                            cg.setRepUserID(contactGroups.getData("RepUserID"));
                            cg.setCreatedBy(contactGroups.getData("CreatedBy"));
                            try {
                                cg.setCreatedDate(contactGroups.getDate("CreatedDate"));
                            } catch (IllegalConfigurationException iec) {
                            }
                            if (contactGroups.isSet("RepUserID")) {
                                cg.setRepUserName(new PersonName(contactGroups.getData("RepFirstName"), contactGroups.getData("RepLastName")));
                            }
                            groups[ix++] = cg;
                        } while (contactGroups.getNext());
                        bean.setGroups(groups);
                    }
                } finally {
                    contactGroups.close();
                }
            }
        }
        return (bean);
    }

    public static Company getErrorCompany(String error) {
        Company bean = new Company();
        bean.setError(error);
        return (bean);
    }

    public static Company getCompanyFromTableData(TableData data) {
        Company bean = new Company();

        bean.setCompanyID(data.getString("CompanyID"));
        bean.setCompany(data.getString("Company"));
        bean.setDepartment(data.getString("Department"));

        bean.setPhone(data.getString("Phone"));
        bean.setFax(data.getString("Fax"));
        bean.setWeb(data.getString("Web"));
        bean.setEmail(data.getString("Email"));
        bean.setCreatedDate((Date) data.getObject("CreatedDate"));
        bean.setModifiedDate((Date) data.getObject("ModifiedDate"));
        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setRepUserID(data.getString("RepUserID"));
        bean.setMgrUserID(data.getString("MgrUserID"));
        bean.setSource(data.getString("Source"));
        bean.setCreatedBy(data.getString("CreatedBy"));
        bean.setType(data.getString("Type"));
        bean.setIndustry(data.getString("Industry"));
        bean.setQualifiedDate((Date) data.getObject("QualifiedDate"));
        bean.setQualifiedBy(data.getString("QualifiedBy"));
        bean.setFollowUpDate((Date) data.getObject("FollowUpDate"));

        bean.setPhone2(data.getString("Phone2"));
        bean.setNotes(data.getString("Notes"));
        bean.setGroupID(data.getString("GroupID"));
        bean.setCompanyStatusID(data.getString("CompanyStatusID"));
        bean.setWholesaler(data.getString("Wholesaler"));
        bean.setAccountNo(data.getString("AccountNo"));
        bean.setLegalCompanyName(data.getString("LegalCompanyName"));
        bean.setABN(data.getString("ABN"));

        Address physical = new Address();
        Address postal = new Address();

        physical.setStreet(data.getString("Street"));
        physical.setStreet2(data.getString("Street2"));
        physical.setCity(data.getString("City"));
        physical.setState(data.getString("State"));
        physical.setPostcode(data.getString("Postcode"));
        physical.setCountry(data.getString("Country"));

        postal.setStreet(data.getString("PostalStreet"));
        postal.setStreet2(data.getString("PostalStreet2"));
        postal.setCity(data.getString("PostalCity"));
        postal.setState(data.getString("PostalState"));
        postal.setPostcode(data.getString("PostalPostcode"));
        postal.setCountry(data.getString("PostalCountry"));

        bean.setPhysicalAddress(physical);
        bean.setPostalAddress(postal);

        Status status = new Status();
        status.setStatusID(data.getString("CurrentStatusID"));
        status.setStatus(data.getString("CurrentStatus"));
        status.setValue(data.getString("CurrentStatusValue"));
        bean.setCurrentStatus(status);

        bean.setError(data.getError());

        bean.setCreatedByName(getPersonName(data.getString("OwnFirstName"), data
                .getString("OwnLastName")));
        bean.setModifiedByName(getPersonName(data.getString("ModFirstName"), data
                .getString("ModLastName")));
        bean.setRepName(getPersonName(data.getString("RepFirstName"), data
                .getString("RepLastName")));
        bean.setMgrName(getPersonName(data.getString("MgrFirstName"), data
                .getString("MgrLastName")));
        bean.setQualifiedByName(getPersonName(data.getString("QuaFirstName"),
                data.getString("QuaLastName")));

        return (bean);
    }

    public static User getErrorUser(String error) {
        User bean = new User();
        bean.setError(error);
        return (bean);
    }

    public static User getUserFromTableData(TableData data) {
        User bean = new User();
        setUserFromTableData(bean, data);
        return bean;
    }

    public static void setUserFromTableData(User bean, TableData data) {

        bean.setUserID(data.getString("UserID"));
        bean.setUsername(data.getString("Username"));
        // bean.setPassword(data.getString("Password"));
        bean.setTitle(data.getString("Title"));
        bean.setFirstName(data.getString("FirstName"));
        bean.setLastName(data.getString("LastName"));
        bean.setPosition(data.getString("Position"));
        bean.setCompany(data.getString("Company"));

        bean.setPhone(data.getString("Phone"));
        bean.setFax(data.getString("Fax"));
        bean.setMobile(data.getString("Mobile"));
        bean.setWeb(data.getString("Web"));
        bean.setEmail(data.getString("Email"));
        bean.setEmailSig(data.getString("EmailSig"));
        bean.setLastLogin((Date) data.getObject("LastLogin"));
        bean.setCreatedDate((Date) data.getObject("CreatedDate"));
        bean.setModifiedDate((Date) data.getObject("ModifiedDate"));
        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setCreatedBy(data.getString("CreatedBy"));
        bean.setEmailHost(data.getString("EmailHost"));
        bean.setEmailUsername(data.getString("EmailUsername"));
        bean.setEmailPassword(data.getString("EmailPassword"));
        bean.setEmailProtocol(data.getString("EmailProtocol"));
        bean.setTimeZone(data.getString("TimeZone"));
        bean.setStatus(data.getString("Status"));
        bean.setSaveEmails(data.getString("SaveEmails"));
        bean.setSupportID(data.getString("SupportID"));
        bean.setMgrUserID(data.getString("MgrUserID"));
        bean.setIPAddress(data.getString("IPAddress"));
        bean.setHomePage(data.getString("HomePage"));
        bean.setListSize(data.getString("ListSize"));
        bean.setDefaultSearchGroup(data.getString("DefaultSearchGroup"));
        bean.setDefaultList(data.getString("DefaultList"));
        bean.setDefaultGroupID(data.getString("DefaultGroupID"));
        bean.setDefaultRoleID(data.getString("DefaultRoleID"));
        bean.setSevenDayCalendar(data.getString("SevenDayCalendar"));
        // bean.setPasswordChanged(data.getString("PasswordChanged"));
        bean.setSignatureImage(data.getString("SignatureImage"));
        bean.setPhotoImage(data.getString("PhotoImage"));
        bean.setDefaultCampaignID(data.getString("DefaultCampaignID"));
        bean.setDefaultDashBoardID(data.getString("DefaultDashBoardID"));

        Address address = new Address();
        address.setStreet(data.getString("Street"));
        address.setStreet2(data.getString("Street2"));
        address.setCity(data.getString("City"));
        address.setState(data.getString("State"));
        address.setPostcode(data.getString("Postcode"));
        address.setCountry(data.getString("Country"));
        bean.setAddress(address);

        bean.setError(data.getError());

        bean.setCreatedByName(getPersonName(data.getString("OwnFirstName"), data
                .getString("OwnLastName")));
        bean.setModifiedByName(getPersonName(data.getString("ModFirstName"), data
                .getString("ModLastName")));

    }

    public static Order getOrderFromTableData(TableData data) {
        Order bean = new Order();
        bean.setOrderID(data.getString("OrderID"));
        bean.setName(data.getString("Name"));
        bean.setRepUserID(data.getString("RepUserID"));
        bean.setRepName(getPersonName(data.getString("RepFirstName"), data.getString("RepLastName")));
        bean.setProductID(data.getData("ProductID"));
        if (data.getString("Quantity").length() != 0) {
            bean.setQuantity(Double.parseDouble(data.getString("Quantity")));
        }
        if (data.getString("Cost").length() != 0) {
            bean.setCost(Double.parseDouble(data.getString("Cost")));
        }
        if (data.getString("GST").length() != 0) {
            bean.setGST(Double.parseDouble(data.getString("GST")));
        }
        bean.setTimeOfSale(data.getString("TimeOfSale"));
        bean.setCreatedDate((Date) data.getObject("CreatedDate"));

        bean.setModifiedDate((Date) data.getObject("ModifiedDate"));
        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setContactID(data.getString("ContactID"));
        bean.setCompanyID(data.getString("CompanyID"));
        bean.setInvitation(data.getString("Invitation"));
        if (data.getString("GST").length() != 0) {
            bean.setTotalCost(Double.parseDouble(data.getString("TotalCost")));
        }
        bean.setDateOfSale((Date) data.getObject("DateOfSale"));
        bean.setPromotionsLocked(OrderService.getInstance().isPromotionLocked(bean.getDateOfSale()));

        bean.setPaymentMethod(data.getString("PaymentMethod"));
        bean.setContactPhone(data.getString("ContactPhone"));
        bean.setEmail(data.getString("Email"));
        bean.setTimeOfCall(data.getString("TimeOfCall"));
        bean.setCreditName(data.getString("CreditName"));
        bean.setCreditType(data.getString("CreditType"));
        bean.setCreditExpire(data.getString("CreditExpire"));
        bean.setOrderNum(data.getString("OrderNum"));
        bean.setCreditNum(data.getString("CreditNum"));
        bean.setOrderStatusID(data.getString("OrderStatusID"));
        bean.setWebSale(data.getString("WebSale"));
        bean.setCreatedBy(data.getString("CreatedBy"));
        bean.setContactName(data.getString("ContactName"));
        bean.setCompanyName(data.getString("CompanyName"));

        bean.setOpportunityID(data.getString("OpportunityID"));
        bean.setCampaignID(data.getString("CampaignID"));
        bean.setNotes(data.getString("Notes"));
        bean.setGroupID(data.getString("GroupID"));
        if (data.getString("Discount").length() != 0) {
            bean.setDiscount((int) Float.parseFloat(data.getString("Discount")));
        }
        if (data.getString("Discount").length() != 0) {
            bean.setDiscountFloat(Float.parseFloat(data.getString("Discount")));
        }
        if (data.getString("DiscountAmount").length() != 0) {
            bean.setDiscountAmount(Double.parseDouble(data
                    .getString("DiscountAmount")));
        }
        bean.setProcessStageID(data.getString("ProcessStageID"));
        bean.setApproval(data.getString("Approval"));
        bean.setHideTotals(data.getString("HideTotals"));
        bean.setDocumentUrl(data.getString("DocumentUrl"));
        if (data.getString("Probability").length() != 0) {
            bean.setProbability(Integer.parseInt(data.getString("Probability")));
        }
        bean.setExpiryAction(data.getString("ExpiryAction"));
        bean.setOutcomeDate((Date) data.getObject("OutcomeDate"));
        bean.setPONumber(data.getString("PONumber"));

        Address address = new Address();
        address.setStreet(data.getString("Street"));
        address.setStreet2(data.getString("Street2"));
        address.setCity(data.getString("City"));
        address.setState(data.getString("State"));
        address.setPostcode(data.getString("Postcode"));
        address.setCountry(data.getString("Country"));
        bean.setDeliveryAddress(address);

        Status status = new Status();
        status.setStatusID(data.getString("CurrentStatusID"));
        status.setStatus(data.getString("CurrentStatus"));
        status.setValue(data.getString("CurrentStatusValue"));
        bean.setCurrentStatus(status);

        if (data.isSet("QuoteViewID")) {
            bean.setQuoteViewID(data.getString("QuoteViewID"));
        }
        if (data.isSet("OrderTemplateID")) {
            bean.setOrderTemplateID(data.getString("OrderTemplateID"));
        }

        bean.setError(data.getError());

        bean.setCreatedByName(getPersonName(data.getString("OwnFirstName"), data
                .getString("OwnLastName")));
        bean.setModifiedByName(getPersonName(data.getString("ModFirstName"), data
                .getString("ModLastName")));

        //default is active.
        bean.setActive(!"N".equals(data.getString("Active")));
        bean.setRegionName(data.getString("RegionName"));
        bean.setProductType(data.getString("ProductType"));
        bean.setRegionID(data.getString("RegionID"));

        return (bean);
    }

    public static Product getProductFromTableData(TableData data) {
        Product bean = new Product();
        bean.setProductID(data.getData("ProductID"));
        bean.setName(data.getString("Name"));
        bean.setProductNum(data.getString("ProductNum"));
        bean.setDescription(data.getString("Description"));
        bean.setSummary(data.getString("Summary"));
        bean.setCategory(data.getString("Category"));
        if (data.getString("BaseCost").length() != 0) {
            bean.setBaseCost(Double.parseDouble(data.getString("BaseCost")));
        }
        if (data.getString("Margin").length() != 0) {
            bean.setMargin(Double.parseDouble(data.getString("Margin")));
        }
        bean.setCost(Double.parseDouble(data.getString("Cost")));
        bean.setGST(Double.parseDouble(data.getString("GST")));
        bean.setTotalCost(Double.parseDouble(data.getString("TotalCost")));

        bean.setCreatedDate((Date) data.getObject("CreatedDate"));
        bean.setCreatedBy(data.getString("CreatedBy"));
        bean.setModifiedDate((Date) data.getObject("ModifiedDate"));
        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setProductGroupID(data.getString("ProductGroupID"));
        bean.setImage(data.getString("Image"));
        bean.setThumbnailImage(data.getString("ThumbnailImage"));
        bean.setProductDetails(data.getString("ProductDetails"));
        bean.setActive(data.getString("Active"));
        bean.setVariablePrice(data.getString("VariablePrice"));
        bean.setStockLevel(data.getString("StockLevel"));
        bean.setPricingType(data.getString("PricingType"));
        bean.setGroupID(data.getString("GroupID"));
        bean.setAvailableDate((Date) data.getObject("AvailableDate"));
        bean.setGSTExempt(data.getString("GSTExempt"));
        bean.setSupplierCompanyID(data.getString("SupplierCompanyID"));

        ProductGroup productgroup = new ProductGroup();
        productgroup.setProductGroupID(data.getString("ProductGroupID"));
        productgroup.setGroupName(data.getString("GroupName"));
        bean.setProductGroup(productgroup);

        return (bean);
    }

    public static ProductGroup getProductGroupFromTableData(TableData data) {
        ProductGroup bean = new ProductGroup();
        bean.setProductGroupID(data.getString("ProductGroupID"));
        bean.setGroupName(data.getString("GroupName"));
        bean.setDescription(data.getString("Description"));
        bean.setSortOrder(data.getString("SortOrder"));
        bean.setCreatedDate((Date) data.getObject("CreatedDate"));
        bean.setModifiedDate((Date) data.getObject("ModifiedDate"));
        bean.setCreatedBy(data.getString("CreatedBy"));
        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setGroupID(data.getString("GroupID"));
        bean.setActive(data.getString("Active"));
        return (bean);
    }

    public static OrderItem getOrderItemFromTableData(TableData data) {
        OrderItem bean = new OrderItem();
        bean.setOrderItemID(data.getString("OrderItemID"));
        bean.setName(data.getString("Name"));
        bean.setProductID(data.getData("ProductID"));
        bean.setProductGroupID(data.getString("ProductGroupID"));
        bean.setDescription(data.getString("Description"));
        bean.setExtraText(data.getString("ExtraText"));
        bean.setHasExtraText("Y".equals(data.getString("HasExtraText")));
        bean.setItemCost(Double.parseDouble(data.getString("ItemCost")));
        bean.setGST(Double.parseDouble(data.getString("GST")));
        bean.setCost(Double.parseDouble(data.getString("Cost")));
        bean.setOrderID(data.getString("OrderID"));
        bean.setQuantity(Double.parseDouble(data.getString("Quantity")));
        bean.setTotalCost(Double.parseDouble(data.getString("TotalCost")));
        bean.setInvitation(data.getString("Invitation"));
        bean.setPricingType(data.getString("PricingType"));
        bean.setProductNum(data.getString("ProductNum"));
        if (data.getString("SortNumber").length() != 0) {
            bean.setSortNumber(Integer.parseInt(data.getString("SortNumber")));
        }
        bean.setIncludePicture(data.getString("IncludePicture"));
        bean.setIncludeSummary(data.getString("IncludeSummary"));
        bean.setIncludePrice(data.getString("IncludePrice"));
        bean.setCategory(data.getString("Category"));
        if (data.isSet("OrderTemplateItemID")) {
            bean.setOrderTemplateItemID(data.getString("OrderTemplateItemID"));
        }
        bean.setItemLocked("Y".equals(data.getColumn("ItemLocked")));
        bean.setQuantityLocked("Y".equals(data.getColumn("QuantityLocked")));
        bean.setCostLocked("Y".equals(data.getColumn("CostLocked")));
        bean.setItemGroup(data.getColumn("ItemGroup"));
        bean.setItemGroupSort(data.getColumn("ItemGroupSort"));
        bean.setItemGroupLocked("Y".equals(data.getColumn("ItemGroupLocked")));
        bean.setItemGroupAdd("Y".equals(data.getColumn("ItemGroupAdd")));
        bean.setItemGroupTotal("Y".equals(data.getColumn("ItemGroupTotal")));
        if (data.isSet("MetaData")) {
            bean.setMetaData(data.getColumn("MetaData"));
        } else {
            bean.setMetaData("{}");
        }
        return (bean);
    }

    public static RowBean getTableDataFromOrderItem(OrderItem bean) {
        RowBean data = new RowBean();
        data.setTableSpec("OrderItems");

        setTableDataFromOrderItem(bean, data);

        return (data);
    }


    public static void setTableDataFromDocument(Document doc, TableData data) {

        data.setParameter("GroupID", doc.getGroupID());
        data.setParameter("DocumentID", doc.getDocumentID());
        if (StringUtils.isNotBlank(doc.getDocumentType())) {
            data.setParameter("DocumentType", doc.getDocumentType());
        }
        if (StringUtils.isNotBlank(doc.getDocumentSubType())) {
            data.setParameter("DocumentSubType", doc.getDocumentSubType());
        }
        data.setParameter("DocumentCaption", doc.getDocumentCaption());
        data.setParameter("Name", doc.getName());

        //FileName - ignored
        data.setParameter("Description", doc.getDescription());
        //FilePath - ignored
        //FileSize - ignored

        //Could change in future if a use case exists.
        //LocalFile - ignored
        //DocumentURL - ignore
        data.setParameter("DocumentStatus", doc.getDocumentStatus());
        data.setParameter("DocumentCategory", doc.getDocumentCategory());
        data.setParameter("ProductGroupID", doc.getProductGroupID());
        //CreatedDates, checked out dates, etc ignored.
    }

    public static Product getErrorProduct(String error) {
        Product bean = new Product();
        bean.setError(error);
        return (bean);
    }

    public static Order getErrorOrder(String error) {
        Order bean = new Order();
        bean.setError(error);
        return (bean);
    }

    public static Note getErrorNote(String error) {
        Note bean = new Note();
        bean.setError(error);
        return (bean);
    }

    public static RowBean getTableDataFromContact(Contact bean) {
        RowBean data = new RowBean();
        setTableDataFromContact(bean, data);
        return data;
    }

    public static void setTableDataFromContact(Contact bean, TableData data) {

        data.setTableSpec("Contacts");
        if (bean.getContactID() != null) {
            data.setColumn("ContactID", bean.getContactID());
        }
        if (bean.getTitle() != null) {
            data.setColumn("Title", bean.getTitle());
        }
        if (bean.getFirstName() != null) {
            data.setColumn("FirstName", bean.getFirstName());
        }
        if (bean.getLastName() != null) {
            data.setColumn("LastName", bean.getLastName());
        }
        if (bean.getSalutation() != null) {
            data.setColumn("Salutation", bean.getSalutation());
        }
        if (bean.getPosition() != null) {
            data.setColumn("Position", bean.getPosition());
        }
        if (bean.getCompany() != null) {
            data.setColumn("Company", bean.getCompany());
        }
        if (bean.getDepartment() != null) {
            data.setColumn("Department", bean.getDepartment());
        }
        if (bean.getPhysicalAddress() != null) {
            if (bean.getPhysicalAddress().getStreet() != null) {
                data.setColumn("Street", bean.getPhysicalAddress().getStreet());
            }
            if (bean.getPhysicalAddress().getStreet2() != null) {
                data.setColumn("Street2", bean.getPhysicalAddress().getStreet2());
            }
            if (bean.getPhysicalAddress().getCity() != null) {
                data.setColumn("City", bean.getPhysicalAddress().getCity());
            }
            if (bean.getPhysicalAddress().getState() != null) {
                data.setColumn("State", bean.getPhysicalAddress().getState());
            }
            if (bean.getPhysicalAddress().getPostcode() != null) {
                data.setColumn("Postcode", bean.getPhysicalAddress().getPostcode());
            }
            if (bean.getPhysicalAddress().getCountry() != null) {
                data.setColumn("Country", bean.getPhysicalAddress().getCountry());
            }
        }
        if (bean.getPhone() != null) {
            data.setColumn("Phone", bean.getPhone());
        }
        if (bean.getFax() != null) {
            data.setColumn("Fax", bean.getFax());
        }
        if (bean.getMobile() != null) {
            data.setColumn("Mobile", bean.getMobile());
        }
        if (bean.getWeb() != null) {
            data.setColumn("Web", bean.getWeb());
        }
        if (bean.getEmail() != null) {
            data.setColumn("Email", bean.getEmail());
        }
        if (bean.getRepUserID() != null) {
            data.setColumn("RepUserID", bean.getRepUserID());
        }
        if (bean.getMgrUserID() != null) {
            data.setColumn("MgrUserID", bean.getMgrUserID());
        }
        if (bean.getCreatedDate() != null) {
            data.put("CreatedDate", bean.getCreatedDate());
        }
        if (bean.getModifiedDate() != null) {
            data.put("ModifiedDate", bean.getModifiedDate());
        }
        if (bean.getModifiedBy() != null) {
            data.setColumn("ModifiedBy", bean.getModifiedBy());
        }
        if (bean.getCreatedBy() != null) {
            data.setColumn("CreatedBy", bean.getCreatedBy());
        }
        if (bean.getReferrerID() != null) {
            data.setColumn("ReferrerID", bean.getReferrerID());
        }
        if (bean.getCompanyID() != null) {
            data.setColumn("CompanyID", bean.getCompanyID());
        }
        if (bean.getFollowUpDate() != null) {
            data.put("FollowUpDate", bean.getFollowUpDate());
        }
        if (bean.getEmailType() != null) {
            data.setColumn("EmailType", bean.getEmailType());
        }
        if (bean.getBandwidth() != null) {
            data.setColumn("Bandwidth", bean.getBandwidth());
        }
        if (bean.getContact() != null) {
            data.setColumn("Contact", bean.getContact());
        }
        if (bean.getKeep() != null) {
            data.setColumn("Keep", bean.getKeep());
        }
        if (bean.getContactStatusID() != null) {
            data.setColumn("ContactStatusID", bean.getContactStatusID());
        }
        if (bean.getGKTitle() != null) {
            data.setColumn("GKTitle", bean.getGKTitle());
        }
        if (bean.getGKFirstName() != null) {
            data.setColumn("GKFirstName", bean.getGKFirstName());
        }
        if (bean.getGKLastName() != null) {
            data.setColumn("GKLastName", bean.getGKLastName());
        }
        if (bean.getQualifiedDate() != null) {
            data.put("QualifiedDate", bean.getQualifiedDate());
        }
        if (bean.getQualifiedBy() != null) {
            data.setColumn("QualifiedBy", bean.getQualifiedBy());
        }
        if (bean.getPostalAddress() != null) {
            if (bean.getPostalAddress().getStreet() != null) {
                data.setColumn("HomeStreet", bean.getPostalAddress().getStreet());
            }
            if (bean.getPostalAddress().getStreet2() != null) {
                data.setColumn("HomeStreet2", bean.getPostalAddress().getStreet2());
            }
            if (bean.getPostalAddress().getCity() != null) {
                data.setColumn("HomeCity", bean.getPostalAddress().getCity());
            }
            if (bean.getPostalAddress().getState() != null) {
                data.setColumn("HomeState", bean.getPostalAddress().getState());
            }
            if (bean.getPostalAddress().getPostcode() != null) {
                data.setColumn("HomePostcode", bean.getPostalAddress().getPostcode());
            }
            if (bean.getPostalAddress().getCountry() != null) {
                data.setColumn("HomeCountry", bean.getPostalAddress().getCountry());
            }
        }
        if (bean.getHomePhone() != null) {
            data.setColumn("HomePhone", bean.getHomePhone());
        }
        if (bean.getNotes() != null) {
            data.setColumn("Notes", bean.getNotes());
        }
        if (bean.getToBeQualified() != null) {
            data.setColumn("ToBeQualified", bean.getToBeQualified());
        }
        if (bean.getEmail2() != null) {
            data.setColumn("Email2", bean.getEmail2());
        }
        if (bean.getEmailStatus() != null) {
            data.setColumn("EmailStatus", bean.getEmailStatus());
        }
        if (bean.getGroupID() != null) {
            data.setColumn("GroupID", bean.getGroupID());
        }
        if (bean.getType() != null) {
            data.setColumn("Type", bean.getType());
        }
        if (bean.getSource() != null) {
            data.setColumn("Source", bean.getSource());
        }
        if (bean.getAccountNo() != null) {
            data.setColumn("AccountNo", bean.getAccountNo());
        }
        if (bean.getIndustry() != null) {
            data.setColumn("Industry", bean.getIndustry());
        }
        if (bean.getUsername() != null) {
            data.setColumn("Username", bean.getUsername());
        }
        if (bean.getPassword() != null) {
            data.setColumn("Password", bean.getPassword());
        }
        if (bean.getCreatedCampaignID() != null) {
            data.setColumn("CreatedCampaignID", bean.getCreatedCampaignID());
        }
    }

    public static RowBean getTableDataFromCompany(Company bean) {
        RowBean data = new RowBean();
        data.setTableSpec("Companies");
        if (bean.getCompanyID() != null) {
            data.setColumn("CompanyID", bean.getCompanyID());
        }
        if (bean.getCompany() != null) {
            data.setColumn("Company", bean.getCompany());
        }
        if (bean.getDepartment() != null) {
            data.setColumn("Department", bean.getDepartment());
        }
        if (bean.getPhysicalAddress().getStreet() != null) {
            data.setColumn("Street", bean.getPhysicalAddress().getStreet());
        }
        if (bean.getPhysicalAddress().getStreet2() != null) {
            data.setColumn("Street2", bean.getPhysicalAddress().getStreet2());
        }
        if (bean.getPhysicalAddress().getCity() != null) {
            data.setColumn("City", bean.getPhysicalAddress().getCity());
        }
        if (bean.getPhysicalAddress().getState() != null) {
            data.setColumn("State", bean.getPhysicalAddress().getState());
        }
        if (bean.getPhysicalAddress().getPostcode() != null) {
            data.setColumn("Postcode", bean.getPhysicalAddress().getPostcode());
        }
        if (bean.getPhysicalAddress().getCountry() != null) {
            data.setColumn("Country", bean.getPhysicalAddress().getCountry());
        }
        if (bean.getPhone() != null) {
            data.setColumn("Phone", bean.getPhone());
        }
        if (bean.getFax() != null) {
            data.setColumn("Fax", bean.getFax());
        }
        if (bean.getWeb() != null) {
            data.setColumn("Web", bean.getWeb());
        }
        if (bean.getEmail() != null) {
            data.setColumn("Email", bean.getEmail());
        }
        if (bean.getCreatedDate() != null) {
            data.put("CreatedDate", bean.getCreatedDate());
        }
        if (bean.getModifiedDate() != null) {
            data.put("ModifiedDate", bean.getModifiedDate());
        }
        if (bean.getModifiedBy() != null) {
            data.setColumn("ModifiedBy", bean.getModifiedBy());
        }
        if (bean.getRepUserID() != null) {
            data.setColumn("RepUserID", bean.getRepUserID());
        }
        if (bean.getMgrUserID() != null) {
            data.setColumn("MgrUserID", bean.getMgrUserID());
        }
        if (bean.getSource() != null) {
            data.setColumn("Source", bean.getSource());
        }
        if (bean.getCreatedBy() != null) {
            data.setColumn("CreatedBy", bean.getCreatedBy());
        }
        if (bean.getType() != null) {
            data.setColumn("Type", bean.getType());
        }
        if (bean.getIndustry() != null) {
            data.setColumn("Industry", bean.getIndustry());
        }
        if (bean.getQualifiedDate() != null) {
            data.put("QualifiedDate", bean.getQualifiedDate());
        }
        if (bean.getQualifiedBy() != null) {
            data.setColumn("QualifiedBy", bean.getQualifiedBy());
        }
        if (bean.getFollowUpDate() != null) {
            data.put("FollowUpDate", bean.getFollowUpDate());
        }
        if (bean.getPostalAddress().getStreet() != null) {
            data.setColumn("PostalStreet", bean.getPostalAddress().getStreet());
        }
        if (bean.getPostalAddress().getStreet2() != null) {
            data.setColumn("PostalStreet2", bean.getPostalAddress().getStreet2());
        }
        if (bean.getPostalAddress().getCity() != null) {
            data.setColumn("PostalCity", bean.getPostalAddress().getCity());
        }
        if (bean.getPostalAddress().getState() != null) {
            data.setColumn("PostalState", bean.getPostalAddress().getState());
        }
        if (bean.getPostalAddress().getPostcode() != null) {
            data
                    .setColumn("PostalPostcode", bean.getPostalAddress()
                            .getPostcode());
        }
        if (bean.getPostalAddress().getCountry() != null) {
            data.setColumn("PostalCountry", bean.getPostalAddress().getCountry());
        }
        if (bean.getPhone2() != null) {
            data.setColumn("Phone2", bean.getPhone2());
        }
        if (bean.getNotes() != null) {
            data.setColumn("Notes", bean.getNotes());
        }
        if (bean.getGroupID() != null) {
            data.setColumn("GroupID", bean.getGroupID());
        }
        if (bean.getCompanyStatusID() != null) {
            data.setColumn("CompanyStatusID", bean.getCompanyStatusID());
        }
        if (bean.getWholesaler() != null) {
            data.setColumn("Wholesaler", bean.getWholesaler());
        }
        if (bean.getAccountNo() != null) {
            data.setColumn("AccountNo", bean.getAccountNo());
        }
        if (bean.getLegalCompanyName() != null) {
            data.setColumn("LegalCompanyName", bean.getLegalCompanyName());
        }
        if (bean.getABN() != null) {
            data.setColumn("ABN", bean.getABN());
        }
        return (data);
    }

    public static RowBean getTableDataFromNote(Note bean) {
        RowBean data = new RowBean();
        data.setTableSpec("Notes");
        if (bean.getNoteID() != null) {
            data.setColumn("NoteID", bean.getNoteID());
        }
        if (bean.getRepUserID() != null) {
            data.setColumn("RepUserID", bean.getRepUserID());
        }
        if (bean.getNoteDate() != null) {
            data.put("NoteDate", bean.getNoteDate());
        }
        if (bean.getCreatedDate() != null) {
            data.put("CreatedDate", bean.getCreatedDate());
        }
        if (bean.getCreatedBy() != null) {
            data.setColumn("CreatedBy", bean.getCreatedBy());
        }
        if (bean.getModifiedDate() != null) {
            data.put("ModifiedDate", bean.getModifiedDate());
        }
        if (bean.getModifiedBy() != null) {
            data.setColumn("ModifiedBy", bean.getModifiedBy());
        }
        if (bean.getContactID() != null) {
            data.setColumn("ContactID", bean.getContactID());
        }
        if (bean.getCompanyID() != null) {
            data.setColumn("CompanyID", bean.getCompanyID());
        }
        if (bean.getProductGroupID() != null) {
            data.setColumn("ProductGroupID", bean.getProductGroupID());
        }
        if (bean.getOpportunityID() != null) {
            data.setColumn("OpportunityID", bean.getOpportunityID());
        }
        if (bean.getType() != null) {
            data.setColumn("Type", bean.getType());
        }
        if (bean.getCompleted() != null) {
            data.setColumn("Completed", bean.getCompleted());
        }
        if (bean.getCampaign() != null) {
            data.setColumn("Campaign", bean.getCampaign());
        }
        if (bean.getCampaignID() != null) {
            data.setColumn("CampaignID", bean.getCampaignID());
        }
        if (bean.getSubject() != null) {
            data.setColumn("Subject", bean.getSubject());
        }
        if (bean.getBody() != null) {
            data.setColumn("Body", bean.getBody());
        }
        if (bean.getDuration() != null) {
            data.setColumn("Duration", bean.getDuration());
        }
        if (bean.getNoteTime() != null) {
            data.put("NoteTime", bean.getNoteTime());
        }
        if (bean.getCallStatus() != null) {
            data.setColumn("CallStatus", bean.getCallStatus());
        }
        if (bean.getAppointmentStatus() != null) {
            data.setColumn("AppointmentStatus", bean.getAppointmentStatus());
        }
        if (bean.getGroupID() != null) {
            data.setColumn("GroupID", bean.getGroupID());
        }
        if (bean.getNoteCode() != null) {
            data.setColumn("NoteCode", bean.getNoteCode());
        }
        if (bean.getOrderID() != null) {
            data.setColumn("OrderID", bean.getOrderID());
        }
        if (bean.getVenue() != null) {
            data.setColumn("Venue", bean.getVenue());
        }
        if (bean.getURL() != null) {
            data.setColumn("URL", bean.getURL());
        }
        if (bean.getEndDate() != null) {
            data.put("EndDate", bean.getEndDate());
        }
        if (bean.getProcessStageID() != null) {
            data.setColumn("ProcessStageID", bean.getProcessStageID());
        }
        if (bean.getApproval() != null) {
            data.setColumn("Approval", bean.getApproval());
        }
        if (bean.getParentNoteID() != null) {
            data.setColumn("ParentNoteID", bean.getParentNoteID());
        }
        return (data);
    }

    public static RowBean getTableDataFromEmail(Note bean) {
        RowBean data = new RowBean();
        data.setTableSpec("Emails");
        if (bean.getNoteID() != null) {
            data.setColumn("EmailID", bean.getNoteID());
        }
        if (bean.getStatus() != null) {
            data.setColumn("Status", bean.getStatus());
        }
        if (bean.getTemplateName() != null) {
            data.setColumn("TemplateName", bean.getTemplateName());
        }
        if (bean.getSentDate() != null) {
            data.put("SentDate", bean.getSentDate());
        }
        if (bean.getTemplateUsed() != null) {
            data.setColumn("TemplateUsed", bean.getTemplateUsed());
        }
        if (bean.getCMSUsed() != null) {
            data.setColumn("CMSUsed", bean.getCMSUsed());
        }
        if (bean.getHTMLBody() != null) {
            data.setColumn("HTMLBody", bean.getHTMLBody());
        }
        if (bean.getEmailTo() != null) {
            data.setColumn("EmailTo", bean.getEmailTo());
        }
        if (bean.getEmailFrom() != null) {
            data.setColumn("EmailFrom", bean.getEmailFrom());
        }
        if (bean.getEmailCC() != null) {
            data.setColumn("EmailCC", bean.getEmailCC());
        }
        if (bean.getEmailBCC() != null) {
            data.setColumn("EmailBCC", bean.getEmailBCC());
        }
        if (bean.getMasterTemplateID() != null) {
            data.setColumn("MasterTemplateID", bean.getMasterTemplateID());
        }
        if (bean.getMessageID() != null) {
            data.setColumn("MessageID", bean.getMessageID());
        }
        return (data);
    }

    public static RowBean getTableDataFromOrder(Order bean) {
        RowBean data = new RowBean();
        data.setTableSpec("Orders");
        setTableDataFromOrder(bean, data);
        return data;
    }

    public static void setTableDataFromOrderItem(OrderItem bean, TableData data) {
        if (bean.getOrderID() != null) {
            data.setColumn("OrderID", bean.getOrderID());
        }
        if (bean.getOrderItemID() != null) {
            data.setColumn("OrderItemID", bean.getOrderItemID());
        }
        if (bean.getName() != null) {
            data.setColumn("Name", bean.getName());
        }
        if (bean.getProductID() != null) {
            data.setColumn("ProductID", bean.getProductID());
        }
        if (bean.getCopiedProductID() != null) {
            data.setColumn("CopiedProductID", bean.getCopiedProductID());
        }
        if (bean.getProductGroupID() != null) {
            data.setColumn("ProductGroupID", bean.getProductGroupID());
        }
        if (bean.getDescription() != null) {
            data.setColumn("Description", bean.getDescription());
        }
        data.put("Quantity", new Double(bean.getQuantity()));
        data.put("ItemCost", new Double(bean.getItemCost()));
        data.put("Cost", new Double(bean.getCost()));
        data.put("GST", new Double(bean.getGST()));
        data.put("TotalCost", new Double(bean.getTotalCost()));

        if (bean.getInvitation() != null) {
            data.setColumn("Invitation", bean.getInvitation());
        }
        if (bean.getPricingType() != null) {
            data.setColumn("PricingType", bean.getPricingType());
        }
        if (bean.getProductNum() != null) {
            data.setColumn("ProductNum", bean.getProductNum());
        }
        data.put("SortNumber", new Integer(bean.getSortNumber()));

        if (bean.getIncludePicture() != null) {
            data.setColumn("IncludePicture", bean.getIncludePicture());
        }
        if (bean.getIncludeSummary() != null) {
            data.setColumn("IncludeSummary", bean.getIncludeSummary());
        }
        if (bean.getIncludePrice() != null) {
            data.setColumn("IncludePrice", bean.getIncludePrice());
        }
        if (StringUtils.isNotBlank(bean.getOrderTemplateItemID())) {
            data.setColumn("OrderTemplateItemID", bean.getOrderTemplateItemID());
        }
        data.setColumn("ItemLocked", bean.isItemLocked() ? "Y" : "N");
        data.setColumn("QuantityLocked", bean.isQuantityLocked() ? "Y" : "N");
        data.setColumn("CostLocked", bean.isCostLocked() ? "Y" : "N");
        data.setColumn("HasExtraText", bean.isHasExtraText() ? "Y" : "N");
        data.setColumn("ExtraText", bean.getExtraText());
        data.setColumn("ItemGroupLocked", bean.isItemGroupLocked() ? "Y" : "N");
        data.setColumn("ItemGroupAdd", bean.isItemGroupAdd() ? "Y" : "N");
        data.setColumn("ItemGroupTotal", bean.isItemGroupTotal() ? "Y" : "N");

        data.setColumn("Category", bean.getCategory());
        data.setColumn("ItemGroup", bean.getItemGroup());
        data.setColumn("ItemGroupSort", String.valueOf(bean.getItemGroupSort()));

        if (bean.getMetaData() != null) {
            data.setColumn("MetaData", bean.getMetaData());
        }
    }

    public static void setTableDataFromOrder(Order bean, TableData data) {
        if (bean.getOrderID() != null) {
            data.setColumn("OrderID", bean.getOrderID());
        }
        if (bean.getName() != null) {
            data.setColumn("Name", bean.getName());
        }
        if (bean.getRepUserID() != null) {
            data.setColumn("RepUserID", bean.getRepUserID());
        }
        if (bean.getProductID() != null) {
            data.setColumn("ProductID", bean.getProductID());
        }
        if (bean.getRegionID() != null) {
            data.setColumn("RegionID", bean.getRegionID());
        }
        if (bean.getOrderProductName() != null) {
            data.setColumn("OrderProductName", bean.getOrderProductName());
        }
        data.put("Quantity", new Double(bean.getQuantity()));
        data.put("Cost", new Double(bean.getCost()));
        data.put("GST", new Double(bean.getGST()));
        if (bean.getTimeOfSale() != null) {
            data.setColumn("TimeOfSale", bean.getTimeOfSale());
        }
        if (bean.getCreatedDate() != null) {
            data.put("CreatedDate", bean.getCreatedDate());
        }
        if (bean.getModifiedDate() != null) {
            data.put("ModifiedDate", bean.getModifiedDate());
        }
        if (bean.getModifiedBy() != null) {
            data.setColumn("ModifiedBy", bean.getModifiedBy());
        }
        if (bean.getContactID() != null) {
            data.setColumn("ContactID", bean.getContactID());
        }
        if (bean.getCompanyID() != null) {
            data.setColumn("CompanyID", bean.getCompanyID());
        }
        if (bean.getInvitation() != null) {
            data.setColumn("Invitation", bean.getInvitation());
        }
        data.put("TotalCost", new Double(bean.getTotalCost()));
        if (bean.getDateOfSale() != null) {
            data.put("DateOfSale", bean.getDateOfSale());
        }
        if (bean.getPaymentMethod() != null) {
            data.setColumn("PaymentMethod", bean.getPaymentMethod());
        }
        if (bean.getContactPhone() != null) {
            data.setColumn("ContactPhone", bean.getContactPhone());
        }
        if (bean.getEmail() != null) {
            data.setColumn("Email", bean.getEmail());
        }
        if (bean.getTimeOfCall() != null) {
            data.setColumn("TimeOfCall", bean.getTimeOfCall());
        }
        if (bean.getCreditName() != null) {
            data.setColumn("CreditName", bean.getCreditName());
        }
        if (bean.getCreditType() != null) {
            data.setColumn("CreditType", bean.getCreditType());
        }
        if (bean.getCreditExpire() != null) {
            data.setColumn("CreditExpire", bean.getCreditExpire());
        }
        if (bean.getOrderNum() != null) {
            data.setColumn("OrderNum", bean.getOrderNum());
        }
        if (bean.getCreditNum() != null) {
            data.setColumn("CreditNum", bean.getCreditNum());
        }
        if (bean.getOrderStatusID() != null) {
            data.setColumn("OrderStatusID", bean.getOrderStatusID());
        }
        if (bean.getWebSale() != null) {
            data.setColumn("WebSale", bean.getWebSale());
        }
        if (bean.getCreatedBy() != null) {
            data.setColumn("CreatedBy", bean.getCreatedBy());
        }
        if (bean.getContactName() != null) {
            data.setColumn("ContactName", bean.getContactName());
        }
        if (bean.getCompanyName() != null) {
            data.setColumn("CompanyName", bean.getCompanyName());
        }
        if (bean.getDeliveryAddress() != null) {
            if (bean.getDeliveryAddress().getStreet() != null) {
                data.setColumn("Street", bean.getDeliveryAddress().getStreet());
            }
            if (bean.getDeliveryAddress().getStreet2() != null) {
                data.setColumn("Street2", bean.getDeliveryAddress().getStreet2());
            }
            if (bean.getDeliveryAddress().getCity() != null) {
                data.setColumn("City", bean.getDeliveryAddress().getCity());
            }
            if (bean.getDeliveryAddress().getPostcode() != null) {
                data.setColumn("Postcode", bean.getDeliveryAddress().getPostcode());
            }
            if (bean.getDeliveryAddress().getState() != null) {
                data.setColumn("State", bean.getDeliveryAddress().getState());
            }
            if (bean.getDeliveryAddress().getCountry() != null) {
                data.setColumn("Country", bean.getDeliveryAddress().getCountry());
            }
        }
        if (bean.getOpportunityID() != null) {
            data.setColumn("OpportunityID", bean.getOpportunityID());
        }
        if (bean.getCampaignID() != null) {
            data.setColumn("CampaignID", bean.getCampaignID());
        }
        if (bean.getNotes() != null) {
            data.setColumn("Notes", bean.getNotes());
        }
        if (bean.getGroupID() != null) {
            data.setColumn("GroupID", bean.getGroupID());
        }
        if (bean.getDiscountFloat() != 0) {
            data.put("Discount", new Float(bean.getDiscountFloat()));
        } else if (bean.getDiscount() != 0) {
            data.put("Discount", new Float(bean.getDiscount()));
        }
        if (bean.getDiscountAmount() != 0) {
            data.put("DiscountAmount", new Double(bean.getDiscountAmount()));
        }
        if (bean.getProcessStageID() != null) {
            data.setColumn("ProcessStageID", bean.getProcessStageID());
        }
        if (bean.getApproval() != null) {
            data.setColumn("Approval", bean.getApproval());
        }
        if (bean.getHideTotals() != null) {
            data.setColumn("HideTotals", bean.getHideTotals());
        }
        if (bean.getDocumentUrl() != null) {
            data.setColumn("DocumentUrl", bean.getDocumentUrl());
        }
        if (bean.getProbability() != 0) {
            data.put("Probability", new Integer(bean.getProbability()));
        }
        if (bean.getOutcomeDate() != null) {
            data.put("OutcomeDate", bean.getOutcomeDate());
        }
        if (bean.getPONumber() != null) {
            data.setColumn("PONumber", bean.getPONumber());
        }
        if (bean.getQuoteViewID() != null) {
            data.setColumn("QuoteViewID", bean.getQuoteViewID());
        }
        if (bean.getOrderTemplateID() != null) {
            //NB, ignored if inserted into Orders table.
            data.setColumn("OrderTemplateID", bean.getOrderTemplateID());
        }
    }

    public static RowBean getTableDataFromProduct(Product bean) {
        RowBean data = new RowBean();
        data.setTableSpec("Products");
        if (bean.getProductID() != null) {
            data.setColumn("ProductID", bean.getProductID());
        }
        if (bean.getName() != null) {
            data.setColumn("Name", bean.getName());
        }
        if (bean.getProductNum() != null) {
            data.setColumn("ProductNum", bean.getProductNum());
        }
        if (bean.getDescription() != null) {
            data.setColumn("Description", bean.getDescription());
        }
        if (bean.getSummary() != null) {
            data.setColumn("Summary", bean.getSummary());
        }
        if (bean.getCategory() != null) {
            data.setColumn("Category", bean.getCategory());
        }
        if (bean.getBaseCost() != 0) {
            data.put("BaseCost", new Double(bean.getBaseCost()));
        }
        if (bean.getMargin() != 0) {
            data.put("Margin", new Double(bean.getMargin()));
            ;
        }
        data.put("Cost", new Double(bean.getCost()));
        data.put("TotalCost", new Double(bean.getTotalCost()));
        data.put("GST", new Double(bean.getGST()));

        if (bean.getCreatedDate() != null) {
            data.put("CreatedDate", bean.getCreatedDate());
        }
        if (bean.getCreatedBy() != null) {
            data.setColumn("CreatedBy", bean.getCreatedBy());
        }
        if (bean.getModifiedDate() != null) {
            data.put("ModifiedDate", bean.getModifiedDate());
        }
        if (bean.getModifiedBy() != null) {
            data.setColumn("ModifiedBy", bean.getModifiedBy());
        }
        if (bean.getProductGroupID() != null) {
            data.setColumn("ProductGroupID", bean.getProductGroupID());
        }
        if (bean.getImage() != null) {
            data.setColumn("Image", bean.getImage());
        }
        if (bean.getThumbnailImage() != null) {
            data.setColumn("ThumbnailImage", bean.getThumbnailImage());
        }
        if (bean.getProductDetails() != null) {
            data.setColumn("ProductDetails", bean.getProductDetails());
        }
        if (bean.getActive() != null) {
            data.setColumn("Active", bean.getActive());
        }
        if (bean.getVariablePrice() != null) {
            data.setColumn("VariablePrice", bean.getVariablePrice());
        }
        if (bean.getStockLevel() != null) {
            data.setColumn("StockLevel", bean.getStockLevel());
        }
        if (bean.getPricingType() != null) {
            data.setColumn("PricingType", bean.getPricingType());
        }
        if (bean.getGroupID() != null) {
            data.setColumn("GroupID", bean.getGroupID());
        }
        if (bean.getAvailableDate() != null) {
            data.put("AvailableDate", bean.getAvailableDate());
        }
        if (bean.getGSTExempt() != null) {
            data.setColumn("GSTExempt", bean.getGSTExempt());
        }
        if (bean.getSupplierCompanyID() != null) {
            data.setColumn("SupplierCompanyID", bean.getSupplierCompanyID());
        }

        return (data);
    }

    public static RowBean getTableDataFromContactAnswer(ContactAnswer bean) {
        RowBean data = new RowBean();
        data.setTableSpec("answers/ContactAnswers");
        if (bean.getAnswerID() != null) {
            data.setColumn("AnswerID", bean.getAnswerID());
        }
        if (bean.getContactID() != null) {
            data.setColumn("ContactID", bean.getContactID());
        }
        if (bean.getAnswer() != null) {
            data.setColumn("Answer", bean.getAnswer());
        }
        if (bean.getAnswerDate() != null) {
            data.put("AnswerDate", bean.getAnswerDate());
        }
        if (bean.getAnswerNumber() != null) {
            data.setColumn("AnswerNumber", bean.getAnswerNumber());
        }
        if (bean.getAnswerText() != null) {
            data.setColumn("AnswerText", bean.getAnswerText());
        }
        if (bean.getQuantifier() != null) {
            data.setColumn("Quantifier", bean.getQuantifier());
        }
        if (bean.getModifiedDate() != null) {
            data.put("ModifiedDate", bean.getModifiedDate());
        }
        if (bean.getModifiedBy() != null) {
            data.setColumn("ModifiedBy", bean.getModifiedBy());
        }
        return (data);
    }

    public static RowBean getTableDataFromCompanyAnswer(CompanyAnswer bean) {
        RowBean data = new RowBean();
        data.setTableSpec("answers/CompanyAnswers");
        if (bean.getAnswerID() != null) {
            data.setColumn("AnswerID", bean.getAnswerID());
        }
        if (bean.getCompanyID() != null) {
            data.setColumn("CompanyID", bean.getCompanyID());
        }
        if (bean.getAnswer() != null) {
            data.setColumn("Answer", bean.getAnswer());
        }
        if (bean.getAnswerDate() != null) {
            data.put("AnswerDate", bean.getAnswerDate());
        }
        if (bean.getAnswerNumber() != null) {
            data.setColumn("AnswerNumber", bean.getAnswerNumber());
        }
        if (bean.getAnswerText() != null) {
            data.setColumn("AnswerText", bean.getAnswerText());
        }
        if (bean.getQuantifier() != null) {
            data.setColumn("Quantifier", bean.getQuantifier());
        }
        if (bean.getModifiedDate() != null) {
            data.put("ModifiedDate", bean.getModifiedDate());
        }
        if (bean.getModifiedBy() != null) {
            data.setColumn("ModifiedBy", bean.getModifiedBy());
        }
        return (data);
    }

    public static Note getNoteFromTableData(TableData data) {
        Note bean = new Note();
        bean.setNoteID(data.getString("NoteID"));
        bean.setRepUserID(data.getString("RepUserID"));
        bean.setStatus(data.getString("Status"));
        bean.setNoteDate((Date) data.getObject("NoteDate"));
        bean.setCreatedDate((Date) data.getObject("CreatedDate"));
        bean.setCreatedBy(data.getString("CreatedBy"));
        bean.setModifiedDate((Date) data.getObject("ModifiedDate"));
        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setContactID(data.getString("ContactID"));
        bean.setCompanyID(data.getString("CompanyID"));
        bean.setProductGroupID(data.getString("ProductGroupID"));
        bean.setOpportunityID(data.getString("OpportunityID"));
        bean.setTemplateName(data.getString("TemplateName"));
        bean.setType(data.getString("Type"));
        bean.setCompleted(data.getString("Completed"));
        bean.setSentDate((Date) data.getObject("SentDate"));
        bean.setTemplateUsed(data.getString("TemplateUsed"));
        bean.setCMSUsed(data.getString("CMSUsed"));
        bean.setCampaign(data.getString("Campaign"));
        bean.setCampaignID(data.getString("CampaignID"));
        bean.setSubject(data.getString("Subject"));
        bean.setBody(data.getString("Body"));
        bean.setHTMLBody(data.getString("HTMLBody"));
        bean.setEmailTo(data.getString("EmailTo"));
        bean.setEmailFrom(data.getString("EmailFrom"));
        bean.setEmailCC(data.getString("EmailCC"));
        bean.setEmailBCC(data.getString("EmailBCC"));
        bean.setDuration(data.getString("Duration"));
        bean.setNoteTime((Date) data.getObject("NoteTime"));
        bean.setCallStatus(data.getString("CallStatus"));
        bean.setAppointmentStatus(data.getString("AppointmentStatus"));
        bean.setGroupID(data.getString("GroupID"));
        bean.setNoteCode(data.getString("NoteCode"));
        bean.setOrderID(data.getString("OrderID"));
        bean.setVenue(data.getString("Venue"));
        bean.setURL(data.getString("URL"));
        bean.setMasterTemplateID(data.getString("MasterTemplateID"));
        bean.setMessageID(data.getString("MessageID"));
        bean.setEndDate((Date) data.getObject("EndDate"));
        bean.setProcessStageID(data.getString("ProcessStageID"));
        bean.setApproval(data.getString("Approval"));
        bean.setParentNoteID(data.getString("ParentNoteID"));
        bean.setError(data.getError());

        bean.setCreatedByName(getPersonName(data.getString("OwnFirstName"), data
                .getString("OwnLastName")));
        bean.setModifiedByName(getPersonName(data.getString("ModFirstName"), data
                .getString("ModLastName")));

        return (bean);
    }

    public static IDList getIDList(String[] ids, String error) {
        IDList bean = new IDList();
        bean.setIDs(ids);
        bean.setError(error);
        return (bean);
    }

    public static IDList getErrorIDList(String error) {
        IDList bean = new IDList();
        bean.setIDs(new String[0]);
        bean.setError(error);
        return (bean);
    }

    public static CompanyQuestion getCompanyQuestionAnswerFromTableData(
            TableData data) {
        CompanyQuestion bean = new CompanyQuestion();
        setQuestionFromTableData(data, bean);
        bean.setAnswer(getCompanyAnswerFromTableData(data));
        return (bean);
    }

    public static CompanyAnswer getCompanyAnswerFromTableData(TableData data) {
        CompanyAnswer bean = new CompanyAnswer();
        setAnswerFromTableData(data, bean);
        bean.setCompanyID(data.getString("CompanyID"));
        return (bean);
    }

    public static ContactQuestion getContactQuestionAnswerFromTableData(
            TableData data) {
        ContactQuestion bean = new ContactQuestion();
        setQuestionFromTableData(data, bean);
        bean.setAnswer(getContactAnswerFromTableData(data));
        return (bean);
    }

    public static OpportunityQuestion getOpportunityQuestionAnswerFromTableData(
            GenRow data) {
        OpportunityQuestion bean = new OpportunityQuestion();
        setQuestionFromTableData(data, bean);
        bean.setAnswer(getOpportunityAnswerFromTableData(data));
        return (bean);
    }

    public static OrderAnswer getOrderAnswerFromTableData(TableData data) {
        OrderAnswer bean = new OrderAnswer();
        setAnswerFromTableData(data, bean);
        bean.setOrderID(data.getString("OrderID"));
        return (bean);
    }

    public static OrderQuestion getOrderQuestionAnswerFromTableData(TableData data) {
        OrderQuestion bean = new OrderQuestion();
        setQuestionFromTableData(data, bean);
        bean.setAnswer(getOrderAnswerFromTableData(data));
        return (bean);
    }

    public static ContactAnswer getContactAnswerFromTableData(TableData data) {
        ContactAnswer bean = new ContactAnswer();
        setAnswerFromTableData(data, bean);
        bean.setContactID(data.getString("ContactID"));
        return (bean);
    }

    public static OpportunityAnswer getOpportunityAnswerFromTableData(TableData data) {
        OpportunityAnswer bean = new OpportunityAnswer();
        setAnswerFromTableData(data, bean);
        bean.setOpportunityID(data.getString("OpportunityID"));
        return (bean);
    }

    protected static void setAnswerFromTableData(TableData data, Answer bean) {
        bean.setAnswerID(data.getString("AnswerID"));
        bean.setAnswer(data.getString("Answer"));
        Object o = data.getObject("AnswerDate");
        if (o instanceof Date) {
            bean.setAnswerDate((Date) o);
        }
        bean.setAnswerNumber(data.getString("AnswerNumber"));
        bean.setAnswerText(data.getString("AnswerText"));
        bean.setQuantifier(data.getString("Quantifier"));
        // bean.setCreatedDate((Date)data.getObject("CreatedDate"));
        // bean.setCreatedBy(data.getString("CreatedBy"));
        o = data.getObject("ModifiedDate");
        if (o instanceof Date) {
            bean.setModifiedDate((Date) o);
        }

        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setModifiedByName(getPersonName(data.getString("ModFirstName"), data
                .getString("ModLastName")));

    }

    protected static void setQuestionFromTableData(TableData data, Question bean) {
        bean.setQuestionID(data.getString("QuestionID"));
        bean.setLabel(data.getString("Label"));
        bean.setExportLabel(data.getString("ExportLabel"));
        bean.setInputType(data.getString("InputType"));
        bean.setValueList(data.getString("ValueList"));
        bean.setParentQuestionID(data.getString("ParentQuestionID"));
        bean.setCreatedDate((Date) data.getObject("CreatedDate"));
        bean.setCreatedBy(data.getString("CreatedBy"));
        bean.setModifiedDate((Date) data.getObject("ModifiedDate"));
        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setDescription(data.getString("Description"));
        bean.setCalculationType(data.getString("CalculationType"));
        bean.setRelatedQuestionID(data.getString("RelatedQuestionID"));
        bean.setCalculationQuestionID(data.getString("CalculationQuestionID"));
        bean.setCalculationPeriod(data.getString("CalculationPeriod"));
        bean.setProductGroupID(data.getString("ProductGroupID"));
        bean.setGroupID(data.getString("GroupID"));

        bean.setSurveyQuestionID(data.getString("SurveyQuestionID"));
        bean.setSortOrder(data.getString("SortOrder"));
        bean.setIndentLevel(data.getString("IndentLevel"));
        bean.setDivider(data.getString("Divider"));
        bean.setQuantifier(data.getString("Quantifier"));
        bean.setDisabled(data.getData("Disabled"));
        bean.setMandatory(data.getString("Mandatory"));
    }

    public static ContactProfile getContactProfileFromTableData(TableData data) {
        ContactProfile bean = new ContactProfile();
        setProfileFromTableData(data, bean);
        return (bean);
    }


    public static OpportunityProfile getOpportunityProfileFromTableData(GenRow data) {
        OpportunityProfile bean = new OpportunityProfile();
        setProfileFromTableData(data, bean);
        return bean;
    }

    public static CompanyProfile getCompanyProfileFromTableData(TableData data) {
        CompanyProfile bean = new CompanyProfile();
        setProfileFromTableData(data, bean);
        return (bean);
    }

    protected static void setProfileFromTableData(TableData data, Profile bean) {
        bean.setSurveyID(data.getString("SurveyID"));
        bean.setName(data.getString("Name"));
        bean.setDescription(data.getString("Description"));
        bean.setPreamble(data.getString("Preamble"));
        bean.setDisclaimer(data.getString("Disclaimer"));
        bean.setAllOpportunities(data.getString("AllOpportunities"));
        bean.setCompanyType(data.getString("CompanyType"));
        bean.setContactType(data.getString("ContactType"));
        bean.setDisabled(data.getString("Disabled"));
        bean.setCreatedDate((Date) data.getObject("CreatedDate"));
        bean.setCreatedBy(data.getString("CreatedBy"));
        bean.setModifiedDate((Date) data.getObject("ModifiedDate"));
        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setDisplayInCompanyView(data.getString("DisplayInCompanyView"));
        bean.setDisplayInContactView(data.getString("DisplayInContactView"));
        bean.setDisplayInOpportunityView(data
                .getString("DisplayInOpportunityView"));
        bean
                .setDisplayInCompanySummary(data
                        .getString("DisplayInCompanySummary"));
        bean.setEditInContactDetailedList(data
                .getString("EditInContactDetailedList"));
        bean.setProductProfile(data.getString("ProductProfile"));
        bean.setOrderProfile(data.getString("OrderProfile"));
        bean.setCompetitorProfile(data.getString("CompetitorProfile"));
        bean.setNoteType(data.getString("NoteType"));
        bean.setNoteProfile(data.getString("NoteProfile"));
        bean.setItemListProfile(data.getString("ItemListProfile"));
        bean.setCheckListProfile(data.getString("CheckListProfile"));
        bean.setRelatedSurveyID(data.getString("RelatedSurveyID"));
        bean.setGroupID(data.getString("GroupID"));
        bean.setCreatedByName(getPersonName(data.getString("OwnFirstName"), data
                .getString("OwnLastName")));
        bean.setModifiedByName(getPersonName(data.getString("ModFirstName"), data
                .getString("ModLastName")));
    }

    public static PersonName getPersonName(String firstname, String lastname) {
        PersonName bean = new PersonName();
        bean.setFirstName(firstname);
        bean.setLastName(lastname);
        return (bean);
    }

    public static ContactProfiles getContactProfiles(ContactProfile[] profiles,
                                                     String contactid, String error) {
        ContactProfiles bean = new ContactProfiles();
        bean.setProfiles(profiles);
        bean.setContactID(contactid);
        bean.setError(error);
        return (bean);
    }

    public static ContactProfiles getErrorContactProfiles(String error) {
        ContactProfiles bean = new ContactProfiles();
        bean.setError(error);
        return (bean);
    }

    public static CompanyProfiles getCompanyProfiles(CompanyProfile[] profiles,
                                                     String companyid, String error) {
        CompanyProfiles bean = new CompanyProfiles();
        bean.setProfiles(profiles);
        bean.setCompanyID(companyid);
        bean.setError(error);
        return (bean);
    }

    public static CompanyProfiles getErrorCompanyProfiles(String error) {
        CompanyProfiles bean = new CompanyProfiles();
        bean.setError(error);
        return (bean);
    }

    public static Result getResultFromTableData(TableData data) {
        Result bean = new Result();
        bean.setAffectedRecordsCount(data.getUpdatedCount());
        bean.setError(data.getError());
        return (bean);
    }

    public static Result getErrorResult(String error) {
        Result bean = new Result();
        bean.setError(error);
        return (bean);
    }

    public static void setTableDataSetFromSearchRequest(TableData data,
                                                        SearchRequest search) {
        SearchParameter[] parameters = search.getParameters();
        for (int i = 0; i < parameters.length; i++) {
            data.setColumn(parameters[i].getName(), parameters[i].getValue());
        }
    }

    public static Status getStatusFromTableData(TableData data) {
        Status status = new Status();
        status.setStatusID(data.getString("StatusID"));
        status.setStatus(data.getString("Status"));
        status.setValue(data.getString("StatusValue"));
        status.setSortNumber(data.getString("SortNumber"));
        status.setListName(data.getString("ListName"));
        status.setInactive(data.getString("Inactive"));
        return (status);
    }

    public static ItemList getItemList(ValueList vl) {
        if (vl == null) {
            return null;
        }

        ItemList bean = new ItemList();
        bean.setDefaultValue(vl.getDefaultValue());
        int max = vl.size() - 1;
        if (max == -1) {
            return bean;
        }
        ListItem[] items = new ListItem[vl.size()];
        bean.setListItems(items);
        for (int i = 0; ; i++) {
            ListItem item = new ListItem();
            items[i] = item;
            item.setItemLabel(vl.getLabel(i));
            item.setItemValue(vl.getValue(i));
            item.setSortNumber(String.valueOf(i));
            item.setDisplayField(vl.getDisplayField(i));
            item.setDisplayName(vl.getDisplayName(i));
            if (i == max) {
                break;
            }
        }
        return (bean);
    }

    /**
     * Use ValueList for external interfaces as is is not tied to session.
     *
     * @deprecated
     */
    public static ItemList getItemList(SingleItemList sil) {
        ItemList bean = new ItemList();
        bean.setDefaultValue(sil.getDefaultValue());
        int max = sil.size() - 1;
        if (max == -1) {
            return bean;
        }
        ListItem[] items = new ListItem[sil.size()];
        bean.setListItems(items);
        for (int i = 0; ; i++) {
            ListItem item = new ListItem();
            items[i] = item;
            item.setItemLabel(sil.getName(i));
            item.setItemValue(sil.getValue(i));
            item.setSortNumber(String.valueOf(i));
            item.setDisplayField(sil.getDisplayField(i));
            item.setDisplayName(sil.getDisplayName(i));
            if (i == max) {
                break;
            }
        }
        return (bean);
    }

    public static ItemList getItemListFromTableData(TableData data) {
        ItemList bean = new ItemList();
        bean.setListID(data.getString("ListID"));
        bean.setParentListID(data.getString("ParentListID"));
        bean.setListName(data.getString("ListName"));
        bean.setGroupLevel(data.getString("GroupLevel"));
        bean.setListType(data.getString("ListType"));
        bean.setDefaultValue(data.getString("DefaultValue"));
        bean.setDisplayField(data.getString("DisplayField"));
        bean.setDisplayName(data.getString("DisplayName"));
        bean.setError(data.getError());
        return (bean);
    }

    public static ItemList getErrorItemList(String error) {
        ItemList bean = new ItemList();
        bean.setError(error);
        return (bean);
    }

    public static ListItem getListItemFromTableData(TableData data) {
        ListItem bean = new ListItem();
        bean.setItemID(data.getString("ItemID"));
        bean.setListID(data.getString("ListID"));
        bean.setItemLabel(data.getString("ItemLabel"));
        bean.setItemValue(data.getString("ItemValue"));
        bean.setSortNumber(data.getString("SortNumber"));
        bean.setQuantifier(data.getString("Quantifier"));
        bean.setDisplayField(data.getString("DisplayField"));
        bean.setDisplayName(data.getString("DisplayName"));
        return (bean);
    }

    public static Document getDocumentFromTableData(TableData data) {
        Document bean = new Document();
        bean.setDocumentID(data.getString("DocumentID"));
        bean.setDocumentType(data.getString("DocumentType"));
        bean.setDocumentSubType(data.getString("DocumentSubType"));
        bean.setDocumentCaption(data.getString("DocumentCaption"));
        bean.setFileName(data.getString("FileName"));
        bean.setDescription(data.getString("Description"));
        bean.setFilePath(data.getString("FilePath"));
        bean.setFileSize(data.getString("FileSize"));
        bean.setLocalFile(data.getString("LocalFile"));
        bean.setDocumentCategory(data.getString("DocumentCategory"));
        bean.setProductGroupID(data.getString("ProductGroupID"));
        bean.setCreatedDate((Date) data.getObject("CreatedDate"));
        bean.setCreatedBy(data.getString("CreatedBy"));
        bean.setModifiedDate((Date) data.getObject("ModifiedDate"));
        bean.setModifiedBy(data.getString("ModifiedBy"));
        bean.setGroupID(data.getString("GroupID"));
        bean.setCheckedOutDate((Date) data.getObject("CheckedOutDate"));
        bean.setCheckedOutBy(data.getString("CheckedOutBy"));
        bean.setDocumentStatus(data.getString("DocumentStatus"));
        bean.setDocumentURL(data.getString("DocumentURL"));
        bean.setName(data.getString("Name"));

        bean.setCreatedByName(getPersonName(data.getString("OwnFirstName"), data
                .getString("OwnLastName")));
        bean.setModifiedByName(getPersonName(data.getString("ModFirstName"), data
                .getString("ModLastName")));

        return (bean);
    }

    public static Document getErrorDocument(String error) {
        Document bean = new Document();
        bean.setError(error);
        return (bean);
    }

    public static IDList getIDList(HttpServletRequest request, String sessionkey, SearchRequest search,
                                   String viewspec, String idname, String security) {
        if (sessionkey != null && sessionkey.length() != 0) {
            UserBean cuser = AxisUtil.getUserBean(sessionkey);
            if (cuser != null && cuser.getString("UserID").length() != 0) {
                if (search != null && search.getParameters() != null
                        && search.getParameters().length != 0) {
                    GenRow bean = new GenRow();
                    bean.setRequest(request);
                    bean.setViewSpec(viewspec);
                    BeanFactory.setTableDataSetFromSearchRequest(bean, search);
                    bean.put(ActionBean.CURRENTUSERID, cuser.getString("UserID"));
                    if (security != null) {
                        cuser.setConstraint(bean, security);
                    }
                    //bean.generateSQLStatement();
                    try {
                        bean.getResults(true);
                    } catch (Exception e) {
                        return (BeanFactory.getErrorIDList(e.getMessage()));
                    }

                    String[] ids = new String[bean.getSize()];
                    for (int i = 0; i < ids.length; i++) {
                        bean.getNext();
                        ids[i] = bean.getString(idname);
                    }
                    bean.close();

                    IDList list = BeanFactory.getIDList(ids, bean.getError());

                    if ("true".equals(bean.getString("-debug"))) {
                        list.setDebug(bean.getStatement());
                    }

                    return (list);

                }
                return (BeanFactory.getErrorIDList(AxisUtil.ID_ERROR));
            }
        }
        return (BeanFactory.getErrorIDList(AxisUtil.SESSION_ERROR));
    }

    public static FieldSet getFieldSetFromTableData(TableData data) {
        FieldSet set = new FieldSet();
        Object[] keys = data.getKeys();
        Field[] fields = new Field[keys.length];
        String keystring = null;
        Object obj = null;
        for (int i = 0; i < fields.length; i++) {
            keystring = keys[i].toString();
            fields[i] = new Field();
            fields[i].setName(keystring);
            fields[i].setStringValue(data.getString(keystring));
            obj = data.getObject(keystring);
            if (obj instanceof Date) {
                fields[i].setDateValue((Date) obj);
            }
        }
        set.setError(data.getError());
        return (set);
    }

    public static GenRow getTableDataFieldSet(FieldSet set) {
        Field[] fields = set.getFields();
        GenRow data = new GenRow();
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].getDateValue() != null) {
                data.put(fields[i].getName(), fields[i].getDateValue());
            } else if (fields[i].getStringValue() != null) {
                data.setParameter(fields[i].getName(), fields[i].getStringValue());
            }
        }
        return (data);
    }

    public static void setTableDataFromOrderContact(OrderContact bean, GenRow data) {

        if (bean.getOrderContactID() != null) {
            data.setColumn("OrderContactID", bean.getOrderContactID());
        }
        if (bean.getOrderID() != null) {
            data.setColumn("OrderID", bean.getOrderID());
        }
        if (bean.getContactID() != null) {
            data.setColumn("ContactID", bean.getContactID());
        }
        if (bean.getRole() != null) {
            data.setColumn("Role", bean.getRole());
        }
        if (bean.getRecipientType() != null) {
            data.setColumn("RecipientType", bean.getRecipientType());
        }

    }

    public static OrderProfile getOrderProfileFromTableData(GenRow data) {
        OrderProfile bean = new OrderProfile();
        setProfileFromTableData(data, bean);
        return (bean);
    }

    public static HouseLandPackage external(com.sok.runway.crm.cms.properties.HouseLandPackage data) {
        HouseLandPackage e = new HouseLandPackage();
        e.setPackageID(data.getData("ProductID"));
        e.setName(data.getData("Name"));
        e.setProductNum(data.getData("ProductNum"));
        e.setDescription(data.getData("Description"));
        e.setPublishDescription(evaluateHeadline(data, data.getData("PublishDescription")));
        e.setPublishHeadline(evaluateHeadline(data, data.getData("Summary")));
        e.setAddress(BeanFactory.getAddressFromTableData(data, ""));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getData("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getData("OwnFirstName"), data.getData("OwnLastName")));
        e.setModifiedBy(data.getData("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getData("ModFirstName"), data.getData("ModLastName")));
        e.setActive(data.getData("Active"));
        e.setError(data.getError());

        String displayPriceType = data.getString("DisplayPriceType");
        if (StringUtils.isBlank(displayPriceType)) {
            displayPriceType = getDisplayPriceType(data.getData("ProductID"));
        }

        if (data.isSet("DripResult")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("DripResult")));
        } else if (data.isSet("TotalCost")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("TotalCost")));
        }

        if (StringUtils.isBlank(e.getDisplayPrice()) && data.isSet("DisplayPrice")) {
            e.setDisplayPrice(data.getString("DisplayPrice"));
        }

        e.setPreAvailableDate(data.getDate("PreAvailableDate"));
        e.setAvailableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setGroups(getProductGroups(data.getConnection(), e.getPackageID()));

        e.setMarketingStatus((data.isSet("MarketingStatus") ? data.getString("MarketingStatus") : data.getString("LotMarketingStatus")));

        e.setPublishing(loadPublishing(data.getData("ProductID")));

        if (StringUtils.isNotBlank(data.getData("Image"))) {
            e.setImage(loadDocument(data.getData("Image")));
        }

        if (data.isSet("LocationID")) {
            e.setLocation(getLocations(data.getConnection(), data.getData("LocationID"), BeanRepresentation.Simple));
        } else {
            LOGGER.debug("Location not set : packageID=[{}]", data.getData("ProductID"));
        }

        if (data.isSet("RegionID")) {
            e.setRegion(getLocations(data.getConnection(), data.getData("RegionID"), BeanRepresentation.Simple));
        } else {
            LOGGER.debug("Region not set : packageID=[{}]", data.getData("ProductID"));
        }

        GenRow rep = new GenRow();
        rep.setViewSpec("ProductSecurityGroupView");
        rep.setConnection(data.getConnection());
        rep.setParameter("ProductID", e.getPackageID());
        rep.setParameter("RepUserID", "!EMPTY");
        if (rep.isSet("ProductID")) {
            rep.doAction(GenerationKeys.SELECTFIRST);
        }
        e.setDetailPdfUrl(getDetailPdfUrl(e.getLocation() != null ?  e.getLocation().getLocationID() : "", e.getPackageID(), e.getModifiedDate(),
                rep.getData("RepUserID")));

        Lot l = new Lot();
        l.setLotID(data.getData("LotProductID"));
        l.setName(data.getData("LotName"));

        l.setStage(new com.sok.runway.externalInterface.beans.cms.properties.Stage());
        l.getStage().setStageID(data.getData("StageProductID"));
        l.getStage().setName(data.getData("StageName"));

        com.sok.runway.externalInterface.beans.cms.properties.Estate es = new com.sok.runway.externalInterface.beans.cms.properties.Estate();
        es.setEstateID(data.getData("EstateProductID"));
        es.setName(data.getData("EstateName"));
        l.getStage().setEstate(es);

        e.setLot(l);

        SimplePlan p = new SimplePlan() {
        };
        p.setPlanID(data.getData("PlanProductID"));
        p.setOriginalPlanID(data.getData("CopyPlanID"));
        p.setName(data.getData("PlanName"));
        e.setPlan(p);

        SimpleHome h = new SimpleHome() {
        };
        h.setHomeID(data.getData("HomeProductID"));
        h.setName(data.getData("HomeName"));
        p.setHome(h);

        SimpleRange r = new SimpleRange() {
        };
        r.setRangeID(data.getData("RangeProductID"));
        r.setName(data.getData("RangeName"));
        h.setRange(r);

        SimpleBrand b = new SimpleBrand() {
        };
        b.setBrandID(data.getData("BrandProductID"));
        b.setName(data.getData("BrandName"));
        r.setBrand(b);

        rep.clear();
        rep = new GenRow();
        rep.setConnection(data.getConnection());
        rep.setTableSpec("CampaignProducts");
        rep.setParameter("-join1", "ProductCampaignCampaignActive");
        rep.setParameter("-select1", "ProductCampaignCampaignActive.CampaignID as CampaignID");
        rep.setParameter("-select2", "ProductCampaignCampaignActive.Name as CampaignName");
        rep.setParameter("ProductID", e.getPackageID());

        if (rep.isSet("ProductID")) {
            try {
                rep.getResults(true);
                if (rep.getNext()) {
                    e.setCampaigns(new ArrayList<SimpleCampaign>(rep.getSize()));
                    do {
                        SimpleCampaign sc = new SimpleCampaign() {
                        };
                        sc.setCampaignID(rep.getData("CampaignID"));
                        sc.setName(rep.getData("CampaignName"));
                        e.getCampaigns().add(sc);
                    } while (rep.getNext());
                }
            } finally {
                rep.close();
            }
        }

        double basePrice = 0, dripPrice = 0, campaignDripPrice = 0, price = 0;
        try {
            price = data.getDouble("DripResult");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            dripPrice = data.getDouble("DripCost");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            campaignDripPrice = data.getDouble("CampaignDripCost");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            basePrice = data.getDouble("TotalCost");
        } catch (IllegalConfigurationException ice) {
        }

        if (price == 0) {
            price = basePrice;
        }

        if (dripPrice != (price - basePrice)) {
            LOGGER.warn("Product {} dripPrice != (price - basePrice)", data.getData("ProductID"));
        }

        e.setBasePrice(basePrice);
        e.setCampaignDripPrice(campaignDripPrice);
        e.setDripPrice(dripPrice);
        e.setPrice(price);

        HouseLandPackageStatusHistory s = new HouseLandPackageStatusHistory();
        s.setStatus(data.getData("CurrentStatus"));
        s.setStatusID(data.getData("CurrentStatusID"));
        e.setCurrentStatus(s);

        return e;
    }

    public static HouseLandPackageCheck externalCheck(com.sok.runway.crm.cms.properties.HouseLandPackage data) {
        HouseLandPackageCheck e = new HouseLandPackageCheck();
        e.setPackageID(data.getData("ProductID"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        if (data.isSet("DripModifiedDate")) {
            e.setDripModifiedDate(data.getDate("DripModifiedDate"));
        }
        e.setActive(data.getData("Active"));
        e.setError(data.getError());

        e.setPublishing(loadPublishing(data.getData("ProductID")));

        HouseLandPackageStatusHistory s = new HouseLandPackageStatusHistory();
        s.setStatus(data.getData("CurrentStatus"));
        s.setStatusID(data.getData("CurrentStatusID"));
        e.setCurrentStatus(s);

        e.setAvailableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setPublishing(BeanFactory.loadPublishing(data.getData("ProductID")));

        return e;
    }

    public static HouseLandPackage external(HouseLandPackageEntity data) {
        return external(data, BeanRepresentation.Simple);
    }

    public static HouseLandPackage external(HouseLandPackageEntity data, BeanRepresentation repr) {
        LOGGER.debug("HouseLandPackage external(data, repr={})", repr != null ? repr.name() : "null");
        HouseLandPackage e = new HouseLandPackage();
        e.setPackageID(data.getField("ProductID"));
        e.setName(data.getField("Name"));
        e.setProductNum(data.getField("ProductNum"));
        e.setDescription(data.getField("Description"));
        e.setPublishDescription(evaluateHeadline(data, data.getField("PublishDescription")));
        e.setPublishHeadline(evaluateHeadline(data, data.getField("Summary")));
        e.setAddress(BeanFactory.getAddressFromTableData(data.getRecord(), ""));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
        e.setModifiedBy(data.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));
        e.setActive(data.getField("Active"));
        e.setDisplayFlag(data.getField("DisplayFlag"));
        e.setError(data.getRecord().getError());

//		if (data.isSet("DisplayPrice")) {
//			e.setDisplayPrice(data.getField("DisplayPrice"));
//		} else if (data.isSet("DripResult")) {
//			e.setDisplayPrice(NumberFormat.getCurrencyInstance().format(data.getDouble("DripResult")));
//		} else if (data.isSet("TotalCost")) {
//			e.setDisplayPrice(NumberFormat.getCurrencyInstance().format(data.getDouble("TotalCost")));
//		}

        if (data.isSet("DripResult")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(data.getField("DisplayPriceType"), data.getDouble("DripResult")));
        } else if (data.isSet("TotalCost")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(data.getField("DisplayPriceType"), data.getDouble("TotalCost")));
        }

        if (StringUtils.isBlank(e.getDisplayPrice()) && data.isSet("DisplayPrice")) {
            e.setDisplayPrice(data.getField("DisplayPrice"));
        }

        e.setPreAvailableDate(data.getDate("PreAvailableDate"));
        e.setAvailableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setGroups(getProductGroups(data.getConnection(), e.getPackageID()));

        e.setMarketingStatus((data.isSet("MarketingStatus") ? data.getField("MarketingStatus") : data.getField("LotMarketingStatus")));

        e.setPublishing(loadPublishing(data.getField("ProductID")));

        if (StringUtils.isNotBlank(data.getField("Image"))) {
            e.setImage(loadDocument(data.getField("Image")));
        }

        if (data.isSet("LocationID")) {
            e.setLocation(getLocations(data.getConnection(), data.getField("LocationID"), repr));
        } else {
            LOGGER.debug("Location not set : packageID=[{}]", data.getField("ProductID"));
        }

        if (data.isSet("RegionID")) {
            e.setRegion(getLocations(data.getConnection(), data.getField("RegionID"), BeanRepresentation.Simple));
        } else {
            LOGGER.debug("Region not set : packageID=[{}]", data.getField("ProductID"));
        }

        GenRow rep = new GenRow();
        rep.setViewSpec("ProductSecurityGroupView");
        rep.setConnection(data.getConnection());
        rep.setParameter("ProductID", e.getPackageID());
        rep.setParameter("RepUserID", "!EMPTY");
        if (rep.isSet("ProductID")) {
            rep.doAction(GenerationKeys.SELECTFIRST);
        }
        e.setDetailPdfUrl(getDetailPdfUrl(e.getLocation() != null ?  e.getLocation().getLocationID() : "", e.getPackageID(), e.getModifiedDate(),
                rep.getData("RepUserID")));

        if (repr == BeanRepresentation.Extended || repr == BeanRepresentation.Full) {
            LotEntity le = new LotEntity(data.getConnection(), data.getField("LotID"));
            if (le.isLoaded()) {
                e.setLot(external(le, repr));
            }
            PlanEntity pe = new PlanEntity(data.getConnection(), data.getField("PlanID"));
            if (pe.isLoaded()) {
                e.setPlan(external(pe, repr));
            }
            e.setFacade(data.getFacade());
            //e.setLot(external(data.getLot()));
            //e.setPlan(external(data.getLot()));
        } else {
            Lot l = new Lot();
            l.setLotID(data.getField("LotID"));
            l.setName(data.getField("LotName"));
            l.setCost(data.getDouble("LotCost"));

            l.setStage(new Stage());
            l.getStage().setStageID(data.getField("StageProductID"));
            l.getStage().setName(data.getField("StageName"));

            Estate es = new Estate();
            es.setEstateID(data.getField("EstateProductID"));
            es.setName(data.getField("EstateName"));

            l.getStage().setEstate(es);

            e.setLot(l);

            SimplePlan p = new SimplePlan() {
            };
            p.setPlanID(data.getField("PlanID"));
            p.setOriginalPlanID(data.getField("CopyPlanID"));
            p.setName(data.getField("PlanName"));
            p.setCost(data.getDouble("PlanCost"));
            e.setPlan(p);

            SimpleHome h = new SimpleHome() {
            };
            h.setHomeID(data.getField("HomeProductID"));
            h.setName(data.getField("HomeName"));
            p.setHome(h);

            SimpleRange r = new SimpleRange() {
            };
            r.setRangeID(data.getField("RangeProductID"));
            r.setName(data.getField("RangeName"));
            h.setRange(r);

            SimpleBrand b = new SimpleBrand() {
            };
            b.setBrandID(data.getField("BrandProductID"));
            b.setName(data.getField("BrandName"));
            r.setBrand(b);
        }

		/*
        if(repr != BeanRepresentation.Simple) {
			e.setPublishHeadline(data.getField("Summary"));
			e.setPublishDescription(data.getField("PublishDescription"));
		}
		*/
        rep.clear();
        rep = new GenRow();
        rep.setConnection(data.getConnection());
        rep.setTableSpec("CampaignProducts");
        rep.setParameter("-join1", "ProductCampaignCampaignActive");
        rep.setParameter("-select1", "ProductCampaignCampaignActive.CampaignID as CampaignID");
        rep.setParameter("-select2", "ProductCampaignCampaignActive.Name as CampaignName");
        rep.setParameter("ProductID", e.getPackageID());

        if (rep.isSet("ProductID")) {
            try {
                rep.getResults(true);
                if (rep.getNext()) {
                    e.setCampaigns(new ArrayList<SimpleCampaign>(rep.getSize()));
                    do {
                        SimpleCampaign sc = new SimpleCampaign() {
                        };
                        sc.setCampaignID(rep.getData("CampaignID"));
                        sc.setName(rep.getData("CampaignName"));
                        e.getCampaigns().add(sc);
                    } while (rep.getNext());
                }
            } finally {
                rep.close();
            }
        }

        double basePrice = 0, dripPrice = 0, campaignDripPrice = 0, price = 0;
        try {
            price = data.getDouble("DripResult");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            dripPrice = data.getDouble("DripCost");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            campaignDripPrice = data.getDouble("CampaignDripCost");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            basePrice = data.getDouble("TotalCost");
        } catch (IllegalConfigurationException ice) {
        }

        if (price == 0) {
            price = basePrice;
        }

        if (dripPrice != (price - basePrice)) {
            LOGGER.warn("Product {} dripPrice != (price - basePrice)", data.getField("ProductID"));
        }

        e.setBasePrice(basePrice);
        e.setDripPrice(dripPrice);
        e.setCampaignDripPrice(campaignDripPrice);
        e.setPrice(StringUtil.getRoundedValue(price));

        HouseLandPackageStatusHistory s = new HouseLandPackageStatusHistory();
        s.setStatus(data.getField("CurrentStatus"));
        s.setStatusID(data.getField("CurrentStatusID"));
        e.setCurrentStatus(s);

        if (repr == BeanRepresentation.Internal) {
            e.setFacade(data.getFacade());
            e.setPackageCosts(data.getPackageCosts());

			/*
			 * e.setQuoteViewID(PlanService.getInstance().getQuoteViewID(hlp))
			e.setFacadeID()

			*/
        }
        return e;
    }

    public static String getDetailPdfUrl(String locationId, final String productId, Date lastModifiedDate, final String repUserId) {
        String apiGatewayHomeUrl = InitServlet.getSystemParam(SYS_PARAM_PDF_DOWNLOAD_URL_HOME);
        String bucketName = InitServlet.getSystemParam(SYS_PARAM_R6_AWSS3_BUCKET_NAME, DEFAULT_CLIENT_BUCKET_NAME);
        String runwayUrlHome = InitServlet.getSystemParam(SYS_PARAM_URL_HOME);
        boolean pdfAutoCachingEnabled = "true".equalsIgnoreCase(InitServlet.getSystemParam(SYS_PARAM_PDF_AUTO_CACHING_ENABLED));

        String lastModifiedDateString = API_LAST_MODIFIED_TIME_FORMATTER.format(lastModifiedDate != null ? lastModifiedDate : new Date());

        String detailPdfUrl = null;
        if (pdfAutoCachingEnabled) {
            detailPdfUrl = buildR6PdfDownloadUrl(apiGatewayHomeUrl, StringUtils.trimToEmpty(locationId), productId, repUserId, lastModifiedDateString
                    , bucketName, runwayUrlHome);
        }
        if(!pdfAutoCachingEnabled || detailPdfUrl == null){
            detailPdfUrl = buildR4PdfDownloadUrl(locationId, lastModifiedDate, productId, repUserId);
        }
        return detailPdfUrl;
    }

    private static String buildR6PdfDownloadUrl(final String apiGatewayHomeUrl, final String locationId, final String productId, final String repUserId
            , final String lastModifiedTime, final String bucketName, final String runwayUrlHome) {
        try {
            return apiGatewayHomeUrl + "?-api=true&" +
                    "PdfType=" + URLEncoder.encode("builder", ActionBean.utf8) + "&" +
                    "BucketName=" + URLEncoder.encode(bucketName, ActionBean.utf8) + "&" +
                    "ProductID=" + URLEncoder.encode(productId, ActionBean.utf8) + "&" +
                    "LocationID=" + URLEncoder.encode(locationId, ActionBean.utf8) + "&" +
                    "LastModifiedTime=" + URLEncoder.encode(lastModifiedTime, ActionBean.utf8) + "&" +
                    "RepUserID=" + URLEncoder.encode(repUserId, ActionBean.utf8) + "&" +
                    "RunwayUrl=" + URLEncoder.encode(runwayUrlHome, ActionBean.utf8);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.error("Error during the url encoding", ex);
            return null;
        }
    }

    private static String buildR4PdfDownloadUrl(final String locationId, final Date lastModifiedDate, final String productId, final String repUserId) {
        return String.format("%1$s/property.pdf?-api=true&ProductID=%2$s&v=%3$d&LocationID=%4$s&RepUserID=%5$s", InitServlet.getSystemParam("URLHome"),
                productId, lastModifiedDate != null ? lastModifiedDate.getTime() : new Double(Math.random() * 100000).intValue(),
                StringUtils.trimToEmpty(locationId), repUserId);
    }

    public static Home external(com.sok.runway.crm.cms.properties.Home data) {
        Home e = new Home();
        e.setHomeID(data.getData("ProductID"));
        e.setName(data.getData("Name"));
        e.setProductNum(data.getData("ProductNum"));
        e.setDescription(data.getData("Description"));
        e.setPublishDescription(evaluateHeadline(data, data.getData("PublishDescription")));
        e.setPublishHeadline(evaluateHeadline(data, data.getData("HomeSummary")));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getData("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getData("OwnFirstName"), data.getData("OwnLastName")));
        e.setModifiedBy(data.getData("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getData("ModFirstName"), data.getData("ModLastName")));
        e.setActive(data.getData("Active"));
        e.setError(data.getError());

        if (StringUtils.isNotBlank(data.getData("Image"))) {
            e.setImage(loadDocument(data.getData("Image")));
        }

        Facade facades = data.getFacades();

        List<com.sok.runway.externalInterface.beans.cms.properties.Facade> facadeList = new ArrayList<com.sok.runway.externalInterface.beans.cms.properties
                .Facade>(facades.getSize());
        while (facades.getNext()) {
            facadeList.add(BeanFactory.external(facades));
        }

        facades.close();
        e.setFacades(facadeList);

        e.setPlans(data.getPlans("true".equals(data.getString("-active"))));

        SimpleRange r = new SimpleRange() {
        };
        r.setRangeID(data.getData("RangeProductID"));
        r.setName(data.getData("RangeName"));
        e.setRange(r);

        SimpleBrand b = new SimpleBrand() {
        };
        b.setBrandID(data.getData("BrandProductID"));
        b.setName(data.getData("BrandName"));
        r.setBrand(b);

        if (data.isSet("LocationID")) {
            e.setLocation(LocationService.getInstance().getLocation(null, data.getString("LocationID")));
        }
        if (data.isSet("RegionID")) {
            e.setRegion(LocationService.getInstance().getLocation(null, data.getString("RegionID")));
        }

        double basePrice = 0, dripPrice = 0, campaignDripPrice = 0, price = 0;
        try {
            price = data.getDouble("DripResult");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            dripPrice = data.getDouble("DripCost");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            campaignDripPrice = data.getDouble("CampaignDripCost");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            basePrice = data.getDouble("TotalCost");
        } catch (IllegalConfigurationException ice) {
        }

        if (price == 0) {
            price = basePrice;
        }

        if (dripPrice != (price - basePrice)) {
            LOGGER.warn("Product {} dripPrice != (price - basePrice)", data.getData("ProductID"));
        }

        return e;
    }

    public static HomeCheck externalCheck(com.sok.runway.crm.cms.properties.Home data) {
        HomeCheck e = new HomeCheck();
        e.setHomeID(data.getData("ProductID"));
        e.setName(data.getData("Name"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setActive(data.getData("Active"));
        e.setError(data.getError());

        e.setPlans(data.getPlansCheck("true".equals(data.getString("-active"))));

        SimpleRange r = new SimpleRange() {
        };
        r.setRangeID(data.getData("RangeProductID"));
        r.setName(data.getData("RangeName"));
        e.setRange(r);

        SimpleBrand b = new SimpleBrand() {
        };
        b.setBrandID(data.getData("BrandProductID"));
        b.setName(data.getData("BrandName"));
        r.setBrand(b);

        return e;
    }

    private static com.sok.runway.externalInterface.beans.cms.properties.Facade external(Facade data) {
        return external(data, BeanRepresentation.Simple);
    }

    private static com.sok.runway.externalInterface.beans.cms.properties.Facade external(Facade data, BeanRepresentation repr) {
        com.sok.runway.externalInterface.beans.cms.properties.Facade f = new com.sok.runway.externalInterface.beans.cms.properties.Facade();

        f.setFacadeID(data.getData("LinkedProductID"));
        f.setName(data.getFacadeName());
        f.setDescription(data.getString("Description"));

        f.setGST(data.getDouble("GST"));
        f.setPrice(data.getDouble("Cost"));
        f.setTotalCost(data.getDouble("TotalCost"));

        f.setActive(data.getString("Active"));
        f.setDefaultFacade(data.getString("IsExclusive"));

        f.setProductNum(data.getString("ProductNum"));

        if (StringUtils.isNotBlank(data.getData("Image"))) {
            f.setImage(loadDocument(data.getData("Image")));
        }

        f.setCreatedBy(data.getString("CreatedBy"));
        f.setCreatedDate(data.getDate("CreatedDate"));
        f.setModifiedBy(data.getString("ModifiedBy"));
        f.setModifiedDate(data.getDate("ModifiedDate"));

        if (repr != BeanRepresentation.Simple) {
            f.setDetails(FacadeService.getInstance().getProductDetails(data.getConnection(), null, data.getData("ProductID")));
        }

        return f;
    }

    public static Home external(HomeEntity data) {
        return external(data, BeanRepresentation.Simple);
    }

    public static Home external(HomeEntity data, BeanRepresentation repr) {
        LOGGER.debug("Home external(data, repr={})", repr != null ? repr.name() : "null");
        Home e = new Home();
        e.setHomeID(data.getField("ProductID"));
        e.setName(data.getField("Name"));
        e.setProductNum(data.getField("ProductNum"));
        e.setDescription(data.getField("Description"));
        e.setCreatedDate(data.getDate("CreatedDate"));

        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
        e.setModifiedBy(data.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));
        e.setActive(data.getField("Active"));
        e.setError(data.getRecord().getError());

        if (StringUtils.isNotBlank(data.getField("PublishHomeDescription"))) {
            e.setPublishDescription(evaluateHeadline(data, data.getField("PublishHomeDescription")));
        } else {
            e.setPublishDescription(evaluateHeadline(data, data.getField("PublishDescription")));
        }
        if (StringUtils.isNotBlank(data.getField("HomeSummary"))) {
            e.setPublishHeadline(evaluateHeadline(data, data.getField("HomeSummary")));
        } else {
            e.setPublishHeadline(evaluateHeadline(data, data.getField("Summary")));
        }

        if (StringUtils.isNotBlank(data.getField("Image"))) {
            e.setImage(loadDocument(data.getField("Image")));
        }

        GenRow rep = new GenRow();
        rep.setViewSpec("ProductSecurityGroupView");
        rep.setConnection(data.getConnection());
        rep.setParameter("ProductID", e.getHomeID());
        rep.setParameter("RepUserID", "!EMPTY");
        if (rep.isSet("ProductID")) {
            rep.doAction(GenerationKeys.SELECTFIRST);
        }
//		e.setDetailPdfUrl(String.format("%1$s/property.pdf?-api=true&ProductID=%2$s&-variant=SinglePackage&v=%3$d&LocationID=%4$s&RepUserID=%5$s", InitServlet
// .getSystemParam("URLHome"), e.getPackageID(), e.getModifiedDate() != null ? e.getModifiedDate().getTime() : new Double(Math.random()*100000).intValue(), e
// .getLocation() != null ? e.getLocation().getLocationID() : "", rep.getData("RepUserID")));

        if (repr == BeanRepresentation.Extended || repr == BeanRepresentation.Full) {
//			LotEntity le = new LotEntity(data.getConnection(), data.getField("LotID"));
//			PlanEntity pe = new PlanEntity(data.getConnection(), data.getField("PlanID"));
//			if(pe.isLoaded()) {
//				e.setPlan(external(pe, repr));
//			}
//			e.setFacade(data.getFacade());
            //e.setLot(external(data.getLot()));
            //e.setPlan(external(data.getLot()));
        } else {
            SimpleRange r = new SimpleRange() {
            };
            r.setRangeID(data.getField("RangeProductID"));
            r.setName(data.getField("RangeName"));
            e.setRange(r);

            SimpleBrand b = new SimpleBrand() {
            };
            b.setBrandID(data.getField("BrandProductID"));
            b.setName(data.getField("BrandName"));
            r.setBrand(b);
        }

        List<FacadeService.Facade> facades = FacadeService.getInstance().getFacades(data.getConnection(), UserService.getInstance().getSimpleUser(data
                .getConnection()), data.getField("ProductID"));

        List<com.sok.runway.externalInterface.beans.cms.properties.Facade> facadeList = new ArrayList<com.sok.runway.externalInterface.beans.cms.properties
                .Facade>(facades.size());
        for (FacadeService.Facade facade : facades) {
            facadeList.add(BeanFactory.external(facade, repr));
        }

        e.setFacades(facadeList);

        List<com.sok.runway.externalInterface.beans.cms.properties.Plan> planList = new ArrayList<com.sok.runway.externalInterface.beans.cms.properties.Plan>();

        if (data.getPlanEntity() == null) {
            List<PlanService.Plan> plans = PlanService.getInstance().getPlans(data.getConnection(), UserService.getInstance().getSimpleUser(data
                    .getConnection()), data.getField("ProductID"));

            for (PlanService.Plan plan : plans) {
                planList.add(BeanFactory.external(plan, repr));
            }
        } else {
            planList.add(BeanFactory.external(data.getPlanEntity(), repr));
        }

        e.setPlans(planList);

//		if(repr != BeanRepresentation.Simple) {
//			e.setPublishHeadline(data.getField("Summary"));
//			e.setPublishDescription(data.getField("PublishDescription"));
//		}

        if (data.isSet("LocationID")) {
            e.setLocation(LocationService.getInstance().getLocation(null, data.getField("LocationID")));
        }
        if (data.isSet("RegionID")) {
            e.setRegion(LocationService.getInstance().getLocation(null, data.getField("RegionID")));
        }

        double basePrice = 0, dripPrice = 0, price = 0;
        try {
            price = data.getDouble("DripResult");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            dripPrice = data.getDouble("DripCost");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            basePrice = data.getDouble("TotalCost");
        } catch (IllegalConfigurationException ice) {
        }

        if (price == 0) {
            price = basePrice;
        }

        if (dripPrice != (price - basePrice)) {
            LOGGER.warn("Product {} dripPrice != (price - basePrice)", data.getField("ProductID"));
        }

//		e.setBasePrice(basePrice);
//		e.setDripPrice(dripPrice);
//		e.setCampaignDripPrice(campaignDripPrice);
//		e.setPrice(price);

        HouseLandPackageStatusHistory s = new HouseLandPackageStatusHistory();
        s.setStatus(data.getField("CurrentStatus"));
        s.setStatusID(data.getField("CurrentStatusID"));
//		e.setCurrentStatus(s);

//		if(repr == BeanRepresentation.Internal) {
//			e.setFacade(data.getFacade());
//			e.setPackageCosts(data.getPackageCosts());
//
			/*
			 * e.setQuoteViewID(PlanService.getInstance().getQuoteViewID(hlp))
			e.setFacadeID()

			*/
//		}
        return e;
    }

    private static Plan external(com.sok.service.crm.cms.properties.PlanService.Plan plan, BeanRepresentation repr) {
        return external(new HomePlan((Connection) null, plan.ProductID), repr);
    }

    private static com.sok.runway.externalInterface.beans.cms.properties.Facade external(
            com.sok.service.crm.cms.properties.FacadeService.Facade facade,
            BeanRepresentation repr) {
        return external(new Facade((Connection) null, facade.ProductID), repr);
    }

    private static com.sok.runway.externalInterface.beans.cms.properties.Plan external(
            com.sok.service.crm.cms.properties.PlanService.Plan plan) {
        return external(new HomePlan((Connection) null, plan.ProductID));
    }

    private static com.sok.runway.externalInterface.beans.cms.properties.Facade external(
            com.sok.service.crm.cms.properties.FacadeService.Facade facade) {
        return external(new Facade((Connection) null, facade.ProductID));
    }

    private static LegacyLocation getLocations(Connection con, String locationID, BeanRepresentation rep) {
        LOGGER.debug("getLocations(con, locationID={}, rep={})", locationID, rep != null ? rep.name() : "null");
        GenRow loc = new GenRow();
        loc.setViewSpec("DisplayEntityView");
        loc.setConnection(con);
        loc.setParameter("DisplayEntityID", locationID);
        if (loc.isSet("DisplayEntityID")) {
            loc.doAction(GenerationKeys.SELECT);
            if (loc.isSuccessful()) {
                if (!"ROOT".equals(loc.getData("Type"))) {
                    LegacyLocation l = new LegacyLocation();
                    l.setLocationID(loc.getData("DisplayEntityID"));
                    l.setName(loc.getData("Name"));
                    l.setType(loc.getData("Type"));
                    if (rep != BeanRepresentation.Simple) {
                        l.setDescription(loc.getData("Description"));
                    }
                    if (loc.isSet("ParentDisplayEntityID") && !loc.getString("ParentDisplayEntityID").equals(locationID)) {
                        l.setParent(getLocations(con, loc.getData("ParentDisplayEntityID"), rep));
                    }
                    return l;
                } else {
                    LOGGER.debug("root was type ? [{}]", loc.getData("Type"));
                }
            } else {
                LOGGER.debug("loc not successful");
                LOGGER.debug(loc.getStatement());
            }
        } else {
            LOGGER.debug("loc wasn't set");
            LOGGER.debug(loc.toString());
        }
        return null;
    }

    public static Estate external(EstateEntity data) {

        Estate e = new Estate();
        e.setEstateID(data.getField("ProductID"));
        e.setName(data.getField("Name"));
        e.setPublishName(data.getField("PublishName"));
        e.setProductNum(data.getField("ProductNum"));
        e.setDescription(data.getField("Description"));
        e.setURL(data.getField("URL"));
        e.setAddress(BeanFactory.getAddressFromTableData(data.getRecord(), ""));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
        e.setModifiedBy(data.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));
        e.setName(data.getName());
        e.setError(data.getRecord().getError());

        SimpleLocation esl = new SimpleLocation();
        esl.setLocationID(data.getField("LocationID"));
        esl.setName(data.getField("LocationName"));
        esl.setType(data.getField("LocationType"));
        e.setLocation(esl);

        if (StringUtils.isNotBlank(data.getField("ThumbnailImage"))) {
            e.setThumbnailImage(loadDocument(data.getField("ThumbnailImage")));
        }

        GenRow maps = new GenRow();
        maps.setViewSpec("LinkedDocumentView");
        //maps.setConnection(data.getConnection());
        maps.setParameter("ProductID", e.getEstateID());
        maps.setParameter("DocumentSubType", "MasterPlan");
        maps.setParameter("LinkedDocumentDocument.DocumentSubType", "MasterPlan");

        if (maps.getString("ProductID").length() > 0) {
            maps.doAction("search");

            maps.getResults();

            if (maps.getNext()) {
                InteractiveMap im = new InteractiveMap();
                im.setMajorID(e.getEstateID());
                im.setProductType("Land");

                do {
                    if (maps.getString("FileName").toLowerCase().endsWith(".jpg") || maps.getString("FileName").toLowerCase().endsWith(".png")) {
                        im.setMapImage(loadDocument(maps.getData("DocumentID")));
                    } else if (maps.getString("FileName").toLowerCase().endsWith(".map")) {
                        im.loadMapHTML(maps.getString("FilePath"));
                    }

                } while (maps.getNext());

                e.setInteractiveMap(im);
            }
        }

        maps.close();

        return e;
    }

    public static Builder external(BuilderEntity data) {
        Builder e = new Builder();
        e.setBuilderID(data.getField("ProductID"));
        e.setName(data.getField("Name"));
        e.setProductNum(data.getField("ProductNum"));
        e.setDescription(data.getField("Description"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
        e.setModifiedBy(data.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));
        e.setName(data.getField("Name"));
        e.setURL(data.getField("URL"));
        if (StringUtils.isNotBlank(data.getField("Image"))) {
            e.setImage(loadDocument(data.getField("Image")));
        }

        if (StringUtils.isNotBlank(data.getField("ThumbnailImage"))) {
            e.setThumbnailImage(loadDocument(data.getField("ThumbnailImage")));
        }

        return e;
    }


    public static Building external(BuildingEntity data) {
        Building e = new Building();
        e.setBuildingID(data.getField("ProductID"));
        e.setName(data.getField("Name"));
        e.setProductNum(data.getField("ProductNum"));
        e.setDescription(data.getField("Description"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
        e.setModifiedBy(data.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));
        e.setName(data.getField("Name"));
        e.setURL(data.getField("URL"));
        if (StringUtils.isNotBlank(data.getField("ThumbnailImage"))) {
            e.setThumbnailImage(loadDocument(data.getField("ThumbnailImage")));
        }
        if (StringUtils.isNotBlank(data.getField("Image"))) {
            e.setImage(loadDocument(data.getField("Image")));
        }
        return e;
    }


    public static Lot external(LotEntity data) {
        return external(data, BeanRepresentation.Simple);
    }

    public static Lot external(LotEntity data, BeanRepresentation repr) {
        Lot e = new Lot();
        e.setLotID(data.getField("ProductID"));
        e.setName(data.getField("Name"));
        e.setCost(data.getDouble("Cost"));
        e.setGST(data.getDouble("GST"));
        e.setTotalCost(data.getDouble("TotalCost"));
        e.setSubType(data.getField("ProductSubType"));
        e.setMarketingStatus(data.getField("MarketingStatus"));
        e.setActive(data.getField("Active"));

        String displayPriceType = data.getField("DisplayPriceType");
        if (StringUtils.isBlank(displayPriceType)) {
            displayPriceType = getDisplayPriceType(data.getField("ProductID"));
        }

        e.setPackageCount(getPackageCount(data.getField("ProductID")));

        if (data.isSet("DripResult")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("DripResult")));
        } else if (data.isSet("TotalCost")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("TotalCost")));
        }

        if (StringUtils.isBlank(e.getDisplayPrice()) && data.isSet("DisplayPrice")) {
            e.setDisplayPrice(data.getField("DisplayPrice"));
        }

        e.setAvailableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setGroups(getProductGroups(data.getConnection(), e.getLotID()));

        e.setPublishing(BeanFactory.loadPublishing(data.getField("ProductID")));

        e.setStage(new Stage());
        e.getStage().setStageID(data.getField("StageProductID"));
        e.getStage().setName(data.getField("StageName"));

        Estate es = new Estate();
        es.setEstateID(data.getField("EstateProductID"));
        es.setName(data.getField("EstateName"));
        e.getStage().setEstate(es);

        e.setRegionID(data.getField("RegionID"));
        e.setLocationID(data.getField("LocationID"));

        if (data.isSet("LocationID")) {
            e.setLocation(LocationService.getInstance().getLegacyLocation(null, data.getField("LocationID")));
        }
        if (data.isSet("RegionID")) {
            e.setRegion(LocationService.getInstance().getLegacyLocation(null, data.getField("RegionID")));
        }

        e.setPublishing(loadPublishing(data.getField("ProductID")));

        if (StringUtils.isNotBlank(data.getField("Image"))) {
            e.setImage(loadDocument(data.getField("Image")));
        } else if (StringUtils.isNotBlank(data.getField("EstateImage"))) {
            e.setImage(loadDocument(data.getField("EstateImage")));
        }

        if (StringUtils.isNotBlank(data.getField("ThumbnailImage"))) {
            e.setThumbnail(loadDocument(data.getField("ThumbnailImage")));
        } else if (StringUtils.isNotBlank(data.getField("EstateThumbnailImage"))) {
            e.setThumbnail(loadDocument(data.getField("EstateThumbnailImage")));
        }

        if (repr != BeanRepresentation.Simple) {
            es.setPublishName(data.getField("EstatePublishName"));
            e.setProductNum(data.getField("ProductNum"));
            e.setDescription(data.getField("Description"));

            e.setAddress(BeanFactory.getAddressFromTableData(data.getRecord(), ""));
            e.setCreatedDate(data.getDate("CreatedDate"));
            e.setModifiedDate(data.getDate("ModifiedDate"));
            e.setCreatedBy(data.getField("CreatedBy"));
            e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
            e.setModifiedBy(data.getField("ModifiedBy"));
            e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));

            e.setTitleMonth(data.getField("LotTitleMonth"));
            e.setTitleYear(data.getField("LotTitleYear"));

            LotStatusHistory s = new LotStatusHistory();
            s.setStatus(data.getField("CurrentStatus"));
            s.setStatusID(data.getField("CurrentStatusID"));
            e.setCurrentStatus(s);

            if (StringUtils.isNotBlank(data.getField("CampaignID"))) {
                SimpleCampaign sc = new SimpleCampaign() {
                };
                sc.setCampaignID(data.getField("CampaignID"));
                sc.setName(data.getField("CampaignName"));
                e.setCampaign(sc);
            }

            e.setDetails(PlanService.getInstance().getProductDetails(data.getConnection(), null, data.getPrimaryKey(), "Land+LandDetails"));
        }

        GenRow maps = new GenRow();
        maps.setViewSpec("LinkedDocumentView");
        //maps.setConnection(data.getConnection());
        maps.setParameter("ProductID", data.getField("ProductID"));
        maps.setParameter("DocumentSubType", "MasterPlan");
        maps.setParameter("LinkedDocumentDocument.DocumentSubType", "MasterPlan");

        if (maps.getString("ProductID").length() > 0) {
            maps.doAction("search");

            maps.getResults();

            if (maps.getNext()) {
                InteractiveMap im = new InteractiveMap();
                im.setMajorID(data.getField("EstateProductID"));
                im.setMinorID(data.getField("StageProductID"));
                im.setProductType("Land");

                do {
                    if (maps.getString("FileName").toLowerCase().endsWith(".jpg") || maps.getString("FileName").toLowerCase().endsWith(".png")) {
                        im.setMapImage(loadDocument(maps.getData("DocumentID")));
                    } else if (maps.getString("FileName").toLowerCase().endsWith(".map")) {
                        im.loadMapHTML(maps.getString("FilePath"));
                    }

                } while (maps.getNext());

                e.setInteractiveMap(im);
            }
        }

        maps.close();
        return e;
    }

    public static Apartment external(com.sok.runway.crm.cms.properties.Apartment data) {
        return external(data, BeanRepresentation.Simple);
    }

    public static Apartment external(com.sok.runway.crm.cms.properties.Apartment data, BeanRepresentation repr) {
        Apartment e = new Apartment();
        e.setApartmentID(data.getData("ProductID"));
        e.setName(data.getString("Name"));
        e.setCost(data.getDouble("Cost"));
        e.setGST(data.getDouble("GST"));
        e.setTotalCost(data.getDouble("TotalCost"));
        //e.setPrice(data.getDouble("DripResult") != 0 ? data.getDouble("DripResult") : e.getTotalCost());
        e.setSubType(data.getString("ProductSubType"));
        e.setMarketingStatus(data.getString("MarketingStatus"));
        e.setActive(data.getData("Active"));

        String displayPriceType = data.getString("DisplayPriceType");
        if (StringUtils.isBlank(displayPriceType)) {
            displayPriceType = getDisplayPriceType(data.getData("ProductID"));
        }

        if (data.isSet("DripResult")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("DripResult")));
        } else if (data.isSet("TotalCost")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("TotalCost")));
        }

        if (StringUtils.isBlank(e.getDisplayPrice()) && data.isSet("DisplayPrice")) {
            e.setDisplayPrice(data.getString("DisplayPrice"));
        }

        e.setAvailableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setGroups(getProductGroups(data.getConnection(), e.getApartmentID()));

        if (StringUtils.isNotBlank(data.getData("Image"))) {
            e.setImage(loadDocument(data.getData("Image")));
        }

        e.setBuildingStage(new BuildingStage());
        e.getBuildingStage().setBuildingStageID(data.getString("BuildingStageProductID"));
        e.getBuildingStage().setName(data.getString("BuildingStageName"));

        Building es = new Building();
        es.setBuildingID(data.getString("BuildingProductID"));
        es.setName(data.getString("BuildingName"));
        e.getBuildingStage().setBuilding(es);

        e.setRegionID(data.getString("RegionID"));
        e.setLocationID(data.getString("LocationID"));

        e.setPublishing(loadPublishing(data.getData("ProductID")));

        if (data.isSet("LocationID")) {
            e.setLocation(LocationService.getInstance().getLocation(null, data.getString("LocationID")));
        }
        if (data.isSet("RegionID")) {
            e.setRegion(LocationService.getInstance().getLocation(null, data.getString("RegionID")));
        }

        if (StringUtils.isNotBlank(data.getData("Image"))) {
            e.setImage(loadDocument(data.getData("Image")));
        } else if (StringUtils.isNotBlank(data.getData("EstateImage"))) {
            e.setImage(loadDocument(data.getData("EstateImage")));
        }

        e.setDescription(data.getString("Description"));
        e.setPublishDescription(evaluateHeadline(data, data.getData("PublishDescription")));
        e.setPublishHeadline(evaluateHeadline(data, data.getData("Summary")));

        if (repr != BeanRepresentation.Simple) {
            es.setPublishName(data.getString("EstatePublishName"));
            e.setProductNum(data.getString("ProductNum"));

            e.setAddress(BeanFactory.getAddressFromTableData(data, ""));
            e.setCreatedDate(data.getDate("CreatedDate"));
            e.setModifiedDate(data.getDate("ModifiedDate"));
            e.setCreatedBy(data.getString("CreatedBy"));
            e.setCreatedByName(BeanFactory.getPersonName(data.getString("OwnFirstName"), data.getString("OwnLastName")));
            e.setModifiedBy(data.getString("ModifiedBy"));
            e.setModifiedByName(BeanFactory.getPersonName(data.getString("ModFirstName"), data.getString("ModLastName")));

            e.setTitleMonth(data.getString("LotTitleMonth"));
            e.setTitleYear(data.getString("LotTitleYear"));

            ApartmentStatusHistory s = new ApartmentStatusHistory();
            s.setStatus(data.getString("CurrentStatus"));
            s.setStatusID(data.getString("CurrentStatusID"));
            e.setCurrentStatus(s);

            if (StringUtils.isNotBlank(data.getString("CampaignID"))) {
                SimpleCampaign sc = new SimpleCampaign() {
                };
                sc.setCampaignID(data.getString("CampaignID"));
                sc.setName(data.getString("CampaignName"));
                e.setCampaign(sc);
            }

            if (repr != BeanRepresentation.Simple) {
                e.setDetails(PlanService.getInstance().getProductDetails(data.getConnection(), null, data.getData("ProductID"), "Land+LandDetails"));
            } else {
                LOGGER.debug("not setting land details for representation {}", repr != null ? repr.name() : "null");
            }
        }

        GenRow maps = new GenRow();
        maps.setViewSpec("LinkedDocumentView");
        //maps.setConnection(data.getConnection());
        maps.setParameter("ProductID", data.getData("ProductID"));
        maps.setParameter("DocumentSubType", "MasterPlan");
        maps.setParameter("LinkedDocumentDocument.DocumentSubType", "MasterPlan");

        if (maps.getString("ProductID").length() > 0) {
            maps.doAction("search");

            maps.getResults();

            if (maps.getNext()) {
                InteractiveMap im = new InteractiveMap();
                im.setMajorID(data.getString("EstateProductID"));
                im.setMinorID(data.getString("StageProductID"));
                im.setProductType("Land");

                do {
                    if (maps.getString("FileName").toLowerCase().endsWith(".jpg") || maps.getString("FileName").toLowerCase().endsWith(".png")) {
                        im.setMapImage(loadDocument(maps.getData("DocumentID")));
                    } else if (maps.getString("FileName").toLowerCase().endsWith(".map")) {
                        im.loadMapHTML(maps.getString("FilePath"));
                    }

                } while (maps.getNext());

                e.setInteractiveMap(im);
            }
        }

        maps.close();


        return e;
    }

    public static Plan external(PlanEntity data, BeanRepresentation repr) {
        LOGGER.debug("Plan external(data, repr={})", repr != null ? repr.name() : "null");
        Plan e = new Plan();
        e.setPlanID(data.getField("ProductID"));
        e.setOriginalPlanID(data.getField("CopiedFromProductID"));
        e.setName(data.getField("Name"));
        e.setProductNum(data.getField("ProductNum"));
        e.setDescription(data.getField("Description"));
        e.setSummary(data.getField("Summary"));
        e.setSubType(data.getField("ProductSubType"));
        e.setMarketingStatus(data.getField("MarketingStatus"));
        e.setActive(data.getField("Active"));

        if (StringUtils.isNotBlank(data.getField("PublishHomeDescription"))) {
            e.setPublishDescription(evaluateHeadline(data, data.getField("PublishHomeDescription")));
        } else {
            e.setPublishDescription(evaluateHeadline(data, data.getField("PublishDescription")));
        }
        if (StringUtils.isNotBlank(data.getField("HomeSummary"))) {
            e.setPublishHeadline(evaluateHeadline(data, data.getField("HomeSummary")));
        } else {
            e.setPublishHeadline(evaluateHeadline(data, data.getField("Summary")));
        }

        if (StringUtils.isNotBlank(data.getField("Image"))) {
            e.setImage(loadDocument(data.getField("Image")));
        }

        String displayPriceType = data.getField("DisplayPriceType");
        if (StringUtils.isBlank(displayPriceType)) {
            displayPriceType = getDisplayPriceType(data.getField("ProductID"));
        }

        if (data.isSet("DripResult")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("DripResult")));
        } else if (data.isSet("TotalCost")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("TotalCost")));
        }

        if (StringUtils.isBlank(e.getDisplayPrice()) && data.isSet("DisplayPrice")) {
            e.setDisplayPrice(data.getField("DisplayPrice"));
        }

        e.setAavalableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setPublishing(BeanFactory.loadPublishing(data.getField("ProductID")));

        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
        e.setModifiedBy(data.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));

        e.setCost(data.getDouble("Cost"));
        e.setGST(data.getDouble("GST"));


        double basePrice = 0, dripPrice = 0, price = 0;
        try {
            price = data.getDouble("DripResult");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            dripPrice = data.getDouble("DripCost");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            basePrice = data.getDouble("TotalCost");
        } catch (IllegalConfigurationException ice) {
        }

        if (price == 0) {
            price = basePrice;
        }

        if (dripPrice != (price - basePrice)) {
            LOGGER.warn("Product {} dripPrice != (price - basePrice)", data.getField("ProductID"));
        }

        e.setTotalCost(basePrice);
        e.setDripPrice(dripPrice);
        e.setPrice(price);

        List<FacadeService.Facade> facades = FacadeService.getInstance().getPlanFacades(data.getConnection(), UserService.getInstance().getSimpleUser(data
                .getConnection()), data.getField("ProductID"));

        List<com.sok.runway.externalInterface.beans.cms.properties.Facade> facadeList = new ArrayList<com.sok.runway.externalInterface.beans.cms.properties
                .Facade>(facades.size());
        for (FacadeService.Facade facade : facades) {
            facadeList.add(BeanFactory.external(facade, repr));
        }

        e.setFacades(facadeList);

        PlanStatusHistory s = new PlanStatusHistory();
        s.setStatus(data.getField("CurrentStatus"));
        s.setStatusID(data.getField("CurrentStatusID"));
        e.setCurrentStatus(s);

        SimpleHome h = new SimpleHome() {
        };
        h.setHomeID(data.getField("HomeProductID"));
        h.setName(data.getField("HomeName"));
        e.setHome(h);

        SimpleRange r = new SimpleRange() {
        };
        r.setRangeID(data.getField("RangeProductID"));
        r.setName(data.getField("RangeName"));
        h.setRange(r);

        SimpleBrand b = new SimpleBrand() {
        };
        b.setBrandID(data.getField("BrandProductID"));
        b.setName(data.getField("BrandName"));
        r.setBrand(b);

        if (repr == BeanRepresentation.Full || repr == BeanRepresentation.Extended) {
            LOGGER.debug("adding full details");
            e.setDetails(PlanService.getInstance().getProductDetails(data.getConnection(), null, data.getPrimaryKey(), "HomePlan+HomePlanDetails+Home Plan"));
        } else {
            LOGGER.debug("not setting plan details for representation {}", repr != null ? repr.name() : "null");
        }

        return e;
    }

    public static RealEstate external(com.sok.runway.crm.cms.properties.ExistingProperty data, BeanRepresentation repr) {
        RealEstate e = new RealEstate();
        e.setRealEstateID(data.getData("ProductID"));
        e.setName(data.getData("Name"));
        e.setActive(data.getData("Active"));
        e.setProductNum(data.getData("ProductNum"));
        e.setDescription(data.getData("Description"));

        e.setMarketingStatus(data.getData("MarketingStatus"));

        //if(repr == BeanRepresentation.Full) {
        e.setPublishHeadline(evaluateHeadline(data, data.getData("Summary")));
        e.setPublishDescription(evaluateHeadline(data, data.getData("PublishDescription")));
        e.setPublishingStatus(data.getData("PublishingStatus"));
        //}

        String displayPriceType = data.getString("DisplayPriceType");
        if (StringUtils.isBlank(displayPriceType)) {
            displayPriceType = getDisplayPriceType(data.getData("ProductID"));
        }

        if (data.isSet("DripResult")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("DripResult")));
        } else if (data.isSet("TotalCost")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("TotalCost")));
        }

        if (StringUtils.isBlank(e.getDisplayPrice()) && data.isSet("DisplayPrice")) {
            e.setDisplayPrice(data.getString("DisplayPrice"));
        }

        e.setAavalableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setGroups(getProductGroups(data.getConnection(), e.getRealEstateID()));

        e.setPublishing(BeanFactory.loadPublishing(data.getData("ProductID")));

        e.setSubType(data.getData("ProductSubType"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getData("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getData("OwnFirstName"), data.getData("OwnLastName")));
        e.setModifiedBy(data.getData("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getData("ModFirstName"), data.getData("ModLastName")));
        e.setAddress(BeanFactory.getAddressFromTableData(data, ""));

        //e.setCost(data.getDouble("Cost"));
        //e.setGST(data.getDouble("GST"));
        e.setBasePrice(data.getDouble("TotalCost"));
        e.setDripPrice(data.getDouble("DripCost"));
        e.setPrice(data.getDouble("DripResult"));

        if (StringUtils.isNotBlank(data.getData("Image"))) {
            e.setImage(loadDocument(data.getData("Image")));
        }
        if (StringUtils.isNotBlank(data.getData("ThumbnailImage"))) {
            e.setThumbnailImage(loadDocument(data.getData("ThumbnailImage")));
        }

        GenRow pp = new GenRow();
        try {
            pp.setConnection(data.getConnection());
            pp.setViewSpec("ProductProductLinkedView");
            pp.setParameter("ProductID", data.getData("ProductID"));
            if (pp.isSet("ProductID")) {
                pp.getResults();
            }
            while (pp.getNext()) {
                if ("Land".equals(pp.getData("ProductType"))) {
                    LotEntity le = new LotEntity(data.getConnection(), pp.getData("LinkedProductID"));
                    if (le.isLoaded()) {
                        e.setLot(external(le, repr));
                    }
                } else if ("Home Plan".equals(pp.getData("ProductType"))) {
                    PlanEntity pe = new PlanEntity(data.getConnection(), pp.getData("LinkedProductID"));
                    if (pe.isLoaded()) {
                        e.setPlan(external(pe, repr));
                    }
                }
            }
        } finally {
            pp.close();
        }

        RealEstateStatusHistory s = new RealEstateStatusHistory();
        s.setStatus(data.getData("CurrentStatus"));
        s.setStatusID(data.getData("CurrentStatusID"));
        e.setCurrentStatus(s);

        if (repr != BeanRepresentation.Simple) {
            e.setDetails(PlanService.getInstance().getProductDetails(data.getConnection(), null, data.getData("ProductID")));
        }

        return e;
    }


    public static RealEstate external(RealEstateEntity data, BeanRepresentation repr) {
        RealEstate e = new RealEstate();

        e.setRealEstateID(data.getProductID());
        e.setName(data.getField("Name"));
        e.setActive(data.getField("Active"));
        e.setProductNum(data.getField("ProductNum"));
        e.setDescription(data.getField("Description"));

        //if(repr == BeanRepresentation.Extended) {
        e.setPublishHeadline(evaluateHeadline(data, data.getField("Summary")));
        e.setPublishDescription(evaluateHeadline(data, data.getField("PublishDescription")));
        e.setPublishingStatus(data.getField("PublishingStatus"));
        e.setMarketingStatus(data.getField("MarketingStatus"));
        //}

        String displayPriceType = data.getField("DisplayPriceType");
        if (StringUtils.isBlank(displayPriceType)) {
            displayPriceType = getDisplayPriceType(data.getField("ProductID"));
        }

        if (data.isSet("DripResult")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("DripResult")));
        } else if (data.isSet("TotalCost")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("TotalCost")));
        }

        if (StringUtils.isBlank(e.getDisplayPrice()) && data.isSet("DisplayPrice")) {
            e.setDisplayPrice(data.getField("DisplayPrice"));
        }

        e.setAavalableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setPublishing(BeanFactory.loadPublishing(data.getField("ProductID")));

        e.setSubType(data.getField("ProductSubType"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
        e.setModifiedBy(data.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));

        e.setAddress(BeanFactory.getAddressFromTableData(data.getRecord(), ""));

        //e.setCost(data.getDouble("Cost"));
        //e.setGST(data.getDouble("GST"));
        e.setBasePrice(data.getDouble("TotalCost"));
        e.setDripPrice(data.getDouble("DripCost"));
        e.setPrice(data.getDouble("DripResult"));

        if (StringUtils.isNotBlank(data.getField("Image"))) {
            e.setImage(loadDocument(data.getField("Image")));
        }
        if (StringUtils.isNotBlank(data.getField("ThumbnailImage"))) {
            e.setThumbnailImage(loadDocument(data.getField("ThumbImage")));
        }

        GenRow pp = new GenRow();
        try {
            pp.setConnection(data.getConnection());
            pp.setViewSpec("ProductProductLinkedView");
            pp.setParameter("ProductID", data.getField("ProductID"));
            if (pp.isSet("ProductID")) {
                pp.getResults();
            }
            while (pp.getNext()) {
                if ("Land".equals(pp.getData("ProductType"))) {
                    LotEntity le = new LotEntity(data.getConnection(), pp.getData("LinkedProductID"));
                    if (le.isLoaded()) {
                        e.setLot(external(le, repr));
                    }
                } else if ("Home Plan".equals(pp.getData("ProductType"))) {
                    PlanEntity pe = new PlanEntity(data.getConnection(), pp.getData("LinkedProductID"));
                    if (pe.isLoaded()) {
                        e.setPlan(external(pe, repr));
                    }
                }
            }
        } finally {
            pp.close();
        }

        RealEstateStatusHistory s = new RealEstateStatusHistory();
        s.setStatus(data.getField("CurrentStatus"));
        s.setStatusID(data.getField("CurrentStatusID"));
        e.setCurrentStatus(s);

        if (repr != BeanRepresentation.Simple) {
            e.setDetails(PlanService.getInstance().getProductDetails(data.getConnection(), null, data.getPrimaryKey()));
        }
        e.setGroups(getProductGroups(data.getConnection(), data.getPrimaryKey()));

        return e;
    }

    public static Brand external(com.sok.runway.crm.cms.properties.Brand data) {
        Brand e = new Brand();
        e.setBrandID(data.getData("ProductID"));
        e.setName(data.getData("Name"));
        e.setProductNum(data.getData("ProductNum"));
        e.setDescription(data.getData("Description"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getData("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getData("OwnFirstName"), data.getData("OwnLastName")));
        e.setModifiedBy(data.getData("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getData("ModFirstName"), data.getData("ModLastName")));
        e.setURL(data.getData("URL"));
        if (StringUtils.isNotBlank(data.getData("ThumbnailImage"))) {
            e.setThumbnailImage(loadDocument(data.getData("ThumbnailImage")));
        }
        return e;
    }

    public static Range external(com.sok.runway.crm.cms.properties.Range data) {
        Range e = new Range();
        e.setRangeID(data.getData("ProductID"));
        e.setName(data.getData("Name"));
        e.setProductNum(data.getData("ProductNum"));
        e.setRangeColor(data.getData("RangeColorHex"));
        e.setDescription(data.getData("Description"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getData("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getData("OwnFirstName"), data.getData("OwnLastName")));
        e.setModifiedBy(data.getData("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getData("ModFirstName"), data.getData("ModLastName")));
        e.setURL(data.getData("URL"));
        if (StringUtils.isNotBlank(data.getData("ThumbnailImage"))) {
            e.setThumbnailImage(loadDocument(data.getData("ThumbnailImage")));
        }
        return e;
    }

    public static Brand external(BrandEntity data) {
        Brand e = new Brand();
        e.setBrandID(data.getField("ProductID"));
        e.setName(data.getField("Name"));
        e.setProductNum(data.getField("ProductNum"));
        e.setDescription(data.getField("Description"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
        e.setModifiedBy(data.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));
        e.setName(data.getField("Name"));
        e.setURL(data.getField("URL"));
        if (StringUtils.isNotBlank(data.getField("ThumbnailImage"))) {
            e.setThumbnailImage(loadDocument(data.getField("ThumbnailImage")));
        }
        return e;
    }

    public static Range external(RangeEntity data) {
        Range e = new Range();
        e.setRangeID(data.getField("ProductID"));
        e.setName(data.getField("Name"));
        e.setProductNum(data.getField("ProductNum"));
        e.setRangeColor(data.getField("RangeColorHex"));
        e.setDescription(data.getField("Description"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
        e.setModifiedBy(data.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));
        e.setName(data.getField("Name"));
        e.setURL(data.getField("URL"));
        if (StringUtils.isNotBlank(data.getField("ThumbnailImage"))) {
            e.setThumbnailImage(loadDocument(data.getField("ThumbnailImage")));
        }
        return e;
    }

    public static BuildingStage external(BuildingStageEntity se) {
        BuildingStage e = new BuildingStage();
        e.setBuildingStageID(se.getField("ProductID"));
        e.setName(se.getField("Name"));
        e.setProductNum(se.getField("ProductNum"));
        e.setDescription(se.getField("Description"));

        Building es = new Building();
        es.setBuildingID(se.getField("BuildingProductID"));
        es.setName(se.getField("BuildingName"));
        e.setBuilding(es);

        e.setAddress(BeanFactory.getAddressFromTableData(se.getRecord(), ""));
        e.setCreatedDate(se.getDate("CreatedDate"));
        e.setModifiedDate(se.getDate("ModifiedDate"));
        e.setCreatedBy(se.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(se.getField("OwnFirstName"), se.getField("OwnLastName")));
        e.setModifiedBy(se.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(se.getField("ModFirstName"), se.getField("ModLastName")));
        e.setError(se.getRecord().getError());

        GenRow maps = new GenRow();
        maps.setViewSpec("LinkedDocumentView");
        //maps.setConnection(se.getConnection());
        maps.setParameter("ProductID", se.getField("ProductID"));
        maps.setParameter("DocumentSubType", "MasterPlan");
        maps.setParameter("LinkedDocumentDocument.DocumentSubType", "MasterPlan");

        if (maps.getString("ProductID").length() > 0) {
            maps.doAction("search");

            maps.getResults();

            if (maps.getNext()) {
                InteractiveMap im = new InteractiveMap();
                im.setMajorID(se.getField("EstateProductID"));
                im.setMinorID(se.getField("ProductID"));
                im.setProductType("Land");

                do {
                    if (maps.getString("FileName").toLowerCase().endsWith(".jpg") || maps.getString("FileName").toLowerCase().endsWith(".png")) {
                        im.setMapImage(loadDocument(maps.getData("DocumentID")));
                    } else if (maps.getString("FileName").toLowerCase().endsWith(".map")) {
                        im.loadMapHTML(maps.getString("FilePath"));
                    }

                } while (maps.getNext());

                e.setInteractiveMap(im);
            }
        }

        maps.clear();
        maps.setParameter("ProductID", se.getField("ProductID"));
        maps.setParameter("DocumentSubType", "MasterPlanZoom");
        maps.setParameter("LinkedDocumentDocument.DocumentSubType", "MasterPlanZoom");

        if (maps.getString("ProductID").length() > 0) {
            maps.doAction("search");

            maps.getResults();

            if (maps.getNext()) {
                InteractiveMap im = new InteractiveMap();
                im.setMajorID(se.getField("EstateProductID"));
                im.setMinorID(se.getField("ProductID"));
                im.setProductType("Land");

                do {
                    if (maps.getString("FileName").toLowerCase().endsWith(".jpg") || maps.getString("FileName").toLowerCase().endsWith(".png")) {
                        im.setMapImage(loadDocument(maps.getData("DocumentID")));
                    } else if (maps.getString("FileName").toLowerCase().endsWith(".map")) {
                        im.loadMapHTML(maps.getString("FilePath"));
                    }

                } while (maps.getNext());

                e.setInteractiveZoomMap(im);
            }
        }

        maps.close();

        try {
            e.setEstSettlementDate(se.getDate("EstSettlementDate"));
        } catch (IllegalConfigurationException ice) {
        }

        return e;
    }

    public static com.sok.runway.externalInterface.beans.cms.properties.Plan external(HomePlan data) {
        return external(data, BeanRepresentation.Simple);
    }

    public static com.sok.runway.externalInterface.beans.cms.properties.Plan external(HomePlan data, BeanRepresentation repr) {
        com.sok.runway.externalInterface.beans.cms.properties.Plan e = new com.sok.runway.externalInterface.beans.cms.properties.Plan();
        e.setPlanID(data.getData("ProductID"));
        e.setName(data.getData("Name"));
        e.setProductNum(data.getData("ProductNum"));
        e.setDescription(data.getData("Description"));
        //e.setPublishingStatus(data.getData("PublishingStatus"));

        if (StringUtils.isNotBlank(data.getData("PublishHomeDescription"))) {
            e.setPublishDescription(evaluateHeadline(data, data.getData("PublishHomeDescription")));
        } else {
            e.setPublishDescription(evaluateHeadline(data, data.getData("PublishDescription")));
        }
        if (StringUtils.isNotBlank(data.getData("HomeSummary"))) {
            e.setPublishHeadline(evaluateHeadline(data, data.getData("HomeSummary")));
        } else {
            e.setPublishHeadline(evaluateHeadline(data, data.getData("Summary")));
        }

        e.setSubType(data.getString("ProductSubType"));
        e.setMarketingStatus(data.getString("MarketingStatus"));

        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        e.setCreatedBy(data.getData("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(data.getData("OwnFirstName"), data.getData("OwnLastName")));
        e.setModifiedBy(data.getData("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(data.getData("ModFirstName"), data.getData("ModLastName")));
        e.setActive(data.getData("Active"));

//		if (data.isSet("DisplayPrice")) {
//			e.setDisplayPrice(data.getString("DisplayPrice"));
//		} else if (data.isSet("DripResult")) {
//			e.setDisplayPrice(NumberFormat.getCurrencyInstance().format(data.getDouble("DripResult")));
//		} else if (data.isSet("TotalCost")) {
//			e.setDisplayPrice(NumberFormat.getCurrencyInstance().format(data.getDouble("TotalCost")));
//		}

        String displayPriceType = data.getString("DisplayPriceType");
        if (StringUtils.isBlank(displayPriceType)) {
            displayPriceType = getDisplayPriceType(data.getData("ProductID"));
        }

        if (data.isSet("DripResult")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("DripResult")));
        } else if (data.isSet("TotalCost")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("TotalCost")));
        }

        if (StringUtils.isBlank(e.getDisplayPrice()) && data.isSet("DisplayPrice")) {
            e.setDisplayPrice(data.getString("DisplayPrice"));
        }

        e.setAavalableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setPublishing(BeanFactory.loadPublishing(data.getData("ProductID")));

        if (StringUtils.isNotBlank(data.getData("Image"))) {
            e.setImage(loadDocument(data.getData("Image")));
        }

        if (repr != BeanRepresentation.Simple) {
            e.setDetails(PlanService.getInstance().getProductDetails(data.getConnection(), null, data.getData("ProductID")));
        }
        Facade facades = data.getFacades();

        List<com.sok.runway.externalInterface.beans.cms.properties.Facade> facadeList = new ArrayList<com.sok.runway.externalInterface.beans.cms.properties
                .Facade>(facades.getSize());
        while (facades.getNext()) {
            facadeList.add(BeanFactory.external(facades));
        }

        facades.close();
        e.setFacades(facadeList);

		/*
		GenRow rep = new GenRow();
		rep.setViewSpec("ProductSecurityGroupView");
		rep.setConnection(data.getConnection());
		rep.setParameter("ProductID",e.getPlanID());
		rep.setParameter("RepUserID", "!EMPTY");
		if(rep.isSet("ProductID")) rep.doAction(GenerationKeys.SELECTFIRST);
		//e.setDetailPdfUrl(String.format("%1$s/property.pdf?-api=true&ProductID=%2$s&-variant=SinglePackage&v=%3$d&LocationID=%4$s&RepUserID=%5$s",
		InitServlet.getSystemParam("URLHome"), e.getPackageID(), e.getModifiedDate() != null ? e.getModifiedDate().getTime() : new Double(Math.random()
		*100000).intValue(), e.getLocation() != null ? e.getLocation().getLocationID() : "", rep.getData("RepUserID")));
		*/

        double basePrice = 0, dripPrice = 0, price = 0;
        try {
            price = data.getDouble("DripResult");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            dripPrice = data.getDouble("DripCost");
        } catch (IllegalConfigurationException ice) {
        }

        try {
            basePrice = data.getDouble("TotalCost");
        } catch (IllegalConfigurationException ice) {
        }

        if (price == 0) {
            price = basePrice;
        }

        if (dripPrice != (price - basePrice)) {
            LOGGER.warn("Product {} dripPrice != (price - basePrice)", data.getData("ProductID"));
        }

        e.setDripPrice(dripPrice);
        e.setPrice(price);

        PlanStatusHistory s = new PlanStatusHistory();
        s.setStatus(data.getData("CurrentStatus"));
        s.setStatusID(data.getData("CurrentStatusID"));
        e.setCurrentStatus(s);

        return e;
    }

    public static com.sok.runway.externalInterface.beans.cms.properties.PlanCheck externalCheck(HomePlan data) {
        return externalCheck(data, BeanRepresentation.Simple);
    }

    public static com.sok.runway.externalInterface.beans.cms.properties.PlanCheck externalCheck(HomePlan data, BeanRepresentation repr) {
        com.sok.runway.externalInterface.beans.cms.properties.PlanCheck e = new com.sok.runway.externalInterface.beans.cms.properties.PlanCheck();
        e.setPlanID(data.getData("ProductID"));
        e.setName(data.getData("Name"));
        e.setCreatedDate(data.getDate("CreatedDate"));
        e.setModifiedDate(data.getDate("ModifiedDate"));
        if (data.isSet("DripModifiedDate")) {
            e.setDripModifiedDate(data.getDate("DripModifiedDate"));
        }
        e.setActive(data.getData("Active"));

        e.setAavalableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setPublishing(BeanFactory.loadPublishing(data.getData("ProductID")));

        PlanStatusHistory s = new PlanStatusHistory();
        s.setStatus(data.getData("CurrentStatus"));
        s.setStatusID(data.getData("CurrentStatusID"));
        e.setCurrentStatus(s);

        return e;
    }

    public static Image externalImage(GenRow data) {
        Image i = new Image();
        i.setActive("Y");
        i.setDocumentID(data.getData("DocumentID"));

        i.setCreatedDate(data.getDate("CreatedDate"));
        i.setCreatedBy(data.getString("CreatedBy"));
        i.setCreatedByName(new PersonName(data.getString("OwnFirstName"), data.getString("OwnLastName")));

        i.setCaption(data.getString("DocumentCaption"));
        i.setDescription(data.getString("Description"));
        i.setFileName(data.getString("FileName"));
        i.setFilePath(data.getString("FilePath"));

        i.setType(data.getString("DocumentType"));
        i.setSubType(data.getString("DocumentSubType"));

        return i;
    }

    public static Album externalAlbum(GenRow data) {
        Album a = new Album();
        a.setActive("Y");
        a.setAlbumID(data.getData("GalleryID"));
        a.setName(data.getData("GalleryName"));

        a.setCreatedDate(data.getDate("CreatedDate"));
        a.setCreatedBy(data.getString("CreatedBy"));
        //a.setCreatedByName(new PersonName(data.getString("OwnFirstName"), data.getString("OwnLastName")));

        a.setModifiedDate(data.getDate("ModifiedDate"));
        a.setModifiedBy(data.getString("ModifiedBy"));
        //a.setModifiedByName(new PersonName(data.getString("ModFirstName"), data.getString("ModLastName")));

        a.setDescription(data.getString("Description"));
        a.setAlbumType(data.getData("AlbumType"));

        String publisher = data.getData("Publisher");
        if (publisher != null && publisher.length() > 0) {
            if (publisher.startsWith("+")) {
                publisher = publisher.substring(1);
            }
            List<String> publishers = new ArrayList<String>();
            String[] pub = publisher.split("\\+");
            for (int s = 0; s < pub.length; ++s) {
                publishers.add(pub[s]);
            }
            a.setPublisher(publishers);
        }

        return a;
    }

    public static Apartment external(ApartmentEntity data) {
        return external(data, BeanRepresentation.Simple);
    }

    public static Apartment external(ApartmentEntity data, BeanRepresentation repr) {
        Apartment e = new Apartment();
        e.setApartmentID(data.getField("ProductID"));
        e.setName(data.getField("Name"));
        e.setCost(data.getDouble("Cost"));
        e.setGST(data.getDouble("GST"));
        e.setTotalCost(data.getDouble("TotalCost"));
        //e.setPrice(data.getDouble("DripResult") != 0 ? data.getDouble("DripResult") : e.getTotalCost());
        e.setSubType(data.getField("ProductSubType"));
        e.setMarketingStatus(data.getField("MarketingStatus"));
        e.setActive(data.getField("Active"));

        String displayPriceType = data.getField("DisplayPriceType");
        if (StringUtils.isBlank(displayPriceType)) {
            displayPriceType = getDisplayPriceType(data.getField("ProductID"));
        }

        if (data.isSet("DripResult")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("DripResult")));
        } else if (data.isSet("TotalCost")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("TotalCost")));
        }

        if (StringUtils.isBlank(e.getDisplayPrice()) && data.isSet("DisplayPrice")) {
            e.setDisplayPrice(data.getField("DisplayPrice"));
        }

        e.setAvailableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));


        if (StringUtils.isNotBlank(data.getField("Image"))) {
            e.setImage(loadDocument(data.getField("Image")));
        }

        e.setBuildingStage(new BuildingStage());
        e.getBuildingStage().setBuildingStageID(data.getField("BuildingStageProductID"));
        e.getBuildingStage().setName(data.getField("BuildingStageName"));

        Building es = new Building();
        es.setBuildingID(data.getField("BuildingProductID"));
        es.setName(data.getField("BuildingName"));
        e.getBuildingStage().setBuilding(es);

        e.setRegionID(data.getField("RegionID"));
        e.setLocationID(data.getField("LocationID"));

        if (data.isSet("LocationID")) {
            e.setLocation(LocationService.getInstance().getLocation(null, data.getField("LocationID")));
        }
        if (data.isSet("RegionID")) {
            e.setRegion(LocationService.getInstance().getLocation(null, data.getField("RegionID")));
        }

        e.setPublishing(loadPublishing(data.getField("ProductID")));

        e.setDescription(data.getField("Description"));
        e.setPublishDescription(evaluateHeadline(data, data.getField("PublishDescription")));
        e.setPublishHeadline(evaluateHeadline(data, data.getField("Summary")));

        if (repr != BeanRepresentation.Simple) {
            es.setPublishName(data.getField("BuildingPublishName"));
            e.setProductNum(data.getField("ProductNum"));


            e.setAddress(BeanFactory.getAddressFromTableData(data.getRecord(), ""));
            e.setCreatedDate(data.getDate("CreatedDate"));
            e.setModifiedDate(data.getDate("ModifiedDate"));
            e.setCreatedBy(data.getField("CreatedBy"));
            e.setCreatedByName(BeanFactory.getPersonName(data.getField("OwnFirstName"), data.getField("OwnLastName")));
            e.setModifiedBy(data.getField("ModifiedBy"));
            e.setModifiedByName(BeanFactory.getPersonName(data.getField("ModFirstName"), data.getField("ModLastName")));

            e.setTitleMonth(data.getField("LotTitleMonth"));
            e.setTitleYear(data.getField("LotTitleYear"));

            ApartmentStatusHistory s = new ApartmentStatusHistory();
            s.setStatus(data.getField("CurrentStatus"));
            s.setStatusID(data.getField("CurrentStatusID"));
            e.setCurrentStatus(s);

            if (StringUtils.isNotBlank(data.getField("CampaignID"))) {
                SimpleCampaign sc = new SimpleCampaign() {
                };
                sc.setCampaignID(data.getField("CampaignID"));
                sc.setName(data.getField("CampaignName"));
                e.setCampaign(sc);
            }

            e.setDetails(PlanService.getInstance().getProductDetails(data.getConnection(), null, data.getPrimaryKey(), "Apartment%"));
        }

        GenRow maps = new GenRow();
        maps.setViewSpec("LinkedDocumentView");
        //maps.setConnection(data.getConnection());
        maps.setParameter("ProductID", data.getField("ProductID"));
        maps.setParameter("DocumentSubType", "MasterPlan");
        maps.setParameter("LinkedDocumentDocument.DocumentSubType", "MasterPlan");

        if (maps.getString("ProductID").length() > 0) {
            maps.doAction("search");

            maps.getResults();

            if (maps.getNext()) {
                InteractiveMap im = new InteractiveMap();
                im.setMajorID(data.getField("EstateProductID"));
                im.setMinorID(data.getField("StageProductID"));
                im.setProductType("Land");

                do {
                    if (maps.getString("FileName").toLowerCase().endsWith(".jpg") || maps.getString("FileName").toLowerCase().endsWith(".png")) {
                        im.setMapImage(loadDocument(maps.getData("DocumentID")));
                    } else if (maps.getString("FileName").toLowerCase().endsWith(".map")) {
                        im.loadMapHTML(maps.getString("FilePath"));
                    }

                } while (maps.getNext());

                e.setInteractiveMap(im);
            }
        }

        maps.close();
        return e;
    }

    public static Lot external(com.sok.runway.crm.cms.properties.Land data) {
        return external(data, BeanRepresentation.Simple);
    }

    public static Lot external(com.sok.runway.crm.cms.properties.Land data, BeanRepresentation repr) {
        Lot e = new Lot();
        e.setLotID(data.getData("ProductID"));
        e.setName(data.getString("Name"));
        e.setCost(data.getDouble("Cost"));
        e.setGST(data.getDouble("GST"));
        e.setTotalCost(data.getDouble("TotalCost"));
        //e.setPrice(data.getDouble("DripResult") != 0 ? data.getDouble("DripResult") : e.getTotalCost());
        e.setSubType(data.getString("ProductSubType"));
        e.setMarketingStatus(data.getString("MarketingStatus"));

        String displayPriceType = data.getString("DisplayPriceType");
        if (StringUtils.isBlank(displayPriceType)) {
            displayPriceType = getDisplayPriceType(data.getData("ProductID"));
        }

        if (data.isSet("DripResult")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("DripResult")));
        } else if (data.isSet("TotalCost")) {
            e.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, data.getDouble("TotalCost")));
        }

        if (StringUtils.isBlank(e.getDisplayPrice()) && data.isSet("DisplayPrice")) {
            e.setDisplayPrice(data.getString("DisplayPrice"));
        }

        e.setStage(new Stage());
        e.getStage().setStageID(data.getString("StageProductID"));
        e.getStage().setName(data.getString("StageName"));

        Estate es = new Estate();
        es.setEstateID(data.getString("EstateProductID"));
        es.setName(data.getString("EstateName"));
        e.getStage().setEstate(es);

        e.setRegionID(data.getString("RegionID"));
        e.setLocationID(data.getString("LocationID"));

        e.setAvailableDate(data.getDate("AvailableDate"));
        e.setExpiryDate(data.getDate("ExpiryDate"));

        e.setGroups(getProductGroups(data.getConnection(), e.getLotID()));

        e.setPublishing(BeanFactory.loadPublishing(data.getData("ProductID")));

        if (data.isSet("LocationID")) {
            e.setLocation(LocationService.getInstance().getLegacyLocation(null, data.getString("LocationID")));
        }
        if (data.isSet("RegionID")) {
            e.setRegion(LocationService.getInstance().getLegacyLocation(null, data.getString("RegionID")));
        }

        if (StringUtils.isNotBlank(data.getData("Image"))) {
            e.setImage(loadDocument(data.getData("Image")));
        } else if (StringUtils.isNotBlank(data.getData("EstateImage"))) {
            e.setImage(loadDocument(data.getData("EstateImage")));
        }

        if (StringUtils.isNotBlank(data.getData("ThumbnailImage"))) {
            e.setThumbnail(loadDocument(data.getData("ThumbnailImage")));
        } else if (StringUtils.isNotBlank(data.getData("EstateThumbnailImage"))) {
            e.setThumbnail(loadDocument(data.getData("EstateThumbnailImage")));
        }

        e.setProductNum(data.getString("ProductNum"));
        e.setDescription(data.getString("Description"));
        e.setPublishDescription(evaluateHeadline(data, data.getData("PublishDescription")));
        e.setPublishHeadline(evaluateHeadline(data, data.getData("Summary")));

        if (repr != BeanRepresentation.Simple) {
            es.setPublishName(data.getString("EstatePublishName"));

            e.setAddress(BeanFactory.getAddressFromTableData(data, ""));
            e.setCreatedDate(data.getDate("CreatedDate"));
            e.setModifiedDate(data.getDate("ModifiedDate"));
            e.setCreatedBy(data.getString("CreatedBy"));
            e.setCreatedByName(BeanFactory.getPersonName(data.getString("OwnFirstName"), data.getString("OwnLastName")));
            e.setModifiedBy(data.getString("ModifiedBy"));
            e.setModifiedByName(BeanFactory.getPersonName(data.getString("ModFirstName"), data.getString("ModLastName")));

            e.setTitleMonth(data.getString("LotTitleMonth"));
            e.setTitleYear(data.getString("LotTitleYear"));

            LotStatusHistory s = new LotStatusHistory();
            s.setStatus(data.getString("CurrentStatus"));
            s.setStatusID(data.getString("CurrentStatusID"));
            e.setCurrentStatus(s);

            if (StringUtils.isNotBlank(data.getString("CampaignID"))) {
                SimpleCampaign sc = new SimpleCampaign() {
                };
                sc.setCampaignID(data.getString("CampaignID"));
                sc.setName(data.getString("CampaignName"));
                e.setCampaign(sc);
            }

            if (repr != BeanRepresentation.Simple) {
                e.setDetails(PlanService.getInstance().getProductDetails(data.getConnection(), null, data.getData("ProductID"), "Land+LandDetails"));
            } else {
                LOGGER.debug("not setting land details for representation {}", repr != null ? repr.name() : "null");
            }
        }

        GenRow maps = new GenRow();
        maps.setViewSpec("LinkedDocumentView");
        //maps.setConnection(data.getConnection());
        maps.setParameter("ProductID", data.getData("ProductID"));
        maps.setParameter("DocumentSubType", "MasterPlan");
        maps.setParameter("LinkedDocumentDocument.DocumentSubType", "MasterPlan");

        if (maps.getString("ProductID").length() > 0) {
            maps.doAction("search");

            maps.getResults();

            if (maps.getNext()) {
                InteractiveMap im = new InteractiveMap();
                im.setMajorID(data.getString("EstateProductID"));
                im.setMinorID(data.getString("StageProductID"));
                im.setProductType("Land");

                do {
                    if (maps.getString("FileName").toLowerCase().endsWith(".jpg") || maps.getString("FileName").toLowerCase().endsWith(".png")) {
                        im.setMapImage(loadDocument(maps.getData("DocumentID")));
                    } else if (maps.getString("FileName").toLowerCase().endsWith(".map")) {
                        im.loadMapHTML(maps.getString("FilePath"));
                    }

                } while (maps.getNext());

                e.setInteractiveMap(im);
            }
        }

        maps.close();


        return e;
    }

    public static SimpleDocument loadDocument(String docimentID) {
        GenRow doc = new GenRow();
        doc.setTableSpec("Documents");
        doc.setParameter("-select0", "*");
        doc.setParameter("DocumentID", docimentID);
        if (doc.getString("DocumentID").length() > 0) {
            doc.doAction("selectfirst");
        }

        if (!doc.isSet("FilePath")) {
            return null;
        }

        SimpleDocument sd = new SimpleDocument();

        sd.setDocumentID(doc.getString("DocumentID"));
        sd.setCaption(doc.getString("DocumentCaption"));
        sd.setDescription(doc.getString("Description"));
        sd.setFileName(doc.getString("FileName"));
        sd.setFilePath(doc.getString("FilePath"));
        sd.setSubType(doc.getString("DocumentSubType"));
        sd.setType(doc.getString("DocumentType"));

        return sd;
    }

    public static SimplePublishing loadPublishing(String productID) {
        GenRow pub = new GenRow();
        pub.setTableSpec("properties/ProductPublishingIndex");
        pub.setParameter("-select0", "*");
        pub.setParameter("ProductID", productID);
        if (pub.getString("ProductID").length() > 0) {
            pub.doAction("selectfirst");
        }

        return new SimplePublishing(pub);
    }

    public static Stage external(StageEntity se) {
        Stage e = new Stage();
        e.setStageID(se.getField("ProductID"));
        e.setName(se.getField("Name"));
        e.setProductNum(se.getField("ProductNum"));
        e.setDescription(se.getField("Description"));

        Estate es = new Estate();
        es.setEstateID(se.getField("EstateProductID"));
        es.setName(se.getField("EstateName"));
        e.setEstate(es);

        e.setAddress(BeanFactory.getAddressFromTableData(se.getRecord(), ""));
        e.setCreatedDate(se.getDate("CreatedDate"));
        e.setModifiedDate(se.getDate("ModifiedDate"));
        e.setCreatedBy(se.getField("CreatedBy"));
        e.setCreatedByName(BeanFactory.getPersonName(se.getField("OwnFirstName"), se.getField("OwnLastName")));
        e.setModifiedBy(se.getField("ModifiedBy"));
        e.setModifiedByName(BeanFactory.getPersonName(se.getField("ModFirstName"), se.getField("ModLastName")));
        e.setError(se.getRecord().getError());

        GenRow maps = new GenRow();
        maps.setViewSpec("LinkedDocumentView");
        //maps.setConnection(se.getConnection());
        maps.setParameter("ProductID", se.getField("ProductID"));
        maps.setParameter("DocumentSubType", "MasterPlan");
        maps.setParameter("LinkedDocumentDocument.DocumentSubType", "MasterPlan");

        if (maps.getString("ProductID").length() > 0) {
            maps.doAction("search");

            maps.getResults();

            if (maps.getNext()) {
                InteractiveMap im = new InteractiveMap();
                im.setMajorID(se.getField("EstateProductID"));
                im.setMinorID(se.getField("ProductID"));
                im.setProductType("Land");

                do {
                    if (maps.getString("FileName").toLowerCase().endsWith(".jpg") || maps.getString("FileName").toLowerCase().endsWith(".png")) {
                        im.setMapImage(loadDocument(maps.getData("DocumentID")));
                    } else if (maps.getString("FileName").toLowerCase().endsWith(".map")) {
                        im.loadMapHTML(maps.getString("FilePath"));
                    }

                } while (maps.getNext());

                e.setInteractiveMap(im);
            }
        }

        maps.setParameter("ProductID", se.getField("ProductID"));
        maps.setParameter("DocumentSubType", "MasterPlanZoom");
        maps.setParameter("LinkedDocumentDocument.DocumentSubType", "MasterPlanZoom");

        if (maps.getString("ProductID").length() > 0) {
            maps.doAction("search");

            maps.getResults();

            if (maps.getNext()) {
                InteractiveMap im = new InteractiveMap();
                im.setMajorID(se.getField("EstateProductID"));
                im.setMinorID(se.getField("ProductID"));
                im.setProductType("Land");

                do {
                    if (maps.getString("FileName").toLowerCase().endsWith(".jpg") || maps.getString("FileName").toLowerCase().endsWith(".png")) {
                        im.setMapImage(loadDocument(maps.getData("DocumentID")));
                    } else if (maps.getString("FileName").toLowerCase().endsWith(".map")) {
                        im.loadMapHTML(maps.getString("FilePath"));
                    }

                } while (maps.getNext());

                e.setInteractiveZoomMap(im);
            }
        }

        maps.close();

        try {
            e.setEstSettlementDate(se.getDate("EstSettlementDate"));
        } catch (IllegalConfigurationException ice) {
        }

        return e;
    }

    private static String getDisplayPriceType(String productID) {
        GenRow row = new GenRow();
        row.setTableSpec("properties/ProductQuickIndex");
        row.setParameter("-select0", "DisplayPriceType");
        row.setParameter("ProductID", productID);
        if (row.isSet("ProductID")) {
            row.doAction("selectfirst");
        }

        return row.getString("DisplayPriceType");
    }

    public static String getPackageCount(String productID) {
        String c = "0";
        if (StringUtils.isNotBlank(productID)) {
            GenRow row = PackageService.getInstance().getPlanSearch(null, null, "LotID=" + productID, true);
            row.doAction(GenerationKeys.SEARCH);
            row.getResults(true);

            LOGGER.debug("getPackageCount {} ", row.getStatement());

            if (row.isSuccessful()) {
                c = Integer.toString(row.getSize());
            }

            row.close();
        }
        return c;
    }

    public static List<ProductSecurityGroup> getProductGroups(Connection con, String productID) {
        List<ProductSecurityGroup> groups = new ArrayList<ProductSecurityGroup>();

        GenRow row = new GenRow();
        row.setViewSpec("ProductSecurityGroupView");
        row.setConnection(con);
        row.setParameter("ProductID", productID);

        if (StringUtils.isNotBlank(productID)) {
            row.doAction("search");
            row.getResults();
        }

        while (row.getNext()) {
            groups.add(new ProductSecurityGroup(row));
        }

        row.close();

        return groups;

    }

    private static String evaluateHeadline(GenRow product, String headline) {
        if (headline != null && headline.length() > 0) {
            if (headline.indexOf("${") >= 0) {
                while (headline.indexOf("${") >= 0) {
                    int start = headline.indexOf("${");
                    int end = headline.indexOf("}");
                    if (start < end && start != -1 && end != -1) {
                        String tag = headline.substring(start + 2, end);
                        String value = product.getString(tag);

                        if (value == null) {
                            value = "";
                        }

                        value = value.replaceAll("\\'", "\\\\'").replaceAll("\\\"", "\\\\\"").replaceAll("\\$", "DOLLAR_SIGN");

                        try {
                            headline = headline.replaceAll((new StringBuilder("\\$\\{")).append(tag).append("\\}").toString(), value);
                            headline = headline.replaceAll("DOLLAR_SIGN", "\\$");
                        } catch (Exception e) {
                            e.printStackTrace();
                            LOGGER.error("evaluateHeadline " + product.getData("ProductID") + " " + tag + " = " + value + "\n-- " + headline);
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        return headline;
    }

    private static String evaluateHeadline(com.sok.runway.crm.Product product, String headline) {
        if (headline != null && headline.length() > 0) {
            if (headline.indexOf("${") >= 0) {
                while (headline.indexOf("${") >= 0) {
                    int start = headline.indexOf("${");
                    int end = headline.indexOf("}");
                    if (start < end && start != -1 && end != -1) {
                        String tag = headline.substring(start + 2, end);
                        String value = product.getField(tag);

                        if (value == null) {
                            value = "";
                        }

                        try {
                            value = value.replaceAll("\\$", "DOLLAR_SIGN");
                            headline = headline.replaceAll((new StringBuilder("\\$\\{")).append(tag).append("\\}").toString(), value);
                            headline = headline.replaceAll("DOLLAR_SIGN", "\\$");
                        } catch (Exception e) {
                            LOGGER.error("evaluateHeadline " + product.getField("ProductID") + " " + tag + " = " + value + "\n-- " + headline);
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        return headline;
    }


}