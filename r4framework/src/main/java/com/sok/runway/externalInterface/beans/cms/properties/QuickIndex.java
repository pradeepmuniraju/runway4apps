package com.sok.runway.externalInterface.beans.cms.properties;

import java.text.NumberFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.GenRow;
import com.sok.framework.RunwayUtil;
import com.sok.runway.externalInterface.beans.SimpleDocument;

@XmlAccessorType(XmlAccessType.NONE)
@XmlSeeAlso(Lot.class)
@XmlRootElement(name="QuickIndex")
//@XmlType(propOrder={"ProductID","Name","Active","Status", "PreAvailableDate", "AvailableDate", "ExpiryDate" })		//MarketingStatus AvailableDate
public abstract class QuickIndex {

	protected String quickIndexID, productID, name, productNum, active, headline, description, productSubType, marketingStatus, orientation, productType;
	protected Date preAvailableDate, availableDate, expiryDate, createdDate;
	
	private double totalCost, advertisedCost, discounts;
	
	private String displayPrice;
	private String displayPriceType;

	
	protected SimpleDocument thumbnailImage, mainImage;
	
	protected QuickIndexStatusHistory statusHistory = null;
	
	protected String locationID, regionID;
	
	public QuickIndex() {}
	
	public QuickIndex(GenRow row) {
		productID = row.getData("ProductID");
		productNum = row.getData("ProductNum");
		name = row.getData("Name");
		active = row.getData("Active");
		totalCost = row.getDouble("TotalCost");
		advertisedCost = row.getDouble("DripResult");
		discounts = row.getDouble("DripCost");
		headline = row.getData("HeadLine");
		description = row.getData("Description");
		
		productType = row.getData("ProductType");
		
		productSubType = row.getData("ProductSubType");
		marketingStatus = row.getData("LotExclusivity");
		
		orientation = row.getData("Orientation");
		
		
		if (row.isSet("PreAvailableDate")) preAvailableDate = row.getDate("PreAvailableDate");
		if (row.isSet("AvailableDate")) availableDate = row.getDate("AvailableDate");
		if (row.isSet("ExpiryDate")) expiryDate = row.getDate("ExpiryDate");
		if (row.isSet("CreatedDate")) createdDate = row.getDate("CreatedDate");
		
		locationID = row.getData("LocationID");
		regionID = row.getData("RegionID");
		
		displayPriceType = row.getString("DisplayPriceType");
		
		if (row.isSet("DripResult")) {
			this.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, row.getDouble("DripResult")));
		} else if (row.isSet("TotalCost")) {
			this.setDisplayPrice(RunwayUtil.displayPrice(displayPriceType, row.getDouble("TotalCost")));
		}
			
		if (StringUtils.isBlank(this.getDisplayPrice()) && row.isSet("DisplayPrice")) {
			this.setDisplayPrice(row.getString("DisplayPrice"));
		}
	}
	
	@XmlElement(name="ProductID")
	public String getProductID() {
		return productID;
	}
	public void setProductID(String lotID) {
		this.productID = lotID;
	}
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getQuickIndexID() {
		return quickIndexID;
	}

	public void setQuickIndexID(String quickIndexID) {
		this.quickIndexID = quickIndexID;
	}

	@XmlElement(name="Active")
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	@XmlElement(name="PreAvailableDate")
	public Date getPreAvailableDate() {
		return preAvailableDate;
	}

	public void setPreAvailableDate(Date preAvailableDate) {
		this.preAvailableDate = preAvailableDate;
	}

	@XmlElement(name="AvailableDate")
	public Date getAvailableDate() {
		return availableDate;
	}

	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}

	@XmlElement(name="ExpiryDate")
	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	@XmlElement(name="ThumbnailImage")
	public SimpleDocument getThumbnailImage() {
		return thumbnailImage;
	}

	public void setThumbnailImage(SimpleDocument thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}

	@XmlElement(name="MainImage")
	public SimpleDocument getMainImage() {
		return mainImage;
	}

	public void setMainImage(SimpleDocument mainImage) {
		this.mainImage = mainImage;
	}

	@XmlElement(name="Status")
	public QuickIndexStatusHistory getStatusHistory() {
		return statusHistory;
	}

	public void setStatusHistory(QuickIndexStatusHistory statusHistory) {
		this.statusHistory = statusHistory;
	}

	@XmlElement(name="TotalCost")
	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	@XmlElement(name="AdvertisedCost")
	public double getAdvertisedCost() {
		if (advertisedCost == 0) return totalCost;
		return advertisedCost;
	}

	public void setAdvertisedCost(double advertisedCost) {
		this.advertisedCost = advertisedCost;
	}

	@XmlElement(name="Discounts")
	public double getDiscounts() {
		return discounts;
	}

	public void setDiscounts(double discounts) {
		this.discounts = discounts;
	}

	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@XmlElement(name="LocationID")
	public String getLocationID() {
		return locationID;
	}

	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}

	@XmlElement(name="RegionID")
	public String getRegionID() {
		return regionID;
	}

	public void setRegionID(String regionID) {
		this.regionID = regionID;
	}

	@XmlElement(name="Headline")
	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name="DisplayPrice")
	public String getDisplayPrice() {
		return displayPrice;
	}

	public void setDisplayPrice(String displayPrice) {
		this.displayPrice = displayPrice;
	}

	@XmlElement(name="ProductSubType")
	public String getProductSubType() {
		return productSubType;
	}

	public void setProductSubType(String productSubType) {
		this.productSubType = productSubType;
	}

	@XmlElement(name="MarketingStatus")
	public String getMarketingStatus() {
		return marketingStatus;
	}

	public void setMarketingStatus(String marketingStatus) {
		this.marketingStatus = marketingStatus;
	}

	public String getDisplayPriceType() {
		return displayPriceType;
	}

	public void setDisplayPriceType(String displayPriceType) {
		this.displayPriceType = displayPriceType;
	}

	@XmlElement(name="ProductNum")
	public String getProductNum() {
		return productNum;
	}

	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}

	@XmlElement(name="Orientation")
	public String getOrientation() {
		return orientation;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	@XmlElement(name="ProductType")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
}
