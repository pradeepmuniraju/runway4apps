package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.runway.externalInterface.beans.shared.AbstractStatusHistory;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="RealEstateStatus")
@XmlType(propOrder={"realEstateStatusID","realEstateID"})
public class RealEstateStatusHistory extends AbstractStatusHistory {

	private String realEstateStatusID, realEstateID;

	@XmlElement(name="RealEstateStatusID")
	public String getRealEstateStatusID() {
		return realEstateStatusID;
	}
	public void setRealEstateStatusID(String realEstateStatusID) {
		this.realEstateStatusID = realEstateStatusID;
	}
	
	@XmlElement(name="RealEstateID")
	public String getRealEstateID() {
		return realEstateID;
	}
	public void setRealEstateID(String realEstateID) {
		this.realEstateID = realEstateID;
	}
}
