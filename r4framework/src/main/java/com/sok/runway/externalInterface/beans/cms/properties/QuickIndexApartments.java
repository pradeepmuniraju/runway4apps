package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.NONE)
@XmlSeeAlso(QuickIndex.class)
@XmlRootElement(name="QuickIndexApartments")
public class QuickIndexApartments extends QuickIndex {

	protected String buildingID, buildingName, buildingStageID, buildingStageName, apartmentID, apartmentName;
	protected double apartmentWidth, apartmentDepth, apartmentArea, internalArea, externalArea;
	protected double storeys, bedrooms, bathrooms, carparks, livingrooms;
	protected String study, garage, media;

	public QuickIndexApartments() {
	}

	public QuickIndexApartments(GenRow row) {
		super(row);

		buildingID = row.getData("BuildingID");
		buildingName = row.getData("BuildingName");
		buildingStageID = row.getData("BuildingStageID");
		buildingStageName = row.getData("BuildingStageName");
		apartmentID = row.isSet("ApartmentID")? row.getData("ApartmentID") : row.getData("ProductID");
		apartmentName = row.isSet("ApartmentName")? row.getData("ApartmentName") : row.getData("Name");

		if (row.isSet("ApartmentArea"))
			apartmentArea = row.getDouble("ApartmentArea");
		else
			apartmentArea = row.getDouble("LotArea");
		if (row.isSet("ApartmentDepth"))
			apartmentDepth = row.getDouble("ApartmentDepth");
		else
			apartmentDepth = row.getDouble("LotDepth");
		if (row.isSet("ApartmentWidth"))
			apartmentWidth = row.getDouble("ApartmentWidth");
		else
			apartmentWidth = row.getDouble("LotWidth");
		
		internalArea = row.getDouble("InternalArea");
		externalArea = row.getDouble("ExternalArea");
		livingrooms = row.getDouble("LivingRooms");
		
		if (apartmentArea == 0) apartmentArea = internalArea + externalArea;
		
		storeys = row.getDouble("Storeys");
		bedrooms = row.getDouble("Bedrooms");
		bathrooms = row.getDouble("Bathrooms");
		carparks = row.getDouble("CarParks");

		study = row.getData("Study");
		garage = row.getData("Garage");
		media = row.getData("Media");
	}

	@XmlElement(name="BuildingID")
	public String getBuildingID() {
		return buildingID;
	}

	public void setBuildingID(String buildingID) {
		this.buildingID = buildingID;
	}

	@XmlElement(name="BuildingName")
	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	@XmlElement(name="BuildingStageID")
	public String getBuildingStageID() {
		return buildingStageID;
	}

	public void setBuildingStageID(String buildingStageID) {
		this.buildingStageID = buildingStageID;
	}

	@XmlElement(name="BuildingStageName")
	public String getBuildingStageName() {
		return buildingStageName;
	}

	public void setBuildingStageName(String buildingStageName) {
		this.buildingStageName = buildingStageName;
	}

	@XmlElement(name="ApartmentID")
	public String getApartmentID() {
		return apartmentID;
	}

	public void setApartmentID(String apartmentID) {
		this.apartmentID = apartmentID;
	}

	@XmlElement(name="ApartmentName")
	public String getApartmentName() {
		return apartmentName;
	}

	public void setApartmentName(String apartmentName) {
		this.apartmentName = apartmentName;
	}

	@XmlElement(name="ApartmentWidth")
	public double getApartmentWidth() {
		return apartmentWidth;
	}

	public void setApartmentWidth(double apartmentWidth) {
		this.apartmentWidth = apartmentWidth;
	}

	@XmlElement(name="ApartmentDepth")
	public double getHomeDepth() {
		return apartmentDepth;
	}

	public void setHomeDepth(double apartmentDepth) {
		this.apartmentDepth = apartmentDepth;
	}

	@XmlElement(name="ApartmentArea")
	public double getHomeArea() {
		return apartmentArea;
	}

	public void setHomeArea(double apartmentArea) {
		this.apartmentArea = apartmentArea;
	}

	@XmlElement(name="Levels")
	public double getStoreys() {
		return storeys;
	}

	public void setStoreys(double storeys) {
		this.storeys = storeys;
	}

	@XmlElement(name="Bedrooms")
	public double getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(double bedrooms) {
		this.bedrooms = bedrooms;
	}

	@XmlElement(name="Bathrooms")
	public double getBathrooms() {
		return bathrooms;
	}

	public void setBathrooms(double bathrooms) {
		this.bathrooms = bathrooms;
	}

	@XmlElement(name="Study")
	public String getStudy() {
		return study;
	}

	public void setStudy(String study) {
		this.study = study;
	}

	@XmlElement(name="CarParks")
	public double getCarparks() {
		return carparks;
	}

	public void setCarparks(double carparks) {
		this.carparks = carparks;
	}

	@XmlElement(name="Garage")
	public String getGarage() {
		return garage;
	}

	public void setGarage(String garage) {
		this.garage = garage;
	}

	@XmlElement(name="InternalArea")
	public double getInternalArea() {
		return internalArea;
	}

	public void setInternalArea(double internalArea) {
		this.internalArea = internalArea;
	}

	@XmlElement(name="ExternalArea")
	public double getExternalArea() {
		return externalArea;
	}

	public void setExternalArea(double externalArea) {
		this.externalArea = externalArea;
	}

	@XmlElement(name="Media")
	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	@XmlElement(name="LivingRooms")
	public double getLivingrooms() {
		return livingrooms;
	}

	public void setLivingrooms(double livingrooms) {
		this.livingrooms = livingrooms;
	}

}
