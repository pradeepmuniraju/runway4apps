package com.sok.runway.externalInterface.resources.cms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.sok.service.cms.WebsiteService;
import com.sok.runway.externalInterface.beans.cms.CMSSite;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.servlet.http.HttpServletRequest;

@Path("cms/websites")
public class WebsiteResource {

	private static final Logger logger = LoggerFactory.getLogger(WebsiteResource.class);
	private static final WebsiteService websiteService = WebsiteService.getInstance();
	private static final WebsiteResource resource = new WebsiteResource();
	public static WebsiteResource getInstance() {
		return resource;
	}
	@Context HttpServletRequest request;
	@GET
	@Path("{CMSID}")
	@Produces(MediaType.APPLICATION_JSON)
	public CMSSite getCMS(@Context HttpServletRequest request, 
			@PathParam("CMSID") String cmsID) {
		logger.debug("getCMS(request, cmsID={})", cmsID);
		return websiteService.getCMS(request, cmsID);
	}
	
	@Path("{CMSID}/links")
	public PageLinkResource getCMSPageLinks(@PathParam("CMSID") String cmsID) {
		return new PageLinkResource(cmsID, request);
	}
	
	@Path("{CMSID}/pages")
	public PageResource getCMSPages(@PathParam("CMSID") String cmsID) {
		return new PageResource(cmsID, request);
	}
	
	@Path("{CMSID}/versiongroups")
	public PageVersionGroupResource getCMSVersionGroups(@PathParam("CMSID") String cmsID) {
		return new PageVersionGroupResource(cmsID, request);
	}
	
	@Path("{CMSID}/users")
	public WebsiteUserResource getCMSUserResource(@PathParam("CMSID") String cmsID) {
		return new WebsiteUserResource(cmsID, request);
	}
	
	@Path("{CMSID}/domains")
	public WebsiteDomainResource getWebsiteDomainResource(@PathParam("CMSID") String cmsID) {
		return new WebsiteDomainResource(cmsID, request);
	}
}
