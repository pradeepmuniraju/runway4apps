package com.sok.runway.externalInterface.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.sok.runway.externalInterface.DataApplication;
import com.sok.service.exception.AccessException;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

@Provider
public class RuntimeExceptionHandler implements ExceptionMapper<RuntimeException> {
	
	private static final Logger logger = LoggerFactory.getLogger(RuntimeExceptionHandler.class);
	public Response toResponse(RuntimeException exception){
		if(exception instanceof IllegalArgumentException) {
			logger.debug("Illegal Argument Supplied", exception);
			return Response.status(Response.Status.BAD_REQUEST)
				    .entity(new DataApplication.DataApplicationException(exception, Response.Status.BAD_REQUEST.getStatusCode()))
				    .build();
		} else if(exception instanceof AccessException) {
			return Response.status(Response.Status.FORBIDDEN)
				    .entity(new DataApplication.DataApplicationException(exception, Response.Status.FORBIDDEN.getStatusCode()))
				    .build();
		} else {
			logger.error("Unknown error", exception);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				    .entity(new DataApplication.DataApplicationException(exception, Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()))
				    .build();
		}
	}
}
