package com.sok.runway.externalInterface.resources.cms.properties;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.LegacyLot;
import com.sok.service.crm.cms.properties.EstateService;

public class LotResource {

	private static final Logger logger = LoggerFactory.getLogger(LotResource.class);
	private static final EstateService estateService = EstateService.getInstance();
	
	protected final String estateID, stageID;
	@Context HttpServletRequest request;
	
	public LotResource() {
		this.estateID = null;
		this.stageID = null;
	}
	
	public LotResource(HttpServletRequest request) {
		this.request = request;
		this.estateID = null;
		this.stageID = null;
	}
	public LotResource(HttpServletRequest request, String estateID) {
		this.request = request;
		this.estateID = estateID;
		this.stageID = null;
	}
	public LotResource(HttpServletRequest request, String estateID, String stageID) {
		this.request = request;
		this.estateID = estateID;
		this.stageID = stageID;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<LegacyLot> getEstateLots() {
		return estateService.getLotsExternalLegacy(request, estateID, stageID, null);
	}
	
	@GET
	@Path("{lotID}")
	public LegacyLot getLot(@Context HttpServletRequest request, @PathParam("lotID") final String lotID) {
		return new LegacyLot(BeanFactory.external(estateService.getLot(request, lotID), BeanRepresentation.Extended));
	}

	@Path("{lotID}/details")
	public PropertyDetailResource getPropertyDetailResource(@Context HttpServletRequest request, @PathParam("lotID") final String lotID) {
		return new PropertyDetailResource(lotID, request);
	}
}
