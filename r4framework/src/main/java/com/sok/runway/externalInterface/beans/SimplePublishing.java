package com.sok.runway.externalInterface.beans;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;

/**
 * Simplified Document Bean, for use with Products / Image/ImageName/ImagePath
 * @author Michael Dekmetzian
 * @date 3 January 2012
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="SimplePublishing")
//@XmlType(propOrder={"documentID", "description", "filename", "filepath","type","subType", "URL"})
public class SimplePublishing {

	private String 	status = "Not Approved";
	private String 	type = "Website";
	private String 	publishedFlag = "N";
	private Date 	startDate = null; 
	private Date 	endDate = null; 
	
	public SimplePublishing() {}
	public SimplePublishing(GenRow row) {
		status = row.getString(type + "PublishStatus");
		publishedFlag = row.getString(type + "PublishedFlag");
		
		if (row.isSet(type + "StartDate")) startDate = row.getDate(type + "StartDate");
		if (row.isSet(type + "EndDate")) endDate = row.getDate(type + "EndDate");
	}

	public SimplePublishing(GenRow row, String type) {
		
	}

	@XmlElement(name="Type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlElement(name="Status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@XmlElement(name="PublishedFlag")
	public String getPublishedFlag() {
		return publishedFlag;
	}

	public void setPublishedFlag(String publishedFlag) {
		this.publishedFlag = publishedFlag;
	}

	@XmlElement(name="StartDate")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@XmlElement(name="EndDate")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}  
	   
}
