package com.sok.runway.externalInterface.resources.cms.properties;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.cms.properties.Apartment;
import com.sok.runway.externalInterface.beans.cms.properties.BuildingStage;
import com.sok.runway.externalInterface.beans.cms.properties.Estate;
import com.sok.runway.externalInterface.beans.cms.properties.Lot;
import com.sok.runway.externalInterface.beans.cms.properties.Stage;
import com.sok.service.crm.cms.properties.EstateService;

@Path("/estates")
@Produces({ MediaType.APPLICATION_JSON})
public class EstateResource {

	private static final Logger logger = LoggerFactory.getLogger(EstateResource.class);
	private static final EstateService estateService = EstateService.getInstance();
	
	private static final EstateResource resource = new EstateResource();
	public static EstateResource getInstance() { return resource; }
	
	@GET
	public List<Estate> getEstates(@Context HttpServletRequest request) {
		return estateService.getEstates(request); 
	}
	
	@GET
	@Path("{estateID}")
	public Estate getEstate(@Context HttpServletRequest request, @PathParam("estateID") final String estateID) {
		return estateService.getEstate(request, estateID);
	}
	
	@Path("{estateID}/lots")
	public LotResource getLotResource(@Context HttpServletRequest request, @PathParam("estateID") final String estateID) {
		return new LotResource(request, estateID);
	}

	@GET
	@Path("stages/{stageID}")
	public Stage getBuildingLevel(@Context HttpServletRequest request, @PathParam("stageID") final String stageID) {
		return estateService.getStage(request, stageID);
	}
	
	@GET
	@Path("stages/lotss/{lotID}")
	public Lot getApartment(@Context HttpServletRequest request, @PathParam("lotID") final String lotID) {
		return BeanFactory.external(estateService.getLot(request, lotID));
	}
}
