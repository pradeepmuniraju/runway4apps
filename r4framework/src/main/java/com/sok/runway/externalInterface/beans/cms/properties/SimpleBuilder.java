package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Builder")
@XmlSeeAlso(Builder.class)
@XmlType(propOrder={"builderID","name" })		//MarketingStatus AvailableDate
public abstract class SimpleBuilder {

	protected String builderID, name;
	
	//@JsonProperty("builderID")
	@XmlElement(name="BuilderID")
	public String getBuilderID() {
		return builderID;
	}
	public void setBuilderID(String builderID) {
		this.builderID = builderID;
	}
	//@JsonProperty("Name")
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
