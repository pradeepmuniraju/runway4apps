package com.sok.runway.externalInterface.rest.rpm;

import java.sql.SQLIntegrityConstraintViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;	
import javax.ws.rs.core.Response;

import javax.xml.bind.annotation.XmlElementWrapper;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.DatabaseException;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.crm.activity.ActivityLogger.ActionType;
import com.sok.runway.crm.cms.properties.entities.BuilderEntity;
import com.sok.runway.externalInterface.beans.cms.properties.Builder;
import com.sok.runway.externalInterface.beans.cms.properties.BuilderContact;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.BuilderService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

/**
 * Extend and annotate the existing BuilderResource implementation for use with the V2 (Public/Documented) API.
 * 
 * This is seperate from the v1 api endpoint to limit the library requirements on those apps which do not yet use the 
 * v2 api. 
 * 
 * @author Michael Dekmetzian
 * @date 2 January 2013
 */
@Path("/builders")
@Api(value = "/builders", description = "Retrieve Builder Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class BuilderResource /* extends com.sok.runway.externalInterface.resources.cms.properties.BuilderResource */ {

	static final Logger logger = LoggerFactory.getLogger(BuilderResource.class);
	@Context HttpServletRequest request;
	
	public BuilderResource() {}
	public BuilderResource(HttpServletRequest request) { this.request = request; }
	
	@GET
	@ApiOperation(value = "List all builders", notes="Optionally filtering by modified date", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Builder", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "Builders")
	public List<Builder> getBuilders(
			@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = false) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active builders", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			logger.debug("getBuilders({})", modifiedSince, modifiedSince);
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		return BuilderService.getInstance().getBuilders(request, mod, active);
	}
	
	@GET
	@Path("/{builderID}")
	@ApiOperation(value = "Return a particular builder", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Builder")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public Builder getBuilder(
			@ApiParam(value = "The BuilderID value as returned from the API", required = true) @PathParam("builderID") final String builderID) {
		
		return BuilderService.getInstance().getBuilder(request, builderID);
	}
	
	@POST
	@Path("/{builderID}/contacts")
	@ApiOperation(value = "Add a builder contact", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.BuilderContact")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public BuilderContact addBuilderContact(@ApiParam(value = "The BuilderID value as returned from the API", required = true) @PathParam("builderID") final String builderID, BuilderContact builderContact) {
		if(!StringUtils.equals(builderContact.getBuilderID(), builderID)) {
			throw new IllegalArgumentException("BuilderIDs must match"); 
		}
		BuilderEntity ee = new BuilderEntity(request); 
		ee.load(builderID); 
		if(!ee.isLoaded()) {
			throw new NotFoundException("The Builder you requested could not be found: " + builderID);
		}
		if(!ee.canEdit()) {
			throw new AccessException("You do not have permission to edit the builder with id " + builderID);
		}
		if(StringUtils.isBlank(builderContact.getContactID())) {
			throw new IllegalArgumentException("ContactID must be provided");
		}
		if(StringUtils.isBlank(builderContact.getLinkType())) {
			throw new IllegalArgumentException("LinkType must be provided");
		}
		GenRow ec = new GenRow(); 
		ec.setRequest(request); 
		ec.setViewSpec("properties/BuilderContactView"); 
		ec.setParameter("BuilderID", builderContact.getBuilderID()); 
		ec.setParameter("ContactID", builderContact.getContactID());
		ec.setParameter("LinkType", builderContact.getLinkType()); 
		ec.doAction(com.sok.framework.generation.GenerationKeys.SELECTFIRST);
		
		UserBean user = UserService.getInstance().getCurrentUser(request); 
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!ec.isSuccessful()) {
			ec.setToNewID("BuilderContactID"); 
			ec.setParameter("CreatedDate","NOW"); 
			ec.setParameter("CreatedBy", user.getUserID());
			ec.setParameter("ModifiedDate","NOW"); 
			ec.setParameter("ModifiedBy", user.getUserID());
			ec.setParameter("DefaultLink", builderContact.isDefaultLink()?"Y":"N");
			try { 
				ec.doAction(com.sok.framework.generation.GenerationKeys.INSERT);
				ActivityLogger.getActivityLogger().logActivity(ee, ActionType.Updated);
				ec.doAction(com.sok.framework.generation.GenerationKeys.SELECT); // do this to set the CreatedBy / Date fields to actual values
			} catch (DatabaseException de) {
				Throwable c = de.getCause(); 
				if(c != null && c instanceof SQLIntegrityConstraintViolationException) {
					ec.clear(); 
					ec.setParameter("BuilderID", builderContact.getBuilderID()); 
					ec.setParameter("ContactID", builderContact.getContactID());
					ec.setParameter("LinkType", builderContact.getLinkType()); 
					ec.doAction(com.sok.framework.generation.GenerationKeys.SELECTFIRST);
					if(!ec.isSuccessful()) {
						// if not successful the problem was something else
						throw de; 
					}
				} else {
					throw de; 
				}
			}
		}
		builderContact.setBuilderContactID(ec.getData("BuilderContactID"));
		builderContact.setCreatedBy(ec.getData("CreatedBy"));
		builderContact.setCreatedDate(ec.getDate("CreatedDate"));
		builderContact.setCreatedByName(new PersonName(ec.getData("OwnFirstName"), ec.getData("OwnLastName")));
		
		builderContact.setModifiedBy(ec.getData("ModifiedBy"));
		builderContact.setModifiedDate(ec.getDate("ModifiedDate"));
		builderContact.setModifiedByName(new PersonName(ec.getData("OwnFirstName"), ec.getData("OwnLastName")));
		return builderContact; 
	}
	
	@PUT
	@Path("/{builderID}/contacts/{builderContactID}")
	@ApiOperation(value = "Updates a builder contact", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.BuilderContact")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public BuilderContact updateBuilderContact(@ApiParam(value = "The BuilderID value as returned from the API", required = true) @PathParam("builderID") final String builderID, 
											 @ApiParam(value = "The BuilderContactID value as returned from the API", required = true) @PathParam("builderContactID") final String builderContactID,
											 BuilderContact builderContact) {
		if(StringUtils.isBlank(builderID) || !StringUtils.equals(builderContact.getBuilderID(), builderID)) {
			throw new IllegalArgumentException("BuilderIDs must match"); 
		}
		if(StringUtils.isBlank(builderContactID) || !StringUtils.equals(builderContact.getBuilderContactID(), builderContactID)) {
			throw new IllegalArgumentException("BuilderContactIDs must match"); 
		}
		BuilderEntity ee = new BuilderEntity(request); 
		ee.load(builderID); 
		if(!ee.isLoaded()) {
			throw new NotFoundException("The Builder you requested could not be found: " + builderID);
		}
		if(!ee.canEdit()) {
			throw new AccessException("You do not have permission to edit the builder with id " + builderID);
		}
		if(StringUtils.isBlank(builderContact.getContactID())) {
			throw new IllegalArgumentException("ContactID must be provided");
		}
		if(StringUtils.isBlank(builderContact.getLinkType())) {
			throw new IllegalArgumentException("LinkType must be provided");
		}
		UserBean user = UserService.getInstance().getCurrentUser(request); 
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		
		GenRow ec = new GenRow(); 
		ec.setRequest(request); 
		ec.setViewSpec("properties/BuilderContactView"); 
		ec.setParameter("BuilderContactID", builderContact.getBuilderContactID()); 
		ec.doAction(GenerationKeys.SELECT);
		
		if(!ec.isSuccessful()) {
			throw new NotFoundException("The builder contact was not found with id " + builderContact.getBuilderContactID());
		}		
		if(!StringUtils.equals(builderContact.getContactID(), ec.getData("ContactID"))) {
			throw new UnsupportedOperationException("Changing of Contacts on existing links are not supported. You should remove and re-add the link with the appropriate contact to ensure all actions are logged correctly.");
		}
		
		ec.setParameter("ModifiedDate","NOW"); 
		ec.setParameter("ModifiedBy", user.getUserID());
		ec.setParameter("DefaultLink", builderContact.isDefaultLink()?"Y":"N");
		ec.setParameter("LinkType", builderContact.getLinkType()); 
		
		if(builderContact.isDefaultLink()) {
			GenRow up = new GenRow(); 
			up.setTableSpec("properties/BuilderContacts"); 
			up.setRequest(request); 
			up.setParameter("ON-BuilderID", builderContact.getBuilderID());
			up.setParameter("DefaultLink", "N");
			up.doAction(GenerationKeys.UPDATEALL);
		}
		
		ec.doAction(GenerationKeys.UPDATE); 
		if(!ec.isSuccessful()) {
			throw new RuntimeException("An unexpected error has occurred " + ec.getError());
		}
		
		builderContact.setBuilderContactID(ec.getData("BuilderContactID"));
		builderContact.setCreatedBy(ec.getData("CreatedBy"));
		builderContact.setCreatedDate(ec.getDate("CreatedDate"));
		builderContact.setCreatedByName(new PersonName(ec.getData("OwnFirstName"), ec.getData("OwnLastName")));
		
		builderContact.setModifiedBy(ec.getData("ModifiedBy"));
		builderContact.setModifiedDate(ec.getDate("ModifiedDate"));
		builderContact.setModifiedByName(new PersonName(ec.getData("ModFirstName"), ec.getData("ModLastName")));
		
		return builderContact; 
	}
	
	@DELETE
	@ApiOperation(value = "Deletes a builder contact")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid builder id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted to modify this builder"), @ApiError(code = 404, reason = "Builder Not Found") })
	@Path("/{builderID}/contacts/{builderContactID}")
	public void deleteBuilderContact(@ApiParam(value = "The BuilderID value as returned from the API", required = true) @PathParam("builderID") final String builderID,
			@ApiParam(value = "The BuilderContactID value as returned from the API", required = true) @PathParam("builderContactID") final String builderContactID) {
		BuilderEntity ee = new BuilderEntity(request); 
		ee.load(builderID); 
		if(!ee.isLoaded()) {
			throw new NotFoundException("The Builder you requested could not be found: " + builderID);
		}
		if(!ee.canEdit()) {
			throw new AccessException("You do not have permission to edit the builder with id " + builderID);
		}
		if(StringUtils.isBlank(builderContactID)) {
			throw new IllegalArgumentException("BuilderContactID must be provided");
		}
		GenRow ec = new GenRow(); 
		ec.setRequest(request); 
		ec.setViewSpec("properties/BuilderContactView"); 
		ec.setParameter("BuilderContactID", builderContactID);
		ec.doAction(com.sok.framework.generation.GenerationKeys.SELECT);
		
		if(ec.isSuccessful()) {
			if(!StringUtils.equals(ec.getData("BuilderID"), builderID)) {
				throw new IllegalArgumentException("BuilderIDs must match"); 
			}
			ec.doAction(GenerationKeys.DELETE);
			ActivityLogger.getActivityLogger().logActivity(ee, ActionType.Updated);
		}
	}
	
	@ApiResource(value = "/{builderID}/brands", resourceClass="com.sok.runway.externalInterface.rest.rpm.BrandResource")
	@Path("/{builderID}/brands")
	public BrandResource getBrandResource(
			@ApiParam(value = "The BuilderID value as returned from the API", required = true) @PathParam("builderID") final String builderID) {
		return new BrandResource(request, builderID);
	}
}
