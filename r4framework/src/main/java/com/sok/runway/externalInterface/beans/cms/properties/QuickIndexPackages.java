package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.runway.externalInterface.BeanFactory;

import java.util.Date;

@XmlAccessorType(XmlAccessType.NONE)
@XmlSeeAlso(QuickIndexPlans.class)
@XmlRootElement(name="QuickIndexPackages")
public class QuickIndexPackages extends QuickIndexPlans {
	protected String estateID, estateName, stageID, stageName, lotID, lotName, lotType, lotExclusivity, packageName, packageID, displayFlag, originalPlanID, facadeName;
	protected double packagePrice, lotPrice, lotWidth, lotDepth, lotArea;
	private String repUserId;
	private Date lastModifiedDate;

	public QuickIndexPackages() {
		// TODO Auto-generated constructor stub
	}

	public QuickIndexPackages(GenRow row) {
		super(row);

		estateID = row.getData("EstateID");
		estateName = row.getData("EstateName");
		stageID = row.getData("StageID");
		stageName = row.getData("StageName");
		facadeName = row.getData("FacadeName");
		lotID = row.isSet("LotID")? row.getData("LotID") : row.getData("ProductID");
		lotName = row.isSet("LotName")? row.getData("LotName") : row.getData("Name");
		lotType = row.getData("LotType");
		lotExclusivity = row.getData("LotExclusivity");
		displayFlag = row.getData("DisplayFlag");
		originalPlanID = row.getData("CopiedFromID");

		planName = row.getString("HomePlanName");
		if (row.getString("HomePlanName").startsWith(row.getString("DesignName")))
			homePlanName = row.getString("HomePlanName").trim();
		else
			homePlanName = (row.getString("DesignName") + " " + row.getString("HomePlanName")).trim();
		//row.getString("DesignName") + " " + row.getString("HomePlanName")).trim();

		lotPrice = row.getDouble("LotPrice");
		lotPrice = row.getDouble("LotPrice");
		lotArea = row.getDouble("LotArea");
		lotDepth = row.getDouble("LotDepth");
		lotWidth = row.getDouble("LotWidth");
		repUserId = row.getData("");
		lastModifiedDate = row.getDate("ModifiedDate");
	}

	@XmlElement(name="EstateID")
	public String getEstateID() {
		return estateID;
	}

	public void setEstateID(String estateID) {
		this.estateID = estateID;
	}

	@XmlElement(name="EstateName")
	public String getEstateName() {
		return estateName;
	}

	public void setEstateName(String estateName) {
		this.estateName = estateName;
	}

	@XmlElement(name="StageID")
	public String getStageID() {
		return stageID;
	}

	public void setStageID(String stageID) {
		this.stageID = stageID;
	}

	@XmlElement(name="StageName")
	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	@XmlElement(name="LotID")
	public String getLotID() {
		return lotID;
	}

	public void setLotID(String lotID) {
		this.lotID = lotID;
	}

	@XmlElement(name="LotName")
	public String getLotName() {
		return lotName;
	}

	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	@XmlElement(name="LotType")
	public String getLotType() {
		return lotType;
	}

	public void setLotType(String lotType) {
		this.lotType = lotType;
	}

	@XmlElement(name="LotExclusivity")
	public String getLotExclusivity() {
		return lotExclusivity;
	}

	public void setLotExclusivity(String lotExclusivity) {
		this.lotExclusivity = lotExclusivity;
	}

	@XmlElement(name="LotPrice")
	public double getLotPrice() {
		return lotPrice;
	}

	public void setLotPrice(double lotPrice) {
		this.lotPrice = lotPrice;
	}

	@XmlElement(name="LotWidth")
	public double getLotWidth() {
		return lotWidth;
	}

	public void setLotWidth(double lotWidth) {
		this.lotWidth = lotWidth;
	}

	@XmlElement(name="LotDepth")
	public double getLotDepth() {
		return lotDepth;
	}

	public void setLotDepth(double lotDepth) {
		this.lotDepth = lotDepth;
	}

	@XmlElement(name="LotArea")
	public double getLotArea() {
		return lotArea;
	}

	public void setLotArea(double lotArea) {
		this.lotArea = lotArea;
	}

	@XmlElement(name="PackageName")
	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	@XmlElement(name="PackageID")
	public String getPackageID() {
		return packageID;
	}

	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}

	@XmlElement(name="FacadeName")
	public String getFacadeName() {
		return facadeName;
	}

	public void setFacadeName(String facadeName) {
		this.facadeName = facadeName;
	}

	@XmlElement(name="PackagePrice")
	public double getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(double packagePrice) {
		this.packagePrice = packagePrice;
	}

	@XmlElement(name="DisplayHomeFlag")
	public String getDisplayFlag() {
		return displayFlag;
	}

	public void setDisplayFlag(String displayFlag) {
		this.displayFlag = displayFlag;
	}


	@XmlElement(name="PropertyPDFURL")
	public String PropertyPDFURL() {
		return BeanFactory.getDetailPdfUrl(locationID, productID, lastModifiedDate, repUserId);
	}

	public void setPropertyPDFURL(String blank) {
	}

	@XmlElement(name="OriginalPlanID")
	public String getOriginalPlanID() {
		return originalPlanID;
	}

	public void setOriginalPlanID(String originalPlanID) {
		this.originalPlanID = originalPlanID;
	}

}
