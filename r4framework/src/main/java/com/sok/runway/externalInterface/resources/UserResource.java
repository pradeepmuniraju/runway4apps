package com.sok.runway.externalInterface.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse; 

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.RowBean;
import com.sok.runway.ItemLists;
import com.sok.runway.StatusList;
import com.sok.runway.UserBean;

import com.sok.runway.externalInterface.beans.Contact;
import com.sok.runway.externalInterface.beans.User;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.DataApplication;
import com.sok.service.crm.UserService;
import com.sok.service.exception.NotAuthenticatedException;

@Path("/admin/users/")
public class UserResource {
	private static final UserResource resource = new UserResource();
	private static final Logger logger = LoggerFactory.getLogger(UserResource.class);
	private static final UserService us = UserService.getInstance();
	private UserResource() {}
	public static UserResource getInstance() {
		return resource;
	}	
	
	final String[] quickNoteFields = { "-FirstName", "-LastName", "-Company", "-Email" };
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getUsers(@Context HttpServletRequest request) {
		logger.trace("getUsers(Request)");
		for(String s: quickNoteFields) {
			if(StringUtils.isNotBlank(request.getParameter(s))) {
				return us.getQuicknoteUserMatches(request);
			}
		}
		throw new UnsupportedOperationException("Not yet implemented for general search");
	}
	
	
	@GET
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	public User getLogin(@Context HttpServletRequest request) {
		logger.debug("UserResource.getLogin()");
		/* can't use the userbean directly as all the hashmap properties will be given, which we don't want */
		UserBean user = us.getCurrentUser(request);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		return us.getUser(request, user.getUserID());
	}
	
	@POST
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	public User getLogin(@Context HttpServletRequest request, @Context HttpServletResponse response,  @FormParam("Username") String username, @FormParam("Password") String password) { 
		if(!request.isSecure()){
			   //no cache breaks downloading pdf in ie over https
				response.setHeader("Expires","Mon, 26 Jul 1997 05:00:00 GMT" );  // disable IE caching
				response.setHeader("Cache-Control","no-cache, must-revalidate" );
				response.setHeader("Pragma","no-cache" );
				response.setDateHeader ("Expires", 0);
		}
		if(StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
			throw new NotAuthenticatedException("Both username and password must be submitted");
		}
		String login = us.login(request, username, password);
		if(logger.isDebugEnabled()) logger.debug("got {} from login request with {} and {}", new String[]{login, username, password});
		if(UserBean.authenticated.equals(login)) {
			
			UserBean user = us.getCurrentUser(request);
			
			RowBean activitylog = new RowBean();
			activitylog.setRequest(request);
		    activitylog.setViewSpec("ActivityView");
		    activitylog.setColumn("Location",user.getString("IPAddress"));
		    activitylog.setColumn("UserID",user.getUserID());
		    activitylog.setAction("insert"); 
		    
			final String sqlJndiName = request.getSession().getServletContext().getInitParameter("sqlJndiName");
			
			StatusList statuslist = new StatusList();
			statuslist.setJndiName(sqlJndiName);
			request.getSession().setAttribute("statuslist",statuslist);

			ItemLists itemlists = new ItemLists();
			itemlists.setJndiName(sqlJndiName);
			request.getSession().setAttribute("itemlists",itemlists);
			
		    request.getSession().setAttribute("activitylog",activitylog);
			
			return getLogin(request);
		}
		throw new NotAuthenticatedException("Username or Password was incorrect");
	}
	
	public static class LoginResponse {
		public String state;
		public String message;
		public User user;
		public LoginResponse(String message) {
			this.state = "error";
			this.user = null;
		}
		public LoginResponse(String state, User user) {
			this.state = state; 
			this.user = user;
		}
	}
	
	
	@GET
	@Path("{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUser(@Context HttpServletRequest request, @PathParam("userId") String userId) {
		logger.debug("UserResource.getUser({})", userId);
		return us.getUser(request, userId);
	}
	
	/**
	 * @deprecated - use UserService.getCurrentUser(HttpServletRequest);
	 */
	public UserBean getCurrentUser(HttpServletRequest request) { 
		logger.debug("UserResource.getCurrentUser()");
		return us.getCurrentUser(request);
		/*
		UserBean currentuser = (UserBean)request.getSession().getAttribute(ActionBean.currentuser);
		if(currentuser == null && (DataApplication.DEV && "true".equals(request.getParameter("-dev")))) {
			final String sysUserID = com.sok.framework.InitServlet.getSystemParam("SystemUserID");
			logger.debug("using dev mode with user ({})", sysUserID);
			currentuser = new UserBean();
			currentuser.setRequest(request);
			currentuser.setViewSpec("UserView");
			currentuser.setParameter("UserID", com.sok.framework.InitServlet.getSystemParam("SystemUserID"));
			currentuser.doAction(ActionBean.SELECT);
		}
		return currentuser; 
		*/
	}
}
