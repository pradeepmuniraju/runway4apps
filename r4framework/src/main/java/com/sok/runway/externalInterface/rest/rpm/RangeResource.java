package com.sok.runway.externalInterface.rest.rpm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.beans.cms.properties.Range;
import com.sok.service.crm.cms.properties.RangeService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

@Path("/ranges")
@Api(value = "/ranges", description = "Retrieve Range Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class RangeResource {
	
	private static final RangeService rangeService = RangeService.getInstance();
	static final Logger logger = LoggerFactory.getLogger(RangeResource.class);
	@Context HttpServletRequest request;
	private String brandID;
	
	public RangeResource() {}
	public RangeResource(HttpServletRequest request) { this.request = request; }
	public RangeResource(HttpServletRequest request, String brandID) { 
		this(request);
		this.brandID = brandID;
	}

	@GET
	@ApiOperation(value = "List ranges", notes="Optionally filtering by modified date", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Range", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "Ranges")
	public List<Range> getRanges(
			@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = false) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active ranges", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		return rangeService.getRangesExternal(request, brandID, mod, active);
	}
	
	@GET
	@Path("/{rangeID}")
	@ApiOperation(value = "Return a particular range", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Range")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public Range getRange(@Context HttpServletRequest request, 
			@PathParam("rangeID") final String rangeID //,
			//@ApiParam(value = "Retrieve extended range information", required=false, defaultValue="false") @DefaultValue("false") @QueryParam("Extended") boolean extended
			) {
		return rangeService.getRangeExternal(request, rangeID);
	}

	@ApiResource(value = "/{rangeID}/homes", resourceClass="com.sok.runway.externalInterface.rest.rpm.HomeResource")
	@Path("/{rangeID}/homes")
	public HomeResource getHomeResource(
			@ApiParam(value = "The RangeID value as returned from the API", required = true) @PathParam("rangeID") final String rangeID) {
		return new HomeResource(request, rangeID);
	}
	
	@GET
	@ApiResource(value = "/{rangeID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Range", notes="<p>This call will return a list of Albums and their Images for a given Range. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/{rangeID}/albums")
	public AlbumResource getAlbumResource(@PathParam("rangeID") final String rangeID, 
			@ApiParam(value = "Document Sub Type", required = false) @QueryParam("DocumentSubType") String subType, 
			@ApiParam(value = "Album Type", required = false) @QueryParam("AlbumType") String albumType, 
			@ApiParam(value = "Album Publsihed for", allowableValues="Website", required=false, defaultValue="") @DefaultValue("") @QueryParam("Publish") String publish) {
		return new AlbumResource(request,  rangeID, albumType, publish, subType);
	}
}