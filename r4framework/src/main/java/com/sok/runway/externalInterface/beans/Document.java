package com.sok.runway.externalInterface.beans;


import com.sok.runway.externalInterface.beans.shared.PersonName;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Document")
public class Document extends SimpleDocument
{

	private String documenttype = null;
	private String documentcaption = null;
	private String documentsubtype = null;
	private String description = null;
	private String filesize = null;
	private String localfile = null;
	private String documentcategory = null;
	private String productgroupid = null;
	private Date createddate = null;
	private String createdby = null;
	private Date modifieddate = null;
	private String modifiedby = null;
	private String groupid = null;
	private Date checkedoutdate = null;
	private String checkedoutby = null;
	private String documentstatus = null;
	private String documenturl = null;
	private String name = null;

	@XmlElement(name="DocumentType")
	public String getDocumentType()
	{
		return(documenttype);
	}  
	public void setDocumentType(String documenttype)
	{
		this.documenttype = documenttype;
	}  

	@XmlElement(name="DocumentCaption")
	public String getDocumentCaption()
	{
		return(documentcaption);
	}  

	public void setDocumentCaption(String documentcaption)
	{
		this.documentcaption = documentcaption;
	}  

	@XmlElement(name="Name")
	public String getName()
	{
		return(name);
	}  

	public void setName(String name)
	{
		this.name = name;
	}  

	@XmlElement(name="DocumentSubType")
	public String getDocumentSubType()
	{
		return(documentsubtype);
	}  

	public void setDocumentSubType(String documentsubtype)
	{
		this.documentsubtype = documentsubtype;
	}  

	@XmlElement(name="Description")
	public String getDescription()
	{
		return(description);
	}  

	public void setDescription(String description)
	{
		this.description = description;
	}  

	@XmlElement(name="FileSize")
	public String getFileSize()
	{
		return(filesize);
	}  

	public void setFileSize(String filesize)
	{
		this.filesize = filesize;
	}  

	@XmlElement(name="LocalFile")
	public String getLocalFile()
	{
		return(localfile);
	}  

	public void setLocalFile(String localfile)
	{
		this.localfile = localfile;
	}  

	@XmlElement(name="DocumentCategory")
	public String getDocumentCategory()
	{
		return(documentcategory);
	}  

	public void setDocumentCategory(String documentcategory)
	{
		this.documentcategory = documentcategory;
	}  

	@XmlElement(name="ProductGroupID")
	public String getProductGroupID()
	{
		return(productgroupid);
	}  

	public void setProductGroupID(String productgroupid)
	{
		this.productgroupid = productgroupid;
	}  

	@XmlElement(name="CreatedDate")
	public Date getCreatedDate()
	{
		return(createddate);
	}

	public void setCreatedDate(Date createddate)
	{
		this.createddate = createddate;
	}  

	public String getCreatedBy()
	{
		return(createdby);
	}  

	public void setCreatedBy(String createdby)
	{
		this.createdby = createdby;
	}  

	public Date getModifiedDate()
	{
		return(modifieddate);
	}  

	public void setModifiedDate(Date modifieddate)
	{
		this.modifieddate = modifieddate;
	}  

	public String getModifiedBy()
	{
		return(modifiedby);
	}  

	public void setModifiedBy(String modifiedby)
	{
		this.modifiedby = modifiedby;
	}  

	@XmlElement(name="GroupID")
	public String getGroupID()
	{
		return(groupid);
	}  

	public void setGroupID(String groupid)
	{
		this.groupid = groupid;
	}  

	public Date getCheckedOutDate()
	{
		return(checkedoutdate);
	}  

	public void setCheckedOutDate(Date checkedoutdate)
	{
		this.checkedoutdate = checkedoutdate;
	}  

	public String getCheckedOutBy()
	{
		return(checkedoutby);
	}  

	public void setCheckedOutBy(String checkedoutby)
	{
		this.checkedoutby = checkedoutby;
	}  

	@XmlElement(name="DocumentStatus")
	public String getDocumentStatus()
	{
		return(documentstatus);
	}  

	public void setDocumentStatus(String documentstatus)
	{
		this.documentstatus = documentstatus;
	}  

	public String getDocumentURL()
	{
		return(documenturl);
	}  

	public void setDocumentURL(String documenturl)
	{
		this.documenturl = documenturl;
	}  

	private PersonName modifiedbyname = null;
	private PersonName createdbyname = null;
	private PersonName checkedoutbyname = null;

	public PersonName getModifiedByName()
	{
		return(modifiedbyname);
	}

	public void setModifiedByName(PersonName modifiedbyname)
	{
		this.modifiedbyname = modifiedbyname;
	}

	public PersonName getCreatedByName()
	{
		return(createdbyname);
	}

	public void setCreatedByName(PersonName createdbyname)
	{
		this.createdbyname = createdbyname;
	} 

	public PersonName getCheckedOutByName()
	{
		return(checkedoutbyname);
	}

	public void setCheckedOutByName(PersonName checkedoutbyname)
	{
		this.checkedoutbyname = checkedoutbyname;
	}   

	private String error = null;

	public String getError()
	{
		return(error);
	}  

	public void setError(String error)
	{
		this.error = error;
	}
}
