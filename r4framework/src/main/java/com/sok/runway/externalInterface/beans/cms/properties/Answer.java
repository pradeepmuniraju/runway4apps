package com.sok.runway.externalInterface.beans.cms.properties;

public class Answer {
	private String answer;
	
	public Answer(String answer) {
		this.answer = answer;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	public String toString() {
		return answer;
	}

}
