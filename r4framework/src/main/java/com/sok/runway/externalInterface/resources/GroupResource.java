package com.sok.runway.externalInterface.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.beans.User;
import com.sok.service.crm.GroupService;

@Path("/admin/groups")
public class GroupResource {

	private static final GroupResource resource = new GroupResource();
	private static final Logger logger = LoggerFactory.getLogger(GroupResource.class);
	private final GroupService gs = GroupService.getInstance();
	public static GroupResource getInstance() {
		return resource;
	}
	
	@GET
	@Path("{groupID}/users")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getGroupUsers(@Context HttpServletRequest request, @PathParam("groupID") String groupID) {
		logger.trace("getGroupUsers(Request)");		
		return gs.getGroupUsers(request, groupID);
	}
}
