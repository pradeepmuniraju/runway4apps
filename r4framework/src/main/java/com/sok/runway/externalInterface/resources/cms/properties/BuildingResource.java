package com.sok.runway.externalInterface.resources.cms.properties;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.cms.properties.entities.ApartmentEntity;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.cms.properties.Apartment;
import com.sok.runway.externalInterface.beans.cms.properties.Building;
import com.sok.runway.externalInterface.beans.cms.properties.BuildingStage;
import com.sok.service.crm.cms.properties.BuildingService;

@Path("/buildings")
@Produces({ MediaType.APPLICATION_JSON})
public class BuildingResource {

	private static final Logger logger = LoggerFactory.getLogger(BuildingResource.class);
	private static final BuildingService buildingService = BuildingService.getInstance();
	
	private static final BuildingResource resource = new BuildingResource();
	public static BuildingResource getInstance() { return resource; }
	
	@GET
	public List<Building> getBuildings(@Context HttpServletRequest request) {
		return buildingService.getBuildings(request); 
	}
	
	@GET
	@Path("{buildingID}")
	public Building getBuilding(@Context HttpServletRequest request, @PathParam("buildingID") final String builderID) {
		return buildingService.getBuilding(request, builderID);
	}
	
	@GET
	@Path("levels/{levelID}")
	public BuildingStage getBuildingLevel(@Context HttpServletRequest request, @PathParam("levelID") final String levelID) {
		return buildingService.getBuildingStage(request, levelID);
	}
	
	@GET
	@Path("levels/apartments/{apartmentID}")
	public Apartment getApartment(@Context HttpServletRequest request, @PathParam("apartmentID") final String apartmentID) {
		return BeanFactory.external(buildingService.getApartment(request, apartmentID));
	}
}