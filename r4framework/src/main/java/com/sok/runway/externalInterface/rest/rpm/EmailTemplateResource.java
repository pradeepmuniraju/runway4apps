package com.sok.runway.externalInterface.rest.rpm;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.beans.cms.properties.EmailTemplate;
import com.sok.service.crm.cms.properties.EmailTemplateService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

/**
 * Extend and annotate the existing EmailTemplateResource implementation for use with the V2 (Public/Documented) API.
 * 
 * This is seperate from the v1 api endpoint to limit the library requirements on those apps which do not yet use the v2 api.
 */
@Path("/email-templates")
@Api(value = "/email-templates", description = "Retrieve Email Template Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class EmailTemplateResource {

	static final Logger logger = LoggerFactory.getLogger(EmailTemplateResource.class);
	@Context
	HttpServletRequest request;

	public EmailTemplateResource() {
	}

	public EmailTemplateResource(HttpServletRequest request) {
		this.request = request;
	}

	@GET
	@ApiOperation(value = "List all email templates", notes = "Optionally filtering by GroupID or MasterTemplateID", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.EmailTemplate", multiValueResponse = true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid GroupID or  MasterTemplateID value"), @ApiError(code = 401, reason = "Not Authenticated"), @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "EmailTemplates")
	public List<EmailTemplate> getEmailTemplates(
			@ApiParam(value = "GroupID", required = false) @QueryParam("groupID") String groupID,
			@ApiParam(value = "The MasterTemplateID that should be used for retrieval", required = false) @QueryParam("masterTemplateID") final String masterTemplateID,			
			@ApiParam(value = "Return only active emailtemplates", required = false, defaultValue = "true") @DefaultValue("true") @QueryParam("active") boolean active) {
		if (StringUtils.isNotBlank(groupID)) {
			logger.debug("getEmailTemplates(Active {})", active);
			logger.debug("getEmailTemplates(GroupID {} , MasterTemplateID {})", groupID, masterTemplateID);
			
		}
		return EmailTemplateService.getInstance().geEmailTemplates(request, groupID, masterTemplateID, active? "Active" : "Inactive" );
	}

	@GET
	@Path("/{templateID}")
	@ApiOperation(value = "Return a particular email template", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.EmailTemplate")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"), @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public EmailTemplate getEmailTemplate(
			@ApiParam(value = "The TemplateID value as returned from the API", required = true) @PathParam("templateID") final String templateID,
			@ApiParam(value = "contactID", required = false) @QueryParam("ContactID") String contactID, 
			@ApiParam(value = "groupID", required = false) @QueryParam("GroupID") String groupID,
			@ApiParam(value = "authorUserID", required = false) @QueryParam("AuthorUserID") String authorUserID) {
		
		return EmailTemplateService.getInstance().geEmailTemplate(request, templateID, contactID, groupID, authorUserID);
	}
}
