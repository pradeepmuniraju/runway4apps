package com.sok.runway.externalInterface.beans;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

//used to designate that anything else can be ignored.
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.sok.framework.GenRow;

//import org.codehaus.jackson.JsonNode;
//import org.codehaus.jackson.JsonFactory;

@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteViewPage {
	
	public QuoteViewPage() {}
	public QuoteViewPage(GenRow g) {
		this.QuoteViewPageID = g.getData("QuoteViewPageID");
		this.QuoteViewID = g.getData("QuoteViewID");
		this.QuoteViewPageTypeID = g.getData("QuoteViewPageTypeID");
		
		this.PageName = g.getData("PageName");
		this.PageTypeName = g.getData("PageTypeName");
		this.SortOrder = g.getInt("SortOrder");
		this.Layout = g.getData("Layout");
		this.Active = "Y".equals(g.getData("Active"));
		
		this.DisplayScreen = "Y".equals(g.getData("DisplayScreen"));
		this.DisplayPDF = "Y".equals(g.getData("DisplayPDF"));
		this.DisplayOJ = "Y".equals(g.getData("DisplayOJ"));
		this.DisplayKDR = "Y".equals(g.getData("DisplayKDR"));
		this.DisplayHNL = "Y".equals(g.getData("DisplayHNL"));
		this.SelectedOptions = QuoteViewPageOptions.fromString(g.getData("SelectedOptions"));
		this.Conditions = g.getData("Conditions");
	}		
	
	public String QuoteViewPageID; 
	public String QuoteViewID;
	public String QuoteViewPageTypeID;
	public String PageName;
	public String PageTypeName;
	public int SortOrder;
	public String Layout;
	public boolean Active;
	public String Conditions;
	
	
	public boolean DisplayScreen;
	public boolean DisplayPDF;
	public boolean DisplayOJ = true;
	public boolean DisplayKDR = true;
	public boolean DisplayHNL = true;
	//this is actually stored as a json String, but we're using QuoteViewPageOptions to parse the data.
	public QuoteViewPageOptions SelectedOptions;
}
