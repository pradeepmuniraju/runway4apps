package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.cms.SimpleCampaign;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.service.crm.cms.properties.PlanService.HLPackageCost;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Facade")
public class Facade extends SimpleFacade {

	private String productNum, description, createdBy, modifiedBy, summary, active, defaultFacade;
	private Date createdDate, modifiedDate;
	private PersonName modifiedByName = null, createdByName = null;
	private Double GST = null, totalCost = null, price = null;

	private List<ProductDetail> details = null;
	private SimpleDocument image = null;
	private PlanStatusHistory currentStatus = null;
	
	@XmlElement(name="ProductNum")
	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}
	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@XmlElement(name="Summary")
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName(){
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedByName = modifiedbyname;
	}
	
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	@XmlElement(name="Image")
	public SimpleDocument getImage() {
		return image;
	}
	public void setImage(SimpleDocument image) {
		this.image = image;
	}
	
	
	@XmlElement(name="GST")
	public Double getGST() {
		return GST;
	}
	public void setGST(Double gST) {
		this.GST = gST;
	}
	@XmlElement(name="TotalCost")
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	
	@XmlElementWrapper(name = "Details")
	@XmlElement(name="Detail")
	public List<ProductDetail> getDetails() {
		return details;
	}
	public void setDetails(List<ProductDetail> details) {
		this.details = details;
	}
	
	@XmlElement(name="CurrentStatus")
	public PlanStatusHistory getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(PlanStatusHistory currentStatus) {
		this.currentStatus = currentStatus; 
	}

	@XmlElement(name="Active")
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	@XmlElement(name="DefaultFacade")
	public String getDefaultFacade() {
		return defaultFacade;
	}
	public void setDefaultFacade(String defaultFacade) {
		this.defaultFacade = defaultFacade;
	}
}
