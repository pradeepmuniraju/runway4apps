package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlElement;

import com.sok.framework.GenRow;
import com.sok.runway.externalInterface.beans.SimpleDocument;

public class Statuses {

	private String	statusID, status, statusValue, forContatcs, forCompany, forLand, forHome, forPackages, forRealEstate, forSearchOnly;
	private SimpleDocument image;
	
/*
	<field name="StatusID" type="12" dnull="false" ddefault="" dtype="VARCHAR" dlen="50"/>
	<field name="Status" type="12" dnull="false" ddefault="" dtype="VARCHAR" dlen="50"/>
	<field name="StatusValue" type="4" dtype="INT" dlen="10"/>
	<field name="SortNumber" type="4" dtype="INT" dlen="10"/>
	<field name="MainPage" type="12" dtype="VARCHAR" dlen="10"/>
	<field name="ListName" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="Inactive" type="12" dtype="CHAR" dlen="1"/>
	<field name="GroupID" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="ForContacts" type="12" dtype="VARCHAR" dlen="5"/>
    <field name="ForCompanies" type="12" dtype="VARCHAR" dlen="5"/>	
    <field name="ForApartments" type="12" dtype="VARCHAR" dlen="1"/>
    <field name="ForLand" type="12" dtype="VARCHAR" dlen="1"/>
    <field name="ForHome" type="12" dtype="VARCHAR" dlen="1"/>
    <field name="ForHouseAndLand" type="12" dtype="VARCHAR" dlen="1"/>
    <field name="ForRealEstate" type="12" ddefault="N" dtype="VARCHAR" dlen="1"/>
    <field name="ForSearchOnly" type="12" ddefault="N" dtype="CHAR" dlen="1"/>
*/
	
	public Statuses() {
	}

	public Statuses(String statusID, String status, String statusValue) {
		this.statusID = statusID;
		this.status = status;
		this.statusValue = statusValue;
	}

	public Statuses(GenRow status) {
		this.statusID = status.getData("StatusID");
		this.status = status.getData("Status");
		this.statusValue = status.getData("StatusValue");
		
		if ("Group".equals(status.getData("ListName"))) {
			this.forCompany = status.getString("ForCompanies");
			this.forContatcs = status.getString("ForContacts");
		}
		if ("Product".equals(status.getData("ListName"))) {
			this.forLand = status.getString("ForLand");
			this.forHome = status.getString("ForHome");
			this.forPackages = status.getString("ForHouseAndLand");
			this.forRealEstate = status.getString("ForRealEstate");
			this.forSearchOnly = status.getString("ForSearchOnly");
		}
	}

	@XmlElement(name="StatusID")
	public String getStatusID() {
		return statusID;
	}

	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}

	@XmlElement(name="Status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@XmlElement(name="Value")
	public String getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}

	@XmlElement(name="ForContacts")
	public String getForContatcs() {
		return forContatcs;
	}

	public void setForContatcs(String forContatcs) {
		this.forContatcs = forContatcs;
	}

	@XmlElement(name="ForCompany")
	public String getForCompany() {
		return forCompany;
	}

	public void setForCompany(String forCompany) {
		this.forCompany = forCompany;
	}

	@XmlElement(name="ForLand")
	public String getForLand() {
		return forLand;
	}

	public void setForLand(String forLand) {
		this.forLand = forLand;
	}

	@XmlElement(name="ForHomes")
	public String getForHome() {
		return forHome;
	}

	public void setForHome(String forHome) {
		this.forHome = forHome;
	}

	@XmlElement(name="ForPackages")
	public String getForPackages() {
		return forPackages;
	}

	public void setForPackages(String forPackages) {
		this.forPackages = forPackages;
	}

	@XmlElement(name="ForRealEstate")
	public String getForRealEstate() {
		return forRealEstate;
	}

	public void setForRealEstate(String forRealEstate) {
		this.forRealEstate = forRealEstate;
	}

	@XmlElement(name="ForSerachOnly")
	public String getForSearchOnly() {
		return forSearchOnly;
	}

	public void setForSearchOnly(String forSearchOnly) {
		this.forSearchOnly = forSearchOnly;
	}

	@XmlElement(name="Image")
	public SimpleDocument getImage() {
		return image;
	}

	public void setImage(SimpleDocument image) {
		this.image = image;
	}

}
