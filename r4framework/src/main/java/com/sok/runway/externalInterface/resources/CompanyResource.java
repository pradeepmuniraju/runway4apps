package com.sok.runway.externalInterface.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.beans.Company;
import com.sok.service.crm.CompanyService;

@Path("companies")
public class CompanyResource {

	private static final CompanyResource resource = new CompanyResource();
	private static final Logger logger = LoggerFactory.getLogger(ContactResource.class);
	private final CompanyService cs = CompanyService.getInstance();
	public static CompanyResource getInstance() {
		return resource;
	}
	
	final String[] quickNoteFields = { "-Department", "-Company", "-Email","-quickSearch"};
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Company> getCompanies(@Context HttpServletRequest request) {
		logger.trace("getCompanies(Request)");
		for(String s: quickNoteFields) {
			if(StringUtils.isNotBlank(request.getParameter(s))) {
				return cs.getQuicknoteCompanyMatches(request);
			}
		}
		throw new UnsupportedOperationException("Not yet implemented for general search");
	}
}
