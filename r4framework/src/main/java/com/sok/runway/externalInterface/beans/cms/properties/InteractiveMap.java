package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.runway.externalInterface.beans.SimpleDocument;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="InteractiveMap")
@XmlType(propOrder={"mapImage", "mapHTML"})
public class InteractiveMap {
	private String majorID, minorID;
	private SimpleDocument mapImage;
	
	private String mapHTML; 
	
	private String productType = "";
	
	private HashMap<String,InteractiveMap.LotDetails> 	lots = null;
	private HashMap<String,String>						status = null;
	
	public InteractiveMap() {}
	
	public String getMajorID() {
		return majorID;
	}
	public void setMajorID(String majorID) {
		this.majorID = majorID;
	}
	public String getMinorID() {
		return minorID;
	}
	public void setMinorID(String minorID) {
		this.minorID = minorID;
	}
	@XmlElement(name="MapImage")
	public SimpleDocument getMapImage() {
		return mapImage;
	}
	public void setMapImage(SimpleDocument mapImage) {
		this.mapImage = mapImage;
	}
	
	@XmlElement(name="MapHTML") 
	public String getMapHTML() {
		return mapHTML;
	}
	public void setMapHTML(String mapHTML) {
		this.mapHTML = mapHTML;
	}
	
	public void loadMapHTML(String filePath) {
		if (filePath == null || filePath.length() == 0) return;
		String apiURL = InitServlet.getSystemParam("swagger.api.basepath");
		System.out.println(apiURL);
		try {
		    BufferedReader br = new BufferedReader(new FileReader(InitServlet.getRealPath(filePath)));
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();

		        while (line != null) {
		            if (majorID != null && minorID == null) line = processEstateMap(line);
		            else if (majorID != null && minorID != null) line = processStageMap(line); 
		            sb.append(line.trim());
		            //sb.append(System.lineSeparator());
		            line = br.readLine();
		        }
		        mapHTML = sb.toString();
		    } finally {
		        br.close();
		    }
		} catch (Exception e){}
	}

	private String processStageMap(String line) {
		String ret = line;
		int s = 0;
		while ((s = ret.toLowerCase().indexOf("href",s)) >= 0) {
			if (ret.toLowerCase().indexOf("href=\"",s) == s) s += "href=\"".length(); 
			else if (ret.toLowerCase().indexOf("href = \"",s) == s) s += "href = \"".length();
			int e = ret.toLowerCase().indexOf("\"",s);
			if (e > s) {
				String lot = ret.substring(s,e);
				if (!lot.toLowerCase().startsWith("http")) {
					LotDetails lotDetails = getLot(majorID, minorID, lot);
					if (lotDetails != null) {
						String zoomAPI = "/api/2/lots/" + lotDetails.productID;
						String lotsAPI = "/api/2/search/land/?query=EstateID%3D" + majorID + "%26StateID%3D" + minorID + "%26ProductID%3D" + lotDetails.productID;
						String packagesAPI = "/api/2/search/packages/?query=EstateID%3D" + majorID + "%26StateIDID%3D" + minorID + "%26ProductID%3D" + lotDetails.productID;
						
						String all = " lots=\"" + lotsAPI + "\" packages=\"" + packagesAPI + "\" ";
						if (hasMap(lotDetails.productID)) all += "zoom=\"" + zoomAPI + "\" ";
						String icon = lotDetails.statusIcon != null? lotDetails.statusIcon : "";
						String all2 = " link=\"javascript:doLotSelect('" + lotDetails.productID + "', '" + lotDetails.forSale + "');\" id=\"lot" + lotDetails.productID + "\" forsale=\"" 
								+ lotDetails.forSale + "\" status=\"" + lotDetails.status + "\" statusIcon=\"" + icon + "\"  " + all;
						
						++e;
						ret = ret.substring(0,e) + all2 + ret.substring(e);
					} else {
						String all = " link=\"javascript:doLotMissing('" + ret.substring(s, e) + "', 'false');\"  forsale=\"false\"";
						++e;
						ret = ret.substring(0,e) + all + ret.substring(e);
					}
				} else {
					String all = " link=\"" + ret.substring(s, e) + "\"  forsale=\"false\"";
					++e;
					ret = ret.substring(0,e) + all + ret.substring(e);
				}
			}
			
			++s;
		}
		
		return ret;
	}

	private LotDetails getLot(String majorID, String minorID, String lot) {
		if (lots == null) {
			lots = new HashMap<String, InteractiveMap.LotDetails>();
			
			String availableID = InitServlet.getSystemParam("ProductAvailableStatus");
			
			GenRow row = new GenRow();
			row.setViewSpec("properties/ProductQuickIndexView");
			row.setParameter("EstateID", majorID);
			row.setParameter("StageID", minorID);
			row.setParameter("ProductType", productType);
			row.doAction("search");
			
			row.getResults();
			
			while (row.getNext()) {
				if (row.isSet("Name")) {
					LotDetails ld = new LotDetails();
					ld.productID = row.getString("ProductID");
					ld.name = row.getString("Name");
					ld.status = row.getString("CurrentStatus");
					ld.active = "Y".equals(row.getString("Active"));
					ld.forSale = "Y".equals(row.getString("Active")) && row.getString("CurrentStatusID").equals(availableID);
					ld.statusIcon = getStatusIcon(row.getString("CurrentStatusID"));
					
					lots.put(row.getString("Name").toLowerCase(), ld);
				}
			}
			
			row.remove("StageID");
			row.doAction("search");
			
			row.getResults();
			
			while (row.getNext()) {
				if (row.isSet("Name") && !lots.containsKey(row.getString("Name").toLowerCase())) {
					LotDetails ld = new LotDetails();
					ld.productID = row.getString("ProductID");
					ld.name = row.getString("Name");
					ld.status = row.getString("CurrentStatus");
					ld.active = "Y".equals(row.getString("Active"));
					ld.forSale = "Y".equals(row.getString("Active")) && row.getString("CurrentStatusID").equals(availableID);
					ld.statusIcon = getStatusIcon(row.getString("CurrentStatusID"));
					
					lots.put(row.getString("Name").toLowerCase(), ld);
				}
			}
			
			row.close();
		}
		
		if (lot == null) return null;
		
		return lots.get(lot.toLowerCase());
	}

	private String getStatusIcon(String statusID) {
		if (status == null) {
			status = new HashMap<String, String>();
			
			GenRow row = new GenRow();
			row.setViewSpec("LinkedDocumentView");
			row.setColumn("DocumentType","Image");
			row.setColumn("GalleryID", "LOT_BROWSER_ICON");
			row.doAction("search");
			
			row.getResults();
			
			String url = InitServlet.getSystemParam("URLHome");
			
			while (row.getNext()) {
				if (row.isSet("NoteID")) status.put(row.getString("NoteID"), url + "/" + row.getString("FilePath"));
			}
			
			row.close();
		}
		
		return status.get(statusID);
	}

	private String processEstateMap(String line) {
		if (majorID == null || majorID.length() == 0) return line;
		
		String ret = line;

		int s = 0;
		
		while ((s = ret.toLowerCase().indexOf("href",s)) >= 0) {
			if (ret.toLowerCase().indexOf("href=\"",s) == s) s += "href=\"".length(); 
			else if (ret.toLowerCase().indexOf("href = \"",s) == s) s += "href = \"".length();
			int e = ret.toLowerCase().indexOf("\"",s);
			if (e > s) {
				String stg = ret.substring(s,e);
				if (!stg.toLowerCase().startsWith("http")) {
					String stageID = getminorID(majorID, stg);
					if (stageID != null && stageID.length() > 0) {
						String zoomAPI = "/api/2/estates/" + majorID + "/stages/" + stageID;
						String lotsAPI = "/api/2/search/land/?query=majorID%3D" + majorID + "%26minorID%3D" + stageID;
						String packagesAPI = "/api/2/search/packages/?query=majorID%3D" + majorID + "%26minorID%3D" + stageID;
						
						String all = " lots=\"" + lotsAPI + "\" packages=\"" + packagesAPI + "\" ";
						if (hasMap(stageID)) all += "zoom=\"" + zoomAPI + "\" ";
						String all2 = " link=\"javascript:doStageSelect('" + stageID + "');\"  StageID=\"" + stageID + "\" " + all;
						++e;
						ret = ret.substring(0,e) + all2 + ret.substring(e);
					} else {
						String all = " link=\"javascript:doStageMissing('" + ret.substring(s,e) + "');\"";
						++e;
						ret = ret.substring(0,e) + all + ret.substring(e);
					}
				} else {
					String all = " link=\"" + ret.substring(s,e) + "\"";
					++e;
					ret = ret.substring(0,e) + all + ret.substring(e);
				}
			}
			
			++s;
		}
		
		return ret;
	}

	private String getminorID(String majorID, String stage) {
		if (majorID == null || majorID.length() == 0 || stage == null || stage.length() == 0) return null;
		
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductStageView");
		row.setParameter("Name|1", stage);
		row.setParameter("Name|1", "Stage " + stage);
		row.setParameter("EstateProductID", majorID);
		row.setParameter("ProductParentEstateStage.ProductVariationEstateStage.ProductID", majorID);
		row.doAction("selectfirst");
		
		return row.getString("ProductID");
	}
	
	private boolean hasMap(String productID) {
		GenRow maps = new GenRow();
		maps.setViewSpec("LinkedDocumentView");
		maps.setParameter("ProductID",productID);
		maps.setParameter("DocumentSubType","MasterPlan");
		maps.setParameter("LinkedDocumentDocument.DocumentSubType","MasterPlan");
		maps.setParameter("FileName","%.jpg+%.png");
		maps.setParameter("LinkedDocumentDocument.FileName","%.jpg+%.png");
		maps.doAction("selectfirst");
		
		return maps.isSet("LinkedDocumentID");
	}
	
	
	private class LotDetails {
		public String productID;
		public String name;
		public String status;
		public String statusIcon;
		public boolean active = false;
		public boolean forSale = false;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

}
