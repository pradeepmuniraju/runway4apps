package com.sok.runway.externalInterface.beans.shared;
import java.util.*;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class Field
{
   private String name = null;
   private String stringvalue = null;   
   private Date datevalue = null;  
   
   public String getName()
   {
      return(name);
   }  
   
   public void setName(String name)
   {
      this.name = name;
   }     
   
   public String getStringValue()
   {
      return(stringvalue);
   }  
   
   public void setStringValue(String stringvalue)
   {
      this.stringvalue = stringvalue;
   }    

   public Date getDateValue()
   {
      return(datevalue);
   }  
   
   public void setDateValue(Date datevalue)
   {
      this.datevalue = datevalue;
   }  
}