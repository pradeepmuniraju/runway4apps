package com.sok.runway.externalInterface.rest.rpm;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.Album;
import com.sok.service.crm.cms.properties.ImageService;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

//@Path("/albums")
//@Api(value = "/albums", description = "Retrieve Album Information")
@XmlRootElement(name = "AlbumResource")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class AlbumResource {

	private String productID = null, albumType = null, publish = null, subType = null;
	private static final ImageService imageService = ImageService.getInstance();

	static final Logger logger = LoggerFactory.getLogger(AlbumResource.class);
	@Context HttpServletRequest request;

	public AlbumResource() {}
	public AlbumResource(HttpServletRequest request, String productID) {
		this.request = request;
		this.productID = productID; 
	}
	
	public AlbumResource(HttpServletRequest request, String productID, String albumType, String publish) {
		this.request = request;
		this.productID = productID;
		this.albumType = albumType;
		this.publish = publish;
	}
	
	public AlbumResource(HttpServletRequest request, String productID, String albumType, String publish, String subType) {
		this.request = request;
		this.productID = productID;
		this.subType = subType;
		this.albumType = albumType;
		this.publish = publish;
	}
	
	@GET
	@ApiOperation(value = "List Albums", notes="", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Album", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "Album")
	public List<Album> getAlbums() {
		return imageService.getAlbums(request, productID, albumType, publish, subType);
	}

	@GET
	@ApiOperation(value = "List Albums", notes="", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Album", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "Album")
	@Path("/type")
	public List<Album> getAlbums(@ApiParam(value = "Document Sub Type", required = false) @QueryParam("DocumentSubType") String subType, 
			@ApiParam(value = "Album Type", required = false) @QueryParam("AlbumType") String albumType,
			@ApiParam(value = "Album Publsihed for", allowableValues=",Website", required=false, defaultValue="Website") @DefaultValue("Website") @QueryParam("Publish") String publish) {
		return imageService.getAlbums(request, productID, albumType, publish, subType);
	}
	
}
