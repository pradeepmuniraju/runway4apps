package com.sok.runway.externalInterface.beans.shared;
import java.util.*;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class CompanyQuestion extends Question
{
	private CompanyAnswer answer = null;
	
	public CompanyAnswer getAnswer()
	{
		return(answer);
	}	
	
	public void setAnswer(CompanyAnswer answer)
	{
		this.answer = answer;
	}		
}