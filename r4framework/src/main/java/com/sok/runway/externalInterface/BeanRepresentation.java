package com.sok.runway.externalInterface;

public enum BeanRepresentation {
	Simple, Extended, Full, Internal;
}
