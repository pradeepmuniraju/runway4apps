package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "EmailTemplate")
@XmlType(propOrder = { "templateID", "masterTemplateID", "templateName", "templateBody", "replacedTemplateBody" })
public class EmailTemplate {
	private String templateID, masterTemplateID, templateName, templateBody, replacedTemplateBody;
	
	public EmailTemplate(GenRow g) {
		this.templateID = g.getString("TemplateID");
		this.masterTemplateID = g.getString("MasterTemplateID");
		this.templateName = g.getString("TemplateName");
	}
	
	// @JsonProperty("TemplateID")
	@XmlElement(name = "TemplateID")
	public String getTemplateID() {
		return templateID;
	}

	public void setTemplateID(String templateID) {
		this.templateID = templateID;
	}

	// @JsonProperty("MasterTemplateID")
	@XmlElement(name = "MasterTemplateID")
	public String getMasterTemplateID() {
		return masterTemplateID;
	}

	public void setMasterTemplateID(String masterTemplateID) {
		this.masterTemplateID = masterTemplateID;
	}

	// @JsonProperty("TemplateName")
	@XmlElement(name = "TemplateName")
	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	// @JsonProperty("TemplateBody")
	@XmlElement(name = "TemplateBody")
	public String getTemplateBody() {
		return templateBody;
	}

	public void setTemplateBody(String templateBody) {
		this.templateBody = templateBody;
	}

	// @JsonProperty("ReplacedTemplateBody")
	@XmlElement(name = "ReplacedTemplateBody")
	public String getReplacedTemplateBody() {
		return replacedTemplateBody;
	}

	public void setReplacedTemplateBody(String replacedTemplateBody) {
		this.replacedTemplateBody = replacedTemplateBody;
	}

}
