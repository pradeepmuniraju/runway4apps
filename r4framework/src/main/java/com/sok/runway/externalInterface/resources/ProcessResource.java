package com.sok.runway.externalInterface.resources;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.beans.shared.ContactProfile;
import com.sok.runway.externalInterface.beans.shared.OpportunityProfile;
import com.sok.runway.externalInterface.beans.shared.Profile;
import com.sok.service.crm.ProfileService;
import com.sok.service.crm.UserService;
import com.sok.service.crm.ProfileService.ProfileType;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

@Path("/processes")
public class ProcessResource {
	private static final ProcessResource resource = new ProcessResource();
	private static final Logger logger = LoggerFactory.getLogger(ProcessResource.class);
	private static final UserService us = UserService.getInstance(); 
	private static final ProfileService profileService = ProfileService.getInstance(); 

	public static ProcessResource getInstance() {
		return resource;
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Process> getProcesses(@Context HttpServletRequest request, @QueryParam("ContactID") String contactID) {
		
		UserBean user = us.getCurrentUser(request);
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!user.canAccess("Opportunities")) {
			throw new AccessException("You do not have permission to access the processes module");
		}
		if(StringUtils.isBlank(contactID)) {
			throw new IllegalArgumentException("only searching via contact is supported");
		}
		
		GenRow procs = new GenRow();
		try { 
			procs.setRequest(request);
			procs.setViewSpec("OpportunityListView");
			procs.setParameter("-select0","OpportunityProcessStage.ProcessStageStage.CanAddQuote AS CanAddQuote");
			procs.setParameter("Inactive","!Y");
			procs.setParameter("ContactID", contactID);
			
			if(logger.isDebugEnabled()) {
				procs.doAction(GenerationKeys.SEARCH);
				logger.debug(procs.getStatement());
			}
			procs.getResults(true);
			
			if(procs.getNext()) {
				List<Process> list = new ArrayList<Process>(procs.getSize());
				do {
					list.add(new Process(procs));
				} while(procs.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally { 
			procs.close();
		}
	}
	
	
	@GET
	@Path("{processID}/profiles")
	@Produces(MediaType.APPLICATION_JSON)
	public List<OpportunityProfile> getProcessProfiles(@Context final HttpServletRequest request, @PathParam("processID") String processID) {
		logger.trace("getProcessProfiles(Request,{})", processID);
		// TODO - fix the types here to use generics etc. 
		final List<Profile> pl = profileService.getProfiles(request, ProfileType.OPPORTUNITY, processID );
		final List<OpportunityProfile> cpl = new ArrayList<OpportunityProfile>(pl.size());
		for(Profile p: pl) {
			cpl.add((OpportunityProfile)p);
		}
		return cpl;
	}
	
	//@PUT
	//@Path("{processID}/profiles/")
	//public List<OpportunityProfile> setContactProfiles(@Context HttpServletRequest request, @PathParam("processID") String processID, List<ContaOpportunityProfilectProfile> profiles) {
	//	return cs.updateProfiles(request, contactID, profiles);
	//}
	
	@PUT
	@Path("{processID}/profiles/{surveyID}")
	@Produces(MediaType.APPLICATION_JSON)
	public OpportunityProfile updateContactProfile(@Context HttpServletRequest request, @PathParam("processID") String processID, @PathParam("surveyID") String surveyID, OpportunityProfile contactProfile) {
		logger.trace("ContactProfile(Request,{},{})", contactProfile, surveyID);
		if(StringUtils.isBlank(processID)) {
			throw new IllegalArgumentException("ProcessID blank");
		}
		if(StringUtils.isBlank(surveyID)) {
			throw new IllegalArgumentException("SurveyID blank");
		}		
		if(!surveyID.equals(contactProfile.getSurveyID())) {
			throw new IllegalArgumentException("SurveyID did not match survey id in object");
		}
		return profileService.saveOpportunityProfile(request, processID, contactProfile);
	}
	
	public static class Process { 
		public String OpportunityID;
		public String Name; 
		public String RepUserID; 
		public String RepName;
		public String Product;
		public String Inactive;
		public String Status;
		public boolean CanAddQuote;
		
		public Process() {} 
		
		public Process(GenRow p) { 
			this.OpportunityID = p.getData("OpportunityID");
			this.Name = p.getData("Name");
			this.RepName = new StringBuilder().append(p.getData("RepFirstName")).append(" ").append(p.getData("RepLastName")).toString();
			this.RepUserID = p.getData("RepUserID");
			this.Product = p.getData("ReferenceProductName");
			this.Status = p.getData("CurrentStatus");
			this.CanAddQuote = "N".equals(p.getData("Inactive")) && "Y".equals(p.getData("CanAddQuote"));
			this.Inactive = p.getData("Inactive");
		}
	}
}
