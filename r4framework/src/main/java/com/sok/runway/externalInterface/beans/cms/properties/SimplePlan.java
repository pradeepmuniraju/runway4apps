package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@XmlAccessorType(XmlAccessType.NONE)
//@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement(name="Plan")
@XmlType(propOrder={"planID","originalPlanID", "name","cost","home" })		//MarketingStatus AvailableDate
//@JsonPropertyOrder(value={"planID","Name","Cost", "home"})
public abstract class SimplePlan {
	protected String planID, name, originalPlanID;
	protected Double cost;
	protected SimpleHome home;
	
	public SimplePlan() {}
	
	@XmlElement(name="PlanID")
	public String getPlanID() {
		return planID;
	}
	public void setPlanID(String planID) {
		this.planID = planID;
	}
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name="Cost")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	@XmlElement(name="Home")
	public SimpleHome getHome() {
		return home;
	}
	public void setHome(SimpleHome home) {
		this.home = home;
	}

	@XmlElement(name="OriginalPlanID")
	public String getOriginalPlanID() {
		return originalPlanID;
	}

	public void setOriginalPlanID(String originalPlanID) {
		this.originalPlanID = originalPlanID;
	}
}
