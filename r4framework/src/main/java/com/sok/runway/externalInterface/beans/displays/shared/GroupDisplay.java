package com.sok.runway.externalInterface.beans.displays.shared;

import com.sok.runway.externalInterface.beans.displays.shared.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class GroupDisplay
{
	private String groupLabel = null;
	private boolean groupNotUsed = false;
	
	public String getGroupLabel()
	{
		return(groupLabel);
	}	
	
	public void setGroupLabel(String groupLabel)
	{
		this.groupLabel = groupLabel;
	}	
	
	public boolean getGroupNotUsed()
	{
		return(groupNotUsed);
	}	
	
	public void setGroupNotUsed(boolean groupNotUsed)
	{
		this.groupNotUsed = groupNotUsed;
	}	
}