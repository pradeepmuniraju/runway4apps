package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;

import com.sok.framework.GenRow;

public class Questions {
	
	private String questionID, question, shortLable, indentLevel, divider, mandatory, readOnly, description, inputType, includeOther, answer;
	private ArrayList<String> values;
/*	
	<data fieldname="SurveyQuestionID" />
	<data fieldname="QuestionID" />
	<data fieldname="SurveyID" />
	<data fieldname="SortOrder" />
	<data fieldname="IndentLevel" />
	<data fieldname="Divider" />
	<data fieldname="Quantifier" />
	<data fieldname="Disabled" />
	<data fieldname="Mandatory" />
	<data fieldname="ReadOnly" />
	<foreigndata asname="GroupID" fieldname="GroupID" relationship="SurveyQuestionSurvey" />
	<foreigndata asname="SectionCode" fieldname="SectionCode" relationship="SurveyQuestionSurvey.SurveyGroup" />
	<foreigndata asname="Label" fieldname="Label" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="ShortLabel" fieldname="ShortLabel" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="ProfileType" fieldname="ProfileType" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="CreatedDate" fieldname="CreatedDate" format="M" formattype="datetime" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="Description" fieldname="Description" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="ExportLabel" fieldname="ExportLabel" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="ShortLabel" fieldname="ShortLabel" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="InputType" fieldname="InputType" relationship="SurveyQuestionQuestion" />
    <foreigndata asname="IncludeOther" fieldname="IncludeOther" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="CalculationType" fieldname="CalculationType" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="CalculationPeriod" fieldname="CalculationPeriod" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="ValueList" fieldname="ValueList" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="RelatedQuestionID" fieldname="RelatedQuestionID" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="CalculationQuestionID" fieldname="CalculationQuestionID" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="ParentLabel" fieldname="Label" relationship="SurveyQuestionQuestion.QuestionParentQuestion" />
	<foreigndata asname="SurveyName" fieldname="Name" relationship="SurveyQuestionSurvey" />
	<foreigndata asname="OwnFirstName" fieldname="FirstName" relationship="SurveyQuestionQuestion.QuestionCreated" />
	<foreigndata asname="OwnLastName" fieldname="LastName" relationship="SurveyQuestionQuestion.QuestionCreated" />
	<foreigndata asname="DefaultGraph" fieldname="DefaultGraph" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="SetsStatus" fieldname="SetsStatus" relationship="SurveyQuestionQuestion" />
	<foreigndata asname="SetsLocation" fieldname="SetsLocation" relationship="SurveyQuestionQuestion" />	
*/
	
	public Questions() {
		
	}
	public Questions(GenRow data) {
		questionID = data.getData("QuestionID");
		question = data.getData("Label");
		description = data.getData("Description");
		divider = data.getData("Divider");
		includeOther = data.getData("IncludeOther");
		indentLevel = data.getData("IndentLevel");
		inputType = data.getData("InputType");
		mandatory = data.getData("Mandatory");
		readOnly = data.getData("ReadOnly");
		shortLable = data.getData("ShortLabel");
		
	}
	@XmlElement(name="QuestionID")
	public String getQuestionID() {
		return questionID;
	}
	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}
	@XmlElement(name="Question")
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	@XmlElement(name="ShortLable")
	public String getShortLable() {
		return shortLable;
	}
	public void setShortLable(String shortLable) {
		this.shortLable = shortLable;
	}
	@XmlElement(name="IndentLevel")
	public String getIndentLevel() {
		return indentLevel;
	}
	public void setIndentLevel(String indentLevel) {
		this.indentLevel = indentLevel;
	}
	@XmlElement(name="Divider")
	public String getDivider() {
		return divider;
	}
	public void setDivider(String divider) {
		this.divider = divider;
	}
	@XmlElement(name="Mandatory")
	public String getMandatory() {
		return mandatory;
	}
	public void setMandatory(String mandatory) {
		this.mandatory = mandatory;
	}
	@XmlElement(name="ReadOnly")
	public String getReadOnly() {
		return readOnly;
	}
	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}
	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@XmlElement(name="InputType")
	public String getInputType() {
		return inputType;
	}
	public void setInputType(String inputType) {
		this.inputType = inputType;
	}
	@XmlElement(name="IncludeOther")
	public String getIncludeOther() {
		return includeOther;
	}
	public void setIncludeOther(String includeOther) {
		this.includeOther = includeOther;
	}
	@XmlElement(name="Values")
	public ArrayList<String> getValues() {
		return values;
	}
	public void setValues(ArrayList<String> values) {
		this.values = values;
	}
	public void addValues(String value) {
		if (this.values == null) this.values = new ArrayList<String>();
		this.values.add(value);
	}
	@XmlElement(name="Answer")
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	

}
