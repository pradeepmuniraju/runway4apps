package com.sok.runway.externalInterface.rest.rpm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.service.crm.cms.properties.DripService;
import com.sok.service.crm.cms.properties.DripService.DripCollection;
import com.sok.service.crm.cms.properties.DripService.DripLibrary;
import com.sok.service.crm.cms.properties.DripService.DripProduct;
import com.wordnik.swagger.annotations.ApiClass;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "DripCollection")
public class DripCollectionExternal{

	private DripCollection dripCollection = null;
	
	public DripCollectionExternal(DripCollection dripCollection) {
		this.dripCollection = dripCollection;
	}
	@XmlElement(name="IncludedDripList")
	public List<HashMap<String, DripLibrary>> getIncludedDripList() {
		return dripCollection.includedProductList;
	}

	@XmlElement(name="OptionalDripList")
	public List<HashMap<String, DripLibrary>> getOptionalDripList() {
		return dripCollection.optionalProductList;
	}

	@XmlElement(name="IncludedDripAmount")
	public double getIncludedDripAmount() {
		return dripCollection.includedDripAmount;
	}
	@XmlElement(name="OptionalDripAmount")
	public double getOptionalDripAmount() {
		return dripCollection.optionalDripAmount;
	}
}
