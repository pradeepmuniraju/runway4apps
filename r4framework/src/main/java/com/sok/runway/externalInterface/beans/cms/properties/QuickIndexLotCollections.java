package com.sok.runway.externalInterface.beans.cms.properties;

import java.text.NumberFormat;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.GenRow;
import com.sok.framework.RunwayUtil;

public class QuickIndexLotCollections extends QuickIndexLot {

	private List<QuickIndexLot> list = null;
	private double minTotalCost = -1, maxTotalCost = 0;
	private double minAdvCost = -1, maxAdvCost = 0;
	private double minArea = -1, maxArea = 0;
	private double minWidth = -1, maxWidth = 0;
	private double minDepth = -1, maxDepth = 0;
	
	private final NumberFormat nf = NumberFormat.getInstance();
	/**
	 * 
	 */
	public QuickIndexLotCollections() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param row
	 */
	public QuickIndexLotCollections(GenRow row) {
		super(row);
		if ("ProductSubType".equals(row.getString("-groupby0"))) {
			GenRow item = new GenRow();
			item.setViewSpec("ListItemView");
			item.setConnection(row.getConnection());
			item.setParameter("ListName", this.getEstateName().replaceAll(" ", "") + "LotSubType");
			item.setParameter("ListItemItemList.ListName", this.getEstateName().replaceAll(" ", "") + "LotSubType");
			item.setParameter("ItemValue", this.getProductSubType());
			
			if (item.isSet("ItemValue")) {
				item.doAction("selectfirst");
				
				if (item.isSet("Description")) {
					this.setDescription(item.getString("Description"));
					this.setHeadline("");
				}
			}
		}
		nf.setMaximumFractionDigits(0);
	}

	@XmlElement(name="Lots")
	public List<QuickIndexLot> getLotList() {
		return list;
	}

	public void setLotList(List<QuickIndexLot> list) {
		this.list = list;
		
		String displayType = "";
		
		for (int l = 0; l < list.size(); ++l) {
			QuickIndexLot qia = list.get(l);
			
			if (StringUtils.isBlank(displayType)) displayType = qia.getDisplayPriceType();
			
			double Area = qia.getLotArea();
			if (Area > 0) {
				if (Area < minArea || minArea == -1) minArea = Area;
				if (Area > maxArea) maxArea = Area;
			}
			double Width = qia.getLotWidth();
			if (Width > 0) {
				if (Width < minWidth || minWidth == -1) minWidth = Width;
				if (Width > maxWidth) maxWidth = Width;
			}
			double Depth = qia.getLotDepth();
			if (Depth > 0) {
				if (Depth < minDepth || minDepth == -1) minDepth = Depth;
				if (Depth > maxDepth) maxDepth = Depth;
			}
			
			double totalCost = qia.getTotalCost();
			if (totalCost > 0) {
				if (totalCost < minTotalCost || minTotalCost == -1) minTotalCost = totalCost;
				if (totalCost > maxTotalCost) maxTotalCost = totalCost;
			}
			double advCost = qia.getAdvertisedCost();
			if (advCost > 0) {
				if (advCost < minAdvCost || minAdvCost == -1) minAdvCost = advCost;
				if (advCost > maxAdvCost) maxAdvCost = advCost;
			}
		}

		if (StringUtils.isBlank(displayType)) displayType = getEstateDisplayType(this.getEstateID());
		
		if (minAdvCost != 0) {
			if (minAdvCost != maxAdvCost)
				this.setDisplayPrice(RunwayUtil.displayPrice(displayType, minAdvCost, maxAdvCost));
			else 
				this.setDisplayPrice(RunwayUtil.displayPrice(displayType, minAdvCost));
		} else if (minTotalCost != 0) {
			if (minTotalCost != maxTotalCost)
				this.setDisplayPrice(RunwayUtil.displayPrice(displayType, minTotalCost, maxTotalCost));
			else
				this.setDisplayPrice(RunwayUtil.displayPrice(displayType, minTotalCost));
		} else if (list.size() > 0){
			this.setDisplayPrice("POA");
		}
	}
	
	private String getEstateDisplayType(String estateID) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setParameter("-select0", "DisplayPriceType");
		row.setParameter("ProductID", estateID);
		row.setParameter("ProductType", "Estate");
		if (row.isSet("ProductID")) row.doAction("selectfirst");
		
		return row.getString("DisplayPriceType");
	}

	@XmlElement(name="LotArea")
	public String getArearoomsVal() {
		if (minArea > maxArea || minArea == -1) minArea = maxArea;
		if (minArea == maxArea) return String.valueOf(maxArea).replaceAll("\\.0$","");
		
		return ("" + minArea).replaceAll("\\.0$","") + ("-" + maxArea).replaceAll("\\.0$","");
	}
	
	public void setAreaVal(String val) {
		
	}
	
	@XmlElement(name="LotWidth")
	public String getWidthVal() {
		if (minWidth > maxWidth || minWidth == -1) minWidth = maxWidth;
		if (minWidth == maxWidth) return String.valueOf(maxWidth).replaceAll("\\.0$","");
		
		return ("" + minWidth).replaceAll("\\.0$","") + ("-" + maxWidth).replaceAll("\\.0$","");
	}
	
	public void setWidthVal(String val) {
		
	}
	
	@XmlElement(name="LotDepth")
	public String getDepthVal() {
		if (minDepth > maxDepth || minDepth == -1) minDepth = maxDepth;
		if (minDepth == maxDepth) return String.valueOf(maxDepth).replaceAll("\\.0$","");
		
		return ("" + minDepth).replaceAll("\\.0$","") + ("-" + maxDepth).replaceAll("\\.0$","");
	}
	
	public void setDepthVal(String val) {
		
	}
	
	@XmlElement(name="AdvertisedCost")
	public String getApartmentCostVal() {
		if (minAdvCost > maxAdvCost || minAdvCost == -1) minAdvCost = maxAdvCost;
		if (minAdvCost == maxAdvCost) return String.valueOf(maxAdvCost).replaceAll("\\.0$","").replaceAll("\\.00$","");
		
		return nf.format(minAdvCost) + "-" + nf.format(maxAdvCost);
	}
	
	@XmlElement(name="TotalCost")
	public String getTotalCostVal() {
		if (minTotalCost > maxTotalCost || minTotalCost == -1) minTotalCost = maxTotalCost;
		if (minTotalCost == maxTotalCost) return String.valueOf(maxTotalCost).replaceAll("\\.0$","").replaceAll("\\.00$","");
		
		return nf.format(minTotalCost) + "-" + nf.format(maxTotalCost);
	}
	
	public void setLevelsVal(String val) {
		
	}
	
	@XmlElement(name="Count")
	public int getCount() {
		if (list == null) return 0;
		
		return list.size();
	}
	
	public void setCount(int val) {
		
	}
}
