package com.sok.runway.externalInterface.exceptions;

import javax.ws.rs.core.Response;

import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.exc.UnrecognizedPropertyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.DataApplication;

@Provider
public class UnrecognizedPropertyExceptionHandler implements ExceptionMapper<UnrecognizedPropertyException> {
	
	private static final Logger logger = LoggerFactory.getLogger(UnrecognizedPropertyExceptionHandler.class);
	public Response toResponse(UnrecognizedPropertyException exception){
		logger.error("Field could not be found {}", exception.getUnrecognizedPropertyName()); 
	    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
	    .entity(new DataApplication.DataApplicationException("Field could not be found :" + exception.getUnrecognizedPropertyName(), Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()))
	    .build();
	}
}


