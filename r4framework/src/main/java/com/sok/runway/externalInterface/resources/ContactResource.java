package com.sok.runway.externalInterface.resources;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;


import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.externalInterface.beans.Contact;
import com.sok.runway.externalInterface.beans.shared.ContactProfile;
import com.sok.runway.externalInterface.beans.shared.OrderProfile;
import com.sok.runway.externalInterface.beans.shared.Profile;
import com.sok.runway.externalInterface.resources.UtilityResource.ContactLocation;
import com.sok.service.crm.ContactService;
import com.sok.service.crm.ProfileService;

@Path("contacts")
public class ContactResource {

	private static final ContactResource resource = new ContactResource();
	private static final Logger logger = LoggerFactory.getLogger(ContactResource.class);
	private final ContactService cs = ContactService.getInstance();
	private final ProfileService ps = ProfileService.getInstance();
	public static ContactResource getInstance() {
		return resource;
	}
	
	final String[] quickNoteFields = { "-FirstName", "-LastName", "-Company", "-Email","-quickSearch"};
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Contact> getContacts(@Context HttpServletRequest request) {
		logger.trace("getContacts(Request)");
		for(String s: quickNoteFields) {
			if(StringUtils.isNotBlank(request.getParameter(s))) {
				return cs.getQuicknoteContactMatches(request);
			}
		}
		if("duplicate-check".equals(request.getParameter("-action"))) {
			return cs.getDuplicateContactMatches(request);
		}
		throw new UnsupportedOperationException("Not yet implemented for general search");
	}
	
	@GET
	@Path("{contactID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Contact getContact(@Context HttpServletRequest request, @PathParam("contactID") String contactID) {
		return cs.getContact(request, contactID);
	}
	
	@GET
	@Path("{contactID}/locations")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ContactLocation> getContactLocations(@Context final HttpServletRequest request, @PathParam("contactID") String contactID) {
		return cs.getContactLocations(request, contactID);
	}
		
	
	@GET
	@Path("{contactID}/linkedcontacts")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Contact> getLinkedContacts(@Context final HttpServletRequest request, @PathParam("contactID") String contactID) {
		return cs.getLinkedContacts(request, contactID);
	}
	
	@GET
	@Path("{contactID}/profiles")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ContactProfile> getContactProfile(@Context final HttpServletRequest request, @PathParam("contactID") String contactID) {
		logger.trace("getContactProfile(Request,{})", contactID);
		// TODO - fix the types here to use generics etc. 
		final List<Profile> pl = ps.getProfiles(request, contactID);
		final List<ContactProfile> cpl = new ArrayList<ContactProfile>(pl.size());
		for(Profile p: pl) {
			cpl.add((ContactProfile)p);
		}
		return cpl;
	}
	
	@PUT
	@Path("{contactID}/profiles/")
	public List<ContactProfile> setContactProfiles(@Context HttpServletRequest request, @PathParam("contactID") String contactID, List<ContactProfile> profiles) {
		return cs.updateProfiles(request, contactID, profiles);
	}
	
	@PUT
	@Path("{contactID}/profiles/{surveyID}")
	@Produces(MediaType.APPLICATION_JSON)
	public ContactProfile updateContactProfile(@Context HttpServletRequest request, @PathParam("contactID") String contactID, @PathParam("surveyID") String surveyID, ContactProfile contactProfile) {
		logger.trace("ContactProfile(Request,{},{})", contactProfile, surveyID);
		if(StringUtils.isBlank(contactID)) {
			throw new IllegalArgumentException("ContactID blank");
		}
		if(StringUtils.isBlank(surveyID)) {
			throw new IllegalArgumentException("SurveyID blank");
		}		
		if(!surveyID.equals(contactProfile.getSurveyID())) {
			throw new IllegalArgumentException("SurveyID did not match survey id in object");
		}
		return ps.saveContactProfile(request, contactID, contactProfile);
	}
	
}
