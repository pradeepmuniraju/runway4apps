package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Estate")
@XmlSeeAlso(Estate.class)
@XmlType(propOrder={"estateID","name" })		//MarketingStatus AvailableDate
public abstract class SimpleEstate {

	protected String estateID, name;
	
	//@JsonProperty("EstateID")
	@XmlElement(name="EstateID")
	public String getEstateID() {
		return estateID;
	}
	public void setEstateID(String estateID) {
		this.estateID = estateID;
	}
	//@JsonProperty("Name")
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
