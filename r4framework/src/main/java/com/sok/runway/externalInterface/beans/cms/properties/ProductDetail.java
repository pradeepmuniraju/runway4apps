package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.GenRow;


@XmlAccessorType(XmlAccessType.FIELD)
public class ProductDetail {

	public String ProductDetailsID, ProductID, CopiedFromProductID, ProductType, DetailsType, YesNo, Alphavalue;
	public Integer SortOrder;
	public Double SQmeters, Squares, Linearmeters, Cubicmeters, Percentage, 
			Width, Depth, Height, DimLeft, DimRight, DimFront, DimRear, Perimeter, NumberOf;
	
	public ProductDetail() { }
	
	public ProductDetail(GenRow g) {
		ProductDetailsID = StringUtils.trimToNull(g.getString("ProductDetailsID"));
		ProductID = StringUtils.trimToNull(g.getString("ProductID"));
		CopiedFromProductID = StringUtils.trimToNull(g.getString("CopiedFromProductID"));
		ProductType = g.getData("ProductType");
		DetailsType = g.getString("DetailsType");
		
		SortOrder = g.getInt("SortOrder");
		
		SQmeters = g.getDoubleNull("SQmeters");
		Squares = g.getDoubleNull("Squares");
		Linearmeters = g.getDoubleNull("Linearmeters");
		Cubicmeters = g.getDoubleNull("Cubicmeters");
		Percentage = g.getDoubleNull("Percentage");
		Width = g.getDoubleNull("Width");
		Depth = g.getDoubleNull("Depth");
		Height = g.getDoubleNull("Height");
		DimLeft = g.getDoubleNull("DimLeft");
		DimRight = g.getDoubleNull("DimRight");
		
		DimFront = g.getDoubleNull("DimFront");
		DimRear = g.getDoubleNull("DimRear");
		Perimeter = g.getDoubleNull("Perimeter");
		NumberOf = g.getDoubleNull("NumberOf");
		
		YesNo = StringUtils.trimToNull(g.getString("YesNo"));
		Alphavalue = StringUtils.trimToNull(g.getString("Alphavalue"));
	}
	
	public void populateGenRow(GenRow g) {
		
		g.setParameter("ProductType", ProductType);
		g.setParameter("DetailsType", DetailsType);
		g.setParameter("SortOrder", SortOrder);
		g.setParameter("SQmeters", SQmeters);
		g.setParameter("Squares", Squares);
		g.setParameter("Linearmeters", Linearmeters);
		g.setParameter("Cubicmeters", Cubicmeters);
		g.setParameter("Percentage", Percentage);
		g.setParameter("Width", Width);
		g.setParameter("Depth",Depth );
		g.setParameter("Height", Height);
		g.setParameter("DimLeft", DimLeft);
		g.setParameter("DimRight", DimRight);
		g.setParameter("DimFront", DimFront);
		g.setParameter("DimRear", DimRear);
		g.setParameter("Perimeter", Perimeter);
		g.setParameter("NumberOf", NumberOf);
		
		g.setParameter("YesNo", YesNo);
		g.setParameter("Alphavalue", Alphavalue);
	}
}
