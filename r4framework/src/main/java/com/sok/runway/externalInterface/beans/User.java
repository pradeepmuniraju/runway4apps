package com.sok.runway.externalInterface.beans;

import com.sok.runway.externalInterface.beans.shared.*;

import java.util.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonProperty;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
public class User
{
	private String userid = null;
	private String username = null;
//	private String password = null;
	private String title = null;
	private String firstname = null;
	private String lastname = null;
	private String position = null;
	private String company = null;

	private String phone = null;
	private String fax = null;
	private String mobile = null;
	private String web = null;
	private String email = null;
	private String emailsig = null;
	private Date lastlogin = null;
	private Date createddate = null;
	private Date modifieddate = null;
	private String modifiedby = null;
	private String createdby = null;
	private String emailhost = null;
	private String emailusername = null;
	private String emailpassword = null;
	private String emailprotocol = null;
	private String timezone = null;
	private String status = null;
	private String saveemails = null;
	private String supportid = null;
	private String mgruserid = null;
	private String ipaddress = null;
	private String homepage = null;
	private String listsize = null;
	private String defaultsearchgroup = null;
	private String defaultlist = null;
	private String defaultgroupid = null;
	private String defaultroleid = null;
	private String sevendaycalendar = null;
//	private String passwordchanged = null;
	private String signatureimage = null;
	private String photoimage = null;
	private String defaultcampaignid = null;
	private String defaultdashboardid = null;
	
	private Address address = null;
	
	private String error = null;
	
	public String getError()
	{
		return(error);
	}	
	
	public void setError(String error)
	{
		this.error = error;
	}	
	
	@JsonProperty("UserID")
	public String getUserID()
	{
		return(userid);
	}	
	
	@JsonProperty("UserID")
	public void setUserID(String userid)
	{
		this.userid = userid;
	}	
	
	@JsonProperty("Username")
	public String getUsername()
	{
		return(username);
	}	
	
	@JsonProperty("Username")
	public void setUsername(String username)
	{
		this.username = username;
	}	
/*
	public String getPassword()
	{
		return(password);
	}	
	
	public void setPassword(String password)
	{
		this.password = password;
	}	
*/	
	@JsonProperty("Title")
	public String getTitle()
	{
		return(title);
	}	
	
	@JsonProperty("Title")
	public void setTitle(String title)
	{
		this.title = title;
	}	
	
	@JsonProperty("FirstName")
	public String getFirstName()
	{
		return(firstname);
	}	
	
	@JsonProperty("FirstName")
	public void setFirstName(String firstname)
	{
		this.firstname = firstname;
	}	
	
	@JsonProperty("LastName")
	public String getLastName()
	{
		return(lastname);
	}	
	
	@XmlTransient 
	public String getName() {
		StringBuilder name = new StringBuilder();
		if(firstname != null)
		{
			name.append(firstname).append(" ");
		}
		if(lastname != null) {
			name.append(lastname);
		}
		return name.toString();
	}
	
	@JsonProperty("LastName")
	public void setLastName(String lastname)
	{
		this.lastname = lastname;
	}	
	
	public String getPosition()
	{
		return(position);
	}	
	
	public void setPosition(String position)
	{
		this.position = position;
	}	
	
	public String getCompany()
	{
		return(company);
	}	
	
	public void setCompany(String company)
	{
		this.company = company;
	}	
	
	@JsonProperty("Phone")
	public String getPhone()
	{
		return(phone);
	}	
	
	@JsonProperty("Phone")
	public void setPhone(String phone)
	{
		this.phone = phone;
	}	
	
	public String getFax()
	{
		return(fax);
	}	
	
	public void setFax(String fax)
	{
		this.fax = fax;
	}	
	
	@JsonProperty("Mobile")
	public String getMobile()
	{
		return(mobile);
	}	
	
	@JsonProperty("Mobile")
	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}	
	
	public String getWeb()
	{
		return(web);
	}	
	
	public void setWeb(String web)
	{
		this.web = web;
	}	
	
	@JsonProperty("Email")
	public String getEmail()
	{
		return(email);
	}	
	
	@JsonProperty("Email")
	public void setEmail(String email)
	{
		this.email = email;
	}	
	
	public String getEmailSig()
	{
		return(emailsig);
	}	
	
	public void setEmailSig(String emailsig)
	{
		this.emailsig = emailsig;
	}	
	
	public Date getLastLogin()
	{
		return(lastlogin);
	}	
	
	public void setLastLogin(Date lastlogin)
	{
		this.lastlogin = lastlogin;
	}	
	
	public Date getCreatedDate()
	{
		return(createddate);
	}	
	
	public void setCreatedDate(Date createddate)
	{
		this.createddate = createddate;
	}	
	
	public Date getModifiedDate()
	{
		return(modifieddate);
	}	
	
	public void setModifiedDate(Date modifieddate)
	{
		this.modifieddate = modifieddate;
	}	
	
	public String getModifiedBy()
	{
		return(modifiedby);
	}	
	
	public void setModifiedBy(String modifiedby)
	{
		this.modifiedby = modifiedby;
	}	
	
	public String getCreatedBy()
	{
		return(createdby);
	}	
	
	public void setCreatedBy(String createdby)
	{
		this.createdby = createdby;
	}	
	
	public String getEmailHost()
	{
		return(emailhost);
	}	
	
	public void setEmailHost(String emailhost)
	{
		this.emailhost = emailhost;
	}	
	
	@XmlTransient public String getEmailUsername()
	{
		return(emailusername);
	}	
	
	public void setEmailUsername(String emailusername)
	{
		this.emailusername = emailusername;
	}	
	
	@XmlTransient public String getEmailPassword()
	{
		return(emailpassword);
	}	
	
	public void setEmailPassword(String emailpassword)
	{
		this.emailpassword = emailpassword;
	}	
	
	@XmlTransient public String getEmailProtocol()
	{
		return(emailprotocol);
	}	
	
	public void setEmailProtocol(String emailprotocol)
	{
		this.emailprotocol = emailprotocol;
	}	
	
	public String getTimeZone()
	{
		return(timezone);
	}	
	
	public void setTimeZone(String timezone)
	{
		this.timezone = timezone;
	}	
	
	public String getStatus()
	{
		return(status);
	}	
	
	public void setStatus(String status)
	{
		this.status = status;
	}	
	
	public String getSaveEmails()
	{
		return(saveemails);
	}	
	
	public void setSaveEmails(String saveemails)
	{
		this.saveemails = saveemails;
	}	
	
	public String getSupportID()
	{
		return(supportid);
	}	
	
	public void setSupportID(String supportid)
	{
		this.supportid = supportid;
	}	
	
	public String getMgrUserID()
	{
		return(mgruserid);
	}	
	
	public void setMgrUserID(String mgruserid)
	{
		this.mgruserid = mgruserid;
	}	
	
	public String getIPAddress()
	{
		return(ipaddress);
	}	
	
	public void setIPAddress(String ipaddress)
	{
		this.ipaddress = ipaddress;
	}	
	
	public String getHomePage()
	{
		return(homepage);
	}	
	
	public void setHomePage(String homepage)
	{
		this.homepage = homepage;
	}	
	
	public String getListSize()
	{
		return(listsize);
	}	
	
	public void setListSize(String listsize)
	{
		this.listsize = listsize;
	}	
	
	public String getDefaultSearchGroup()
	{
		return(defaultsearchgroup);
	}	
	
	public void setDefaultSearchGroup(String defaultsearchgroup)
	{
		this.defaultsearchgroup = defaultsearchgroup;
	}	
	
	public String getDefaultList()
	{
		return(defaultlist);
	}	
	
	public void setDefaultList(String defaultlist)
	{
		this.defaultlist = defaultlist;
	}	
	
	public String getDefaultGroupID()
	{
		return(defaultgroupid);
	}	
	
	public void setDefaultGroupID(String defaultgroupid)
	{
		this.defaultgroupid = defaultgroupid;
	}	
	
	public String getDefaultRoleID()
	{
		return(defaultroleid);
	}	
	
	public void setDefaultRoleID(String defaultroleid)
	{
		this.defaultroleid = defaultroleid;
	}	
	
	public String getSevenDayCalendar()
	{
		return(sevendaycalendar);
	}	
	
	public void setSevenDayCalendar(String sevendaycalendar)
	{
		this.sevendaycalendar = sevendaycalendar;
	}	
	
/*	
	public String getPasswordChanged()
	{
		return(passwordchanged);
	}	
	
	public void setPasswordChanged(String passwordchanged)
	{
		this.passwordchanged = passwordchanged;
	}	
*/	
	public String getSignatureImage()
	{
		return(signatureimage);
	}	
	
	public void setSignatureImage(String signatureimage)
	{
		this.signatureimage = signatureimage;
	}	
	
	public String getPhotoImage()
	{
		return(photoimage);
	}	
	
	public void setPhotoImage(String photoimage)
	{
		this.photoimage = photoimage;
	}	
	
	public String getDefaultCampaignID()
	{
		return(defaultcampaignid);
	}	
	
	public void setDefaultCampaignID(String defaultcampaignid)
	{
		this.defaultcampaignid = defaultcampaignid;
	}	
	
	public String getDefaultDashBoardID()
	{
		return(defaultdashboardid);
	}	
	
	public void setDefaultDashBoardID(String defaultdashboardid)
	{
		this.defaultdashboardid = defaultdashboardid;
	}	

	@JsonProperty("Address")
	public Address getAddress()
	{
		return(address);
	}
	
	@JsonProperty("Address")
	public void setAddress(Address address)
	{
		this.address = address;
	}		
	
	private PersonName modifiedbyname = null;
	private PersonName createdbyname = null;
	
	public PersonName getModifiedByName()
	{
		return(modifiedbyname);
	}
	
	public void setModifiedByName(PersonName modifiedbyname)
	{
		this.modifiedbyname = modifiedbyname;
	}
	
	public PersonName getCreatedByName()
	{
		return(createdbyname);
	}
	
	public void setCreatedByName(PersonName createdbyname)
	{
		this.createdbyname = createdbyname;
	}	
}
