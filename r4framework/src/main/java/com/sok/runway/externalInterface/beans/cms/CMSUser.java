package com.sok.runway.externalInterface.beans.cms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.GenRow;
import com.sok.runway.JSPLogger;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="CMSUser")
public class CMSUser {
	public static enum CMSUserRole { None, Editor, Publisher };
	private String CMSUserID, CMSID, UserID, FirstName, LastName, Company, Position;
	private CMSUserRole Role = CMSUserRole.None;
	public CMSUser(GenRow r) {
		CMSUserID = r.getString("CMSUserID");
		CMSID = r.getString("CMSID");
		UserID = r.getString("UserID");
		FirstName = r.getString("FirstName");
		LastName = r.getString("LastName");
		Company = r.getString("Company");
		Position = r.getString("Position");
		setRole(r.getString("Role"));
	}
	public CMSUser() { } 
	
	@XmlElement(name="CMSUserID")
	public String getCMSUserID() {
		return CMSUserID;
	}
	public void setCMSUserID(String cMSUserID) {
		CMSUserID = cMSUserID;
	}
	@XmlElement(name="CMSID")
	public String getCMSID() {
		return CMSID;
	}

	public void setCMSID(String cMSID) {
		CMSID = cMSID;
	}
	@XmlElement(name="UserID")
	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}
	@XmlElement(name="Role")
	public String getRole() {
		return Role.name();
	}
	@XmlElement(name="Role")
	public void setRole(String role) {
		if(StringUtils.isBlank(role)) {
			Role = CMSUserRole.None;
			return;
		}
		try { 
			Role = CMSUserRole.valueOf(role);
		} catch (IllegalArgumentException iae) {
			JSPLogger.error(this, "Unknown CMSUserRole found: CMSUserID=[{}] Role=[{}]", role, CMSUserID);
			Role = CMSUserRole.None;
		}
	}
	public void setRole(CMSUserRole role) {
		Role = role;
	}
	@XmlElement(name="FirstName")
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	@XmlElement(name="LastName")
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	@XmlElement(name="Company")
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	@XmlElement(name="Position")
	public String getPosition() {
		return Position;
	}
	public void setPosition(String position) {
		Position = position;
	}
	
	
}
