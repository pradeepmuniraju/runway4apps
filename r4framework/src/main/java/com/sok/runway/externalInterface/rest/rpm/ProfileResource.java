package com.sok.runway.externalInterface.rest.rpm;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.externalInterface.beans.cms.properties.Answer;
import com.sok.runway.externalInterface.beans.cms.properties.Profile;
import com.sok.runway.externalInterface.beans.cms.properties.Questions;
import com.sok.runway.externalInterface.beans.cms.properties.SimpleProfile;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.servlet.http.HttpServletRequest;

@Path("/profiles")
@Api(value = "/profiles", description = "Returns a Profile for rendering to a website")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class ProfileResource {

	private static final Logger logger = LoggerFactory.getLogger(ProfileResource.class);
	
	
	@Context HttpServletRequest request;
	
	public ProfileResource() {
	}
	
	public ProfileResource(HttpServletRequest request) {
		this.request = request;
	}
	
	@GET
	@ApiOperation(value = "Profiles Lists", notes="<p>Returns a list of all active Profiles in Runway.</p>")
	public List<SimpleProfile> getProfiles() {
        GenRow profiles = new GenRow();
        profiles.setRequest(request);
        profiles.setTableSpec("Surveys");
        profiles.setParameter("-select0", "SurveyID");
        profiles.setParameter("-select1", "Name");
        profiles.setParameter("-select2", "ProfileType");
        profiles.setParameter("Disabled", "!Y");
        profiles.setParameter("Name", "!PROFILE_GENERATOR_NO_DASHBOARD");
        profiles.sortBy("Name", 0);
        profiles.doAction(GenerationKeys.SEARCH);
        
        profiles.getResults();
        
        ArrayList<SimpleProfile> list = new ArrayList<SimpleProfile>();
        
        while (profiles.getNext()) {
        	list.add(new SimpleProfile(profiles));
        }
        
        profiles.close();
		
		return list;
	}
	
	@GET
	@Path("/{profile}")
	@ApiOperation(value = "return a specified profile", notes="<p>A profile, All it's Questions, Answers and related profiles</p>", responseClass="import com.sok.runway.externalInterface.beans.cms.properties.Profile", multiValueResponse=false)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public Profile getProfile(@ApiParam(value = "The ID or Name of a profile", required = true) @PathParam("profile") String profile) {
		if(StringUtils.isNotBlank(profile)) {
			logger.debug("getListItems({} {})", profile, true);
		} else {
			return null;
		}
		
        GenRow profiles = new GenRow();
        profiles.setRequest(request);
        profiles.setTableSpec("Surveys");
        profiles.setParameter("-select0", "SurveyID");
        profiles.setParameter("-select1", "Name");
        profiles.setParameter("-select2", "ProfileType");
        profiles.setParameter("-select3", "Description");
        profiles.setParameter("-select4", "Preamble");
        profiles.setParameter("-select5", "Disclaimer");
        profiles.setParameter("-select6", "RelatedSurveyID");
        profiles.setParameter("Disabled", "!Y");
        profiles.setParameter("Name|1", profile);
        profiles.setParameter("SurveyID|1", profile);
        profiles.doAction(GenerationKeys.SELECTFIRST);
        
        if (StringUtils.isBlank(profiles.getData("SurveyID"))) {
        	return null;
        }
        
        Profile p = new Profile(profiles);
        
        GenRow quest = new GenRow();
        quest.setViewSpec("SurveyQuestionView");
        quest.setConnection(profiles.getConnection());
        quest.setParameter("SurveyID", p.getProfileID());
        quest.sortBy("SortOrder", 0);
        
        quest.doAction("search");
        quest.getResults();
        
        if (quest.getNext()) {
        	ArrayList<Questions> questions = new ArrayList<Questions>();
        	
        	do {
        		Questions q = new Questions(quest);
        		if ("YesNo".equalsIgnoreCase(q.getInputType())) {
        			q.addValues(new String("Yes"));
        			q.addValues(new String("No"));
        		} else if ("Drop Down".equalsIgnoreCase(q.getInputType()) || "Checkbox".equalsIgnoreCase(q.getInputType()) || "Radio Button".equalsIgnoreCase(q.getInputType())) {
        			if (quest.isSet("ValueList")) {
        				GenRow list = new GenRow();
        				list.setViewSpec("ListItemView");
        				list.setConnection(quest.getConnection());
        				list.setParameter("ListID", quest.getString("ValueList"));
        				//list.setParameter("ListItemItemList.ListName", quest.getString("ValueList"));
        				list.sortBy("SortNumber", 0);
        				list.sortBy("ItemValue", 1);
        				list.doAction("search");
        				
        				list.getResults();
        				
        				while (list.getNext()) {
        					q.addValues(list.getString("ItemValue"));
        				}
        				
        				list.close();
        			}
        		}
        		
        		questions.add(q);
        	} while (quest.getNext());
        	
        	p.setQuestions(questions);
        }
        
        quest.close();
        
        if (profiles.isSet("RelatedSurveyID")) p.setRelatedProfile(getProfile(profiles.getString("RelatedSurveyID")));
        
		return p;
	}
}

