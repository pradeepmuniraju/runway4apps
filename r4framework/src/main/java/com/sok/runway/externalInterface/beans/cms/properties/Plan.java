package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.sok.framework.InitServlet;
import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.SimplePublishing;
import com.sok.runway.externalInterface.beans.cms.SimpleCampaign;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.service.crm.cms.properties.PlanService.HLPackageCost;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Plan")
public class Plan extends SimplePlan {

	private String productNum, description, createdBy, modifiedBy, summary, active, subType, marketingStatus;
	private Date createdDate, modifiedDate;
	private Date availableDate, expiryDate;
	private PersonName modifiedByName = null, createdByName = null;
	private Double GST = null, totalCost = null, dripPrice = null, price = null;

	private String displayPrice = ""; 
	
	private List<ProductDetail> details = null;
	private SimpleDocument image = null;
	private PlanStatusHistory currentStatus = null;
	private List<Facade> facades = null;
	private SimplePublishing publishing = null;
	private String publishHeadline;
	private String publishDescription;
	
	@XmlElement(name="ProductNum")
	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}
	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@XmlElement(name="PublishHeadline")
	public String getPublishHeadline() {
		return publishHeadline;
	}
	public void setPublishHeadline(String publishHeadline) {
		this.publishHeadline = publishHeadline;
	}
	
	@XmlElement(name="PublishDescription")
	public String getPublishDescription() {
		return publishDescription;
	}
	public void setPublishDescription(String publishDescription) {
		this.publishDescription = publishDescription;
	}
	
	@XmlElement(name="Summary")
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName(){
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedByName = modifiedbyname;
	}
	
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	@XmlElement(name="Image")
	public SimpleDocument getImage() {
		return image;
	}
	public void setImage(SimpleDocument image) {
		this.image = image;
	}
	
	
	@XmlElement(name="GST")
	public Double getGST() {
		return GST;
	}
	public void setGST(Double gST) {
		this.GST = gST;
	}
	@XmlElement(name="TotalCost")
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	
	@XmlElement(name="DripPrice")
	public Double getDripPrice() {
		return dripPrice;
	}
	public void setDripPrice(Double dripPrice) {
		this.dripPrice = dripPrice;
	}
	
	@XmlElement(name="Price")
	public Double getPrice() {
		return price; 
	}
	public void setPrice(Double price) {
		this.price = price; 
	}
	
	@XmlElementWrapper(name = "Details")
	@XmlElement(name="Detail")
	public List<ProductDetail> getDetails() {
		return details;
	}
	public void setDetails(List<ProductDetail> details) {
		this.details = details;
	}
	
	@XmlElement(name="CurrentStatus")
	public PlanStatusHistory getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(PlanStatusHistory currentStatus) {
		this.currentStatus = currentStatus; 
	}
	@XmlElement(name="Active")
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	@XmlElement(name="Facades")
	public List<Facade> getFacades() {
		return facades;
	}
	public void setFacades(List<Facade> facades) {
		this.facades = facades;
	}
	@XmlElement(name="Publishing")
	public SimplePublishing getPublishing() {
		return publishing;
	}
	public void setPublishing(SimplePublishing publishing) {
		this.publishing = publishing;
	}
	@XmlElement(name="VisibilityStatus")
	public String getVisibilityStatus() {
		if (currentStatus == null || currentStatus.getStatusID() == null) return "Offline";
		
		if (!VisibilityStatus.checkVisibilityStatus(active, currentStatus.getStatusID(), availableDate, expiryDate)) return "Offline";
		
		if ("true".equals(InitServlet.getSystemParam("RPM-Publishing-Plans"))) {
			if (publishing == null) return "Offline";
			if (!VisibilityStatus.checkPublishedStatus(publishing.getPublishedFlag(), publishing.getStatus(), publishing.getStartDate(), publishing.getEndDate())) return "Offline";
		}
		
		return "Online";
	}
	
	public void setVisibilityStatus(String status) {
		
	}
	@XmlElement(name="AvailableDate")
	public Date getAavalableDate() {
		return availableDate;
	}
	public void setAavalableDate(Date avalableDate) {
		this.availableDate = avalableDate;
	}
	@XmlElement(name="ExpiryDate")
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	@XmlElement(name="DisplayPrice")
	public String getDisplayPrice() {
		return displayPrice;
	}
	public void setDisplayPrice(String displayPrice) {
		this.displayPrice = displayPrice;
	}
	@XmlElement(name="ProductSubType")
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	@XmlElement(name="MarketingStatus")
	public String getMarketingStatus() {
		return marketingStatus;
	}
	public void setMarketingStatus(String marketingStatus) {
		this.marketingStatus = marketingStatus;
	}
}
