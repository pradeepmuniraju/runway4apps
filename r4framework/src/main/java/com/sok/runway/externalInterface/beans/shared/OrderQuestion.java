package com.sok.runway.externalInterface.beans.shared;

import org.codehaus.jackson.annotate.JsonProperty;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class OrderQuestion extends Question
{
	private OrderAnswer answer = null;
	
	@JsonProperty("Answer")
	public OrderAnswer getAnswer()
	{
		return(answer);
	}	
	
	@JsonProperty("Answer")
	public void setAnswer(OrderAnswer answer)
	{
		this.answer = answer;
	}		
}