package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.runway.externalInterface.beans.shared.AbstractStatusHistory;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="PlanStatus")
@XmlType(propOrder={"planStatusID","planID"})
public class PlanStatusHistory extends AbstractStatusHistory {
	private String planStatusID, planID;

	@XmlElement(name="PlanStatusID")
	public String getPlanStatusID() {
		return planStatusID;
	}
	public void setPlanStatusID(String planStatusID) {
		this.planStatusID = planStatusID;
	}
	
	@XmlElement(name="PlanID")
	public String getPlanID() {
		return planID;
	}
	public void setPlanID(String planID) {
		this.planID = planID;
	}
}
