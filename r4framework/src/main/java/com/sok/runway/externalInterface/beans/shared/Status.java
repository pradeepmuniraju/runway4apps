package com.sok.runway.externalInterface.beans.shared;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class Status
{
	private String statusid = null;
	private String status = null;
	private String statusvalue = null;
	private String sortnumber = null;
	private String listname = null;
	private String inactive = null;
	
	public String getStatusID()
	{
		return(statusid);
	}	
	
	public void setStatusID(String statusid)
	{
		this.statusid = statusid;
	}	
	
	public String getStatus()
	{
		return(status);
	}	
	
	public void setStatus(String status)
	{
		this.status = status;
	}	
	
	public String getValue()
	{
		return(statusvalue);
	}	
	
	public void setValue(String statusvalue)
	{
		this.statusvalue = statusvalue;
	}	
	
	public String getSortNumber()
	{
		return(sortnumber);
	}	
	
	public void setSortNumber(String sortnumber)
	{
		this.sortnumber = sortnumber;
	}		
	
	public String getListName()
	{
		return(listname);
	}	
	
	public void setListName(String listname)
	{
		this.listname = listname;
	}	
	
	public String getInactive()
	{
		return(inactive);
	}	
	
	public void setInactive(String inactive)
	{
		this.inactive = inactive;
	}	

}
