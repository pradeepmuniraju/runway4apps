package com.sok.runway.externalInterface.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

//used to designate that anything else can be ignored.
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteView {
	
	public QuoteView() {}
	public QuoteView(String QuoteViewID, String ViewName, String Description, String Active, String Expiry) {
		this.QuoteViewID = QuoteViewID; 
		this.ViewName = ViewName; 
		this.Description = Description; 
		this.Active = "Y".equals(Active) ? true : false; 
		this.Expiry = Expiry;
		
	}
	public QuoteView(GenRow data) {
		this.QuoteViewID = data.getString("QuoteViewID");
		this.ViewName = data.getString("ViewName");
		this.Description = data.getString("Description");
		this.Active = "Y".equals(data.getString("Active"));
		this.Expiry = data.getString("Expiry");
		this.headerImage = data.getString("HeaderImage"); 
		this.headerImagePath = data.getString("HeaderImagePath");
		this.footerText = data.getString("FooterText");
	}
	
	public String QuoteViewID;
	public boolean Active;
	public String ViewName; 
	public String Description;
	public String Expiry;
	public Order OrderTemplate;
	public String headerImage; 
	public String headerImagePath;
	public String footerText;
	
	@Override
	public String toString() {
		return String.format("QuoteView(%1$s, %2$s, %3$s, %4$b, %5$s)", QuoteViewID, ViewName, Description, Active, Expiry);
	}
	
}

