package com.sok.runway.externalInterface.rest.rpm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.Home;
import com.sok.runway.externalInterface.beans.cms.properties.Lot;
import com.sok.runway.externalInterface.beans.cms.properties.LotCheck;
import com.sok.service.crm.cms.properties.EstateService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.servlet.http.HttpServletRequest;

@Path("/lots")
@Api(value = "/lots", description = "Retrieve Lot Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class LotResource {

	private static final Logger logger = LoggerFactory.getLogger(LotResource.class);
	private static final EstateService estateService = EstateService.getInstance();
	
	protected final String estateID, stageID;
	@Context HttpServletRequest request;
	
	public LotResource() {
		this.estateID = null;
		this.stageID = null;
	}
	
	public LotResource(HttpServletRequest request) {
		this.request = request;
		this.estateID = null;
		this.stageID = null;
	}
	public LotResource(HttpServletRequest request, String estateID) {
		this.request = request;
		this.estateID = estateID;
		this.stageID = null;
	}
	public LotResource(HttpServletRequest request, String estateID, String stageID) {
		this.request = request;
		this.estateID = estateID;
		this.stageID = stageID;
	}
	
	@GET
	@ApiOperation(value = "List all lots", notes="Optionally filtering by modified date", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Lot", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<Lot> getEstateLots(@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = true) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active lots", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			logger.debug("getStages({})", modifiedSince, modifiedSince);
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		return estateService.getLotsExternal(request, estateID, stageID, mod, active);
	}
	
	@GET
	@Path("/check")
	@ApiOperation(value = "List all lots", notes="Optionally filtering by modified date", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Lot", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<LotCheck> getEstateLotsCheck(@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = false) @QueryParam("ModifiedSince") String modifiedSince) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			logger.debug("getStages({})", modifiedSince, modifiedSince);
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		return estateService.getLotsExternalCheck(request, estateID, stageID, mod);
	}
	
	@GET
	@Path("/{lotID}")
	@ApiOperation(value = "Return a particular lot", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Lot")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public Lot getLot(@Context HttpServletRequest request, @PathParam("lotID") final String lotID, 
			@ApiParam(value = "The returned lot representation", allowableValues="Simple,Extended", required=false, defaultValue="Simple") @DefaultValue("Simple") @QueryParam("View") BeanRepresentation extended) {
		return BeanFactory.external(estateService.getLot(request, lotID), extended);
	}
	
	@GET
	@ApiResource(value = "/{lotID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Lot", notes="<p>This call will return a list of Albums and their Images for a given Lot. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/{lotID}/albums")
	public AlbumResource getAlbumResource(@PathParam("lotID") final String lotID, 
			@ApiParam(value = "Document Sub Type", required = false) @QueryParam("DocumentSubType") String subType, 
			@ApiParam(value = "Album Type", required = false) @QueryParam("AlbumType") String albumType, 
			@ApiParam(value = "Album Publsihed for", allowableValues="Website", required=false, defaultValue="") @DefaultValue("") @QueryParam("Publish") String publish) {
		return new AlbumResource(request,  lotID, albumType, publish, subType);
	}

	@GET
	@ApiResource(value = "/{lotID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Lot", notes="<p>This call will return a list of Albums and their Images for a given Lot. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/{lotID}/findplans")
	public List<Home> getHomesResource(@PathParam("lotID") final String lotID) {
		return estateService.findLand(request, lotID);
	}
}

