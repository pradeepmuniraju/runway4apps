package com.sok.runway.externalInterface.resources;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.generation.nodes.LeafNode;
import com.sok.framework.generation.nodes.Node;
import com.sok.runway.UserBean;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.externalInterface.DataApplication;
import com.sok.runway.externalInterface.beans.Order;
import com.sok.runway.externalInterface.beans.QuoteView;
import com.sok.runway.externalInterface.beans.QuoteViewPage;
import com.sok.runway.externalInterface.beans.shared.OrderItem;
import com.sok.service.crm.OrderService;
import com.sok.service.crm.QuoteViewService;
import com.sok.service.crm.UserService;
import com.sok.service.exception.NotAuthenticatedException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * This resource shows the methods which are available from the REST API
 * Currently this includes business logic also - this is to be moved to an appropriate controller
 * which would include all user-based security settings. 
 * 
 * @author Michael Dekmetzian 
 */
/* base path for the resource */
@Path("/admin/quoteview/")
public class QuoteViewResource {
	private static final QuoteViewResource resource = new QuoteViewResource();
	private static final Logger logger = LoggerFactory.getLogger(QuoteViewResource.class);
	private static final OrderService os = OrderService.getInstance();
	private static final QuoteViewService qvs = QuoteViewService.getInstance();
	private static final UserService us = UserService.getInstance();
	private QuoteViewResource() {}
	public static QuoteViewResource getInstance() {
		return resource;
	}	
	
	@GET
	@Path("pagetypes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPageTypes(@Context HttpServletRequest request) {
		final UserBean currentuser = us.getCurrentUser(request);
		if(currentuser == null) {
			return jsonResponse(Status.FORBIDDEN, getErrorJson(new RuntimeException("Not logged in")));
		}
		try {
			GenRow templates = new GenRow();
			templates.setRequest(request);
			templates.setViewSpec("quoteviews/QuoteViewPageTypeView");
			templates.getResults();
			final JSONArray data = new JSONArray();
			if(templates.getNext()) {
				do {
					data.add(serializeData(templates));
				} while(templates.getNext());
			} 
			templates.close();
			return jsonResponse(Status.OK,data);
		} catch (Exception e) {
			logger.error("Error producing page types", e);
			return jsonResponse(Status.INTERNAL_SERVER_ERROR,getErrorJson(e));
		}
	}
	
	@GET
	@Path("views")
	@Produces(MediaType.APPLICATION_JSON)
	public List<QuoteView> getQuoteViewTemplates(@Context HttpServletRequest request) {
		logger.trace("getQuoteViewTemplates");
		return qvs.getQuoteViews(request);
	}
	
	
	@POST
	@Path("views")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QuoteView saveQuoteViewTemplate(@Context HttpServletRequest request, QuoteView view) {
		/* if we're creating a new one, it may be a duplicate and if so, we do not want to keep the old template id's */
		if(view.OrderTemplate != null) {
			view.OrderTemplate.setOrderTemplateID(null);
			if(view.OrderTemplate.getOrderItems() != null) {
				for(OrderItem item: view.OrderTemplate.getOrderItems()) {
					item.setOrderTemplateItemID(null);
				}
			}
		}
		logger.trace("saveQuoteViewTemplate");
		return qvs.saveQuoteView(request, view);
	}
	
	
	@PUT
	@Path("views/{QuoteViewID}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QuoteView updateQuoteViewTemplate(@Context HttpServletRequest request, @PathParam("QuoteViewID") String QuoteViewID, QuoteView view) {
		if(StringUtils.isBlank(QuoteViewID)) {
			throw new IllegalArgumentException("QuoteViewID not provided for PUT");
		}
		/* we check this as we will not pass through the path id */
		if(!QuoteViewID.equals(view.QuoteViewID)) {
			throw new IllegalArgumentException(String.format("QuoteViewID in Object [%1$s] did not match path ID [%2$s]",view.QuoteViewID, QuoteViewID));
		}
		
		return qvs.updateQuoteView(request, view);
	}
	
	@POST
	@Path("views/{quoteViewID}/duplicate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public QuoteView duplicateQuoteView(@Context HttpServletRequest request, @PathParam("quoteViewID") String quoteViewID) {
		logger.debug("duplicateOrder(Request, {}, {})", quoteViewID);
		if(StringUtils.isBlank(quoteViewID)) {
			throw new IllegalArgumentException("QuoteViewID must be supplied for duplicate action");
		}
		return qvs.duplicateQuoteView(request, quoteViewID);
	}
	
	@DELETE
	@Path("views/{QuoteViewID}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteQuoteViewTemplate(@Context HttpServletRequest request, @PathParam("QuoteViewID") String quoteViewID) {
		
		if(StringUtils.isBlank(quoteViewID)) {
			throw new IllegalArgumentException("QuoteViewID not provided for DELETE");
		}
		qvs.deleteQuoteView(request, quoteViewID);
	}
	
	@GET
	@Path("views/{quoteViewID}")
	@Produces(MediaType.APPLICATION_JSON)
	public QuoteView getQuoteViewTemplate(@Context HttpServletRequest request, @PathParam("quoteViewID") String quoteViewID) {
		//I don't think this is possible so long as the views/ path exists.
		if(StringUtils.isBlank(quoteViewID)) {
			throw new IllegalArgumentException("QuoteViewID not provided for GET");
		}
		return qvs.getQuoteView(request, quoteViewID);
	}
	
	@GET
	@Path("views/{QuoteViewID}/pages")
	@Produces(MediaType.APPLICATION_JSON)
	public List<QuoteViewPage> getQuoteViewTemplatePages(@Context HttpServletRequest request, @PathParam("QuoteViewID") String quoteViewID) {
		return qvs.getQuoteViewPages(request, quoteViewID);
	}
	
	@POST
	@Path("views/{QuoteViewID}/pages")
	@Produces(MediaType.APPLICATION_JSON)
	public QuoteViewPage createPage(@Context HttpServletRequest request, 
				@PathParam("QuoteViewID") String quoteViewID, QuoteViewPage quoteViewPage) {
		
		if(StringUtils.isBlank(quoteViewID)) {
			throw new IllegalArgumentException("QuoteViewID not provided for action update");
		}
		if(quoteViewPage == null) {
			throw new IllegalArgumentException("QuoteViewPage not provided for action update");
		}
		if(!quoteViewID.equals(quoteViewPage.QuoteViewID)) {
			throw new IllegalArgumentException("QuoteViewID did not match object");
		}
		return qvs.saveQuoteViewPage(request, quoteViewPage);
	}
	
	@PUT
	@Path("views/{QuoteViewID}/pages/{QuoteViewPageID}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QuoteViewPage createPage(@Context HttpServletRequest request, 
				@PathParam("QuoteViewID") String quoteViewID, 
				@PathParam("QuoteViewPageID") String quoteViewPageID, 
				QuoteViewPage quoteViewPage) {
		
		if(StringUtils.isBlank(quoteViewID)) {
			throw new IllegalArgumentException("QuoteViewID not provided for action update");
		}
		if(StringUtils.isBlank(quoteViewPageID)) {
			throw new IllegalArgumentException("QuoteViewPageID not provided for action update");
		}
		if(quoteViewPage == null) {
			throw new IllegalArgumentException("QuoteViewPage not provided for action update");
		}
		if(!quoteViewID.equals(quoteViewPage.QuoteViewID)) {
			throw new IllegalArgumentException("QuoteViewID did not match object");
		}
		if(!quoteViewPageID.equals(quoteViewPage.QuoteViewPageID)) {
			throw new IllegalArgumentException("QuoteViewPageID did not match object");
		}
		
		return qvs.updateQuoteViewPage(request, quoteViewPage);
	}
	
	@DELETE
	@Path("views/{QuoteViewID}/pages/{QuoteViewPageID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletePage(@Context HttpServletRequest request, 
				@PathParam("QuoteViewID") String QuoteViewID, 
				@PathParam("QuoteViewPageID") String QuoteViewPageID
				) {
		final UserBean currentuser = us.getCurrentUser(request);
		if(currentuser == null) {
			return jsonResponse(Status.FORBIDDEN, getErrorJson(new RuntimeException("Not logged in")));
		}
		try {
			final GenRow templates = new GenRow();
			templates.setRequest(request);
			templates.setViewSpec("quoteviews/QuoteViewPageView");
			templates.setParameter("-join1","QuoteViewPageQuoteView"); 
			templates.setParameter("-select1", "QuoteViewPageQuoteView.OrderTemplateID"); 
			templates.setParameter("QuoteViewID", QuoteViewID);
			templates.setParameter("QuoteViewPageID", QuoteViewPageID);
			templates.doAction(GenerationKeys.SELECT);
			if(templates.isSuccessful()) {
				
				GenRow items = new GenRow();
				items.setConnection(templates.getConnection());
				items.setTableSpec("OrderItems"); 
				items.setParameter("-select1","OrderItemID");
				items.setParameter("Category", "Custom-" + QuoteViewPageID);
				items.doAction(GenerationKeys.SELECTFIRST);

				if(logger.isTraceEnabled()) {
					logger.trace(items.getStatement());
				}
				
				if(items.isSuccessful()) {
					throw new RuntimeException("This page cannot be deleted as quote items reference it. If you are testing, deleting those quotes will allow you to proceed.");
				}
				
				items.clear(); 
				//items.setConnection(templates.getConnection());
				items.setTableSpec("OrderTemplateItems");
				items.setParameter("ON-Category", "%-" + QuoteViewPageID);
				items.doAction(GenerationKeys.DELETEALL);
				
				if(logger.isTraceEnabled()) {
					logger.trace(items.getStatement());
				}
				
				if(templates.isSet("OrderTemplateID")) {
					logger.debug("Clearing template for OrderTemplateID={}", templates.getData("OrderTemplateID"));
					OrderService.getInstance().setCachedTemplate(templates.getData("OrderTemplateID"), null);
					
					Order o = OrderService.getInstance().getCachedTemplate(templates.getData("OrderTemplateID"));
					
					if(o != null) {
						logger.warn("Somehow this did not clear the template! The sky is falling!"); 
						OrderService.getInstance().clearTemplateCache(); 
						o = OrderService.getInstance().getCachedTemplate(templates.getData("OrderTemplateID"));
						if(o != null) {
							logger.error("Somehow this did not clear the template cache! The sky has fallen. WTF!");
						}
					}
				} 
				templates.doAction(GenerationKeys.DELETE);
				
				if(!templates.isSuccessful()) {
					throw new RuntimeException(StringUtils.defaultIfEmpty(templates.getError(),"Error deleting page"));
				}
				
				ActivityLogger.getActivityLogger().logActivity(currentuser.getUser(), templates.getConnection(), "QuoteView", QuoteViewID, ActivityLogger.ActionType.Updated.name());
				ActivityLogger.getActivityLogger().logActivity(currentuser.getUser(), templates.getConnection(), "QuoteViewPage", QuoteViewPageID, ActivityLogger.ActionType.Deleted.name());
				
				if(logger.isTraceEnabled()) {
					logger.trace(templates.getStatement());
				}
				
				return jsonResponse(Status.OK, new JSONObject());
			}
		} catch (Exception e) {
			logger.error("Error producing quote view templates", e);
			return jsonResponse(Status.INTERNAL_SERVER_ERROR,getErrorJson(e));
		}
		return jsonResponse(Status.OK, new JSONObject());
	}

	
	@GET
	@Path("views/{QuoteViewID}/pages/{QuoteViewPageID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getQuoteViewTemplatePage(@Context HttpServletRequest request, @PathParam("QuoteViewID") String QuoteViewID, 
			@PathParam("QuoteViewPageID") String QuoteViewPageID) {
		final UserBean currentuser = us.getCurrentUser(request);
		if(currentuser == null) {
			return jsonResponse(Status.FORBIDDEN, getErrorJson(new RuntimeException("Not logged in")));
		}
		try {
			final GenRow templates = new GenRow();
			templates.setRequest(request);
			templates.setViewSpec("quoteviews/QuoteViewPageView");
			templates.setParameter("QuoteViewID", QuoteViewID);
			templates.setParameter("QuoteViewPageID", QuoteViewPageID);
			templates.setParameter("-sort1", "Active");
			templates.setParameter("-sort2","ViewName");
			templates.doAction(GenerationKeys.SELECT);
			if(templates.isSuccessful()) {
				return jsonResponse(Status.OK, serializeData(templates));
			} else {
				return jsonResponse(Status.NOT_FOUND, new JSONObject());
			}
		}  catch (Exception e) {
			logger.error("Error producing quote view templates", e);
			return jsonResponse(Status.INTERNAL_SERVER_ERROR,getErrorJson(e));
		}
	}
	
	private JSONObject serializeData(GenRow item) {
		final JSONObject e = new JSONObject();
		LeafNode n = null;
		Node n1 = null;
		for(Iterator<Node> i = item.entrySet(Node.type_DataNode).iterator(); i.hasNext(); ) {
			n1 = i.next(); 
			if(n1 instanceof LeafNode) { 
				n = (LeafNode)n1;
				//TODO Special case for	 json type?
				if(n.getValue() instanceof Date) {
					e.put(n.getKey() + "_ts", ((Date)n.getValue()).getTime());
				} else if ("Active".equals(n.getKey())) { 
					e.put("Active", "Y".equals(n.getValue()) ? true : false);
				} else {
					e.put(n.getKey(), n.getValue());
				}
			}
		} 
		return e;
	}

	private Object getErrorJson(Exception e) {
		return new DataApplication.DataApplicationException(e);
	}
	
	@POST
	@Path("views")
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.APPLICATION_JSON)
	public QuoteView saveQuoteViewTemplate(@FormParam("ViewName") String name, @FormParam("Description") String desc, 
			@FormParam("Active") boolean active, @FormParam("Expiry") String expiry, @Context HttpServletRequest request) {
		logger.trace("saveQuoteViewTemplate");
		final UserBean currentuser = us.getCurrentUser(request);
		if(currentuser == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		final GenRow r = new GenRow();
		r.setRequest(request);
		r.setViewSpec("quoteviews/QuoteViewView");
		r.setToNewID("QuoteViewID");
		r.setParameter("ViewName", name);
		r.setParameter("Description", desc);
		r.setParameter("Active", active ? "Y" : "N");
		r.setParameter("Expiry", expiry);
		r.setParameter("CreatedBy",currentuser.getUserID());
		r.setParameter("CreatedDate", "NOW");
		r.setParameter("ModifiedBy",currentuser.getUserID());
		r.setParameter("ModifiedDate", "NOW");
		r.setParameter("OrderTemplateID", KeyMaker.generate());
		final String qt = KeyMaker.generate();
		r.setParameter("QuoteViewID", qt);
		r.doAction(GenerationKeys.INSERT);
		GenRow ot = new GenRow();
		ot.setRequest(request);
		ot.setViewSpec("OrderTemplateView");
		ot.setParameter("OrderTemplateID", qt);
		ot.setParameter("GroupID", currentuser.getString("DefaultGroupID"));
		ot.setParameter("CreatedBy",currentuser.getUserID());
		ot.setParameter("CreatedDate","NOW");
		ot.setParameter("ModifiedBy",currentuser.getUserID());
		ot.setParameter("ModifiedDate","NOW");
		ot.setParameter("Name","Generated Quote");
		ot.setParameter("Description", name + " Quote Template");
		ot.doAction(GenerationKeys.INSERT);
		
		if(!r.isSuccessful()) {
			throw new RuntimeException("Failed to insert data into database " + r.getError());
		}
		r.doAction(GenerationKeys.SELECT);
		
		return this.getQuoteViewTemplate(request, r.getString("QuoteViewID"));
	}
	
	/** 
	 * NB, object should have a jax-b annoted class 
	 * @param s
	 * @param entity
	 * @return
	 */
	private Response jsonResponse(Status s, Object entity) {
		return Response.status(s).entity(entity).type(MediaType.APPLICATION_JSON_TYPE).build();
	}
}
