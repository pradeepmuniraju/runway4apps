package com.sok.runway.externalInterface.rest.rpm;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.RealEstate;
import com.sok.service.crm.cms.properties.PlanService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

@Path("/realestate")
@Api(value = "/realestate", description = "Retrieve Real Estate Property Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class RealEstateResource {
	private static final Logger logger = LoggerFactory.getLogger(RealEstateResource.class);
	@Context HttpServletRequest request;
	private static final PlanService planService = PlanService.getInstance();
	
	@GET
	@ApiOperation(value = "List all real estate", notes="Optionally filtering by modified date", responseClass="com.sok.runway.externalInterface.beans.cms.properties.RealEstate", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<RealEstate> getRealEstateProperties(@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = true) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active real estate", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			logger.debug("getRealEstateProperties({})", modifiedSince, modifiedSince);
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		return planService.getRealEstateExternal(request, mod, active);
	}

	@GET
	@Path("/{realEstateID}")
	@ApiOperation(value = "Return a particular real estate property", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.RealEstate")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public RealEstate getRealEstateProperty(@Context HttpServletRequest request, @PathParam("realEstateID") final String realEstateID, 
			@ApiParam(value = "The returned lot representation", allowableValues="Simple,Extended", required=false, defaultValue="Simple") @DefaultValue("Simple") @QueryParam("View") BeanRepresentation extended) {
		return BeanFactory.external(planService.getRealEstate(request, realEstateID), extended);
	}
	
	@GET
	@ApiResource(value = "/{realEstateID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Real estate", notes="<p>This call will return a list of Albums and their Images for a given Real estate. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/{realEstateID}/albums")
	public AlbumResource getRealEstateAlbumResource(@PathParam("realEstateID") final String realEstateID) {
		return new AlbumResource(request,  realEstateID);
	}
	
	@ApiResource(value = "/publishing", resourceClass="com.sok.runway.externalInterface.rest.rpm.PublishingListResource")
	@Path("publishing")
	public PublishingListResource getPublishingResource() {
		return new PublishingListResource(request, PropertyType.ExistingProperty);
	}
	
	@GET
	@ApiResource(value = "/{realEstateID}/publishing", resourceClass="com.sok.runway.externalInterface.rest.rpm.PublishingResource")
	@Path("/{realEstateID}/publishing")
	public PublishingResource getPackagePublishingResource(@PathParam("realEstateID") final String realEstateID,@ApiParam(value = "A publish method", allowableValues="Website", required = true) @QueryParam("PublishMethod") String publishMethod) {
		return new PublishingResource(request, PropertyType.ExistingProperty, realEstateID, publishMethod);
	}
}
