package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.InitServlet;

public class VisibilityStatus {

	public VisibilityStatus() {
		// TODO Auto-generated constructor stub
	}
	
	public static boolean checkVisibilityStatus(String active, String statusID, Date availableDate, Date expiryDate) {
		
		if (!"Y".equals(active)) return false;
		
		String availableID = InitServlet.getSystemParam("ProductAvailableStatus");
		
		if (StringUtils.isNotBlank(availableID) && !availableID.equals(statusID)) return false;
		
		if (availableDate != null && availableDate.after(new Date())) return false;
		
		if (expiryDate != null && expiryDate.before(new Date())) return false;

		return true;
	}

	public static boolean checkPublishedStatus(String publishFlag, String status, Date startDate, Date endDate) {
		
		//if (!"Y".equals(publishFlag)) return false;
		
		if (!"Published".equals(status) && !"Pre-release".equals(status)) return false;
		
		if (startDate != null && startDate.after(new Date())) return false;
		
		if (endDate != null && endDate.before(new Date())) return false;

		return true;
	}

}
