package com.sok.runway.externalInterface.beans.shared;
import java.util.*;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class CompanyAnswer extends Answer
{
	private String companyid = null;
	
	public String getCompanyID()
	{
		return(companyid);
	}	
	
	public void setCompanyID(String companyid)
	{
		this.companyid = companyid;
	}		
}