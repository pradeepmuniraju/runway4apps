package com.sok.runway.externalInterface.beans.shared;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties({ "ItemIncCost" }) /* using for storage of inc gst item cost */
public class PackageCost
{
	private String name = null;
	private String productid = null;
	private String description = null;
	private double itemcost = 0;
	private double gst = 0;
	private double cost = 0;
	private double quantity = 1;
	private double totalcost = 0;
	private String category = null;
	
	@JsonProperty("Name")
	public String getName()
	{
		return(name);
	}	
	
	@JsonProperty("Name")
	public void setName(String name)
	{
		this.name = name;
	}	
	
	@JsonProperty("ProductID")
	public String getProductID()
	{
		return(productid);
	}	
	
	@JsonProperty("ProductID")
	public void setProductID(String productid)
	{
		this.productid = productid;
	}	
	
	@JsonProperty("Description")
	public String getDescription()
	{
		return(description);
	}	
	
	@JsonProperty("Description")
	public void setDescription(String description)
	{
		this.description = description;
	}	
	
	@JsonProperty("ItemCost")
	public double getItemCost()
	{
		return(itemcost);
	}	
	
	@JsonProperty("ItemCost")
	public void setItemCost(double itemcost)
	{
		this.itemcost = itemcost;
	}	
	
	@JsonProperty("GST")
	public double getGST()
	{
		return(gst);
	}	
	
	@JsonProperty("GST")
	public void setGST(double gst)
	{
		this.gst = gst;
	}	
	
	@JsonProperty("Cost")
	public double getCost()
	{
		return(cost);
	}	
	
	@JsonProperty("Cost")
	public void setCost(double cost)
	{
		this.cost = cost;
	}	
	
	
	@JsonProperty("Quantity")
	public double getQuantity()
	{
		return(quantity);
	}	
	
	@JsonProperty("Quantity")
	public void setQuantity(double quantity)
	{
		this.quantity = quantity;
	}	
	
	@JsonProperty("TotalCost")
	public double getTotalCost()
	{
		return(totalcost);
	}	
	
	@JsonProperty("TotalCost")
	public void setTotalCost(double totalcost)
	{
		this.totalcost = totalcost;
	}	
	
	@JsonProperty("Category")
	public void setCategory(String category) 
	{
		this.category = category;
	}
	@JsonProperty("Category")
	public String getCategory() {
		return this.category;
	}
	public double getItemcost() {
		return itemcost;
	}

	public double getGst() {
		return gst;
	}

	public double getTotalcost() {
		return totalcost;
	}
}