package com.sok.runway.externalInterface.beans;

import java.util.*;
import com.sok.runway.externalInterface.beans.shared.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class Note
{
	private String noteid = null;
	private String repuserid = null;
	private String status = null;
	private Date notedate = null;
	private Date createddate = null;
	private String createdby = null;
	private Date modifieddate = null;
	private String modifiedby = null;
	private String contactid = null;
	private String companyid = null;
	private String productgroupid = null;
	private String opportunityid = null;
	private String templatename = null;
	private String type = null;
	private String completed = null;
	private Date sentdate = null;
	private String templateused = null;
	private String cmsused = null;
	private String campaign = null;
	private String campaignid = null;
	private String subject = null;
	private String body = null;
	private String htmlbody = null;
	private String emailto = null;
	private String emailfrom = null;
	private String emailcc = null;
	private String emailbcc = null;
	private String duration = null;
	private Date notetime = null;
	private String callstatus = null;
	private String appointmentstatus = null;
	private String groupid = null;
	private String notecode = null;
	private String orderid = null;
	private String venue = null;
	private String url = null;
	private String mastertemplateid = null;
	private String messageid = null;
	private Date enddate = null;
	private String processstageid = null;
	private String approval = null;
   private String parentnoteid = null;	
	
	private String error = null;
	
	/**
	 * @return
	 */
	public String getError()
	{
		return(error);
	}	
	
	/**
	 * @param error
	 */
	public void setError(String error)
	{
		this.error = error;
	}	
	
	
	/**
	 * @return
	 */
	public String getNoteID()
	{
		return(noteid);
	}	
	
	/**
	 * @param noteid
	 * 
	 * Primary Key for notes. Runway keys are generally 30 characters and must be alphanumeric.
	 */
	public void setNoteID(String noteid)
	{
		this.noteid = noteid;
	}	
	
	/**
	 * @return
	 */
	public String getRepUserID()
	{
		return(repuserid);
	}	
	
	/**
	 * @param repuserid
	 */
	public void setRepUserID(String repuserid)
	{
		this.repuserid = repuserid;
	}	
	
	/**
	 * @return
	 */
	public String getStatus()
	{
		return(status);
	}	
	
	/**
	 * @param status
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}	
	
	/**
	 * @return
	 */
	public Date getNoteDate()
	{
		return(notedate);
	}	
	
	/**
	 * @param notedate
	 */
	public void setNoteDate(Date notedate)
	{
		this.notedate = notedate;
	}	
	
	/**
	 * @return
	 */
	public Date getCreatedDate()
	{
		return(createddate);
	}	
	
	/**
	 * @param createddate
	 */
	public void setCreatedDate(Date createddate)
	{
		this.createddate = createddate;
	}	
	
	/**
	 * @return
	 */
	public String getCreatedBy()
	{
		return(createdby);
	}	
	
	/**
	 * @param createdby
	 */
	public void setCreatedBy(String createdby)
	{
		this.createdby = createdby;
	}	
	
	/**
	 * @return
	 */
	public Date getModifiedDate()
	{
		return(modifieddate);
	}	
	
	/**
	 * @param modifieddate
	 */
	public void setModifiedDate(Date modifieddate)
	{
		this.modifieddate = modifieddate;
	}	
	
	/**
	 * @return
	 */
	public String getModifiedBy()
	{
		return(modifiedby);
	}	
	
	public void setModifiedBy(String modifiedby)
	{
		this.modifiedby = modifiedby;
	}	
	
	public String getContactID()
	{
		return(contactid);
	}	
	
	public void setContactID(String contactid)
	{
		this.contactid = contactid;
	}	
	
	public String getCompanyID()
	{
		return(companyid);
	}	
	
	public void setCompanyID(String companyid)
	{
		this.companyid = companyid;
	}	
	
	public String getProductGroupID()
	{
		return(productgroupid);
	}	
	
	public void setProductGroupID(String productgroupid)
	{
		this.productgroupid = productgroupid;
	}	
	
	public String getOpportunityID()
	{
		return(opportunityid);
	}	
	
	public void setOpportunityID(String opportunityid)
	{
		this.opportunityid = opportunityid;
	}	
	
	public String getTemplateName()
	{
		return(templatename);
	}	
	
	public void setTemplateName(String templatename)
	{
		this.templatename = templatename;
	}	
	
	public String getType()
	{
		return(type);
	}	
	
	public void setType(String type)
	{
		this.type = type;
	}	
	
	public String getCompleted()
	{
		return(completed);
	}	
	
	public void setCompleted(String completed)
	{
		this.completed = completed;
	}	
	
	public Date getSentDate()
	{
		return(sentdate);
	}	
	
	public void setSentDate(Date sentdate)
	{
		this.sentdate = sentdate;
	}	
	
	public String getTemplateUsed()
	{
		return(templateused);
	}	
	
	public void setTemplateUsed(String templateused)
	{
		this.templateused = templateused;
	}	
	
	public String getCMSUsed()
	{
		return(cmsused);
	}	
	
	public void setCMSUsed(String cmsused)
	{
		this.cmsused = cmsused;
	}	
	
	public String getCampaign()
	{
		return(campaign);
	}	
	
	public void setCampaign(String campaign)
	{
		this.campaign = campaign;
	}	
	
	public String getCampaignID()
	{
		return(campaignid);
	}	
	
	public void setCampaignID(String campaignid)
	{
		this.campaignid = campaignid;
	}	
	
	public String getSubject()
	{
		return(subject);
	}	
	
	public void setSubject(String subject)
	{
		this.subject = subject;
	}	
	
	public String getBody()
	{
		return(body);
	}	
	
	public void setBody(String body)
	{
		this.body = body;
	}	
	
	public String getHTMLBody()
	{
		return(htmlbody);
	}	
	
	public void setHTMLBody(String htmlbody)
	{
		this.htmlbody = htmlbody;
	}	
	
	public String getEmailTo()
	{
		return(emailto);
	}	
	
	public void setEmailTo(String emailto)
	{
		this.emailto = emailto;
	}	
	
	public String getEmailFrom()
	{
		return(emailfrom);
	}	
	
	public void setEmailFrom(String emailfrom)
	{
		this.emailfrom = emailfrom;
	}	
	
	public String getEmailCC()
	{
		return(emailcc);
	}	
	
	public void setEmailCC(String emailcc)
	{
		this.emailcc = emailcc;
	}	
	
	public String getEmailBCC()
	{
		return(emailbcc);
	}	
	
	public void setEmailBCC(String emailbcc)
	{
		this.emailbcc = emailbcc;
	}	
	
	public String getDuration()
	{
		return(duration);
	}	
	
	public void setDuration(String duration)
	{
		this.duration = duration;
	}	
	
	public Date getNoteTime()
	{
		return(notetime);
	}	
	
	public void setNoteTime(Date notetime)
	{
		this.notetime = notetime;
	}	
	
	public String getCallStatus()
	{
		return(callstatus);
	}	
	
	public void setCallStatus(String callstatus)
	{
		this.callstatus = callstatus;
	}	
	
	public String getAppointmentStatus()
	{
		return(appointmentstatus);
	}	
	
	public void setAppointmentStatus(String appointmentstatus)
	{
		this.appointmentstatus = appointmentstatus;
	}	
	
	public String getGroupID()
	{
		return(groupid);
	}	
	
	public void setGroupID(String groupid)
	{
		this.groupid = groupid;
	}	
	
	public String getNoteCode()
	{
		return(notecode);
	}	
	
	public void setNoteCode(String notecode)
	{
		this.notecode = notecode;
	}	
	
	public String getOrderID()
	{
		return(orderid);
	}	
	
	public void setOrderID(String orderid)
	{
		this.orderid = orderid;
	}	
	
	public String getVenue()
	{
		return(venue);
	}	
	
	public void setVenue(String venue)
	{
		this.venue = venue;
	}	
	
	public String getURL()
	{
		return(url);
	}	
	
	public void setURL(String url)
	{
		this.url = url;
	}	
	
	public String getMasterTemplateID()
	{
		return(mastertemplateid);
	}	
	
	public void setMasterTemplateID(String mastertemplateid)
	{
		this.mastertemplateid = mastertemplateid;
	}	
	
	public String getMessageID()
	{
		return(messageid);
	}	
	
	public void setMessageID(String messageid)
	{
		this.messageid = messageid;
	}	
	
	public Date getEndDate()
	{
		return(enddate);
	}	
	
	public void setEndDate(Date enddate)
	{
		this.enddate = enddate;
	}	
	
	public String getProcessStageID()
	{
		return(processstageid);
	}	
	
	public void setProcessStageID(String processstageid)
	{
		this.processstageid = processstageid;
	}	
	
	public String getApproval()
	{
		return(approval);
	}	
	
	public void setApproval(String approval)
	{
		this.approval = approval;
	}	
	
	private PersonName modifiedbyname = null;
	private PersonName createdbyname = null;
	
	public PersonName getModifiedByName()
	{
		return(modifiedbyname);
	}
	
	public void setModifiedByName(PersonName modifiedbyname)
	{
		this.modifiedbyname = modifiedbyname;
	}
	
	public PersonName getCreatedByName()
	{
		return(createdbyname);
	}
	
	public void setCreatedByName(PersonName createdbyname)
	{
		this.createdbyname = createdbyname;
	}	
	
   public String getParentNoteID()
   {
      return(parentnoteid);
   }  
   
   public void setParentNoteID(String parentnoteid)
   {
      this.parentnoteid = parentnoteid;
   }  	
}

