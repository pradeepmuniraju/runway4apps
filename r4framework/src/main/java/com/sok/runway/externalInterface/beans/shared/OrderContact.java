package com.sok.runway.externalInterface.beans.shared;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.sok.runway.externalInterface.beans.Contact;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@XmlAccessorType(XmlAccessType.NONE)
//TODO - find where this is being submitted and remove.
@JsonIgnoreProperties(ignoreUnknown=true)
public class OrderContact extends Contact {

	private String orderContactID;
	private String role;
	private String orderID;
	private String recipientType;
	
	@JsonProperty("OrderContactID")
	public String getOrderContactID() {
		return orderContactID;
	}
	@JsonProperty("OrderContactID")
	public void setOrderContactID(String orderContactID) {
		this.orderContactID = orderContactID;
	}
	@JsonProperty("Role")
	public String getRole() {
		return role;
	}
	@JsonProperty("Role")
	public void setRole(String role) {
		this.role = role;
	}
	@JsonProperty("OrderID")
	public String getOrderID() {
		return orderID;
	}
	@JsonProperty("OrderID")
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	@JsonProperty("RecipientType")
	public String getRecipientType() {
		return recipientType;
	}
	@JsonProperty("RecipientType")
	public void setRecipientType(String recipientType) {
		this.recipientType = recipientType;
	}
}
