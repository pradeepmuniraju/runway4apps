package com.sok.runway.externalInterface.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.externalInterface.DataApplication;
import com.sok.runway.externalInterface.beans.Display;
import com.sok.runway.externalInterface.beans.Document;
import com.sok.runway.externalInterface.beans.LinkedDocument;
import com.sok.runway.externalInterface.beans.PropertyOption;
import com.sok.runway.externalInterface.beans.shared.ProductSecurityGroup;
import com.sok.runway.externalInterface.resources.cms.properties.BuilderResource;
import com.sok.runway.externalInterface.resources.cms.properties.BuildingResource;
import com.sok.runway.externalInterface.resources.cms.properties.EstateResource;
import com.sok.runway.externalInterface.resources.cms.properties.LotResource;
import com.sok.runway.externalInterface.resources.cms.properties.PropertyDetailResource;
import com.sok.service.crm.DocumentService;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.DisplayService;
import com.sok.service.crm.cms.properties.DripService;
import com.sok.service.crm.cms.properties.DripService.DripCollection;
import com.sok.service.crm.cms.properties.DripService.DripProduct;
import com.sok.service.crm.cms.properties.FacadeService;
import com.sok.service.crm.cms.properties.PlanService;
import com.sok.service.crm.cms.properties.PlanService.Plan;
import com.sok.service.crm.cms.properties.PlanService.ExistingProperty;
import com.sok.service.crm.cms.properties.PropertyOptionDevineService;
import com.sok.service.crm.cms.properties.PropertyOptionFairHavenService;
import com.sok.service.crm.cms.properties.PropertyOptionMetriconService;
import com.sok.service.crm.cms.properties.PropertyOptionPDHService;
import com.sok.service.crm.cms.properties.PropertyOptionService;

@Path("/cms/properties/")
public class PropertyResource {
	private static final PropertyResource resource = new PropertyResource();
	private static final PropertyOptionService pos = PropertyOptionService.getInstance();
	private static PropertyOptionPDHService pdh = null;
	private static PropertyOptionMetriconService metricon = null;
	private static PropertyOptionFairHavenService fairhaven = null;
	private static PropertyOptionDevineService dev = null;
	private static final DisplayService ds = DisplayService.getInstance();
	private static final DocumentService documentService = DocumentService.getInstance();
	private static final FacadeService fs = FacadeService.getInstance();
	private static final PlanService ps = PlanService.getInstance();
	private static final DripService dls = DripService.getInstance();
	private static final UserService us =  UserService.getInstance();
	private static final Logger logger = LoggerFactory.getLogger(PropertyResource.class);
	public static PropertyResource getInstance() {
		return resource;
	}	
	@Context HttpServletRequest request;
	
	/* TODO this isn't really correct, it should be option-categories/ to return the categories and either this or /option-categories/{categoryId}/options to return the options */
	@GET
	@Path("options")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPropertyOptionCategories(@Context HttpServletRequest request, @QueryParam("CategoryID") String categoryId, @QueryParam("search") String search) {
		logger.debug("getPropertyOptionCategories()");
		if(StringUtils.isNotBlank(categoryId)) {
			return DataApplication.jsonResponse(Status.OK, pos.getPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), categoryId, search));
		}
		return DataApplication.jsonResponse(Status.OK, pos.getPropertyOptionCategories(ActionBean.getConnection(request), us.getCurrentUser(request)));
	}
	
	
	/* TODO this isn't really correct, it should be option-categories/ to return the categories and either this or /option-categories/{categoryId}/options to return the options */
	@GET
	@Path("order-options")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPropertyOptionCategories(@Context HttpServletRequest request, @QueryParam("CategoryID") String categoryId, @QueryParam("search") String search, @QueryParam("OrderID") String orderId) {
		logger.debug("getPropertyOptionCategories()");
		if(StringUtils.isNotBlank(categoryId)) {
			if (StringUtils.isNotBlank(orderId)) {
				if (pdh == null) pdh = PropertyOptionPDHService.getInstance();
				return DataApplication.jsonResponse(Status.OK, pdh.getPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), categoryId, search, orderId));
			} else {
				return DataApplication.jsonResponse(Status.OK, pos.getPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), categoryId, search));
			}
		}
		return DataApplication.jsonResponse(Status.OK, pos.getPropertyOptionCategories(ActionBean.getConnection(request), us.getCurrentUser(request)));
	}
	
	/* TODO this isn't really correct, it should be option-categories/ to return the categories and either this or /option-categories/{categoryId}/options to return the options */
	@GET
	@Path("devine-options")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPropertyOptionDevine(@Context HttpServletRequest request, @QueryParam("CategoryID") String categoryId, @QueryParam("search") String search, @QueryParam("OrderID") String orderId) {
		logger.debug("getPropertyOptionCategories()");
		if(StringUtils.isNotBlank(categoryId)) {
			if (StringUtils.isNotBlank(orderId)) {
				if (dev == null) dev = PropertyOptionDevineService.getInstance();
				return DataApplication.jsonResponse(Status.OK, dev.getPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), categoryId, search, orderId));
			} else {
				return DataApplication.jsonResponse(Status.OK, pos.getPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), categoryId, search));
			}
		}
		return DataApplication.jsonResponse(Status.OK, pos.getPropertyOptionCategories(ActionBean.getConnection(request), us.getCurrentUser(request)));
	}
	
	/* TODO this isn't really correct, it should be option-categories/ to return the categories and either this or /option-categories/{categoryId}/options to return the options */
	@GET
	@Path("metricon-options")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPropertyOptionMetricon(@Context HttpServletRequest request, @QueryParam("CategoryID") String categoryId, @QueryParam("search") String search, @QueryParam("OrderID") String orderId) {
		logger.debug("getPropertyOptionCategories()");
		if(StringUtils.isNotBlank(categoryId)) {
			if (StringUtils.isNotBlank(orderId)) {
				if (metricon == null) metricon = PropertyOptionMetriconService.getInstance();
				return DataApplication.jsonResponse(Status.OK, metricon.getPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), categoryId, search, orderId));
			} else {
				return DataApplication.jsonResponse(Status.OK, pos.getPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), categoryId, search));
			}
		}
		return DataApplication.jsonResponse(Status.OK, pos.getPropertyOptionCategories(ActionBean.getConnection(request), us.getCurrentUser(request)));
	}
	
	/* TODO this isn't really correct, it should be option-categories/ to return the categories and either this or /option-categories/{categoryId}/options to return the options */
	@GET
	@Path("fairhaven-options")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPropertyOptionFairHaven(@Context HttpServletRequest request, @QueryParam("CategoryID") String categoryId, @QueryParam("search") String search, @QueryParam("OrderID") String orderId) {
		logger.debug("getPropertyOptionCategories()");
		if(StringUtils.isNotBlank(categoryId)) {
			if (StringUtils.isNotBlank(orderId)) {
				if (fairhaven == null) fairhaven = PropertyOptionFairHavenService.getInstance();
				return DataApplication.jsonResponse(Status.OK, fairhaven.getPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), categoryId, search, orderId));
			} else {
				return DataApplication.jsonResponse(Status.OK, pos.getPropertyOptions(ActionBean.getConnection(request), us.getCurrentUser(request), categoryId, search));
			}
		}
		return DataApplication.jsonResponse(Status.OK, pos.getPropertyOptionCategories(ActionBean.getConnection(request), us.getCurrentUser(request)));
	}
	
	@GET
	@Path("displays")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Display> getDisplays(@Context HttpServletRequest request, @QueryParam("Type") String type) {
		logger.debug("getDisplays(Request, {})", type);
		return ds.getDisplays(request, type); 
	}
	
	@GET
	@Path("displays/{productId}/options")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PropertyOption> getDisplayOptions(@Context HttpServletRequest request, @PathParam("productId") String productId, @QueryParam("CategoryID") String categoryId, @QueryParam("search") String search, @QueryParam("Type") String type) {
		if(logger.isTraceEnabled()) logger.trace("getDisplayOptions({}, {}, {}, {})", new String[]{productId, categoryId, search, type});
		return pos.getDisplayPropertyOptions(request, productId, categoryId, search, type);
	}
	
	@GET
	@Path("displays/{productId}/order-options")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PropertyOption> getDisplayOptions(@Context HttpServletRequest request, @PathParam("productId") String productId, @QueryParam("CategoryID") String categoryId, @QueryParam("search") String search, @QueryParam("Type") String type, @QueryParam("OrderID") String orderId) {
		if(logger.isTraceEnabled()) logger.trace("getDisplayOptions({}, {}, {}, {})", new String[]{productId, categoryId, search, type});
		if(StringUtils.isNotBlank(categoryId)) {
			if (StringUtils.isNotBlank(orderId)) {
				if (pdh == null) pdh = PropertyOptionPDHService.getInstance();
				return pdh.getDisplayPropertyOptions(request, productId, categoryId, search, type, orderId);
			}
		}
		return pos.getDisplayPropertyOptions(request, productId, categoryId, search, type);
	}
	
	@GET
	@Path("displays/{productId}/metricon-options")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PropertyOption> getDisplayMetriconOptions(@Context HttpServletRequest request, @PathParam("productId") String productId, @QueryParam("CategoryID") String categoryId, @QueryParam("search") String search, @QueryParam("Type") String type, @QueryParam("OrderID") String orderId) {
		if(logger.isTraceEnabled()) logger.trace("getDisplayOptions({}, {}, {}, {})", new String[]{productId, categoryId, search, type});
		if(StringUtils.isNotBlank(categoryId)) {
			if (StringUtils.isNotBlank(orderId)) {
				if (metricon == null) metricon = PropertyOptionMetriconService.getInstance();
				return metricon.getDisplayPropertyOptions(request, productId, categoryId, search, type, orderId);
			}
		}
		return pos.getDisplayPropertyOptions(request, productId, categoryId, search, type);
	}
	
	@GET
	@Path("facades")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FacadeService.Facade> getFacades(@Context HttpServletRequest request) {
		return fs.getFacades(request, "ALL");
	}
	
	/**
	 * Search for Homes
	 * @return
	 */
	@GET
	@Path("homes/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PlanService.HLRange> getHomes(@Context HttpServletRequest request, @QueryParam("RangeProductID") String RangeProductID)
	{
		return ps.getHomes(request, RangeProductID);
	}
	
	@GET
	@Path("homes/{HomeProductID}/plans")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Plan> getHomePlans(@Context HttpServletRequest request, @PathParam("HomeProductID") String HomeProductID)
	{
		return ps.getPlans(request, HomeProductID);
	}
	
	@GET
	@Path("homes/{homeID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PlanService.HLRange getHome(@Context HttpServletRequest request, @PathParam("homeID") String homeID)
	{
		return ps.getHome(request, homeID);
	}
	
	
	@GET
	@Path("homes/{homeProductID}/facades")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FacadeService.Facade> getFacades(@Context HttpServletRequest request, @PathParam("homeProductID") String homeProductID) {
		return fs.getFacades(request, homeProductID);
	}
	
	@GET
	@Path("drips")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DripProduct> getDripProductsList(@Context HttpServletRequest request, @QueryParam("Active") @DefaultValue("false") boolean showDisabled) {
		logger.trace("getDripLibraries(Request)");
		return dls.getDripProducts(request, showDisabled);
	}
	/*
	@GET
	@Path("drips/{productLibraryID}")
	@Produces(MediaType.APPLICATION_JSON)
	public DripLibrary getDripLibrary(@Context HttpServletRequest request, @PathParam("productLibraryID") String productLibraryID) {
		logger.trace("getDripLibrary(Request, {})", productLibraryID);
		return dls.getDripLibrary(request, productLibraryID);
	}
	
	@GET
	@Path("drips/{productLibraryID}/products")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DripProduct> getDripProducts(@Context HttpServletRequest request, @PathParam("productLibraryID") String productLibraryID, @QueryParam("ProductID") String productID) {
		logger.trace("getDripProducts(Request, {})", productLibraryID);
		return dls.getDripProducts(request, productID, productLibraryID);
	}
	*/
	
	/*
	 * get an individual drip product 			-- NB, apparently this was never completed.
	 * @param request
	 * @param productLibraryID
	 * @param dripProductID
	 * @return
	 
	@GET
	@Path("drips/{productLibraryID}/products/{dripProductID}")
	@Produces(MediaType.APPLICATION_JSON)
	public DripProduct getDripProduct(@Context HttpServletRequest request, @PathParam("productLibraryID") String productLibraryID, @PathParam("dripProductID") String dripProductID) {
		logger.trace("getDripProduct(Request, {}, {})", productLibraryID, dripProductID);
		return dls.getDripProduct(request, productLibraryID, dripProductID);
	}
	*/
	
	@Path("estates")
	public EstateResource getEstateResource() {
		return EstateResource.getInstance();
	}
	
	@Path("lots")
	public LotResource getLotResource() {
		return new LotResource(request);
	}
	
	@Path("builders")
	public BuilderResource getBuilderResource() {
		return BuilderResource.getInstance();
	}
	
	@Path("buildings")
	public BuildingResource getBuildingResource() {
		return BuildingResource.getInstance();
	}

	@GET
	@Path("existingproperties/{productId}")
	@Produces(MediaType.APPLICATION_JSON)
	public PlanService.ExistingProperty getExistingProperty(@Context HttpServletRequest request, @PathParam("productId") String productId) {
		return ps.getExistingProperty(request,  productId);
	}
	
	/* homedetails = details 
	@GET
	@Path("existingproperties/{productId}/homedetails")
	@Produces(MediaType.APPLICATION_JSON)
	public PlanService.Property getExistingPropertyDetails(@Context HttpServletRequest request, @PathParam("productId") String productId) {
		return null;
	}

	@GET
	@Path("existingproperties/{productId}/homedetails/{detailID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PlanService.Property getExistingPropertyDetail(@Context HttpServletRequest request, @PathParam("productId") String productId) {
		return null;
	}
	
	@POST
	@Path("existingproperties/{productId}/homedetails/")
	@Produces(MediaType.APPLICATION_JSON)
	public PlanService.Property saveExistingPropertyDetail(@Context HttpServletRequest request, @PathParam("productId") String productId) {
		return null;
	}
	
	@PUT
	@Path("existingproperties/{productId}/homedetails/")
	@Produces(MediaType.APPLICATION_JSON)
	public PlanService.Property updateExistingPropertyDetail(@Context HttpServletRequest request, @PathParam("productId") String productId) {
		return null;
	}
	*/
	
	/* propertydetails = dimentions 
	@GET
	@Path("products/{productId}/details")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProductDetail> getExistingPropertyDimentions(@Context HttpServletRequest request, @PathParam("productId") String productId) {
		return ps.getProductDetails(request, productId);
	}
	 */
	
	/* 
	@GET
	@Path("products/{productId}/details/{productDetailID}")
	@Produces(MediaType.APPLICATION_JSON)
	public ProductDetail getExistingPropertyDimentions(@Context HttpServletRequest request, @PathParam("productId") String productId, @PathParam("productDetailID") String productDetailID) {
		return ps.getProductDetail(request, productId, productDetailID);
	}
	
	@POST
	@Path("products/{productId}/details/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ProductDetail saveExistingPropertyDimentions(@Context HttpServletRequest request, @PathParam("productId") String productId, ProductDetail productDetail) {
		productDetail.ProductID = productId;
		productDetail.ProductDetailsID = null;
		return ps.saveProductDetail(request, productDetail);
	}
	*/
	
	@GET
	@Path("products/{productId}/documents/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Document> getProductDocuments(@Context HttpServletRequest request, @PathParam("productId") String productId) {
		return documentService.getProductLinkedDocuments(request, productId);
	}
	
	@POST
	@Path("products/{productId}/documents/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LinkedDocument linkDocumentToProduct(@Context HttpServletRequest request, @PathParam("productId") String productId, LinkedDocument linkedDocument) {
		if(!productId.equals(linkedDocument.getProductID())) {
			throw new IllegalArgumentException("ProductIDs must match");
		}
		return documentService.createLinkedDocument(request, linkedDocument);
	}
	
	@POST
	@Path("products/{productId}/documents/Pipeline")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LinkedDocument linkDocumentToProductForPipeline(@Context HttpServletRequest request, @PathParam("productId") String productId, LinkedDocument linkedDocument) {
		if(!productId.equals(linkedDocument.getProductID())) {
			throw new IllegalArgumentException("ProductIDs must match");
		}
		return documentService.createLinkedDocumentForPipeline(request, linkedDocument);
	}	
	
	@Path("products/{productId}/details")
	public PropertyDetailResource getPropertyDetailResource(@PathParam("productId") String productId) {
		return new PropertyDetailResource(productId, request);
	}

	/* productsecuritygroups */
	@GET
	@Path("products/{productId}/productsecuritygroups")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProductSecurityGroup> getProductSecurityGroups(@Context HttpServletRequest request, @PathParam("productId") String productId) {
		return ps.getProductSecurityGroups(request, productId);
	}
		
	@PUT
	@Path("products/{productId}/productsecuritygroups/{ProductSecurityGroupID}")
	@Produces(MediaType.APPLICATION_JSON)
	public ProductSecurityGroup updateProductSecurityGroup(@Context HttpServletRequest request, @PathParam("productId") String productId, @PathParam("ProductSecurityGroupID") String productSecurityGroupID, ProductSecurityGroup productSecurityGroup) {
		productSecurityGroup.ProductID = productId;
		productSecurityGroup.ProductSecurityGroupID = productSecurityGroupID;
		return ps.saveProductSecurityGroup(request, productSecurityGroup);
	}
	
	@POST
	@Path("products/{productId}/productsecuritygroups")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ProductSecurityGroup saveProductSecurityGroup(@Context HttpServletRequest request, @PathParam("productId") String productId, ProductSecurityGroup productSecurityGroup) {
		productSecurityGroup.ProductID = productId;
		productSecurityGroup.ProductSecurityGroupID = null;
		return ps.saveProductSecurityGroup(request, productSecurityGroup);
	}
	
	@POST
	@Path("existingproperties")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PlanService.ExistingProperty saveExistingProperty(@Context HttpServletRequest request, PlanService.ExistingProperty property) {
		logger.debug("saveExistingProperty(request, property(ProductSubType={}))", property.ProductSubType);
		return ps.saveExistingProperty(request,  property);
	}
	
	@PUT
	@Path("existingproperties/{productId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public PlanService.ExistingProperty updateProperty(@Context HttpServletRequest request, @PathParam("productId") String productId, PlanService.ExistingProperty property) {
		if(!StringUtils.equals(productId, property.ProductID)) {
			throw new RuntimeException("ProductIDs did not match");
		}
		return ps.saveExistingProperty(request,  property);
	}
	
	@GET
	@Path("plans/{productId}")
	@Produces(MediaType.APPLICATION_JSON)
	public PlanService.Plan getPlan(@Context HttpServletRequest request, @PathParam("productId") String productId) {
		return ps.getPlan(request,  productId);
	}
	
	@GET
	@Path("plans/{planProductID}/facades")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FacadeService.Facade> getPlanFacades(@Context HttpServletRequest request, @PathParam("planProductID") String planProductID) {
		return fs.getPlanFacades(request, planProductID);
	}
	
	@GET
	@Path("ranges")
	@Produces(MediaType.APPLICATION_JSON)
	public List<PlanService.HLRange> getRanges(@Context HttpServletRequest request) {
		return ps.getRanges(request);
	}
	
	@GET
	@Path("ranges/{rangeID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PlanService.HLRange getRanges(@Context HttpServletRequest request, @PathParam("rangeID") String rangeID) {
		return ps.getRange(request, rangeID);
	}
	
	@GET
	@Path("plans/{productId}/drips")
	@Produces(MediaType.APPLICATION_JSON)
	public DripCollection getPlanDrips(@Context HttpServletRequest request, @PathParam("productId") String productId, @QueryParam("Location") String location, @QueryParam("QuoteViewID") String quoteViewID) {
		//return dls.getAllDrips(request, new String[]{ productId}, new java.util.Date());
		/* range, home, plan */
		PlanService.Plan p = ps.getPlan(request,  productId);
		
		int pos = -1;
		
		List<String> list = new ArrayList<String>();
		if(StringUtils.isNotBlank(p.RangeProductID)) {
			list.add(p.RangeProductID);
			++pos;
		}
		if(StringUtils.isNotBlank(p.HomeProductID)) {
			list.add(p.HomeProductID);
			++pos;
		}
		if(StringUtils.isNotBlank(p.ProductID)) {
			list.add(p.ProductID);
			++pos;
		}
		if(StringUtils.isNotBlank(quoteViewID)) {
			list.add("");
			list.add(quoteViewID);
		}
		String[] productIds = new String[list.size()];
		int ix = 0;
		for(String s: list) {
			productIds[ix++] = s;
		}
		DripCollection dc =  dls.getAllDrips(request, productIds, new java.util.Date());
		if(StringUtils.isNotBlank(location) && StringUtils.isNotBlank(quoteViewID)) {
			PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(request, productIds[pos]);
			dls.populateDripInfo(ActionBean.getConnection(request), dc, mainProduct, location, quoteViewID, null);
		}
		return dc;
	}
	@GET
	@Path("plans/{productId}/{orderId}/drips")
	@Produces(MediaType.APPLICATION_JSON)
	public DripCollection getPlanDrips(@Context HttpServletRequest request, @PathParam("productId") String productId, @QueryParam("Location") String location, @QueryParam("QuoteViewID") String quoteViewID, @PathParam("orderId") String orderID) {
		//return dls.getAllDrips(request, new String[]{ productId}, new java.util.Date());
		/* range, home, plan */
		PlanService.Plan p = ps.getPlan(request,  productId);
		
		int pos = -1;
		
		List<String> list = new ArrayList<String>();
		if(StringUtils.isNotBlank(p.RangeProductID)) {
			list.add(p.RangeProductID);
			++pos;
		}
		if(StringUtils.isNotBlank(p.HomeProductID)) {
			list.add(p.HomeProductID);
			++pos;
		}
		if(StringUtils.isNotBlank(p.ProductID)) {
			list.add(p.ProductID);
			++pos;
		}
		if(StringUtils.isNotBlank(orderID)) {
			list.add(orderID);
		}
		if(StringUtils.isNotBlank(quoteViewID)) {
			list.add(quoteViewID);
		}
		String[] productIds = new String[list.size()];
		int ix = 0;
		for(String s: list) {
			productIds[ix++] = s;
		}
		DripCollection dc =  dls.getAllDrips(request, productIds, new java.util.Date());
		if(StringUtils.isNotBlank(location) && StringUtils.isNotBlank(quoteViewID)) {
			PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(request, productIds[pos]);
			dls.populateDripInfo(ActionBean.getConnection(request), dc, mainProduct, location, quoteViewID, null);
		}
		return dc;
	}
	
	/*
	@GET
	@Path("plans/{productId}/drips/{productLibraryID}/products")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DripProduct> getDripProducts(@Context HttpServletRequest request, @PathParam("productId") String productId, @PathParam("productLibraryID") String productLibraryID, @QueryParam("ProductID") String productID) {
		logger.trace("getDripProducts(Request, {})", productLibraryID);
		return dls.getDripProducts(request, productID, productLibraryID);
	}
	*/
	@GET
	@Path("packages/{productId}")
	@Produces(MediaType.APPLICATION_JSON)
	public PlanService.HLPackage getPackage(@Context HttpServletRequest request, @PathParam("productId") String productId) {
		return ps.getPackage(request, productId);
	}
	
	@GET
	@Path("packages/{productId}/drips")
	@Produces(MediaType.APPLICATION_JSON)
	public DripCollection getPackageDrips(@Context HttpServletRequest request, @PathParam("productId") String productId, @QueryParam("Location") String location, @QueryParam("QuoteViewID") String quoteViewID) {
		DripCollection dc = dls.getAllDrips(request, new String[]{productId, productId, productId}, new java.util.Date());
		if(StringUtils.isNotBlank(location) && StringUtils.isNotBlank(quoteViewID)) {
			PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(request,productId);
			dls.populateDripInfo(ActionBean.getConnection(request), dc, mainProduct, location, quoteViewID, null);
		}
		return dc;
	}
	@GET
	@Path("packages/{productId}/{orderId}/drips")
	@Produces(MediaType.APPLICATION_JSON)
	public DripCollection getPackageDrips(@Context HttpServletRequest request, @PathParam("productId") String productId, @QueryParam("Location") String location, @QueryParam("QuoteViewID") String quoteViewID, @PathParam("orderId") String orderID) {
		String[] pIDs = new String[StringUtils.isNotBlank(orderID)? 4 : 3];
		for (int x = 0; x < 3; ++x) pIDs[x] = productId;
		if (StringUtils.isNotBlank(orderID)) pIDs[3] = orderID;
		DripCollection dc = dls.getAllDrips(request, pIDs, new java.util.Date());
		if(StringUtils.isNotBlank(location) && StringUtils.isNotBlank(quoteViewID)) {
			PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(request,productId);
			dls.populateDripInfo(ActionBean.getConnection(request), dc, mainProduct, location, quoteViewID, null);
		}
		return dc;
	}
	
	/*
	private static class FakeDripService extends DripService {
		
		private static final Logger logger = LoggerFactory.getLogger(FakeDripService.class);
		private static final DripService service = new FakeDripService();
		public static DripService getInstance() {
			return service;
		}
		
		private static java.util.Map<String, DripLibrary> fakeLibraryMap = new java.util.HashMap<String, DripLibrary>();
		private static java.util.Map<String, DripProduct> fakeProductMap = new java.util.HashMap<String, DripProduct>();
		static {
			DripLibrary dl = new DripLibrary();
			dl.ProductLibraryID = "FAKE_PL_1";
			dl.Name = "April / May 2012"; 
			dl.Type = DripLibraryType.SINGLE;
			dl.products = new ArrayList<DripProduct>();
			
			fakeLibraryMap.put(dl.ProductLibraryID, dl);
			
			DripProduct dp = new DripProduct();
			dp.ProductID = "FAKE_D_1";
			dp.ProductLibraryID = "FAKE_PL_1";
			dp.Name = "Save 60k";
			dp.Cost = -60000;
			dp.GST = dp.Cost * 0.10;
			dp.TotalCost = dp.Cost + dp.GST;
			dp.ImagePath = "files/fake_drip_single_1.png";

			fakeProductMap.put(dp.ProductID, dp);
			
			
			dl.products.add(dp);
			
			dp = new DripProduct();
			dp.ProductID = "FAKE_D_2";
			dp.ProductLibraryID = "FAKE_PL_1";
			dp.Name = "Save 80k";
			dp.Cost = -80000;
			dp.GST = dp.Cost * 0.10;
			dp.TotalCost = dp.Cost + dp.GST;
			dp.ImagePath = "files/fake_drip_single_2.png";
			
			fakeProductMap.put(dp.ProductID, dp);
			dl.products.add(dp);
			
			dl = new DripLibrary();
			dl.ProductLibraryID = "FAKE_PL_2";
			dl.Name = "Builder Offers"; 
			dl.Type = DripLibraryType.MULTIPLE;
			dl.products = new ArrayList<DripProduct>();
			
			fakeLibraryMap.put(dl.ProductLibraryID, dl);

			dp = new DripProduct();
			dp.ProductID = "FAKE_D_3";
			dp.ProductLibraryID = "FAKE_PL_2";
			dp.Name = "State Grant";
			dp.Cost = -44000;
			dp.GST = dp.Cost * 0.10;
			dp.TotalCost = dp.Cost + dp.GST;
			dp.ImagePath = "files/fake_drip_multi_1.png";
			dl.products.add(dp);

			fakeProductMap.put(dp.ProductID, dp);
			
			dp = new DripProduct();
			dp.ProductID = "FAKE_D_4";
			dp.ProductLibraryID = "FAKE_PL_2";
			dp.Name = "Federal Grant";
			dp.Cost = -53000;
			dp.GST = dp.Cost * 0.10;
			dp.TotalCost = dp.Cost + dp.GST;
			dp.ImagePath = "files/fake_drip_multi_2.png";
			dl.products.add(dp);
			
			fakeProductMap.put(dp.ProductID, dp);
		}
		
		@Override
		public List<DripLibrary> getDripLibraries(final Connection conn, final UserBean user) {
			logger.trace("getDripLibraries(Conn, {})", user != null ? user.getName() : "null user");

			return new java.util.ArrayList<DripLibrary>(fakeLibraryMap.values());
		}
		
		@Override
		public DripLibrary getDripLibrary(final Connection conn, final UserBean user, final String productLibraryID) {
			logger.trace("getDripLibrary(Conn, {}, {})", user != null ? user.getName() : "null user", productLibraryID);
			if(user == null) {
				throw new NotAuthenticatedException("You are not logged in");
			}
			if(!user.canAccess("Products")) {
				throw new AccessException("You do not have permission to access the properties module");
			}
			
			if(fakeLibraryMap.containsKey(productLibraryID)) {
				return fakeLibraryMap.get(productLibraryID);
			}
			throw new NotFoundException("The drip library could not be found : " + productLibraryID);
		}
		
		@Override
		public List<DripProduct> getDripProducts(final Connection conn, final UserBean user, final String productID, final String productLibraryID) {
			logger.trace("getDripProducts(Conn, {}, {})", user != null ? user.getName() : "null user", productLibraryID);
			if(user == null) {
				throw new NotAuthenticatedException("You are not logged in");
			}
			if(!user.canAccess("Products")) {
				throw new AccessException("You do not have permission to access the properties module");
			}
		
			List<DripProduct> list = new ArrayList<DripProduct>();
			for(DripProduct dp: fakeProductMap.values()) {
				if(dp.ProductLibraryID.equals(productLibraryID)) {
					list.add(dp);
				}
			}
			return list;
		}
		
		@Override
		public DripProduct getDripProduct(final Connection conn, final UserBean user, final String productLibraryID, final String dripProductID) {
			if(logger.isTraceEnabled()) logger.trace("getDripProduct(Conn, {}, {}, {})", new String[]{user != null ? user.getName() : "null user", productLibraryID, dripProductID});
			if(user == null) {
				throw new NotAuthenticatedException("You are not logged in");
			}
			if(!user.canAccess("Products")) {
				throw new AccessException("You do not have permission to access the properties module");
			}
			
			if(fakeProductMap.containsKey(dripProductID)) {
				return fakeProductMap.get(dripProductID);
			}
			throw new NotFoundException("The drip library could not be found : " + dripProductID);
		}	
	}
	*/
}
