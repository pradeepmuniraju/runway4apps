package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.cms.SimpleCampaign;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.service.crm.cms.properties.FacadeService;
import com.sok.service.crm.cms.properties.PlanService.HLPackageCost;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Home")
public class HomeCheck extends SimpleHome {

	private String active, error;
	private Date createdDate, modifiedDate;
	//private Double GST = null, totalCost = null, dripPrice = null, price = null;

	//private List<ProductDetail> details = null;
	private HomeStatusHistory currentStatus = null;
	private List<PlanCheck> plans = null;
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	
	@XmlElement(name="CurrentStatus")
	public HomeStatusHistory getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(HomeStatusHistory currentStatus) {
		this.currentStatus = currentStatus; 
	}
	
	@XmlElement(name="Active")
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
	@XmlElement(name="Error")
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	@XmlElement(name="Plans")
	public List<PlanCheck> getPlans() {
		return plans;
	}
	public void setPlans(List<PlanCheck> plans) {
		this.plans = plans;
	}
}
