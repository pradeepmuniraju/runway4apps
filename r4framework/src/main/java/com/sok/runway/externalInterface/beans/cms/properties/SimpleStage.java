package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Stage")
@XmlSeeAlso(Stage.class)
@XmlType(propOrder={"stageID","name", "estate"})
public class SimpleStage {
	protected String stageID, name;
	protected SimpleEstate estate;
	
	@XmlElement(name="StageID")
	public String getStageID() {
		return stageID;
	}
	public void setStageID(String stageID) {
		this.stageID = stageID;
	}
	
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name="Estate")
	public SimpleEstate getEstate() {
		return estate;
	}
	public void setEstate(SimpleEstate estate) {
		this.estate = estate;
	}
}
