package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.GenRow;
import com.sok.runway.externalInterface.beans.SimpleDocument;

@XmlAccessorType(XmlAccessType.NONE)
@XmlSeeAlso(QuickIndex.class)
@XmlRootElement(name="QuickIndexPlans")
public class QuickIndexPlans extends QuickIndex {
	protected String rangeID, rangeName, designID, designName, planID, planName, homePlanName, rangeColorHex;
	protected String brandID, brandName, builderID, builderName;
	protected double homePrice, homeWidth, homeDepth, homeArea, homeSquares;
	protected double buildWidth, buildDepth, canFitOnFrontage, canFitOnDepth;
	protected double storeys, bedrooms, bathrooms, carparks, livingrooms;
	protected String study, garage, media;
	
	protected SimpleDocument designMainImage, designThumbImage;

	public QuickIndexPlans() {
		// TODO Auto-generated constructor stub
	}

	public QuickIndexPlans(GenRow row) {
		super(row);

		rangeID = row.getData("RangeID");
		rangeName = row.getData("RangeName");
		designID = row.getData("DesignID");
		designName = row.getData("DesignName");
		planID = row.isSet("PlanID")? row.getData("PlanID") : row.getData("ProductID");
		planName = row.getData("Name");
		homePlanName = row.getData("HomePlanName");
		
		brandID = row.getData("BuilderID");
		brandName = row.getData("BrandName");

		builderID = row.getData("BuilderProductID");
		builderName = row.getData("BuilderName");

		homePrice = row.getDouble("HomePrice");
		homeArea = row.getDouble("HomeArea");
		homeDepth = row.getDouble("HomeDepth");
		homeWidth = row.getDouble("HomeWidth");
		
		buildWidth = row.getDouble("BuildWidth");
		buildDepth = row.getDouble("BuildDepth");
		canFitOnDepth = row.getDouble("CanFitOnDepth");
		canFitOnFrontage = row.getDouble("CanFitOnWidth");

		storeys = row.getDouble("Storeys");
		bedrooms = row.getDouble("Bedrooms");
		bathrooms = row.getDouble("Bathrooms");
		carparks = row.getDouble("CarParks");
		livingrooms = row.getDouble("LivingRooms");

		study = row.getData("Study");
		garage = row.getData("Garage");
		media = row.getData("Media");
		
		homeSquares = homeArea / 9.29;
		
		rangeColorHex = row.getString("RangeColorHex");
	}

	@XmlElement(name="RangeID")
	public String getRangeID() {
		return rangeID;
	}

	public void setRangeID(String rangeID) {
		this.rangeID = rangeID;
	}

	@XmlElement(name="RangeName")
	public String getRangeName() {
		return rangeName;
	}

	public void setRangeName(String rangeName) {
		this.rangeName = rangeName;
	}

	@XmlElement(name="DesignID")
	public String getDesignID() {
		return designID;
	}

	public void setDesignID(String designID) {
		this.designID = designID;
	}

	@XmlElement(name="DesignName")
	public String getDesignName() {
		return designName;
	}

	public void setDesignName(String designName) {
		this.designName = designName;
	}

	@XmlElement(name="PlanID")
	public String getPlanID() {
		return planID;
	}

	public void setPlanID(String planID) {
		this.planID = planID;
	}

	@XmlElement(name="PlanName")
	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	@XmlElement(name="HomePlanName")
	public String getHomePlanName() {
		return homePlanName;
	}

	public void setHomePlanName(String homePlanName) {
		this.homePlanName = homePlanName;
	}

	@XmlElement(name="HomePrice")
	public double getHomePrice() {
		return homePrice;
	}

	public void setHomePrice(double homePrice) {
		this.homePrice = homePrice;
	}

	@XmlElement(name="HomeWidth")
	public double getHomeWidth() {
		return homeWidth;
	}

	public void setHomeWidth(double homeWidth) {
		this.homeWidth = homeWidth;
	}

	@XmlElement(name="HomeDepth")
	public double getHomeDepth() {
		return homeDepth;
	}

	public void setHomeDepth(double homeDepth) {
		this.homeDepth = homeDepth;
	}

	@XmlElement(name="HomeArea")
	public double getHomeArea() {
		return homeArea;
	}

	@XmlElement(name="HomeSquares")
	public double getHomeSquares() {
		return homeSquares;
	}

	public void setHomeArea(double homeArea) {
		this.homeArea = homeArea;
	}

	@XmlElement(name="BuildWidth")
	public double getBuildWidth() {
		return buildWidth;
	}

	public void setBuildWidth(double buildWidth) {
		this.buildWidth = buildWidth;
	}

	@XmlElement(name="BuildDepth")
	public double getBuildDepth() {
		return buildDepth;
	}

	public void setBuildDepth(double buildDepth) {
		this.buildDepth = buildDepth;
	}

	@XmlElement(name="CanFitOnFrontage")
	public double getCanFitOnFrontage() {
		return canFitOnFrontage;
	}

	public void setCanFitOnFrontage(double canFitOnFrontage) {
		this.canFitOnFrontage = canFitOnFrontage;
	}

	@XmlElement(name="CanFitOnDepth")
	public double getCanFitOnDepth() {
		return canFitOnDepth;
	}

	public void setCanFitOnDepth(double canFitOnDepth) {
		this.canFitOnDepth = canFitOnDepth;
	}

	@XmlElement(name="Storeys")
	public double getStoreys() {
		return storeys;
	}

	public void setStoreys(double storeys) {
		this.storeys = storeys;
	}

	@XmlElement(name="Bedrooms")
	public double getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(double bedrooms) {
		this.bedrooms = bedrooms;
	}

	@XmlElement(name="Bathrooms")
	public double getBathrooms() {
		return bathrooms;
	}

	public void setBathrooms(double bathrooms) {
		this.bathrooms = bathrooms;
	}

	@XmlElement(name="Study")
	public String getStudy() {
		return study;
	}

	public void setStudy(String study) {
		this.study = study;
	}

	@XmlElement(name="CarParks")
	public double getCarparks() {
		return carparks;
	}

	public void setCarparks(double carparks) {
		this.carparks = carparks;
	}

	@XmlElement(name="Garage")
	public String getGarage() {
		return garage;
	}

	public void setGarage(String garage) {
		this.garage = garage;
	}

	@XmlElement(name="DesignThumbnailImage")
	public SimpleDocument getDesignThumbnailImage() {
		return designThumbImage;
	}

	public void setDesignThumbnailImage(SimpleDocument designThumbImage) {
		this.designThumbImage = designThumbImage;
	}

	@XmlElement(name="DesignMainImage")
	public SimpleDocument getDesignMainImage() {
		return designMainImage;
	}

	public void setDesignMainImage(SimpleDocument designMainImage) {
		this.designMainImage = designMainImage;
	}

	@XmlElement(name="RangeColorHex")
	public String getRangeColorHex() {
		return rangeColorHex;
	}

	public void setRangeColorHex(String rangeColorHex) {
		this.rangeColorHex = rangeColorHex;
	}

	@XmlElement(name="BrandID")
	public String getBrandID() {
		return brandID;
	}

	public void setBrandID(String brandID) {
		this.brandID = brandID;
	}

	@XmlElement(name="BrandName")
	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	@XmlElement(name="BuilderID")
	public String getBuilderID() {
		return builderID;
	}

	public void setBuilderID(String builderID) {
		this.builderID = builderID;
	}

	@XmlElement(name="BuilderName")
	public String getBuilderName() {
		return builderName;
	}

	public void setBuilderName(String builderName) {
		this.builderName = builderName;
	}

	@XmlElement(name="Media")
	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	@XmlElement(name="LivingRooms")
	public double getLivingrooms() {
		return livingrooms;
	}

	public void setLivingrooms(double livingrooms) {
		this.livingrooms = livingrooms;
	}
}
