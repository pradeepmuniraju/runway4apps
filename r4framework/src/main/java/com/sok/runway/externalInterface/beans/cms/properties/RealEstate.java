package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.InitServlet;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.ProductSecurityGroup;
import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.SimplePublishing;
import com.sok.runway.externalInterface.beans.shared.PersonName;

/**
 * v2 of Existing Properties taking on the externally facing persona of 'Real Estate'
 * 
 * This will be served by the v2 REST API.
 * This representation uses standardised plan/lot/document etc objects. 
 * 
 * @author Michael Dekmetzian
 * 
 * PublishingStatusIcon is not present in this representation
 */
@XmlAccessorType(XmlAccessType.NONE)
//@XmlType(propOrder={"realEstateID","name","subType","active","currentStatus","plan","lot","address","createdBy","modifiedBy","createdDate","createdByName","modifiedDate","modifiedByName","publishHeadline","publishDescription","detailPdfUrl","marketingStatus","publishingStatus","basePrice","dripPrice","price","image","thumbnailImage","details"})
@XmlRootElement(name="RealEstate")
public class RealEstate {
	
	private String realEstateID, name, subType, createdBy, modifiedBy, publishHeadline /* summary */, publishDescription, marketingStatus, publishingStatus;  
	
	private String active; 
	private Date createdDate, modifiedDate;
	private Date availableDate, expiryDate;
	private PersonName modifiedByName = null, createdByName = null;
	private Double /* cost = null, GST = null, */ basePrice = null /* TotalCost */, dripPrice = null, /* DripCost */ price = null /* DripResult */;

	private String displayPrice = ""; 
	
	private List<ProductDetail> details = null;
	public List<ProductSecurityGroup> groups = Collections.emptyList();
	private SimpleDocument image = null, thumbnailImage = null;
	private SimplePlan plan;
	// HomeDesignName, PlanProductID
	private SimpleLot lot; 
	// LotProductID LotNumber Stage,
	
	//private List<ProductSecurityGroup> productSecurityGroups = Collections.emptyList();
	private Address address = null;

	private RealEstateStatusHistory currentStatus; 
	private SimplePublishing publishing = null;

	private String productNum;

	private String description;

	//private Contact vendor, sellingAgent; 
	
    // Not Implemented : 
	// PublishingStatusIcon,
	// Vendor VendorDisplay VendorPhone VendorEmail, VEndorType 
	// SellingAgent, SellingAgentDisplay, SellingAgentContactID, SellingAgentContact, SellingAgentPhone SellingAgentEmail
	// Developer, DeveloperCompany, 
	// Project, GroupID, UpdateByProcess;
	
	@XmlElement(name="RealEstateID")
	public String getRealEstateID() {
		return realEstateID;
	}
	public void setRealEstateID(String realEstateID) {
		this.realEstateID = realEstateID;
	}
	
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name="Active")
	public String getActive() {
		return active;
	}
	public void setActive(String string) {
		this.active = string; 
	}
	
	@XmlElement(name="SubType")
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	
	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName(){
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedByName = modifiedbyname;
	}
	
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@XmlElement(name="Address")
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}

	@XmlElement(name="Lot")
	public SimpleLot getLot() {
		return lot;
	}
	public void setLot(SimpleLot lot) {
		this.lot = lot;
	}
	@XmlElement(name="Plan")
	public SimplePlan getPlan() {
		return plan;
	}
	public void setPlan(SimplePlan plan) {
		this.plan = plan;
	}
	@XmlElement(name="ProductNum")
	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}
	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@XmlElement(name="PublishHeadline")
	public String getPublishHeadline() {
		return publishHeadline;
	}
	public void setPublishHeadline(String publishHeadline) {
		this.publishHeadline = publishHeadline;
	}
	
	@XmlElement(name="PublishDescription")
	public String getPublishDescription() {
		return publishDescription;
	}
	public void setPublishDescription(String publishDescription) {
		this.publishDescription = publishDescription;
	}
	
	@XmlElement(name="MarketingStatus")
	public String getMarketingStatus() {
		return marketingStatus;
	}
	public void setMarketingStatus(String marketingStatus) {
		this.marketingStatus = marketingStatus;
	}
	
	@XmlElement(name="PublishingStatus")
	public String getPublishingStatus() {
		return publishingStatus;
	}
	public void setPublishingStatus(String publishingStatus) {
		this.publishingStatus = publishingStatus;
	}

	@XmlElement(name="Image")
	public SimpleDocument getImage() {
		return image;
	}
	public void setImage(SimpleDocument image) {
		this.image = image;
	}
	
	@XmlElement(name="ThumbnailImage")
	public SimpleDocument getThumbnailImage() {
		return thumbnailImage;
	}
	public void setThumbnailImage(SimpleDocument thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}
	
	@XmlElementWrapper(name = "Details")
	@XmlElement(name="Detail")
	public List<ProductDetail> getDetails() {
		return details;
	}
	public void setDetails(List<ProductDetail> details) {
		this.details = details;
	}
	
	
	@XmlElementWrapper(name = "Groups")
	@XmlElement(name="Group")
	public List<ProductSecurityGroup> getGroups() {
		return groups;
	}
	public void setGroups(List<ProductSecurityGroup> groups) {
		this.groups = groups;
	}	
	
	@XmlElement(name="CurrentStatus")
	public RealEstateStatusHistory getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(RealEstateStatusHistory currentStatus) {
		this.currentStatus = currentStatus; 
	}
	/*
	@XmlElement(name="Cost")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	
	@XmlElement(name="GST")
	public Double getGST() {
		return GST;
	}
	public void setGST(Double GST) {
		this.GST = GST;
	}
	*/
	@XmlElement(name="BasePrice")
	public Double getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(Double basePrice) {
		this.basePrice = basePrice;
	}
	
	@XmlElement(name="DripPrice")
	public Double getDripPrice() {
		return dripPrice;
	}
	public void setDripPrice(Double dripPrice) {
		this.dripPrice = dripPrice;
	}
	
	@XmlElement(name="Price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	@XmlElement(name="PropertyPDFURL")
	public String getDetailPdfUrl() {
		return String.format("%1$s/property.pdf?-api=true&ProductID=%2$s&-variant=ExistingPropertyPackagePdf&RepUserID=%3$s&v=%4$d", InitServlet.getSystemParam("URLHome"), getRealEstateID(), (getGroups()!= null && getGroups().size() > 0)? ((ProductSecurityGroup)getGroups().get(0)).RepUserID : "", getModifiedDate() != null ? getModifiedDate().getTime() : new Double(Math.random()*100000).intValue());
	}
	@XmlElement(name="Publishing")
	public SimplePublishing getPublishing() {
		return publishing;
	}
	public void setPublishing(SimplePublishing publishing) {
		this.publishing = publishing;
	}
	@XmlElement(name="VisibilityStatus")
	public String getVisibilityStatus() {
		if (currentStatus == null || currentStatus.getStatusID() == null) return "Offline";
		
		if (!VisibilityStatus.checkVisibilityStatus(active, currentStatus.getStatusID(), availableDate, expiryDate)) return "Offline";
		
		if ("true".equals(InitServlet.getSystemParam("RPM-Publishing-RealEstate"))) {
			if (publishing == null) return "Offline";
			if (!VisibilityStatus.checkPublishedStatus(publishing.getPublishedFlag(), publishing.getStatus(), publishing.getStartDate(), publishing.getEndDate())) return "Offline";
		}
		
		return "Online";
	}
	
	public void setVisibilityStatus(String status) {
		
	}
	@XmlElement(name="AvailableDate")
	public Date getAavalableDate() {
		return availableDate;
	}
	public void setAavalableDate(Date avalableDate) {
		this.availableDate = avalableDate;
	}
	@XmlElement(name="ExpiryDate")
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	@XmlElement(name="DisplayPrice")
	public String getDisplayPrice() {
		return displayPrice;
	}
	public void setDisplayPrice(String displayPrice) {
		this.displayPrice = displayPrice;
	}
}
