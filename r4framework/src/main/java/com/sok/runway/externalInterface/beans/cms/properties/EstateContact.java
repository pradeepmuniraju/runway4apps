package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.runway.externalInterface.beans.shared.PersonName;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="EstateContact")
@XmlType(propOrder={"estateContactID","estateID","contactID", "linkType", "createdDate", "createdBy","createdByName","modifiedDate", "modifiedBy","modifiedByName" })
public class EstateContact {

	private String estateContactID, estateID, contactID, linkType, createdBy, modifiedBy;
	private Date createdDate, modifiedDate; 
	private PersonName createdByName = null, modifiedByName = null;
	private Boolean defaultLink = null;
	@XmlElement(name="EstateContactID")
	public String getEstateContactID() {
		return estateContactID;
	}
	public void setEstateContactID(String estateContactID) {
		this.estateContactID = estateContactID;
	}
	@XmlElement(name="EstateID")
	public String getEstateID() {
		return estateID;
	}
	public void setEstateID(String estateID) {
		this.estateID = estateID;
	}
	@XmlElement(name="ContactID")
	public String getContactID() {
		return contactID;
	}
	public void setContactID(String contactID) {
		this.contactID = contactID;
	}
	@XmlElement(name="LinkType")
	public String getLinkType() {
		return linkType;
	}
	public void setLinkType(String linkType) {
		this.linkType = linkType;
	} 
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName() {
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedByName) {
		this.modifiedByName = modifiedByName;
	}
	@XmlElement(name="DefaultLink")
	public boolean isDefaultLink() {
		return (defaultLink != null && defaultLink.booleanValue()); 
	}
	public void setDefaultLink(Boolean defaultLink) {
		this.defaultLink = (defaultLink != null && defaultLink.booleanValue());
	}
}
