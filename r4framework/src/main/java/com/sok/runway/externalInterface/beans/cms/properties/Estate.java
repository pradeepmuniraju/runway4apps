package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.PersonName;

import com.sok.framework.GenRow;
import com.wordnik.swagger.annotations.ApiClass;

import java.util.Date;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Estate")
@XmlType(propOrder={"publishName","productNum", "description", "URL","address","location","thumbnailImage", "interactiveMap","createdBy","createdByName","createdDate","modifiedBy","modifiedByName","modifiedDate","error","productID"})
@ApiClass(description="Deprecated: ProductID")
public class Estate extends SimpleEstate {
	private String productNum, description, url, createdBy, modifiedBy, publishName;
	private Address address;
	private SimpleDocument thumbnailImage;
	private SimpleLocation location;
	private InteractiveMap interactiveMap;

	private Date createdDate, modifiedDate;
	private String error; 
	private PersonName modifiedByName = null, createdByName = null;
	
	public Estate() {}
	public Estate(String error) {
		this.error = error;
	}
	public Estate(GenRow g) {
		this.estateID = g.getString("ProductID");
		this.name = g.getString("Name");
	}

	@Deprecated
	@XmlElement(name="ProductID")
	//@JsonProperty("ProductID")
	public String getProductID() {
		return estateID;
	}
	@Deprecated
	public void setProductID(String productID) {
		this.estateID = productID;
	}
	
	//@JsonProperty("ProductNum")
	@XmlElement(name="ProductNum")
	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}
	//@JsonProperty("Description")
	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	//@JsonProperty("URL")
	@XmlElement(name="URL")
	public String getURL() {
		return url;
	}
	public void setURL(String uRL) {
		this.url = uRL;
	}
	//@JsonProperty("CreatedBy")
	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	//@JsonProperty("ModifiedBy")
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	//@JsonProperty("ModifiedByName")
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName(){
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedByName = modifiedbyname;
	}
	
	//@JsonProperty("CreatedByName")
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	
	//@JsonProperty("CreatedDate")
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	//@JsonProperty("ModifiedDate")
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	//@JsonProperty("Address")
	@XmlElement(name="Address")
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	//@JsonProperty("ThumbnailImage")
	@XmlElement(name="ThumbnailImage")
	public SimpleDocument getThumbnailImage() {
		return thumbnailImage;
	}
	public void setThumbnailImage(SimpleDocument thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}
	
	//@JsonProperty("Error")
	@XmlElement(name="Error")
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	@XmlElement(name="Location")
	public SimpleLocation getLocation() {
		return location;
	}
	public void setLocation(SimpleLocation location) {
		this.location = location;
	}
	
	@XmlElement(name="PublishName")
	public String getPublishName() {
		return publishName;
	}
	
	public void setPublishName(String publishName) {
		this.publishName = publishName;
	}
	
	@XmlElement(name="InteractiveMap")
	public InteractiveMap getInteractiveMap() {
		return interactiveMap;
	}
	public void setInteractiveMap(InteractiveMap interactiveMap) {
		this.interactiveMap = interactiveMap;
	}
}
