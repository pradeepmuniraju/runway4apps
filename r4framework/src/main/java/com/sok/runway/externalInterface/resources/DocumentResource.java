package com.sok.runway.externalInterface.resources;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.oreilly.servlet.Up


import com.sok.framework.InitServlet;
import com.sok.framework.UploadWrapper;
import com.sok.runway.externalInterface.beans.Document;
import com.sok.service.crm.DocumentService;

@Path("/documents")
public class DocumentResource {

	private static final DocumentResource resource = new DocumentResource();
	private static final UserResource us = UserResource.getInstance();
	private static final DocumentService ds = DocumentService.getInstance();
	private static final Logger logger = LoggerFactory.getLogger(DocumentResource.class);
	public static DocumentResource getInstance() {
		return resource;
	}
	
	private static final String[] imagetypes = {".xls",	".xlsx", ".jpg", ".png", ".gif", ".pdf", ".ppt", ".txt", ".doc", ".docx", ".rtf", ".zip"};

	private static final String[] doctype = {"Excel", "Excel", "Image",	"Image", "Image", "PDF", "PowerPoint", "Text", "Word Doc", "Word Doc", "Word Doc", "Zip"};

	private static final String[] subtype = {"", "", "", "", "", "Download PDF", "", "", "", "", "", ""};
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Document> getDocuments(@Context HttpServletRequest request, @QueryParam("Type") String type, @QueryParam("SubType") String subType, @QueryParam("search") String search) {
		if(logger.isDebugEnabled()) logger.debug("getDocuments(Request, {}, {}, {})", new String[]{ type, subType, search});
		return ds.getDocuments(request,  type, subType, search);
	}
	
	/**
	 * Upload a document or documents, return an array which contains those documents with default details.
	 * @param request
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadDocuments(@Context HttpServletRequest request) {
	   	UploadWrapper multi = null;
	   	if (!(request instanceof UploadWrapper)) {
	   		if(request.getAttribute("MultipartWrapper")!=null) {
	   			multi = (UploadWrapper)request.getAttribute("MultipartWrapper");
	   		}
	   	}
	   	else {
	   		multi = (UploadWrapper) request;
	   	}
	   	if(multi == null) 
	   		throw new IllegalArgumentException("No documents submitted or end point not configured for multi-part submission");
	   	
	   	String documentType = StringUtils.trimToNull(request.getParameter("DocumentType"));
	   	String documentSubType = StringUtils.trimToNull(request.getParameter("DocumentSubType"));
	   	
	   	@SuppressWarnings("rawtypes")
		Enumeration files = multi.getFileNames();
	   	List<Document> list = new ArrayList<Document>();
   		while (files.hasMoreElements()) {   			
   		
   			String name = (String)files.nextElement();
   			String filename = multi.getFilesystemName(name);
   			   			
   		   	if (StringUtils.isBlank(documentType)) {
   		   		for (int i = 0; i < imagetypes.length; ++i) {
   		   			if (StringUtils.isNotBlank(filename) && filename.toLowerCase().endsWith(imagetypes[i])) {
   		   				documentType = doctype[i];
   		   				if (StringUtils.isBlank(documentSubType)) documentSubType = subtype[i];
   		   				break;
   		   			}
   		   		}
   		   	} if ("pdf".equalsIgnoreCase(documentType)) {
   		   		documentSubType = "Download PDF";
   		   	}
   		   	
   			File f = multi.getFile(name);
   			if (f == null) {
   				// try the real file path
   				f = new File(InitServlet.getRealPath(InitServlet.getSystemParam("uploadDir","files/incoming")) + filename);
   				logger.warn("Could not get file from multi.getFile with name {}, fsexist={}", filename, f.exists());
   			}
   			if(f != null) {
   				Document d = new Document();
   				d.setDocumentType(documentType);
   				d.setDocumentSubType(documentSubType);
   				d.setDocumentStatus("Pending");
   				list.add(ds.uploadDocument(request, f, d));
   			}
   		}
   		
   		String acc = StringUtils.trimToEmpty(request.getHeader("Accept")); 
   		if(acc.startsWith(MediaType.TEXT_HTML)) {
   			ObjectMapper om = new ObjectMapper();
   		     AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
   	         //make deserializer use JAXB annotations (only)
   		     om.setDeserializationConfig(om.getDeserializationConfig().withAnnotationIntrospector(introspector));
   		 	 om.setSerializationConfig(om.getSerializationConfig().withAnnotationIntrospector(introspector));
   			try {
   				return Response.status(Response.Status.OK).type(MediaType.TEXT_HTML).entity(om.writeValueAsString(list)).build();
   			} catch (Exception e) {
   				throw new RuntimeException(e);
   			}
   		} else {
   			return Response.status(Response.Status.OK).type(MediaType.APPLICATION_JSON).entity(list).build();
   		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{documentID}")
	public Document getDocument(@Context HttpServletRequest request, @PathParam("documentID") String documentID) {
		logger.debug("getDocument(request, {})", documentID);
		return ds.getDocument(request, documentID);
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{documentID}")
	public void deleteDocument(@Context HttpServletRequest request, @PathParam("documentID") String documentID) {
		logger.debug("deleteDocument(request, {})", documentID);
		ds.deleteDocument(request, documentID);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{documentID}")
	public Document updateDocument(@Context HttpServletRequest request, @PathParam("documentID") String documentID, Document document) {
		logger.debug("updateDocument(request, {}, {})", documentID, document != null ? document.getDocumentID() : "null document");
		if(document == null) {
			throw new IllegalArgumentException("Document was not supplied for update");
		}
		if(!StringUtils.equals(documentID, document.getDocumentID())) {
			throw new IllegalArgumentException(documentID + "Path DocumentID did not match Document" + document.getDocumentID());
		}
		return ds.updateDocument(request, document);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("super/{documentID}")
	public Document updateSuperDocument(@Context HttpServletRequest request, @PathParam("documentID") String documentID, Document document) {
		logger.debug("updateDocument(request, {}, {})", documentID, document != null ? document.getDocumentID() : "null document");
		if(document == null) {
			throw new IllegalArgumentException("Document was not supplied for update");
		}
		if(!StringUtils.equals(documentID, document.getDocumentID())) {
			throw new IllegalArgumentException(documentID + "Path DocumentID did not match Document" + document.getDocumentID());
		}
		return ds.updateDocument(request, document);
	}
	
	/**
	 * If the document is a pdf, this method should produce and return an array of paths where image screenshots 
	 * may be found. 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{documentID}/pages")
	public Map<Integer, String> getDocumentPages(@Context HttpServletRequest request, @PathParam("documentID") String documentID, 
			@QueryParam("start") @DefaultValue("1") int start, @QueryParam("end") @DefaultValue("1") int end, @QueryParam("password") @DefaultValue("") String password) {
		return ds.getDocumentPages(request, documentID, start, end, password);
	}
	/**
	 * If the document is a pdf, this method should produce and return an array of paths where image screenshots 
	 * may be found. 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("super/{documentID}/pages")
	public Map<Integer, String> getSuperDocumentPages(@Context HttpServletRequest request, @PathParam("documentID") String documentID, 
			@QueryParam("start") @DefaultValue("1") int start, @QueryParam("end") @DefaultValue("1") int end, @QueryParam("password") @DefaultValue("") String password) {
		return ds.getDocumentPages(request, documentID, start, end, password);
	}
}
