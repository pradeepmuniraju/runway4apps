package com.sok.runway.externalInterface.beans.shared;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author mike
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class OpportunityProfile extends Profile
{
	private OpportunityQuestion[] questions = null;
	
	@JsonProperty("Questions")
	public OpportunityQuestion[] getQuestions()
	{
		return(questions);
	}	
	
	@JsonProperty("Questions")
	public void setQuestions(OpportunityQuestion[] questions)
	{
		this.questions = questions;
	}	
	
	@Override
	public String toString() {
		return String.format("OpportunityProfile(%1$s)", this.getSurveyID());
	}
}