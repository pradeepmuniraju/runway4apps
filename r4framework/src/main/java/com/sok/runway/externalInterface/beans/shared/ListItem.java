package com.sok.runway.externalInterface.beans.shared;

import com.sok.runway.externalInterface.beans.*;
import java.util.*;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class ListItem
{
   private String itemid = null;
   private String listid = null;
   private String itemlabel = null;
   private String itemvalue = null;
   private String sortnumber = null;
   private String quantifier = null;
   private String displayField = null;
   private String displayName = null;
   
   public String getItemID()
   {
      return(itemid);
   }  
   
   public void setItemID(String itemid)
   {
      this.itemid = itemid;
   }  
   
   public String getListID()
   {
      return(listid);
   }  
   
   public void setListID(String listid)
   {
      this.listid = listid;
   }  
   
   @JsonProperty("Label")
   public String getItemLabel()
   {
      return(itemlabel);
   }  
   
   @JsonProperty("Label")
   public void setItemLabel(String itemlabel)
   {
      this.itemlabel = itemlabel;
   }  
   
   @JsonProperty("Value")
   public String getItemValue()
   {
      return(itemvalue);
   }  
   
   @JsonProperty("Value")
   public void setItemValue(String itemvalue)
   {
      this.itemvalue = itemvalue;
   }  
   
   @JsonProperty("SortNumber")
   public String getSortNumber()
   {
      return(sortnumber);
   }  
   
   @JsonProperty("SortNumber")
   public void setSortNumber(String sortnumber)
   {
      this.sortnumber = sortnumber;
   }  
   
   public String getQuantifier()
   {
      return(quantifier);
   }  
   
   public void setQuantifier(String quantifier)
   {
      this.quantifier = quantifier;
   }
	
   @JsonProperty("DisplayField")
	public String getDisplayField() {
		return displayField;
	}
   
   @JsonProperty("DisplayField")
	public void setDisplayField(String displayField) {
		this.displayField = displayField;
	}  
   
   @JsonProperty("DisplayName")
	public String getDisplayName() {
		return displayName;
	}
  
  @JsonProperty("DisplayName")
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}  
}
