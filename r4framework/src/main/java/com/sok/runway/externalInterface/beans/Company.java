package com.sok.runway.externalInterface.beans;

import java.util.*;

import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.runway.externalInterface.beans.shared.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class Company
{
	private String companyid = null;
	private String company = null;
	private String department = null;

	private String phone = null;
	private String fax = null;
	private String web = null;
	private String email = null;
	private Date createddate = null;
	private Date modifieddate = null;
	private String modifiedby = null;
	private String repuserid = null;
	private String mgruserid = null;
	private String source = null;
	private String createdby = null;
	private String type = null;
	private String industry = null;
	private Date qualifieddate = null;
	private String qualifiedby = null;
	private Date followupdate = null;

	private String phone2 = null;
	private String notes = null;
	private String groupid = null;
	private String companystatusid = null;
	private String wholesaler = null;
	private String accountno = null;
	private String legalcompanyname = null;
	private String abn = null;
	
	private Address physicaladdress = null;
	private Address postaladdress = null;	
	private Status currentstatus = null;
	
	private String error = null;
	
	public String getError()
	{
		return(error);
	}	
	
	public void setError(String error)
	{
		this.error = error;
	}	
	@JsonProperty("CompanyID")
	public String getCompanyID()
	{
		return(companyid);
	}	
	
	@JsonProperty("CompanyID")
	public void setCompanyID(String companyid)
	{
		this.companyid = companyid;
	}	
	
	@JsonProperty("Company")
	public String getCompany()
	{
		return(company);
	}	
	
	@JsonProperty("Company")
	public void setCompany(String company)
	{
		this.company = company;
	}	
	
	@JsonProperty("Department")
	public String getDepartment()
	{
		return(department);
	}	
	
	@JsonProperty("Department")
	public void setDepartment(String department)
	{
		this.department = department;
	}	
	
	@JsonProperty("Phone")
	public String getPhone()
	{
		return(phone);
	}	
	
	@JsonProperty("Phone")
	public void setPhone(String phone)
	{
		this.phone = phone;
	}	
	
	@JsonProperty("Fax")
	public String getFax()
	{
		return(fax);
	}	
	
	@JsonProperty("Fax")
	public void setFax(String fax)
	{
		this.fax = fax;
	}	
	
	@JsonProperty("Web")
	public String getWeb()
	{
		return(web);
	}	
	
	@JsonProperty("Web")
	public void setWeb(String web)
	{
		this.web = web;
	}	
	
	@JsonProperty("Email")
	public String getEmail()
	{
		return(email);
	}	
	
	@JsonProperty("Email")
	public void setEmail(String email)
	{
		this.email = email;
	}	
	
	public Date getCreatedDate()
	{
		return(createddate);
	}	
	
	public void setCreatedDate(Date createddate)
	{
		this.createddate = createddate;
	}	
	
	public Date getModifiedDate()
	{
		return(modifieddate);
	}	
	
	public void setModifiedDate(Date modifieddate)
	{
		this.modifieddate = modifieddate;
	}	
	
	public String getModifiedBy()
	{
		return(modifiedby);
	}	
	
	public void setModifiedBy(String modifiedby)
	{
		this.modifiedby = modifiedby;
	}	
	
	public String getRepUserID()
	{
		return(repuserid);
	}	
	
	public void setRepUserID(String repuserid)
	{
		this.repuserid = repuserid;
	}	
	
	public String getMgrUserID()
	{
		return(mgruserid);
	}	
	
	public void setMgrUserID(String mgruserid)
	{
		this.mgruserid = mgruserid;
	}	
	
	public String getSource()
	{
		return(source);
	}	
	
	public void setSource(String source)
	{
		this.source = source;
	}	
	
	public String getCreatedBy()
	{
		return(createdby);
	}	
	
	public void setCreatedBy(String createdby)
	{
		this.createdby = createdby;
	}	
	
	public String getType()
	{
		return(type);
	}	
	
	public void setType(String type)
	{
		this.type = type;
	}	
	
	public String getIndustry()
	{
		return(industry);
	}	
	
	public void setIndustry(String industry)
	{
		this.industry = industry;
	}	
	
	public Date getQualifiedDate()
	{
		return(qualifieddate);
	}	
	
	public void setQualifiedDate(Date qualifieddate)
	{
		this.qualifieddate = qualifieddate;
	}	
	
	public String getQualifiedBy()
	{
		return(qualifiedby);
	}	
	
	public void setQualifiedBy(String qualifiedby)
	{
		this.qualifiedby = qualifiedby;
	}	
	
	public Date getFollowUpDate()
	{
		return(followupdate);
	}	
	
	public void setFollowUpDate(Date followupdate)
	{
		this.followupdate = followupdate;
	}	
	
	@JsonProperty("Phone2")
	public String getPhone2()
	{
		return(phone2);
	}	
	
	@JsonProperty("Phone2")
	public void setPhone2(String phone2)
	{
		this.phone2 = phone2;
	}	
	
	public String getNotes()
	{
		return(notes);
	}	
	
	public void setNotes(String notes)
	{
		this.notes = notes;
	}	
	
	public String getGroupID()
	{
		return(groupid);
	}	
	
	public void setGroupID(String groupid)
	{
		this.groupid = groupid;
	}	
	
	public String getCompanyStatusID()
	{
		return(companystatusid);
	}	
	
	public void setCompanyStatusID(String companystatusid)
	{
		this.companystatusid = companystatusid;
	}	
	
	public String getWholesaler()
	{
		return(wholesaler);
	}	
	
	public void setWholesaler(String wholesaler)
	{
		this.wholesaler = wholesaler;
	}	
	
	public String getAccountNo()
	{
		return(accountno);
	}	
	
	public void setAccountNo(String accountno)
	{
		this.accountno = accountno;
	}	
	
	public String getLegalCompanyName()
	{
		return(legalcompanyname);
	}	
	
	public void setLegalCompanyName(String legalcompanyname)
	{
		this.legalcompanyname = legalcompanyname;
	}	
	
	public String getABN()
	{
		return(abn);
	}	
	
	public void setABN(String abn)
	{
		this.abn = abn;
	}	

	@JsonProperty("PhysicalAddress")
	public Address getPhysicalAddress()
	{
		return(physicaladdress);
	}
	
	@JsonProperty("PhysicalAddress")
	public void setPhysicalAddress(Address address)
	{
		physicaladdress = address;
	}
	
	@JsonProperty("PostalAddress")
	public Address getPostalAddress()
	{
		return(postaladdress);
	}
	
	@JsonProperty("PostalAddress")
	public void setPostalAddress(Address address)
	{
		postaladdress = address;
	}	
	
	public Status getCurrentStatus()
	{
		return(currentstatus);
	}
	
	public void setCurrentStatus(Status status)
	{
		currentstatus = status;
	}	
	
	private PersonName modifiedbyname = null;
	private PersonName createdbyname = null;
	private PersonName qualifiedbyname = null;	
	private PersonName repname = null;	
	private PersonName mgrname = null;	
	
	public PersonName getModifiedByName()
	{
		return(modifiedbyname);
	}
	
	public void setModifiedByName(PersonName modifiedbyname)
	{
		this.modifiedbyname = modifiedbyname;
	}
	
	public PersonName getCreatedByName()
	{
		return(createdbyname);
	}
	
	public void setCreatedByName(PersonName createdbyname)
	{
		this.createdbyname = createdbyname;
	}	
	
	public PersonName getQualifiedByName()
	{
		return(qualifiedbyname);
	}
	
	public void setQualifiedByName(PersonName qualifiedbyname)
	{
		this.qualifiedbyname = qualifiedbyname;
	}	
	
	public PersonName getRepName()
	{
		return(repname);
	}
	
	public void setRepName(PersonName repname)
	{
		this.repname = repname;
	}
	
	public PersonName getMgrName()
	{
		return(mgrname);
	}
	
	public void setMgrName(PersonName mgrname)
	{
		this.mgrname = mgrname;
	}		
}

