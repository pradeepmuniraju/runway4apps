package com.sok.runway.externalInterface;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.*;
import com.sok.runway.*;

public class ExternalSessionManager extends SessionManager 
{
	private static final Logger logger = LoggerFactory.getLogger(ExternalSessionManager.class);
	public static final String externalSessionManager = "externalSessionManager";
	
	protected void removeDuplicateUsers(HttpSession session, UserBean user) {
		   UserSession us = null;

		   for(int i=0; i<sessions.size(); i++) {
		      us = (UserSession)sessions.get(i);
		      
		      //Precautionary checks
		      if (us != null && us.user != null && us.session != null) {
		         //The real checking done here
		         if (us.user.getString(UserBean.sessionkey).equals(user.getString(UserBean.sessionkey))) {
		        	 sessions.remove(us);
		         }
		      }
		   }

	}
	
	protected void storeInServletContext(HttpSession session)
	{
		logger.warn("storeInServletContext(session) no longer in use");
		/* 
			context = session.getServletContext();
			context.setAttribute(externalSessionManager,this);
		*/
	}
	
	public UserBean getUserBeanBySessionKey(String keystring)
	{
		UserBean bean = null;
		if(keystring!=null && keystring.length()!=0)
		{
			for(int i=0; i<sessions.size(); i++)
			{
				bean = getUserBean(i);
				if(bean.getString(UserBean.sessionkey).equals(keystring))
				{
					return(bean);
				}
			}
		}
		return(null);
	}		
	
	public static void setUser(HttpServletRequest req, ServletContext context, UserBean user)
	{
   		ExternalSessionManager exSessionManager = (ExternalSessionManager)context.getAttribute(ExternalSessionManager.externalSessionManager);
		user.put(UserBean.sessionkey, KeyMaker.generate());
		// by their very nature, external users won't have a session as they typically do not maintain JSESSIONID cookies.
		// previously, this code would use a session that was created by axis, however this doesn't make sense in a stateless environment. 
   		exSessionManager.setUser(new com.sok.framework.servlet.HttpSession(context), user);
	}
	
	public static UserBean getUserBean(ServletContext context, String keystring)
	{
   		ExternalSessionManager exSessionManager = (ExternalSessionManager)context.getAttribute(ExternalSessionManager.externalSessionManager);
   		return(exSessionManager.getUserBeanBySessionKey(keystring));
	}
	
	public static String transferSession(HttpServletRequest req, ServletContext context, String sessionid)
	{
	   //logError("transferSession "+sessionid);
		SessionManager sessionManager = (SessionManager)context.getAttribute(SessionManager.sessionManager);
		if(sessionManager!=null)
		{
			UserBean user = sessionManager.getUserBeanBySessionID(sessionid);
			if(user!=null)
			{
				ExternalSessionManager.setUser(req, context, user);
				return(user.getString(UserBean.sessionkey));
			}
		}
		return(null);
	}
	
	@Override
	public void contextInitialized(ServletContextEvent sctxe) {
		logger.info("context Initialized");
		//set up connections to external resources, if used?		
		context = sctxe.getServletContext();
		context.setAttribute(externalSessionManager,this);
	}
}