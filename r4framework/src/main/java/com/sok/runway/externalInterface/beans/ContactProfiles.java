package com.sok.runway.externalInterface.beans;

import java.util.*;
import com.sok.runway.externalInterface.beans.shared.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class ContactProfiles
{
	private ContactProfile[] profiles = null;
	
	private String contactid = null;
	
	private String error = null;
	
	public String getError()
	{
		return(error);
	}	
	
	public void setError(String error)
	{
		this.error = error;
	}
	
	public String getContactID()
	{
		return(contactid);
	}	
	
	public void setContactID(String contactid)
	{
		this.contactid = contactid;
	}		
	
	public ContactProfile[] getProfiles()
	{
		return(profiles);
	}	
	
	public void setProfiles(ContactProfile[] profiles)
	{
		this.profiles = profiles;
	}
}