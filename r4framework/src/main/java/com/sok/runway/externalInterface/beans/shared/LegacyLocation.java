package com.sok.runway.externalInterface.beans.shared;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "LegacyLocation")
//@XmlType(propOrder={"locationID","name","description","type","parent"})
public class LegacyLocation {
	private String locationID, name, description, type; 
	private LegacyLocation parent;
	
	
	@XmlElement(name="LocationID")
	public String getLocationID() {
		return locationID;
	}
	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@XmlElement(name="Type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@XmlElement(name="Parent")
	public LegacyLocation getParent() {
		return parent;
	}
	public void setParent(LegacyLocation parent) {
		this.parent = parent;
	} 
}
