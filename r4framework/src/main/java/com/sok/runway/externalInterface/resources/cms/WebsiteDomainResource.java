package com.sok.runway.externalInterface.resources.cms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.beans.cms.CMSDomain;
import com.sok.service.cms.WebsiteService;

public class WebsiteDomainResource {
	
	private static final Logger logger = LoggerFactory.getLogger(WebsiteDomainResource.class);
	private static final WebsiteService websiteService = WebsiteService.getInstance();
	
	@Context HttpServletRequest request; 
	private final String cmsID;
	public WebsiteDomainResource(String cmsID, HttpServletRequest request) {
		this.cmsID = cmsID;
		this.request = request; 
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CMSDomain> getCMSDomains() {
		logger.debug("getCMSDomains(request, {}, {})", cmsID);
		return websiteService.getCMSDomains(request, cmsID);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CMSDomain createCMSDomain(CMSDomain domain) {
		logger.debug("createCMSDomain(request, {}, {})", cmsID, domain.getDomain());
		if(cmsID == null || !cmsID.equals(domain.getCMSID())) {
			throw new RuntimeException("CMSID provided " + cmsID + " did not match object " + domain.getCMSID());
		}
		return websiteService.createCMSDomain(request, domain);
	}
	
	@DELETE
	@Path("{CMSDomainID}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteCMSDomain(@PathParam("CMSDomainID") String CMSDomainID) {
		logger.debug("deleteCMSDomain(request, {}, {})", cmsID, CMSDomainID);
		websiteService.deleteCMSDomain(request, cmsID, CMSDomainID);
	}
}
