package com.sok.runway.externalInterface.beans.shared;
import java.util.*;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class FieldSet
{
   private String error = null;
   private Field[] fields = null;   
   
   public String getError()
   {
      return(error);
   }  
   
   public void setError(String error)
   {
      this.error = error;
   }     
   
   public Field[] getFields()
   {
      return(fields);
   }  
   
   public void setFields(Field[] fields)
   {
      this.fields = fields;
   }    
}