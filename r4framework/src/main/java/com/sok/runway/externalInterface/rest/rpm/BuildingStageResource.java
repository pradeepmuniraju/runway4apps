package com.sok.runway.externalInterface.rest.rpm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.beans.cms.properties.BuildingStage;
import com.sok.service.crm.cms.properties.BuildingService;
import com.sok.service.crm.cms.properties.EstateService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

@Path("/buildingstages")
@Api(value = "/buildingstages", description = "Retrieve BuildingStage Information")
@Produces({ MediaType.APPLICATION_JSON,  MediaType.TEXT_XML })
public class BuildingStageResource {

	static final Logger logger = LoggerFactory.getLogger(BuildingStageResource.class);
	@Context HttpServletRequest request;
	
	private String buildingID = null;
	public BuildingStageResource() {}
	public BuildingStageResource(HttpServletRequest request) { this.request = request; }
	public BuildingStageResource(HttpServletRequest request, String buildingID) { 
		this(request);
		this.buildingID = buildingID;
	}
	
	@GET
	@ApiOperation(value = "List all stages", notes="Optionally filtering by modified date", responseClass="com.sok.runway.externalInterface.beans.cms.properties.BuildingStage", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "BuildingStages")
	public List<BuildingStage> getBuildingStages(
			@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = false) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active stages", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			logger.debug("getBuildingStages({})", modifiedSince, modifiedSince);
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		return BuildingService.getInstance().getBuildingStages(request, buildingID, mod, active);
	}
	
	@GET
	@Path("/{stageID}")
	@ApiOperation(value = "Return a particular stage", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.BuildingStage")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public BuildingStage getBuildingStage(
			@ApiParam(value = "The BuildingStageID value as returned from the API", required = true) @PathParam("stageID") final String stageID) {
		
		return BuildingService.getInstance().getBuildingStage(request, stageID);
	}
	
	@ApiResource(value = "/{stageID}/apartments", resourceClass="com.sok.runway.externalInterface.rest.rpm.ApartmentResource")
	@Path("{stageID}/apartments")
	public ApartmentResource getApartmentResource(@PathParam("stageID") final String stageID) {
		return new ApartmentResource(request, buildingID, stageID);
	}
	
	@GET
	@ApiResource(value = "/{stageID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the BuildingStage", notes="<p>This call will return a list of Albums and their Images for a given BuildingStage. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/{stageID}/albums")
	public AlbumResource getAlbumResource(@PathParam("stageID") final String stageID, 
			@ApiParam(value = "Document Sub Type", required = false) @QueryParam("DocumentSubType") String subType, 
			@ApiParam(value = "Album Type", required = false) @QueryParam("AlbumType") String albumType, 
			@ApiParam(value = "Album Publsihed for", allowableValues="Website", required=false, defaultValue="") @DefaultValue("") @QueryParam("Publish") String publish) {
		return new AlbumResource(request,  stageID, albumType, publish, subType);
	}
	
}
