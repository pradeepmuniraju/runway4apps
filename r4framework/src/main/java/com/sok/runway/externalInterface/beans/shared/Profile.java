package com.sok.runway.externalInterface.beans.shared;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
public abstract class Profile
{
	private String surveyid = null;
	private String name = null;
	private String description = null;
	private String preamble = null;
	private String disclaimer = null;
	private String allopportunities = null;
	private String companytype = null;
	private String contacttype = null;
	private String disabled = null;
	private Date createddate = null;
	private String createdby = null;
	private Date modifieddate = null;
	private String modifiedby = null;
	private String displayincompanyview = null;
	private String displayincontactview = null;
	private String displayinopportunityview = null;
	private String displayincompanysummary = null;
	private String editincontactdetailedlist = null;
	private String productprofile = null;
	private String orderprofile = null;
	private String competitorprofile = null;
	private String notetype = null;
	private String noteprofile = null;
	private String itemlistprofile = null;
	private String checklistprofile = null;
	private String relatedsurveyid = null;
	private String groupid = null;
	
	@JsonProperty("SurveyID")
	public String getSurveyID()
	{
		return(surveyid);
	}	
	
	@JsonProperty("SurveyID")
	public void setSurveyID(String surveyid)
	{
		this.surveyid = surveyid;
	}	
	
	@JsonProperty("Name")
	public String getName()
	{
		return(name);
	}	
	
	@JsonProperty("Name")
	public void setName(String name)
	{
		this.name = name;
	}	
	
	@JsonProperty("Description")
	public String getDescription()
	{
		return(description);
	}	
	
	@JsonProperty("Description")
	public void setDescription(String description)
	{
		this.description = description;
	}	
	
	public String getPreamble()
	{
		return(preamble);
	}	
	
	public void setPreamble(String preamble)
	{
		this.preamble = preamble;
	}	
	
	public String getDisclaimer()
	{
		return(disclaimer);
	}	
	
	public void setDisclaimer(String disclaimer)
	{
		this.disclaimer = disclaimer;
	}	
	
	public String getAllOpportunities()
	{
		return(allopportunities);
	}	
	
	public void setAllOpportunities(String allopportunities)
	{
		this.allopportunities = allopportunities;
	}	
	
	public String getCompanyType()
	{
		return(companytype);
	}	
	
	public void setCompanyType(String companytype)
	{
		this.companytype = companytype;
	}	
	
	public String getContactType()
	{
		return(contacttype);
	}	
	
	public void setContactType(String contacttype)
	{
		this.contacttype = contacttype;
	}	
	
	@JsonIgnore
	public String getDisabled()
	{
		return(disabled);
	}	
	
	@JsonIgnore
	public void setDisabled(String disabled)
	{
		this.disabled = disabled;
	}	
	
	@JsonProperty("Disabled")
	public void setDisabled(boolean disabled) {
		this.disabled = disabled ? "Y" : "N";
	}
	
	@JsonProperty("Disabled")
	public boolean isDisabled() {
		return "Y".equals(this.disabled);
	}
	
	public Date getCreatedDate()
	{
		return(createddate);
	}	
	
	public void setCreatedDate(Date createddate)
	{
		this.createddate = createddate;
	}	
	
	public String getCreatedBy()
	{
		return(createdby);
	}	
	
	public void setCreatedBy(String createdby)
	{
		this.createdby = createdby;
	}	
	
	public Date getModifiedDate()
	{
		return(modifieddate);
	}	
	
	public void setModifiedDate(Date modifieddate)
	{
		this.modifieddate = modifieddate;
	}	
	
	public String getModifiedBy()
	{
		return(modifiedby);
	}	
	
	public void setModifiedBy(String modifiedby)
	{
		this.modifiedby = modifiedby;
	}	
	
	public String getDisplayInCompanyView()
	{
		return(displayincompanyview);
	}	
	
	public void setDisplayInCompanyView(String displayincompanyview)
	{
		this.displayincompanyview = displayincompanyview;
	}	
	
	public String getDisplayInContactView()
	{
		return(displayincontactview);
	}	
	
	public void setDisplayInContactView(String displayincontactview)
	{
		this.displayincontactview = displayincontactview;
	}	
	
	public String getDisplayInOpportunityView()
	{
		return(displayinopportunityview);
	}	
	
	public void setDisplayInOpportunityView(String displayinopportunityview)
	{
		this.displayinopportunityview = displayinopportunityview;
	}	
	
	public String getDisplayInCompanySummary()
	{
		return(displayincompanysummary);
	}	
	
	public void setDisplayInCompanySummary(String displayincompanysummary)
	{
		this.displayincompanysummary = displayincompanysummary;
	}	
	
	public String getEditInContactDetailedList()
	{
		return(editincontactdetailedlist);
	}	
	
	public void setEditInContactDetailedList(String editincontactdetailedlist)
	{
		this.editincontactdetailedlist = editincontactdetailedlist;
	}	
	
	public String getProductProfile()
	{
		return(productprofile);
	}	
	
	public void setProductProfile(String productprofile)
	{
		this.productprofile = productprofile;
	}	
	
	public String getOrderProfile()
	{
		return(orderprofile);
	}	
	
	public void setOrderProfile(String orderprofile)
	{
		this.orderprofile = orderprofile;
	}	
	
	public String getCompetitorProfile()
	{
		return(competitorprofile);
	}	
	
	public void setCompetitorProfile(String competitorprofile)
	{
		this.competitorprofile = competitorprofile;
	}	
	
	public String getNoteType()
	{
		return(notetype);
	}	
	
	public void setNoteType(String notetype)
	{
		this.notetype = notetype;
	}	
	
	public String getNoteProfile()
	{
		return(noteprofile);
	}	
	
	public void setNoteProfile(String noteprofile)
	{
		this.noteprofile = noteprofile;
	}	
	
	public String getItemListProfile()
	{
		return(itemlistprofile);
	}	
	
	public void setItemListProfile(String itemlistprofile)
	{
		this.itemlistprofile = itemlistprofile;
	}	
	
	public String getCheckListProfile()
	{
		return(checklistprofile);
	}	
	
	public void setCheckListProfile(String checklistprofile)
	{
		this.checklistprofile = checklistprofile;
	}	
	
	public String getRelatedSurveyID()
	{
		return(relatedsurveyid);
	}	
	
	public void setRelatedSurveyID(String relatedsurveyid)
	{
		this.relatedsurveyid = relatedsurveyid;
	}	
	
	public String getGroupID()
	{
		return(groupid);
	}	
	
	public void setGroupID(String groupid)
	{
		this.groupid = groupid;
	}	
	
	private PersonName modifiedbyname = null;
	private PersonName createdbyname = null;
	
	public PersonName getModifiedByName()
	{
		return(modifiedbyname);
	}
	
	public void setModifiedByName(PersonName modifiedbyname)
	{
		this.modifiedbyname = modifiedbyname;
	}
	
	public PersonName getCreatedByName()
	{
		return(createdbyname);
	}
	
	public void setCreatedByName(PersonName createdbyname)
	{
		this.createdbyname = createdbyname;
	}	
	
}
