package com.sok.runway.externalInterface.beans.cms.properties.publishing;
/*
 *	Transmissions would also go in this package, in case you were wondering. 
 */

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.GenRow;
import com.sok.framework.generation.IllegalConfigurationException;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.shared.PersonName;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="PublishingRecord")
//@XmlType(propOrder={"publishingID","recordID","recordType","publishMethod", "publishStatus", "startDate","endDate"
//		,"createdBy","createdByName","createdDate","modifiedBy","modifiedByName","modifiedDate"})
public class PublishingRecord {

	private PublishMethod publishMethod;
	private Date startDate; 
	private Date endDate;
	private String publishingID, recordID, recordType, publishStatus, createdBy, modifiedBy;
	private PersonName modifiedByName = null, createdByName = null;
	private Date createdDate, modifiedDate;
	
	public PublishingRecord() {}
	public PublishingRecord(GenRow data) {
		if(!("House and Land".equals(data.getData("ProductType")) || "Existing Property".equals(data.getData("ProductType")))) {
			throw new UnsupportedOperationException("Unsupported Type found : " + data.getData("ProductType"));
		}
		setRecordID(data.getData("ProductID"));
		setRecordType(data.getData("ProductType"));
		setPublishingID(data.getData("ProductPublishingID"));
		setPublishMethod(data.getData("PublishMethod"));
		setPublishStatus(data.getData("PublishStatus"));
		try { 
			setStartDate(data.getDate("StartDate"));
		} catch (IllegalConfigurationException ice) {} 
		try { 
			setEndDate(data.getDate("EndDate"));
		} catch (IllegalConfigurationException ice) {}
		
		try { 
			setCreatedDate(data.getDate("CreatedDate"));
		} catch (IllegalConfigurationException ice) {} 
		try { 
			setModifiedDate(data.getDate("ModifiedDate"));
		} catch (IllegalConfigurationException ice) {}
		
		setCreatedBy(data.getData("CreatedBy"));
	    setCreatedByName(BeanFactory.getPersonName(data.getData("OwnFirstName"), data.getData("OwnLastName")));
	    setModifiedBy(data.getData("ModifiedBy"));
	    setModifiedByName(BeanFactory.getPersonName(data.getData("ModFirstName"), data.getData("ModLastName")));
	}
	
	@XmlElement(name="PublishingID")
	public String getPublishingID() {
		return publishingID;
	}
	public void setPublishingID(String publishingID) {
		this.publishingID = publishingID;
	}
	
	@XmlElement(name="RecordID")
	public String getRecordID() {
		return recordID;
	}
	public void setRecordID(String recordID) {
		this.recordID = recordID;
	}
	@XmlElement(name="RecordType")
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	
	@XmlElement(name="PublishMethod")
	public String getPublishMethod() {
		return publishMethod.name();
	}
	public void setPublishMethod(String publishMethod) {
		this.publishMethod = PublishMethod.valueOf(publishMethod);
	}
	public void setPublishMethod(PublishMethod publishMethod) {
		this.publishMethod = publishMethod;
	}
	
	@XmlElement(name="PublishStatus")
	public String getPublishStatus() {
		return publishStatus;
	}
	public void setPublishStatus(String publishStatus) {
		this.publishStatus = publishStatus;
	}

	@XmlElement(name="StartDate")
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@XmlElement(name="EndDate")
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName(){
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedByName = modifiedbyname;
	}
	
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}
