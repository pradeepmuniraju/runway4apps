package com.sok.runway.externalInterface;
import javax.servlet.*;
import javax.servlet.http.*;

import com.sok.runway.*;

import org.apache.axis.transport.http.HTTPConstants;
import org.apache.axis.MessageContext;

public class AxisUtil
{
	public static final String SESSION_ERROR = "Session error, invalid session key";
	public static final String ID_ERROR = "Error, record ID not specified";	
	
	public static ServletContext getServletContext()
	{
		MessageContext mcontext = MessageContext.getCurrentContext(); 
		ServletContext scontext = ((HttpServlet)mcontext.getProperty(HTTPConstants.MC_HTTP_SERVLET)).getServletContext();
		return(scontext);
	}
	
	public static HttpServletRequest getHttpServletRequest()
	{
	    MessageContext context = MessageContext.getCurrentContext(); 
	    HttpServletRequest req = (HttpServletRequest) context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST); 
		return(req);
	}

	public static UserBean getUserBean(String sessionkey)
	{
		return(ExternalSessionManager.getUserBean(getServletContext(), sessionkey));
	}

	public static void setUser(UserBean user)
	{
		ExternalSessionManager.setUser(getHttpServletRequest(), getServletContext(), user);
	}
}