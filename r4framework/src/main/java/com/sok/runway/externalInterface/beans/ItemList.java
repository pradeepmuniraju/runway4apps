package com.sok.runway.externalInterface.beans;

import java.util.*;

import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.runway.externalInterface.beans.shared.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class ItemList
{
   private String listid = null;
   private String parentlistid = null;
   private String listname = null;
   private String grouplevel = null;
   private String listtype = null;
   private String defaultvalue = null;
   private String displayField = null;
   private String displayName = null;
   
   private ListItem[] listitems = null;
   
   private String error = null;        
   
   public String getError()
   {
      return(error);
   }  
   
   public void setError(String error)
   {
      this.error = error;
   }     
   
   public String getListID()
   {
      return(listid);
   }  
   
   public void setListID(String listid)
   {
      this.listid = listid;
   }  
   
   public String getParentListID()
   {
      return(parentlistid);
   }  
   
   public void setParentListID(String parentlistid)
   {
      this.parentlistid = parentlistid;
   }  
   
   public String getListName()
   {
      return(listname);
   }  
   
   public void setListName(String listname)
   {
      this.listname = listname;
   }  
   
   public String getGroupLevel()
   {
      return(grouplevel);
   }  
   
   public void setGroupLevel(String grouplevel)
   {
      this.grouplevel = grouplevel;
   }  
   
   public String getListType()
   {
      return(listtype);
   }  
   
   public void setListType(String listtype)
   {
      this.listtype = listtype;
   }  
   
   @JsonProperty("DefaultValue")
   public String getDefaultValue()
   {
      return(defaultvalue);
   }  
   
   @JsonProperty("DefaultValue")
   public void setDefaultValue(String defaultvalue)
   {
      this.defaultvalue = defaultvalue;
   }  

   @JsonProperty("ListItems")
   public ListItem[] getListItems()
   {
      return(listitems);
   }
   
   @JsonProperty("ListItems")
   public void setListItems(ListItem[] listitems)
   {
      this.listitems = listitems;
   }
   
   @JsonProperty("DisplayField")
	public String getDisplayField() {
		return displayField;
	}
   
   @JsonProperty("DisplayField")
	public void setDisplayField(String displayField) {
		this.displayField = displayField;
	}   
   
   @JsonProperty("DisplayName")
	public String getDisplayName() {
		return displayName;
	}
   
   @JsonProperty("DisplayName")
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}   


}
