package com.sok.runway.externalInterface.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.shared.Activity;
import com.sok.service.crm.UserService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/activites")
@Api(value = "/activites", description = "Retrieve Record Activity Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class ActivityResource {
	
	static final Logger logger = LoggerFactory.getLogger(ActivityResource.class);
	@Context HttpServletRequest request;
	
	@GET
	@ApiOperation(value = "Retrieve Activity", notes="Up to a maximum of 500 records", responseClass="com.sok.runway.externalInterface.beans.shared.Activity", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<Activity> getActivities(
			@ApiParam(value = "The type of record that you are interested in", allowableValues="Estate,Stage,Lot,House and Land,Real Estate", required = true) @QueryParam("RecordType") String recordType, 
			@ApiParam(value = "The type of action to return", allowableValues="Inserted,Updated,Deleted", required = true) @QueryParam("ActionType") String actionType,
			@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss]", required = true) @QueryParam("DateSince") String dateSince) {
		
		if(logger.isDebugEnabled()) logger.debug("getActivity(recordType={},actionType={},dateSince={}", new String[]{recordType, actionType, dateSince});
		UserBean user = UserService.getInstance().getCurrentUser(request);
		if(user == null) {
			throw new NotAuthenticatedException("You are not authenticated");
		}
	
		boolean realEstate = "Real Estate".equals(recordType);  
		
		final GenRow activityList = new GenRow();
		activityList.setViewSpec("ActivityView");
		
		// sort of a special case, this feature got renamed and is published externally as real estate to ease confusion.
		if(realEstate) {
			activityList.setParameter("RecordType","Existing Property+Real Estate");
		} else if(StringUtils.isNotBlank(recordType)) {
			activityList.setParameter("RecordType",recordType);
		}
		if(StringUtils.isNotBlank(actionType)) {
			activityList.setParameter("ActionType",actionType);
		}
		activityList.setParameter("-sort0","CreatedDate");
		activityList.setParameter("-order0","DESC");
		activityList.setTop(500);
	    
		Date mod = null;
		if(StringUtils.isNotBlank(dateSince)) {
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(dateSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(dateSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("dateSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		if(mod != null) { 
			activityList.setParameter("CreatedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(mod));
		}
		
	    try {
	    	activityList.setRequest(request);
	    	
	    	if(logger.isTraceEnabled()) {
	    		activityList.doAction(GenerationKeys.SEARCH);
	    		logger.trace(activityList.getStatement());
	    	}
	    	
	    	activityList.getResults(true);
			
			if(activityList.getNext()) {
				List<Activity> list = new java.util.ArrayList<Activity>(activityList.getSize());
				do {
					Activity a = new Activity();
					a.setActivityID(activityList.getData("ActivityID"));
					a.setRecordID(activityList.getData("RecordID"));
					a.setRecordType(realEstate ? "Real Estate" : activityList.getData("RecordType"));
					a.setActionType(activityList.getData("ActionType"));
					a.setCreatedBy(activityList.getData("UserID"));
					try { 
					a.setCreatedDate(activityList.getDate("CreatedDate"));
					} catch (com.sok.framework.generation.IllegalConfigurationException ice) {} 
					a.setCreatedByName(BeanFactory.getPersonName(activityList.getData("FirstName"), activityList.getData("LastName")));
					list.add(a);
				}while(activityList.getNext());
				return list;
			}
			return Collections.emptyList();
		} finally {
			activityList.close();
		}	
	}
}
