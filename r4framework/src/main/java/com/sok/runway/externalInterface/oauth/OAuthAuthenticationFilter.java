package com.sok.runway.externalInterface.oauth;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import org.apache.amber.oauth2.common.OAuth;
import org.apache.amber.oauth2.common.exception.OAuthProblemException;
import org.apache.amber.oauth2.common.exception.OAuthSystemException;
import org.apache.amber.oauth2.rs.request.OAuthAccessResourceRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;
import com.sok.service.crm.UserService;

import com.wordnik.swagger.jaxrs.ApiAuthorizationFilter;
import com.wordnik.swagger.jaxrs.JaxrsApiReader;

import java.security.*;

import org.bouncycastle.jce.provider.*; 
import org.apache.commons.codec.binary.*;

public class OAuthAuthenticationFilter implements Filter, ApiAuthorizationFilter{

	private static final Logger logger = LoggerFactory.getLogger(OAuthAuthenticationFilter.class);
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		logger.debug("oauth filter");
		boolean oauth = false;
		if(req instanceof HttpServletRequest) {
			HttpServletRequest hreq = (HttpServletRequest)req;
			if(hreq.isSecure() || "127.0.0.1".equals(req.getRemoteAddr())) {
				HttpServletResponse res = (HttpServletResponse)resp;
				logger.debug("adding access control headers");
			    res.addHeader("Access-Control-Allow-Origin", "*");
			    res.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
			    //res.addHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
			    // some external client can not send in mixed cases, this needs to fix it
				if(StringUtils.isNotBlank(hreq.getHeader(OAuth.HeaderType.AUTHORIZATION))) {
					oauth = true;
					try {
						OAuthAccessResourceRequest oauthRequest = new OAuthAccessResourceRequest((HttpServletRequest)req);
						if(!StringUtils.containsAny(oauthRequest.getAccessToken(), new char[]{'_','%'})) {
							logger.debug("OAuth authentication being used");
							GenRow token = new GenRow();
							token.setRequest((HttpServletRequest)req);
							token.setViewSpec("oauth/OAuthTokenView");
							token.setParameter("ExpiryDate",">NOW");
							token.setParameter("AccessToken", oauthRequest.getAccessToken());
							token.doAction(GenerationKeys.SELECTFIRST);
							if(token.isSuccessful()) {
								logger.debug("OAuth found {} ", oauthRequest.getAccessToken());
								UserBean user = UserService.getInstance().getSimpleUser(token.getConnection(), token.getData("UserID"), true);
								user.setParameter("APIAccess", "RO");
								hreq.setAttribute(SessionKeys.CURRENT_USER, user);
								logger.debug("OAuth token found for user {}", user.getUserID());
							} else {
								logger.debug("IPLockedKey authentication being used");
								GenRow app = new GenRow();
								app.setViewSpec("oauth/ClientApplicationView");
								app.setParameter("IPLockedKey", oauthRequest.getAccessToken());
								app.doAction("selectfirst");
								
								if (app.isSet("ClientID")) {
									logger.debug("IPLockedKey found {} ", oauthRequest.getAccessToken());
									Security.addProvider(new BouncyCastleProvider());

									try {
										MessageDigest mda = MessageDigest.getInstance("SHA-512", "BC");
										String[] parts = req.getRemoteHost().split("\\.");
										logger.debug("IPLockedKey for ip {}", req.getRemoteHost());
										if (parts.length == 4) {
											logger.debug("IPLockedKey for standard ip {}", req.getRemoteHost());
											for (int x = 3; x >= 0; --x) {
												String key = app.getString("ClientID") + "-" + app.getString("ClientSecret") + "-" + 
														parts[0] + "." + parts[1] + "." + parts[2] + "." + parts[3];

												byte[] digesta = mda.digest(key.getBytes());
												
												char[] hex = Hex.encodeHex(digesta);
												if ((new String(hex)).equalsIgnoreCase(oauthRequest.getAccessToken())) {
													UserBean user = UserService.getInstance().getSimpleUser(token.getConnection(), app.getData("ClientID"), true);
													user.setParameter("APIAccess", app.getString("Access"));
													hreq.setAttribute(SessionKeys.CURRENT_USER, user);
													logger.debug("IPLockedKey matches ip {}", (parts[0] + "." + parts[1] + "." + parts[2] + "." + parts[3]));
													logger.debug("IPLockedKey found for user {}", user.getUserID());
													break;
												}
												parts[x] = "0";
											}
										} else {
											logger.debug("IPLockedKey for non-standard ip {}", req.getRemoteHost());
											String key = app.getString("ClientID") + "-" + app.getString("ClientSecret") + "-" + req.getRemoteHost();
											
											byte[] digesta = mda.digest(key.getBytes());
											
											char[] hex = Hex.encodeHex(digesta);
											if ((new String(hex)).equalsIgnoreCase(oauthRequest.getAccessToken())) {
												UserBean user = UserService.getInstance().getSimpleUser(token.getConnection(), app.getData("ClientID"), true);
												user.setParameter("APIAccess", app.getString("Access"));
												hreq.setAttribute(SessionKeys.CURRENT_USER, user);
												logger.debug("IPLockedKey matches ip {}", req.getRemoteHost());
												logger.debug("IPLockedKey found for user {}", user.getUserID());
											}
										}
									} catch (NoSuchAlgorithmException e) {
									} catch (NoSuchProviderException e) {
									}
									
								}
								// TODO add in the new client_id-client_secret-IP checking
								
								/*
								 * requires new API user licenses
								 * SHA-256 has checking
								 * IP can be IP or class A, B or C subnets
								 * 
								 */
							}
						} 
					} catch (OAuthSystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (OAuthProblemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} 
			}
		}
		try
	    {
		 chain.doFilter(req, resp);
	    }
	    finally 
	    {
	    	if(oauth && resp instanceof HttpServletResponse) {
	    		((HttpServletResponse)resp).setHeader("Set-Cookie", null);
	    	}
	    }
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean authorize(String arg0, String arg1, HttpHeaders arg2, UriInfo arg3) {
		//we're going to assume that we can return everything to the api docs.
		return true;
	}

	@Override
	public boolean authorizeResource(String arg0, HttpHeaders arg1, UriInfo arg2) {
		//we're going to assume that we can return everything to the api docs.
		return true;
	}
}
