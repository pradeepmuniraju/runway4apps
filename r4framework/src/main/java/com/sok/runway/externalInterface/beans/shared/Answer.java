package com.sok.runway.externalInterface.beans.shared;
import java.util.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.annotate.JsonProperty;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
public abstract class Answer
{
	private String answerid = null;
	private String answer = null;
	private Date answerdate = null;
	private String answernumber = null;
	private String answertext = null;
	private String quantifier = null;
//	private Date createddate = null;
//	private String createdby = null;
	private Date modifieddate = null;
	private String modifiedby = null;

	@JsonProperty("AnswerID")
	public String getAnswerID()
	{
		return(answerid);
	}	
	
	@JsonProperty("AnswerID")
	public void setAnswerID(String answerid)
	{
		this.answerid = answerid;
	}	
	
	@JsonProperty("Answer")
	public String getAnswer()
	{
		return(answer);
	}	
	
	@JsonProperty("Answer")
	public void setAnswer(String answer)
	{
		this.answer = answer;
	}	
	
	@JsonProperty("AnswerDate")
	public Date getAnswerDate()
	{
		return(answerdate);
	}	
	
	@JsonProperty("AnswerDate")
	public void setAnswerDate(Date answerdate)
	{
		this.answerdate = answerdate;
	}		
	
	@JsonProperty("AnswerNumber")
	public String getAnswerNumber()
	{
		return(answernumber);
	}	

	@JsonProperty("AnswerNumber")
	public void setAnswerNumber(String answernumber)
	{
		this.answernumber = answernumber;
	}	
	
	@JsonProperty("AnswerText")
	public String getAnswerText()
	{
		return(answertext);
	}	
	
	@JsonProperty("AnswerText")
	public void setAnswerText(String answertext)
	{
		this.answertext = answertext;
	}	
	
	public String getQuantifier()
	{
		return(quantifier);
	}	
	
	public void setQuantifier(String quantifier)
	{
		this.quantifier = quantifier;
	}	
/*
	public Date getCreatedDate()
	{
		return(createddate);
	}	
	
	public void setCreatedDate(Date createddate)
	{
		this.createddate = createddate;
	}	
	
	public String getCreatedBy()
	{
		return(createdby);
	}	
	
	public void setCreatedBy(String createdby)
	{
		this.createdby = createdby;
	}	
*/	
	public Date getModifiedDate()
	{
		return(modifieddate);
	}	
	
	public void setModifiedDate(Date modifieddate)
	{
		this.modifieddate = modifieddate;
	}	
	
	public String getModifiedBy()
	{
		return(modifiedby);
	}	
	
	public void setModifiedBy(String modifiedby)
	{
		this.modifiedby = modifiedby;
	}	
	
	private PersonName modifiedbyname = null;

	public PersonName getModifiedByName()
	{
		return(modifiedbyname);
	}
	
	public void setModifiedByName(PersonName modifiedbyname)
	{
		this.modifiedbyname = modifiedbyname;
	}
	
}
