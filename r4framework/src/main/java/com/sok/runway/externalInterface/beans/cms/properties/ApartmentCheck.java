package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.InitServlet;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.SimplePublishing;


/**
 * v2 of the Lot external bean. 
 * 
 * Rather than maintaining extra fields with backwards compatibility in the one object and forcing updates on xml services, we're using a new object. 
 *
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="ApartmentCheck")
//@XmlType(propOrder={"productNum", "description","address","locationID", "location", "regionID", "region", "currentStatus","details", "titleMonth", "titleYear", "GST", "totalCost","createdBy","createdByName","createdDate","modifiedBy","modifiedByName","modifiedDate","campaign","error"})
//@ApiClass(description="Deprecated: ProductID, EstateProductID, StageProductID")
public class ApartmentCheck extends SimpleApartment {
	
	private Date createdDate, modifiedDate;
	private Date availableDate, expiryDate;
	private String error; 
	private LotStatusHistory currentStatus = null;
	private String active;
	private SimplePublishing publishing = null;
	
	public ApartmentCheck() {}
	public ApartmentCheck(com.sok.runway.crm.cms.properties.Apartment data) {
		this.setApartmentID(data.getData("ProductID"));
		this.setName(data.getString("Name"));
		this.setActive(data.getString("Active"));
		
		this.setCreatedDate(data.getDate("CreatedDate"));
		this.setModifiedDate(data.getDate("ModifiedDate"));
		
		LotStatusHistory s = new LotStatusHistory();
		s.setStatus(data.getString("CurrentStatus"));
		s.setStatusID(data.getString("CurrentStatusID"));
		this.setCurrentStatus(s);
		
		this.setAavalableDate(data.getDate("AvailableDate"));
		this.setExpiryDate(data.getDate("ExpiryDate"));
		
		this.setPublishing(BeanFactory.loadPublishing(data.getData("ProductID")));
	}
	
	
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@XmlElement(name="Error")
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	@XmlElement(name="CurrentStatus")
	public LotStatusHistory getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(LotStatusHistory currentStatus) {
		this.currentStatus = currentStatus; 
	}
	
	@XmlElement(name="Active")
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	@XmlElement(name="Publishing")
	public SimplePublishing getPublishing() {
		return publishing;
	}
	public void setPublishing(SimplePublishing publishing) {
		this.publishing = publishing;
	}
	@XmlElement(name="VisibilityStatus")
	public String getVisibilityStatus() {
		if (currentStatus == null || currentStatus.getStatusID() == null) return "Offline";
		
		if (!VisibilityStatus.checkVisibilityStatus(active, currentStatus.getStatusID(), availableDate, expiryDate)) return "Offline";
		
		if ("true".equals(InitServlet.getSystemParam("RPM-Publishing-Apartments"))) {
			if (publishing == null) return "Offline";
			if (!VisibilityStatus.checkPublishedStatus(publishing.getPublishedFlag(), publishing.getStatus(), publishing.getStartDate(), publishing.getEndDate())) return "Offline";
		}
		
		return "Online";
	}
	
	public void setVisibilityStatus(String status) {
		
	}
	@XmlElement(name="AvailableDate")
	public Date getAavalableDate() {
		return availableDate;
	}
	public void setAavalableDate(Date avalableDate) {
		this.availableDate = avalableDate;
	}
	@XmlElement(name="ExpiryDate")
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
}