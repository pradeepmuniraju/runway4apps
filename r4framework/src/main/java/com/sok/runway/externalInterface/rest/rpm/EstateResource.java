package com.sok.runway.externalInterface.rest.rpm;

import java.sql.SQLIntegrityConstraintViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;	
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.DatabaseException;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.URLFilter;
import com.sok.runway.UserBean;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.crm.activity.ActivityLogger.ActionType;
import com.sok.runway.crm.cms.properties.entities.EstateEntity;
import com.sok.runway.externalInterface.beans.cms.properties.Estate;
import com.sok.runway.externalInterface.beans.cms.properties.EstateContact;
import com.sok.runway.externalInterface.beans.shared.PersonName;
//import com.sok.runway.externalInterface.resources.cms.properties.LotResource;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.EstateService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

/**
 * Extend and annotate the existing EstateResource implementation for use with the V2 (Public/Documented) API.
 * 
 * This is seperate from the v1 api endpoint to limit the library requirements on those apps which do not yet use the 
 * v2 api. 
 * 
 * @author Michael Dekmetzian
 * @date 2 January 2013
 */
@Path("/estates")
@Api(value = "/estates", description = "Retrieve Estate Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class EstateResource /* extends com.sok.runway.externalInterface.resources.cms.properties.EstateResource */ {

	static final Logger logger = LoggerFactory.getLogger(EstateResource.class);
	@Context HttpServletRequest request;
	
	public EstateResource() {}
	public EstateResource(HttpServletRequest request) { this.request = request; }
	
	@GET
	@ApiOperation(value = "List all estates", notes="Optionally filtering by modified date", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Estate", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "Estates")
	public List<Estate> getEstates(
			@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = false) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active estates", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		URLFilter.exceptionLogger(this, request, null);
		
		if(StringUtils.isNotBlank(modifiedSince)) {
			logger.debug("getEstates({})", modifiedSince, modifiedSince);
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		return EstateService.getInstance().getEstates(request, mod, active);
	}
	
	@GET
	@Path("/{estateID}")
	@ApiOperation(value = "Return a particular estate", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Estate")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public Estate getEstate(
			@ApiParam(value = "The EstateID value as returned from the API", required = true) @PathParam("estateID") final String estateID) {
		
		return EstateService.getInstance().getEstate(request, estateID);
	}
	
	@POST
	@Path("/{estateID}/contacts")
	@ApiOperation(value = "Add an estate contact", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.EstateContact")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public EstateContact addEstateContact(@ApiParam(value = "The EstateID value as returned from the API", required = true) @PathParam("estateID") final String estateID, EstateContact estateContact) {
		if(!StringUtils.equals(estateContact.getEstateID(), estateID)) {
			throw new IllegalArgumentException("EstateIDs must match"); 
		}
		EstateEntity ee = new EstateEntity(request); 
		ee.load(estateID); 
		if(!ee.isLoaded()) {
			throw new NotFoundException("The Estate you requested could not be found: " + estateID);
		}
		if(!ee.canEdit()) {
			throw new AccessException("You do not have permission to edit the estate with id " + estateID);
		}
		if(StringUtils.isBlank(estateContact.getContactID())) {
			throw new IllegalArgumentException("ContactID must be provided");
		}
		if(StringUtils.isBlank(estateContact.getLinkType())) {
			throw new IllegalArgumentException("LinkType must be provided");
		}
		GenRow ec = new GenRow(); 
		ec.setRequest(request); 
		ec.setViewSpec("properties/EstateContactView"); 
		ec.setParameter("EstateID", estateContact.getEstateID()); 
		ec.setParameter("ContactID", estateContact.getContactID());
		ec.setParameter("LinkType", estateContact.getLinkType()); 
		ec.doAction(com.sok.framework.generation.GenerationKeys.SELECTFIRST);
		
		UserBean user = UserService.getInstance().getCurrentUser(request); 
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!ec.isSuccessful()) {
			ec.setToNewID("EstateContactID"); 
			ec.setParameter("CreatedDate","NOW"); 
			ec.setParameter("CreatedBy", user.getUserID());
			ec.setParameter("ModifiedDate","NOW"); 
			ec.setParameter("ModifiedBy", user.getUserID());
			ec.setParameter("DefaultLink", estateContact.isDefaultLink()?"Y":"N");
			try { 
				ec.doAction(com.sok.framework.generation.GenerationKeys.INSERT);
				ActivityLogger.getActivityLogger().logActivity(ee, ActionType.Updated);
				ec.doAction(com.sok.framework.generation.GenerationKeys.SELECT); // do this to set the CreatedBy / Date fields to actual values
			} catch (DatabaseException de) {
				Throwable c = de.getCause(); 
				if(c != null && c instanceof SQLIntegrityConstraintViolationException) {
					ec.clear(); 
					ec.setParameter("EstateID", estateContact.getEstateID()); 
					ec.setParameter("ContactID", estateContact.getContactID());
					ec.setParameter("LinkType", estateContact.getLinkType()); 
					ec.doAction(com.sok.framework.generation.GenerationKeys.SELECTFIRST);
					if(!ec.isSuccessful()) {
						// if not successful the problem was something else
						throw de; 
					}
				} else {
					throw de; 
				}
			}
		}
		estateContact.setEstateContactID(ec.getData("EstateContactID"));
		estateContact.setCreatedBy(ec.getData("CreatedBy"));
		estateContact.setCreatedDate(ec.getDate("CreatedDate"));
		estateContact.setCreatedByName(new PersonName(ec.getData("OwnFirstName"), ec.getData("OwnLastName")));
		
		estateContact.setModifiedBy(ec.getData("ModifiedBy"));
		estateContact.setModifiedDate(ec.getDate("ModifiedDate"));
		estateContact.setModifiedByName(new PersonName(ec.getData("OwnFirstName"), ec.getData("OwnLastName")));
		return estateContact; 
	}
	
	@PUT
	@Path("/{estateID}/contacts/{estateContactID}")
	@ApiOperation(value = "Updates an estate contact", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.EstateContact")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public EstateContact updateEstateContact(@ApiParam(value = "The EstateID value as returned from the API", required = true) @PathParam("estateID") final String estateID, 
											 @ApiParam(value = "The EstateContactID value as returned from the API", required = true) @PathParam("estateContactID") final String estateContactID,
											 EstateContact estateContact) {
		if(StringUtils.isBlank(estateID) || !StringUtils.equals(estateContact.getEstateID(), estateID)) {
			throw new IllegalArgumentException("EstateIDs must match"); 
		}
		if(StringUtils.isBlank(estateContactID) || !StringUtils.equals(estateContact.getEstateContactID(), estateContactID)) {
			throw new IllegalArgumentException("EstateContactIDs must match"); 
		}
		EstateEntity ee = new EstateEntity(request); 
		ee.load(estateID); 
		if(!ee.isLoaded()) {
			throw new NotFoundException("The Estate you requested could not be found: " + estateID);
		}
		if(!ee.canEdit()) {
			throw new AccessException("You do not have permission to edit the estate with id " + estateID);
		}
		if(StringUtils.isBlank(estateContact.getContactID())) {
			throw new IllegalArgumentException("ContactID must be provided");
		}
		if(StringUtils.isBlank(estateContact.getLinkType())) {
			throw new IllegalArgumentException("LinkType must be provided");
		}
		UserBean user = UserService.getInstance().getCurrentUser(request); 
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		
		GenRow ec = new GenRow(); 
		ec.setRequest(request); 
		ec.setViewSpec("properties/EstateContactView"); 
		ec.setParameter("EstateContactID", estateContact.getEstateContactID()); 
		ec.doAction(GenerationKeys.SELECT);
		
		if(!ec.isSuccessful()) {
			throw new NotFoundException("The estate contact was not found with id " + estateContact.getEstateContactID());
		}		
		if(!StringUtils.equals(estateContact.getContactID(), ec.getData("ContactID"))) {
			throw new UnsupportedOperationException("Changing of Contacts on existing links are not supported. You should remove and re-add the link with the appropriate contact to ensure all actions are logged correctly.");
		}
		
		ec.setParameter("ModifiedDate","NOW"); 
		ec.setParameter("ModifiedBy", user.getUserID());
		ec.setParameter("DefaultLink", estateContact.isDefaultLink()?"Y":"N");
		ec.setParameter("LinkType", estateContact.getLinkType()); 
		
		if(estateContact.isDefaultLink()) {
			GenRow up = new GenRow(); 
			up.setTableSpec("properties/EstateContacts"); 
			up.setRequest(request); 
			up.setParameter("ON-EstateID", estateContact.getEstateID());
			up.setParameter("DefaultLink", "N");
			up.doAction(GenerationKeys.UPDATEALL);
		}
		
		ec.doAction(GenerationKeys.UPDATE); 
		if(!ec.isSuccessful()) {
			throw new RuntimeException("An unexpected error has occurred " + ec.getError());
		}
		
		estateContact.setEstateContactID(ec.getData("EstateContactID"));
		estateContact.setCreatedBy(ec.getData("CreatedBy"));
		estateContact.setCreatedDate(ec.getDate("CreatedDate"));
		estateContact.setCreatedByName(new PersonName(ec.getData("OwnFirstName"), ec.getData("OwnLastName")));
		
		estateContact.setModifiedBy(ec.getData("ModifiedBy"));
		estateContact.setModifiedDate(ec.getDate("ModifiedDate"));
		estateContact.setModifiedByName(new PersonName(ec.getData("ModFirstName"), ec.getData("ModLastName")));
		
		return estateContact; 
	}
	
	@DELETE
	@ApiOperation(value = "Deletes an estate contact")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid estate id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted to modify this estate"), @ApiError(code = 404, reason = "Estate Not Found") })
	@Path("/{estateID}/contacts/{estateContactID}")
	public void deleteEstateContact(@ApiParam(value = "The EstateID value as returned from the API", required = true) @PathParam("estateID") final String estateID,
			@ApiParam(value = "The EstateContactID value as returned from the API", required = true) @PathParam("estateContactID") final String estateContactID) {
		EstateEntity ee = new EstateEntity(request); 
		ee.load(estateID); 
		if(!ee.isLoaded()) {
			throw new NotFoundException("The Estate you requested could not be found: " + estateID);
		}
		if(!ee.canEdit()) {
			throw new AccessException("You do not have permission to edit the estate with id " + estateID);
		}
		if(StringUtils.isBlank(estateContactID)) {
			throw new IllegalArgumentException("EstateContactID must be provided");
		}
		GenRow ec = new GenRow(); 
		ec.setRequest(request); 
		ec.setViewSpec("properties/EstateContactView"); 
		ec.setParameter("EstateContactID", estateContactID);
		ec.doAction(com.sok.framework.generation.GenerationKeys.SELECT);
		
		if(ec.isSuccessful()) {
			if(!StringUtils.equals(ec.getData("EstateID"), estateID)) {
				throw new IllegalArgumentException("EstateIDs must match"); 
			}
			ec.doAction(GenerationKeys.DELETE);
			ActivityLogger.getActivityLogger().logActivity(ee, ActionType.Updated);
		}
	}
	
	@ApiResource(value = "/{estateID}/stages", resourceClass="com.sok.runway.externalInterface.rest.rpm.StageResource")
	@Path("/{estateID}/stages")
	public StageResource getStageResource(
			@ApiParam(value = "The EstateID value as returned from the API", required = true) @PathParam("estateID") final String estateID) {
		return new StageResource(request, estateID);
	}
	
	@ApiResource(value = "/{estateID}/lots", resourceClass="com.sok.runway.externalInterface.rest.rpm.LotResource")
	@Path("{estateID}/lots")
	public LotResource getLotResource(@PathParam("estateID") final String estateID) {
		return new LotResource(request, estateID);
	}

	@GET
	@ApiResource(value = "/{estateID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Estate", notes="<p>This call will return a list of Albums and their Images for a given Estate. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/{estateID}/albums")
	public AlbumResource getAlbumResource(@PathParam("estateID") final String estateID, 
			@ApiParam(value = "Document Sub Type", required = false) @QueryParam("DocumentSubType") String subType, 
			@ApiParam(value = "Album Type", required = false) @QueryParam("AlbumType") String albumType, 
			@ApiParam(value = "Album Publsihed for", allowableValues="Website", required=false, defaultValue="") @DefaultValue("") @QueryParam("Publish") String publish) {
		return new AlbumResource(request,  estateID, albumType, publish, subType);
	}
}
