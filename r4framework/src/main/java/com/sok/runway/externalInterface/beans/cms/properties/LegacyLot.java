package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.framework.InitServlet;
import com.sok.runway.crm.cms.properties.Land;
import com.sok.runway.crm.cms.properties.entities.LotEntity;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.SimplePublishing;
import com.sok.runway.externalInterface.beans.shared.Address;

@XmlAccessorType(XmlAccessType.FIELD)
public class LegacyLot {

	private String ProductID;
	private String Name;
	private String ProductNum;

	private String displayPrice = ""; 
	
	private String EstateProductID;
	private String EstateName;
	private String StageProductID;
	private String StageName;
	//private String LotName;
	private String LocationID, RegionID, Status, StatusID, SubType;
	private Address Address;
	private HomeDetails Details;
	private String TitleMonth, TitleYear;
	private double Cost, GST, TotalCost;
	private String Error; 
	private String active;
	private SimplePublishing publishing = null;
	private Date availableDate, expiryDate;
	
	
	public LegacyLot() {}
	public LegacyLot(String error) {
		this.Error = error;
	}
	public LegacyLot(Land data) {
		this.ProductID = data.getString("ProductID");
		this.Name = data.getString("Name");
		this.ProductNum = data.getString("ProductNum");
		this.active = data.getData("Active");
		
		this.EstateProductID = data.getString("EstateProductID");
		this.EstateName = data.getString("EstateName");
		
		this.StageProductID = data.getString("StageProductID");
		this.StageName = data.getString("StageName");
		this.RegionID = data.getString("RegionID");
		this.LocationID = data.getString("LocationID");
		this.Address = BeanFactory.getAddressFromTableData(data, "");
		this.Details = new HomeDetails(data);
		
		this.Cost = data.getDouble("Cost");
		this.GST = data.getDouble("GST");
		this.TotalCost = data.getDouble("TotalCost");
		this.TitleMonth = data.getString("LotTitleMonth");
		this.TitleYear = data.getString("LotTitleYear");
		
		this.Status = data.getString("CurrentStatus");
		this.StatusID = data.getString("CurrentStatusID");
		this.SubType = data.getString("ProductSubType");

		this.setPublishing(BeanFactory.loadPublishing(data.getData("ProductID")));
	}
	
	public LegacyLot(LotEntity le) {
		this(BeanFactory.external(le));
        setDetails(le.getDetails());		
	}
	
	public LegacyLot(Lot lot) {
		
		this.ProductID = lot.getLotID();
		this.Name = lot.getName();
		this.ProductNum = lot.getProductNum();
		//this.active = lot.getActive();
		
		this.EstateProductID = (lot.getStage() != null && lot.getStage().getEstate() != null) ? lot.getStage().getEstate().getEstateID() : null;
		this.EstateName = (lot.getStage() != null && lot.getStage().getEstate() != null) ? lot.getStage().getEstate().getName() : null;
		
		this.StageProductID = (lot.getStage() != null) ? lot.getStage().getStageID() : null;
		this.StageName = (lot.getStage() != null) ? lot.getStage().getName() : null;
		this.RegionID = lot.getRegionID();
		this.LocationID = lot.getLocationID();
		this.Address = lot.getAddress();
		
		this.Cost = lot.getCost() != null ? lot.getCost() : 0d;
		this.GST = lot.getGST() != null ? lot.getGST() : 0d;
		this.TotalCost = lot.getTotalCost() != null ? lot.getTotalCost() : 0d;
		this.TitleMonth = lot.getTitleMonth();
		this.TitleYear = lot.getTitleYear();
		
		this.Status = lot.getCurrentStatus() != null ? lot.getCurrentStatus().getStatus() : null;
		this.StatusID = lot.getCurrentStatus() != null ? lot.getCurrentStatus().getStatusID() : null;
		this.SubType = lot.getSubType();
	}
	
	@JsonProperty("ProductID")
	public String getProductID() {
		return ProductID;
	}
	@JsonProperty("ProductID")
	public void setProductID(String productID) {
		ProductID = productID;
	}
	@JsonProperty("Name")
	public String getName() {
		return Name;
	}
	@JsonProperty("Name")
	public void setName(String name) {
		Name = name;
	}
	@JsonProperty("ProductNum")
	public String getProductNum() {
		return ProductNum;
	}
	@JsonProperty("ProductNum")
	public void setProductNum(String productNum) {
		ProductNum = productNum;
	}
	@JsonProperty("EstateProductID")
	public String getEstateProductID() {
		return EstateProductID;
	}
	@JsonProperty("EstateProductID")
	public void setEstateProductID(String estateProductID) {
		EstateProductID = estateProductID;
	}
	@JsonProperty("EstateName")
	public String getEstateName() {
		return EstateName;
	}
	@JsonProperty("EstateName")
	public void setEstateName(String estateName) {
		EstateName = estateName;
	}
	@JsonProperty("StageProductID")
	public String getStageProductID() {
		return StageProductID;
	}
	@JsonProperty("StageProductID")
	public void setStageProductID(String stageProductID) {
		StageProductID = stageProductID;
	}
	@JsonProperty("StageName")
	public String getStageName() {
		return StageName;
	}
	@JsonProperty("StageName")
	public void setStageName(String stageName) {
		StageName = stageName;
	}
	@JsonProperty("LotName")
	public String getLotName() {
		return Name;
	}
	@JsonProperty("LotName")
	public void setLotName(String lotName) {
		this.Name = lotName;
	}
	@JsonProperty("LocationID")
	public String getLocationID() {
		return LocationID;
	}
	@JsonProperty("LocationID")
	public void setLocationID(String locationID) {
		LocationID = locationID;
	}
	@JsonProperty("RegionID")
	public String getRegionID() {
		return RegionID;
	}
	@JsonProperty("RegionID")
	public void setRegionID(String regionID) {
		RegionID = regionID;
	}
	@JsonProperty("Address")
	public Address getAddress() {
		return Address;
	}
	@JsonProperty("Address")
	public void setAddress(Address address) {
		Address = address;
	}
	@JsonProperty("TitleMonth")
	public String getTitleMonth() {
		return TitleMonth;
	}
	@JsonProperty("TitleMonth")
	public void setTitleMonth(String titleMonth) {
		TitleMonth = titleMonth;
	}
	@JsonProperty("TitleYear")
	public String getTitleYear() {
		return TitleYear;
	}
	@JsonProperty("TitleYear")
	public void setTitleYear(String titleYear) {
		TitleYear = titleYear;
	}
	@JsonProperty("StatusID")
	public String getStatusID() {
		return StatusID;
	}
	@JsonProperty("StatusID")
	public void setStatusID(String StatusID) {
		this.StatusID = StatusID;
	}
	@JsonProperty("Status")
	public String getStatus() {
		return Status;
	}
	@JsonProperty("Status")
	public void setStatus(String Status) {
		this.Status = Status;
	}
	@JsonProperty("SubType")
	public String getSubType() {
		return SubType;
	}
	@JsonProperty("SubType")
	public void setSubType(String SubType) {
		this.SubType = SubType;
	}
	
	@JsonProperty("Cost")
	public double getCost() {
		return Cost;
	}
	@JsonProperty("Cost")
	public void setCost(double cost) {
		Cost = cost;
	}
	@JsonProperty("GST")
	public double getGST() {
		return GST;
	}
	@JsonProperty("GST")
	public void setGST(double gST) {
		GST = gST;
	}
	@JsonProperty("TotalCost")
	public double getTotalCost() {
		return TotalCost;
	}
	@JsonProperty("TotalCost")
	public void setTotalCost(double totalCost) {
		TotalCost = totalCost;
	}
	@JsonProperty("Detail")
	public HomeDetails getDetails() {
		return Details;
	}
	@JsonProperty("Detail")
	public void setDetails(HomeDetails Details) {
		this.Details = Details;
	}
	@JsonProperty("Error")
	public String getError() {
		return Error;
	}
	@JsonProperty("Error")
	public void setError(String error) {
		Error = error;
	}
	@XmlElement(name="Publishing")
	public SimplePublishing getPublishing() {
		return publishing;
	}
	public void setPublishing(SimplePublishing publishing) {
		this.publishing = publishing;
	}
	@XmlElement(name="VisibilityStatus")
	public String getVisibilityStatus() {
		if (StatusID == null) return "Offline";
		
		if (!VisibilityStatus.checkVisibilityStatus(active, StatusID, availableDate, expiryDate)) return "Offline";
		
		if ("true".equals(InitServlet.getSystemParam("RPM-Publishing-Land"))) {
			if (publishing == null) return "Offline";
			if (!VisibilityStatus.checkPublishedStatus(publishing.getPublishedFlag(), publishing.getStatus(), publishing.getStartDate(), publishing.getEndDate())) return "Offline";
		}
		
		return "Online";
	}
	
	public void setVisibilityStatus(String status) {
		
	}
	@XmlElement(name="AvailableDate")
	public Date getAavalableDate() {
		return availableDate;
	}
	public void setAavalableDate(Date avalableDate) {
		this.availableDate = avalableDate;
	}
	@XmlElement(name="ExpiryDate")
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	@XmlElement(name="DisplayPrice")
	public String getDisplayPrice() {
		return displayPrice;
	}
	public void setDisplayPrice(String displayPrice) {
		this.displayPrice = displayPrice;
	}
}