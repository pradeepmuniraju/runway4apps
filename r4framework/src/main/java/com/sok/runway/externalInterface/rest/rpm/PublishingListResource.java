package com.sok.runway.externalInterface.rest.rpm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.externalInterface.beans.cms.properties.publishing.PublishingRecord;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class PublishingListResource {

	private final HttpServletRequest request; 
	private final PropertyType propertyType;
	private String productID = null;
	static final Logger logger = LoggerFactory.getLogger(PublishingListResource.class);
	
	public PublishingListResource(HttpServletRequest request, PropertyType propertyType) {
		this.request = request; 
		this.propertyType = propertyType;
	}
	
	@GET
	@ApiOperation(value = "List packages/realestate", notes="Optionally filtering by modified date, this modified date only checks if the publishing has been modifed it doesn't include any changes to the product. Please use <a class='toggleOperation' href='#!/packages/getPackages_get_1'>/packages</a> or <a class='toggleOperation' href='#!/packages/getRealEstateProperties_get_1'>/realestate</a> to get a list of modified products.", responseClass="List[com.sok.runway.externalInterface.beans.cms.properties.publishing.PublishingRecord]")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "PublishingRecords")
	public List<PublishingRecord> getPackages(@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = true) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "A publish method", allowableValues="Website", required = true) @QueryParam("PublishMethod") String publishMethod) {
		return getPackageList(modifiedSince, publishMethod);
	}

	public List<PublishingRecord> getPackageList(String modifiedSince, String publishMethod) {
		Date mod = null;
		publishMethod = StringUtils.trimToNull(publishMethod);
		if(StringUtils.isNotBlank(modifiedSince)) {
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try {  
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);		 
				} catch (ParseException pe) {	
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}	
		GenRow search = new GenRow(); 
		search.parseRequest(request, "_");
		search.setViewSpec("properties/ProductPublishingView");
		if(mod != null) { 
			search.setParameter("ModifiedDate", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(mod));
		}
		if(publishMethod!=null) {
			search.setParameter("PublishMethod", publishMethod);
		}
		if(propertyType != null) {
			search.setParameter("ProductPublishingProduct.ProductType",propertyType.getProductType());
		}
		if(productID != null) {
			search.setParameter("ProductID", productID);
		}
		
		search.setParameter("-join1","ProductPublishingCreated"); 
		search.setParameter("-join2","ProductPublishingModified"); 
		search.setParameter("-select1","ProductPublishingCreated.FirstName as OwnFirstName"); 
		search.setParameter("-select2","ProductPublishingCreated.LastName as OwnLastName"); 
		search.setParameter("-select3","ProductPublishingModified.FirstName as ModFirstName"); 
		search.setParameter("-select4","ProductPublishingModified.LastName as ModLastName");
		
		if(logger.isTraceEnabled()) {
			search.doAction(GenerationKeys.SEARCH);
			logger.trace(search.getStatement());
		}
		
		if(!(PropertyType.HouseLandPackage == propertyType || PropertyType.ExistingProperty == propertyType)) {
			throw new UnsupportedOperationException("Property Type not yet supported : " + (propertyType != null ? propertyType.name() : "null"));
		}
		try {
			search.getResults(true);
			if(search.getNext()) {
				List<PublishingRecord>list = new ArrayList<PublishingRecord>(search.getSize());
				do {
					if("House and Land".equals(search.getData("ProductType"))) {
						list.add( new PublishingRecord(search));
					} else if("Existing Property".equals(search.getData("ProductType"))) {
							list.add( new PublishingRecord(search));
					} else {
						logger.warn("Unsupported Product Type found: [{}]", search.getData("ProductType"));
					}
				} while(search.getNext());
				return list; 
			}
			return (List<PublishingRecord>) new ArrayList<PublishingRecord>(0);
		} finally {
			search.close();
		}
	}
}
