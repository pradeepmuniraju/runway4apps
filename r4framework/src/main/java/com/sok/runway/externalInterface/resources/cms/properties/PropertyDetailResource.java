package com.sok.runway.externalInterface.resources.cms.properties;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.beans.cms.properties.ProductDetail;
import com.sok.service.crm.cms.properties.PlanService;

public class PropertyDetailResource {
	private static final Logger logger = LoggerFactory.getLogger(PropertyDetailResource.class);
	private static final PlanService planService = PlanService.getInstance();
	
	private final String productID; 
	public PropertyDetailResource(String productID, HttpServletRequest request) {
		this.productID = productID;
		this.request = request;
	}
	
	@Context HttpServletRequest request;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<ProductDetail> getExistingPropertyDimentions() {
		return planService.getProductDetails(request, productID);
	}

	@GET
	@Path("{productDetailID}")
	@Produces(MediaType.APPLICATION_JSON)
	public ProductDetail getExistingPropertyDimentions(@PathParam("productDetailID") String productDetailID) {
		return planService.getProductDetail(request, productID, productDetailID);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ProductDetail saveExistingPropertyDimentions(@Context HttpServletRequest request, ProductDetail productDetail) {
		productDetail.ProductID = productID;
		productDetail.ProductDetailsID = null;
		return planService.saveProductDetail(request, productDetail);
	}
	
	@PUT
	@Path("{productDetailID}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ProductDetail updateExistingPropertyDimentions(@PathParam("productDetailID") String productDetailID, ProductDetail productDetail) {
		productDetail.ProductID = productID;
		productDetail.ProductDetailsID = productDetailID;
		return planService.saveProductDetail(request, productDetail);
	}

	
	
}
