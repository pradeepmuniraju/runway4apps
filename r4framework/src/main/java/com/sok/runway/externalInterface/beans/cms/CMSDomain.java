package com.sok.runway.externalInterface.beans.cms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import org.codehaus.jackson.annotate.JsonProperty;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class CMSDomain {

	private String CMSDomainID, CMSID, Domain;
	public CMSDomain() {}
	public CMSDomain(GenRow g) {
		setCMSDomainID(g.getData("CMSDomainID"));
		setCMSID(g.getData("CMSID"));
		setDomain(g.getData("Domain"));
	}
	
	@JsonProperty("CMSDomainID")
	public String getCMSDomainID() {
		return CMSDomainID;
	}
	@JsonProperty("CMSDomainID")
	public void setCMSDomainID(String cMSDomainID) {
		CMSDomainID = StringUtils.trimToNull(cMSDomainID);
	}
	
	@JsonProperty("CMSID")
	public String getCMSID() {
		return CMSID;
	}
	@JsonProperty("CMSID")
	public void setCMSID(String cMSID) {
		CMSID = StringUtils.trimToNull(cMSID);
	}
	
	@JsonProperty("Domain")
	public String getDomain() {
		return Domain;
	}
	@JsonProperty("Domain")
	public void setDomain(String domain) {
		Domain = StringUtils.trimToNull(domain);
	}
}
