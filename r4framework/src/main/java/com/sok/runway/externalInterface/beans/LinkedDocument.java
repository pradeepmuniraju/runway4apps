package com.sok.runway.externalInterface.beans;

//import com.sok.runway.externalInterface.beans.shared.*;
import java.util.Date;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class LinkedDocument
{
	private String linkeddocumentid = null;
	private String documentid = null;
	private String noteid = null;
	private String companyid = null;
	private String contactid = null;
	private String orderid = null;
	private String opportunityid = null;
	private String pageid = null;
	private Date createddate = null;
	private String createdby = null;
	private String location = null;
	private String templateid = null;
	private String defaultdocument = null;
	private String mastertemplateid = null;
	private String productid = null;
	private String stageid = null;
	private String processstageid = null;
	private String campaignid = null;
	private String sortorder = null;
	private String galleyID = null;


	@JsonProperty("LinkedDocumentID")
	public String getLinkedDocumentID()
	{
		return(linkeddocumentid);
	}  

	@JsonProperty("LinkedDocumentID")
	public void setLinkedDocumentID(String linkeddocumentid)
	{
		this.linkeddocumentid = linkeddocumentid;
	}  

	@JsonProperty("DocumentID")
	public String getDocumentID()
	{
		return(documentid);
	}  

	@JsonProperty("DocumentID")
	public void setDocumentID(String documentid)
	{
		this.documentid = documentid;
	}  

	public String getNoteID()
	{
		return(noteid);
	}  

	public void setNoteID(String noteid)
	{
		this.noteid = noteid;
	}  

	public String getCompanyID()
	{
		return(companyid);
	}  

	public void setCompanyID(String companyid)
	{
		this.companyid = companyid;
	}  

	public String getContactID()
	{
		return(contactid);
	}  

	public void setContactID(String contactid)
	{
		this.contactid = contactid;
	}  

	public String getOrderID()
	{
		return(orderid);
	}  

	public void setOrderID(String orderid)
	{
		this.orderid = orderid;
	}  

	public String getOpportunityID()
	{
		return(opportunityid);
	}  

	public void setOpportunityID(String opportunityid)
	{
		this.opportunityid = opportunityid;
	}  

	public String getPageID()
	{
		return(pageid);
	}  

	public void setPageID(String pageid)
	{
		this.pageid = pageid;
	}  

	public Date getCreatedDate()
	{
		return(createddate);
	}  

	public void setCreatedDate(Date createddate)
	{
		this.createddate = createddate;
	}  

	public String getCreatedBy()
	{
		return(createdby);
	}  

	public void setCreatedBy(String createdby)
	{
		this.createdby = createdby;
	}  

	public String getLocation()
	{
		return(location);
	}  

	public void setLocation(String location)
	{
		this.location = location;
	}  

	public String getTemplateID()
	{
		return(templateid);
	}  

	public void setTemplateID(String templateid)
	{
		this.templateid = templateid;
	}  

	public String getDefaultDocument()
	{
		return(defaultdocument);
	}  

	public void setDefaultDocument(String defaultdocument)
	{
		this.defaultdocument = defaultdocument;
	}  

	public String getMasterTemplateID()
	{
		return(mastertemplateid);
	}  

	public void setMasterTemplateID(String mastertemplateid)
	{
		this.mastertemplateid = mastertemplateid;
	}  

	@JsonProperty("ProductID")
	public String getProductID()
	{
		return(productid);
	}  

	@JsonProperty("ProductID")
	public void setProductID(String productid)
	{
		this.productid = productid;
	}  

	public String getStageID()
	{
		return(stageid);
	}  

	public void setStageID(String stageid)
	{
		this.stageid = stageid;
	}  

	public String getProcessStageID()
	{
		return(processstageid);
	}  

	public void setProcessStageID(String processstageid)
	{
		this.processstageid = processstageid;
	}  

	public String getCampaignID()
	{
		return(campaignid);
	}  

	public void setCampaignID(String campaignid)
	{
		this.campaignid = campaignid;
	}  

	public String getSortOrder()
	{
		return(sortorder);
	}  

	public void setSortOrder(String sortorder)
	{
		this.sortorder = sortorder;
	}  

	private Document document = null;

	public Document getDocument()
	{
		return(document);
	}

	public void setDocument(Document document)
	{
		this.document = document;
	}

	@JsonProperty("GalleryID")
	public String getGalleyID() {
		return galleyID;
	}

	public void setGalleyID(String galleyID) {
		this.galleyID = galleyID;
	}

}
