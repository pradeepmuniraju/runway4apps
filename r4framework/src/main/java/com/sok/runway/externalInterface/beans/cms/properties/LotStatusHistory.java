package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.runway.externalInterface.beans.shared.AbstractStatusHistory;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="LotStatus")
@XmlType(propOrder={"lotStatusID","lotID"})
public class LotStatusHistory extends AbstractStatusHistory {
	private String lotStatusID, lotID;

	@XmlElement(name="LotStatusID")
	public String getLotStatusID() {
		return lotStatusID;
	}
	public void setLotStatusID(String lotStatusID) {
		this.lotStatusID = lotStatusID;
	}
	
	@XmlElement(name="LotID")
	public String getLotID() {
		return lotID;
	}
	public void setLotID(String lotID) {
		this.lotID = lotID;
	}
}
