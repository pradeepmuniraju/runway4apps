package com.sok.runway.externalInterface.beans;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class Result
{
	private int affectedRecords = 0;
	private String error = null;
   private String debug = null;	
	
	public int getAffectedRecordsCount()
	{
		return(affectedRecords);
	}	
	
	public void setAffectedRecordsCount(int affectedRecords)
	{
		this.affectedRecords = affectedRecords;
	}	
	
	public String getError()
	{
		return(error);
	}	
	
	public void setError(String error)
	{
		this.error = error;
	}
	
   public String getDebug()
   {
      return(debug);
   }  
   
   public void setDebug(String debug)
   {
      this.debug = debug;
   }  	
}