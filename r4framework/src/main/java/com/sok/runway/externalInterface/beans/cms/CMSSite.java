package com.sok.runway.externalInterface.beans.cms;

import com.sok.framework.GenRow;

public class CMSSite {
	public String CMS, Type, Name, Description, Folder, Path, HomeLinkID, PageMetaID, GroupID, CMSMode;
	public boolean Active;
	public CMSSite() {}
	public CMSSite(GenRow cms) {
		Type = cms.getString("Type");
		Name = cms.getString("Name");
		Description = cms.getString("Description");
		Folder = cms.getString("Folder");
		Path = cms.getString("Path");
		Path = cms.getString("Path");
		Active = "Y".equals(cms.getString("Active"));
		HomeLinkID = cms.getString("HomeLinkID");
		PageMetaID = cms.getString("PageMetaID");
		GroupID = cms.getString("GroupID");
		CMSMode = cms.getString("CMSMode");
		CMS = cms.getString("CMS");
	}
}
