package com.sok.runway.externalInterface.beans.shared;
import java.util.*;

import org.codehaus.jackson.annotate.JsonProperty;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class OrderAnswer extends Answer
{
	private String orderid = null;
	
	@JsonProperty("OrderID")
	public String getOrderID()
	{
		return(orderid);
	}	
	
	@JsonProperty("OrderID")
	public void setOrderID(String orderid)
	{
		this.orderid = orderid;
	}		
}