package com.sok.runway.externalInterface.rest.rpm;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.Home;
import com.sok.runway.externalInterface.beans.cms.properties.HomeCheck;
import com.sok.runway.security.SecurityFactory;
//import com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackageSearch;
import com.sok.service.crm.cms.properties.HomeService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

@Path("/homes")
@Api(value = "/homes", description = "Retrieve Home Design Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class HomeResource {
	
	private static final HomeService homeService = HomeService.getInstance();
	static final Logger logger = LoggerFactory.getLogger(HomeResource.class);
	@Context HttpServletRequest request;
	
	private String rangeID;
	
	public HomeResource() {}
	public HomeResource(HttpServletRequest request) { this.request = request; }
	public HomeResource(HttpServletRequest request, String rangeID) { 
		this(request);
		this.rangeID = rangeID;
	}

	@GET
	@ApiOperation(value = "List Homes, plans and facades", notes="<p>Optionally filtering by modified date, this test will only do the first 20 homes. This API will return all homes and plans when not used via api-docs.</p><p>In production it is recommended that the homes feed be started after 4am. This allows for the DRIP calculation process which starts after midnight, otherwise prices may be incorrect.</p><p>You should also set Active to false in your request so you get all homes and plans that have been modified and then remove those from your database that Active is not set to 'Y' and CurrentStatus is not set to 'Available'.<p><p>Any deleted packages will not show in this feed, please use <a onclick='Docs.toggleEndpointListForResource(\'activites\');' href='#!/activites/getActivities_get_0'>/activties</a> to list deleted homes and plans</p>", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Home", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "Home")
	public Response getHomes(
			@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = false) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active packages", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try {  
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);		 
				} catch (ParseException pe) {	
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		/** Search option 2. Downside to this is the limit of the GET request.
		 * this is going to be used by the henley developer portal, i'm not 100% if this is how we want to do api-search or 
		 * not. Consider it a hack if you like ;) */
		GenRow search = new GenRow(); 
		
		if(request.getHeader("Referer") != null && request.getHeader("Referer").indexOf("api-docs") != -1) {
			search.setParameter("-top", 20);
		}
		
		search.parseRequest(request, "_");
		if(mod != null) { 
			search.setParameter("ModifiedDate|1", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(mod));
			search.setParameter("ProductChildHomePlan.ProductVariationHomePlan.ModifiedDate|1", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(mod));
		}
		if (StringUtils.isNotBlank(rangeID)) search.setParameter("ProductParentHomeRange.ProductVariationHomeRange.ProductID",rangeID);
		if(active) {
			search.setParameter("Active","Y");
		}
		
		/* semi-hacky content-negotiation */
		String accept = StringUtils.trimToEmpty(request.getHeader("Accept"));
		int xml = accept.indexOf("text/xml"), json = accept.indexOf("application/json"); 
		MediaType mt = xml != -1 && (xml < json || json == -1) ? MediaType.TEXT_XML_TYPE : MediaType.APPLICATION_JSON_TYPE;
		
		if(!"true".equals(request.getParameter("-paged"))) {
			/*
			Response.ok(new StreamingOutput() {
				public void write(OutputStream output) throws IOException, WebApplicationException {
					// * TODO streamed package output  
					// * Maybe only do for JSON, as it's easier 
					org.codehaus.jackson.JsonFactory jf = new org.codehaus.jackson.JsonFactory();
					org.codehaus.jackson.JsonGenerator jg = jf.createJsonGenerator(output);
				}
			}).build();
			*/
			return Response.ok(mt == MediaType.TEXT_XML_TYPE ? new HomeList(homeService.getHomesExternal(request, search)) : homeService.getHomesExternal(request, search)).build();
		} else {
			
			// take the search params, make up a unique search identifier based on those params. 
			
			/* 
			 * Price EstateID StageID LotID HomeID Frontage Size 
			 */
			String pagingKey = StringUtils.defaultIfEmpty(request.getParameter("-pagingKey"), "-lastHLAPI2Search"); 
			int perPage = new Integer(StringUtils.defaultIfEmpty(request.getParameter("-perPage"), "50"));
			int page = new Integer(StringUtils.defaultIfEmpty(request.getParameter("-page"), "0"));
			
			logger.debug("using pagingKey={}, perPage={}, page={}", new String[]{pagingKey, String.valueOf(perPage), String.valueOf(page) });
			
			List<String> keys = new ArrayList<String>(search.keySet());
			Collections.sort(keys);
			StringBuilder params = new StringBuilder(); 
			boolean first = true;
			for(String key: keys) {
				if(first) first = false; else params.append("&"); 
				params.append(key).append("=").append(String.valueOf(search.get(key))); 
			}
			
			logger.debug(search.toString());
			logger.debug(params.toString());
			
			LastSearch ls = (LastSearch)request.getSession().getAttribute(pagingKey);
			String hash = SecurityFactory.hashPassword(params.toString());
			
			logger.debug("New hash: [{}] - old hash [{}]", hash, ls != null ? ls.hash : "ls was null"); 
			
			if(ls == null || !hash.equals(ls.hash)) {
				logger.debug("doing new search"); 
				ls = new LastSearch(homeService.getHomesExternal(request, search), hash);
				request.getSession().setAttribute(pagingKey, ls);
			}
			
			int startCount = page * perPage;
			
			if(startCount > ls.total) {
				throw new IllegalArgumentException("Page requested is greater than the amount of available pages");
			}
			
			int resultSize = startCount + perPage > ls.total ? ls.total - startCount : perPage;
			int lastIndex = startCount + resultSize - 1; 
			
			List<Home> results = Collections.emptyList();
			if(resultSize != 0) { 
				results = new ArrayList<Home>(resultSize); 
				for(int i=startCount;;i++) {
					results.add(ls.list.get(i));
					if(i == lastIndex) {
						break;
					}
				}
			}
			return Response.ok(new PagedSearchResult(results, ls.total, page)).build();
		}
	}
	
	@GET
	@Path("/check")
	@ApiOperation(value = "List Homes, plans and facades", notes="<p>Optionally filtering by modified date, this test will only do the first 20 homes. This API will return all homes and plans when not used via api-docs.</p><p>In production it is recommended that the homes feed be started after 4am. This allows for the DRIP calculation process which starts after midnight, otherwise prices may be incorrect.</p><p>You should also set Active to false in your request so you get all homes and plans that have been modified and then remove those from your database that Active is not set to 'Y' and CurrentStatus is not set to 'Available'.<p><p>Any deleted packages will not show in this feed, please use <a onclick='Docs.toggleEndpointListForResource(\'activites\');' href='#!/activites/getActivities_get_0'>/activties</a> to list deleted homes and plans</p>", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Home", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "Home")
	public Response getHomesCheck(
			@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = false) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active packages", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try {  
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);		 
				} catch (ParseException pe) {	
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		/** Search option 2. Downside to this is the limit of the GET request.
		 * this is going to be used by the henley developer portal, i'm not 100% if this is how we want to do api-search or 
		 * not. Consider it a hack if you like ;) */
		GenRow search = new GenRow(); 
		
		if(request.getHeader("Referer") != null && request.getHeader("Referer").indexOf("api-docs") != -1) {
			search.setParameter("-top", 20);
		}
		
		search.parseRequest(request, "_");
		if(mod != null) { 
			search.setParameter("ModifiedDate|1", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(mod));
			search.setParameter("ProductChildHomePlan.ProductVariationHomePlan.ModifiedDate|1", new SimpleDateFormat(">dd/MM/yyyy HH:mm:ss").format(mod));
		}
		if(active) {
			search.setParameter("Active","Y");
		}
		
		/* semi-hacky content-negotiation */
		String accept = StringUtils.trimToEmpty(request.getHeader("Accept"));
		int xml = accept.indexOf("text/xml"), json = accept.indexOf("application/json"); 
		MediaType mt = xml != -1 && (xml < json || json == -1) ? MediaType.TEXT_XML_TYPE : MediaType.APPLICATION_JSON_TYPE;
		
		if(!"true".equals(request.getParameter("-paged"))) {
			/*
			Response.ok(new StreamingOutput() {
				public void write(OutputStream output) throws IOException, WebApplicationException {
					// * TODO streamed package output  
					// * Maybe only do for JSON, as it's easier 
					org.codehaus.jackson.JsonFactory jf = new org.codehaus.jackson.JsonFactory();
					org.codehaus.jackson.JsonGenerator jg = jf.createJsonGenerator(output);
				}
			}).build();
			*/
			return Response.ok(mt == MediaType.TEXT_XML_TYPE ? new HomeListCheck(homeService.getHomesExternalCheck(request, search)) : homeService.getHomesExternalCheck(request, search)).build();
		} else {
			
			// take the search params, make up a unique search identifier based on those params. 
			
			/* 
			 * Price EstateID StageID LotID HomeID Frontage Size 
			 */
			String pagingKey = StringUtils.defaultIfEmpty(request.getParameter("-pagingKey"), "-lastHLAPI2Search"); 
			int perPage = new Integer(StringUtils.defaultIfEmpty(request.getParameter("-perPage"), "50"));
			int page = new Integer(StringUtils.defaultIfEmpty(request.getParameter("-page"), "0"));
			
			logger.debug("using pagingKey={}, perPage={}, page={}", new String[]{pagingKey, String.valueOf(perPage), String.valueOf(page) });
			
			List<String> keys = new ArrayList<String>(search.keySet());
			Collections.sort(keys);
			StringBuilder params = new StringBuilder(); 
			boolean first = true;
			for(String key: keys) {
				if(first) first = false; else params.append("&"); 
				params.append(key).append("=").append(String.valueOf(search.get(key))); 
			}
			
			logger.debug(search.toString());
			logger.debug(params.toString());
			
			LastSearch ls = (LastSearch)request.getSession().getAttribute(pagingKey);
			String hash = SecurityFactory.hashPassword(params.toString());
			
			logger.debug("New hash: [{}] - old hash [{}]", hash, ls != null ? ls.hash : "ls was null"); 
			
			if(ls == null || !hash.equals(ls.hash)) {
				logger.debug("doing new search"); 
				ls = new LastSearch(homeService.getHomesExternal(request, search), hash);
				request.getSession().setAttribute(pagingKey, ls);
			}
			
			int startCount = page * perPage;
			
			if(startCount > ls.total) {
				throw new IllegalArgumentException("Page requested is greater than the amount of available pages");
			}
			
			int resultSize = startCount + perPage > ls.total ? ls.total - startCount : perPage;
			int lastIndex = startCount + resultSize - 1; 
			
			List<Home> results = Collections.emptyList();
			if(resultSize != 0) { 
				results = new ArrayList<Home>(resultSize); 
				for(int i=startCount;;i++) {
					results.add(ls.list.get(i));
					if(i == lastIndex) {
						break;
					}
				}
			}
			return Response.ok(new PagedSearchResult(results, ls.total, page)).build();
		}
	}
	
	@XmlRootElement(name="Search")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class PagedSearchResult {
		public final List<Home> results; 
		public final int count;
		public final int page;  
		
		public PagedSearchResult(List<Home> results, int count, int page) {
			this.results = results; 
			this.count = count; 
			this.page = page; 
		}
	}
	
	@XmlAccessorType(XmlAccessType.PROPERTY)
	@XmlRootElement(name = "Homes")
	public static class HomeList {
		
		private final List<Home> list; 
		public HomeList() {
			list = Collections.emptyList(); 
		}
		public HomeList(List<Home> list) {
			this.list = list;
		}
		
		@XmlElement(name="Home")
		public List<Home> getHomes() {
			return list;
		}
	}
	
	@XmlAccessorType(XmlAccessType.PROPERTY)
	@XmlRootElement(name = "Homes")
	public static class HomeListCheck {
		
		private final List<HomeCheck> list; 
		public HomeListCheck() {
			list = Collections.emptyList(); 
		}
		public HomeListCheck(List<HomeCheck> list) {
			this.list = list;
		}
		
		@XmlElement(name="Home")
		public List<HomeCheck> getHomes() {
			return list;
		}
	}
	
	private class LastSearch {
		private final List<Home> list; 
		private final String hash; 
		private final int total; 
		
		public LastSearch(List<Home> list, String hash) {
			this.list = list; 
			this.hash = hash; 
			this.total = list.size();
		}
	}
	
	///**
	// * Search Option 1, unknown format at this stage.
	// */
	//@POST
	//@Path("/search")
	//@ApiOperation(value="Create a package search", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.HouseLandPackageSearch")
	//@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	//public Response getPackageSearch(Object search) throws java.net.URISyntaxException {
	//	String searchID = null;
	//	return Response.status(Status.TEMPORARY_REDIRECT).location(new URI(request.getContextPath()  + "/api/2/homes/?SearchID=" + searchID)).build();
	//}
	
	/*
	@ApiResource(value = "/publishing", resourceClass="com.sok.runway.externalInterface.rest.rpm.PublishingResource")
	@Path("publishing")
	public PublishingResource getPublishingResource() {
		return new PublishingResource(request, PropertyType.HouseLandPackage);
	}
	*/
	@GET
	@Path("/{homeID}")
	@ApiOperation(value = "Return a particular home with it's plans and facades", notes="<p>In production it is recommended that the home plan feed be started after 4am. This allows for the DRIP calculation process which starts after midnight, otherwise prices may be incorrect.</p><p>You should remove those plans from your database that Active is not set to 'Y' and CurrentStatus is not set to 'Available'.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Home")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public Home getHome(@Context HttpServletRequest request, 
			@PathParam("homeID") final String homeID,
			@ApiParam(value = "Retrieve extended package information", allowableValues="Simple,Extended", required=false, defaultValue="Simple") @DefaultValue("Simple") @QueryParam("View") BeanRepresentation extended
			) {
		return homeService.getHomesExternal(request, homeID, extended);
	}
	
	@GET
	@ApiResource(value = "/{homeID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Home Design", notes="<p>This call will return a list of Albums and their Images for a given Home Design. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/{homeID}/albums")
	public AlbumResource getHomeAlbumResource(@PathParam("homeID") final String homeID, 
			@ApiParam(value = "Document Sub Type", required = false) @QueryParam("DocumentSubType") String subType, 
			@ApiParam(value = "Album Type", required = false) @QueryParam("AlbumType") String albumType, 
			@ApiParam(value = "Album Publsihed for", allowableValues="Website", required=false, defaultValue="") @DefaultValue("") @QueryParam("Publish") String publish) {
		return new AlbumResource(request,  homeID, albumType, publish, subType);
	}

	@GET
	@ApiResource(value = "/{homeID}/plans/{planID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Home Plan", notes="<p>This call will return a list of Albums and their Images for a given Home Plan. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/{homeID}/plans/{planID}/albums")
	public AlbumResource getHomePlanAlbumResource(@PathParam("homeID") final String homeID, @PathParam("planID") final String planID, 
			@ApiParam(value = "Document Sub Type", required = false) @QueryParam("DocumentSubType") String subType, 
			@ApiParam(value = "Album Type", required = false) @QueryParam("AlbumType") String albumType, 
			@ApiParam(value = "Album Publsihed for", allowableValues="Website", required=false, defaultValue="") @DefaultValue("") @QueryParam("Publish") String publish) {
		return new AlbumResource(request,  planID, albumType, publish, subType);
	}

	
	@GET
	@ApiResource(value = "/plans/{planID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Home Plan", notes="<p>This call will return a list of Albums and their Images for a given Home Plan. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/plans/{planID}/albums")
	public AlbumResource getPlanAlbumResource(@PathParam("planID") final String planID, 
			@ApiParam(value = "Document Sub Type", required = false) @QueryParam("DocumentSubType") String subType, 
			@ApiParam(value = "Album Type", required = false) @QueryParam("AlbumType") String albumType, 
			@ApiParam(value = "Album Publsihed for", allowableValues="Website", required=false, defaultValue="") @DefaultValue("") @QueryParam("Publish") String publish) {
		return new AlbumResource(request,  planID, albumType, publish, subType);
	}

	@GET
	@Path("/plans/{planID}")
	@ApiOperation(value = "Return a particular plan and facades", notes="<p>In production it is recommended that the home plan feed be started after 4am. This allows for the DRIP calculation process which starts after midnight, otherwise prices may be incorrect.</p><p>You should remove those plans from your database that Active is not set to 'Y' and CurrentStatus is not set to 'Available'.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Home")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public Home getPlan(@Context HttpServletRequest request, 
			@PathParam("planID") final String planID,
			@ApiParam(value = "Retrieve extended package information", allowableValues="Simple,Extended", required=false, defaultValue="Simple") @DefaultValue("Simple") @QueryParam("View") BeanRepresentation extended
			) {
		return homeService.getPlanExternal(request, planID, extended);
	}
}
