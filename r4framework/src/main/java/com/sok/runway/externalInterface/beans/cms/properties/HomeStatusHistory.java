package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sok.runway.externalInterface.beans.shared.AbstractStatusHistory;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="HomeStatus")
//@XmlType(propOrder={"homeStatusID","homeID"})
public class HomeStatusHistory extends AbstractStatusHistory {
	private String homeStatusID, homeID;

	@XmlElement(name="HomeStatusID")
	public String getPlanStatusID() {
		return homeStatusID;
	}
	public void setPlanStatusID(String homeStatusID) {
		this.homeStatusID = homeStatusID;
	}
	
	@XmlElement(name="HomeID")
	public String getHomeID() {
		return homeID;
	}
	public void setHomeID(String homeID) {
		this.homeID = homeID;
	}
}
