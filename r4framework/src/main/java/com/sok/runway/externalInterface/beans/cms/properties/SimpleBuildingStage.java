package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="BuildingStage")
@XmlSeeAlso(BuildingStage.class)
@XmlType(propOrder={"buildingStageID","name", "building"})
public class SimpleBuildingStage {
	protected String buildingStageID, name;
	protected SimpleBuilding building;
	
	@XmlElement(name="BuildingStageID")
	public String getBuildingStageID() {
		return buildingStageID;
	}
	public void setBuildingStageID(String buildingStageID) {
		this.buildingStageID = buildingStageID;
	}
	
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name="Building")
	public SimpleBuilding getBuilding() {
		return building;
	}
	public void setBuilding(SimpleBuilding building) {
		this.building = building;
	}
}
