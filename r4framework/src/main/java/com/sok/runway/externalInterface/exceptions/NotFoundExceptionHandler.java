package com.sok.runway.externalInterface.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.DataApplication;
import com.sok.service.exception.NotFoundException;

@Provider
public class NotFoundExceptionHandler implements ExceptionMapper<NotFoundException> {

	private static final Logger logger = LoggerFactory.getLogger(NotFoundExceptionHandler.class);
	
	public Response toResponse(NotFoundException exception){
		logger.error("Not Found: ", exception);
	    return Response.status(Response.Status.NOT_FOUND)
	    .entity(new DataApplication.DataApplicationException(exception, Response.Status.NOT_FOUND.getStatusCode()))
	    .build();
	}
}
