package com.sok.runway.externalInterface.beans.shared;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class CompanyProfile extends Profile
{
	private CompanyQuestion[] questions = null;
	
	public CompanyQuestion[] getQuestions()
	{
		return(questions);
	}	
	
	public void setQuestions(CompanyQuestion[] questions)
	{
		this.questions = questions;
	}	
}