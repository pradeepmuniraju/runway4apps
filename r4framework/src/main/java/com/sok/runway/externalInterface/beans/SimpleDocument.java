package com.sok.runway.externalInterface.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.StringUtils;

import com.sok.filemigration.FileMoveMap;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;

/**
 * Simplified Document Bean, for use with Products / Image/ImageName/ImagePath
 * @author Michael Dekmetzian
 * @date 3 January 2012
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="SimpleDocument")
//@XmlType(propOrder={"documentID", "description", "filename", "filepath","type","subType", "URL"})
public class SimpleDocument {

	private String documentid = null;
	private String filename = null;
	private String description = null;
	private String caption = null;
	private String filepath = null;
	private String type = null;
	private String subType = null;
	private String url = null;

	@XmlElement(name="DocumentID")
	public String getDocumentID()
	{
		return(documentid);
	}  

	public void setDocumentID(String documentid)
	{
		this.documentid = documentid;
	}  
	
	@XmlElement(name="FileName")
	public String getFileName()
	{
		return(filename);
	}  
	public void setFileName(String filename)
	{
		this.filename = filename;
	}  
	
	@XmlElement(name="FilePath")
	public String getFilePath()
	{
		return(filepath);
	}  
	public void setFilePath(String filepath)
	{
		this.filepath = filepath;
	}

	@XmlElement(name="URL")
	public String getURL()
	{
		String s3Url = null;
		if (!StringUtil.isBlankOrEmpty(filepath) && StringUtils.isNotBlank(InitServlet.getSystemParam("Pipeline-ClientID"))) {
			String[] sArr = filepath.split("/");
			if (sArr.length>2) {
				try {
					s3Url = FileMoveMap.getInstance().getDocumentURLByFilePath(filepath, sArr[sArr.length-1], sArr[sArr.length-2]);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		if (!StringUtil.isBlankOrEmpty(s3Url)) {
            if (!s3Url.startsWith("http")) s3Url = InitServlet.getSystemParam("URLHome") + "/" + s3Url;
			return s3Url;
		} else {
			url = InitServlet.getSystemParam("URLHome") + "/" + filepath;
			return url;
		}
	}  
	public void setUrl(String url) {
		this.url = url;
	}   

	@XmlElement(name="Type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlElement(name="SubType")
	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}  
	   
	@XmlElement(name="Caption")
	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}  
	   
}
