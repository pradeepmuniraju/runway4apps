package com.sok.runway.externalInterface.beans;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class SearchParameter
{
	private String name = null;
	private String value = null;
	
	public String getName()
	{
		return(name);
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getValue()
	{
		return(value);
	}
	
	public void setValue(String value)
	{
		this.value = value;
	}
}