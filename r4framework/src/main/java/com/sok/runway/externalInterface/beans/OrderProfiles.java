package com.sok.runway.externalInterface.beans;

import java.util.*;
import com.sok.runway.externalInterface.beans.shared.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class OrderProfiles
{
	private OrderProfile[] profiles = null;
	
	private String orderid = null;
	
	private String error = null;
	
	public String getError()
	{
		return(error);
	}	
	
	public void setError(String error)
	{
		this.error = error;
	}
	
	public String getContactID()
	{
		return(orderid);
	}	
	
	public void setOrderID(String orderid)
	{
		this.orderid = orderid;
	}		
	
	public OrderProfile[] getProfiles()
	{
		return(profiles);
	}	
	
	public void setProfiles(OrderProfile[] profiles)
	{
		this.profiles = profiles;
	}
}