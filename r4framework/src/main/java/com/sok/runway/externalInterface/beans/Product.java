package com.sok.runway.externalInterface.beans;

import com.sok.runway.externalInterface.beans.shared.*;
import java.util.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class Product
{
	private String productid = null;
	private String name = null;
	private String productnum = null;
	private String description = null;
	private String summary = null;
	private String category = null;
	private double basecost = 0;
	private double margin = 0;
	private double cost = 0;
	private double gst = 0;
	private double totalcost = 0;
	private Date createddate = null;
	private String createdby = null;
	private Date modifieddate = null;
	private String modifiedby = null;
	private String productgroupid = null;
	private String image = null;
	private String thumbnailimage = null;
	private String productdetails = null;
	private String active = null;
	private String variableprice = null;
	private String stocklevel = null;
	private String pricingtype = null;
	private String groupid = null;
	private Date availabledate = null;
	private String gstexempt = null;
	private String suppliercompanyid = null;
	
	private ProductGroup productgroup = null;
	
	private String error = null;        
        
	public String getError()
	{
		return(error);
	}	
	
	public void setError(String error)
	{
		this.error = error;
	}	        
        
	public String getProductID()
	{
		return(productid);
	}	
	
	public void setProductID(String productid)
	{
		this.productid = productid;
	}	
	
	public String getName()
	{
		return(name);
	}	
	
	public void setName(String name)
	{
		this.name = name;
	}	
	
	public String getProductNum()
	{
		return(productnum);
	}	
	
	public void setProductNum(String productnum)
	{
		this.productnum = productnum;
	}	
	
	public String getDescription()
	{
		return(description);
	}	
	
	public void setDescription(String description)
	{
		this.description = description;
	}	
	
	public String getSummary()
	{
		return(summary);
	}	
	
	public void setSummary(String summary)
	{
		this.summary = summary;
	}	
	
	public String getCategory()
	{
		return(category);
	}	
	
	public void setCategory(String category)
	{
		this.category = category;
	}	
	
	public double getBaseCost()
	{
		return(basecost);
	}	
	
	public void setBaseCost(double basecost)
	{
		this.basecost = basecost;
	}	
	
	public double getMargin()
	{
		return(margin);
	}	
	
	public void setMargin(double margin)
	{
		this.margin = margin;
	}	
	
	public double getCost()
	{
		return(cost);
	}	
	
	public void setCost(double cost)
	{
		this.cost = cost;
	}	
	
	public double getGST()
	{
		return(gst);
	}	
	
	public void setGST(double gst)
	{
		this.gst = gst;
	}	
	
	public double getTotalCost()
	{
		return(totalcost);
	}	
	
	public void setTotalCost(double totalcost)
	{
		this.totalcost = totalcost;
	}	
	
	public Date getCreatedDate()
	{
		return(createddate);
	}	
	
	public void setCreatedDate(Date createddate)
	{
		this.createddate = createddate;
	}	
	
	public String getCreatedBy()
	{
		return(createdby);
	}	
	
	public void setCreatedBy(String createdby)
	{
		this.createdby = createdby;
	}	
	
	public Date getModifiedDate()
	{
		return(modifieddate);
	}	
	
	public void setModifiedDate(Date modifieddate)
	{
		this.modifieddate = modifieddate;
	}	
	
	public String getModifiedBy()
	{
		return(modifiedby);
	}	
	
	public void setModifiedBy(String modifiedby)
	{
		this.modifiedby = modifiedby;
	}	
	
	public String getProductGroupID()
	{
		return(productgroupid);
	}	
	
	public void setProductGroupID(String productgroupid)
	{
		this.productgroupid = productgroupid;
	}	
	
	public String getImage()
	{
		return(image);
	}	
	
	public void setImage(String image)
	{
		this.image = image;
	}	
	
	public String getThumbnailImage()
	{
		return(thumbnailimage);
	}	
	
	public void setThumbnailImage(String thumbnailimage)
	{
		this.thumbnailimage = thumbnailimage;
	}	
	
	public String getProductDetails()
	{
		return(productdetails);
	}	
	
	public void setProductDetails(String productdetails)
	{
		this.productdetails = productdetails;
	}	
	
	public String getActive()
	{
		return(active);
	}	
	
	public void setActive(String active)
	{
		this.active = active;
	}	
	
	public String getVariablePrice()
	{
		return(variableprice);
	}	
	
	public void setVariablePrice(String variableprice)
	{
		this.variableprice = variableprice;
	}	
	
	public String getStockLevel()
	{
		return(stocklevel);
	}	
	
	public void setStockLevel(String stocklevel)
	{
		this.stocklevel = stocklevel;
	}	
	
	public String getPricingType()
	{
		return(pricingtype);
	}	
	
	public void setPricingType(String pricingtype)
	{
		this.pricingtype = pricingtype;
	}	
	
	public String getGroupID()
	{
		return(groupid);
	}	
	
	public void setGroupID(String groupid)
	{
		this.groupid = groupid;
	}	
	
	public Date getAvailableDate()
	{
		return(availabledate);
	}	
	
	public void setAvailableDate(Date availabledate)
	{
		this.availabledate = availabledate;
	}	
	
	public String getGSTExempt()
	{
		return(gstexempt);
	}	
	
	public void setGSTExempt(String gstexempt)
	{
		this.gstexempt = gstexempt;
	}	
	
	public String getSupplierCompanyID()
	{
		return(suppliercompanyid);
	}	
	
	public void setSupplierCompanyID(String suppliercompanyid)
	{
		this.suppliercompanyid = suppliercompanyid;
	}	
	
   public ProductGroup getProductGroup()
   {
      return(productgroup);
   }  
   
   public void setProductGroup(ProductGroup productgroup)
   {
      this.productgroup = productgroup;
   }  	

}
