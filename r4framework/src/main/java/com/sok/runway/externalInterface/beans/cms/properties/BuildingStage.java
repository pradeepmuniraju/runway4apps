package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.framework.GenRow;
import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.wordnik.swagger.annotations.ApiClass;

@XmlAccessorType(XmlAccessType.NONE)
//@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement(name="BuildingStage")
//@XmlType(propOrder={"productNum", "description","address","interactiveMap", "estSettlementDate","createdBy","createdByName","createdDate","modifiedBy","modifiedByName","modifiedDate","error","productID"})
@ApiClass(description="Deprecated: ProductID, EstateProductID")
public class BuildingStage extends SimpleBuildingStage {

/* 
 * Name Latitude Longitude Estate Location ACtive EstSettlementDate Description ProductNum
 * 
 */
	private String productNum, description, createdBy, modifiedBy;
	private Address address;
	private Date createdDate, modifiedDate, estSettlementDate;
	private String error; 
	private PersonName modifiedByName = null, createdByName = null;
	
	private InteractiveMap interactiveMap;
	private InteractiveMap interactiveZoomMap;
	
	public BuildingStage() {}
	public BuildingStage(String error) {
		this.error = error;
	}
	public BuildingStage(GenRow g) {
		this.building = new Building();
		building.setBuildingID(g.getString("ProductID"));
		this.name = g.getString("Name");
	}

	@Deprecated
	@XmlElement(name="ProductID")
	public String getProductID() {
		return buildingStageID;
	}
	@Deprecated
	public void setProductID(String productID) {
		this.buildingStageID = productID;
	}
	@XmlElement(name="ProductNum")
	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}
	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName(){
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedByName = modifiedbyname;
	}
	
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	@XmlElement(name="EstSettlementDate")
	public Date getEstSettlementDate() {
		return estSettlementDate;
	}
	public void setEstSettlementDate(Date estSettlementDate) {
		this.estSettlementDate = estSettlementDate;
	}
	
	@XmlElement(name="Address")
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}

	@XmlElement(name="Error")
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	@XmlElement(name="InteractiveMap")
	public InteractiveMap getInteractiveMap() {
		return interactiveMap;
	}
	public void setInteractiveMap(InteractiveMap interactiveMap) {
		this.interactiveMap = interactiveMap;
	}
	
	@XmlElement(name="InteractiveZoomMap")
	public InteractiveMap getInteractiveZoomMap() {
		return interactiveZoomMap;
	}
	public void setInteractiveZoomMap(InteractiveMap interactiveZoomMap) {
		this.interactiveZoomMap = interactiveZoomMap;
	}
}
