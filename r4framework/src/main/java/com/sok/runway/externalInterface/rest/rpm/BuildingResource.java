package com.sok.runway.externalInterface.rest.rpm;

import java.sql.SQLIntegrityConstraintViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;	
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.DatabaseException;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.crm.activity.ActivityLogger.ActionType;
import com.sok.runway.crm.cms.properties.entities.BuildingEntity;
import com.sok.runway.externalInterface.beans.cms.properties.Building;
import com.sok.runway.externalInterface.beans.cms.properties.BuildingContact;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.runway.externalInterface.rest.rpm.ApartmentResource;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.BuildingService;
import com.sok.service.exception.AccessException;
import com.sok.service.exception.NotAuthenticatedException;
import com.sok.service.exception.NotFoundException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

/**
 * Extend and annotate the existing BuildingResource implementation for use with the V2 (Public/Documented) API.
 * 
 * This is seperate from the v1 api endpoint to limit the library requirements on those apps which do not yet use the 
 * v2 api. 
 * 
 * @author Michael Dekmetzian
 * @date 2 January 2013
 */
@Path("/buildings")
@Api(value = "/buildings", description = "Retrieve Buildling Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class BuildingResource /* extends com.sok.runway.externalInterface.resources.cms.properties.BuildingResource */ {

	static final Logger logger = LoggerFactory.getLogger(BuildingResource.class);
	@Context HttpServletRequest request;
	
	public BuildingResource() {}
	public BuildingResource(HttpServletRequest request) { this.request = request; }
	
	@GET
	@ApiOperation(value = "List all buildings", notes="Optionally filtering by modified date", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Building", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	@XmlElementWrapper(name = "Buildings")
	public List<Building> getBuildings(
			@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = false) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active buildings", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			logger.debug("getBuildings({})", modifiedSince, modifiedSince);
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		return BuildingService.getInstance().getBuildings(request, mod, active);
	}
	
	@GET
	@Path("/{buildingID}")
	@ApiOperation(value = "Return a particular building", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Building")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public Building getBuilding(
			@ApiParam(value = "The BuildingID value as returned from the API", required = true) @PathParam("buildingID") final String buildingID) {
		
		return BuildingService.getInstance().getBuilding(request, buildingID);
	}
	
	@POST
	@Path("/{buildingID}/contacts")
	@ApiOperation(value = "Add a building contact", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.BuildingContact")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public BuildingContact addBuildingContact(@ApiParam(value = "The BuildingID value as returned from the API", required = true) @PathParam("buildingID") final String buildingID, BuildingContact buildingContact) {
		if(!StringUtils.equals(buildingContact.getBuildingID(), buildingID)) {
			throw new IllegalArgumentException("BuildingIDs must match"); 
		}
		BuildingEntity ee = new BuildingEntity(request); 
		ee.load(buildingID); 
		if(!ee.isLoaded()) {
			throw new NotFoundException("The Building you requested could not be found: " + buildingID);
		}
		if(!ee.canEdit()) {
			throw new AccessException("You do not have permission to edit the building with id " + buildingID);
		}
		if(StringUtils.isBlank(buildingContact.getContactID())) {
			throw new IllegalArgumentException("ContactID must be provided");
		}
		if(StringUtils.isBlank(buildingContact.getLinkType())) {
			throw new IllegalArgumentException("LinkType must be provided");
		}
		GenRow ec = new GenRow(); 
		ec.setRequest(request); 
		ec.setViewSpec("properties/ApartmentContactView"); 
		ec.setParameter("BuildingID", buildingContact.getBuildingID()); 
		ec.setParameter("ContactID", buildingContact.getContactID());
		ec.setParameter("LinkType", buildingContact.getLinkType()); 
		ec.doAction(com.sok.framework.generation.GenerationKeys.SELECTFIRST);
		
		UserBean user = UserService.getInstance().getCurrentUser(request); 
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		if(!ec.isSuccessful()) {
			ec.setToNewID("ApartmentContactID"); //BuildingContactID
			ec.setParameter("CreatedDate","NOW"); 
			ec.setParameter("CreatedBy", user.getUserID());
			ec.setParameter("ModifiedDate","NOW"); 
			ec.setParameter("ModifiedBy", user.getUserID());
			ec.setParameter("DefaultLink", buildingContact.isDefaultLink()?"Y":"N");
			try { 
				ec.doAction(com.sok.framework.generation.GenerationKeys.INSERT);
				ActivityLogger.getActivityLogger().logActivity(ee, ActionType.Updated);
				ec.doAction(com.sok.framework.generation.GenerationKeys.SELECT); // do this to set the CreatedBy / Date fields to actual values
			} catch (DatabaseException de) {
				Throwable c = de.getCause(); 
				if(c != null && c instanceof SQLIntegrityConstraintViolationException) {
					ec.clear(); 
					ec.setParameter("BuildingID", buildingContact.getBuildingID()); 
					ec.setParameter("ContactID", buildingContact.getContactID());
					ec.setParameter("LinkType", buildingContact.getLinkType()); 
					ec.doAction(com.sok.framework.generation.GenerationKeys.SELECTFIRST);
					if(!ec.isSuccessful()) {
						// if not successful the problem was something else
						throw de; 
					}
				} else {
					throw de; 
				}
			}
		}
		buildingContact.setBuildingContactID(ec.getData("ApartmentContactID"));
		buildingContact.setCreatedBy(ec.getData("CreatedBy"));
		buildingContact.setCreatedDate(ec.getDate("CreatedDate"));
		buildingContact.setCreatedByName(new PersonName(ec.getData("OwnFirstName"), ec.getData("OwnLastName")));
		
		buildingContact.setModifiedBy(ec.getData("ModifiedBy"));
		buildingContact.setModifiedDate(ec.getDate("ModifiedDate"));
		buildingContact.setModifiedByName(new PersonName(ec.getData("OwnFirstName"), ec.getData("OwnLastName")));
		return buildingContact; 
	}
	
	@PUT
	@Path("/{buildingID}/contacts/{buildingContactID}")
	@ApiOperation(value = "Updates a building contact", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.BuildingContact")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public BuildingContact updateBuildingContact(@ApiParam(value = "The BuildingID value as returned from the API", required = true) @PathParam("buildingID") final String buildingID, 
											 @ApiParam(value = "The BuildingContactID value as returned from the API", required = true) @PathParam("buildingContactID") final String buildingContactID,
											 BuildingContact buildingContact) {
		if(StringUtils.isBlank(buildingID) || !StringUtils.equals(buildingContact.getBuildingID(), buildingID)) {
			throw new IllegalArgumentException("BuildingIDs must match"); 
		}
		if(StringUtils.isBlank(buildingContactID) || !StringUtils.equals(buildingContact.getBuildingContactID(), buildingContactID)) {
			throw new IllegalArgumentException("BuildingContactIDs must match"); 
		}
		BuildingEntity ee = new BuildingEntity(request); 
		ee.load(buildingID); 
		if(!ee.isLoaded()) {
			throw new NotFoundException("The Building you requested could not be found: " + buildingID);
		}
		if(!ee.canEdit()) {
			throw new AccessException("You do not have permission to edit the building with id " + buildingID);
		}
		if(StringUtils.isBlank(buildingContact.getContactID())) {
			throw new IllegalArgumentException("ContactID must be provided");
		}
		if(StringUtils.isBlank(buildingContact.getLinkType())) {
			throw new IllegalArgumentException("LinkType must be provided");
		}
		UserBean user = UserService.getInstance().getCurrentUser(request); 
		if(user == null) {
			throw new NotAuthenticatedException("You are not logged in");
		}
		
		GenRow ec = new GenRow(); 
		ec.setRequest(request); 
		ec.setViewSpec("properties/ApartmentContactView"); 
		ec.setParameter("ApartmentContactID", buildingContact.getBuildingContactID()); 
		ec.doAction(GenerationKeys.SELECT);
		
		if(!ec.isSuccessful()) {
			throw new NotFoundException("The building contact was not found with id " + buildingContact.getBuildingContactID());
		}		
		if(!StringUtils.equals(buildingContact.getContactID(), ec.getData("ContactID"))) {
			throw new UnsupportedOperationException("Changing of Contacts on existing links are not supported. You should remove and re-add the link with the appropriate contact to ensure all actions are logged correctly.");
		}
		
		ec.setParameter("ModifiedDate","NOW"); 
		ec.setParameter("ModifiedBy", user.getUserID());
		ec.setParameter("DefaultLink", buildingContact.isDefaultLink()?"Y":"N");
		ec.setParameter("LinkType", buildingContact.getLinkType()); 
		
		if(buildingContact.isDefaultLink()) {
			GenRow up = new GenRow(); 
			up.setTableSpec("properties/BuildingContacts"); 
			up.setRequest(request); 
			up.setParameter("ON-BuildingID", buildingContact.getBuildingID());
			up.setParameter("DefaultLink", "N");
			up.doAction(GenerationKeys.UPDATEALL);
		}
		
		ec.doAction(GenerationKeys.UPDATE); 
		if(!ec.isSuccessful()) {
			throw new RuntimeException("An unexpected error has occurred " + ec.getError());
		}
		
		buildingContact.setBuildingContactID(ec.getData("BuildingContactID"));
		buildingContact.setCreatedBy(ec.getData("CreatedBy"));
		buildingContact.setCreatedDate(ec.getDate("CreatedDate"));
		buildingContact.setCreatedByName(new PersonName(ec.getData("OwnFirstName"), ec.getData("OwnLastName")));
		
		buildingContact.setModifiedBy(ec.getData("ModifiedBy"));
		buildingContact.setModifiedDate(ec.getDate("ModifiedDate"));
		buildingContact.setModifiedByName(new PersonName(ec.getData("ModFirstName"), ec.getData("ModLastName")));
		
		return buildingContact; 
	}
	
	@DELETE
	@ApiOperation(value = "Deletes a building contact")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid building id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted to modify this building"), @ApiError(code = 404, reason = "Building Not Found") })
	@Path("/{buildingID}/contacts/{buildingContactID}")
	public void deleteBuildingContact(@ApiParam(value = "The BuildingID value as returned from the API", required = true) @PathParam("buildingID") final String buildingID,
			@ApiParam(value = "The BuildingContactID value as returned from the API", required = true) @PathParam("buildingContactID") final String buildingContactID) {
		BuildingEntity ee = new BuildingEntity(request); 
		ee.load(buildingID); 
		if(!ee.isLoaded()) {
			throw new NotFoundException("The Building you requested could not be found: " + buildingID);
		}
		if(!ee.canEdit()) {
			throw new AccessException("You do not have permission to edit the building with id " + buildingID);
		}
		if(StringUtils.isBlank(buildingContactID)) {
			throw new IllegalArgumentException("BuildingContactID must be provided");
		}
		GenRow ec = new GenRow(); 
		ec.setRequest(request); 
		ec.setViewSpec("properties/ApartmentContactView"); 
		ec.setParameter("ApartmentContactID", buildingContactID);
		ec.doAction(com.sok.framework.generation.GenerationKeys.SELECT);
		
		if(ec.isSuccessful()) {
			if(!StringUtils.equals(ec.getData("BuildingID"), buildingID)) {
				throw new IllegalArgumentException("BuildingIDs must match"); 
			}
			ec.doAction(GenerationKeys.DELETE);
			ActivityLogger.getActivityLogger().logActivity(ee, ActionType.Updated);
		}
	}
	
	@ApiResource(value = "/{buildingID}/buildingstages", resourceClass="com.sok.runway.externalInterface.rest.rpm.BuildingStageResource")
	@Path("/{buildingID}/buildingstages")
	public BuildingStageResource getStageResource(
			@ApiParam(value = "The BuildingID value as returned from the API", required = true) @PathParam("buildingID") final String buildingID) {
		return new BuildingStageResource(request, buildingID);
	}
	
	@ApiResource(value = "/{buildingID}/apartements", resourceClass="com.sok.runway.externalInterface.rest.rpm.ApartmentResource")
	@Path("{buildingID}/apartments")
	public ApartmentResource getApartmentResource(@PathParam("buildingID") final String buildingID) {
		return new ApartmentResource(request, buildingID);
	}
	
	@GET
	@ApiResource(value = "/{buildingID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Building", notes="<p>This call will return a list of Albums and their Images for a given Building. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Album")
	@Path("/{buildingID}/albums")
	public AlbumResource getAlbumResource(@PathParam("buildingID") final String buildingID, 
			@ApiParam(value = "Document Sub Type", required = false) @QueryParam("DocumentSubType") String subType, 
			@ApiParam(value = "Album Type", required = false) @QueryParam("AlbumType") String albumType, 
			@ApiParam(value = "Album Publsihed for", allowableValues="Website", required=false, defaultValue="") @DefaultValue("") @QueryParam("Publish") String publish) {
		return new AlbumResource(request,  buildingID, albumType, publish, subType);
	}
	
}
