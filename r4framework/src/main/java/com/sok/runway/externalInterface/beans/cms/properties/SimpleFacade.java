package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Facade")
@XmlType(propOrder={"facadeID","name", "price" })
public abstract class SimpleFacade {

	protected String facadeID, name;
	protected double price;
	
	
	@XmlElement(name="FacadeID")
	public String getFacadeID() {
		return facadeID;
	}
	public void setFacadeID(String facadeID) {
		this.facadeID = facadeID;
	}
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="Price")
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double d) {
		this.price = d;
		
	}
}