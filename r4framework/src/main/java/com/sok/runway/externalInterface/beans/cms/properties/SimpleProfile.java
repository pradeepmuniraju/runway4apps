package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Profile")
public class SimpleProfile {
	protected String profileID, name, type;
	
	public SimpleProfile() {}
	
	public SimpleProfile(GenRow profile) {
		profileID = profile.getData("SurveyID");
		name = profile.getData("Name");
		type = profile.getData("ProfileType");
	}
	
	@XmlElement(name="ProfileID")
	public String getProfileID() {
		return profileID;
	}
	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="Type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
