package com.sok.runway.externalInterface.rest.validation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/*
@Path("/test")
@Api(value="/test", description = "Operations about test")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
*/
public class TestResource {
	
	@GET
	@ApiOperation(value = "Return a test object", notes = "Just a test", responseClass = "com.sok.runway.externalInterface.rest.validation.TestResource.Test")
	public Test doTest() {
		return new Test();
	}
	
	@XmlRootElement(name="Test")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Test {
		public String id = "TEST";
		public String name = "Test Object";
		public Double value = new Double(20.37);
	}	
}
