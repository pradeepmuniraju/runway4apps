package com.sok.runway.externalInterface.resources.cms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.beans.cms.CMSUser;
import com.sok.service.cms.WebsiteService;

public class WebsiteUserResource {

	private static final Logger logger = LoggerFactory.getLogger(WebsiteUserResource.class);
	private static final WebsiteService websiteService = WebsiteService.getInstance();
	
	private final String cmsID;
	@Context HttpServletRequest request;
	public WebsiteUserResource(String cmsID, HttpServletRequest request) {
		this.cmsID = cmsID;
		this.request = request; 
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CMSUser> getCMSUsers() {
		logger.debug("getCMSUsers(request, cmsID={})", cmsID);
		return websiteService.getCMSUsers(request, cmsID);
	}
	
	@PUT
	@Path("{CMSUserID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CMSUser updateCMSUser(@PathParam("CMSUserID") String cmsUserID, CMSUser user) {
		logger.debug("updateCMSUser(request, user)");
		if(!cmsUserID.equals(user.getCMSUserID())) {
			throw new IllegalArgumentException("CMSUserIDs did not match");
		}
		return websiteService.updateCMSUser(request, user);
	}
	
	@DELETE
	@Path("{CMSUserID}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteCMSUser(@PathParam("CMSUserID") String CMSUserID) {
		logger.debug("deleteCMSUser(request, {}, {})", cmsID, CMSUserID);
		websiteService.deleteCMSUser(request, cmsID, CMSUserID);
	}
}
