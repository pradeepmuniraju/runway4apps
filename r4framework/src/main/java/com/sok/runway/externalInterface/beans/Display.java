package com.sok.runway.externalInterface.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.FIELD)
public class Display {
	
	public String ProductID;
	public String Name; 
	
	public Display() {} 
	public Display(GenRow row) {
		this.ProductID = row.getString("ProductID");
		this.Name = row.getString("Name");
	}
	public Display(String productID, String name) {
		this.ProductID = productID;
		this.Name = name;
	}
}
