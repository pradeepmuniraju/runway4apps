package com.sok.runway.externalInterface.beans.displays.shared;


/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class AddressDisplay
{
	private String streetLabel = null;
	private boolean streetNotUsed = false;
	private String street2Label = null;
	private boolean street2NotUsed = false;
	private String cityLabel = null;
	private boolean cityNotUsed = false;
	private String stateLabel = null;
	private boolean stateNotUsed = false;
	private String postcodeLabel = null;
	private boolean postcodeNotUsed = false;
	private String countryLabel = null;
	private boolean countryNotUsed = false;
	
	public String getStreetLabel()
	{
		return(streetLabel);
	}	
	
	public void setStreetLabel(String streetLabel)
	{
		this.streetLabel = streetLabel;
	}	
	
	public boolean getStreetNotUsed()
	{
		return(streetNotUsed);
	}	
	
	public void setStreetNotUsed(boolean streetNotUsed)
	{
		this.streetNotUsed = streetNotUsed;
	}	
	
	public String getStreet2Label()
	{
		return(street2Label);
	}	
	
	public void setStreet2Label(String street2Label)
	{
		this.street2Label = street2Label;
	}	
	
	public boolean getStreet2NotUsed()
	{
		return(street2NotUsed);
	}	
	
	public void setStreet2NotUsed(boolean street2NotUsed)
	{
		this.street2NotUsed = street2NotUsed;
	}	
	
	public String getCityLabel()
	{
		return(cityLabel);
	}	
	
	public void setCityLabel(String cityLabel)
	{
		this.cityLabel = cityLabel;
	}	
	
	public boolean getCityNotUsed()
	{
		return(cityNotUsed);
	}	
	
	public void setCityNotUsed(boolean cityNotUsed)
	{
		this.cityNotUsed = cityNotUsed;
	}	
	
	public String getStateLabel()
	{
		return(stateLabel);
	}	
	
	public void setStateLabel(String stateLabel)
	{
		this.stateLabel = stateLabel;
	}	
	
	public boolean getStateNotUsed()
	{
		return(stateNotUsed);
	}	
	
	public void setStateNotUsed(boolean stateNotUsed)
	{
		this.stateNotUsed = stateNotUsed;
	}	
	
	public String getPostcodeLabel()
	{
		return(postcodeLabel);
	}	
	
	public void setPostcodeLabel(String postcodeLabel)
	{
		this.postcodeLabel = postcodeLabel;
	}	
	
	public boolean getPostcodeNotUsed()
	{
		return(postcodeNotUsed);
	}	
	
	public void setPostcodeNotUsed(boolean postcodeNotUsed)
	{
		this.postcodeNotUsed = postcodeNotUsed;
	}	
	
	public String getCountryLabel()
	{
		return(countryLabel);
	}	
	
	public void setCountryLabel(String countryLabel)
	{
		this.countryLabel = countryLabel;
	}	
	
	public boolean getCountryNotUsed()
	{
		return(countryNotUsed);
	}	
	
	public void setCountryNotUsed(boolean countryNotUsed)
	{
		this.countryNotUsed = countryNotUsed;
	}	
}