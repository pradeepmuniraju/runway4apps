package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sok.runway.externalInterface.beans.shared.PersonName;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "Album")
//@XmlType(propOrder={"galleryID", "name","description","publisher", "createdBy","createdByName","createdDate","modifiedBy","modifiedByName","modifiedDate","active"})
public class Album {

	private String galleryID, name, description, createdBy, modifiedBy, active;
	private List<String> publisher;
	private Date createdDate, modifiedDate;
	private PersonName modifiedByName = null, createdByName = null;
	private List<Image>	images = new ArrayList<Image>();
	private String albumType = null;

	@XmlElement(name="AlbumID")
	public String getAlbumID() {
		return galleryID;
	}
	public void setAlbumID(String galleryID) {
		this.galleryID = galleryID;
	}
	
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@XmlElement(name="Publisher")
	public List<String> getPublisher() {
		return publisher;
	}
	public void setPublisher(List<String> publisher) {
		this.publisher = publisher;
	}
	
	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@XmlElement(name="ModifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@XmlElement(name="ModifiedByName")
	public PersonName getModifiedByName(){
		return(modifiedByName);
	}
	public void setModifiedByName(PersonName modifiedbyname) {
		this.modifiedByName = modifiedbyname;
	}
	
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	@XmlElement(name="Active")
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
	@XmlElement(name="Images")
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		if (images != null)
			this.images = images;
		else
			new ArrayList<Image>();
	}
	public void addImage(Image image) {
		if (image != null) this.images.add(image);
	}
	@XmlElement(name="Type")
	public String getAlbumType() {
		return albumType;
	}
	public void setAlbumType(String albumType) {
		this.albumType = albumType;
	}
}
