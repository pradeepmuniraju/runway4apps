package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.sok.framework.GenRow;

@XmlAccessorType(XmlAccessType.NONE)
@XmlSeeAlso(QuickIndexPackages.class)
@XmlRootElement(name="QuickIndexRealEstate")
public class QuickIndexRealEstate extends QuickIndexPackages {
	
	private double internalArea, externalArea, totalArea;
	public QuickIndexRealEstate() {
	}

	public QuickIndexRealEstate(GenRow row) {
		super(row);
		
		
		internalArea = row.getDouble("InternalArea");
		externalArea = row.getDouble("ExternalArea");
		
		if (totalArea == 0) totalArea = internalArea + externalArea;
		
	}
	
	@XmlElement(name="TotalArea")
	public double getTotalArea() {
		return totalArea;
	}

	public void setTotalArea(double totalArea) {
		this.totalArea = totalArea;
	}

	@XmlElement(name="InternalArea")
	public double getInternalArea() {
		return internalArea;
	}

	public void setInternalArea(double internalArea) {
		this.internalArea = internalArea;
	}

	@XmlElement(name="ExternalArea")
	public double getExternalArea() {
		return externalArea;
	}

	public void setExternalArea(double externalArea) {
		this.externalArea = externalArea;
	}

}
