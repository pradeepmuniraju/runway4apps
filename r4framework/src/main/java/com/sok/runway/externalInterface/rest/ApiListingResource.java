package com.sok.runway.externalInterface.rest;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.core.SwaggerContext;
import com.wordnik.swagger.jaxrs.listing.ApiListing;
import com.wordnik.swagger.jaxrs.JaxrsApiReader;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/api-docs")
@Api("/api-docs")
@Produces({"application/json","application/xml"})
public class ApiListingResource extends ApiListing {
	static {
		JaxrsApiReader.setFormatString("");
		/* handle a case where swagger is in a different class-loading context to the webapp code */
		try { 
			System.out.println("Comparing Swagger Context to API Listing Context:");
			System.out.println(SwaggerContext.class.getClassLoader());
			System.out.println(ApiListingResource.class.getClassLoader());
			
			if(!SwaggerContext.class.getClassLoader().equals(ApiListingResource.class.getClassLoader())) {
				System.out.println("Adding classloader to swagger context");
				SwaggerContext.registerClassLoader(ApiListingResource.class.getClassLoader());
			} else {
				System.out.println("Same class loader, continuing..");
			}
		} catch (Exception e) {
			System.out.println("Error registering class loader");
			e.printStackTrace();
			//JSPLogger.error(SwaggerContext.class, "Error registing swagger class loader", e);
		}
	}
}