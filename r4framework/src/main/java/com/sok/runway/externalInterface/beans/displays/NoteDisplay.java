package com.sok.runway.externalInterface.beans.displays;

import com.sok.runway.externalInterface.beans.displays.shared.*;
import java.util.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class NoteDisplay
{
	private String repuseridLabel = null;
	private boolean repuseridNotUsed = false;
	private String statusLabel = null;
	private boolean statusNotUsed = false;
	private String notedateLabel = null;
	private boolean notedateNotUsed = false;

	private String contactidLabel = null;
	private boolean contactidNotUsed = false;
	private String companyidLabel = null;
	private boolean companyidNotUsed = false;
	private String productgroupidLabel = null;
	private boolean productgroupidNotUsed = false;
	private String opportunityidLabel = null;
	private boolean opportunityidNotUsed = false;
	private String templatenameLabel = null;
	private boolean templatenameNotUsed = false;
	private String typeLabel = null;
	private boolean typeNotUsed = false;
	private String completedLabel = null;
	private boolean completedNotUsed = false;
	private String sentdateLabel = null;
	private boolean sentdateNotUsed = false;
	private String templateusedLabel = null;
	private boolean templateusedNotUsed = false;
	private String cmsusedLabel = null;
	private boolean cmsusedNotUsed = false;
	private String campaignLabel = null;
	private boolean campaignNotUsed = false;
	private String campaignidLabel = null;
	private boolean campaignidNotUsed = false;
	private String subjectLabel = null;
	private boolean subjectNotUsed = false;
	private String bodyLabel = null;
	private boolean bodyNotUsed = false;
	private String htmlbodyLabel = null;
	private boolean htmlbodyNotUsed = false;
	private String emailtoLabel = null;
	private boolean emailtoNotUsed = false;
	private String emailfromLabel = null;
	private boolean emailfromNotUsed = false;
	private String emailccLabel = null;
	private boolean emailccNotUsed = false;
	private String emailbccLabel = null;
	private boolean emailbccNotUsed = false;
	private String durationLabel = null;
	private boolean durationNotUsed = false;
	private String notetimeLabel = null;
	private boolean notetimeNotUsed = false;
	private String callstatusLabel = null;
	private boolean callstatusNotUsed = false;
	private String appointmentstatusLabel = null;
	private boolean appointmentstatusNotUsed = false;
	private String groupidLabel = null;
	private boolean groupidNotUsed = false;
	private String notecodeLabel = null;
	private boolean notecodeNotUsed = false;

	private String venueLabel = null;
	private boolean venueNotUsed = false;
	private String urlLabel = null;
	private boolean urlNotUsed = false;
	private String mastertemplateidLabel = null;
	private boolean mastertemplateidNotUsed = false;

	private String enddateLabel = null;
	private boolean enddateNotUsed = false;
	private String processstageidLabel = null;
	private boolean processstageidNotUsed = false;
	private String approvalLabel = null;
	private boolean approvalNotUsed = false;	
	
	public String getRepUserIDLabel()
	{
		return(repuseridLabel);
	}	
	
	public void setRepUserIDLabel(String repuseridLabel)
	{
		this.repuseridLabel = repuseridLabel;
	}	
	
	public boolean getRepUserIDNotUsed()
	{
		return(repuseridNotUsed);
	}	
	
	public void setRepUserIDNotUsed(boolean repuseridNotUsed)
	{
		this.repuseridNotUsed = repuseridNotUsed;
	}	
	
	public String getStatusLabel()
	{
		return(statusLabel);
	}	
	
	public void setStatusLabel(String statusLabel)
	{
		this.statusLabel = statusLabel;
	}	
	
	public boolean getStatusNotUsed()
	{
		return(statusNotUsed);
	}	
	
	public void setStatusNotUsed(boolean statusNotUsed)
	{
		this.statusNotUsed = statusNotUsed;
	}	
	
	public String getNoteDateLabel()
	{
		return(notedateLabel);
	}	
	
	public void setNoteDateLabel(String notedateLabel)
	{
		this.notedateLabel = notedateLabel;
	}	
	
	public boolean getNoteDateNotUsed()
	{
		return(notedateNotUsed);
	}	
	
	public void setNoteDateNotUsed(boolean notedateNotUsed)
	{
		this.notedateNotUsed = notedateNotUsed;
	}	
	
	public String getContactIDLabel()
	{
		return(contactidLabel);
	}	
	
	public void setContactIDLabel(String contactidLabel)
	{
		this.contactidLabel = contactidLabel;
	}	
	
	public boolean getContactIDNotUsed()
	{
		return(contactidNotUsed);
	}	
	
	public void setContactIDNotUsed(boolean contactidNotUsed)
	{
		this.contactidNotUsed = contactidNotUsed;
	}	
	
	public String getCompanyIDLabel()
	{
		return(companyidLabel);
	}	
	
	public void setCompanyIDLabel(String companyidLabel)
	{
		this.companyidLabel = companyidLabel;
	}	
	
	public boolean getCompanyIDNotUsed()
	{
		return(companyidNotUsed);
	}	
	
	public void setCompanyIDNotUsed(boolean companyidNotUsed)
	{
		this.companyidNotUsed = companyidNotUsed;
	}	
	
	public String getProductGroupIDLabel()
	{
		return(productgroupidLabel);
	}	
	
	public void setProductGroupIDLabel(String productgroupidLabel)
	{
		this.productgroupidLabel = productgroupidLabel;
	}	
	
	public boolean getProductGroupIDNotUsed()
	{
		return(productgroupidNotUsed);
	}	
	
	public void setProductGroupIDNotUsed(boolean productgroupidNotUsed)
	{
		this.productgroupidNotUsed = productgroupidNotUsed;
	}	
	
	public String getOpportunityIDLabel()
	{
		return(opportunityidLabel);
	}	
	
	public void setOpportunityIDLabel(String opportunityidLabel)
	{
		this.opportunityidLabel = opportunityidLabel;
	}	
	
	public boolean getOpportunityIDNotUsed()
	{
		return(opportunityidNotUsed);
	}	
	
	public void setOpportunityIDNotUsed(boolean opportunityidNotUsed)
	{
		this.opportunityidNotUsed = opportunityidNotUsed;
	}	
	
	public String getTemplateNameLabel()
	{
		return(templatenameLabel);
	}	
	
	public void setTemplateNameLabel(String templatenameLabel)
	{
		this.templatenameLabel = templatenameLabel;
	}	
	
	public boolean getTemplateNameNotUsed()
	{
		return(templatenameNotUsed);
	}	
	
	public void setTemplateNameNotUsed(boolean templatenameNotUsed)
	{
		this.templatenameNotUsed = templatenameNotUsed;
	}	
	
	public String getTypeLabel()
	{
		return(typeLabel);
	}	
	
	public void setTypeLabel(String typeLabel)
	{
		this.typeLabel = typeLabel;
	}	
	
	public boolean getTypeNotUsed()
	{
		return(typeNotUsed);
	}	
	
	public void setTypeNotUsed(boolean typeNotUsed)
	{
		this.typeNotUsed = typeNotUsed;
	}	
	
	public String getCompletedLabel()
	{
		return(completedLabel);
	}	
	
	public void setCompletedLabel(String completedLabel)
	{
		this.completedLabel = completedLabel;
	}	
	
	public boolean getCompletedNotUsed()
	{
		return(completedNotUsed);
	}	
	
	public void setCompletedNotUsed(boolean completedNotUsed)
	{
		this.completedNotUsed = completedNotUsed;
	}	
	
	public String getSentDateLabel()
	{
		return(sentdateLabel);
	}	
	
	public void setSentDateLabel(String sentdateLabel)
	{
		this.sentdateLabel = sentdateLabel;
	}	
	
	public boolean getSentDateNotUsed()
	{
		return(sentdateNotUsed);
	}	
	
	public void setSentDateNotUsed(boolean sentdateNotUsed)
	{
		this.sentdateNotUsed = sentdateNotUsed;
	}	
	
	public String getTemplateUsedLabel()
	{
		return(templateusedLabel);
	}	
	
	public void setTemplateUsedLabel(String templateusedLabel)
	{
		this.templateusedLabel = templateusedLabel;
	}	
	
	public boolean getTemplateUsedNotUsed()
	{
		return(templateusedNotUsed);
	}	
	
	public void setTemplateUsedNotUsed(boolean templateusedNotUsed)
	{
		this.templateusedNotUsed = templateusedNotUsed;
	}	
	
	public String getCMSUsedLabel()
	{
		return(cmsusedLabel);
	}	
	
	public void setCMSUsedLabel(String cmsusedLabel)
	{
		this.cmsusedLabel = cmsusedLabel;
	}	
	
	public boolean getCMSUsedNotUsed()
	{
		return(cmsusedNotUsed);
	}	
	
	public void setCMSUsedNotUsed(boolean cmsusedNotUsed)
	{
		this.cmsusedNotUsed = cmsusedNotUsed;
	}	
	
	public String getCampaignLabel()
	{
		return(campaignLabel);
	}	
	
	public void setCampaignLabel(String campaignLabel)
	{
		this.campaignLabel = campaignLabel;
	}	
	
	public boolean getCampaignNotUsed()
	{
		return(campaignNotUsed);
	}	
	
	public void setCampaignNotUsed(boolean campaignNotUsed)
	{
		this.campaignNotUsed = campaignNotUsed;
	}	
	
	public String getCampaignIDLabel()
	{
		return(campaignidLabel);
	}	
	
	public void setCampaignIDLabel(String campaignidLabel)
	{
		this.campaignidLabel = campaignidLabel;
	}	
	
	public boolean getCampaignIDNotUsed()
	{
		return(campaignidNotUsed);
	}	
	
	public void setCampaignIDNotUsed(boolean campaignidNotUsed)
	{
		this.campaignidNotUsed = campaignidNotUsed;
	}	
	
	public String getSubjectLabel()
	{
		return(subjectLabel);
	}	
	
	public void setSubjectLabel(String subjectLabel)
	{
		this.subjectLabel = subjectLabel;
	}	
	
	public boolean getSubjectNotUsed()
	{
		return(subjectNotUsed);
	}	
	
	public void setSubjectNotUsed(boolean subjectNotUsed)
	{
		this.subjectNotUsed = subjectNotUsed;
	}	
	
	public String getBodyLabel()
	{
		return(bodyLabel);
	}	
	
	public void setBodyLabel(String bodyLabel)
	{
		this.bodyLabel = bodyLabel;
	}	
	
	public boolean getBodyNotUsed()
	{
		return(bodyNotUsed);
	}	
	
	public void setBodyNotUsed(boolean bodyNotUsed)
	{
		this.bodyNotUsed = bodyNotUsed;
	}	
	
	public String getHTMLBodyLabel()
	{
		return(htmlbodyLabel);
	}	
	
	public void setHTMLBodyLabel(String htmlbodyLabel)
	{
		this.htmlbodyLabel = htmlbodyLabel;
	}	
	
	public boolean getHTMLBodyNotUsed()
	{
		return(htmlbodyNotUsed);
	}	
	
	public void setHTMLBodyNotUsed(boolean htmlbodyNotUsed)
	{
		this.htmlbodyNotUsed = htmlbodyNotUsed;
	}	
	
	public String getEmailToLabel()
	{
		return(emailtoLabel);
	}	
	
	public void setEmailToLabel(String emailtoLabel)
	{
		this.emailtoLabel = emailtoLabel;
	}	
	
	public boolean getEmailToNotUsed()
	{
		return(emailtoNotUsed);
	}	
	
	public void setEmailToNotUsed(boolean emailtoNotUsed)
	{
		this.emailtoNotUsed = emailtoNotUsed;
	}	
	
	public String getEmailFromLabel()
	{
		return(emailfromLabel);
	}	
	
	public void setEmailFromLabel(String emailfromLabel)
	{
		this.emailfromLabel = emailfromLabel;
	}	
	
	public boolean getEmailFromNotUsed()
	{
		return(emailfromNotUsed);
	}	
	
	public void setEmailFromNotUsed(boolean emailfromNotUsed)
	{
		this.emailfromNotUsed = emailfromNotUsed;
	}	
	
	public String getEmailCCLabel()
	{
		return(emailccLabel);
	}	
	
	public void setEmailCCLabel(String emailccLabel)
	{
		this.emailccLabel = emailccLabel;
	}	
	
	public boolean getEmailCCNotUsed()
	{
		return(emailccNotUsed);
	}	
	
	public void setEmailCCNotUsed(boolean emailccNotUsed)
	{
		this.emailccNotUsed = emailccNotUsed;
	}	
	
	public String getEmailBCCLabel()
	{
		return(emailbccLabel);
	}	
	
	public void setEmailBCCLabel(String emailbccLabel)
	{
		this.emailbccLabel = emailbccLabel;
	}	
	
	public boolean getEmailBCCNotUsed()
	{
		return(emailbccNotUsed);
	}	
	
	public void setEmailBCCNotUsed(boolean emailbccNotUsed)
	{
		this.emailbccNotUsed = emailbccNotUsed;
	}	
	
	public String getDurationLabel()
	{
		return(durationLabel);
	}	
	
	public void setDurationLabel(String durationLabel)
	{
		this.durationLabel = durationLabel;
	}	
	
	public boolean getDurationNotUsed()
	{
		return(durationNotUsed);
	}	
	
	public void setDurationNotUsed(boolean durationNotUsed)
	{
		this.durationNotUsed = durationNotUsed;
	}	
	
	public String getNoteTimeLabel()
	{
		return(notetimeLabel);
	}	
	
	public void setNoteTimeLabel(String notetimeLabel)
	{
		this.notetimeLabel = notetimeLabel;
	}	
	
	public boolean getNoteTimeNotUsed()
	{
		return(notetimeNotUsed);
	}	
	
	public void setNoteTimeNotUsed(boolean notetimeNotUsed)
	{
		this.notetimeNotUsed = notetimeNotUsed;
	}	
	
	public String getCallStatusLabel()
	{
		return(callstatusLabel);
	}	
	
	public void setCallStatusLabel(String callstatusLabel)
	{
		this.callstatusLabel = callstatusLabel;
	}	
	
	public boolean getCallStatusNotUsed()
	{
		return(callstatusNotUsed);
	}	
	
	public void setCallStatusNotUsed(boolean callstatusNotUsed)
	{
		this.callstatusNotUsed = callstatusNotUsed;
	}	
	
	public String getAppointmentStatusLabel()
	{
		return(appointmentstatusLabel);
	}	
	
	public void setAppointmentStatusLabel(String appointmentstatusLabel)
	{
		this.appointmentstatusLabel = appointmentstatusLabel;
	}	
	
	public boolean getAppointmentStatusNotUsed()
	{
		return(appointmentstatusNotUsed);
	}	
	
	public void setAppointmentStatusNotUsed(boolean appointmentstatusNotUsed)
	{
		this.appointmentstatusNotUsed = appointmentstatusNotUsed;
	}	
	
	public String getGroupIDLabel()
	{
		return(groupidLabel);
	}	
	
	public void setGroupIDLabel(String groupidLabel)
	{
		this.groupidLabel = groupidLabel;
	}	
	
	public boolean getGroupIDNotUsed()
	{
		return(groupidNotUsed);
	}	
	
	public void setGroupIDNotUsed(boolean groupidNotUsed)
	{
		this.groupidNotUsed = groupidNotUsed;
	}	
	
	public String getNoteCodeLabel()
	{
		return(notecodeLabel);
	}	
	
	public void setNoteCodeLabel(String notecodeLabel)
	{
		this.notecodeLabel = notecodeLabel;
	}	
	
	public boolean getNoteCodeNotUsed()
	{
		return(notecodeNotUsed);
	}	
	
	public void setNoteCodeNotUsed(boolean notecodeNotUsed)
	{
		this.notecodeNotUsed = notecodeNotUsed;
	}	
	
	public String getVenueLabel()
	{
		return(venueLabel);
	}	
	
	public void setVenueLabel(String venueLabel)
	{
		this.venueLabel = venueLabel;
	}	
	
	public boolean getVenueNotUsed()
	{
		return(venueNotUsed);
	}	
	
	public void setVenueNotUsed(boolean venueNotUsed)
	{
		this.venueNotUsed = venueNotUsed;
	}	
	
	public String getURLLabel()
	{
		return(urlLabel);
	}	
	
	public void setURLLabel(String urlLabel)
	{
		this.urlLabel = urlLabel;
	}	
	
	public boolean getURLNotUsed()
	{
		return(urlNotUsed);
	}	
	
	public void setURLNotUsed(boolean urlNotUsed)
	{
		this.urlNotUsed = urlNotUsed;
	}	
	
	public String getMasterTemplateIDLabel()
	{
		return(mastertemplateidLabel);
	}	
	
	public void setMasterTemplateIDLabel(String mastertemplateidLabel)
	{
		this.mastertemplateidLabel = mastertemplateidLabel;
	}	
	
	public boolean getMasterTemplateIDNotUsed()
	{
		return(mastertemplateidNotUsed);
	}	
	
	public void setMasterTemplateIDNotUsed(boolean mastertemplateidNotUsed)
	{
		this.mastertemplateidNotUsed = mastertemplateidNotUsed;
	}	
	
	public String getEndDateLabel()
	{
		return(enddateLabel);
	}	
	
	public void setEndDateLabel(String enddateLabel)
	{
		this.enddateLabel = enddateLabel;
	}	
	
	public boolean getEndDateNotUsed()
	{
		return(enddateNotUsed);
	}	
	
	public void setEndDateNotUsed(boolean enddateNotUsed)
	{
		this.enddateNotUsed = enddateNotUsed;
	}	
	
	public String getProcessStageIDLabel()
	{
		return(processstageidLabel);
	}	
	
	public void setProcessStageIDLabel(String processstageidLabel)
	{
		this.processstageidLabel = processstageidLabel;
	}	
	
	public boolean getProcessStageIDNotUsed()
	{
		return(processstageidNotUsed);
	}	
	
	public void setProcessStageIDNotUsed(boolean processstageidNotUsed)
	{
		this.processstageidNotUsed = processstageidNotUsed;
	}	
	
	public String getApprovalLabel()
	{
		return(approvalLabel);
	}	
	
	public void setApprovalLabel(String approvalLabel)
	{
		this.approvalLabel = approvalLabel;
	}	
	
	public boolean getApprovalNotUsed()
	{
		return(approvalNotUsed);
	}	
	
	public void setApprovalNotUsed(boolean approvalNotUsed)
	{
		this.approvalNotUsed = approvalNotUsed;
	}	

	private GroupDisplay groupdisplay = null;
	
	public GroupDisplay getGroupDisplay()
	{
		return(groupdisplay);
	}
	
	public void setGroupDisplay(GroupDisplay groupdisplay)
	{
		this.groupdisplay = groupdisplay;
	}		
}
