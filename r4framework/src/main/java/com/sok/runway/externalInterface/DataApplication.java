package com.sok.runway.externalInterface;

import java.util.Set;
import java.util.HashSet;

import com.sok.runway.ErrorMap;
import com.sok.runway.externalInterface.exceptions.AuthenticationExceptionHandler;
import com.sok.runway.externalInterface.exceptions.DataExceptionHandler;
import com.sok.runway.externalInterface.exceptions.NotAuthenticatedExceptionHandler;
import com.sok.runway.externalInterface.exceptions.NotFoundExceptionHandler;
import com.sok.runway.externalInterface.exceptions.RuntimeExceptionHandler;
import com.sok.runway.externalInterface.exceptions.UnrecognizedPropertyExceptionHandler;
import com.sok.runway.externalInterface.resources.CompanyResource;
import com.sok.runway.externalInterface.resources.ContactResource;
import com.sok.runway.externalInterface.resources.DocumentResource;
import com.sok.runway.externalInterface.resources.GroupResource;
import com.sok.runway.externalInterface.resources.ProcessResource;
import com.sok.runway.externalInterface.resources.ProfileResource;
import com.sok.runway.externalInterface.resources.OrderResource;
import com.sok.runway.externalInterface.resources.PropertyResource;
import com.sok.runway.externalInterface.resources.QuoteViewResource;
import com.sok.runway.externalInterface.resources.UserResource;
import com.sok.runway.externalInterface.resources.UtilityResource;
import com.sok.runway.externalInterface.resources.cms.WebsiteResource;
import com.sok.service.exception.DataException;

import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.json.simple.JSONAware;

public class DataApplication extends Application {

	public static final boolean DEV = Boolean.FALSE;
	
	protected final Set<Object> singletons;
	protected final Set<Class<?>> classes; 
	
	public DataApplication() {
		singletons = new HashSet<Object>(13);
		singletons.add(QuoteViewResource.getInstance());
		singletons.add(PropertyResource.getInstance());
		singletons.add(UserResource.getInstance());
		singletons.add(OrderResource.getInstance());
		singletons.add(ContactResource.getInstance());
		singletons.add(CompanyResource.getInstance());
		singletons.add(ProfileResource.getInstance());
		singletons.add(DocumentResource.getInstance());
		singletons.add(ProcessResource.getInstance());
		singletons.add(UtilityResource.getInstance());
		singletons.add(GroupResource.getInstance());
		singletons.add(WebsiteResource.getInstance());
		singletons.add(TestResource.getInstance());
		
		classes = new HashSet<Class<?>>(6);
		classes.add(NotAuthenticatedExceptionHandler.class);
		classes.add(AuthenticationExceptionHandler.class);
		classes.add(NotFoundExceptionHandler.class);
		classes.add(RuntimeExceptionHandler.class);
		classes.add(DataExceptionHandler.class);
		classes.add(UnrecognizedPropertyExceptionHandler.class);
		
		System.out.println("DataApplication initialized");
	}
	
	public DataApplication(Set<Object> singletons, Set<Class<?>> classes) {
		this.singletons = singletons;
		this.classes = classes;
	}

	@Override
	public Set<Class<?>> getClasses() {
		//returns the error handlers.
		return classes;
	}

	@Override
	public Set<Object> getSingletons() {
		//return the singleton resources.
		return singletons;
	}
	
	public static Response jsonResponse(Status s, JSONAware entity) {
		return Response.status(s).entity(entity.toJSONString()).type(MediaType.APPLICATION_JSON_TYPE).build();
	}
	public static Response jsonResponse(Exception e) {
		return jsonResponse(Status.INTERNAL_SERVER_ERROR,new DataApplicationException(e));
	}
	public static Response jsonResponse(Status s, Object entity) {
		return Response.status(s).entity(entity).type(MediaType.APPLICATION_JSON_TYPE).build();
	}
	/**
	 * Non mutable class used to pass exceptions back to the front end
	 */
	@XmlRootElement(name="Exception")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class DataApplicationException {
		public final String status = "failed";
		public final String error;
		public final int errorCode;
		public final ErrorMap em;
		public DataApplicationException() {
			error = null;
			errorCode = -1;
			em = new ErrorMap(0);
		}
		public DataApplicationException(Exception e) {
			error = e.getMessage();
			errorCode = 503; //internal server error;
			em = null;
		}
		public DataApplicationException(Exception e, int errorCode) {
			error = e.getMessage();
			this.errorCode = errorCode;
			em = null;
		}
		public DataApplicationException(String error, int errorCode) {
			this.error = error;
			this.errorCode = errorCode;
			em = null;
		}
		public DataApplicationException(DataException de, int errorCode) {
			this.error = de.getMessage();
			this.errorCode = errorCode; 
			this.em = de.getErrorMap();
		}
	}
	
	@Path("test")
	public static class TestResource {
		public static TestResource instance = new TestResource();
		private TestResource() {}
		public static TestResource getInstance() {
			return instance;
		}
		@GET
		@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
		public Test doTest() {
			return new Test();
		}
		
		@XmlRootElement(name="Test")
		@XmlAccessorType(XmlAccessType.FIELD)
		public static class Test {
			public String id = "TEST";
			public String name = "Test Object";
			public Double value = new Double(20.37);
		}
	}
}

/*
 * 
 * Future
 * 
//handling custom exception types 
 
@Provider
public class BadURIException implements ExceptionMapper<NotFoundException> {

public Response toResponse(NotFoundException exception){

    return Response.status(Response.Status.NOT_FOUND).
    entity(new ErrorResponse(exception.getClass().toString(),
                exception.getMessage()) ).
    build();
}
}

//thrown when parameters dont match the description
@Provider
public class ParamExceptionMapper implements ExceptionMapper<ParamException> {
    @Override
    public Response toResponse(ParamException exception) {
        return Response.status(Status.BAD_REQUEST).entity(exception.getParameterName() + " incorrect type").build();
    }
}

//queryparam custom type 

public class DateParam {
  private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

  private Calendar date;

  public DateParam(String in) throws WebApplicationException {
    try {
      date = Calendar.getInstance();
      date.setTime(format.parse(in));
    }
    catch (ParseException exception) {
      throw new WebApplicationException(400);
    }
  }
  public Calendar getDate() {
    return date;
  }
  public String format() {
    return format.format(value.getTime());
  }
}
*/