package com.sok.runway.externalInterface.resources;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest; 

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.beans.shared.Profile;
import com.sok.service.crm.ProfileService;


@Path("profiles")
public class ProfileResource {
	private static final ProfileResource resource = new ProfileResource();
	private static final ProfileService ps = ProfileService.getInstance();
	private static final Logger logger = LoggerFactory.getLogger(PropertyResource.class);
	public static ProfileResource getInstance() {
		return resource;
	}	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Profile> getProfiles(@Context HttpServletRequest request) {
		logger.trace("getProfiles(Request)");
		return ps.getProfiles(request);
	}
	
	
	
}
