package com.sok.runway.externalInterface.beans.shared;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties({ "ItemIncCost" }) /* using for storage of inc gst item cost */
public class OrderItem
{
	private String orderitemid = null;
	private String name = null;
	private String productid = null;
	private String copiedproductid = null;
	private String productgroupid = null;
	private String description = null;
	private double itemcost = 0;
	private double gst = 0;
	private double cost = 0;
	private String orderid = null;
	private double quantity = 0;
	private double totalcost = 0;
	private String invitation = null;
	private String pricingtype = null;
	private String productnum = null;
	private int sortnumber = 0;
	private String includepicture = null;
	private String includesummary = null;
	private String includeprice = null;
	private String category = null;
	private String orderTemplateItemID = null;
	private boolean isItemLocked = false;
	private boolean isQuantityLocked = false;
	private boolean isCostLocked = false;
	private boolean hasExtraText = false;
	private String ExtraText = null;
	private String itemGroup;
	private boolean itemGroupLocked;
	private boolean itemGroupAdd;
	private boolean itemGroupTotal;
	private int itemGroupSort;
	private String metaData = null;
	private boolean isPackageCost = false;
	private PackageCost[] packageCosts = null;
	
	@JsonProperty("OrderItemID")
	public String getOrderItemID()
	{
		return(orderitemid);
	}	
	
	@JsonProperty("OrderItemID")
	public void setOrderItemID(String orderitemid)
	{
		this.orderitemid = orderitemid;
	}	
	
	@JsonProperty("Name")
	public String getName()
	{
		return(name);
	}	
	
	@JsonProperty("Name")
	public void setName(String name)
	{
		this.name = name;
	}	
	
	@JsonProperty("ProductID")
	public String getProductID()
	{
		return(productid);
	}	
	
	@JsonProperty("ProductID")
	public void setProductID(String productid)
	{
		this.productid = productid;
	}	
	
	@JsonProperty("CopiedProductID")
	public String getCopiedProductID()
	{
		return(copiedproductid);
	}	
	
	@JsonProperty("CopiedProductID")
	public void setCopiedProductID(String copiedproductid)
	{
		this.copiedproductid = copiedproductid;
	}	
	
	@JsonProperty("ProductGroupID")
	public String getProductGroupID()
	{
		return(productgroupid);
	}	
	
	@JsonProperty("ProductGroupID")
	public void setProductGroupID(String productgroupid)
	{
		this.productgroupid = productgroupid;
	}	
	
	@JsonProperty("Description")
	public String getDescription()
	{
		return(description);
	}	
	
	@JsonProperty("Description")
	public void setDescription(String description)
	{
		this.description = description;
	}	
	
	@JsonProperty("ItemCost")
	public double getItemCost()
	{
		return(itemcost);
	}	
	
	@JsonProperty("ItemCost")
	public void setItemCost(double itemcost)
	{
		this.itemcost = itemcost;
	}	
	
	@JsonProperty("GST")
	public double getGST()
	{
		return(gst);
	}	
	
	@JsonProperty("GST")
	public void setGST(double gst)
	{
		this.gst = gst;
	}	
	
	@JsonProperty("Cost")
	public double getCost()
	{
		return(cost);
	}	
	
	@JsonProperty("Cost")
	public void setCost(double cost)
	{
		this.cost = cost;
	}	
	
	@JsonProperty("OrderID")
	public String getOrderID()
	{
		return(orderid);
	}	
	
	@JsonProperty("OrderID")
	public void setOrderID(String orderid)
	{
		this.orderid = orderid;
	}	
	
	@JsonProperty("Quantity")
	public double getQuantity()
	{
		return(quantity);
	}	
	
	@JsonProperty("Quantity")
	public void setQuantity(double quantity)
	{
		this.quantity = quantity;
	}	
	
	@JsonProperty("TotalCost")
	public double getTotalCost()
	{
		return(totalcost);
	}	
	
	@JsonProperty("TotalCost")
	public void setTotalCost(double totalcost)
	{
		this.totalcost = totalcost;
	}	
	
	public String getInvitation()
	{
		return(invitation);
	}	
	
	public void setInvitation(String invitation)
	{
		this.invitation = invitation;
	}	
	
	@JsonProperty("PricingType")
	public String getPricingType()
	{
		return(pricingtype);
	}	
	
	@JsonProperty("PricingType")
	public void setPricingType(String pricingtype)
	{
		this.pricingtype = pricingtype;
	}	
	
	@JsonProperty("ProductNum")
	public String getProductNum()
	{
		return(productnum);
	}	
	
	@JsonProperty("ProductNum")
	public void setProductNum(String productnum)
	{
		this.productnum = productnum;
	}	
	
	@JsonProperty("SortNumber")
	public int getSortNumber()
	{
		return(sortnumber);
	}	
	
	@JsonProperty("SortNumber")
	public void setSortNumber(int sortnumber)
	{
		this.sortnumber = sortnumber;
	}	
	
	public String getIncludePicture()
	{
		return(includepicture);
	}	
	
	public void setIncludePicture(String includepicture)
	{
		this.includepicture = includepicture;
	}	
	
	public String getIncludeSummary()
	{
		return(includesummary);
	}	
	
	public void setIncludeSummary(String includesummary)
	{
		this.includesummary = includesummary;
	}	
	
	@JsonProperty("IncludeSummary")
	public boolean isIncludeSummary() {
		return "Y".equals(this.includesummary);
	}
	
	@JsonProperty("IncludeSummary")
	public void setIncludeSummary(boolean includesummary) {
		this.includesummary = includesummary ? "Y" : "N";
	}
	
	
	public String getIncludePrice()
	{
		return(includeprice);
	}	
	
	public void setIncludePrice(String includeprice)
	{
		this.includeprice = includeprice;
	}
	
	@JsonProperty("IncludePrice")
	public boolean isIncludePrice() {
		return "Y".equals(this.includeprice);
	}
	
	@JsonProperty("IncludePrice")
	public void setIncludePrice(boolean includePrice) {
		this.includeprice = includePrice ? "Y" : "N";
	}
	
	@JsonProperty("Category")
	public void setCategory(String category) 
	{
		this.category = category;
	}
	@JsonProperty("Category")
	public String getCategory() {
		return this.category;
	}
	@JsonProperty("OrderTemplateItemID")
	public void setOrderTemplateItemID(String orderTemplateItemID) 
	{
		this.orderTemplateItemID = orderTemplateItemID;
	}
	@JsonProperty("OrderTemplateItemID")
	public String getOrderTemplateItemID() {
		return this.orderTemplateItemID;
	}
	@JsonProperty("ItemLocked")
	public void setItemLocked(boolean isItemLocked) {
		this.isItemLocked = isItemLocked;
	}
	@JsonProperty("ItemLocked")
	public boolean isItemLocked() {
		return isItemLocked == true;
	}
	@JsonProperty("QuantityLocked")
	public void setQuantityLocked(boolean isQuantityLocked) {
		this.isQuantityLocked = isQuantityLocked;
	}
	@JsonProperty("QuantityLocked")
	public boolean isQuantityLocked() {
		return isQuantityLocked == true;
	}
	@JsonProperty("CostLocked")
	public void setCostLocked(boolean isCostLocked) {
		this.isCostLocked = isCostLocked;
	}
	@JsonProperty("CostLocked")
	public boolean isCostLocked() {
		return isCostLocked == true;
	}
	@JsonProperty("ItemGroup")
	public void setItemGroup(String itemGroup) 
	{
		this.itemGroup = itemGroup;
	}
	@JsonProperty("ItemGroup")
	public String getItemGroup() {
		return this.itemGroup;
	}
	@JsonProperty("ItemGroupSort")
	public void setItemGroupSort(int itemGroupSort) 
	{
		this.itemGroupSort = itemGroupSort;
	}
	@JsonProperty("ItemGroupLocked")
	public boolean isItemGroupLocked() {
		return this.itemGroupLocked;
	}
	@JsonProperty("ItemGroupLocked")
	public void setItemGroupLocked(boolean itemGroupLocked) 
	{
		this.itemGroupLocked = itemGroupLocked;
	}
	@JsonProperty("ItemGroupAdd")
	public boolean isItemGroupAdd() {
		return this.itemGroupAdd;
	}
	@JsonProperty("ItemGroupAdd")
	public void setItemGroupAdd(boolean itemGroupAdd) 
	{
		this.itemGroupAdd = itemGroupAdd;
	}
	@JsonProperty("PackageCost")
	public boolean isPackageCost() {
		return this.isPackageCost;
	}
	@JsonProperty("PackageCost")
	public void setPackageCost(boolean packageCost) 
	{
		this.isPackageCost = packageCost;
	}
	@JsonProperty("PackageCosts")
	public PackageCost[] getPackageCosts()
	{
		return packageCosts;
	}
	public void setPackageCosts(PackageCost[] packageCosts) {
		this.packageCosts = packageCosts;
	}
	
	@JsonProperty("ItemGroupTotal")
	public boolean isItemGroupTotal() {
		return this.itemGroupTotal;
	}
	@JsonProperty("ItemGroupTotal")
	public void setItemGroupTotal(boolean itemGroupTotal) 
	{
		this.itemGroupTotal = itemGroupTotal;
	}
	public void setItemGroupSort(String itemGroupSort) {
		try {
			this.itemGroupSort = Integer.parseInt(itemGroupSort);
		} catch (NumberFormatException nfe) {
			// do not need to stack trace this bnnfe.printStackTrace();
			this.itemGroupSort = 1000;
		}
	}
	@JsonProperty("ItemGroupSort")
	public int getItemGroupSort() {
		return this.itemGroupSort;
	}
	
	@JsonProperty("MetaData")
	public String getMetaData() {
		return this.metaData;
	}
	
	@JsonProperty("MetaData")
	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	public double getItemcost() {
		return itemcost;
	}

	public double getGst() {
		return gst;
	}

	public double getTotalcost() {
		return totalcost;
	}

	/**
	 * @return the hasExtraText
	 */
	@JsonProperty("HasExtraText")
	public boolean isHasExtraText() {
		return hasExtraText;
	}

	/**
	 * @param hasExtraText the hasExtraText to set
	 */
	@JsonProperty("HasExtraText")
	public void setHasExtraText(boolean hasExtraText) {
		this.hasExtraText = hasExtraText;
	}

	/**
	 * @return the extraText
	 */
	@JsonProperty("ExtraText")
	public String getExtraText() {
		return ExtraText;
	}

	/**
	 * @param extraText the extraText to set
	 */
	@JsonProperty("ExtraText")
	public void setExtraText(String extraText) {
		ExtraText = extraText;
	}
}