package com.sok.runway.externalInterface.resources.cms;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.cms.PageMeta;
import com.sok.service.cms.WebsiteService;

public class PageMetaResource {

	private static final Logger logger = LoggerFactory.getLogger(PageMetaResource.class);
	private static final WebsiteService websiteService = WebsiteService.getInstance();
	private final String cmsID, pageID;
	@Context HttpServletRequest request;
	
	public PageMetaResource(String cmsID, String pageID, HttpServletRequest request) {
		this.cmsID = cmsID; 
		this.pageID = pageID;
		this.request = request; 
	}
	
	@GET
	@Path("{PageMetaID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PageMeta getPageMeta(@PathParam("PageMetaID") String pageMetaID) {
		logger.debug("getPageMeta({})", pageMetaID);
		return websiteService.getPageMeta(request, cmsID, null, pageID, pageMetaID);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public PageMeta createPageMeta(PageMeta pageMeta) {
		logger.debug("createPageMeta(pageMeta)");
		return websiteService.createPageMeta(request, cmsID, null, pageID, pageMeta);
	}
	
	@PUT
	@Path("{PageMetaID}")
	@Produces(MediaType.APPLICATION_JSON)
	public PageMeta updatePageMeta(@PathParam("PageMetaID") String pageMetaID, PageMeta pageMeta) {
		logger.debug("updatePageMeta({}, pageMeta)", pageMetaID);
		return websiteService.updatePageMeta(request, cmsID, null, pageID, pageMetaID, pageMeta);
	}
}
