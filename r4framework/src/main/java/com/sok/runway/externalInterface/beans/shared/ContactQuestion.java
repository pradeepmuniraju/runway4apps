package com.sok.runway.externalInterface.beans.shared;
import java.util.*;

import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.runway.externalInterface.beans.ItemList;
/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class ContactQuestion extends Question
{
	private ContactAnswer answer = null;
	
	@JsonProperty("Answer")
	public ContactAnswer getAnswer()
	{
		return(answer);
	}	
	
	@JsonProperty("Answer")
	public void setAnswer(ContactAnswer answer)
	{
		this.answer = answer;
	}
}