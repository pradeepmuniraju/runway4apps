/**
 * 
 */
package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sok.framework.GenRow;

/**
 * @author Puggs
 *
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Profile")
public class Profile extends SimpleProfile {
	
	private String description, preamble, disclaimer;
	
	private Profile relatedProfile;
	
	private ArrayList<Questions> questions;

	/*
	<field name="SurveyID" type="12" dnull="false" ddefault="" dtype="VARCHAR" dlen="50"/>
	<field name="Name" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="Description" type="12" dtype="TEXT"/>
	<field name="Preamble" type="12" dtype="TEXT"/>
	<field name="Disclaimer" type="12" dtype="TEXT"/>
	<field name="AllOpportunities" type="12" dtype="CHAR" dlen="1"/>
	<field name="CompanyType" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="ContactType" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="Disabled" type="12" ddefault="N" dtype="CHAR" dlen="1"/>
	<field name="CreatedDate" type="93" dtype="DATETIME"/>
	<field name="CreatedBy" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="ModifiedDate" type="93" dtype="DATETIME"/>
	<field name="ModifiedBy" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="DisplayInCompanyView" type="12" dtype="CHAR" dlen="1"/>
	<field name="DisplayInContactView" type="12" dtype="CHAR" dlen="1"/>
	<field name="DisplayInOpportunityView" type="12" dtype="CHAR" dlen="1"/>
	<field name="DisplayInCompanySummary" type="12" dtype="CHAR" dlen="1"/>
	<field name="EditInContactDetailedList" type="12" dtype="CHAR" dlen="1"/>
	<field name="ProductProfile" type="12" dtype="CHAR" dlen="1"/>
	<field name="ProductType" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="ProductGroupProfile" type="12" dtype="CHAR" dlen="1"/>
	<field name="ProfileType" type="12" dtype="VARCHAR" dlen="20"/> <!-- See com.sok.runway.crm.profile.ProfileTypes for all possible values -->
	<field name="OrderProfile" type="12" dtype="CHAR" dlen="1"/>
	<field name="OrderItemProfile" type="12" dtype="CHAR" dlen="1"/>
	<field name="CompetitorProfile" type="12" dtype="CHAR" dlen="1"/>
	<field name="NoteType" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="NoteProfile" type="12" dtype="CHAR" dlen="1"/>
	<field name="ItemListProfile" type="12" dtype="CHAR" dlen="1"/>
	<field name="CheckListProfile" type="12" dtype="CHAR" dlen="1"/>
	<field name="RelatedSurveyID" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="GroupID" type="12" dtype="VARCHAR" dlen="50"/>
	 */
	/**
	 * 
	 */
	public Profile() {
	}

	/**
	 * @param profile
	 */
	public Profile(GenRow profile) {
		super(profile);
		
		description = profile.getData("Discription");
		preamble = profile.getData("Preamble");
		disclaimer = profile.getData("Disclaimer");
	}

	@XmlElement(name="Description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name="Preamble")
	public String getPreamble() {
		return preamble;
	}

	public void setPreamble(String preamble) {
		this.preamble = preamble;
	}

	@XmlElement(name="Disclaimer")
	public String getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	@XmlElement(name="RelatedProfile")
	public Profile getRelatedProfile() {
		return relatedProfile;
	}

	public void setRelatedProfile(Profile relatedProfile) {
		this.relatedProfile = relatedProfile;
	}

	@XmlElement(name="Questions")
	public ArrayList<Questions> getQuestions() {
		return questions;
	}

	public void setQuestions(ArrayList<Questions> questions) {
		this.questions = questions;
	}

}
