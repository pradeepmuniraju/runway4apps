package com.sok.runway.externalInterface.rest.rpm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.Lot;
import com.sok.runway.externalInterface.beans.cms.properties.LotCheck;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndex;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartmentCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartments;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexLot;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexLotCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPackageCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPackages;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPlanCollections;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPlans;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexRealEstate;
import com.sok.runway.externalInterface.beans.cms.properties.QuickIndexRealEstateCollections;
import com.sok.runway.externalInterface.beans.cms.properties.SearchKey;
import com.sok.service.crm.cms.properties.BuilderService;
import com.sok.service.crm.cms.properties.BuildingService;
import com.sok.service.crm.cms.properties.EstateService;
import com.sok.service.crm.cms.properties.PackageService;
import com.sok.service.crm.cms.properties.PlanService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiError;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.servlet.http.HttpServletRequest;

@Path("/search")
@Api(value = "/search", description = "Run queries on ProductQuickIndex")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class SearchResource {

	private static final Logger logger = LoggerFactory.getLogger(SearchResource.class);
	private static final EstateService estateService = EstateService.getInstance();
	private static final PlanService planService = PlanService.getInstance();
	private static final PackageService packageService = PackageService.getInstance();
	private static final BuildingService buildingService = BuildingService.getInstance();
	
	
	protected final String estateID, stageID;
	@Context HttpServletRequest request;
	
	public SearchResource() {
		this.estateID = null;
		this.stageID = null;
	}
	
	public SearchResource(HttpServletRequest request) {
		this.request = request;
		this.estateID = null;
		this.stageID = null;
	}
	public SearchResource(HttpServletRequest request, String estateID) {
		this.request = request;
		this.estateID = estateID;
		this.stageID = null;
	}
	public SearchResource(HttpServletRequest request, String estateID, String stageID) {
		this.request = request;
		this.estateID = estateID;
		this.stageID = stageID;
	}
	
	@GET
	@ApiOperation(value = "Search", notes="<p>Please see each of the search types for Search Key information</p>")
	public String getSearchKeys() {
		return "Please used other Search Keys";
	}
	
	@GET
	@Path("/land")
	@ApiOperation(value = "search all land", notes="<p>The Following Search Keys Apply to Land Searches</p>" +
			"<ul><li><strong>EstateName</strong> - Any Name of an Estate, mutiple Estates are allowed and are separated by a URL encoded '+'. Use <i>/estates</i> for a list of the Estate Names.</li>" +
			"<li><strong>EstateID</strong> - Any Estate Runway ID, again these can be separated by an encoded '+'. Use <i>/estates</i> for a list of IDs and their Estates.</li>" +
			"<li><strong>StageName</strong> - This should be used with EstateName or EstateID as there are mutiple Stages with the same name over different Estates. Do not add the word 'Stage' as the API searches with and without Stage added.</li>" +
			"<li><strong>StageID</strong> - This can be used on its own and doesn't need EstateName or EstateID. Use <i>/estates</i> for a list of StageIDs and what Estate they belong to.</li>" +
			"<li><strong>LotName</strong> - This should be used with EstateName and StageName or StageID as there are mutiple Lots with the same name over different Estates and Stages. Do not add the word 'Lot' as the API searches with and without Lot added.</li>" +
			"<li><strong>LotID</strong> - This can be used on its own. The <i>/lots</i> will give you a list of LotIDs.</li>" +
			"<li><strong>LocationID</strong> and <strong>RegionID</strong> - Use the <i>/location</i> API for a List of Location/Region IDs that can be used with this search field.</li>" +
			"<li><strong>LocationName</strong> - Use the <i>/location</i> API for a List of Location Names.</li>" +
			"<li><strong>LocationNum</strong> - Use the <i>/location</i> API for a List of Location Unique IDs.</li>" +
			"<li><strong>LotType</strong> - The values that can be used for this is dependent on the client Runway, please contact Support for a list of values.</li>" +
			"<li><strong>MarketingStatus</strong> - The values that can be used for this is dependent on the client Runway, please contact Support for a list of values.</li>" +
			"<li><strong>Package Count</strong> - The number of packages in this land.</li>" +
			"<li><strong>LotPrice</strong> - This can be a single value such as 199000 or a range 150000-250000.</li>" +
			"<li><strong>LotWidth</strong> - As above such as 10-20</li>" +
			"<li><strong>LotDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>LotArea</strong> - As above such as 300-400</li>" +
			"<li><strong>Orientation</strong> - As above such as North</li>" +
			"<li><strong>KeyWord</strong> - This key will search the description for the Key Word see below for how to add more than one</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>-sort0</strong> - Use this to change what the results are sorted on. The keys above can be used.</li>" +
			"<li><strong>-order0</strong> - Use this to change the order of -sort0, ASC or DESC are valid with the former being the default.</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>NOTES</strong> You are able to use encoded '+' for OR (||) and encoded ';' for AND (&&) in all Search Keys, but not when a range is used. When ranges are used such as LotArea then lots that have no value (or zero) in that field would also be included in the results.</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>Examples</strong> <i>LotWidth=15-20&LotArea=350-500</i> will give you all lots that have a Front Width of 15 to 20 and an Area of 350m2 to 500m2 inclusive. <i>EstateName=abc&StageName=2</i> will give you all lots in Stage 2 of abc Estate (not case sensitive). You can also use 1=1 to get all results, but this may be a lot of information and should be done as a background task</li>" +
			"</ul>"
			, responseClass = "com.sok.runway.externalInterface.beans.cms.properties.QuickIndexLot")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndexLot> getLand(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getLand({} {})", query, true);
		}
		return estateService.getLotsSearch(request, query, true);
	}
	
	@GET
	@Path("/land/collection")
	@ApiOperation(value = "search all land and return them as collections", notes="<p>Collections use the same search keys as Apartments, but also have a collection field that is a list of search keys to collate by.</p>" +
			"<p><strong>Examples</strong></p><ul>" +
			"<li>query: <strong>Bedrooms=2-4</strong> collection: <strong>ProductSubType</strong> - This will return all 2-4 bedroom apartments but break them up on the Sub Type, such as West Side, North Side, etc.</li>" +
			"<li>query: <strong>ProductSubType=NorthSide</strong> collection: <strong>Bedrooms+Bathrooms</strong> - This will return all the sub type of North Side, but will break them up on a combination of Bedrooms and Bathrooms</li>" +
			"</ul>"
			, responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndexLotCollections")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndexLotCollections> getLandCollections(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query,@ApiParam(value = "Collection Method", required = true) @QueryParam("collection") String collection) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getApartmentCollections({} {})", query, collection);
		}
		return estateService.getLandCollectionSearch(request, query, collection);
	}

	@GET
	@Path("/land/{searchKey}")
	@ApiOperation(value = "Returns the min and max values for land", notes="If the value is not a number then 0 is returned for both Minimum and Maximum. Zero is ignored as a Minimum.", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.SearchKey")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public SearchKey getLandSearchKeys(@Context HttpServletRequest request, @PathParam("searchKey") final String searchKey) {
		return estateService.getMinMax(request, searchKey);
	}

	@GET
	@Path("/plans")
	@ApiOperation(value = "search all plans", notes="<p>The Following Search Keys Apply to Home Plan Searches</p>" +
			"<ul><li><strong>RangeName</strong> - Any Name of a Range, mutiple Ranges are allowed and are separated by a URL encoded '+'.</li>" +
			"<li><strong>RangeID</strong> - Any Range Runway ID, again these can be separated by an encoded '+'.</li>" +
			"<li><strong>DesignName</strong> - This is a Home Design Name, you can use <i>/homes</i> to get a list of these.</li>" +
			"<li><strong>DesignID</strong> - This is a Home Design Name, you can use <i>/homes</i> to get a list of these.</li>" +
			"<li><strong>PlanName</strong> - This the name of the actual plan, most cases this will be a number, eg 21 or 39." +
			"<li><strong>PlanID</strong> - this is the Plan ID you can use <i>/homes</i> for a list of these.</li>" +
			"<li><strong>ProductSubType</strong> - The values that can be used for this is dependent on the client Runway, please contact Support for a list of values.</li>" +
			"<li><strong>MarketingStatus</strong> - The values that can be used for this is dependent on the client Runway, please contact Support for a list of values.</li>" +
			"<li><strong>HomePlanName</strong> - This is the Design Name and Plan Name together. eg Clasic 39.</li>" +
			"<li><strong>LocationID</strong> and <strong>RegionID</strong> - Use the <i>/location</i> API for a List of Location/Region IDs that can be used with this search field.</li>" +
			"<li><strong>LocationName</strong> - Use the <i>/location</i> API for a List of Location Names.</li>" +
			"<li><strong>LocationNum</strong> - Use the <i>/location</i> API for a List of Location Unique IDs.</li>" +
			"<li><strong>HomePrice</strong> - This can be a single value such as 199000 or a range 150000-250000.</li>" +
			"<li><strong>HomeWidth</strong> - As above such as 10-20</li>" +
			"<li><strong>HomeDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>HomeArea</strong> - As above such as 300-400</li>" +
			"<li><strong>BuildWidth</strong> - As above such as 10-20</li>" +
			"<li><strong>BuildDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>CanFitOnFrontage</strong> - As above such as 10-20</li>" +
			"<li><strong>CanFitOnDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>Storeys</strong> - As above such as 1-2</li>" +
			"<li><strong>Bedrooms</strong> - As above such as 2-5</li>" +
			"<li><strong>Bathrooms</strong> - As above such as 1-3</li>" +
			"<li><strong>Study</strong> - As above such as Yes or No or it can be a number such as 1.0</li>" +
			"<li><strong>Media</strong> - As above such as Yes or No</li>" +
			"<li><strong>Orientation</strong> - As above such as North</li>" +
			"<li><strong>Carparks</strong> - As above such as 1-2</li>" +
			"<li><strong>Garage</strong> - this is usually the area of the Garage.</li>" +
			"<li><strong>KeyWord</strong> - This key will search the description for the Key Word see below for how to add more than one</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>-sort0</strong> - Use this to change what the results are sorted on. The keys above can be used.</li>" +
			"<li><strong>-order0</strong> - Use this to change the order of -sort0, ASC or DESC are valid with the former being the default.</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>NOTES</strong> You are able to use encoded '+' for OR (||) and encoded ';' for AND (&&) in all Search Keys, but not when a range is used. When ranges are used such as LotArea then lots that have no value (or zero) in that field would also be included in the results.</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>Examples</strong> <i>HomeWidth=10-15&LotArea=200-350</i> will give you all plans that have a Width of 10 to 15 and an Area of 200m2 to 350m2 inclusive. <i>RangeName=clasic</i> will give you all plans in the Clasic Range (not case sensitive). You can also use 1=1 to get all results, but this may be a lot of information and should be done as a background task</li>" +
			"</ul>"
			, responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPlans")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndexPlans> getPlans(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getPlans({} {})", query, true);
		}
		return planService.getPlansSearch(request, query, true);
	}
	
	@GET
	@Path("/plan/collection")
	@ApiOperation(value = "search all plans and return them as collections", notes="<p>Collections use the same search keys as Apartments, but also have a collection field that is a list of search keys to collate by.</p>" +
			"<p><strong>Examples</strong></p><ul>" +
			"<li>query: <strong>Bedrooms=2-4</strong> collection: <strong>ProductSubType</strong> - This will return all 2-4 bedroom apartments but break them up on the Sub Type, such as West Side, North Side, etc.</li>" +
			"<li>query: <strong>ProductSubType=NorthSide</strong> collection: <strong>Bedrooms+Bathrooms</strong> - This will return all the sub type of North Side, but will break them up on a combination of Bedrooms and Bathrooms</li>" +
			"</ul>"
			, responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPlanCollections")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndexPlanCollections> getPlanCollections(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query,@ApiParam(value = "Collection Method", required = true) @QueryParam("collection") String collection) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getApartmentCollections({} {})", query, collection);
		}
		return planService.getPlanCollectionSearch(request, query, collection);
	}

	@GET
	@Path("/plans/{searchKey}")
	@ApiOperation(value = "Returns the min and maxi values for Plans", notes="If the value is not a number then 0 is returned for both Minimum and Maximum. Zero is ignored as a Minimum.", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.SearchKey")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public SearchKey getPlanSearchKeys(@Context HttpServletRequest request, @PathParam("searchKey") final String searchKey) {
		return planService.getMinMax(request, searchKey);
	}
	
	@GET
	@Path("/packages")
	@ApiOperation(value = "search all packages", notes="<p>The Following Search Keys Apply to Package Searches</p>" +
			"<ul><li><strong>RangeName</strong> - Any Name of a Range, mutiple Ranges are allowed and are separated by a URL encoded '+'.</li>" +
			"<li><strong>RangeID</strong> - Any Range Runway ID, again these can be separated by an encoded '+'.</li>" +
			"<li><strong>DesignName</strong> - This is a Home Design Name, you can use <i>/homes</i> to get a list of these.</li>" +
			"<li><strong>DesignID</strong> - This is a Home Design Name, you can use <i>/homes</i> to get a list of these.</li>" +
			"<li><strong>PlanName</strong> - This the name of the actual plan, most cases this will be a number, eg 21 or 39." +
			"<li><strong>PlanID</strong> - this is the Plan ID you can use <i>/homes</i> for a list of these.</li>" +
			"<li><strong>HomePlanName</strong> - This is the Design Name and Plan Name together. eg Clasic 39.</li>" +
			"<li><strong>EstateName</strong> - Any Name of an Estate, mutiple Estates are allowed and are separated by a URL encoded '+'. Use <i>/estates</i> for a list of the Estate Names.</li>" +
			"<li><strong>EstateID</strong> - Any Estate Runway ID, again these can be separated by an encoded '+'. Use <i>/estates</i> for a list of IDs and their Estates.</li>" +
			"<li><strong>StageName</strong> - This should be used with EstateName or EstateID as there are mutiple Stages with the same name over different Estates. Do not add the word 'Stage' as the API searches with and without Stage added.</li>" +
			"<li><strong>StageID</strong> - This can be used on its own and doesn't need EstateName or EstateID. Use <i>/estates</i> for a list of StageIDs and what Estate they belong to.</li>" +
			"<li><strong>LotName</strong> - This should be used with EstateName and StageName or StageID as there are mutiple Lots with the same name over different Estates and Stages. Do not add the word 'Lot' as the API searches with and without Lot added.</li>" +
			"<li><strong>LotID</strong> - This can be used on its own. The <i>/lots</i> will give you a list of LotIDs.</li>" +
			"<li><strong>PackageName</strong> - This can be used by it's self as package names are mostly unique.</li>" +
			"<li><strong>PackageID</strong> - This can be used on its own. The <i>/packages</i> will give you a list of PackageIDs.</li>" +
			"<li><strong>LocationID</strong> and <strong>RegionID</strong> - Use the <i>/location</i> API for a List of Location/Region IDs that can be used with this search field.</li>" +
			"<li><strong>LocationName</strong> - Use the <i>/location</i> API for a List of Location Names.</li>" +
			"<li><strong>LocationNum</strong> - Use the <i>/location</i> API for a List of Location Unique IDs.</li>" +
			"<li><strong>ProductSubType</strong> - The values that can be used for this is dependent on the client Runway, please contact Support for a list of values.</li>" +
			"<li><strong>MarketingStatus</strong> - The values that can be used for this is dependent on the client Runway, please contact Support for a list of values.</li>" +
			"<li><strong>LotPrice</strong> - This can be a single value such as 199000 or a range 150000-250000.</li>" +
			"<li><strong>LotWidth</strong> - As above such as 10-20</li>" +
			"<li><strong>LotDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>LotArea</strong> - As above such as 300-400</li>" +
			"<li><strong>HomePrice</strong> - This can be a single value such as 199000 or a range 150000-250000.</li>" +
			"<li><strong>HomeWidth</strong> - As above such as 10-20</li>" +
			"<li><strong>HomeDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>HomeArea</strong> - As above such as 300-400</li>" +
			"<li><strong>BuildWidth</strong> - As above such as 10-20</li>" +
			"<li><strong>BuildDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>CanFitOnFrontage</strong> - As above such as 10-20</li>" +
			"<li><strong>CanFitOnDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>Storeys</strong> - As above such as 1-2</li>" +
			"<li><strong>Bedrooms</strong> - As above such as 2-5</li>" +
			"<li><strong>Bathrooms</strong> - As above such as 1-3</li>" +
			"<li><strong>Study</strong> - As above such as Yes or No or it can be a number such as 1.0</li>" +
			"<li><strong>Media</strong> - As above such as Yes or No</li>" +
			"<li><strong>Orientation</strong> - As above such as North</li>" +
			"<li><strong>Carparks</strong> - As above such as 1-2</li>" +
			"<li><strong>Garage</strong> - this is usually the area of the Garage.</li>" +
			"<li><strong>PackagePrice</strong> - This can be a single value such as 199000 or a range 150000-250000.</li>" +
			"<li><strong>KeyWord</strong> - This key will search the description for the Key Word see below for how to add more than one</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>-sort0</strong> - Use this to change what the results are sorted on. The keys above can be used.</li>" +
			"<li><strong>-order0</strong> - Use this to change the order of -sort0, ASC or DESC are valid with the former being the default.</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>NOTES</strong> You are able to use encoded '+' for OR (||) and encoded ';' for AND (&&) in all Search Keys, but not when a range is used. When ranges are used such as LotArea then lots that have no value (or zero) in that field would also be included in the results.</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>Examples</strong> <i>HomeWidth=10-15&LotArea=200-350&-sort0=LotArea</i> will give you all plans that have a Width of 10 to 15 and an Area of 200m2 to 350m2 inclusive, sorted by the LotArea. <i>RangeName=clasic</i> will give you all plans in the Clasic Range (not case sensitive). You can also use 1=1 to get all results, but this may be a lot of information and should be done as a background task</li>" +
			"</ul>"
			, responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPackages")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndexPackages> getPackages(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getLand({} {})", query, true);
		}
		return packageService.getPackageSearch(request, query, true);
	}
	
	@GET
	@Path("/packages/collection")
	@ApiOperation(value = "search all packages and return them as collections", notes="<p>Collections use the same search keys as Apartments, but also have a collection field that is a list of search keys to collate by.</p>" +
			"<p><strong>Examples</strong></p><ul>" +
			"<li>query: <strong>Bedrooms=2-4</strong> collection: <strong>ProductSubType</strong> - This will return all 2-4 bedroom apartments but break them up on the Sub Type, such as West Side, North Side, etc.</li>" +
			"<li>query: <strong>ProductSubType=NorthSide</strong> collection: <strong>Bedrooms+Bathrooms</strong> - This will return all the sub type of North Side, but will break them up on a combination of Bedrooms and Bathrooms</li>" +
			"</ul>"
			, responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndexPackageCollections")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndexPackageCollections> getPackageCollections(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query,@ApiParam(value = "Collection Method", required = true) @QueryParam("collection") String collection) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getApartmentCollections({} {})", query, collection);
		}
		return packageService.getPackageCollectionSearch(request, query, collection);
	}

	@GET
	@Path("/packages/{searchKey}")
	@ApiOperation(value = "Returns the min and maxi values for packages", notes="If the value is not a number then 0 is returned for both Minimum and Maximum. Zero is ignored as a Minimum.", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.SearchKey")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public SearchKey getPackageSearchKeys(@Context HttpServletRequest request, @PathParam("searchKey") final String searchKey) {
		return packageService.getMinMax(request, searchKey);
	}
	
	@GET
	@Path("/realestate")
	@ApiOperation(value = "search all realestate", notes="<p>The Following Search Keys Apply to Realestate Searches. These searches are similar to Package Searches</p>" +
			"<ul><li><strong>RangeName</strong> - Any Name of a Range, mutiple Ranges are allowed and are separated by a URL encoded '+'.</li>" +
			"<li><strong>RangeID</strong> - Any Range Runway ID, again these can be separated by an encoded '+'.</li>" +
			"<li><strong>DesignName</strong> - This is a Home Design Name, you can use <i>/homes</i> to get a list of these.</li>" +
			"<li><strong>DesignID</strong> - This is a Home Design Name, you can use <i>/homes</i> to get a list of these.</li>" +
			"<li><strong>PlanName</strong> - This the name of the actual plan, most cases this will be a number, eg 21 or 39." +
			"<li><strong>PlanID</strong> - this is the Plan ID you can use <i>/homes</i> for a list of these.</li>" +
			"<li><strong>HomePlanName</strong> - This is the Design Name and Plan Name together. eg Clasic 39.</li>" +
			"<li><strong>EstateName</strong> - Any Name of an Estate, mutiple Estates are allowed and are separated by a URL encoded '+'. Use <i>/estates</i> for a list of the Estate Names.</li>" +
			"<li><strong>EstateID</strong> - Any Estate Runway ID, again these can be separated by an encoded '+'. Use <i>/estates</i> for a list of IDs and their Estates.</li>" +
			"<li><strong>StageName</strong> - This should be used with EstateName or EstateID as there are mutiple Stages with the same name over different Estates. Do not add the word 'Stage' as the API searches with and without Stage added.</li>" +
			"<li><strong>StageID</strong> - This can be used on its own and doesn't need EstateName or EstateID. Use <i>/estates</i> for a list of StageIDs and what Estate they belong to.</li>" +
			"<li><strong>LotName</strong> - This should be used with EstateName and StageName or StageID as there are mutiple Lots with the same name over different Estates and Stages. Do not add the word 'Lot' as the API searches with and without Lot added.</li>" +
			"<li><strong>LotID</strong> - This can be used on its own. The <i>/lots</i> will give you a list of LotIDs.</li>" +
			"<li><strong>PackageName</strong> - This can be used by it's self as package names are mostly unique.</li>" +
			"<li><strong>PackageID</strong> - This can be used on its own. The <i>/packages</i> will give you a list of PackageIDs.</li>" +
			"<li><strong>LocationID</strong> and <strong>RegionID</strong> - Use the <i>/location</i> API for a List of Location/Region IDs that can be used with this search field.</li>" +
			"<li><strong>LocationName</strong> - Use the <i>/location</i> API for a List of Location Names.</li>" +
			"<li><strong>LocationNum</strong> - Use the <i>/location</i> API for a List of Location Unique IDs.</li>" +
			"<li><strong>ProductSubType</strong> - The values that can be used for this is dependent on the client Runway, please contact Support for a list of values.</li>" +
			"<li><strong>MarketingStatus</strong> - The values that can be used for this is dependent on the client Runway, please contact Support for a list of values.</li>" +
			"<li><strong>LotPrice</strong> - This can be a single value such as 199000 or a range 150000-250000.</li>" +
			"<li><strong>LotWidth</strong> - As above such as 10-20</li>" +
			"<li><strong>LotDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>LotArea</strong> - As above such as 300-400</li>" +
			"<li><strong>HomePrice</strong> - This can be a single value such as 199000 or a range 150000-250000.</li>" +
			"<li><strong>HomeWidth</strong> - As above such as 10-20</li>" +
			"<li><strong>HomeDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>HomeArea</strong> - As above such as 300-400</li>" +
			"<li><strong>BuildWidth</strong> - As above such as 10-20</li>" +
			"<li><strong>BuildDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>CanFitOnFrontage</strong> - As above such as 10-20</li>" +
			"<li><strong>CanFitOnDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>Storeys</strong> - As above such as 1-2</li>" +
			"<li><strong>Bedrooms</strong> - As above such as 2-5</li>" +
			"<li><strong>Bathrooms</strong> - As above such as 1-3</li>" +
			"<li><strong>Study</strong> - As above such as Yes or No or it can be a number such as 1.0</li>" +
			"<li><strong>Media</strong> - As above such as Yes or No</li>" +
			"<li><strong>Orientation</strong> - As above such as North</li>" +
			"<li><strong>Carparks</strong> - As above such as 1-2</li>" +
			"<li><strong>Garage</strong> - this is usually the area of the Garage.</li>" +
			"<li><strong>PackagePrice</strong> - This can be a single value such as 199000 or a range 150000-250000.</li>" +
			"<li><strong>KeyWord</strong> - This key will search the description for the Key Word see below for how to add more than one</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>-sort0</strong> - Use this to change what the results are sorted on. The keys above can be used.</li>" +
			"<li><strong>-order0</strong> - Use this to change the order of -sort0, ASC or DESC are valid with the former being the default.</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>NOTES</strong> You are able to use encoded '+' for OR (||) and encoded ';' for AND (&&) in all Search Keys, but not when a range is used. When ranges are used such as LotArea then lots that have no value (or zero) in that field would also be included in the results.</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>Examples</strong> <i>HomeWidth=10-15&LotArea=200-350&-sort0=LotArea</i> will give you all plans that have a Width of 10 to 15 and an Area of 200m2 to 350m2 inclusive, sorted by the LotArea. <i>RangeName=clasic</i> will give you all plans in the Clasic Range (not case sensitive). You can also use 1=1 to get all results, but this may be a lot of information and should be done as a background task</li>" +
			"</ul>"
			, responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndexRealEstate")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndexRealEstate> getRealEstate(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getLand({} {})", query, true);
		}
		return planService.getRealEstateSearch(request, query, true);
	}
	
	@GET
	@Path("/realestate/collection")
	@ApiOperation(value = "search all realestate and return them as collections", notes="<p>Collections use the same search keys as Apartments, but also have a collection field that is a list of search keys to collate by.</p>" +
			"<p><strong>Examples</strong></p><ul>" +
			"<li>query: <strong>Bedrooms=2-4</strong> collection: <strong>ProductSubType</strong> - This will return all 2-4 bedroom apartments but break them up on the Sub Type, such as West Side, North Side, etc.</li>" +
			"<li>query: <strong>ProductSubType=NorthSide</strong> collection: <strong>Bedrooms+Bathrooms</strong> - This will return all the sub type of North Side, but will break them up on a combination of Bedrooms and Bathrooms</li>" +
			"</ul>"
			, responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndexRealEstateCollections")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndexRealEstateCollections> getRealEsateCollections(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query,@ApiParam(value = "Collection Method", required = true) @QueryParam("collection") String collection) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getApartmentCollections({} {})", query, collection);
		}
		return planService.getRealEstateCollectionSearch(request, query, collection);
	}
	@GET
	@Path("/realestate/{searchKey}")
	@ApiOperation(value = "Returns the min and maxi values for realestate", notes="If the value is not a number then 0 is returned for both Minimum and Maximum. Zero is ignored as a Minimum.", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.SearchKey")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public SearchKey getRealEstateSearchKeys(@Context HttpServletRequest request, @PathParam("searchKey") final String searchKey) {
		return planService.getREMinMax(request, searchKey);
	}
	
	
	@GET
	@Path("/apartments")
	@ApiOperation(value = "search all apartments", notes="<p>The Following Search Keys Apply to Apartment Searches</p>" +
			"<ul>" +
			"<li><strong>BuildingName</strong> - Any Name of an Building, mutiple Building are allowed and are separated by a URL encoded '+'. Use <i>/buildings</i> for a list of the Building Names.</li>" +
			"<li><strong>BuildingID</strong> - Any Building Runway ID, again these can be separated by an encoded '+'. Use <i>/buildings</i> for a list of IDs and their Building.</li>" +
			"<li><strong>BuildingStageName</strong> - This should be used with BuildingName or BuildingID as there are mutiple BuildingStages with the same name over different Buildings." +
			"<li><strong>BuildingStageID</strong> - This can be used on its own and doesn't need BuildingName or BuildingID. Use <i>/buildings</i> for a list of BuildingStageIDs and what Buildings they belong to.</li>" +
			"<li><strong>ApartmentName</strong> - This should be used with BuildingName and BuildingName or StageID as there are mutiple Lots with the same name over different Buildings and BuildingStages. Do not add the word 'Apartment' as the API searches with and without Apartment added.</li>" +
			"<li><strong>ApartmentID</strong> - This can be used on its own. The <i>/apartments</i> will give you a list of BuildingIDs.</li>" +
			"<li><strong>LocationID</strong> and <strong>RegionID</strong> - Use the <i>/location</i> API for a List of Location/Region IDs that can be used with this search field.</li>" +
			"<li><strong>LocationName</strong> - Use the <i>/location</i> API for a List of Location Names.</li>" +
			"<li><strong>LocationNum</strong> - Use the <i>/location</i> API for a List of Location Unique IDs.</li>" +
			"<li><strong>ApartmentPrice</strong> - This can be a single value such as 199000 or a range 150000-250000.</li>" +
			"<li><strong>ApartmentWidth</strong> - As above such as 10-20</li>" +
			"<li><strong>ApartmentDepth</strong> - As above such as 26-35</li>" +
			"<li><strong>ApartmentArea</strong> - As above such as 300-400</li>" +
			"<li><strong>InternalArea</strong> - As above such as 300-400</li>" +
			"<li><strong>ExternalArea</strong> - As above such as 30-40</li>" +
			"<li><strong>Storeys</strong> - As above such as 1-2</li>" +
			"<li><strong>Bedrooms</strong> - As above such as 2-5</li>" +
			"<li><strong>Bathrooms</strong> - As above such as 1-3</li>" +
			"<li><strong>Study</strong> - As above such as Yes or No or it can be a number such as 1.0</li>" +
			"<li><strong>Media</strong> - As above such as Yes or No</li>" +
			"<li><strong>Orientation</strong> - As above such as North</li>" +
			"<li><strong>Carparks</strong> - As above such as 1-2</li>" +
			"<li><strong>Garage</strong> - this is usually the area of the Garage.</li>" +
			"<li><strong>ProductSubType</strong> - this is the equivalent to Apartment Type.</li>" +
			"<li><strong>MarketingStatus</strong> - The values that can be used for this is dependent on the client Runway, please contact Support for a list of values.</li>" +
			"<li><strong>KeyWord</strong> - This key will search the description for the Key Word see below for how to add more than one</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>-sort0</strong> - Use this to change what the results are sorted on. The keys above can be used.</li>" +
			"<li><strong>-order0</strong> - Use this to change the order of -sort0, ASC or DESC are valid with the former being the default.</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>NOTES</strong> You are able to use encoded '+' for OR (||) and encoded ';' for AND (&&) in all Search Keys, but not when a range is used. When ranges are used such as LotArea then lots that have no value (or zero) in that field would also be included in the results.</li>" +
			"<li>&nbsp;</li>" +
			"<li><strong>Examples</strong> <i>HomeWidth=10-15&LotArea=200-350&-sort0=LotArea</i> will give you all plans that have a Width of 10 to 15 and an Area of 200m2 to 350m2 inclusive, sorted by the LotArea. <i>RangeName=clasic</i> will give you all plans in the Clasic Range (not case sensitive). You can also use 1=1 to get all results, but this may be a lot of information and should be done as a background task</li>" +
			"</ul>"
			, responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartments")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndexApartments> getApartments(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getApartments({} {})", query, true);
		}
		return buildingService.getApartmentsSearch(request, query, true);
	}
	
	@GET
	@Path("/apartments/collection")
	@ApiOperation(value = "search all apartments and return them as collections", notes="<p>Collections use the search keys as below and also have a collection field that is a list of search keys to collate by.</p>" +
			"<p><strong>Examples</strong></p><ul>" +
			"<li>query: <strong>Bedrooms=2-4</strong> collection: <strong>ProductSubType</strong> - This will return all 2-4 bedroom apartments but break them up on the Sub Type, such as West Side, North Side, etc.</li>" +
			"<li>query: <strong>ProductSubType=NorthSide</strong> collection: <strong>Bedrooms+Bathrooms</strong> - This will return all the sub type of North Side, but will break them up on a combination of Bedrooms and Bathrooms</li>" +
			"</ul>"
			, responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndexApartmentCollections")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndexApartmentCollections> getApartmentCollections(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query,@ApiParam(value = "Collection Method", required = true) @QueryParam("collection") String collection) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getApartmentCollections({} {})", query, collection);
		}
		return buildingService.getApartmentsCollectionSearch(request, query, collection);
	}
	
	@GET
	@Path("/apartments/{searchKey}")
	@ApiOperation(value = "Returns the min and maxi values for apartments", notes="If the value is not a number then 0 is returned for both Minimum and Maximum. Zero is ignored as a Minimum.", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.SearchKey")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public SearchKey getApartmentSearchKeys(@Context HttpServletRequest request, @PathParam("searchKey") final String searchKey) {
		return buildingService.getMinMax(request, searchKey);
	}
	/*
	@GET
	@Path("/products")
	@ApiOperation(value = "search all products", notes="<p>You can use any of the Search Keys from the other /search APIs but results may very.</p>", responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndex", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndex> getProducts(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getPlans({} {})", query, true);
		}
		return planService.getProductsSearch(request, query, true);
	}
	*/
	@GET
	@Path("/products")
	@ApiOperation(value = "search all products", notes="<p>You can use any of the Search Keys from the other /search APIs but results may very.</p>", responseClass="com.sok.runway.externalInterface.beans.cms.properties.QuickIndex")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<QuickIndex> getProducts(@ApiParam(value = "Search query to use for the results", required = true) @QueryParam("query") String query, @ApiParam(value = "<b>WARNING</b> setting false will return Products that may not be valid for sale", required = false, defaultValue = "true") @DefaultValue("true") @QueryParam("filter") boolean filter) {
		if(StringUtils.isNotBlank(query)) {
			logger.debug("getPlans({} {})", query, filter);
		}
		return planService.getProductsSearch(request, query, filter);
	}
	
}

