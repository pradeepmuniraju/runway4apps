package com.sok.runway.externalInterface.rest.rpm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.BeanRepresentation;
import com.sok.runway.externalInterface.beans.cms.properties.ApartmentCheck;
import com.sok.runway.externalInterface.beans.cms.properties.Apartment;
import com.sok.service.crm.cms.properties.BuildingService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResource;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.servlet.http.HttpServletRequest;

@Path("/apartments")
@Api(value = "/apartments", description = "Retrieve Apartment Information")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
public class ApartmentResource {

	private static final Logger logger = LoggerFactory.getLogger(ApartmentResource.class);
	private static final BuildingService buildingService = BuildingService.getInstance();
	
	protected final String buildingID, stageID;
	@Context HttpServletRequest request;
	
	public ApartmentResource() {
		this.buildingID = null;
		this.stageID = null;
	}
	
	public ApartmentResource(HttpServletRequest request) {
		this.request = request;
		this.buildingID = null;
		this.stageID = null;
	}
	public ApartmentResource(HttpServletRequest request, String buildingID) {
		this.request = request;
		this.buildingID = buildingID;
		this.stageID = null;
	}
	public ApartmentResource(HttpServletRequest request, String buildingID, String stageID) {
		this.request = request;
		this.buildingID = buildingID;
		this.stageID = stageID;
	}
	
	@GET
	@ApiOperation(value = "List all apartments", notes="Optionally filtering by modified date", responseClass="com.sok.runway.externalInterface.beans.cms.properties.Apartment", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<Apartment> getEstateApartments(@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = true) @QueryParam("ModifiedSince") String modifiedSince,
			@ApiParam(value = "Return only active builders", required=false, defaultValue="true") @DefaultValue("true") @QueryParam("Active") boolean active) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			logger.debug("getStages({})", modifiedSince, modifiedSince);
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		return buildingService.getApartmentsExternal(request, buildingID, stageID, mod, active);
	}
	
	@GET
	@Path("/check")
	@ApiOperation(value = "List all apartments", notes="This method is used just for checking the status and modified date of the apartments. If you don’t want all the information and just want to check if you need to take it offline or put something new up then use this method.", responseClass="com.sok.runway.externalInterface.beans.cms.properties.ApartmentCheck", multiValueResponse=true)
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid ModifiedSince value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted") })
	public List<ApartmentCheck> getEstateApartmentsCheck(@ApiParam(value = "A date formatted in the format dd/MM/yyyy[ HH:mm:ss], relative to ModifiedDate", required = true) @QueryParam("ModifiedSince") String modifiedSince) {
		Date mod = null;
		if(StringUtils.isNotBlank(modifiedSince)) {
			logger.debug("getStages({})", modifiedSince, modifiedSince);
			try { 
				mod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(modifiedSince);
			} catch (ParseException pe) { }
			if(mod == null) { 
				try { 
					mod = new SimpleDateFormat("dd/MM/yyyy").parse(modifiedSince);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("modifiedSince must conform to format dd/MM/yyyy[ HH:mm:ss]", pe);
				}
			}
		}
		return buildingService.getApartmentsExternalCheck(request, buildingID, stageID, mod);
	}
	
	@GET
	@Path("/{apartmentID}")
	@ApiOperation(value = "Return a particular apartment", responseClass = "com.sok.runway.externalInterface.beans.cms.properties.Apartment")
	@ApiErrors(value = { @ApiError(code = 400, reason = "Invalid id path value"), @ApiError(code = 401, reason = "Not Authenticated"),  @ApiError(code = 403, reason = "Authenticated but not permitted"), @ApiError(code = 404, reason = "Not Found") })
	public Apartment getApartment(@Context HttpServletRequest request, @PathParam("apartmentID") final String apartmentID, 
			@ApiParam(value = "The returned lot representation", allowableValues="Simple,Extended", required=false, defaultValue="Simple") @DefaultValue("Simple") @QueryParam("View") BeanRepresentation extended) {
		return BeanFactory.external(buildingService.getApartment(request, apartmentID), extended);
	}
	
	@GET
	@ApiResource(value = "/{apartmentID}/albums", resourceClass="com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@ApiOperation(value = "Return a list of Albums and Images from the Apartments", notes="<p>This call will return a list of Albums and their Images for a given Apartment. The first Album will be a blank Album, it contains the images not within an Album.</p><p>There is a possibility that images may be repeated in different albums. Resizing and cropping the images is up to your system, but aspect ratio must be maintained.</p>", responseClass = "com.sok.runway.externalInterface.rest.rpm.AlbumResource")
	@Path("/{apartmentID}/albums")
	public AlbumResource getAlbumResource(@PathParam("apartmentID") final String apartmentID, 
			@ApiParam(value = "Document Sub Type", required = false) @QueryParam("DocumentSubType") String subType, 
			@ApiParam(value = "Album Type", required = false) @QueryParam("AlbumType") String albumType, 
			@ApiParam(value = "Album Publsihed for", allowableValues="Website", required=false, defaultValue="") @DefaultValue("") @QueryParam("Publish") String publish) {
		return new AlbumResource(request,  apartmentID, albumType, publish, subType);
	}
}

