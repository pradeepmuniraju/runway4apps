package com.sok.runway.externalInterface.beans.cms.properties;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.*;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.sok.framework.InitServlet;
import com.sok.runway.JSPLogger;
import com.sok.runway.externalInterface.beans.SimpleDocument;
import com.sok.runway.externalInterface.beans.SimplePublishing;
import com.sok.runway.externalInterface.beans.cms.SimpleCampaign;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.LegacyLocation;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.service.crm.cms.properties.PlanService.HLPackageCost;
import com.wordnik.swagger.annotations.ApiClass;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiProperty;

@XmlAccessorType(XmlAccessType.NONE)
//@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement(name="HouseLandPackageCheck")
@ApiClass(description="Deprecated: ProductID")
//@XmlType(propOrder={"packageID"
//		,"currentStatus","active","createdDate","modifiedDate","error"})
public class HouseLandPackageCheck {

	private String packageID, active;
	private Date createdDate, modifiedDate, dripModifiedDate;
	private Date availableDate, expiryDate;
	private String error; 
	private HouseLandPackageStatusHistory currentStatus; 
	private SimplePublishing publishing = null;
	
	/* this could potentially include the lot or plan information */

	@XmlElement(name="PackageID")
	public String getPackageID() {
		return packageID;
	}
	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@XmlElement(name="ModifiedDate")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	@XmlElement(name="Error")
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	@ApiProperty(allowableValues="Y|N")
	@XmlElement(name="Active")
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active; 
	}
	
	@XmlElement(name="CurrentStatus")
	public HouseLandPackageStatusHistory getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(HouseLandPackageStatusHistory currentStatus) {
		this.currentStatus = currentStatus; 
	}
	
	public Date getDripModifiedDate() {
		return dripModifiedDate;
	}
	public void setDripModifiedDate(Date dripModifiedDate) {
		this.dripModifiedDate = dripModifiedDate;
	}
	
	@XmlElement(name="IsDripProcessed")
	public boolean isDripProcesses() {
		if (modifiedDate == null || dripModifiedDate == null) return false;
		
		if (!dripModifiedDate.before(modifiedDate)) return true;
		
		return false;
	}
	@XmlElement(name="Publishing")
	public SimplePublishing getPublishing() {
		return publishing;
	}
	public void setPublishing(SimplePublishing publishing) {
		this.publishing = publishing;
	}
	@XmlElement(name="VisibilityStatus")
	public String getVisibilityStatus() {
		if (currentStatus == null || currentStatus.getStatusID() == null) return "Offline";
		
		if (!VisibilityStatus.checkVisibilityStatus(active, currentStatus.getStatusID(), availableDate, expiryDate)) return "Offline";
		
		if ("true".equals(InitServlet.getSystemParam("RPM-Publishing-HouseAndLand"))) {
			if (publishing == null) return "Offline";
			if (!VisibilityStatus.checkPublishedStatus(publishing.getPublishedFlag(), publishing.getStatus(), publishing.getStartDate(), publishing.getEndDate())) return "Offline";
		}
		
		return "Online";
	}
	
	public void setVisibilityStatus(String status) {
		
	}
	@XmlElement(name="AvailableDate")
	public Date getAavalableDate() {
		return availableDate;
	}
	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}
	@XmlElement(name="ExpiryDate")
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
}
