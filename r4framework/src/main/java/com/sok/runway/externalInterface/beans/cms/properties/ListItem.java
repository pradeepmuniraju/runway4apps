package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlElement;

import com.sok.framework.GenRow;

public class ListItem {

	private String	itemID, itemLabel, itemValue;
	
/*
 	<field name="ItemID" type="12" dnull="false" ddefault="" dtype="VARCHAR" dlen="50"/>
 	<field name="ListID" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="ItemLabel" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="ItemValue" type="12" dtype="VARCHAR" dlen="100"/>
	<field name="SortNumber" type="4" dtype="INT" dlen="10"/>
	<field name="Quantifier" type="4" dtype="INT" dlen="10"/>
	<field name="SetStatusID" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="SetLocationID" type="12" dtype="VARCHAR" dlen="50"/>	
	<field name="SetRegionID" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="CanDelete" type="12" ddefault="Y" dtype="VARCHAR" dlen="1"/>
	<field name="ShortValue" type="12" dtype="VARCHAR" dlen="25"/>
	<field name="DisplayField" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="DisplayName" type="12" dtype="VARCHAR" dlen="50"/>
	<field name="Description" type="12" dtype="TEXT"/>
*/
	
	public ListItem() {
	}

	public ListItem(String itemID, String itemLabel, String itemValue) {
		this.itemID = itemID;
		this.itemLabel = itemLabel;
		this.itemValue = itemValue;
	}

	public ListItem(GenRow item) {
		this.itemID = item.getData("ItemID");
		this.itemLabel = item.getData("ItemLabel");
		this.itemValue = item.getData("ItemValue");
	}

	@XmlElement(name="ItemID")
	public String getItemID() {
		return itemID;
	}

	public void setItemID(String itemID) {
		this.itemID = itemID;
	}

	@XmlElement(name="Label")
	public String getItemLabel() {
		return itemLabel;
	}

	public void setItemLabel(String itemLabel) {
		this.itemLabel = itemLabel;
	}

	@XmlElement(name="Value")
	public String getItemValue() {
		return itemValue;
	}

	public void setItemValue(String itemValue) {
		this.itemValue = itemValue;
	}

}
