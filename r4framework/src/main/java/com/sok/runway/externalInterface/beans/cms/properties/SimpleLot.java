package com.sok.runway.externalInterface.beans.cms.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.NONE)
@XmlSeeAlso(Lot.class)
@XmlRootElement(name="Lot")
@XmlType(propOrder={"lotID","name","cost","subType","stage" })		//MarketingStatus AvailableDate
public abstract class SimpleLot {

	protected String lotID, name, subType;
	protected Double cost = null;
	protected SimpleStage stage = null;
	
	public SimpleLot() {}
	
	@XmlElement(name="LotID")
	public String getLotID() {
		return lotID;
	}
	public void setLotID(String lotID) {
		this.lotID = lotID;
	}
	@XmlElement(name="Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name="Cost")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	@XmlElement(name="SubType")
	public String getSubType() {
		return subType;
	}
	public void setSubType(String SubType) {
		this.subType = SubType;
	}
	@XmlElement(name="Stage")
	public SimpleStage getStage() {
		return stage;
	}
	public void setStage(SimpleStage stage) {
		this.stage = stage;
	}
}
