package com.sok.runway.externalInterface.beans.shared;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class OrderProfile extends Profile
{
	private OrderQuestion[] questions = null;
	
	@JsonProperty("Questions")
	public OrderQuestion[] getQuestions()
	{
		return(questions);
	}	
	
	@JsonProperty("Questions")
	public void setQuestions(OrderQuestion[] questions)
	{
		this.questions = questions;
	}	
	
	@Override
	public String toString() {
		return String.format("OrderProfile(%1$s)", this.getSurveyID());
	}
}