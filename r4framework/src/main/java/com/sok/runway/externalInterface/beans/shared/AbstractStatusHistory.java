package com.sok.runway.externalInterface.beans.shared;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="Status")
@XmlType(propOrder={"statusID", "status", "createdDate", "createdByName", "createdBy"})
public abstract class AbstractStatusHistory {
	
	private String createdBy = null, statusID, status; 
	private Date createdDate = null;
	private PersonName createdByName = null;
	
	@XmlElement(name="StatusID")
	public String getStatusID() {
		return statusID;
	}
	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}
	
	@XmlElement(name="Status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@XmlElement(name="CreatedByName")
	public PersonName getCreatedByName() {
		return(createdByName);
	}
	public void setCreatedByName(PersonName createdByName) {
		this.createdByName = createdByName;
	}	
	
	@XmlElement(name="CreatedDate")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@XmlElement(name="CreatedBy")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
}
