package com.sok.runway.externalInterface.beans.shared;
import java.util.*;

import org.codehaus.jackson.annotate.JsonProperty;
/**
 * 
 * @author mike
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class OpportunityQuestion extends Question
{
	private OpportunityAnswer answer = null;
	
	@JsonProperty("Answer")
	public OpportunityAnswer getAnswer()
	{
		return(answer);
	}	
	
	@JsonProperty("Answer")
	public void setAnswer(OpportunityAnswer answer)
	{
		this.answer = answer;
	}
}