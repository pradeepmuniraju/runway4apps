package com.sok.runway.externalInterface.beans;

import java.util.*;
import com.sok.runway.externalInterface.beans.shared.*;

/**
 * 
 * @author jong
 *
 * Java Bean for data transport only, contains no business logic.
 * 
 */
public class CompanyProfiles
{
	private CompanyProfile[] profiles = null;
	
	private String companyid = null;
	
	private String error = null;
	
	public String getError()
	{
		return(error);
	}	
	
	public void setError(String error)
	{
		this.error = error;
	}
	
	public String getCompanyID()
	{
		return(companyid);
	}	
	
	public void setCompanyID(String companyid)
	{
		this.companyid = companyid;
	}		
	
	public CompanyProfile[] getProfiles()
	{
		return(profiles);
	}	
	
	public void setProfiles(CompanyProfile[] profiles)
	{
		this.profiles = profiles;
	}
}