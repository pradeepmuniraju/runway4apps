package com.sok.runway;

import java.util.HashMap;
import java.util.ArrayList;
import java.sql.*;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.jsp.*; 

import org.apache.commons.lang.StringUtils;

import java.io.*;
import com.sok.framework.*;
import com.sok.runway.view.ValueList;

public class ItemLists
{
	HashMap<String, SingleItemList> lists = new HashMap<String, SingleItemList>();
	Connection conn = null;
	String datasource	= null;
	boolean closeconnection = true;
	boolean useDefaults = true; 
	UserBean user = null;
   
	static final int OPTION = 1;
	static final int CHECKBOX = 2;
	static final int RADIO = 3;
	static final int SEARCHCHECKBOX = 4;
	static final int CHECKBOX_RIGHT = 5;
	static final int RADIO_RIGHT = 6;

	StringBuffer errors = new StringBuffer();

	public ItemLists() {
	   datasource = ActionBean.getJndiName();
   }

	public boolean hasValues(String listname)
	{
		SingleItemList list = getList(listname, null);
		if(list!=null && list.size()==0)
		{
			return(false);
		}
		return(true);
		
	}
	
	public void setUseDefaults(boolean useDefaults) { 
		this.useDefaults = useDefaults; 
	}

	public void setUser(UserBean u)
	{
		user = u;
	}

	/*public boolean print(String selectedvalue, String listname, int type, JspWriter out)
	{
		print(null, selectedvalue, listname, null, type, out, false);
	}

	public boolean print(String selectedvalue, String listname, int type, JspWriter out, boolean includeQt)
	{
	   print(null, selectedvalue, listname, null, type, out, includeQt);
	}
	
	public boolean print(String inputname, String selectedvalue, String listname, int type, JspWriter out)
	{
		print(inputname, selectedvalue, listname, null, type, out, false);
	}
	
	public boolean print(String inputname, String selectedvalue, String listname, int type, JspWriter out, boolean includeQt)
	{
		print(inputname, selectedvalue, listname, null, type, out, includeQt);
	}
	
	public boolean print(String inputName, String selectedvalue, String listname, String stmt, int type, JspWriter out, boolean includeQt)
	{
		//LinkedHashMap list = getList(listname, stmt);
		SingleItemList list = getList(listname, stmt);

		try{
		if(type==OPTION)
		{
			printOptions(selectedvalue, list, out, includeQt);
		}
		else if(type==CHECKBOX)
		{
			printCheckboxes(inputName, selectedvalue, list, out, false);
		}
		else if(type==SEARCHCHECKBOX)
		{
			printCheckboxes(inputName, selectedvalue, list, out, true);
		}
		else if(type==RADIO)
		{
			printRadioButtons(inputName, selectedvalue, list, out, false);
		}
		else if(type==RADIO_RIGHT)
		{
			printRadioButtonsRight(inputName, selectedvalue, list, out, false);
		}
		else if(type==CHECKBOX_RIGHT)
		{
			printCheckboxesRight(inputName, selectedvalue, list, out, false);
		}
		}catch(Exception e){}
	}*/
	

	public SingleItemList getList(String listname) {
	   return getList(listname, null);
   }   
   
	public SingleItemList getList(String listname, String stmt)
	{
	   SingleItemList list = (SingleItemList)lists.get(listname);
		//LinkedHashMap list = (LinkedHashMap)lists.get(listname);
		if(list == null)
		{
		   list = loadList(listname, stmt);
		}
		return(list);
	}
	
   public SingleItemList loadList(String listname)
   {
      return(loadList(listname, null));
   }
	
	public SingleItemList loadList(String listname, String stmt)
	{
      SingleItemList list = null;

         if(stmt != null)
         {
            list = getListFromDB(listname, stmt);
         }
         else
         {
            list = getListFromDB(listname, getItemListStmt(listname));
         }
         lists.put(listname, list);

      return(list);
	}

	public void setList(String listname, String[] values)
	{
	   setList(listname, values, null);
	}
	
	public String getDefaultValue(String listname)
	{
		SingleItemList sil = getList(listname);
		return (sil.getDefaultValue());	
	}
	
	public void reload(String listname)
	{
		lists.put(listname, null);
		ValueList.reload(listname);
	}
	
	public void setList(String listname, String[] values, int[] quantifiers)
	{
		setList(listname, values, quantifiers, null);
	}
	
	public void setList(String listname, String[] values, int[] quantifiers, String defaultValue)
	{
		setList(listname, values, quantifiers, defaultValue, null, null, null);
	}
	
	public void setList(String listname, String[] values, int[] quantifiers, String defaultValue, String[] displayFields, String[] displayNames, String[] canDeleteList)
	{
	   if(listname != null && listname.length() > 0) {
   	   SingleItemList list = new SingleItemList();
   		//LinkedHashMap list = new LinkedHashMap();
   		
		if (quantifiers != null && displayFields!= null && displayNames!= null) {
			for (int i = 0; i < quantifiers.length; i++) {
				list.put(values[i], values[i], quantifiers[i], displayFields[i], displayNames[i], canDeleteList[i]);
			}
		} else if (quantifiers != null) {
      		for(int i=0; i<quantifiers.length; i++) {
      			list.put(values[i],values[i],quantifiers[i]);
      		}
   		}
   		else {
      		for(int i=0; i<values.length; i++) {
      			list.put(values[i],values[i],0);
      		}
   		}
   		
   		if (defaultValue != null && defaultValue.length() > 0) 
   		{
   			list.setDefaultValue(defaultValue);
   		}
   		lists.put(listname, list);
   		saveList(listname, list);
   		ValueList.reload(listname);
	   }
	}

	String getColumnGroupStatement(String table, String column)
	{
		StringBuffer stmt = new StringBuffer();
		stmt.append("select ");
		stmt.append(column);
		stmt.append(" from ");
		stmt.append(table);
		stmt.append(" where CreatedDate > dateadd(Month , -2, getdate()) group by ");
		stmt.append(column);
		return(stmt.toString());
	} 

	public static String getItemListStmt(String listname)
	{
		StringBuffer stmt = new StringBuffer();
		stmt.append("select ListItems.ItemLabel, ListItems.ItemValue, ListItems.Quantifier, ItemLists.DefaultValue, ListItems.DisplayField, ListItems.DisplayName, ListItems.CanDelete from ItemLists, ListItems ");
		stmt.append("where ItemLists.ListName = '");
		stmt.append(listname);
		stmt.append("' and ListItems.ListID = ItemLists.ListID order by SortNumber, ItemLabel, ItemValue");
		return(stmt.toString());
	}

	void checkConnection() throws SQLException
	{
		if(conn==null || conn.isClosed())
		{
			conn = ActionBean.connect(); 
			closeconnection = true;
		}
	}

	public void setJndiName(String t)
	{
		datasource = t;
	}

	public void setCloseConnection(boolean b)
	{
		closeconnection = b;
	}

	public void setConnection(Object con)
	{
		if (con != null) {
			this.conn = (Connection)con;
			closeconnection = false;
		}
	}

	public void setConnection(Connection con)
	{
		if (con != null) {
			this.conn = con;
			closeconnection = false;
		}
	}

	public Connection getConnection()
	{
		return(conn);
	}

	public String getErrors()
	{
		String errorstring = errors.toString();
		errors = new StringBuffer();
		return(errorstring);
	}

	public String getListID(String listname)
	{
		try{
			checkConnection();
		}
		catch(Exception e)
		{
			errors.append("At getListID(): ");
			errors.append(e.toString());
		}
		
      GenRow row = new GenRow();
      row.setConnection(conn);
      row.setTableSpec("ItemLists");
      row.setParameter("-select1", "ListID");
      row.setParameter("ListName", listname);
      
      if(listname.length()!=0)
      {
         row.doAction("selectfirst");
      }
		return(row.getString("ListID"));
	}

	public void deleteList(String listid)
	{
	   GenRow row = new GenRow();
      try
      {
         checkConnection();
      }
      catch (Exception ex)
      {
         errors.append(ex.getMessage());
         //throw new ServletException(ex.getMessage(), ex);
      }

	   row.setConnection(conn);
	   row.setTableSpec("ListItems");
	   row.setParameter("ON-ListID", listid);
	   row.doAction("deleteall");
	}
	
   public void deleteList(String[] itemids)
   {
      GenRow row = new GenRow();
      try
      {
         checkConnection();
      }
      catch (Exception ex)
      {
         errors.append(ex.getMessage());
         //throw new ServletException(ex.getMessage(), ex);
      }
      
      for(int i=0; i<itemids.length; i++)
      {
         row.setConnection(conn);
         row.setTableSpec("ListItems");
         row.setParameter("ItemID", itemids[i]);
         row.doAction("delete");
      }      
   }	

   String[] getItemIDs(String listid)
   {
      try
      {
         checkConnection();
      }
      catch (Exception ex)
      {
         errors.append(ex.getMessage());
      }      
      String[] itemids = null;
      if(listid.length()!=0)
      {
         GenRow row = new GenRow();
         row.setConnection(conn);
         row.setTableSpec("ListItems");
         row.setParameter("-select1", "ItemID");
         row.setParameter("ListID", listid);
         row.doAction("search");
         row.getResults(true);
         itemids = new String[row.getSize()];
         for(int i=0; i<itemids.length; i++)
         {
            if(row.getNext())
            {
               itemids[i] = row.getString("ItemID");
            }
         }
      }
      return(itemids);
   }
   
   public synchronized void saveList(String listname, SingleItemList list)
   {
      String listid = getListID(listname);
      String[] itemids = getItemIDs(listid);
      try
      {
         checkConnection();
      }
      catch (Exception ex)
      {
         errors.append(ex.getMessage());
      }    
      GenRow row = new GenRow();
      row.setTableSpec("ListItems");
      int sort = 0;
      int size = list.size();
      for (int i=0; i < size; i++) 
      {
         row.setConnection(conn);
         row.createNewID();
         row.setParameter("ListID", listid);
         row.setParameter("ItemValue", list.getValue(i));
         row.setParameter("SortNumber", String.valueOf(sort));
         row.setParameter("Quantifier", String.valueOf(list.getQuantifier(i)));
         row.setParameter("DisplayField", String.valueOf(list.getDisplayField(i)));
         row.setParameter("DisplayName", String.valueOf(list.getDisplayName(i)));
         row.setParameter("CanDelete", String.valueOf(list.getCanDelete(i)));
         row.doAction("insert");
         sort++;
      }
      if(size!=0)
      {
         deleteList(itemids);
      }
   }
   
   /*
	public void saveList(String listname, SingleItemList list)
	{

		String listid = getListID(listname);
		deleteList(listid);

		StringBuffer updatestmt = new StringBuffer();
		updatestmt.append("insert ListItems (ItemID, ListID, ItemValue, SortNumber, Quantifier) values(");
		updatestmt.append(ActionBean.getDatabase().getNewIDMethod());
		updatestmt.append(", ?, ?, ?, ?)");
		PreparedStatement updateprestmt = null;
		try
		{
			checkConnection();
			updateprestmt = conn.prepareStatement(updatestmt.toString());

			//Iterator it = list.keySet().iterator();
			int r = -1;
			int sort = 0;
			for (int i=0; i < list.size(); i++) {
				updateprestmt.setString(1, listid);
				updateprestmt.setString(2, list.getValue(i));
				updateprestmt.setInt(3, sort);
				updateprestmt.setInt(4, list.getQuantifier(i));
				r = updateprestmt.executeUpdate();
				sort++;
			}
		}
		catch (Exception ex)
		{
			errors.append(ex.getMessage());
    		//throw new ServletException(ex.getMessage(), ex);
		}
		finally
		{
	    	if (updateprestmt != null) 
	    	{
	       	try{
	           	updateprestmt.close();
	       	}catch (Exception ex){}
	    	}
			close(false);
		}
	}
*/
	public SingleItemList getListFromDB(String listname, String stmtstring)
	{
		Statement stmt = null;
		ResultSet current = null;
		SingleItemList list = new SingleItemList();
		try{
			checkConnection();
			stmt = conn.createStatement();
			current = stmt.executeQuery(stmtstring);

			//ResultSetMetaData meta = current.getMetaData();
			//int columncount = meta.getColumnCount();

			String name = null;
			String value = null;
			int quantifier = 0;
			String displayField = null;
			String displayName = null;
			String canDelete = null;

			while(current.next())
			{
				list.setDefaultValue(current.getString(4));
				name = current.getString(1);
				value = current.getString(2);
				quantifier = current.getInt(3);
				
				displayField = StringUtils.isNotBlank(current.getString(5)) ? current.getString(5) : "";
				displayName = StringUtils.isNotBlank(current.getString(6)) ? current.getString(6) : "";
				canDelete = StringUtils.isNotBlank(current.getString(7)) ? current.getString(7) : "";
				
				if (name == null || name.length() == 0) {
				   name = value;
				}
				//list.put(name,value,quantifier);
				list.put(name,value,quantifier,displayField,displayName,canDelete);
			}
		}
		catch(Exception e)
		{
			errors.append("At getListFromDB(String, Connection): ");
			errors.append(e.toString());
		}
		finally
		{
	    	if (current != null) 
	    	{
	       	try{
	           	current.close();
	       	}catch (Exception ex){}
	    	}
	    	if (stmt != null) 
	    	{
	     		try{       
	           	stmt.close();
	     		}catch (Exception ex){}
	  		}
			close();
		}
		return(list);
	}

	public boolean printOptions(String listname, JspWriter out)
	{
		return printOptions(null, listname, out, false);
	}
	
	public boolean printOptions(boolean forSearch, String listname, JspWriter out)
	{
		return printOptions(forSearch, null, listname, out, false);
	}
	
	public boolean printOptions(String listname, JspWriter out, boolean includeQt)
	{
		return printOptions(null, listname, out, includeQt);
	}

	public boolean printOptions(String selectedvalue, String listname, JspWriter out)
	{
		return printOptions(selectedvalue, listname, out, false);
	}

	public boolean printOptions(boolean forSearch, String selectedvalue, String listname, JspWriter out)
	{
		return printOptions(forSearch, selectedvalue, listname, out, false);
	}
	
	public boolean printOptions(String selectedvalue, String listname, JspWriter out, boolean includeQt)
	{
	   return printOptions(false, selectedvalue, listname, out, includeQt);
	}	
	
	public boolean printOptions(boolean forSearch, String selectedvalue, String listname, JspWriter out, boolean includeQt)
	{
	   try {
	      SingleItemList list = getList(listname, null);
		   return printOptions(forSearch, selectedvalue, null, list, out, includeQt, false);
	   }
	   catch (Exception e) {}
	   
	   return false;
	}
	
	public boolean printOptionsForEdit(String listname, JspWriter out)
	{
	   try {
	      SingleItemList list = getList(listname, null);
		   return printOptions(false, null, null, list, out, true, true);
	   }
	   catch (Exception e) {}
	   
	   return false;
	}

	public boolean optionsFromColumnGroup(String table, String column, JspWriter out)
	{
		//print(null, null, table+column, getColumnGroupStatement(table, column), OPTION, out);
		try {
	      SingleItemList list = getList(table+column, getColumnGroupStatement(table, column));
		   return printOptions(false, null, null, list, out, false, false);
	   }
	   catch (Exception e) {}
		
		return false;
	}
	
	public boolean printOptions(String selectedvalue, String selectedname, String listname, JspWriter out)
	{
	   try {
	      SingleItemList list = getList(listname, null);
		  return printOptions(false, selectedvalue, selectedname, list, out, false, false);
	   }
	   catch (Exception e) {}
	   
	   return false;
	}
	
	protected boolean printOptions(boolean forSearch, String selectedvalue, String selectedname, SingleItemList list, JspWriter out, boolean includeQt, boolean qtInLabel)throws IOException
	{
		boolean ret = false;
		if(list!=null)
		{
			String defaultvalue = null;
			//boolean found = false;
			//Iterator it = list.keySet().iterator();
			String name = null;
			String value = null;
			int quantifier = 0;  
			if ((selectedvalue == null || selectedvalue.equals("")) && useDefaults)
			{
				defaultvalue = list.getDefaultValue();
				selectedvalue = defaultvalue;
				if(selectedvalue!=null && !"".equals(selectedvalue) && selectedvalue.lastIndexOf("::")!= -1)
					selectedvalue = selectedvalue.substring(0,selectedvalue.lastIndexOf("::"));
			}
			MultiField mf = new MultiField(selectedvalue);
			for (int i=0; i<list.size(); i++) {
			   name = list.getName(i);
			   value = list.getValue(i);
			   quantifier = list.getQuantifier(i);
				out.print("<option value=\"");
				if (forSearch) {
				   out.print("%");
				}
				out.print(value);
				if (includeQt) {
				   out.print("::");
				   out.print(quantifier);
			   }
				if (forSearch) {
				   out.print("%");
				   //value = "%" + value + "%";
				}
				out.print("\" ");
				if(mf.contains(value, forSearch))
				{
					out.print("selected=\"selected\" ");
					ret = true;
					//found = true;
				}
				out.print(">");
				out.print(name);
				if (qtInLabel) {
				   out.print(" - (");
				   out.print(quantifier);
				   out.print(")");
				}
				out.print("</option>");
			}
   	   /*for (int j=0; j < mf.length(); j++) {
   	      selectedvalue = mf.get(j,forSearch);
   	      if (selectedvalue.length() > 0 && list.getIndexOfValue(selectedvalue) == -1) {
   				out.print("<option value=\"");
   				out.print(selectedvalue);
   				if (includeQt) {
   				   out.print("::0");
   			   }
   				out.print("\" ");
   				out.print("selected=\"selected\" ");
   				//found = true;
   				out.print(">");
   				out.print(selectedvalue);
   				out.print("</option>");
   			}
   		}*/
		}
		
		return ret;

	}

	public boolean printCheckboxes(String inputname, String listname, JspWriter out)
	{
		return printCheckboxes(inputname, null, listname, out, false);
	}
	
	public boolean printCheckboxes(String inputname, String listname, JspWriter out, boolean includeQt)
	{
		return printCheckboxes(inputname, null, listname, out,includeQt);
	}

	public boolean printCheckboxes(String selectedvalue, String inputname, String listname, JspWriter out)
	{
		return printCheckboxes(selectedvalue, inputname, listname, out, false);
	}
	
	public boolean printCheckboxes(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt)
	{
		return printCheckboxes(selectedvalue, inputname, listname, out, includeQt,0,0);
   }

	public boolean printCheckboxes(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt, int printAmount, int startAt, boolean disabled)
	{
		return printCheckboxes(selectedvalue, inputname, listname, out, false, includeQt, printAmount, startAt, disabled);
	}

	public boolean printCheckboxes(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt, int printAmount, int startAt)
	{
		return printCheckboxes(selectedvalue, inputname, listname, out, false, includeQt, printAmount, startAt, false);
	}
	
	public boolean printSearchCheckboxes(String inputname, String listname, JspWriter out)
	{
		return printCheckboxes(null, inputname, listname, out, true, false);
	}

	public boolean printCheckboxes(String selectedvalue, String inputname, String listname, JspWriter out, boolean search, boolean includeQt)
	{
		return printCheckboxes(selectedvalue, inputname, listname, out, search, includeQt, 0, 0, false);
   }
   
	public boolean printCheckboxes(String selectedvalue, String inputname, String listname, JspWriter out, boolean search, boolean includeQt, int printAmount, int startAt)
	{
		return printCheckboxes(selectedvalue, inputname, listname, out, search, includeQt, printAmount, startAt, false);
   }
   
	public boolean printCheckboxes(String selectedvalue, String inputname, String listname, JspWriter out, boolean search, boolean includeQt, int printAmount, int startAt, boolean disabled)
	{
		   try {
		      SingleItemList list = getList(listname, null);
		      return printCheckboxes(selectedvalue, inputname, list, out, search, includeQt, printAmount, startAt, disabled);
		   }
		   catch (Exception e) {}
		   
		   return false;
	}
	
	protected boolean printCheckboxes(String selectedvalue, String inputname, SingleItemList list, JspWriter out, boolean search, boolean includeQt, int printAmount, int startAt, boolean disabled)throws IOException
	{
		return printInputs(false, "checkbox", selectedvalue, inputname, list, out, search, includeQt, printAmount, startAt, disabled);
	}

	public boolean printRadioButtons(String inputname, String listname, JspWriter out)
	{
		return printRadioButtons(null, inputname, listname, out, false,0,0);
	}
	
	public boolean printRadioButtons(String inputname, String listname, JspWriter out, boolean includeQt)
	{
		return printRadioButtons(null, inputname, listname, out, includeQt,0,0);
	}

	public boolean printRadioButtons(String selectedvalue, String inputname, String listname, JspWriter out)
	{
		return printRadioButtons(selectedvalue, inputname, listname, out, false,0,0);
	}
	
	public boolean printRadioButtons(String selectedvalue, String inputname, String listname, JspWriter out, int printAmount, int startAt)
   {
		return printRadioButtons(selectedvalue, inputname, listname, out, false, printAmount, startAt);
	}
	
	public boolean printRadioButtons(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt)
	{
		return printRadioButtons(selectedvalue, inputname, listname, out, false,0,0);
	}
	
	public boolean printRadioButtons(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt, int printAmount, int startAt)
	{
		return printRadioButtons(selectedvalue, inputname, listname, out, includeQt, printAmount, startAt, false);
	}
	
	public boolean printRadioButtons(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt, int printAmount, int startAt, boolean disabled)
	{
	   try {
	      SingleItemList list = getList(listname, null);
	      return printRadioButtons(selectedvalue, inputname, list, out, false, includeQt, printAmount, startAt, disabled);
	   }
	   catch (Exception e) {}
	   
	   return false;
	}
	
	protected boolean printRadioButtons(String selectedvalue, String inputname, SingleItemList list, JspWriter out, boolean search, boolean includeQt, int printAmount, int startAt, boolean disabled)throws IOException
	{
		return printInputs(false, "radio", selectedvalue, inputname, list, out, search, includeQt, printAmount, startAt, disabled);
	}

	public boolean printCheckboxesRight(String inputname, String listname, JspWriter out)
	{
		return printCheckboxesRight(null, inputname, listname, out, false);
	}
	
	public boolean printCheckboxesRight(String inputname, String listname, JspWriter out, boolean includeQt)
	{
		return printCheckboxesRight(null, inputname, listname, out, includeQt);
	}
	
	public boolean printCheckboxesRight(String selectedvalue, String inputname, String listname, JspWriter out)
	{
		return printCheckboxesRight(selectedvalue, inputname, listname, out, false);
	}
	
	public boolean printCheckboxesRight(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt)
	{
		return printCheckboxesRight(selectedvalue, inputname, listname, out, includeQt,0,0);
   }
   
	public boolean printCheckboxesRight(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt, int printAmount, int startAt)
	{
		return printCheckboxesRight(selectedvalue, inputname, listname, out, includeQt, printAmount, startAt, false);
	}
	
	public boolean printCheckboxesRight(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt, int printAmount, int startAt, boolean disabled)
	{
	   try {
	      SingleItemList list = getList(listname, null);
	      return printCheckboxesRight(selectedvalue, inputname, list,  out, false, includeQt, printAmount, startAt, disabled);
	   }
	   catch (Exception e) {}
	   
	   return false;
	}
	
	protected boolean printCheckboxesRight(String selectedvalue, String inputname, SingleItemList list, JspWriter out, boolean search, boolean includeQt, int printAmount, int startAt, boolean disabled)throws IOException
	{
		return printInputs(true, "checkbox", selectedvalue, inputname, list, out, search, includeQt, printAmount, startAt, disabled);
	}

	public boolean printRadioButtonsRight(String inputname, String listname, JspWriter out)
	{
		return printRadioButtonsRight(null, inputname, listname, out, false);
	}
	public boolean printRadioButtonsRight(String inputname, String listname, JspWriter out, boolean includeQt)
	{
		return printRadioButtonsRight(null, inputname, listname, out, includeQt);
	}

	public boolean printRadioButtonsRight(String selectedvalue, String inputname, String listname, JspWriter out)
	{
		return printRadioButtonsRight(selectedvalue, inputname, listname, out, false);
	}
	
	public boolean printRadioButtonsRight(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt)
	{
		return printRadioButtonsRight(selectedvalue, inputname, listname, out, includeQt,0,0,false);
   }

	public boolean printRadioButtonsRight(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt, int printAmount, int startAt)
	{
		return printRadioButtonsRight(selectedvalue, inputname, listname, out, includeQt, printAmount, startAt, false);
   }
   
	public boolean printRadioButtonsRight(String selectedvalue, String inputname, String listname, JspWriter out, boolean includeQt, int printAmount, int startAt, boolean disabled)
	{
	   try {
	      SingleItemList list = getList(listname, null);
	      return printRadioButtonsRight(selectedvalue, inputname, list, out, false, includeQt, printAmount, startAt, disabled);
	   }
	   catch (Exception e) {}
	   
	   return false;
	}

	protected boolean printRadioButtonsRight(String selectedvalue, String inputname, SingleItemList list, JspWriter out, boolean search, boolean includeQt, int printAmount, int startAt, boolean disabled)throws IOException
	{	
		return printInputs(true, "radio", selectedvalue, inputname, list, out, search, includeQt, printAmount, startAt, disabled);
	}
	
	protected boolean printInputs(boolean right, String type, String selectedvalue, String inputname, SingleItemList list, JspWriter out, boolean search, boolean includeQt, int printAmount, int startAt, boolean disabled)throws IOException
	{
		boolean ret = false;
		MultiField mf = null;
		String defaultvalue = null;
		if(selectedvalue == null || selectedvalue.equals("")){
			defaultvalue = list.getDefaultValue();
			selectedvalue = defaultvalue;
		}
		if(selectedvalue!=null){
			if(!"".equals(selectedvalue) && selectedvalue.lastIndexOf("::")!= -1)
				selectedvalue = selectedvalue.substring(0,selectedvalue.lastIndexOf("::"));
			mf = new MultiField(selectedvalue);
		}
		if(list!=null)
		{
			String name = null;
			String value = null;
			int quantifier = 0;
			for (int i=0; i<list.size(); i++) {
			   if (printAmount == 0 || (i >= startAt && i < startAt + printAmount)) {
   			   name = list.getName(i);
   			   value = list.getValue(i);
   			   quantifier = list.getQuantifier(i);
   			   if (right) {
   				   out.print(name);
   				}
   				out.print("<input name=\"");
   				out.print(inputname);
   				out.print("\" type=\"");
   				out.print(type);
   				out.print("\" value=\"");
   				if(search) {
   					if (type.equals("radio")) {
	   				   out.print(value);
   					}
   					else {
	   					out.print("%");
	   				   out.print(value);
	   					out.print("%");
   					}
   				}
   				else {
   				   out.print(value);
      				if (includeQt) {
      				   out.print("::");
      				   out.print(quantifier);
      			   }
   			   }
   			   out.print("\"");
   				if(mf!=null && mf.contains(value, search)) {
   					out.print(" checked=\"checked\"");
   					ret = true;
   				}
   				if (disabled) {
   				   out.print(" disabled=\"true\" readonly=\"true\"");
   				}
   				out.print(" />");
   			   if (!right) {
   				   out.print(name);
   				}
   				out.print("<br/>");
   			}
			}
			if (printAmount == 0 || (startAt + printAmount > list.size())) {
   			if(selectedvalue!=null && selectedvalue.length()!=0) {
   			   //Print those that aren't in the List
   			   String multi = null;
   			   int index = 0;
   			   for (int j=0; j < mf.length(); j++) {
   			      multi = mf.get(j,search);
   			      index = list.getIndexOfValue(multi);
   			      if (index == -1) {
         				out.print(multi);
         				out.print("<input name=\"");
         				out.print(inputname);
         				
         				out.print("\" type=\"");
         				out.print(type);
         				out.print("\" value=\"");
   				
         				out.print(multi);
         				if (includeQt) {
         				   out.print("::0");
         			   }
         				out.print("\"");
         				out.print(" checked=\"checked\"");
         				if (disabled) {
         				   out.print(" disabled=\"true\"");
         				}
         				out.print(" />");
         				out.print("<br/>");
         			}
         		}
   			}
   		}
		}
		
		return ret;
	}

	protected void close(boolean setnull)
	{
		if(conn!=null)
		{
			if(closeconnection)
			{
				try{
					conn.close();
				}catch(Exception e){}
			}
			if(setnull)
			{
				conn = null;
			}
		}
	}	

	public void close()
	{
		close(true);
	}

   public String markup(String field, String listname) {
      return markup(field, listname, "<br>");
   }
   
   public String markup(String field, String listname, String seperator) {
      SingleItemList list = getList(listname, null);
      
		StringBuffer temp = new StringBuffer();
		if(field!=null)
		{
         MultiField mf = new MultiField(field);
			for(int i=0; i< mf.length();i++) {
			   if (temp.length() > 0) {
               temp.append(seperator);
            }
            temp.append(list.getNameFromValue(mf.get(i)));
			}		
			return(temp.toString());
		}
		return("");

   }
}
