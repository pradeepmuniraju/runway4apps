package com.sok.runway;

import javax.servlet.*;     
import javax.servlet.http.*;
import java.util.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import java.text.*;
import com.sok.framework.*;
import com.sok.runway.campaigns.*;

public class ApplyEvent extends OfflineProcess
{
	StringBuffer status = new StringBuffer();
	
	String campaignID = null;
	String eventID = null;
	
	UserBean user = null;
	
	Vector<String> items = new Vector<String>(100);
	
	int proccessed = 0;
	boolean forcontacts = true;
   HttpSession session = null;
   String eventId = null;
   
	Connection con = null;
	Context ctx = null;
	
	public ApplyEvent(HttpServletRequest request, ServletContext context, String eventID)
	{
		super(request, context);
		this.eventID = eventID;
	   session = request.getSession();
		user = (UserBean)session.getAttribute(SessionKeys.CURRENT_USER);
			
		start();
	}

	void getConnection() throws Exception {
		if(con==null || con.isClosed()) {
			ctx = new InitialContext();

			DataSource ds = null;
				try{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource)envContext.lookup(dbconn);
				}catch(Exception e){}
			if(ds == null) {
				ds = (DataSource)ctx.lookup(dbconn);
			}
			con = ds.getConnection();
		}
	}
	
	void retrieveItems(TableData searchbean, String idField)throws Exception
	{
		if(debug) {		  
			errors.append(searchbean.getSearchStatement());
		}			
		try {
		   getConnection();
		   searchbean.setConnection(con);
		   RowSetWrapperBean list = new RowSetWrapperBean();		
		   list.setConnection(con);
		   list.setSearchBean(searchbean);
		   list.getResults();
		   
		   TableData resultset = list.getResultBean();
                   int i = 0;
		   while (list.getNext()) {
                      i++;
				items.add(resultset.getString(idField));
			}                   
		}		
		finally {    		
			try{
            	con.close();
        	}catch (Exception ex){}    		  		    
		}
		
	}
                
	void applyCampaign(String idField) throws Exception {

      getConnection();
		
		for(int i=0; i< items.size() && !isKilled(); i++) {
		   
		   Campaign campaign = new Campaign(con, campaignID);
		   campaign.setCurrentUser(user.getUser());
		   
		   if (idField.equals("CompanyID")) {
   		   if (eventID != null && eventID.length() != 0) {
   		      campaign.applyCompanyCampaignAndEvent(items.get(i), eventID);
   		   }
   		   else {
   		      campaign.applyCompanyCampaign(items.get(i), true);
   		   }
   		}
   		else if (idField.equals("ContactID")) {
   			
   			
   		   if (eventID != null && eventID.length() != 0) {
   		      campaign.applyContactCampaignAndEvent(items.get(i), eventID);
   		   }
   		   else {
   		      campaign.applyContactCampaign(items.get(i), true);
   		   }
   		}
   		
			proccessed++;
		}
	}


	public void execute()
	{
		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, getLocale());
			dt.setTimeZone(TimeZone.getTimeZone("Australia/Melbourne"));
		status.append(context.getServletContextName());
		status.append(" Runway offline process - Apply Campaign\r\n");
		status.append("Initiated ");
		status.append(dt.format(new java.util.Date()));
		status.append("\r\n");
		
		campaignID = requestbean.getString("CampaignID");
		//eventID = requestbean.getString("EventID");
		com.sok.Debugger.getDebugger().debug("EventId is : "+eventID);
		TableData searchbean1 = null;
		TableData searchbean2 = null;
		try
		{
			String idField = null;
			
			searchbean1 = (TableData)session.getAttribute(SessionKeys.CONTACT_SEARCH);
			if(searchbean1 != null) {
				searchbean1.setViewSpec("OfflineContactView");
				// we need to remove all group by
				for (int x = 0; x < 10; ++x) {
					searchbean1.remove("-groupby" + x);
					searchbean1.remove("-sort" + x);
					searchbean1.remove("-order" + x);
				}
				searchbean1.setParameter("-groupby0", "ContactID");
				idField = "ContactID";
			
				retrieveItems(searchbean1, idField);				
				applyCampaign(idField);
				status.append("Campaign applied to ");
				status.append(String.valueOf(proccessed));
				status.append(" of ");
				status.append(String.valueOf(items.size()));	
				status.append(" contacts.\r\n");
			}
			searchbean2 = (TableData)session.getAttribute(SessionKeys.COMPANY_SEARCH);
			if(searchbean2 != null){
				items.clear();
				proccessed = 0;
				searchbean2.setViewSpec("OfflineCompanyView");
				// we need to remove all group by
				for (int x = 0; x < 10; ++x) {
					searchbean2.remove("-groupby" + x);
					searchbean2.remove("-sort" + x);
					searchbean2.remove("-order" + x);
				}
				searchbean2.setParameter("-groupby0", "ContactID");
				idField = "CompanyID";
		
				retrieveItems(searchbean2, idField);				
				applyCampaign(idField);
				status.append("Campaign applied to ");
				status.append(String.valueOf(proccessed));
				status.append(" of ");
				status.append(String.valueOf(items.size()));	
				status.append(" companies.\r\n");
			}
						
   		if (isKilled()) {
   	      status.append("Offline process terminated by - ");
   	      status.append(getKiller());
   	      status.append("\r\n\r\n");
   	   }
   	   else {	   
   	      status.append("Offline process completed.\r\n\r\n");
   	   }
		}
		catch(Exception e)
		{
			status.append("Offline process halted on ");
			status.append(String.valueOf(proccessed));
			status.append(" of ");
			status.append(String.valueOf(items.size()));
			status.append(".\r\n\r\n");
			status.append(ActionBean.writeStackTraceToString(e));				
			
		   if (e instanceof java.sql.SQLException && searchbean1 != null) {		      
		      status.append(searchbean1.getSearchStatement());
		   }
		   if (e instanceof java.sql.SQLException && searchbean2 != null) {		      
			   status.append(searchbean1.getSearchStatement());
		   }

		}
	}

	public String getStatusMailBody()
	{
		return(status.toString());
	}

	public String getStatusMailSubject()
	{
		return(context.getServletContextName() + " Runway offline process - Apply Campaign");
	}

	public String getStatusMailRecipient()
	{
		return user.getEmail();
	}
	
	
   public int getProcessSize() {
      return items.size();
   }
   
   public int getProcessedCount() {
      return proccessed;
   }
   
   public int getErrorCount() {
      return -1;
   }
}
