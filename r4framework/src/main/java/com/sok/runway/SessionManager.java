package com.sok.runway;
import java.util.*;
//import java.util.logging.FileHandler;
//import java.util.logging.Level;
//import java.util.logging.Logger;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.*;
import com.sok.framework.*;
public class SessionManager implements HttpSessionListener, ServletContextListener
{
	
	private static final Logger logger = LoggerFactory.getLogger(SessionManager.class);
	
	private int totalSessionCount = 0;
	private int maxSessionCount = 0;
	protected ServletContext context = null;
	protected ArrayList sessions = new ArrayList(); 
	
	StringBuffer errors = null;
	
	public static final String sessionManager = "sessionManager";

   //protected static Logger logger = null;	
	
   public SessionManager()
   {
      //initLogger();
   }
/*   
   public static void logError(String message)
   {
       log(Level.SEVERE, message);
   }   
   
   public static void log(Level type, String message)
   {
       StringBuffer msg = new StringBuffer();
       msg.append(message);
       msg.append("\r\n");

       //logger.log(type, msg.toString());
   }   
*/   
	public int getSessionSize()
	{
		return(sessions.size());
	}
/*	
   protected synchronized void initLogger()
   {
       if(logger==null)
       {
           try{
               String pattern = "sessionmanager%g.log";
               int limit = 50000000; // 50 Mb
               int numLogFiles = 2;
               FileHandler fh = new FileHandler(pattern, limit, numLogFiles);
               logger = Logger.getLogger("com.sok.runway.SessionManager");
               logger.addHandler(fh);
           }catch(Exception e){
               
           }
       }
   }	
*/
	public String getError()
	{
		if(errors!=null)
		{
			return(errors.toString());
		}
		return("");
	}

	protected void appendError(String es)
	{
		if(errors == null)
		{
			errors = new StringBuffer();
		}
		errors.append(es);
	}

	public UserBean getUser(int index)
	{
		try
		{
			if(sessions.get(index)!=null && sessions.get(index) instanceof UserSession)
			{
				UserSession hs = (UserSession)sessions.get(index);
				return(hs.user);
			}
			else
			{
				appendError(String.valueOf(index));
				appendError(" user session is null\r\n");
			}
		}
		catch(Exception e)
		{
			appendError(String.valueOf(index));
			appendError(ActionBean.writeStackTraceToString(e));
		}
		return(null);
	}

	public synchronized void setUser(HttpSession session, UserBean user)
	{
		removeDuplicateUsers(session, user);
		addUserSession(session, user);
	}
	
	protected void addUserSession(HttpSession session, UserBean user)
	{
		UserSession usersession = new UserSession();
		usersession.session = session;
		usersession.user = user;
		sessions.add(usersession);

		if(sessions.size() > maxSessionCount)
		{
			maxSessionCount = sessions.size();	
		}
	}
	
	protected void removeDuplicateUsers(HttpSession session, UserBean user) {
	   UserSession us = null;
	   ArrayList removed = new ArrayList();
	   
	   for(int i=0; i<sessions.size(); i++) {
	      us = (UserSession)sessions.get(i);
	      
	      //Precautionary checks
	      if (us != null && us.user != null && us.session != null) {
	         //The real checking done here
	         if (us.user.getCurrentUserID().equals(user.getCurrentUserID())) {
   	         sessions.remove(us);
	            if (!session.equals(us.session)) {
   	            try { 
   	               us.user.killExchange();
   	               removed.add(us.session);
   	               LockManager.unlockAll(us.user);
   	            } 
   	            catch (Exception e) {}
	            }
	         }
	         else if (session.equals(us.session)) {
   	         sessions.remove(us);
	         }
	      }
	   }
	   HttpSession oldSession = null;
	   //Invalidate all sessions at the end so we don't screw up array iteration
	   //updated 20070808 add try/catch, no check exists for session already invalidated.. 
	   for(int i=0; i<removed.size(); i++) {
	      oldSession = (HttpSession)removed.get(i);
	      try { 
	    	  oldSession.invalidate();
	      } catch (IllegalStateException e) {}
	   }
	}
	
	/*public synchronized void removeDuplicateUser(String userid)
	{
		UserSession us = null;
		UserSession lastus = null;
		
		for(int i=0; i<sessions.size(); i++)
		{
			us = (UserSession)sessions.get(i);
			if(us!=null)
			{
				try{
						if(us.user!=null)
						{
							if(userid.equals(us.user.getCurrentUserID()))
							{
								if(lastus!=null)
								{
									LockManager.unlockAll(us.user);
									try{
										lastus.session.invalidate();
									}catch(Exception e){sessions.remove(lastus);}
								}
								lastus = us;
							}
						}

				}catch(Exception e){}
			}
		}
	}*/

	public HttpSession getSession(int index)
	{
		UserSession us = (UserSession)sessions.get(index);
		return(us.session);
	}
	
	public UserBean getUserBeanBySessionID(String sessionid)
	{
	   UserSession us = null;
		for(int i=0; i<sessions.size(); i++)
		{
		   us = (UserSession)sessions.get(i);
			if(sessionid.equals(us.session.getId()))
			{
				return(us.user);
			}
		}
		return(null);
	}
	
	public UserBean getUserBean(int index)
	{
		UserSession us = (UserSession)sessions.get(index);
		return(us.user);
	}

	public synchronized void removeUser(String userid)
	{
		UserSession us = null;

		for(int i=0; i<sessions.size(); i++)
		{
			us = (UserSession)sessions.get(i);
			if(us!=null)
			{	
				try{

				if(us.user!=null)
				{
					if(userid.equals(us.user.getCurrentUserID()))
					{
						LockManager.unlockAll(us.user);
						us.session.invalidate();
					}
				}
				}catch(Exception e){}
			}
		}
	}

	@Override
	public void sessionCreated(HttpSessionEvent event)
	{
		if(logger.isDebugEnabled()) logger.debug("{} sessionCreated({})", this.getClass().getName(), event.toString());
		totalSessionCount++;
		/*
		if(context == null)
		{
			storeInServletContext(event.getSession());
		}
		*/
	}
	
	@Override
	public void sessionDestroyed(HttpSessionEvent event)
	{
		HttpSession hs = event.getSession();
		removeSession(hs);
	}
	
	protected synchronized void removeSession(HttpSession hs)
	{
		UserSession us = null;
		for(int i=0; i<sessions.size(); i++)
		{
			us = (UserSession)sessions.get(i);
			if(us!=null && us.session == hs)
			{	
				us.user.cleanUp();
				sessions.remove(us);
			}
		}
	}
	
	public int getTotalSessionCount()
	{
		return(totalSessionCount);	
	}
	
	public int getCurrentSessionCount()
	{
		return(sessions.size());
	}
	
	public int getMaxSessionCount()
	{
		return(maxSessionCount);
	}
	
	protected synchronized void storeInServletContext(HttpSession session)
	{
		logger.warn("storeInServletContext(session) no longer in use");
		/*
		if(context == null)
		{
			context = session.getServletContext();
			context.setAttribute(sessionManager,this);
		}
		*/
	}

	protected class UserSession
	{
		public HttpSession session;
		public UserBean user;
		
		public UserSession(){}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sctxe) {
		logger.info("{} context destroyed", this.getClass().getName());
		sctxe.getServletContext().removeAttribute(sessionManager);
	}

	@Override
	public void contextInitialized(ServletContextEvent sctxe) {
		logger.info("{} context Initialized", this.getClass().getName());
		//set up connections to external resources, if used?		
		context = sctxe.getServletContext();
		context.setAttribute(sessionManager,this);
	}
}
