package com.sok.runway;

import java.sql.Connection;
import java.text.DateFormat;
import java.util.TimeZone;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.sok.runway.OfflineProcess; 

import com.sok.framework.ActionBean; 
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean; 
import com.sok.framework.SpecManager;
import com.sok.framework.ViewSpec;

public class OfflineLinker extends OfflineProcess {
	
	int numRecords = 0; 
	int numErrors = 0; 
	int numProcessed = 0; 
	int numExisted = 0; 
	
	Connection connection;
	StringBuffer status = new StringBuffer(); 
	StringBuffer importLog = new StringBuffer(); 
	String linkID; 
	String linkIDName; 
	String[] linkPrimaryKeyValues; 
	String linkPrimaryKey; 
	GenRow check;
	GenRow action; 
	UserBean user; 
	String notificationEmail = "info@switched-on.com.au"; 
	
	ViewSpec vspec; 
	ViewSpec insertSpec; 

	public OfflineLinker(HttpServletRequest req, ServletContext ctx) {
		super(req, ctx);
		initProcess(req,ctx); 
	}

	public OfflineLinker(String str, HttpServletRequest req, ServletContext ctx) {
		super(str, req, ctx);
		initProcess(req,ctx); 
	}

	public OfflineLinker(com.sok.framework.RowBean rb, HttpServletRequest req, ServletContext ctx) {
		super(rb, req, ctx);
		initProcess(req,ctx); 
	}
	
	private void initProcess(HttpServletRequest req, ServletContext ctx) { 
		
		this.linkID = req.getParameter("-linkID");
		this.linkIDName = req.getParameter("-linkIDName");

		this.vspec = SpecManager.getViewSpec(req.getParameter("-viewSpec"));
		this.linkPrimaryKey = SpecManager.getTableSpec(vspec.getMainTableName()).getPrimaryKeyName(); 
		this.linkPrimaryKeyValues = (String[])req.getParameterValues(this.linkPrimaryKey);
		
		this.insertSpec = SpecManager.getViewSpec(req.getParameter("-insertSpec")); 
		
		this.user = (UserBean)req.getSession().getAttribute("currentuser");
		 
		
		setEmail(user.getEmail());
		start(); 
	}

	public Connection getConnection() {
		try {
			if (connection == null || connection.isClosed()) {
				connection.isClosed();
			}
		} catch(java.sql.SQLException e) { errors.append(ActionBean.writeStackTraceToString(e)); }
		return connection;
	}

	public void execute() {
		try {
      		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, getLocale());
      		
      		this.status.append(context.getServletContextName());
      		this.status.append(" Runway offline process - Link Records \r\n");
      		this.status.append("Initiated ");
      		this.status.append(dt.format(new java.util.Date()));
      		this.status.append("\r\n");
      		
   			connection = getConnection();
   			
   			action = new GenRow(); 
   			action.setViewSpec(insertSpec); 
   			action.setConnection(connection); 
   			action.setAction(GenerationKeys.INSERT); 
			action.setParameter("CreatedDate","NOW");
			action.setParameter("ModifiedDate","NOW");
			action.setParameter("CreatedBy",user.getUserID());
			action.setParameter("ModifiedBy",user.getUserID());
   			
   			check = new GenRow(); 
   			check.setViewSpec(insertSpec); 
   			check.setConnection(connection); 
   			
   			for(String pkValue : linkPrimaryKeyValues) { 
   				numRecords++; 
   				
   				check.clear(); 
   				check.setAction(GenerationKeys.SELECTFIRST); 
   				check.setParameter(linkIDName,linkID); 
   				check.setParameter(linkPrimaryKey, pkValue); 
   				check.doAction(); 
   				
   				if(!check.isSuccessful()) { 
   					
   					check.clear(); 
   					check.setParameter("-select1","count(*) as c"); 
   					check.setParameter(linkPrimaryKey, pkValue); 
   					check.setAction(GenerationKeys.SELECTFIRST); 
   					
   					action.createNewID(); 
   					action.setParameter(linkIDName,linkID); 
   					action.setParameter(linkPrimaryKey, pkValue);
   					action.setParameter("SortOrder",check.getInt("c")+1);
   					action.doAction(); 
   					
   					if(action.getError().length()>0) { 
   						numErrors++;
   						errors.append("Error linking ").append(linkPrimaryKey).append(" ").append(pkValue).append(" \r\n").append(action.getError()).append("\r\n"); 
   					}else { 
   						numProcessed++; 
   					}
   				} else { 
   					numExisted++; 
   					errors.append("Error linking ").append(linkPrimaryKey).append(" ").append(pkValue).append(" - Already Linked \r\n");
   				}
   			}
   			   			
      		status.append("Total Records : ").append(numRecords).append("\r\n"); 
      		status.append("Records processed : ").append(numProcessed).append("\r\n");
      		status.append("Records skipped as link records existed : ").append(numExisted).append("\r\n");
      		status.append("Errors : ").append(numErrors).append("\r\n");
      		
      		//status.append("\r\n\r\n Import Log: \r\n" ); 
      		//status.append(importLog);
      		
      		if(errors.length()>0) { 
	      		System.out.println("\r\n\r\n Link Errors: ");
	      		System.out.print(errors.toString()); 
      		} 
      		
      		connection.close();
   			
   		} catch (Exception e) { 
   			status.append(ActionBean.writeStackTraceToString(e)); 
   		}
	}
	
	public void setEmail(String email)  {
		if (email!= null && email.length()>0) { 
			notificationEmail = email;
		}
	}
	
	public void startProcess() { 
		super.start(); 
	}
	   
	public String getStatusMailBody() {
		return status.toString();
	}

	public String getStatusMailSubject() {
		return  "Runway Offline Link Creation";
	}

	public String getStatusMailRecipient() {
		return notificationEmail;
	}

	public int getProcessSize() {
		return numRecords;
	}

	public int getProcessedCount() {
		return numProcessed;
	}

	public int getErrorCount() {
		return numErrors;
	}
}
