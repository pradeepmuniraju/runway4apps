package com.sok.runway;

import com.sok.framework.*;
import java.util.*;

public class FieldLabeler {
   
   public static final String CONTACT_ADDRESS = "CONTACT_ADDRESS";
   public static final String CONTACT_HOME_ADDRESS = "CONTACT_HOME_ADDRESS";
      
   static Hashtable contactLabelMappings = null;
   
   public static String getContactLabelForField(String field) {
      if (contactLabelMappings == null) {
         rebuildLabels();
      }
      
      String label = (String)contactLabelMappings.get(field);
      
      if (label == null) {
         label = field;
      }
      return label;
   }
   
   public static void rebuildLabels() {
      Hashtable tempMapping = new Hashtable();
      
      String addressLabel = InitServlet.getSystemParams().getProperty("ContactAddressLabel");
      String homeAddressLabel = InitServlet.getSystemParams().getProperty("ContactHomeAddressLabel");
      
      String prefix = "";
      String homePrefix = "Postal";
      
      if (addressLabel == null || addressLabel.length() == 0) {
         addressLabel = "Physical Address";
      }
      else {
         int i = addressLabel.indexOf(" ");
         if (i > -1) {
            prefix = addressLabel.substring(0,i);
         }
      }
      tempMapping.put(CONTACT_ADDRESS, addressLabel);
      tempMapping.put("Street", prefix + "Street");
      tempMapping.put("Street2", prefix + "Street2");
      tempMapping.put("City", prefix + "City");
      tempMapping.put("State", prefix + "State");
      tempMapping.put("Postcode", prefix + "Postcode");
      tempMapping.put("Country", prefix + "Country");
      
      
      if (homeAddressLabel == null || homeAddressLabel.length() == 0) {
         homeAddressLabel = "Postal Address";
      }
      else {
         int i = homeAddressLabel.indexOf(" ");
         if (i > -1) {
            homePrefix = homeAddressLabel.substring(0,i);
         }
      }
      tempMapping.put(CONTACT_HOME_ADDRESS, homeAddressLabel);
      tempMapping.put("HomeStreet", homePrefix + "Street");
      tempMapping.put("HomeStreet2", homePrefix + "Street2");
      tempMapping.put("HomeCity", homePrefix + "City");
      tempMapping.put("HomeState", homePrefix + "State");
      tempMapping.put("HomePostcode", homePrefix + "Postcode");
      tempMapping.put("HomeCountry", homePrefix + "Country");

      contactLabelMappings = tempMapping;
   }
}