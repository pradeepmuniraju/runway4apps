package com.sok.runway;
import javax.servlet.*;	  
import javax.servlet.http.*;
import java.util.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import java.text.*;
import com.sok.runway.crm.ContactProductGroup;
import com.sok.framework.*;
import com.sok.framework.sql.*;
import com.sok.framework.generation.GenerationKeys;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OfflineUpdate extends OfflineProcess
{
	private static final Logger logger = LoggerFactory.getLogger(OfflineUpdate.class);
	public static final String DEBUG = "-debug";
	public static final String CHECKCOUNT = "-checkcount";
	public static final String BEANNAME = "-beanname";
	public static final String VIEW = "-view";
	StringBuffer status = new StringBuffer();
	StringBuffer errors = new StringBuffer();

	UserBean user = null;
	TableData search = null;
	String campaign = null;
	String template = null;
	boolean debug = false;
	ArrayList foundset = new ArrayList(500);
	HashMap questionAnswers = null; 
	AnswerBean answerBean = null; 
	GenRow productBean = null; 

	Connection con = null;
	Context ctx = null;
	PreparedStatement updatestm = null;
	PreparedStatement statusstm = null;
	String statementstring = null;
	int total = 0;
	int updated = 0;
	int failed = 0;

	int checkcount = -1;


	String tablename = null;
	String keyname = null;
	String viewname = null;

	TableSpec tspec;
	ViewSpec vspec;

	static final String _blank = "";
	static final String _dot = ".";
	static final String _comma = ", ";
	static final String _and = " and ";
	static final String _or = " or ";
	static final String _as = " as ";
	static final String _omit = "omit";

	static final String NOW = "NOW";
	static final String NULL= "NULL";
	static final String EMPTY = "EMPTY";

	String contactStatusStatementString; 
	String companyStatusStatementString;
	String opportunityStatusStatementString;
	String orderStatusStatementString;
	String productStatusStatementString;
	double GSTValue; 

    boolean insertProductGroupStatus = false;
	boolean insertcontactstatus = false;
	boolean insertcompanystatus = false;
	boolean insertopportunitystatus = false;
	boolean insertorderstatus = false;
	boolean insertproductstatus = false; 
	boolean recalculateProductGST = false;
   boolean updaterelatedgroup = false;
   boolean insertgroups = false;
   
	GenRow requestrow = new GenRow();
	
	 public OfflineUpdate(HttpServletRequest request, ServletContext context, boolean autostart)
	 {
		  super(request, context);
		  logger.debug("new OfflineUpdate(request, context, autostart=[{}]);", autostart);
		  if(autostart)
		  {
				initProcess(request, context);
				start();
		  }
	 }	 
	 
	public OfflineUpdate(HttpServletRequest request, ServletContext context)
	{
		super(request, context);
		logger.debug("new OfflineUpdate(request, context);");
		initProcess(request, context);
		start();
	}

	public OfflineUpdate(String query, HttpServletRequest request, ServletContext context)
	{
		super(query, request, context);
		logger.debug("new OfflineUpdate(query=[{}], request, context);", query);
		initProcess(request, context);
		start();
	}

	public OfflineUpdate(RowBean bean, HttpServletRequest request, ServletContext context)
	{
		super(bean, request, context);
		logger.debug("new OfflineUpdate(bean, request, context);");
		initProcess(request, context);
		start();
	}	

	public void initProcess(HttpServletRequest request, ServletContext context)
	{
		HttpSession session = request.getSession();
		user = (UserBean)session.getAttribute(SessionKeys.CURRENT_USER);
		String searchcriteria = (String)requestbean.get(BEANNAME);
		search = (TableData)session.getAttribute(searchcriteria);
		requestrow.parseRequest(request);
		
		vspec = search.getViewSpec();
		tspec = search.getTableSpec();
		tablename = getSourceTableName();
		keyname = tspec.getPrimaryKeyName();
		
		// if it has a group by, then it may not be in the limited view, so don't use that
		if ((search.getParameter("-sort0") == null || search.getParameter("-sort0").length() == 0) && (search.getParameter("-sort1") == null || search.getParameter("-sort1").length() == 0)) {
			if (tablename.equals("Contacts")) {
			   search.setViewSpec("OfflineContactView");
			}
			else if (tablename.equals("Companies")) {
			   search.setViewSpec("OfflineCompanyView");
			}
			/*else if (tablename.equals("Products")) { 
			   search.setViewSpec("OfflineProductView");
			}*/ 
			//Hack - we'll get a fix for this later
			
			else if (tablename.equals("Orders")) { 
			   search.setViewSpec("OfflineOrderView");
			}
		}
		
		search.generateSQLStatement();
		if(logger.isTraceEnabled()) logger.trace(search.getSearchStatement());
		setStatements();
		
	}

	void setStatements()
	{
		SqlDatabase sql = ActionBean.getDatabase();
		StringBuffer consb = new StringBuffer();
		consb.append("insert into ContactStatus (ContactStatusID, StatusID, CreatedDate, CreatedBy, ContactID) values ( ?, ?, ");
		consb.append(sql.getCurrentDateMethod());
		consb.append(", ? , ?)");
		contactStatusStatementString = consb.toString();

		StringBuffer comsb = new StringBuffer();
		comsb.append("insert into CompanyStatus (CompanyStatusID, StatusID, CreatedDate, CreatedBy, CompanyID) values ( ?, ?, ");
		comsb.append(sql.getCurrentDateMethod());
		comsb.append(", ? , ?)");
		companyStatusStatementString = comsb.toString();
		
		StringBuffer oppsb = new StringBuffer();
		oppsb.append("insert into OpportunityStatus (OpportunityStatusID, StatusID, CreatedDate, CreatedBy, OpportunityID) values ( ?, ?, ");
		oppsb.append(sql.getCurrentDateMethod());
		oppsb.append(", ? , ?)");
		opportunityStatusStatementString = oppsb.toString();		

		StringBuffer ordsb = new StringBuffer();
		ordsb.append("insert into OrderStatus (OrderStatusID, StatusID, CreatedDate, CreatedBy, OrderID) values ( ?, ?, ");
		ordsb.append(sql.getCurrentDateMethod());
		ordsb.append(", ? , ?)");
		orderStatusStatementString = ordsb.toString();
		
		StringBuffer prdsb = new StringBuffer();
		prdsb.append("insert into ProductStatus (ProductStatusID, StatusID, CreatedDate, CreatedBy, ProductID) values ( ?, ?, ");
		prdsb.append(sql.getCurrentDateMethod());
		prdsb.append(", ? , ?)");
		productStatusStatementString = prdsb.toString();
	}
	
	protected void processExtraFields(TableData current) throws SQLException {
		//This method exists for subclasses to do extra processing
	}
	
	void retrieveRecords()throws Exception
	{
		if(debug)
		{
			errors.append(search.getSearchStatement());
		}

		try
		{
			search.setConnection(con);
      	RowSetWrapperBean list = new RowSetWrapperBean();
      	list.setConnection(con);
      	list.setSearchBean(search);
      	list.getResults();
      	
      	TableData resultset = list.getResultBean();
      	
			foundset = new ArrayList(500);
			while (list.getNext()) {
				foundset.add(resultset.getString(keyname));
				processExtraFields(resultset);
			}
			total = foundset.size();
			
      	// Reset viewspec back to previous
      	if (vspec != null) {
   		   search.setViewSpec(vspec);
      	   search.generateSQLStatement();
         }
		} catch (Exception e) {
		}
		finally
		{
			try{
				con.close();
	  		}catch (Exception ex){}
		}
	}

	void getStatementString()
	{
		StringBuffer statement = new StringBuffer();
		statement.append("update ");
		statement.append(getTargetTableName());
		statement.append(" set ");
		String tablename = getTargetTableName();
		int n = 0;
		for(int i=0; i< tspec.getFieldsLength(); i++)
		{
			String fieldname = tspec.getFieldName(i);
			String value = (String)requestbean.get(fieldname);
			if(insertcontactstatus && fieldname.equals("ContactStatusID"))
			{
				if(n!=0)
				{
					statement.append(_comma);
				}
				statement.append(tablename);
				statement.append(_dot);
				statement.append("ContactStatusID");
				statement.append(" = ?");
			}
			else if(insertcompanystatus && fieldname.equals("CompanyStatusID"))
			{
				if(n!=0)
				{
					statement.append(_comma);
				}
				statement.append(tablename);
				statement.append(_dot);
				statement.append("CompanyStatusID");
				statement.append(" = ?");
			}		
			else if(insertopportunitystatus && fieldname.equals("OpportunityStatusID"))
			{
				if(n!=0)
				{
					statement.append(_comma);
				}
				statement.append(tablename);
				statement.append(_dot);
				statement.append("OpportunityStatusID");
				statement.append(" = ?");
			}
			else if(insertorderstatus && fieldname.equals("OrderStatusID"))
			{
				if(n!=0)
				{
					statement.append(_comma);
				}
				statement.append(tablename);
				statement.append(_dot);
				statement.append("OrderStatusID");
				statement.append(" = ?");
			}
			else if(insertproductstatus && fieldname.equals("ProductStatusID"))
			{
				if(n!=0)
				{
					statement.append(_comma);
				}
				statement.append(tablename);
				statement.append(_dot);
				statement.append("ProductStatusID");
				statement.append(" = ?");
			}
			else if(requestbean.containsKey(fieldname) && value.length() > 0)
			{
				if(n!=0)
				{
					statement.append(_comma);
				}
				statement.append(tablename);
				statement.append(_dot);
				statement.append(fieldname);
				statement.append(" = ?");
				n++;
			}
			
		}



		statement.append(" where ");
		statement.append(tablename);
		statement.append(_dot);
		statement.append(keyname);
		statement.append(" = ?");
		statementstring = statement.toString();
		if(debug)
		{
			errors.append("\r\n");
			errors.append(statementstring);
			errors.append("\r\n");
		}
	}

	void setParameters(int index)throws Exception
	{
		int n = 0;
		String fieldname = null;
		String value = null;
		String recordid = (String)foundset.get(index);
		for(int j=0; j<tspec.getFieldsLength(); j++)
		{
			fieldname = tspec.getFieldName(j);
			int fieldtype = tspec.getFieldType(j);
			if(requestbean.containsKey(fieldname))
			{
				value = (String)requestbean.get(fieldname);
				if (value!=null && value.length()>0)
				{
					n++;
						if(value.equals(ActionBean.NULL))
						{
							updatestm.setNull(n,fieldtype);
						}	
						else if(fieldtype==Types.TIMESTAMP)
						{
						Timestamp ts = new Timestamp(getDate(j,value).getTime());
						updatestm.setTimestamp(n, ts);
						}	
						else if(value.equals(ActionBean.EMPTY))
						{
							updatestm.setString(n, ActionBean._blank);
						}	
						else
						{
							updatestm.setString(n, value);
						}
				}
			}
		}

		n++;
		updatestm.setString(n, recordid);
	}

	protected int updateRow(int index)
	{
		int updated = 0;
		String id = (String)foundset.get(index);
		try
		{
			getPreparedStatement();
			setParameters(index);
			if(debug)
			{
				updated = 1;
			}
			else
			{
				updated = updatestm.executeUpdate();
			}
		}
		catch (Exception ex)
		{
		   //ex.printStackTrace();
			setLastError(ex.toString());
				errors.append(ex.toString());
				errors.append(" - ");
				errors.append((String)context.getInitParameter("URLHome"));
				errors.append("/crm/");
				errors.append(viewname);
				errors.append(".jsp?-action=select&");
				errors.append(keyname);
				errors.append("=");
				errors.append(id);
				errors.append("\r\n");
		}
		return(updated);
					
	} 

	protected java.util.Date getDate(int specindex , String value)
	{
		if(value!=null && !value.equals(NOW))
		{
			SimpleDateFormat sdf = new SimpleDateFormat(tspec.getFormat(specindex), getLocale());
			try{
				return(sdf.parse(value));
			}catch(Exception e){}			
		}
		return(new java.util.Date());
	}
	
	void getConnection() throws Exception
	{
		if(con==null || con.isClosed())
		{
			ctx = new InitialContext();

			DataSource ds = null;
				try{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource)envContext.lookup(dbconn);
				}catch(Exception e){}
			if(ds == null)
			{
				ds = (DataSource)ctx.lookup(dbconn);
			}
			con = ds.getConnection();
		}
	}

	void getPreparedStatement() throws Exception
	{
		if(con==null || con.isClosed())
		{
			getConnection();
			updatestm = con.prepareStatement(statementstring);
		}
		if(updatestm==null)
		{
			updatestm = con.prepareStatement(statementstring);
		}
	}

	void getStatusPreparedStatement(String statusStatementString) throws Exception
	{
		if(con==null || con.isClosed())
		{
			getConnection();
			statusstm = con.prepareStatement(statusStatementString);
		}
		if(updatestm==null)
		{
			statusstm = con.prepareStatement(statusStatementString);
		}
	}

   void setRelatedGroup(String table, String onkey, String onvalue, String groupid)
   {
      GenRow row = new GenRow();
      row.setConnection(con);
      row.setTableSpec(table);
      row.setParameter("GroupID", groupid);
      row.setParameter(onkey, onvalue);
      row.doAction("updateall");    
   }  

   void setGroup(String table, String pkeyname, String pkeyvalue, String groupid)
   {
      GenRow row = new GenRow();
      row.setConnection(con);
      row.setTableSpec(table);
      row.setParameter("GroupID", groupid);
      row.setParameter(pkeyname, pkeyvalue);
      row.doAction("update");    
   }	
   
   String getContactCompanyID(String contactid)
   {
      GenRow row = new GenRow();
      row.setConnection(con);
      row.setTableSpec("Contacts");
      row.setParameter("-select1", "CompanyID");
      row.setParameter("ContactID", contactid);
      row.doAction("select");   
      return(row.getString("CompanyID"));
   }
   
   void updateRelatedGroup(String id)
   {
      try
      {
         getConnection();
         if(tablename.equals("Contacts"))
         {
            updateRelatedGroup(id, getContactCompanyID(id), requestrow.getString("GroupID"));
         }
         else if(tablename.equals("Companies"))
         {     
            updateRelatedGroup(null, id, requestrow.getString("GroupID"));
         }
      }
      catch(Exception e)
      {
         errors.append("Error while updating group in related records");
         errors.append(ActionBean.writeStackTraceToString(e));
      }
   }
   
   void updateRelatedGroup(String contactid, String companyid, String groupid)
   {
      if(companyid!=null && companyid.length()!=0) //company or contact with company
      {
         setRelatedGroup("Notes", "ON-CompanyID", companyid, groupid);
         
         GenRow contacts = new GenRow();
         contacts.setTableSpec("Contacts");
         contacts.setConnection(con);
         contacts.setParameter("-select1", "ContactID");
         contacts.setParameter("CompanyID", companyid);
         contacts.doAction("search");
         contacts.getResults();
         while(contacts.getNext())
         {
            setGroup("Contacts", "ContactID", contacts.getString("ContactID"), groupid);
            setRelatedGroup("Notes", "ON-ContactID", contacts.getString("ContactID"), groupid);
         }
         contacts.close();
      }
      else if(contactid !=null && contactid.length()!=0)  //contact without company
      {     
         setRelatedGroup("Notes", "ON-ContactID", contactid, groupid);
      }      
   }
	
   void insertStatus(String stmtname, String pkeyname, String fkeyvalue) throws Exception
   {
      getStatusPreparedStatement(productStatusStatementString);
      String relatedstatusid = KeyMaker.generate();
      String statusid = requestbean.getString("StatusID");
      String modifiedby = requestbean.getString("ModifiedBy");

      statusstm.setString(1, relatedstatusid);
      statusstm.setString(2, statusid);
      statusstm.setString(3, modifiedby);
      statusstm.setString(4, fkeyvalue);
      statusstm.executeUpdate();

      requestbean.put(pkeyname, relatedstatusid);
   }	
	
	void insertContactStatus(String contactid) throws Exception
	{
	   insertStatus(contactStatusStatementString, "ContactStatusID", contactid);
	}
	
	void insertCompanyStatus(String companyid) throws Exception
	{
	   insertStatus(companyStatusStatementString, "CompanyStatusID", companyid);
	}
	
	void insertOpportunityStatus(String opportunityid) throws Exception
	{
      insertStatus(opportunityStatusStatementString, "OpportunityStatusID", opportunityid);	   
	}
	
	void insertOrderStatus(String orderid) throws Exception
	{
      insertStatus(orderStatusStatementString, "OrderStatusID", orderid);     	   
	}
	
	void insertProductStatus(String productid) throws Exception
	{
      insertStatus(productStatusStatementString, "ProductStatusID", productid);	   
	}
	
	

	/**
	 * This will recalculate the products GST and TotalCost values where a bulk 
	 * change has been requested to the GSTExempt functionality. 
	 * 
	 * This really only needs to run in situations where the Cost, GST, TotalCost 
	 * have not been specified, but it runs anyway in this case for all products
	 * where a value for the GSTExempt has been recorded. 
	 * @param productid
	 * @throws Exception
	 */
	void recalculateProductGST(String productid) throws Exception 
	{ 
		if(productBean == null) { 
			productBean = new GenRow();
			productBean.setConnection(con); 
			productBean.setViewSpec("ProductListView"); 
		} else { 
			productBean.clear(); 
		}
		productBean.setParameter("ProductID",productid);
		productBean.doAction(GenerationKeys.SELECT);
		
		double cost = productBean.getDouble("Cost");
		double gst = 0; 
		double totalcost = 0; 
		
		gst = cost * GSTValue; 
		totalcost = cost + gst; 
		
		productBean.setParameter("Cost",cost+""); 
		productBean.setParameter("GST",gst+""); 
		productBean.setParameter("TotalCost",totalcost+""); 
		productBean.doAction(GenerationKeys.UPDATE); 
	}
	/**
	 * Creates a hashmap of all of the questionid's and their new answers. 
	 * @return true if hashmap created (answers specified) else false. 
	 */
	private boolean setProfileFields() { 
		
		/*
		 * Creates a hashmap of the questions where answers have been specified.
		 */
		
		String[] questionIDs = requestbean.getString("QuestionID").split("\\+");  
		boolean idMode = requestbean.getString("Answer"+questionIDs[0]).length()>0;
		String answer = ActionBean._blank;
		questionAnswers = new HashMap();
		
		if(idMode) { 
			for (int i=0; i<questionIDs.length; i++) { 
				answer = requestbean.getString("Answer"+questionIDs[i]); 
				if (answer.length()>0) { 
					questionAnswers.put(questionIDs[i],answer);
				} 
			}
		} else { 
			for (int i=0; i<questionIDs.length; i++) { 
				answer = requestbean.getString("Answer"+i); 
				if (answer.length()>0) { 
					questionAnswers.put(questionIDs[i],answer);
				} 
			}
		}
		if(!questionAnswers.isEmpty()) { 
			/*
			 * If answers were specified, initialise the answerBean and return true
			 */
			answerBean = new AnswerBean(); 
			//answerBean knows which answer spec to use
			//answerBean.setViewSpec("answers/ XXXX AnswerView"); 
			try { 
				if (con == null || con.isClosed()) { 
					getConnection(); 
				}
			} catch (Exception e) { errors.append("error getting connection for answerbean\r\n"); } 
			
			answerBean.setConnection(con);
			answerBean.setCurrentUser(user); 
			return true; 
		}else { 
			
			/*
			 * If no answered were specified, and the hashmap is empty, return false
			 */
			questionAnswers = null; 
			
			return false; 
		}
		
	}
	/**
	 * Updates profiles for a given recordNum, work is delegated to AnswerBean. 
	 * @param recordNum
	 */
	private void updateProfiles(int recordNum) { 
		
		String id = (String)foundset.get(recordNum);
		Iterator it = questionAnswers.keySet().iterator(); 
		
		answerBean.setConnection(con);
			
		if (tablename.equals("Contacts")) {
			while (it.hasNext()) { 
				String questionID = (String)it.next();
				String answer = (String)questionAnswers.get(questionID); 
				answerBean.saveContactAnswer(id,questionID,answer);
				
				if(answerBean.getError().length()>0) { 
					errors.append("Error inserting answer \r\n"+answerBean.getError()); 
				}
			}
		}
		else if (tablename.equals("Companies")) {
			while (it.hasNext()) { 
				String questionID = (String)it.next();
				String answer = (String)questionAnswers.get(questionID); 
				answerBean.saveCompanyAnswer(id,questionID,answer);
				
				if(answerBean.getError().length()>0) { 
					errors.append("Error inserting answer \r\n"+answerBean.getError()); 
				}
			}
		}
		else if (tablename.equals("Products")) { 
			while (it.hasNext()) { 
				String questionID = (String)it.next();
				String answer = (String)questionAnswers.get(questionID); 
				answerBean.saveProductAnswer(id, questionID, null,new String[] {answer}, 0); 
				
				if(answerBean.getError().length()>0) { 
					errors.append("Error inserting answer \r\n"+answerBean.getError()); 
				}
			} 
		}
		else if (tablename.equals("Orders")) { 
			while (it.hasNext()) { 
				String questionID = (String)it.next();
				String answer = (String)questionAnswers.get(questionID); 
				answerBean.saveOrderAnswer(id, questionID, null,new String[] {answer}, 0); 
				
				if(answerBean.getError().length()>0) { 
					errors.append("Error inserting answer \r\n"+answerBean.getError()); 
				}
			} 
		}/* REMOVED AS FUNCTIONALITY NOT REQUIRED
		else if (tablename.equals("Opportunities")) {
			while (it.hasNext()) { 
				String questionID = (String)it.next();
				String answer = (String)questionAnswers.get(questionID); 
				answerBean.saveOpportunityAnswer(id, questionID, null,new String[] {answer}, 0); 
			} 
		}*/
	}
        
   void updateProductGroupStatus(String id) {
      try{
         if (tablename.equals("Contacts")) {
            String contactID = id;
   
            String[] productGroupIDList = requestrow.getParameterValues("ProductGroupID");
            String[] productGroupStatusList = requestrow.getParameterValues("ProductGroupStatus");    
   
            ContactProductGroup cpg = new ContactProductGroup(con);
            cpg.setCurrentUser(user.getString("UserID"));
            
            for(int i = 0; i < productGroupStatusList.length; i++) {            
               if(productGroupStatusList[i]!=null && productGroupStatusList[i].length()!=0) {
                  cpg.load(contactID, productGroupIDList[i]);
                  cpg.setField("StatusID", productGroupStatusList[i]);  
                  if(cpg.getPrimaryKey().length()!=0){
                     cpg.update();  
                  }else{
                     cpg.insert();  
                  }
               }
            }      
         }
      }catch(Exception e){
         errors.append("Error inserting Product Group Status " + e.toString());
      }
   }
   
   GenRow getGroups(String id, String groupid)
   {
      GenRow row = new GenRow();
      row.setConnection(con);
      if (tablename.equals("Contacts")) {
         row.setViewSpec("ContactGroupView");     
      }else if (tablename.equals("Companies")){
         row.setViewSpec("CompanyGroupView");       
      }else if (tablename.equals("Products")){
         row.setViewSpec("ProductSecurityGroupView");        
      }

      row.setParameter("GroupID", groupid);
      row.setParameter(tspec.getPrimaryKeyName(), id);
      row.doAction("search");
      
      return(row);
   }
   
   void updateGroup(String id, String statusid, String repuserid)
   {
	  boolean hasInput = false; 
      GenRow row = new GenRow();
      row.setConnection(con);
      if (tablename.equals("Contacts")) {
         row.setTableSpec("ContactGroups");
         if(statusid!=null && statusid.length()!=0 && !"EMPTY".equals(statusid)) {
            row.setParameter("ContactGroupStatusID", insertGroupStatus(id, statusid));
            hasInput = true;
         } else if (StringUtils.isNotBlank(repuserid)) { 
        	hasInput = true;
         }         
      }else if (tablename.equals("Companies")){
         row.setTableSpec("CompanyGroups");
         if(statusid!=null && statusid.length()!=0 && !"EMPTY".equals(statusid)) {
            row.setParameter("CompanyGroupStatusID", insertGroupStatus(id, statusid));
            hasInput = true;
         } else if (StringUtils.isNotBlank(repuserid)) { 
        	 hasInput = true;
         }         
      }else if (tablename.equals("Products")){
         row.setTableSpec("ProductSecurityGroups");
         if(statusid!=null && statusid.length()!=0 && !"EMPTY".equals(statusid)) {
            row.setParameter("ProductSecurityGroupStatusID", insertGroupStatus(id, statusid));
            hasInput = true;
         } else if (StringUtils.isNotBlank(repuserid)) { 
        	 hasInput = true;
         }
      }
      /* this code does nothing as StatusID is not in ContactGroups
      if (statusid != null && statusid.length() > 0) {
    	  if ("EMPTY".equals(statusid)) statusid = "";
    	  row.setParameter("StatusID", statusid);
      }
      */
      if (repuserid != null && repuserid.length() > 0) {
    	  if ("EMPTY".equals(repuserid)) repuserid = "";
    	  row.setParameter("RepUserID", repuserid);
      }
      row.setParameter(row.getTableSpec().getPrimaryKeyName(), id);
      if (hasInput) row.doAction("update");   
   }   
   
   String insertGroupStatus(String groupkeyid, String statusid)
   {
      GenRow row = new GenRow();
      row.setConnection(con);
      row.setTableSpec("GroupStatus");
      if (tablename.equals("Contacts")) {
         row.setParameter("ContactGroupID", groupkeyid);
      }else if (tablename.equals("Companies")){
         row.setParameter("CompanyGroupID", groupkeyid);
      }else if (tablename.equals("Products")){
         row.setParameter("ProductGroupID", groupkeyid);
      }      
      row.setParameter("StatusID", statusid);
      row.setParameter("CreatedBy", user.getCurrentUserID());
      row.setParameter("CreatedDate", "NOW");
      row.createNewID();
      row.doAction("insert");        
      return(row.getString(row.getTableSpec().getPrimaryKeyName()));
   }
   
   void insertGroup(String id, String groupid, String statusid, String repuserid)
   {
      GenRow row = new GenRow();
      row.setConnection(con);
      
      if (tablename.equals("Contacts")) {
         row.setTableSpec("ContactGroups");
         row.createNewID();
         if(statusid!=null && statusid.length()!=0 && !"EMPTY".equals(statusid)) {
            String thisprimarykeyname = row.getTableSpec().getPrimaryKeyName(); 
            row.setParameter("ContactGroupStatusID", insertGroupStatus(row.getString(thisprimarykeyname), statusid));
         } else {
        	 GenRow group = new GenRow();
        	 group.setConnection(con);
        	 group.setViewSpec("ContactGroupView");
        	 group.setParameter("ContactID", id);
        	 group.sortBy("SortOrder", 0);
        	 group.doAction("selectfirst");
        	 
        	 if (StringUtils.isNotBlank(group.getString("CurrentStatusID"))) {
        		 statusid = group.getString("CurrentStatusID");
                 String thisprimarykeyname = row.getTableSpec().getPrimaryKeyName(); 
                 row.setParameter("ContactGroupStatusID", insertGroupStatus(row.getString(thisprimarykeyname), statusid));
        	 }
        	 if (StringUtils.isNotBlank(group.getString("RepUserID")) && ("".equals(repuserid) || "EMPTY".equals(repuserid))) repuserid = group.getString("RepUserID");
         }
      }else if (tablename.equals("Companies")){
         row.setTableSpec("CompanyGroups");
         row.createNewID();
         if(statusid!=null && statusid.length()!=0 && !"EMPTY".equals(statusid)) {
            String thisprimarykeyname = row.getTableSpec().getPrimaryKeyName(); 
            row.setParameter("CompanyGroupStatusID", insertGroupStatus(row.getString(thisprimarykeyname), statusid));
         }          
      }else if (tablename.equals("Products")){
         row.setTableSpec("ProductSecurityGroups");
         row.createNewID();
         if(statusid!=null && statusid.length()!=0 && !"EMPTY".equals(statusid)) {
            String thisprimarykeyname = row.getTableSpec().getPrimaryKeyName(); 
            row.setParameter("ProductSecurityGroupStatusID", insertGroupStatus(row.getString(thisprimarykeyname), statusid));
         }          
      }

      row.setParameter("GroupID", groupid);
      //if (!"".equals(statusid) && !"EMPTY".equals(statusid)) row.setParameter("StatusID", statusid);
      if (!"".equals(repuserid) && !"EMPTY".equals(repuserid)) {
    	  row.setParameter("RepUserID", repuserid);
      } else {
     	 GenRow group = new GenRow();
     	 group.setConnection(con);
     	 group.setViewSpec("ContactGroupView");
     	 group.setParameter("ContactID", id);
     	 group.sortBy("SortOrder", 0);
     	 group.doAction("selectfirst");
     	 
     	 if (StringUtils.isNotBlank(group.getString("RepUserID")) && ("".equals(repuserid) || "EMPTY".equals(repuserid))) row.setParameter("RepUserID", group.getString("RepUserID"));
      }
      row.setParameter(tspec.getPrimaryKeyName(), id);
      row.setParameter("CreatedBy", user.getUserID());
      row.setParameter("CreatedDate", "NOW");

      row.doAction("insert");   
   }

   void checkContactShortlist(String contactID, String linkedProductID, String groupID, String origRep, String newRep) {
	   if (linkedProductID == null || linkedProductID.length() == 0) return;
	   GenRow prod = new GenRow();
	   prod.setConnection(con);
	   prod.setTableSpec("Products");
	   prod.setParameter("ProductID", linkedProductID);
	   prod.setParameter("-select1","ProductID");
	   prod.setParameter("-select2","CopiedFromProductID");
	   prod.setParameter("-select3","ProductType");
	   prod.doAction(GenerationKeys.SELECT);

	   final String productID = "Home Plan".equals(prod.getData("ProductType")) && prod.isSet("CopiedFromProductID") ? prod.getData("CopiedFromProductID") : linkedProductID;

	   GenRow cp = new GenRow();
	   cp.setConnection(con);
	   cp.setTableSpec("ContactProducts");
	   cp.setParameter("-select1","ContactProductID");
	   cp.setParameter("ContactID", contactID);
	   cp.setParameter("ProductID", productID);
	   cp.setParameter("RepUserID", newRep);
	   cp.doAction(GenerationKeys.SELECTFIRST);
	   if(logger.isTraceEnabled()) {
		   logger.trace(cp.getStatement());
	   }

	   if(cp.isSuccessful()) {
		   cp.clear();
		   cp.setParameter("ON-RepUserID", origRep);
		   cp.setParameter("ON-ContactID", contactID);
		   cp.setParameter("ON-ProductID", productID);
		   cp.doAction(GenerationKeys.DELETEALL);
		   if(logger.isTraceEnabled()) {
			   logger.trace(cp.getStatement());
		   }
	   } else {
		   cp.setParameter("RepUserID", origRep);
		   cp.doAction(GenerationKeys.SELECTFIRST);
		   if(logger.isTraceEnabled()) {
			   logger.trace(cp.getStatement());
		   }

		   if(cp.isSuccessful()) {
			   cp.setParameter("RepUserID", newRep);
			   cp.doAction(GenerationKeys.UPDATE);
			   if(logger.isTraceEnabled()) {
				   logger.trace(cp.getStatement());
			   }
		   } else {
			   cp.setToNewID("ContactProductID");
			   cp.setParameter("CreatedBy", newRep);
			   cp.setParameter("CreatedDate","NOW");
			   cp.setParameter("RepUserID", newRep);
			   cp.setParameter("GroupID", groupID);
			   cp.doAction(GenerationKeys.INSERT);
			   if(logger.isTraceEnabled()) {
				   logger.trace(cp.getStatement());
			   }
		   }
	   }		
   }

   void repHandover(String id, String origGroupID, String origRepUserID, String RepUserID) {

	   final String process = StringUtil.getDefaultString(requestrow.getString("-processes"), "none");
	   final String quotes = StringUtil.getDefaultString(requestrow.getString("-quotes"), "none");
	   final String reminders = StringUtil.getDefaultString(requestrow.getString("-reminders"), "none");
	   logger.debug("repHandover(id=[{}],origGroupID=[{}],origRepUserID=[{}],RepUserID=[{}],process=[{}],quotes=[{}],reminders=[{}]",
			   new String[]{id, origGroupID, origRepUserID, RepUserID, process, quotes, reminders});


	   GenRow search = new GenRow();
	   search.setConnection(con);

	   if(tablename.equals("Contacts")) {
		   search.setParameter("ContactID", id);
	   } else {
		   LoggerFactory.getLogger(this.getClass()).info("Not doing handover for tablename {}", tablename);
		   return;
	   }
	   search.setParameter("GroupID", origGroupID);
	   search.setParameter("RepUserID", origRepUserID);
	   //

	   GenRow update = new GenRow();
	   update.setConnection(con);
	   update.setParameter("RepUserID", RepUserID);

	   GenRow related = new GenRow();
	   related.setConnection(con);
	   related.setParameter("RepUserID", RepUserID);
	   related.setParameter("GroupID", origGroupID);

	   Set<String> productIDs = new HashSet<String>();

	   if(!"none".equals(process)) {
		   try { 
			   search.setTableSpec("Opportunities");
			   search.setParameter("-select1","OpportunityID");
			   search.setParameter("-select2","ReferenceProductID as ProductID");
			   if("all".equals(process)) {
				   search.getResults();
			   }
			   else if("active".equals(process)) { 
				   search.setParameter("Inactive","N");
				   search.getResults();
				   search.remove("Inactive");
			   }
			   while(search.getNext()) {
				   // there is a possibility of blank OpportunityID and this doesn't look good in an email.
				   if (search.getString("OpportunityID").length() > 0) {
					   update.setTableSpec("Opportunities");
					   update.setParameter("OpportunityID", search.getString("OpportunityID"));
					   update.doAction(GenerationKeys.UPDATE);
					   if(logger.isTraceEnabled()) {
						   logger.trace(update.getStatement());
					   }
					   checkContactShortlist(id, search.getData("ProductID"), origGroupID, origRepUserID, RepUserID);
	
					   update.remove("OpportunityID");
					   related.setTableSpec("Orders");
					   related.setParameter("ON-OpportunityID", search.getString("OpportunityID"));
					   related.doAction(GenerationKeys.UPDATEALL);
					   if(logger.isTraceEnabled()) {
						   logger.trace(related.getStatement());
					   }
					   related.remove("ON-OpportunityID");
				   }
			   }
		   } finally { 
			   search.close();
		   }
	   }
	   if(!"none".equals(quotes)) {
		   try {
			   if(!"none".equals(process)) {
				   search = new GenRow();
				   search.setConnection(con);
				   if(tablename.equals("Contacts")) {
					   search.setParameter("ContactID", id);
				   }
				   search.setParameter("GroupID", origGroupID);
				   search.setParameter("RepUserID", origRepUserID);
			   }
			   search.setTableSpec("Orders");
			   search.setParameter("-select1","OpportunityID");
			   search.setParameter("-select2","ProductID");
			   search.setParameter("-select3","OrderID");
			   if("all".equals(quotes)) {
				   search.doAction(GenerationKeys.SEARCH);
				   search.getResults();
			   }
			   else if("active".equals(quotes)) { 
				   search.setParameter("Active","Y");
				   search.doAction(GenerationKeys.SEARCH);
				   search.getResults();
				   search.remove("Active");
			   }
			   while(search.getNext()) {
				   productIDs.add(search.getString("ProductID"));
				   update.setTableSpec("Orders");
				   update.setParameter("OrderID", search.getString("OrderID"));
				   update.doAction(GenerationKeys.UPDATE);
				   if(logger.isTraceEnabled()) {
					   logger.trace(update.getStatement());
				   }
				   checkContactShortlist(id, search.getData("ProductID"), origGroupID, origRepUserID, RepUserID);
				   update.remove("OrderID");
				   related.setTableSpec("Opportunities");
				   related.setParameter("OpportunityID", search.getString("OpportunityID"));
				   related.doAction(GenerationKeys.UPDATE);
				   if(logger.isTraceEnabled()) {
					   logger.trace(related.getStatement());
				   }
				   related.remove("OpportunityID");
			   }
		   } finally {
			   search.close();
		   }
	   }

	   if("active".equals(reminders)) {
		   update.setTableSpec("Notes");
		   update.setParameter("ON-Type","Reminder");
		   update.setParameter("ON-ContactID", id);
		   update.setParameter("ON-RepUserID", origRepUserID);
		   update.setParameter("ON-Completed","No+N+EMPTY+NULL");
		   update.doAction(GenerationKeys.UPDATEALL);
		   if(logger.isTraceEnabled()) {
			   logger.debug(update.getStatement());
		   }
	   } 
   }
   
   /**
    * There should be two variants of this method. 
    * 
    * 1. Where the contact is allowed to have multiple reps per group. 
    * 	-	Handover	:	Check for the rep given in the search. Use this rep to change the group rep and hand over the records.
    * 	-	Overwrite	:	Check for the rep given in the search. Use this rep to change the group rep only.
    * 2. Where the contact is only allowed one rep per group.
    * 	-	Handover	:	Check for the current rep assigned to the group. If found, use this rep to change the group rep and hand over the records. If not found, only add the group.
    * 	-	Overwrite	:	Check for the current rep assigned to the group. If found, update to the rep specified. If not found, add the group.
    * 
    */
   void checkGroups(String id, String groupid, String statusid, String repuserid, String overwrite, String handover, String delete)
   {
	   if(logger.isDebugEnabled()) 
		   logger.debug("checkGroups({},{},{},{},{},{})", new String[]{id, groupid, statusid, repuserid, overwrite, handover});
	   
	   GenRow groups = getGroups(id, groupid);
	   String groupkeyname = groups.getTableSpec().getPrimaryKeyName();
	   String setstatusid = null;
	   if("true".equals(handover) && tablename.equals("Contacts")) {
		   //search for the group with the rep from the search. 
		   String repUserID = StringUtils.trimToNull(search.getParameter("ContactContactGroups#Handover.RepUserID"));
		   logger.debug("doing handover for {} with rep {}", id, repUserID);
		   if(repUserID != null) {
			   groups.setParameter("RepUserID", repUserID);
			   groups.doAction(GenerationKeys.SELECTFIRST);
			   if(groups.isSuccessful()) {
				   if(groups.getString("CurrentStatusID").equals(statusid))
				   {
					   setstatusid = null;
				   }
				   else
				   {
					   setstatusid = statusid;
				   }
				   //original rep's group, but if the new rep already has a group then just delete this one.

				   GenRow check = getGroups(id, groupid);
				   check.setParameter("RepUserID",repuserid);
				   check.doAction(GenerationKeys.SELECTFIRST);
				   if(check.isSuccessful()) {
					   if(logger.isDebugEnabled()) logger.debug("deleting old rep group {} for handover as new {} existed", 
							   groups.getString(groupkeyname), check.getString(groupkeyname));
					   check.clear();
					   check.setParameter(groupkeyname, groups.getString(groupkeyname));
					   check.doAction(GenerationKeys.DELETE);
					   //TODO - delete group status entries.
				   } else {
					   logger.debug("updating old rep group for handover");
					   updateGroup(groups.getString(groupkeyname), setstatusid, repuserid);
				   }
				   repHandover(id, groupid, repUserID, repuserid);
			   } else {
				   logger.debug("old rep group was not found, inserting a new one.");
				   insertGroup(id, groupid, statusid, repuserid);
			   }
			   return;
		   } 
	   }

      groups.getResults();
      boolean found = false;
      if(groups.getNext())
      {
         found = true;
         if("true".equals(overwrite))
         {
            if(groups.getString("CurrentStatusID").equals(statusid))
            {
               setstatusid = "EMPTY";
            }
            else
            {
               setstatusid = statusid;
            }
            logger.trace("existing group found {} = {}", groupkeyname, groups.getString(groupkeyname));
            updateGroup(groups.getString(groupkeyname), setstatusid, repuserid);
         }
      }
      groups.close();
      if(!found)
      {
    	  logger.trace("inserting new group for id={} groupID={}", id, groupid);
         insertGroup(id, groupid, statusid, repuserid);
      }
   }  
   
   void insertGroups(String id) {
      String[] groupids = requestrow.getParameterValues("-addgroup_GroupID");
      if (groupids != null) {
	      String[] statusids = requestrow.getParameterValues("-addgroup_StatusID");
	      String[] repuserids = requestrow.getParameterValues("-addgroup_RepUserID");
	      String[] overwrites = requestrow.getParameterValues("-addgroup_overwrite");
	      String[] deletes = requestrow.getParameterValues("-addgroup_delete");
	      String[] handovers = requestrow.getParameterValues("-addgroup_handover");     
	      if((handovers != null && overwrites != null && handovers.length != overwrites.length) || (handovers == null && overwrites != null)) {
	    	  handovers = new String[overwrites.length];	//to cope with cases where the ui doesn't have handovers. warning, if it doesn't then you can't try doing length on it
	      } else if (handovers == null && overwrites == null) {
	    	  handovers = new String[groupids.length];
	    	  overwrites = new String[groupids.length];
	      }
	      if (deletes == null) {
	    	  deletes = new String[groupids.length];
	      }
	      if(groupids!=null){
	         for(int i=0; i<groupids.length; i++){
	            if(groupids[i]!=null && groupids[i].length()!=0){
	            	if (deletes != null && "true".equals(deletes[i]))
	            		deleteGroup(id, groupids[i]);
	            	else
	            		checkGroups(id, groupids[i], statusids[i], repuserids[i], overwrites[i], handovers[i], deletes[i]);
	            }
	         }
	      }
      }
   }
   
   void deleteGroup(String id, String groupid) {
	   if (StringUtils.isBlank(id) || StringUtils.isBlank(groupid)) return;
	   
	   GenRow check = new GenRow();
	   check.setViewSpec("ContactGroupView");
	   check.setParameter("ContactID", id);
	   check.setParameter("GroupID", "!" + groupid);
	   check.doAction("selectfirst");
	   
	   if (check.isSet("ContactGroupID")) {
		   GenRow del = new GenRow();
		   del.setViewSpec("ContactGroupView");
		   del.setParameter("ON-ContactID", id);
		   del.setParameter("ON-GroupID", groupid);
		   del.doAction("deleteall");
	   }
   }
   
	void updateRecords()
	{
		int success = 0;
		String id = null;
		
		
		boolean doProfiles = requestbean.getString("QuestionID").length()>0; 
		
		if (doProfiles) { 
			doProfiles = setProfileFields(); 
		}
		for(int i=0; i<total && !isKilled(); i++)
		{
			try
			{
				id = (String)foundset.get(i);
				if(insertcontactstatus)
				{
					insertContactStatus(id);
				}
				else if(insertcompanystatus)
				{
					insertCompanyStatus(id);
				}
				else if(insertopportunitystatus)
				{
					insertOpportunityStatus(id);
				}
				else if(insertorderstatus)
				{
					insertOrderStatus(id);
				}
				else if(insertproductstatus)
				{ 
					insertProductStatus(id); 
				}
				
				if(recalculateProductGST) 
				{
					recalculateProductGST(id); 
				} 
				
				if(updaterelatedgroup)
				{
				   updateRelatedGroup(id); 
				}
				
				success = updateRow(i);
				if(doProfiles) { 
					updateProfiles(i); 
				}
            if(insertgroups) { 
               insertGroups(id); 
            }
                if(insertProductGroupStatus) { 
					updateProductGroupStatus(id); 
				}
			}
			catch(Exception e)
			{
				errors.append(ActionBean.writeStackTraceToString(e));
				errors.append(" - ");
				errors.append((String)context.getInitParameter("URLHome"));

		      if ("Products".equals(tablename)) {
		         errors.append("/crm/cms/products/");
		      }
		      else {
		         errors.append("/crm/");
		      }
				errors.append(viewname);
				errors.append(".jsp?-action=select&");
				errors.append(keyname);
				errors.append("=");
				errors.append(id);
				errors.append("\r\n");
			}
			if(success==1)
			{
				updated++;
			}
			else
			{
				failed++;
			}
		}
	}

	public void execute()
	{
		String debugstring = requestbean.getString(DEBUG);
		String checkcountstring = requestbean.getString(CHECKCOUNT);

		viewname = requestbean.getString(VIEW);

		if(debugstring.length()!=0)
		{
			debug = true;
		}
		if(checkcountstring.length()!=0)
		{
			checkcount = Integer.parseInt(checkcountstring);
		}
		con = null;
		ctx = null;
		try
		{

			setEmailHeader();
			if(user == null || search == null)
			{
				status.append("Resources not found, offline process halted.\r\n");
				return;
			}

			requestbean.put("ModifiedDate","NOW");
			requestbean.put("ModifiedBy",user.getCurrentUserID());

			if(requestbean.containsKey("StatusID"))
			{
				String statusid = (String)requestbean.get("StatusID");
				if(statusid!=null && statusid.length()!=0)
				{
					if (tablename.equals("Contacts")) {
						insertcontactstatus = true;
					}
					else if (tablename.equals("Companies")) {
						insertcompanystatus = true;
					}
					else if (tablename.equals("Opportunities")) {
						insertopportunitystatus = true;
					}
					else if (tablename.equals("Orders")) {
						insertorderstatus = true;
					}
					else if (tablename.equals("Products")) { 
						insertproductstatus = true; 
					}
				}
			}

         if (tablename.equals("Contacts")) {
            insertgroups = true;
         }
         else if (tablename.equals("Companies")) {
            insertgroups = true;
         }
         else if (tablename.equals("Products")) { 
            insertgroups = true;
         }

         if(requestbean.getString("GroupID").length()!=0 && "true".equals(requestbean.getString("-updaterelatedgroupid")))
         {
            updaterelatedgroup = true;
         }
         
			
			if(requestbean.getString("ProductGroupStatus").length()!=0)
			{
				insertProductGroupStatus = true;
			}

			/*
			 * THIS BLOCK SHOULD BE IGNORED IF THERE IS A COST SPECIFIED
			 * AS IT WILL NOT NEED TO BE CALCULATED IF IT IS ONLY TO BE OVERWRITTEN. 
			 */
			if(tablename.equals("Products")) { 
				boolean updatesCosts = false; 
				if(requestbean.containsKey("Cost")) { 
					String Cost = (String)requestbean.get("Cost");
					if(Cost != null && Cost.length()>0) { 
						updatesCosts = true;
					}
				}
				if(!updatesCosts && requestbean.containsKey("GSTExempt")) { 
					String GSTExempt = (String)requestbean.get("GSTExempt"); 
					if(GSTExempt != null && GSTExempt.length()>0) { 
						recalculateProductGST = true;
						if ("Y".equals(GSTExempt)) { 
							GSTValue = 0.0; 
						} else if ("N".equals(GSTExempt)) { 
							GSTValue = Double.parseDouble(InitServlet.getSystemParams().getProperty("SalesTaxRate"));
						}
					}
				}
			}


			getStatementString();
			getConnection();

			retrieveRecords();

			if(checkcount!=-1)
			{
				if(total!=checkcount)
				{
					status.append("Search results count does not match, offline process halted.\r\n");
					status.append("Current - ");
					status.append(String.valueOf(total));
					status.append(", Original - ");
					status.append(String.valueOf(checkcount));
					status.append("\r\n");
					//status.append("\r\n");
					//status.append(search.getStatement());
					//status.append("\r\n");
					return;	
				}
			}

			updateRecords();
			status.append(String.valueOf(total));
			status.append(" ");
			status.append(tablename);
			status.append(" found.\r\n");
			status.append(String.valueOf(updated));
			status.append(" ");
			status.append(getTargetTableName());
			status.append(" updated.\r\n");
			status.append(String.valueOf(failed));
			status.append(" ");
			status.append(getTargetTableName());
			status.append(" failed.\r\n");
			if (isKilled()) {
				status.append("Offline process terminated by - ");
				status.append(getKiller());
				status.append("\r\n\r\n");
			}
			else {		
				status.append("Offline process completed.\r\n\r\n");
			}

		}
		catch(Exception e)
		{
			status.append("Offline process halted on ");
			status.append(String.valueOf(updated+failed));
			status.append(" of ");
			status.append(String.valueOf(total));
			status.append(".\r\n\r\n");
			status.append(e.toString());
		}
		finally
		{
	 		if (updatestm != null) 
	 		{
				try{
					statusstm.close();
					statusstm = null;
		  		}catch (Exception ex){}
	 		}
	 		if (updatestm != null) 
	 		{
				try{
					updatestm.close();
					updatestm = null;
		  		}catch (Exception ex){}
	 		}
	 		if (con != null) 
	 		{
				try{
					con.close();
					con = null;
		  		}catch (Exception ex){}
	 		}
	 		if (ctx != null) 
	 		{
		  		try{		 
					ctx.close();
					ctx = null;
		  		}catch (Exception ex){}
	 		}

			if(errors.length()!=0)
			{
				status.append("\r\n");
				status.append(errors.toString());
			}
		}
		logger.debug(status.toString());
	}
/*
	protected String getParamValue(String paramname)
	{
		String[] params = request.getParameterValues(paramname);
		if(params != null)
		{
			if(params.length==1)
			{
				return(params[0]);
			}
			else
			{
				StringBuffer temp = new StringBuffer();
				temp.append(params[0]);
				for(int i=1; i<params.length; i++)
				{
					if(params[i]!=null && params[i].length()!=0)
					{
						temp.append("+");
						temp.append(params[i]);
					}
				}
				return(temp.toString());
			}
		}
		return(null);
	}
*/
	void setEmailHeader()
	{
		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
		status.append(contextname);
		status.append(" Runway offline process - Update ");
		status.append(getTargetTableName());
		status.append("\r\n");
		status.append("Initiated ");
		status.append(dt.format(new java.util.Date()));
		status.append("\r\n");
	}

	public String getStatusMailBody()
	{
		return(status.toString());
	}

	public String getStatusMailSubject()
	{
		return(contextname + " Runway offline process - Update "+tablename);
	}

	public String getStatusMailRecipient()
	{
		return(user.getEmail());
	}
	
	protected String getTargetTableName() {
		return tspec.getTableName();
	}
	
	protected String getSourceTableName() {
		return tspec.getTableName();
	}
	
	public int getProcessSize() {
		return foundset.size();
	}
	
	public int getProcessedCount() {
		return updated+failed;
	}
	
	public int getErrorCount() {
		return failed;
	}
}
