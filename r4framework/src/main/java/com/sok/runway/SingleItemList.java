package com.sok.runway;

import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class SingleItemList {
      ArrayList names = null;
      ArrayList values = null;
      ArrayList quantifiers = null;
      ArrayList displayFields = null;
      ArrayList displayNames = null;
      ArrayList canDeleteList = null;
            
      /*Assigning a default value named 'MS' from Title itemlists just for testing*/
      String defaultvalue = null;
     
      public SingleItemList() {
         names = new ArrayList(100);
         values = new ArrayList(100);
         quantifiers = new ArrayList(100);
         displayFields = new ArrayList(100);
         displayNames = new ArrayList(100);
         canDeleteList = new ArrayList(100);
         //defaultvalue = null;
      }
      public SingleItemList(String thedefaultvalue) {
          names = new ArrayList(100);
          values = new ArrayList(100);
          quantifiers = new ArrayList(100);
          this.defaultvalue = thedefaultvalue;
          displayFields = new ArrayList(100);
          displayNames = new ArrayList(100);
          canDeleteList = new ArrayList(100);          
       }     
      
      public String toJSONString() 
      {
    	  JSONObject o = null; 
    	  
    	  JSONArray a = new JSONArray(); 
    	  for(int i=0; i<names.size(); i++) {
    		  o = new JSONObject();
    		  o.put("name", names.get(i));
    		  o.put("value", values.get(i));
    		  o.put("quantifier", quantifiers.get(i));
    		  o.put("displayfield", displayFields.get(i));
    		  o.put("displayname", displayNames.get(i));
    		  o.put("canDelete", canDeleteList.get(i));
    		  a.add(o);
    	  }
    	  o = new JSONObject();
    	  o.put("default",this.defaultvalue); 
    	  o.put("items",a); 
    	  
    	  return o.toJSONString();
      }
      
      /*Creating accessor and mutator methods for the itemlist default value*/
      public String getDefaultValue() {
    	   return defaultvalue;
       }      
      public void setDefaultValue(String aValue) {
   	   		this.defaultvalue = aValue;
      }
      
      public boolean valueExists(String value) {
         return values.indexOf(value) != -1;
      }
      
      public String getNameFromValue(String value) {
         int i = values.indexOf(value);
         if (i == -1) {
            return value;
         }
         else {
            return (String)names.get(i);
         }
      }
      
      public void put(String name, String value, int quantifier) {
         names.add(name);
         values.add(value);
         quantifiers.add(new Integer(quantifier));
      }
      
      public void put(String name, String value, int quantifier, String displayField,  String displayName, String systemItem) {
          names.add(name);
          values.add(value);
          quantifiers.add(new Integer(quantifier));
          displayFields.add(displayField);
          displayNames.add(displayName);
          canDeleteList.add(systemItem);
      }
      
      public int size() {
         return values.size();
      }
      
      public String getValue(int i) {
         return (String)values.get(i);
      }
      
      public String getName(int i) {
         return (String)names.get(i);
      }
      
      public int getQuantifier(int i) {
         return ((Integer)quantifiers.get(i)).intValue();
      }
      
      public String getDisplayField(int i) {
          return (String)displayFields.get(i);
       }
      
      public String getDisplayName(int i) {
          return (String)displayNames.get(i);
       }
      
      public String getCanDelete(int i) {
          return (String)canDeleteList.get(i);
       }
            
      public int getIndexOfValue(String value) {
         return values.indexOf(value);
      }
   }