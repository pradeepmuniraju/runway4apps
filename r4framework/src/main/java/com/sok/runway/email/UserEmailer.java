package com.sok.runway.email;

//import java.sql.Connection;
//import java.sql.SQLException;

import javax.servlet.http.HttpSession;

import com.sok.framework.*;
import com.sok.framework.generation.*;
//import com.sok.runway.ErrorMap;
import com.sok.runway.SessionKeys;
//import com.sok.runway.crm.Contact;
//import com.sok.runway.crm.Note;
import com.sok.runway.email.subtype.*;

/* failed email testing *//*
import javax.mail.Address; 
import javax.mail.MessagingException;
import javax.mail.SendFailedException; 
import javax.mail.internet.InternetAddress;
import com.sun.mail.smtp.SMTPSendFailedException;
import com.sun.mail.smtp.SMTPAddressFailedException; 
import com.sun.mail.smtp.SMTPAddressSucceededException;*/

public class UserEmailer extends AbstractEmailer implements Emailer {
	/*
	 * Email types are implemented as EmailerSubtype.
	 * 
	 * Do not put any code specific to types of emails in here.
	 */

	public static final String USERID = "UserID";
	public static final String USERLINK = "/crm/admin/users/userview.jsp?-action=select&UserID=";

	protected void loadSharedTokens() {
		super.loadSharedTokens();
	}

	protected void loadSpecificTokens() {
		loadSpecificTokens(requestrow.getString(USERID));
	}

	public boolean isEmailerType(int type) {
		if (type == Emailer.ContactEmailer) {
			return (true);
		}
		return (false);
	}

	public boolean isHTMLEmail() {
		if (specificcontext != null) {
			if (specificcontext.getString("EmailType").equals("Plain Text")) {
				return (false);
			}
		}
		return (true);
	}

	protected void loadSpecificTokens(String id) {
		specificcontext = getBean("UserView",USERID, id);
		EmailerSubtype subtype = subtypehead;
		while (subtype != null) {
			subtype.loadSpecificTokens(this, id);
			subtype = subtype.getNext();
		}
	}

	public void appendError(StringBuffer errors, String msg, String recordid) {
		errors.append(msg);
		errors.append(" - ");
		appendLinkURL(errors, recordid);
		errors.append("\r\n");
	}

	public void appendLinkURL(StringBuffer buffer, String recordid) {
		buffer.append(sharedcontext.getString("URLHome"));
		buffer.append(USERLINK);
		buffer.append(recordid);
	}

	public TableData getOfflineSearch(HttpSession session) {
		TableData search = (TableData) session
				.getAttribute(SessionKeys.USER_SEARCH);

		if (search != null) {
			search.setViewSpec("UserView");
		}
		return (search);
	}

	public static final String userlist = "-userlist";

	public String getOfflineList() {
		return (requestrow.getString(userlist));
	}

	public String getPrimaryKeyName() {
		return (USERID);
	}

	@Override
	protected boolean hasOptOut() {
		return false;
	}

	@Override
	protected boolean isRep() {
		return false;
	}

	@Override
	protected boolean hasGlobalOptOut() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean hasGroupOptOut() {
		// TODO Auto-generated method stub
		return false;
	}
}