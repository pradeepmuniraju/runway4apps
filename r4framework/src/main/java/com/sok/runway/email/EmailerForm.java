package com.sok.runway.email;

public interface EmailerForm
{  
   public void generateForm();   
   
   public void generateSavedForm();     
   
   public String getFormTo();
   
   public String getFormFrom();
   
   public String getFormCc();
   
   public String getFormReplyTo();   
   
   public String getFormText();
   
   public String getFormSubject();
   
   public boolean isOptedOut();
   
   public boolean hasTemplate();
}