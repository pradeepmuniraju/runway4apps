package com.sok.runway.email;
import javax.servlet.http.HttpSession;
import com.sok.framework.*;

public class TemplatePreview extends AbstractEmailer implements Emailer
{
   public static final String TEMPLATEID = "TemplateID";
   public static final String CMSID = "CMSID";
   
   protected void loadSpecificTokens()
   {
      loadSpecificTokens(null);
   } 
   
   public boolean isEmailerType(int type)
   {
      if(type == Emailer.TemplatePreview)
      {
         return(true);
      }
      return(false);
   }
   
   public boolean isEmailerSubType(int type)
   {
      if(type == Emailer.BaseEmailer)
      {
         return(true);
      }
      return(false);
   }     
   
   protected void loadSpecificTokens(String id)
   {
      specificcontext = new GenRow();
   }
   
   protected void setNoteParameters(GenRow notebean)
   {

   }
   
  public void loadProfileTokens(String id)
  {

   }  
  
  public String send()
  {
     throw new EmailerException("This class for previewing templates only.");
  }
  
  protected boolean hasOptOut()
  {
     return(false);
  }
  
  protected boolean isRep()
  {
     return(true);
  }  
  
  public TableData getOfflineSearch(HttpSession session)
  {
     return(null);
  }
  
  public String getOfflineList()
  {
     return(null);
  }
  
  public String getPrimaryKeyName()
  {
     return(null);
  }
  
  public void appendLinkURL(StringBuffer buffer, String recordid)
  {
     buffer.append(" - ");
     buffer.append(recordid);
  }

@Override
protected boolean hasGlobalOptOut() {
	// TODO Auto-generated method stub
	return false;
}

@Override
protected boolean hasGroupOptOut() {
	// TODO Auto-generated method stub
	return false;
}  
}