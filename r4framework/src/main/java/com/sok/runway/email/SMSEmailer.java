package com.sok.runway.email;

import java.io.StringWriter;
import java.util.Iterator;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.RowBean;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.SMSBean;
import com.sok.runway.sms.SMSProvider;

public class SMSEmailer {

	SMSBean transport = null;
	VelocityEngine ve = null;

	protected TableData sharedcontext = null;
	protected TableData specificcontext = null;
	protected TableData requestrow = null;
	protected GenRow template = null;
	protected ServletContext context = null;
	
	private boolean debug = false;

	public SMSEmailer() {
		// TODO Auto-generated constructor stub
	}

	public SMSEmailer(TableData sharedcontext, TableData specificcontext) {
		this.sharedcontext = sharedcontext;
		this.specificcontext = specificcontext;
	}

	public SMSBean getTransport() {
		return transport;
	}

	public void setTransport(SMSBean transport) {
		this.transport = transport;
	}

	public TableData getSharedcontext() {
		return sharedcontext;
	}

	public void setSharedcontext(TableData sharedcontext) {
		this.sharedcontext = sharedcontext;
	}

	public TableData getSpecificcontext() {
		return specificcontext;
	}

	public void setSpecificcontext(TableData specificcontext) {
		this.specificcontext = specificcontext;
	}

	public GenRow getTemplate() {
		return template;
	}

	public void setTemplate(GenRow template) {
		this.template = template;
	}

	public ServletContext getContext() {
		return context;
	}

	public void setContext(ServletContext context) {
		this.context = context;
	}
	
	public boolean loadTemplate() {
		if (sharedcontext == null || StringUtils.isBlank(sharedcontext.getString("SMSTemplateID"))) return false;
		
		template = new GenRow();
		template.setViewSpec("TemplateView");
		template.setParameter("TemplateID", sharedcontext.getString("SMSTemplateID"));
		template.doAction(GenerationKeys.SELECTFIRST);
		
		System.out.println(template.getStatement());
		
		return template.isSuccessful();
	}

	public boolean sendMessage() throws Exception
	{
		try
		{
			transport = new SMSBean();
			transport.connect();
			
			ve = VelocityManager.getEngine();
			
			String newnoteid = KeyMaker.generate();
			String emailstatus = "Sent";

			if(specificcontext == null)
			{
				//appendError("Specific Data not set");
				return false;
			}
			
		   String mobileName = "Mobile";
		   if (specificcontext.getString("MainPhoneField").startsWith("Mobile")) mobileName = specificcontext.getString("MainPhoneField");

			String mobile = specificcontext.getString(mobileName).replaceAll("[^0-9]", "");
			if (mobile == null || mobile.length() == 0) mobile = specificcontext.getString("Mobile").replaceAll("[^0-9]", "");
			if (mobile == null || mobile.length() == 0) mobile = specificcontext.getString("Mobile2").replaceAll("[^0-9]", "");
			if (mobile == null || mobile.length() == 0) mobile = specificcontext.getString("Mobile3").replaceAll("[^0-9]", "");
			if (mobile == null || mobile.length() == 0) mobile = specificcontext.getString("Mobile4").replaceAll("[^0-9]", "");
			
			if ("RepMobile".equals(template.getString("formFieldFull"))) mobile = specificcontext.getString("RepMobile").replaceAll("[^0-9]", "");
			
			/* need to really update this, but it should have already passed this in the Email part
			String contactstatus = contact.getString("CurrentStatus");
			
			if(contactstatus != null && contactstatus.equals("OptOut"))
			{
				appendError("Opt-out contact", contactid);
				return(1);
			}
			if(contactstatus != null && contactstatus.equals("Left Company"))
			{
				appendError("Contact Left Company", contactid);
				return(1);
			}
			String contactstatusid = (String)contact.get("CurrentStatusID");

			if(contactstatusid != null && contactstatusid.equals(context.getInitParameter("ContactOptOutStatusID")))
			{
				appendError("Opt-out contact", contactid);
				return(1);
			}
			if(contactstatusid != null && contactstatusid.equals(context.getInitParameter("ContactLeftCompanyStatusID")))
			{
				appendError("Contact Left Company", contactid);
				return(1);
			}
			*/
			

			String emailfrom = (String) specificcontext.get("RepEmail");
			if (StringUtils.isBlank(emailfrom)) emailfrom = InitServlet.getSystemParam("DefaultEmail");
			
			//contact.put("NoteID",newnoteid);
	     
			StringWriter textw = new StringWriter();
			StringWriter sw = new StringWriter();


			try
			{
				String smsFrom = template.getString("EmailFrom");
				if("RepMobile".equals(smsFrom)) smsFrom = (String) specificcontext.get("RepMobile");
				if (StringUtils.isBlank(smsFrom)) smsFrom = InitServlet.getSystemParam("SMSFrom");

				if ("RepMobile".equals(template.getString("formFieldFull"))) {
					mobile = specificcontext.getString("RepMobile").replaceAll("[^0-9]", "");
					smsFrom = InitServlet.getSystemParam("SMSFrom");
				}

				if(StringUtils.isNotBlank(smsFrom)) {
					smsFrom = smsFrom.replaceAll("[^0-9]", "");
				}
				
				ve.evaluate( specificcontext, sw, specificcontext.getString("ContactID"), template.getString("Subject"));
				sw.flush();
				sw.close();
				String notesubject = sw.toString();


				//ve.evaluate( contact, textw, contactid, formbean.getString("TextHeader") );
				ve.evaluate( specificcontext, textw, specificcontext.getString("ContactID"), template.getString("Body"));
				//ve.evaluate( contact, textw, contactid, formbean.getString("TextFooter") );
				textw.flush();
				textw.close();
				String textbody = textw.toString();

				if(mobile == null || mobile.length()==0)
				{
					saveNote(specificcontext, emailfrom, newnoteid, textbody, notesubject, "No Mobile");
					
					return false;
				}

				transport.clear();
				transport.setNumber(mobile);
				transport.setMessage(textbody);
				transport.setSubject(notesubject);
				transport.setFrom(emailfrom);
				transport.setSender(smsFrom);

				if(!debug)
				{
					emailstatus  = transport.send();
				}
				else
				{
					emailstatus = "Testing";
					//errors.append(textbody);
					//errors.append("\r\n");
				}
				
				if(!SMSProvider.sent.equals(emailstatus)) emailstatus += "|" + smsFrom;

				saveNote(specificcontext, emailfrom, newnoteid, textbody, notesubject, emailstatus);
				
				if(SMSProvider.sent.equals(emailstatus))
				{
					return true;
				}
				else
				{
					System.out.println("SMS for Emails failed for " + specificcontext.getString("ContactID") + " with " + emailstatus);
					//appendError(emailstatus, specificcontext.getString("ContactID"));
					return false;
				}
			}
			catch(Exception e)
			{
				//appendError(e.toString(), contactid);
				return false;
			}
			

		}
		finally
		{
			if(transport!=null)
			{
				try{
					transport.close();
					transport = null;
				}catch(Exception te){}
			}
		}
	}

	void saveNote(TableData contact, String from, String newnoteid, String notebody, String notesubject, String status)
	{
		try
		{
			RowBean notesave = new RowBean();
			notesave.setViewSpec("NoteView");
			notesave.setColumn("NoteID",newnoteid);
			notesave.setColumn("ParentNoteID", contact.getString("NoteID"));
			notesave.setColumn("ContactID",contact.getString("ContactID"));
			notesave.setColumn("CompanyID",contact.getString("CompanyID"));
			notesave.setColumn("Type","SMS");
			notesave.setColumn("RepUserID",contact.getString("RepUserID"));
			notesave.setColumn("ModifiedBy",contact.getString("RepUserID"));
			notesave.setColumn("TemplateUsed",template.getString("TemplateID"));
			notesave.setColumn("Campaign",specificcontext.getString("Campaign"));
			notesave.setColumn("CallStatus",status);
			notesave.setColumn("Completed","Yes");
			notesave.setColumn("Subject",notesubject);
			notesave.setColumn("Body",notebody);
			notesave.setColumn("CreatedBy",contact.getString("RepUserID"));
			notesave.setColumn("NoteTypeDescription","EmailSMS");
			if(status.equals(SMSProvider.sent))
			{
				notesave.setColumn("SentDate","NOW");
			}
            notesave.setColumn("EmailFrom", from);
            notesave.setColumn("EmailTo", contact.getString("Mobile"));
			notesave.setColumn("NoteDate","NOW");
			notesave.setColumn("CreatedDate","NOW");
			notesave.setColumn("ModifiedDate","NOW");
			notesave.setColumn("GroupID",contact.getString("GroupID"));
			notesave.doAction("insert");
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
