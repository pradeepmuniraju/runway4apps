package com.sok.runway.email;

import java.text.NumberFormat;

//import java.sql.Connection;
//import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.RowSetBean;
import com.sok.framework.RowSetWrapperBean;
import com.sok.framework.RunwayUtil;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.TableSpec;
import com.sok.framework.ViewSpec;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.Contact;
import com.sok.runway.email.subtype.EmailerSubtype;

/* failed email testing */ /*
import javax.mail.Address; 
import javax.mail.MessagingException;
import javax.mail.SendFailedException; 
import javax.mail.internet.InternetAddress;
import com.sun.mail.smtp.SMTPSendFailedException;
import com.sun.mail.smtp.SMTPAddressFailedException; 
import com.sun.mail.smtp.SMTPAddressSucceededException;*/

public class ContactEmailer extends AbstractEmailer implements Emailer
{
	private static final Logger logger = LoggerFactory.getLogger(ContactEmailer.class); 
	/*
	 * Email types are implemented as EmailerSubtype.
	 * 
	 * Do not put any code specific to types of emails in here.
	 */       

	public static final String CONTACTID = "ContactID";
	public static final String COMPANYID = "CompanyID";

	private static final NumberFormat currancyFull = NumberFormat.getCurrencyInstance();
	private static final NumberFormat currancyShort = NumberFormat.getCurrencyInstance();

	protected boolean hasOptOut()
	{
		GenRow temp = getOptOutChecker();
		temp.setColumn(CONTACTID, specificcontext.getString(CONTACTID));
		temp.doAction(GenerationKeys.SELECTFIRST);

		return(temp.isSuccessful() && temp.getString("OptOutID").length() > 0);
	}    

	public boolean isOptedOut()
	{
		if(specificcontext!=null)
		{
			if(super.isOptedOut())
			{
				return(true);
			}
		}
		return(false);
	} 

	protected boolean hasGlobalOptOut()
	{
		if(specificcontext.getString("CurrentStatusID").length()!=0 && 
				sysprops.getProperty("ContactLeftCompanyStatusID") != null && 
				sysprops.getProperty("ContactLeftCompanyStatusID").length() > 0 &&
				sysprops.getProperty("ContactOptOutStatusID") != null &&
				sysprops.getProperty("ContactOptOutStatusID").length() > 0) 
		{
			if (specificcontext.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("leftcompany")
					|| specificcontext.getString("CurrentStatusID").equals(sysprops.getProperty("ContactLeftCompanyStatusID"))) {

				return true;
			} else if (specificcontext.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
					|| specificcontext.getString("CurrentStatusID").equals(sysprops.getProperty("ContactOptOutStatusID"))) {
				//System.out.println("AbstractEmailer:isOptedOut(" + specificcontext.getString("ContactID") + ")");
				return true;
			}
		}
		return(false);
	}

	private boolean hasSingleGroupOptOut(String groupID)
	{
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("email/ContactGroupGroupStatusView");
		row.setParameter("GroupID", groupID);
		row.setParameter(CONTACTID, specificcontext.getString(CONTACTID));
		row.doAction("selectfirst");

		specificcontext.setColumn("GroupID", row.getData("GroupID"));
		specificcontext.setColumn("GroupName", row.getString("Name"));

		if (row.getString("CurrentStatus").length() > 0) {
			return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
					|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
					|| "Y".equals(row.getString("EmailOptOut")));         
		}
		return false;
	}

	protected boolean hasGroupOptOut()
	{
		// if multiple groups has been set, they must all be checked.
		if (!sharedcontext.isSet("CampaignID")) {
			if(requestrow.isSet("GroupIDs")) { 
				String[] groupIDs = requestrow.getParameterValues("GroupIDs");
				for(String gID: groupIDs) 
				{
					if(hasSingleGroupOptOut(gID)) {

						return true;
					}  
				}
				return false;
			}
			// check to see if there is a Group for the GroupID and if it is OptOut
			if(requestrow.isSet("GroupID")) 
			{
				if(hasSingleGroupOptOut(requestrow.getString("GroupID"))) { 
					return true;
				}
				return false;
			}
		}

		// check for a single group and if it is OptOut (bulk emailer will provide the wrong groupID
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("email/ContactGroupGroupStatusView");
		row.setParameter("GroupID", "!NILL;!EMPTY");
		row.setParameter(CONTACTID, specificcontext.getString(CONTACTID));
		row.sortBy("SortOrder", 0);
		row.doAction("search");
		row.getResults(true);

		if (row.getSize() == 1) {
			row.getNext();

			if (sharedcontext.isSet("CampaignID")) {
				GenRow groups = new GenRow();
				groups.setConnection(getConnection());
				groups.setTableSpec("LinkedCampaigns");
				groups.setParameter("-select","*");
				groups.setParameter("CampaignID",sharedcontext.getString("CampaignID"));
				groups.setParameter("LinkedGroupID",row.getData("GroupID"));
				groups.doAction("selectfirst");
				if (groups.isSuccessful()) {
					specificcontext.setColumn("GroupID", row.getData("GroupID"));
					specificcontext.setColumn("GroupName", row.getString("Name"));

					row.close();

					return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
							|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
							|| "Y".equals(row.getString("EmailOptOut")));
				}
			} else if (requestrow.isSet("GroupIDs")) {
				String[] groupIDs = requestrow.getParameterValues("GroupIDs");
				for(String gID: groupIDs) 
				{
					if (gID != null && gID.equals(row.getString("GroupID"))) { 
						specificcontext.setColumn("GroupID", row.getData("GroupID"));
						specificcontext.setColumn("GroupName", row.getString("Name"));

						row.close();

						return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
								|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
								|| "Y".equals(row.getString("EmailOptOut")));
					}
				}
				row.close();
				return false;
			} else if (requestrow.isSet("GroupID") && requestrow.getString("GroupID").equals(row.getString("GroupID"))) {
				specificcontext.setColumn("GroupID", row.getData("GroupID"));
				specificcontext.setColumn("GroupName", row.getString("Name"));

				row.close();

				return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
						|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
						|| "Y".equals(row.getString("EmailOptOut")));
			}

			specificcontext.setColumn("GroupID", row.getData("GroupID"));
			specificcontext.setColumn("GroupName", row.getString("Name"));

			row.close();

			return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
					|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID")));         
		} else if (row.getSize() > 1) {
			while (row.getNext()) {

				//System.out.println("ContactEmailer: multigroup");
				if (sharedcontext.isSet("CampaignID")) {
					GenRow groups = new GenRow();
					groups.setConnection(getConnection());
					groups.setTableSpec("LinkedCampaigns");
					groups.setParameter("-select","*");
					groups.setParameter("CampaignID",sharedcontext.getString("CampaignID"));
					groups.setParameter("LinkedGroupID",row.getData("GroupID"));
					groups.doAction("selectfirst");
					if (groups.isSuccessful()) {
						specificcontext.setColumn("GroupID", row.getData("GroupID"));
						specificcontext.setColumn("GroupName", row.getString("Name"));

						TableData rep = getRepBean();
						//System.out.println("ContactEmailer rep: "+rep.toString() + ""+ );
						if (rep.isSet("UserID") && rep.isSet("Status") && "Active".equals(rep.getString("Status"))) {
							boolean r = (row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
									|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
									|| "Y".equals(row.getString("EmailOptOut")));

							if (!r) {
								row.close();
								return false;
							}
						}
					}
				} else if (requestrow.isSet("GroupIDs") && !requestrow.isSet("GroupID")) {
					String[] groupIDs = requestrow.getParameterValues("GroupIDs");
					for(String gID: groupIDs) 
					{
						if (gID != null && gID.equals(row.getString("GroupID"))) { 
							specificcontext.setColumn("GroupID", row.getData("GroupID"));
							specificcontext.setColumn("GroupName", row.getString("Name"));

							TableData rep = getRepBean();

							if (rep.isSet("UserID") && rep.isSet("Status") && "Active".equals(rep.getString("Status"))) {
								boolean r = (row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
										|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
										|| "Y".equals(row.getString("EmailOptOut")));

								if (!r) {
									row.close();
									return false;
								}
							}
						}
					}
				} else if (requestrow.isSet("GroupID") && requestrow.getString("GroupID").equals(row.getString("GroupID"))) {
					specificcontext.setColumn("GroupID", row.getData("GroupID"));
					specificcontext.setColumn("GroupName", row.getString("Name"));

					TableData rep = getRepBean();

					if (rep.isSet("UserID") && rep.isSet("Status") && "Active".equals(rep.getString("Status"))) {
						boolean r = (row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
								|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
								|| "Y".equals(row.getString("EmailOptOut")));

						if (!r) {
							row.close();
							return false;
						}
					}
				}
			}
			row.close();
			specificcontext.setColumn("GroupName", "No Active Sales Rep Found for " + getGroupName(specificcontext.getString("GroupID")));
			return true;
		}

		return(false);
	}

	protected String getGroupName(String groupID) {
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("GroupView");
		row.setParameter("GroupID", groupID);
		row.doAction("selectfirst");

		return row.getString("Name");
	}

	protected boolean isRep()
	{
		if(specificcontext!=null){
			GenRow row = new GenRow();
			row.setConnection(getConnection());
			row.setTableSpec("ContactGroups");
			row.setParameter("-select1", "ContactGroupID");
			row.setParameter(CONTACTID, specificcontext.getString(CONTACTID));
			row.setParameter("RepUserID", currentUser.getUserID());
			row.doAction("selectfirst");

			return(row.isSuccessful());    
		}
		return(true);
	}   

	protected void loadSharedTokens()
	{
		super.loadSharedTokens();
	}

	protected void loadSpecificTokens()
	{
		loadSpecificTokens(requestrow.getString(CONTACTID));
	} 

	public boolean isEmailerType(int type)
	{
		if(type == Emailer.ContactEmailer)
		{
			return(true);
		}
		return(false);
	} 

	public boolean isHTMLEmail()
	{
		if(specificcontext!=null)
		{
			if(specificcontext.getString("EmailType").equals("Plain Text"))
			{
				return(false);
			}
		}
		return(true);
	}   

	protected void loadSpecificTokens(String id)
	{
		specificcontext = getBean("email/ContactEmailView",CONTACTID, id);
		/* need to get the 'first (default) group and put it into the specific context */

		// we use the optout method to find a group we can send on
		hasGroupOptOut();

		String defGroupID = Contact.getDefaultGroupID(getConnection(), id);
		if(defGroupID != null)
			specificcontext.setParameter("DefaultGroupID", defGroupID);
		else 
			specificcontext.setParameter("DefaultGroupID", specificcontext.getString("GroupID"));

		loadProfileTokens(id);
		if (specificcontext.getString(COMPANYID).length() > 0) loadProfileTokens(specificcontext.getString(COMPANYID),"Company");
		loadNoteTokens(id); 

		TableData rep = getRepBean();
		specificcontext.put("rep", rep);

		if (rep != null && rep.isSet("Email")) {
			specificcontext.put("RepUserID",rep.getString("UserID"));
			specificcontext.put("RepTitle",rep.getString("Title"));
			specificcontext.put("RepFirstName",rep.getString("FirstName"));
			specificcontext.put("RepLastName",rep.getString("LastName"));
			specificcontext.put("RepCompany",rep.getString("Company"));
			specificcontext.put("RepPosition",rep.getString("Position"));
			specificcontext.put("RepEmail",rep.getString("Email"));
			specificcontext.put("RepStreet",rep.getString("Street"));
			specificcontext.put("RepStreet2",rep.getString("Street2"));
			specificcontext.put("RepCity",rep.getString("City"));
			specificcontext.put("RepState",rep.getString("State"));
			specificcontext.put("RepPostcode",rep.getString("Postcode"));
			specificcontext.put("RepCountry",rep.getString("Country"));
			specificcontext.put("RepPhone",rep.getString("Phone"));
			specificcontext.put("RepFax",rep.getString("Fax"));
			specificcontext.put("RepMobile",rep.getString("Mobile"));
			specificcontext.put("MgrFirstName",rep.getString("MgrFirstName"));
			specificcontext.put("MgrLastName",rep.getString("MgrLastName"));
			specificcontext.put("MgrEmail",rep.getString("MgrEmail"));
			specificcontext.put("RepSupportFirstName",rep.getString("SupportFirstName"));
			specificcontext.put("RepSupportLastName",rep.getString("SupportLastName"));
			specificcontext.put("RepSupportEmail",rep.getString("SupportEmail"));
			specificcontext.put("RepPhotoImage",rep.getString("PhotoImage"));
		} else {
			specificcontext.put("RepUserID","");
		}

		GenRow groupReps = new GenRow(); 
		groupReps.setConnection(getConnection());
		groupReps.setTableSpec("ContactGroups"); 
		groupReps.setParameter("ContactID", id); 
		groupReps.setParameter("RepUserID", "!NULL;!EMPTY");
		groupReps.setParameter("ContactGroupRep.UserID", "!NULL;!EMPTY");
		groupReps.setParameter("-join1", "ContactGroupRep");
		groupReps.setParameter("-join2","ContactGroupGroup");
		groupReps.setParameter("-select0","ContactGroupGroup.Name as GroupName");
		int i = 1; 
		String[] fields = new String[]{"UserID","Title","FirstName","LastName","Company","Position","Email","Street","Street2","City","State","Postcode","Country","Phone","Fax","Mobile"};
		for(String field: fields) {
			groupReps.setParameter("-select" + i++, "ContactGroupRep." + field);
		} 
		groupReps.sortBy("SortOrder",0); 
		groupReps.sortOrder("DESC",0);

		if(logger.isTraceEnabled()) {
			groupReps.doAction(GenerationKeys.SEARCH); 
			logger.trace(groupReps.getStatement());
		}
		try {
			groupReps.getResults(); 
			while(groupReps.getNext()) {
				String suffix = "-" + StringUtil.ToAlphaNumeric(groupReps.getData("GroupName"));
				for(String field: fields) { 
					specificcontext.put(new StringBuilder().append("Rep").append(field).append(suffix).toString(), groupReps.getData(field));
				}
			}
		} finally { 
			groupReps.close(); 
		}

		EmailerSubtype subtype = subtypehead;
		while(subtype!=null)
		{
			subtype.loadSpecificTokens(this, id);
			subtype = subtype.getNext();
		}

		loadAllLinkedSpecificTokens(id);
		loadContactGroupTokens(id, "Contact_");

		if (StringUtils.isNotBlank(specificcontext.getString("RepUserID"))) {
			loadDisplayTokens(specificcontext.getString("RepUserID"));
			loadDisplayLocationDetailTokens(specificcontext.getString("RepUserID"));
	
			loadDisplayLocationAddressTokens(specificcontext.getString("RepUserID"));
		}

		if (sharedcontext.isSet("OpportunityID")) {
			loadOpportunityTokens(sharedcontext.getString("OpportunityID"));
			loadOpportunityLinkedContactTokens(sharedcontext.getString("OpportunityID"));
			loadOpportunityLinkedCompanyTokens(sharedcontext.getString("OpportunityID"));
			loadOpportunityStageTokens(sharedcontext.getString("OpportunityID"),specificcontext.getString("Process.ProcessStageID"));
			loadOpportunityDocuments(sharedcontext.getString("OpportunityID"));
			loadOpportunityCommissionTokens(sharedcontext.getString("OpportunityID"));
		}

		if (sharedcontext.isSet("ProductID")) {
			loadProductTokens(sharedcontext.getString("ProductID"));
			LoadProductAnswers("Product",sharedcontext.getString("ProductID"));
			loadProductDetailsTokens(sharedcontext.getString("ProductID"));
		}
	}

	private void loadOpportunityTokens(String opportunityID) {
		GenRow opportunity = new GenRow();
		opportunity.setViewSpec("OpportunityView");
		opportunity.setParameter("OpportunityID", opportunityID);
		opportunity.doAction("selectfirst");

		if (opportunity.isSuccessful()) {
			ViewSpec vs = opportunity.getViewSpec();

			String table = vs.getMainTableName();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				if("date".equalsIgnoreCase(vs.getLocalItemFormatType(i))) {
					specificcontext.put("Process_" + vs.getLocalItemName(i), opportunity.getData(vs.getLocalItemName(i)));
				} else {
					specificcontext.put("Process_" + vs.getLocalItemName(i), opportunity.getString(vs.getLocalItemName(i)));
				}
				
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("Process_" + vs.getRelatedItemName(i), opportunity.getString(vs.getRelatedItemName(i)));
			}
		}
		
		if (opportunity.isSet("RegionID")) loadLocationTokens(opportunity.getString("RegionID"), "Process_Region_");
		if (opportunity.isSet("LocationID")) loadLocationTokens(opportunity.getString("LocationID"), "Process_Location_");
		

	}
	
	/** Token is going to be like Process_Contact_<Role>_<ViewSpecColumnName>
	 * @param opportunityID
	 */
	private void loadOpportunityLinkedContactTokens(String opportunityID) {
		GenRow opportunity = new GenRow();
		opportunity.setViewSpec("OpportunityContactsListView");
		opportunity.setParameter("OpportunityID", opportunityID);
		opportunity.doAction("search");
		opportunity.getResults(true);

		while (opportunity.getNext()) {
			String role = opportunity.getString("Role").replaceAll(" ", "_");
			ViewSpec vs = opportunity.getViewSpec();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put("Process_Contact_" + role + "_" + vs.getLocalItemName(i), opportunity.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("Process_Contact_" + role + "_" + vs.getRelatedItemName(i), opportunity.getString(vs.getRelatedItemName(i)));
			}
			
			loadContactGroupTokens(opportunity.getString("ContactID"), "Process_Contact_" + role + "_");
		}
		
		opportunity.close();
	}
	
	/** Token is going to be like Process_Commission_<Name>_<ViewSpecColumnName>
	 * @param opportunityID
	 */
	private void loadOpportunityCommissionTokens(String opportunityID) {
		GenRow opportunity = new GenRow();
		opportunity.setViewSpec("CommissionView");
		opportunity.setParameter("OpportunityID", opportunityID);
		opportunity.setParameter("-groupby", "CommissionID");
		opportunity.doAction("search");
		opportunity.getResults();

		GenRow payments = new GenRow();
		payments.setViewSpec("CommissionPaymentsView");
		payments.setConnection(opportunity.getConnection());

		ViewSpec vs1 = opportunity.getViewSpec();
		ViewSpec vs2 = payments.getViewSpec();

		while (opportunity.getNext()) {
			String name = opportunity.getString("Name").replaceAll(" ", "_");
			
			for (int i = 0; i < vs1.getLocalLength(); ++i) {
				addTag("Process_Commission_" + name + "_", vs1.getLocalItemName(i), opportunity.getString(vs1.getLocalItemName(i)));
			}
			for (int i = 0; i < vs1.getRelatedLength(); ++i) {
				addTag("Process_Commission_" + name + "_", vs1.getRelatedItemName(i), opportunity.getString(vs1.getRelatedItemName(i)));
			}

			payments.setParameter("CommissionID", opportunity.getString("CommissionID"));
			payments.sortBy("SortOrder", 0);
			payments.doAction("search");
			payments.getResults();
			
			int c = 1;
			
			while (payments.getNext()) {
				for (int i = 0; i < vs2.getLocalLength(); ++i) {
					if (StringUtils.isNotBlank(payments.getString("Description"))) addTag("Process_Commission_" + name + "_" + payments.getString("Description").replaceAll(" ", "_") + "_", vs2.getLocalItemName(i), payments.getString(vs2.getLocalItemName(i)));
					addTag("Process_Commission_" + name + "_" + c + "_", vs2.getLocalItemName(i), payments.getString(vs2.getLocalItemName(i)));
				}
				for (int i = 0; i < vs2.getRelatedLength(); ++i) {
					if (StringUtils.isNotBlank(payments.getString("Description"))) addTag("Process_Commission_" + name + "_" + payments.getString("Description").replaceAll(" ", "_") + "_", vs2.getRelatedItemName(i), payments.getString(vs2.getRelatedItemName(i)));
					addTag("Process_Commission_" + name + "_" + c + "_", vs2.getRelatedItemName(i), payments.getString(vs2.getRelatedItemName(i)));
				}
				++c;
			}
		}
		
		opportunity.close();
		
		payments.close();
	}
	
	private void loadContactGroupTokens(String contactID, String key) {
		List<String> groupNames = new ArrayList<String>();
		GenRow cg = new GenRow();
		cg.setViewSpec("ContactGroupView");
		cg.setParameter("ContactID", contactID);
		cg.sortBy("SortOrder", 0);
		cg.doAction("search");
		cg.getResults(true);

		ViewSpec vs = cg.getViewSpec();

		if (cg.getNext()) {			
			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put(key + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
				specificcontext.put(key + "Default_" + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put(key + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
				specificcontext.put(key + "Default_" + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
			}
			do {
				groupNames.add(cg.getString("Name"));
				String group = cg.getString("Name").replaceAll(" ", "_");

				for (int i = 0; i < vs.getLocalLength(); ++i) {
					specificcontext.put(key + group + "_" + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
				}
				for (int i = 0; i < vs.getRelatedLength(); ++i) {
					specificcontext.put(key + group + "_" + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
				}
			} while (cg.getNext());
		}
		
		specificcontext.put("GroupNames", StringUtils.join(groupNames, ", "));
		cg.close();

	}

	/** Token is going to be like Process_Company_<Role>_<ViewSpecColumnName>
	 * @param opportunityID
	 */
	private void loadOpportunityLinkedCompanyTokens(String opportunityID) {
		GenRow opportunity = new GenRow();
		opportunity.setViewSpec("OpportunityCompanyListView");
		opportunity.setParameter("OpportunityID", opportunityID);
		opportunity.doAction("search");
		opportunity.getResults(true);

		while (opportunity.getNext()) {
			String role = opportunity.getString("Role").replaceAll(" ", "_");
			ViewSpec vs = opportunity.getViewSpec();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put("Process_Company_" + role + "_" + vs.getLocalItemName(i), opportunity.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("Process_Company_" + role + "_" + vs.getRelatedItemName(i), opportunity.getString(vs.getRelatedItemName(i)));
			}
			
			loadCompanyGroupTokens(opportunity.getString("CompanyID"), "Process_Company_" + role + "_" );
		}
		
		opportunity.close();

	}

	private void loadCompanyGroupTokens(String companyID, String key) {
		GenRow cg = new GenRow();
		cg.setViewSpec("CompanyGroupView");
		cg.setParameter("CompanyID", companyID);
		cg.sortBy("SortOrder", 0);
		cg.doAction("search");
		cg.getResults(true);

		ViewSpec vs = cg.getViewSpec();

		if (cg.getNext()) {
			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put(key + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
				specificcontext.put(key + "Default_" + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put(key + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
				specificcontext.put(key + "Default_" + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
			}
			do {
				String group = cg.getString("Name").replaceAll(" ", "_");

				for (int i = 0; i < vs.getLocalLength(); ++i) {
					specificcontext.put(key + group + "_" + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
				}
				for (int i = 0; i < vs.getRelatedLength(); ++i) {
					specificcontext.put(key + group + "_" + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
				}
			} while (cg.getNext());
		}
		
		cg.close();

	}

	private void loadOpportunityStageTokens(String opportunityID, String processStageID) {
		GenRow opportunity = new GenRow();
		opportunity.setViewSpec("OpportunityStageStageView");
		opportunity.setParameter("OpportunityID", opportunityID);
		opportunity.doAction("search");

		opportunity.getResults();

		ViewSpec vs = opportunity.getViewSpec();

		while (opportunity.getNext()) {

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put("Process_" + opportunity.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_" + vs.getLocalItemName(i), opportunity.getString(vs.getLocalItemName(i)));
				if (opportunity.getString("ProcessStageID").equals(processStageID))
					specificcontext.put("Process_CurrentStage_" + vs.getLocalItemName(i), opportunity.getString(vs.getLocalItemName(i)));

			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("Process_" + opportunity.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_" + vs.getRelatedItemName(i), opportunity.getString(vs.getRelatedItemName(i)));
				if (opportunity.getString("ProcessStageID").equals(processStageID))
					specificcontext.put("Process_CurrentStage_" + vs.getRelatedItemName(i), opportunity.getString(vs.getRelatedItemName(i)));
			}

			LoadOpportunityStageAnswers("Process_" + opportunity.getString("Name").replaceAll("[^a-zA-Z0-9]", ""), opportunityID, opportunity.getString("ProcessStageID"));
			if (opportunity.getString("ProcessStageID").equals(processStageID)) {
				LoadOpportunityStageAnswers("Process_CurrentStage", opportunityID, opportunity.getString("ProcessStageID"));
			}
		}

		LoadOpportunityStageAnswers("Process_AllStages", opportunityID, "NULL;EMPTY");

		opportunity.close();

	}



	private void loadProductDetailsTokens(String productID) {
		String ignorelist = "ProductDetailsID,ProductID,CopiedFromProductID,ProductType,DetailsType,SortOrder";

		GenRow product = new GenRow();
		product.setTableSpec("properties/ProductDetails");
		product.setParameter("-select0","*");
		product.setParameter("ProductID", productID);
		product.doAction("search");

		product.getResults();

		TableSpec ts = product.getTableSpec();

		while (product.getNext()) {

			for (int i = 0; i < ts.getFieldsLength(); ++i) {
				if (ignorelist.indexOf(ts.getFieldName(i)) == -1) specificcontext.put("Product_Details_" + product.getString("DetailsType").replaceAll("[^a-zA-Z0-9]", "") + "_" + ts.getFieldName(i), product.getString(ts.getFieldName(i)));
			}
		}

		product.close();

	}

	private void LoadOpportunityStageAnswers(String key, String opportunityID, String processStageID) {
		GenRow row = new GenRow();
		row.setViewSpec("answers/OpportunityAnswerView");
		row.setParameter("OpportunityID", opportunityID);
		row.setParameter("ProcessStageID", processStageID);
		row.doAction("search");

		row.getResults();

		while (row.getNext()) {
			if (row.isSet("Answer")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_Answer", row.getString("Answer"));
			if (row.isSet("AnswerText")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerText", row.getString("AnswerText"));
			if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerNumber", row.getString("AnswerNumber"));
			if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerCurrency", row.getString("AnswerCurrency"));
			if (row.isSet("AnswerDate")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerDate", row.getString("AnswerDate"));
			if (row.isSet("OtherAnswer")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_OtherAnswer", row.getString("OtherAnswer"));
			if (row.isSet("QuestionShortLabel")) {
				if (row.isSet("Answer")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_Answer", row.getString("Answer"));
				if (row.isSet("AnswerText")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerText", row.getString("AnswerText"));
				if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerNumber", row.getString("AnswerNumber"));
				if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerCurrency", row.getString("AnswerCurrency"));
				if (row.isSet("AnswerDate")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerDate", row.getString("AnswerDate"));
				if (row.isSet("OtherAnswer")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_OtherAnswer", row.getString("OtherAnswer"));
			}
		}
		
		row.close();
	}

	private void loadOpportunityDocuments(String opportunityID) {
		GenRow document = new GenRow();
		document.setViewSpec("LinkedDocumentView");
		document.setParameter("OpportunityID", opportunityID);
		document.sortBy("CreatedDate", 0);
		document.sortOrder("DESC", 0);
		document.doAction("search");

		document.getResults();

		boolean first = true;

		while (document.getNext()) {
			if (document.isSet("Name")) {
				specificcontext.put("Process_Documents_" + document.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Name", document.getString("Name"));
				specificcontext.put("Process_Documents_" + document.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_DocumentID", document.getString("DocumentID"));
				specificcontext.put("Process_Documents_" + document.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_FileName", document.getString("FileName"));
				specificcontext.put("Process_Documents_" + document.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_FilePath", document.getString("FilePath"));
				specificcontext.put("Process_Documents_" + document.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Type", document.getString("DocumentType"));
				specificcontext.put("Process_Documents_" + document.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_SubType", document.getString("DocumentSubType"));
				specificcontext.put("Process_Documents_" + document.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Category", document.getString("DocumentCategory"));
				specificcontext.put("Process_Documents_" + document.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_URL", document.getString("DocumentURL"));
				specificcontext.put("Process_Documents_" + document.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Description", document.getString("Description"));
			} else if (document.isSet("FileName")) {
				specificcontext.put("Process_Documents_" + document.getString("FileName").replaceAll("[^a-zA-Z0-9]", "") + "_Name", document.getString("Name"));
				specificcontext.put("Process_Documents_" + document.getString("FileName").replaceAll("[^a-zA-Z0-9]", "") + "_DocumentID", document.getString("DocumentID"));
				specificcontext.put("Process_Documents_" + document.getString("FileName").replaceAll("[^a-zA-Z0-9]", "") + "_FileName", document.getString("FileName"));
				specificcontext.put("Process_Documents_" + document.getString("FileName").replaceAll("[^a-zA-Z0-9]", "") + "_FilePath", document.getString("FilePath"));
				specificcontext.put("Process_Documents_" + document.getString("FileName").replaceAll("[^a-zA-Z0-9]", "") + "_Type", document.getString("DocumentType"));
				specificcontext.put("Process_Documents_" + document.getString("FileName").replaceAll("[^a-zA-Z0-9]", "") + "_SubType", document.getString("DocumentSubType"));
				specificcontext.put("Process_Documents_" + document.getString("FileName").replaceAll("[^a-zA-Z0-9]", "") + "_Category", document.getString("DocumentCategory"));
				specificcontext.put("Process_Documents_" + document.getString("FileName").replaceAll("[^a-zA-Z0-9]", "") + "_URL", document.getString("DocumentURL"));
				specificcontext.put("Process_Documents_" + document.getString("FileName").replaceAll("[^a-zA-Z0-9]", "") + "_Description", document.getString("Description"));
			}
			if (first) {
				specificcontext.put("Process_Documents_CurrentDocument_Name", document.getString("Name"));
				specificcontext.put("Process_Documents_CurrentDocument_DocumentID", document.getString("DocumentID"));
				specificcontext.put("Process_Documents_CurrentDocument_FileName", document.getString("FileName"));
				specificcontext.put("Process_Documents_CurrentDocument_FilePath", document.getString("FilePath"));
				specificcontext.put("Process_Documents_CurrentDocument_Type", document.getString("DocumentType"));
				specificcontext.put("Process_Documents_CurrentDocument_SubType", document.getString("DocumentSubType"));
				specificcontext.put("Process_Documents_CurrentDocument_Category", document.getString("DocumentCategory"));
				specificcontext.put("Process_Documents_CurrentDocument_URL", document.getString("DocumentURL"));
				specificcontext.put("Process_Documents_CurrentDocument_Description", document.getString("Description"));

				first = false;
			}
		}

		document.close();

	}

	private void loadProductTokens(String productID) {
		GenRow find = new GenRow();
		find.setTableSpec("Products");
		find.setParameter("-select0", "ProductType");
		find.setParameter("ProductID", productID);
		find.doAction("selectfirst");

		GenRow product = new GenRow();
		product.setViewSpec("ProductView");
		product.setParameter("ProductID", productID);
		product.setParameter("ProductType", find.getString("ProductType"));


		if ("House and Land".equals(find.getString("ProductType"))) {
			product.setViewSpec("properties/ProductHouseAndLandView");
		} else if ("Land".equals(find.getString("ProductType"))) {
			product.setViewSpec("properties/ProductLotView");
		} else if ("Home Plan".equals(find.getString("ProductType"))) {
			product.setViewSpec("properties/ProductPlanView");
		} else if ("Apartment".equals(find.getString("ProductType"))) {
			product.setViewSpec("properties/ProductApartmentView");
		} else if ("Existing Property".equals(find.getString("ProductType"))) {
			product.setViewSpec("properties/ProductExistingPropertyView");
		}

		product.doAction("selectfirst");

		if (product.isSuccessful()) {
			ViewSpec vs = product.getViewSpec();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put("Product_" + vs.getLocalItemName(i), product.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("Product_" + vs.getRelatedItemName(i), product.getString(vs.getRelatedItemName(i)));
			}
		}
		
		GenRow quickIndex = new GenRow();
		quickIndex.setViewSpec("{properties/}ProductQuickIndexView");
		quickIndex.setParameter("ProductID", productID);
		quickIndex.doAction("selectfirst");
		
		if (quickIndex.isSuccessful()) {
			ViewSpec vs = quickIndex.getViewSpec();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put("ProductQuick_" + vs.getLocalItemName(i), quickIndex.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("ProductQuick_" + vs.getRelatedItemName(i), quickIndex.getString(vs.getRelatedItemName(i)));
			}
			
			String fullName = "";
			if ("Land".equals(quickIndex.getString("ProductType"))) {
				if (!quickIndex.getString("Name").startsWith("Lot") && !quickIndex.getString("Name").startsWith("Block")) fullName = "Lot ";
				fullName += quickIndex.getString("Name") + " " + quickIndex.getString("StageName") + " " + quickIndex.getString("EstateName"); 
			} else if ("Home Plan".equals(quickIndex.getString("ProductType"))) {
				if (!quickIndex.getString("Name").startsWith("Lot") && !quickIndex.getString("Name").startsWith("Block")) fullName = "Lot ";
				fullName += quickIndex.getString("DesignName") + " " + quickIndex.getString("Name") + ", " + quickIndex.getString("RangeName"); 
			} else if ("Apartment".equals(quickIndex.getString("ProductType"))) {
				if (!quickIndex.getString("Name").startsWith("Apartment")) fullName = "Apartment ";
				fullName += quickIndex.getString("Name") + " Level " + quickIndex.getString("StageName") + ", " + quickIndex.getString("EstateName"); 
			} else {
				fullName = quickIndex.getString("Name");
			}

			specificcontext.put("ProductQuick_FullName", fullName);
		}
		
		if (product.isSet("RegionID")) loadLocationTokens(product.getString("RegionID"), "Product_Region_");
		if (product.isSet("LocationID")) loadLocationTokens(product.getString("LocationID"), "Product_Location_");
		
		GenRow addressRow = new GenRow();
		addressRow.setConnection(getConnection());
		addressRow.setTableSpec("Addresses");
		addressRow.setColumn("ProductID", productID);
		addressRow.setParameter("-select1", "AddressID");
		addressRow.setParameter("-select2", "ProductID");
		addressRow.setParameter("-select3", "LocationID");
		addressRow.setParameter("-select4", "GroupID");
		addressRow.setParameter("-select5", "StreetNumber");
		addressRow.setParameter("-select6", "Street");
		addressRow.setParameter("-select7", "Street2");
		addressRow.setParameter("-select8", "City");
		addressRow.setParameter("-select9", "State");
		addressRow.setParameter("-select10", "Postcode");
		addressRow.setParameter("-select11", "Region");
		addressRow.setParameter("-select12", "Country");
		addressRow.setParameter("-select13", "UseMap");
		addressRow.setParameter("-select14", "Estate");
		addressRow.setParameter("-select15", "Latitude");
		addressRow.setParameter("-select16", "Longitude");
		addressRow.setParameter("-select17", "MapRef");

		if(addressRow.isSet("ProductID")) { 
			addressRow.doAction(GenerationKeys.SELECTFIRST);
		} 
		
		String street1 = addressRow.getString("Street");
		String street2 = addressRow.getString("Street2");
		String city = addressRow.getString("City");
		String state = addressRow.getString("State");
		String postcode = addressRow.getString("Postcode");
		String region = addressRow.getString("Region");
		String country = addressRow.getString("Country");
		String mapRef = addressRow.getString("MapRef");

		String fullAddress = ""; 
		if (!StringUtil.isBlankOrEmpty(street1))
		{
			fullAddress += street1;
		}

		if (!StringUtil.isBlankOrEmpty(street2))
		{
			fullAddress += " " + street2;
		}

		if (!StringUtil.isBlankOrEmpty(city))
		{
			if (StringUtil.isBlankOrEmpty(fullAddress))
			{
				fullAddress += city;
			}
			else
			{	
				fullAddress += ", " + city;
			}
		}

		if (!StringUtil.isBlankOrEmpty(state))
		{
			fullAddress += ", " + state;
		}

		if (!StringUtil.isBlankOrEmpty(postcode))
		{
			fullAddress += " " + postcode;
		}

		specificcontext.put("Product_Street", street1);
		specificcontext.put("Product_Street2", street2);
		specificcontext.put("Product_City", city);
		specificcontext.put("Product_Postcode", postcode);
		specificcontext.put("Product_State", state);
		specificcontext.put("Product_Country", country);

		specificcontext.put("Product_FullAddress", fullAddress);
	}
	
	private void loadLocationTokens(String locationID, String key) {
		GenRow row = new GenRow();
		row.setViewSpec("DisplayEntityView");
		row.setParameter("DisplayEntityID", locationID);
		row.doAction("selectfirst");
		
		specificcontext.put(key + "Name", row.getString("Name"));
		specificcontext.put(key + "Type", row.getString("Name"));
		specificcontext.put(key + "GroupName", row.getString("GroupName"));
		specificcontext.put(key + "UniqueID", row.getString("ProductNumber"));
		specificcontext.put(key + "Phone", row.getString("Phone"));
		specificcontext.put(key + "Email", row.getString("Email"));
		specificcontext.put(key + "Web", row.getString("Web"));
		specificcontext.put(key + "DisplayHours", row.getString("DisplayHours"));

		String street1 = row.getString("Street");
		String street2 = row.getString("Street2");
		String city = row.getString("City");
		String state = row.getString("State");
		String postcode = row.getString("Postcode");
		String region = row.getString("Region");
		String country = row.getString("Country");
		String mapRef = row.getString("MapRef");

		String fullAddress = ""; 
		if (!StringUtil.isBlankOrEmpty(street1))
		{
			fullAddress += street1;
		}

		if (!StringUtil.isBlankOrEmpty(street2))
		{
			fullAddress += " " + street2;
		}

		if (!StringUtil.isBlankOrEmpty(city))
		{
			if (StringUtil.isBlankOrEmpty(fullAddress))
			{
				fullAddress += city;
			}
			else
			{	
				fullAddress += ", " + city;
			}
		}

		if (!StringUtil.isBlankOrEmpty(state))
		{
			fullAddress += ", " + state;
		}

		if (!StringUtil.isBlankOrEmpty(postcode))
		{
			fullAddress += " " + postcode;
		}

		specificcontext.put(key + "Street", street1);
		specificcontext.put(key + "Street2", street2);
		specificcontext.put(key + "City", city);
		specificcontext.put(key + "Postcode", postcode);
		specificcontext.put(key + "State", state);
		specificcontext.put(key + "Country", country);

		specificcontext.put(key + "FullAddress", fullAddress);
		
		GenRow companyLinkedContacts = new GenRow();
		companyLinkedContacts.setViewSpec("CompanyContactLinkView");
		companyLinkedContacts.setConnection(row.getConnection());
		companyLinkedContacts.setParameter("CompanyID", row.getString("ProductID"));
		companyLinkedContacts.sortBy("RepAllocation", 0);
		if (companyLinkedContacts.isSet("CompanyID")) {
			companyLinkedContacts.doAction("search");
			
			companyLinkedContacts.getResults();
			
			StringBuilder emails = new StringBuilder();
			
			int c = 1;
			
			while (companyLinkedContacts.getNext()) {

				specificcontext.put(key + "Staff_" + c + "_FirstName", companyLinkedContacts.getString("FirstName"));
				specificcontext.put(key + "Staff_" + c + "_LastName", companyLinkedContacts.getString("LastName"));
				specificcontext.put(key + "Staff_" + c + "_Phone", companyLinkedContacts.getString("Phone"));
				specificcontext.put(key + "Staff_" + c + "_Mobile", companyLinkedContacts.getString("Mobile"));
				specificcontext.put(key + "Staff_" + c + "_Email", companyLinkedContacts.getString("Email"));
				
				if ("Y".equals(companyLinkedContacts.getString("ToFlag")) || "Y".equals(companyLinkedContacts.getString("CCFlag")) || "Y".equals(companyLinkedContacts.getString("BCCFlag"))) {
					specificcontext.put(key + "Staff_" + c + "_DistributeTo", "Y");
					if (companyLinkedContacts.isSet("Email")) {
						if (emails.length() > 0) emails.append(", ");
						emails.append(companyLinkedContacts.getString("Email"));
					}
				}
				
			}
			
			specificcontext.put(key + "RepEmails", emails.toString());
		}
		
		companyLinkedContacts.close();
		row.close();
	}

	private void LoadProductAnswers(String key, String productID) {
		GenRow row = new GenRow();
		row.setViewSpec("answers/ProductAnswerView");
		row.setParameter("ProductID", productID);
		row.doAction("search");

		row.getResults();

		while (row.getNext()) {
			if (row.isSet("Answer")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_Answer", row.getString("Answer"));
			if (row.isSet("AnswerText")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerText", row.getString("AnswerText"));
			if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerNumber", row.getString("AnswerNumber"));
			if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerCurrency", row.getString("AnswerCurrency"));
			if (row.isSet("AnswerDate")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerDate", row.getString("AnswerDate"));
			if (row.isSet("OtherAnswer")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_OtherAnswer", row.getString("OtherAnswer"));
			if (row.isSet("QuestionShortLabel")) {
				if (row.isSet("Answer")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_Answer", row.getString("Answer"));
				if (row.isSet("AnswerText")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerText", row.getString("AnswerText"));
				if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerNumber", row.getString("AnswerNumber"));
				if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerCurrency", row.getString("AnswerCurrency"));
				if (row.isSet("AnswerDate")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerDate", row.getString("AnswerDate"));
				if (row.isSet("OtherAnswer")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_OtherAnswer", row.getString("OtherAnswer"));
			}
		}
		
		row.close();
	}

	private void loadDisplayTokens(String repUserID) {
		GenRow contactLocations = new GenRow();

		contactLocations.setTableSpec("ContactLocations");
		contactLocations.setColumn("UserID", repUserID);
		contactLocations.setConnection(getConnection());

		contactLocations.setParameter("-join0", "ContactLocationLocation.DisplayEntitiesLinkedDocuments.LinkedDocumentImage");		
		contactLocations.setParameter("-select0", "LinkedDocumentImage.FilePath as FilePath");
		//contactLocations.setParameter("-select1", "ContactLocationLocation.Name as DisplayCentreName");
		contactLocations.setParameter("DisplayEntitiesLinkedDocuments.ShowOnEmail", "Y");

		/*contactLocations.setParameter("-join1", "ContactLocationLocation.DisplayEntitiesLinkedDocuments.DisplayEntityID");		
		contactLocations.setParameter("-select1", "DisplayEntities.Name as DisplayCentreName");*/
		if(contactLocations.isSet("UserID")) { 
			contactLocations.doAction(GenerationKeys.SELECTFIRST);
		} 
		//System.out.println("loadDisplayTokens query :-" + contactLocations.getStatement());

		if (contactLocations.isSuccessful()) {
			// specificcontext.put("RepMudMap",contactLocations.getString("FilePath"));
			String tmpMupMapPath = contactLocations.getString("FilePath");
			specificcontext.put("RepMudMap", tmpMupMapPath.replaceAll(" ", "%20"));
			//System.out.println("loadDisplayTokens query :-" + tmpMupMapPath);

			//specificcontext.put("RepDisplayCentreName",contactLocations.getString("DisplayCentreName"));
			//System.out.println("RepDisplayCentreName query :-" + contactLocations.getString("DisplayCentreName"));
		}
	}

	private void loadDisplayLocationDetailTokens(String repUserID) {
		GenRow contactLocations = new GenRow();
		contactLocations.setConnection(getConnection());
		contactLocations.setTableSpec("ContactLocations");
		contactLocations.setColumn("UserID", repUserID);

		contactLocations.setParameter("-select0", "ContactLocationLocation.Name as DisplayCentreName");

		if(contactLocations.isSet("UserID")) { 
			contactLocations.doAction(GenerationKeys.SELECTFIRST);
		} 
		//System.out.println("loadDisplayLocationDetailTokens query :-" + contactLocations.getStatement());
		if (contactLocations.isSuccessful()) {
			specificcontext.put("RepDisplayCentreName",contactLocations.getString("DisplayCentreName"));
		}
	}


	private void loadDisplayLocationAddressTokens(String repUserID) {

		try
		{
			GenRow contactLocations = new GenRow();
			contactLocations.setConnection(getConnection());
			contactLocations.setTableSpec("ContactLocations");
			contactLocations.setColumn("UserID", repUserID);
			contactLocations.setParameter("-select0", "RegionID");
			contactLocations.setParameter("-select1", "LocationID");
			contactLocations.doAction(GenerationKeys.SELECTFIRST);

			String regionID = contactLocations.getString("RegionID");
			String locationID = contactLocations.getString("LocationID");


			//System.out.println("contactLocations query :-" + contactLocations.getStatement());

			GenRow displayRow = new GenRow();
			displayRow.setConnection(getConnection());
			displayRow.setTableSpec("DisplayEntities");
			displayRow.setColumn("DisplayEntityID", locationID);
			if(displayRow.isSet("DisplayEntityID")) { 
				displayRow.setParameter("-select0", "ProductID");
				displayRow.doAction(GenerationKeys.SELECTFIRST);
			}
			String ProductID = displayRow.getString("ProductID");

			//System.out.println("displayRow query :-" + displayRow.getStatement());

			GenRow addressRow = new GenRow();
			addressRow.setConnection(getConnection());
			addressRow.setTableSpec("Addresses");
			addressRow.setColumn("ProductID", ProductID);
			addressRow.setParameter("-select1", "AddressID");
			addressRow.setParameter("-select2", "ProductID");
			addressRow.setParameter("-select3", "LocationID");
			addressRow.setParameter("-select4", "GroupID");
			addressRow.setParameter("-select5", "StreetNumber");
			addressRow.setParameter("-select6", "Street");
			addressRow.setParameter("-select7", "Street2");
			addressRow.setParameter("-select8", "City");
			addressRow.setParameter("-select9", "State");
			addressRow.setParameter("-select10", "Postcode");
			addressRow.setParameter("-select11", "Region");
			addressRow.setParameter("-select12", "Country");
			addressRow.setParameter("-select13", "UseMap");
			addressRow.setParameter("-select14", "Estate");
			addressRow.setParameter("-select15", "Latitude");
			addressRow.setParameter("-select16", "Longitude");
			addressRow.setParameter("-select17", "MapRef");

			if(addressRow.isSet("ProductID")) { 
				addressRow.doAction(GenerationKeys.SELECTFIRST);
			} 

			if (StringUtil.isBlankOrEmpty(addressRow.getString("Street")))
			{
				displayRow = new GenRow();
				displayRow.setConnection(getConnection());
				displayRow.setTableSpec("DisplayEntities");
				displayRow.setColumn("DisplayEntityID", regionID);
				displayRow.setParameter("-select0", "ProductID");

				if(displayRow.isSet("DisplayEntityID")) { 
					displayRow.doAction(GenerationKeys.SELECTFIRST);			   	
					ProductID = displayRow.getString("ProductID");
					//System.out.println("displayRow query :-" + displayRow.getStatement());

					addressRow = new GenRow();
					addressRow.setConnection(getConnection());
					addressRow.setTableSpec("Addresses");
					addressRow.setColumn("ProductID", ProductID);
					addressRow.setParameter("-select1", "AddressID");
					addressRow.setParameter("-select2", "ProductID");
					addressRow.setParameter("-select3", "LocationID");
					addressRow.setParameter("-select4", "GroupID");
					addressRow.setParameter("-select5", "StreetNumber");
					addressRow.setParameter("-select6", "Street");
					addressRow.setParameter("-select7", "Street2");
					addressRow.setParameter("-select8", "City");
					addressRow.setParameter("-select9", "State");
					addressRow.setParameter("-select10", "Postcode");
					addressRow.setParameter("-select11", "Region");
					addressRow.setParameter("-select12", "Country");
					addressRow.setParameter("-select13", "UseMap");
					addressRow.setParameter("-select14", "Estate");
					addressRow.setParameter("-select15", "Latitude");
					addressRow.setParameter("-select16", "Longitude");
					addressRow.setParameter("-select17", "MapRef");

					if(addressRow.isSet("ProductID")) { 
						addressRow.doAction(GenerationKeys.SELECTFIRST);
					} 
				}
			}	

			//System.out.println("addressRow query :-" + addressRow.getStatement());

			String street1 = addressRow.getString("Street");
			String street2 = addressRow.getString("Street2");
			String city = addressRow.getString("City");
			String state = addressRow.getString("State");
			String postcode = addressRow.getString("Postcode");
			String region = addressRow.getString("Region");
			String country = addressRow.getString("Country");
			String mapRef = addressRow.getString("MapRef");

			String fullAddress = ""; 
			if (!StringUtil.isBlankOrEmpty(street1))
			{
				fullAddress += street1;
			}

			if (!StringUtil.isBlankOrEmpty(street2))
			{
				fullAddress += " " + street2;
			}

			if (!StringUtil.isBlankOrEmpty(city))
			{
				if (StringUtil.isBlankOrEmpty(fullAddress))
				{
					fullAddress += city;
				}
				else
				{	
					fullAddress += ", " + city;
				}
			}

			if (!StringUtil.isBlankOrEmpty(state))
			{
				fullAddress += ", " + state;
			}

			if (!StringUtil.isBlankOrEmpty(postcode))
			{
				fullAddress += " " + postcode;
			}

			specificcontext.put("DisplayCentreStreet", street1);
			specificcontext.put("DisplayCentreStreet2", street2);
			specificcontext.put("DisplayCentreCity", city);
			specificcontext.put("DisplayCentrePostcode", postcode);
			specificcontext.put("DisplayCentreState", state);
			specificcontext.put("DisplayCentreCountry", country);

			specificcontext.put("RepDisplayCentreFullAddress", fullAddress);

		}
		catch(Exception ee)
		{
			System.out.println("Exception occurred. Message is:" + ee.getMessage());
			ee.printStackTrace();
		}


		/*
	   GenRow contactLocations = new GenRow();
		contactLocations.setConnection(con);
		contactLocations.setTableSpec("ContactLocations");
		contactLocations.setColumn("UserID", repUserID);

		contactLocations.setParameter("-select0", "ContactLocationAddress.Street as DisplayCentreStreet");
		contactLocations.setParameter("-select1", "ContactLocationAddress.Street2 as DisplayCentreStreet2");
		contactLocations.setParameter("-select2", "ContactLocationAddress.City as DisplayCentreCity");
		contactLocations.setParameter("-select3", "ContactLocationAddress.Postcode as DisplayCentrePostcode");
		contactLocations.setParameter("-select4", "ContactLocationAddress.State as DisplayCentreState");
		contactLocations.setParameter("-select5", "ContactLocationAddress.Country as DisplayCentreCountry");

		contactLocations.doAction("search");
		contactLocations.getResults();

		System.out.println("loadDisplayLocationAddressTokens query :-" + contactLocations.getStatement());

		if (contactLocations.getNext()) {

			String fullAddress = ""; 
			if (!StringUtil.isBlankOrEmpty(contactLocations.getString("DisplayCentreStreet")))
			{
				fullAddress += contactLocations.getString("DisplayCentreStreet");
			}

			if (!StringUtil.isBlankOrEmpty(contactLocations.getString("DisplayCentreStreet2")))
			{
				fullAddress += " " + contactLocations.getString("DisplayCentreStreet2");
			}

			if (!StringUtil.isBlankOrEmpty(contactLocations.getString("DisplayCentreCity")))
			{
				fullAddress += ", " + contactLocations.getString("DisplayCentreCity");
			}

			if (!StringUtil.isBlankOrEmpty(contactLocations.getString("DisplayCentreState")))
			{
				fullAddress += ", " + contactLocations.getString("DisplayCentreState");
			}

			if (!StringUtil.isBlankOrEmpty(contactLocations.getString("DisplayCentrePostcode")))
			{
				fullAddress += " " + contactLocations.getString("DisplayCentrePostcode");
			}

			specificcontext.put("DisplayCentreStreet", contactLocations.getString("DisplayCentreStreet"));
			specificcontext.put("DisplayCentreStreet2", contactLocations.getString("DisplayCentreStreet2"));
			specificcontext.put("DisplayCentreCity", contactLocations.getString("DisplayCentreCity"));
			specificcontext.put("DisplayCentrePostcode", contactLocations.getString("DisplayCentrePostcode"));
			specificcontext.put("DisplayCentreState", contactLocations.getString("DisplayCentreState"));
			specificcontext.put("DisplayCentreCountry", contactLocations.getString("DisplayCentreCountry"));

			specificcontext.put("RepDisplayCentreFullAddress", fullAddress);
		}*/

	}


	protected void loadAllLinkedSpecificTokens(String id) {
		loadLinkedSpecificTokens(id);

		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("LinkeeContactListView");
		row.setParameter("LinkerID", id);
		row.setParameter("LinkedIn", "Y");
		if (row.isSet("LinkerID")) {
			row.doAction("search");
			row.getResults();
		}

		while (row.getNext()) {
			if (row.getString("LinkeeID").length() > 0) { 
				loadLinkedSpecificTokens(row.getString("LinkeeID"));
				loadLinkedContactTokens(row.getString("LinkeeID"), row.getString("LinkType"));
			}
		}
		row.close();

	}

	private String[]		tokens = {"Title","FirstName","LastName","Salutation","Company"};
	protected void loadLinkedSpecificTokens(String id)
	{
		TableData linkedcontext = getBean("email/ContactEmailLinkedView",CONTACTID, id);

		for (int t = 0; t < tokens.length; ++t) {
			if (specificcontext.get("Linked" + tokens[t]) == null || ((String) specificcontext.get("Linked" + tokens[t])).length() == 0) {
				specificcontext.put("Linked" + tokens[t], linkedcontext.get(tokens[t]));
			} else {
				String value = (String) specificcontext.get("Linked" + tokens[t]);
				value = value.replaceAll(" and ",", ") + " and " + linkedcontext.get(tokens[t]);
				if (value.endsWith(" and ")) value = value.replaceAll(" and ", "");
				specificcontext.put("Linked" + tokens[t], value);
			}
		}

		if (specificcontext.get("LinkedNames") == null || ((String) specificcontext.get("LinkedNames")).length() == 0) {
			specificcontext.put("LinkedNames", (linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim());
		} else {
			String value = (String) specificcontext.get("LinkedNames");
			value = value.replaceAll(" and ",", ") + " and " + (linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim();
			if (value.endsWith(" and ")) value = value.replaceAll(" and ", "");
			specificcontext.put("LinkedNames", value);
		}

		if (specificcontext.get("LinkedFormalNames") == null || ((String) specificcontext.get("LinkedFormalNames")).length() == 0) {
			specificcontext.put("LinkedFormalNames", (linkedcontext.get("Title") + " " + linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim());
		} else {
			String value = (String) specificcontext.get("LinkedFormalNames");
			value = value.replaceAll(" and ",", ") + " and " + (linkedcontext.get("Title") + " " + linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim();
			if (value.endsWith(" and ")) value = value.replaceAll(" and ", "");
			specificcontext.put("LinkedFormalNames", value);
		}
	}
	
	/** Token is going to be like Linked_Contact_<LinkType>_<ViewSpecColumnName>
	 * @param opportunityID
	 */
	private void loadLinkedContactTokens(String contactID, String linkType) {
		GenRow contactRow = new GenRow();
		contactRow.setViewSpec("ContactListView");
		contactRow.setParameter("ContactID", contactID);
		contactRow.doAction("search");
		contactRow.getResults(true);

		String prefix = "Linked_Contact_" + linkType.replaceAll(" ", "_") + "_";

		while (contactRow.getNext()) {
			ViewSpec vs = contactRow.getViewSpec();
			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put(prefix + vs.getLocalItemName(i), contactRow.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put(prefix + vs.getRelatedItemName(i), contactRow.getString(vs.getRelatedItemName(i)));
			}

			loadContactGroupTokens(contactRow.getString("ContactID"), prefix);
		}

		contactRow.close();
	}

	public void addTag(String prefix, String field, String value) {
		if (StringUtils.isNotBlank(field) && StringUtils.isNotBlank(value)) {
			specificcontext.put(prefix + field, value);

			if (value.trim().length() == value.trim().replaceAll("[^0-9\\$\\.\\-\\,]", "").length()) {
				try {
					double d = Double.parseDouble(value.replaceAll("[^0-9\\.\\-]", ""));

					specificcontext.put(prefix + field + "_CurrencyFull", currancyFull.format(d));

					currancyShort.setMaximumFractionDigits(0);
					specificcontext.put(prefix + field + "_CurrencyShort", currancyShort.format(StringUtil.getRoundedValue(d)));
					
					specificcontext.put(prefix + field + "_Int", String.valueOf(Math.round(StringUtil.getRoundedValue(d))));

					String srt = String.valueOf(d);
					if (srt.endsWith(".0") || srt.endsWith(".00")) srt = srt.substring(0, srt.indexOf("."));
					specificcontext.put(prefix + field + "_Short", srt);
				} catch (Exception e) {}
			}
		}
	}

	protected boolean repActive() 
	{
		if(specificcontext.getObject("rep")!=null) { 
			return "Active".equals(((TableData)specificcontext.getObject("rep")).getString("Status"));
		}
		return false;
	}

	protected String getGroupRep(String groupID) { 
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("ContactGroupView");
		row.setParameter("GroupID", groupID);
		row.setParameter(CONTACTID, specificcontext.getString(CONTACTID));
		row.setParameter("-sort1","SortOrder");

		if (requestrow != null && requestrow.getString("CURRENTUSERID").length() > 0) row.setParameter("RepUserID", requestrow.getString("CURRENTUSERID"));
		row.doAction("selectfirst");
		if(logger.isTraceEnabled()) {
			logger.trace("getGroupRep({}) statement: {}", row.getStatement());
		}
		if (!row.isSuccessful() || row.getString("ContactGroupID").length() == 0) {
			row.remove("RepUserID");
			row.doAction("selectfirst");
		}

		if(row.isSuccessful()) 
			return row.getData("RepUserID");
		return null;
	}

	/**
	 * Checks for appropriate rep/group to use for this specific context 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected TableData getRepBean() {
		if ("Appointment".equals(sharedcontext.getString("Type")) && specificcontext.isSet("GroupID") && sharedcontext.isSet("CurrentUserID")) {
			GenRow groups = new GenRow();
			groups.setConnection(getConnection());
			groups.setTableSpec("ContactGroups");
			groups.setParameter("-select","*");
			groups.setParameter("ContactID",specificcontext.getString("ContactID"));
			groups.setParameter("GroupID",specificcontext.getString("GroupID"));
			groups.setParameter("RepUserID", sharedcontext.getString("CurrentUserID"));
			groups.sortBy("SortOrder", 0);
			groups.doAction("selectfirst");

			if (!groups.isSuccessful() || groups.getString("ContactGroupID").length() == 0) {
				groups.setParameter("RepUserID", sharedcontext.getString("RepUserID"));
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
			}
			
			String repUserID = groups.getData("RepUserID");
			if (StringUtils.isBlank(repUserID)) repUserID = sharedcontext.getString("RepUserID");
			if (StringUtils.isBlank(repUserID)) repUserID = sharedcontext.getString("CurrentUserID");
			
			if (StringUtils.isBlank(repUserID)) {
				groups.remove("RepUserID");
				groups.setParameter("RepUserID", "!NULL;!EMPTY");
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
				
				repUserID = groups.getData("RepUserID");
			}

			return getUserBean(repUserID);
		// we need to handle campaign sends differently
		} else if (sharedcontext.isSet("CampaignID") && sharedcontext.isSet("EventID")) {
			GenRow row = new GenRow();
			row.setConnection(getConnection());
			row.setViewSpec("EventView");
			row.setParameter("EventID", sharedcontext.getString("EventID"));
			row.doAction("selectfirst");

			String repOpt = row.getString("RepSpecific");
			String repUserID = "";
			if ("A".equals(repOpt) || "I".equals(repOpt)) {
				GenRow groups = new GenRow();
				groups.setConnection(getConnection());
				groups.setTableSpec("LinkedCampaigns");
				groups.setParameter("-select","*");
				groups.setParameter("CampaignID",sharedcontext.getString("CampaignID"));
				groups.setParameter("LinkedGroupID",specificcontext.getString("GroupID"));
				groups.sortBy("SortOrder", 0);
				groups.doAction("selectfirst");
				repUserID = groups.getString("RepUserID");
				if ("A".equals(repOpt) && groups.isSet("RepUserID")) return getUserBean(repUserID);   
			}

			GenRow groups = new GenRow();
			groups.setConnection(getConnection());
			groups.setTableSpec("ContactGroups");
			groups.setParameter("-select","*");
			groups.setParameter("ContactID",specificcontext.getString("ContactID"));
			groups.setParameter("GroupID",specificcontext.getString("GroupID"));
			if (requestrow != null && requestrow.getString("CURRENTUSERID").length() > 0) groups.setParameter("RepUserID", requestrow.getString("CURRENTUSERID"));
			groups.sortBy("SortOrder", 0);
			groups.doAction("selectfirst");

			if (!groups.isSuccessful() || groups.getString("ContactGroupID").length() == 0) {
				groups.remove("RepUserID");
				groups.setParameter("RepUserID", "!NULL;!EMPTY");
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
			}

			if ("I".equals(repOpt)) {
				// if no rep in the group use the one from campaign
				if (!groups.isSet("RepUserID")) return getUserBean(repUserID);
				TableData rep = getUserBean(groups.getData("RepUserID"));
				// if the user is invalid the use the one from the campaign 
				if (!rep.isSet("Email")) return getUserBean(repUserID);
				// if user is inactive use the one from campaigns
				if (rep.isSet("Status") && !"Active".equals(rep.getString("Status"))) return getUserBean(repUserID);
				return rep;
			}

			return getUserBean(groups.getData("RepUserID"));
		} else if (specificcontext.isSet("GroupID") && sharedcontext.isSet("CurrentUserID")) {
			// by now we know the group we are sending this too, so lets just get the rep
			GenRow groups = new GenRow();
			groups.setConnection(getConnection());
			groups.setTableSpec("ContactGroups");
			groups.setParameter("-select","*");
			groups.setParameter("ContactID",specificcontext.getString("ContactID"));
			groups.setParameter("GroupID",specificcontext.getString("GroupID"));
			groups.setParameter("RepUserID", sharedcontext.getString("CurrentUserID"));
			groups.sortBy("SortOrder", 0);
			groups.doAction("selectfirst");

			if (!groups.isSuccessful() || groups.getString("ContactGroupID").length() == 0) {
				groups.setParameter("RepUserID", sharedcontext.getString("RepUserID"));
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
			}

			if (!groups.isSuccessful() || groups.getString("ContactGroupID").length() == 0) {
				groups.remove("RepUserID");
				groups.setParameter("RepUserID", "!NULL;!EMPTY");
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
			}

			return getUserBean(groups.getData("RepUserID"));
		} else if (specificcontext.isSet("GroupID") && !specificcontext.isSet("RepUserID")) {
			// by now we know the group we are sending this too, so lets just get the rep
			GenRow groups = new GenRow();
			groups.setConnection(getConnection());
			groups.setTableSpec("ContactGroups");
			groups.setParameter("-select","*");
			groups.setParameter("ContactID",specificcontext.getString("ContactID"));
			groups.setParameter("GroupID",specificcontext.getString("GroupID"));
			if (requestrow != null && requestrow.getString("CURRENTUSERID").length() > 0) groups.setParameter("RepUserID", requestrow.getString("CURRENTUSERID"));
			groups.sortBy("SortOrder", 0);
			groups.doAction("selectfirst");

			if (!groups.isSuccessful() || groups.getString("ContactGroupID").length() == 0) {
				groups.remove("RepUserID");
				groups.setParameter("RepUserID", "!NULL;!EMPTY");
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
			}

			return getUserBean(groups.getData("RepUserID"));
		} else {
			String repUserID = null; 
			String[] groupIDs = null;

			Object o = sharedcontext.getObject("GroupIDs");
			Map<String, String> groupMap = null; 

			if(o != null && o instanceof Map<?, ?>) { 
				groupMap =  (Map<String, String>)o;
			}
			/* campaigns will have a set of groups assigned */
			if(groupMap != null) { 
				if(specificcontext.isSet("DefaultGroupID")) { 
					if(groupMap.containsKey(specificcontext.getParameter("DefaultGroupID")))
					{
						specificcontext.setParameter("GroupID",specificcontext.getParameter("DefaultGroupID"));
						repUserID = groupMap.get(specificcontext.getParameter("DefaultGroupID"));
						if(repUserID != null && repUserID.length()==0) { 
							repUserID = null; // - noticed this was changed in 1.19 getGroupRep(specificcontext.getParameter("DefaultGroupID")); 
						}
					}
				}
				if(repUserID == null) {
					/* returns the rep from the contact group that matches any of the context groups, in order */
					for(String gID: groupMap.keySet()) 
					{
						if((repUserID = getGroupRep(gID)) != null) { 
							/* found a contact group rep */
							String tempRep = groupMap.get(gID);
							/* does the group map have an override rep */
							if(tempRep != null && tempRep.length()>0) {
								repUserID = tempRep;
							}
							specificcontext.setParameter("GroupID", gID);
							break;
						}
					}
				}
			}
			/* check for groupIDs in the shared context (for ex set by campaigns) and then the request row */
			if((groupIDs = requestrow.getParameterValues("GroupIDs")) != null) { 

				/* returns the rep from the *DEFAULT* contact group 
				 * 	that matches the request group */
				if(specificcontext.isSet("DefaultGroupID")) { 
					for(String gID: groupIDs) 
					{				  
						/* this groupid needs to be the 'first' group in the list, not the group at the contact level 
						 * this is getColumn so that it will check parameters first and THEN the data */
						if(gID.equals(specificcontext.getParameter("DefaultGroupID"))) { 
							specificcontext.setParameter("GroupID",gID);
							repUserID = getGroupRep(gID);
						}
					}
				}
				if(repUserID == null) {
					/* returns the rep from the contact group that matches any of the context groups, in order */
					for(String gID: groupIDs) 
					{
						if((repUserID = getGroupRep(gID)) != null) { 
							specificcontext.setParameter("GroupID", gID);
							break;
						}
					}
				}
			}

			/* we've checked (or have no) list of groups, if the contact has the group that was selected in the request lets use the rep from that */
			if(repUserID == null && requestrow.isSet("GroupID")) 
			{
				/* returns the rep from the contact group that matches the request group */
				repUserID = getGroupRep(requestrow.getParameter("GroupID"));
				if(repUserID != null) 
					specificcontext.setParameter("GroupID", requestrow.getParameter("GroupID"));
			}

			/* so we have no list of groups and the contact is not in the group that came with the request but the contact has a default group - lets use that if it has a rep */
			if(repUserID == null && specificcontext.isSet("DefaultGroupID"))
			{
				repUserID = getGroupRep(specificcontext.getParameter("DefaultGroupID"));
				/* ok, so this is unlikely.. but lets check it anyway */
				if(repUserID != null) 
					specificcontext.setParameter("GroupID", specificcontext.getParameter("DefaultGroupID"));
			}

			/* if all else fails just use whatever was pulled out of the contact to begin with, which was the way emailer worked previously. */
			if(repUserID == null) 
			{
				repUserID = specificcontext.getString("RepUserID");
			} else {
				specificcontext.setParameter("RepUserID", repUserID);
			}

			return getUserBean(repUserID);   
		}
	}

	protected void setNoteParameters(GenRow notebean)
	{
		notebean.setParameter(CONTACTID, specificcontext.getString(CONTACTID));
	}

	public void loadProfileTokens(String id)
	{
		loadProfileTokens(id, "Contact");
	} 

	public void loadProfileTokens(String id, String type)
	{
		if (type == null || type.length() == 0) type = "Contact";
		if (id.length() > 0) {
			RowSetBean questionList = new RowSetBean();
			questionList.setConnection(getConnection());
			questionList.setViewSpec("answers/" + type + "AnswerView");
			questionList.setColumn("" + type + "ID",id);
			if (specificcontext.getString("Type").length() > 0) {
				questionList.setColumn("" + type + "AnswerQuestionSurveyQuestion.SurveyQuestionSurvey." + type + "Type",specificcontext.getString("Type")+"+All");
			}
			else {
				questionList.setColumn("" + type + "AnswerQuestionSurveyQuestion.SurveyQuestionSurvey." + type + "Type","All");
			}
			questionList.generateSQLStatment();
			questionList.getResults();

			while(questionList.getNext()) {
				specificcontext.put("Profile-"+StringUtil.ToAlphaNumeric(questionList.getString("QuestionLabel")),questionList.getString("Answer"));
				specificcontext.put("Profile-"+questionList.getString("QuestionID"),questionList.getString("Answer"));
			}
			questionList.close();
		}
	} 

	public void loadNoteTokens(String id) { 
		if(id.length()>0 && !"true".equals(sysprops.getProperty("IgnoreNoteEmailTokens"))) { 
			GenRow noteBean = new GenRow(); 
			noteBean.setConnection(getConnection());
			noteBean.setViewSpec("NoteView"); 
			noteBean.setColumn("ContactID",id); 
			String lastnotetype = sysprops.getProperty("LastNoteType");
			if(lastnotetype!=null)
			{
				noteBean.setColumn("Type",lastnotetype);
			}
			else
			{
				noteBean.setColumn("Type","Export"); //This could be set by system parameter, if required.			   
			}
			noteBean.sortBy("CreatedDate",0); 
			noteBean.sortOrder("DESC",0); 
			noteBean.doAction(GenerationKeys.SELECTFIRST);  

			specificcontext.put("LastNoteDate",noteBean.getColumn("NoteDate"));
			specificcontext.put("LastNoteSubject",noteBean.getColumn("Subject")); 
			specificcontext.put("LastNoteBody",noteBean.getColumn("Body")); 
		}
	}

	public void appendError(StringBuffer errors, String msg, String recordid)
	{
		errors.append(msg);
		errors.append(" - ");
		appendLinkURL(errors, recordid);
		errors.append("\r\n");
	}

	public void appendLinkURL(StringBuffer buffer, String recordid)
	{
		buffer.append(sharedcontext.getString("URLHome"));
		buffer.append("/runway/contact.view?ContactID=");
		buffer.append(recordid);
	}  

	public TableData getOfflineSearch(HttpSession session)
	{
		TableData search = (TableData) session.getAttribute(SessionKeys.CONTACT_SEARCH);

		if (search != null){
			search.setViewSpec("OfflineContactView");
			// we need to remove all group by
			for (int x = 0; x < 10; ++x) {
				search.remove("-groupby" + x);
				search.remove("-sort" + x);
				search.remove("-order" + x);
			}
			search.setParameter("-groupby0", "ContactID");
		}

		boolean hasEmailLimit = StringUtils.isNotBlank(currentUser.getString("BulkEmailLimit"));
		if(hasEmailLimit)
		{
			int limitCount = RunwayUtil.getLeftEmailBulkLimit(getConnection(), currentUser);
			logger.debug("Setting limit to {} for getOfflineSearch", limitCount);
			search.setTop(limitCount);
			search.doAction("search");
			logger.debug("getOfflineSearch query : ", search.getStatement());
		}

		return(search);
	}

	public static final String contactlist = "-contactList";
	public String getOfflineList()
	{
		return(requestrow.getString(contactlist));
	}

	public String getPrimaryKeyName()
	{
		return(CONTACTID);
	}
	/*
  public ErrorMap setEmailStatus(String status , String detail) { 

	  //specificcontext.setParameter("EmailStatus",status); 

	  Contact c = new Contact(specificcontext.getConnection(),specificcontext.getData(CONTACTID)); 
	  c.setCurrentUser(currentUser.getUser());
	  c.setField("EmailStatus",status); 

	  if(detail != null && detail.length()>0) { 
		  Note n = new Note(c.getConnection());
		  n.setField("RepUserID",c.getField("RepUserID")); 
		  n.setField(CONTACTID,c.getField(CONTACTID));
		  n.setField("CompanyID",c.getField("CompanyID")); 
		  n.setField("GroupID",c.getField("GroupID"));
		  n.setCurrentUser(currentUser.getUser());
		  n.setField("Subject","Email Failed Details"); 
		  n.setField("Body",detail); 
		  n.setField("Type","Note"); 
		  n.setField("NoteDate","NOW"); 
		  n.setField("NoteTime","NOW"); 
		  n.insert(); 
	  } 

	  return c.update(); 
  }

  protected String sendEmail() { 
	  String resp = super.sendEmail(); 

	  if(mailerBean.getLastException() != null && mailerBean.getLastException() instanceof SendFailedException) { 
		  SendFailedException sfe = (SendFailedException)mailerBean.getLastException(); 

	      MessagingException me = sfe;
	      Exception ne;
	      boolean upd = false; 
	      while ((ne = me.getNextException()) != null && ne instanceof MessagingException) {
	    	  me = (MessagingException)ne;
	          if (me instanceof SMTPAddressFailedException) {
	        	  SMTPAddressFailedException e = (SMTPAddressFailedException)me;
	        	  String failed = e.getAddress().getAddress(); 

        		  if(failed.equals(specificcontext.getData("Email"))) {
 					 this.setEmailStatus("Invalid Address", logSendFailedException(sfe));
 					 upd = true; 
 					 break; 
 				  }
	          } else if (me instanceof SMTPAddressSucceededException) {
	              SMTPAddressSucceededException e = (SMTPAddressSucceededException)me;
	              String success = e.getAddress().getAddress();
        		  if(success.equals(specificcontext.getData("Email"))) {
  					 this.setEmailStatus("Validated", logSendFailedException(sfe));
  					 upd = true; 
  					 break; 
  				  }
	          } 
	      }
	      if(!upd) { 
	    	 this.setEmailStatus("UNK",logSendFailedException(sfe)); 
	      } 		  
	  }    	 	  
	  return resp; 
  }
	 */

	/*
	 * private method to log the extended SendFailedException introduced in JavaMail 1.3.2.
	 */
	/*
  private String logSendFailedException(SendFailedException sfe) {
	  StringBuffer log = new StringBuffer(); 

      MessagingException me = sfe;
      if (me instanceof SMTPSendFailedException) {
          SMTPSendFailedException ssfe = (SMTPSendFailedException)me;
          log.append("SMTP SEND FAILED:");
          log.append(ssfe.toString());
          log.append("  Command: " + ssfe.getCommand());
          log.append("  RetCode: " + ssfe.getReturnCode());
          log.append("  Response: " + ssfe.getMessage());
      } else {
          log.append("Send failed: " + me.toString());
      }
      Exception ne;
      while ((ne = me.getNextException()) != null && ne instanceof MessagingException) {
          me = (MessagingException)ne;
          if (me instanceof SMTPAddressFailedException) {
              SMTPAddressFailedException e = (SMTPAddressFailedException)me;
              log.append("ADDRESS FAILED:");
              log.append(e.toString());
              log.append("  Address: " + e.getAddress());
              log.append("  Command: " + e.getCommand());
              log.append("  RetCode: " + e.getReturnCode());
              log.append("  Response: " + e.getMessage());
          } else if (me instanceof SMTPAddressSucceededException) {
              log.append("ADDRESS SUCCEEDED:");
              SMTPAddressSucceededException e = (SMTPAddressSucceededException)me;
              log.append(e.toString());
              log.append("  Address: " + e.getAddress());
              log.append("  Command: " + e.getCommand());
              log.append("  RetCode: " + e.getReturnCode());
              log.append("  Response: " + e.getMessage());
          }
      }
      return log.toString(); 
  }
	 */
}