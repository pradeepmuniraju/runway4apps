package com.sok.runway.email;

import com.sok.framework.StringUtil;

public class VelocityUtil
{
   public String convertHtmlToText(String oldString)
   {
      return(StringUtil.convertHtmlToText(oldString));
   }  
   
   public String stripHtml(String oldString)   
   {
      return(StringUtil.stripHtml(oldString));
   }
}