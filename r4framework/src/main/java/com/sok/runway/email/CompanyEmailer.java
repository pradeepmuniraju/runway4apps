package com.sok.runway.email;

import java.sql.Connection;

import javax.servlet.http.*;

import com.sok.framework.*;
import com.sok.framework.generation.*;

import com.sok.runway.SessionKeys;
import com.sok.runway.email.subtype.*;

public class CompanyEmailer extends AbstractEmailer
{
   /*
    * Email types are implemented as EmailerSubtype.
    * 
    * Do not put any code specific to types of emails in here.
    */      
   
   public static final String COMPANYID = "CompanyID";
   public static final String CONTACTID = "ContactID";
   
   protected boolean hasOptOut()
   {
      GenRow temp = getOptOutChecker();;
      temp.setColumn(COMPANYID, specificcontext.getString(COMPANYID));  
      temp.doAction(GenerationKeys.SELECTFIRST);
      
      return(temp.isSuccessful());
   }   
   
   public boolean isOptedOut()
   {
      if(specificcontext!=null)
      {
         if(super.isOptedOut())
         {
            return(true);
         }
         
         return(hasGroupOptOut());
      }
      return(false);
   }    
   
   protected boolean hasGroupOptOut()
   {
      if(requestrow.getString("GroupID").length()!=0) 
      {
         GenRow row = new GenRow();
         row.setConnection(con);
         row.setViewSpec("email/CompanyGroupGroupStatusView");
         row.setParameter("GroupID", requestrow.getString("GroupID"));
         row.setParameter(COMPANYID, specificcontext.getString(COMPANYID));
         row.doAction("selectfirst");
         
         return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
                 || row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID")));         
      }
      return(false);
   }   
   
   protected boolean isRep()
   {
      if(specificcontext!=null){
         GenRow row = new GenRow();
         row.setConnection(con);
         row.setTableSpec("CompanyGroups");
         row.setParameter("-select1", "CompanyGroupID");
         row.setParameter(COMPANYID, specificcontext.getString(COMPANYID));
         row.setParameter("RepUserID", currentUser.getUserID());
         row.doAction("selectfirst");
         
         return(row.isSuccessful());    
      }
      return(true);
   }    
   
   protected void loadSpecificTokens()
   {
      loadSpecificTokens(requestrow.getString(COMPANYID));
   }
   
   public boolean isEmailerType(int type)
   {
      if(type == Emailer.CompanyEmailer)
      {
         return(true);
      }
      return(false);
   }
   
   public boolean isEmailerSubType(int type)
   {
      if(type == Emailer.BaseEmailer)
      {
         return(true);
      }
      return(false);
   }      
   
   protected void loadSpecificTokens(String id)
   {
      specificcontext = getBean("email/CompanyEmailView",COMPANYID, id);
      loadProfileTokens(id);
      if (specificcontext.getString(CONTACTID).length() > 0) loadProfileTokens(specificcontext.getString(CONTACTID),"Contact");
      
      specificcontext.put("rep", getUserBean(specificcontext.getString("RepUserID")));
      
      EmailerSubtype subtype = subtypehead;
      while(subtype!=null)
      {
         subtype.loadSpecificTokens(this, id);
         subtype = subtype.getNext();
      }      
   }

   protected void setNoteParameters(GenRow notebean)
   {
      notebean.setParameter(COMPANYID, specificcontext.getString(COMPANYID));
   }
   
   public void loadProfileTokens(String id)
   {
 	  loadProfileTokens(id, "Company");
    } 
   
   public void loadProfileTokens(String id, String type)
   {
 	  if (type == null || type.length() == 0) type = "Company";
       if (id.length() > 0) {
          RowSetBean questionList = new RowSetBean();
          questionList.setConnection(con);
          questionList.setViewSpec("answers/" + type + "AnswerView");
          questionList.setColumn("" + type + "ID",id);
          if (specificcontext.getString("Type").length() > 0) {
             questionList.setColumn("" + type + "AnswerQuestionSurveyQuestion.SurveyQuestionSurvey." + type + "Type",specificcontext.getString("Type")+"+All");
          }
          else {
             questionList.setColumn("" + type + "AnswerQuestionSurveyQuestion.SurveyQuestionSurvey." + type + "Type","All");
          }
          questionList.generateSQLStatment();
          questionList.getResults();
          
          while(questionList.getNext()) {
             specificcontext.put("Profile-"+StringUtil.ToAlphaNumeric(questionList.getString("QuestionLabel")),questionList.getString("Answer"));
             specificcontext.put("Profile-"+questionList.getString("QuestionID"),questionList.getString("Answer"));
          }
          questionList.close();
       }
    } 

   public void appendLinkURL(StringBuffer buffer, String recordid)
   {
      buffer.append(sharedcontext.getString("URLHome"));
      buffer.append("/runway/company.view?CompanyID=");
      buffer.append(recordid);
   }
   
   public TableData getOfflineSearch(HttpSession session)
   {
      TableData search = (TableData) session.getAttribute(SessionKeys.COMPANY_SEARCH);
      
      if (search != null){
         search.setViewSpec("OfflineCompanyView");
      }
      return(search);
   }
   
   public static final String companylist = "-companyList";
   public String getOfflineList()
   {
      return(requestrow.getString(companylist));
   }
   
   public String getPrimaryKeyName()
   {
      return(COMPANYID);
   }

@Override
protected boolean hasGlobalOptOut() {
	// TODO Auto-generated method stub
	return false;
}
  
   
}