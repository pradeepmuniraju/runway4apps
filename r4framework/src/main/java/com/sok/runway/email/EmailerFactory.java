package com.sok.runway.email;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.sok.framework.generation.nodes.*;
import com.sok.framework.generation.*;
import com.sok.framework.*;
import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;
import java.sql.Connection;
import com.sok.runway.email.subtype.*;
public class EmailerFactory
{
  
   public static Emailer getCompanyEmailer(GenRow requestrow, ServletContext context)
   {
      Emailer emailer = new CompanyEmailer();
      addSubtypes(requestrow, emailer);
      emailer.init(requestrow, context);
      return(emailer);
   }
   
   public static Emailer getContactEmailer(GenRow requestrow, ServletContext context)
   {
      Emailer emailer = new ContactEmailer();
      addSubtypes(requestrow, emailer);
      emailer.init(requestrow, context);
      return(emailer);
   }   
   
   public static Emailer getReportEmailer(GenRow requestrow, ServletContext context)
   {
      Emailer emailer = new ReportEmailer();
      addSubtypes(requestrow, emailer);
      emailer.init(requestrow, context);
      return(emailer);
   }
   
   public static Emailer getTemplatePreview(GenRow requestrow, ServletContext context)
   {
      Emailer emailer = new TemplatePreview();
      addSubtypes(requestrow, emailer);
      emailer.init(requestrow, context);
      return(emailer);
   }
   
   public static Emailer getUserEmailer(GenRow requestrow, ServletContext context)
   {
	   final Emailer emailer = new UserEmailer();
	   addSubtypes(requestrow, emailer);
	   emailer.init(requestrow, context);
	   return(emailer);
   }
   
   public static void addSubtypes(GenRow requestrow, Emailer emailer)
   {
      if(requestrow.getString(AppointmentEmailer.APPOINTMENTID).length() > 0) {
         emailer.addSubtype(new AppointmentEmailer());
      }      
      if(requestrow.getString("-notification").equals("true"))
      {
         emailer.addSubtype(new NotificationEmailer());
      }
      if(requestrow.getString("CMSID").length() > 0)
      {
         emailer.addSubtype(new NewsletterEmailer());
      }
      if(requestrow.getString(OrderEmailer.ORDERID).length() > 0)
      {
         emailer.addSubtype(new OrderEmailer());
      }
      if(requestrow.getString(ProductEmailer.PRODUCTID).length() > 0 
    		  || requestrow.getString(ProductEmailer.PRODUCTIDS).length() > 0)
      {
         emailer.addSubtype(new ProductEmailer());
      }
      if(requestrow.getString(OpportunityEmailer.OPPORTUNITYID).length() > 0)
      {
         emailer.addSubtype(new OpportunityEmailer());
      }     
      if(requestrow.getString(CampaignEmailer.CAMPAIGNID).length() > 0)
      {
         emailer.addSubtype(new CampaignEmailer());
      }         
      if(requestrow.getString(ProductGroupEmailer.PRODUCTGROUPID).length() > 0)
      {
         emailer.addSubtype(new ProductGroupEmailer());
      }
      if(requestrow.getString(OrderItemEmailer.ORDERITEMID).length() > 0)
      {
         emailer.addSubtype(new OrderItemEmailer());
      }
   }
   
   public static Emailer getEmailer(GenRow requestrow, ServletContext context)
   {
      Emailer emailer = null;
      if(requestrow.getString(Emailer.OFFLINEMODE).equals(Emailer.OFFLINEMODECOMPANY))
      {
         emailer = getCompanyEmailer(requestrow, context);
      }
      else if(requestrow.getString(Emailer.OFFLINEMODE).equals(Emailer.OFFLINEMODECONTACT) || requestrow.getString("-checkcount").length()!=0)
      {
         emailer = getContactEmailer(requestrow, context);
      }
      else if(requestrow.getString(CompanyEmailer.COMPANYID).length()!=0 && requestrow.getString(ContactEmailer.CONTACTID).length()==0)
      {
         emailer = getCompanyEmailer(requestrow, context);
      }
      else if(requestrow.getString(ContactEmailer.CONTACTID).length()!=0 && requestrow.getString(CompanyEmailer.COMPANYID).length()!=0)
      {
         emailer = getContactEmailer(requestrow, context);
         //((ContactEmailer) emailer).loadProfileTokens(requestrow.getString(CompanyEmailer.COMPANYID), "Company");
      }
      else if(requestrow.getString(ContactEmailer.CONTACTID).length()!=0)
      {
         emailer = getContactEmailer(requestrow, context);
      }
      else if(requestrow.getString(ReportEmailer.HISTORICREPORTID).length() > 0)
      {
         emailer = getReportEmailer(requestrow, context);
      }
      else if(requestrow.getString(UserEmailer.USERID).length()!=0)
      {
         emailer = getUserEmailer(requestrow, context);
      }
      else if(requestrow.getString(TemplatePreview.TEMPLATEID).length()!=0
    		  || requestrow.getString(TemplatePreview.CMSID).length()!=0)
      {
         emailer = getTemplatePreview(requestrow, context);
      }
      else
      {
         throw new EmailerException ("No template selected");
      }
      return(emailer);
   }
    
   public static Emailer getSavedEmailer(HttpServletRequest request, ServletContext context)
   {
      GenRow bean = new GenRow();
      bean.setRequest(request);
      bean.setViewSpec("email/NoteEmailView");
      bean.parseRequest(request);
      String templateid = bean.getString("TemplateID");
      bean.doAction();

      setCurrentUser(request, bean);
      
      if(bean.getString("TemplateID").length()==0)
      {
         bean.put("TemplateID",bean.getData("TemplateUsed"));
         bean.putInternalToken(AbstractEmailer._savedform,"true");
      }
      return(getEmailer(bean, context));
   }
   
   public static GenRow getRequestRow(HttpServletRequest request)
   {
      GenRow requestrow = new GenRow();
      requestrow.parseRequest(request);

      setCurrentUser(request, requestrow);

      return(requestrow);
   }
   
   public static void setCurrentUser(HttpServletRequest request, GenRow requestrow)
   {
      HttpSession session = request.getSession();
      UserBean currentUser = (UserBean) session.getAttribute(
            SessionKeys.CURRENT_USER);

      requestrow.putInternalToken(SessionKeys.CURRENT_USER,currentUser);
   }   
   
   public static Emailer getEmailer(HttpServletRequest request, ServletContext context)
   {
      return(getEmailer(getRequestRow(request), context));
   }

}