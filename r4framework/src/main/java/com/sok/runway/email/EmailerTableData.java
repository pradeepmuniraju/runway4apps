package com.sok.runway.email;

import java.sql.Connection;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import com.sok.framework.*;
import com.sok.framework.sql.SqlDatabase;
/*
 * Hides some GenRow methods from Velocity
 */
public class EmailerTableData extends MapWrapper implements TableData
{  
   TableData row = null;
   
   public EmailerTableData(TableData map)
   {
      super(map);
      this.row = map;
   }
   
   public EmailerTableData()
   {
      super(new GenRow());
      this.row = (TableData)super.map;
   }   
   
   public Relation getRelatedFields()
   {
      return(row.getRelatedFields());
   }

   public void setDateInputFormat(String d)
   {
      row.setDateInputFormat(d);
   }
   
   public Object getObject(String name)
   {
      return(row.getObject(name));
   }

   public String getColumn(String name, String format)
   {
      return(row.getColumn(name, format));
   }

   public String getColumn(String name)
   {
      return(row.getColumn(name));
   }
   
   public String getColumnHTML(String name)
   {
      return(row.getColumnHTML(name));
   }

   public String getCodedString(String name)
   {
      return(row.getCodedString(name));
   }
   
   public String getCodedColumn(String name)
   {
      return(row.getCodedColumn(name));
   }
   
   public String getLockKey()
   {
      return(row.getLockKey());
   }
   
   public void setColumn(String name, String value)
   {
      row.setColumn(name, value);
   }   
   
   public void setParameter(String name, Object value)
   {
      row.setParameter(name, value);
   }     
   
   public String getParameter(String name)
   {
      return(row.getParameter(name));
   }   
   
   public String[] getParameterValues(String name)
   {
      return(row.getParameterValues(name));
   }      

   public String getData(String name)
   {
      return(row.getData(name));
   }      
   
   public String getDateTime(String name)
   {
      return(row.getDateTime(name));
   }      

   public void sortBy(String field, int index)
   {
      row.sortBy(field, index);
   }       

   public void sortOrder(String order, int index)
   {
      row.sortOrder(order, index);
   }

   public void setToNewID(String name)
   {
      row.setToNewID(name);
   }   

   public void createNewID()
   {
      row.createNewID();
   }      

   public StringBuffer createdDataOnIDStatment()
   {
      return(row.createdDataOnIDStatment());
   }    

   public void generateSQLStatement()
   {
      row.generateSQLStatement();
   }       

   public void setSearchStatement(String s)
   {
      row.setSearchStatement(s);
   }      

   public void setSearchCriteria(String s)
   {
      row.setSearchCriteria(s);
   }       
   
   public String getSearchCriteria()
   {
      return(row.getSearchCriteria());
   }    

   public void setSort(int index, String sort, String order)
   {
      row.setSort(index, sort, order);
   }    

   public String[] getSortfields()
   {
      return(row.getSortfields());
   }

   public String[] getSortorder()
   {
      return(row.getSortorder());
   }   

   public String getDateConversionsType()
   {
      return(row.getDateConversionsType());
   }    

   public void clear()
   {
      row.clear();
   }      

   public boolean containsKey(Object key)
   {
      return(row.containsKey(key));
   }     

   
   public Map getSearchGroups()
   {
      return(row.getSearchGroups());
   }

   public void setSearchGroups(Map groups)
   {
      row.setSearchGroups(groups);
   }

   public int getTop()
   {
      return(row.getTop());
   }

   public void setTop(int topcount)
   {
      row.setTop(topcount);
   }
   

    public String getSearchStatement()
    {
       return(row.getSearchStatement());
    }    

   public boolean isSet(String string)
   {
         return(row.isSet(string));  
   }

   public void setAction(String s)
   {
      row.setAction(s);
      
   }

   public void doAction()
   {
      row.doAction();
   }

   public void doAction(String s)
   {
      row.doAction(s);
   }

   public String getAction()
   {
      return(row.getAction());  
   }

   public void setJndiName(String t)
   {
      row.setJndiName(t);
   }

   public void processRequest(HttpServletRequest request)
   {
      row.processRequest(request);
   }

   public void parseRequest(HttpServletRequest request)
   {
      row.parseRequest(request);
   }

   public void setRequest(HttpServletRequest request)
   {
      row.setRequest(request);
   }

   public void parseRequest(HttpServletRequest request, String prefix)
   {
      row.parseRequest(request, prefix);
   }

   public void parseRequest(String query)
   {
      row.parseRequest(query);
   }

   public void setUpdatedCount(int c)
   {
      row.setUpdatedCount(c);
   }

   public int getUpdatedCount()
   {
      return(row.getUpdatedCount());  
   }

   public void setLocale(Locale l)
   {
      row.setLocale(l);
   }

   public void setTimeZone(String s)
   {
      row.setTimeZone(s);
   }

   public Locale getLocale()
   {
      return(row.getLocale());  
   }

   public TimeZone getTimeZone()
   {
      return(row.getTimeZone());  
   }

   public void close()
   {
      row.close();
   }

   public String getError()
   {
      return(row.getError());  
   }

   public String getStatement()
   {
      return(row.getStatement());  
   }

   public void setCloseConnection(boolean b)
   {
      row.setCloseConnection(b);
   }

   public boolean getCloseConnection()
   {
      return(row.getCloseConnection());  
   }

   public void setConnection(Object con)
   {
      row.setConnection(con);
   }

   public void setConnection(Connection con)
   {
      row.setConnection(con);
   }

   public void setConnection(ServletRequest request)
   {
      row.setConnection(request);
   }

   public Connection getConnection()
   {
      return(row.getConnection());  
   }

   public String getJndiName()
   {
      return(row.getJndiName());  
   }

   public void setError(String s)
   {
      row.setError(s);  
   }

   public String getString(String name)
   {
      return(row.getString(name));  
   }

   public void setViewSpec(ViewSpec vs)
   {
      row.setViewSpec(vs);  
   }

   public void setTableSpec(TableSpec ts)
   {
      row.setTableSpec(ts);  
   }

   public void setViewSpec(String vs)
   {
      row.setViewSpec(vs);  
   }

   public void setTableSpec(String ts)
   {
      row.setTableSpec(ts);  
   }

   public TableSpec getTableSpec()
   {
      return(row.getTableSpec());  
   }

   public ViewSpec getViewSpec()
   {
      return(row.getViewSpec());  
   }

   public RelationSpec getRelationSpec()
   {
      return(row.getRelationSpec());  
   }

   public SqlDatabase getDatabase()
   {
      return(row.getDatabase());  
   }

   public void setDatabase(SqlDatabase database)
   {
      row.setDatabase(database);  
   }

   public void setDateConversionsType(String type)
   {
      row.setDateConversionsType(type);  
   }
   
 //unused
   public TimeZone getUserTimeZoneObject()
   {
	   return null;
   }
   
   public void setUserTimeZone(String zonestring)
   {
       //unused
   }
}
