package com.sok.runway.email;

import javax.servlet.*;
import javax.servlet.http.*;

import java.util.*;
import javax.sql.*;
import java.sql.*;

import javax.naming.*;
import java.text.*;
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OfflineEmailer extends OfflineProcess
{
	private static final Logger logger = LoggerFactory.getLogger(OfflineEmailer.class);

	static final String SENT = "Sent";
	static final String TEMPLATE = "Template Errors";

	protected StringBuffer status = null;

	protected RowBean recordselect = null;
	protected RowBean formbean = null;

	protected ArrayList<String[]> records = null;

	protected Connection con = null;
	protected Context ctx = null;

	protected int total = 0;
	protected int sent = 0;
	protected int failed = 0;

	protected int checkcount = -1;

	protected TableData recordsearch = null;  
	protected Emailer emailer = null;

	protected static final int maxdbconusage = 100;
	protected int dbconusage = 0;  
	protected UserBean currentUser = null;

	protected Set<String> emails		= new HashSet<String>(); 

	public OfflineEmailer(HttpServletRequest request, ServletContext context)
	{
		super(request, context);
		emailer = EmailerFactory.getEmailer(request, context);
		status = new StringBuffer();
		records = new ArrayList<String[]>(500);
		HttpSession session = request.getSession();

		recordsearch = emailer.getOfflineSearch(session);
		UserBean u = (UserBean) session.getAttribute(SessionKeys.CURRENT_USER);
		if (u != null) {
			currentUser = u;
			currentUser.setParameter("OFFLINE_PROCESS_INPROGRESS", "true");
			logger.debug("OFFLINE_PROCESS_INPROGRESS added to user");
		}

		start();    
	}

	public OfflineEmailer(GenRow requestrow, ServletContext context, TableData search) 
	{
		/* fake this */
		super(new com.sok.framework.servlet.ServletRequest(new com.sok.framework.servlet.HttpSession(InitServlet.getConfig().getServletContext()), "/"), context);
		emailer = EmailerFactory.getEmailer(requestrow, context);
		status = new StringBuffer();
		records = new ArrayList<String[]>(500);
		recordsearch = search;
		start();	   
	}

	void retrieveRecordFromList()
	{
		logger.debug("retrieveRecordFromList()");
		StringTokenizer st = new StringTokenizer(emailer.getOfflineList(),"+");
		while(st.hasMoreTokens())
		{
			records.add(new String[]{st.nextToken(),ActionBean._blank});
		}
		total = records.size();
		logger.debug("OfflineEmailer with {} records", total);
	}

	void retrieveRecordsFromSearch() throws Exception
	{  
		logger.debug("retrieveRecordsFromSearch()");
		if(debug)
		{       
			errors.append(recordsearch.getSearchStatement());
		}        
		if(logger.isTraceEnabled()) { 
			logger.trace(recordsearch.getSearchStatement());
		}

		recordsearch.setConnection(getConnection());
		RowSetWrapperBean list = new RowSetWrapperBean();     
		list.setConnection(getConnection());
		list.setSearchBean(recordsearch);
		list.getResults();

		TableData resultset = list.getResultBean();
		while (list.getNext()) {
			records.add(new String[]{resultset.getString(emailer.getPrimaryKeyName()),resultset.getString("EventID")});
		}                   
		total = records.size();   
		logger.debug("OfflineEmailer query: ", list.getSearchBean().getStatement());
		logger.debug("OfflineEmailer with {} records", total);
		list.close();
	}

	void retrieveContacts() throws Exception
	{
		if(emailer.getOfflineList().length()==0)
		{
			retrieveRecordsFromSearch();
		}
		else
		{
			retrieveRecordFromList();
		}
	}

	void log(String s) {
		errors.append(s);
		logger.debug(s);
	}

	void sendEmails() throws Exception
	{
		try
		{
			int success = 0;
			int max = 5;
			int reconnectAttempt = 0;
			int currentsent = 0;
			int currentfailed = 0;
			
			try {
				max = Integer.parseInt(InitServlet.getSystemParam("SMTPmax"));
			} catch (Exception e) {
				max = 5;
			}
			//emailer.getMailerBean().connect();
			for(int i = 0; i < total && !isKilled(); i++)
			{
				// changed this to one connection per 5 message
				if ((i % max) == 0) {
					try
					{
						emailer.getMailerBean().close();
						Thread.sleep(1000);
					}
					catch(Exception te)
					{
					}
				}
				// if we can't connect then we should keep trying for 5 minutes
				while (reconnectAttempt <= 10) {
					try {
						emailer.getMailerBean().connect();
						break;
					} catch (Exception e) {
						++reconnectAttempt;
						log("\r\nReconnecting to email server (Attempt:");
						log(String.valueOf(reconnectAttempt));
						log(", current run S:");
						log(String.valueOf(currentsent));
						log(" F:");
						log(String.valueOf(currentfailed));
						log(").\r\n\r\n");
						Thread.sleep(10000);
					}
				}
				if(reconnectAttempt > 50)
				{
					log("\r\nAborting offline send at ");
					log(String.valueOf(i));
					log(", due to too many consecutive failed email server connections.\r\n");
					setContactEventStatus((String) records.get(i)[1], "Connection Refused", "");					
					break;
				}

				if(dbconusage > maxdbconusage)
				{
					regetConnection();
					dbconusage = 0;
				}

				success = sendEmail((String) records.get(i)[0], (String) records.get(i)[1]);

				// retry current contact if failure was caused by broken connection
				if (success != 0 && !emailer.getMailerBean().isConnected()) {
					log("\r\nMail Server disconnected - Skipping Contact " + (String) records.get(i)[0] + " with event " + (String) records.get(i)[1] + "\r\n\r\n");
					setContactEventStatus((String) records.get(i)[1], "Connection Refused", "");					
				} else {
					reconnectAttempt = 0;
				}

				if(success == 0)
				{
					sent++;
					currentsent++;
				}
				else
				{
					failed++;
					currentfailed++;
				}

				dbconusage++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			try
			{
				emailer.getMailerBean().close();
			}
			catch(Exception te)
			{
			}
		}
	}

	private void setContactEventStatus(final String eventID, final String status, final String noteID) {
		GenRow update = new GenRow();
		try {
			update.setConnection(getConnection(),true);
			update.setTableSpec("ContactEvents");
			update.setParameter("EventID",eventID);
			update.setParameter("SendStatus", status);
			if (!Emailer.SENT.equals(status)) update.setParameter("SendOutcome", "Failed");
			update.setParameter("SendMethod","Manual");
			update.setParameter("SendEmailID",noteID);
			update.setParameter("EventCompleted","Y");
			update.setParameter("SendDate","NOW");
			if(update.isSet("EventID")) { 
				update.doAction(GenerationKeys.UPDATE);
			}
		} catch (Exception e) {
			// for some reason it's not updating the status, we need to know why
			logger.error("OfflineEmailer error ContactEvents {} status {} note {} error {}", new String[]{eventID, status, noteID, e.getMessage()});
		}
	}
	protected int sendEmail(String recordid)
	{
		return sendEmail(recordid, null);
	}
	protected int sendEmail(String recordid, String contactEventID)
	{
		int success = 1;

		try
		{
			emailer.generate(recordid);

			if(emailer.getSpecificContextString("CreatedDate") == null)
			{
				appendError("Record not found", recordid);
				if (contactEventID != null && contactEventID.length() > 0) setContactEventStatus(contactEventID,"Failed",emailer.getSpecificContextString("NoteID"));
				return (success);
			}

			String emailstatus = null;
			if(!debug)
			{
				emailstatus = emailer.send();
				if(emailstatus.equals(Emailer.SENT))
				{
					success = 0;
				}
				else
				{
					appendError(emailstatus, recordid);
				}
				emailer.save(emailstatus); 

				if(contactEventID != null) { 
					setContactEventStatus(contactEventID, emailstatus, emailer.getSpecificContextString("NoteID"));
				}
			}
			else
			{
				emailstatus = "Testing";
				log(emailer.getMailerBean().getTo());
				log("\r\n");
				success = 0;
			}
			return (success);
		}
		catch(Exception e)
		{
			if(contactEventID != null) { 
				setContactEventStatus(contactEventID, TEMPLATE, emailer.getSpecificContextString("NoteID"));
			}
			appendError(e.toString(), recordid);
			return (success);
		}

	}

	public void appendError(String msg, String recordid)
	{
		setLastError(msg);
		emailer.appendError(errors, msg, recordid);
	}

	protected void regetConnection() throws Exception
	{
		closeConnection();
		getConnection();
	}

	protected Connection getConnection() throws Exception
	{
		if(con == null || con.isClosed())
		{
			try
			{
				if(ctx == null)
				{
					ctx = new InitialContext();
				}

				DataSource ds = null;
				try
				{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource) envContext.lookup(dbconn);
				}
				catch(Exception e)
				{
				}
				if(ds == null)
				{
					ds = (DataSource) ctx.lookup(dbconn);
				}
				con = ds.getConnection();
				if(emailer!=null)
				{
					emailer.setConnection(con); 
				}
			}
			finally
			{
				try
				{
					ctx.close();
					ctx = null;
				}
				catch(Exception e)
				{
				}
			}
		}

		return con;
	}

	public void execute()
	{
		con = null;
		ctx = null;
		try
		{

			if(emailer.getCurrentUser() == null || (recordsearch == null && emailer.getOfflineList().length()==0))
			{
				status.append("Resources not found, offline process halted.\r\n");
				logger.debug(status.toString());
				return;
			}

			setEmailHeader();

			if(requestbean.getString("-debug").length()!=0)
			{
				debug = true;
				status.append("Debug mode on.\r\n");
			}

			getConnection();

			if(emailer.getRequestString("-checkcount").length() != 0)
			{
				checkcount = Integer.parseInt(emailer.getRequestString("-checkcount"));
			}

			if(emailer.hasEmailerSubType(Emailer.NewsletterEmailer))
			{
				TableData newsletter = (TableData)emailer.getSharedContext().get("cms");
				if(newsletter == null || newsletter.getString("PageID").length() == 0)
				{
					status.append("Newsletter not found, offline process halted.\r\n");
					logger.debug(status.toString());
					return;
				}
			}
			else
			{
				if(emailer.getTemplateString("TemplateName").length() == 0)
				{
					status.append("Template not found, offline process halted.\r\n");
					status.append(formbean.getError());
					logger.debug(status.toString());
					return;
				}
			}

			retrieveContacts();

			if(checkcount != -1)
			{
				if(total != checkcount)
				{
					status.append("Search results count does not match, offline process halted.\r\n");
					status.append("Current - ");
					status.append(String.valueOf(total));
					status.append(", Original - ");
					status.append(String.valueOf(checkcount));
					status.append("\r\n");
					logger.debug(status.toString());
					return;
				}
			}       

			sendEmails();
			status.append(String.valueOf(total));
			status.append(" records found.\r\n");
			status.append(String.valueOf(sent));
			status.append(" emails sent.\r\n");
			status.append(String.valueOf(failed));
			status.append(" emails failed.\r\n");
			if (isKilled()) {
				status.append("Offline process terminated by - ");
				status.append(getKiller());
				status.append("\r\n\r\n");
			}
			else {      
				status.append("Offline process completed.\r\n\r\n");
			}

		}
		catch(Exception e)
		{
			status.append("Offline process halted on ");
			status.append(String.valueOf(sent + failed));
			status.append(" of ");
			status.append(String.valueOf(total));
			status.append(".\r\n\r\n");
			status.append(ActionBean.writeStackTraceToString(e));

		}
		finally
		{
			closeConnection();
		}
		logger.debug(status.toString());
		currentUser.remove("OFFLINE_PROCESS_INPROGRESS");
		logger.debug("OFFLINE_PROCESS_INPROGRESS removed from user");
	}

	protected void closeConnection()
	{
		if(emailer!=null)
		{
			emailer.close();
		}

		if(con != null)
		{
			try
			{
				con.close();
				con = null;
			}
			catch(Exception ex)
			{
			}
		}

		if(ctx != null)
		{
			try
			{
				ctx.close();
				ctx = null;
			}
			catch(Exception ex)
			{
			}
		}
	}

	void setEmailHeader()
	{
		status.append(contextname);
		status.append(" Runway offline process - Send Email\r\n");
		status.append("Initiated ");
		try
		{
			DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM,
					super.getLocale());
			status.append(dt.format(new java.util.Date()));
		}
		catch(Exception e)
		{
			status.append(new java.util.Date().toString());
		}
		status.append("\r\n");
	}

	String statusstring = null;

	public String getStatusMailBody()
	{
		if(statusstring == null)
		{
			if(errors != null && errors.length() != 0)
			{
				status.append("\r\n");
				status.append(errors.toString());
			}
			statusstring = status.toString();
		}
		return (statusstring);
	}

	public String getStatusMailSubject()
	{
		return (contextname + " Runway offline process - Send Email");
	}

	public String getStatusMailRecipient()
	{
		return (emailer.getCurrentUser().getEmail());
	}

	public int getProcessSize() {
		return total;
	}

	public int getProcessedCount() {
		return sent+failed;
	}

	public int getErrorCount() {
		return failed;
	}
}