package com.sok.runway.email.subtype;

import java.sql.Connection;
import java.util.*;

import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.runway.*;
import com.sok.runway.email.*;

public class ProductGroupEmailer extends AbstractEmailerSubtype
{
   public static final String PRODUCTGROUPID = "ProductGroupID";
   
   public boolean isEmailerSubtype(int type)
   {
      return(type == Emailer.ProductGroupEmailer);
   }
 
   public void loadSharedTokens(EmailerSubtypeHandler emailer)
   {
   }
   
   public void loadProductGroupTokens(EmailerSubtypeHandler emailer, String id)
   {
      GenRow productGroupBean = new GenRow();
      productGroupBean.setConnection(emailer.getConnection());
            
      sharedcontext.put("productGroup",productGroupBean); 
      
      productGroupBean.setColumn("ProductGroupID",requestrow.getString(PRODUCTGROUPID)); 
      boolean success = false; 
      if(emailer.isEmailerType(Emailer.ContactEmailer))
      {
         productGroupBean.setViewSpec("ContactProductGroupView"); 
         productGroupBean.setColumn("ContactID", id); 
         productGroupBean.doAction("selectfirst");
         
         if(productGroupBean.isSuccessful()) { success = true;
	         String key = productGroupBean.getTableSpec().getPrimaryKeyName();
	         emailer.loadProfile("{answers/}ContactProductGroupAnswerView", key, productGroupBean.getString(key), productGroupBean);
         } 
      }
      else if(emailer.isEmailerType(Emailer.CompanyEmailer))
      {
         productGroupBean.setViewSpec("CompanyProductGroupView"); 
         productGroupBean.setColumn("CompanyID", requestrow.getString("CompanyID"));
         productGroupBean.doAction("selectfirst");
         
         if(productGroupBean.isSuccessful()) { success = true;  
	         String key = productGroupBean.getTableSpec().getPrimaryKeyName();
	         emailer.loadProfile("{answers/}CompanyProductGroupAnswerView", key, productGroupBean.getString(key), productGroupBean);
         } 
      }
      com.sok.Debugger.getDebugger().debug("Success - "+success);
      
      if(!success) { //IF RECORDS NOT FOUND, PUT PRODUCT GROUP RECORD IN EMAILER.
    	  TableData t = emailer.getBean("ProductGroupView", PRODUCTGROUPID, requestrow.getString(PRODUCTGROUPID));
    	  sharedcontext.put("productGroup",t);
    	  com.sok.Debugger.getDebugger().debug(t.toString());
      }
   }
   /**
    * @deprecated
    */
   public void loadSpecificTokens(EmailerSubtypeHandler emailer)
   {
	   loadSpecificTokens(emailer, requestrow.getString("ContactID"));
   }
   
   public void loadSpecificTokens(EmailerSubtypeHandler emailer, String id)
   {
      loadProductGroupTokens(emailer, id);
   }
   
   public void setNoteParameters(TableData notebean)
   {
      notebean.setParameter(PRODUCTGROUPID, requestrow.getString(PRODUCTGROUPID));
   }   
}