package com.sok.runway.email.subtype;

import java.sql.Connection;

import com.sok.framework.*;
import com.sok.runway.email.Emailer;
import com.sok.runway.email.EmailerSubtypeHandler;

public class OpportunityEmailer extends AbstractEmailerSubtype
{
   public static final String OPPORTUNITYID = "OpportunityID";
   
   public boolean isEmailerSubtype(int type)
   {
      return(type == Emailer.OpportunityEmailer);
   }
   
   public void loadSharedTokens(EmailerSubtypeHandler emailer)
   {
      loadOpporunityTokens(emailer, requestrow.getString(OPPORTUNITYID), sharedcontext);
   }
   
   protected void loadOpporunityTokens(EmailerSubtypeHandler emailer, String opportunityID, TableData emailcontext)
   {
      if (opportunityID != null && opportunityID.length() > 0) {
         TableData opportunityBean = emailer.getBean("OpportunityView", OPPORTUNITYID, opportunityID);
         
         emailcontext.put("opportunity", opportunityBean);
         
         GenRow profileResults = new GenRow();
         
         emailer.loadProfile(profileResults, "{answers/}OpportunityAnswerView", OPPORTUNITYID, opportunityID, opportunityBean);
         
         // Add opportunity linked Companies
         RowArrayBean linkedCompanies = emailer.getRowArrayBean("OpportunityCompanyListView", OPPORTUNITYID, opportunityID, null);
         
         for(int i=0; i<linkedCompanies.getSize(); i++) {
            emailer.loadProfile(profileResults, "{answers/}CompanyAnswerView", "CompanyID", linkedCompanies.get(i).getString("CompanyID"), linkedCompanies.get(i));
         }
         
         opportunityBean.put("linkedCompanies", linkedCompanies.getList());
         
         // Add opportunity linked Companies
         RowArrayBean linkedContacts = emailer.getRowArrayBean("OpportunityContactsListView", OPPORTUNITYID, opportunityID, null);
         
         for(int i=0; i<linkedContacts.getSize(); i++) {
            emailer.loadProfile(profileResults, "{answers/}ContactAnswerView", "ContactID", linkedContacts.get(i).getString("ContactID"), linkedContacts.get(i));
         }
         
         opportunityBean.put("linkedContacts", linkedContacts.getList());
         
         opportunityBean.put("setup", emailer.getSetupBean(opportunityBean.getString("RepUserID"), null));
         opportunityBean.put("rep", emailer.getUserBean(opportunityBean.getString("RepUserID")));
      }
   }
   
   /**
    * @deprecated
    */
   public void loadSpecificTokens(EmailerSubtypeHandler emailer)
   {  
   }  
   
   public void loadSpecificTokens(EmailerSubtypeHandler emailer, String id)
   {
   }  
   
   public void setNoteParameters(TableData notebean)
   {
      notebean.setParameter(OPPORTUNITYID, requestrow.getParameter(OPPORTUNITYID));      
   }
}