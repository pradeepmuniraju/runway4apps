package com.sok.runway.email.subtype;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.TableData;
import com.sok.runway.email.Emailer;
import com.sok.runway.email.EmailerSubtypeHandler;

public class OrderItemEmailer extends AbstractEmailerSubtype {
	public static final String ORDERITEMID = "OrderItemID";

	public boolean isEmailerSubtype(int type) {
		return (type == Emailer.OrderItemEmailer);
	}

	public void loadSharedTokens(EmailerSubtypeHandler emailer) {

	}

	/**
	 * @deprecated
	 */
	public void loadSpecificTokens(EmailerSubtypeHandler emailer) {
		loadOrderTokens(emailer, requestrow.getString(ORDERITEMID), emailer.getSpecificContext());
	}

	public void loadSpecificTokens(EmailerSubtypeHandler emailer, String id) {
		loadOrderTokens(emailer, requestrow.getString(ORDERITEMID), emailer.getSpecificContext());
	}

	protected void loadOrderTokens(EmailerSubtypeHandler emailer, String orderItemID, TableData emailcontext) {
		if (StringUtils.isNotBlank(orderItemID)) {
			TableData orderItemBean = emailer.getBean("OrderItemsTasksView", ORDERITEMID, orderItemID);
			emailcontext.put("orderitem", orderItemBean);
			orderItemBean.put("setup", emailer.getSetupBean(orderItemBean.getString("RepUserID"), null));
			orderItemBean.put("rep", emailer.getUserBean(orderItemBean.getString("RepUserID")));

			ArrayList<TableData> historyList = getStatusHistoryArray(orderItemID, emailer);
			orderItemBean.put("historylist", historyList);			
			
			// explicitly add empty string if value is null so that email doesnot show up the key
			Iterator<String> iterator = orderItemBean.keySet().iterator();
			while (iterator.hasNext()) {
				String key = iterator.next();
				if (StringUtils.isBlank(orderItemBean.getColumn(key))) {
					orderItemBean.setColumn(key, "");
				}
			}

			setTimeParameters(orderItemBean, orderItemID, emailer);

		}
	}

	private ArrayList<TableData> getStatusHistoryArray(String orderItemID, EmailerSubtypeHandler emailer) {
		GenRow statusHistory = new GenRow();
		statusHistory.setConnection(emailer.getConnection());
		statusHistory.setTableSpec("TaskStatus");
		statusHistory.setParameter("-select0", "*");
		statusHistory.setParameter("OrderItemID", orderItemID);
		statusHistory.sortBy("CreatedDate", 0);
		statusHistory.doAction(ActionBean.SEARCH);
		statusHistory.getResults();

		ArrayList<TableData> historyList = new ArrayList<TableData>();
		while (statusHistory.getNext()) {
			historyList.add(emailer.getBean("TaskStatusView", "TaskStatusID", statusHistory.getData("TaskStatusID")));
		}
		statusHistory.close();
		return historyList;
	}

	/**
	 * if capped, cap hours will be the total bill hrs, if not sum of all the chargeable slips
	 * 
	 * @param request
	 * @param OrderItemID
	 * @return
	 */
	private void setTimeParameters(TableData orderItemBean, String orderItemID, EmailerSubtypeHandler emailer) {
		String totalHrs = "0";
		String afterHrs = "0";
		String bizHrs = "0";
		String ncHrs = "0";

		GenRow row = new GenRow();
		row.setViewSpec("SlipsView");
		row.setConnection(emailer.getConnection());
		row.setParameter("OrderItemID", orderItemID);
		row.setParameter("Chargeable", "Y");
		row.setParameter("-select0", "SUM(TimeTaken) as TotalTimeTaken");

		if ("Y".equals(orderItemBean.getString("CostLocked"))) {
			totalHrs = orderItemBean.getString("BillTime");
		} else {
			row.doAction("selectfirst");
			if (row.isSuccessful() && StringUtils.isNotBlank(row.getData("TotalTimeTaken")))
				totalHrs = row.getData("TotalTimeTaken");
		}
		
		row.setParameter("Chargeable", "N");
		row.doAction("selectfirst");
		if (row.isSuccessful() && StringUtils.isNotBlank(row.getData("TotalTimeTaken")))
			ncHrs = row.getData("TotalTimeTaken");
		
		row.setParameter("Chargeable", "Y");
		row.setParameter("RateType", "AH");
		row.doAction("selectfirst");
		if (row.isSuccessful() && StringUtils.isNotBlank(row.getData("TotalTimeTaken")))
			afterHrs = row.getData("TotalTimeTaken");
		
		row.setParameter("Chargeable", "Y");
		row.setParameter("RateType", "BH");
		row.doAction("selectfirst");
		if (row.isSuccessful() && StringUtils.isNotBlank(row.getData("TotalTimeTaken")))
			bizHrs = row.getData("TotalTimeTaken");

		orderItemBean.put("TH", totalHrs);
		orderItemBean.put("AH", afterHrs);
		orderItemBean.put("BH", bizHrs);
		orderItemBean.put("NC", ncHrs);

		row.close();
	}

	public void setNoteParameters(TableData notebean) {
		notebean.setParameter(ORDERITEMID, requestrow.getString(ORDERITEMID));
	}
}