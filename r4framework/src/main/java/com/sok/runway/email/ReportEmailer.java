package com.sok.runway.email;

import java.io.File;
import java.io.FileOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import com.sok.ErrorDebugger;
import com.sok.framework.DateUtil;
import com.sok.framework.EmailAttachmentBuilder;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.servlet.ResponseOutputStream;
import com.sok.runway.HistoricReport;
import com.sok.runway.CustomReportBean;
import com.sok.runway.ReportBean;
import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;

public class ReportEmailer extends ContactEmailer {

   private static final String REPORTS_FILE_PATH = "/files/reports/";
   
   public static final String HISTORICREPORTID = "HistoricReportID";
   
   public static final String REPORTNAME = "reportName";
   public static final String REPORTCREATIONDATE = "reportCreationDate";
   public static final String SENDERTITLE = "senderTitle";
   public static final String SENDERFIRSTNAME = "senderFirstName";
   public static final String SENDERLASTNAME = "senderLastName";
   
   private com.sok.framework.servlet.HttpSession session;
   private UserBean currentuser;
   
   public void init(GenRow requestrow, ServletContext context) {
      super.init(requestrow, context);
      session = new com.sok.framework.servlet.HttpSession(context);
      currentuser = new UserBean();
   }

   @Override
   public boolean isEmailerType(int type) {
      return(type == Emailer.ReportEmailer);
   }

   @Override
   protected void loadSpecificTokens() {
      loadSpecificTokens(requestrow.getString(HISTORICREPORTID));
   }

   @Override
   protected void loadSpecificTokens(String id) {
      HistoricReport historicReport = getHistoricReport(id);
      
      if(historicReport != null) {
         String reportName = historicReport.getString("Name");
         String reportCreationDate = DateUtil.formatDate(historicReport.getDate("CreatedDate"), DateUtil.DATE_FORMAT_SHORT);      
         String senderFirstName = requestrow.getString("senderFirstName");
         String senderLastName = requestrow.getString("senderLastName");
         
         specificcontext = new GenRow();   
         specificcontext.put("reportName", reportName);
         specificcontext.put("reportCreationDate", reportCreationDate);
         specificcontext.put("senderFirstName", senderFirstName);
         specificcontext.put("senderLastName", senderLastName);
         
         historicReport.close();
      }
   }
   
   private HistoricReport getHistoricReport(String historicReportID) {
      HistoricReport historicReport = null;
      
      if(historicReportID != null && historicReportID.length() > 0) {
         historicReport = new HistoricReport();
         historicReport.setTableSpec("HistoricReports");
         historicReport.setParameter("HistoricReportID", historicReportID);
         historicReport.setParameter("-join1", "HistoricCustomReportBean");
         historicReport.setParameter("-join2", "HistoricReportUser");
         historicReport.setParameter("-select1", "CreatedDate");     
         historicReport.setParameter("-select2", "HistoricCustomReportBean.Name");
         historicReport.setAction(GenerationKeys.SELECTFIRST);
         historicReport.doAction();
      }
      
      return historicReport;
   }
   
   @Override
   protected void generateEmail() {
      super.generateEmail();
      generateHistoricReportPDFAttachment();
   }
   
   private void generateHistoricReportPDFAttachment() {
      String historicReportID = requestrow.getString(HISTORICREPORTID);
      ReportBean report = getCustomReportBean(historicReportID);
      
      if(report != null && !report.isEmpty()) {
         
         File reportsFolder = new File(context.getRealPath(REPORTS_FILE_PATH));
         reportsFolder.mkdir();

         String docURL = createReportImage(historicReportID, report, reportsFolder);
         session.setAttribute("-docURL", docURL);

         File pdfFile = generatePDF(historicReportID, report, reportsFolder);
                 
         String noteID = this.getSpecificContextString("NoteID");
         EmailAttachmentBuilder emailAttachmentBuilder = EmailAttachmentBuilder.getEmailAttachmentBuilder(noteID);
         
         if(emailAttachmentBuilder != null) {
            emailAttachmentBuilder.addUploadedFile(pdfFile);
         }
      }
   }
   
   private ReportBean getCustomReportBean(String historicReportID) {
      ReportBean report = null;
      
      if(historicReportID != null && historicReportID.length() > 0) {
         HistoricReport historicReport = new HistoricReport();
         historicReport.setTableSpec("HistoricReports");
         historicReport.setParameter("HistoricReportID", historicReportID);
         historicReport.setParameter("-select1", "Results");     
         historicReport.setAction(GenerationKeys.SELECTFIRST);
         historicReport.doAction();
         report = historicReport.getCustomReportBean();
         historicReport.close();
      }
      
      return report;
   }
   
   public String createReportImage(String historicReportID, ReportBean report, File reportsFolder) {
      StringBuffer fileName = new StringBuffer();
      fileName.append(historicReportID);
      fileName.append("_");      
      fileName.append(DateUtil.formatDate(new java.util.Date(), DateUtil.DATE_FORMAT_FILE));
      fileName.append(".jpg");
      
      File temp = new File(reportsFolder, fileName.toString());

      try {
         report.saveChartToJpegFile(temp);
      } catch (Exception e) {
         ErrorDebugger.getErrorDebugger().addError(e);
      }

      return REPORTS_FILE_PATH + fileName;
   }
   
   private File generatePDF(String historicReportID, ReportBean report, File reportsFolder) {    
      session.setAttribute("report", report);
      
      String urlPath =  "/crm/autopilot/generation/reportsetpdf.jsp";
      String suffix = ".pdf";
      
      StringBuffer fileName = new StringBuffer();
      fileName.append(historicReportID);
      fileName.append("_");

      fileName.append(DateUtil.formatDate(new java.util.Date(), DateUtil.DATE_FORMAT_FILE));
      fileName.append(suffix);

      File pdfFile = new File(reportsFolder, fileName.toString());

      doFileOutput(pdfFile, urlPath);
      
      session.setAttribute("report", null);
      
      return pdfFile;
   }
   
   private void doFileOutput(File file, String urlPath) {
      ResponseOutputStream responseOS = null;
      FileOutputStream fos = null;

      try {
         fos = new FileOutputStream(file);
         responseOS = new ResponseOutputStream(session, fos, urlPath);
         responseOS.service(getConnection());
      } catch (Exception e) {
         ErrorDebugger.getErrorDebugger().addError(e);
      } finally {
         if (fos != null) {
            try {
               fos.flush();
               fos.close();
            } catch (Exception ex) {
               ErrorDebugger.getErrorDebugger().addError(ex);
            }

         }
         if (responseOS != null) {
            responseOS.close();
         }
      }
   }

 



}
