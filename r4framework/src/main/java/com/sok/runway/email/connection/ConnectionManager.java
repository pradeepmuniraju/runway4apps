/**
 * 
 */
package com.sok.runway.email.connection;

import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * @author Dion Chapman
 *
 */
public class ConnectionManager {
	
	public static void setTrustManager() {
        try {
               // no valid certificate so we need to override this (This is Dion's Code from personal projects)
               TrustManagerFactory tmf = null;
               try {
                     tmf = TrustManagerFactory.getInstance("SunX509");
               } catch (NoSuchAlgorithmException e) {
                     // TODO Auto-generated catch block
                     e.printStackTrace();
               }
               KeyStore ks = null;
               try {
                     tmf.init(ks);
               } catch (KeyStoreException e) {
                     // TODO Auto-generated catch block
                     e.printStackTrace();
               }

               TrustManager[] tm = tmf.getTrustManagers();
               SSLContext sc = null;
               try {
                     sc = SSLContext.getInstance("SSL");
               } catch (NoSuchAlgorithmException e) {
                     // TODO Auto-generated catch block
                     e.printStackTrace();
               }
               TrustManager[] trustAllCerts = new TrustManager[]{
                            new X509TrustManager() {
                                   public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                          return null;
                                   }
                                   public void checkClientTrusted(
                                                 java.security.cert.X509Certificate[] certs, String authType) {
                                   }
                                   public void checkServerTrusted(
                                                 java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException {

                                          boolean trusted = true;

                                          boolean ourTrust = true;

                                          if (!trusted) {
                                                 throw new java.security.cert.CertificateException("Certificate not trusted");
                                          }
                                          if (!ourTrust) {
                                                 throw new java.security.cert.CertificateException("Not an valid certified server");
                                          }
                                   }

                            }
               };

               try {
                     sc.init(null, trustAllCerts, new java.security.SecureRandom());
               } catch (KeyManagementException e) {
                     // TODO Auto-generated catch block
                     e.printStackTrace();
               }
               SSLSocketFactory sslSocketFactory = sc.getSocketFactory();

               HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);
               HttpsURLConnection.setDefaultHostnameVerifier(new e3Verified());

        } catch (Exception e) {}
 }


 /**
 * Server doesn't have a valid certificate on the APIs, we need to bypass this
 * 
  * @author dion
 *
 */
 public static class e3Verified implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
               return true;
        }
 }

}
