package com.sok.runway.email.subtype;

import java.sql.Connection;
import com.sok.framework.*;
import com.sok.runway.email.*;

public abstract interface EmailerSubtype
{
   public void setNext(EmailerSubtype subtype);
   public EmailerSubtype getNext();
   public boolean isEmailerSubtype(int type);
   
   public void init(EmailerSubtypeHandler emailer); 
   public void loadSharedTokens(EmailerSubtypeHandler emailer);
   /**
    * @deprecated
    */
   public void loadSpecificTokens(EmailerSubtypeHandler emailer);
   public void loadSpecificTokens(EmailerSubtypeHandler emailer, String id);
   
   public boolean overridesEmailGeneration();
   public void generateEmail(EmailerSubtypeHandler emailer);
   
   public void setNoteParameters(TableData notebean);
}