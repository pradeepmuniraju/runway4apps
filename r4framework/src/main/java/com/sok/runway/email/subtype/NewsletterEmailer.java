package com.sok.runway.email.subtype;
import java.sql.Connection;

import com.sok.framework.ActionBean;
import com.sok.framework.RowArrayBean;
import com.sok.framework.RowBean;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.runway.email.*;
import com.sok.runway.*;

public class NewsletterEmailer extends AbstractEmailerSubtype
{
   private RowBean newsletter = null;   
   
   public boolean isEmailerSubtype(int type)
   {
      return(type == Emailer.NewsletterEmailer);
   }
   
   public void loadSharedTokens(EmailerSubtypeHandler emailer)
   {
      getNewsletterBean(requestrow.getString("CMSID"), emailer);
      sharedcontext.put("cms", newsletter);     
      sharedcontext.put("contextpath", context.getInitParameter("URLHome"));
      sharedcontext.put("URLHome", context.getInitParameter("URLHome"));
   }
   
   /**
    * @deprecated
    */
   public void loadSpecificTokens(EmailerSubtypeHandler emailer)
   {
   }
   
   public void loadSpecificTokens(EmailerSubtypeHandler emailer, String id)
   {
   }
   
   public void generateEmail(EmailerSubtypeHandler emailer)
   {
	   //System.out.println("%%%%%%%%%%%%%%%%%%%%INSIDE generateEmail NewsletterEmailer%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+emailer.getTemplateString("TemplateID"));
      TableData specificcontext = emailer.getSpecificContext();
      
      //System.out.println("%%%%%%%%%%%%%%%%%%%%emailer.getTemplateString(Subject)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+emailer.getTemplateString("Subject")+"::"+requestrow.getString("Subject"));
      
      String subject = requestrow.get("Subject")!=null?requestrow.getString("Subject"):emailer.getTemplateString("Subject");
      
      //System.out.println("%%%%%%%%%%%%%%%%%%%%subject%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+subject);
      
      requestrow.setColumn("Subject",subject);   
            
      if(requestrow.getString("Subject").length() == 0)
      {
         requestrow.setColumn("Subject", StringUtil.stripBreaks(StringUtil.stripHtml(newsletter.getString("Title"))));
      }

      // this was wiping the ContactID
      //specificcontext.putAll(sharedcontext);
      mergeContexts(specificcontext);
      
      emailer.generateEmail(requestrow.getString("Subject"),
            requestrow.getString("Body"), requestrow.getString("HTMLBody"));

      TemplateURLs nlurllinks = new TemplateURLs();
      nlurllinks.setConnection(emailer.getConnection());
      nlurllinks.getCMSLinks(requestrow.getString("CMSID"), context);
      
      specificcontext.put("urllinks", nlurllinks);
      String temp = null;
      
      if(emailer.isHTMLEmail())
      {
         StringBuffer roottemplate = new StringBuffer();
         roottemplate.append("#parse(\"");
         roottemplate.append(newsletter.getString("EmailTemplate"));
         roottemplate.append(".vt\")");
         
         specificcontext.setColumn("Body", mailerBean.getHtmlbody());
         
         nlurllinks.setHTMLLinks(specificcontext);
         temp = emailer.generateText(roottemplate.toString());
         mailerBean.setHtmlbody(emailer.generateText(AbstractEmailer.fixHrefTags(AbstractEmailer.fixImageTags(AbstractEmailer.fixStandardLinks(temp)))));
      }
      else {
         mailerBean.setHtmlbody(null);
      }
      
      StringBuffer textroottemplate = new StringBuffer();
      textroottemplate.append("#parse(\"");
      textroottemplate.append(newsletter.getString("EmailTemplate"));
      textroottemplate.append("_text.vt\")");      
      
      specificcontext.setColumn("Body", mailerBean.getTextbody());      
      
      nlurllinks.setTextLinks(specificcontext);
      temp = emailer.generateText(textroottemplate.toString());
      mailerBean.setTextbody(emailer.generateText(AbstractEmailer.fixHrefTags(AbstractEmailer.fixImageTags(AbstractEmailer.fixStandardLinks(temp)))));
   }
   
   public boolean overridesEmailGeneration()
   {
      return(true);
   }
   
   private void getNewsletterBean(String cmsID, EmailerSubtypeHandler emailer)
   {
      if(newsletter == null)
      {
         newsletter = getNewsletterBean(cmsID, emailer.getConnection());
      }
   }   
   
   public static RowBean getNewsletterBean(String cmsID, Connection con)
   {
      RowBean newsletter = new RowBean();
      newsletter.setConnection(con);
      newsletter.setViewSpec("PageView");
      newsletter.setColumn("CMSID", cmsID);
      newsletter.setColumn("ParentID", "NULL+EMPTY");
      newsletter.setColumn("Active", "Active");
      newsletter.retrieveData();

      RowArrayBean newslist = new RowArrayBean();
      newslist.setConnection(con);
      newslist.setViewSpec("PageView");
      newslist.setColumn("ParentID", newsletter.getColumn("PageID"));
      newslist.setColumn("Active", "Active");
      newslist.sortBy("SortOrder", 0);
      newslist.generateSQLStatment();
      if(newslist.isSet("ParentID")) newslist.getResults();

      RowArrayBean productlist = null;
      productlist = new RowArrayBean();
      productlist.setConnection(con);
      productlist.setViewSpec("PageProductView");
      productlist.setColumn("PageID", newsletter.getString("PageID"));
      productlist.generateSQLStatment();
      if(productlist.isSet("PageID")) productlist.getResults();
      newsletter.put("products", productlist.getList());

      RowArrayBean newsitemlist = null;
      for (int i = 0; i < newslist.getSize(); i++)
      {
         newsitemlist = new RowArrayBean();
         newsitemlist.setViewSpec("PageView");
         newsitemlist.setColumn("ParentID", newslist.get(i).getColumn(
               "PageID"));
         newsitemlist.setColumn("Active", "Active");
         newsitemlist.sortBy("SortOrder", 0);
         newsitemlist.getResults();
         newslist.get(i).put("newsitems", newsitemlist.getList());

         productlist = new RowArrayBean();
         productlist.setViewSpec("PageProductView");
         productlist
               .setColumn("PageID", newslist.get(i).getColumn("PageID"));
         productlist.generateSQLStatment();
         productlist.getResults();
         newslist.get(i).put("products", productlist.getList());
      }

      newsletter.put("news", newslist.getList());
      
      return(newsletter);
   }
   
   public void setNoteParameters(TableData notebean)
   {

   }
}