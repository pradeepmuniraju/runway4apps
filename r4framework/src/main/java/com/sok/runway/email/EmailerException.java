package com.sok.runway.email;
import com.sok.framework.generation.*;
public class EmailerException extends TracedException
{    
    public EmailerException(String message)
    {
        super(message);
    }
    
    public EmailerException(Exception ex)
    {
        super(ex);
    }           
    
    public EmailerException(String message, Exception ex)
    {
        super(message, ex);
    }     
    
}