package com.sok.runway.email;

import java.util.Iterator;
import java.util.Map;

import com.sok.framework.*;
import com.sok.framework.TableData;
import com.sok.framework.MapWrapper;
import com.sok.framework.generation.nodes.Node;

public class EmailerMap extends MapWrapper
{ 
   TableData row = null;
   
   public EmailerMap(TableData map)
   {
      super(map);
      this.row = map;
   }
   
   public EmailerMap()
   {
      super(new GenRow());
      this.row = (TableData)super.map;
   }
   
   public Object get(String key)
   {
      //this stops you from using another data structure within this map
      return(row.getColumn(key));
   } 
      
   public Object get(Object key)
   {
      //this stops you from using another data structure within this map
      return(row.getColumn(key.toString()));
   } 

   public Object getFormatted(String key, String format)
   {
      return(row.getColumn(key, format));
   } 
   
   public Object put(String key, String value)
   {
      return(row.put(key, value));
   }   
   
   public void putAll(GenRow row)
   {
      Iterator keyit = row.keySet(Node.type_minLeaf, Node.type_maxLeaf).iterator();
      Object value = null;
      Object key = null;
      String keystr = null;
      while(keyit.hasNext())
      {
         key = keyit.next();
         keystr = key.toString();
         //System.out.println("Key:"+keystr);
         value = row.getDataHTML(keystr);
         if(value==null || value.toString().length()==0)
         {
            value = row.getColumnHTML(keystr);
         }
         if(!containsKey(keystr))
         {
            this.row.put(keystr, value);
         }
      }
   }
  
}