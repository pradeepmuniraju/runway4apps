package com.sok.runway.email;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.sok.runway.email.connection.ConnectionManager;

public class SpamChecker {
	
	private String message = "";
	private String score = "";
	private String response = "";
	
	private final String urlStr = "http://spamcheck.postmarkapp.com/filter";

	public SpamChecker(String message) {
		this.message = message;
	}
	
	public boolean check() {
		if (StringUtils.isBlank(message)) return false;
		
		URL url;
		InputStream is = null ;
		OutputStream os = null ;
		InputStreamReader isr = null;
		OutputStreamWriter osw = null;
		try {
			ConnectionManager.setTrustManager();
			
//			System.out.println("FilterUtil.java ==> callAPI()  ==>   apiPath: " + apiPath  + "   ,  ipLockedKey: " + ipLockedKey + "   ,  resourcepath: " + resourcepath);
			
			url = new URL(urlStr);
			final HttpURLConnection urlConnection = (HttpURLConnection) url
					.openConnection();
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setDoInput(true);
			urlConnection.setDoOutput(true);
			os = urlConnection.getOutputStream();
			osw = new OutputStreamWriter(os);
			
			JSONObject json = new JSONObject();
			
			json.put("email", message);
			json.put("options", "long");	
			
			osw.write(json.toJSONString());
			
			osw.flush();
			
			urlConnection.connect();

			is = urlConnection.getInputStream();
			isr = new InputStreamReader(is);
			int numCharsRead;
			final char[] charArray = new char[1024];
			final StringBuffer sb = new StringBuffer();
			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			System.out.println(sb.toString());
			
			json = (JSONObject)new JSONParser().parse(sb.toString());
			
			if ("true".equals(String.valueOf(json.get("success")))) {
				String[] parts = ((String) json.get("report")).split("\n");
				score = (String) json.get("score");
				if (parts != null && parts.length > 0) {
					ArrayList<String> lines = new ArrayList<String>();
					
					String oldStr = "";
					
					for (int p = 0; p < parts.length; ++p) {
						String part = parts[p];
						if (part.startsWith(" pts rule name")) continue;
						if (part.startsWith("---- ---------------------- ---")) continue;
						if (part.startsWith("-") || (part.startsWith(" ") && !part.startsWith("  "))) {
							if (StringUtils.isNotBlank(oldStr)) lines.add(oldStr);
							oldStr = part.trim();
						} else {
							oldStr += " " + part.trim();
						}
					}
					if (StringUtils.isNotBlank(oldStr)) lines.add(oldStr);
					
					response = "";
					
					for (int l = 0; l < lines.size(); ++l) {
						if (StringUtils.isNotBlank(response)) response += "\n";
						response += lines.get(l);
					}
				}
			}
			
			return true;
		}catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {

			try {
				if (isr!=null){
					isr.close();
				}
				if (osw!=null){
					osw.close();
				}
				if (is!=null){
					is.close();
				}
				if (os!=null){
					os.close();
				}
			} catch(Exception ee) {
				ee.printStackTrace();
			}

		} // End of Finally block

		return false;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
