package com.sok.runway.email;

import java.io.File;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





import com.sok.framework.ActionBean;
import com.sok.framework.DateToken;
import com.sok.framework.EmailAttachmentBuilder;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.MailerBean;
import com.sok.framework.RowArrayBean;
import com.sok.framework.RowBean;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.generation.nodes.Parameter;
import com.sok.runway.SessionKeys;
import com.sok.runway.TemplateURLs;
import com.sok.runway.UserBean;
import com.sok.runway.email.subtype.EmailerSubtype;

public abstract class AbstractEmailer implements Emailer, EmailerSubtypeHandler
{

	/*
	 * Email types are implemented as EmailerSubtype.
	 * Context types are subclass.
	 * 
	 * Do not put any specific code in here.
	 */   


	protected MailerBean mailerBean = null;
	protected TableData sharedcontext = null;
	protected TableData specificcontext = null;
	protected TableData requestrow = null;
	protected TableData template = null;
	protected ServletContext context = null;
	protected Connection con = null;

	//protected StringBuffer errors = null;
	protected UserBean currentUser = null;

	VelocityEngine ve = null;

	Properties sysprops = null; 

	protected boolean offlinemode = false;
	protected boolean formmode = false;
	protected boolean previewmode = false;   
	protected boolean savedemail = false;
	public static final String _savedform = "-savedform";
	protected TableData recordssearch = null;

	protected EmailerSubtype subtypehead = null;

	protected boolean closeconnection = true;

	protected boolean checkspam = false;

	protected String spamScore = "";
	protected String spamResp = "";
	private static final Logger logger = LoggerFactory.getLogger(AbstractEmailer.class);

	private static String[][] links = new String[][]{{"/cms/emails/shared/notehtmlview.jsp?ID=","&version"},
		{"/cms/emails/shared/notehtmlview.jsp?NoteID=","&version"},
		{".link?ID=",""},
		{".link?NoteID=",""},
		{"/cms/optout/optout_sendemail.jsp?ID=",""},
		{"/cms/optout/optout_sendemail.jsp?NoteID=",""},
		{"/actions/optout.jsp?ID=",""},
		{"/actions/optout.jsp?NoteID=",""},
		{"/RESPONSE/","/Viewed_Email.gif"},
		{"/NORESPONSE/","/Viewed_Email.gif"}};

	public AbstractEmailer()
	{
		mailerBean = new MailerBean();
		sharedcontext = new GenRow();
	}

	public void addSubtype(EmailerSubtype subtype)
	{
		if(subtypehead == null)
		{
			subtypehead = subtype;
		}
		else
		{
			subtype.setNext(subtypehead);
			subtypehead = subtype;
		}
	}

	public EmailerSubtype getSubtype(int type)
	{
		EmailerSubtype subtype = subtypehead;
		while(subtype!=null)
		{
			if(subtype.isEmailerSubtype(type))
			{
				return(subtype);
			}
			subtype = subtype.getNext();
		}   
		return(null);
	}

	public abstract boolean isEmailerType(int type);

	public boolean hasEmailerSubType(int type)
	{
		EmailerSubtype subtype = subtypehead;
		while(subtype!=null)
		{
			if(subtype.isEmailerSubtype(type))
			{
				return(true);
			}
			subtype = subtype.getNext();
		}      
		return(false);
	}  

	protected GenRow getOptOutChecker()
	{
		GenRow temp = new GenRow();
		temp.setConnection(getConnection());
		temp.setViewSpec("OptOutView");

		temp.addParameter("Type|1","Email");
		temp.addParameter("Type|1","All");

		//temp.setColumn("CampaignID",requestrow.getString("NULL"));

		return(temp);
	}

	public boolean isOptedOut()
	{
		if(specificcontext!=null)
		{
			if(hasOptOut())
			{
				//System.out.println("AbstractEmailer:isOptedOut.hasOptOut(" + specificcontext.getString("ContactID") + ")");
				return(true);
			}
		}
		return(false);
	}

	public boolean canSend()
	{
		if(specificcontext!=null)
		{
			// if the current user is not rep user and not the special system user then check if they can send
			if (!currentUser.getUser().getUserID().equals(specificcontext.get("RepUserID")) && 
					!(sysprops.getProperty("DefaultRepUserID") != null && sysprops.getProperty("DefaultRepUserID").length() > 0 && currentUser.getUser().getUserID().equals(sysprops.getProperty("DefaultRepUserID")))) {
				if(currentUser.getUser().getRestriction("OnlyIfIsRep")) 
				{
					return(isRep());
				}
			}
		} else {
			if(currentUser.getUser().getRestriction("OnlyIfIsRep")) 
			{
				return(isRep());
			}
		}
		return(true);
	}   

	protected abstract boolean hasOptOut();
	protected abstract boolean hasGlobalOptOut();
	protected abstract boolean hasGroupOptOut();

	protected abstract boolean isRep();   

	public void setConnection(Connection con)
	{
		try
		{
			if(con != null && !con.isClosed())
			{
				this.con = con;
				closeconnection = false;
			}
			else
			{
				this.con = null;
			}
		}
		catch(Exception e)
		{

		}
	}

	protected void loadSharedTokens()
	{
		sharedcontext.put("Date", new DateToken());
		sharedcontext.put("CurrentDate", new DateToken().format(("dd/MM/yyyy")));
		sharedcontext.put("VelocityUtil", new VelocityUtil());      
		sharedcontext.put("URLHome", context.getInitParameter("URLHome"));
		if(requestrow.isSet("Headline")) {
			sharedcontext.put("Headline",requestrow.getString("Headline"));
		}
		loadCurrentUser();
		loadAuthor();
		loadSetupTokens();
		EmailerSubtype subtype = subtypehead;
		while(subtype!=null)
		{
			subtype.loadSharedTokens(this);
			subtype = subtype.getNext();
		}
	}

	protected void loadAuthor()
	{
		String repUserID = requestrow.getString("RepUserID");
		if(repUserID.length()==0 && currentUser!=null)
		{
			repUserID = currentUser.getUserID();
			requestrow.put("RepUserID", repUserID);
		}

		if(repUserID.length() > 0)
		{
			TableData user = getUserBean(repUserID);

			sharedcontext.put("author", user);
			loadBean(sharedcontext, "email/UserAuthorView", "UserID", repUserID);
		}
		else
		{
			throw new EmailerException("Author could not be determined.");
		}
	}

	public void setCurrentUser(UserBean user)
	{
		currentUser = user;
	}

	public UserBean getCurrentUser()
	{
		return(currentUser);
	}   

	public TableData getBean(String viewspec, String idname, String idvalue)
	{
		//TODO issue with genrow and velocity context
		/*
      GenRow temp = new GenRow();
      temp.setConnection(getConnection());
      temp.setViewSpec(viewspec);
      temp.setColumn(idname, idvalue);
      temp.doAction(ActionBean.SELECT);
      return(temp);
		 */

		RowBean temp = new RowBean();
		temp.setConnection(getConnection());
		temp.setViewSpec(viewspec);
		temp.setColumn(idname, idvalue);
		temp.doAction(ActionBean.SELECT);
		return(temp);
	}

	public TableData getTableData(String viewspec, String idname, String idvalue)
	{   
		GenRow temp = new GenRow();
		temp.setConnection(getConnection());
		temp.setViewSpec(viewspec);
		temp.setColumn(idname, idvalue);
		temp.doAction(ActionBean.SELECT);
		return(new EmailerTableData(temp));
	}

	public void loadProfile(String viewspec, String keyname, String keyvalue, TableData context)
	{
		GenRow profileResults = new GenRow();
		loadProfile(profileResults, viewspec, keyname, keyvalue, context);
	}

	public void loadProfile(GenRow genrow, String viewspec, String keyname, String keyvalue, TableData context)
	{
		genrow.clear();

		if (keyvalue != null && keyvalue.length() != 0) {
			genrow.setViewSpec(viewspec);
			genrow.setConnection(getConnection());
			genrow.setColumn(keyname, keyvalue);
			genrow.doAction("search");
			genrow.getResults();

			while (genrow.getNext()) {
				context.put("Profile-"+StringUtil.ToAlphaNumeric(genrow.getString("QuestionLabel")), genrow.getString("AnswerText"));
				context.put("Profile-"+genrow.getString("QuestionID"), genrow.getString("AnswerText"));
			}
		}

		genrow.close();
	}

	public ArrayList<TableData> getBeanArray(String viewspec, String idname, String idvalues)
	{
		String[] ids = idvalues.split("\\+");
		ArrayList<TableData> items = new ArrayList<TableData>(ids.length);
		for(int i=0; i<ids.length; i++){
			if(ids[i]!=null && ids[i].length()!=0){
				items.add(getBean(viewspec, idname, ids[i]));
			}
		}
		return(items);
	}

	public RowArrayBean getRowArrayBean(String viewspec, String idname, String idvalue, String sort)
	{
		RowArrayBean arraybean = new RowArrayBean();
		arraybean.setViewSpec(viewspec);
		arraybean.setConnection(getConnection());
		arraybean.setColumn(idname, idvalue);
		if(sort!=null)
		{
			arraybean.sortBy(sort, 0);
		}
		arraybean.generateSQLStatment();
		arraybean.getResults();
		arraybean.close();
		return(arraybean);
	}

	protected void loadBean(TableData context, String viewspec, String idname,
			String idvalue)
	{
		context.putAll(getBean(viewspec, idname, idvalue));
	}

	public TableData getUserBean(String userID) {
		// if there is no userID return an empty table data
		if (userID == null || userID.length() == 0) return new GenRow();
		return getBean("email/UserView", "UserID", userID);
	}

	protected void loadCurrentUser()
	{
		if(currentUser == null)
		{
			throw new EmailerException("Current user not found.");
		}      
		loadBean(sharedcontext, "email/UserCurrentView", "UserID", currentUser
				.getUserID());
	}

	protected void loadSetupTokens()
	{
		String userID = requestrow.getString("RepUserID");
		String setupID = requestrow.getString("SetupID");

		TableData setupBean = getSetupBean(userID, setupID);

		sharedcontext.put("setup", setupBean);
	}

	public TableData getSetupBean(String userID, String setupID) {

		TableData setupBean = null;
		boolean found = false;
		if(userID != null && userID.length() > 0)
		{
			setupBean = getBean("UserSetupView", "UserID", userID);
			if(setupBean.getString("SetupID").length() > 0)
			{
				found = true;
			}
		}
		if(!found)
		{
			if(setupID == null || setupID.length() == 0)
			{
				setupBean = getBean("SetupView", "SetupID", "Default");
			}
			else
			{
				setupBean = getBean("SetupView", "SetupID", setupID);
			}
		}
		return setupBean;
	}

	protected abstract void loadSpecificTokens();

	protected abstract void loadSpecificTokens(String id);

	protected void loadTemplate()
	{
		if(template == null && !requestrow.isSet("TemplateID") && requestrow.isSet("CMSID"))
		{
			TableData newsletter = getBean("CMSView", "CMSID", requestrow.getString("CMSID"));
			if(newsletter.isSet("TemplateID")) {
				requestrow.setParameter("TemplateID",newsletter.getData("TemplateID"));
			} else { 
				throw new EmailerException("Sorry, that newsletter has not been configured with a default template and cannot be previewed.");
			}
		}  
		if(template == null && requestrow.isSet("TemplateID"))
		{ 
			template = getBean("TemplateView", "TemplateID", requestrow
					.getString("TemplateID"));

			if(!requestrow.isSet("MasterTemplateID")) { 
				requestrow.put("MasterTemplateID", template.getString("MasterTemplateID"));
			}

			if(requestrow.isSet("MasterTemplateID"))
			{
				template.putAll(getBean("MasterTemplateView", "MasterTemplateID",
						requestrow.getString("MasterTemplateID")));
			}  
			setTemplateDefaults();
		}
		else
		{
			throw new EmailerException("No email template specified");
		}
	}

	protected void setTemplateDefaults()
	{
		boolean fromForm = sharedcontext.get("Image1") != null;

		//      if (!fromForm) {
		//         GenRow imagelist = new GenRow();
		//         imagelist.setConnection(con);
		//         imagelist.setViewSpec("LinkedDocumentEmailView");
		//         imagelist.setColumn("TemplateID|1", template.getString("TemplateID"));
		//         imagelist.setColumn("MasterTemplateID|1", template.getString("MasterTemplateID"));
		//         imagelist.setColumn("DefaultDocument", "Y");
		//         //imagelist.sortBy("MasterTemplateID", 0);
		//         //imagelist.sortOrder("DESC", 0);
		//         imagelist.doAction("search");
		//         imagelist.getResults();
		//   
		//         while(imagelist.getNext())
		//         {
		//            sharedcontext.put("Image" + imagelist.getString("Location"), sharedcontext.getString("URLHome")
		//                  + "/" + imagelist.getString("FilePath"));
		//         }
		//         imagelist.close();
		//      }

		if(requestrow.getString("TO").length()==0)
		{
			requestrow.setParameter("TO",template.getString("EmailTo"));
		}
		if(requestrow.getString("FROM").length()==0)
		{
			requestrow.setParameter("FROM",template.getString("EmailFrom"));
		}
		if(requestrow.getString("CC").length()==0)
		{
			requestrow.setParameter("CC",template.getString("EmailCC"));
		}
		if(requestrow.getString("REPLY_TO").length()==0)
		{
			requestrow.setParameter("REPLY_TO",template.getString("EmailReplyTo"));
		} 
		if(!requestrow.isSet("Headline")) { 
			requestrow.setParameter("Headline", template.getString("Headline"));
		}
	}

	protected void setHeaders()
	{
		String cc = requestrow.getString("CC");
		String bcc = requestrow.getString("BCC");
		String to = requestrow.getString("TO");
		String from = requestrow.getString("FROM");
		String replyto = requestrow.getString("REPLY_TO");

		if (mailerBean.getSender() == null || mailerBean.getSender().length() == 0) mailerBean.setSender(InitServlet.getSystemParams().getProperty("DefaultEmail"));

		if(from.length() > 0)
		{
			mailerBean.setFrom(getFromAddress(from, specificcontext, currentUser, null));
		}
		else if(sharedcontext.getString("EmailFrom").length() > 0)
		{
			mailerBean.setFrom(getFromAddress(sharedcontext
					.getString("EmailFrom"), specificcontext, currentUser, null));
		}
		else if(specificcontext.getString("RepEmail").length() > 0)
		{
			mailerBean.setFrom(specificcontext.getString("RepEmail"));
		}
		else
		{
			mailerBean.setFrom(InitServlet.getSystemParams().getProperty(
					"DefaultEmail"));
		}

		if(to.length() > 0)
		{
			mailerBean.setTo(getToAddress(to, specificcontext, currentUser, null));
		}
		else if(sharedcontext.getString("EmailTo").length() > 0)
		{
			mailerBean.setTo(getToAddress(sharedcontext
					.getString("EmailTo"), specificcontext, currentUser, null));
		}
		else
		{
			mailerBean.setTo(specificcontext.getString("Email"));
		}

		if(replyto != null)
		{
			mailerBean.setReplyTo(getReplyToAddress(replyto, specificcontext, currentUser, null));
		} 
		else if(sharedcontext.getString("EmailReplyTo").length() > 0)
		{
			mailerBean.setReplyTo(getReplyToAddress(sharedcontext
					.getString("EmailReplyTo"), specificcontext, currentUser, null));
		}   

		if(cc.length() > 0)
		{
			mailerBean.setCc(getCCAddress(cc, specificcontext, currentUser, null));
		}

		if(bcc.length() > 0)
		{
			mailerBean.setBcc(getCCAddress(bcc, specificcontext, currentUser, null));
		}

	}

	public static String getFromAddress(String emailfrom, TableData contact, TableData user,
			TableData note)
	{
		String from = null;
		if(emailfrom.length() != 0)
		{
			emailfrom = ActionBean.getSubstituted(emailfrom, contact, "fromemailaddress");

			if(emailfrom.equals("Support") || emailfrom.equals("RepSupportEmail"))
			{
				from = contact.getString("RepSupportEmail");
			}
			else if(emailfrom.equals("User") || emailfrom.equals("CurrentUser"))
			{
				from = user.getString("Email");
			}
			else if(note != null && emailfrom.equals("AuthorEmail"))
			{
				from = note.getString("AuthorEmail");
			}
			else if(emailfrom.equals("RepEmail") || emailfrom.equals("Rep"))
			{
				from = contact.getString("RepEmail");
			}
			else if (emailfrom.indexOf("$") >= 0)
			{
				from = decodeTokens(contact, emailfrom);
			}
			else if(emailfrom.indexOf("@") > 0)
			{
				from = emailfrom;
			}
			else
			{
				from = contact.getString("RepEmail");
			}
		}
		else
		{
			from = contact.getString("RepEmail");
		}
		return (from);
	}
	
	public static String decodeTokens(TableData emailContext, String value) {
		try
		{
			VelocityEngine ve = VelocityManager.getEngine();

			StringWriter text = new StringWriter();
			ve.evaluate(emailContext, text, "Email", value);
			text.flush();
			text.close();
			return(text.toString());
		}
		catch(Exception e)
		{
			return value;
		}
	}

	/**
	 * Could be used for CC and BCC fields, same as From but without defaulting to Rep
	 * @param emailcc
	 * @param contact
	 * @param user
	 * @param note
	 * @return
	 */
	public static String getCCAddress(String emailcc, TableData contact, TableData user,
			TableData note)
	{
		String cc = null;
		if(emailcc.length() != 0)
		{
			emailcc = ActionBean.getSubstituted(emailcc, contact, "ccemailaddress");

			if(emailcc.equals("Support") || emailcc.equals("RepSupportEmail"))
			{
				cc = contact.getString("RepSupportEmail");
			}
			else if(emailcc.equals("User") || emailcc.equals("CurrentUser"))
			{
				cc = user.getString("Email");
			}
			else if(note != null && emailcc.equals("AuthorEmail"))
			{
				cc = note.getString("AuthorEmail");
			}
			else if(emailcc.equals("RepEmail"))
			{
				cc = contact.getString("RepEmail");
			}
			else if (emailcc.indexOf("$") >= 0)
			{
				cc = decodeTokens(contact, emailcc);
			}
			else if(emailcc.indexOf("@") > 0)
			{
				cc = emailcc;
			}
			else
			{
				cc = ActionBean._blank;;
			}
		}
		else
		{
			cc = ActionBean._blank;
		}
		return (cc);
	}

	public static String getToAddress(String emailTo, TableData contact, TableData user, TableData note) {
		String to = null;
		if(emailTo.length() != 0)
		{
			emailTo = ActionBean.getSubstituted(emailTo, contact, "toemailaddress");

			if(emailTo.equals("Support") || emailTo.equals("RepSupportEmail"))
			{
				to = contact.getString("RepSupportEmail");
			}
			else if(emailTo.equals("User") || emailTo.equals("CurrentUser"))
			{
				to = user.getString("Email");
			}
			else if(note != null && emailTo.equals("AuthorEmail"))
			{
				to = note.getString("AuthorEmail");
			}
			else if(emailTo.equals("RepEmail"))
			{
				to = contact.getString("RepEmail");
			}
			else if (emailTo.indexOf("$") >= 0)
			{
				to = decodeTokens(contact, emailTo);
			}
			else if(emailTo.indexOf("@") > 0)
			{
				to = emailTo;
			}
			else
			{
				to = contact.getString("Email");
			}
		}
		else
		{
			to = contact.getString("Email");
		}
		return (to);
	}   

	public static String getReplyToAddress(String replyTo, TableData contact, TableData user, TableData note) {

		if(replyTo.length() != 0)
		{
			replyTo = ActionBean.getSubstituted(replyTo, contact, "replytoemailaddress");

			if(replyTo.equals("User") || replyTo.equals("CurrentUser"))
			{
				return(user.getString("Email")); 
			}
			else if(note != null && replyTo.equals("AuthorEmail"))
			{
				return(note.getString("AuthorEmail"));
			}
			else if(replyTo.equals("RepEmail"))
			{
				return(contact.getString("RepEmail"));
			}
			else if (replyTo.indexOf("$") >= 0)
			{
				return(decodeTokens(contact, replyTo));
			}
			else if(replyTo.indexOf("@") > 0)
			{
				return(replyTo);
			}
		}
		return ActionBean._blank; 
	}   

	public String generateText(String template)
	{    
		try
		{
			StringWriter text = new StringWriter();
			ve.evaluate(specificcontext, text, "Email", template);
			text.flush();
			text.close();
			return(text.toString());
		}
		catch(Exception e)
		{
			throw new EmailerException(e);
		}
	}

	protected TemplateURLs getTemplateURLs()
	{
		TemplateURLs urllinks = new TemplateURLs();
		urllinks.setConnection(getConnection());
		urllinks.getLinks(requestrow.getString("TemplateID"), requestrow
				.getString("MasterTemplateID"), context);
		return(urllinks);
	}

	public void generateEmail(String subjecttemplate, String texttemplate, String htmltemplate)
	{
		/* if line breaks or html exists, emailing *will* fail */
		subjecttemplate = StringUtil.stripBreaks(StringUtil.stripHtml(subjecttemplate));

		mergeContexts();
		generateTokenList();

		if(hasTemplate())
		{
			TemplateURLs urllinks = getTemplateURLs();

			mailerBean.setSubject(generateText(subjecttemplate));

			if(isHTMLEmail())
			{
				urllinks.setHTMLLinks(specificcontext);
				mailerBean.setHtmlbody(generateText(fixBodyScript(fixHrefTags(fixImageTags(fixStandardLinks(htmltemplate))))));
			}
			else {
				mailerBean.setHtmlbody(null);
			}
			urllinks.setTextLinks(specificcontext);
			mailerBean.setTextbody(generateText(fixBodyScript(fixHrefTags(fixImageTags(fixStandardLinks(texttemplate))))));
		}
		else
		{
			mailerBean.setSubject(subjecttemplate);
			if(isHTMLEmail())
			{
				if(htmltemplate.length()!=0)
				{
					mailerBean.setHtmlbody(fixBodyScript(fixHrefTags(fixImageTags(fixStandardLinks(htmltemplate)))));
				}
				else
				{
					mailerBean.setHtmlbody(fixBodyScript(fixHrefTags(fixImageTags(fixStandardLinks(embedInHTML(texttemplate))))));
				}
			}
			else {
				mailerBean.setHtmlbody(null);
			}
			mailerBean.setTextbody(fixBodyScript(fixHrefTags(fixImageTags(fixStandardLinks(texttemplate)))));
		}
	}

	public static String fixBodyScript(String body) {
		try {
			int s = body.indexOf("<br/><script");
			int e = body.lastIndexOf("</script>");
			if (s > 0 && e > 0) {
				if ("\n".equals(body.substring(s - 1, s))) --s;
				body = body.substring(0, s) + body.substring(e + "</script>".length());
			} else if (s == 0 && e > 0) {
				body = body.substring(e + "</script>".length());
			} else {
				s = body.indexOf("<script");
				e = body.lastIndexOf("</script>");
				if (s > 0 && e > 0) {
					if ("\n".equals(body.substring(s - 1, s))) --s;
					body = body.substring(0, s) + body.substring(e + "</script>".length());
				} else if (s == 0 && e > 0) {
					body = body.substring(e + "</script>".length());
				}

			}
		} catch (Exception e) {
		}

		return body;
	}

	protected void generateDefaultEmail()
	{      
		if(hasTemplate())
		{
			StringBuffer textbuffer = new StringBuffer();
			StringBuffer htmlbuffer = new StringBuffer();

			String body = requestrow.get("Body")!=null?requestrow.getString("Body"):template.getString("Body");
			String htmlbody = requestrow.get("HTMLBody")!=null?requestrow.getString("HTMLBody"):getTemplateBody();
			String subject = requestrow.get("Subject")!=null?requestrow.getString("Subject"):template.getString("Subject");

			// sometimes we get a script that stops links being used, this is good for design, but not for emails
			// for some reason some tokens are not getting decoded correctly
			//body = fixBodyScript(fixStandardLinks(body));
			//htmlbody = fixBodyScript(fixStandardLinks(htmlbody));

			textbuffer.append(template.getString("TextHeader"));
			//textbuffer.append(" \r\n\r\n ");
			textbuffer.append(body);
			//textbuffer.append(" \r\n\r\n ");
			textbuffer.append(template.getString("TextFooter"));

			htmlbuffer.append(template.getString("HTMLHeader"));
			//htmlbuffer.append(" ");
			htmlbuffer.append(htmlbody); 
			//htmlbuffer.append(" ");
			htmlbuffer.append(template.getString("HTMLFooter"));



			generateEmail(subject, fixBodyScript(fixHrefTags(fixImageTags(fixStandardLinks(textbuffer.toString())))), fixBodyScript(fixHrefTags(fixImageTags(fixStandardLinks(htmlbuffer.toString())))));         
		}
		else
		{
			generateEmail(requestrow.getString("Subject"), fixBodyScript(fixHrefTags(fixImageTags(fixStandardLinks(requestrow.getString("Body"))))), fixBodyScript(fixHrefTags(fixImageTags(fixStandardLinks(requestrow.getString("HTMLBody"))))));  
		}
	}

	public static String fixImageTags(String html) {
		int s = 0, e = 0;
		String text = html.replaceAll("&quote;", "\"");

		while (s < text.length() && text.toLowerCase().indexOf("<img",s) > 0) {
			s = text.toLowerCase().indexOf("<img",s);
			if (s == -1) break;
			e = text.toLowerCase().indexOf(">",s);
			if (e == -1) break;
			String tag = text.substring(s, e);
			int ssrc = tag.toLowerCase().indexOf("src=");
			if (ssrc >= 0) {
				int esrc =  tag.indexOf("\"",ssrc + 5);
				if (esrc >= 0) {
					String src = StringUtil.urlDecode(tag.substring(ssrc + 5,esrc));
					tag = tag.substring(0,ssrc + 5) + src + tag.substring(esrc);
				}
			}
			boolean wB = tag.indexOf("width=\"") >= 0 || tag.indexOf("width = \"") >= 0;
			boolean hB = tag.indexOf("height=\"") >= 0 || tag.indexOf("height = \"") >= 0;
			if (tag.toLowerCase().indexOf("style=") >= 0) {
				String width = "";
				String height = "";
				String sfloat = "";
				int t = tag.toLowerCase().indexOf("style=");
				if (tag.toLowerCase().indexOf("width:",t) >= 0){
					if (tag.charAt(tag.toLowerCase().indexOf("width:",t) - 1) != '-') {
						int w = tag.toLowerCase().indexOf("width:",t) + "width:".length();
						width = tag.substring(w).trim();
						if (width.toLowerCase().indexOf("px") >= 0) {
							width = width.substring(0,width.toLowerCase().indexOf("px")).trim();
						} else if (width.toLowerCase().indexOf(";") >= 0) {
							width = width.substring(0,width.toLowerCase().indexOf(";")).trim();
						} else {
							width = "";
						}
					}

				}
				if (tag.toLowerCase().indexOf("height:",t) >= 0){
					if (tag.charAt(tag.toLowerCase().indexOf("height:",t) - 1) != '-') {
						int w = tag.toLowerCase().indexOf("height:",t) + "height:".length();
						height = tag.substring(w).trim();
						if (height.toLowerCase().indexOf("px") >= 0) {
							height = height.substring(0,height.toLowerCase().indexOf("px")).trim();
						} else if (height.toLowerCase().indexOf(";") >= 0) {
							height = height.substring(0,height.toLowerCase().indexOf(";")).trim();
						} else {
							height = "";
						}
					}
				}
				if (tag.toLowerCase().indexOf("float:",t) >= 0){
					int w = tag.toLowerCase().indexOf("float:",t) + "float:".length();
					sfloat = tag.substring(w);
					if (sfloat.indexOf(";") >= 0) {
						sfloat = sfloat.substring(0,sfloat.indexOf(";")).trim();
					}
					if (sfloat.indexOf("left") >= 0){
						sfloat = "left";
					}
					else if (sfloat.indexOf("right") >= 0){
						sfloat = "right";
					}
					else{
						sfloat = "";
					}
				}

				if (tag.endsWith("/")) {
					tag = tag.substring(0,tag.length() - 1);
					--e;
				}
				if (!wB && width.length() > 0) tag += " width=\"" + width + "\"";
				if (!hB && height.length() > 0) tag += " height=\"" + height + "\"";
				if (sfloat.length() > 0) tag += " align=\"" + sfloat + "\"";
				text = text.substring(0,s) + tag + text.substring(e);
			} else {
				text = text.substring(0,s) + tag + text.substring(e);
			}
			s = e + 1;

		}

		return text;
	} 

	public static String fixHrefTags(String html) {
		int s = 0, e = 0;
		String text = html;

		while (s < text.length() && text.toLowerCase().indexOf("<a",s) > 0) {
			s = text.toLowerCase().indexOf("<a",s);
			if (s == -1) break;
			e = text.toLowerCase().indexOf(">",s);
			if (e == -1) break;
			String tag = text.substring(s, e);
			int ssrc = tag.toLowerCase().indexOf("href=");
			if (ssrc >= 0) {
				int esrc =  tag.indexOf("\"",ssrc + 6);
				if (esrc >= 0) {
					String src = StringUtil.urlDecode(tag.substring(ssrc + 6,esrc));
					src = src.replaceAll("&amp;","&");
					tag = tag.substring(0,ssrc + 6) + src + tag.substring(esrc);
					text = text.substring(0,s) + tag + text.substring(e);
				}
			}
			s = e + 1;

		}

		return text;
	}

	public static String fixStandardLinks(String html) {
		int s = 0, e = 0, t = 0, p = 0;
		String text = html;

		for (int i = 0; i < links.length; ++i) {
			String start = links[i][0];
			String end = links[i][1];

			while (s < text.length() && text.toLowerCase().indexOf(start.toLowerCase(),s) > 0) {
				s = text.toLowerCase().indexOf(start.toLowerCase(),s);

				if (s == -1) break;

				String link1 = start + "${NoteID}" + end;
				String link2 = start + "$NoteID" + end;
				String tmp = text.substring(s).toLowerCase();

				if (!tmp.startsWith(link1.toLowerCase()) && !tmp.startsWith(link2.toLowerCase())) {
					// ok we don't have ${NoteID} lets put it in there
					if (end.length() == 0) {
						int e1 = text.toLowerCase().indexOf("&",s + start.length());
						int e2 = text.toLowerCase().indexOf("\"",s + start.length());
						if (e1 == -1) e = e2;
						else if (e2 == -1) e = e1;
						else if (e1 < e2) e = e1;
						else e = e2;
					} else {
						e = text.toLowerCase().indexOf(end.toLowerCase(),s + start.length());
					}
					if (e > s + start.length() && (e - s) <= 100) {
						text = text.substring(0,s + start.length()) + "${NoteID}" + text.substring(e);
					}
				}

				// now we should check that the URL part is correct;
				//if (start.startsWith("/"))
				//	tmp = text.substring(0,s + 1);
				//else
					tmp = text.substring(0,s);
				if (".link?ID=".equalsIgnoreCase(start) || ".link?NoteID=".equalsIgnoreCase(start)) {
					if (tmp.lastIndexOf("/") >= 0) tmp = tmp.substring(0, tmp.lastIndexOf("/"));
				}
				if (start.indexOf("RESPONSE") == -1) {
					t = tmp.toLowerCase().lastIndexOf("href=\"");
					if (t != -1) {
						// we only need to replace what is there with ${URLHome} even if it is already a correct address
						t += "href=\"".length();
						if ((tmp.length() - t) <= 100) text = text.substring(0,t) + "${URLHome}" + text.substring(tmp.length());
					}
				} else {
					t = tmp.toLowerCase().lastIndexOf("src=\"");
					if (t != -1) {
						// we only need to replace what is there with ${URLHome} even if it is already a correct address
						t += "src=\"".length();
						if ((tmp.length() - t) <= 100) text = text.substring(0,t) + "${URLHome}" + text.substring(tmp.length());
					}
				}
				if (e > s)
					s = e + 1;
				else 
					++s;
			}
		}

		return text;
	}

	protected void generateEmail()
	{
		mergeContexts();
		EmailerSubtype subtype = getOverrideSubtype();
		generateTokenList();

		if(subtype == null)
		{
			generateDefaultEmail();
		}
		else
		{
			subtype.generateEmail(this);
		}
	}

	protected EmailerSubtype getOverrideSubtype()
	{
		EmailerSubtype subtype = subtypehead;
		while(subtype!=null)
		{
			if(subtype.overridesEmailGeneration())
			{
				return(subtype);
			}
			subtype = subtype.getNext();
		}   
		return(null);
	}

	protected void generateHTMLOfflinePreview()
	{
		StringBuffer htmlbuffer = new StringBuffer();
		getTemplateURLs().setHTMLLinks(specificcontext);

		htmlbuffer.append(generateText(template.getString("HTMLHeader")));
		htmlbuffer.append(requestrow.getString("HTMLBody"));
		htmlbuffer.append(generateText(template.getString("HTMLFooter")));

		mailerBean.setHtmlbody(htmlbuffer.toString());
	}   

	protected void generateTextOfflinePreview()
	{
		StringBuffer textbuffer = new StringBuffer();
		getTemplateURLs().setTextLinks(specificcontext);

		textbuffer.append(generateText(template.getString("TextHeader")));
		//textbuffer.append("\r\n\r\n");
		textbuffer.append(requestrow.getString("Body"));
		//textbuffer.append("\r\n\r\n");
		textbuffer.append(generateText(template.getString("TextFooter")));

		mailerBean.setTextbody(textbuffer.toString());
	}    

	protected void initPreview(boolean html)
	{
		previewmode = true;
		if(offlinemode)
		{
			specificcontext = new GenRow();
			mergeContexts();  
			generateEmail();
			/*
         if(html)
         {
            generateHTMLOfflinePreview();
         }
         else
         {
            generateTextOfflinePreview();
         }
			 */
		}
		else
		{
			generate();
		}
	}

	public String getHTMLPreview()
	{
		initPreview(true);
		String preview = mailerBean.getHtmlbody();
		if(preview == null || preview.length()==0)
		{
			preview = embedInHTML(mailerBean.getTextbody());
		}
		return(preview);
	}

	public String getTextPreviewInHTML()
	{ 
		return(embedInHTML(getTextPreview()));
	}

	protected String embedInHTML(String text)
	{
		StringBuffer out = new StringBuffer();
		out.append("<html><head></head>");
		out.append("<STYLE>BODY {margin: 0px;}TABLE {margin: 0px; width: 100%; height: 100%;}</STYLE>");
		out.append("<body><table><tr><td valign=\"top\">");
		out.append(StringUtil.toHTML(text));
		out.append("</body></td></tr></table></html>");     
		return(out.toString());
	}

	public String getTextPreview()
	{
		initPreview(false);
		return(mailerBean.getTextbody());
	}

	protected boolean repActive() 
	{
		return true; 
	}

	protected String sendEmail()
	{
		if(isOptedOut())
		{
			String msg = Emailer.OPTOUT + " for Communications";  
			return(msg);
		}
		if(hasGlobalOptOut()) {
			String msg = Emailer.OPTOUT + " for Old Style Contact";  
			return(msg);
		}
		if (hasGroupOptOut()) {
			String groupName = specificcontext.getString("GroupName");
			String groupID = specificcontext.getString("GroupID");

			String msg = Emailer.OPTOUT + " for Group " + groupName + "(" + groupID + ")";  
			return(msg);
		}
		if(!canSend())
		{
			return(Emailer.NOTPERMITTED);
		}
		String groupID = specificcontext.getString("GroupID");
		if(groupID.length() == 0) 
		{
			String msg = Emailer.NOGROUP;  
			return(msg);
		}
		if(!specificcontext.isSet("RepUserID")) 
		{
			String groupName = specificcontext.getString("GroupName");

			String msg = Emailer.NOREP + " for Group " + groupName;  
			return(msg);
		}
		if(!repActive()) 
		{
			String groupName = specificcontext.getString("GroupName");

			String msg = Emailer.REPINACTIVE + " Rep " + specificcontext.get("RepFirstName") + " " + specificcontext.get("RepLastName") + " for Group " + groupName;  
			return(msg);
		}
		// lets see if we can get a To address from all of these
		if (mailerBean.getTo() == null || mailerBean.getTo().length() == 0) {
			if (mailerBean.getCc() != null && mailerBean.getCc().length() > 0) mailerBean.setTo(mailerBean.getCc());
		}
		if (mailerBean.getTo() == null || mailerBean.getTo().length() == 0) {
			if (mailerBean.getBcc() != null && mailerBean.getBcc().length() > 0) mailerBean.setTo(mailerBean.getBcc());
		}
		if (mailerBean.getTo() == null || mailerBean.getTo().length() == 0) {
			String msg = "Empty email address for Contact";  
			return(msg);
		}

		if(offlinemode)
		{
			mailerBean.setAttachments(getAttachments(requestrow.getString(Emailer.ATTACHMENTID)));
		}
		else
		{
			mailerBean.setAttachments(getAttachments(specificcontext.getString("NoteID")));
		}
		if (specificcontext.isSet("CampaignID")) {
			if (checkPreviousMail(specificcontext.getString("ContactID"), specificcontext.getString("CampaignID"), specificcontext.getString("EventID"), specificcontext.getString("TemplateID")) 
					&& !specificcontext.getString("TemplateID").equals(InitServlet.getSystemParam("OptOutTemplateID"))) 
				return "Already Sent";
		}
		String response = mailerBean.getResponse();
		if (checkspam) {
			processSpam();
		}

		return(response);
	}
	
	private boolean checkPreviousMail(String contactID, String campaignID, String eventID, String templateID) {
		GenRow check = new GenRow();
		check.setTableSpec("Notes");
		check.setConnection(getConnection());
		check.setParameter("-select0", "NoteID");
		check.setParameter("ContactID", contactID);
		check.setParameter("CampaignID", campaignID);
		check.setParameter("EventID", eventID);
		check.setParameter("NoteEmail.TemplateUsed", templateID);
		check.doAction("selectfirst");
		
		return check.getString("NoteID").length() > 0;
	}

	public void processSpam() {
		String fullMessage = mailerBean.getFullMessage();
		if (StringUtils.isNotBlank(fullMessage)) {
			SpamChecker sc = new SpamChecker(fullMessage);
			if (sc.check()) {
				spamScore = sc.getScore();
				spamResp = sc.getResponse();

				String templateID = specificcontext.getString("TemplateID");

				if (StringUtils.isNotBlank(templateID)) {
					GenRow template = new GenRow();
					template.setTableSpec("Templates");
					template.setParameter("TemplateID", templateID);
					template.setParameter("SpamScore", spamScore);
					template.setParameter("SpamReport", spamResp);
					template.doAction("update");
				}

			}
		}
	}

	protected void setServletContext(ServletContext context)
	{
		this.context = context;
		mailerBean.setHost(context.getInitParameter("SMTPHost"));
		mailerBean.setUsername(context.getInitParameter("SMTPUsername"));
		mailerBean.setPassword(context.getInitParameter("SMTPPassword"));
		mailerBean.setPort(context.getInitParameter("SMTPPort"));
		mailerBean.setSender(context.getInitParameter("SMTPFrom"));
		mailerBean.setURLHome(context.getInitParameter("URLHome"));
	}

	public void init(GenRow requestrow, ServletContext context)
	{
		setRequest(requestrow);
		this.requestrow = requestrow;
		setServletContext(context);
		init();
		EmailerSubtype subtype = subtypehead;
		while(subtype!=null)
		{
			subtype.init(this);
			subtype = subtype.getNext();
		}   
		if(requestrow.isSet("TemplateID") || requestrow.isSet("CMSID"))
		{
			// requires a URLHome which is set after here loadSharedTokens()
			sharedcontext.put("URLHome", context.getInitParameter("URLHome"));
			loadTemplate();
		}
		if(hasTemplate())
		{
			loadSharedTokens();
		}
		// Values from Form getting over ridden so do this again
		//sharedcontext.putAll(requestrow);
	}

	protected void setNoteID()
	{
		if((offlinemode && (!previewmode && !formmode)) || (!offlinemode && requestrow.getString("NoteID").length()==0))
		{
			if (specificcontext.getString("NoteID").length() == 0) specificcontext.setToNewID("NoteID");
		}
		else
		{
			specificcontext.put("NoteID", requestrow.getString("NoteID"));
		}
		mailerBean.setNoteID(specificcontext.getString("NoteID"));
	}

	public void init(HttpServletRequest request, ServletContext context)
	{
		setRequest(request);
		GenRow requestrow = new GenRow();
		requestrow.parseRequest(request);
		init(requestrow, context);
	}

	protected void setRequest(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		UserBean u = (UserBean) session.getAttribute(SessionKeys.CURRENT_USER);
		if (u != null) { 
			currentUser = u;
		}
	}

	protected void setRequest(GenRow requestrow)
	{
		UserBean u = (UserBean) requestrow.get(SessionKeys.CURRENT_USER, Parameter.type_InternalTokenParameter);
		if (u != null) { 
			currentUser = u;
		}   
	}   

	protected void init()
	{  
		setNoteDefaults();

		sharedcontext.putAll(requestrow);

		if(requestrow.get(Emailer.OFFLINEMODE)!=null)
		{
			offlinemode = true;
		}
		this.sysprops = InitServlet.getSystemParams(); 

		setConnection(requestrow.getConnection());
		closeconnection = requestrow.getCloseConnection();

		try
		{
			ve = VelocityManager.getEngine();
		}
		catch(Exception e)
		{
			throw new EmailerException(e);
		}         
	}

	public boolean hasTemplate()
	{
		return(template!=null);
	}

	public String send(HttpServletRequest request, ServletContext context)
	{
		init(request, context);
		return(generateSendAndSave());
	}

	protected String sendAndSave()
	{
		String status = send();
		save(status);
		if (StringUtils.isNotBlank(InitServlet.getSystemParam("SMSProvider"))) {
			specificcontext.put("NoteLink", sharedcontext.getString("URLHome") + "?n=" + specificcontext.getString("NoteID"));
			specificcontext.put("CodeLink", sharedcontext.getString("URLHome") + "?n=" + specificcontext.getString("CodeLinkID"));
			if ((Emailer.SENT.equals(status) || Emailer.SENTOUTCOME.equals(status)) && template != null && template.isSet("SuccessSMSTemplateID")) {
				sharedcontext.put("SMSTemplateID", template.getString("SuccessSMSTemplateID"));
				SMSEmailer sms = new SMSEmailer(sharedcontext, specificcontext);
				if (sms.loadTemplate()) {
					try {
						sms.sendMessage();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else if (template != null && template.isSet("FailSMSTemplateID")) {
					sharedcontext.put("SMSTemplateID", template.getString("FailSMSTemplateID"));
					SMSEmailer sms = new SMSEmailer(sharedcontext, specificcontext);
					if (sms.loadTemplate()) {
						try {
							sms.sendMessage();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
			}
		}
			
		return(status);
	}

	public String generateSendAndSave()
	{
		generate();
		return(sendAndSave());
	}

	public String generateSendAndSave(TableData context)
	{
		generate(context);
		return(sendAndSave());
	}

	public String generateSendAndSave(String id)
	{
		generate(id);
		return(sendAndSave());
	}

	public String getFormText()
	{
		if(savedemail)
		{
			if(mailerBean.getHtmlbody().length()!=0)
			{
				return(mailerBean.getHtmlbody());
			}
			else
			{
				return(embedInHTML(mailerBean.getTextbody()));
			}
		}
		else if(hasTemplate())
		{        
			return(getTemplateBody());
		}

		return("");
	}

	protected String getTemplateBody()
	{
		String body = template.getString("Body");

		if( !(offlinemode && formmode) )
		{
			body = generateText(body);
		}
		// if it contains only line feeds and no <br> then encode it
		if (body.indexOf("\n") >= 0 && !StringUtil.isHtml(body)) body = StringUtil.encodeHTMLLinefeeds(body);

		return(body);     
	}

	public String getFormSubject()
	{
		if(savedemail)
		{
			return(mailerBean.getSubject());
		}      
		else if(hasTemplate())
		{
			if(false && offlinemode)
			{
				return(template.getString("Subject"));
			}
			else
			{
				return(generateText(template.getString("Subject")));
			}
		}
		return("");
	}

	protected void setNoteDefaults()
	{
		if(requestrow.getString("Type").length()==0)
		{
			requestrow.setParameter("Type","Email");
		}
		if(requestrow.getString("Completed").length()==0)
		{
			requestrow.setParameter("Completed","Yes");
		}
		if(requestrow.getString("NoteDate").length()==0)
		{
			requestrow.setParameter("NoteDate","NOW");
		}
	}

	public String getFormTo()
	{
		if(savedemail)
		{
			return(mailerBean.getTo());
		}        
		else if(hasTemplate())
		{      
			return(getToAddress(template.getString("EmailTo"), specificcontext, currentUser, null));
		}
		return("");         
	}

	public String getFormFrom()
	{     
		if(savedemail)
		{
			return(mailerBean.getFrom());
		}       
		else if(hasTemplate())
		{        
			if (template.getString("EmailFrom").length() > 0)
			{
				return(getFromAddress(template.getString("EmailFrom"), specificcontext, currentUser, null));
			}
		}
		return(sharedcontext.getString("AuthorEmail"));         
	}   

	public String getFormCc()
	{   
		if(savedemail)
		{
			return(mailerBean.getCc());
		}        
		else if(hasTemplate())
		{        
			if(template.getString("EmailCC").length()>0)
			{
				return(getCCAddress(template.getString("EmailCC"), specificcontext, currentUser, null));
			}
		}
		return("");  
	}     

	public String getFormReplyTo()
	{    
		if(savedemail)
		{
			return(mailerBean.getReplyTo());
		}       
		else if(hasTemplate())
		{        
			if(template.getString("EmailReplyTo").length()>0)
			{
				return(getReplyToAddress(template.getString("EmailReplyTo"), specificcontext, currentUser, null));
			}
		}
		return("");  
	}    

	public String getRequestString(String name)
	{
		return(requestrow.getString(name));
	}

	public String getTemplateString(String name)
	{
		if(template == null)
		{
			return("");
		}
		return(template.getString(name));
	} 

	public String getSharedContextString(String name)
	{
		if(sharedcontext == null)
		{
			return("");
		}
		return(sharedcontext.getString(name));
	}

	public String getSpecificContextString(String name)
	{
		if(specificcontext == null)
		{
			return("");
		}
		return(specificcontext.getString(name));
	}

	public void generate(TableData context)
	{     
		specificcontext = context;
		generateEmail();
	}

	public void generate(String id)
	{
		loadSpecificTokens(id);
		generateEmail();
	}

	public void generate()
	{ 
		loadSpecificTokens();
		generateEmail();
	}

	public void generateForm()
	{
		formmode = true;
		if(hasTemplate() && !offlinemode)
		{
			loadSpecificTokens();
		}
		else
		{
			specificcontext = new GenRow();
		}
		mergeContexts();
	}

	public void generateSavedForm()
	{
		if(requestrow.getString(_savedform).length()!=0)
		{
			specificcontext = new GenRow();
			loadSavedParameters();
			savedemail = true;
		}
		else
		{
			generateForm();
		}
	}

	protected void loadSavedParameters()
	{
		mailerBean.setFrom(requestrow.getString("EmailFrom"));
		mailerBean.setTo(requestrow.getString("EmailTo"));
		mailerBean.setReplyTo(requestrow.getString("EmailReplyTo"));
		mailerBean.setCc(requestrow.getString("EmailCC"));
		mailerBean.setBcc(requestrow.getString("EmailBCC"));
		mailerBean.setSubject(requestrow.getString("Subject"));
		mailerBean.setTextbody(requestrow.getString("Body"));
		mailerBean.setHtmlbody(requestrow.getString("HTMLBody"));
		specificcontext.putAll(requestrow);
	}   

	protected void mergeContexts()
	{
		// We only put those entries of sharedcontext into
		//  specific context that are not already there
		for (Object o : sharedcontext.entrySet()) {
			Map.Entry e = (Map.Entry) o;
			if (e.getValue() != null) {
				if (e.getValue() instanceof String) {
					String s = (String) e.getValue();
					if (s.trim().length() > 0) {
						specificcontext.put(e.getKey(), e.getValue());
					} else {
						if (!specificcontext.containsKey(e.getKey())) {
							specificcontext.put(e.getKey(), e.getValue());
						}
					}
				} else {
					specificcontext.put(e.getKey(), e.getValue());
				}
			}
		}      
		setNoteID();
	}

	public MailerBean getMailerBean()
	{
		return(mailerBean);
	}

	public String generateEmailSource()
	{
		generate();
		setHeaders();
		return(mailerBean.getMessageSource());
	}

	public String send()
	{
		setHeaders();

		if(mailerBean.getTo() == null || mailerBean.getTo().length() == 0)
		{
			return (Emailer.TOEMPTY);
		}

		if(mailerBean.getFrom() == null || mailerBean.getFrom().length() == 0)
		{
			return (Emailer.FROMEMPTY);
		}      

		String status = sendEmail();

		if("Message Sent".equals(status))
		{
			return(Emailer.SENT);
		}
		return(status);
	}

	protected void setNoteParameters(GenRow notebean)
	{
		EmailerSubtype subtype = subtypehead;
		while(subtype!=null)
		{
			subtype.setNoteParameters(notebean);
			subtype = subtype.getNext();
		}   
	}

	public void save(String status)
	{
		if (Emailer.SENT.equals(status) || !offlinemode) { 
			saveNote(status);
			saveDocuments();
		}
	}

	/**
	 * By default, pull this out of the requestrow or specific context. 
	 * @return (String)GroupID
	 * 
	 * @since 28/06/2011 it now uses specificcontext GroupID 1st then the request
	 */
	protected String getNoteGroupID() {  	   
		return specificcontext.getString("GroupID").length()==0?requestrow.getString("GroupID"):specificcontext.getString("GroupID");
	}

	protected void saveNote(String status)
	{
		GenRow row = new GenRow();
		row.setTableSpec("Notes");
		row.setConnection(getConnection());
		row.putAll(specificcontext);
		setNoteEmailParameters(row);
		setNoteParameters(row);

		row.setParameter("NoteID", specificcontext.getString("NoteID"));
		row.setParameter("CodeLink", KeyMaker.generate(3));
		
		specificcontext.put("CodeLinkID", row.getString("CodeLink"));


		if(requestrow.getString("-action").equals("update"))
		{
			row.setAction(requestrow.getString("-action"));
		}
		else
		{
			if (row.getString("NoteDate").length() == 0) row.setParameter("NoteDate", "NOW");
			if (row.getString("NoteTime").length() == 0) row.setParameter("NoteTime", "NOW"); 
			row.setParameter("CreatedDate","NOW");
			row.setParameter("CreatedBy",currentUser.getCurrentUserID());
			row.setAction("insert");
		}

		row.setParameter("GroupID", getNoteGroupID());
		row.setParameter("ModifiedDate","NOW");
		row.setParameter("ModifiedBy",currentUser.getCurrentUserID());
		try {
			row.doAction();
		} catch(Exception e) {
			// RegionID passed for emails is too long to be stored in RegionID column
			logger.debug("Note save failed, try after removing regionid {}", e.getMessage());
			boolean regionPricing = "true".equals(InitServlet.getSystemParam("RegionPricingEnabled"));
			if(regionPricing) {
				row.remove("RegionID");
				logger.debug("Note save second attempt after removing regionid {}");
				row.doAction();
				logger.debug("Note save second attempt after removing regionid {}" , row.getStatement());
			}
		}


		row = new GenRow();
		row.setTableSpec("Emails");
		row.setConnection(getConnection());
		row.putAll(specificcontext);
		setEmailParameters(row);    
		row.setParameter("EmailID", specificcontext.getString("NoteID"));
		if(requestrow.getString("-action").equals("update"))
		{
			row.setAction(requestrow.getString("-action"));
		}
		else
		{
			row.setAction("insert");
		}   
		if(status!=null && status.equals(SENT))
		{
			row.setParameter("Status", mailerBean.isTracked()? SENTOUTCOME : SENT);
			row.setParameter("SentDate", "NOW");
		} else {
			row.setParameter("Status", status);
		}
		row.setParameter("TemplateUsed", requestrow.getString("TemplateID"));
		row.setParameter("CMSUsed", requestrow.getString("CMSID"));   
		row.doAction();
		
	}

	private Collection getAttachments(String noteID) {
		if (EmailAttachmentBuilder.hasEmailAttachmentBuilder(noteID)) {
			EmailAttachmentBuilder builder = EmailAttachmentBuilder.getEmailAttachmentBuilder(noteID);
			return builder.getAttachedFiles();
		}
		else {
			return null;
		}
	}   

	private void saveDocuments()
	{
		String noteID = specificcontext.getString("NoteID");

		boolean hasattachment = false;
		if(offlinemode){
			hasattachment = EmailAttachmentBuilder.hasEmailAttachmentBuilder(requestrow.getString(ATTACHMENTID));
		}
		else{
			hasattachment = EmailAttachmentBuilder.hasEmailAttachmentBuilder(noteID);
		}

		if (hasattachment) {
			String contactID = requestrow.getString("ContactID");
			String companyID = requestrow.getString("CompanyID");
			String opportunityID = requestrow.getString("OpportunityID");

			EmailAttachmentBuilder builder = null;

			if(offlinemode)
			{
				builder = EmailAttachmentBuilder.getEmailAttachmentBuilder(requestrow.getString(ATTACHMENTID));
			}
			else
			{
				builder = EmailAttachmentBuilder.getEmailAttachmentBuilder(noteID);
			}

			Iterator iter = builder.getUploadedFiles().iterator();
			File file = null;
			File tempDir = null;
			File parentDir = null;
			while (iter.hasNext()) {
				file = (File)iter.next();
				if (file != null) {
					if (parentDir == null) {
						tempDir = file.getParentFile();
						parentDir = tempDir.getParentFile().getParentFile();
						parentDir = new File(parentDir,KeyMaker.generate());
						parentDir.mkdir();
					}             
					saveDocument(builder, file, parentDir);
				}
			}
			if (tempDir != null) {
				tempDir.delete();
			}

			iter = builder.getDocumentIDs().iterator();
			while (iter.hasNext()) {
				String documentID = (String)iter.next();

				GenRow doc = new GenRow();
				doc.setConnection(getConnection());
				doc.setViewSpec("LinkedDocumentView");
				doc.setColumn("LinkedDocumentID",KeyMaker.generate());
				doc.setColumn("DocumentID",documentID);
				doc.setColumn("NoteID",noteID);
				doc.setColumn("ContactID",contactID);
				doc.setColumn("OpportunityID",opportunityID);
				doc.setColumn("CompanyID",companyID);
				doc.setColumn("CreatedDate","NOW");
				doc.setColumn("CreatedBy",currentUser.getCurrentUserID());
				doc.setAction("insert");
				doc.doAction();
			}

			builder.removeAllFiles();
		}
	}   

	protected void saveDocument(EmailAttachmentBuilder builder, File file, File parentDir)
	{
		File movedFile = new File(parentDir,file.getName());
		file.renameTo(movedFile);
		String filePath = getDocumentFilePath(movedFile);
		String documentID = KeyMaker.generate();

		GenRow doc = new GenRow();
		doc.setConnection(getConnection());
		doc.setViewSpec("DocumentView");
		doc.putAll(requestrow);
		doc.setColumn("DocumentID",documentID);
		doc.setColumn("DocumentType","-Unspecified-");
		doc.setColumn("FileName",movedFile.getName());
		doc.setColumn("Description","Email Attachment");
		doc.setColumn("FilePath",filePath);
		doc.setColumn("DocumentCategory","MailAttachment");
		doc.setColumn("ModifiedDate","NOW");
		doc.setColumn("CreatedDate","NOW");
		doc.setColumn("ModifiedBy",currentUser.getCurrentUserID());
		doc.setColumn("CreatedBy",currentUser.getCurrentUserID());
		doc.setAction("insert");
		doc.doAction();

		builder.moveUploadedFileToLinkedDocument(movedFile ,documentID);    
	}

	protected String getDocumentFilePath(File file)
	{
		String filePath = file.getAbsolutePath().substring(context.getRealPath("").length());
		if (File.separatorChar != '/') {
			filePath = StringUtil.replace(filePath,File.separatorChar,'/');
		}
		return(filePath);
	}

	protected void setNoteEmailParameters(GenRow row)
	{
		row.setParameter("Subject", mailerBean.getSubject());
		row.setParameter("Body", mailerBean.getTextbody());
	}   

	protected void setEmailParameters(GenRow row)
	{
		row.setParameter("EmailTo", mailerBean.getTo());
		row.setParameter("EmailFrom", mailerBean.getFrom());
		row.setParameter("EmailCC", mailerBean.getCc());
		row.setParameter("EmailBCC", mailerBean.getBcc());
		row.setParameter("EmailReplyTo", mailerBean.getReplyTo());
		row.setParameter("HTMLBody", mailerBean.getHtmlbody());
	}

	public boolean isHTMLEmail()
	{
		return(true);
	}

	public TableData getRequestRow()
	{
		return(requestrow);
	}

	public void close()
	{
		if(con!=null)
		{
			if(closeconnection)
			{
				try{
					con.close();
				}catch(Exception e){}
			}
			con = null;
		}

		if(requestrow != null)
		{
			requestrow.close();
		}
	}

	/*
   public Connection getConnection()
   {
      return(con);
   }
	 */

	 private Context ctx = null;
	 protected String dbconn;
	 protected String contextname = null;
	 protected Locale thislocale;

	 public Connection getConnection() {
		 /* we will let GenRow handle the connections
	   try {
		   if (con == null || con.isClosed()) {
			   ctx = new InitialContext();

			   DataSource ds = null;
			   try {
				   dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
				   Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
				   ds = (DataSource) envContext.lookup(dbconn);
			   } catch (Exception e) {
			   }
			   if (ds == null) {
				   ds = (DataSource) ctx.lookup(dbconn);
			   }
			   con = ds.getConnection();
		   }
	   } catch (Exception e) {
		   return null;
	   }
	   finally
	   {
		   try
		   {
			   ctx.close();
			   ctx = null;
		   }
		   catch(Exception e)
		   {
		   }
	   }

	   return con;
		  */
		 return null;
	 }


	 public TableData getSharedContext()
	 {
		 return(sharedcontext);
	 }

	 public TableData getSpecificContext()
	 {
		 return(specificcontext);
	 }

	 public ServletContext getServletContext()
	 {
		 return(context);
	 }

	 public Properties getSystemProperties()
	 {
		 return(this.sysprops);
	 }

	 /**
	  * Generate a massive token called 'Tokens' that may be used to display all posible tokens that can be used in the email. 
	  * 
	  * This should be called after the contexts have be merged.
	  */
	 public void generateTokenList() {
		 StringBuilder sb = new StringBuilder();
		 for (Object o : specificcontext.entrySet()) {
			 Map.Entry e = (Map.Entry) o;
			 if (e.getKey() != null && !"${Toekns}".equals(e.getKey())) {
				 sb.append("{").append(e.getKey()).append("} = ").append(e.getValue()).append("<br>\n");
			 }
		 }      

		 specificcontext.put("Tokens", sb.toString());       
	 }

	 public void appendError(StringBuffer errors, String msg, String recordid)
	 {
		 errors.append(msg);
		 errors.append(" - ");
		 appendLinkURL(errors, recordid);
		 errors.append("\r\n");
	 }   

	 public void setTemplate(GenRow template)
	 {
		 this.template = template;
	 }

	 public void setCheckSpam(boolean b) {
		 checkspam = b;
	 }

	 public String getSpamScore() {
		 return spamScore;
	 }

	 public String getSpamResp() {
		 return spamResp;
	 }
}