package com.sok.runway.email.subtype;

import java.sql.Connection;
import com.sok.runway.email.*;
import com.sok.framework.*;

public class AppointmentEmailer extends AbstractEmailerSubtype
{
   public static final String APPOINTMENTID = "AppointmentID";  
   
   public boolean isEmailerSubtype(int type)
   {
      return(type == Emailer.AppointmentEmailer);
   }
   
   public void loadSharedTokens(EmailerSubtypeHandler emailer)
   {

   }
   
   /**
    * @deprecated
    */
   public void loadSpecificTokens(EmailerSubtypeHandler emailer)
   {
      loadAppointmentTokens(emailer, requestrow.getString(APPOINTMENTID), emailer.getSpecificContext());
   } 
   
   public void loadSpecificTokens(EmailerSubtypeHandler emailer, String id)
   {
      loadAppointmentTokens(emailer, requestrow.getString(APPOINTMENTID), emailer.getSpecificContext());
   } 
   
   protected void loadAppointmentTokens(EmailerSubtypeHandler emailer, String appointmentID, TableData emailcontext)
   {
      if(appointmentID != null && appointmentID.length() > 0)
      {
                 
         com.sok.Debugger.getDebugger().debug("loadAppointmentTokens: First NoteID"+appointmentID);
         
         TableData appointmentBean = emailer.getBean("email/AppointmentEmailView", "NoteID"/*APPOINTMENTID*/, appointmentID);
         
         
         com.sok.Debugger.getDebugger().debug("Venue="+appointmentBean.getData("Venue")+", ");
         com.sok.Debugger.getDebugger().debug("Subject="+appointmentBean.getData("Subject")+", ");
         com.sok.Debugger.getDebugger().debug("NoteID="+appointmentBean.getData("NoteID")+", ");
         
         com.sok.Debugger.getDebugger().debug("!!!!!!!!!!!!! Appointment date :"+appointmentBean.getString("NoteDate"));
         appointmentBean.setParameter("NoteDate",appointmentBean.getData("NoteDate"));
         appointmentBean.setParameter("NoteTime",appointmentBean.getData("NoteTime"));
         
         emailcontext.put("appointment", appointmentBean);
         
         appointmentBean.setColumn("Attending",requestrow.getString("Attending"));  
         appointmentBean.setColumn("ContactID",requestrow.getString("ContactID"));  
         com.sok.Debugger.getDebugger().debug("Attending="+requestrow.getString("Attending")+", ");
//
////         RowArrayBean orderitemlist = emailer.getRowArrayBean("OrderItemsView", APPOINTMENTID, appointmentID, "SortNumber");
////         orderitemlist.setConnection(emailer.getConnection());
//
//         GenRow profileResults = new GenRow();
//         for (int i = 0; i < orderitemlist.getSize(); i++)
//         {
//
//            if(orderitemlist.get(i).getString("ProductID").length() > 0)
//            {
//               emailer.loadProfile(profileResults, "answers/OrderAnswerView", "ProductID", orderitemlist.get(i).getString("ProductID"), orderitemlist.get(i));
//            }
//         }
//
//         appointmentBean.put("orderitems", orderitemlist.getList());
//         
         appointmentBean.put("setup", emailer.getSetupBean(appointmentBean.getString("RepUserID"), null));
         appointmentBean.put("rep", emailer.getUserBean(appointmentBean.getString("RepUserID")));
      }      
   }   
   
   public void setNoteParameters(TableData notebean)
   {
      com.sok.Debugger.getDebugger().debug("<br>Inside setNoteParameters<br>");
      notebean.setParameter(APPOINTMENTID, requestrow.getString(APPOINTMENTID));
   }

   public void generateEmail(EmailerSubtypeHandler emailer)
   {
      com.sok.Debugger.getDebugger().debug("<br>Inside generateEmail<br>");
      com.sok.Debugger.getDebugger().debug("Generate email");
//      TableData specificcontext = emailer.getSpecificContext();
      
//      if(requestrow.getString("Subject").length() == 0)
//      {
//         requestrow.setColumn("Subject", newsletter.getString("Title"));
//      }

//      specificcontext.putAll(sharedcontext);
//
      emailer.generateEmail(requestrow.getString("Subject"),requestrow.getString("Body"), requestrow.getString("HTMLBody"));

//      TemplateURLs nlurllinks = new TemplateURLs();
//      nlurllinks.setConnection(emailer.getConnection());
//      nlurllinks.getCMSLinks(requestrow.getString("CMSID"), context);
      
//      specificcontext.put("urllinks", nlurllinks);
//      String temp = null;
//      
//      if(emailer.isHTMLEmail())
//      {
//         StringBuffer roottemplate = new StringBuffer();
//         roottemplate.append("#parse(\"");
//         roottemplate.append(newsletter.getString("EmailTemplate"));
//         roottemplate.append(".vt\")");
//         
//         specificcontext.setColumn("Body", mailerBean.getHtmlbody());
         
//         nlurllinks.setHTMLLinks(specificcontext);
//         temp = emailer.generateText(roottemplate.toString());
//         mailerBean.setHtmlbody(emailer.generateText(temp));
//      }
//      else {
//         mailerBean.setHtmlbody(null);
//      }
//      
//      StringBuffer textroottemplate = new StringBuffer();
//      textroottemplate.append("#parse(\"");
//      textroottemplate.append(newsletter.getString("EmailTemplate"));
//      textroottemplate.append("_text.vt\")");      
//      
//      specificcontext.setColumn("Body", mailerBean.getTextbody());      
//      
//      nlurllinks.setTextLinks(specificcontext);
//      temp = emailer.generateText(textroottemplate.toString());
//      mailerBean.setTextbody(emailer.generateText(temp));
   }
   
}