package com.sok.runway.email.subtype;

import java.sql.Connection;
import java.util.Map;

import javax.servlet.ServletContext;

import com.sok.framework.GenRow;
import com.sok.framework.MailerBean;
import com.sok.framework.RowSetBean;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.runway.email.*;

public abstract class AbstractEmailerSubtype implements EmailerSubtype
{
   protected EmailerSubtype next;
   protected MailerBean mailerBean = null;
   protected TableData sharedcontext = null;
   protected TableData requestrow = null;
   protected ServletContext context = null;
   
   public void setNext(EmailerSubtype subtype)
   {
      next = subtype;
   }
   
   public EmailerSubtype getNext()
   {
      return(next);
   }
   
   public abstract boolean isEmailerSubtype(int type);   
   
   public void init(EmailerSubtypeHandler emailer)
   {
      requestrow = emailer.getRequestRow();
      mailerBean = emailer.getMailerBean();
      sharedcontext = emailer.getSharedContext();
      context = emailer.getServletContext();
   }
   
   public abstract void loadSharedTokens(EmailerSubtypeHandler emailer);
   /**
    * @deprecated
    */
   public abstract void loadSpecificTokens(EmailerSubtypeHandler emailer);  
   
   public abstract void loadSpecificTokens(EmailerSubtypeHandler emailer, String id);  
   
   public void generateEmail(EmailerSubtypeHandler emailer)
   {

   }   
   
   public boolean overridesEmailGeneration()
   {
      return false ;
   }
   
   public abstract void setNoteParameters(TableData notebean);
   
   protected void mergeContexts(TableData specificcontext)
   {
       // We only put those entries of sharedcontext into
       //  specific context that are not already there
       for (Object o : sharedcontext.entrySet()) {
           Map.Entry e = (Map.Entry) o;
           if (e.getValue() != null) {
               if (e.getValue() instanceof String) {
                   String s = (String) e.getValue();
                   if (s.trim().length() > 0) {
                       specificcontext.put(e.getKey(), e.getValue());
                   } else {
                       if (!specificcontext.containsKey(e.getKey())) {
                           specificcontext.put(e.getKey(), e.getValue());
                       }
                   }
               } else {
                   specificcontext.put(e.getKey(), e.getValue());
               }
           }
       }      
   }

}