package com.sok.runway.email;

import javax.servlet.http.HttpSession;
import java.sql.Connection;
import com.sok.framework.TableData;

public interface OfflineEmailable
{
   public TableData getOfflineSearch(HttpSession session);
   
   public String getOfflineList();
   
   public void setConnection(Connection con);
   
   public void close();
   
   public void appendError(StringBuffer error, String msg, String recordid);
   
   public TableData getSharedContext();
   
   public TableData getSpecificContext();
}