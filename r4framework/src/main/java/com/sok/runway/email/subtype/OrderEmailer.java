package com.sok.runway.email.subtype;

import java.sql.Connection;
import java.text.DecimalFormat;

import com.sok.runway.SupportUtil;
import com.sok.runway.email.*;
import com.sok.framework.*;

public class OrderEmailer extends AbstractEmailerSubtype
{
   public static final String ORDERID = "OrderID";  
   
   public boolean isEmailerSubtype(int type)
   {
      return(type == Emailer.OrderEmailer);
   }
   
   public void loadSharedTokens(EmailerSubtypeHandler emailer)
   {

   }
   
   /**
    * @deprecated
    */
   public void loadSpecificTokens(EmailerSubtypeHandler emailer)
   {
      loadOrderTokens(emailer, requestrow.getString(ORDERID), emailer.getSpecificContext());
   } 
   
   public void loadSpecificTokens(EmailerSubtypeHandler emailer, String id)
   {
      loadOrderTokens(emailer, requestrow.getString(ORDERID), emailer.getSpecificContext());
   } 
   
   protected void loadOrderTokens(EmailerSubtypeHandler emailer, String orderID, TableData emailcontext)
   {
      if(orderID != null && orderID.length() > 0)
      {
         TableData orderBean = emailer.getBean("email/OrderEmailView", ORDERID, orderID);
         emailcontext.put("order", orderBean);
         
         DecimalFormat format = new DecimalFormat();
         format.setDecimalSeparatorAlwaysShown(false);
         
         double remHours = 0; 
         try {
        	 SupportUtil.getRemainingHours(emailer.getConnection(), orderID);
         } catch (Exception e) { /* exception is being declared for an error on isClosed(). Way overkill guys. */ } 
         if(remHours > 0)
        	 orderBean.put("RemainingHours", format.format(remHours));
         else
        	 orderBean.put("RemainingHours", format.format(0));

         RowArrayBean orderitemlist = emailer.getRowArrayBean("OrderItemsView", ORDERID, orderID, "SortNumber");
         orderitemlist.setConnection(emailer.getConnection());

         GenRow profileResults = new GenRow();
         for (int i = 0; i < orderitemlist.getSize(); i++)
         {
        	 if(orderitemlist.get(i).getString("OrderItemID").length() > 0)
             {
                emailer.loadProfile(profileResults, "answers/OrderItemAnswerView", "OrderItemID", orderitemlist.get(i).getString("OrderItemID"), orderitemlist.get(i));
                if(orderitemlist.get(i).getString("ProductID").length() > 0)
                {
                	TableData product = emailer.getBean("ProductView","ProductID", orderitemlist.get(i).getString("ProductID")); 
                    emailer.loadProfile(profileResults, "answers/ProductAnswerView", "ProductID", orderitemlist.get(i).getString("ProductID"), product);
                    orderitemlist.get(i).put("product",product); 
                }
             }
         }

         orderBean.put("orderitems", orderitemlist.getList());
         
         orderBean.put("setup", emailer.getSetupBean(orderBean.getString("RepUserID"), null));
         orderBean.put("rep", emailer.getUserBean(orderBean.getString("RepUserID")));
      }      
   }   
   
   public void setNoteParameters(TableData notebean)
   {
      notebean.setParameter(ORDERID, requestrow.getString(ORDERID));
   }

}