package com.sok.runway.email.subtype;

import java.sql.Connection;
import java.util.*;

import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.runway.*;
import com.sok.runway.campaigns.Campaign;
import com.sok.runway.email.*;

public class CampaignEmailer extends AbstractEmailerSubtype
{
   public static final String CAMPAIGNID = "CampaignID";
   
   public boolean isEmailerSubtype(int type)
   {
      return(type == Emailer.CampaignEmailer);
   }
 
   public void init(EmailerSubtypeHandler emailer)
   {
      super.init(emailer);
      setDefaultTemplateID(emailer);
   }
   
   public void loadSharedTokens(EmailerSubtypeHandler emailer)
   {
      GenRow campaignBean = new GenRow();
      campaignBean.setViewSpec("ContactCampaignView"); 
      campaignBean.setConnection(emailer.getConnection());
      campaignBean.setColumn("CampaignID",requestrow.getString(CAMPAIGNID)); 
      if(emailer.isEmailerType(Emailer.ContactEmailer))
      {
         campaignBean.setColumn("ContactID", requestrow.getString("ContactID")); 
         campaignBean.doAction("selectfirst");
      }
      else if(emailer.isEmailerType(Emailer.CompanyEmailer))
      {
         campaignBean.setColumn("CompanyID", requestrow.getString("CompanyID"));
         campaignBean.doAction("selectfirst");
      }

      sharedcontext.put("CampaignName",campaignBean.getString("Name")); 
      sharedcontext.put("CampaignStatus",campaignBean.getString("CurrentStatus")); 
      
      if(campaignBean.isSuccessful()) { 
    	  
    	  Map<String, String> groupIDs = Campaign.getLinkedGroups(emailer.getConnection(), campaignBean.getString("CampaignID")); 
    	  if(groupIDs != null) 
    	  {
    		  sharedcontext.setParameter("GroupIDs", groupIDs);
    	  }
      }
   }
   /**
    * @deprecated
    */
   public void loadSpecificTokens(EmailerSubtypeHandler emailer)
   {
      
   }
   
   public void loadSpecificTokens(EmailerSubtypeHandler emailer, String id)
   {
      
   }
   
   public void setNoteParameters(TableData notebean)
   {
      
   }   
   
   /**
    * We only want to set a template from the campaign if one has not been specified in the request 
    * and a newsletter has not been specified - this would be the case for email campaigns
    * @param emailer Subtypehandler 
    */
   protected void setDefaultTemplateID(EmailerSubtypeHandler emailer)
   {
      if (!requestrow.isSet("TemplateID") && !requestrow.isSet("CMSID")){
         GenRow eventTemplates = new GenRow();
         eventTemplates.setViewSpec("ContactEventView");
         eventTemplates.setConnection(emailer.getConnection());
         eventTemplates.setColumn("ContactEventCampaign.CampaignID", requestrow.getString(CAMPAIGNID));
         eventTemplates.setColumn("Status", "Active");
         if(emailer.isEmailerType(Emailer.ContactEmailer))
         {         
            eventTemplates.setColumn("ContactEventCampaign.ContactID", requestrow.getString("ContactID"));
            eventTemplates.doAction("selectfirst");
         }
         else if(emailer.isEmailerType(Emailer.CompanyEmailer))
         {
            eventTemplates.setColumn("ContactEventCampaign.CompanyID", requestrow.getString("CompanyID"));
            eventTemplates.doAction("selectfirst");
         }

         if (eventTemplates.isSuccessful()) {
            requestrow.setColumn("TemplateID", eventTemplates.getString("TemplateID"));
         }
      }      
   }
}