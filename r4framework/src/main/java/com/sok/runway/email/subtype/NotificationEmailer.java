package com.sok.runway.email.subtype;

import java.sql.Connection;
import java.util.*;

import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.runway.*;
import com.sok.runway.email.*;

public class NotificationEmailer extends AbstractEmailerSubtype
{
   Properties sysprops;
   
   public boolean isEmailerSubtype(int type)
   {
      return(type == Emailer.NotificationEmailer);
   }
 
   public void init(EmailerSubtypeHandler emailer)
   {
      super.init(emailer);      
      sysprops = emailer.getSystemProperties();
      if (requestrow.getString("TemplateID").length() == 0) {
         String notificationTemplate = sysprops.getProperty("NotificationTemplateID");
         if (notificationTemplate != null) {
            requestrow .put("TemplateID", notificationTemplate);
         } else {
            generateNotification(emailer);
         }
      }
   }
   
   protected void generateNotification(EmailerSubtypeHandler emailer)
   { 
      if(emailer.isEmailerType(Emailer.CompanyEmailer) || emailer.isEmailerType(Emailer.ContactEmailer))
      {
         GenRow template = new GenRow();
         template.setViewSpec("TemplateView");
         template.replaceData("From",emailer.getCurrentUser().getEmail()); 
         
         StringBuffer body = new StringBuffer(); 
         body.append("\r\n"); 
         body.append("${URLHome}"); //normally appendLinkURL will take care of this 
         //but templates are loaded before shared contexts
         if(requestrow.getString("ParentNoteID").length()!=0)
         {
            template.replaceData("Subject","Runway Note Link"); 
            appendNoteLinkURL(body,"${ParentNoteID}");
         }
         else if(emailer.isEmailerType(Emailer.CompanyEmailer))
         {
            template.replaceData("Subject","Runway Company Link"); 
            emailer.appendLinkURL(body,"${CompanyID}");
         }
         else if(emailer.isEmailerType(Emailer.ContactEmailer))
         {
            template.replaceData("Subject","Runway Contact Link"); 
            emailer.appendLinkURL(body,"${ContactID}");
         }
         template.replaceData("Body",body.toString()); 
         
         emailer.setTemplate(template);
      }
   }   
   
   public void appendNoteLinkURL(StringBuffer buffer, String recordid)
   {
      buffer.append(sharedcontext.getString("URLHome"));
      buffer.append("/runway/note.view?NoteID=");
      buffer.append(recordid);
   }     
   
   public void loadSharedTokens(EmailerSubtypeHandler emailer)
   {

   }
   /**
    * @deprecated
    */
   public void loadSpecificTokens(EmailerSubtypeHandler emailer)
   {
      loadSpecificTokens(emailer, null);
   }
   
   public void loadSpecificTokens(EmailerSubtypeHandler emailer, String id)
   {
      TableData specificcontext = emailer.getSpecificContext();
      UserBean currentUser = emailer.getCurrentUser();
      GenRow reminders = new GenRow();
      reminders.setConnection(emailer.getConnection());
      reminders.setTableSpec("Notes");
      reminders.setParameter("-select#1", "count(*) AS theTotal");
      reminders.setConnection(emailer.getConnection());
      reminders.setParameter("Type","Reminder");
      reminders.setParameter("Completed","No");
      reminders.setParameter("RepUserID",requestrow.getString("RepUserID"));
      currentUser.setConstraint(reminders,"NoteGroup");
      reminders.setParameter("NoteDate", "OVERDUE");
      reminders.doAction(GenerationKeys.SELECTFIRST);

      specificcontext.put("RemindersOVERDUE",reminders.getData("theTotal"));
      
      reminders.setParameter("NoteDate", "TODAY");
      reminders.doAction(GenerationKeys.SELECTFIRST);
      
      specificcontext.put("RemindersTODAY",reminders.getData("theTotal"));
      
      if (sysprops.getProperty("ContactUnqualifiedStatusID") != null) {
         reminders.clear();
         reminders.setConnection(emailer.getConnection());
         reminders.setTableSpec("Contacts");
         reminders.setParameter("-select#1", "count(*) AS theTotal");
         reminders.setConnection(emailer.getConnection());
         reminders.setParameter("RepUserID",requestrow.getString("RepUserID"));
         reminders.setParameter("ContactCurrentStatus.StatusID",sysprops.getProperty("ContactUnqualifiedStatusID"));
         currentUser.setConstraint(reminders,"ContactGroup");
         reminders.doAction(GenerationKeys.SELECTFIRST);
         
         specificcontext.put("ContactsUnqualified", reminders.getData("theTotal"));
      }
      
      if(requestrow.getString("ParentNoteID").length()!=0)
      {
         loadParentNoteTokens(emailer, requestrow.getString("ParentNoteID"));
      }
   }
   
   protected void loadParentNoteTokens(EmailerSubtypeHandler emailer, String id) { 
      TableData specificcontext = emailer.getSpecificContext();
       if(id.length()>0) { 
          GenRow noteBean = new GenRow(); 
          noteBean.setConnection(emailer.getConnection());
          noteBean.setViewSpec("NoteView"); 
          noteBean.setColumn("NoteID",id); 
          noteBean.doAction(GenerationKeys.SELECT);  
          
          loadParentNoteProfileTokens(emailer, noteBean, id);
          
          EmailerMap map = new EmailerMap(noteBean);
          specificcontext.put("parentnote",map);
       }
    }      
   
   protected void loadParentNoteProfileTokens(EmailerSubtypeHandler emailer, TableData map, String id)
   {
       if (id.length() > 0) {
          RowSetBean questionList = new RowSetBean();
          questionList.setConnection(emailer.getConnection());
          
          questionList.setViewSpec("answers/NoteAnswerView");
          questionList.setColumn("NoteID",id);
          questionList.setColumn("NoteAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.NoteType","!EMPTY");
          questionList.setColumn("NoteAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.NoteProfile","Y");
          questionList.setColumn("NoteAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.Disabled","N+EMPTY+NULL");
          
          questionList.generateSQLStatment();
          questionList.getResults();
          while(questionList.getNext()) {
             map.put("Profile-"+StringUtil.ToAlphaNumeric(questionList.getString("QuestionLabel")),questionList.getString("Answer"));
             map.put("Profile-"+questionList.getString("QuestionID"),questionList.getString("Answer"));
          }
          map.put("ProfileStatus",questionList.getError());
          map.put("ProfileStatement",questionList.getStatement());
          questionList.close();
       }
    } 
   
   public void setNoteParameters(TableData notebean)
   {
      
   }
  
}