package com.sok.runway.email;

import javax.servlet.*;
import javax.servlet.http.*;
import com.sok.framework.*;
import com.sok.runway.*;
import java.sql.Connection;
import java.util.*;

public interface EmailerSubtypeHandler extends Emailer
{  
   public Connection getConnection();
   
   public TableData getSharedContext();
   
   public TableData getSpecificContext();
   
   public ServletContext getServletContext();
   
   public String generateText(String template);
   
   public void generateEmail(String subjecttemplate, String texttemplate,
         String htmltemplate);
   
   public Properties getSystemProperties();

   public TableData getBean(String viewspec, String idname, String idvalue);
   
   public RowArrayBean getRowArrayBean(String viewspec, String idname, String idvalue, String sort);
   
   public ArrayList<TableData> getBeanArray(String viewspec, String idname, String idvalues);
   
   public TableData getUserBean(String userID);
   
   public TableData getSetupBean(String userID, String setupID);
      
   public void loadProfile(GenRow genrow, String viewspec, String keyname, String keyvalue, TableData context);
   
   public void loadProfile(String viewspec, String keyname, String keyvalue, TableData context);

   public void setTemplate(GenRow template);
   
   public TableData getTableData(String viewspec, String idname, String idvalue);   
}