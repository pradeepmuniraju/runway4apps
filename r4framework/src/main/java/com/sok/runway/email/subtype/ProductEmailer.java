package com.sok.runway.email.subtype;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.RunwayUtil;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.ViewSpec;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.ExistingProperty;
import com.sok.runway.email.Emailer;
import com.sok.runway.email.EmailerSubtypeHandler;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.DripService;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

public class ProductEmailer extends AbstractEmailerSubtype
{
	private static final Logger logger = LoggerFactory.getLogger(ProductEmailer.class);
	
	public static final String PRODUCTID = "ProductID";
	public static final String PRODUCTIDS = "ProductIDs";

	protected String productviewspec = "ProductView";
	protected String productanswerviewspec = "{answers/}ProductAnswerView";
	
	DripService ds = DripService.getInstance();
	protected static final UserService us = UserService.getInstance();

	public boolean isEmailerSubtype(int type)
	{
		return(type == Emailer.ProductEmailer);
	}

	public void loadSharedTokens(EmailerSubtypeHandler emailer)
	{
		String productID = requestrow.getString(PRODUCTID);
		if (productID.length() > 0) {
			loadProductTokens(emailer, productID, sharedcontext);
		}
		String productIDs = requestrow.getString(PRODUCTIDS);
		if (productIDs.length() > 0) {
			loadProductListTokens(emailer, productIDs, sharedcontext);
		}

		sharedcontext.put("ProductDetailLoader", new ProductDetailLoader(emailer.getConnection()));
	}
	/**
	 * @deprecated
	 */
	public void loadSpecificTokens(EmailerSubtypeHandler emailer)
	{
	}  

	public void loadSpecificTokens(EmailerSubtypeHandler emailer, String id)
	{
		if(emailer.isEmailerType(Emailer.ContactEmailer)) {
			String contactID = requestrow.getString("ContactID");
			if(StringUtils.isNotBlank(contactID)) { 
				try { 
					String productID = requestrow.getString(PRODUCTID);
					if (productID.length() > 0) {
						String[] ids = productID.split("\\+");
						for(String pId: ids) { 
							linkContactProduct(emailer, contactID, pId);
						}
					}
					String productIDs = requestrow.getString(PRODUCTIDS);
					if (productIDs.length() > 0) {
						String[] ids = productIDs.split("\\+");
						for(String pId: ids) {
							linkContactProduct(emailer, contactID, pId);
						}
					}
				} catch (Exception e) { 
					e.printStackTrace();
				}
			}
		}
	}  
	
	/* emailing is (currently) single threaded, so a single worker will be created for any email process */
	private GenRow worker = null;
	private void linkContactProduct(EmailerSubtypeHandler emailer, String contactID, String productID) {
		if(StringUtils.isNotBlank(contactID) && StringUtils.isNotBlank(productID)) { 
			if(worker == null) {
				worker = new GenRow();
				worker.setConnection(emailer.getConnection());
				worker.setTableSpec("ContactProducts"); 
			} else {
				worker.clear();
			}
			worker.setParameter("-select1","ContactProductID");
			worker.setParameter("ContactID", contactID);
			worker.setParameter("ProductID", productID);
			worker.doAction(GenerationKeys.SELECTFIRST);
			if(!worker.isSuccessful()) { 
				worker.createNewID();
				worker.doAction(GenerationKeys.INSERT);
			}
		}
	}

	protected void loadProductTokens(EmailerSubtypeHandler emailer, String productID, TableData emailcontext)
	{
		TableData productBean = getProductBean(productID, emailer);
		emailer.loadProfile(productanswerviewspec, PRODUCTID, productID, productBean);
		emailcontext.put("product", productBean);
	}

	protected void loadProductListTokens(EmailerSubtypeHandler emailer, String productIDs, TableData emailcontext)
	{
		ArrayList<TableData> products = getProductBeanArray(productIDs, emailer);

		GenRow profileResults = new GenRow();
		TableData data = null;
		for(int i=0; i<products.size(); i++) {
			data = products.get(i);
			emailer.loadProfile(profileResults, productanswerviewspec, PRODUCTID, data.getString(PRODUCTID), data);
		}
		emailcontext.put("products", products);
	}   

	protected ArrayList<TableData> getProductBeanArray(String idvalues, EmailerSubtypeHandler emailer)
	{
		String[] ids = idvalues.split("\\+");
		ArrayList<TableData> items = new ArrayList<TableData>(ids.length);
		for(int i=0; i<ids.length; i++){
			if(ids[i]!=null && ids[i].length()!=0){
				items.add(getProductBean(ids[i], emailer, i));
			}
		}
		return(items);
	}

	protected ArrayList<TableData> getLinkedDocumentArray(String idvalue, EmailerSubtypeHandler emailer)
	{
		GenRow documents = new GenRow();
		documents.setConnection(emailer.getConnection());
		documents.setTableSpec("LinkedDocuments");
		documents.setParameter("-select1", "LinkedDocumentID");
		documents.setParameter(PRODUCTID, idvalue);
		documents.setParameter("ShowOnEmail", "Y");
		documents.setParameter("-sort1", "SortOrder");
		documents.doAction(ActionBean.SEARCH);
		documents.getResults();

		ArrayList<TableData> items = new ArrayList<TableData>();
		while(documents.getNext()){
			items.add(emailer.getTableData("LinkedDocumentView", "LinkedDocumentID", documents.getData("LinkedDocumentID")));
		}
		documents.close();
		return(items);
	}

	protected TableData getProductBean(String idvalue, EmailerSubtypeHandler emailer)
	{      
		return getProductBean(idvalue, emailer, -1);
	}
	
	protected TableData getProductBean(String idvalue, EmailerSubtypeHandler emailer, int regionIndex)
	{      
		String producttype = getProductType(idvalue, emailer.getConnection());

		TableData bean = null;
		if("Home".equals(producttype)){
			bean = getHome(idvalue, emailer);
		}
		else if("House and Land".equals(producttype)){
			bean = getHouseAndLand(idvalue, emailer);
		}
		else if("Home Plan".equals(producttype)){
			bean = getPlan(idvalue, emailer);
		}
		else if("Home Range".equals(producttype)){
			bean = getRange(idvalue, emailer);
		}
		else if("Land".equals(producttype)){
			bean = getLot(idvalue, emailer, regionIndex);
		}
		else if("Estate".equals(producttype)){
			bean = emailer.getTableData("properties/ProductHomeView", PRODUCTID, idvalue);
		}
		else if("Stage".equals(producttype)){
			bean = getStage(idvalue, emailer);
		}
		else if("Brand".equals(producttype)){
			bean = emailer.getTableData("properties/ProductHomeView", PRODUCTID, idvalue);
		}
		else if("Developer".equals(producttype)){
			bean = emailer.getTableData("properties/ProductHomeView", PRODUCTID, idvalue);
		}
		else if("Apartment".equals(producttype)){
			bean = getApartments(idvalue, emailer);
		}
		else if("Existing Property".equals(producttype)){
			bean = getExistingProperties(idvalue, emailer);
		}
		else{
			bean = emailer.getBean(productviewspec, PRODUCTID, idvalue);
		}
		bean.put("quickproduct", loadProductQuickTokens(idvalue, emailer));		

		try {
			boolean regionPricing = "true".equals(InitServlet.getSystemParam("RegionPricingEnabled"));
			if(regionPricing && regionIndex != -1) {
				setRegionName(bean, regionIndex);
			}
		} catch(Exception ee) {
			logger.debug("Exception occurred while setting RegionName from Request RegionID. Exception message is: " + ee.getMessage());
		}
		
		return(bean);
	}
	
	private TableData loadProductQuickTokens(String idvalue, EmailerSubtypeHandler emailer) {
		GenRow quickIndex = new GenRow();
		quickIndex.setViewSpec("{properties/}ProductQuickIndexView");
		quickIndex.setParameter("ProductID", idvalue);
		quickIndex.doAction("selectfirst");
		
		TableData bean = new GenRow(); //emailer.getTableDataSelectFirst("properties/ProductQuickIndexView", PRODUCTID, idvalue);
		
		if (quickIndex.isSuccessful()) {
			ViewSpec vs = quickIndex.getViewSpec();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				bean.put(vs.getLocalItemName(i), quickIndex.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				bean.put(vs.getRelatedItemName(i), quickIndex.getString(vs.getRelatedItemName(i)));
			}
			
			String fullName = "";
			if ("Land".equals(quickIndex.getString("ProductType"))) {
				if (!quickIndex.getString("Name").startsWith("Lot") && !quickIndex.getString("Name").startsWith("Block")) fullName = "Lot ";
				fullName += quickIndex.getString("Name") + " " + quickIndex.getString("StageName") + " " + quickIndex.getString("EstateName"); 
			} else if ("Home Plan".equals(quickIndex.getString("ProductType"))) {
				if (!quickIndex.getString("Name").startsWith("Lot") && !quickIndex.getString("Name").startsWith("Block")) fullName = "Lot ";
				fullName += quickIndex.getString("DesignName") + " " + quickIndex.getString("Name") + ", " + quickIndex.getString("RangeName"); 
			} else if ("Apartment".equals(quickIndex.getString("ProductType"))) {
				if (!quickIndex.getString("Name").startsWith("Apartment")) fullName = "Apartment ";
				fullName += quickIndex.getString("Name") + " Level " + quickIndex.getString("StageName") + ", " + quickIndex.getString("EstateName"); 
			} else {
				fullName = quickIndex.getString("Name");
			}

			bean.put("ProductQuick_FullName", fullName);
		}
		
		return bean;
	}
	protected TableData getHouseAndLand(String idvalue, EmailerSubtypeHandler emailer)
	{
		TableData bean = emailer.getTableData("properties/ProductHouseAndLandView", PRODUCTID, idvalue);
		String planproductid = getLinkedProductID(idvalue, "Home Plan", emailer.getConnection());
		String landproductid = getLinkedProductID(idvalue, "Land", emailer.getConnection());
		String facadeproductid = getLinkedProductID(idvalue, "Home Facade", emailer.getConnection());
		
		if(planproductid!=null && planproductid.length()!=0){
			bean.put("planproduct", getPlan(planproductid, emailer));
		}
		if(landproductid!=null && landproductid.length()!=0){
			bean.put("lotproduct", getLot(landproductid, emailer, -1));
		}
		if(facadeproductid!=null && facadeproductid.length()!=0){
			bean.put("facadeproduct", emailer.getBean(productviewspec, PRODUCTID, facadeproductid));
		}
		String estateProductId = bean.getData("EstateProductID");
		if(estateProductId != null && !estateProductId.equalsIgnoreCase("")) {
			bean.put("estateproduct", getEstate(estateProductId, emailer));
		}
		try
		{
			String RoundedPrice = bean.getData("TotalCost");
			if (RoundedPrice != null) {
					try {
						RoundedPrice = StringUtil.getRoundedFormattedValue(RoundedPrice, false);
						
					} catch (Exception e) {}
			} 
			bean.put("RoundedPrice", RoundedPrice);
		}
		catch(Exception ee) {}
		
		
		try
		{
			ArrayList FeaturesList  = getInclusionList(bean.getData("ProductID"));
			String listString = "";
			for (int i=0; i<FeaturesList.size(); i++) 
			{
			    listString += FeaturesList.get(i) + "\n";
			}
			//System.out.println("PRODUCTID is: " + PRODUCTID +  "  , FeaturesList.size is: " + FeaturesList.size() + " , FeaturesList is: " + FeaturesList);
			bean.put("FeaturesList", listString);
			
			bean.put("CurrentDate", getCurrentDate());
		}
		catch(Exception ee) {
			ee.printStackTrace();
		}


		try
		{
			setBrandProductDetails (bean);
		}
		catch(Exception ee) {
			ee.printStackTrace();
		}

		

		
		return(bean);
	}

	protected TableData getApartments(String idvalue, EmailerSubtypeHandler emailer)
	{
			TableData bean = emailer.getTableData("properties/ProductApartmentView", PRODUCTID, idvalue);
			
			// Getting the Dimenstions
			Map<String, Object> dimensionsMap = new HashMap<String, Object>();
			dimensionsMap = getDimensions(idvalue);
	
			String levels = "0";
			if (dimensionsMap.get("Levels.NumberOf") != null && !"0.0".equalsIgnoreCase((String)dimensionsMap.get("Levels.NumberOf")))
			{	
				levels = dimensionsMap.get("Levels.NumberOf").toString();
			}
			int intLevels = new Double(levels).intValue();

			String floorLevel = "0";
			if (dimensionsMap.get("Level.Alphavalue") != null)
			{
				floorLevel = dimensionsMap.get("Level.Alphavalue").toString();
			}	
			
			String apartmentArea = "0";
			if (dimensionsMap.get("ApartmentArea.SQmeters") != null)
				apartmentArea = dimensionsMap.get("ApartmentArea.SQmeters").toString();
			double doubleApartmentArea = Double.parseDouble(apartmentArea);
					

			
			
			String balconyAreaLabel = "Balcony Area";
			String balconyArea = "0";
			if (dimensionsMap.get("Balcony.SQmeters") != null && !"0.0".equalsIgnoreCase((String)dimensionsMap.get("Balcony.SQmeters")))
			{	
				balconyArea = dimensionsMap.get("Balcony.SQmeters").toString();
			}
			else if (dimensionsMap.get("BalconyArea.SQmeters") != null && !"0.0".equalsIgnoreCase((String)dimensionsMap.get("BalconyArea.SQmeters")))
			{
				balconyArea = dimensionsMap.get("BalconyArea.SQmeters").toString();
				balconyAreaLabel = "Balcony Area";
			}
			else if (dimensionsMap.get("CourtyardArea.SQmeters") != null && !"0.0".equalsIgnoreCase((String)dimensionsMap.get("CourtyardArea.SQmeters")))
			{	
				balconyArea = dimensionsMap.get("CourtyardArea.SQmeters").toString();
				balconyAreaLabel = "Courtyard Area";
			}	
			double doubleBalconyArea = Double.parseDouble(balconyArea);
			
			
			
			
			
			
			double dTotalArea = 0;
			dTotalArea = doubleApartmentArea + doubleBalconyArea; 
	
			String carSpaces = "0";
			if (dimensionsMap.get("CarParks.NumberOf") != null)
				carSpaces = dimensionsMap.get("CarParks.NumberOf").toString();
			int intCarSpaces = new Double(carSpaces).intValue();
	

			String study = "No";
			if (dimensionsMap.get("Study.YesNo") != null)
				study = dimensionsMap.get("Study.YesNo").toString();
			if ("Y".equals(study))
			{
				study = "Yes";
			}
			else
			{
				study = "No";
			}	
			
			String storage = "No";
			if (dimensionsMap.get("Storage.YesNo") != null)
				storage = dimensionsMap.get("Storage.YesNo").toString();
			if ("Y".equals(storage))
			{
				storage = "Yes";
			}
			else
			{
				storage = "No";
			}
			
			String bedrooms = "0";
			if (dimensionsMap.get("Bedrooms.NumberOf") != null)
				bedrooms = dimensionsMap.get("Bedrooms.NumberOf").toString();
			int intBedrooms = new Double(bedrooms).intValue();

			String bathrooms = "0";
			if (dimensionsMap.get("Bathrooms.NumberOf") != null)
				bathrooms = dimensionsMap.get("Bathrooms.NumberOf").toString();
			// 10-Oct-2012 NI:  int intBathrooms = new Double(bathrooms).intValue();
			double doubleBathrooms = Double.parseDouble(bathrooms);
			bathrooms = doubleBathrooms + "";
			bathrooms = bathrooms.replace(".0","");

			String PowderRoom = "0";
			if (dimensionsMap.get("PowderRoom.NumberOf") != null)
				PowderRoom = dimensionsMap.get("PowderRoom.NumberOf").toString();
			int intPowderRoom = new Double(PowderRoom).intValue();

			
			/*String garage = "0";
			if (dimensionsMap.get("Garage.NumberOf") != null)
				garage = dimensionsMap.get("Garage.NumberOf").toString();
			int intGarage = new Double(garage).intValue();			
			*/
			
			String garage = "0";
			String garageLabel = "Car Bays";
			if (dimensionsMap.get("Garage.NumberOf") != null && !"0.0".equalsIgnoreCase((String)dimensionsMap.get("Garage.NumberOf")))
			{	
				garage = dimensionsMap.get("Garage.NumberOf").toString();
				garageLabel = "Garage";
			}
			else if (dimensionsMap.get("CarBays.NumberOf") != null && !"0.0".equalsIgnoreCase((String)dimensionsMap.get("CarBays.NumberOf")))
			{	
				garage = dimensionsMap.get("CarBays.NumberOf").toString();
				garageLabel = "Car Bays";
			}	
			else if (dimensionsMap.get("CarPort.NumberOf") != null  && !"0.0".equalsIgnoreCase((String)dimensionsMap.get("CarPort.NumberOf")))
			{	
				garage = dimensionsMap.get("CarPort.NumberOf").toString();
				garageLabel = "Car Ports";
			}	
			int intGarage = new Double(garage).intValue();

			String apartmentLotNumber = "";
			if (dimensionsMap.get("ApartmentLotnumber.Alphavalue") != null)
			{	
				apartmentLotNumber = dimensionsMap.get("ApartmentLotnumber.Alphavalue").toString();
			}
			bean.put("ApartmentLotNumber", apartmentLotNumber);
			
			
			bean.put("Levels", intLevels); // Used for Quest
			bean.put("FloorLevel", floorLevel); // Used for Brock Urban
			
			bean.put("ApartmentAreaSqm", doubleApartmentArea);
			
			bean.put("BalconySqm", doubleBalconyArea);
			bean.put("BalconyAreaLabel", balconyAreaLabel);
			
			bean.put("TotalArea", dTotalArea);
			bean.put("CarParks", intCarSpaces);
			
			bean.put("Study", study);
			bean.put("Storage", storage);
			bean.put("Bedrooms", intBedrooms);
			bean.put("Bathrooms", bathrooms);
			bean.put("PowderRoom", intPowderRoom);
			bean.put("Garage", intGarage);
			bean.put("GarageLabel", garageLabel);
			
			
			
			// 04-Oct-2012 NI: Added to store the Building Address
			String buildingID = bean.getString("BuildingProductID");
			GenRow buildingDetailsQuery = new GenRow(); 
			buildingDetailsQuery.setViewSpec("ProductView");
			buildingDetailsQuery.setParameter("ProductType", "Building");
			buildingDetailsQuery.setAction(GenerationKeys.SEARCH);

			if (!StringUtil.isBlankOrEmpty(buildingID))
			{
				buildingDetailsQuery.setParameter("ProductID", buildingID);
			}

			buildingDetailsQuery.doAction();
			buildingDetailsQuery.getResults();
			buildingDetailsQuery.getNext();
			
			String buildingStreet = buildingDetailsQuery.getString("Street");
			if (!StringUtil.isBlankOrEmpty(buildingDetailsQuery.getString("Street2")))
			{
				buildingStreet += " " + buildingDetailsQuery.getString("Street2");
			}
			
			String buildingCity = buildingDetailsQuery.getString("City");
			String buildingState = buildingDetailsQuery.getString("State");
			String buildingPostcode = buildingDetailsQuery.getString("Postcode");
			buildingDetailsQuery.close();
			
			bean.put("BuildingStreet", buildingStreet);
			bean.put("BuildingCity", buildingCity);
			bean.put("BuildingState", buildingState);
			bean.put("BuildingPostcode", buildingPostcode);
			
			
			
			
			
			
			
			GenRow imagesList = new GenRow();
			imagesList.setViewSpec("LinkedDocumentView");
			
			ArrayList<String> apartmentImgList = new ArrayList<String>();
			ArrayList<String> aptFloorPlanImgList = new ArrayList<String>();
			
			try { 
				imagesList.setConnection(emailer.getConnection());
				imagesList.setParameter("ProductID", idvalue);
				
				if(imagesList.isSet("ProductID")) { 
					if(logger.isTraceEnabled()) {
						imagesList.doAction(GenerationKeys.SEARCH);
						logger.trace("Apartments Images Query is: {}", imagesList.getStatement());
					}
					imagesList.getResults();
				}
				
				String imgPath = "";
				String imgType = "";
				while (imagesList.getNext())  
				{
						imgPath = imagesList.getString("FilePath");
						imgType = imagesList.getString("DocumentSubType");
						if (imgPath!=null && !"".equalsIgnoreCase(imgPath))
						{
							if ("Floor Plan".equalsIgnoreCase(imgType))
							{
								aptFloorPlanImgList.add(imgPath);	
							}
							else
							{
								apartmentImgList.add(imgPath);	
							}
							
						}	
				}
			} finally { 
				imagesList.close();
			} 
			
			String aptImg1 = "";
			if (apartmentImgList!=null && !apartmentImgList.isEmpty())
			{
				aptImg1 = apartmentImgList.get(0);
				aptImg1 = aptImg1.replaceAll(" ","%20");
			}	
			bean.put("ApartmentImage1", aptImg1);
	
			String floorPlanImg1 = "";
			if (aptFloorPlanImgList!=null && !aptFloorPlanImgList.isEmpty())
			{
				floorPlanImg1 = aptFloorPlanImgList.get(0);
				floorPlanImg1 = floorPlanImg1.replaceAll(" ","%20");
			}
			
			if (StringUtil.isBlankOrEmpty(floorPlanImg1))
			{
				if (apartmentImgList!=null && apartmentImgList.size() >= 2)
				{	
					floorPlanImg1 = apartmentImgList.get(1);
					floorPlanImg1 = floorPlanImg1.replaceAll(" ","%20");
				}	
			}	

			bean.put("ApartmentFloorPlanImage1", floorPlanImg1);
			
			/*GenRow officeAddress = new GenRow();
			officeAddress.setViewSpec("SetupView");
			officeAddress.setParameter("SetupID", "Default");
			///officeAddress.setParameter("SetupID", "0O1E2O5H1D865O2H5Z300B2Z6P6F");  // this is used for Riviera Homes
			officeAddress.doAction(GenerationKeys.SELECTFIRST);

			String WebsiteAddress = officeAddress.getString("Web");
			officeAddress.close();
			bean.put("WebsiteAddress", WebsiteAddress);
			*/
			
			return(bean);
	}
	
	protected TableData getExistingProperties(String idvalue, EmailerSubtypeHandler emailer)
	{
		TableData bean = emailer.getTableData("properties/ProductExistingPropertyView", PRODUCTID, idvalue);
		
		// Getting the Dimenstions
		Map<String, Object> dimensionsMap = new HashMap<String, Object>();
		dimensionsMap = getDimensions(idvalue);

		String Bedrooms = "0";
		if (dimensionsMap.get("Bedrooms.NumberOf") != null)
			Bedrooms = dimensionsMap.get("Bedrooms.NumberOf").toString();
		int intBedrooms = new Double(Bedrooms).intValue();
		
		String Bathrooms = "0";
		if (dimensionsMap.get("Bathrooms.NumberOf") != null)
			Bathrooms = dimensionsMap.get("Bathrooms.NumberOf").toString();
		int intBathrooms = new Double(Bathrooms).intValue();

		String Garage = "0";
		if (dimensionsMap.get("Garage.NumberOf") != null)
			Garage = dimensionsMap.get("Garage.NumberOf").toString();
		int intGarage = new Double(Garage).intValue();


		String HomeAreaSqm = "0";
		if (dimensionsMap.get("HomeArea.SQmeters") != null)
			HomeAreaSqm = dimensionsMap.get("HomeArea.SQmeters").toString();
		double doubleHomeAreaSqm = Double.parseDouble(HomeAreaSqm);

		String Study = "No";
		if (dimensionsMap.get("Study.YesNo") != null)
			Study = dimensionsMap.get("Study.YesNo").toString();
		if ("Y".equals(Study))
		{
			Study = "Yes";
		}
		else
		{
			Study = "No";
		}	
		
		String Storeys = "0";
		if (dimensionsMap.get("Storeys.NumberOf") != null)
			Storeys = dimensionsMap.get("Storeys.NumberOf").toString();
		int intStoreys = new Double(Storeys).intValue();

		String PowderRoom = "0";
		if (dimensionsMap.get("PowderRoom.NumberOf") != null)
			PowderRoom = dimensionsMap.get("PowderRoom.NumberOf").toString();
		int intPowderRoom = new Double(PowderRoom).intValue();

		bean.put("Bedrooms", intBedrooms);
		bean.put("Bathrooms", intBathrooms);
		bean.put("Garage", intGarage);
		bean.put("PowderRoom", intPowderRoom);
		

		bean.put("Study", Study);
		bean.put("Storeys", intStoreys);
		bean.put("HomeAreaSqm", doubleHomeAreaSqm);
		
		
		String projectGroupImagePath = getProjectGroupImage(idvalue);
		bean.put("projectGroupImagePath", projectGroupImagePath);
		
		
		GenRow quickIndex = new GenRow();
		quickIndex.setConnection(emailer.getConnection());
		quickIndex.setViewSpec("{properties/}ProductQuickIndexView");
		quickIndex.setParameter("ProductID", idvalue);
		quickIndex.doAction("selectfirst");
		
		String estateProductId = quickIndex.getData("EstateID");
		if(StringUtils.isNotBlank(estateProductId)) {
			bean.put("estateproduct", getEstate(estateProductId, emailer));
		}
		
		String planProductid = quickIndex.getData("PlanID");
		if(StringUtils.isNotBlank(planProductid)) {
			bean.put("planproduct", getPlan(planProductid, emailer));
		}
		
		String landproductid = quickIndex.getData("LotID");
		if(landproductid!=null && landproductid.length()!=0){
			bean.put("lotproduct", getLot(landproductid, emailer, -1));
		}
		
		
		try
		{
			String RoundedPrice = bean.getData("TotalCost");
			if (RoundedPrice != null) {
					try {
						RoundedPrice = StringUtil.getRoundedFormattedValue(RoundedPrice, false);
						
					} catch (Exception e) {}
			} 
			bean.put("RoundedPrice", RoundedPrice);
		}
		catch(Exception ee) {}
		
		/*GenRow imagesList = new GenRow();
		imagesList.setViewSpec("LinkedDocumentView");
		imagesList.setParameter("ProductID", idvalue);
		if (idvalue!=null && !"".equalsIgnoreCase(idvalue))
		{	
			imagesList.doAction(GenerationKeys.SEARCH);
			imagesList.getResults();
		}
		
		ArrayList<String> apartmentImgList = new ArrayList<String>();
		ArrayList<String> aptFloorPlanImgList = new ArrayList<String>();
		String imgPath = "";
		String imgType = "";
		while (imagesList.getNext())  
		{
				imgPath = imagesList.getString("FilePath");
				imgType = imagesList.getString("DocumentSubType");
				if (imgPath!=null && !"".equalsIgnoreCase(imgPath))
				{
					if ("Floor Plan".equalsIgnoreCase(imgType))
					{
						aptFloorPlanImgList.add(imgPath);	
					}
					else
					{
						apartmentImgList.add(imgPath);	
					}
					
				}	
		}
		imagesList.close();
		System.out.println("Apartments Images Query is:  " + imagesList.getStatement());
		
		String aptImg1 = "";
		if (apartmentImgList!=null && !apartmentImgList.isEmpty())
		{
			aptImg1 = apartmentImgList.get(0);
			aptImg1 = aptImg1.replaceAll(" ","%20");
		}	
		bean.put("ApartmentImage1", aptImg1);

		String floorPlanImg1 = "";
		if (aptFloorPlanImgList!=null && !aptFloorPlanImgList.isEmpty())
		{
			floorPlanImg1 = aptFloorPlanImgList.get(0);
			floorPlanImg1 = floorPlanImg1.replaceAll(" ","%20");
		}	
		bean.put("ApartmentFloorPlanImage1", floorPlanImg1);
		*/
		
		
		return(bean);
	}
	
	protected TableData getHome(String idvalue, EmailerSubtypeHandler emailer)
	{  
		TableData bean = emailer.getTableData("properties/ProductHomeView", PRODUCTID, idvalue);
		String rangeproductid = bean.getData("RangeProductID");
		if(rangeproductid!=null && rangeproductid.length()!=0){
			bean.put("rangeproduct", getRange(rangeproductid, emailer));
		}
		bean.put("documents", getLinkedDocumentArray(idvalue, emailer));
		GenRow row = new GenRow();
		
		try
		{
			String RoundedPrice = bean.getData("TotalCost");
			if (RoundedPrice != null) {
					try {
						RoundedPrice = StringUtil.getRoundedFormattedValue(RoundedPrice, false);
						
					} catch (Exception e) {}
			} 
			bean.put("RoundedPrice", RoundedPrice);
			row.setViewSpec("ProductLinkedProductView");
			row.setColumn("ProductID", idvalue);
			row.setColumn("ProductProductsLinkedProduct.ProductType","Home Facade");
			row.sortBy("TotalCost",0);
			if (row.getString("ProductID").length() > 0) {
				row.doAction("search");
				row.getResults();
			}
			int number = 1;
			while(row.getNext()) { 
				if(row.getString("IsExclusive").equalsIgnoreCase("Y")) 
					bean.put("HeroImage", row.getString("ImagePath"));
				else 
				{
					bean.put("facade"+number, row.getString("ImagePath"));
					number++;
				}
			}
		}
		catch(Exception ee) {}

		row.close();
		
		return(bean);
	}   

	protected TableData getLot(String idvalue, EmailerSubtypeHandler emailer, int indexPos)
	{  
		TableData bean = emailer.getTableData("properties/ProductLotView", PRODUCTID, idvalue);		
		bean.put("documents", getLinkedDocumentArray(idvalue, emailer));
		String stageproductid = bean.getData("StageProductID");
		if(stageproductid!=null && stageproductid.length()!=0){
			bean.put("stageproduct", getStage(stageproductid, emailer));
		}
		
		try {
			//Stage plan for each lot
			String stagePlan = getDocumentPath(bean.getData("StageProductID"), null, "Stage Plan");
			if(StringUtils.isNotBlank(stagePlan)) {
				bean.put("stagePlan", stagePlan);
			}
			
			if(indexPos == 0) {
				Map<String, String> dm = ds.getDripMapForTemplates(bean.getConnection(), us.getSimpleUser(bean.getConnection(), sharedcontext.getString("RepUserID")), new String[] { bean.getData("EstateProductID"), bean.getData("StageProductID"), bean.getData("ProductID") }, "Email", requestrow.getString("MasterTemplateID"), requestrow, -1);
				String dripList = null;
				
				//Setting drip values as inclusions and disclaimer
				if(dm != null) {
					String inclusion = dm.get("Text1");
					if(StringUtils.isNotBlank(inclusion)) {
						dripList = this.getDripString(inclusion,"\r\n", 6);
						bean.put("dripList", dripList);
					}
				
					String disclaimer = null;
					if(StringUtils.isNotBlank(dm.get("Text2"))) { 
						disclaimer = dm.get("Text2");
						if(StringUtils.isNotBlank(disclaimer)) {
							bean.put("disclaimer", disclaimer);
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.debug("Exception in getting stage plans and drips for first lot.", ex.getMessage());
		}
		
		return(bean);
	}

	protected TableData getPlan(String planproductid, EmailerSubtypeHandler emailer)
	{  
		TableData bean = emailer.getTableData("properties/ProductPlanView", PRODUCTID, planproductid);
		String homeproductid = bean.getData("HomeProductID");
		if(homeproductid!=null && homeproductid.length()!=0){
			bean.put("homeproduct", getHome(homeproductid, emailer));
		}
		
		try
		{
			String RoundedPrice = bean.getData("TotalCost");
			if (RoundedPrice != null) {
					try {
						RoundedPrice = StringUtil.getRoundedFormattedValue(RoundedPrice, false);
						
					} catch (Exception e) {}
			} 
			bean.put("RoundedPrice", RoundedPrice);
			bean.put("CurrentDate", getCurrentDate());
				
		}
		catch(Exception ee) {}
		
		
		try
		{
			setBrandProductDetails (bean);
		}
		catch(Exception ee) {
			ee.printStackTrace();
		}
		
		return(bean);
	}
	/*
	protected TableData getLand(String landproductid, EmailerSubtypeHandler emailer)
	{  
		TableData bean = emailer.getTableData("properties/ProductLotView", PRODUCTID, landproductid);
		return(bean);
	}
	*/
	protected TableData getRange(String idvalue, EmailerSubtypeHandler emailer)
	{  
		TableData bean = emailer.getTableData(productviewspec, PRODUCTID, idvalue);
		return(bean);
	}    

	protected TableData getStage(String idvalue, EmailerSubtypeHandler emailer)
	{  
		TableData bean = emailer.getTableData(productviewspec, PRODUCTID, idvalue);
		bean.put("documents", getLinkedDocumentArray(idvalue, emailer));
		return(bean);
	}   
	
	protected TableData getEstate(String idvalue, EmailerSubtypeHandler emailer)
	{  
		TableData bean = emailer.getTableData(productviewspec, PRODUCTID, idvalue);
		bean.put("documents", getLinkedDocumentArray(idvalue, emailer));
		return(bean);
	} 

	protected String getLinkedProductID(String productID, String type, Connection con)
	{
		GenRow row = new GenRow();
		row.setConnection(con);
		row.setTableSpec("ProductProducts");
		row.setParameter("-select1", "LinkedProductID");
		row.setParameter(PRODUCTID, productID);
		row.setParameter("ProductProductsLinkedProduct.ProductType", type);
		row.doAction("selectfirst");
		return(row.getString("LinkedProductID"));    
	}  

	protected String getProductType(String idvalue, Connection con)
	{
		GenRow product = new GenRow();
		product.setConnection(con);
		product.setTableSpec("Products");
		product.setParameter("-select1", "ProductType");
		product.setParameter(PRODUCTID, idvalue);
		product.doAction("select");
		return(product.getString("ProductType"));
	}

	public void setNoteParameters(TableData notebean)
	{
		notebean.setParameter(PRODUCTID, requestrow.getParameter(PRODUCTID));    
	}

	/**
	 * The idea here is that any instance of ProductEmailer should have access to this object which can be used
	 * to load additional product details.
	 *
	 * It should be noted that this will block whilst waiting for detail so it's probably not preferable. Perhaps
	 * some kind of template header could be used to concurrently pull in whichever data is required.
	 */
	public class ProductDetailLoader {

		// create a worker to load additional details.
		private GenRow worker = null;
		private Connection con = null;
		/**
		 * Create an instance of ProductDetailLoader
		 * @param con java.sql.Connection
		 */
		public ProductDetailLoader(Connection con) {
			this.con = con;
		}
		
		public void load(TableData product) {
			load(product, -1);
		}

		/*
		 * Overloaded this method to handle regional pricing changes
		 */
		public void load(TableData product, int regionIndex) {
			if(product == null) return;

			worker = new GenRow();
			worker.setConnection(con);
			worker.setTableSpec("properties/ProductDetails");
			worker.setParameter("ProductID", product.getColumn("ProductID"));
			//worker.setParameter("ProductType", product.getColumn("ProductType").replaceAll(" ",""));
			worker.setParameter("-select","*");
			try {
				worker.getResults();
			} catch (Exception e) {
				logger.error("Inside ProductEmailer.java 1st error {} in {}",e.getMessage(),  worker.getStatement());
				try {
					// lets try again, maybe it has cleared
					Thread.sleep(250);
					worker.getResults();
				} catch (Exception e2) {
					logger.error("Inside ProductEmailer.java 2nd error {} in {}",e2.getMessage(),  worker.getStatement());
					
				}
				
			}

			Map<String, Object> detailMap = null;

			if(logger.isDebugEnabled()) {
				logger.trace("Inside ProductEmailer.java 22 ==> load() ==> worker Query is: {}", worker.getStatement());
				logger.debug("Inside ProductEmailer.java 22 ==> load() ==> ProductID: {}, ProductType: {}", product.getColumn("ProductID"), product.getColumn("ProductType").replaceAll(" ",""));
				logger.debug("Inside ProductEmailer.java 22 ==> ProductType: " + requestrow.getString("ProductType") + "  ,  GroupID: " + requestrow.getString("GroupID") + "  ,  ContactID: " + requestrow.getString("ContactID") + "   ,  RegionID: " + requestrow.getString("RegionID"));
			}
			
			while(worker.getNext()) { 
				String type = worker.getColumn("DetailsType").replaceAll(" ","");
				detailMap = new HashMap<String, Object>();

				for(int i=0; i<worker.getTableSpec().getFieldsLength(); i++) { 
					String field = worker.getTableSpec().getFieldName(i);
					Object o = worker.getObject(field);
					if(o != null) { 
						detailMap.put(field,o);
						logger.trace("adding {} = {}", field, String.valueOf(o)); 
					}
				}
				product.put(type, detailMap);
				logger.trace("adding type map for {}", type);
			}
			worker.close();


			if (product.getString("ProductID").length() != 0 && "Home Plan".equals(product.getString("ProductType"))) {
				List<String> features = Collections.emptyList();
				worker = new GenRow();
				worker.setViewSpec("ProductLinkedProductView");
				worker.setConnection(con);
				worker.setColumn("ProductID",product.getString("ProductID"));
				worker.setColumn("ProductProductsLinkedProduct.ProductType","Plan Feature");
				worker.setColumn("ProductProductsLinkedProduct.Active", "Y");
				worker.sortBy("SortOrder",0);
				worker.sortBy("ProductProductsLinkedProduct.Name", 1);
				try {
					worker.getResults();
				} catch (Exception e) {
					logger.error("Inside ProductEmailer.java 1st error {} in {}",e.getMessage(),  worker.getStatement());
					try {
						// lets try again, maybe it has cleared
						Thread.sleep(250);
						worker.getResults();
					} catch (Exception e2) {
						logger.error("Inside ProductEmailer.java 2nd error {} in {}",e2.getMessage(),  worker.getStatement());
						
					}
				}

				if(worker.getNext()) {
					features = new LinkedList<String>();
					do {
						features.add(worker.getData("Name"));
					} while(worker.getNext());
				}
				worker.close();
				product.put("features", features);
			}
			logger.debug("ProductType: {}", product.getString("ProductType"));
			if (product.getString("ProductID").length() != 0 && "House and Land".equals(product.getString("ProductType"))) {
				List<String> features = Collections.emptyList();
				worker = new GenRow();
				worker.setViewSpec("ProductLinkedProductView");
				worker.setConnection(con);
				worker.setColumn("ProductID",product.getString("ProductID"));
				worker.setColumn("ProductProductsLinkedProduct.ProductType","Package Cost");
				worker.setColumn("ProductProductsLinkedProduct.Active", "Y");
				worker.sortBy("SortOrder",0);
				worker.sortBy("ProductProductsLinkedProduct.Name", 1);
				try {
					worker.getResults();
				} catch (Exception e) {
					logger.error("Inside ProductEmailer.java 1st error {} in {}",e.getMessage(),  worker.getStatement());
					try {
						// lets try again, maybe it has cleared
						Thread.sleep(250);
						worker.getResults();
					} catch (Exception e2) {
						logger.error("Inside ProductEmailer.java 2nd error {} in {}",e2.getMessage(),  worker.getStatement());
						
					}
				}

				if(logger.isTraceEnabled()) logger.trace("SQL Query: {}", worker.getStatement());
				// Limit to 5 items in the land features
				int count = 0;
				if(worker.getNext()) {
					features = new LinkedList<String>();
					do {
						features.add(worker.getData("Name"));
						if(logger.isTraceEnabled()) logger.trace("Added: " + worker.getData("Name"));
					} while(worker.getNext() && count++ < 4);
				}
				worker.close();
				product.put("landfeatures", features);
			}
			
			// Load the drip details
			String[] pIDs = null; 
			if("House and Land".equals(product.getString("ProductType"))){
				pIDs = new String[]{product.getColumn("ProductID"), product.getColumn("ProductID"), product.getColumn("ProductID")};
			} else if("Home Plan".equals(product.getString("ProductType"))){
				pIDs = new String[] {product.getColumn("RangeProductID"), product.getColumn("HomeProductID"), product.getColumn("ProductID")};
			}
			
			if(pIDs != null)
				product.put("dripmap", ds.getDripMapForTemplates(con, us.getSimpleUser(con, sharedcontext.getString("RepUserID")), pIDs, "Email", requestrow.getString("MasterTemplateID"), requestrow, regionIndex));
		}
	}
	
	public static Map<String, Object> getDimensions(String productID) 
	{
		GenRow details = new GenRow();
		details.setTableSpec("properties/ProductDetails");
		details.setParameter("-select", "*");
		details.setColumn("ProductID", productID);
		//details.setColumn("DetailsType", "Bedroom %");
		details.sortBy("ProductType", 0);
		details.sortBy("SortOrder", 1);
		details.sortBy("DetailsType", 2);
		if (details.getString("ProductID").length() > 0) {
			details.doAction("search");
			details.getResults(true);
		}
		
		
		Map<String, Object> detailMap = new HashMap<String, Object>();
		String tmpKey = "";
		while(details.getNext()) 
		{ 
			String type = details.getColumn("DetailsType").replaceAll(" ","");

			for(int i=0; i<details.getTableSpec().getFieldsLength(); i++) 
			{ 
					String field = details.getTableSpec().getFieldName(i);
					Object o = details.getObject(field);
					if(o != null) 
					{ 
						tmpKey = (type+"."+field).trim();
						detailMap.put(tmpKey, String.valueOf(o));
						
						// System.out.println("tmpKey:  " + tmpKey  + "    ,  value: "  + String.valueOf(o));
					}
			}
		}
		details.close();

		return detailMap;
	}
	
	public static String getProjectGroupImage (String productID) 
	{
		String projectGroupImg = "";
		
		if (productID != null && !"".equalsIgnoreCase(productID)) 
		{
					 GenRow r = new GenRow();
					 //r.setRequest(request);
					 r.setViewSpec("ProductSecurityGroupView");
					 r.setParameter("-join1", "ProductSecurityGroupGroup.GroupImageDocuments");
				     r.setParameter("-select1","ProductSecurityGroupGroup.GroupImageDocuments.FilePath");
					 r.setParameter("ProductID", productID);
					 r.doAction(GenerationKeys.SELECTFIRST);

					 if(r.isSuccessful()) 
					 {
							 if(r.isSet("FilePath")) 
							 {
								 projectGroupImg = r.getData("FilePath");
							 }
					 }
					 logger.debug("projectGroupImg: {}", projectGroupImg);
		}	
		return projectGroupImg;
	}
	
	
	public static ArrayList<String> getInclusionList(String productID) {

		ArrayList pkgItems = new ArrayList();

		GenRow linkedproducts = new GenRow();

		try
			{
			//Details for Package Inclusions 
			String FacadeName = "";
			linkedproducts.setViewSpec("ProductLinkedProductView");
			linkedproducts.sortBy("ProductProductsLinkedProduct.Name",0);
		
			//linkedproducts.setRequest(request);
			linkedproducts.setColumn("ProductID", productID);
			linkedproducts.setColumn("ProductProductsLinkedProduct.ProductType","Home Facade");
			
			if (productID!=null && !"".equalsIgnoreCase(productID))
			{
				linkedproducts.doAction("search");
				linkedproducts.getResults(true);
			}
			
//			System.out.println("linkedproducts.getStatement 01: " + linkedproducts.getStatement());
			 
			while (linkedproducts.getNext()) 
			{
				FacadeName = linkedproducts.getColumn("Name")  + " Facade";
				pkgItems.add(FacadeName);
			}
		
			//linkedproducts.setRequest(request);
			linkedproducts.setColumn("ProductID",  productID);
			linkedproducts.setColumn("ProductProductsLinkedProduct.ProductType","Plan Feature");
		
			if (productID!=null && !"".equalsIgnoreCase(productID))
			{
				linkedproducts.doAction("search");
				linkedproducts.getResults(true);
			}
//			System.out.println("linkedproducts.getStatement 02: " + linkedproducts.getStatement());
			
			while (linkedproducts.getNext()) 
			{
				pkgItems.add(linkedproducts.getColumn("Name"));
			}
			
			//linkedproducts.setRequest(request);
			linkedproducts.setColumn("ProductID", productID);
			linkedproducts.setColumn("ProductProductsLinkedProduct.ProductType","Home Option");
			
			if (productID!=null && !"".equalsIgnoreCase(productID))
			{
				linkedproducts.doAction("search");
				linkedproducts.getResults(true);
			}
		
//			System.out.println("linkedproducts.getStatement 03: " + linkedproducts.getStatement());
			
			while (linkedproducts.getNext()) 
			{
				pkgItems.add(linkedproducts.getColumn("Name"));
			}
			
			//linkedproducts.setRequest(request);
			linkedproducts.setColumn("ProductID", productID);
			linkedproducts.setColumn("ProductProductsLinkedProduct.ProductType","Package Cost");
			// NI: Need attention here... linkedproducts.setColumn("ProductProductsLinkedProduct.Category",packageCostsCategory.getString("PCCategory"));
			
			if (productID!=null && !"".equalsIgnoreCase(productID))
			{
				linkedproducts.doAction("search");
				linkedproducts.getResults(true);
			}
		
//			System.out.println("linkedproducts.getStatement 04: " + linkedproducts.getStatement());
			
			while (linkedproducts.getNext()) 
			{
				pkgItems.add(linkedproducts.getColumn("Name"));
			}
			
		
			//linkedproducts.setRequest(request);
			linkedproducts.setColumn("ProductID", productID);
			linkedproducts.setColumn("IsExclusive","Y");
			linkedproducts.setColumn("ProductProductsLinkedProduct.ProductType","Package Cost");
			
			if (productID!=null && !"".equalsIgnoreCase(productID))
			{
				linkedproducts.doAction("search");
				linkedproducts.getResults(true);
			}
			
//			System.out.println("linkedproducts.getStatement 05: " + linkedproducts.getStatement());
			
			while (linkedproducts.getNext()) 
			{
				pkgItems.add(linkedproducts.getColumn("Name"));
			}
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
			 pkgItems = new ArrayList();
		} finally {
			if (linkedproducts != null) linkedproducts.close();
		}
		return pkgItems;
	}	
	
	public static void setBrandProductDetails(TableData bean) {
		
			String builderImg = "";
			String builderUrl = "";
			GenRow builderLinkedDocs = new GenRow();
			builderLinkedDocs.setViewSpec("LinkedDocumentView");
			builderLinkedDocs.setColumn("DocumentType", "Image");
			builderLinkedDocs.setColumn("ProductID", bean.getData("BrandProductID"));
			builderLinkedDocs.sortBy("SortOrder", 0);
			
			if( bean.getData("BrandProductID") != null && !"".equalsIgnoreCase(bean.getData("BrandProductID")))
			{
				builderLinkedDocs.doAction(ActionBean.SEARCH);
				builderLinkedDocs.getResults(true);
			}
			while(builderLinkedDocs.getNext())
			{
				builderImg = builderLinkedDocs.getData("FilePath");
				builderUrl = builderLinkedDocs.getData("DocumentURL");
				
				bean.put("BrandLogoPath", builderImg);
				break;
			}
			builderLinkedDocs.close();
	}
	
	protected void setRegionName(TableData bean, int index) {
		try {
			logger.debug("Setting Region Name");
			String regionIDsInRequest = requestrow.getString("RegionID");
			
			ArrayList<String> regionIDs = RunwayUtil.getRegionIDList(regionIDsInRequest);
			String usedRegionID = regionIDs.get(index);
				
			logger.debug("regionIDsInRequest: " + regionIDsInRequest + "   ,  ProductID: " + bean.getString("ProductID") +  "   ,   usedRegionID: " + usedRegionID);
			
			if (StringUtils.isNotBlank(usedRegionID)) {
				GenRow grRegionName = new GenRow();
				grRegionName.setTableSpec("DisplayEntities");
				grRegionName.setParameter("DisplayEntityID", usedRegionID);
				grRegionName.setParameter("-select0", "Description"); // Changed on 12-Aug-2014: As requested by Dwayne
				grRegionName.doAction("selectfirst");
				
				logger.debug("grRegionName Query is: " + grRegionName.getStatement());
				
				String regionName = grRegionName.getString("Description");
				
				logger.debug("grRegionName Description is: " + regionName);

				if (StringUtils.isNotBlank(regionName)) {
					bean.put("RegionName", regionName);
				} 
			}
		} catch(Exception ee) {
			logger.debug("Exception Occurred while setting the Region Name. Exception Message is: ", ee.getMessage());
		}
	}
	

	private String getCurrentDate()
	{
		Date dNow = new Date();
		SimpleDateFormat format = new SimpleDateFormat(	" MMMM YYYY");
		SimpleDateFormat formatDate = new SimpleDateFormat(	"dd");
		String tmpNo = formatDate.format(dNow);
		String dateAppender = "th";
		if(tmpNo.length() == 2)
			tmpNo = tmpNo.substring(1,tmpNo.length());
		if(tmpNo.equals("1"))
			dateAppender = "st";
		else if(tmpNo.equals("2"))
			dateAppender = "nd";
		return formatDate.format(dNow) + dateAppender + format.format(dNow);
	}
	
	public String getDocumentPath(String productID, String fileName, String type) {
		GenRow documentPath = new GenRow();
		documentPath.setViewSpec("LinkedDocumentView");
		String docPath = null; 
		try {
			if (StringUtils.isNotBlank(productID))
			{	
				documentPath.setParameter("ProductID", productID);
				documentPath.setParameter("ShowOnEmail", "Y");
				documentPath.setParameter("LinkedDocumentDocument.DocumentSubType", type);
				documentPath.doAction(GenerationKeys.SEARCH);
				documentPath.getResults();
				documentPath.getNext();
				logger.debug("Document Query is: " + documentPath.getStatement());
				
				if(!StringUtils.isEmpty(documentPath.getString("FilePath"))) {
					docPath = documentPath.getString("FilePath");
				}
			}
		}catch (Exception e) {
			logger.debug("Exception in trying to get the document for a product : " + documentPath.getStatement());
			e.printStackTrace();
		}
		finally {
			documentPath.close();
		}
		return docPath;
	}
	
	public String getDripString(String drip, String token, int maxCount) {
		   String[] str = drip.split(token);
		   if(str.length > maxCount) {
			   String tmpInclusions = "";
				for(int j = 0; j < maxCount-1; j++) {
					if (tmpInclusions.length() > 0) tmpInclusions += "\n";
					tmpInclusions += tmpInclusions;
				}
				drip = tmpInclusions;
		   }
		   
		   return drip;
	}
	
}