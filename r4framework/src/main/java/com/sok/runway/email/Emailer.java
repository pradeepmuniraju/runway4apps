package com.sok.runway.email;

import javax.servlet.*;
import javax.servlet.http.*;

import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.email.subtype.*;

import java.sql.Connection;

public interface Emailer extends EmailerForm, OfflineEmailable
{  
	public static final int TemplatePreview = 100;
	public static final int NewsletterPreview = 150;   
	public static final int ContactEmailer = 200;
	public static final int CompanyEmailer = 300;

	public static final int BaseEmailer = 1;   
	public static final int NewsletterEmailer = 2;
	public static final int NotificationEmailer = 3;
	public static final int OrderEmailer = 4;
	public static final int ProductEmailer = 5;
	public static final int OpportunityEmailer = 6;
	public static final int CampaignEmailer = 7;
	public static final int ReferEmailer = 8;
	public static final int ProductGroupEmailer = 9;
	public static final int AppointmentEmailer = 10;
	public static final int ReportEmailer = 11;
	public static final int OrderItemEmailer = 5;

	public static final String OFFLINEMODE = "-offlinemode";
	public static final String OFFLINEMODECONTACT = "Contact";
	public static final String OFFLINEMODECOMPANY = "Company";

	public static final String SENT = "Sent";
	public static final String SENTOUTCOME = "Awaiting Outcome";

	public static final String OPTOUT = "Opt-out or left company";
	public static final String TOEMPTY = "Empty email address";
	public static final String FROMEMPTY = "Empty from email address";
	public static final String NOTPERMITTED = "Insufficient Privileges";
	public static final String REPINACTIVE = "Sales Rep Inactive";
	public static final String NOGROUP = "No Valid Group";
	public static final String NOREP = "No Valid Rep";

	public static final String ATTACHMENTID = "-attachmentid";


	public boolean isEmailerType(int type);

	public boolean hasEmailerSubType(int type);

	public void init(GenRow requestrow, ServletContext context);

	//public void init(HttpServletRequest request, ServletContext context);     

	public String send();

	public String send(HttpServletRequest request, ServletContext context);

	public MailerBean getMailerBean();

	public void generate();

	public void generate(TableData context);

	public void generate(String id);   

	public String generateSendAndSave();

	public String generateSendAndSave(TableData context);

	public String generateSendAndSave(String id);    

	public String generateEmailSource();

	public void save(String status);  

	public String getRequestString(String name);

	public String getTemplateString(String name);

	public String getSharedContextString(String name);

	public String getSpecificContextString(String name);

	public TableData getRequestRow();

	public void setCurrentUser(UserBean user);

	public UserBean getCurrentUser();

	public String getHTMLPreview();

	public String getTextPreview();

	public String getTextPreviewInHTML();

	public boolean isHTMLEmail();

	public String getPrimaryKeyName();

	public void addSubtype(EmailerSubtype subtype);

	public EmailerSubtype getSubtype(int type);

	public void appendLinkURL(StringBuffer buffer, String recordid);

	public void setCheckSpam(boolean b);

	public String getSpamScore();

	public String getSpamResp();
	
	public void processSpam();


}