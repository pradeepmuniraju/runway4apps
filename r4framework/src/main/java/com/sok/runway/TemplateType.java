/*
 * User: Behrang
 * Date: Jul 19, 2010
 * Time: 3:41:35 PM 
 */
package com.sok.runway;

public enum TemplateType {

    EMAIL,
    ORDER,
    SMS,
    APPOINTMENT

//    public static final String EMAIL  = "EMAIL";
//    public static final String ORDER = "ORDER";
//    public static final String SMS = "SMS";
//    public static final String APPOINTMENT = "APPOINTMENT";

}
