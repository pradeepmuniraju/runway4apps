package com.sok.runway;

public interface SessionKeys {
   
	public static final String CURRENT_USER = "currentuser";  
   
   // USER
	public static final String CONTACT_SEARCH = "currentcontactssearch";  
	public static final String CONTACT_DETAILED_SEARCH = "currentcontactsdetailedsearch";  
	public static final String COMPANY_SEARCH = "currentcompanysearch";  
	public static final String NOTE_SEARCH = "currentnotesearch";  
	public static final String RESPONSE_SEARCH = "currentresponsesearch";  
	public static final String OPPORTUNITY_SEARCH = "currentopportunitysearch";
	public static final String ORDER_SEARCH = "currentordersearch";  
	public static final String DOCUMENT_SEARCH = "currentattacheddocsearch";  
	
	// ADMIN
	public static final String USER_SEARCH = "currentuserssearch";  
	public static final String GROUP_SEARCH = "currentgroupssearch";  
	public static final String ROLE_SEARCH = "currentrolesearch";  
	public static final String STAGE_SEARCH = "currentstagesearch";  
	public static final String PROCESS_SEARCH = "currentprocesssearch";  
	public static final String ORDERTEMPLATES_SEARCH = "currentordertemplatesearch"; 
	
	// CMS 
	public static final String PRODUCT_SEARCH = "currentproductssearch";  
	public static final String PRODUCTGROUP_SEARCH = "currentproductgroupsearch";  
	public static final String SURVEY_SEARCH = "currentSurveySearch";  
	public static final String QUESTION_SEARCH = "currentQuestionSearch";  
	public static final String CAMPAIGN_SEARCH = "currentcampaignsearch";  
	public static final String CMS_DOCUMENT_SEARCH = "currentdocumentsearch";  
	
	public static final String ACTIVITY_LOG = "activitylog";
}