package com.sok.runway;
import javax.servlet.*;     
import javax.servlet.http.*;
import java.util.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import java.text.*;
import com.sok.framework.*;
public class OfflineRemoveFromCampaign extends OfflineProcess
{
	StringBuffer status = new StringBuffer();
	String email = null;
	String userid = null;
	Vector items = new Vector(100);
	StringBuffer campaignbuffer = null;
	StringBuffer campaignstatusbuffer = null;
	StringBuffer campaigneventbuffer = null;
	int proccessed = 0;
	boolean forcontacts = true;
   HttpSession session = null;
   
	Connection con = null;
	Context ctx = null;
	
	public OfflineRemoveFromCampaign(HttpServletRequest request, ServletContext context)
	{
		super(request, context);
		   session = request.getSession();
			UserBean user = (UserBean)session.getAttribute(SessionKeys.CURRENT_USER);
			this.userid = user.getCurrentUserID();
			this.email = user.getEmail();					
		start();	
	}

	void getConnection() throws Exception
	{
		if(con==null || con.isClosed())
		{
			ctx = new InitialContext();

			DataSource ds = null;
				try{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource)envContext.lookup(dbconn);
				}catch(Exception e){}
			if(ds == null)
			{
				ds = (DataSource)ctx.lookup(dbconn);
			}
			con = ds.getConnection();
		}
	}
	
	void retrieveItems(TableData searchbean, String idField)throws Exception	
	{
		try
		{
		   getConnection();
		   searchbean.setConnection(con);
		   RowSetWrapperBean list = new RowSetWrapperBean();		
		   list.setConnection(con);
		   list.setSearchBean(searchbean);
		   list.getResults();
		   
		   TableData resultset = list.getResultBean();
		   while (list.getNext()) {
				items.add(resultset.getString(idField));
			}		   			        			
		}		
		finally
		{    		
			try{
            	con.close();
        	}catch (Exception ex){}    		  		    
		}
	}

	void removeFromCampaign()throws Exception
    {	   		
	  /* Delete from Contact Campaigns */	
		String prefix = "ContactID";
		if (!forcontacts){
			prefix = "CompanyID";
		}
		
		/*Delete ContactCampaignStatus*/		
		campaignstatusbuffer = new StringBuffer();
		campaignstatusbuffer.append("delete ContactCampaignStatus from ContactCampaignStatus");
		campaignstatusbuffer.append(" left outer join ContactCampaigns");
		campaignstatusbuffer.append(" on ContactCampaigns.ContactCampaignID = ContactCampaignStatus.ContactCampaignID");
		campaignstatusbuffer.append(" where ContactCampaigns.CampaignID=");		
		campaignstatusbuffer.append("'"+requestbean.getString("CampaignID")+"'");
		campaignstatusbuffer.append("and ContactCampaigns."+prefix+"= ? ");	
		
		/*Delete ContactEvent*/
		campaigneventbuffer = new StringBuffer();
		campaigneventbuffer.append("delete ContactEvents from ContactEvents");
		campaigneventbuffer.append(" left outer join ContactCampaigns");
		campaigneventbuffer.append(" on ContactCampaigns.ContactCampaignID = ContactEvents.ContactCampaignID");
		campaigneventbuffer.append(" where ContactCampaigns.CampaignID=");		
		campaigneventbuffer.append("'"+requestbean.getString("CampaignID")+"'");
		campaigneventbuffer.append("and ContactCampaigns."+prefix+"= ? ");	
		
		/*Delete ContactCampaign*/		
		campaignbuffer = new StringBuffer();	
		campaignbuffer.append("delete from ContactCampaigns where ContactCampaigns.CampaignID=");
		campaignbuffer.append("'"+requestbean.getString("CampaignID")+"'");
		campaignbuffer.append(" and ");
	    campaignbuffer.append(prefix+"= ? ");						
						   
		Exception exception = null;
		
		PreparedStatement campaigndeletestmt = null;
		PreparedStatement campaignstatusdeletestmt = null;
		PreparedStatement campaigneventdeletestmt = null;
		try
		{		
			getConnection();
			
			campaigndeletestmt = con.prepareStatement(campaignbuffer.toString());
			campaignstatusdeletestmt = con.prepareStatement(campaignstatusbuffer.toString());
			campaigneventdeletestmt = con.prepareStatement(campaigneventbuffer.toString());
						
			for(int i=0; i< items.size() && !isKilled(); i++)
			{
				removeCampaignItem(campaignstatusdeletestmt, (String)items.get(i), false);
				removeCampaignItem(campaigneventdeletestmt, (String)items.get(i), false);
				removeCampaignItem(campaigndeletestmt, (String)items.get(i),true);							
			}
		}
		catch (Exception e) {
			exception = e;
		}
		finally
		{
   		if (campaigndeletestmt != null) {
				try{
		            campaigndeletestmt.close();
		      }catch (Exception ex){}
			}
   		if (campaignstatusdeletestmt != null) {
				try{
					campaignstatusdeletestmt.close();
		      }catch (Exception ex){}
			}
    		if (con != null)  {
				try{
            	con.close();
        		}catch (Exception ex){}
    		}
		}
		if(exception!=null)
		{
			throw(exception);
		}	
	}

	void removeCampaignItem(PreparedStatement campaigndeletestmt, String itemid, boolean checkProcessed)throws Exception
	{	
		campaigndeletestmt.setString(1, itemid);		
		int cr = campaigndeletestmt.executeUpdate(); 	
		
		if (checkProcessed){
			if (cr >0){
				proccessed++;
			}				
		}
	}
	
	public void execute()
	{		
		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
		status.append(context.getServletContextName());
		status.append(" Runway offline process - Remove from Campaign\r\n");
		status.append("Initiated ");
		status.append(dt.format(new java.util.Date())+" ");	
				
		String mode = requestbean.getString("-mode");		
		
		if(mode!=null && mode.equals("Companies"))
		{
			forcontacts = false;
		}
				
		
		TableData searchbean = null;
		try
	   {		
			String idField = null;
			
			if(forcontacts) {
				searchbean = (TableData)session.getAttribute(SessionKeys.CONTACT_SEARCH);
				searchbean.setViewSpec("OfflineContactView");
				idField = "ContactID";
			}
			else
			{
				searchbean = (TableData)session.getAttribute(SessionKeys.COMPANY_SEARCH);
				searchbean.setViewSpec("OfflineCompanyView");
				idField = "CompanyID";
			}
			retrieveItems(searchbean, idField);				
			removeFromCampaign();
			
			status.append("\r\n");
			status.append(String.valueOf(proccessed));
			status.append(" of ");
			status.append(String.valueOf(items.size()));
			if(forcontacts)
			{
				status.append(" contacts ");
			}
			else
			{
				status.append(" companies ");
			}
			status.append("removed from Campaign.\r\n");
							
			
   		if (isKilled()) {
   	      status.append("Offline process terminated by - ");
   	      status.append(getKiller());
   	      status.append("\r\n\r\n");
   	   }
   	   else {	   
   	      status.append("Offline process completed.");
   	   }
		}
		catch(Exception e)
		{
			status.append("Offline process halted on ");
			status.append(String.valueOf(proccessed));
			status.append(" of ");
			status.append(String.valueOf(items.size()));
			status.append(".\r\n\r\n");
			status.append(e.toString());					
			
		   if (e instanceof java.sql.SQLException && searchbean != null) {		      
		      status.append(searchbean.getSearchStatement());
		   }
		}
	}

	public String getStatusMailBody()
	{
		return(status.toString());
	}

	public String getStatusMailSubject()
	{
		return(context.getServletContextName() + " Runway offline process - Remove from Campaign");
	}

	public String getStatusMailRecipient()
	{
		return(email);
	}
	
	
   public int getProcessSize() {
      return items.size();
   }
   
   public int getProcessedCount() {
      return proccessed;
   }
   
   public int getErrorCount() {
      return -1;
   }
}
