package com.sok.runway;

import java.util.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;
import java.text.*;
import com.sok.framework.*;
import com.sok.framework.sql.SqlDatabase;

public class UserCalendar
{
	GregorianCalendar 	gCal 						= null;
	GregorianCalendar 	appointmentdate 		= null;
	Appointment[][][] 	appointments 			= new Appointment[6][7][]; //weeks in month, days in week, halfhours in day 
	boolean[] 				hasappointments 		= new boolean[31]; 			//days in month
	int[][] 					dates 					= new int[6][7]; 				//weeks in month, days in week
	boolean[][] 			dateinmonth 			= new boolean[6][7]; 		//weeks in month, days in week
	
	Map<String,String>	appointmentfilters 	= null;
	GenRow					calendarbean			= null;

	public static int MON = 0;
	public static int TUE = 1;
	public static int WED = 2;
	public static int THU = 3;
	public static int FRI = 4;
	public static int SAT = 5;
	public static int SUN = 6;

	public static int AM0600 = 0;
	public static int AM0630 = 1;
	public static int AM0700 = 2;
	public static int AM0730 = 3;
	public static int AM0800 = 4;
	public static int AM0830 = 5;
	public static int AM0900 = 6;
	public static int AM0930 = 7;
	public static int AM1000 = 8;
	public static int AM1030 = 9;
	public static int AM1100 = 10;
	public static int AM1130 = 11;
	public static int PM1200 = 12;
	public static int PM1230 = 13;
	public static int PM0100 = 14;
	public static int PM0130 = 15;
	public static int PM0200 = 16;
	public static int PM0230 = 17;
	public static int PM0300 = 18;
	public static int PM0330 = 19;
	public static int PM0400 = 20;
	public static int PM0430 = 21;
	public static int PM0500 = 22;
	public static int PM0530 = 23;
	public static int PM0600 = 24;
	public static int PM0630 = 25;
	public static int PM0700 = 26;
	public static int PM0730 = 27;
	public static int PM0800 = 28;
	public static int PM0830 = 29;
	public static int PM0900 = 30;
	public static int PM0930 = 31;
	public static int PM1000 = 32;
	public static int PM1030 = 33;
	public static int PM1100 = 34;
	public static int PM1130 = 35;
	
	public static int MIDNIGHT = 36;
	
	public static int AM0000 = 36;
	public static int AM0030 = 37;
	public static int AM0100 = 38;
	public static int AM0130 = 39;
	public static int AM0200 = 40;
	public static int AM0230 = 41;
	public static int AM0300 = 42;
	public static int AM0330 = 43;
	public static int AM0400 = 44;
	public static int AM0430 = 45;
	public static int AM0500 = 46;
	public static int AM0530 = 47;

	public static final String[] daysofweek = {
		"MON",
		"TUE",
		"WED",
		"THU",
		"FRI",
		"SAT",
		"SUN"
	};

	public static final String[] duration = {
		"0:30",
		"1:00",
		"1:30",
		"2:00",
		"2:30",
		"3:00",
		"3:30",
		"4:00",
		"4:30",
		"5:00",
		"5:30",
		"6:00",
		"6:30",
		"7:00",
		"7:30",
		"8:00"
	};

	public static final String[] times = {
		"6:00AM",
		"6:30AM",
		"7:00AM",
		"7:30AM",
		"8:00AM",
		"8:30AM",
		"9:00AM",
		"9:30AM",
		"10:00AM",
		"10:30AM",
		"11:00AM",
		"11:30AM",
		"12:00PM",
		"12:30PM",
		"1:00PM",
		"1:30PM",
		"2:00PM",
		"2:30PM",
		"3:00PM",
		"3:30PM",
		"4:00PM",
		"4:30PM",
		"5:00PM",
		"5:30PM",
		"6:00PM",
		"6:30PM",
		"7:00PM",
		"7:30PM",
		"8:00PM",
		"8:30PM",
		"9:00PM",
		"9:30PM",
		"10:00PM",
		"10:30PM",
		"11:00PM",
		"11:30PM",
		"12:00AM",
		"12:30AM",
		"1:00AM",
		"1:30AM",
		"2:00AM",
		"2:30AM",
		"3:00AM",
		"3:30AM",
		"4:00AM",
		"4:30AM",
		"5:00AM",
		"5:30AM"
	};
	
	public static final String[] fulltimes = {
		"06:00:00",
		"06:30:00",
		"07:00:00",
		"07:30:00",
		"08:00:00",
		"08:30:00",
		"09:00:00",
		"09:30:00",
		"10:00:00",
		"10:30:00",
		"11:00:00",
		"11:30:00",
		"12:00:00",
		"12:30:00",
		"13:00:00",
		"13:30:00",
		"14:00:00",
		"14:30:00",
		"15:00:00",
		"15:30:00",
		"16:00:00",
		"16:30:00",
		"17:00:00",
		"17:30:00",
		"18:00:00",
		"18:30:00",
		"19:00:00",
		"19:30:00",
		"20:00:00",
		"20:30:00",
		"21:00:00",
		"21:30:00",
		"22:00:00",
		"22:30:00",
		"23:00:00",
		"23:30:00",
		"24:00:00",
		"24:30:00",
		"01:00:00",
		"01:30:00",
		"02:00:00",
		"02:30:00",
		"03:00:00",
		"03:30:00",
		"04:00:00",
		"04:30:00",
		"05:00:00",
		"05:30:00"
	};
	
	public static final String[] displaytimes = {
			 "6:00<br>AM",null,
			 "7:00", null,
			 "8:00", null,
			 "9:00", null,
			 "10:00", null,
			 "11:00", null,
			 "12:00<br>PM", null,
			 "1:00",null,
			 "2:00",null,
			 "3:00",null,
			 "4:00",null,
			 "5:00",null,
			 "6:00",null,
			 "7:00",null,
			 "8:00",null,
			 "9:00",null,
			 "10:00",null,
			 "11:00",null,
			 "12:00<br>AM",null,
			 "1:00",null,
			 "2:00",null,
			 "3:00",null,
			 "4:00",null,
			 "5:00",null};
			 
			 
	public static String SAM0600 = times[0];
	public static String SAM0630 = times[1];
	public static String SAM0700 = times[2];
	public static String SAM0730 = times[3];
	public static String SAM0800 = times[4];
	public static String SAM0830 = times[5];
	public static String SAM0900 = times[6];
	public static String SAM0930 = times[7];
	public static String SAM1000 = times[8];
	public static String SAM1030 = times[9];
	public static String SAM1100 = times[10];
	public static String SAM1130 = times[11];
	public static String SPM1200 = times[12];
	public static String SPM1230 = times[13];
	public static String SPM0100 = times[14];
	public static String SPM0130 = times[15];
	public static String SPM0200 = times[16];
	public static String SPM0230 = times[17];
	public static String SPM0300 = times[18];
	public static String SPM0330 = times[19];
	public static String SPM0400 = times[20];
	public static String SPM0430 = times[21];
	public static String SPM0500 = times[22];
	public static String SPM0530 = times[23];
	public static String SPM0600 = times[24];
	public static String SPM0630 = times[25];
	public static String SPM0700 = times[26];
	public static String SPM0730 = times[27];
	public static String SPM0800 = times[28];
	public static String SPM0830 = times[29];
	public static String SPM0900 = times[30];
	public static String SPM0930 = times[31];
	public static String SPM1000 = times[32];
	public static String SPM1030 = times[33];
	public static String SPM1100 = times[34];
	public static String SPM1130 = times[35];
	public static String SAM0000 = times[36];
	public static String SAM0030 = times[37];
	public static String SAM0100 = times[38];
	public static String SAM0130 = times[39];
	public static String SAM0200 = times[40];
	public static String SAM0230 = times[41];
	public static String SAM0300 = times[42];
	public static String SAM0330 = times[43];
	public static String SAM0400 = times[44];
	public static String SAM0430 = times[45];
	public static String SAM0500 = times[46];
	public static String SAM0530 = times[47];

	public static String HR03 = duration[0];
	public static String HR10 = duration[1];
	public static String HR13 = duration[2];
	public static String HR20 = duration[3];
	public static String HR23 = duration[4];
	public static String HR30 = duration[5];
	public static String HR33 = duration[6];
	public static String HR40 = duration[7];
	public static String HR43 = duration[8];
	public static String HR50 = duration[9];
	public static String HR53 = duration[10];
	public static String HR60 = duration[11];
	public static String HR63 = duration[12];
	public static String HR70 = duration[13];
	public static String HR73 = duration[14];
	public static String HR80 = duration[15];

	public static String[] months = {
		"January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"
	};

	static final String blank = "";
	String userid = null;
	String contextpath = null;
	String datasource = null;
	StringBuffer errors = null;
	StringBuffer statement = null;
	String thisday = null;
	int taskcount = 0;
	static final int tasksize = 36;
	Appointment[] tasks = null;
	
	//today
	int today = 0;	
	int thismonth = 0;
	int thisyear = 0;

	//currently displayed
	int currentday = 0;
	int currentdayofweek = 0;
	int currentweek = 0;
	int currentmonth = 0;
	int currentyear = 0;
	int lastweekinmonth = 0;

	String appointmentcontactid = null;
	String appointmentcompanyid = null;
	String appointmentopportunityid = null;
	String appointmentprocessstageid = null;
	String appointmentorderid = null;
	String appointmentcampaignid = null; 
	
	static int maxchars = 20;
	static int mmaxchars = 250;
	
	static String daymode = "day";
	static String weekmode = "week";
	static String monthmode = "month";
	String currentmode = weekmode;
	StringBuffer appointmentstmt = null;
	String action = null;
	
	boolean populated = false; 
	boolean refresh = false;
	boolean debug = true;
	
	private boolean replaceCarriageReturns = false;
	
	public UserCalendar()
	{
		gCal = new GregorianCalendar();
		gCal.setFirstDayOfWeek(Calendar.MONDAY);
		today = gCal.get(Calendar.DAY_OF_MONTH);

		appointmentdate = new GregorianCalendar();
		appointmentdate.setFirstDayOfWeek(Calendar.MONDAY);

		thismonth = gCal.get(Calendar.MONTH);
		thisyear = gCal.get(Calendar.YEAR);
		DateFormat dayformat = new SimpleDateFormat("dd/MM/yyyy");
		thisday = dayformat.format(gCal.getTime());
		//currentweek = gCal.get(Calendar.WEEK_OF_MONTH) - 1;

		currentmonth = thismonth;
		int dayofweek = gCal.get(Calendar.DAY_OF_WEEK);
		currentdayofweek = getDay(dayofweek);
		currentweek = getWeekOfMonth(today, dayofweek) - 1;
		currentday = today;
		currentyear = thisyear;
	}

	//sun's gCal.get(Calendar.WEEK_OF_MONTH) is boken
	//it ignores gCal.setFirstDayOfWeek(Calendar.MONDAY)
	//this method assumes monday as FirstDayOfWeek
	int getWeekOfMonth(int today, int dayofweek)
	{
		int weeks = 0;
		int days = today;
		if(dayofweek != Calendar.SUNDAY)
		{
			weeks = weeks + 1;
			days = days - dayofweek + 1;
		}
		if(days>0)
		{
			weeks = weeks + (days / 7);
			int re = days % 7;
			if(re!=0)
			{
				weeks = weeks + 1;
			}
		}
		return(weeks);
	}
	
	public String getMode()
	{
		return(currentmode);
	}

	public void setDebug(boolean b)
	{
		debug = b;
	}

	public String isModeSelected(String mode)
	{
		if(mode!=null && mode.equals(currentmode))
		{
			return("selected");
		}
		return(blank);
	}

	public void setAppointmentContact(String contactid)
	{
		if(contactid!=null)
		{
			appointmentcontactid = contactid;
		}
	}
	
	public void setAppointmentOrder(String orderid)
	{
		if(orderid!=null)
		{
			appointmentorderid = orderid;
		}
	}
	
	public void setAppointmentCampaign(String campaignid) { 
		if(campaignid != null) { 
			appointmentcampaignid = campaignid; 
		}
	}
	
	public void setAppointmentCompany(String companyid)
	{
		if(companyid!=null)
		{
			appointmentcompanyid = companyid;
		}
	}

	public void setAppointmentOpportunity(String opportunityid)
	{
		if(opportunityid!=null)
		{
			appointmentopportunityid = opportunityid;
		}
	}	

	public void setAppointmentProcessStage(String processstageid)
	{
		if(processstageid!=null)
		{
			appointmentprocessstageid = processstageid;
		}
	}		
	
	public boolean getPopulated() { 
		return populated; 
	}
	public void setPopulated() { 
		populated = true; 
	}
	public void set(String userid, String mode, String action)
	{
		refresh = false;
		if(errors==null || errors.length()!=0)
		{
			errors = new StringBuffer();
		}
		setAction(action);
		setUserID(userid);
		setMode(mode);
	}

	void setUserID(String id)
	{
		if(userid==null || !userid.equals(id))
		{
			refresh = true;
		}
		userid = id;
	}

	public String getUserID()
	{
		return(userid);
	}

	public String getErrors()
	{
		return(errors.toString());	
	}

	protected void initArray()
	{
		tasks = new Appointment[tasksize];
		taskcount = 0;
		for(int i=0; i<appointments.length; i++)
		{
			for(int j=0; j<appointments[i].length; j++)
			{
				appointments[i][j] = new Appointment[36];
			}	
		}

		for(int i=0; i<dateinmonth.length; i++)
		{
			for(int j=0; j<dateinmonth[i].length; j++)
			{
				dateinmonth[i][j] = false;
			}	
		}

		for(int i=0; i<hasappointments.length; i++)
		{
			hasappointments[i] = false;
		}
	
	}

	public void setJndiName(String t)
	{
		datasource = t;
	}
	
	public void setContextPath(String t)
	{
		contextpath = t;
	}	
	
	public String getStatement()
	{
		return(statement.toString());	
	}

	void clearDates()
	{
		for(int i=0; i<dates.length; i++) //weeks
		{
			for(int j=0; j<dates[i].length; j++) //days
			{
				dates[i][j] = 0;
			}
		}
	}

	public void setDay()
	{
		setMonth();
	}

	public void setMonth()
	{
		int daysinmonth = getDaysInMonth();
		int tempdate = gCal.get(Calendar.DAY_OF_MONTH);
		int tempmonth = gCal.get(Calendar.MONTH);
		int tempyear = gCal.get(Calendar.YEAR);

		clearDates();
		gCal.set(Calendar.DAY_OF_MONTH,1);
		adjustToWeekStart();

		StringBuffer start = new StringBuffer();
		
		start.append(String.valueOf(gCal.get(Calendar.DAY_OF_MONTH)));
		start.append('/');
		start.append(String.valueOf(gCal.get(Calendar.MONTH)+1));
		start.append('/');
		start.append(String.valueOf(gCal.get(Calendar.YEAR)));
		
		boolean reachedfirst = false;
		boolean pastmonth = false;
		boolean notreachedendofweek = true;

		int tempdayofmonth = 0;	
		int tempdayofweek = 0;

		for(int i=0; i<dates.length && notreachedendofweek; i++) // week of month
		{
			for(int j=0; j<dates[i].length && notreachedendofweek; j++) // day of week
			{
				tempdayofmonth = gCal.get(Calendar.DAY_OF_MONTH);
				tempdayofweek = gCal.get(Calendar.DAY_OF_WEEK);

				if(tempdayofmonth == 1)
				{
					reachedfirst = true;
				}

				if(reachedfirst && !pastmonth)
				{
					dateinmonth[i][j] = true;
				}
				else
				{
					dateinmonth[i][j] = false;
				}

				if(reachedfirst && tempdayofmonth == daysinmonth)
				{
					pastmonth = true;
				}	

				if(tempdayofweek==Calendar.MONDAY && j==MON)
				{
					dates[i][j] = tempdayofmonth;
					if(notreachedendofweek)
					{
						gCal.add(Calendar.DATE, 1);
					}
				}
				else if(tempdayofweek==Calendar.TUESDAY && j==TUE)
				{
					dates[i][j] = tempdayofmonth;
					if(notreachedendofweek)
					{
						gCal.add(Calendar.DATE, 1);
					}
				}
				else if(tempdayofweek==Calendar.WEDNESDAY && j==WED)
				{
					dates[i][j] = tempdayofmonth;
					if(notreachedendofweek)
					{
						gCal.add(Calendar.DATE, 1);
					}
				}
				else if(tempdayofweek==Calendar.THURSDAY && j==THU)
				{
					dates[i][j] = tempdayofmonth;
					if(notreachedendofweek)
					{
						gCal.add(Calendar.DATE, 1);
					}
				}
				else if(tempdayofweek==Calendar.FRIDAY && j==FRI)
				{
					dates[i][j] = tempdayofmonth;
					if(notreachedendofweek)
					{
						gCal.add(Calendar.DATE, 1);
					}
				}
				else if(tempdayofweek==Calendar.SATURDAY && j==SAT)
				{
					dates[i][j] = tempdayofmonth;
					if(notreachedendofweek)
					{
						gCal.add(Calendar.DATE, 1);
					}
				}
				else if(tempdayofweek==Calendar.SUNDAY && j==SUN)
				{
					dates[i][j] = tempdayofmonth;
					if(pastmonth)
					{
						notreachedendofweek = false;
						lastweekinmonth = i;
					}

					if(notreachedendofweek)
					{
						gCal.add(Calendar.DATE, 1);
					}
				}

			}
		}

		gCal.add(Calendar.DAY_OF_MONTH,1); 
		
		StringBuffer end = new StringBuffer();
		
		end.append(String.valueOf(gCal.get(Calendar.DAY_OF_MONTH)));
		end.append('/');
		end.append(String.valueOf(gCal.get(Calendar.MONTH)+1));
		
		end.append('/');
		end.append(String.valueOf(gCal.get(Calendar.YEAR)));
		
		initialiseCalendarBean(start, end);

		gCal.set(Calendar.YEAR,tempyear);
		gCal.set(Calendar.MONTH,tempmonth);
		gCal.set(Calendar.DAY_OF_MONTH,tempdate);
	}
	
	/**
	 * Takes a map containing key-value pair which will be used to add or
	 * modify the GenRow used by this UserCalendar class to generate its SQL
	 * query, e.g. to set Notes.AppointmentStatus. Each pair will be applied to 
	 * the GenRow via a setParameter(key, value) command.
	 * 
	 * @param filters
	 */
	public void setAppointmentFilter(Map<String,String> appointmentfilters)
	{
		this.appointmentfilters = appointmentfilters;
	}
	
	/**
	 * Initialise calendar GenRow with all the appropriate parameters, including
	 * additional filters.
	 * 
	 * @param start 
	 * @param end
	 */
	private void initialiseCalendarBean(StringBuffer start, StringBuffer end)
	{
		calendarbean = new GenRow();
		
		calendarbean.setJndiName(ActionBean.getJndiName());
		
		calendarbean.setViewSpec("AppointmentView");
		
		calendarbean.setParameter("Type", 						"Appointment");
		calendarbean.setParameter("RepUserID|1", 				userid);
		calendarbean.setParameter("NoteAttendee.UserID|1", userid);
		calendarbean.setParameter("NoteDate", 		start.toString() + "-" + end.toString());
		
		if(appointmentfilters != null)
      {
      	Iterator itr = appointmentfilters.entrySet().iterator();
      	
      	while (itr.hasNext()) 
      	{
      		Map.Entry<String,String> pairs = (Map.Entry<String,String>)itr.next();
      		calendarbean.setParameter(pairs.getKey(), pairs.getValue());
      	}
      }
		
		calendarbean.setParameter("-sort0", "Notes.NoteDate");
		calendarbean.setParameter("-sort1", "Notes.NoteTime");
	}
	
	StringBuffer getStatement(StringBuffer start, StringBuffer end)
	{
		SqlDatabase database = ActionBean.getDatabase();
		
		StringBuffer stmt = new StringBuffer();
		
		stmt.append("SELECT Notes.NoteID, Notes.NoteDate, Notes.NoteTime, Notes.Duration, Notes.Subject, Notes.Body, ");
		stmt.append("Contacts.ContactID, Contacts.FirstName, Contacts.LastName, Contacts.Company, Contacts.Street, ");
		stmt.append("Contacts.Street2, Contacts.City, Contacts.State, Contacts.Postcode, Contacts.Phone, Contacts.Mobile, Contacts.CompanyID, ");
		stmt.append("Attendees.Status, Notes.RepUserID, Attendees.UserID ");
		stmt.append("FROM Notes ");
		stmt.append("LEFT OUTER JOIN Contacts ON Notes.ContactID = Contacts.ContactID ");
		stmt.append("LEFT OUTER JOIN Attendees ON Attendees.NoteID = Notes.NoteID ");
		stmt.append("where ");
		stmt.append("Notes.Type = 'Appointment' and ");
		stmt.append("(Notes.RepUserID = '");
		stmt.append(userid);
		stmt.append("' OR Attendees.UserID = '");
		stmt.append(userid);
		stmt.append("') and Notes.NoteDate >= ");
		database.appendStrToDateStmt(stmt, start.toString());
		stmt.append(" and Notes.NoteDate <= ");
		database.appendStrToDateStmt(stmt, end.toString());
		stmt.append(" order by Notes.NoteDate, Notes.NoteTime");

		return(stmt);
	}
	
	public int getWeekNumber()
	{
		return(currentweek);
	}

	void adjustToWeekStart()
	{
		if(gCal.get(Calendar.DAY_OF_WEEK)==Calendar.TUESDAY)
		{
			gCal.add(Calendar.DATE, -1);
		}
		else if(gCal.get(Calendar.DAY_OF_WEEK)==Calendar.WEDNESDAY)
		{
			gCal.add(Calendar.DATE, -2);
		}
		else if(gCal.get(Calendar.DAY_OF_WEEK)==Calendar.THURSDAY)
		{
			gCal.add(Calendar.DATE, -3);
		}
		else if(gCal.get(Calendar.DAY_OF_WEEK)==Calendar.FRIDAY)
		{
			gCal.add(Calendar.DATE, -4);
		}
		else if(gCal.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY)
		{
			gCal.add(Calendar.DATE, -5);
		}
		else if(gCal.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY)
		{
			gCal.add(Calendar.DATE, -6);
		}
	}
	
	public String getCellClass(int t)
	{
		return(getCellClass(currentweek, currentdayofweek, t));
	}

	public String getCellClass(int d, int t)
	{
		return(getCellClass(currentweek, d, t));
	}

	public String getCellClass(int w, int d, int t)
	{
		if(hasAppointment(w,d,t))
		{
			if(appointments[w][d][t].overlap)
			{
				return("overlapapp");	
			}
			else if(appointments[w][d][t].status==null || appointments[w][d][t].status.length()==0)
			{
				return("unacknowledgedapp");
			}
			else
			{
				return("borderhighlight");	
			}
		}
		else if(t>3 && t<26)
		{
			return("borderbr");
		}
		else
		{
			return("borderbrdark");
		}
	}
	
	public String getDateSuffix(int d)
	{
		if(d!=11 && d!=12 && d!=13)
		{
			int r = d % 10;
			
			if(r == 1)
			{
				return("<sup>st</sup>");
			}
			else if(r == 2)
			{
				return("<sup>nd</sup>");
			}
			else if(r == 3)
			{
				return("<sup>rd</sup>");
			}
		}
		return("<sup>th</sup>");
	}
	
	public int getDaySize()
	{
		return(appointments[0].length);
	}
	
	public int getTimeSize()
	{
		return(appointments[0][0].length);
	}	
	
	public String getTimeHeader(int i)
	{
		if(displaytimes[i]!=null)
		{
			StringBuffer temp = new StringBuffer();
			temp.append("<td rowspan=\"2\" align=\"center\" class=\"timecell\">");
			temp.append(displaytimes[i]);
			temp.append("</td>");
			return(temp.toString());			
		}
		return(blank);
	}

	public String getTime(int i)
	{
		if(times[i]!=null)
		{
			return(times[i]);			
		}
		return(blank);
	}

	public String getDayOfMonthString(int w, int i)
	{
		if(dates[w][i]!=0)
		{
			return(String.valueOf(dates[w][i]));
		}
		return(blank);
	}
	
	public String getDayOfMonthString(int i)
	{
		return(getDayOfMonthString(currentweek,i));
	}

	public String getDayOfMonthString()
	{
		return(getDayOfMonthString(currentweek,currentdayofweek));
	}

	public boolean isDayInMonth(int w, int i)
	{
		return(dateinmonth[w][i]);
	}
	
	public boolean isDayInMonth(int i)
	{
		return(isDayInMonth(currentweek,i));
	}

	public boolean isDayInMonth()
	{
		return(isDayInMonth(currentweek,currentdayofweek));
	}

	public String getDayOfMonth(int w, int i)
	{
		if(dates[w][i]!=0)
		{
			StringBuffer temp = new StringBuffer();
			if(currentmode.equals(monthmode))
			{
				temp.append("<div class=\"monthday\">");
				temp.append(String.valueOf(dates[w][i]));
				temp.append(getDateSuffix(dates[w][i]));
				temp.append("&nbsp;</div>");
			}
			else
			{
				temp.append(String.valueOf(dates[w][i]));
				temp.append(getDateSuffix(dates[w][i]));
				temp.append("&nbsp;");
			}
			return(temp.toString());
		}
		return(blank);
	}
	
	public String getDayOfMonth(int i)
	{
		return(getDayOfMonth(currentweek,i));
	}

	public String getDayOfMonth()
	{
		return(getDayOfMonth(currentweek,currentdayofweek));
	}

	public String getDayOfWeek(int i)
	{
		return(daysofweek[i]);
	}

	public String getDayOfWeek()
	{
		return(daysofweek[currentdayofweek]);
	}

	public int getAppointmentBlock(int time)
	{
		return(getAppointmentBlock(currentweek, currentdayofweek, time));
	}

	public int getAppointmentBlock(int day, int time)
	{
		return(getAppointmentBlock(currentweek, day, time));
	}
	
	public int getAppointmentBlock(int week, int day, int time)
	{
		Appointment app = appointments[week][day][time];
		if(app == null)
		{
			return(0);
		}
		return(app.blocks);
	}

	public void setSingleLineTitleText(boolean replaceCarriageReturns) {
		this.replaceCarriageReturns = replaceCarriageReturns;
	}
	
	public boolean isSingleLineTitleText() {
		return replaceCarriageReturns;
	}
	
	public String getAppointment(int time)
	{
		return(getAppointment(currentweek, currentdayofweek, time));
	}

	public String getAppointment(int day, int time)
	{
		return(getAppointment(currentweek, day, time));
	}
	
	public String getAppointment(int week, int day, int time)
	{		
		Appointment app = appointments[week][day][time];
		StringBuffer temp = new StringBuffer();
		if(app!=null)
		{
			if(app.appointment!=null && app.appointment.length()!=0 || app.subject!=null && app.subject.length()!=0)
			{
				temp.append("<a href=\"");			
				if(contextpath!=null) {
					temp.append(contextpath);
					temp.append("/crm/");
				}
				temp.append("appointment.ics?NoteID=");
				temp.append(app.id);
				temp.append("\" title=\"");
				temp.append("export this appointment to MS Outlook");
				temp.append("\">");
				temp.append("<img src=\"");
				if(contextpath!=null) {
					temp.append(contextpath);
				}
				temp.append("/images/appointment_icon.gif\" width=\"15\" height=\"15\" border=\"0\">");
				temp.append("</a>");
				if(app.contactid != null && app.contactid.length()!=0)
				{
					temp.append("<a href=\"");		
					if(contextpath!=null) {
						temp.append(contextpath);
						temp.append("/runway/");
					}	
					temp.append("contact.view?ContactID=");
					temp.append(app.contactid);
					temp.append("&-action=select\" title=\"");
					temp.append("view contact");
					temp.append("\">");
					temp.append("<img src=\"");
					if(contextpath!=null)
					{
						temp.append(contextpath);
					}
					temp.append("/images/contact_icon.gif\" width=\"15\" height=\"15\" border=\"0\">");
					temp.append("</a>");
				}
				if(app.companyid != null && app.companyid.length()!=0)
				{
					temp.append("<a href=\"");		
					if(contextpath!=null) {
						temp.append(contextpath);
						temp.append("/runway/");
					}
					temp.append("company.view?CompanyID=");
					temp.append(app.companyid);
					temp.append("&-action=select\" title=\"");
					temp.append("view company");
					temp.append("\">");
					temp.append("<img src=\"");
					if(contextpath!=null)
					{
						temp.append(contextpath);
					}
					temp.append("/images/company_icon.gif\" width=\"15\" height=\"15\" border=\"0\">");
					temp.append("</a>");
				}				
				temp.append("<a href=\"");		
				if(contextpath!=null) {
					temp.append(contextpath);
					temp.append("/runway/");
				}	
				temp.append("appointment.view?NoteID=");
				temp.append(app.id);
				temp.append("&-action=select\" title=\"");
				//todo: Need to remove carriage returns if mozilla
				String notestr = getNoteText(app);
					if(replaceCarriageReturns)
					{
						/*Check for the carriage returns and replace them 
						  with a ( - ) */

						notestr = StringUtil.replace(notestr,"\r\n","-");
						notestr = StringUtil.replace(notestr,"\n","-");
					}
				temp.append(StringUtil.replace(notestr,"\"","&quot;"));
				temp.append("\">");
				if(currentmode.equals(daymode))
				{
					temp.append(getShortAppointmentText(app,mmaxchars));
				}
				else
				{
					temp.append(getShortAppointmentText(app,maxchars));
				}
				temp.append("</a>");
			}

		}
		else if(!currentmode.equals(monthmode))
		{
			temp.append("<a href=\"");	
			temp.append("javascript:newAppointment('NoteDate=");
			temp.append(getDateString(week, day));
			temp.append("&NoteTime=");
			temp.append(times[time]);
			if(appointmentcompanyid!=null && appointmentcompanyid.length()!=0)
			{	
				temp.append("&CompanyID=");
				temp.append(appointmentcompanyid);
			}
			if(appointmentcontactid!=null && appointmentcontactid.length()!=0)
			{	
				temp.append("&ContactID=");
				temp.append(appointmentcontactid);
			}
			if(appointmentorderid!=null && appointmentorderid.length()!=0)
			{	
				temp.append("&OrderID=");
				temp.append(appointmentorderid);
			}
			if(appointmentcampaignid!=null && appointmentcampaignid.length()!=0) { 
				temp.append("&CampaignID=");
				temp.append(appointmentcampaignid);
			}
			if(appointmentopportunityid!=null && appointmentopportunityid.length()!=0)
			{	
				temp.append("&OpportunityID=");
				temp.append(appointmentopportunityid);
			}
			if(appointmentprocessstageid!=null && appointmentprocessstageid.length()!=0)
			{	
				temp.append("&ProcessStageID=");
				temp.append(appointmentprocessstageid);
			}			
			temp.append("&RepUserID=");
			temp.append(userid);
			temp.append("');\" title=\"");
			temp.append("new appointment");
			temp.append("\">");
			temp.append("<img src=\"");
			if(contextpath!=null)
			{
				temp.append(contextpath);
			}
			temp.append("/images/pixel.gif\" width=\"100%\" height=\"15\" border=\"0\">");
			temp.append("</a>");
		}

		return(temp.toString());	
	}

	public String getAppointmentText(int time)
	{
		return(getAppointmentText(currentweek, currentdayofweek, time));
	}

	public String getAppointmentText(int day, int time)
	{
		return(getAppointmentText(currentweek, day, time));
	}

	public String getAppointmentText(int week, int day, int time)
	{

		Appointment app = appointments[week][day][time];
		StringBuffer temp = new StringBuffer();
		if(app!=null)
		{
			if(app.appointment!=null && app.appointment.length()!=0 || app.subject!=null && app.subject.length()!=0)
			{
				if(app.subject!=null && app.subject.length()!=0)
				{
					temp.append("<div class=\"appointmentTitleCell\">");
					temp.append(app.subject);
					temp.append("</div>");
				}
				if(app.appointment!=null && app.appointment.length()!=0)
				{
					temp.append("<div class=\"appointmentTextCell\">");
					temp.append(app.appointment);
					temp.append("</div>");
				}
			}
		}
		return(temp.toString());	
	}

	public String getAppointmentAddress(int time)
	{
		return(getAppointmentAddress(currentweek, currentdayofweek, time));
	}

	public String getAppointmentAddress(int day, int time)
	{
		return(getAppointmentAddress(currentweek, day, time));
	}

	public String getAppointmentAddress(int week, int day, int time)
	{

		Appointment app = appointments[week][day][time];
		StringBuffer temp = new StringBuffer();
		if(app!=null)
		{
			if(app.appointment!=null && app.appointment.length()!=0 || app.subject!=null && app.subject.length()!=0)
			{
				if(app.contactname!=null && app.contactname.length()!=0)
				{
					temp.append("<div class=\"appointmentTitleCell\">");
					temp.append(app.contactname);
					temp.append("</div>");
				}
				if(app.contactcompany!=null && app.contactcompany.length()!=0)
				{
					temp.append("<div class=\"appointmentTextCell\">");
					temp.append(app.contactcompany);
					temp.append("</div>");
				}
				if(app.contactaddress!=null && app.contactaddress.length()!=0)
				{
					temp.append("<div class=\"appointmentTextCell\">");
					temp.append(app.contactaddress);
					temp.append("</div>");
				}
			}
		}
		return(temp.toString());	
	}

	public boolean hasAppointment(int day, int time)
	{
		return(hasAppointment(currentweek, day, time));
	}

	public boolean hasAppointment(int week, int day, int time)
	{
		if(week >-1 && day >-1 && time >-1)
		{
			if(week < appointments.length && day < appointments[0].length && time < appointments[0][0].length)
			{
				Appointment app = appointments[week][day][time];
				if(app!=null)
				{	
					return(true);
				}
			}
		}
		return(false);
	}	
	
	public boolean hasAppointment(int dayinmonth)
	{
		return(hasappointments[dayinmonth]);
	}
	
	/**
	 * Loops through calendar bean and sets found appointments
	 */
	protected void populateMonthData()
	{
		if(calendarbean.isSuccessful())
		{
			while(calendarbean.getNext())
			{
				setAppointment();
			}
		}
	}
	
	protected void initMonthData(ResultSet rs)
	{
		try{

			while(rs.next())
			{
				try{
				setAppointment(rs);
				}catch(Exception ex){errors.append("@ initMonthData: ");errors.append(ActionBean.writeStackTraceToString(ex));errors.append("\r\n");}
			}
		}catch(Exception e){errors.append("@ initMonthData: ");errors.append(ActionBean.writeStackTraceToString(e));errors.append("\r\n");}
	}

	protected void initDayData(ResultSet rs)
	{
		try{
			while(rs.next())
			{
				try{
				addtask(rs.getString(1), rs.getTimestamp(2), rs.getString(3));
				}catch(Exception ex){errors.append("@ initDayData: ");errors.append(ActionBean.writeStackTraceToString(ex));errors.append("\r\n");}
			}
		}catch(Exception e){errors.append("@ initDayData: ");errors.append(ActionBean.writeStackTraceToString(e));errors.append("\r\n");}
	}
	
	protected void addtask(String aid, Timestamp date, String subject)
	{
		Appointment app =  new Appointment();
		app.id = aid;
		app.subject = subject;
		app.appointment = getShortText(subject, maxchars);
		tasks[taskcount] = app;
		taskcount++;
	}
	
	public int getTaskSize()
	{
		return(tasksize);
	}

	public String getTaskShortText(int i)
	{
		Appointment app = tasks[i];
		if(app!=null)
		{
			return(app.appointment);
		}
		return(ActionBean._blank);
	}

	public String getTaskText(int i)
	{
		Appointment app = tasks[i];
		if(app!=null)
		{
			return(getNoteText(app));
		}
		return(ActionBean._blank);
	}

	public String getTaskID(int i)
	{
		Appointment app = tasks[i];
		if(app!=null)
		{
			return(app.id);
		}
		return(ActionBean._blank);
	}

	public boolean hasTask(int i)
	{
		return(i < tasks.length && tasks[i] != null);
	}
	
	public String getTask(int i)
	{
		Appointment app = tasks[i];
		StringBuffer temp = new StringBuffer();
		temp.append("<tr><td class=\"todolistcell\">");
		if(app!=null)
		{
			temp.append("<a href=\"");
			temp.append("javascript:viewTask('TaskID=");
			temp.append(app.id);
			temp.append("');\">");
			temp.append(app.appointment);
			temp.append("</a>");
			return(temp.toString());		
		}
		else
		{
			temp.append("<a href=\"");	
			temp.append("javascript:newTask('TaskDate=");
			temp.append(getDateString());
			temp.append("&UserID=");
			temp.append(userid);
			temp.append("');\" title=\"");
			temp.append("new task");
			temp.append("\">");
			temp.append("<img src=\"");
			if(contextpath!=null)
			{
				temp.append(contextpath);
			}
			temp.append("/images/pixel.gif\" width=\"100%\" height=\"15\" border=\"0\">");
			temp.append("</a>");
		}
		temp.append("</td></tr>");
		return(temp.toString());
	}

	int getWeek(int week, int month)
	{
		if(month == currentmonth)
		{
			return(week - 1);
		}
		else if(month < currentmonth)
		{
			return(0);
		}
		else
		{
			return(lastweekinmonth);
		}
	}
	
	protected String getAddress(ResultSet rs)
	{
		String street1 = getString(rs, 11);
		String street2 = getString(rs, 12);
		String city = getString(rs, 13);
		String state = getString(rs, 14);
		String postcode = getString(rs, 15);
		String phone = getString(rs, 16);
		String mobile = getString(rs, 17);
		
		StringBuffer sb = new StringBuffer();
		if(street1.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(street1);
		}
		if(street2.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(street2);
		}
		if(city.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(city);
		}
		if(state.length()!=0 || postcode.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(state);
			sb.append(" ");
			sb.append(postcode);
		}
		if(phone.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(phone);
		}
		if(mobile.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(mobile);
		}
		return(sb.toString());
	}
	
	/**
	 * Construct address from the calendarbean (GenRow)
	 *
	 * @return address
	 */
	protected String getAddress()
	{
		String street1 	= calendarbean.getData("Street");
		String street2 	= calendarbean.getData("Street2");
		String city 		= calendarbean.getData("City");
		String state 		= calendarbean.getData("State");
		String postcode 	= calendarbean.getData("Postcode");
		String phone 		= calendarbean.getData("Phone");
		String mobile 		= calendarbean.getData("Mobile");
		
		StringBuffer sb = new StringBuffer();
		
		if(street1.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(street1);
		}
		if(street2.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(street2);
		}
		if(city.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(city);
		}
		if(state.length()!=0 || postcode.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(state);
			sb.append(" ");
			sb.append(postcode);
		}
		if(phone.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(phone);
		}
		if(mobile.length()!=0)
		{
			if(sb.length()!=0)
			{
				sb.append("<br>");
			}
			sb.append(mobile);
		}
		return(sb.toString());
	}

	protected String getString(ResultSet rs , int index)
	{
		String str = null;
		try
		{
			str = rs.getString(index);
		}
		catch(Exception e){}
		
		if(str == null)
		{
			str = "";
		}
		return(str);
	}
	
	protected Timestamp getTimestamp(ResultSet rs , int index)
	{
		try
		{
			return(rs.getTimestamp(index));
		}
		catch(Exception e){}
		return(null);
	}

	protected String getTimeString(ResultSet rs , int index)
	{
		DateFormat timeformat = new SimpleDateFormat("h:mma");
		String str = "";
		try
		{
			Time time = rs.getTime(index);
			if(time!=null)
			{
				str = timeformat.format(time);
			}
		}
		catch(Exception e){}
		return(str);
	}	
	
	static final int notsame = 0;
	static final int sameoverwrite = 1;	
	static final int sameignore = 2;	
	
	protected int compareAppointment(int week, int day, int time, String aid)
	{
		Appointment app = appointments[week][day][time];
		if(app!=null)
		{	
			if(aid.equals(app.id))
			{
				/*
				 * app.overlap added as a second attendee will remove the overlap
				 * flag otherwise, making it appear as though there is no conflict when 
				 * in fact, there is one. 
				 */
				if(userid.equals(app.attdrepid) || app.overlap)
				{
					return(sameignore);
				}
				else
				{
					return(sameoverwrite);
				}
			}
		}
		return(notsame);
	}
	
	protected void setAppointment(ResultSet rs)
	{
		String aid = getString(rs, 1);
		Timestamp date = getTimestamp(rs, 2);
		String time = getTimeString(rs, 3);
		String duration = getString(rs, 4);
		String subject = getString(rs, 5);
		String body = getString(rs, 6);
		String contactid = getString(rs, 7);
		String firstname = getString(rs, 8);
		String lastname = getString(rs, 9);
		String company = getString(rs, 10);
		String address = getAddress(rs);
		String companyid = getString(rs, 18);
		String status = getString(rs, 19);
		String nrepid = getString(rs, 20);
		String arepid = getString(rs, 21);
		
	   if (subject.length()==0) {
	      subject = "Unspecified";
	   }

		if(date!=null)
		{
			appointmentdate.setTime(date);
			int dayofmonth = appointmentdate.get(Calendar.DAY_OF_MONTH);
			int dayofweek = appointmentdate.get(Calendar.DAY_OF_WEEK);
			int m = appointmentdate.get(Calendar.MONTH);

			int w = getWeek(getWeekOfMonth(dayofmonth, dayofweek), m);
			int wd = getDay(dayofweek);
			int t = getTime(time);
			int l = getDuration(duration);
			int md = dayofmonth - 1;

			if(l!=0)
			{
				Appointment app =  new Appointment();
				app.id = aid;
				app.subject = subject;
				app.appointment = body;
				app.time = time;
				app.contactid = contactid;
				app.contactname = new StringBuffer().append(firstname).append(" ").append(lastname).toString();
				app.contactcompany = company;
				app.contactaddress = address;
				app.companyid = companyid;
				app.noterepid = nrepid;
				app.attdrepid = arepid;
				if(arepid.equals(userid))
				{
					app.status = status;
				}
				else
				{
					app.status = "n/a";
				}
				app.blocks = l;

				boolean hasoverlap = false;
				int compapp = compareAppointment(w,wd,t, aid);
				if(hasAppointment(w,wd,t))
				{
					if(compapp == notsame)
					{
						app.overlap = true;
						hasoverlap = true;	
						setOverlapBlocks(w,wd,t);
						appointments[w][wd][t] = app;
					}
					else if(compapp == sameoverwrite)
					{
						appointments[w][wd][t] = app;
					}
				}
				else
				{
					appointments[w][wd][t] = app;
				}

				if(m == currentmonth)
				{
					hasappointments[md] = true;
				}
				int block = 0;
				for(int i=1; i<l && i<appointments[w][wd].length; i++)
				{
					block = t+i;
					if(hasAppointment(w,wd,block) && compareAppointment(w,wd,block, aid) == notsame)
					{
						appointments[w][wd][block].overlap = true;
						setOverlapBlocks(w,wd,block);
						hasoverlap = true;
					}
					else
					{
						Appointment capp = new Appointment();
						capp.id = aid;
						capp.appointment = blank;
						capp.subject = blank;
						capp.contactname = blank;
						capp.contactcompany = blank;
						capp.contactaddress = blank;
						capp.noterepid = blank;
						capp.attdrepid = blank;
						capp.status = blank;
						appointments[w][wd][block] = capp;
					}
				}

				if(hasoverlap)
				{
					setOverlapBlocks(w,wd,t);
				}
			}
		}
	}
	
	/**
	 * Retrieve appointment details currently pointed to by calendarbean, and
	 * sets it on the appointment array accordingly.
	 */
	private void setAppointment()
	{		
		String 		aid 			= calendarbean.getData("NoteID");
		String 		time 			= calendarbean.getData("NoteTime");
		String 		duration 	= calendarbean.getData("Duration");
		String 		subject 		= calendarbean.getData("Subject");
		String 		body 			= calendarbean.getData("Body");
		String 		contactid 	= calendarbean.getData("ContactID");
		String 		firstname 	= calendarbean.getData("FirstName");
		String 		lastname 	= calendarbean.getData("LastName");
		String 		company 		= calendarbean.getData("Company");
		String 		address 		= getAddress();
		String 		companyid 	= calendarbean.getData("CompanyID");
		String 		status 		= calendarbean.getData("NoteAttendee.Status");
		String 		nrepid 		= calendarbean.getData("RepUserID");
		String 		arepid 		= calendarbean.getData("NoteAttendee.UserID");
		
	   if (subject.length() == 0) 
	   {
	      subject = "Unspecified";
	   }
	   
	   appointmentdate = new GregorianCalendar();
	   
	   try
	   {
	   	appointmentdate.setTime(calendarbean.getDate("NoteDate"));
	   }
	   catch(Exception e)
	   {
	   	appointmentdate = null;
	   }
	   
		if(appointmentdate!=null)
		{			
			int dayofmonth = appointmentdate.get(Calendar.DAY_OF_MONTH);
			int dayofweek 	= appointmentdate.get(Calendar.DAY_OF_WEEK);
			int m 			= appointmentdate.get(Calendar.MONTH);

			int w 	= getWeek(getWeekOfMonth(dayofmonth, dayofweek), m);
			int wd 	= getDay(dayofweek);
			int t 	= getTime(time);
			int l 	= getDuration(duration);
			int md 	= dayofmonth - 1;

			if(l != 0)
			{
				Appointment app =  new Appointment();
				
				app.id 					= aid;
				app.subject 			= subject;
				app.appointment 		= body;
				app.time 				= time;
				app.contactid 			= contactid;
				app.contactname 		= new StringBuffer().append(firstname).append(" ").append(lastname).toString();
				app.contactcompany 	= company;
				app.contactaddress 	= address;
				app.companyid 			= companyid;
				app.noterepid 			= nrepid;
				app.attdrepid 			= arepid;
				
				if(arepid.equals(userid))
				{
					app.status = status;
				}
				else
				{
					app.status = "n/a";
				}
				
				app.blocks = l;

				boolean 	hasoverlap 	= false;
				int 		compapp 		= compareAppointment(w,wd,t, aid);
				
				if(hasAppointment(w,wd,t))
				{
					if(compapp == notsame)
					{
						app.overlap 	= true;
						hasoverlap 		= true;
						
						setOverlapBlocks(w,wd,t);
						
						appointments[w][wd][t] = app;
					}
					else if(compapp == sameoverwrite)
					{
						appointments[w][wd][t] = app;
					}
				}
				else
				{
					appointments[w][wd][t] = app;
				}

				if(m == currentmonth)
				{
					hasappointments[md] = true;
				}
				
				int block = 0;
				
				for(int i = 1; i < l && i < appointments[w][wd].length; i++)
				{
					block = t+i;
					
					if(hasAppointment(w,wd,block) && compareAppointment(w,wd,block, aid) == notsame)
					{
						appointments[w][wd][block].overlap = true;
						
						setOverlapBlocks(w,wd,block);
						
						hasoverlap = true;
					}
					else
					{
						Appointment capp 		= new Appointment();
						capp.id 					= aid;
						capp.appointment 		= blank;
						capp.subject 			= blank;
						capp.contactname 		= blank;
						capp.contactcompany 	= blank;
						capp.contactaddress 	= blank;
						capp.noterepid 		= blank;
						capp.attdrepid 		= blank;
						capp.status 			= blank;

						appointments[w][wd][block] = capp;
					}
				}

				if(hasoverlap)
				{
					setOverlapBlocks(w,wd,t);
				}
			}
		}
	}

	void setOverlapBlocks(int w, int wd, int t)
	{
		String id = appointments[w][wd][t].id;
		int i = t;
		//while(hasAppointment(w,wd,i) && appointments[w][wd][i].id.equals(id))
		while(hasAppointment(w,wd,i))
		{
			appointments[w][wd][i].blocks = 1;
			i = i - 1;
		}
		i = t + 1;
		//while(hasAppointment(w,wd,i) && appointments[w][wd][i].id.equals(id))
		while(hasAppointment(w,wd,i))
		{
			appointments[w][wd][i].blocks = 1;
			i = i + 1;
		}
	}

	String getNoteText(Appointment app)
	{
		StringBuffer temp = new StringBuffer();
		if(app!=null)
		{
			if(app.contactname!=null && app.contactname.length()>1)
			{
				temp.append(app.contactname);
			}
			if(app.contactcompany!=null && app.contactcompany.length()!=0)
			{
				if(temp.length()!=0)
				{
					temp.append(" - ");
				}
					temp.append(app.contactcompany);
			}
			if(app.appointment!=null)
			{
				if(temp.length()!=0)
				{
					temp.append("\r\n");
				}
				temp.append(app.appointment);
			}
		}
		return(temp.toString());
	}
	

	protected String getShortAppointmentText(Appointment app)
	{
		return(getShortAppointmentText(app, maxchars));
	}

	protected String getShortAppointmentText(Appointment app, int max)
	{
		int temp = app.blocks * max;
		if(temp < max)
		{
			temp = max;
		}
		return(getShortAppointmentText(app.subject, app.appointment, temp));
	}

	protected String getShortAppointmentText(String subject, String body, int size)
	{
		if(subject!=null && subject.length()!=0)
		{
			return(getShortText(subject,size));	
		}
		else if(body!=null && body.length()!=0)
		{
			return(getShortText(body,size));	
		}
		return(blank);
	}
	
	public static String getShortText(String inputtext, int size)
	{
		if(inputtext.length()>size)
		{
			inputtext = inputtext.substring(0,size);
			inputtext = inputtext + "...";
		}
		return(inputtext);
	}
	
	static int getTime(String time)
	{
		int t = 0;
		if(time!=null)
		{
		   for (int i=0; i < times.length; i++) {
		      if (time.equals(times[i])) {
		         t = i;
		         break;
		      }
		   }
		}
		return(t);
	}

	static int getDurationBlocks(String blocks)
	{
		for(int i=0; i<duration.length; i++)
		{
			if(duration[i].equals(blocks))
			{
				return(i+1);
			}
		}
		return(0);
	}

	public static int getDurationMinutes(String blocks)
	{
		return(getDurationBlocks(blocks)*30);
	}	
	
	public static String getEndDate(String notedate, String blocks)
	{
		try
		{
			String format = "dd/MM/yyyy hh:mma";
			java.util.Date date = ActionBean.getDate(notedate,format,null,null);
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(date);
			cal.add(Calendar.MINUTE, getDurationMinutes(blocks));
			DateFormat dt = new SimpleDateFormat(format);
			return(dt.format(cal.getTime()));
		}
		catch(Exception e){}
		
		return(null);
	}
	
	/**
	 * @return String of today's date
	 */
	public String getTodayString()
	{
		return thisday;
	}
	
	protected int getDay(int calday)
	{
		int day = 0;
		if(calday ==Calendar.MONDAY){
			day = MON;
		}else if(calday==Calendar.TUESDAY){
			day = TUE;
		}else if(calday==Calendar.WEDNESDAY){
			day = WED;
		}else if(calday==Calendar.THURSDAY){
			day = THU;
		}else if(calday==Calendar.FRIDAY){
			day = FRI;
		}else if(calday==Calendar.SATURDAY){
			day = SAT;
		}else if(calday==Calendar.SUNDAY){
			day = SUN;
		}
		return(day);
	}
	
	protected int getDuration(String time)
	{
		int halfhrs = 0;
		if(time!=null)
		{
			if(time.equals(HR03)){
				halfhrs = 1;
			}else if(time.equals(HR10)){
				halfhrs = 2;
			}else if(time.equals(HR13)){
				halfhrs = 3;
			}else if(time.equals(HR20)){
				halfhrs = 4;
			}else if(time.equals(HR23)){
				halfhrs = 5;
			}else if(time.equals(HR30)){
				halfhrs = 6;
			}else if(time.equals(HR33)){
				halfhrs = 7;
			}else if(time.equals(HR40)){
				halfhrs = 8;
			}else if(time.equals(HR43)){
				halfhrs = 9;
			}else if(time.equals(HR50)){
				halfhrs = 10;
			}else if(time.equals(HR53)){
				halfhrs = 11;
			}else if(time.equals(HR60)){
				halfhrs = 12;
			}else if(time.equals(HR63)){
				halfhrs = 13;
			}else if(time.equals(HR70)){
				halfhrs = 14;
			}else if(time.equals(HR73)){
				halfhrs = 15;
			}else if(time.equals(HR80)){
				halfhrs = 16;
			}else{
				halfhrs = 1;
			}
			
		}
		return(halfhrs);
	}
	
	/**
	 * Executes calendarbean
	 */
	private boolean executeCalendarBean()
	{		
		if(calendarbean != null)
		{
			calendarbean.doAction(ActionBean.SEARCH);
			
			statement.append("\r\n"+calendarbean.getStatement());
			
			return calendarbean.getResults();
		}
		else 
		{
			return false;
		}
	}
	
	protected void getData()
	{

		StringBuffer taskstmt = new StringBuffer();
		
		taskstmt.append("SELECT Tasks.TaskID, Tasks.TaskDate, Tasks.Task ");
		taskstmt.append("FROM Tasks WHERE ");
		taskstmt.append("Tasks.UserID = '");
		taskstmt.append(userid);
		taskstmt.append("' order by Tasks.Priority, Tasks.Task");		

		statement = new StringBuffer();

		statement.append(taskstmt.toString());
		
		Connection 	con 		= ActionBean.connect(calendarbean);
		Statement 	stmt 		= null;
		ResultSet 	current 	= null;
		
		try
		{	
			if(executeCalendarBean())
			{
				populateMonthData();
			}

			stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					     				ResultSet.CONCUR_READ_ONLY);
			
			current = stmt.executeQuery(taskstmt.toString());
			
			initDayData(current);

			try
			{
				current.close();
			}
			catch (Exception ex) 
			{
				
			}
			
			try
			{
				stmt.close();
			}
			catch (Exception ex)
			{
				
			}
		}
		catch (Exception ex)
		{
			errors.append("@ getData: ");
			errors.append(ActionBean.writeStackTraceToString(ex));errors.append("\r\n");
		}
		finally
		{
    		if (con != null) 
    		{
        		try
        		{
            	con.close();
        		}
        		catch (Exception ex)
        		{
        			
        		}
    		}
		}
	}

	void setMode(String mode)
	{
		if(mode == null)
		{
			currentmode = weekmode;
			refresh = true;
		}
		else
		{
			if(!currentmode.equals(mode) || currentmode.equals(monthmode))
			{
				refresh = true;
			}
			currentmode = mode;
		}
	}

	void setAction(String actionstring)
	{
		action = actionstring;
	}

	public void doAction()
	{
		boolean adjustoweek = false;
		
		if(action!=null && action.length()!=0)
		{
			int templastmonth = gCal.get(Calendar.MONTH);

			if(action.equals("nextday"))
			{
				gCal.add(Calendar.DATE, 1);
			}
			else if(action.equals("priorday"))
			{
				gCal.add(Calendar.DATE, -1);	
			}
			else if(action.equals("nextweek"))
			{
				adjustoweek = true;
				gCal.add(Calendar.DATE, 7);
			}
			else if(action.equals("priorweek"))
			{
				gCal.add(Calendar.DATE, -7);	
			}
			else if(action.equals("priormonth"))
			{
				adjustoweek = true;
				gCal.add(Calendar.MONTH, -1);
				gCal.set(Calendar.DAY_OF_MONTH,1);
			}
			else if(action.equals("priormonthend"))
			{
				adjustoweek = true;
				gCal.add(Calendar.MONTH, -1);
				gCal.set(Calendar.DAY_OF_MONTH, gCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			}			
			else if(action.equals("nextmonth"))
			{
				adjustoweek = true;
				gCal.add(Calendar.MONTH, 1);
				gCal.set(Calendar.DAY_OF_MONTH,1);
			}
			else if(action.equalsIgnoreCase("today"))
			{
				gCal.set(Calendar.YEAR,thisyear);
				gCal.set(Calendar.MONTH,thismonth);
				gCal.set(Calendar.DAY_OF_MONTH,today);
			}
			else if(action.indexOf("/")>0)
			{
				String[] gotodate = action.split("/");
				if(dates.length>0)
				{
					try{
						int day = Integer.parseInt(gotodate[0]);
						gCal.set(Calendar.DAY_OF_MONTH,day);
					}catch(Exception e){}
				}
				if(dates.length>1)
				{
					try{
						int month = Integer.parseInt(gotodate[1]);
						gCal.set(Calendar.MONTH,month-1);
					}catch(Exception e){}
				}				
				if(dates.length>2)
				{
					try{
						int year = Integer.parseInt(gotodate[2]);
						gCal.set(Calendar.YEAR,year);
					}catch(Exception e){}
				}
			}
			else
			{
				try{
					int day = Integer.parseInt(action);
					gCal.set(Calendar.DAY_OF_MONTH,day);
				}catch(Exception e){}
			}

			int tempthismonth = gCal.get(Calendar.MONTH);

			if(templastmonth!=tempthismonth)
			{
				refresh = true;
			}

		}
		else
		{
			refresh = true;
			adjustoweek = true;
		}

		if(currentmode.equals(weekmode) && adjustoweek)
		{
			if(gCal.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY)
			{
				gCal.add(Calendar.DATE, 2);
			}
			else if(gCal.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY)
			{
				gCal.add(Calendar.DATE, 1);
			}
		}

		int dayofweek = gCal.get(Calendar.DAY_OF_WEEK);

		currentmonth 		= gCal.get(Calendar.MONTH);
		currentdayofweek 	= getDay(dayofweek);
		currentday 			= gCal.get(Calendar.DAY_OF_MONTH);
		currentyear 		= gCal.get(Calendar.YEAR);
		currentweek 		= getWeekOfMonth(currentday, dayofweek) - 1;

		if(refresh)
		{
			initArray();
			setMonth();
			getData();
		}
	}

	public void setRefresh(boolean b)
	{
		refresh = b;
	}
	
	public String getMonth()
	{
		return(months[gCal.get(Calendar.MONTH)]);	
	}
	
	public String getYear()
	{
		return(String.valueOf(gCal.get(Calendar.YEAR)));	
	}
	
	public int getDaysInMonth()
	{
		return(gCal.getActualMaximum(Calendar.DAY_OF_MONTH));
	}
	
	public String renderMonth()
	{
		StringBuffer sb = new StringBuffer();
		java.util.Date current = gCal.getTime();
		gCal.set(Calendar.DAY_OF_MONTH,1);
		int rowcount = 0;
		int daysinmonth = getDaysInMonth();
		boolean pad = false;
		for(int i=0; i<daysinmonth; i++)
		{
			int day = gCal.get(Calendar.DAY_OF_WEEK);
			if(i==0)
			{
				if(day==Calendar.SUNDAY)
				{
					rowcount = rowcount + 1;
					pad = true;
				}	
				if(rowcount == currentweek && currentmode.equals(weekmode))
				{
					sb.append("<tr class=\"weekhighlight\">\r\n");
				}
				else
				{
					sb.append("<tr>\r\n");
				}
				rowcount = rowcount + 1;
				if(day==Calendar.MONDAY)
				{
					sb.append("<td></td>");
				}
				if(day==Calendar.TUESDAY)
				{
					sb.append("<td></td><td></td>");
				}
				else if(day==Calendar.WEDNESDAY)
				{
					sb.append("<td></td><td></td><td></td>");
				}
				else if(day==Calendar.THURSDAY)
				{
					sb.append("<td></td><td></td><td></td><td></td>");
				}
				else if(day==Calendar.FRIDAY)
				{
					sb.append("<td></td><td></td><td></td><td></td><td></td>");
				}
				else if(day==Calendar.SATURDAY)
				{
					sb.append("<td></td><td></td><td></td><td></td><td></td><td></td>");
				}				
			}
			
			sb.append("<td class=\"cell");
			if(hasappointments[i])
			{
				sb.append("bold");
			}
			if(currentday == i+1)
			{
				sb.append("highlight");
			}
			sb.append("\">");

			if(day!=Calendar.SUNDAY&&day!=Calendar.SATURDAY)
			{
				sb.append("<span class=\"");
				sb.append("daycell");
				if(today == i+1 && thismonth == currentmonth)
				{
					sb.append("bold");
				}
				sb.append("\">");
				
				sb.append("<a href=\"");
				if(contextpath!=null)
				{
					sb.append(contextpath);
					sb.append("/runway/");
				}
				sb.append("calendar.view?-keepParams=true&-mode=");
				
				sb.append(currentmode);
				sb.append("&-action=");
				sb.append(String.valueOf(i+1));
				sb.append("&filterUserID=");
				sb.append(userid);
				sb.append("\">");

				sb.append(String.valueOf(i+1));
				sb.append("</a>");
				sb.append("</span>");
			}
			else
			{
				sb.append("<span class=\"");
				sb.append("weekendcell");
				if(today == i+1 && thismonth == currentmonth)
				{
					sb.append("bold");
				}
				sb.append("\">");
				sb.append(String.valueOf(i+1));
				sb.append("</span>");
			}

			sb.append("</td>");
			if(day==Calendar.SATURDAY)
			{
				sb.append("</tr>");
				if(i+1!=daysinmonth)
				{
					if(rowcount == currentweek && currentmode.equals(weekmode))
					{
						sb.append("<tr class=\"weekhighlight\">\r\n");
					}
					else
					{
						sb.append("<tr>\r\n");
					}
					rowcount = rowcount + 1;
				}
			}
			gCal.add(Calendar.DATE, 1);
		}
		sb.append("</tr>\r\n");
		if(rowcount<6 || pad)
		{
			sb.append("<tr>\r\n<td><br><!--");
			sb.append(String.valueOf(rowcount));
			sb.append("--></td>\r\n</tr>\r\n");	
		}
		
		gCal.setTime(current);
		return(sb.toString());
	}

	public String renderPrintMonth(int add)
	{
		StringBuffer sb = new StringBuffer();
		java.util.Date current = gCal.getTime();
		gCal.set(Calendar.DAY_OF_MONTH,1);
		gCal.add(Calendar.MONTH, add);
		int rowcount = 0;
		int daysinmonth = getDaysInMonth();
		boolean pad = false;
		sb.append("<tr><td colspan=\"7\" class=\"headerCell\">");
		sb.append(getMonth());
		sb.append(" ");
		sb.append(getYear());
		sb.append("</td></tr>");
		for(int i=0; i<daysinmonth; i++)
		{
			int day = gCal.get(Calendar.DAY_OF_WEEK);
			if(i==0)
			{
				if(day==Calendar.SUNDAY)
				{
					rowcount = rowcount + 1;
					pad = true;
				}	

				sb.append("<tr>\r\n");

				rowcount = rowcount + 1;
				if(day==Calendar.MONDAY)
				{
					sb.append("<td></td>");
				}
				if(day==Calendar.TUESDAY)
				{
					sb.append("<td></td><td></td>");
				}
				else if(day==Calendar.WEDNESDAY)
				{
					sb.append("<td></td><td></td><td></td>");
				}
				else if(day==Calendar.THURSDAY)
				{
					sb.append("<td></td><td></td><td></td><td></td>");
				}
				else if(day==Calendar.FRIDAY)
				{
					sb.append("<td></td><td></td><td></td><td></td><td></td>");
				}
				else if(day==Calendar.SATURDAY)
				{
					sb.append("<td></td><td></td><td></td><td></td><td></td><td></td>");
				}				
			}
			
			if(day!=Calendar.SUNDAY&&day!=Calendar.SATURDAY)
			{
				sb.append("<td class=\"");
				sb.append("daycell");
				sb.append("\">");
				sb.append(String.valueOf(i+1));
				sb.append("</td>");
			}
			else
			{
				sb.append("<td class=\"");
				sb.append("weekendcell");
				sb.append("\">");
				sb.append(String.valueOf(i+1));
				sb.append("</td>");
			}

			if(day==Calendar.SATURDAY)
			{
				sb.append("</tr>");
				if(i+1!=daysinmonth)
				{
					sb.append("<tr>\r\n");
					rowcount = rowcount + 1;
				}
			}
			gCal.add(Calendar.DATE, 1);
		}
		sb.append("</tr>\r\n");
		if(rowcount<6 || pad)
		{
			sb.append("<tr>\r\n<td><br></td>\r\n</tr>\r\n");
		}
		
		gCal.setTime(current);
		return(sb.toString());
	}

	public String getDateString()
	{
		return(getDateString(currentweek,currentdayofweek));
	}

	public String getDateString(int day)
	{
		return(getDateString(currentweek,day));
	}

	public String getDateString(int week, int day)
	{
		StringBuffer datestring = new StringBuffer();
		int monthvalue = currentmonth + 1;
		int dayvalue = dates[week][day];
		if(week>3 && dayvalue<7)
		{
			monthvalue = monthvalue + 1;
		}
		if(week<2 && dayvalue>20)
		{
			monthvalue = monthvalue - 1;
		}
		if (dates[week][day] < 10) {
		   datestring.append("0");
		}
		datestring.append(String.valueOf(dayvalue));
		datestring.append("/");
		if (monthvalue < 10) {
		   datestring.append("0");
		}
		datestring.append(String.valueOf(monthvalue));
		datestring.append("/");
		datestring.append(String.valueOf(currentyear));
		return(datestring.toString());
	}
	
	protected class Appointment
	{
		public String id = null;
		public String appointment = null;
		public String duration = null;
		public String subject = null;
		public String time = null;
		public String date = null;
		public boolean overlap = false;
		public String contactid = null;
		public String companyid = null;
		public String contactname = null;
		public String contactcompany = null;
		public String contactaddress = null;
		public String status = null;
		public String noterepid = null;
		public String attdrepid = null;
		public int blocks = -1;
	}	
	
	public static void main(String[] args)
	{
		System.out.println(getEndDate("30/06/2004 10:30PM","0:30"));
	}
}
