package com.sok.runway.autopilot;

import com.sok.framework.GenRow;
import com.sok.runway.autopilot.*;
import com.sok.runway.crm.*;
import com.sok.runway.RunwayEntity; 

import java.sql.*;
import javax.servlet.ServletContext;

public interface ActionTypeEmail  {
    
   public void init(ServletContext context, AutoPilotSchedule schedule, Connection con);
   
   public boolean needsSending();
   
   public void setContextPath(String contextPath);
   
   public void setEntity(RunwayEntity entity);
   
   public void setAutoPilotSchedule(AutoPilotSchedule schedule);
   
   public void setEmailFrom(String emailFrom);
 
   public void setEmailReplyTo(String emailReplyTo);
   
   public boolean isHTML();
   
   public String getContextPath();
   
   public RunwayEntity getEntity();
   
   public String getEmailFrom();
   
   public String getEmailReplyTo();
   
   public String getEmailCC();
   
   public void setEmailCC(String emailCC);
   
   public AutoPilotSchedule getAutoPilotSchedule();
   
   public void write(String s);
   
   public String getOutput();
   
   public boolean generateEmail(String noteID);
   
   public void setSpecificEmailTokens(String id, GenRow requestrow);
   
   public boolean generateHTMLEmail(String noteID);
   
   public boolean generateTextEmail(String noteID);
   
   public void clear();
}