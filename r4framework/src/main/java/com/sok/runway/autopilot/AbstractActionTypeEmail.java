package com.sok.runway.autopilot;

import com.sok.Debugger;
import com.sok.framework.*;
import com.sok.runway.offline.*;
import com.sok.runway.crm.*;
import com.sok.runway.RunwayEntity; 

import java.sql.Connection;
import java.io.*;
import java.util.*;
import javax.servlet.ServletContext;

public abstract class AbstractActionTypeEmail implements ActionTypeEmail {
    
   private String contextPath;
   private RunwayEntity entity;
   private AutoPilotSchedule schedule;
   ServletContext context;
   protected Connection con = null;
   private String emailFrom;
   private String emailReplyTo;
   private String emailCC = ActionBean._blank; 
   private StringBuffer output = null;
   
   private boolean isHTML = true;
   
   public AbstractActionTypeEmail() {
   }  
 
   public void init(ServletContext context, AutoPilotSchedule schedule, Connection con) {
      output = new StringBuffer();
      this.con = con;
      this.context = context;
      setAutoPilotSchedule(schedule);
   }
   
   public void setContextPath(String contextPath) {
      this.contextPath = contextPath;
   }
   
   public void setEntity(RunwayEntity entity) {
      this.entity = entity;
   }
   
   public void setAutoPilotSchedule(AutoPilotSchedule schedule) {
      this.schedule = schedule;
   }
 
   public void setEmailFrom(String emailFrom) {
      this.emailFrom = emailFrom;
   }
 
   public void setEmailReplyTo(String emailReplyTo) {
      this.emailReplyTo = emailReplyTo;
   }
   
   public void setEmailCC(String emailCC) {
      this.emailCC = emailCC;
   }
   
   public boolean isHTML() {
      return isHTML;
   }
   
   public String getContextPath() {
      return contextPath;
   }
   
   public RunwayEntity getEntity() {
      return entity;
   }
   
   public String getEmailFrom() {
      return emailFrom;
   }
   
   public String getEmailReplyTo() {
      return emailReplyTo;
   }
   
   public String getEmailCC() {
      return emailCC;
   }
	   
   
   public AutoPilotSchedule getAutoPilotSchedule() {
      return schedule;
   }
   
   public void write(String s) {
      output.append(s);
   }
   
   public String getOutput() {
      return output.toString();
   }
   
   public boolean generateEmail(String noteID) {
      if (entity.isLoaded()) {
         
         if (entity instanceof Contact &&  AutoPilot.EMAIL_TYPE_PLAIN_TEXT.equals(entity.getField("EmailType")) ) {
            return generateTextEmail(noteID);
         }
         else {
            return generateHTMLEmail(noteID);
         }
      }
      else {
         return false;
      }
   }
   
   public boolean generateHTMLEmail(String noteID) {
      isHTML = true;
      return false;
   }
   
   public boolean generateTextEmail(String noteID) {
      isHTML = false;
      return false;
   }
   
   public void setSpecificEmailTokens(String id, GenRow requestrow) {}
   
   public void clear() {
      emailFrom = null;
      //output = new StringBuffer();
      output.delete(0,output.length());
      
      isHTML = true;
   }
}