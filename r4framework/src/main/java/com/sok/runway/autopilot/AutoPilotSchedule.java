package com.sok.runway.autopilot;

import com.sok.framework.GenRow;
import com.sok.framework.TableBean;
import javax.servlet.ServletRequest;
import com.sok.runway.*;
import com.sok.runway.validationRule.MandatoryValidationRule;
import com.sok.runway.validationRule.OrValidationRule;
import org.apache.commons.lang.StringUtils;

import java.sql.*;
import java.util.*;

public class AutoPilotSchedule extends RunwayEntity {
   
   public static final String UNIT_TYPE_MINUTE  = "Minutes";
   public static final String UNIT_TYPE_HOUR    = "Hours";
   public static final String UNIT_TYPE_DAY     = "Days";
   public static final String UNIT_TYPE_WEEK    = "Weeks";
   public static final String UNIT_TYPE_MONTH   = "Months";
   public static final String[] overrideFields = {"-begin","-end","GroupID","RepUserID","RegionID","LocationID"};
   
   private GenRow util = null;
   private AutoPilot autopilot = null; 
   
   private GenRow getUtilityBean() {
      if (util == null) {
         util = new GenRow();
      }
      util.clear();
      util.setConnection(getConnection());
      return util;
   }
   
   public AutoPilotSchedule(Connection con) {
      super(con, "autopilot/AutoPilotScheduleView");
   }
	
	public AutoPilotSchedule(ServletRequest request) {
	    super(request, "autopilot/AutoPilotScheduleView");
	    populateFromRequest(request.getParameterMap());
	}
   
   public AutoPilotSchedule(Connection con, String scheduleID) {
      this(con);
      load(scheduleID);
   }
   
   public void loadNextSchedule() {
      clear();
            
      GenRow nextSchedule = getUtilityBean();
      nextSchedule.setViewSpec("autopilot/AutoPilotScheduleIDView");
      nextSchedule.setParameter("NextRunDateTime","<NOW");
      nextSchedule.setParameter("EndDateTime",">NOW");
      nextSchedule.setParameter("Active","Yes");
      nextSchedule.setParameter("InProgress","!Y");
      nextSchedule.setParameter("AutoPilotScheduleBean.BeanID|1","!NULL");
      nextSchedule.setParameter("AutoPilotScheduleDashBoard.DashBoardID|1","!NULL");
      nextSchedule.sortBy("NextRunDateTime",0);
      nextSchedule.doAction(SELECTFIRST);
      
      load(nextSchedule.getString("AutoPilotScheduleID"));
   }
   
   public void setAutoPilot(AutoPilot autopilot) { 
	   this.autopilot = autopilot; 
   }
   
   public AutoPilot getAutoPilot() { 
	   return this.autopilot; 
   }
   
   public static Collection<String> getScheduleIDs(Connection con) {
      return getScheduleIDs(con, null);
   }
   
   public static Collection<String> getScheduleIDs(Connection con, String userID) {
	  Collection<String> ids = new ArrayList<String>();
      
      GenRow scheduleList = new GenRow();
      scheduleList.setConnection(con);
      scheduleList.setViewSpec("autopilot/AutoPilotScheduleIDView");
      scheduleList.setParameter("UserID", userID);
   	  scheduleList.sortBy("Name",0);
      scheduleList.doAction(SEARCH);
      scheduleList.getResults();
      
      while (scheduleList.getNext()) {
         ids.add(scheduleList.getString("AutoPilotScheduleID"));
      }
      scheduleList.close();
      return ids;
   }
   
   public void removeAllUsers() {
      if (getAutoPilotScheduleID().length() > 0) {
         GenRow autoPilotUser = getUtilityBean();   
      	autoPilotUser.setTableSpec("autopilot/AutoPilotUsers");
      	autoPilotUser.setParameter("ON-AutoPilotScheduleID",getAutoPilotScheduleID());
      	autoPilotUser.setAction(DELETEALL);
      	autoPilotUser.doAction();
      }
   }
   
   public void addUsers(String[] userID) {
      GenRow autoPilotUser = getUtilityBean();
      autoPilotUser.setViewSpec("autopilot/AutoPilotUserView");
      autoPilotUser.setParameter("AutoPilotScheduleID", getAutoPilotScheduleID());
      autoPilotUser.setAction(INSERT);
      
      if (userID != null) {
         for (int i=0; i < userID.length; i++) {
            autoPilotUser.setParameter("UserID", userID[i]);
            autoPilotUser.createNewID();
            autoPilotUser.doAction();
         }
   	}
   }
   
   public void addContacts(String[] contactID) {
	      GenRow autoPilotUser = getUtilityBean();
	      autoPilotUser.setViewSpec("autopilot/AutoPilotUserView");
	      autoPilotUser.setParameter("AutoPilotScheduleID", getAutoPilotScheduleID());
	      autoPilotUser.setAction(INSERT);
	      
	      if (contactID != null) {
	         for (int i=0; i < contactID.length; i++) {
	            autoPilotUser.setParameter("ContactID", contactID[i]);
	            autoPilotUser.createNewID();
	            autoPilotUser.doAction();
	         }
	   	}
	   }
   
   
   public Collection<String> getUserIDsForAutoPilot() {
	   Collection<String> ids = new ArrayList<String>();
      
      if (getAutoPilotScheduleID().length() > 0) {
         GenRow list = getUtilityBean();
         list.setViewSpec("autopilot/AutoPilotUserView");
         list.setParameter("AutoPilotScheduleID", getAutoPilotScheduleID());
         list.setParameter("-join0","AutoPilotScheduleUserUser");
         list.setParameter("-select0","AutoPilotScheduleUserUser.FirstName");
         list.setParameter("-select1","AutoPilotScheduleUserUser.LastName");
         list.doAction(SEARCH);
         list.getResults();
         
         while (list.getNext()) {
        	 if(list.getString("UserID") != null && list.getString("UserID").length() > 0 &&
         			(list.getString("FirstName").length() > 0 || list.getString("LastName").length() > 0)){
        		 ids.add(list.getString("UserID"));
        	 }
         }
         list.close();
      }
      return ids;
   }
   
   public Collection<String> getContactIDsForAutoPilot() {
	   Collection<String> ids = new ArrayList<String>();
      
      if (getAutoPilotScheduleID().length() > 0) {
         GenRow list = getUtilityBean();
         list.setViewSpec("autopilot/AutoPilotUserView");
         list.setParameter("AutoPilotScheduleID", getAutoPilotScheduleID());
         list.setParameter("-join0","AutoPilotScheduleUserContact");
         list.setParameter("-select0","AutoPilotScheduleUserContact.FirstName");
         list.setParameter("-select1","AutoPilotScheduleUserContact.LastName");
         list.doAction(SEARCH);
         list.getResults();
         
         while (list.getNext()) {
        	if(list.getString("ContactID") != null && list.getString("ContactID").length() > 0 &&
        			(list.getString("FirstName").length() > 0 || list.getString("LastName").length() > 0)){
        		ids.add(list.getString("ContactID"));
        	}
         }
         list.close();
      }
      return ids;
   }
	
   public String getAutoPilotScheduleID() {
      return getField("AutoPilotScheduleID");
   }
   
   public String getName() {
      return getField("Name");
   }
   
   public String getDescription() {
      return getField("Description");
   }

   public String getDashBoardID() { 
	   return getField("DashBoardID"); 
   }
   public String getBeanID() {
      return getField("BeanID");
   }
   public String getUserID() {
      return getField("UserID");
   }
   
   public String getUnits() {
      return getField("Units");
   }
   
   public String getUnitType() {
      return getField("UnitType");
   }
   
   public String getBeanType() {
      return getField("BeanType");
   }
   
   public String getBeanString() {
      return getField("BeanString");
   }
   
   public String getBeanLayoutID() {
	   return getField("BeanLayoutID");
   }
   
   public String getBeanName() {
      return getField("BeanName");
   }
   
   public String getBeanDescription() {
      return getField("BeanDescription");
   }
   
   public AutoPilotActionType getActionType() {
      AutoPilotActionType actionType = null;
      if (getField("AutoPilotActionTypeID").length() != 0) {
         actionType = new AutoPilotActionType(getConnection());
         actionType.load(getField("AutoPilotActionTypeID"));
         if (!actionType.isLoaded()) {
            actionType = null;
         }
      }
      return actionType;
   }
   
   public boolean sendIfEmptySet() {
      return getField("SendEmptyData").equals("Yes");
   }
   
   public int getListLimit() {
      try {
         int limit = getInt("ListLimit");
         if (limit < 0) {
            limit = 200;
         }
         return limit;
      }
      catch (Exception e) {
         return 200;
      }
   }
   
   
   public boolean isActive() {
      return getField("Active").equals("Yes");
   }
   
   public void incrementSchedule() {
      java.util.Date nextRunDate = (java.util.Date)getObject("NextRunDateTime");
      
      try {
         long now = new java.util.Date().getTime();
         
         Calendar cal = new GregorianCalendar();
         cal.setTime(nextRunDate);
                  
         int units = Integer.parseInt(getUnits());
         
         while (cal.getTimeInMillis() < now) {
            
            if (getUnitType().equals(UNIT_TYPE_MINUTE)) {
               cal.add(Calendar.MINUTE,units);
            }
            else if (getUnitType().equals(UNIT_TYPE_HOUR)) {
               cal.add(Calendar.HOUR,units);
            }
            else if (getUnitType().equals(UNIT_TYPE_DAY)) {
               cal.add(Calendar.DATE,units);
            }
            else if (getUnitType().equals(UNIT_TYPE_WEEK)) {
               cal.add(Calendar.DATE,units*7);
            }
            else if (getUnitType().equals(UNIT_TYPE_MONTH)) {
               cal.add(Calendar.MONTH,units);
            }
            else {
               setField("Active", "Error");
            }
         }
         setField("NextRunDateTime", cal.getTime());
      }
      catch (Exception e) {
         setField("Active", "Error");
      }
   }
   
   public void beforeSave() {
		if (getParameter("NextRunDate").length() > 0 && getParameter("NextRunTime").length() > 0) {
		setField("NextRunDateTime", getFormattedParameter("NextRunDate") + " " + getFormattedParameter("NextRunTime"));
		}
		if (getParameter("EndDate").length() > 0 && getParameter("EndTime").length() > 0) {
		setField("EndDateTime", getFormattedParameter("EndDate") + " " + getFormattedParameter("EndTime"));
		}
      	TableBean override = new TableBean();
      	for(String f: overrideFields) {
			if(bean.get(f) != null) {
				Object fo = bean.get(f);
				if(fo instanceof String) {
					override.put(f,fo);
				} else if(fo instanceof String[]) {
					override.put(f,StringUtils.join((String[])fo, "+"));
				}
			} 
		}
		setField("OverrideData",override.toString());
   }
   
   public boolean delete() {
      removeAllUsers();
      
      return super.delete();
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("AutoPilotSchedule");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("AutoPilotScheduleID","SYSTEM ERROR");
         
         val.addValidatorRule(new OrValidationRule("Schedule must have a dashboard or a saved search",
        		 									new MandatoryValidationRule("BeanID",""), 
        		 									new MandatoryValidationRule("DashBoardID",""))); 
         
         val.addMandatoryField("Name","Schedule must have a name.");
         val.addMandatoryField("Units","Schedule must have frequency set.");
         val.addMandatoryField("UnitType","Schedule must have frequency set.");
         val.addMandatoryField("NextRunDateTime","Schedule must have start date.");
         val.addMandatoryField("EndDateTime","Schedule must have an end date.");
         
         HashSet<String> set = new HashSet<String>(5);
         set.add(UNIT_TYPE_MINUTE);
         set.add(UNIT_TYPE_HOUR);
         set.add(UNIT_TYPE_DAY);
         set.add(UNIT_TYPE_WEEK);
         set.add(UNIT_TYPE_MONTH);

         val.addRestrictedValueField("UnitType",set);
         Validator.addValidator("AutoPilotSchedule", val);
      }
      return val;
   }
}