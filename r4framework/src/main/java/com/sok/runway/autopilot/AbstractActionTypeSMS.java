/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sok.runway.autopilot;

import com.sok.runway.RunwayEntity;
import java.sql.Connection;
import javax.servlet.ServletContext;

/**
 *
 * @author emil
 */
public abstract class AbstractActionTypeSMS implements ActionTypeSMS {

   private RunwayEntity entity;
   private AutoPilotSchedule schedule;
   private StringBuffer output = null;
   private String contextPath;
   private ServletContext context;
   
   public void init(ServletContext context, AutoPilotSchedule schedule, Connection con) {
      output = new StringBuffer();
      this.context = context;
      setAutoPilotSchedule(schedule);
   }

   public void setContextPath(String contextPath) {
      this.contextPath = contextPath;
   }

   public void setEntity(RunwayEntity entity) {
      this.entity = entity;
   }

   public void setAutoPilotSchedule(AutoPilotSchedule schedule) {
      this.schedule = schedule;
   }

   public String getContextPath() {
      return contextPath;
   }

   public RunwayEntity getEntity() {
      return entity;
   }

   public AutoPilotSchedule getAutoPilotSchedule() {
      return schedule;
   }

   public abstract boolean generateSMS(String noteID);

   public void write(String s) {
      output.append(s);
   }

   public void clear() {
      output.delete(0,output.length());
   }

   public String getOutput() {
      return output.toString();
   }
}
