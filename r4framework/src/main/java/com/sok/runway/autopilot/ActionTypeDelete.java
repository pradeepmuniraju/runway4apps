package com.sok.runway.autopilot;

import com.sok.runway.RunwayEntity; 

public interface ActionTypeDelete {
	
	abstract boolean validRecord(RunwayEntity re);

	abstract void deleteRecord(RunwayEntity re, AutoPilotQueue apq, boolean debug); 
}
