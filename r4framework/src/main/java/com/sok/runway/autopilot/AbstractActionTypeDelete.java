package com.sok.runway.autopilot;

import com.sok.runway.RunwayEntity;

public class AbstractActionTypeDelete implements ActionTypeDelete {

	public boolean validRecord(RunwayEntity re) {
		return true; 
	}
	
	public void deleteRecord(RunwayEntity re, AutoPilotQueue apq, boolean debug) {
		
		if(validRecord(re)) {
			deleteAndLog(re, apq); 
		} else if(debug) { 
			apq.appendInfo("Skipped, not valid"); 
		} 
	}
	
	protected void deleteAndLog(RunwayEntity entity, AutoPilotQueue apq) { 
		if (entity.delete()) {
			apq.incrementActioned();
			apq.appendInfo("Deleted",entity.getPrimaryKey()); 
		}
		else {
			apq.appendError("Delete Failed :"+entity.getRecord().getError(), entity.getPrimaryKey());
		}
	}
}
