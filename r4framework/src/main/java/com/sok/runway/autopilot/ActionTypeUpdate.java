package com.sok.runway.autopilot;

import com.sok.runway.RunwayEntity; 

public interface ActionTypeUpdate {
	
	abstract boolean validRecord(RunwayEntity entity);

	abstract void updateRecord(RunwayEntity entity, String updString, AutoPilotQueue apq, boolean debug); 
}
