package com.sok.runway.autopilot;

import com.sok.runway.ErrorMap;
import com.sok.runway.RunwayEntity;

public class AbstractActionTypeUpdate implements ActionTypeUpdate {

	public boolean validRecord(RunwayEntity entity) {
		return true; 
	}
	
	public void updateRecord(RunwayEntity entity, String updString, AutoPilotQueue apq, boolean debug) {
		
		if(validRecord(entity)) {
			entity.populate(updString);
			updateAndLog(entity, apq); 
		} else if(debug) { 
			apq.appendInfo("Skipped, not valid"); 
		} 
	}
	
	protected void updateAndLog(RunwayEntity entity, AutoPilotQueue apq) { 
		ErrorMap errors = entity.update();
		if (errors.isEmpty()) {
			apq.incrementActioned();
			apq.appendInfo("Updated",entity.getPrimaryKey()); 
		}
		else {
			apq.appendError(errors.toString(), entity.getPrimaryKey());
		}
	}
}
