package com.sok.runway.autopilot;

import com.sok.runway.*;
import com.sok.runway.crm.*;

import java.sql.*;

public class AutoPilotActionType extends RunwayEntity {
   
   
   public AutoPilotActionType(Connection con) {
      super(con, "{autopilot/}AutoPilotActionTypeView");
   }
   
   public AutoPilotActionType(Connection con, String actionTypeID) {
      this(con);
      load(actionTypeID);
   }
   
   public String getActionType() {
      return getField("ActionType");
   }
   
   public String getClassName() {
      return getField("ClassName");
   }
   
   public String getUpdateString() {
      return getField("UpdateString");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("AutoPilotActionType");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("AutoPilotActionTypeID","SYSTEM ERROR");
         val.addMandatoryField("ActionType","ActionType must have an action type");         

         Validator.addValidator("AutoPilotActionType", val);
      }
      return val;
   }
}