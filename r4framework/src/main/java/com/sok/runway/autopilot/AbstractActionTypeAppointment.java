/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sok.runway.autopilot;

import com.sok.runway.crm.Contact;
import com.sok.runway.crm.Note;
import java.sql.*;
import javax.servlet.ServletContext;

/**
 *
 * @author emil
 */
public abstract class AbstractActionTypeAppointment extends AbstractActionTypeEmail implements ActionTypeSMS {

   private boolean isSMS = false;
   protected Note note = null;
   
   public void init(ServletContext context, AutoPilotSchedule schedule, Connection con) {
     super.init(context, schedule, con);
      
      //loadProducts(con);
   }

   public boolean isHTML() {
      return !isSMS;
   }
   
   public boolean generateSMS(String noteID) {
      if(getEntity().isLoaded() && getEntity() instanceof Contact) {
         return generateTextSMS(noteID);
      } else {
         return false;
      }
      
      
//      throw new UnsupportedOperationException("Not supported yet.");
   }

   private boolean generateTextSMS(String noteID) {
      isSMS = true;
      return isSMS;
   }

   public void setAppointment(Note note) {
      this.note = note;
   }
   
}
