/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sok.runway.autopilot;

import com.sok.runway.RunwayEntity;
import com.sok.runway.crm.*;
import com.sok.runway.offline.*;

import java.sql.*;
import javax.servlet.ServletContext;

/**
 *
 * @author emil
 */
public interface ActionTypeSMS {

   
   public void init(ServletContext context, AutoPilotSchedule schedule, Connection con);
   
   public void setContextPath(String contextPath);
   
   public void setEntity(RunwayEntity entity);
   
   public void setAutoPilotSchedule(AutoPilotSchedule schedule);
   
   public String getContextPath();
   
   public RunwayEntity getEntity();
   
   public AutoPilotSchedule getAutoPilotSchedule();
   
   public boolean generateSMS(String noteID);
   
   public void write(String s);
   
   public void clear();
   
   public String getOutput();
   
   
}
