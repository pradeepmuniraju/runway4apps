package com.sok.runway.autopilot;

import com.sok.framework.*;
import com.sok.runway.security.*;
import com.sok.runway.offline.*;

import javax.servlet.*;

import java.io.*;
import java.util.*;

public class AutoPilotQueue implements OfflineNotifier {
   
   private ServletContext context = null;
   private ArrayList<String> IDs = null;
   private User user = null;
   
   private String autoPilotName = null;
   
   private int errorCount = 0;
   private int current = 0;
   private int actioned = 0;
   
   private StringBuffer errors = null;
   private StringBuffer message = null;
   private StringBuffer info = null;
   
   private TableSpec tSpec = null;
   
   
   public AutoPilotQueue(ServletContext context, GenRow searchBean, User user, String autoPilotName) {
      this.context = context;
      this.user = user;
      this.autoPilotName = autoPilotName;
      
      IDs = new ArrayList();
      
      tSpec = searchBean.getTableSpec();
      
      String idColumn = tSpec.getPrimaryKeyName();
      
      while (searchBean.getNext()) {
         IDs.add(searchBean.getData(idColumn));
      }
      
      errors = new StringBuffer();
      message = new StringBuffer();
      info = new StringBuffer();
      
		message.append(context.getServletContextName());
		message.append(" Runway AutoPilot process - " + autoPilotName);
		message.append("\r\nInitiated ");

		message.append(new java.util.Date().toString());

		message.append("\r\n");
      
   }
   
   public boolean hasNext() {
      return current < IDs.size();
   }
   
   public String getNext() {
      current++;
      return IDs.get(current-1);
   }
   
   public String getStatusMailRecipient() {
      String email = user.getEmail();
      if (email == null || email.length() == 0) {
         Properties sysprops = InitServlet.getSystemParams();
         email = sysprops.getProperty("DefaultEmail");
      }
      return email;
   }
   
   public String getStatusMailBody() {
	   String statusstring = message.toString();
	   
		if(errors != null && errors.length() != 0){
		   statusstring = statusstring + "\r\n" + errors.toString();
		}
		return (statusstring);
	}
	
   public String getStatusMailSubject() {
      return (context.getServletContextName() + " Runway AutoPilot process - " + autoPilotName);
   }
   
   public File getStatusAttachment() {
      return null;
   }
   
   public int getProcessSize() {
      return IDs.size();
   }
   
   public int getProcessedCount() {
      return current;
   }
   
   public ServletContext getServletContext() {
      return this.context;
   }
   
   public int getErrorCount() {
      return errorCount;
   }
   
   public void incrementActioned() {
      actioned++;
   }
   ///////////////////////////
   
   public void queueComplete(boolean killed) {
   
         if (killed) {
			   message.append("\r\nAutoPilot KILLED\r\n\r\n");
         }
         
			message.append(getProcessSize());
			message.append(" ");
			message.append(tSpec.getTableName());
			message.append(" found.\r\n");
			
			if (killed) {
   			message.append(getProcessedCount());
   			message.append(" ");
   			message.append(tSpec.getTableName());
   			message.append(" processed.\r\n");
			}
			
			message.append(actioned);
			message.append(" ");
			message.append(tSpec.getTableName());
			message.append(" actioned.\r\n");
			
			message.append(getErrorCount());
			message.append(" ");
			message.append(tSpec.getTableName());
			message.append(" failed.\r\n\r\n");
			
			message.append(info);
			
		   OfflineNotificationSender notificationSender = new OfflineNotificationSender();
	      notificationSender.notify(this);
   }
	
	public void appendInfo(String theInfo) {
	   info.append(theInfo);
	   info.append("\r\n");
	}
	
	public void appendInfo(String msg, String id) {
		append(info, msg, id);
	}
	
	public void appendError(String msg, String id) {
	    errorCount++;
		append(errors, msg, id); 
	}
   
	private void append(StringBuffer sb, String msg, String id) {
		sb.append(msg);
		sb.append(" - ");
		sb.append(context.getInitParameter("URLHome") );
		sb.append("/runway/");
		sb.append(StringUtil.replace(tSpec.getPrimaryKeyName(),"ID","").toLowerCase());
		sb.append(".view?");
		sb.append(tSpec.getPrimaryKeyName());
		sb.append("=");
		sb.append(id);
		sb.append("\r\n");
	}

	@Override
	public String getStatusMailHtmlBody() {
		return null;
	}
}