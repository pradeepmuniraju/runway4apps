package com.sok.runway.autopilot;

import com.sok.Debugger;
import com.sok.runway.*;
import com.sok.runway.security.User;
import com.sok.runway.crm.*;
import com.sok.runway.crm.reports.ReportPDFServlet;
import com.sok.runway.email.*;

import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.framework.servlet.*;

import com.sok.runway.sms.SMS;
import javax.servlet.*;

import java.io.*;
import java.text.*;
import java.util.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AutoPilot extends DaemonProcess {
	
	private static final Logger logger = LoggerFactory.getLogger(AutoPilot.class); 

   public static final String STATUS_COMPANIES = "finding companies";
   public static final String STATUS_CONTACTS = "finding contacts";
   public static final String STATUS_NOTES = "finding notes";
   public static final String STATUS_RESPONSES = "finding responses";
   public static final String STATUS_OPPORTUNITIES = "finding opportunities";
   public static final String STATUS_ORDERS = "finding orders";
   public static final String STATUS_DOCUMENTS = "finding documents";
   public static final String STATUS_PRODUCTS = "finding products";
   public static final String STATUS_USERS = "finding users";
   public static final String STATUS_APPOINTMENTS = "finding attendees";
   public static final String STATUS_REPORT = "creating report";
   public static final String STATUS_DASHBOARD = "creating dashboard";
   public static final String STATUS_CONTACTS_PROCESS = "processing contacts";
   public static final String STATUS_PRODUCTS_PROCESS = "processing products";
   public static final String STATUS_ORDERS_PROCESS = "processing orders";
   public static final String STATUS_OPPORTUNITIES_PROCESS = "processing opportunities";
   public static final String ATTRIBUTE_EMAIL_FROM = "-emailFrom";
   public static final String ATTRIBUTE_EMAIL_REPLY_TO = "-emailReplyTo";
   public static final String ATTRIBUTE_EMAIL_TYPE = "-emailType";
   public static final String ATTRIBUTE_SEND_EMAIL = "-sendEmail";
   public static final String EMAIL_TYPE_HTML = "HTML";
   public static final String EMAIL_TYPE_PLAIN_TEXT = "Plain Text";
   private static final String file_Date_Format = "yyyy_MM_dd_HH_mm";
   private static final String readable_Date_Format = "h:mma d MMM yyyy";
   /* Action Types */
   public static final String ACTION_SEND_REMINDER = "SendGeneratedReminder";
   public static final String ACTION_SEND_GENERATED_SMS = "SendGeneratedSMS";
   public static final int DAYS_IN_ADVANCE = 0;
   public static final String ACTION_SEND_TEMPLATE = "SendTemplate";
   //public static final String ACTION_SEND_JSP   = "SendPage";
   public static final String ACTION_SEND_GENERATED_EMAIL = "SendGeneratedEmail";
   public static final String ACTION_SEND_EMAIL_TEMPLATE = "SendEmailTemplate";
   public static final String ACTION_UPDATE_RECORD = "UpdateRecord";
   public static final String ACTION_DELETE_RECORD = "DeleteRecord";
   public static final String AUTOPILOT_FILE_PATH = "/files/autopilot/";
   private static final long THIRTY_DAYS_IN_MILLIS = 2592000000l;
   String name = "AutoPilot";
   private Connection con = null;
   private ServletContext context = null;
   private com.sok.framework.servlet.HttpSession session = null;
   private UserBean currentuser = null;
   private SimpleDateFormat dateFormat = null;
   private File autopilotFolder = null;
   private CustomReportBean report = null;

   public AutoPilot(DaemonFactory daemonFactory) {
      super(daemonFactory);
      context = daemonFactory.getServletContext();

      session = new com.sok.framework.servlet.HttpSession(context); 

      currentuser = new UserBean();

      dateFormat = new SimpleDateFormat(file_Date_Format);

      autopilotFolder = new File(context.getRealPath(AUTOPILOT_FILE_PATH));
      autopilotFolder.mkdir();

   }

   @Override
   public void killProcess(boolean sendNotification) {
	   super.killProcess(sendNotification);
	   context = null;
	   session = null; 
   }
   
   public String getName() {
      return name;
   }

   public HttpSession getSession() {
      return session;
   }

   public void run() {
      try {

         int errorcount = 0;


         AutoPilotSchedule schedule = new AutoPilotSchedule(getConnection());
         schedule.setAutoPilot(this);
         // LOOP WHILE THE PROCESS IS STILL ALIVE (ie Hasn't been killed)
         while (!isKilled()) {
            setRunning(true);

            try{
            
               // LOOP WHILE RUNNING (ie Has work to do)
               while (!isKilled() && isRunning()) {
   
                  // Don't do any work if we are paused
                  while (!isKilled() && isRunning() && !isPaused()) {
                     schedule.setConnection(getConnection());
                     schedule.loadNextSchedule();
   
   
                     session.setAttribute("autoPilotSchedule", schedule);
                     
                     if (session.getAttribute("autoPilotSchedule") != null) {
                    	 //System.out.println("Auto Pilot " + context.getRealPath("./") + " Run " + ((AutoPilotSchedule) session.getAttribute("autoPilotSchedule")).toString());
                    	 logger.info("Auto Pilot {} Run {}", context.getRealPath("./"), ((AutoPilotSchedule) session.getAttribute("autoPilotSchedule")).toString());
                     } else {
                    	 //System.out.println("Auto Pilot " + context.getRealPath("./") + " Run Failed");
                    	 logger.error("Auto Pilot {} Run Failed", context.getRealPath("./"));
                     }
   
                     if (schedule.getAutoPilotScheduleID().length() == 0) {
                        // No work to be done at this time
                        setRunning(false);
                     } else {
                         schedule.setField("LastRunDateTime", new java.util.Date());
                         schedule.setField("InProgress","Y");
                         schedule.update();
                         loadCurrentUser(schedule.getUserID());
                    	 if (schedule.getBeanType().equals("Companies")) {
	                        setStatus(STATUS_COMPANIES);
	                        processCompanies(schedule);
	                     } else if (schedule.getBeanType().equals("Contacts")) {
	                        setStatus(STATUS_CONTACTS);
	                        processContacts(schedule);
	                     } else if (schedule.getBeanType().equals("Notes")) {
	                        setStatus(STATUS_NOTES);
	                        processNotes(schedule);
	                     } else if (schedule.getBeanType().equals("Responses")) {
	                        setStatus(STATUS_RESPONSES);
	                        processResponses(schedule);
	                     } else if (schedule.getBeanType().equals("Opportunities")) {
	                        setStatus(STATUS_OPPORTUNITIES);
	                        processOpportunities(schedule);
	                     } else if (schedule.getBeanType().equals("Orders")) {
	                        setStatus(STATUS_ORDERS);
	                        processOrders(schedule);
	                     } else if (schedule.getBeanType().equals("Documents")) {
	                        setStatus(STATUS_DOCUMENTS);
	                        processDocuments(schedule);
	                     } else if (schedule.getBeanType().equals("Products")) {
	                        setStatus(STATUS_PRODUCTS);
	                        processProducts(schedule);
	                     } else if (schedule.getBeanType().equals("Report") || (schedule.getDashBoardID() != null && (schedule.getDashBoardID()).length() > 0)) {
	                    	if(schedule.getDashBoardID() != null && (schedule.getDashBoardID()).length() > 0){
	                    		setStatus(STATUS_DASHBOARD);
	                    	}else{
	                    		setStatus(STATUS_REPORT);
	                    	}
	                    	/* 	this logging is duplicated above.
	                        if (session.getAttribute("autoPilotSchedule") != null) {
	                       	 System.out.println("Auto Pilot Run Report " + ((AutoPilotSchedule) session.getAttribute("autoPilotSchedule")).toString());
	                        } else {
	                       	 System.out.println("Auto Pilot Run Report Failed");
	                        }
	                        */
	                        processReport(schedule);
	                     } else if (schedule.getBeanType().equals("Users")) {
	                        setStatus(STATUS_USERS);
	                        processUsers(schedule);
	                     }
                    	 logger.info("Just processed {}", schedule.getBeanType());
                         //com.sok.Debugger.getDebugger().debug("Just Processed " + schedule.getBeanType());
                     }
   
                     if (session.getAttribute("autoPilotSchedule") != null) {
                    	 //System.out.println("Auto Pilot " + context.getRealPath("./") + " Run Last " + ((AutoPilotSchedule) session.getAttribute("autoPilotSchedule")).toString());
                    	 logger.info("Auto Pilot {} Run Last {}", context.getRealPath("./"), ((AutoPilotSchedule) session.getAttribute("autoPilotSchedule")).toString());
                     } else {
                    	 //System.out.println("Auto Pilot " + context.getRealPath("./") + " Run Last Failed");
                    	 logger.error("Auto Pilot {} Run Last Failed", context.getRealPath("./"));
                     }
                     
                     session.setAttribute("autoPilotSchedule", null);
   
                     // If we just did some work, increment the schedule
                     if (isRunning() && !isKilled()) {
                        schedule.setConnection(getConnection());

                        schedule.setField("InProgress","N");

                        schedule.incrementSchedule();
   
                        ErrorMap errors = schedule.update();
                        if (!errors.isEmpty()) {
                           addError("VALIDATION ERROR" + errors.toString());
                           killProcess();
                        }else{
                           errorcount = 0;
                        }
                        // Close connection as we may be sleeping for a while after this
                        try {
                           con.close();
                        } catch (Exception e) {
                        }
                        waitUnlessKilled(1);
                     }
                     Thread.yield();
                  }
                  // If we are paused, sleep for 10 seconds
                  if (!isKilled() && isPaused()) {
                     waitUnlessKilled(10);
                  }
               }
               // When there is no work to be done, do some house keeping.
               if (!isKilled()) {
                  removeOldFiles();
                  waitUnlessKilled(60);
               }
            }catch(Exception e){
               errorcount++;
               if(errorcount>5){
                  throw e;
               }else{
                  addError(e);
               }
            }
         }
      } catch (Exception e) {
         addError(e);
         // Suicide if we hit an error
         killProcess();
      }
      try {
         con.close();
      } catch (Exception e) {
      }
   }

   private Connection getConnection() {
      try {
         if (con == null || con.isClosed()) {
        	 con = ActionBean.connect(); 
         }
      } catch (Exception ex) {
    	  logger.error("Error on connection", ex);
      } 
      return con;
   }

   private void processAppointmentsActionType(AutoPilotActionType actionType, GenRow searchbean, AutoPilotSchedule schedule) {
      
      try {
        
       if(ACTION_SEND_GENERATED_EMAIL.equals(actionType.getActionType())) {
          String className = actionType.getField("ClassName");
             Class emailClass = Class.forName(className);
             ActionTypeEmail emailGenerator = (ActionTypeEmail) emailClass.newInstance();
             emailGenerator.setContextPath(InitServlet.getSystemParams().getProperty("URLHome"));
             emailGenerator.init(context, schedule, getConnection());
             
             session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
             searchbean.setConnection(getConnection());
             searchbean.getResults();
             
             User user = new User(getConnection());
             user.setCurrentUser(currentuser.getUser());
                          
             /* Use the bean string to make a more general search */                                
             emailGenerator.clear();
                
             GenRow searchBean = new GenRow();
             searchBean.setConnection(getConnection());
             searchBean.parseRequest(schedule.getBeanString());
                             
             emailGenerator.setSpecificEmailTokens(null, searchBean);
             String noteID = KeyMaker.generate();

             if (emailGenerator.generateEmail(noteID)) {

                try {
                   String emailFrom = emailGenerator.getEmailFrom();
                   String emailReplyTo = emailGenerator.getEmailReplyTo();
                   String emailCC = emailGenerator.getEmailCC(); 
                   String emailBody = emailGenerator.getOutput();
                  
                   com.sok.Debugger.getDebugger().debug("EmailFrom"+emailFrom);
                   com.sok.Debugger.getDebugger().debug("emailReplyTo"+emailReplyTo);
                   com.sok.Debugger.getDebugger().debug("emailCC"+emailCC);
                   com.sok.Debugger.getDebugger().debug("emailBody"+emailBody);
                  
                   
                   String emailresponse = sendEmail(schedule, emailBody, null, null, null, emailCC, actionType.getField("Subject"), false);

                   if (emailresponse.equals("Message Sent")) {                      
                      com.sok.Debugger.getDebugger().debug("Email sent");
                   } else {
                    com.sok.Debugger.getDebugger().debug("not sent"+emailresponse);                      
                   }

                } catch(Exception e) {
                     com.sok.Debugger.getDebugger().debug("Error: "+e);
                }                   
                } else {
                  com.sok.Debugger.getDebugger().debug("Dont need to send");
                }
                // If we are paused, sleep for 10 seconds
                while (!isKilled() && isPaused()) {
                   wait(10);
                }                   
       } else if (ACTION_SEND_REMINDER.equals(actionType.getActionType())) {

//            String className = actionType.getField("ClassName");
//            Class appointmentClass = Class.forName(className);
//            AbstractActionTypeAppointment appointmentGenerator = (AbstractActionTypeAppointment) appointmentClass.newInstance();
//            appointmentGenerator.setContextPath(InitServlet.getSystemParams().getProperty("URLHome"));
//            appointmentGenerator.init(context, schedule, getConnection());
            searchbean.setConnection(getConnection());
            searchbean.getResults();
            AutoPilotQueue appointmentQueue = new AutoPilotQueue(context, searchbean, currentuser.getUser(), actionType.getActionType() + ": " + schedule.getField("Name"));

            Note note = new Note(getConnection());
            note.setCurrentUser(currentuser.getUser());
            Contact contact = new Contact(getConnection());
            String optOutStatusID = InitServlet.getSystemParams().getProperty("ContactOptOutStatusID");
            String leftCompanyStatusID = InitServlet.getSystemParams().getProperty("ContactLeftCompanyStatusID");

            while (!isKilled() && appointmentQueue.hasNext()) {
               setStatus(STATUS_CONTACTS_PROCESS, appointmentQueue.getProcessedCount(), appointmentQueue.getProcessSize());
               note.setConnection(getConnection());
               note.load(appointmentQueue.getNext());
               GenRow searchAttendees = new GenRow();
               searchAttendees.setConnection(getConnection());
               searchAttendees.setViewSpec("AttendeeView");
               searchAttendees.setParameter("NoteID", note.getPrimaryKey());
               searchAttendees.setParameter("UserID", "EMPTY+NULL");
               searchAttendees.getResults();

               d.debug("NoteID " + note.getPrimaryKey() + "\r\n");

               while (!isKilled() && searchAttendees.getNext()) {
                  d.debug("Attendee " + searchAttendees.getData("ContactID") + "\r\n");
//                  appointmentGenerator.clear();
                  contact.load(searchAttendees.getData("ContactID"));
                  String email = contact.getField("Email");
                  String mobile = contact.getField("Mobile");
                  String statusID = contact.getField("CurrentStatusID");
                  if (contact.isLoaded() && email.length() != 0 && !statusID.equals(optOutStatusID) && !statusID.equals(leftCompanyStatusID)) {
                     String contactID = searchAttendees.getData("ContactID");
                     GenRow requestRow = new GenRow();
                     requestRow.setParameter("TemplateID", actionType.getField("TemplateID"));
                     if (actionType.getField("Subject").length() != 0) {
                        requestRow.setParameter("Subject", actionType.getField("Subject"));
                     }
                     requestRow.setParameter("ContactID", contactID);
                     requestRow.setParameter("AppointmentID", note.getPrimaryKey());
                     requestRow.setParameter("AttendeeID", searchAttendees.getData("AttendeeID"));

                     requestRow.putInternalToken(SessionKeys.CURRENT_USER, currentuser);
                     String numberOfSeats = searchAttendees.getData("Attending");
//                   com.sok.Debugger.getDebugger().debug("Attending = " + numberOfSeats + " <br>");
                     if (numberOfSeats != null) {
                        requestRow.setParameter("Attending", numberOfSeats);
                     } else {
                        requestRow.setParameter("Attending", "");
                     }


                     Emailer emailer = EmailerFactory.getContactEmailer(requestRow, context);
                     emailer.getMailerBean().connect();
                     int reconnectAttempt = 0;
                     int currentsent = 0;
                     int currentfailed = 0;

                     //  Send the Email
                     if (reconnectAttempt > 10) {
                        appointmentQueue.appendInfo("Aborting offline send at " + appointmentQueue.getProcessedCount() + ", due to too many consecutive failed email server connections.");
                        break;
                     }

                     if (!emailer.getMailerBean().isConnected()) {
                        reconnectAttempt++;

                        appointmentQueue.appendInfo("Reconnecting to email server (Attempt:" + reconnectAttempt + ", current run Sent:" + currentsent + " Failed:" + currentfailed + ").");
                        emailer.getMailerBean().connect();
                        currentsent = 0;
                        currentfailed = 0;
                     } else {
                        reconnectAttempt = 0;
                     }

                     if (emailer.getSpecificContextString("CreatedDate") == null) {
                        appointmentQueue.appendError("Record not found", contactID);
                        currentfailed++;

                     } else {
                        com.sok.Debugger.getDebugger().debug("9");
                        try {
                           String emailstatus = emailer.generateSendAndSave();//send();

                           if (emailstatus.equals(Emailer.SENT)) {
                              com.sok.Debugger.getDebugger().debug("Email sent\n");
                              //System.out.println("Email has been sent");
                              logger.debug("Email has been sent");

                              appointmentQueue.incrementActioned();
                              currentsent++;

                           } else {
                              com.sok.Debugger.getDebugger().debug("Email not sent\n");
                              appointmentQueue.appendError(emailstatus, contactID);
                              currentfailed++;
                           }

                        } catch (Exception e) {
                           appointmentQueue.appendError(e.toString(), contactID);
                           currentfailed++;

                        }
                     }

                  } else if (contact.isLoaded() && mobile.length() != 0 && !statusID.equals(optOutStatusID) && !statusID.equals(leftCompanyStatusID)) {

                     String contactID = searchAttendees.getData("ContactID");
                     GenRow requestRow = new GenRow();
                     requestRow.setParameter("SMSTemplateID", actionType.getField("SMSTemplateID"));
                     if (actionType.getField("Subject").length() != 0) {
                        requestRow.setParameter("Subject", actionType.getField("Subject"));
                     }
                     requestRow.setParameter("ContactID", contactID);
                     requestRow.setParameter("AppointmentID", note.getPrimaryKey());

                     requestRow.putInternalToken(SessionKeys.CURRENT_USER, currentuser);

                     SMS sms = new SMS(requestRow, context);

                     sms.getSMSBean().connect();

                     try {
                        String smsStatus = sms.generateSendAndSave();//send();                          

                        if (smsStatus.equals(SMS.SENT)) {
                           com.sok.Debugger.getDebugger().debug("Message sent <br>\n");
                        } else {
                           com.sok.Debugger.getDebugger().debug("Message not sent <br>\n");
                        }
                     } catch (Exception e) {
                        com.sok.Debugger.getDebugger().debug("Exception " + e + "<br>\n");
                        appointmentQueue.appendError(e.toString(), contactID);
                     }

                  } else {
                     //System.out.println("Autopilot did not need sending");
                     logger.debug("Autopilot did not need sending");
                  }
                  while (!isKilled() && isPaused()) {
                     wait(10);
                  }

                  Thread.yield();
               }
            }
            appointmentQueue.queueComplete(isKilled());
         } else {
            //System.out.println("ActionType not found0");
            logger.debug("ActionType not found0");
         }

      } catch (Exception e) {
         addError(e);
         killProcess();
      }
   }

   public void processCompanies(AutoPilotSchedule schedule) {

      TableBean searchbean = new TableBean();
      searchbean.setViewSpec("CompanyListView");
      searchbean.parseRequest(schedule.getBeanString());
      currentuser.setConstraint(searchbean, "CompanyGroup");
      searchbean.setAction(ActionBean.SEARCH);
      searchbean.doAction();

      session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
      session.setAttribute(SessionKeys.COMPANY_SEARCH, searchbean);

      String pdfPath = generatePDF(schedule, "/crm/reports/companylistpdf.jsp?-action=search");

      Integer resultCount = (Integer) session.getAttribute("-resultCount");
      if (resultCount == null || resultCount.intValue() > 0 || schedule.sendIfEmptySet()) {
         pdfPath = "/crm/autopilot/emails/companylist.jsp?pdfPath=" + StringUtil.urlEncode(pdfPath);
         sendNotifyEmail(schedule, generateEmail(schedule, pdfPath));
      }

      session.setAttribute(SessionKeys.CURRENT_USER, null);
      session.setAttribute(SessionKeys.COMPANY_SEARCH, null);
   }

   public void processContacts(AutoPilotSchedule schedule) {
      AutoPilotActionType actionType = schedule.getActionType();

      GenRow searchbean = new GenRow();
      searchbean.setViewSpec("ContactListView");
      searchbean.parseRequest(schedule.getBeanString());
      currentuser.setConstraint(searchbean, "ContactGroup");
      searchbean.doAction(ActionBean.SEARCH);

      if (actionType != null) {
         processContactsActionType(actionType, searchbean, schedule);
      } else {
         session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
         session.setAttribute(SessionKeys.CONTACT_SEARCH, searchbean);

         String printPath = searchbean.getParameter("-printPath");
         String printSuffix = searchbean.getParameter("-printSuffix");
         
         if(printPath == null || printPath.length()==0) { 
          printPath = "/crm/reports/contactlistpdf.jsp?-action=search";
         }
         if(printSuffix == null || printSuffix.length()==0) { 
          printSuffix = ".pdf";
         }
         
         String pdfPath = generatePDF(schedule, printPath, null, printSuffix);

         Integer resultCount = (Integer) session.getAttribute("-resultCount");
         if (resultCount == null || resultCount.intValue() > 0 || schedule.sendIfEmptySet()) {
            pdfPath = "/crm/autopilot/emails/contactlist.jsp?pdfPath=" + StringUtil.urlEncode(pdfPath);
            sendNotifyEmail(schedule, generateEmail(schedule, pdfPath));
         }

         session.setAttribute(SessionKeys.CURRENT_USER, null);
         session.setAttribute(SessionKeys.CONTACT_SEARCH, null);
      }
   }
   
   private void processActionTypeUpdate(AutoPilotActionType actionType, RunwayEntity e, GenRow searchbean, AutoPilotSchedule aps, String process) 
            throws ClassNotFoundException, IllegalAccessException, InstantiationException { 
      
       searchbean.setConnection(getConnection());
       searchbean.getResults();
       
       AutoPilotQueue apq = new AutoPilotQueue(context, searchbean, currentuser.getUser(), actionType.getActionType() + ": " + aps.getField("Name"));
       apq.appendInfo("Update String: " + actionType.getUpdateString());
       
      String className = actionType.getClassName();
       ActionTypeUpdate recordUpdater = null;

       if (className != null && className.length() > 0) {
         recordUpdater = (ActionTypeUpdate) Class.forName(className).newInstance();
       } else {
         recordUpdater = new AbstractActionTypeUpdate();
       }

       while (!isKilled() && apq.hasNext()) {

          setStatus(process, apq.getProcessedCount(), apq.getProcessSize());
          e.load(apq.getNext());

          recordUpdater.updateRecord(e, actionType.getUpdateString(), apq, false);

          // If we are paused, sleep for 10 seconds
          while (!isKilled() && isPaused()) {
             waitUnlessKilled(10);
          }
          Thread.yield();
       }

       apq.queueComplete(isKilled());
   }
   
   private void processActionTypeDelete(AutoPilotActionType actionType, RunwayEntity e, GenRow searchbean, AutoPilotSchedule aps, String process) 
      throws ClassNotFoundException, IllegalAccessException, InstantiationException { 

      searchbean.setConnection(getConnection());
      searchbean.getResults();
      
      AutoPilotQueue apq = new AutoPilotQueue(context, searchbean, currentuser.getUser(), actionType.getActionType() + ": " + aps.getField("Name"));
      apq.appendInfo("Update String: " + actionType.getUpdateString());
      
      String className = actionType.getClassName();
      ActionTypeDelete recordUpdater = null;
      
      if (className != null && className.length() > 0) {
        recordUpdater = (ActionTypeDelete) Class.forName(className).newInstance();
      } else {
        recordUpdater = new AbstractActionTypeDelete();
      }
      
      while (!isKilled() && apq.hasNext()) {
      
       setStatus(process, apq.getProcessedCount(), apq.getProcessSize());
       e.load(apq.getNext());
      
       recordUpdater.deleteRecord(e, apq, false);
      
       // If we are paused, sleep for 10 seconds
       while (!isKilled() && isPaused()) {
          waitUnlessKilled(10);
       }
       Thread.yield();
      }
      
      apq.queueComplete(isKilled());
   }

   private void processContactsActionType(AutoPilotActionType actionType, GenRow searchbean, AutoPilotSchedule schedule) {

      try {
         if (ACTION_UPDATE_RECORD.equals(actionType.getActionType())) {

            Contact contact = new Contact(getConnection());
            contact.setCurrentUser(currentuser.getUser());

            processActionTypeUpdate(actionType, contact, searchbean, schedule, STATUS_CONTACTS_PROCESS); 
            
         } else if (ACTION_SEND_EMAIL_TEMPLATE.equals(actionType.getActionType())) {

            searchbean.setConnection(getConnection());
            searchbean.getResults();

            AutoPilotQueue contactQueue = new AutoPilotQueue(context, searchbean, currentuser.getUser(), actionType.getActionType() + ": " + schedule.getField("Name"));

            GenRow requestRow = new GenRow();
            requestRow.setParameter("TemplateID", actionType.getField("TemplateID"));
            if (actionType.getField("Subject").length() != 0) {
               requestRow.setParameter("Subject", actionType.getField("Subject"));
            }

            requestRow.putInternalToken(SessionKeys.CURRENT_USER, currentuser);

            Emailer emailer = EmailerFactory.getContactEmailer(requestRow, context);
            emailer.getMailerBean().connect();

            int reconnectAttempt = 0;
            int currentsent = 0;
            int currentfailed = 0;

            while (!isKilled() && contactQueue.hasNext()) {

               setStatus(STATUS_CONTACTS_PROCESS, contactQueue.getProcessedCount(), contactQueue.getProcessSize());

               // TODO - send the Email
               if (reconnectAttempt > 10) {
                  contactQueue.appendInfo("Aborting offline send at " + contactQueue.getProcessedCount() + ", due to too many consecutive failed email server connections.");
                  break;
               }

               if (!emailer.getMailerBean().isConnected()) {
                  reconnectAttempt++;

                  contactQueue.appendInfo("Reconnecting to email server (Attempt:" + reconnectAttempt + ", current run Sent:" + currentsent + " Failed:" + currentfailed + ").");
                  emailer.getMailerBean().connect();
                  currentsent =
                          0;
                  currentfailed =
                          0;
               } else {
                  reconnectAttempt = 0;
               }

               boolean success = true;
               String contactID = contactQueue.getNext();
               emailer.generate(contactID);

               if (emailer.getSpecificContextString("CreatedDate") == null) {
                  contactQueue.appendError("Record not found", contactID);
                  currentfailed++;

               } else {
                  try {
                     String emailstatus = emailer.send();
                     if (emailstatus.equals(Emailer.SENT)) {
                        emailer.save(emailstatus);
                        contactQueue.incrementActioned();
                        currentsent++;

                     } else {
                        contactQueue.appendError(emailstatus, contactID);
                        currentfailed++;
                     }

                  } catch (Exception e) {
                     contactQueue.appendError(e.toString(), contactID);
                     currentfailed++;
                  }
               }

               // If we are paused, sleep for 10 seconds
               while (!isKilled() && isPaused()) {
                  wait(10);
               }

               Thread.yield();
            }

            contactQueue.queueComplete(isKilled());
         } else if (ACTION_SEND_GENERATED_EMAIL.equals(actionType.getActionType())) {

            String className = actionType.getField("ClassName");
            Class emailClass = Class.forName(className);
            ActionTypeEmail emailGenerator = (ActionTypeEmail) emailClass.newInstance();
            emailGenerator.setContextPath(InitServlet.getSystemParams().getProperty("URLHome"));
            emailGenerator.init(context, schedule, getConnection());

            if (emailGenerator.needsSending()) {

               searchbean.setConnection(getConnection());
               searchbean.getResults();

               AutoPilotQueue contactQueue = new AutoPilotQueue(context, searchbean, currentuser.getUser(), actionType.getActionType() + ": " + schedule.getField("Name"));

               String optOutStatusID = InitServlet.getSystemParams().getProperty("ContactOptOutStatusID");
               String leftCompanyStatusID = InitServlet.getSystemParams().getProperty("ContactLeftCompanyStatusID");

               Contact contact = new Contact(getConnection());
               contact.setCurrentUser(currentuser.getUser());

               int collector = 0;

               //for (int i=0; !isKilled() && i < contactIDs.size(); i++) {  
               while (!isKilled() && contactQueue.hasNext()) {

                  emailGenerator.clear();

                  setStatus(STATUS_CONTACTS_PROCESS, contactQueue.getProcessedCount(), contactQueue.getProcessSize());
                  collector++;

                  contact.setConnection(getConnection());
                  contact.load(contactQueue.getNext());

                  String statusID = contact.getField("CurrentStatusID");

                  if (contact.isLoaded() && contact.getField("Email").length() != 0 && !statusID.equals(optOutStatusID) && !statusID.equals(leftCompanyStatusID)) {

                     String noteID = KeyMaker.generate();

                     emailGenerator.setEntity(contact);

                     if (emailGenerator.generateEmail(noteID)) {

                        String emailFrom = emailGenerator.getEmailFrom();
                        String emailReplyTo = emailGenerator.getEmailReplyTo();
                        String emailBody = emailGenerator.getOutput();

                        String emailCC = contact.getField("Email2");
                        if (emailCC.length() != 0) {
                           try {
                              InternetAddress.parse(emailCC, false);
                           } catch (Exception e) {
                              emailCC = null;
                           }
                        }
                        String emailresponse = sendEmail(schedule, emailBody, contact.getField("Email"), emailFrom, emailReplyTo, emailCC, actionType.getField("Subject"), !emailGenerator.isHTML());

                        if (emailresponse.equals("Message Sent")) {
                           if (emailGenerator.isHTML()) {
                              saveEmailNote(noteID, contact, actionType.getField("Subject"), emailBody, null, emailFrom, emailCC, emailReplyTo);
                           } else {
                              saveEmailNote(noteID, contact, actionType.getField("Subject"), null, emailBody, emailFrom, emailCC, emailReplyTo);
                           }

                           contactQueue.incrementActioned();
                        } else {
                           contactQueue.appendError(emailresponse, contact.getPrimaryKey());
                        }

                        emailBody = null;
                     }

                  }
                  // If we are paused, sleep for 10 seconds
                  while (!isKilled() && isPaused()) {
                     wait(10);
                  }

                  Thread.yield();

               }

               contactQueue.queueComplete(isKilled());

            } else {
               System.out.println("Autopilot did not need sending");
            }

         } else {
            System.out.println("ActionType not found");
         }

      } catch (Exception e) {
         addError(e);
         //killProcess();
         
         schedule.setField("Active","ERROR"); 
         Map errors = schedule.update();
         if(!errors.isEmpty()) { 
          addError(errors.toString()); 
         }
      }
   }

   public void processNotes(AutoPilotSchedule schedule) {

      AutoPilotActionType actionType = schedule.getActionType();
      GenRow searchbean = new GenRow();
      searchbean.setViewSpec("NoteListView");
      searchbean.parseRequest(schedule.getBeanString());
      currentuser.setConstraint(searchbean, "NoteGroup");
      /*
      DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
      java.util.Date todayDate = new java.util.Date();
      Calendar triggerDate = Calendar.getInstance();
      triggerDate.setTime(todayDate);
      triggerDate.add(Calendar.DAY_OF_MONTH, DAYS_IN_ADVANCE);
      
      searchbean.setViewSpec("NoteListView");
      
      searchbean.setParameter("NoteDate", sdf.format(triggerDate.getTime()));
       */
      searchbean.setAction(ActionBean.SEARCH);
      searchbean.doAction();

      if (actionType != null) {
         processAppointmentsActionType(actionType, searchbean, schedule);
      } else {

         session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
         session.setAttribute(SessionKeys.NOTE_SEARCH, searchbean);

         String pdfPath = generatePDF(schedule, "/crm/reports/notelistpdf.jsp?-action=search");
         Integer resultCount = (Integer) session.getAttribute("-resultCount");
         if (resultCount == null || resultCount.intValue() > 0 || schedule.sendIfEmptySet()) {
            pdfPath = "/crm/autopilot/emails/notelist.jsp?pdfPath=" + StringUtil.urlEncode(pdfPath);
            sendNotifyEmail(schedule, generateEmail(schedule, pdfPath));
         }

         session.setAttribute(SessionKeys.CURRENT_USER, null);
         session.setAttribute(SessionKeys.NOTE_SEARCH, null);
      }
   }

   public void processResponses(AutoPilotSchedule schedule) {

      TableBean searchbean = new TableBean();
      searchbean.setViewSpec("ResponseListView");
      searchbean.parseRequest(schedule.getBeanString());
      currentuser.setConstraint(searchbean, "ResponseContact.ContactGroup");
      searchbean.setAction(ActionBean.SEARCH);
      searchbean.doAction();

      session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
      session.setAttribute(SessionKeys.RESPONSE_SEARCH, searchbean);

      //String pdfPath = generatePDF(schedule, "/crm/reports/responselistpdf.jsp?-action=search");

      //pdfPath = "/crm/autopilot/emails/responselist.jsp?pdfPath="+StringUtil.urlEncode(pdfPath);
      String pdfPath = "/crm/autopilot/emails/responselist.jsp";
      String email = generateEmail(schedule, pdfPath);
      Integer resultCount = (Integer) session.getAttribute("-resultCount");
      if (resultCount == null || resultCount.intValue() > 0 || schedule.sendIfEmptySet()) {
         sendNotifyEmail(schedule, email);
      }

      session.setAttribute(SessionKeys.CURRENT_USER, null);
      session.setAttribute(SessionKeys.RESPONSE_SEARCH, null);
   }

   public void processOpportunities(AutoPilotSchedule schedule) {
       
       GenRow searchbean = new GenRow();
       searchbean.setViewSpec("OpportunityListView");
       searchbean.parseRequest(schedule.getBeanString());
       currentuser.setConstraint(searchbean, "OpportunityGroup");
       searchbean.doAction(ActionBean.SEARCH);
      
      AutoPilotActionType actionType = schedule.getActionType();     

      if (actionType != null) {
         processOpportunitiesActionType(actionType, searchbean, schedule);
      } else {

    	  final String layoutID = StringUtils.trimToNull(schedule.getBeanLayoutID());
    	  final String layoutParam = (layoutID != null ? ("&-LayoutID=" + layoutID) : ""); 
    	  
         session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
         session.setAttribute(SessionKeys.OPPORTUNITY_SEARCH, searchbean);

         String pdfPath = generatePDF(schedule, "/crm/reports/opportunitylistpdf.jsp?-action=search" + layoutParam);

         Integer resultCount = (Integer) session.getAttribute("-resultCount");
         if (resultCount == null || resultCount.intValue() > 0 || schedule.sendIfEmptySet()) {
            pdfPath = new StringBuilder().append("/crm/autopilot/emails/opportunitylist.jsp?-action=search&pdfPath=").append(StringUtil.urlEncode(pdfPath)).append(layoutParam).toString();
            sendNotifyEmail(schedule, generateEmail(schedule, pdfPath));
         }

         session.setAttribute(SessionKeys.CURRENT_USER, null);
         session.setAttribute(SessionKeys.OPPORTUNITY_SEARCH, null);
      }
   }

   public void processOrders(AutoPilotSchedule schedule) {

      TableBean searchbean = new TableBean();
      searchbean.setViewSpec("OrderListView");
      searchbean.parseRequest(schedule.getBeanString());
      currentuser.setConstraint(searchbean, "OrderGroup");
      searchbean.setAction(ActionBean.SEARCH);
      searchbean.doAction();

      
      
      
      //TEMG
      
      GenRow aSearchbean = new GenRow();
      aSearchbean.setViewSpec("OrderListView");
      aSearchbean.setConnection(con);
      aSearchbean.parseRequest(schedule.getBeanString());
      currentuser.setConstraint(aSearchbean, "OrderGroup");
      //aSearchbean.setAction(ActionBean.SEARCH);
      aSearchbean.doAction(ActionBean.SEARCH);
      aSearchbean.doAction();
      AutoPilotActionType actionType = schedule.getActionType();
      
      if (actionType != null) {
      processOrderActionTypes(actionType, aSearchbean, schedule);
        
       } else {
      
      
      session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
      session.setAttribute(SessionKeys.ORDER_SEARCH, (TableData)aSearchbean);

      String pdfPath = generatePDF(schedule, "/crm/reports/orderlistpdf.jsp?-action=search");

      Integer resultCount = (Integer) session.getAttribute("-resultCount");
      if (resultCount.intValue() > 0 || schedule.sendIfEmptySet()) {
         pdfPath = "/crm/autopilot/emails/orderlist.jsp?pdfPath=" + StringUtil.urlEncode(pdfPath);
         sendNotifyEmail(schedule, generateEmail(schedule, pdfPath));
      }

      session.setAttribute(SessionKeys.CURRENT_USER, null);
      session.setAttribute(SessionKeys.ORDER_SEARCH, null);
       }
   }

   private void processOrderActionTypes(AutoPilotActionType actionType, GenRow searchbean, AutoPilotSchedule schedule) 
   {   	
   	String className = null;
   	
   	try
   	{
	   	if(ACTION_UPDATE_RECORD.equals(actionType.getActionType()))
	   	{
	   		Order order = new Order(getConnection());
	   		order.setCurrentUser(currentuser.getUser());
	
	   		processActionTypeUpdate(actionType, order, searchbean, schedule, STATUS_ORDERS_PROCESS);
	      }
			else if(ACTION_SEND_GENERATED_EMAIL.equals(actionType.getActionType())) 
			{
				className = actionType.getField("ClassName");
				Class emailClass = Class.forName(className);
				ActionTypeEmail emailGenerator = (ActionTypeEmail) emailClass.newInstance();
				emailGenerator.setContextPath(InitServlet.getSystemParams().getProperty("URLHome"));
				emailGenerator.init(context, schedule, getConnection());
				
				session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
				
				searchbean.setConnection(getConnection());
				searchbean.getResults();
				 	   	  
				AutoPilotQueue ordersQueue = new AutoPilotQueue(context, 
				searchbean, currentuser.getUser(), actionType.getActionType() + ": " + schedule.getField("Name"));
				
				Order order = new Order(getConnection());
				Contact contact = new Contact(getConnection());	   	  
				
				String optOutStatusID = InitServlet.getSystemParams().getProperty("ContactOptOutStatusID");
				String leftCompanyStatusID = InitServlet.getSystemParams().getProperty("ContactLeftCompanyStatusID");
				 
				while (!isKilled() && ordersQueue.hasNext()) 
				{
					/* A new Order for every loop */
					order.load(ordersQueue.getNext());
					order.setCurrentUser(currentuser.getUser());
					 		   		 
					/* Retrieves the Contact from the order */
					String contactID = order.getField("ContactID");		   
					contact.load(contactID);
					
					String email = contact.getField("Email");           
					String statusID = contact.getField("CurrentStatusID");
					            
					  
					if (contact.isLoaded() && email.length() != 0 && !statusID.equals(optOutStatusID) && !statusID.equals(leftCompanyStatusID)) 
					{			   
					   /* Parameters passed to the emailer 
						 * ContactID and TemplateID will be used by the emailer to send the correct email
						 * to the proper contact
						 * Additional values inserted into requestRow can be used as tokens in the emailtemplate
						 */
						GenRow requestRow = new GenRow();
						requestRow.setParameter("TemplateID", actionType.getField("TemplateID"));
						requestRow.setParameter("OrderID", order.getPrimaryKey());
						emailGenerator.setSpecificEmailTokens(order.getField("OrderID"),requestRow);    		  
						
						
						if (actionType.getField("Subject").length() != 0) 
						{
						   requestRow.setParameter("Subject", actionType.getField("Subject"));
						}			  
										    
						requestRow.setParameter("ContactID", contactID);                
						      
						requestRow.putInternalToken(SessionKeys.CURRENT_USER, currentuser);                                              
						                     
						Emailer emailer = EmailerFactory.getContactEmailer(requestRow, context);
						      
						try 
						{
							emailer.getMailerBean().connect();
						} 
						catch (MessagingException e) 
						{
							d.debug("<br>AutoPilot:processOrdersActionType:emailer.getMailerBean().connect() exception!<br>");
							e.printStackTrace();
						}                 
						
						emailer.generate();
						String emailstatus = emailer.send();
						
						if (emailstatus.equals(Emailer.SENT)) 
						{
						    d.debug("<br>AutoPilot:processOrdersActionType:Email sent\n");
						} 
						else 
						{
							d.debug("<br>AutoPilot:processOrdersActionType:Email not sent\n");
						}  				 		    
					}
				}
			}
	   } 
   	catch(ClassNotFoundException cnfe) 
   	{
		   d.debug("<br>AutoPilot:processOrdersActionType:emailer:Class not found Exception: " + className);
	   } 
   	catch(IllegalAccessException iae) 
   	{
		   d.debug("<br>AutoPilot:processOrdersActionType:emailer:Illegal Access Exception");
	   } 
   	catch(InstantiationException ie)
   	{
		   d.debug("<br>AutoPilot:processOrdersActionType:emailer:Instantiation Exception");
	   }
   }

   public void processDocuments(AutoPilotSchedule schedule) {

      TableBean searchbean = new TableBean();
      searchbean.setViewSpec("DocumentView");
      searchbean.parseRequest(schedule.getBeanString());
      currentuser.setConstraint(searchbean, "DocumentGroup");
      searchbean.setAction(ActionBean.SEARCH);
      searchbean.doAction();

      session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
      session.setAttribute(SessionKeys.DOCUMENT_SEARCH, searchbean);

      //String pdfPath = generatePDF(schedule, "/crm/reports/documentlistpdf.jsp?-action=search");

      //pdfPath = "/crm/autopilot/emails/documentlist.jsp?pdfPath="+StringUtil.urlEncode(pdfPath);
      String pdfPath = "/crm/autopilot/emails/documentlist.jsp";
      String email = generateEmail(schedule, pdfPath);
      Integer resultCount = (Integer) session.getAttribute("-resultCount");
      if (resultCount == null || resultCount.intValue() > 0 || schedule.sendIfEmptySet()) {
         sendNotifyEmail(schedule, email);
      }

      session.setAttribute(SessionKeys.CURRENT_USER, null);
      session.setAttribute(SessionKeys.DOCUMENT_SEARCH, null);
   }

   public void processProducts(AutoPilotSchedule schedule) {
      AutoPilotActionType actionType = schedule.getActionType();

      GenRow searchbean = new GenRow();
      searchbean.setViewSpec("ProductListView");
      searchbean.parseRequest(schedule.getBeanString());
      currentuser.setConstraint(searchbean, "ProductGroup");
      searchbean.setAction(ActionBean.SEARCH);
      searchbean.doAction();

      if (actionType != null) {
         processProductsActionType(actionType, searchbean, schedule);
      } else {

         session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
         session.setAttribute(SessionKeys.PRODUCT_SEARCH, searchbean);

         //String pdfPath = generatePDF(schedule, "/crm/reports/productlistpdf.jsp?-action=search");

         //pdfPath = "/crm/autopilot/emails/productlist.jsp?pdfPath="+StringUtil.urlEncode(pdfPath);
         String pdfPath = "/crm/autopilot/emails/productlist.jsp";
         String email = generateEmail(schedule, pdfPath);

         Integer resultCount = (Integer) session.getAttribute("-resultCount");
         if (resultCount == null || resultCount.intValue() > 0 || schedule.sendIfEmptySet()) {
            sendNotifyEmail(schedule, email);
         }

         session.setAttribute(SessionKeys.CURRENT_USER, null);
         session.setAttribute(SessionKeys.PRODUCT_SEARCH, null);
      }

   }

   private int getResultSetSize(GenRow search, AutoPilotSchedule schedule, String constraint) {
//      search.clear();

      search.setViewSpec("OpportunityListView");
      search.parseRequest(schedule.getBeanString());
      search.setParameter("OpportunityOpportunityStatus.StatusID", constraint);
      currentuser.setConstraint(search, "NoteGroup");
      search.setAction(ActionBean.SEARCH);
      search.doAction();

      PagingRowSetBean sarchList = new PagingRowSetBean();
      sarchList.setSearchBean(search);
      sarchList.getResults();
      return sarchList.getSize();
   }

   private void processOpportunitiesActionType(AutoPilotActionType actionType, GenRow searchbean, AutoPilotSchedule schedule) {
     
      try {
        Opportunity op = new Opportunity(getConnection());
          op.setCurrentUser(currentuser.getUser());
          
          if (ACTION_UPDATE_RECORD.equals(actionType.getActionType())) {

             processActionTypeUpdate(actionType, op, searchbean, schedule, STATUS_OPPORTUNITIES_PROCESS); 
             
          } else if (ACTION_DELETE_RECORD.equals(actionType.getActionType())) { 
           
           processActionTypeDelete(actionType, op, searchbean, schedule, STATUS_OPPORTUNITIES_PROCESS); 
           
          }
      } catch (Exception e) {
          addError(e);
          //killProcess();
       }
   }

   private void processProductsActionType(AutoPilotActionType actionType, GenRow searchbean, AutoPilotSchedule schedule) {

      try {
         if (ACTION_UPDATE_RECORD.equals(actionType.getActionType())) {

            searchbean.setConnection(getConnection());
            searchbean.getResults();

            Product product = new Product(getConnection());
            product.setCurrentUser(currentuser.getUser());

            AutoPilotQueue productQueue = new AutoPilotQueue(context, searchbean, currentuser.getUser(), actionType.getActionType() + ": " + schedule.getField("Name"));

            productQueue.appendInfo("Update String: " + actionType.getUpdateString());

            while (!isKilled() && productQueue.hasNext()) {

               setStatus(STATUS_PRODUCTS_PROCESS, productQueue.getProcessedCount(), productQueue.getProcessSize());

               product.load(productQueue.getNext());

               product.populate(actionType.getUpdateString());

               ErrorMap errors = product.update();

               if (errors.isEmpty()) {
                  productQueue.incrementActioned();
                  StringBuffer info = new StringBuffer();
                  info.append(product.getField("Name"));
                  info.append(" (");
                  info.append(product.getField("ProductNum"));
                  info.append(") Updated - ");
                  info.append(context.getInitParameter("URLHome"));
                  info.append("/runway/cms.product.view?ContactID=");
                  info.append(product.getPrimaryKey());
                  productQueue.appendInfo(info.toString());
               } else {
                  productQueue.appendError(errors.toString(), product.getPrimaryKey());
               }

// If we are paused, sleep for 10 seconds
               while (!isKilled() && isPaused()) {
                  wait(10);
               }

               Thread.yield();
            }

            productQueue.queueComplete(isKilled());
         }

      } catch (Exception e) {
         addError(e);
         //killProcess();
      }
   }

   public void processReport(AutoPilotSchedule schedule) {

      session.setAttribute(SessionKeys.CURRENT_USER, currentuser);
      generateReport(schedule);

      if (session.getAttribute("autoPilotSchedule") != null) {
     	 System.out.println("Auto Pilot processReport " + ((AutoPilotSchedule) session.getAttribute("autoPilotSchedule")).toString());
      } else {
     	 System.out.println("Auto Pilot processReport Failed");
      }
      if (!report.isEmpty() || schedule.sendIfEmptySet() || StringUtils.isNotBlank(schedule.getDashBoardID())) {
    	  //report images no longer in use.
         //String docURL = createReportImage(schedule, report);
         //session.setAttribute("-docURL", docURL);
         sendReportNotifyEmail(schedule, "");
      }

      if (session.getAttribute("autoPilotSchedule") != null) {
      	 System.out.println("Auto Pilot processReport2 " + ((AutoPilotSchedule) session.getAttribute("autoPilotSchedule")).toString());
       } else {
      	 System.out.println("Auto Pilot processReport2 Failed");
       }
      session.setAttribute(SessionKeys.CURRENT_USER, null);
   }

   private void generateReport(AutoPilotSchedule schedule) {
      if (report == null) {
         report = new CustomReportBean();
         session.setAttribute("report", report);
      }

      report.clear();
      report.put("contextPath", session.getURLHome().toString());
      report.setCurrentUser(currentuser);
      report.setConnection(getConnection());
      report.parseRequest(schedule.getBeanString());
      
      if(StringUtils.isNotBlank(schedule.getField("OverrideData"))) {
    	  /* the report version parseRequest will run setup() afterwards, but we want that to occur after we have done the subsequent overriding of params */
    	  ActionBean.parseRequest(schedule.getBeanString(), report, false);
    	  System.out.println("Bean STring: " + schedule.getBeanString());
    	  System.out.println("Testing Report, after parse: " + report.toString());
    	  /* create a new bean with the override data. This could be parsed into the original report, but we need to check -begin & -end */
    	  TableData override = new TableBean();
    	  override.parseRequest(schedule.getField("OverrideData"));
    	  /* some reports will only contain a begin, for example if they're using a keyword. 
    	   * The -begin will be overridden but the -end might remain - so lets remove it in that case.
    	   */
    	  if(override.containsKey("-begin")) { 
    		  report.remove("-end");
    	  }
    	  /* this second parseRequest will run setup() */
    	  report.parseRequest(override.toString());
      } 
      try{
         report.doAction();
         takeReportSnapshot(schedule.getBeanID());
      }catch(Exception e){
         addError(e);
      }
      
      if(report.getError().length()!=0)
      {
         addError(report.getError());
      }
   }
   
   protected void takeReportSnapshot(String beanID)
   {
      HistoricReport historicReport = new HistoricReport();
      historicReport.setConnection(getConnection());
      historicReport.setTableSpec("HistoricReports");
      historicReport.setAction(GenerationKeys.INSERT);
      historicReport.createNewID();
      historicReport.setParameter("BeanID", beanID);
      historicReport.setParameter("CreatedDate", "NOW");
      historicReport.setParameter("CreatorID", currentuser.getUserID());
      historicReport.setParameter("CurrentReport", "No");
      if(report.hasRowSummary()){
         historicReport.setParameter("Records", report.getSummary());
      }else{
         historicReport.setParameter("Records", "NULL");
      }
      historicReport.setResults(report); // Sets Results field
      if(historicReport.getString("Results").length() > 0) {
        historicReport.doAction();
      }      
   }

   public void processUsers(AutoPilotSchedule schedule) {

      GenRow searchbean = new GenRow();
      searchbean.setViewSpec("UserView");
      searchbean.parseRequest(schedule.getBeanString());
      currentuser.setConstraint(searchbean, "UserGroups.UserGroup");
      searchbean.setAction(ActionBean.SEARCH);
      searchbean.doAction();

      d.debug("Processing Users <br/>");

      session.setAttribute(SessionKeys.CURRENT_USER, currentuser);

      AutoPilotActionType actionType = schedule.getActionType();
      /*
       * Other type group autopilot not supported
       */

      if (actionType != null) {
         processUsersActionType(actionType, searchbean, schedule);
      }

      session.setAttribute(SessionKeys.CURRENT_USER, null);
   }

   private void processUsersActionType(AutoPilotActionType actionType, GenRow searchbean, AutoPilotSchedule schedule) {

      d.debug("Processing Users Action Type <br/>");
      try {
         if (ACTION_SEND_GENERATED_EMAIL.equals(actionType.getActionType())) {


            d.debug("Processing Users Action Type Email <br/>");

            String className = actionType.getField("ClassName");

            ActionTypeEmail emailGenerator = (ActionTypeEmail) Class.forName(className).newInstance();
            emailGenerator.setContextPath(InitServlet.getSystemParams().getProperty("URLHome"));
            emailGenerator.init(context, schedule, getConnection());

            session.setAttribute(SessionKeys.CURRENT_USER, currentuser);

            searchbean.setConnection(getConnection());
            searchbean.getResults();

            User user = new User(getConnection());
            user.setCurrentUser(currentuser.getUser());

            AutoPilotQueue userQueue = new AutoPilotQueue(context, searchbean, currentuser.getUser(), actionType.getActionType() + ": " + schedule.getField("Name"));

            while (!isKilled() && userQueue.hasNext()) {

               setStatus(STATUS_PRODUCTS_PROCESS, userQueue.getProcessedCount(), userQueue.getProcessSize());

               user.clear();
               user.load(userQueue.getNext());
               emailGenerator.clear();
               emailGenerator.setEntity(user);

               String noteID = KeyMaker.generate();

               if (emailGenerator.needsSending() && emailGenerator.generateEmail(noteID)) {

                  try {
                  String emailFrom = emailGenerator.getEmailFrom();
                  String emailReplyTo = emailGenerator.getEmailReplyTo();
                  String emailCC = emailGenerator.getEmailCC(); 
                  String emailBody = emailGenerator.getOutput();
               
                  String emailresponse = sendEmail(schedule, emailBody, user.getField("Email"), emailFrom, emailReplyTo, emailCC, actionType.getField("Subject"), false);

                  if (emailresponse.equals("Message Sent")) {
                     userQueue.incrementActioned();
                     com.sok.Debugger.getDebugger().debug("Email sent");
                  } else {
                    com.sok.Debugger.getDebugger().debug("not sent"+emailresponse);
                     userQueue.appendError(emailresponse, user.getPrimaryKey());
                  }

                  } catch(Exception e) {
                     com.sok.Debugger.getDebugger().debug("Error: "+e);
                  }
                  
               } else {
                  com.sok.Debugger.getDebugger().debug("Dont need to send");
               }
               // If we are paused, sleep for 10 seconds
               while (!isKilled() && isPaused()) {
                  wait(10);
               }

               Thread.yield();
            }
         } else if (ACTION_SEND_GENERATED_SMS.equals(actionType.getActionType())) {


            String className = actionType.getField("ClassName");
            Class smsClass = Class.forName(className);
            ActionTypeSMS smsGenerator = (ActionTypeSMS) smsClass.newInstance();
            smsGenerator.setContextPath(InitServlet.getSystemParams().getProperty("URLHome"));
            smsGenerator.init(context, schedule, getConnection());

            session.setAttribute(SessionKeys.CURRENT_USER, currentuser);

            searchbean.setConnection(getConnection());
            searchbean.getResults();

            User user = new User(getConnection());
            user.setCurrentUser(currentuser.getUser());

            AutoPilotQueue userQueue = new AutoPilotQueue(context, searchbean, currentuser.getUser(), actionType.getActionType() + ": " + schedule.getField("Name"));


            while (!isKilled() && userQueue.hasNext()) {

               setStatus(STATUS_PRODUCTS_PROCESS, userQueue.getProcessedCount(), userQueue.getProcessSize());

               user.clear();
               user.load(userQueue.getNext());
               smsGenerator.clear();
               smsGenerator.setEntity(user);

               String noteID = KeyMaker.generate();
               smsGenerator.generateSMS(noteID);
               String smsBody = smsGenerator.getOutput();

               //String contactID = searchAttendees.getData("ContactID");

               GenRow requestRow = new GenRow();
//               requestRow.setParameter("SMSTemplateID", actionType.getField("SMSTemplateID"));
               if (actionType.getField("Subject").length() != 0) {
                  requestRow.setParameter("Subject", actionType.getField("Subject"));
               }
               requestRow.setParameter("Body", smsBody);


               //requestRow.setParameter("UserID", "021L250C6U9V385N119Z2H6Y4X97");
               //com.sok.Debugger.getDebugger().debug("mit USerID 021L250C6U9V385N119Z2H6Y4X97");
               com.sok.Debugger.getDebugger().debug("Body: "+smsBody);
               com.sok.Debugger.getDebugger().debug("UserID"+user.getPrimaryKey());
               requestRow.setParameter("UserID", user.getPrimaryKey());
//                     requestRow.setParameter("AppointmentID", note.getPrimaryKey());

               requestRow.putInternalToken(SessionKeys.CURRENT_USER, currentuser);

               SMS sms = new SMS(requestRow, context);
               sms.toUser();
               sms.getSMSBean().connect();

               try {
                  String smsStatus = sms.generateSendAndSave();//send(); 
                   com.sok.Debugger.getDebugger().debug("Message: "+smsStatus+"<br>\n");   
                  if (smsStatus.equals(SMS.SENT)) {
                     com.sok.Debugger.getDebugger().debug("Message sent <br>\n");
                  } else {
                      userQueue.appendError(smsStatus,user.getPrimaryKey());
                     com.sok.Debugger.getDebugger().debug("Message not sent <br>\n");
                  }
               } catch (Exception e) {
                  com.sok.Debugger.getDebugger().debug("Exception " + e + "<br>\n");
//                        appointmentQueue.appendError(e.toString(), contactID);
               }

               /*
               Properties sysprops = InitServlet.getSystemParams();
               String smshost = sysprops.getProperty("SMSHost");
               String smsid = sysprops.getProperty("SMSID");
               String smspassword = sysprops.getProperty("SMSPassword");
               String smsprovider = sysprops.getProperty("SMSProvider");
               
               SMSBean sms = new SMSBean();
               if (smshost != null) {
               sms.setIP(smshost);
               }
               if (smsprovider != null) {
               sms.setProvider(smsprovider);
               sms.setAccount(smsid);
               sms.setPassword(smspassword);
               }
               com.sok.Debugger.getDebugger().debug("The body is: " + smsBody);
               com.sok.Debugger.getDebugger().debug("User" + user.getFirstName() + " " + user.getLastName());
               
               //sms.setNumber(user.getEmail());               
               sms.setNumber("0406950441");
               sms.setSubject(actionType.getField("Subject"));
               sms.setMessage(smsBody);
               String smsresponse = sms.getResponse();
               
               if (smsresponse.equals("Message Sent")) {
               userQueue.incrementActioned();
               } else {
               userQueue.appendError(smsresponse, user.getPrimaryKey());
               }
                */
               // If we are paused, sleep for 10 seconds
               while (!isKilled() && isPaused()) {
                  waitUnlessKilled(10);
               }

               Thread.yield();
            }

         }
      } catch (Exception e) {
         addError(e);
      }
   }

   private void removeOldFiles() {
      File directory = new File(context.getRealPath(AUTOPILOT_FILE_PATH));

      File[] tempFiles = directory.listFiles();

      if (tempFiles != null) {
         long expireTime = new java.util.Date().getTime();

         expireTime =
                 expireTime - THIRTY_DAYS_IN_MILLIS;

         for (int i = 0; i <
                 tempFiles.length; i++) {
            if (tempFiles[i].lastModified() < expireTime) {
               tempFiles[i].delete();
            }

         }
      }
   }

   private void waitUnlessKilled(int seconds)
   {
      if(seconds>=5)
      {
         int count = seconds / 5;
   
         for(int i=0; i<count && !isKilled(); i++)
         {
            wait(5);
         }
      }
      else if(seconds>0 && !isKilled())
      {
         wait(seconds);
      }
   }   
   
   private void wait(int seconds) {
      try {
         Thread.sleep(seconds * 1000);
      } catch (Exception e) {
      }
   }

   private void loadCurrentUser(String userID) {
	  currentuser = null;
      currentuser = new UserBean();
      currentuser.clear();
      currentuser.setViewSpec("UserView");
      currentuser.setConnection(getConnection());
      currentuser.setColumn("UserID", userID);
      currentuser.setAction("select");
      currentuser.doAction();
      currentuser.loadGroups(getConnection());
   }

   public String generateEmail(
           AutoPilotSchedule schedule, String urlPath) {

      ResponseOutputStream responseOS = null;
      ByteArrayOutputStream baos = new ByteArrayOutputStream();

      try {
         responseOS = new ResponseOutputStream(session, baos, urlPath);
         responseOS.service(getConnection());
      } catch (Exception e) {
         addError(e);
         //killProcess();
      } finally {
         if (baos != null) {
            try {
               baos.flush();
               baos.close();
            } catch (Exception ex) {
               addError(ex);
               //killProcess();
            }

         }
         if (responseOS != null) {
            responseOS.close();
         }

      }
      return baos.toString();
   }

   public String generatePDF(AutoPilotSchedule schedule, String urlPath) {
      return generatePDF(schedule, urlPath, null, ".pdf");
   }
   
   public String generatePDF(AutoPilotSchedule schedule, String urlPath, String param) {
      return generatePDF(schedule, urlPath, param, ".pdf");
   }

   public String generatePDF(AutoPilotSchedule schedule, String urlPath, String param, String suffix) {
      StringBuffer fileName = new StringBuffer();
      fileName.append(schedule.getAutoPilotScheduleID());
      fileName.append("_");
      if (param != null && param.length() > 0) {
         fileName.append(param).append("_");
      }
      dateFormat.applyPattern(file_Date_Format);
      fileName.append(dateFormat.format(new java.util.Date()));
      fileName.append(suffix);

      File temp = new File(autopilotFolder, fileName.toString());

      //session.setAttribute("-resultCount", new Integer(10));
      doFileOutput(temp, urlPath);
      return AUTOPILOT_FILE_PATH + fileName;
   }

   public void hitURL(AutoPilotSchedule schedule, String urlPath) {
      ResponseOutputStream responseOS = null;
      try {
         responseOS = new ResponseOutputStream(session, null, urlPath);
         responseOS.service(getConnection());
      } catch (Exception e) {
         addError(e);
         //killProcess();
      } finally {
         if (responseOS != null) {
            responseOS.close();
         }
      }
   }

   /**
    * @deprecated - this code is no longer in use.
    */
   public String createReportImage(
           AutoPilotSchedule schedule, CustomReportBean report) {
      StringBuffer fileName = new StringBuffer();
      fileName.append(schedule.getAutoPilotScheduleID());
      fileName.append("_");
      dateFormat.applyPattern(file_Date_Format);
      fileName.append(dateFormat.format(new java.util.Date()));
      fileName.append(".jpg");

      File temp = new File(autopilotFolder, fileName.toString());

      try {
         report.saveChartToJpegFile(temp);
      } catch (Exception e) {
         addError(e);
         //killProcess();
      }

      return AUTOPILOT_FILE_PATH + fileName;
   }

   private void doFileOutput(File file, String urlPath) {
	   if(logger.isDebugEnabled()) {
		   logger.debug("doFileOutput({},{})", file.getAbsolutePath(), urlPath);
	   }
      ResponseOutputStream responseOS = null;
      FileOutputStream fos = null;
      try {
         fos = new FileOutputStream(file);
         responseOS = new ResponseOutputStream(session, fos, urlPath);
         responseOS.service(getConnection());
         logger.debug("Should have completed file output");
      } catch (Exception e) {
    	  logger.error("Error doing file output from autopilot", e);
         addError(e);
         //killProcess();
      } finally {
         if (fos != null) {
            try {
               logger.debug("flushing FileOutputStream");
               fos.flush();
               logger.debug("closing FileOutputStream");
               fos.close();
            } catch (Exception ex) {
               logger.error("Error doing file output from autopilot", ex);
               addError(ex);
               //killProcess();
            }
         } else {
        	 logger.error("FileOutputStream was null");
         }
         if (responseOS != null) {
            responseOS.close();
         }
      }
      if(file.length() == 0) {
    	  throw new RuntimeException("Failed to output file");
      }
   }

   private void sendNotifyEmail(AutoPilotSchedule schedule, String body) {
      String emailresponse = sendEmail(schedule, body, null, null, null, null, null, false);

      if (!emailresponse.equals("Message Sent")) {
         addError(emailresponse);
      }

   }

   private String sendEmail(AutoPilotSchedule schedule, String body, String toEmail, String fromEmail, String replyToEmail, String ccEmail, String subject, boolean plainText) {

      MailerBean mailerBean = new MailerBean();
      mailerBean.setHost(context.getInitParameter("SMTPHost"));

      if (fromEmail != null && fromEmail.length() != 0) {
         mailerBean.setFrom(fromEmail);
      } else {
         mailerBean.setFrom(currentuser.getString("Email"));
      }

      if (ccEmail != null && ccEmail.length() != 0) {
         mailerBean.setCc(ccEmail);
      }

      if (replyToEmail != null && replyToEmail.length() != 0) {
         mailerBean.setReplyTo(replyToEmail);
      }

      if (toEmail == null || toEmail.length() == 0) {
         StringBuffer toAddresses = new StringBuffer();
         toAddresses.append(schedule.getField("EmailTo"));

         User user = new User(getConnection());
         Contact contact = new Contact(getConnection());

         Iterator iter = schedule.getUserIDsForAutoPilot().iterator();
         while (iter.hasNext()) {
            user.load((String) iter.next());

            if (user.getEmail().length() > 0) {
               if (toAddresses.length() > 0) {
                  toAddresses.append(", ");
               }

               toAddresses.append(user.getEmail());
            }

         }
         
         iter = schedule.getContactIDsForAutoPilot().iterator();
         while (iter.hasNext()) {
        	 contact.load((String) iter.next());

            if (contact.getField("Email").length() > 0) {
               if (toAddresses.length() > 0) {
                  toAddresses.append(", ");
               }

               toAddresses.append(contact.getField("Email"));
            }

         }
         
         if (toAddresses.length() == 0) {
            toAddresses.append(schedule.getField("Email"));
         }

         mailerBean.setTo(toAddresses.toString());
      } else {
         mailerBean.setTo(toEmail);
      }

      if (subject != null && subject.length() != 0) {
         mailerBean.setSubject(subject);
      } else {
         dateFormat.applyPattern(readable_Date_Format);
         mailerBean.setSubject(schedule.getName() + " - " + dateFormat.format(new java.util.Date()));
      }

      if (plainText) {
         mailerBean.setTextbody(body);
      } else {
         mailerBean.setHtmlbody(body);
      }

      return mailerBean.getResponse();
   }
   
   
   private void sendReportNotifyEmail(AutoPilotSchedule schedule, String body) {
	      String emailresponse = sendReportUserEmail(schedule, body, null, null, null, null, null, false);
	      
	      String emailresponscontact = sendReportContactEmail(schedule, body, null, null, null, null, null, false);

	      if (!emailresponse.equals("Message Sent")) {
	         addError(emailresponse);
	      }
	      
	      if (!emailresponscontact.equals("Message Sent")) {
		         addError(emailresponscontact);
		      }
	      
	      setStatus(STATUS_REPORT + " -Sent");

	   }

   private String sendReportUserEmail(AutoPilotSchedule schedule, String body, String toEmail, String fromEmail, String replyToEmail, String ccEmail, String subject, boolean plainText) {
	   String emailPath = "/crm/autopilot/emails/report.jsp?-contactemail=false";

	   //only allow this if generating via server side rendering
	   if(ReportPDFServlet.MODE == ReportPDFServlet.GenerationMode.PhantomJS) {
		   setStatus(STATUS_REPORT + " -PDF");
		   StringBuilder pdfGenerator = new StringBuilder();
		   //if (StringUtils.isNotBlank(InitServlet.getSystemParam("PhantomJSURL"))) {
			//   pdfGenerator.append(InitServlet.getSystemParam("PhantomJSURL"));
		   //}
		   if(StringUtils.isNotBlank(schedule.getDashBoardID())) {
			   pdfGenerator.append("/dashboard.pdf?-dashboardID=").append(schedule.getDashBoardID()).append("&");
		   } else {
			   pdfGenerator.append("/report.pdf?-beanID=").append(schedule.getBeanID()).append("&");
		   }
		   //i'm a little unclear as to what the report generated within autopilot is doing, so lets make this implementation consistent.
		   session.setAttribute("report", null);
		   pdfGenerator.append(schedule.getField("OverrideData"));
		   String pdfPath = generatePDF(schedule, pdfGenerator.toString());
		   emailPath = new StringBuilder(emailPath).append("&pdfPath=").append(StringUtil.urlEncode(pdfPath)).toString();
	   }

	   setStatus(STATUS_REPORT + " -Email");
	   MailerBean mailerBean = new MailerBean();
	   mailerBean.setHost(context.getInitParameter("SMTPHost"));

	   if (fromEmail != null && fromEmail.length() != 0) {
		   mailerBean.setFrom(fromEmail);
	   } else {
		   mailerBean.setFrom(currentuser.getString("Email"));
	   }

	   if (ccEmail != null && ccEmail.length() != 0) {
		   mailerBean.setCc(ccEmail);
	   }

	   if (replyToEmail != null && replyToEmail.length() != 0) {
		   mailerBean.setReplyTo(replyToEmail);
	   }

	   if (toEmail == null || toEmail.length() == 0) {
		   StringBuffer toAddresses = new StringBuffer();
		   toAddresses.append(StringUtils.trimToEmpty(schedule.getField("EmailTo")));

		   User user = new User(getConnection());

		   Iterator<String> iter = schedule.getUserIDsForAutoPilot().iterator();
		   while (iter.hasNext()) {
			   user.load(iter.next());

			   if (user.getEmail().length() > 0) {
				   if (toAddresses.length() != 0) {
					   toAddresses.append(", ");
				   }

				   toAddresses.append(user.getEmail());
			   }
			   logger.debug("Inside USER: {}", toAddresses);
		   }
		   if (toAddresses.length() == 0) {
			   toAddresses.append(schedule.getField("Email"));
		   }

		   mailerBean.setTo(toAddresses.toString());
	   } else {
		   mailerBean.setTo(toEmail);
	   }

	   if (subject != null && subject.length() != 0) {
		   mailerBean.setSubject(subject);
	   } else {
		   dateFormat.applyPattern(readable_Date_Format);
		   mailerBean.setSubject(schedule.getName() + " - " + dateFormat.format(new java.util.Date()));
	   }

	   if (plainText) {
		   mailerBean.setTextbody(generateEmail(schedule,emailPath));
	   } else {
		   mailerBean.setHtmlbody(generateEmail(schedule,emailPath));
	   }

	   return mailerBean.getResponse();
   }

   private String sendReportContactEmail(AutoPilotSchedule schedule, String body, String toEmail, String fromEmail, String replyToEmail, String ccEmail, String subject, boolean plainText) {
	   String emailPath =  "/crm/autopilot/emails/report.jsp?-contactemail=true";

	   if(ReportPDFServlet.MODE == ReportPDFServlet.GenerationMode.PhantomJS) {
		   setStatus(STATUS_REPORT + " -PDF");
		   StringBuilder pdfGenerator = new StringBuilder();
		   if(StringUtils.isNotBlank(schedule.getDashBoardID())) {
			   pdfGenerator.append("/dashboard.pdf?-dashboardID=").append(schedule.getDashBoardID()).append("&");
		   } else {
			   pdfGenerator.append("/report.pdf?-beanID=").append(schedule.getBeanID()).append("&");
		   }
		   //i'm a little unclear as to what the report generated within autopilot is doing, so lets make this implementation consistent.
		   session.setAttribute("report", null);
		   pdfGenerator.append(schedule.getField("OverrideData"));
		   String pdfPath = generatePDF(schedule, pdfGenerator.toString());
		   emailPath = new StringBuilder(emailPath).append("&pdfPath=").append(StringUtil.urlEncode(pdfPath)).toString();
	   }


	   setStatus(STATUS_REPORT + " -Email");
	   boolean sendEmail = true;
	   MailerBean mailerBean = new MailerBean();
	   mailerBean.setHost(context.getInitParameter("SMTPHost"));

	   if (fromEmail != null && fromEmail.length() != 0) {
		   mailerBean.setFrom(fromEmail);
	   } else {
		   mailerBean.setFrom(currentuser.getString("Email"));
	   }

	   if (ccEmail != null && ccEmail.length() != 0) {
		   mailerBean.setCc(ccEmail);
	   }

	   if (replyToEmail != null && replyToEmail.length() != 0) {
		   mailerBean.setReplyTo(replyToEmail);
	   }

	   if (toEmail == null || toEmail.length() == 0) {
		   StringBuffer toAddresses = new StringBuffer();
		   toAddresses.append(schedule.getField("EmailTo"));


		   Contact contact = new Contact(getConnection());

		   Iterator<String> iter = schedule.getContactIDsForAutoPilot().iterator();
		   while (iter.hasNext()) {
			   contact.load((String) iter.next());

			   if (contact.getField("Email").length() > 0) {
				   if (toAddresses.length() != 0) {
					   toAddresses.append(", ");
				   }

				   toAddresses.append(contact.getField("Email"));
			   }
			   logger.debug("Inside CONTACT: {}", toAddresses);
		   }

		   if (toAddresses.length() == 0) {
			   //toAddresses.append(schedule.getField("Email"));
			   sendEmail = false;
		   }

		   mailerBean.setTo(toAddresses.toString());
	   } else {
		   mailerBean.setTo(toEmail);
	   }
	   if(sendEmail){

		   if (subject != null && subject.length() != 0) {
			   mailerBean.setSubject(subject);
		   } else {
			   dateFormat.applyPattern(readable_Date_Format);
			   mailerBean.setSubject(schedule.getName() + " - " + dateFormat.format(new java.util.Date()));
		   }

		   if (plainText) {
			   mailerBean.setTextbody(generateEmail(schedule,emailPath));
		   } else {
			   mailerBean.setHtmlbody(generateEmail(schedule,emailPath));
		   }	      
		   return mailerBean.getResponse();

	   }else{
		   return "";
	   }
   }

   private void saveEmailNote(String noteID, Contact contact, String subject, String htmlBody, String textBody, String fromEmail, String ccEmail, String replyToEmail) {
      GenRow note = new GenRow();
      note.setViewSpec("email/NoteEmailView");
      note.setConnection(getConnection());

      note.setParameter("NoteID", noteID);
      note.setParameter("ContactID", contact.getPrimaryKey());
      note.setParameter("CompanyID", contact.getField("CompanyID"));
      note.setParameter("GroupID", contact.getGroupID());
      note.setParameter("RepUserID", currentuser.getUserID());

      note.setParameter("Type", "Email");
      note.setParameter("Status", "Sent");
      note.setParameter("SentDate", "NOW");
      note.setParameter("NoteDate", "NOW");
      note.setParameter("Completed", "Yes");

      note.setParameter("Subject", subject);
      note.setParameter("Body", textBody);
      note.setParameter("HTMLBody", htmlBody);
      note.setParameter("EmailTo", contact.getField("Email"));

      if (fromEmail != null && fromEmail.length() != 0) {
         note.setParameter("EmailFrom", fromEmail);
      } else {
         note.setParameter("EmailFrom", currentuser.getString("Email"));
      }

      if (ccEmail != null && ccEmail.length() != 0) {
         note.setParameter("EmailCC", ccEmail);
      }

      if (replyToEmail != null && replyToEmail.length() != 0) {
         note.setParameter("EmailReplyTo", replyToEmail);
      }

      note.setParameter("CreatedDate", "NOW");
      note.setParameter("CreatedBy", currentuser.getUserID());
      note.setParameter("ModifiedDate", "NOW");
      note.setParameter("ModifiedBy", currentuser.getUserID());
      try { 
      note.doAction(ActionBean.INSERT);
      } catch (com.sok.framework.generation.DatabaseException de) { 
        com.sok.Debugger.getDebugger().debug("Failed to insert note, First try - getting new connection - CID: "+contact.getPrimaryKey()); 
        
        con = null; 
        note.setConnection(getConnection());
        
        try { 
           note.doAction(ActionBean.INSERT);
        } catch (com.sok.framework.generation.DatabaseException de2) { 
           com.sok.Debugger.getDebugger().debug("Failed twice to insert note, skipping.. "); 
        }
      }
   }
   
   private void saveSMSNote(String noteID, Contact contact, String subject, String textBody) {
      GenRow note = new GenRow();
      note.setTableSpec("Notes");
      note.setConnection(getConnection());

      note.setParameter("NoteID", noteID);
      note.setParameter("ContactID", contact.getPrimaryKey());
      note.setParameter("CompanyID", contact.getField("CompanyID"));
      note.setParameter("GroupID", contact.getGroupID());
      note.setParameter("RepUserID", currentuser.getUserID());

      note.setParameter("Type", "SMS");
      note.setParameter("Status", "Sent");
      note.setParameter("SentDate", "NOW");
      note.setParameter("NoteDate", "NOW");
      note.setParameter("Completed", "Yes");

      note.setParameter("Subject", subject);
      note.setParameter("Body", textBody);

      note.setParameter("CreatedDate", "NOW");
      note.setParameter("CreatedBy", currentuser.getUserID());
      note.setParameter("ModifiedDate", "NOW");
      note.setParameter("ModifiedBy", currentuser.getUserID());

      note.doAction(ActionBean.INSERT);
   }
}
