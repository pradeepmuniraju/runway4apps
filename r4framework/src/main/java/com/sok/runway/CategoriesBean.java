package com.sok.runway;
import java.util.*;
import java.sql.*;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.jsp.*; 
import com.sok.framework.*;
public class CategoriesBean
{
	HashMap cattree = null;

	HashMap codetosubcat = null;
	String[] cats = null;

	static final String catstmtstring = "select Category, SubCategory, Code from Categories order by Category, SubCategory";

	StringBuffer errors = new StringBuffer();

	String[] selectedCodes = null;

	public CategoriesBean()
	{

	}

	public String getErrors()
	{
		return(errors.toString());
	}

	public String[] getCategories()
	{
		return(cats);
	}

	public static String[] split(String s, char c)
	{
		String[] list = null;
		int index = 0;
		if(s.length()!=0)
		{
			index++;
			for(int i=0; i<s.length(); i++)
			{
				if(s.charAt(i)==c)
				{
					index++;
				}
			}
			list = new String[index];
			index = 0;
			int lastindex = 0;
			for(int j=0; j<s.length(); j++)
			{
				if(s.charAt(j)==c)
				{
					list[index] = s.substring(lastindex,j);
					lastindex = j+1;
					index++;
				}
				else if((j+1) == s.length())
				{
					list[index] = s.substring(lastindex,j+1);
				}
			}
		}
		return(list);
	}

	public void setCodes(String scodes)
	{
		if(scodes!=null)
		{
			selectedCodes = split(scodes,'+');
		}
		else
		{
			selectedCodes = null;
		}
	}

	public boolean selected(String s, int index)
	{
		if(selectedCodes!=null)
		{
			if(index>=0 && index<selectedCodes.length)
			{
				if(selectedCodes[index].equals(s))
				{
					return(true);	
				}	
			}
			else if(index==-1)
			{
				for(int i=0; i< selectedCodes.length; i++)
				{
					if(selectedCodes[i].equals(s))
					{
						return(true);	
					}	
				}
			}
		}
		return(false);
	}

	public String getCategoriesSearchOptions()
	{
		return(getCategoriesOptions(true,-1));
	}

	public String getCategoriesSearchCheckBoxes()
	{
		return(getCategoriesCheckBoxes(true));
	}

	public String getCategoriesCheckBoxes()
	{
		return(getCategoriesCheckBoxes(false));
	}

	String getCategoriesOptions(boolean search, int index)
	{
		StringBuffer out = new StringBuffer();
		String subcat = null;
		String code = null;
		ArrayList tempcodes = null;
		for(int i=0; i<cats.length; i++)
		{
			tempcodes = (ArrayList)cattree.get(cats[i]);
			if(index==-1)
			{
				out.append("<optgroup label=\"");
				out.append(cats[i]);
				out.append("\">");
			}
			for(int j=0; j<tempcodes.size(); j++)
			{
				code = (String)tempcodes.get(j);
				subcat = (String)codetosubcat.get(code);
				out.append("<option value=\"");
				if(search)
				{
					out.append("%");
				}
				out.append(code);
				if(search)
				{
					out.append("%");
				}
				out.append("\"");
				if(selected(code,index))
				{
					out.append(" selected");
				}
				out.append(">");
				if(index!=-1)
				{
					out.append(cats[i]);
					out.append(" - ");
				}
				out.append(subcat);
				out.append("</option>");
			}
			if(index==-1)
			{
				out.append("</optgroup>");
			}
		}
		//selectedCodes = null;
		return(out.toString());
	}

	String getCategoriesCheckBoxes(boolean search)
	{
		StringBuffer out = new StringBuffer();
		String subcat = null;
		String code = null;
		ArrayList tempcodes = null;
		for(int i=0; i<cats.length; i++)
		{
			tempcodes = (ArrayList)cattree.get(cats[i]);
			out.append("<span class=\"detailfield\">");
			out.append(cats[i]);
			out.append("</span><br>");
			for(int j=0; j<tempcodes.size(); j++)
			{
				code = (String)tempcodes.get(j);
				subcat = (String)codetosubcat.get(code);
				out.append("<input type=\"checkbox\" name=\"Categories\" value=\"");
				if(search)
				{
					out.append("%");
				}
				out.append(code);
				if(search)
				{
					out.append("%");
				}
				out.append("\"");
				if(selected(code,-1))
				{
					out.append(" checked");
				}
				out.append(">");
				out.append(subcat);
				out.append("<br>");
			}

		}
		selectedCodes = null;
		return(out.toString());
	}

	public String getCategoriesOptions()
	{
		return(getCategoriesOptions(false,-1));
	}

	public String getCategoriesOptions(int index)
	{
		return(getCategoriesOptions(false,index));
	}

	public String getSelectedCategories()
	{
		StringBuffer out = new StringBuffer();
		String cat = null;
		String subcat = null;
		String code = null;
		ArrayList tempcodes = null;
		for(int i=0; i<cats.length; i++)
		{
			tempcodes = (ArrayList)cattree.get(cats[i]);

			for(int j=0; j<tempcodes.size(); j++)
			{
				code = (String)tempcodes.get(j);

				if(selected(code,-1))
				{
					subcat = (String)codetosubcat.get(code);
					if(cat==null || !cat.equals(cats[i]))
					{
						cat = cats[i];
						out.append("<span class=\"detailfield\">");
						out.append(cat);
						out.append("</span><br>");
					}
					out.append("&nbsp;&nbsp;&nbsp;&nbsp;");
					out.append(subcat);
					out.append("<br>");
				}
			}
		}
		selectedCodes = null;
		return(out.toString());
	}

	public void printSelectedCategories(JspWriter out)
	{
		String cat = null;
		String subcat = null;
		String code = null;
		ArrayList tempcodes = null;
		for(int i=0; i<cats.length; i++)
		{
			tempcodes = (ArrayList)cattree.get(cats[i]);

			for(int j=0; j<tempcodes.size(); j++)
			{
				code = (String)tempcodes.get(j);

				if(selected(code,-1))
				{
					try{
						subcat = (String)codetosubcat.get(code);
						cat = cats[i];
						out.print(cat);
						out.print(" - ");
						out.print(subcat);
						out.print("<br>");
					}catch(Exception e){}
				}
			}
		}
		selectedCodes = null;
	}

	public void printSelectedCategoriesOneLine(JspWriter out)
	{
		String cat = null;
		String subcat = null;
		String code = null;
		ArrayList tempcodes = null;
		int printedcount = 0;
		for(int i=0; i<cats.length; i++)
		{
			tempcodes = (ArrayList)cattree.get(cats[i]);

			for(int j=0; j<tempcodes.size(); j++)
			{
				code = (String)tempcodes.get(j);

				if(selected(code,-1))
				{
					try{
						subcat = (String)codetosubcat.get(code);
						cat = cats[i];
						if(printedcount!=0)
						{
							out.print(", ");
						}
						out.print("<span class=\"detailfield\">");
						out.print(cat);
						out.print("</span>");
						out.print(" - ");
						out.print(subcat);
						printedcount++;
					}catch(Exception e){}
				}
			}
		}
		selectedCodes = null;
	}

	public void setCategories(Object dbname)
	{
		setCategories((String)dbname);
	}	

	public void setCategories(String dbname)
	{
		Context ctx = null;
		Connection con = null;
		Statement stmt = null;
		ResultSet current = null;
		try
		{
			con = ActionBean.connect(); 

			cattree = new HashMap();
			codetosubcat = new HashMap();

			stmt = con.createStatement();
			current = stmt.executeQuery(catstmtstring);

			String code = null;
			String cat = null;
			String lastcat = null;
			String subcat = null;

			ArrayList codelist = null;
			ArrayList tempcats = new ArrayList();
			while(current.next())
			{
				cat = current.getString(1);
				subcat = current.getString(2);
				code = current.getString(3);

				if(lastcat==null || !lastcat.equals(cat))
				{
					codelist = new ArrayList();
					cattree.put(cat, codelist);
					tempcats.add(cat);
					lastcat = cat;
				}
				codelist.add(code);

				codetosubcat.put(code,subcat);
			}
			cats = new String[tempcats.size()];
			cats = (String[])tempcats.toArray(cats);

		}
		catch(Exception e)
		{
			errors.append("At setCategories(String): ");
			errors.append(e.toString());
			errors.append("\r\n");
		}
		finally
		{
	    	if (current != null) 
	    	{
	       	try{
	           	current.close();
	       	}catch (Exception ex){}
	    	}
	    	if (stmt != null) 
	    	{
	     		try{       
	           	stmt.close();
	     		}catch (Exception ex){}
	  		}
	    	if (con != null) 
	    	{
	     		try{       
	           	con.close();
	     		}catch (Exception ex){}
	  		}
	    	if (ctx != null) 
	    	{
	     		try{       
	           	ctx.close();
	     		}catch (Exception ex){}
	  		}
		}
	}

	public static void main(String[] args)
	{
		String teststring = "Blah";
		System.out.println(teststring);

		String[] testresult = split(teststring,'+');

		for(int i=0; i<testresult.length; i++)
		{
			System.out.println(testresult[i]);
		}

		teststring = "Blah+Blah+Blah";
		System.out.println(teststring);
		testresult = split(teststring,'+');
		for(int i=0; i<testresult.length; i++)
		{
			System.out.println(testresult[i]);
		}
		
	}
}
