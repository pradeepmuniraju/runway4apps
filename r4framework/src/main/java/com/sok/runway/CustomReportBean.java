/**
 * 
 */
package com.sok.runway;

import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.Locale;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.framework.ViewSpec;
import com.sok.framework.generation.GenerationKeys;
import com.sok.service.crm.UserService;

import java.util.Map; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dion
 * 
 */
public class CustomReportBean extends ReportBean /*
 * doesn't need these, extends
 * from ReportBean implements
 * DatasetProducer, CalendarKeys
 */{

	private static final Logger logger = LoggerFactory.getLogger(CustomReportBean.class);

	private String reportType = "report";
	private String contextPath = "/";

	/**
	 * 
	 */
	public CustomReportBean() {
		super();
	}

	private Map<String, Object> resultMap = null;

	//We will add this to ensure that if there are any issues with it, we can disable it. In future this will be the default option.
	private boolean useCache = "true".equals(InitServlet.getSystemParam("EnableCustomReportCache"));
	public void doAction() {
		super.doAction();
		if("Custom".equals(this.getString("-reportType"))) {
			if(resultMap != null) {
				resultMap.clear();
			} else {
				resultMap = new HashMap<String, Object>();
			}
		}
		useCache = "true".equals(InitServlet.getSystemParam("EnableCustomReportCache"));
	}

	@Override 
	public void setRequest(HttpServletRequest request) {
		super.setRequest(request); 
		contextPath = request.getContextPath();
	}

	@Override
	public void parseRequest(HttpServletRequest request) {
		contextPath = request.getContextPath();
		super.parseRequest(request);
	}

	public void reparseRequest(HttpServletRequest request) {
		contextPath = request.getContextPath();

		Enumeration<String> param = request.getParameterNames();
		while (param.hasMoreElements()) {
			String s = (String) param.nextElement();
			String[] values = request.getParameterValues(s);
			if (values != null) {
				if (values.length == 1) {
					if (values[0].length() > 0)
						this.setParameter(s, values[0]);
				} else {
					this.put(s, values); //using setParameter here will call toString on the array
				}
				// for (int v = 0; v < values.length; ++v)
				// System.out.println(s + " = " + values[v]);
			} else {
				// System.out.println(s + " is null");
			}
		}

	}

	@Override
	public String getReportType() {
		if (getReportSpec() != null && getReportSpec().getCustomClass()!=null) {
			final String cc = getReportSpec().getCustomClass();
			if(!getClass().getName().equals(cc)) { 
				if(logger.isDebugEnabled()) { 
					logger.debug("getClass().getName {} ne {}", getClass().getName(), cc);
				}
				//System.out.println(String.format("getClass().getName [%1$s] ne ", getClass().getName(), cc));
				try {
					CustomReportBean report = (CustomReportBean)Class.forName(cc).newInstance();
					report.clear();
					report.setCurrentUser(currentuser);
					report.setConnection(getConnection());
					report.parseRequest(this.toString(true), true); 
					return report.getReportType();
				} catch (Exception e) { 
					logger.error("Error getting report type", e);
					//try { ActionBean.writeStackTrace(e, System.out); } catch (IOException ioe) { ioe.printStackTrace(); }
				}
			}
		}
		return super.getReportType();
	}

	public void setProgressRow(int row, int totalRows, int cells, int totalCells) {


	}

	public List<List<ReportCell>> getChartData() {
		logger.debug("getChartData()"); 
		if(chartData != null) {
			return chartData; 
		}
		if (getReportSpec() != null && getReportSpec().getCustomClass()!=null) { 
			//when in rome
			final String cc = getReportSpec().getCustomClass();
			if(!getClass().getName().equals(cc)) { 
				if(logger.isDebugEnabled()) { 
					logger.debug("getClass().getName {} ne {}", getClass().getName(), cc);
				}
				//System.out.println(String.format("getClass().getName [%1$s] ne ", getClass().getName(), cc));
				try {
					/* NOTE! 
					 * If you're going to use this you'd best make sure the client is using the report-caching, or doing this will make it run super slow. 
					 * In future CustomReportBean could be turned into a wrapper for the class in question, etc.
					 */
					CustomReportBean report = (CustomReportBean)Class.forName(cc).newInstance();

					report.clear();
					report.setCurrentUser(currentuser);
					report.setConnection(getConnection());
					report.parseRequest(this.toString(true), true); 
					report.doAction();

					return (chartData = report.getChartData());
				} catch (Exception e) { 
					logger.error("error generating custom classed report", e);
					//try { ActionBean.writeStackTrace(e, System.out); } catch (IOException ioe) { ioe.printStackTrace(); }
				}
			} 
		} else if (!"Custom".equals(this.getString("-reportType")) && !"Custom".equals(this.getString("-type"))) {
			//worst. pattern. ever. 
			return super.getChartData();
		} else { 
			//System.out.println(String.format("getReportSpec() != null [%1$b], getReportSpec().getCustomClass()!=null [%2$b]",getReportSpec() != null, getReportSpec().getCustomClass()!=null ));
			//System.out.println(getReportSpec().getName());
		}

		List<List<ReportCell>> data = (chartData = new LinkedList<List<ReportCell>>());
		List<ReportCell> row = null;

		/* headers */

		JSONArray info = getSeriesLegend();
		row = new LinkedList<ReportCell>();
		data.add(row);
		row.add(new BlankCell());
		for(Object l: info) {
			//unchecked , but we're adding these as Strings in getSeriesLegend();
			row.add(new TitleCell((String)l));
		}
		if ("Y".equals(getString("-useAvg"))) {
			row.add(new TotalCell(getString("avg-input")));
			//out.println("<th class=\"first\" style=\"border-bottom: solid 1px #333; text-align: right; padding: 2px 5px;\">" + report.getString("avg-input") + "</th>");
		}
		if ("Y".equals(getString("-useTotal"))) {
			row.add(new TotalCell(getString("total-input")));
			//out.println("<th class=\"first\" style=\"border-bottom: solid 1px #333; text-align: right; padding: 2px 5px;\">" + report.getString("total-input") + "</th>");
		}
		ReportBean report = this;

		Calendar start = Calendar.getInstance();
		Calendar next = Calendar.getInstance();
		List<Date> array = dateRangeList; //new ArrayList<Date>();
		//List<String> keys = new ArrayList<String>();
		Map<String,String> values = new HashMap<String,String>();
		//Map<String,String> used = new HashMap<String,String>();
		ValueCell value = null;
		String lines[]  = getString("-customOrder").split(",");
		int l = lines.length;

		List<String> valueKeys = null;
		if(getString("Interval").startsWith("salesrep")) {
			if(displayEntityList == null || displayEntityList.isEmpty()) {
				valueKeys = new ArrayList<String>(1);
				valueKeys.add("NO_VALUES");
			} else {
				valueKeys = new ArrayList<String>(displayEntityList.size());
				for(DisplayEntity de: displayEntityList) {
					valueKeys.add(de.DisplayEntityID);
				}
			}
		}

		if (l > 0) {

			/* if we have valuekeys, use that. if we have array, use that. if not, return 0. progress will be ignored if totalCells is 0 */
			int totalCells = ( ((valueKeys != null) ? valueKeys.size() : ( dateRangeList != null ? dateRangeList.size(): 0 )  - 1) * l );  
			int progress = 0; 

			setProgress(progress, totalCells);

			for (int s = 0; s < l; ++s) {	

				String tag = lines[s];
				String type = StringUtils.trimToEmpty(report.getString(tag + "type"));
				String label = report.getString(tag + "input");
				boolean doOutput = "Y".equals(report.getString(tag + "table")); 
				String summary = StringUtils.trimToNull(report.getString(tag + "summary"));
				String fmt = StringUtils.trimToNull(report.getString(tag + "format"));
				int totalItems = 0; 

				if(doOutput) { 
					row = new LinkedList<ReportCell>(); 
					data.add(row);
				} else { 
					//will throw exceptions if it tries to do it but better to find them. 
					row = null;
				}
				// search search-sum
				if (type.indexOf("search") == 0) {
					String S = report.getString(tag + "id");
					String D = report.getString(tag + "datesearch");
					String Q = type.equals("search-sum") ? report.getString(tag + "field") : null;
					if(doOutput) row.add(new TitleCell(label));
					double searchTotal = 0;


					if(getString("Interval").startsWith("salesrep")) {
						String F = this.getString(tag + "repsearch");
						/*
						if(displayEntityList == null) {
							displayEntityList = this.getLocationsList();
						}
						 */
						for(DisplayEntity de: displayEntityList) {
							if("Region".equals(de.Type)){
								value = getBeanRepRegionValue(S, F, de.DisplayEntityID, D, Q);
							} else if("User".equals(de.Type)) {
								//value = getBeanRepLocationValue(S, F, de.DisplayEntityID, D, Q);
								value = getBeanValue(S, F, de.DisplayEntityID, D, Q);
							} else {
								/* the last types will be random, we're grouping them */
								value = getBeanRepLocationValue(S, F, de.DisplayEntityID, D, Q);
							}
							String t = value.text;
							value.text = format(value.text, fmt);
							if(doOutput) row.add(value);
							values.put(label.toUpperCase() + de.DisplayEntityID, t);
							if(!("salesreplocation".equals(getString("Interval")) || "salesrepregion".equals(getString("Interval"))) || !"User".equals(de.Type)) {
								try {
									searchTotal += Double.parseDouble(t);
									totalItems++;
								} catch (Exception e) { }	
							}
							this.setProgress(++progress, totalCells);
						}
					} else if("none".equals(getString("Interval"))) {
						value = getBeanValue(S, "-none", "none", D, Q, true);
						String t = value.text; 
						value.text = format(value.text, fmt);
						if(doOutput) row.add(value);
						values.put(label.toUpperCase() + "none", t);
						try {
							searchTotal += Double.parseDouble(t);
							totalItems++;
						} catch (Exception e) { }	
						this.setProgress(++progress, totalCells);

					} else {
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						for(int x=0; x<array.size()-1; x++) {
							start.setTime(array.get(x));
							next.setTime(array.get(x+1));
							value = getBeanDateValue(S,D,start.getTime(), next.getTime(), Q, df);

							String t = value.text; //getBeanResult(S,D,start.getTime(), next.getTime());
							/* at this point, t is a number in a string ie "23" */
							//String link = StringUtils.trimToNull(getBeanString(S,D,start.getTime(), next.getTime()));
							//if(link != null) { 
							//	link = contextPath + link;
							//}
							value.text = format(value.text, fmt);
							if(doOutput) row.add(value); //new ValueCell(format(t, fmt), link));
							//if (!used.containsKey(tag)) {
							//	keys.add(label);
							//	used.put(tag,label);
							//}
							values.put(label.toUpperCase() + start.getTime().getTime(), t);
							try {
								searchTotal += Double.parseDouble(t);
								totalItems++;
							} catch (Exception e) {};
							this.setProgress(++progress, totalCells);
						}
					}
					if(doOutput) {
						if ("Y".equals(report.getString("-useTotal"))) {
							if("Total".equals(summary)) {
								searchTotal = Math.round(searchTotal * 100) / 100;
								String total = "" + searchTotal;
								if (total.endsWith(".0")) total = total.substring(0,total.length() - 2); 
								if (total.endsWith(".00")) total = total.substring(0,total.length() - 3); 
								//if(doOutput) out.println("<td class=\"\" style=\"text-align: right;\">" + total + "</td>");
								if(doOutput) row.add(new TotalCell(total, format(searchTotal,fmt)));
							} else if ("Average".equals(summary)) {
								if (totalItems != 0) {
									double avg = searchTotal / totalItems;
									avg = Math.round(avg * 100) / 100;
									String average = "" + avg;
									if (average.endsWith(".0")) average = average.substring(0,average.length() - 2); 
									if (average.endsWith(".00")) average = average.substring(0,average.length() - 3); 
									if(doOutput) row.add(new TotalCell(average, format(avg,fmt)));
									//if(doOutput) out.println("<td class=\"\" style=\"text-align: right;\">" + average + "</td>");
								} else {
									//if(doOutput) out.println("<td class=\"\" style=\"text-align: right;\">&nbsp;</td>");
									if(doOutput) row.add(new TotalCell("",""));
								}
							} else {
								if(doOutput) row.add(new TotalCell("",""));
							}
						}
					}	
				}  else if (type.startsWith("calc")) {
					String M = report.getString(tag + "type-calc");

					if(doOutput) row.add(new TitleCell(label));
					//if(doOutput) out.println("<tr><td class=\"total first\">" + label + "</td>");
					double searchTotal = 0;

					if("none".equals(getString("Interval"))) {
						String t = evaluateTerm(M.toUpperCase(), values, "none"); 
						double val = 0d;
						try {
							val = Double.parseDouble(t);
							logger.debug("adding value to total {}",val);
							if(doOutput) {
								if(type.equals("calc-cumulative")) {
									row.add(new TitleCell(format(searchTotal + val, fmt)));
									t = String.valueOf(searchTotal + val);
								} else { 
									row.add(new TitleCell(format(val, fmt)));
								}
							}
							searchTotal += val; 
							totalItems++;
						} catch (Exception e) {
							if(doOutput) row.add(new TitleCell(format(t, fmt)));
						}
					} else { 
						int max = (valueKeys != null) ? valueKeys.size() : array.size() - 1;

						for(int x=0; x<max; x++) {

							final String valueKey = valueKeys != null ? valueKeys.get(x) : String.valueOf(array.get(x).getTime());
							//start.setTime(array.get(x));

							String t = evaluateTerm(M.toUpperCase(), values, valueKey); 
							double val = 0d;
							try {
								val = Double.parseDouble(t);
								logger.debug("adding value to total {}",val);
								if(doOutput) {
									if(type.equals("calc-cumulative")) {
										row.add(new TitleCell(format(searchTotal + val, fmt)));
										t = String.valueOf(searchTotal + val);
									} else { 
										row.add(new TitleCell(format(val, fmt)));
									}
								}
								if(!"salesreplocation".equals(getString("Interval")) && !"salesrepregion".equals(getString("Interval"))) {
									searchTotal += val; 
									totalItems++;
								} else {
									for(DisplayEntity d: displayEntityList) {
										if(d.DisplayEntityID.equals(valueKey)) {
											if (!"User".equals(d.Type)) {
												searchTotal += val;
												totalItems++;
											}
											break;
										}
									}
								}
							} catch (Exception e) {
								if(doOutput) row.add(new TitleCell(format(t, fmt)));
							}
							//if(doOutput) out.println("<td class=\"total\" style=\"text-align: right;\">" + formatNumber( t, fmt) + "&nbsp;&nbsp;</td>");
							//if (!used.containsKey(tag)) {
							//	keys.add(label);
							//	used.put(tag,label);
							//}
							values.put(label.toUpperCase() + valueKey, t);
							this.setProgress(++progress, totalCells);
						}
					}
					if(doOutput) { 
						if ("Y".equals(report.getString("-useTotal"))) {
							if("Total".equals(summary)) {
								if(doOutput) row.add(new TotalCell(format(searchTotal, fmt), format(searchTotal, fmt)));

								/* this may be wrong.. */
							} else if ("Average".equals(summary) && totalItems != 0 && doOutput) {
								logger.debug("useAvg searchTotal [{}] size [{}]",searchTotal, totalItems);
								double avg = searchTotal / totalItems; 
								row.add(new TotalCell(format(avg,fmt), format(avg,fmt))); 
								logger.debug("useAvg [{}]", avg);
							} else if(doOutput) {
								row.add(new TotalCell("",""));
							}
						}
						/*
						if ("Y".equals(report.getString("-useAvg"))) {
							if (array.size() > 0) {
								logger.debug("useAvg searchTotal [{}] size [{}]",searchTotal, array.size()-1);

								double avg = searchTotal / (array.size()-1);
								//avg = Math.round(avg * 100) / 100;
								//String average = "" + avg;
								//if (average.endsWith(".0")) average = average.substring(0,average.length() - 2); 
								//if (average.endsWith(".00")) average = average.substring(0,average.length() - 3); 
								if(doOutput) row.add(new TotalCell(format(avg,fmt), format(avg,fmt))); 
								//if(doOutput) out.println("<td class=\"total\" style=\"text-align: right;\">" + average + "</td>");
								logger.debug("useAvg [{}]", avg);
							} else {
								if(doOutput) row.add(new BlankCell());
							}
						}
						if ("Y".equals(report.getString("-useTotal"))) {
							//searchTotal = Math.round(searchTotal * 100) / 100;
							//String total = "" + searchTotal;
							//if (total.endsWith(".0")) total = total.substring(0,total.length() - 2); 
							//if (total.endsWith(".00")) total = total.substring(0,total.length() - 3); 
							//if(doOutput) out.println("<td class=\"total\" style=\"text-align: right;\">" + total + "</td>");
							//if(doOutput) row.add(new TotalCell(total, String.valueOf(searchTotal)));
							logger.debug("useTotal [{}] with format {}", searchTotal, fmt);
							if(doOutput) row.add(new TitleCell(format(searchTotal, fmt), format(searchTotal, fmt)));
						}
						 */
					}
				} else {
					//blank line ? 

				}
				//end loop
			}
			//end if 
			
			System.out.println(row.toString());
		}




		//JSONObject sdata = null;

		/* actually the problem here is that the series data is not indicative of what the chart should look like.
		 * code below looks fine for the most part but will look disorderly for averages and totals as they take up a line 
		 * in the series but only mention a cell in the chart.  
		 */
		/*

		if(series == null) { 
			this.getSeriesData(); //will regenerate
		}
		for(Object o : series) { 
			sdata = (JSONObject)o;

			row = new LinkedList<ReportCell>();
			data.add(row);

			row.add(new TitleCell((String)sdata.get("name")));
			info = (JSONArray)sdata.get("data");

			for(Object dataO: info) { 
				row.add(new ValueCell(dataO.toString(), null));
			}
		}
		 */
		return data;
	}

	public String evaluateTerm(String t, Map<String, String> values, Calendar start) {
		return evaluateTerm(t, values, String.valueOf(start.getTime().getTime()));
	}

	public String evaluateTerm(String t, Map<String, String> values, String valueKey) {
		logger.debug("evaluateTerm({}, Map<Values>, {}", t, valueKey);
		if (t != null) {
			//	for(String k: values.keySet()) {
			//System.out.format("value entries: [%1$s]\n", k);
			//}

			StringBuilder term = new StringBuilder();
			StringBuilder out = new StringBuilder();
			for(char c: t.toCharArray()) {
				switch(c) {
				case '+':
				case '-':
				case '*':
				case '/':
				case ')':
					logger.trace("orig term [{}]", t);
					String tm = StringUtils.trim(term.toString()).toUpperCase();
					logger.trace("term was [{}]", tm);
					String tmv = tm + valueKey;
					logger.trace("term lookup [{}]", tmv);
					if(values.containsKey(tmv)) { 
						out.append(values.get(tmv));
					} else {
						out.append(tm);
					}
					term.delete(0, term.length());						
					logger.trace("current output [{}]",out.toString());
				case '(':
					out.append(c);
					break;
				default:
					term.append(c);
				}
			}
			if(term.length()>0) { 
				String tm = StringUtils.trim(term.toString()).toUpperCase();
				logger.trace("term was [{}]", tm);
				String tmv = tm + valueKey;
				logger.trace("term lookup [{}]", tmv);
				if(values.containsKey(tmv)) { 
					out.append(values.get(tmv));
				} else {
					out.append(tm);
				}
			}
			logger.debug("Evaluated Statement: {} = {} ", t, out);
			return evalResult(out.toString());
			/*
			for (int k = keys.size() - 1; k >= 0 ; --k) {
				String key = keys.get(k);
				t = t.replaceAll(key.toUpperCase(),(values.get(key + start.getTime().getTime()))); 
			}							
			 */
		}
		return null;
	}


	/**
	 * It is intended that this not be used once the report wrapper object is
	 * functioning correctly It was in the original implementation.
	 */
	public JSONObject getChartObject(String renderTo) {
		if (!"Custom".equals(this.getString("-type")) && !"Custom".equals(this.getString("-reportType"))) {
			return super.getChartObject(renderTo);
		}
		return getCustomChartObject(renderTo);
	}

	/**
	 * Generates a custom report chart object, based on the data stored within
	 * this report bean.
	 * 
	 * @param renderTo
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public JSONObject getCustomChartObject(String renderTo) {

		if(resultMap == null) {
			resultMap = new HashMap<String, Object>();
		}
		if(chartJson != null) {
			try { 
				synchronized(chartJson) { 
					((JSONObject)chartJson.get("chart")).put("renderTo", renderTo);
				}
				return chartJson; 
			} catch (RuntimeException re) {
				logger.debug("Was unable to set render to on saved chart, recreating", re);
			}
		}

		JSONObject obj = new JSONObject();

		JSONObject chart = getJElem("renderTo", renderTo);
		String jChartType = this.getJsonChartType();
		// if (!this.getChartOptionSelected("average")) {
		chart.put("defaultSeriesType", super.getSeriesType(jChartType));
		// }
		/*
		 * chart:: plotBackgroundColor null plotBorderWidth null plotShadow
		 * false
		 */
		JSONArray margins = new JSONArray();
		margins.add(50);
		margins.add(50);
		margins.add(50);
		margins.add(50);
		chart.put("margins", margins);

		/* can actually take a x, y, xy, "" */
		if (this.getChartOptionSelected("zoomable")) {
			if (jChartType.equals("bar")) {
				chart.put("zoomType", "y");
			} else {
				chart.put("zoomType", "x");
			}
		}
		if (this.getChartOptionSelected("inverted")) {
			chart.put("inverted", new Boolean(true));
		}

		obj.put("chart", chart);
		String sub = this.getSubTitle();
		if (getString("Description").length() > 0) {
			sub = getString("Description");
			// chart.put("marginTop",20);
			/*
			 * JSONArray items = new JSONArray(); chart = new JSONObject();
			 * chart.put("html", getString("Description")); chart.put("style",
			 * new JSONData("{ bottom: '30px', align: 'center' }"));
			 * items.add(chart); obj.put("labels", getJElem("items",items));
			 */
		}

		chart = new JSONObject();
		// to disable: chart.put("enabled",new Boolean(false));
		chart.put("href", "");
		chart.put("text", "");
		obj.put("credits", chart);

		String export = InitServlet.getSystemParams().getProperty("exportURL");
		if (export == null || export.length() == 0) {
			export = "/exporter/";
		}
		obj.put("exporting", getJElem("url", export));

		obj.put("title", getJElem("text", this.getTitle()));
		obj.put("subtitle", getJElem("text", sub));

		String tooltip = " return '<b>'+ (this.point.name?this.point.name:this.point.series.name) +'</b>: '+ this.y; ";
		/*
		 * if (getRowCount() > 0 && getColumnCount() > 0) { tooltip =
		 * " return '<b>'+ (this.point.name?this.point.name:'') + '</b> : <b>' + (this.point.series.name?this.point.series.name:'') +'</b>: '+ this.y; "
		 * ; } else if (this.getChartOptionSelected("pie-summary")) { tooltip =
		 * " return '<b>'+ (this.point.name?this.point.name:this.point.series.name) +'</b>: '+ this.y + (this.point.perc?(' - '+this.point.perc):''); "
		 * ; }
		 */
		obj.put("tooltip",
				getJElem("formatter",
						new JSONFunction(tooltip, (String[]) null)));

		if (this.getColumnCount() > 8) {
			// obj.put("legend",getJElem("enabled",false));
		}

		/* plot options */
		JSONObject typeOptions = new JSONObject();
		JSONObject plotOptions = getJElem(this.getSeriesType(jChartType),
				typeOptions);
		/* lets try and keep a degree of backwards compatibility */
		if (this.getChartOptionSelected("stacking-percentage")) {
			plotOptions.put("series", getJElem("stacking", "percent"));
			// typeOptions.put("stacking", "percent");
		} else if (/* this.getChartType().contains("stacked") || */this
				.getChartOptionSelected("stacking")) {
			plotOptions.put("series", getJElem("stacking", "normal"));
			// typeOptions.put("stacking", "normal");
		}

		obj.put("plotOptions", plotOptions);

		if (this.getChartOptionSelected("labels")
				|| this.getChartOptionSelected("labels-rotated")) {

			JSONObject dl = getJElem("enabled", new Boolean("true"));
			if (this.getChartOptionSelected("labels-rotated")) {
				dl.put("rotation", new Integer(-90));
				dl.put("x", -3);
				dl.put("y", 10);
				dl.put("align", "right");
				dl.put("color", "#FFFFFF");
			}
			typeOptions.put("dataLabels", dl);

			/*
			 * dataLabels: { enabled: true, color: Highcharts.theme.textColor ||
			 * '#000000', connectorColor: Highcharts.theme.textColor ||
			 * '#000000', formatter: function() { return '<b>'+ this.point.name
			 * +'</b>: '+ this.y +' %'; } }
			 */
		}
		/*
		 * I'd like this to eventually be a context menu type operation. Shows a
		 * jQuery UI box at present
		 */
		typeOptions.put("events", getJElem("click", new PointInfo()));

		/*
		 * line: { events: { click: function(event) {
		 * console.log(event.point.config.link); } } }
		 */
		if ("bar".equals(jChartType)) {
			typeOptions.put("allowPointSelect", "true");
			typeOptions.put("cursor", "pointer");
		} else if ("pie".equals(this.getChartType())) {
			typeOptions.put("allowPointSelect", "true");
			typeOptions.put("cursor", "pointer");
			/* display the legend at the bottom of the page */
			typeOptions.put("showInLegend", new Boolean(true));
			// TODO
			JSONObject dLabels = new JSONObject();
			String pielabel = " return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage) + '%'; ";
			dLabels.put("formatter",
					new JSONFunction(pielabel, (String[]) null));
			dLabels.put("enabled", "true");
			typeOptions.put("dataLabels", dLabels);
		}

		/* we can use this if we want pie charts restricted to one series */
		if ("pie".equals(this.getChartType())) {
			JSONObject s1 = null;
			JSONArray series = new JSONArray();

			if ("territory_disabled".equals(this.getString(coltrigger))) {
				NumberFormat nf = new DecimalFormat("0.00%");
				double d = (1.0d / (double) this.getColumnCount()) * 0.9d;
				if (this.getRowCount() > 0) {
					int i = 0;

					for (int j = (this.getColumnCount() - 1); j >= 0; j--) {
						// for(int j=0; j<this.getColumnCount(); j++) {
						s1 = new JSONObject();
						s1.put("type", this.getJsonChartType());
						s1.put("name", this.getColumnTitle(j));
						s1.put("size", nf.format(1d - d
								* ((double) (this.getColumnCount() - j))));
						JSONArray data = new JSONArray();
						data.add(getJData(StringUtil.replace(
								this.getColumnTitle(j), String.valueOf('+'),
								"<br/>"), new Double(this.getItem(i, j)), this
								.getLink(i, j)));
						s1.put("data", data);
						series.add(s1);
					}
				}
			} else {
				// TODO Pie Chart Impl 
				s1 = new JSONObject();
				s1.put("type", this.getJsonChartType());
				s1.put("name", this.getTitle());

				//JSONArray data = new JSONArray();
				/*
				 * if (this.getRowCount() > 0) { // for(int i=0;
				 * i<this.getRowCount(); i++) { int i = 0; for (int j = 0; j <
				 * this.getColumnCount(); j++) {
				 * data.add(getJData(StringUtil.replace( this.getColumnTitle(j),
				 * String.valueOf('+'), "<br/>"), new Double(this.getItem(i,
				 * j)), this .getLink(i, j))); } // } s1.put("data", data);
				 * series.add(s1); }
				 */}
			obj.put("series", series);
		} else {
			JSONArray series = getSeriesData();

			JSONArray data = null;
			JSONObject sVal = null;
			//JSONObject sVal2 = null;

			// NumberFormat nf = new DecimalFormat("0.00%");
			// double d = (1.0d/(double)this.getColumnCount())*0.9d;
			//boolean ts = false;
			//long range = 0;
			//Calendar sdc = null;
			/*
			if (this.getChartOptionSelected("timeseries")) {

				if ("hourrange".equals(get("-rowtrigger"))) {
					range = 3600L * 1000L;
				} else if ("dayrange".equals(get("-rowtrigger"))) {
					range = 3600L * 24L * 1000L;
				} else if ("weekrange".equals(get("-rowtrigger"))) {
					range = 3600L * 24L * 7L * 1000L;
				} else if ("monthrange".equals(get("-rowtrigger"))) {
					// TODO: this may be different month to month 
					range = 3600L * 24L * 28L * 1000L;
				} else if ("yearrange".equals(get("-rowtrigger"))) {
					range = 3600L * 365L * 1000L;
				}
			}
			 */
			/*
			 * for (int j = 0; j < this.getColumnCount(); j++) { sVal = new
			 * JSONObject(); if (this.getChartOptionSelected("timeseries")) { //
			 * default to hour if (range != 0) { if (rowvars.length > 0) {
			 * java.util.Date sd = getDate(rowvars[1][0]); sdc = new
			 * GregorianCalendar(); sdc.setTime(getDate(rowvars[1][0]));
			 * sVal.put("name", this.getColumnTitle(j));
			 * sVal.put("pointInterval", new Long(range)); sVal.put(
			 * "pointStart", new JSONData("\"Date.UTC(" + sdc.get(Calendar.YEAR)
			 * + "," + sdc.get(Calendar.MONTH) + "," +
			 * sdc.get(Calendar.DAY_OF_MONTH) + ")\""));
			 * 
			 * data = new JSONArray(); for (int i = 0; i < this.getRowCount();
			 * i++) { // data.add(new Double(getItem(i,j)));
			 * data.add(getJData(StringUtil.replace( this.getRowTitle(i),
			 * String.valueOf('+'), "<br/>"), new Double(this.getItem(i, j)),
			 * this .getLink(i, j))); } sVal.put("data", data); ts = true; } } }
			 * 
			 * if (this.getChartOptionSelected("pie-summary")) { NumberFormat f
			 * = NumberFormat.getPercentInstance(); if (sVal2 == null) { sVal2 =
			 * new JSONObject(); sVal2.put("type", "pie"); sVal2.put("data", new
			 * JSONArray()); sVal2.put("size", 120); sVal2.put("center", new
			 * JSONData("[65,20]")); sVal2.put("showInLegend", false);
			 * sVal2.put("dataLabels", getJElem("enabled", false)); } long sum =
			 * 0L; for (int i = 0; i < this.getRowCount(); i++) { sum +=
			 * Long.parseLong(this.getItem(i, j)); } JSONObject o = new
			 * JSONObject(); o.put("name", this.getColumnTitle(j)); o.put("y",
			 * sum); o.put("perc", f.format(sum / total)); o.put("color", new
			 * JSONData( "\"Highcharts.getOptions().colors[" + ((JSONArray)
			 * sVal2.get("data")).size() + "]\"")); ((JSONArray)
			 * sVal2.get("data")).add(o); }
			 * 
			 * if (!ts) { sVal.put("type", getSeriesType(jChartType)); /*
			 * unreachable at present. if("pie".equals(jChartType)) {
			 * if(this.getColumnCount()>1) { sVal.put("dataLabels",getJElem
			 * ("enabled",j==(this.getColumnCount()-1)?"true":"false"));
			 * sVal.put("size",
			 * nf.format(d*((double)(this.getColumnCount()-j)))); //in some
			 * cases need innerSize } }
			 *//*
			 * sVal.put("name", this.getColumnTitle(j)); data = new
			 * JSONArray(); for (int i = 0; i < this.getRowCount(); i++) {
			 * // data.add(new Double(this.getValue(i, j)));
			 * data.add(getJData( StringUtil.replace(this.getRowTitle(i),
			 * String.valueOf('+'), "<br/>"), new Double(this.getItem(i,
			 * j)), this.getLink(i, j))); } sVal.put("data", data); }
			 * series.add(sVal); }

			if (sVal2 != null) {
				// we want to add this last explicitly.
				series.add(sVal2);
			}
			  */
			/*
			 * we're actually calculating this in the opposite direction to the
			 * above, so iterating twice is *probably* better.
			 */
			/* sdc always null 
			if (this.getChartOptionSelected("average-trailing") && ts
					&& range != 0 && sdc != null) {

				int points = 0;

				String spoints = (String) get("-points");
				if (spoints == null || spoints.length() == 0) {
					spoints = "6";
				}
				try {
					points = Integer.parseInt(spoints);
				} catch (Exception e) {
				}

				if (points == 0) {
					if ("hourrange".equals(get("-rowtrigger"))) {
						// range = 3600 * 1000;
						points = 24 * 44;
					} else if ("dayrange".equals(get("-rowtrigger"))) {
						// range = 3600 * 24 * 1000;
						points = 44;
					} else if ("weekrange".equals(get("-rowtrigger"))) {
						// range = 3600 * 24 * 7 * 1000;
						points = 6;
					} else if ("monthrange".equals(get("-rowtrigger"))) {
						// range = 3600 * 24 * 28 * 1000;
						points = 1;
					} else if ("yearrange".equals(get("-rowtrigger"))) {
						// range = 3600 * 365 * 1000;
					}
					if (points > 0)
						put("-points", String.valueOf(points));
				}

				if (points > 0) {
					/*
			 * A trailing average is the 3 month range around a
			 * particular point. To do this, we need to average out the
			 * values forward - and if available - back from a given
			 * data point To produce the average.
			 */
			/*
			 * for (int j = 0; j < this.getColumnCount(); j++) { sVal =
			 * new JSONObject(); sVal.put("name", this.getColumnCount()
			 * > 0 ? "Tr Av - " + this.getColumnTitle(j) :
			 * "Trailing Average"); sVal.put("type", "spline");
			 * 
			 * sVal.put("pointInterval", new Long(range)); sVal.put(
			 * "pointStart", new JSONData("\"Date.UTC(" +
			 * sdc.get(Calendar.YEAR) + "," + sdc.get(Calendar.MONTH) +
			 * "," + sdc.get(Calendar.DAY_OF_MONTH) + ")\""));
			 * 
			 * data = new JSONArray();
			 * 
			 * int rowmax = this.getRowCount() - 1; for (int i = 0; i <
			 * this.getRowCount(); i++) { double val = 0d; int counted =
			 * 0; int start = i - points; int end = i + points; if
			 * (start < 0) start = 0; if (end > rowmax) { end = rowmax;
			 * } for (int k = start; k <= end; k++) { // doing this a
			 * zillion times is craptastic. // maybe cast the whole
			 * result set? val += Double.parseDouble(this.getItem(k,
			 * j)); counted++; } if (val > 0) { val = val / counted; }
			 * data.add(new Double(val)); }
			 * 
			 * sVal.put("data", data); series.add(sVal); }
			 */
			//}

			//} else */
			if (this.getChartOptionSelected("average")) {
				sVal = new JSONObject();
				sVal.put("name", "Average");
				sVal.put("type", "spline");
				data = new JSONArray();
				/*
				 * for (int i = 0; i < this.getRowCount(); i++) { double val =
				 * 0d; for (int j = 0; j < this.getColumnCount(); j++) { val +=
				 * Double.parseDouble(this.getItem(i, j)); } if (val > 0) { val
				 * = val / this.getColumnCount(); } data.add(new Double(val)); }
				 */sVal.put("data", data);
				 series.add(sVal);
			}

			obj.put("series", series);


			double min = 0d;
			for(Object o: series) {
				if(o instanceof JSONObject) { 
					JSONObject jo = (JSONObject)o;
					Object o2 = jo.get("data"); 
					if(o2 instanceof JSONArray) {
						JSONArray a = (JSONArray)o2;
						for(Object o3: a) { 
							try {
								double d = Double.parseDouble(o3.toString());
								if(d < min) { 
									min = Math.floor(d);
								}
							} catch (Exception e) { 
								e.printStackTrace();
							}
						}
					}
				}
			}
			/* axis labels */

			JSONObject axis = new JSONObject();

			/*
			 * if (this.hasRowTitles() && !ts) { JSONArray labels = new
			 * JSONArray(); for (int j = 0; j < this.getRowCount(); j++) {
			 * labels.add(this.getRowTitle(j)); } axis.put("categories",
			 * labels);
			 * 
			 * if (labels.size() > 10) { axis.put( "labels", new JSONData(
			 * "{ \"x\": 5, \"y\" : 20, \"rotation\": -45, \"align\": \"right\" }"
			 * )); } } if (ts) { axis.put("type", "datetime");
			 * axis.put("maxZoom", new Double(14 / 24 * 60 * 60 * 1000)); }
			 */axis.put("title", this.getRowSubsetName());

			 if (axis.get("title").equals(ActionBean._blank)) {
				 axis.put("title", "Records");
			 }

			 JSONArray legend = new JSONArray(); 
			 for(Object o: getSeriesLegend()) {
				 if(o instanceof String) {
					 String s = (String)o;
					 while(s.startsWith("&nbsp;")) {
						 s = s.substring(6);
					 }
					 legend.add(s);
				 } else {
					 legend.add(o);
				 }
			 }

			 axis.put("categories", legend);

			 if("true".equals(getString("-swapaxis"))) {
				 JSONArray seriesN = new JSONArray();
				 JSONArray categoriesN = new JSONArray();

				 int max = legend.size() -1;

				 for(int i=0; ; i++) {
					 JSONObject row = new JSONObject();
					 seriesN.add(row);

					 row.put("name", legend.get(i));

					 JSONArray rowData = new JSONArray();
					 row.put("data", rowData);

					 for(Object o: series) {
						 if(o instanceof JSONObject) { 
							 JSONObject jo = (JSONObject)o;
							 if(i==0) categoriesN.add(jo.get("name"));
							 Object o2 = jo.get("data"); 
							 if(o2 instanceof JSONArray) {
								 JSONArray a = (JSONArray)o2;
								 rowData.add(a.get(i));
							 }
						 }
					 }
					 if(i == max) 
						 break;
				 }

				 axis.put("categories", categoriesN);
				 obj.put("series", seriesN);
			 }

			 JSONObject labels = new JSONObject();

			 labels.put("rotation", Integer.valueOf(-45));
			 labels.put("align", "right");

			 axis.put("labels", labels);

			 /*
			  * if(this.hasColTitles()) { JSONArray labels = new JSONArray();
			  * for(int i=0; i<this.getColumnCount(); i++) {
			  * labels.add(this.getColumnTitle(i)); }
			  * axis.put("categories",labels); }
			  */
			 obj.put("xAxis", axis);

			 axis = new JSONObject();

			 if (this.hasColTitles()) {
				 /*
				  * We don't need col titles, they're defined in the series
				  * JSONArray labels = new JSONArray(); for(int j=0;
				  * j<this.getColumnCount(); j++) {
				  * labels.add(this.getColumnTitle(j)); }
				  * axis.put("categories",labels);
				  */
			 }
			 if (this.hasColSummary()) {
				 axis.put("title", getJElem("text", this.getColSubsetName()));
			 } else {
				 axis.put("title", getJElem("text", "Records"));
			 }
			 axis.put("min", new Double(min));
			 obj.put("yAxis", axis);
			 /* in CustomReportBean rpspecr will always be null.
			if (this.rpspecr != null && rightaxis) {
				JSONArray yAxis = new JSONArray();
				yAxis.add(axis);
				axis = new JSONObject();
				// axis.put("min", 0);
				if (this.hasRightSubset()) {
					axis.put("title",
							getJElem("text", this.getRightSubsetName()));
				} else {
					axis.put("title", getJElem("text", "count"));
				}
				axis.put("opposite", new Boolean(true));
				yAxis.add(axis);
				obj.put("yAxis", yAxis);
			} else {
				obj.put("yAxis", axis);
			}
			  */
		}
		return (chartJson = obj);
	}

	protected List<Date> dateRangeList = null;
	protected List<List<ReportCell>> cellData = null;
	protected JSONArray series = null;

	public List<Date> getDateRangeList() {
		if (dateRangeList != null)
			return dateRangeList;
		return Collections.emptyList();
	}

	@SuppressWarnings("unchecked")
	protected JSONArray getSeriesData() {

		series = new JSONArray();
		dateRangeList = new ArrayList<Date>();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat mdf = new SimpleDateFormat("dd/MM/yyyy");

		final String interval = StringUtil.getDefaultString(getString("Interval"), "monthly");
		final String sortOrder = getString("-customOrder");
		final String lines[] = sortOrder.split(",");

		/*
		 * String startDate = (this.getString("-begin") != null)?
		 * this.getString("-begin") + " 00:00" : ""; String endDate =
		 * (this.getString("-end") != null)? this.getString("-end") + " 23:59" :
		 * "";
		 * 
		 * 
		 * 
		 * int shift = Calendar.MONTH; int amount = 1; if
		 * ("quarterly".equals(interval)) { shift = Calendar.MONTH; amount = 3;
		 * } else if ("monthly".equals(interval)) { shift = Calendar.MONTH;
		 * amount = 1; } else if ("fortnightly".equals(interval)) { shift =
		 * Calendar.DAY_OF_YEAR; amount = 14; } else if
		 * ("weekly".equals(interval)) { shift = Calendar.DAY_OF_YEAR; amount =
		 * 7; } else if ("daily".equals(interval)) { shift =
		 * Calendar.DAY_OF_YEAR; amount = 1; }
		 * 
		 * Calendar start = Calendar.getInstance(); try {
		 * start.setTime(sdf.parse(startDate)); } catch (Exception e) {}
		 * Calendar end = Calendar.getInstance(); try {
		 * end.setTime(sdf.parse(endDate)); } catch (Exception e) {} Calendar
		 * next = Calendar.getInstance();
		 */
		/*
		 * THIS CODE HAS NO EFFECT AS NEXT HAS SET TIME CALLED. try {
		 * next.setTime(sdf.parse(startDate)); } catch (Exception e) {}
		 * next.add(shift,amount); next.add(Calendar.MINUTE,-1);
		 */
		//ArrayList<String> keys = new ArrayList<String>();
		HashMap<String, String> values = new HashMap<String, String>();
		//HashMap<String, String> used = new HashMap<String, String>();

		if("none".equals(interval)) { 

		} else if(!interval.startsWith("salesrep")) { 
			String[][] vars = null;
			if ("monthly".equals(interval)) {
				vars = getRange(Calendar.MONTH);
			} else if ("daily".equals(interval)) {
				vars = getRange(Calendar.DAY_OF_MONTH);
			} else if ("weekly".equals(interval)) {
				vars = getRange(Calendar.WEEK_OF_YEAR);
			} else if ("fortnightly".equals(interval)) {
				vars = getRange(CALENDAR_FORTNIGHT);
			} else if ("quarterly".equals(interval)) {
				vars = getRange(CALENDAR_QUARTER);
			} else {
				// default to month
				vars = getRange(Calendar.MONTH);
			}
			String[] datevars = vars[ReportBean.values];

			for (String s : datevars) {
				logger.trace(s);
				try {

					dateRangeList.add(mdf.parse(s));
				} catch (java.text.ParseException pe) {
					logger.error("Error parsing date for CustomReport", pe);
					dateRangeList.add(new java.util.Date());
				}
			}
		} else if(getString("Interval").startsWith("salesrep")) {
			displayEntityList = this.getLocationsList();
		}
		List<String> valueKeys = null;
		if(getString("Interval").startsWith("salesrep")) {
			if(displayEntityList == null || displayEntityList.isEmpty()) {
				valueKeys = new ArrayList<String>(1);
				valueKeys.add("NO_VALUES");
			} else {
				valueKeys = new ArrayList<String>(displayEntityList.size());
				for(DisplayEntity de: displayEntityList) {
					valueKeys.add(de.DisplayEntityID);
				}
			}
		}

		try {
			int l = lines.length;
			if (l > 0) {
				ValueCell value = null;
				for (int s = 0; s < l; ++s) {

					String tag = lines[s];
					String type = StringUtils.trimToEmpty(this.getString(tag + "type"));
					String label = this.getString(tag + "input");
					boolean graph = "Y".equals(this.getString(tag + "graph"));

					if (type.indexOf("search") == 0) {
						String S = this.getString(tag + "id");
						String Q = type.equals("search-sum") ? getString(tag + "field") : null;
						String D = this.getString(tag + "datesearch");


						JSONArray data = new JSONArray();
						double searchTotal = 0;

						if(getString("Interval").startsWith("salesrep")) {
							String F = this.getString(tag + "repsearch");
							for(DisplayEntity de: displayEntityList) {
								if("Region".equals(de.Type)){
									value = getBeanRepRegionValue(S, F, de.DisplayEntityID, D, Q);
								} else if("User".equals(de.Type)) {
									value = getBeanValue(S, F, de.DisplayEntityID, D, Q);
								} else {
									value = getBeanRepLocationValue(S, F, de.DisplayEntityID, D, Q);
								}
								String t = value.text;
								data.add(Double.parseDouble(StringUtil.replace(StringUtil.replace(t, "$",""), ",", "")));
								values.put(label.toUpperCase() + de.DisplayEntityID, t);
								try {
									searchTotal += Double.parseDouble(t);
								} catch (Exception e) {
								}
							}
						} else if("none".equals(getString("Interval"))) {
							value = getBeanValue(S, "-none", "none", D, Q, true);
							String t = value.text; 
							data.add(Double.parseDouble(StringUtil.replace(StringUtil.replace(t, "$",""), ",", "")));
							values.put(label.toUpperCase() + "none", t);
							try {
								searchTotal += Double.parseDouble(t);
							} catch (Exception e) { }
						} else {
							for (int x = 0; x < dateRangeList.size() - 1; x++) {

								logger.debug("Using Date:");
								logger.debug(dateRangeList.get(x).toString());
								logger.debug("TO");
								logger.debug(dateRangeList.get(x+1).toString());

								//start.setTime(dateRangeList.get(x));
								//next.setTime(dateRangeList.get(x + 1));
								//next.add(Calendar.MINUTE, -1);

								value = getBeanDateValue(S,D,dateRangeList.get(x), dateRangeList.get(x + 1), Q, sdf);
								String t = value.text;
								/*
								String t = getBeanResult(S, D, start.getTime(),
										next.getTime());
								String link = getBeanString(S, D, start.getTime(),
										next.getTime());
								if (link.length() > 0) {
									link = contextPath + link;
								}
								 */
								data.add(Double.parseDouble(StringUtil.replace(StringUtil.replace(t, "$",""), ",", "")));
								//if (!used.containsKey(tag)) {
								//	keys.add(label.toUpperCase());
								//	used.put(tag, label.toUpperCase());
								//}
								values.put(label.toUpperCase() + dateRangeList.get(x).getTime(), t);
								try {
									searchTotal += Double.parseDouble(t);
								} catch (Exception e) {
								}
								//this.setProgress(++progress, totalCells);
							}
						} 



						if (graph && data.size() > 0) {
							JSONObject sVal = new JSONObject();
							sVal.put("name", label);
							sVal.put("data", data);
							series.add(sVal);
						}
						if (graph && "Y".equals(this.getString("-useAvg"))) {
							if (dateRangeList.size() > 0) {
								double avg = searchTotal / dateRangeList.size();
								avg = Math.round(avg * 100) / 100;
								data = new JSONArray();
								for (int x = 0; x < dateRangeList.size() - 1; x++) {
									data.add(avg);
								}
								JSONObject sVal = new JSONObject();
								sVal.put(
										"name",
										label + "-"
												+ this.getString("avg-input"));
								sVal.put("data", data);
								sVal.put("type", "line");
								series.add(sVal);
							}
						}
					}  else if (type.startsWith("calc")) {
						String M = this.getString(tag + "type-calc");
						JSONArray data = new JSONArray();
						double searchTotal = 0;

						if("none".equals(getString("Interval"))) {
							String t = evaluateTerm(M.toUpperCase(), values, "none");
							double val = 0d;
							try {
								val = Double.parseDouble(StringUtil.replace(StringUtil.replace(t, "$",""), ",", ""));
								searchTotal += val; 
								if(type.equals("calc-cumulative")) {
									data.add(searchTotal);
									t = String.valueOf(searchTotal);
								} else { 
									data.add(val);
								}
							} catch (Exception e) {
								data.add(t);
							}
							values.put(label.toUpperCase() + "none", t);
						} else { 
							int max = (valueKeys != null) ? valueKeys.size() : dateRangeList.size() - 1;
							logger.debug("adding calc values for {} to {}", label.toUpperCase(), max);
							for (int x = 0; x < max; x++) {
								//start.setTime(dateRangeList.get(x));
								//next.setTime(dateRangeList.get(x + 1));
								final String valueKey = valueKeys != null ? valueKeys.get(x) : String.valueOf(dateRangeList.get(x).getTime());
								String t = evaluateTerm(M.toUpperCase(), values, valueKey); 
								double val = 0d;
								try {
									val = Double.parseDouble(StringUtil.replace(StringUtil.replace(t, "$",""), ",", ""));
									searchTotal += val; 
									if(type.equals("calc-cumulative")) {
										data.add(searchTotal);
										t = String.valueOf(searchTotal);
									} else { 
										data.add(val);
									}
								} catch (Exception e) {
									data.add(t);
								}
								//if (!used.containsKey(tag)) {
								//	keys.add(label.toUpperCase());
								//	used.put(tag, label);
								//}

								values.put(label.toUpperCase() + valueKey, t);
								//System.out.format("adding value key : [%1$s]\n",label.toUpperCase() + start.getTime().getTime());
								//this.setProgress(++progress, totalCells);
							}
						}
						if (graph && data.size() > 0) {
							JSONObject sVal = new JSONObject();
							sVal.put("name", label);
							sVal.put("data", data);
							series.add(sVal);
						}
						if (graph && "Y".equals(this.getString("-useAvg"))) {
							if (dateRangeList != null && !dateRangeList.isEmpty()) {
								double avg = searchTotal / dateRangeList.size();
								avg = Math.round(avg * 100) / 100;
								data = new JSONArray();
								for (int x = 0; x < dateRangeList.size() - 1; x++) {
									data.add(avg);
								}
								JSONObject sVal = new JSONObject();
								sVal.put(
										"name",
										label + "-"
												+ this.getString("avg-input"));
								sVal.put("data", data);
								sVal.put("type", "line");
								series.add(sVal);
							}
						}
					} /*  else {
						for (int x = 0; x < dateRangeList.size(); ++x) {
							if (dateRangeList.get(x) != null) {
							} else {
							}
						}
					} */
				}
			}
		} catch (NumberFormatException nfe) {
			logger.error("Error Formatting Report", nfe); 
		}

		return series;
	}


	private List<DisplayEntity> displayEntityList = null;

	@SuppressWarnings("unchecked")
	protected JSONArray getSeriesLegend() {

		final JSONArray legend = new JSONArray();
		final String interval = StringUtils.defaultIfEmpty(getString("Interval"),"monthly");
		if("none".equals(getString("Interval"))) {
			logger.debug("Adding Blank to Legend");
			legend.add("");
		} else if(getString("Interval").startsWith("salesrep")) {
			displayEntityList = this.getLocationsList();
			for(DisplayEntity de: displayEntityList) {
				if("Region".equals(de.Type)) {
					legend.add(new StringBuilder().append("<strong>").append(de.Name).append("</strong>").toString());
				} else if("User".equals(de.Type)) {
					if(getString("Interval").equals("salesrep")) {
						legend.add(de.Name);
					} else {
						legend.add("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + de.Name);
					}
				} else {
					legend.add("&nbsp;&nbsp;&nbsp;&nbsp;" + de.Name);
				}
			}
		} else {
			if (dateRangeList == null) {
				getSeriesData(); // will populate dateRangeList
			}
			//final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
			final SimpleDateFormat mdf = new SimpleDateFormat("dd/MM/yy");

			for (int x = 0; x < dateRangeList.size() - 1; x++) {
				if ("daily".equals(interval)) {
					legend.add(mdf.format(dateRangeList.get(x)));
				} else {
					Date d = new Date(dateRangeList.get(x + 1).getTime());
					d.setTime(d.getTime() - (1000*60*60*24));
					legend.add(new StringBuilder(30).append(mdf.format(dateRangeList.get(x))).append(" - ").append(mdf.format(d)).toString());
				}
			}
		}
		return legend;

		/*
		 * All this shit is here duplicates the previous functionality (in
		 * getSeriesData) for no discernable reason.
		 * 
		 * String startDate = (this.getString("-begin") != null)?
		 * this.getString("-begin") + " 00:00" : ""; String endDate =
		 * (this.getString("-end") != null)? this.getString("-end") + " 23:59" :
		 * "";
		 * 
		 * String sortOrder = (this.getString("-customOrder") != null)?
		 * this.getString("-customOrder") : ""; String lines[] =
		 * sortOrder.split(",");
		 * 
		 * int shift = Calendar.MONTH; int amount = 1; if
		 * ("quarterly".equals(interval)) { shift = Calendar.MONTH; amount = 3;
		 * } else if ("monthly".equals(interval)) { shift = Calendar.MONTH;
		 * amount = 1; } else if ("fortnightly".equals(interval)) { shift =
		 * Calendar.DAY_OF_YEAR; amount = 14; } else if
		 * ("weekly".equals(interval)) { shift = Calendar.DAY_OF_YEAR; amount =
		 * 7; } else if ("daily".equals(interval)) { shift =
		 * Calendar.DAY_OF_YEAR; amount = 1; }
		 * 
		 * Calendar start = Calendar.getInstance(); try {
		 * start.setTime(sdf.parse(startDate)); } catch (Exception e) {}
		 * Calendar end = Calendar.getInstance(); try {
		 * end.setTime(sdf.parse(endDate)); } catch (Exception e) {} Calendar
		 * next = Calendar.getInstance(); try {
		 * next.setTime(sdf.parse(startDate)); } catch (Exception e) {}
		 * next.add(shift,amount); next.add(Calendar.MINUTE,-1);
		 * 
		 * ArrayList<Date> array = new ArrayList<Date>(); ArrayList<String> keys
		 * = new ArrayList<String>(); HashMap<String,String> values = new
		 * HashMap<String,String>(); HashMap<String,String> used = new
		 * HashMap<String,String>();
		 * 
		 * int loop = 0; while (start.before(end)) { if
		 * ("daily".equals(interval)) { legend.add(mdf.format(start.getTime()));
		 * } else { legend.add(mdf.format(start.getTime()) + " - " +
		 * mdf.format(next.getTime())); } array.add(start.getTime());
		 * start.add(shift,amount); next.setTime(start.getTime());
		 * next.add(shift,amount); next.add(Calendar.MINUTE,-1); }
		 * 
		 * return legend;
		 */
	}

	private ValueCell getBeanRepLocationValue(String beanID, String repField, String locationValue, String dateField, String sumQuestionID) {
		if(logger.isDebugEnabled()) logger.debug("getBeanRepLocationValue({}, {}, {}, {}, {})", new String[]{beanID, repField, locationValue, dateField, sumQuestionID});

		List<DisplayEntity> users = getFilteredReps(getLocationReps("UserContactLocations.LocationID", locationValue));
		if(users.isEmpty()) {
			return new ValueCell("0");
		} 
		return getBeanValue(beanID, repField, StringUtils.join(users, "+"), dateField, sumQuestionID); 
	}

	private List<DisplayEntity> getLocationsList() {
		List<DisplayEntity> list = new LinkedList<DisplayEntity>();

		@SuppressWarnings("unchecked")
		List<String> rll = (List<String>) get("RegionID" + ReportBean.set);
		@SuppressWarnings("unchecked")
		List<String> lll = (List<String>) get("LocationID" + ReportBean.set);
		if(rll == null) rll = Collections.emptyList();
		if(lll == null) lll = Collections.emptyList();

		GenRow regionCheck = null;

		if(!rll.isEmpty() || !lll.isEmpty()) {
			regionCheck = new GenRow();
			regionCheck.setTableSpec("ContactLocations");
			regionCheck.setParameter("-select1","ContactLocationID");
			if(!rll.isEmpty()) {
				regionCheck.setParameter("RegionID|1", StringUtils.join(rll, "+"));
			}
			if(!lll.isEmpty()) {
				regionCheck.setParameter("LocationID|1", StringUtils.join(lll, "+"));
			}
		}

		final String repSelection = StringUtils.trimToNull(getString("RepSelection"));
		if("salesrep".equals(getString("Interval"))) {
			@SuppressWarnings("unchecked")
			List<String> repll = (List<String>) get("RepUserID" + ReportBean.set);
			if(repll == null) {
				List<DisplayEntity> reps = getFilteredReps();
				if(regionCheck == null) 
					return reps;
				List<DisplayEntity> ret = new ArrayList<DisplayEntity>(reps.size());
				for(DisplayEntity de: reps) {
					regionCheck.setParameter("UserID", de.DisplayEntityID);
					regionCheck.doAction(GenerationKeys.SELECTFIRST);

					if(!regionCheck.isSuccessful()) {
						logger.debug("Rep {} skipped as not in loc/region {}", de.DisplayEntityID);
						logger.trace(regionCheck.getStatement());
						continue;
					}
					ret.add(de);
				}
				return ret; 
			} else {
				DisplayEntity other = null;
				StringBuilder otherID = null;
				for(String userId: repll) {
					UserBean user = UserService.getInstance().getSimpleUser(getConnection(), userId);
					if(user != null) {
						if(regionCheck != null) {
							regionCheck.setParameter("UserID", user.getUserID());
							regionCheck.doAction(GenerationKeys.SELECTFIRST);

							if(!regionCheck.isSuccessful()) {
								logger.debug("Rep {} skipped as not in loc/region {}", user.getUserID());
								logger.trace(regionCheck.getStatement());
								continue;
							}
						}
						if(repSelection == null ||
								"all".equals(repSelection) ||
								("active".equals(repSelection) && "Active".equals(user.getString("Status"))) ||
								("allwithother".equals(repSelection) && "Active".equals(user.getString("Status"))) ||
								("inactive".equals(repSelection) && "Inactive".equals(user.getString("Status")))) { 
							list.add(new DisplayEntity(user.getUserID(), user.getName(), "User", user.getString("Status")));
						} else if("allwithother".equals(repSelection) && "Inactive".equals(user.getString("Status"))) {
							if(other == null) {
								other = new DisplayEntity(user.getUserID(), "Other", "User", "Inactive");
								otherID = new StringBuilder().append(user.getUserID());
							} else {
								otherID.append("+").append(user.getUserID()).toString();
							}
						}
					}
				}
				if(other != null) {
					list.add(other);
				}
				return list;
			}
		}
		addLocationsList("NULL", list, rll, lll);
		return list;
	}

	private void addLocationsList(String parentDisplayID, List<DisplayEntity> list, List<String> rll, List<String> lll) {

		GenRow locations = new GenRow();
		try { 
			locations.setConnection(getConnection());
			locations.setTableSpec("DisplayEntities");
			locations.setParameter("-select0","DisplayEntityID");
			locations.setParameter("-select1","Name");
			locations.setParameter("-select2","Type");
			locations.setParameter("-select3","ProductID");
			locations.setParameter("ParentDisplayEntityID",parentDisplayID);
			locations.sortBy("Name",0);
			locations.getResults();

			while(locations.getNext()) {
				DisplayEntity de = null;
				if("Region".equals(locations.getData("Type")) && rll.contains(locations.getData("DisplayEntityID"))) {
					list.add(de = new DisplayEntity(locations));
				} else if (lll.contains(locations.getData("DisplayEntityID"))) {
					list.add(de = new DisplayEntity(locations));
				}
				int size = (de == null ? 0 : list.size());
				addLocationsList(locations.getData("DisplayEntityID"), list, rll, lll);

				if(de != null && list.size() == size && ("salesreplocation".equals(getString("Interval")) || "salesrepregion".equals(getString("Interval")))) {	//salesreplocationonly does not include reps themselves.
					/* ie, no children were added after this */ 
					if("Region".equals(de.Type)) {

						GenRow params = new GenRow();
						params.setParameter("UserContactLocations.RegionID|1", de.DisplayEntityID);
						params.setParameter("UserContactLocations.ContactLocationRegion.DisplayEntityParent.DisplayEntityID|1", de.DisplayEntityID);
						params.setParameter("UserContactLocations.ContactLocationRegion.DisplayEntityParent.DisplayEntityParent.DisplayEntityID|1", de.DisplayEntityID);

						list.addAll(getFilteredReps(getLocationReps(params)));
					} else if ("salesreplocation".equals(getString("Interval"))) {
						if("Office".equals(de.Type)) {
							list.addAll(getFilteredReps(getLocationReps("UserUserContacts.UserContactContacts.ContactCompanyContactLink.CompanyID", locations.getData("ProductID"))));
						} else {
							list.addAll(getFilteredReps(getLocationReps("UserContactLocations.LocationID", de.DisplayEntityID)));
						}
					}
				}
			}
		} finally {
			locations.close();
		}
	}

	private static class DisplayEntity extends Object {
		public String DisplayEntityID;
		public String Name;
		public String Type;
		public String Status = null;

		public DisplayEntity(String DisplayEntityID, String Name, String Type, String Status) {
			this.DisplayEntityID = DisplayEntityID;
			this.Name = Name;
			this.Type = Type;
			this.Status = Status;
		}
		public DisplayEntity(GenRow r) {
			DisplayEntityID = r.getData("DisplayEntityID");
			Name = r.getData("Name");
			Type = r.getData("Type");
		}

		@Override
		public String toString() {
			//for use in StringUtils.join
			return DisplayEntityID;
		}

		@Override
		public boolean equals(Object e) {
			if(!(e instanceof DisplayEntity)) return false;
			DisplayEntity e2 = (DisplayEntity)e;
			return e2 != null && StringUtils.equals(e2.Name, Name) && StringUtils.equals(DisplayEntityID, e2.DisplayEntityID);
		}
	}

	private List<DisplayEntity> getFilteredReps(List<DisplayEntity> list) {
		final String repSelection = StringUtils.trimToNull(getString("RepSelection"));
		if(repSelection == null || "all".equals(repSelection)) {
			return list;
		}
		final List<DisplayEntity> outList = new ArrayList<DisplayEntity>(list.size());
		if("active".equals(repSelection)) {
			for(DisplayEntity de: list) {
				if("Active".equals(de.Status)) {
					outList.add(de);
				}
			}
		}
		if("inactive".equals(repSelection)) {
			for(DisplayEntity de: list) {
				if("Inactive".equals(de.Status)) {
					outList.add(de);
				}
			}
		}
		if("allwithother".equals(repSelection)) {
			DisplayEntity other = null;
			StringBuilder otherID = null;
			for(DisplayEntity de: list) {
				if("Inactive".equals(de.Status)) {
					if(other == null) {
						other = de; 
						other.Name = "Other";
						otherID = new StringBuilder().append(other.DisplayEntityID);
					} else {
						otherID.append("+").append(de.DisplayEntityID);
					}
				} else {
					outList.add(de);
				}
			}
			if(other != null) {
				other.DisplayEntityID = otherID.toString();
			} else {
				other = new DisplayEntity("NONE_FOUND", "Other", "User", "Inactive");
			}
			outList.add(other);
		}
		return outList;
	}

	private List<DisplayEntity> getFilteredReps() {
		final String repSelection = StringUtils.trimToEmpty(getString("RepSelection"));
		if(repSelection == null) {
			return Collections.emptyList();
		}
		if("all".equals(repSelection)) {
			return getLocationReps("Status","Active+Inactive");
		} 
		if("active".equals(repSelection)) {
			return getLocationReps("Status","Active");
		}
		if("inactive".equals(repSelection)) {
			return getLocationReps("Status","Inactive");
		}
		if("allwithother".equals(repSelection)) {
			return getFilteredReps(getLocationReps("Status","Active+Inactive"));
		}
		return Collections.emptyList();
	}

	private List<DisplayEntity> getLocationReps(String restrictionField, String fieldValue) {
		GenRow params = new GenRow();
		params.setParameter(restrictionField, fieldValue);
		return getLocationReps(params);
	}

	private List<DisplayEntity> getLocationReps(GenRow params) {
		logger.debug("getLocationReps({})",params != null ? params.toString() : "{}");
		GenRow reps = new GenRow();
		try { 
			reps.setConnection(getConnection());
			reps.setTableSpec("Users");
			reps.putAll(params);
			//reps.setParameter(restrictionField, fieldValue);
			//if(restrictionField2 != null) {
			//	reps.setParameter(restrictionField2, fieldValue2);
			//}
			reps.setParameter("-select1","UserID as UserID");
			reps.setParameter("-select2","FirstName as FirstName");
			reps.setParameter("-select3","LastName as LastName");
			reps.setParameter("-select4","Status as Status");
			if(isSet("RepUserID")) {
				reps.setParameter("RepUserID", getString("RepUserID"));
			}
			reps.setParameter("-sort1","FirstName");
			reps.setParameter("-sort2","LastName");
			if(logger.isTraceEnabled()) {
				logger.trace(reps.toString());
				reps.doAction(GenerationKeys.SEARCH);
				logger.trace("LOCATION_REPS:" + reps.getSearchStatement());
			}
			reps.getResults(true);

			@SuppressWarnings("unchecked")
			List<String> rll = (List<String>) get("RepUserID" + ReportBean.set);
			if(rll == null) rll = Collections.emptyList();

			if(reps.getNext()) {
				List<DisplayEntity> list = new ArrayList<DisplayEntity>(reps.getSize());
				do {
					if(rll.isEmpty() || rll.contains(reps.getData("UserID"))) { 
						list.add(new DisplayEntity(reps.getData("UserID"), new StringBuilder().append(reps.getData("FirstName")).append(" ").append(reps.getData("LastName")).toString(), "User", reps.getData("Status")));
					}
				} while(reps.getNext());
				return list;
			}
			logger.debug("No reps found for search {}", reps.getSearchStatement());
			return Collections.emptyList();
		} finally {
			reps.close();
		}
	}

	private ValueCell getBeanRepRegionValue(String beanID, String repField, String regionValue, String dateField, String sumQuestionID) {
		if(logger.isDebugEnabled()) logger.debug("getBeanRepRegionValue({}, {}, {}, {}, {})", new String[]{beanID, repField, regionValue, dateField, sumQuestionID});

		GenRow params = new GenRow();
		params.setParameter("UserContactLocations.RegionID|1", regionValue);
		params.setParameter("UserContactLocations.ContactLocationRegion.DisplayEntityParent.DisplayEntityID|1", regionValue);
		params.setParameter("UserContactLocations.ContactLocationRegion.DisplayEntityParent.DisplayEntityParent.DisplayEntityID|1", regionValue);

		List<DisplayEntity> users = getFilteredReps(getLocationReps(params));
		if(users.isEmpty()) {
			return new ValueCell("0");
		} 
		return getBeanValue(beanID, repField, StringUtils.join(users, "+"), dateField, sumQuestionID); 
	}

	/**
	 * @deprecated - use pass dateformat 
	 */
	public ValueCell getBeanDateValue(String beanID, String dateField, Date start, Date end, String sumQuestionID) {
		if(logger.isDebugEnabled()) logger.debug("[dep]getBeanDateValue({},{},{},{},{})", new String[]{beanID, dateField, start.toString(), end.toString(), sumQuestionID});
		return getBeanDateValue(beanID, dateField, start, end, sumQuestionID, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"));
	}

	public ValueCell getBeanDateValue(String beanID, String dateField, Date start, Date end, String sumQuestionID, DateFormat sdf) {
		if(logger.isDebugEnabled()) logger.debug("getBeanDateValue({},{},{},{},{},DateFormat)", new String[]{beanID, dateField, start.toString(), end.toString(), sumQuestionID});
		String field = StringUtils.isNotBlank(dateField) ? dateField : "CreatedDate"; 
		return getBeanValue(beanID,field, sdf.format(start) + "-" + sdf.format(new Date(end.getTime()-60000L)), null, sumQuestionID, field.endsWith("CreatedDate") && new Date().after(end));
	}

	/**
	 * @deprecated - use specific methods.
	 */
	public ValueCell getBeanValue(String beanID, String dateField, Date start, Date end, String sumQuestionID) {
		if(logger.isDebugEnabled()) logger.debug("[dep]getBeanValue({},{},{},{},{})", new String[]{beanID, dateField, start.toString(), end.toString(), sumQuestionID});
		return getBeanDateValue(beanID, dateField, start, end, sumQuestionID);
	}

	private DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	private String getTomorrowDate(String endDate) {
		try { 
			Calendar c = Calendar.getInstance();
			c.setTime(df.parse(endDate));
			c.add(Calendar.DAY_OF_WEEK, 1);
			return df.format(c.getTime());
		} catch (ParseException pe) {
			logger.error("Error parsing date to find tomorrow [{}]", endDate);
			return endDate;
		}
	}

	public ValueCell getBeanValue(String beanID, String field, String fieldValue, String dateField, String sumQuestionID) {
		return getBeanValue(beanID, field, fieldValue, dateField, sumQuestionID, false);
	}

	public ValueCell getBeanValue(String beanID, String field, String fieldValue, String dateField, String sumQuestionID, boolean cachable) {
		if(logger.isDebugEnabled()) logger.debug("getBeanValue(beanID={}, field={}, fieldValue={}, dateField={}, sumQuestionID={}, cacheable={})", new String[]{beanID, field, fieldValue, dateField, sumQuestionID, String.valueOf(cachable)});
		if (StringUtils.isBlank(beanID) || StringUtils.isBlank(field) || StringUtils.isBlank(fieldValue))
			return new ValueCell("-");

		GenRow bean = new GenRow();
		bean.setTableSpec("Beans");
		bean.setConnection(getConnection());
		bean.setParameter("-select0", "Bean");
		bean.setParameter("-select1", "Type");
		bean.setColumn("BeanID", beanID);
		bean.doAction(GenerationKeys.SELECTFIRST);

		if (bean.getString("Bean").length() == 0)
			return new ValueCell("-");

		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.parseRequest(bean.getString("Bean"));

		ViewSpec vs = row.getViewSpec();
		row.setViewSpec((ViewSpec)null);
		row.setTableSpec(vs.getMainTableName());

		row.setParameter(field, fieldValue);

		if(StringUtils.isNotBlank(sumQuestionID)) {
			if ("Contacts".equals(bean.getString("Type"))) {
				row.setParameter("-join1","QuestionContactAnswer#ContactAnswerQuestionID=" + sumQuestionID);
				row.setParameter("-select0","SUM(QuestionContactAnswer_ContactAnswerQuestionID"+sumQuestionID+".AnswerNumber) as Count");
			} else if ("Companies".equals(bean.getString("Type"))) {
				row.setParameter("-join1","QuestionCompanyAnswer#CompanyAnswerQuestionID=" + sumQuestionID);
				row.setParameter("-select0","SUM(QuestionCompanyAnswer_CompanyAnswerQuestionID"+sumQuestionID+".AnswerNumber) as Count");
			} else if ("Notes".equals(bean.getString("Type"))) {
				if("NoOfGroups".equals(sumQuestionID)) {
					row.setParameter("-join1","NoteVisit");
					row.setParameter("-select0","SUM(NoteVisit.NoOfGroups) as Count");
				} else {
					row.setParameter("-join1","QuestionNoteAnswer#NoteAnswerQuestionID=" + sumQuestionID);
					row.setParameter("-select0","SUM(QuestionNoteAnswer_NoteAnswerQuestionID"+sumQuestionID+".AnswerNumber) as Count");
				}
			} else if ("Responses".equals(bean.getString("Type"))) {

			} else if ("Opportunities".equals(bean.getString("Type"))) {
				row.setParameter("-join1","QuestionOpportunityAnswer#OpportunityAnswerQuestionID=" + sumQuestionID);
				row.setParameter("-select0","SUM(QuestionOpportunityAnswer_OpportunityAnswerQuestionID"+sumQuestionID+".AnswerNumber) as Count");
			}
		}

		if(!row.isSet("-select0")) {
			if ("Contacts".equals(bean.getString("Type"))) {
				row.setParameter("-select0", "COUNT(DISTINCT Contacts.ContactID) AS Count");
			} else if ("Companies".equals(bean.getString("Type"))) {
				row.setParameter("-select0", "COUNT(DISTINCT Companies.CompanyID) AS Count");
			} else if ("Notes".equals(bean.getString("Type"))) {
				row.setParameter("-select0", "COUNT(DISTINCT Notes.NoteID) AS Count");
			} else if ("Responses".equals(bean.getString("Type"))) {
				row.setParameter("-select0", "COUNT(DISTINCT Responses.ResponseID) AS Count");
			} else if ("Opportunities".equals(bean.getString("Type"))) {
				row.setParameter("-select0", "COUNT(DISTINCT Opportunities.OpportunityID) AS Count");
			} else if ("Orders".equals(bean.getString("Type"))) {
				row.setParameter("-select0", "COUNT(DISTINCT Orders.OrderID) AS Count");
			} else {
				row.setParameter("-select0", "COUNT(*) AS Count");
			}
		}
		/* 
		 * the notation for date-date2 is >= date and <= date2 , so we put it a minute back to get it correct.
		 */
		//row.setParameter(dateField, sdf.format(start) + "-" + sdf.format(new Date(end.getTime()-60000L)));
		if(getString("GroupID").length()>0)  {
			if ("Contacts".equals(bean.getString("Type"))) {
				row.setParameter("ContactContactGroups.GroupID", getString("GroupID"));
			} else if ("Companies".equals(bean.getString("Type"))) {
				row.setParameter("CompanyCompanyGroups.GroupID", getString("GroupID"));
			} else if ("Notes".equals(bean.getString("Type"))) {
				row.setParameter("GroupID", getString("GroupID"));
			} else if ("Responses".equals(bean.getString("Type"))) {
				row.setParameter("ResponseNote.GroupID", getString("GroupID"));
			} else if ("Opportunities".equals(bean.getString("Type")) || "Orders".equals(bean.getString("Type"))) {
				row.setParameter("GroupID", getString("GroupID"));
			} 
		}
		if((getString("LocationID_URL").length() > 0 || getString("RegionID_URL").length() > 0) && !getString("Interval").startsWith("salesreplocation") && !getString("Interval").startsWith("salesrepregion"))  {
			if ("Contacts".equals(bean.getString("Type"))) {
				if (getString("LocationID_URL").length() > 0) row.setParameter("ContactContactLocations.LocationID|L", StringUtil.urlDecode(getString("LocationID_URL")));
				if (getString("RegionID_URL").length() > 0) row.setParameter("ContactContactLocations.RegionID|L", StringUtil.urlDecode(getString("RegionID_URL")));
			} else if ("Companies".equals(bean.getString("Type"))) {
				//row.setParameter("CompanyCompanyGroups.GroupID", getString("GroupID"));
			} else if ("Notes".equals(bean.getString("Type"))) {
				if (getString("LocationID_URL").length() > 0) row.setParameter("LocationID|L", StringUtil.urlDecode(getString("LocationID_URL")));
				if (getString("RegionID_URL").length() > 0) row.setParameter("RegionID|L", StringUtil.urlDecode(getString("RegionID_URL")));
			} else if ("Responses".equals(bean.getString("Type"))) {
				if (getString("LocationID_URL").length() > 0) row.setParameter("ResponseNote.LocationID|L", StringUtil.urlDecode(getString("LocationID_URL")));
				if (getString("RegionID_URL").length() > 0) row.setParameter("ResponseNote.RegionID|L", StringUtil.urlDecode(getString("RegionID_URL")));
			} else if ("Opportunities".equals(bean.getString("Type")) || "Orders".equals(bean.getString("Type"))) {
				if (getString("LocationID_URL").length() > 0) row.setParameter("LocationID|L", StringUtil.urlDecode(getString("LocationID_URL")));
				if (getString("RegionID_URL").length() > 0) row.setParameter("RegionID|L", StringUtil.urlDecode(getString("RegionID_URL")));
			} 
		} else if((getString("LocationID").length() > 0 || getString("RegionID").length() > 0) && !getString("Interval").startsWith("salesreplocation") && !getString("Interval").startsWith("salesrepregion"))  {
			if ("Contacts".equals(bean.getString("Type"))) {
				if (getString("LocationID").length() > 0) row.setParameter("ContactContactLocations.LocationID|L", getString("LocationID"));
				if (getString("RegionID").length() > 0) row.setParameter("ContactContactLocations.RegionID|L", getString("RegionID"));
			} else if ("Companies".equals(bean.getString("Type"))) {
				//row.setParameter("CompanyCompanyGroups.GroupID", getString("GroupID"));
			} else if ("Notes".equals(bean.getString("Type"))) {
				if (getString("LocationID").length() > 0) row.setParameter("LocationID|L", getString("LocationID"));
				if (getString("RegionID").length() > 0) row.setParameter("RegionID|L", getString("RegionID"));
			} else if ("Responses".equals(bean.getString("Type"))) {
				if (getString("LocationID").length() > 0) row.setParameter("ResponseNote.LocationID|L", getString("LocationID"));
				if (getString("RegionID").length() > 0) row.setParameter("ResponseNote.RegionID|L", getString("RegionID"));
			} else if ("Opportunities".equals(bean.getString("Type")) || "Orders".equals(bean.getString("Type"))) {
				if (getString("LocationID").length() > 0) row.setParameter("LocationID|L", getString("LocationID"));
				if (getString("RegionID").length() > 0) row.setParameter("RegionID|L", getString("RegionID"));
			} 
		}
		if(!getString("Interval").startsWith("salesrep")) { 
			if(getString("RepUserID").length()>0)  {
				if ("Contacts".equals(bean.getString("Type"))) {
					row.setParameter("ContactContactGroups.RepUserID", getString("RepUserID"));
				} else if ("Companies".equals(bean.getString("Type"))) {
					row.setParameter("CompanyCompanyGroups.RepUserID", getString("RepUserID"));
				} else if ("Notes".equals(bean.getString("Type"))) {
					row.setParameter("RepUserID", getString("RepUserID"));
				} else if ("Responses".equals(bean.getString("Type"))) {
					row.setParameter("ResponseNote.RepUserID", getString("RepUserID"));
				} else if ("Opportunities".equals(bean.getString("Type")) || "Orders".equals(bean.getString("Type"))) {
					row.setParameter("RepUserID", getString("RepUserID"));
				} else {
					logger.debug("had unknown type {}", bean.getString("Type"));
				}
			}
		}
		if((getString("Interval").startsWith("salesrep") || "none".equals(getString("Interval"))) && StringUtils.isNotBlank(dateField)){
			//If the interval is a date field, then we will set cachable in the getBeanDateValue method. Sales Rep types will be set here if they're limited.
			logger.debug("Begin [{}], End [{}], AltEnd [{}]", new String[]{getString(begin), getString(end), getString(altend)});
			if(isSet(begin) && isSet(altend)) {
				logger.debug("BeginAltEnd: begin={} altend={}", getString(begin), getString(end));
				row.setParameter(dateField, getString(begin) + "-" + getString(altend));
				String tom = getTomorrowDate(getString(altend));
				if(useCache && !cachable && new Date().after(getDate(tom)) && dateField.endsWith("CreatedDate")) {
					logger.debug("Setting cachable = true for begin={} and end={}", getString(begin), tom);
					cachable = true;
				}
			} else if (isSet(begin) && isSet(end)) {
				logger.debug("BeginEnd: begin={} end={}", getString(begin), getString(end));
				row.remove(dateField);
				//row.setParameter(dateField+"^1", ">=" + getString(begin));
				/* end should be the end of the end day, not the start of the end day */
				//row.setParameter(dateField+"^2", "<" +  tom);
				row.setParameter(dateField, getString(begin) + "-" + getString(end));
				String tom = getTomorrowDate(getString(end));
				if(useCache && !cachable && new Date().after(getDate(tom)) && dateField.endsWith("CreatedDate")) {
					logger.debug("Setting cachable = true for begin={} and end={}", getString(begin), tom);
					cachable = true;
				}
			} else if(isSet(begin)) {
				row.setParameter(dateField,  ">=" + getString(begin));
			}
		}
		/* remove all group, sort, order from the saved searches, this causes performance problems. */
		final String[] osf= { "-groupby","-sort","-order" };
		GenRow preSearch = new GenRow();
		for(int i=-1; ; i++) {
			boolean novalues = true;
			for(String s: osf) {
				s += (i == -1 ? "" : i);
				if(row.isSet(s)) {
					preSearch.setParameter(s, row.getString(s));
					row.remove(s);
					novalues = false;
				}
			}
			if(i > 2 && novalues) {
				break;
			}
		}
		row.doAction(GenerationKeys.SEARCH);

		System.out.println(row.getStatement());

		Cache c = InitServlet.getCache("reportingCache");
		String resultKey = ReportBean.hashStatement(row.getStatement());
		if(logger.isTraceEnabled()) {
			logger.trace(row.getStatement());
		}	
		Element e = null;
		if(useCache && cachable && c != null) {
			if ((e = c.get(resultKey)) != null) {
				logger.debug("cache hit {}" , resultKey);
				/* value should be cached and exists within the cache */
				return (ValueCell) e.getObjectValue();			
			}
		} else if(resultMap.containsKey(resultKey)) {
			logger.debug("cache hit {}" , resultKey);
			return (ValueCell)resultMap.get(resultKey);
		}
		row.doAction(GenerationKeys.SELECTFIRST);
		String result = row.isSet("Count") ? row.getString("Count") : "0";
		logger.trace("Result [{}]", result);
		if(!preSearch.isEmpty()) {
			row.parseRequest(preSearch);
		}
		/* generate the click throughs */
		final StringBuilder searchStr = new StringBuilder(200).append(contextPath);
		if ("Contacts".equals(bean.getString("Type"))) {
			searchStr.append("/runway/contact.search.action?");
		} else if ("Companies".equals(bean.getString("Type"))) {
			searchStr.append("/runway/company.search.action?");
		} else if ("Notes".equals(bean.getString("Type"))) {
			searchStr.append("/runway/note.search.action?");
		} else if ("Responses".equals(bean.getString("Type"))) {
			searchStr.append("/runway/response.search.action?");
		} else if ("Opportunities".equals(bean.getString("Type"))) {
			searchStr.append("/runway/process.search.action?");
		} else if ("Orders".equals(bean.getString("Type"))) {
			searchStr.append("/runway/order.search.action?");
		} else {
			return new ValueCell(result);
		}
		row.setViewSpec(vs);
		row.remove("-select0");
		row.setParameter("-action", "search");
		ValueCell res = new ValueCell(result, searchStr.append(row.toString()).toString());

		if(useCache && cachable && c != null) {
			c.put(new Element(resultKey, res));
		} else {
			resultMap.put(resultKey, res);
		}
		return res;
	}

	private static final String[][] customRowTypes = new String[][]{{"Search","search"}, {"Search Sum","search-sum"}, {"Calculation","calc"}, { "Calc. Cumulative","calc-cumulative"}}; 

	public static void printCustomRowTypeOptions(Writer out, String currentValue)
			throws IOException {
		printReportOptions(out, currentValue, customRowTypes);
	}

	/**
	 * This method uses MySQL to evaluate math expressions
	 * @param expression
	 * @return
	 */
	public String evalResult(String expression) {
		if (expression == null || expression.length() == 0)
			return "-";

		if(resultMap.containsKey(expression)) {
			logger.debug("cache hit {}" , expression);
			return (String)resultMap.get(expression);
		}
		/*
		 * MathParser mp = new MathParser(); Double d = mp.eval(expression);
		 * 
		 * return "" + d;
		 */
		Connection con = getConnection();
		if (con == null) {
			connect();
			setCloseConnection(true);
			con = getConnection();
		}

		try {
			GenRow row = new GenRow();
			row.setTableSpec("Contacts");
			row.setConnection(con);
			row.setParameter("-select0", new StringBuilder().append("( ").append(expression).append(" ) AS Results").toString());
			row.doAction("selectfirst");

			resultMap.put(expression, row.getString("Results"));
			return row.getString("Results");
		} catch (Exception e) {
			// just give 0 and don't stack trace
			resultMap.put(expression, "0");
			//e.printStackTrace();
			return "error";
		}
	}

	public String getTitle() {
		if (this.getString("Name").length() > 0) {
			return this.getString("Name");
		}
		if (rpspec != null) {
			return (rpspec.getTitle());
		}
		return (ActionBean._blank);
	}

	/*
	 * public static class GraphPreset extends ReportBean.GraphPreset {
	 * 
	 * public GraphPreset(String presetName, String category, String chartType,
	 * String[] options) { super(presetName, category, chartType, options); //
	 * TODO Auto-generated constructor stub } }
	 */

}
