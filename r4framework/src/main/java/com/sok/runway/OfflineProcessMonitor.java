package com.sok.runway;

import com.sok.framework.*;
import java.util.*;

public class OfflineProcessMonitor {
   
   HashMap executingProcesses = null;
   
   private static OfflineProcessMonitor offlineProcessMonitor = null;
   
   private OfflineProcessMonitor() {
      executingProcesses = new HashMap();
   }
   
   public synchronized void addOfflineProcess(OfflineProcess process) {
      executingProcesses.put(process.getProcessID(), process);
   }
   
   public synchronized void removeOfflineProcess(OfflineProcess process) {
      executingProcesses.remove(process.getProcessID());
   }
   
	public static synchronized OfflineProcessMonitor getOfflineProcessMonitor() {
	   if (offlineProcessMonitor == null) {
	      offlineProcessMonitor = new OfflineProcessMonitor();
	   }
	   return offlineProcessMonitor;
	}
	
	public Object[] getOfflineProcesses() {
	   return executingProcesses.values().toArray();
	}
	
	public void killProcess(String processID, String killer) {
	   OfflineProcess process = (OfflineProcess)executingProcesses.get(processID);
	   if (process != null) {
	      process.kill(killer);
	   }
	}
	
	public OfflineProcess getProcess(String processID) {
	   return (OfflineProcess)executingProcesses.get(processID);
	}
	
}