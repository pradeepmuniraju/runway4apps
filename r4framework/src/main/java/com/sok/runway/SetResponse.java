package com.sok.runway; 

 /*
  *	SetResponse is designed to alleviate the problems incurred with using <img src with a jsp file
  * It should not be used as a substitute for standard JSP/CMS pages etc. 
  */

import com.sok.framework.ActionBean; 
import com.sok.framework.InitServlet; 
import com.sok.framework.RowBean; 
import com.sok.framework.RowSetBean; 
import com.sok.framework.StringUtil;

import java.io.File; 
import java.io.FileInputStream;
import java.util.Properties;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletOutputStream;

import java.io.FileNotFoundException;
import java.io.IOException;

public class SetResponse extends HttpServlet {

	private static final long serialVersionUID = -7114533857358862601L;
	private static final String BR = "<br>"; 
	private static final String UNDER_SCORE = "_";
	private static final boolean DEBUG = false; 
	private static byte[] PIXEL_GIF; 
	private static boolean GIF_LOADED; 
	private static Properties sysprops;
	private static String sysUserID;
	
	public void init() { 
		//Initialise the servlet. 
		sysprops = InitServlet.getSystemParams();
		sysUserID = sysprops.getProperty("SystemUserID");
		
		//Try to load the gif on init
		GIF_LOADED = loadGif(); 
	}
	
	public void destroy() { 
		
	}
	
	public synchronized void doGet(HttpServletRequest request, 
		HttpServletResponse response) throws IOException {
		
		//Get the output stream. 
		ServletOutputStream out = response.getOutputStream();
		
		try { 
			//Create the response 
			String message = createResponse(request); 
			
			//If the init gif load has failed, try to do so again.
			if (!GIF_LOADED){ 
				GIF_LOADED = loadGif(); 
			}
			response.setContentType("image/gif");
			response.setContentLength(PIXEL_GIF.length);
			
			if (DEBUG) {
				//Set the content type to html and print the returned messages.
				response.setContentType("text/html"); 
				out.print(message); 
			} else {
				//Write the Byte array to the output stream
				out.write(PIXEL_GIF);
			}
		} catch (Exception e) {
			response.setContentType("text/html");
			out.print(ActionBean.writeStackTraceToString(e)); 
		}
	} 
	
	private boolean loadGif() { 
		try { 
			
			//Get the pixel gif, and confert to a FileInputStream
			StringBuffer gifpath = new StringBuffer(this.getServletContext().getRealPath("")); 		
			gifpath.append("/images/pixel.gif");
			FileInputStream gifis = new FileInputStream(new File(gifpath.toString()));
			
			//Convert the FileInputStream to a byte array
			PIXEL_GIF = new byte[gifis.available()];
			gifis.read(PIXEL_GIF);
			if (gifis.available() == PIXEL_GIF.length) { 
				return true; 
			} else { 
				return false;
			}
		}catch (FileNotFoundException e) {  
			//Send an email to SysAdmin to say that the file was not found.
			
			/*
			 * TODO : Add Email Creation Code
			 */
			
			return false; 
		}catch (Exception e) { 
			return false; 
		}
	}
	
	
	private String createResponse(HttpServletRequest request) {  

		String noteID = null; 
		String responseText = null; 		
		RowBean bean = new RowBean(); 
		RowSetBean responseBean = new RowSetBean();
		
		String path = request.getPathInfo(); 
		
		StringBuffer msg = new StringBuffer("Init Path:").append(path).append(BR);  
		if (true || path.startsWith("/")) { 
			path = path.substring(1); 
			msg.append("Cut Path:").append(path).append(BR); 
		}
		
		String[] options = path.split("/"); 
		noteID = options[0]; 
		responseText = options[1];
		
		msg.append("NoteID :").append(noteID).append(BR); 
		msg.append("InitResponse :").append(responseText).append(BR);
		
		if (responseText.endsWith(".gif")) { 
			responseText = responseText.substring(0,responseText.length()-4); 
		}
		
		msg.append("Response with underscore :").append(responseText).append(BR);
		
		responseText = StringUtil.replace(responseText,UNDER_SCORE,ActionBean._space); 
		msg.append("Response with space :").append(responseText).append(BR);
		
		if (noteID.length()>0) { 
			bean.setRequest(request); 
			bean.setViewSpec("NoteIDView"); 
			bean.setColumn("NoteID",noteID); 
			bean.setAction(ActionBean.SELECT); 
			bean.doAction(); 
		} else { 
			msg.append("NoteID was blank").append(BR); 
		}
		
		String contactID = bean.getString("ContactID"); 
		String referreruri = request.getHeader("Referer");
		if (referreruri == null) { 
			referreruri = ActionBean._blank; 
		}
		
		if (noteID.length() > 0 &&  contactID.length() > 0 &&  referreruri.indexOf("/crm/")< 0) {	
			
			boolean insertResponse = true;

			responseBean.setViewSpec("ResponseView");
			responseBean.setRequest(request);
			responseBean.setColumn("NoteID",noteID);
			responseBean.setColumn("ContactID",contactID);
			responseBean.setColumn("Description",responseText);
			responseBean.generateSQLStatement();
			responseBean.getResults();

			insertResponse = !responseBean.getNext();
			
			if (insertResponse) { 
				bean.clear();
				bean.setViewSpec("ResponseView");
				bean.setRequest(request);
				bean.setToNewID("ResponseID");
				bean.setColumn("NoteID",noteID);
				bean.setColumn("ContactID",contactID);
				bean.setColumn("Description",responseText);
				bean.setColumn("CreatedDate",ActionBean.NOW);
				bean.setColumn("CreatedBy",sysUserID);
				bean.setColumn("ModifiedDate",ActionBean.NOW);
				bean.setColumn("ModifiedBy",sysUserID);
				bean.setColumn("UserAgent",request.getHeader("User-Agent"));
				bean.setColumn("Referrer",referreruri);
				bean.setColumn("IPAddress",request.getRemoteAddr());
				bean.setAction(ActionBean.INSERT);
				bean.doAction();
			}
			
		} else { 
			msg.append("ContactID or referreruri is to blame<br>"); 
			msg.append("ContactID : ").append(contactID).append(BR); 
			msg.append("refferer : ").append(referreruri).append(BR); 
		}
		msg.append("Bean error: ").append(bean.getError()); 
		
		responseBean.close();
		bean.close(); 
		
		return msg.toString(); 
	}
}
