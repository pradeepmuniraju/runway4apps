package com.sok.runway;
import javax.servlet.*; 
import java.io.*;
import java.util.*;
import javax.servlet.jsp.*; 
import com.sok.framework.*;
public class FormatFileBean
{
	public static final String pagepath = "includes/pages";
	public static final String formatpath = "includes/formats";
	public static final String headerpath = "includes/headers";
	public static final String footerpath = "includes/footers";
	public static final String navigationpath = "includes/navigations";
	public static final String newsletterpath = "templates";

	public static final String NAV_GRANDPARENT = "Grandparent";
	public static final String NAV_PARENT = "Parent";
	public static final String NAV_SIBLING = "Sibling";
	public static final String NAV_CHILD = "Child";
	
	String webapp = null;
	File webappfolder = null;
	StringBuffer errors = new StringBuffer();
	boolean debug = false;
	String home = null;

	FormatFile filefilter = new FormatFile();

	public void setHome(String home)
	{
		this.home = home;
	}

	public void setDebug(boolean b)
	{
		debug = b;
	}

	public void setWebApp(String app)
	{
		webapp = app;
	}

	public void setContext(ServletContext cont)
	{
		webappfolder = new File(cont.getRealPath(""));
		webappfolder = webappfolder.getParentFile();
		webapp = cont.getInitParameter("CMSWebApp");
		webappfolder = new File(webappfolder,webapp);
		//webappfolder = new File(webappfolder,"includes");
		if(debug || !webappfolder.canRead())
		{
			if (!webappfolder.canRead()) {
				System.out.print("Error in ");
				errors.append("Error in ");
			}
			System.out.println("Webapp folder - " + webappfolder.getAbsolutePath());
			errors.append("Webapp folder - ");
			errors.append(webappfolder.getAbsolutePath());
			errors.append("\r\n");
		}
	}

	public String getError()
	{
		return(errors.toString());
	}

	/**
	 * @deprecated
	 * Modified to give a sorted list also, other method more efficient. 	
	 */
	public String[] getFileNames(String type)
	{
		String[] names = null;

		File[] formatfiles = getFiles(type);
		if(formatfiles!=null && formatfiles.length!=0)
		{
			names = new String[formatfiles.length];
			String tempname = null;
			int dotindex = 0;
			int nameend = 0;
			for(int i=0; i<names.length; i++)
			{
				tempname = formatfiles[i].getName();
				dotindex = tempname.indexOf('.');
				nameend = tempname.length();
				if(dotindex>0)
				{
					nameend = dotindex;
				}
				names[i] = tempname.substring(0,nameend);
			}
		}
		
		names = StringUtil.sortArray(names); 
		
		return(names);
	}
	
	/**
	 * Updated version of getFileNames which uses a TreeMap to sort the names via natural order.
	 * @param String type of files (ie formats, pages, navigations, headers) 
	 * @return String array of file names
	 */
	public String[] getFileNamesSorted(String type) { 

		File[] formatfiles = getFiles(type);
		
		TreeMap<String,String> list = new TreeMap<String,String>();
		String tempname = ""; 
		int dotindex = 0; 
		int nameend = 0; 
		
		for(File f: formatfiles) { 

			tempname = f.getName();
			dotindex = tempname.indexOf('.');
			nameend = tempname.length();
			if(dotindex>0)
			{
				nameend = dotindex;
			}
			tempname = tempname.substring(0,nameend);
			list.put(tempname,tempname); 
		}
		
		String[] names = new String[list.size()]; 
		
		Iterator<String> i = list.keySet().iterator();
		int ic = 0; 
		while(i.hasNext()) { 
			names[ic] = (String)i.next(); 
			ic++; 
		}
		return names; 
		
	}

	public File[] getFiles(String type)
	{
		File[] formatfiles = null;
		try
		{
			File formatfolder = null;
			if(home!=null)
			{
				formatfolder = new File(webappfolder,home);
				//System.out.println(formatfolder.getAbsolutePath());
				//System.out.println(home+ " set home "+formatfolder.getAbsolutePath());
			}
			else
			{
				formatfolder = webappfolder;
			}
			formatfolder = new File(formatfolder,type);
			if(formatfolder.exists())
			{
				formatfiles = formatfolder.listFiles(filefilter);
				
			}
				if(debug)
				{
					errors.append("Format folder - ");
					errors.append(formatfolder.getAbsolutePath());
					errors.append("\r\n");
				}
		}
		catch(Exception e){errors.append(ActionBean.writeStackTraceToString(e));}

		return(formatfiles);
	}


	public String[] getFolderNames()
	{
		String[] names = null;

		File[] formatfiles = getFolders();
		if(formatfiles!=null && formatfiles.length!=0) {
			names = new String[formatfiles.length];
			
			for(int i=0; i<formatfiles.length; i++) {
				names[i] = formatfiles[i].getName();
			}
		}
		return(names);
	}
   
	public File[] getFolders()
	{
	   
		File[] cmsfolders = null;
		try
		{
			cmsfolders = webappfolder.listFiles(new DirFilter());
			if(debug)
			{
				errors.append("CMS folder - ");
				errors.append(webappfolder.getAbsolutePath());
				errors.append("\r\n");
			}
		}
		catch(Exception e){errors.append(ActionBean.writeStackTraceToString(e));}

		return(cmsfolders);
	}

	public void printOptions(String name, JspWriter out)throws IOException
	{
		printOptions(name, null, out);
	}

	public boolean hasOptions(String name)
	{
		File[] files = getFiles(name);
		if(files!=null && files.length!=0)
		{
			return(true);
		}
		return(false);
	}

	public boolean hasSingleOption(String name)
	{
		File[] files = getFiles(name);
		if(files!=null && files.length==1)
		{
			return(true);
		}
		return(false);
	}

	public String getTemplatePath(String filename)
	{
		StringBuffer valuebuffer = new StringBuffer();
			valuebuffer.append(webapp.substring(webapp.indexOf('/')+1,webapp.length()));
			if(home!=null)
			{
				valuebuffer.append('/');
				valuebuffer.append(home);
			}
			valuebuffer.append('/');
			valuebuffer.append(newsletterpath);
			valuebuffer.append('/');
			valuebuffer.append(filename);
		return(valuebuffer.toString());
	}

	public void printOptions(String name, String selected, JspWriter out)throws IOException
	{
		String[] names = getFileNamesSorted(name); 
		String value = null;
		if(names!=null)
		{
			boolean selectedfound = false;
			for(int i=0; i<names.length; i++)
			{
			   if (names[i].indexOf(".") != 0) {
   				out.print("<option value=\"");
   				if(name.equals(newsletterpath))
   				{
   					value = getTemplatePath(names[i]);
   					out.print(value);
   					out.print("\"");
   					if(selected!=null && selected.equals(value))
   					{
   						out.print(" selected");
   						selectedfound = true;
   					}
   					out.print(">");
   				}
   				else
   				{
   					out.print(names[i]);
   					out.print("\"");
   					if(selected!=null && selected.equals(names[i]))
   					{
   						out.print(" selected");
   						selectedfound = true;
   					}
   					out.print(">");
   				}

   				out.print(names[i]);
   				out.print("</option>");
   			}
			}
			if(selected!=null && selected.length() > 0 && !selectedfound)
			{
				out.print("<option value=\"");
				out.print(selected);
				out.print("\" class=\"red\" selected=\"selected\">");
				out.print(selected);
				out.print("</option>");
			}
		}
	}

   public void printNavLevelOptions(String selected, JspWriter out) throws IOException {
      printNavLevelOptions(selected, out, null);
   }
   
   public void printNavLevelOptions(String selected, JspWriter out, List ancestors) throws IOException {
     
		String labels[] = {NAV_GRANDPARENT,NAV_PARENT,NAV_SIBLING,NAV_CHILD};
		String values[] = {NAV_GRANDPARENT,NAV_PARENT,NAV_SIBLING,NAV_CHILD};
		if (ancestors != null) {
		   if (ancestors.size() > 0) {
		      labels[3] = (String)ancestors.get(ancestors.size()-1);
		      if (ancestors.size() > 1) {
		         labels[2] = (String)ancestors.get(ancestors.size()-2);
   		      if (ancestors.size() > 2) {
   		         labels[1] = (String)ancestors.get(ancestors.size()-3);
      		      if (ancestors.size() > 3) {
      		         labels[0] = (String)ancestors.get(ancestors.size()-4);
      		      }
      		      else {
      		         labels[0] = "";
      		      }
   		      }
   		      else {
      		      labels[1] = "";
      		      labels[0] = "";
   		      }
		      }
		      else {
   		      labels[2] = "";
   		      labels[1] = "";
   		      labels[0] = "";
		      }
		   }
		   else {
		      labels[3] = "";
		      labels[2] = "";
		      labels[1] = "";
		      labels[0] = "";
		   }
		}  
		  
      out.print("<option value=\"\"></option>");
      if (selected == null || selected.length() == 0) {
         selected = NAV_CHILD;
      }
      
      for (int i=0; i < labels.length; i++) {
         if (labels[i].length() > 0) {
            out.print("<option value=\"");
            out.print(values[i]);
            out.print("\"");
            if (values[i].equals(selected)) {
            	out.print(" selected=\"selected\"");
            }
            out.print(">");
            out.print(labels[i]);
            out.print("</option>");
         }
      }
   }
   
	class FormatFile implements FilenameFilter
	{
		public boolean accept(File dir, String name)
		{
			if(name.indexOf(".jsp")>0 || (name.indexOf(".vt")>0 && name.indexOf("_text.vt")<0))
			{
				return(true);
			}
			return(false);
		}
	}
	
   class DirFilter implements FileFilter {
      public boolean accept(File pathname) {
          return pathname.isDirectory();
      }
   }
}
