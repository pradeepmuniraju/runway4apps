/*
 * User: Behrang
 * Date: Jun 21, 2010
 * Time: 12:33:31 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.Event;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class EventDAO {

    private HttpServletRequest request;

    public EventDAO(HttpServletRequest request) {
        this.request = request;
    }

    public List<Event> findByCampaignId(String id) {
        GenRow eventRow = createEventRow();
        eventRow.setParameter("CampaignID", id);
        eventRow.doAction(GenerationKeys.SEARCH);
        eventRow.getResults(true);

        List<Event> events = new ArrayList<Event>();
        while (eventRow.getNext()) {
            events.add(fetch(eventRow));
        }

        eventRow.close();

        return events;
    }

    public Event findById(String id) {
        GenRow eventRow = createEventRow();
        eventRow.setParameter("EventID", id);
        eventRow.doAction(GenerationKeys.SELECT);
        Event event = fetch(eventRow);
        return event;
    }

    private GenRow createEventRow() {
        GenRow eventRow = new GenRow();
        eventRow.setRequest(request);
        eventRow.setTableSpec("Events");
        eventRow.setParameter("-sort0","SortOrder");
        eventRow.setParameter("-select1", "EventID");
        eventRow.setParameter("-select2", "CampaignID");
        eventRow.setParameter("-select3", "Name");
        return eventRow;
    }

    private Event fetch(GenRow row) {
        Event event = new Event();
        event.setAttemptsAllocated(row.getInt("AttemptsAllocated"));
        event.setBeanId(row.getString("BeanID"));
        event.setBody(row.getString("Body"));
        event.setCampaignId(row.getString("CampaignID"));
        event.setCreatedBy(row.getString("CreatedBy"));
        event.setCreatedDate(row.getString("CreatedDate"));
        event.setEventDate(row.getDateTime("EventDate"));
        event.setId(row.getString("EventID"));
        event.setLeadtime(row.getString("Leadtime"));
        event.setModifiedBy(row.getString("ModifiedBy"));
        event.setModifiedDate(row.getString("ModifiedDate"));
        event.setName(row.getString("Name"));
        event.setRepSpecific(row.getString("RepSpecific"));
        event.setRepUserId(row.getString("RepUserID"));
        event.setSortOrder(row.getInt("SortOrder"));
        event.setStatus(row.getString("Status"));
        event.setSubject(row.getString("Subject"));
        event.setTemplateId(row.getString("TemplateID"));
        event.setType(row.getString("Type"));

        return event;       
    }

}
