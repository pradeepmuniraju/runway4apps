/*
 * User: Behrang
 * Date: Jun 30, 2010
 * Time: 1:52:39 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.Bean;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class BeanDAO {

    private HttpServletRequest request;

    public BeanDAO(HttpServletRequest request) {
        this.request = request;
    }

    public Bean findById(String id) {
        GenRow beanRow = new GenRow();
        beanRow.setRequest(request);
        beanRow.setViewSpec("BeanView");
        beanRow.setParameter("BeanID", id);
        beanRow.doAction(GenerationKeys.SELECT);
        Bean bean = new Bean();
        bean.setBean(beanRow.getString("Bean"));
        bean.setName(beanRow.getString("Name"));
        bean.setType(beanRow.getString("Type"));
        bean.setId(beanRow.getString("BeanID"));
        beanRow.close();
        return bean;
    }

    public List<Bean> findByType(String type) {

        List<Bean> result = new ArrayList<Bean>();

        GenRow beanRow = new GenRow();
        beanRow.setRequest(request);
        beanRow.setViewSpec("BeanView");
        beanRow.setParameter("Type", type);
        beanRow.doAction(GenerationKeys.SEARCH);
        beanRow.getResults(true);

        while (beanRow.getNext()) {
            Bean bean = new Bean();
            bean.setBean(beanRow.getString("Bean"));
            bean.setName(beanRow.getString("Name"));
            bean.setType(beanRow.getString("Type"));
            bean.setId(beanRow.getString("BeanID"));
            result.add(bean);
        }

        beanRow.close();

        return result;
    }

}
