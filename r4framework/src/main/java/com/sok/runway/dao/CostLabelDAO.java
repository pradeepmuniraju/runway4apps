/*
 * User: Behrang
 * Date: Jun 16, 2010
 * Time: 12:52:52 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.CostLabel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class CostLabelDAO {

    private HttpServletRequest request;

    public CostLabelDAO(HttpServletRequest request) {
        this.request = request;
    }

    public CostLabel findById(String id) {
        GenRow labelRow = new GenRow();
        labelRow.setRequest(request);
        labelRow.setTableSpec("CostLabels");
        labelRow.setParameter("CostLabelID", id);
        labelRow.setParameter("-select1", "CostLabelID");
        labelRow.setParameter("-select2", "CostCategoryID");
        labelRow.setParameter("-select3", "Label");
        labelRow.setParameter("-select4", "Description");
        labelRow.setParameter("-select5", "DefaultCost");
        labelRow.doAction(GenerationKeys.SELECT);

        CostLabel cl = null;
        if (labelRow.isSuccessful()) {
            cl = new CostLabel();
            cl.setId(labelRow.getString("CostLabelID"));
            cl.setCategoryId(labelRow.getString("CostCategoryID"));
            cl.setLabel(labelRow.getString("Label"));
            cl.setDescription(labelRow.getString("Description"));
            cl.setDefaultCost(labelRow.getString("DefaultCost"));
        }

        return cl;
    }

    public List<CostLabel> findByCostCategoryId(String ccId) {
        GenRow labelRow = new GenRow();
        labelRow.setRequest(request);
        labelRow.setTableSpec("CostLabels");
        labelRow.setParameter("CostCategoryID", ccId);
        labelRow.setParameter("-select1", "CostLabelID");
        labelRow.setParameter("-select2", "CostCategoryID");
        labelRow.setParameter("-select3", "Label");
        labelRow.setParameter("-select4", "Description");
        labelRow.setParameter("-select5", "DefaultCost");
        labelRow.doAction(GenerationKeys.SEARCH);
        labelRow.getResults(true);

        List<CostLabel> labels = new ArrayList<CostLabel>();

        while (labelRow.getNext()) {
            CostLabel cl = new CostLabel();
            cl.setId(labelRow.getString("CostLabelID"));
            cl.setCategoryId(labelRow.getString("CostCategoryID"));
            cl.setLabel(labelRow.getString("Label"));
            cl.setDescription(labelRow.getString("Description"));
            cl.setDefaultCost(labelRow.getString("DefaultCost"));
            labels.add(cl);
        }

        return labels;
    }

}
