/*
 * User: Behrang
 * Date: Jul 13, 2010
 * Time: 1:31:45 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.UserPreference;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class UserPreferencesDAO {

    private HttpServletRequest request;

    public UserPreferencesDAO(HttpServletRequest request) {
        this.request = request;
    }

    public void saveOrUpdate(UserPreference userPref) {

        UserPreference up = findByUserIdAndKey(userPref.getUserId(), userPref.getPreferenceKey());
        if (up == null) {
            GenRow prefRow = new GenRow();
            prefRow.setRequest(request);
            prefRow.setTableSpec("UserPreferences");
            prefRow.createNewID();
            prefRow.setParameter("UserID", userPref.getUserId());
            prefRow.setParameter("PreferenceKey", userPref.getPreferenceKey());
            prefRow.setParameter("PreferenceValue", userPref.getPreferenceValue());
            prefRow.setAction(ActionBean.INSERT);
            prefRow.doAction();
        } else {
            GenRow prefRow = new GenRow();
            prefRow.setRequest(request);
            prefRow.setTableSpec("UserPreferences");
            prefRow.setParameter("PreferenceID", up.getId());
            prefRow.setParameter("UserID", userPref.getUserId());
            prefRow.setParameter("PreferenceKey", userPref.getPreferenceKey());
            prefRow.setParameter("PreferenceValue", userPref.getPreferenceValue());
            prefRow.setAction(ActionBean.UPDATE);
            prefRow.doAction();
        }
    }

    public UserPreference findByUserIdAndKey(String userId, String key) {
        GenRow prefRow = new GenRow();
        prefRow.setRequest(request);
        prefRow.setTableSpec("UserPreferences");
        prefRow.setParameter("UserID", userId);
        prefRow.setParameter("PreferenceKey", key);
        prefRow.setParameter("-select1", "PreferenceID");
        prefRow.setParameter("-select2", "UserID");
        prefRow.setParameter("-select3", "PreferenceKey");
        prefRow.setParameter("-select4", "PreferenceValue");        
        prefRow.setAction(GenerationKeys.SELECTFIRST);
        prefRow.doAction();
        UserPreference userPref = null;
        if (prefRow.isSuccessful()) {
            userPref = new UserPreference();
            userPref.setId(prefRow.getString("PreferenceID"));
            userPref.setUserId(prefRow.getString("UserID"));
            userPref.setPreferenceKey(prefRow.getString("PreferenceKey"));
            userPref.setPreferenceValue(prefRow.getString("PreferenceValue"));
        }
        return userPref;        
    }

    public Map<String, String> findByUserIdAsMap(String userId) {
        GenRow prefRow = new GenRow();
        prefRow.setRequest(request);
        prefRow.setTableSpec("UserPreferences");
        prefRow.setParameter("UserID", userId);        
        prefRow.setParameter("-select1", "PreferenceID");
        prefRow.setParameter("-select2", "UserID");
        prefRow.setParameter("-select3", "PreferenceKey");
        prefRow.setParameter("-select4", "PreferenceValue");
        prefRow.setAction(GenerationKeys.SEARCH);
        prefRow.getResults();

        Map<String, String> userPrefs = new HashMap<String, String>();
        while (prefRow.getNext()) {
            String key = prefRow.getString("PreferenceKey");
            String val = prefRow.getString("PreferenceValue");
            userPrefs.put(key, val);
        }
        prefRow.close();

        return userPrefs;
    }
}
