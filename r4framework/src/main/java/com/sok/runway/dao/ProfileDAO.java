package com.sok.runway.dao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.Profile;

public class ProfileDAO {
    public List<Profile> findAll(HttpServletRequest request) {
        GenRow profileRow = new GenRow();
        profileRow.setTableSpec("Surveys");
        profileRow.setRequest(request);
        profileRow.setParameter("-select1", "SurveyID");
        profileRow.setParameter("-select2", "Name");
        profileRow.setParameter("-select3", "Description");
        profileRow.setParameter("-select4", "GroupID");
        profileRow.setParameter("-select5", "ProfileType");
        profileRow.sortBy("Name",0);
        profileRow.doAction(GenerationKeys.SEARCH);
        profileRow.getResults(true);

        List<Profile> profiles = new ArrayList<Profile>();
        populateProfiles(profileRow, profiles);
        profileRow.close();

        return profiles;
    }
    
    public Profile findById(HttpServletRequest request, String id) {
        GenRow profileRow = new GenRow();
        profileRow.setTableSpec("Surveys");
        profileRow.setRequest(request);
        profileRow.setParameter("SurveyID", id);
        profileRow.setParameter("-select1", "Name");
        profileRow.setParameter("-select2", "Description");
        profileRow.setParameter("-select3", "GroupID");
        profileRow.setParameter("-select4", "ProfileType");
        
        profileRow.doAction(GenerationKeys.SELECT);
        profileRow.getResults(true);
        Profile profile = null;
        if (profileRow.getNext()) {
            profile = new Profile();
            populateProfile(profileRow, profile);
        }
        profileRow.close();
        return profile;
    }
    
    public List<Profile> findAllByGroupID(HttpServletRequest request, String groupID) {
        GenRow profileRow = new GenRow();
        profileRow.setTableSpec("Surveys");
        profileRow.setRequest(request);
        profileRow.setParameter("-select1", "SurveyID");
        profileRow.setParameter("-select2", "Name");
        profileRow.setParameter("-select3", "Description");
        profileRow.setParameter("-select4", "ProfileType");
        profileRow.setParameter("GroupID", groupID);
        profileRow.sortBy("Name",0);
        profileRow.doAction(GenerationKeys.SEARCH);
        profileRow.getResults(true);

        List<Profile> profiles = new ArrayList<Profile>();
        populateProfiles(profileRow, profiles);
        profileRow.close();

        return profiles;
    }
    
    public List<Profile> findAllByGroupIDAndProfileType(HttpServletRequest request, String groupID, String profileType) {
        GenRow profileRow = new GenRow();
        profileRow.setTableSpec("Surveys");
        profileRow.setRequest(request);
        profileRow.setParameter("-select1", "SurveyID");
        profileRow.setParameter("-select2", "Name");
        profileRow.setParameter("-select3", "Description");
        profileRow.setParameter("ProfileType", profileType);
        profileRow.setParameter("GroupID", groupID);
        profileRow.sortBy("Name",0);
        profileRow.doAction(GenerationKeys.SEARCH);
        profileRow.getResults(true);

        List<Profile> profiles = new ArrayList<Profile>();
        populateProfiles(profileRow, profiles);
        profileRow.close();

        return profiles;
    }    
    
    private void populateProfiles(GenRow profileRow, List<Profile> profiles) {
        while (profileRow.getNext()) {
            Profile profile = new Profile();
            populateProfile(profileRow, profile);
            profiles.add(profile);
        }
    }
    
    private void populateProfile(GenRow profileRow, Profile profile) {
        profile.setId(profileRow.getString("SurveyID"));
        profile.setName(profileRow.getString("Name"));
        profile.setDescription(profileRow.getString("Description"));
        profile.setGroupID(profileRow.getString("GroupID"));
        profile.setProfileType(profileRow.getString("ProfileType"));
    }
}
