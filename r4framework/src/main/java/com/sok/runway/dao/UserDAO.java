/*
 * User: Behrang
 * Date: Jun 9, 2010
 * Time: 5:03:35 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.Group;
import com.sok.runway.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    public List<User> findAll(HttpServletRequest request) {

        GenRow userRow = new GenRow();
        userRow.setTableSpec("Users");
        userRow.setRequest(request);
        userRow.setParameter("-select1", "UserID");
        userRow.setParameter("-select2", "FirstName");
        userRow.setParameter("-select3", "LastName");
        userRow.setParameter("-select4", "Username");
        userRow.sortBy("FirstName", 0);
        userRow.sortBy("LastName", 1);
        userRow.doAction(GenerationKeys.SEARCH);
        userRow.getResults(true);

        List<User> users = new ArrayList<User>();
        while (userRow.getNext()) {
            User u = new User();
            u.setId(userRow.getString("UserID"));
            u.setFirstName(userRow.getString("FirstName"));
            u.setLastName(userRow.getString("LastName"));
            u.setUsername(userRow.getString("Username"));
            users.add(u);
        }

        userRow.close();

        return users;
    }

    public List<User> findAll(HttpServletRequest request, String... joins) {

        GenRow userRow = new GenRow();
        userRow.setTableSpec("Users");
        userRow.setRequest(request);
        for (int i = 0; i < joins.length; i++) {
            userRow.setParameter("-join" + (i + 1), joins[i]);
        }
        userRow.setParameter("-groupby1", "UserID");
        userRow.setParameter("-select1", "UserID");
        userRow.setParameter("-select2", "FirstName");
        userRow.setParameter("-select3", "LastName");
        userRow.setParameter("-select4", "Username");
        userRow.sortBy("FirstName", 0);
        userRow.sortBy("LastName", 1);
        userRow.doAction(GenerationKeys.SEARCH);
        userRow.getResults(true);

        List<User> users = new ArrayList<User>();
        while (userRow.getNext()) {
            User u = new User();
            u.setId(userRow.getString("UserID"));
            u.setFirstName(userRow.getString("FirstName"));
            u.setLastName(userRow.getString("LastName"));
            u.setUsername(userRow.getString("Username"));
            users.add(u);
        }

        userRow.close();

        return users;
    }

    public List<User> findByTerrtoryId(HttpServletRequest request, String terrId) {
        GenRow terRepRow = new GenRow();
        terRepRow.setRequest(request);
        terRepRow.setTableSpec("TerritorySalesReps");
        terRepRow.setParameter("-select1", "TerritorySalesRepID");
        terRepRow.setParameter("-select2", "TerritoryID");
        terRepRow.setParameter("-select3", "SalesRepID");
        terRepRow.setParameter("TerritoryID", terrId);
        terRepRow.doAction(GenerationKeys.SEARCH);
        terRepRow.getResults(true);

        List<String> salesRepIds = new ArrayList<String>();
        while (terRepRow.getNext()) {
            String id = terRepRow.getString("SalesRepID");
            if (id != null && id.trim().length() != 0) {
                salesRepIds.add(id);
            }            
        }
        terRepRow.close();

        List<User> users = new ArrayList<User>();
        for (String userId : salesRepIds) {
            users.add(findById(request, userId));
        }

        return users;
    }

    public User findById(HttpServletRequest request, String id) {
        GenRow userRow = new GenRow();
        userRow.setTableSpec("Users");
        userRow.setRequest(request);
        userRow.setParameter("UserID", id);
        userRow.setParameter("-select1", "UserID");
        userRow.setParameter("-select2", "FirstName");
        userRow.setParameter("-select3", "LastName");
        userRow.setParameter("-select4", "Username");
        userRow.doAction(GenerationKeys.SELECT);
        userRow.getResults(true);
        User u = null;
        if (userRow.getNext()) {
            u = new User();
            u.setId(userRow.getString("UserID"));
            u.setFirstName(userRow.getString("FirstName"));
            u.setLastName(userRow.getString("LastName"));
            u.setUsername(userRow.getString("Username"));
        }
        userRow.close();        
        return u;
    }

}