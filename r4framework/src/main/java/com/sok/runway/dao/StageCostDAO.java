/*
 * User: Behrang
 * Date: Jul 5, 2010
 * Time: 11:51:23 AM 
 */
package com.sok.runway.dao;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.roi.StageCost;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class StageCostDAO {

    private HttpServletRequest request;

    public StageCostDAO(HttpServletRequest request) {
        this.request = request;
    }

    public List<StageCost> findByRoiItemId(String roiItemId) {

        List<String> stageCostIds = new ArrayList<String>();
        
        GenRow roiItemCostRow = new GenRow();
        roiItemCostRow.setRequest(request);
        roiItemCostRow.setTableSpec("RoiItemCosts");
        roiItemCostRow.setParameter("RoiItemID", roiItemId);
        roiItemCostRow.setParameter("-select1", "StageCostID");
        roiItemCostRow.setAction(GenerationKeys.SEARCH);
        roiItemCostRow.doAction();
        roiItemCostRow.getResults(true);
        while (roiItemCostRow.getNext()) {
            stageCostIds.add(roiItemCostRow.getString("StageCostID"));
        }
        roiItemCostRow.close();

        List<StageCost> stageCosts = new ArrayList<StageCost>();
        for (String stageCostId : stageCostIds) {
            stageCosts.add(findById(stageCostId));
        }

        return stageCosts;
    }

    public StageCost findById(String id) {
        GenRow stageCostRow = new GenRow();
        stageCostRow.setRequest(request);
        stageCostRow.setTableSpec("StageCosts");
        stageCostRow.setParameter("StageCostID", id);
        stageCostRow.setParameter("-select1", "StageCostID");
        stageCostRow.setParameter("-select2", "RoiStageID");
        stageCostRow.setParameter("-select3", "CostLabelID");
        stageCostRow.setParameter("-select4", "UnitCost");
        stageCostRow.setParameter("-select5", "ActualUnitsCountType");
        stageCostRow.setParameter("-select6", "ForecastedUnitsCount");
        stageCostRow.setParameter("-select7", "ActualUnitsCount");
        stageCostRow.setAction(GenerationKeys.SELECT);
        StageCost cost = new StageCost();
        cost.setId(stageCostRow.getString("StageCostID"));
        cost.setReportStageId(stageCostRow.getString("RoiStageID"));
        cost.setCostLabelId(stageCostRow.getString("CostLabelID"));
        cost.setUnitCost(stageCostRow.getDouble("UnitCost"));
        cost.setActualUnitsCountType(stageCostRow.getString("ActualUnitsCountType"));
        cost.setForecastedUnitsCount(stageCostRow.getInt("ForecastedUnitsCount"));
        cost.setActualUnitsCount(stageCostRow.getInt("ActualUnitsCount"));

        return cost;
    }

}
