/*
 * User: Behrang
 * Date: Jul 29, 2010
 * Time: 3:08:05 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.MasterTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class MasterTemplateDAO {

    private HttpServletRequest request;

    public MasterTemplateDAO(HttpServletRequest request) {
        this.request = request;
    }

    public List<MasterTemplate> findAllByType(String type) {
        List<MasterTemplate> result = new ArrayList<MasterTemplate>();

        GenRow mtRow = new GenRow();
        mtRow.setRequest(request);
        mtRow.setTableSpec("MasterTemplates");
        mtRow.setParameter("Type", type);        
        mtRow.setParameter("-select1", "MasterTemplateID");
        mtRow.setParameter("-select2", "Name");
        mtRow.setParameter("-sort1", "Name");
        mtRow.setAction(GenerationKeys.SEARCH);
        mtRow.doAction();
        mtRow.getResults();
        while (mtRow.getNext()) {
            MasterTemplate mt = new MasterTemplate();
            mt.setId(mtRow.getString("MasterTemplateID"));
            mt.setName(mtRow.getString("Name"));
            result.add(mt);
        }
        mtRow.close();

        return result;
    }

}
