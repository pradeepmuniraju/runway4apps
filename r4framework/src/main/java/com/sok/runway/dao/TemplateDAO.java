/*
 * User: Behrang
 * Date: Jul 7, 2010
 * Time: 5:46:40 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.Template;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class TemplateDAO {

    public static final String ALL_CAMPAIGNS = "<All>";
    public static final String NO_CAMPAIGNS = "<No Campaign>";

    private HttpServletRequest request;

    public TemplateDAO(HttpServletRequest request) {
        this.request = request;
    }

    public List<Template> findByMasterTemplateIdAndStatus(String masterTemplateId, String status) {
        List<Template> result = new ArrayList<Template>();

        GenRow tempRow = new GenRow();
        tempRow.setTableSpec("Templates");
        tempRow.setParameter("MasterTemplateID", masterTemplateId);
        tempRow.setParameter("Status", status);
        tempRow.setParameter("-select1", "TemplateID");
        tempRow.setParameter("-select2", "TemplateName");
        tempRow.setParameter("-sort1", "TemplateName");        
        tempRow.setAction(GenerationKeys.SEARCH);
        tempRow.doAction();
        tempRow.getResults();
        while (tempRow.getNext()) {
            Template temp = new Template();
            temp.setId(tempRow.getString("TemplateID"));
            temp.setName(tempRow.getString("TemplateName"));
            result.add(temp);
        }
        tempRow.close();

        return result;
    }

}
