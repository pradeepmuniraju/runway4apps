/*
 * User: Behrang
 * Date: Jun 15, 2010
 * Time: 6:15:12 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.CostCategory;
import com.sok.runway.model.CostLabel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class CostCategoryDAO {

    private HttpServletRequest request;

    public CostCategoryDAO(HttpServletRequest request) {
        this.request = request;
    }

    public List<CostCategory> findAll() {

        List<CostCategory> result = new ArrayList<CostCategory>();

        GenRow ccRow = new GenRow();
        ccRow.setRequest(request);
        ccRow.setTableSpec("CostCategories");
        ccRow.setParameter("-select1", "CostCategoryID");
        ccRow.setParameter("-select2", "Label");
        ccRow.setParameter("-select3", "Description");
        ccRow.setParameter("-sort1", "Label");
        ccRow.doAction(GenerationKeys.SEARCH);
        ccRow.getResults(true);

        while (ccRow.getNext()) {
            CostCategory cc = new CostCategory();
            cc.setId(ccRow.getString("CostCategoryID"));
            cc.setLabel(ccRow.getString("Label"));
            cc.setDescription(ccRow.getString("Description"));
            result.add(cc);
        }

        ccRow.close();

        return result;

    }

    public List<CostCategory> findAllEagerly() {

        List<CostCategory> result = new ArrayList<CostCategory>();

        GenRow ccRow = new GenRow();
        ccRow.setRequest(request);
        ccRow.setTableSpec("CostCategories");
        ccRow.setParameter("-select1", "CostCategoryID");
        ccRow.setParameter("-select2", "Label");
        ccRow.setParameter("-select3", "Description");
        ccRow.setParameter("-sort1", "Label");
        ccRow.doAction(GenerationKeys.SEARCH);
        ccRow.getResults(true);


        CostLabelDAO clDao = new CostLabelDAO(request);
        while (ccRow.getNext()) {
            CostCategory cc = new CostCategory();
            cc.setId(ccRow.getString("CostCategoryID"));
            cc.setLabel(ccRow.getString("Label"));
            cc.setDescription(ccRow.getString("Description"));

            List<CostLabel> labels = clDao.findByCostCategoryId(cc.getId());
            cc.setCostLabels(labels);

            result.add(cc);
        }

        ccRow.close();

        return result;

    }

    public CostCategory findById(String id) {
        GenRow ccRow = new GenRow();
        ccRow.setRequest(request);
        ccRow.setTableSpec("CostCategories");
        ccRow.setParameter("CostCategoryID", id);
        ccRow.setParameter("-select1", "CostCategoryID");
        ccRow.setParameter("-select2", "Label");
        ccRow.setParameter("-select3", "Description");
        ccRow.doAction(GenerationKeys.SELECT);
        CostCategory cc = null;
        if (ccRow.isSuccessful()) {
            cc = new CostCategory();
            cc.setId(ccRow.getString("CostCategoryID"));
            cc.setLabel(ccRow.getString("Label"));
            cc.setDescription(ccRow.getString("Description"));
        }
        ccRow.close();
        return cc;        
    }

    public boolean deleteById(String id) {
        GenRow ccRow = new GenRow();
        ccRow.setRequest(request);
        ccRow.setTableSpec("CostCategories");
        ccRow.setParameter("CostCategoryID", id);
        ccRow.doAction(GenerationKeys.DELETE);
        boolean success = ccRow.isSuccessful();
        ccRow.close();
        return success;
    }

}
