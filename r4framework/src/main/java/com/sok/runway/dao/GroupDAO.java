/*
 * User: Behrang
 * Date: Jun 9, 2010
 * Time: 5:03:35 PM 
 */
package com.sok.runway.dao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.model.Group;

public class GroupDAO {
    
    private HttpServletRequest request;
    
    public GroupDAO(HttpServletRequest request) {
        this.request = request;
    }

    public List<Group> findAll() {

        GenRow groupRow = new GenRow();
        groupRow.setTableSpec("Groups");
        groupRow.setRequest(this.request);
        groupRow.setParameter("-select1", "GroupID");
        groupRow.setParameter("-select2", "Name");
        groupRow.sortBy("Name",0);
        groupRow.doAction(GenerationKeys.SEARCH);
        groupRow.getResults(true);

        List<Group> groups = new ArrayList<Group>();
        populateGroups(groupRow, groups);
        
        groupRow.close();

        return groups;
    }

    public Group findById(String id) {
        GenRow groupRow = new GenRow();
        groupRow.setTableSpec("Groups");
        groupRow.setRequest(this.request);
        groupRow.setParameter("GroupID", id);
        groupRow.setParameter("-select1", "GroupID");
        groupRow.setParameter("-select2", "Name");
        groupRow.doAction(GenerationKeys.SELECT);
        groupRow.getResults(true);
        Group g = null;
        if (groupRow.getNext()) {
            g = new Group();
            populateGroup(groupRow, g);
        }
        groupRow.close();
        return g;
    }
    
    public List<Group> findAllByUserID(String userID) {
       GenRow groupRow = new GenRow();
       groupRow.setTableSpec("Groups");
       groupRow.setRequest(this.request);
       groupRow.setParameter("-join1", "GroupUsers");
       groupRow.setParameter("-select1", "GroupID");
       groupRow.setParameter("-select2", "Name");
       groupRow.setParameter("-select3", "GroupUsers.UserID");
       groupRow.sortBy("Name",0);
       groupRow.doAction(GenerationKeys.SEARCH);
       groupRow.getResults(true);

       List<Group> groups = new ArrayList<Group>();
       populateGroups(groupRow, groups);

       groupRow.close();

       return groups;
    }

    private void populateGroups(GenRow groupRow, List<Group> groups) {
        while (groupRow.getNext()) {
            Group g = new Group();
            populateGroup(groupRow, g);
            groups.add(g);
        }
    }
    
    private void populateGroup(GenRow groupRow, Group g) {
        g.setId(groupRow.getString("GroupID"));
        g.setName(groupRow.getString("Name"));        
    }
}
