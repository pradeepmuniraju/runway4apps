package com.sok.runway.dao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.Profile;
import com.sok.runway.model.ProfileQuestion;

public class ProfileQuestionDAO {
    public static List<ProfileQuestion> findAll(HttpServletRequest request) {
        GenRow profileQuestionRow = new GenRow();
        profileQuestionRow.setTableSpec("Questions");
        profileQuestionRow.setRequest(request);
        profileQuestionRow.setParameter("-select1", "QuestionID");
        profileQuestionRow.setParameter("-select2", "Label");
        profileQuestionRow.setParameter("-select3", "InputType");
        profileQuestionRow.setParameter("-select4", "ValueList");
        profileQuestionRow.setParameter("-select5", "GroupID");
        profileQuestionRow.setParameter("-select6", "ProfileType");
        profileQuestionRow.sortBy("Label", 0);
        profileQuestionRow.doAction(GenerationKeys.SEARCH);
        profileQuestionRow.getResults(true);

        List<ProfileQuestion> profileQuestions = new ArrayList<ProfileQuestion>();
        populateProfileQuestions(profileQuestionRow, profileQuestions);
        profileQuestionRow.close();

        return profileQuestions;
    }
    
    public static ProfileQuestion findById(HttpServletRequest request, String id) {
        GenRow profileQuestionRow = new GenRow();
        profileQuestionRow.setTableSpec("Questions");
        profileQuestionRow.setRequest(request);
        profileQuestionRow.setParameter("QuestionID", id);
        profileQuestionRow.setParameter("-select1", "Label");
        profileQuestionRow.setParameter("-select2", "InputType");
        profileQuestionRow.setParameter("-select3", "ValueList");
        profileQuestionRow.setParameter("-select4", "GroupID");
        profileQuestionRow.setParameter("-select5", "ProfileType");
        profileQuestionRow.setParameter("-select6", "DefaultGraph");
        
        profileQuestionRow.doAction(GenerationKeys.SELECT);
        profileQuestionRow.getResults(true);
        ProfileQuestion profileQuestion = null;
        if (profileQuestionRow.getNext()) {
            profileQuestion = new ProfileQuestion();
            populateProfileQuestion(profileQuestionRow, profileQuestion);
        }
        profileQuestionRow.close();
        return profileQuestion;
    }
    
    public static List<ProfileQuestion> findAllByProfileID(HttpServletRequest request, String profileID) {
        GenRow profileQuestionRow = new GenRow();
        profileQuestionRow.setTableSpec("Questions");
        profileQuestionRow.setRequest(request);
        profileQuestionRow.setParameter("-select1", "QuestionID");
        profileQuestionRow.setParameter("-select2", "Label");
        profileQuestionRow.setParameter("-select3", "InputType");
        profileQuestionRow.setParameter("-select4", "ValueList");
        profileQuestionRow.setParameter("-select5", "GroupID");
        profileQuestionRow.setParameter("-select6", "ProfileType");
        profileQuestionRow.setParameter("-join1", "QuestionSurveyQuestion");
        profileQuestionRow.setParameter("QuestionSurveyQuestion.SurveyID", profileID);
        profileQuestionRow.sortBy("Label", 0);
        profileQuestionRow.doAction(GenerationKeys.SEARCH);
        profileQuestionRow.getResults(true);

        List<ProfileQuestion> profileQuestions = new ArrayList<ProfileQuestion>();
        populateProfileQuestions(profileQuestionRow, profileQuestions);
        profileQuestionRow.close();

        return profileQuestions;
    }
    
    private static void populateProfileQuestions(GenRow profileQuestionRow, List<ProfileQuestion> profileQuestions) {
        while (profileQuestionRow.getNext()) {
            ProfileQuestion profileQuestion = new ProfileQuestion();
            populateProfileQuestion(profileQuestionRow, profileQuestion);
            profileQuestions.add(profileQuestion);
        }
    }
    
    private static void populateProfileQuestion(GenRow profileRow, ProfileQuestion profileQuestion) {
        profileQuestion.setId(profileRow.getString("QuestionID"));
        profileQuestion.setLabel(profileRow.getString("Label"));
        profileQuestion.setInputType(profileRow.getString("InputType"));
        profileQuestion.setGroupID(profileRow.getString("GroupID"));
        profileQuestion.setProfileType(profileRow.getString("ProfileType"));
        profileQuestion.setDefaultGraph(profileRow.getString("DefaultGraph"));
    }
}
