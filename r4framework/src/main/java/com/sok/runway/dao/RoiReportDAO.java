/*
 * User: Behrang
 * Date: Jun 30, 2010
 * Time: 6:27:55 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.Bean;
import com.sok.runway.model.roi.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class RoiReportDAO {

    HttpServletRequest request;

    public RoiReportDAO(HttpServletRequest request) {
        this.request = request;
    }

    public void save(RoiReport roiReport, Map<String, StageCost> coordCostMap) {

        GenRow roiReportRow = new GenRow();
        roiReportRow.setRequest(request);
        roiReportRow.setTableSpec("RoiReports");
        roiReportRow.createNewID();
        roiReportRow.setParameter("CampaignID", roiReport.getCampaignId());
        roiReportRow.setParameter("Name", roiReport.getName());
        roiReportRow.doAction(ActionBean.INSERT);
        roiReport.setId(roiReportRow.getString("RoiReportID"));

        for (ReportStage stage: roiReport.getStages()) {
            saveStage(roiReport, stage);
        }

        for (ReportItem ri : roiReport.getReportItems()) {
            saveReportItem(roiReport, ri, coordCostMap);
        }

    }

    private void saveReportItem(RoiReport report, ReportItem reportItem, Map<String, StageCost> coordCostMap) {
        GenRow itemRow = new GenRow();
        itemRow.setRequest(request);
        itemRow.setTableSpec("RoiItems");
        itemRow.createNewID();
        itemRow.setParameter("RoiReportID", report.getId());
        itemRow.setParameter("CountBeanID", reportItem.getCountBeanId());
        itemRow.setParameter("Name", reportItem.getName());
        itemRow.setParameter("ItemCount", reportItem.getCount());
        itemRow.setParameter("DisplayIndex", reportItem.getDisplayIndex());
        itemRow.setAction(ActionBean.INSERT);
        itemRow.doAction();
        reportItem.setId(itemRow.getString("RoiItemID"));

        for (String costCoord : reportItem.getReportItemCostCoords()) {
            StageCost sc = coordCostMap.get(costCoord);
            GenRow costRow = new GenRow();
            costRow.setTableSpec("RoiItemCosts");
            costRow.createNewID();
            costRow.setParameter("RoiItemID", reportItem.getId());
            costRow.setParameter("StageCostID", sc.getId());
            costRow.setAction(ActionBean.INSERT);
            costRow.doAction();            
        }
    }

    private void saveStage(RoiReport report, ReportStage stage) {
        GenRow stageRow = new GenRow();
        stageRow.setRequest(request);
        stageRow.setTableSpec("RoiStages");
        stageRow.createNewID();
        stageRow.setParameter("RoiReportID", report.getId());
        stageRow.setParameter("EventID", stage.getEventId());
        stageRow.setParameter("CountMode", stage.getStageCountMode());
        stageRow.setParameter("DisplayIndex", stage.getDisplayIndex());
        stageRow.doAction(ActionBean.INSERT);
        stage.setId(stageRow.getParameter("RoiStageID"));
        stage.setRoiReportId(report.getId());

        for (StageCost cost : stage.getCosts()) {
            saveStageCost(stage, cost);
        }
    }

    private void saveStageCost(ReportStage stage, StageCost cost) {
        GenRow costRow = new GenRow();
        costRow.setRequest(request);
        costRow.setTableSpec("StageCosts");
        costRow.createNewID();
        costRow.setParameter("RoiStageID", stage.getId());
        costRow.setParameter("CostLabelID", cost.getCostLabelId());
        costRow.setParameter("UnitCost", cost.getUnitCost());
        costRow.setParameter("ForecastedUnitsCount", cost.getForecastedUnitsCount());
        costRow.setParameter("ActualUnitsCount", cost.getActualUnitsCount());
        costRow.setParameter("ActualUnitsCountType", cost.getActualUnitsCountType());
        costRow.setParameter("DisplayIndex", cost.getDisplayIndex());
        costRow.doAction(ActionBean.INSERT);
        cost.setId(costRow.getString("StageCostID"));
    }

    public RoiReport loadRoiReport(String id) {
        RoiReport report = new RoiReport();

        GenRow roiRow = new GenRow();
        roiRow.setRequest(request);
        roiRow.setTableSpec("RoiReports");
        roiRow.setParameter("RoiReportID", id);
        roiRow.setParameter("-select1", "RoiReportID");
        roiRow.setParameter("-select2", "CampaignID");
        roiRow.setParameter("-select3", "Name");
        roiRow.setAction(GenerationKeys.SELECT);
        roiRow.doAction();

        report.setName(roiRow.getString("Name"));
        report.setId(roiRow.getString("RoiReportID"));
        report.setCampaignId(roiRow.getString("CampaignID"));

        loadStages(report);
        loadItems(report);

        return report;
    }

    private void loadItems(RoiReport report) {
        GenRow itemRow = new GenRow();
        itemRow.setRequest(request);
        itemRow.setTableSpec("RoiItems");
        itemRow.setParameter("RoiReportID", report.getId());
        itemRow.setParameter("-select1", "RoiItemID");
        itemRow.setParameter("-select2", "RoiReportID");
        itemRow.setParameter("-select3", "Name");
        itemRow.setParameter("-select4", "ItemCount");
        itemRow.setParameter("-select5", "CountBeanID");
        itemRow.setParameter("-select6", "DisplayIndex");
        itemRow.setParameter("-sort1", "DisplayIndex");
        itemRow.setAction(ActionBean.SEARCH);
        itemRow.doAction();
        itemRow.getResults(true);
        while (itemRow.getNext()) {
            ReportItem item = new ReportItem();
            item.setId(itemRow.getString("RoiItemID"));
            item.setRoiReportId(report.getId());
            item.setCount(itemRow.getInt("ItemCount"));
            item.setName(itemRow.getString("Name"));
            item.setCountBeanId(itemRow.getString("CountBeanID"));
            item.setDisplayIndex(itemRow.getInt("DisplayIndex"));
            loadItemCosts(item);
            loadItemCount(item);
            report.addRoiItem(item);
        }

        itemRow.close();
    }

    private void loadItemCount(ReportItem item) {
        BeanDAO beanDao = new BeanDAO(request);
        Bean bean = beanDao.findById(item.getCountBeanId());
        Integer count = null;
        GenRow countRow = new GenRow();
        if ("Companies".equals(bean.getType())) {
            countRow.setViewSpec("CompanyListView");
        } else if ("Contacts".equals(bean.getType())) {
            countRow.setViewSpec("ContactListView");
        }
        countRow.setRequest(request);
        countRow.parseRequest(bean.getBean());
        countRow.doAction(GenerationKeys.SEARCH);
        countRow.getResults(true);
        count = countRow.getSize();
        countRow.close();
        item.setCount(count);
    }

    private void loadItemCosts(ReportItem item) {        
        GenRow costRow = new GenRow();
        costRow.setTableSpec("RoiItemCosts");
        costRow.setParameter("RoiItemID", item.getId());
        costRow.setParameter("-select1", "RoiItemCostID");
        costRow.setParameter("-select2", "RoiItemID");
        costRow.setParameter("-select3", "StageCostID");
        costRow.setAction(ActionBean.SEARCH);
        costRow.doAction();
        costRow.getResults(true);
        while (costRow.getNext()) {
            item.addReportItemCostId(costRow.getString("RoiItemCostID"));
            RoiItemCost itemCost = new RoiItemCost();
            itemCost.setId(costRow.getString("RoiItemCostID"));
            itemCost.setReportItemId(costRow.getString("RoiItemID"));
            itemCost.setStageCostId(costRow.getString("StageCostID"));
            item.addReportItemCost(itemCost);            
        }
        costRow.close();
    }

    private void loadStages(RoiReport report) {
        GenRow stageRow = new GenRow();
        stageRow.setRequest(request);
        stageRow.setTableSpec("RoiStages");
        stageRow.setParameter("RoiReportID", report.getId());
        stageRow.setParameter("-select1", "RoiStageID");
        stageRow.setParameter("-select2", "RoiReportID");
        stageRow.setParameter("-select3", "EventID");
        stageRow.setParameter("-select4", "CountMode");
        stageRow.setParameter("-select5", "DisplayIndex");
        stageRow.setParameter("-sort1", "DisplayIndex");
        stageRow.setAction(GenerationKeys.SEARCH);
        stageRow.doAction();
        stageRow.getResults(true);
        while (stageRow.getNext()) {
            ReportStage stage = new ReportStage();
            stage.setId(stageRow.getString("RoiStageID"));
            stage.setRoiReportId(stageRow.getString("RoiReportID"));
            stage.setEventId(stageRow.getString("EventID"));
            stage.setStageCountMode(stageRow.getString("CountMode"));
            stage.setDisplayIndex(stageRow.getInt("DisplayIndex"));
            loadStageCosts(stage);
            report.addStage(stage);
        }
        stageRow.close();
    }

    private void loadStageCosts(ReportStage stage) {
        GenRow costRow = new GenRow();
        costRow.setRequest(request);
        costRow.setTableSpec("StageCosts");
        costRow.setParameter("RoiStageID", stage.getId());
        costRow.setParameter("-select1", "StageCostID");
        costRow.setParameter("-select2", "RoiStageID");
        costRow.setParameter("-select3", "CostLabelID");
        costRow.setParameter("-select4", "UnitCost");
        costRow.setParameter("-select5", "ForecastedUnitsCount");
        costRow.setParameter("-select6", "ActualUnitsCount");
        costRow.setParameter("-select7", "ActualUnitsCountType");
        costRow.setParameter("-select8", "DisplayIndex");
        costRow.setParameter("-sort1", "DisplayIndex");
        costRow.setAction(GenerationKeys.SEARCH);
        costRow.doAction();
        costRow.getResults(true);
        while (costRow.getNext()) {
            StageCost cost = new StageCost();
            cost.setActualUnitsCount(costRow.getInt("ActualUnitsCount"));
            cost.setActualUnitsCountType(costRow.getString("ActualUnitsCountType"));
            cost.setForecastedUnitsCount(costRow.getInt("ForecastedUnitsCount"));
            cost.setUnitCost(costRow.getDouble("UnitCost"));
            cost.setCostLabelId(costRow.getString("CostLabelID"));
            cost.setReportStageId(costRow.getString("RoiStageID"));
            cost.setId(costRow.getString("StageCostID"));
            cost.setDisplayIndex(costRow.getInt("DisplayIndex"));
            stage.addCost(cost);
        }
        costRow.close();
    }

}
