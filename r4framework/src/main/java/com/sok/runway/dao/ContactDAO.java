/*
 * User: Behrang
 * Date: Jul 13, 2010
 * Time: 1:15:23 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.model.Contact;

import javax.servlet.http.HttpServletRequest;

public class ContactDAO {

    private HttpServletRequest request;

    public ContactDAO(HttpServletRequest request) {
        this.request = request;
    }

    public Contact findById(String id) {
        GenRow contactRow = new GenRow();
        contactRow.setRequest(request);
        contactRow.setTableSpec("Contacts");
        contactRow.setParameter("ContactID", id);
        contactRow.setParameter("-select1", "ContactID");
        contactRow.setParameter("-select2", "FirstName");
        contactRow.setParameter("-select3", "LastName");
        contactRow.setParameter("-select4", "Email");
        contactRow.setParameter("-select5", "GroupID");
        contactRow.doAction(GenerationKeys.SELECT);

        if (contactRow.isSuccessful()) {
            Contact contact = new Contact();
            contact.setId(contactRow.getString("ContactID"));
            contact.setFirstName(contactRow.getString("FirstName"));
            contact.setLastName(contactRow.getString("LastName"));
            contact.setEmail(contactRow.getString("Email"));
            contact.setGroupId(contactRow.getString("GroupID"));
            return contact;
        }

        return null;
    }

}
