/*
 * User: Behrang
 * Date: Jul 15, 2010
 * Time: 5:02:53 PM 
 */
package com.sok.runway.dao;

import com.sok.framework.GenRow;
import com.sok.runway.model.Order;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class OrderDAO {

    private HttpServletRequest request;

    public OrderDAO(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Returns a list of all orders that belong to the campaign with the given <code>campaignId</code>.
     *
     * @param campaignId A non-null campaignId
     * @return list of all orders that belong to the campaign with the given <code>campaignId</code>
     */
    public List<Order> findByCampaignId(String campaignId) {

        if (campaignId == null) {
            throw new IllegalArgumentException("campaignId must not be null");
        }

        List<Order> result = new ArrayList<Order>();

        GenRow orderRow = new GenRow();
        orderRow.setRequest(request);
        orderRow.setTableSpec("Orders");
        orderRow.setParameter("CampaignID", campaignId);
        setSelectedColumns(orderRow);
        orderRow.getResults();
        while (orderRow.getNext()) {
            result.add(fetchOrder(orderRow));
        }
        orderRow.close();

        return result;
    }

    private void setSelectedColumns(GenRow orderRow) {
        orderRow.setParameter("-select1", "OrderID");
        orderRow.setParameter("-select2", "Cost");
        orderRow.setParameter("-select5", "TotalCost");
        orderRow.setParameter("-select3", "GST");
        orderRow.setParameter("-select4", "Quantity");
        orderRow.setParameter("-select6", "Discount");
        orderRow.setParameter("-select7", "DiscountAmount");
    }

    private Order fetchOrder(GenRow orderRow) {
        Order order = new Order();
        order.setId(orderRow.getString("OrderID"));
        order.setCost(orderRow.getDouble("Cost"));
        order.setTotalCost(orderRow.getDouble("TotalCost"));
        order.setGst(orderRow.getDouble("GST"));
        order.setQuantity(orderRow.getDouble("Quantity"));
        order.setDiscount(orderRow.getDouble("Discount"));
        order.setDiscountAmount(orderRow.getDouble("DiscountAmount"));
        return order;
    }

}
