package com.sok.runway;

import javax.servlet.*;
import javax.servlet.http.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import com.sok.framework.*;
import com.sok.runway.security.User;

public class ICSServlet extends HttpServlet
{

	public final String defaultEmail = InitServlet.getSystemParam("DefaultEmail");
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		doGet(req, res);
	}

	String makeStmt(HttpServletRequest request)
	{
		String noteid = request.getParameter("NoteID");
		TableBean notebean = null;
		if(noteid == null)
		{
			HttpSession session = request.getSession();
			notebean = (TableBean)session.getAttribute(SessionKeys.NOTE_SEARCH);
			notebean.setViewSpec("AppointmentView");
			notebean.generateSQLStatment();

		}
		else
		{
			notebean = new TableBean();
			notebean.setViewSpec("AppointmentView");
			notebean.setColumn("NoteID",noteid);
			notebean.generateSQLStatment();
		}

		return(notebean.getSearchStatement());
	}

   public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
		response.setContentType("text/calendar");
		PrintWriter out = response.getWriter();
		out.println("BEGIN:VCALENDAR");
		out.println("PRODID:-//Switched on Group//Runway//EN");
		out.println("VERSION:2.0");
		
		User cu = null;
		
		try{
		   cu = User.getCurrentUser(request); //may be null if using coming from Appointment Invitation
		}catch(Exception e){}
		
		if(cu == null || User.OUTLOOK_2007 != cu.getInt("EmailClient")) { 
			out.println("METHOD:REQUEST");
		}else{
			out.println("METHOD:PUBLISH");
		}

		String stmtstring = makeStmt(request);

		String dbconn = getServletContext().getInitParameter("sqlJndiName");
		Connection con = null;
		Context ctx = null;
		ResultSet current = null;
		try
		{
			ctx = new InitialContext();

				DataSource ds = null;
				try{
					Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource)envContext.lookup(dbconn);
				}catch(Exception e){}
				if(ds == null)
				{
					ds = (DataSource)ctx.lookup(dbconn);
				}
			con = ds.getConnection();
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					     ResultSet.CONCUR_UPDATABLE);
			current = stmt.executeQuery(stmtstring);
			while(current.next())
			{
				writeAppointment(request, out, current);
			}
 
		}
		catch(SQLException sqlex)
		{
			throw(new ServletException(sqlex));
		}
		catch(NamingException nex)
		{
			throw(new ServletException(nex));
		}
		finally
		{
    		if (current != null) 
    		{
				try{
            	current.close();
        		}catch (Exception ex){}
    		}
    		if (con != null) 
    		{
				try{
            	con.close();
        		}catch (Exception ex){}
    		}
    		if (ctx != null) 
    		{
        		try{       
            	ctx.close();
        		}catch (Exception ex){}
    		}
		}
		out.println("END:VCALENDAR");  
	}

	int getHalfHourBlocks(String dur)
	{
		int blocks = 0;
		for(;blocks<UserCalendar.duration.length; blocks++)
		{
			if(dur.equals(UserCalendar.duration[blocks]))
			{
				return(blocks++);
			}
		}
		return(blocks);
	}

	void addDuration(GregorianCalendar cal, String dur)
	{
		int minutes = (getHalfHourBlocks(dur) * 30)+30;
		cal.add(Calendar.MINUTE, minutes);
	}

	public static void main(String[] args) { 
		
		GregorianCalendar cal = new GregorianCalendar();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhhmmssZ"); 
		System.out.println(df.format(cal.getTime()));  
		
	}
	
	
	String formatDate(GregorianCalendar cal)
	{	//20020516T210000Z yyyyMMddThhmmssZ
		StringBuffer date = new StringBuffer();
		date.append(cal.get(Calendar.YEAR));
		String temp = String.valueOf(cal.get(Calendar.MONTH)+1);
		if(temp.length()==1){date.append("0");}
		date.append(temp);
		temp = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
		if(temp.length()==1){date.append("0");}
		date.append(temp);
		date.append("T");
		temp = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
		if(temp.length()==1){date.append("0");}
		date.append(temp);
		temp = String.valueOf(cal.get(Calendar.MINUTE));
		if(temp.length()==1){date.append("0");}
		date.append(temp);
		date.append("00");
		return(date.toString());
	}


	String[] formatDates(java.sql.Date notedate, java.sql.Time notetime, String duration)
	{
		String[] dates = new String[3];
		GregorianCalendar appointmentdate = new GregorianCalendar();
		GregorianCalendar appointmenttime = new GregorianCalendar();
		appointmentdate.setTime(notedate);
		appointmenttime.setTime(notetime);
		appointmentdate.set(Calendar.HOUR_OF_DAY, appointmenttime.get(Calendar.HOUR_OF_DAY));
		appointmentdate.set(Calendar.MINUTE, appointmenttime.get(Calendar.MINUTE));

		dates[0] = formatDate(appointmentdate);
		addDuration(appointmentdate, duration);
		dates[1] = formatDate(appointmentdate);
		appointmentdate.add(Calendar.DAY_OF_MONTH, 1);
		dates[2] = formatDate(appointmentdate);

		return(dates);
	}

	void writeAppointment(HttpServletRequest request, PrintWriter out, ResultSet rs) throws ServletException, IOException, SQLException
	{
		String body = nullToBlank(rs.getString("Body"));
		body = body.replaceAll("\\r\\n", "=0D=0A");
		String noteid = rs.getString("NoteID");

		java.sql.Date notedate = rs.getDate("NoteDate");
		java.sql.Time notetime = rs.getTime("NoteTime");
		String duration = rs.getString("Duration");
		
		String venue = rs.getString("Venue");
		if (venue == null || venue.length()==0){
			venue = "Not specified";
		}

		String[] dates = formatDates(notedate, notetime, duration);

		out.println("BEGIN:VEVENT");
		out.print("DTSTART:");
		out.println(dates[0]);//start time in 20020516T210000Z yyyyMMddThhmmssZ
		out.print("DTEND:");
		out.println(dates[1]);//end time in 20020516T210000Z yyyyMMddThhmmssZ
		out.print("LOCATION:");
		out.println(venue);//url to appointment
		out.println("TRANSP:OPAQUE");
		out.println("SEQUENCE:0");
		out.print("UID:");
		out.print(noteid);//noteid
		out.println("@switched-on.com.au");
/*
		out.print("ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=FALSE;CN=\"");
		out.print(nullToBlank(rs.getString("FirstName")));
		out.print(" ");
		out.print(nullToBlank(rs.getString("LastName")));
      	out.print("\":MAILTO:");
		out.println(nullToBlank(rs.getString("Email")));
*/
		String email = defaultEmail != null? defaultEmail : "info@switched-on.com.au";
		
		out.print("ORGANIZER;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=FALSE;CN=\"");
		out.print("NOREPLY");// organizer firstname
		//out.print(nullToBlank(rs.getString("RepFirstName")));// organizer firstname
		//out.print(" ");
		//out.print(nullToBlank(rs.getString("RepLastName")));// organizer secondname
		out.print("\":MAILTO:");
		out.print(email);// organizer firstname
		//out.println(nullToBlank(rs.getString("RepEmail")));// email address


		out.print("DTSTAMP:");
		out.println(dates[2]); //20020528T074800Z  ???
		out.print("DESCRIPTION;ENCODING=QUOTED-PRINTABLE:");
		out.print("Name: ");
		out.print(nullToBlank(rs.getString("Title")));
		out.print(" ");
		out.print(nullToBlank(rs.getString("FirstName")));
		out.print(" ");
		out.print(nullToBlank(rs.getString("LastName")));
		out.println(" =0D=0A=");
		out.print("Position: ");
		out.print(nullToBlank(rs.getString("Position")));
		out.println(" =0D=0A=");
		out.print("Company: ");
		out.print(nullToBlank(rs.getString("Company")));
		out.println(" =0D=0A=");
		out.print("Phone: ");
		out.print(nullToBlank(rs.getString("Phone")));
		out.println(" =0D=0A=");
		out.print("Mobile: ");
		out.print(nullToBlank(rs.getString("Mobile")));
		out.println(" =0D=0A=");
		out.print("Source: ");
		out.print(getServletContext().getInitParameter("URLHome"));
		out.print("/crm/noteview.jsp?-action=select&NoteID=3D");
		out.print(noteid);
		out.println(" =0D=0A=");
		out.println(" =0D=0A=");
		out.println(body); //note body replace \r\n with =0D=0A
		out.print("SUMMARY:");
		out.println(nullToBlank(rs.getString("Subject"))); //subject
		out.println("PRIORITY:5");
		out.println("CLASS:PUBLIC");
		out.println("STATUS:CONFIRMED");
		out.println("BEGIN:VALARM");
		out.println("TRIGGER:PT15M");
		out.println("ACTION:DISPLAY");
		out.println("DESCRIPTION:Reminder");
		out.println("END:VALARM"); 
		out.println("END:VEVENT"); 

	}

	String nullToBlank(String value)
	{
		if(value!=null)
		{
			return(value);
		}
		return("");
	}
}
