package com.sok.runway;

import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.runway.payment.*;

import java.text.*;
import java.util.*;
import java.util.logging.*;

import javax.servlet.http.*;
import javax.servlet.*;

public class PaymentBean
{
    public static final int PAYMENT = 0;
    public static final int CAPTURE = 1;    
    public static final int REFUND = 2;
    public static final int QUERY = 3;
    
    public static final int MERCHANT_HOSTED = 0;
    public static final int SERVER_HOSTED = 1;
    
    protected int type = PAYMENT;
    protected int mode = MERCHANT_HOSTED;
    protected String server = null;
    protected String merchantid = null;
    protected String user = null;
    protected String password = null;
    protected String accesscode = null;
    protected String cardname = null;
    protected String cardnumber = null;
    protected String cardtype = null;
    protected String cardexpirymonth = null;
    protected String cardexpiryyear = null;    
    protected String ccardsecurtycode = null;
    protected String amount = null;
    protected String ipaddress = null;
    protected String orderid = null;  
    protected String responsecode = null;
    protected String responsemsg = null;
    protected String transactionno = null;
    protected String receiptno = null;  
    protected String securesecret = null;  
    protected String returnurl = null;  
    protected String failedreturnurl = null;  
    protected String paymentid = null;
    
    public static final String migs = "MIGS";
    public static final String nsips = "NSIPS";
    protected PaymentProvider paymentprovider = null;
    
    protected boolean test = false;
    protected boolean debug = false;
    protected boolean usepaymentrecords = true;
    
    protected RowBean payment = null;
    //protected RowBean order = null;  
    protected ServletContext context = null;
    
    protected static Logger logger = null;
    
    Date date = new Date();
    DateFormat dateformat = DateFormat.getDateInstance();
    
    public static final String PAYMENT_PROVIDER = "PaymentProvider";
    public static final String PAYMENT_SERVER = "PaymentServer";
    public static final String PAYMENT_MERCHANTID = "PaymentMerchantID";
    public static final String PAYMENT_ACCESSCODE = "PaymentAccessCode";    
    public static final String PAYMENT_USER = "PaymentUser";
    public static final String PAYMENT_PASSWORD = "PaymentPassword";
    public static final String PAYMENT_SECURE_SECRET = "PaymentSecureSecret";
    
    public PaymentBean()
    {
        if(logger==null)
        {
            initLogger();
        }
    }    
    
    protected synchronized void initLogger()
    {
        if(logger==null)
        {
            try{
                String pattern = "runwaypayment%g.log";
                int limit = 50000000; // 50 Mb
                int numLogFiles = 10;
                FileHandler fh = new FileHandler(pattern, limit, numLogFiles);
                logger = Logger.getLogger("com.sok.runway.payment");
                logger.addHandler(fh);
            }catch(Exception e){
                
            }
        }
    }
    
    
    //use the runway payments table
    public void setUsePayments(boolean b)
    {
        usepaymentrecords = b;
    }
    
    public boolean getUsePayments()
    {
        return(usepaymentrecords);
    }    
    
    //tell the payment provider that this is a test transaction
    public void setTest(boolean b)
    {
        test = b;
    }
    
    public boolean isTest()
    {
        return(test);
    }
    
    //not used
    public void setDebug(boolean b)
    {
        debug = b;
    }
    
    public boolean isDebug()
    {
        return(debug);
    }
    
    public void setPaymentProvider(String name)
    {
        if(name!=null)
        {
            if(name.equals(migs))
            {
                paymentprovider = new MIGS(this);
            }        
            else if(name.equals(nsips))
            {
                paymentprovider = new NSIPS(this);
            }              
        }
    }
    
    public void setServerURL(String address)
    {
        server = address;
    }
    
    public String getServerURL()
    {
        return(server);
    }
    
    public void setClientIP(String address)
    {
        ipaddress = address;
    }
    
    public String getClientIP()
    {
        return(ipaddress);
    }    
    
    public void setReturnURL(String address)
    {
    	returnurl = address;
    }
    
    public String getReturnURL()
    {
        return(returnurl);
    }    
    
    public void setFailedURL(String address)
    {
    	failedreturnurl = address;
    }
    
    public String getFailedURL()
    {
        return(failedreturnurl);
    }       
    
    public void setMerchantID(String id)
    {
        merchantid = id;
    }
    
    public String getMerchantID()
    {
        return(merchantid);
    }    
    
    public void setMerchantAccessCode(String code)
    {
        accesscode = code;
    }
    
    public String getMerchantAccessCode()
    {
        return(accesscode);
    }    
    
    public void setSecureSecret(String code)
    {
        securesecret = code;
    }
    
    public String getSecureSecret()
    {
        return(securesecret);
    }        
    
    // User and Password required for Refund and Capture transactions.
    public void setUser(String id)
    {
        user = id;
    }
    
    public String getUser()
    {
        return(user);
    }  
    
    public void setPassword(String id)
    {
        password = id;
    }
    
    public String getPassword()
    {
        return(password);
    }      
    
    //Type of transaction
    public void setType(int code)
    {
        type = code;
    }
    
    public int getType()
    {
        return(type);
    }    
    
    public void setMode(int code)
    {
        mode = code;
    }
    
    public int getMode()
    {
        return(mode);
    }        
    
    /* This field is used to bypass the card details page on the Payment Server. It is the number of the
    card to be used for processing the payment. It can only be a long integer value with no white
    space or formatting characters. */
    public void setCardNumber(String number)
    {
        cardnumber = number;
    }
    
    public String getCardNumber()
    {
        return(cardnumber);
    }    
    
    //name on card
    public void setCardName(String name)
    {
        cardname = name;
    }
    
    public String getCardName()
    {
        return(cardname);
    }     
    
    public void setCardType(String type)
    {
        cardtype = type;
    }
    
    public String getCardType()
    {
        return(cardtype);
    }       
    

    public void setCardExpiry(String number)
    {
        int slashindex = number.indexOf('/');
        int strlength = number.length();
        
        if(strlength==4 && slashindex<0) //yymm
        {
            cardexpiryyear = number.substring(0,2);
            cardexpirymonth = number.substring(2,4);
        }
        else if(strlength==5 && slashindex==2) //mm/yy
        {
            cardexpiryyear = number.substring(3,5);
            cardexpirymonth = number.substring(0,2); 
        }
        else
        {
            String msg = "Unexpected Expiry format "+number;
            logError(msg);
            throw new IllegalConfigurationException(msg);   
        }
    }

    public void setCardExpiryYear(String year)
    {
        cardexpiryyear = year;
    }    
    
    public void setCardExpiryMonth(String month)
    {
        cardexpirymonth = month;
    } 

    
    /* The expiry date of the card to be processed for payment. The format for this is YYMM, for
    example, for an expiry date of May 2009, the value would be 0905. The value must be expressed
    as a 4-digit number (integer) with no white space or formatting characters */    
    public String getIntegerCardExpiry()
    {
        return(cardexpiryyear + cardexpirymonth);
    }    
    
    public String getCardExpiry()
    {
        return(new StringBuffer().append(cardexpirymonth).append("/").append(cardexpiryyear).toString());
    }       
    
    /* The Card Security Code (CSC) is a security feature used for card not present transactions that
    compares the Card Security Code on the card with the records held in the card issuer's database.
    For example, on Visa and MasterCard credit cards, it is the three digit value printed on the
    signature panel on the back following the credit card account number. For American Express,
    the number is the 4 digit value printed on the front above the credit card account number. Once
    the transaction is successfully processed and authorised, the card issuer returns a result code
    (CSC result code) in its authorisation response message verifying the CSC level
    (vpc_CSCLevel) of accuracy used to match the card security code. */
    public void setCardSecurtyCode(String code)
    {
        ccardsecurtycode = code;
    }
    
    public String getCardSecurtyCode()
    {
        return(ccardsecurtycode);
    }    
    
    public void setAmount(String number)
    {
        int dotindex = number.indexOf('.');
        int length = number.length();
        if(dotindex < 0)
        {
        	amount = number + ".00";
        } 
        else if(dotindex == length - 2)
        {
        	amount = number + "0";
        }
        else
        {
        	amount = number;
        }
    }
    
    public String getAmount()
    {
        return(amount);
    }  
    
    /* The amount of the transaction in the smallest currency unit expressed as an integer. For example, if
    the transaction amount is $49.95 then the amount in cents is 4995.*/
    public String getIntegerAmount()
    { 
        int dotindex = amount.indexOf('.');
        StringBuffer amountstr = new StringBuffer();
        amountstr.append(amount.substring(0, dotindex));
        amountstr.append(amount.substring(dotindex + 1, amount.length()));
        return(amountstr.toString());
    }
    
    /* A response code that is generated by the Payment Server to indicate the status of the
    transaction.
    A vpc_TxnResponseCode of  (zero) indicates that the transaction was processed
    successfully and approved by the Acquiring Bank. Any other value indicates the transaction */
    public String getResponseCode()
    {
        return(responsecode);
    }
    
    public void setResponseCode(String code)
    {
        responsecode = code;
    }    
    
    /* Indicates any errors the transaction may have encountered. */
    public String getResponseMessage()
    {
        return(responsemsg);
    }
    
    public void setResponseMessage(String msg)
    {
        responsemsg = msg;
    }    
    
    /* A unique number generated by the Payment Server for the transaction. It is stored in the
    Payment Server as a reference and used to perform actions such as a refund or capture.*/    
    public String getTransactionID()
    {
        return(transactionno);
    }
    
    public void setTransactionID(String no)
    {
        transactionno = no;
    }    
    
    /* This is also known as the Reference Retrieval Number (RRN), which is a unique identifier.
    This value is passed back to the cardholder for their records if the merchant application does
    not generate its own receipt number. */
    public String getReceiptNo()
    {
        return(receiptno);
    }
 
    public void setReceiptNo(String no)
    {
        receiptno = no;
    }      
    
    
    //runway payment id
    public String getPaymentID()
    {
        return(paymentid);
    }
    
    public void setPaymentID(String id)
    {
    	paymentid = id;
    }   
    
 
    //runway order id
    public String getOrderID()
    {
        return(this.orderid);
    }    
    
    public void setOrderID(String id)
    {
        this.orderid = id;
    }
    
    //For Server Hosted Tranactions only
    public String getTransactionURL(HttpServletRequest request, ServletContext context)
    {
        this.context = context;
        
        if(sanityCheck())
        {
            if(!usepaymentrecords || initPaymentRecord(request))
            {
            	return(paymentprovider.getPaymentURL());
            }
        }
        return(null);
    }
    
    //For Server Hosted Tranactions only
    public void setPaymentResponse(HttpServletRequest request, ServletContext context)
    {
        this.context = context;
        
    	paymentprovider.setPaymentResponse(request.getParameterMap());
        if(usepaymentrecords && !completePaymentRecord(request))
        {
            logError("Could not create update record");
        }
    }
    
    //For Merchant Hosted Tranactions only
    public void sendTransaction(HttpServletRequest request, ServletContext context)
    {
        this.context = context;
      
        if(sanityCheck())
        {
            if(initPaymentRecord(request))
            {
                try
                {
                    paymentprovider.sendPayment();
                }
                catch(Exception e)
                {
                    String stack = ActionBean.writeStackTraceToString(e);
                    logError(stack);
                    throw new PaymentProviderException(stack, e);
                }
                if(!completePaymentRecord(request))
                {
                    logError("Could not create update record");
                }
            }
            else
            {
                String msg = "Could not create new record";
                logError(msg);
                throw new DatabaseException(msg);
            }
        }
        else
        {
            String msg = "Required parameters not set";
            logError(msg);
            throw new IllegalConfigurationException(msg);            
        }    
        if(debug)
        {
            logError("Debug");
        }
    }
    
    protected void checkProvider()
    {
        if(paymentprovider==null)
        {
            String msg = "Payment provider not set";
            logError(msg);
            throw new IllegalConfigurationException(msg); 
        }  
    }
    
    protected void checkFormat()
    {
        int dotindex = amount.indexOf('.');
        if(dotindex < 0 || dotindex != amount.length()-3)
        {
            String msg = "Amount "+amount+" is not a valid format, expected #.##";
            logError(msg);
            throw new IllegalConfigurationException(msg);  
        }
    }
    
    protected boolean sanityCheck()
    {
    	checkProvider();
        if(server == null ||
            merchantid == null ||
            accesscode == null ||
            orderid == null ||
            amount == null )
        {
            String msg = "Missing required parameters";
            logError(msg);
            throw new IllegalConfigurationException(msg);  
        }
        if(mode == MERCHANT_HOSTED)
        {
        	if(cardnumber == null ||
                cardexpirymonth == null ||
                cardexpiryyear == null )
        	{
                String msg = "Missing required parameters for merchant hosted opperation";
                logError(msg);
                throw new IllegalConfigurationException(msg);  
        	}
        }
        checkFormat();
        return(true);
    }
    
    protected boolean initPaymentRecord(HttpServletRequest request)
    {
    	this.setClientIP(request.getRemoteAddr());
        payment = new RowBean();
        payment.setTableSpec("Payments");
        payment.setRequest(request);
        payment.setAction("insert");
        payment.createNewID();
        payment.setColumn("Type",String.valueOf(this.getType()));  
        payment.setColumn("OrderID",this.getOrderID()); 
        payment.setColumn("Amount",this.getAmount()); 
        payment.setColumn("IPAddress", this.getClientIP());
        payment.setColumn("CardName",this.getCardName()); 
        payment.setColumn("CardNumber",this.getCardNumber());
        payment.setColumn("CardExpiry",this.getCardExpiry()); 
        payment.setColumn("CardSecurtyCode",this.getCardSecurtyCode());         
        payment.doAction(true);
        setPaymentID(payment.getString("PaymentID"));
        return(payment.getUpdatedCount()==1);
    }
    
    protected boolean completePaymentRecord(HttpServletRequest request)
    {
    	if(payment==null)
    	{
            payment = new RowBean();
            payment.setTableSpec("Payments");
    	}
    	else
    	{
    		payment.clear();
    	}
        payment.setRequest(request);

        payment.setColumn("PaymentID",this.getPaymentID());
        payment.setColumn("ReceiptNo",this.getReceiptNo());  
        payment.setColumn("TransactionID",this.getTransactionID());   
        payment.setColumn("ResponseCode",this.getResponseCode()); 
        payment.setColumn("Response",this.getResponseMessage()); 
        payment.setColumn("StatusID",paymentprovider.getRunwayPaymentStatusID());

        payment.setColumn("CardType",this.getCardType());     
        
        payment.setAction("update");
        payment.doAction(true);
        
        String error = payment.getError();
        if(error!=null && error.length()!=0)
        {
            logError(error);
        }
        return(payment.getUpdatedCount()==1);
    }
    
    public void logError(String message)
    {
        StringBuffer msg = new StringBuffer();
        msg.append(message);
        msg.append("\r\n");
        msg.append(this.toString());
        if(paymentprovider!=null)
        {
            msg.append("\r\n");
            msg.append(paymentprovider.getLog());
        }
        log(Level.SEVERE, msg.toString());
        sendAlertEmail(msg.toString());
    }
    
    public void log(Level type, String message)
    {
        StringBuffer msg = new StringBuffer();
        msg.append("\r\n");
        msg.append(this.getPaymentID());
        msg.append(" - ");
        msg.append(context.getServletContextName());
        msg.append(" : ");
        msg.append(message);
        msg.append("\r\n");

        logger.log(type, msg.toString());
    }
    
    public void sendAlertEmail(String message)
    {
        try
        {
            MailerBean mailer = new MailerBean();
            mailer.setFrom("Runway<info@switched-on.com.au>");
            mailer.setTo("jong@switched-on.com.au");
            //mailer.setTo("network@switched-on.com.au");
            //mailer.setBcc("jong@switched-on.com.au");
            mailer.setHost((String)context.getInitParameter("SMTPHost"));
            mailer.setUsername((String)context.getInitParameter("SMTPUsername"));
            mailer.setPassword((String)context.getInitParameter("SMTPPassword"));
            mailer.setPort((String)context.getInitParameter("SMTPPort"));
            mailer.setTextbody(message);
            mailer.setSubject("PaymentBean error alert");
            mailer.getResponse();
        }
        catch(Exception e)
        {
            logError("Could not send email - "+e.getMessage());
        }
    }
    
    public String toString()
    {
        StringBuffer str = new StringBuffer();
        str.append('{');
        str.append("server=");
        str.append(server);
        str.append(", merchantid=");
        str.append(merchantid);
        str.append(", accesscode=");
        str.append(accesscode);
        str.append(", cardname=");
        str.append(cardname);        
        str.append(", cardnumber=");
        str.append(cardnumber);
        str.append(", cardexpiry=");
        str.append(getCardExpiry());
        str.append(", ccardsecurtycode=");
        str.append(ccardsecurtycode);
        str.append(", amount=");
        str.append(amount);
        str.append(", orderid=");
        str.append(orderid);
        
        str.append(", responsecode=");
        str.append(responsecode);
        str.append(", responsemsg=");
        str.append(responsemsg);
        str.append(", transactionno=");
        str.append(transactionno);
        str.append(", receiptno=");
        str.append(receiptno);
        str.append(", clientipaddress=");
        str.append(ipaddress);  
        
        str.append(", debug=");
        str.append(String.valueOf(debug));
        str.append(", test=");
        str.append(String.valueOf(test));          
        
        str.append('}');
  
        if(payment!=null)
        {
            str.append("\r\nPayment\r\n");
            str.append(payment.toString());
            str.append("\r\nPayment Error\r\n");
            str.append(payment.getError());            
        }  
        str.append("\r\n");
        return(str.toString());
    }
    
    public void setProperties(Properties config)
    {
        if(config.getProperty(PAYMENT_PROVIDER)!=null)
        {
            this.setPaymentProvider(config.getProperty(PAYMENT_PROVIDER));
        }        
        if(config.getProperty(PAYMENT_SERVER)!=null)
        {
            this.setServerURL(config.getProperty(PAYMENT_SERVER));
        }
        if(config.getProperty(PAYMENT_MERCHANTID)!=null)
        {
            this.setMerchantID(config.getProperty(PAYMENT_MERCHANTID));
        }        
        if(config.getProperty(PAYMENT_ACCESSCODE)!=null)
        {
            this.setMerchantAccessCode(config.getProperty(PAYMENT_ACCESSCODE));
        }
        if(config.getProperty(PAYMENT_USER)!=null)
        {
            this.setUser(config.getProperty(PAYMENT_USER));
        } 
        if(config.getProperty(PAYMENT_PASSWORD)!=null)
        {
            this.setPassword(config.getProperty(PAYMENT_PASSWORD));
        }   
        if(config.getProperty(PAYMENT_SECURE_SECRET)!=null)
        {
            this.setSecureSecret(config.getProperty(PAYMENT_SECURE_SECRET));
        }          
    }
    
    public void setOrder(RowData order)
    {
        setOrderID(order.getString("OrderID"));
        setAmount(order.getString("TotalCost"));
    }
    
    public static void main(String[] args)
    {
        //PaymentBean pb = new PaymentBean();
       // pb.setAmount("12321.99");
        String amount = "12321.99";
        int dotindex = amount.indexOf('.');
        if(dotindex < 0 || dotindex != amount.length()-3)
        {
               System.out.println(dotindex);
        }
        else
        {
            System.out.println("fine");
        }
    }
}