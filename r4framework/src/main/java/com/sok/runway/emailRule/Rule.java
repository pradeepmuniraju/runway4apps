package com.sok.runway.emailRule;

import org.dom4j.*;
import java.util.regex.*;
import com.sok.runway.*;
import com.sok.framework.*;
   
public class Rule
{
   protected static final String _filter = "filter";
   protected static final String _filterSet = "filterSet";   
   protected static final String _type = "type";
   protected static final String _field = "field";
   protected static final String _setType = "setType";
   protected static final String _action = "action";    
   protected static final String _address = "address"; 
   protected static final String _removeFromServer = "removeFromServer";
   
   public String setType;
   public boolean removeFromServer = false;
   public FilterSet[] filterSets;
   public Action[] actions;
   
   public Rule(Element ele)
   {
      setType = ele.attributeValue(_setType);   
      removeFromServer = "true".equals(ele.attributeValue(_removeFromServer));
      setFilterSets(ele);
      setActions(ele);
   }    
   
   public void setFilterSets(Element parent)
   {
      Element[] eles = XMLSpec.getElementArray(parent, _filterSet);
      filterSets = new FilterSet[eles.length];
      for(int i=0; i<eles.length; i++)
      {
         filterSets[i] = new FilterSet(eles[i]);
      }
   } 
   
   public void setActions(Element parent)
   {
      Element[] eles = XMLSpec.getElementArray(parent, _action);
      actions = new Action[eles.length];
      for(int i=0; i<eles.length; i++)
      {
         actions[i] = new Action(eles[i]);
      }
   }       
   
   public class Field
   {
      public static final String SUBJECT = "subject";
      public static final String FROM = "from";
      public static final String TO = "to";
      public static final String MESSAGE = "message";
      public static final String ATTACHMENT = "attachment";
   }     
   
   public class FilterSet
   {
      public String setType;        
      public Filter[] filters;
      public FilterSet[] filterSets;
      
      public FilterSet(Element ele)
      {
         setType = ele.attributeValue(_setType);   
         setFilters(ele);
         setFilterSets(ele);
      }         
      
      public void setFilters(Element parent)
      {
         Element[] eles = XMLSpec.getElementArray(parent, _filter);
         if(eles!=null)
         {         
            filters = new Filter[eles.length];
            for(int i=0; i<eles.length; i++)
            {
               filters[i] = new Filter(eles[i]);
            }
         }
      }
      
      public void setFilterSets(Element parent)
      {
         Element[] eles = XMLSpec.getElementArray(parent, _filterSet);
         if(eles!=null)
         {
            filterSets = new FilterSet[eles.length];
            for(int i=0; i<eles.length; i++)
            {
               filterSets[i] = new FilterSet(eles[i]);
            }
         }
      }      
   }   
   
   public class Filter
   {
      public String type;
      public String[] fields;
      public String text;
      public Pattern pattern;
      
      public Filter(Element ele)
      {
         type = ele.attributeValue(_type);
         fields = ele.attributeValue(_field).split("\\|");
         text = ele.getText();

         if(Type.REGEX.equals(type) || Type.NOTREGEX.equals(type))
         {
            pattern = Pattern.compile(text, Pattern.CASE_INSENSITIVE);
         }
         else if(text!=null)
         {
            text = text.toLowerCase();
         }
      }         
      
      public class Type
      {
         public static final String CONTAINS = "contains";
         public static final String DOESNOTCONTAIN = "does not contain";
         public static final String IS = "is";
         public static final String ISNOT = "is not";
         public static final String BEGINSWITH = "begins with";
         public static final String DOESNOTBEGINSWITH = "does not begins with";         
         public static final String ENDSWITH = "ends with";
         public static final String DOESNOTENDSWITH = "does not ends with";
         public static final String REGEX = "regex";   
         public static final String NOTREGEX = "not regex";  
      }          
   }
   
   public class Action
   {
      protected static final String _forwardAddress = "forwardAddress"; 
      protected static final String _match = "match";
      protected static final String _updateContact = "updateContact";
      protected static final String _insertNote = "insertNote";
      protected static final String _insertResponse = "insertResponse";
      protected static final String _updateEvent = "updateEvent"; 
      
      public String forwardAddress;
      public InsertResponse insertResponse = null;
      public InsertNote insertNote = null;
      public UpdateContact updateContact = null;
      public UpdateEvent updateEvent = null;
      public Match[] matches = null;
      
      public Action(Element ele)
      {
         forwardAddress = ele.attributeValue(_forwardAddress); 
         setMatch(ele);
         setInsertResponse(ele);
         setInsertNote(ele);
         setUpdateContact(ele);
         setUpdateEvent(ele);
      }         
      
      protected void setMatch(Element parent)
      {
         Element[] eles = XMLSpec.getElementArray(parent, _match);
         if(eles!=null)
         {         
            matches = new Match[eles.length];
            for(int i=0; i<eles.length; i++)
            {
               matches[i] = new Match(eles[i]);
            }
         }        
      }
      
      protected void setInsertResponse(Element parent)
      {
         Element[] eles = XMLSpec.getElementArray(parent, _insertResponse);
         if(eles!=null)
         {         
            if(eles.length>0)
            {
               insertResponse = new InsertResponse(eles[0]);
            }
         } 
      }
      
      protected void setInsertNote(Element parent)
      {
         Element[] eles = XMLSpec.getElementArray(parent, _insertNote);
         if(eles!=null)
         {         
            if(eles.length>0)
            {
               insertNote = new InsertNote(eles[0]);
            }
         } 
      }
      
      protected void setUpdateContact(Element parent)
      {
         Element[] eles = XMLSpec.getElementArray(parent, _updateContact);
         if(eles!=null)
         {         
            if(eles.length>0)
            {
               updateContact = new UpdateContact(eles[0]);
            }
         } 
      }  
      protected void setUpdateEvent(Element parent)
      {
         Element[] eles = XMLSpec.getElementArray(parent, _updateEvent);
         if(eles!=null)
         {         
            if(eles.length>0)
            {
               updateEvent = new UpdateEvent(eles[0]);
            }
         } 
      }     
      
      public class Match
      {
         protected static final String _pattern = "pattern";   
         protected static final String _group = "group";  
         
         public Pattern pattern = null;
         public String type = null;
         public String[] fields = null;
         public int group = -1;
         
         public Match(Element ele)
         {
            String p = ele.getText();
            if(p!=null && p.length()!=0)
            {
               pattern = Pattern.compile(p, Pattern.CASE_INSENSITIVE);
            }
            type = ele.attributeValue(_type);
            fields = ele.attributeValue(_field).split("\\|");;
            String groupstr = ele.attributeValue(_group);
            if(groupstr!=null && groupstr.length()!=0)
            {
               group = Integer.parseInt(groupstr);
            }
         }
      }     
      
      public abstract class FieldAction
      {
         public Field[] fields = null;
         
         public FieldAction(Element ele)
         {
            setFields(ele);
         }
         
         public void setFields(Element parent)
         {
            Element[] eles = XMLSpec.getElementArray(parent, _field);
            if(eles!=null)
            {
               fields = new Field[eles.length];
               for(int i=0; i<eles.length; i++)
               {
                  fields[i] = new Field(eles[i]);
               }
            }
         } 
      }
      
      public class InsertResponse extends FieldAction
      {         
         public InsertResponse(Element ele)
         {
            super(ele);
         } 
      }
      
      public class InsertNote extends FieldAction
      {
         public InsertNote(Element ele)
         {
            super(ele);
         } 
      } 
      
      public class UpdateContact extends FieldAction
      {
         public UpdateContact(Element ele)
         {
            super(ele);
         } 
      } 
      
      public class UpdateEvent extends FieldAction
      {
         public UpdateEvent(Element ele)
         {
            super(ele);
         } 
      }   
      
      public class Field
      {
         public static final String _name = "name"; 
         
         public String name = null;
         public String value = null;
         
         public Field(Element ele)
         {
            name = ele.attributeValue(_name);
            value = ele.getText();
            if(value==null)
            {
               value = "";
            }
         }
      }
   }
   
   public class FiltersType
   {
      public static final String ALL = "all";
      public static final String ANY = "any";
   }
}