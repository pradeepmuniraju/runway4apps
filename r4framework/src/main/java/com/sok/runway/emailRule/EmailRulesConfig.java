package com.sok.runway.emailRule;

import java.io.File;
import javax.servlet.ServletContext;
import org.dom4j.*;

import com.sok.runway.*;
import com.sok.framework.*;
import com.sok.runway.offline.*;

public class EmailRulesConfig extends XMLSpec implements DaemonWorkerConfig
{
   protected static EmailRulesConfig emailfilterconfig = null;
   protected static final String filepath = "/WEB-INF/emailfilter.xml";
   protected static String fullpath = null;
   protected static Account[] accounts = null;
   protected int interval = 0;
   protected boolean debug = false;
   protected boolean pretend = false;
   protected String debugaddress = null;
   
   protected static final String _emailFilter = "emailFilter";
   protected static final String _interval = "interval";
   protected static final String _account = "account";
   protected static final String _debug = "debug";   
   protected static final String _debugAddress = "debugAddress"; 
   
   public static EmailRulesConfig getEmailRulesConfig(String parentpath)
   {
      if(emailfilterconfig == null || emailfilterconfig.isModified())
      {
         loadConfig(parentpath);
      }
      return(emailfilterconfig);
   }

   protected static synchronized void loadConfig(String parentpath)
   {
      if(emailfilterconfig == null || emailfilterconfig.isModified())
      {
         if(fullpath == null)
         {
            StringBuffer sb = new StringBuffer(parentpath);
            sb.append(filepath);
            fullpath = sb.toString();
         }
         emailfilterconfig = new EmailRulesConfig(fullpath);
      }
   }

   public EmailRulesConfig(String path)
   {
      super(path);
      if(super.document != null)
      {
         Element root = super.document.getRootElement();
         if(root != null)
         {
            setAccounts(root);
            interval = Integer.parseInt(root.attributeValue(_interval));
            debug = "true".equals(root.attributeValue(_debug));
            debugaddress = root.attributeValue(_debugAddress);
         }
         else
         {
            throw new NullPointerException("Document has no root element - "
                  + path);
         }
      }
      else
      {
         throw new NullPointerException("Document cannot be loaded - " + path);
      }
   }

   protected void setAccounts(Element parent)
   {
      Element[] ruleEles = getElementArray(parent, _account);
      accounts = new Account[ruleEles.length];
      for (int i = 0; i < ruleEles.length; i++)
      {
         accounts[i] = new Account(ruleEles[i]);
      }
   }

   public int getInterval()
   {
      return(interval);
   }
   
   public Account[] getAccounts()
   {
      return(accounts);
   }
   
   public boolean getDebug()
   {
      return(debug);
   }
   
   public boolean getPretend()
   {
      return(pretend);
   }   
   
   public String getDebugAddress()
   {
      return(debugaddress);
   }

   public boolean hasWorkerTimer()
   {
      return false;
   } 
   
   public WorkerTimer getWorkerTimer()
   {
      return(null);
   }
}
/* example config file
 * 

<emailRule interval="120" debug="false" debugAddress="shinytoaster@gmail.com">
<!-- interval - wait seconds -->
<!-- debug="true|false", debugAddress used for testing -->
<!-- emailRule can have 1+ number of accounts -->
<!-- this file automatically reloads -->
<!--
<account username="" password="" serverAddress="mail.switched-on.com.au" serverPort="" protocol="imap">

   <!-- with imap actioned messages will be moved to "Runway Proccessed" or if not matched by any filters "Runway No Match" -->
   <!-- serverAddress, username, password - email account details -->
   <!-- serverPort optional, protocol="pop3|imap|imaps"-->  
   <!-- account can have 1+ number of rules -->
   
      <rule setType="all" removeFromServer="false">
         <filterSet setType="all">
            <filter field="subject|message" type="contains" > </filter>
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com"/>
      </rule>
         
      <!-- Invalid Address Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="all">
            <filterSet setType="any">
            <!-- filterSet can have 1+ number of filters or 1+ number of filterSets -->
            <!-- setType="all|any" match all|any filters -->
               <filter field="subject|message" type="contains" >said: 550 MAILBOX</filter>
               <filter field="subject|message" type="contains" >Unknown or misspelled user</filter>
               <filter field="subject|message" type="contains" >unknown at</filter>
               <filter field="subject|message" type="contains" >does not exist</filter>
               <filter field="subject|message" type="contains" >not found in LDAP server</filter>
               <filter field="subject|message" type="contains" >Rule imposed mailbox access</filter>
               <filter field="subject|message" type="contains" >Unknown local part</filter>
               <filter field="subject|message" type="contains" ><![CDATA[#5.1.1]]></filter>
               <filter field="subject|message" type="contains" ><![CDATA[#5.7.1]]></filter>
               <filter field="subject|message" type="contains" ><![CDATA[#5.1.0]]></filter>              
               <filter field="subject|message" type="contains" >is either : misspelled, not permitted to receive mail</filter>
               <filter field="subject|message" type="contains" >Recipient name not found at destination</filter>
               <filter field="subject|message" type="contains" >550 Unroutable</filter>
               <filter field="subject|message" type="contains" >user not found</filter>
               <filter field="subject|message" type="contains" >550 MAILBOX NOT</filter>
               <filter field="subject|message" type="contains" >Addressee unknown</filter>
               <filter field="subject|message" type="contains" >user invalid</filter>
               <filter field="subject|message" type="contains" >550 Invalid</filter>
               <filter field="subject|message" type="contains" >The recipient's e-mail address was not found in the recipient's e-mail system</filter>
               <filter field="subject|message" type="contains" >Delivery to the following address failed as unknown</filter>
               <filter field="subject|message" type="contains" >550 No such</filter>
               <filter field="subject|message" type="contains" >550-5.1.1</filter>
               <filter field="subject|message" type="contains" >user doesn't have a yahoo.com account</filter>
               <filter field="subject|message" type="contains" >user doesn't have a yahoo.com.au account</filter>
               <filter field="subject|message" type="contains" >user does not exist</filter>
               <filter field="subject|message" type="contains" >Not our customer</filter>
               <filter field="subject|message" type="contains" >malformed</filter>
               <filter field="subject|message" type="contains" >MAILBOX NOT FOUND</filter>
               <filter field="subject|message" type="contains" >illegal alias</filter>
               <filter field="subject|message" type="contains" >Address does not exist</filter>
               <filter field="subject|message" type="contains" >does not like recipient</filter>
               <filter field="subject|message" type="contains" >User Unknown</filter>
               <filter field="subject|message" type="contains" >Unroutable address</filter>
               <filter field="subject|message" type="contains" >unknown user</filter>
               <filter field="subject|message" type="contains" >Unknown email address</filter>
               <filter field="subject|message" type="contains" >this recipient is not in my validrcptto</filter>
               <filter field="subject|message" type="contains" >This account is not allowed</filter>
               <filter field="subject|message" type="contains" >Recipient unknow</filter>
               <filter field="subject|message" type="contains" >recipient rejected</filter>
               <filter field="subject|message" type="contains" >recipient name is not recognized</filter>
               <filter field="subject|message" type="contains" >recipient cannot be verified</filter>
               <filter field="subject|message" type="contains" >not listed in Domino directory</filter>
               <filter field="subject|message" type="contains" >no valid recipients</filter>
               <filter field="subject|message" type="contains" >No such user</filter>
               <filter field="subject|message" type="contains" >No such recipient</filter>
               <filter field="subject|message" type="contains" >no such address</filter>
               <filter field="subject|message" type="contains" >No such account</filter>
               <filter field="subject|message" type="contains" >No such mailbox</filter>
               <filter field="subject|message" type="contains" >no users here by that name</filter>               
               <filter field="subject|message" type="contains" >no mailbox here by that name</filter>
               <filter field="subject|message" type="contains" >Malformed or unexpected name server reply</filter>
               <filter field="subject|message" type="contains" >invalid recipient</filter>
               <filter field="subject|message" type="contains" >Invalid address</filter>
               <filter field="subject|message" type="contains" >following destination addresses were unknown</filter>
               <filter field="subject|message" type="contains" >e-mail account does not exist</filter>         
               <filter field="subject|message" type="contains" >deny invalid addresses</filter> 
               <filter field="subject|message" type="contains" >Bad destination mailbox</filter>   
               <filter field="subject|message" type="contains" >Bad destination email address</filter>   
               <filter field="subject|message" type="contains" >bad address</filter>   
               <filter field="subject|message" type="contains" >address unknown</filter>  
               <filter field="subject|message" type="contains" >Address rejected</filter> 
               <filter field="subject|message" type="contains" >Address Error</filter>    
               <filter field="subject|message" type="contains" >Address invalid</filter>  
               <!-- field="subject|from|to|message|attachment" -->
               <!-- if more than one field separated by "|" is specified, they are concatenated -->
               <!-- type="contains|does not contain|is|is not|begins with|does not begins with|ends with|does not ends with|regex|not regex" -->
            </filterSet>
            <filterSet setType="all">
               <filter field="subject|message" type="does not contain" >550 Content</filter>
               <filter field="subject|message" type="does not contain" >not configured to relay mail</filter>
               <filter field="subject|message" type="does not contain" >relay access denied</filter>
               <filter field="subject|message" type="does not contain" >553-Message Filtered</filter>
               <filter field="subject|message" type="does not contain" >Message Filtered</filter>
               <filter field="subject|message" type="does not contain" >Spam value exceeded</filter>
               <filter field="subject|message" type="does not contain" >message refused</filter>
            </filterSet>
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Invalid User</field>
            </insertResponse>
            <updateContact>
               <field name="Email"></field>
               <field name="EmailStatus">Invalid User</field>              
            </updateContact>
         </action>
         <action forwardAddress="shinytoaster@gmail.com"/>
      </rule>     
      
      <!-- Inactive Account Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="all">
            <filterSet setType="any">
            <!-- filterSet can have 1+ number of filters or 1+ number of filterSets -->
            <!-- setType="all|any" match all|any filters -->
               <filter field="subject|message" type="contains" >This account has been disabled</filter>
               <filter field="subject|message" type="contains" >address no longer accepts mail</filter>
               <filter field="subject|message" type="contains" >Account Inactive</filter>
               <filter field="subject|message" type="contains" >account expired</filter>
               <filter field="subject|message" type="contains" >disabled or discontinued</filter>
               <filter field="subject|message" type="contains" >User account is expired</filter>
               <filter field="subject|message" type="contains" >Mailbox is inactive</filter> 
               <!-- field="subject|from|to|message|attachment" -->
               <!-- if more than one field separated by "|" is specified, they are concatenated -->
               <!-- type="contains|does not contain|is|is not|begins with|does not begins with|ends with|does not ends with|regex|not regex" -->
            </filterSet>
            <filterSet setType="all">
               <filter field="subject|message" type="does not contain" >not configured to relay mail</filter>
               <filter field="subject|message" type="does not contain" >relay access denied</filter>
            </filterSet>
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Inactive Account</field>
            </insertResponse><!-- type - one of list of bounce types -->
            <updateContact>
               <field name="Email"></field>
               <field name="EmailStatus">Inactive Account</field>             
            </updateContact>
         </action>   
      </rule>     
         
      <!-- Invalid Domain Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="all">
            <filterSet setType="any">
            <!-- filterSet can have 1+ number of filters or 1+ number of filterSets -->
            <!-- setType="all|any" match all|any filters -->
               <filter field="subject|message" type="contains" >Domain does not exist</filter>
               <filter field="subject|message" type="contains" >domain name not found</filter>
               <filter field="subject|message" type="contains" >Host unknown</filter>
               <filter field="subject|message" type="contains" >Illegal host</filter>
               <filter field="subject|message" type="contains" >this server doesn't handle mail for</filter>
               <filter field="subject|message" type="contains" >Non existent host/domain</filter>
               <filter field="subject|message" type="contains" >Unable to resolve the domain</filter>
               <!-- field="subject|from|to|message|attachment" -->
               <!-- if more than one field separated by "|" is specified, they are concatenated -->
               <!-- type="contains|does not contain|is|is not|begins with|does not begins with|ends with|does not ends with|regex|not regex" -->
            </filterSet>
            <filterSet setType="all">
               <filter field="subject|message" type="does not contain" >not configured to relay mail</filter>
               <filter field="subject|message" type="does not contain" >relay access denied</filter>
               <filter field="subject|message" type="does not contain" >isn't in my list of allowed rcpthosts</filter>
            </filterSet>
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Invalid Domain</field>
            </insertResponse>
            <updateContact>
               <field name="Email"></field>
               <field name="EmailStatus">Invalid Domain</field>               
            </updateContact>
         </action>               
      </rule>           
            
      <!-- Blocked Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="any">
         <!-- filterSet can have 1+ number of filters or 1+ number of filterSets -->
         <!-- setType="all|any" match all|any filters -->
            <filter field="subject|message" type="contains" >You have been blocked by the recipient</filter>
            <filter field="subject|message" type="contains" >IP address has been blacklisted</filter>
            <filter field="subject|message" type="contains" >are not allowed to send mail to</filter>
            <!-- field="subject|from|to|message|attachment" -->
            <!-- if more than one field separated by "|" is specified, they are concatenated -->
            <!-- type="contains|does not contain|is|is not|begins with|does not begins with|ends with|does not ends with|regex|not regex" -->
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Blocked</field>
            </insertResponse>
         </action>               
      </rule>     
               
      <!-- Relay Denied Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="any">
         <!-- filterSet can have 1+ number of filters or 1+ number of filterSets -->
         <!-- setType="all|any" match all|any filters -->
            <filter field="subject|message" type="contains" >relay denied</filter>
            <filter field="subject|message" type="contains" >RELAYING DENIED</filter>
            <filter field="subject|message" type="contains" >relay through this server</filter>
            <filter field="subject|message" type="contains" >550 relaying </filter>
            <filter field="subject|message" type="contains" >Unable to relay</filter>
            <filter field="subject|message" type="contains" >Relay access denied</filter>
            <filter field="subject|message" type="contains" >No relaying allowed</filter>
            <!-- field="subject|from|to|message|attachment" -->
            <!-- if more than one field separated by "|" is specified, they are concatenated -->
            <!-- type="contains|does not contain|is|is not|begins with|does not begins with|ends with|does not ends with|regex|not regex" -->
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Relay Denied</field>
            </insertResponse><!-- type - one of list of bounce types -->
         </action>               
      </rule>  
                  
      <!-- Spam Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="all">
            <filterSet setType="any">
            <!-- filterSet can have 1+ number of filters or 1+ number of filterSets -->
            <!-- setType="all|any" match all|any filters -->
      
               <filter field="subject|message" type="contains" >No spam accepted</filter>
               <filter field="subject|message" type="contains" >zonealarm</filter>
               <filter field="subject|message" type="contains" >security suite</filter>
               <filter field="subject|message" type="contains" >validate your</filter>
               <filter field="subject|message" type="contains" >Incoming Policy</filter>
               <filter field="subject|message" type="contains" >blocked by our Spam firewall</filter>
               <filter field="subject|message" type="contains" >unsolicited</filter>
               <filter field="subject|message" type="contains" >policy violation</filter>
               <filter field="subject|message" type="contains" >detected as possibly containing spam related content</filter>
               <filter field="subject|message" type="contains" >looks like spam or phish to me</filter>
               <filter field="subject|message" type="contains" >Email Spam Blocked</filter>
               <filter field="subject|message" type="contains" >SPAM BLOCKED</filter>
               <filter field="subject|message" type="contains" >JunkMail</filter>
               <filter field="subject|message" type="contains" >Death2Spam</filter>
               <filter field="subject|message" type="contains" >Spam rejection</filter>
               <filter field="subject|message" type="contains" >rejected for policy reasons</filter>
               <filter field="subject|message" type="contains" >We do not accept junk mail</filter>
               <filter field="subject|message" type="contains" >content rejected</filter>
               <filter field="subject|message" type="contains" >looks like spam</filter>
               <filter field="subject|message" type="contains" >Your email is considered spam</filter>
               <filter field="subject|message" type="contains" >Rule imposed mailbox</filter>
               <filter field="subject|message" type="contains" >said: 550 Rule</filter>
               <filter field="subject|message" type="contains" >Message rejected because of unacceptable content</filter>
               <filter field="subject|message" type="contains" >Your message wasn't delivered because of security policies</filter>
                     
               <!-- field="subject|from|to|message|attachment" -->
               <!-- if more than one field separated by "|" is specified, they are concatenated -->
               <!-- type="contains|does not contain|is|is not|begins with|does not begins with|ends with|does not ends with|regex|not regex" -->
   
            </filterSet>
            <filterSet setType="all">
               <filter field="subject|message" type="does not contain" >Not enough storage space</filter>
               <filter field="subject|message" type="does not contain" >out of office</filter>
               <filter field="subject|message" type="does not contain" >quota exceeded</filter>
               <filter field="subject|message" type="does not contain" >annual leave</filter>
               <filter field="subject|message" type="does not contain" >unknown</filter>
               <filter field="subject|message" type="does not contain" >re:</filter>
            </filterSet>
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Spam</field>
            </insertResponse>
         </action>               
      </rule>                    
            
      <!-- Out of Office Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="all">
            <filterSet setType="any">
            <!-- filterSet can have 1+ number of filters or 1+ number of filterSets -->
            <!-- setType="all|any" match all|any filters -->
      
               <filter field="subject|message" type="contains" >out of office</filter>
               <filter field="subject|message" type="contains" >annual leave</filter>
               <filter field="subject|message" type="contains" >on holiday</filter>
               <filter field="subject|message" type="contains" >on leave</filter>
               <filter field="subject|message" type="contains" >auto reply</filter>
               <filter field="subject|message" type="contains" >autoreply</filter>
               
               <!-- field="subject|from|to|message|attachment" -->
               <!-- if more than one field separated by "|" is specified, they are concatenated -->
               <!-- type="contains|does not contain|is|is not|begins with|does not begins with|ends with|does not ends with|regex|not regex" -->
   
            </filterSet>
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Out of Office</field>
            </insertResponse>
         </action>               
      </rule>     
      
      <!-- Exceed Quota Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="any">
            <!-- filterSet can have 1+ number of filters or 1+ number of filterSets -->
            <!-- setType="all|any" match all|any filters -->
      
               <filter field="subject|message" type="contains" >over quota</filter>
               <filter field="subject|message" type="contains" >mailfolder is full</filter>
               <filter field="subject|message" type="contains" >not enough storage space</filter>
               <filter field="subject|message" type="contains" >User mailbox exceeds allowed size</filter>
               <filter field="subject|message" type="contains" >full, please try again later</filter>
               <filter field="subject|message" type="contains" >over quota</filter>
               <filter field="subject|message" type="contains" >mailbox limit</filter>
               <filter field="subject|message" type="contains" >mailbox full</filter>
               <filter field="subject|message" type="contains" >mailbox is full</filter>
               <filter field="subject|message" type="contains" >delivery temporarily suspended</filter>
               <filter field="subject|message" type="contains" >said: 452</filter>
               <filter field="subject|message" type="contains" >mailbox size limit exceeded</filter>
               
               <!-- field="subject|from|to|message|attachment" -->
               <!-- if more than one field separated by "|" is specified, they are concatenated -->
               <!-- type="contains|does not contain|is|is not|begins with|does not begins with|ends with|does not ends with|regex|not regex" -->
   
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Exceed Quota</field>
            </insertResponse>
         </action>               
      </rule>                    
            
      <!-- Loop Back Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="any">
         <!-- filterSet can have 1+ number of filters or 1+ number of filterSets -->
         <!-- setType="all|any" match all|any filters -->
            <filter field="subject|message" type="contains" >loops back to</filter>
            <!-- field="subject|from|to|message|attachment" -->
            <!-- if more than one field separated by "|" is specified, they are concatenated -->
            <!-- type="contains|does not contain|is|is not|begins with|does not begins with|ends with|does not ends with|regex|not regex" -->
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Loop Back</field>
            </insertResponse>
         </action>               
      </rule>              
               
      <!-- Need to Action Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="all">
      
               <filter field="subject" type="contains" >"automated"</filter>
               <filter field="subject|message" type="contains" >daemon</filter>
               <filter field="subject|message" type="contains" >runway</filter>
               <filter field="subject|message" type="contains" >switched-on</filter>
               <filter field="subject|message" type="contains" >mailmarshal</filter>
               <filter field="subject|message" type="contains" >re:</filter>
               <filter field="subject|message" type="contains" >postmaster</filter>

               <filter field="subject|message" type="does not contain" >timed out</filter>
               <filter field="subject|message" type="does not contain" >returned mail</filter>
               <filter field="subject|message" type="does not contain" >delivery failure</filter>
               <filter field="subject|message" type="does not contain" >out of office</filter>
               <filter field="subject|message" type="does not contain" >outoreply</filter>
               <filter field="subject|message" type="does not contain" >holiday</filter>

         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Spam</field>
            </insertResponse>
         </action>
         <action forwardAddress="shinytoaster@gmail.com"/>     
      </rule>  
      
      <!-- Too Many Headers to Action Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="all">
               <filter field="subject|message" type="contains" >header counts of 27</filter>
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Too many headers</field>
            </insertResponse>
         </action>
      </rule>              
                  
      <!-- Message Refused to Action Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="any">
               <filter field="subject|message" type="contains" >message refused</filter>
               <filter field="subject|message" type="contains" >delivery not authorized</filter>               
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Too many headers</field>
            </insertResponse>
         </action>
      </rule>  
      
      <!-- Message Delayed to Action Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="any">
               <filter field="subject|message" type="contains" >YOU DO NOT NEED TO RESEND YOUR MESSAGE</filter>
               <filter field="subject|message" type="contains" >delay</filter>
               <filter field="subject|message" type="contains" >THIS IS A WARNING MESSAGE ONLY</filter>  
               <filter field="subject|message" type="contains" >Timed Out</filter>  
               <filter field="subject|message" type="contains" >This is just a warning</filter> 
               <filter field="subject|message" type="contains" >Delivery attempts will continue</filter> 
               <filter field="subject|message" type="contains" >Delivery time expired</filter>  
               <filter field="subject|message" type="contains" >Your message was not delivered within</filter> 
               <filter field="subject|message" type="contains" >delivery temporarily suspended</filter>  
               <filter field="subject|message" type="contains" >Command time limit exceeded</filter>
               <filter field="subject|message" type="contains" >Connection time out</filter>             
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Rule: Delayed</field>
            </insertResponse>
         </action>
      </rule>  
      
      <!-- Lost Connection Action Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="any">
               <filter field="subject|message" type="contains" >lost connection</filter>
               <filter field="subject|message" type="contains" >Connection timed</filter>
               <filter field="subject|message" type="contains" >you may be experiencing network problems</filter> 
               <filter field="subject|message" type="contains" >Timed Out</filter>  
               <filter field="subject|message" type="contains" >queue too long</filter>   
               <filter field="subject|message" type="contains" >message too old</filter>  
               <filter field="subject|message" type="contains" >4.7.1</filter>   
               <filter field="subject|message" type="contains" >Connection refused</filter>              
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Lost Connection</field>
            </insertResponse>
         </action>
      </rule>              
      
      <!-- Too Many Hops Action Rule -->
      <rule setType="all" removeFromServer="false">
      <!-- rule can have 1+ number of filtersets or actions -->
      <!-- setType="all|any" match all|any filter sets -->
      <!-- removeFromServer="true|false" delete email after successfully actioned  -->
         <filterSet setType="any">
               <filter field="subject|message" type="contains" >too many hops</filter>
               <filter field="subject|message" type="contains" >Hop count exceeded</filter>           
         </filterSet>
         <action forwardAddress="shinytoaster@gmail.com">
         <!-- forwardAddress - address to forward email if match could not be found or if no match specified -->
         <!-- action can have any number of match -->
         <!-- action can have one or none of insertResponse, insertNote, updateContact -->
         <!-- insertResponse, insertNote and updateContact is dependent on at least one match succeeding -->
            <match type="NoteID" field="message|attachment" group="2"><![CDATA[(ID\=)(\w{10,})]]></match>
            <match type="Email" field="message|attachment" group="1"><![CDATA[Final-Recipient\:\s+RFC822\;\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>          
            <match type="Email" field="message|attachment" group="1"><![CDATA[To\:\s+\<?([\w\d\_\.]+\@[\w\d\_\.]+)\>?]]></match>
            <!-- type="NoteID|Email" -->
            <!-- field="subject|from|to|message|attachment" -->
            <insertResponse>
               <field name="Description">Email Bounce: Too Many Hops</field>
            </insertResponse>
         </action>
      </rule>        
      
   </account>
-->
</emailRule>
*/