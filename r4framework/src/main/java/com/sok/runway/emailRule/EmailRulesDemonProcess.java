package com.sok.runway.emailRule;

import java.io.File;

import com.sok.runway.*;
import com.sok.runway.offline.AbstractDaemonWorkerController;
import com.sok.framework.*;

/*
 * To run it put this in your web.xml, you may need to use a daemonClass number which is not in use.
 * You will need a emailfilter.xml in your WEB-INF
<servlet>
    <servlet-name>Initiate</servlet-name>
    <servlet-class>com.sok.framework.InitServlet</servlet-class>
    <load-on-startup>0</load-on-startup>
     <init-param>
       <param-name>daemonClass0</param-name>
       <param-value>com.sok.runway.emailRule.EmailRulesDemonProcess</param-value>
     </init-param>      
</servlet>
*/

public class EmailRulesDemonProcess extends AbstractDaemonWorkerController
{
   
   public static String NAME = "Email Filter";
   EmailRules rules = null;
   
   public EmailRulesDemonProcess(DaemonFactory daemonFactory)
   {
      super(daemonFactory);
   }
   
   protected void initWorker()
   {
      rules = new EmailRules(InitServlet.getRealPath());
      worker = rules;
   }   
   
   public static void main(String[] args)
   {
      EmailRulesConfig config = EmailRulesConfig.getEmailRulesConfig("/usr/local/tomcat/webapps/runway/");
      Account[] accounts = config.getAccounts();
      for(int i=0; i< accounts.length; i++)
      {
         System.out.println(accounts[i].username);
      }
   }

   public String getName()
   {
      return "Email Rules";
   }
}
