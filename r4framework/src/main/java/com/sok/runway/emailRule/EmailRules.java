package com.sok.runway.emailRule;

import java.util.regex.*;
import java.util.*;
import com.sok.runway.*;
import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.runway.offline.*;

import javax.mail.internet.InternetAddress;
import javax.mail.*;
import javax.servlet.*;

public class EmailRules implements DaemonWorker
{
   public EmailRulesConfig config = null;
   EmailBean mailstore = null;
   String configfilepath = null;
   int currentAccount = 0;
   int currentEmail = 0;
   int currentEmailsInAccount = 0;
   String firstmatchedfiltertext = null;
   int errorcount = 0;

   Account[] accounts = null;
   
   StringBuffer message = null;
   boolean debug = false;
   
   
   // damm this is bad
   String systemuserid = "E3B3653517125741C947061350C510E4";   
   com.sok.Debugger debugger = null;  
   
   public EmailRules(String configfilepath)
   {     
      this.configfilepath = configfilepath;
      debugger = com.sok.Debugger.getDebugger();
   }
   
   protected void error(Exception e)
   {
      error(ActionBean.writeStackTraceToString(e));
   }   
   
   protected void error(String s)
   {
      message.append(s);
      message.append("\r\n");
      debugger.debug("EmailRules: " + s);      
   }
   
   protected void log(String s)
   {
      if(debug)
      {
         message.append(s);
         message.append("\r\n");
      }
      debugger.debug("EmailRules: " + s);
   }
   
   public void close()
   {
      closeMailstore();
      currentEmailsInAccount = 0;
      currentAccount = 0;
      accounts = null;
      if(debug)
      {
         emailDebug();
      }
   }
   
   public void checkConfig()
   {
      config = EmailRulesConfig.getEmailRulesConfig(configfilepath);
      if(debug)
      {
         log("Wait interval set to "+String.valueOf(config.interval));
      }
   }
   
   protected void init()
   {
      message = new StringBuffer();
      debug = config.getDebug();
      accounts = config.getAccounts();

      Properties sysprops = InitServlet.getSystemParams();
      if(sysprops!=null)
      {
         systemuserid = sysprops.getProperty("SystemUserID");
      }      

  
   }
   
   protected void connectMailstore()
   {
      Account account = accounts[currentAccount];      
      try
      {
         mailstore = new EmailBean();
         mailstore.connect(account.serverAddress, account.serverPort, account.protocol, account.username, account.password);        
         String errors = mailstore.getErrors();
         if(errors!=null && errors.length()!=0)
         {
            error("Could not connected to "+account.username + " at " +account.serverAddress + "\r\n" + errors);
            throw new IllegalConfigurationException(errors);
         }
         currentEmailsInAccount = mailstore.getMessageCount();
         if(debug)
         {
            log("Connected to "+account.username + " at " +account.serverAddress);
            log("Number of emails is mailbox - " + String.valueOf(currentEmailsInAccount));
         }  
      }
      catch(Exception e)
      {
         error("Could not connected to "+account.username + " at " +account.serverAddress + "\r\n" + e.toString());
      }
   }
   
   protected void closeMailstore()
   {
      if(mailstore!=null)
      {
         try{
            mailstore.closePop();
         }catch(Exception e){}
         
         mailstore = null;
         if(debug)
         {
            log("\nClosing connection to mailbox");
         }         
      }
      currentEmail = 0;
   } 
   
   protected void processMessage()
   {  
      firstmatchedfiltertext = null;
      Rule[] rules = accounts[currentAccount].rules;
      boolean success = false;
      for(int i=0; i<rules.length; i++)
      {
         success = processWithRule(rules[i]) | success;
      }
      
      if(mailstore.hasFolders())
      {
         try
         {
            if(success)
            {
               mailstore.moveMessageToFolder("Runway Processed", currentEmail);
            }
            else
            {
               mailstore.moveMessageToFolder("Runway No Match", currentEmail);
            }
            if(debug)
            {
               log("\n\tMOVED server message index "+String.valueOf(currentEmail));
            }
         }
         catch(Exception e)
         {
            log("\n\t\tCould not move message index "+String.valueOf(currentEmail));
            log(e.toString());
         }
      } 
   }
   
   protected boolean processWithRule(Rule rule)
   {
      boolean success = false;
      if(rule.setType.equals(Rule.FiltersType.ALL))
      {
         success = matchsWithAllFilterSets(rule.filterSets);
      }
      else
      {
         success = matchsWithAnyFilterSets(rule.filterSets);      
      }
      if(success)
      {
         if(debug)
         {
            log("\n\tSET MATCH message index " + String.valueOf(currentEmail));
         }         
         success = processActions(rule.actions);  
         if(!success)
         {
            if(debug)
            {
               log("\n\t\tprocessActions FAILED " + String.valueOf(currentEmail));
               for (int a = 0; a < rule.actions.length; ++a) {
               		log("\n\t\t\t" + rule.actions[a].forwardAddress.toString());
               }
            }
         }
      }
      
      if(success && rule.removeFromServer)
      {
         mailstore.setDelete(mailstore.getMessageID(currentEmail));
         if(debug)
         {
            log("\n\tREMOVE from server message index "+String.valueOf(currentEmail));
         }
      }
        
      return(success);
   }
   
   protected String getMatchText(String[] fields)
   {
      if(fields.length==1)
      {
         return(getMatchText(fields[0]));
      }
      StringBuffer temp = new StringBuffer();
      for(int i=0; i<fields.length; i++)
      {
         temp.append(getMatchText(fields[i]));
         temp.append("\r\n");
      }
      return(temp.toString());
   }
   
   protected String getMatchText(String field)
   {  
      if(field.equals(Rule.Field.TO))
      {        
         return(mailstore.getToAddress(currentEmail));
      }
      else if(field.equals(Rule.Field.FROM))
      {          
         return(mailstore.getFromAddress(currentEmail));
      }
      else if(field.equals(Rule.Field.MESSAGE))
      {
    	  String body = mailstore.getBody(currentEmail);
    	  if (body.length() > 0) {
    		  body = body.replaceAll("&lt;","<").replaceAll("&gt;", ">");
    	  }
         return(body);
      }
      else if(field.equals(Rule.Field.SUBJECT))
      {            
         return(mailstore.getSubject(currentEmail));
      }  
      else if(field.equals(Rule.Field.ATTACHMENT))
      {
         return(mailstore.getTextAttachments(currentEmail));
      } 
      else
      {
         errorcount++;
         throw new IllegalConfigurationException("Unknown field - "+ field);
      }
   }   
   
   protected boolean matchsWithFilter(Rule.Filter filter, String emailtext)
   {
      if(filter.text!=null && filter.text.length()!=0)
      {
         boolean match = false;      
         if(emailtext != null)
         {
            if(filter.type.equals(Rule.Filter.Type.BEGINSWITH))
            {
               match = beginsWith(emailtext, filter.text);
            }
            else if(filter.type.equals(Rule.Filter.Type.DOESNOTBEGINSWITH))
            {
               match = !beginsWith(emailtext, filter.text);
            }         
            else if(filter.type.equals(Rule.Filter.Type.ENDSWITH))
            {         
               match = endsWith(emailtext, filter.text);
            }
            else if(filter.type.equals(Rule.Filter.Type.DOESNOTENDSWITH))
            {         
               match = !endsWith(emailtext, filter.text);
            }         
            else if(filter.type.equals(Rule.Filter.Type.CONTAINS))
            {           
               match = contains(emailtext, filter.text);
            }
            else if(filter.type.equals(Rule.Filter.Type.DOESNOTCONTAIN))
            {          
               match = !contains(emailtext, filter.text);
            }
            else if(filter.type.equals(Rule.Filter.Type.IS))
            {          
               match = is(emailtext, filter.text);
            }
            else if(filter.type.equals(Rule.Filter.Type.ISNOT))
            {           
               match = !is(emailtext, filter.text);
            }     
            else if(filter.type.equals(Rule.Filter.Type.REGEX))
            {            
               match = regex(emailtext, filter.pattern);
            }  
            else if(filter.type.equals(Rule.Filter.Type.NOTREGEX))
            {            
               match = !regex(emailtext, filter.pattern);
            }          
            else
            {
               errorcount++;
               throw new IllegalConfigurationException("Unknown Filter type - "+ filter.type);
            }
         }
         
         if(match && firstmatchedfiltertext==null)
         {
            firstmatchedfiltertext = filter.text;
            if(debug)
            {
               log("\n\tFIRST MATCH set TO " + filter.text + " FOR message index " + String.valueOf(currentEmail));
            }
         }
         
         if(debug)
         {
            if(match)
            {
               log("\n\t\tPARTIAL MATCH found FOR " + filter.text + " IN message index " + String.valueOf(currentEmail));
            }
            else
            {
               //log("NO MATCH found for " + filter.text + " IN message index " + String.valueOf(currentEmail));
            }
         }      
         
         return(match);
      }
      else
      {
         errorcount++;
         throw new IllegalConfigurationException("Missing Filter text");
      }         
   }
   
   protected boolean beginsWith(String s, String m)
   {
      return(s.indexOf(m)==0);
   }
   
   protected boolean endsWith(String s, String m)
   {
      return(s.endsWith(m));
   }  
  
   protected boolean contains(String s, String m)
   {
      return(s.indexOf(m)>=0);
   }   
   
   protected boolean is(String s, String m)
   {
      return(s.equals(m));
   }    
      
   protected boolean regex(String s, Pattern p)
   {
      if(p == null)
      {
         throw new IllegalConfigurationException("Missing pattern");
      }

      Matcher matcher = p.matcher(s);        
      boolean result = matcher.find();
      if(debug)
      {
         log("\n\t\tRegex "+ p.pattern() + " matches " +String.valueOf(result));
      }
      return(result);
   }     
   
   protected boolean matchsWithFilterSet(Rule.FilterSet filterset)
   {
      if((filterset.filterSets==null || filterset.filterSets.length==0)
            && (filterset.filters == null || filterset.filters.length==0))
      {
         throw new IllegalConfigurationException("FilterSet contains no filterSets or filters");
      }
      if(filterset.setType.equals(Rule.FiltersType.ALL))
      {
         if(filterset.filterSets!=null && filterset.filterSets.length!=0 && filterset.filters != null && filterset.filters.length!=0)
         {
            return(matchsWithAllFilterSets(filterset.filterSets) && matchsWithAllFilters(filterset.filters));
         }
         else if(filterset.filterSets!=null && filterset.filterSets.length!=0)
         {
            return(matchsWithAllFilterSets(filterset.filterSets));
         }
         else
         {
            return(matchsWithAllFilters(filterset.filters));
         }
      }
      else
      {
         if(filterset.filterSets!=null && filterset.filterSets.length!=0 && filterset.filters != null && filterset.filters.length!=0)
         {
            return(matchsWithAnyFilterSets(filterset.filterSets) || matchsWithAnyFilters(filterset.filters));
         }
         else if(filterset.filterSets!=null && filterset.filterSets.length!=0)
         {
            return(matchsWithAnyFilterSets(filterset.filterSets));
         }         
         else
         {
            return(matchsWithAnyFilters(filterset.filters));
         }         
      }
   }     
   
   protected boolean matchsWithAllFilterSets(Rule.FilterSet[] filterSets)
   {
      for(int i=0; i<filterSets.length; i++)
      {
         if(!matchsWithFilterSet(filterSets[i]))
         {
            return(false);
         }
      }
      return(true);
   }
   
   protected boolean matchsWithAnyFilterSets(Rule.FilterSet[] filterSets)
   {
      for(int i=0; i<filterSets.length; i++)
      {
         if(matchsWithFilterSet(filterSets[i]))
         {
            return(true);
         }
      }      
      return(false);
   }    
   
   protected boolean matchsWithAllFilters(Rule.Filter[] filters)
   {
      String[] lastmatchon = null;
      String emailtext = null;
      boolean result = true;
      for(int i=0; i<filters.length; i++)
      {
         if(!equals(lastmatchon, filters[i].fields))
         {   
            emailtext = getMatchText(filters[i].fields);
            emailtext = emailtext.toLowerCase();
         }
         lastmatchon = filters[i].fields;
         
         if(!matchsWithFilter(filters[i], emailtext))
         {
            result = false;
            break;
         }
      }
      if(debug && !result)
      {
         //log("*****************************************");
         //log("No match found in:");
         //log(emailtext);
         //log("*****************************************");
      }
      return(result);
   }
   
   protected boolean equals(String[] a, String[] b)
   {
      if(a!=null && b!=null)
      {
         if(a.length == b.length)
         {
            for(int i=0; i<a.length; i++)
            {
               if(!a.equals(b))
               {
                  return(false);
               }
            }
            return(true);
         }
      }
      return(false);
   }

   protected boolean matchsWithAnyFilters(Rule.Filter[] filters)
   {
      String[] lastmatchon = null;
      String emailtext = null;     
      boolean result = false;
      for(int i=0; i<filters.length; i++)
      {
         if(!equals(lastmatchon, filters[i].fields))
         {   
            emailtext = getMatchText(filters[i].fields);
            emailtext = emailtext.toLowerCase();
         }
         lastmatchon = filters[i].fields;         
         if(matchsWithFilter(filters[i], emailtext))
         {
            result = true;
            break;
         }
      }
      if(debug && !result)
      {
         //log("*****************************************");
         //log("No match found in:");
         //log(emailtext);
         //log("*****************************************");
      }      
      return(result);
   }   
   
   protected boolean processActions(Rule.Action[] actions)
   {
      boolean success = false;
      for(int i=0; i<actions.length; i++)
      {
         if(success)
         {
            processAction(actions[i]);
         }
         else
         {
            success = processAction(actions[i]);
         }
      }
      return(success);
   }
   
   protected boolean processAction(Rule.Action action)
   {   
      boolean success = false;
      GenRow row = null;
      
      if(action.matches != null && action.matches.length != 0)
      {
         row = processActionMatches(action);
         if(row == null)
         {
      		log("\n\t\tNo Row for ACTION " + action.matches.toString());
            return(false);
         }
      }

      success = processActionInsertResponse(action, row) | success;
      success = processActionInsertNote(action, row) | success;
      success = processActionUpdateContact(action, row) | success;  
      
      String address = action.forwardAddress;
      if(address!=null && address.length()!=0)
      {
         if(!success)
         {
            try
            {
               forwardEmail(address);
            }
            catch(Exception e)
            {
               log("\n\t\tCould not forward email:");
               log(e.toString());
            }
         }
      }
      return(success);
   }
   
   protected boolean processActionInsertResponse(Rule.Action action, GenRow relateddata)
   {
      if(action.insertResponse!=null)
      {
	      log("\n\t\tTRY ACTION Insert Response " + relateddata.toString());
         GenRow row = new GenRow();
         row.setTableSpec("Responses");
         row.createNewID();
         row.setParameter("CreatedBy", systemuserid);
         row.setParameter("CreatedDate","NOW");
         row.setParameter("ModifiedDate","NOW");
         if(relateddata != null)
         {
            if(relateddata.getTableSpec().getTableName().equals("Notes"))
            {
               row.setParameter("NoteID", relateddata.getString("NoteID"));
               row.setParameter("ContactID", relateddata.getString("ContactID"));
               row.setParameter("CompanyID", relateddata.getString("CompanyID"));
               row.setParameter("ModifiedBy", relateddata.getString("RepUserID"));
            }
            else if(relateddata.getTableSpec().getTableName().equals("Contacts"))
            {
               row.setParameter("ContactID",relateddata.getString("ContactID"));
               row.setParameter("ModifiedBy", relateddata.getString("RepUserID"));
            }
         }
         if(firstmatchedfiltertext!=null)
         {
            if(firstmatchedfiltertext.length()>50)
            {
               firstmatchedfiltertext = firstmatchedfiltertext.substring(0,45);
               firstmatchedfiltertext = firstmatchedfiltertext + "...";
            }
            row.setParameter("Type", firstmatchedfiltertext);
         }
         if(action.insertResponse.fields!=null)
         {
            for(int i=0; i<action.insertResponse.fields.length; i++)
            {
               row.setParameter(action.insertResponse.fields[i].name, action.insertResponse.fields[i].value);
            }
         }      
         try
         {
            row.doAction("insert");
         }
         catch(Exception e)
         {
            errorcount++;
            error(e);
         }
         if(row.isSet("Description") && row.getParameter("Description").startsWith("Email Bounce")
        		 && relateddata.isSet("NoteID")) { 
        	 GenRow cerow = new GenRow();
             cerow.setTableSpec("ContactEvents");
             cerow.setParameter("-select1","EventID");
             cerow.setParameter("SendEmailID", relateddata.getString("NoteID"));
             try
             {
             	cerow.doAction("selectfirst"); 
             }
             catch(Exception e)
             {
                errorcount++;
                error(e);
             }
             if(cerow.isSuccessful()) 
             { 
             	 cerow.setParameter("ModifiedBy", systemuserid);
             	 cerow.setParameter("ModifiedDate","NOW");    
             	 cerow.setParameter("SendOutcome","Bounced"); 
                 try
                 {
                 	cerow.doAction("update");
                 }
                 catch(Exception e)
                 {
                    errorcount++;
                    error(e);
                 }
             } 
         }
         log("\n\t\tACTION Insert Response " + row.isSuccessful());
         
         return(row.isSuccessful());
      }
      return(false);
   }
   
   protected boolean processActionInsertNote(Rule.Action action, GenRow relateddata)
   {
      if(action.insertNote!=null)
      {
      log("\n\t\tTRY ACTION Insert Note " + relateddata.toString());
         GenRow row = mailstore.getEmailAsGenRow(currentEmail);
         row.setParameter("CreatedBy", systemuserid);
         row.setParameter("ModifiedBy", systemuserid);
         
         if(relateddata != null)
         {
            if(relateddata.getTableSpec().getTableName().equals("Notes"))
            {
               row.setParameter("ParentNoteID", relateddata.getString("NoteID"));
               row.setParameter("ContactID", relateddata.getString("ContactID"));
               row.setParameter("CompanyID", relateddata.getString("CompanyID"));      
               row.setParameter("GroupID", relateddata.getData("GroupID"));
               row.setParameter("CampaignID", relateddata.getString("CampaignID"));
               row.setParameter("RepUserID", relateddata.getString("RepUserID"));    
            }
            else if(relateddata.getTableSpec().getTableName().equals("Contacts"))
            {
               row.setParameter("ContactID",relateddata.getString("ContactID"));
               row.setParameter("RepUserID",relateddata.getString("RepUserID"));
               row.setParameter("GroupID",relateddata.getData("GroupID"));
            }
         }
                  
         if(action.insertNote.fields!=null)
         {
            for(int i=0; i<action.insertNote.fields.length; i++)
            {
               row.setParameter(action.insertNote.fields[i].name, action.insertNote.fields[i].value);
            }
         }
         try
         {
            row.doAction("insert");
         }
         catch(Exception e)
         {
            errorcount++;
            error(e);
         }
         log("\n\t\tACTION Insert Note " + row.isSuccessful());
         return(row.isSuccessful());
      }
      return(false);
   }

   protected boolean processActionUpdateContact(Rule.Action action, GenRow relateddata)
   {
      if(action.updateContact!=null)
      {
	      log("\n\t\tTRY ACTION Update Contact " + relateddata.toString());
         if(relateddata != null && relateddata.getString("ContactID").length()!=0)
         {
            GenRow row = new GenRow();
            row.setTableSpec("Contacts");
            row.setParameter("ModifiedBy", systemuserid);
            row.setParameter("ModifiedDate","NOW");         
            row.setParameter("ContactID", relateddata.getString("ContactID"));
            if(action.updateContact.fields!=null)
            {
               for(int i=0; i<action.updateContact.fields.length; i++)
               {
                  row.setParameter(action.updateContact.fields[i].name, action.updateContact.fields[i].value);
               }
            }    
            try
            {
               row.doAction("update");
            }
            catch(Exception e)
            {
               errorcount++;
               error(e);
            }
            log("\n\t\tACTION Update Contact " + row.isSuccessful());
            return(row.isSuccessful());
         }
      }
      return(false);
   }   
   
   protected GenRow processActionMatches(Rule.Action action)
   {
      GenRow row = null;
      for(int i=0; i<action.matches.length; i++)
      {
         row = processActionMatch(action.matches[i]);
         if(row!=null)
         {
            return(row);
         }
      }
      return(row);
   }
   
   protected GenRow processActionMatch(Rule.Action.Match match)
   {
      GenRow row = null;      
      if("NoteID".equals(match.type))
      {
         row = processActionNoteIDMatch(match);
      }
      else if("Email".equals(match.type))
      {
         row = processActionEmailMatch(match);
      }
      else
      {
         errorcount++;
         throw new IllegalConfigurationException("Missing Match type");
      }  
      return(row);      
   }
   
   protected GenRow processActionNoteIDMatch(Rule.Action.Match match)
   {
      GenRow row = null;  
      String noteid = findMatch(match);
      if(noteid!=null && noteid.length()!=0)
      {
         row = getNoteWithID(noteid);
      }
      return(row); 
   }
   
   protected GenRow processActionEmailMatch(Rule.Action.Match match)
   {
      GenRow row = null;  
      String email = findMatch(match);
      if(email!=null && email.length()!=0)
      {
         row = getContactWithEmail(email);
      }      
      return(row); 
   }   
   
   protected boolean forwardEmail(String address) throws MessagingException
   {
      return(forwardEmail("Fwd: ", address));
   }
   
   protected boolean forwardEmail(String subjectprefix, String address) throws MessagingException
   {
         MailerBean mailer = mailstore.getForwardMailerBean(currentEmail);
         ServletConfig sc = InitServlet.getConfig();
         if(sc!=null)
         {
            mailer.setServletContext(InitServlet.getConfig().getServletContext());
         }
         else
         {
            mailer.setHost("10.144.1.7");
         }

         mailer.setFrom("Runway<info@switched-on.com.au>");
         mailer.setTo(address);
         mailer.setSubject(subjectprefix + mailer.getSubject());
         String response = mailer.getResponse();
         if(debug)
         {
            log("\n\t\tACTION forwardEmail "+ mailer.getSubject() + " to " + mailer.getTo() + " " + response);
         }            
         return(MailerBean.STATUS_SENT.equals(response));            

   }
   
   protected String findMatch(Rule.Action.Match match)
   {
      String str = null;
      String text = getMatchText(match.fields);
      Matcher matcher = match.pattern.matcher(text);
      
      if(matcher.find())
      {
         str = matcher.group(match.group);          
      }

      log("\n\t\tACTION matcher " + matcher.toString() + " to " + str);
      log("\n--------------------------------------------------------------------------------------------------------------------");
      log("\n" + text);
      log("\n--------------------------------------------------------------------------------------------------------------------");
      
      return(str);
   }
   
   protected GenRow getContactWithEmail(String email)
   {
      try
      {
         GenRow row = new GenRow();
         row.setTableSpec("Contacts");
         row.setParameter("Email", email);      
         row.setParameter("-select1", "FirstName"); 
         row.setParameter("-select2", "LastName"); 
         row.setParameter("-select3", "ContactID");
         row.setParameter("-select4", "CompanyID");      
         row.setParameter("-select5", "GroupID");
         row.setParameter("-select6", "RepUserID"); 
         row.doAction("selectfirst");
         if(row.isSuccessful())
         {
             log("\n\t\tACTION Found Contact " + row.getString("FirstName") + " " + row.getString("LastName"));
            return(row);
         } else {
             log("\n\t\tACTION Not Found Contact " + email);
		 }         
      }
      catch(Exception e)
      {
         errorcount++;
         log("\n\t\tCould not look up contact with email "+email + "\r\n"+ e.getMessage());
      }
      return(null);
   }
   
   protected GenRow getNoteWithID(String noteid)
   {
      try
      {
         GenRow row = new GenRow();
         row.setTableSpec("Notes");
         row.setParameter("NoteID", noteid);      
         row.setParameter("-select1", "NoteID");
         row.setParameter("-select2", "ContactID");
         row.setParameter("-select3", "CompanyID");      
         row.setParameter("-select4", "GroupID");
         row.setParameter("-select5", "CampaignID");
         row.setParameter("-select6", "RepUserID");      
         row.setParameter("-select6", "Subject");      
         row.doAction("select");
         if(row.isSuccessful())
         {
             log("\n\t\tACTION Found Note " + row.getString("Subject"));
            return(row);
         } else {
             log("\n\t\tACTION Not Found Note " + noteid);
         }
      }
      catch(Exception e)
      {
         errorcount++;
         log("\n\t\tCould not look up note with ID "+noteid + "\r\n"+ e.getMessage());
      }
      return(null);
   }   
   
   protected void emailDebug()
   {    
      String address = config.getDebugAddress();
      if(address!=null && address.length()!=0)
      {
         ServletConfig sc = InitServlet.getConfig();
         if(sc!=null)
         {
            MailerBean mailer = new MailerBean();
            mailer.setServletContext(sc.getServletContext());
            mailer.setFrom("Runway<info@switched-on.com.au>");
            mailer.setTo(address);
            mailer.setSubject("Debug message from EmailRules");
            mailer.setTextbody(message.toString());
            mailer.getResponse();
         }
         else
         {
            System.out.println(message);
         }
      }
   }
   
   public boolean processNext()
   {
      try
      {
         if(accounts == null)
         {           
            init();
            //log("init");
         }
         if(currentEmail == 0)
         {           
            connectMailstore();
            //log("connectMailstore");
         }
         if(currentEmail < currentEmailsInAccount)
         {
            try
            {            
               processMessage();
               //log("processMessage");
               if(debug)
               {
                  log("\nProcessing account index " + String.valueOf(currentAccount) + " message index " + String.valueOf(currentEmail));
               }
            }
            catch(Exception e)
            {
               error(e);
               errorcount++;
            }
            currentEmail++;
         }
         else
         {             
            closeMailstore();
            //log("closeMailstore");
            currentAccount++;
         }
         if(currentAccount >= accounts.length)
         {
            close();
            //log("close");
            return(false);
         }   
      }
      catch(Exception e)
      {
         error(e);
         errorcount++;
         return(false);
      }
      
      return(true);      
   }
   
   public int getProcessSize() {
      return this.currentEmailsInAccount;
   }
   
   public int getProcessedCount() {
      return this.currentEmail;
   }   

   public int getErrorCount() {
      return errorcount;
   }     
   
   public DaemonWorkerConfig getWorkerConfig()
   {
      return(config);
   }   
   
   public static void main(String[] args)
   {

      try
      {
         EmailRules rules = new EmailRules("/usr/local/tomcat/webapps/runway/");
        //System.out.println("init");
         while (rules.processNext())
         {
            System.out.println("Next");
         }

         System.out.println("close" + String.valueOf(rules.errorcount));
      }
      catch(Exception e)
      {
         System.out.println(ActionBean.writeStackTraceToString(e));
      }

   }



}
