package com.sok.runway.emailRule;

import org.dom4j.*;

import com.sok.runway.*;
import com.sok.framework.*;
   
public class Account
{
   protected static final String _rule = "rule";

   protected static final String _username = "username";
   protected static final String _password = "password";
   protected static final String _protocol = "protocol";   
   protected static final String _serverAddress = "serverAddress";
   protected static final String _serverPort = "serverPort";      
   
   public String username;
   public String password;
   public String serverAddress;
   public String serverPort;
   public String protocol;
     
   public Rule[] rules;
   
   public Account(Element ele)
   {
      username = ele.attributeValue(_username);
      password = ele.attributeValue(_password);
      serverAddress = ele.attributeValue(_serverAddress);
      serverPort = ele.attributeValue(_serverPort);
      protocol = ele.attributeValue(_protocol);      
      setRules(ele);         
   }
   
   public void setRules(Element parent)
   {
      Element[] eles = XMLSpec.getElementArray(parent, _rule);
      rules = new Rule[eles.length];
      for(int i=0; i<eles.length; i++)
      {
         rules[i] = new Rule(eles[i]);
      }
   } 
}