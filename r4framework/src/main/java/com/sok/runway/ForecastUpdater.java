package com.sok.runway;
import javax.servlet.*;     
import javax.servlet.http.*;
import java.util.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import java.text.*;
import com.sok.framework.*;
import com.sok.framework.sql.*;

public class ForecastUpdater extends OfflineProcess
{
   
	Connection con = null;
	Context ctx = null;
	
	StringBuffer status = new StringBuffer();
	String email = null;
	
	String userid = null;
	ArrayList items = new ArrayList();
	
	int proccessed = 0;
	
   HttpSession session = null;
   
	public ForecastUpdater(HttpServletRequest request, ServletContext context)
	{
		super(request, context);
	   session = request.getSession();
		UserBean user = (UserBean)session.getAttribute(SessionKeys.CURRENT_USER);
		this.userid = user.getCurrentUserID();
		this.email = user.getEmail();
		start();
	}
	
	void getConnection() throws Exception {
		if(con == null || con.isClosed()) {
			try {
				if(ctx == null) {
					ctx = new InitialContext();
				}

				DataSource ds = null;
				try {
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource) envContext.lookup(dbconn);
				}
				catch(Exception e) { }
				if(ds == null) {
					ds = (DataSource) ctx.lookup(dbconn);
				}
				con = ds.getConnection();
			}
			finally {
				try {
					ctx.close();
					ctx = null;
				}
				catch(Exception e) { }
			}
		}
	}
	
	void retrieveItems(StringBuffer stmtstring)throws Exception
	{
		Exception e = null;
		ResultSet current = null;
		try
		{
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					     ResultSet.CONCUR_UPDATABLE);
			current = stmt.executeQuery(stmtstring.toString());
			
			while(current.next()) {
				items.add(current.getString(1));
			}
		}
		catch (Exception ex) {
			e = ex;
		}
		finally {
    		if (current != null) {
				try{
            	current.close();
        		}catch (Exception ex){}
    		}
		}
		if(e!=null) {
			throw(e);
		}	
	}

	void updateForecasts(String mode) throws Exception {
	   Iterator iter = items.iterator();
	   
	   String field = null;
	   if (mode.equals("Companies")) {
	      field = "CompanyID";
	   }
	   else if (mode.equals("Contacts")) {
	      field = "ContactID";
	   }
	   String id = null;
	   while (iter.hasNext()) {
	      id = (String)iter.next();
	      updateForecast(id , field);
	      proccessed++;
	   }
	}

	private void updateForecast(String id, String fieldName) {
	   String error = updateForecast(con, id, fieldName);
	   
	   if (error.length() > 0) {
   	   status.append("\r\nError at number ");
   	   status.append(proccessed);
   	   status.append("\r\n\r\n");
   	   status.append(error);
   	   status.append("\r\n\r\n");
   	}
	}
	
	public static String updateForecast(Connection con, String id, String fieldName) {
	   
	   SqlDatabase database = ActionBean.getDatabase();
	   
      RowBean bean = new RowBean();
      bean.setConnection(con);
      
      StringBuffer sql = new StringBuffer();
      sql.append("UPDATE Forecasts set CalculationDate = ");
      sql.append(database.getCurrentDateMethod());
      sql.append(", Actual = (SELECT SUM(TotalCost) ");
                                 sql.append("FROM Orders ");
                           sql.append("INNER JOIN OrderStatus ON OrderStatus.OrderStatusID = Orders.OrderStatusID ");
                           sql.append("WHERE Orders.CompanyID = Forecasts.CompanyID AND OrderStatus.StatusID = Forecasts.OrderStatusID ");
                           sql.append("AND Orders.DateOfSale >= Forecasts.StartDate AND Orders.DateOfSale <= Forecasts.EndDate) ");
      sql.append("WHERE OrderStatusID LIKE '_%' AND ");
   
      sql.append(fieldName);
      sql.append(" = '");
      sql.append(id);
      sql.append("'; ");
      bean.setAction("execute");
   	bean.setSearchStatement(sql.toString());
   	bean.doAction();
   	
   	StringBuffer sql2 = new StringBuffer();
      sql2.append("UPDATE Forecasts set CalculationDate = ");
      sql2.append(database.getCurrentDateMethod());
      sql2.append(", Actual = (SELECT count(*) ");
                                                   sql2.append("FROM Notes ");
                                                   sql2.append("WHERE Notes.CompanyID = Forecasts.CompanyID AND Notes.Type = Forecasts.NoteType ");
                                                   sql2.append("AND Notes.NoteDate >= Forecasts.StartDate AND Notes.NoteDate < Forecasts.EndDate)");
      sql2.append("WHERE NoteType LIKE '_%' AND ");
      sql2.append(fieldName);
      sql2.append(" = '");
      sql2.append(id);
      sql2.append("'; ");
   	bean.setSearchStatement(sql2.toString());
   	bean.doAction();

      StringBuffer sql3 = new StringBuffer();
      sql3.append("UPDATE Forecasts set Percentage = (Actual/Value)*100 WHERE ");
      sql3.append(fieldName);
      sql3.append(" = '");
      sql3.append(id);
      sql3.append("'; ");
      
   	bean.setSearchStatement(sql3.toString());
   	bean.doAction();
   	
   	return bean.getError();   	
   }
   

	public void execute()
	{
		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, getLocale());
			dt.setTimeZone(TimeZone.getTimeZone("Australia/Melbourne"));
		status.append(context.getServletContextName());
		status.append(" Runway offline process - Forecast Update\r\n");
		status.append("Initiated ");
		status.append(dt.format(new java.util.Date()));
		status.append("\r\n");

		String mode = requestbean.getString("-mode");
		if(mode == null)
		{
			mode = "Companies";
		}

		StringBuffer searchstmt = new StringBuffer();
		try
		{
		   com.sok.framework.sql.SqlDatabase database = ActionBean.getDatabase();
			TableData searchbean = null;
			
			if(mode.equals("Contacts")) {
				searchbean = (TableData)session.getAttribute(SessionKeys.CONTACT_SEARCH);
            searchstmt.append("select ");
            if (database instanceof com.sok.framework.sql.MSSQL) {
               database.appendTopStatement(searchstmt, searchbean.getTop());
            }
				searchstmt.append(" Contacts.ContactID from Contacts where ");
			}
			else if(mode.equals("Companies")) {
				searchbean = (TableBean)session.getAttribute(SessionKeys.COMPANY_SEARCH);
            searchstmt.append("select ");
            if (database instanceof com.sok.framework.sql.MSSQL) {
               database.appendTopStatement(searchstmt, searchbean.getTop());
            }
				searchstmt.append(" Companies.CompanyID from Companies where ");
			}
			searchstmt.append(searchbean.getSearchCriteria());
         if (database instanceof com.sok.framework.sql.MySQL) {
            searchstmt.append(" ");
            database.appendTopStatement(searchstmt, searchbean.getTop());
         }
         
         try {
			   getConnection();
		      retrieveItems(searchstmt);
		      updateForecasts(mode);
			
   			status.append("Forecasts updated: ");
   			status.append(String.valueOf(proccessed));
   			status.append(" of ");
   			status.append(String.valueOf(items.size()));
   			if(mode.equals("Contacts")) {
   				status.append(" contacts.\r\n");
   			}
   			else if(mode.equals("Companies")) {
   				status.append(" companies.\r\n");
   			}
			}
   		catch(Exception ex) {
   			status.append("Offline process halted on ");
   			status.append(String.valueOf(proccessed));
   			status.append(" of ");
   			status.append(String.valueOf(items.size()));
   			status.append(".\r\n\r\n");
   			status.append(ActionBean.writeStackTraceToString(ex));
   		}
   		finally {
   			if(con != null) {
   				try {
   					con.close();
   					con = null;
   				}
   				catch(Exception ex2) { }
   			}
   			if(ctx != null) {
   				try {
   					ctx.close();
   					ctx = null;
   				}
   				catch(Exception ex2) { }
   			}
   		}
			
   		if (isKilled()) {
   	      status.append("Offline process terminated by - ");
   	      status.append(getKiller());
   	      status.append("\r\n\r\n");
   	   }
   	   else {	   
   	      status.append("Offline process completed.\r\n\r\n");
   	   }
		}
		catch(Exception e)
		{
			status.append("Offline process halted on ");
			status.append(String.valueOf(proccessed));
			status.append(" of ");
			status.append(String.valueOf(items.size()));
			status.append(".\r\n\r\n");
			status.append(e.toString());
		   if (e instanceof java.sql.SQLException) {
		      status.append(searchstmt.toString());
		   }
		}
	}

	public String getStatusMailBody()
	{
		return(status.toString());
	}

	public String getStatusMailSubject()
	{
		return(context.getServletContextName() + " Runway offline process - Forecast Update");
	}

	public String getStatusMailRecipient()
	{
		return(email);
	}
	
	
   public int getProcessSize() {
      return items.size();
   }
   
   public int getProcessedCount() {
      return proccessed;
   }
   
   public int getErrorCount() {
      return -1;
   }
}
