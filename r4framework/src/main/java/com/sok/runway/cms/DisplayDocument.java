package com.sok.runway.cms;

import com.sok.runway.*;
import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class DisplayDocument extends PageDocument {
   
   public static final String EDIT_DOCUMENT_ID = "EDIT";
   
   private boolean insertMode = false;
   
   DisplayPage parent = null;
   
   public DisplayDocument(DisplayPage parent) {
      super(parent.getConnection());
      this.parent = parent;
   }
   
   public DisplayDocument(DisplayPage parent, String pageDocumentID) {
      super(parent.getConnection(), pageDocumentID);
      this.parent = parent;
   }
   
   public boolean load(String pk) {
      if (EDIT_DOCUMENT_ID.equals(pk)) {
         clear();
         setField("FileName","Click here to add Document");
         insertMode = true;
      }
      else {
         super.load(pk);
         insertMode = false;
      }
      return isLoaded();
   }
   
   public String getFileName() {
      if (parent.getEditMode() == DisplayPage.MODE_EDITING) {
         StringBuffer editLink = new StringBuffer();
         
         editLink.append("<span class=\"");
         if (insertMode) {
            editLink.append(Site.CSS_STYLE_INSERT);
         }
         else {
            editLink.append(Site.CSS_STYLE_EDIT);
         }
         editLink.append("\"><a target=\"_blank\" href=\"");
            
         editLink.append(parent.getContextPath());
         editLink.append("/");
         editLink.append(Site.EDIT_PATH);
            
         editLink.append("?SitePageDocumentID=");
         editLink.append(getSitePageDocumentID());
         
         if (insertMode) {
            editLink.append("&SitePageID=");
            editLink.append(getSitePageID());
         }
         editLink.append("\">");
         
         if (super.getFileName().length() == 0) {
            editLink.append("&lt;No Name&gt;");
         }
         else {
            editLink.append(super.getFileName());
         }
         editLink.append("</a></span>");
   
         return editLink.toString();
      }
      else {
         return super.getFileName();
      }
   }
   
}