package com.sok.runway.cms;

/**
 * @deprecated CMS2
 */
public class HTMLUtil {


   public static String buildLink(String url, String label) {
      StringBuffer link = new StringBuffer();
      link.append("<a href=\"");
      link.append(url);
      link.append("\">");
      link.append(label);
      link.append("</a>");
      return link.toString();
   }
   
   public static String buildImageLink(String url, String imagePath) {
      StringBuffer link = new StringBuffer();
      link.append("<a href=\"");
      link.append(url);
      link.append("\">");
      link.append(buildImageTag(imagePath));
      link.append("</a>");
      return link.toString();
   }
   
   public static String buildRollOverLink(String url, String imagePath, String rollOverPath, String id) {
      StringBuffer link = new StringBuffer();
      link.append("<a href=\"");
      link.append(url);
      link.append("\">");
      link.append(buildRollOverTag(imagePath, rollOverPath, id));
      link.append("</a>");
      return link.toString();
   }

   public static String buildImageTag(String imagePath) {
      return buildImageTag(imagePath, "", "");
   }
   
   public static String buildImageTag(String imagePath, String width, String height) {
      StringBuffer tag = new StringBuffer();

      tag.append("<img src=\"");
      tag.append(imagePath);
      tag.append("\" border=\"0\"");
      if (width.length() > 0) {
         tag.append(" width=\"");
         tag.append(width);
         tag.append("\"");
      }
      if (height.length() > 0) {
         tag.append(" height=\"");
         tag.append(height);
         tag.append("\"");
      }
      tag.append("/>");
      
      return tag.toString();
   }
   
   public static String buildFlashTag(String imagePath, String width, String height) {
      StringBuffer tag = new StringBuffer();
      tag.append("<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0\" width=\"");
      tag.append(width);
      tag.append("\" height=\"");
      tag.append(height);
      tag.append("\">");
      tag.append("<param name=\"movie\" value=\"");

      tag.append(imagePath);
      tag.append("\">");

      tag.append("<param name=\"quality\" value=\"high\">");
      
      tag.append("<embed src=\"");
      tag.append(imagePath);
      tag.append("\"");
   
      tag.append(" quality=\"high\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" width=\"");
      tag.append(width);
      tag.append("\" height=\"");
      tag.append(height);
      tag.append("\"></embed>");
      
      
      tag.append("</object>");
      
      return tag.toString();
   }
   
   
   public static String buildRollOverTag(String imagePath, String rollOverPath, String id) {
      if (rollOverPath == null) {
         return buildImageTag(imagePath);
      }
      else {
         StringBuffer tag = new StringBuffer();
   
         tag.append("<img src=\"");
         tag.append(imagePath);
         tag.append("\" border=\"0\" ");
   
         tag.append("name=\"");
         tag.append(id);
         tag.append("\" ");
         tag.append("id=\"");
         tag.append(id);
         tag.append("\" ");
         
         tag.append("onMouseOver=\"MM_swapImage('");
         tag.append(id);
         tag.append("','','");
         tag.append(rollOverPath);
         tag.append("',1)\"");
         
         tag.append("onMouseOut=\"MM_swapImgRestore()\"");
         tag.append("/>");
         
         return tag.toString();
      }
   }
}
