package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class Navigation extends RunwayEntity {
   
   boolean liveLinksOnly = true;
      
   public Navigation(Connection con, boolean liveLinksOnly) {
      super(con, "cms/SiteNavigationView");
      this.liveLinksOnly = liveLinksOnly;
   }
   
   public Navigation(Connection con, String navigationID, boolean liveLinksOnly) {
      this(con, liveLinksOnly);
      load(navigationID);
   }
   
   public static Collection getNavigationIDs(Connection con, String siteID) {
      ArrayList ids = new ArrayList();
      
      RowSetBean navList = new RowSetBean();
      navList.setViewSpec("cms/SiteNavigationIDView");
      navList.setConnection(con);
      navList.setColumn("SiteID", siteID);
   	navList.sortBy("Name",0);
      navList.generateSQLStatement();
      navList.getResults();
      
      if (navList.getError().length() > 0) {
         throw new RuntimeException(navList.getError());
      }
      
      while (navList.getNext()) {
         ids.add(navList.getString("SiteNavigationID"));
      }
      navList.close();
      return ids;
   }
   
   public Collection getNavigationLinkIDs() {
      ArrayList ids = new ArrayList();
      
      RowSetBean navLinkList = new RowSetBean();
      navLinkList.setViewSpec("cms/SiteNavigationLinkIDView");
      navLinkList.setConnection(getConnection());
      navLinkList.setColumn("SiteNavigationID", getSiteNavigationID());
      if (liveLinksOnly) {
         navLinkList.setColumn("Live", "Y");
      }
   	navLinkList.sortBy("SortOrder",0);
      navLinkList.generateSQLStatement();
      navLinkList.getResults();
      
      if (navLinkList.getError().length() > 0) {
         throw new RuntimeException(navLinkList.getError());
      }
      
      while (navLinkList.getNext()) {
         ids.add(navLinkList.getString("SiteNavigationLinkID"));
      }
      navLinkList.close();
      return ids;
   }
   
   public Site getSite() {
      return new Site(getConnection(), getSiteID());
   }
	
   public String getSiteNavigationID() {
      return getField("SiteNavigationID");
   }
	
   public String getSiteID() {
      return getField("SiteID");
   }
   
   public String getName() {
      return getField("Name");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("Navigation");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SiteNavigationID","SYSTEM ERROR - SiteNavigationID");
         val.addMandatoryField("Name","Navigation must have a name.");

         Validator.addValidator("Navigation", val);
      }
      return val;
   }
   
}