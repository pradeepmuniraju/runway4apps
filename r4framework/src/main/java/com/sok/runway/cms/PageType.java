package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.crm.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class PageType extends RunwayEntity {
   
   //private RowBean pageTypeBean = new RowBean();
   
   //private Connection con = null;
      
   public PageType(Connection con) {
      super(con, "cms/SitePageTypeView");
      //this.con = con;
      //pageTypeBean.setViewSpec("cms/SitePageTypeView");
      //pageTypeBean.setConnection(con);
   }
   
   public PageType(Connection con, String pageTypeID) {
      this(con);
      load(pageTypeID);
      //this(con);
      //loadPageType(pageTypeID);
   }
   
   /*public void loadPageType(String pageTypeID) {
      pageTypeBean.clear();
      pageTypeBean.setColumn("SitePageTypeID", pageTypeID);
      pageTypeBean.setAction("select");
      pageTypeBean.doAction();
      if (pageTypeBean.getError().length() > 0) {
         throw new RuntimeException(pageTypeBean.getError());
      }
   }*/
   
   public static Collection getPageTypeIDs(Connection con, String siteID) {
      ArrayList ids = new ArrayList();
      
      RowSetBean pageTypeList = new RowSetBean();
      pageTypeList.setViewSpec("cms/SitePageTypeIDView");
      pageTypeList.setConnection(con);
      pageTypeList.setColumn("SiteID", siteID);
   	pageTypeList.sortBy("Name",0);
      pageTypeList.generateSQLStatement();
      pageTypeList.getResults();
      
      if (pageTypeList.getError().length() > 0) {
         throw new RuntimeException(pageTypeList.getError());
      }
      
      while (pageTypeList.getNext()) {
         ids.add(pageTypeList.getString("SitePageTypeID"));
      }
      pageTypeList.close();
      return ids;
   }
   
   public Site getSite() {
      return new Site(getConnection(), getSiteID());
   }
   
   public Collection getPageTypeElementsIDs() {
      if (getField("SitePageTypeID").length() > 0) {
         return PageTypeElement.getPageTypeElementIDs(getConnection(), getField("SitePageTypeID"));
      }
      else {
         return null;
      }
   }
   
   /*public void clear() {
      pageTypeBean.clear();
   }
  
   public String getField(String field) {
      return pageTypeBean.getString(field);
   }*/
	
   public String getSitePageTypeID() {
      return getField("SitePageTypeID");
   }
   
   public String getSiteID() {
      return getField("SiteID");
   }
   
   public String getHeader() {
      return getField("Header");
   }
   
   public String getFooter() {
      return getField("Footer");
   }
   
   public String getFormat() {
      return getField("Format");
   }
   
   public String getPageType() {
      return getField("PageType");
   }
   
   public String getNavigation() {
      return getField("Navigation");
   }
   
   public String getName() {
      return getField("Name");
   }
   
   public String getDescription() {
      return getField("Description");
   }
   
   public boolean hasProducts() {
      return getField("ProductFlg").equals("Y");
   }
   
   public boolean hasDocuments() {
      return getField("DocumentFlg").equals("Y");
   }
   
   public boolean hasProfiles() {
      return getField("SurveyFlg").equals("Y");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("PageType");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SitePageTypeID","SYSTEM ERROR - SitePageTypeID");
         val.addMandatoryField("SiteID","SYSTEM ERROR - SiteID");
         val.addMandatoryField("Name","Page Type must have a name.");
         val.addMandatoryField("Format","Page Type must have a format selected.");

         Validator.addValidator("PageType", val);
      }
      return val;
   }
   
}