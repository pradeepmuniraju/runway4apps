package com.sok.runway.cms;

import com.sok.framework.*;

import java.sql.*;

/**
 * @deprecated CMS2
 */
public class DisplayImage {
   
   RowSetBean pageImages = new RowSetBean();

   DisplayPage parent = null;
   
   String name = null;
   boolean printedAddNewText = false;
      
   public DisplayImage (DisplayPage parent, Connection con, String name) {
      this.name = name;
      this.parent = parent;
      
      if (parent.getEditMode() == DisplayPage.MODE_EDITING || parent.getEditMode() == DisplayPage.MODE_VIEW_EDIT) {
         pageImages.setViewSpec("cms/SitePageImageEditView");
         pageImages.setConnection(con);
         pageImages.setColumn("SitePageID", parent.getSitePageID());
         pageImages.setColumn("Name", name);
   		pageImages.sortBy("SortOrder",0);
         pageImages.generateSQLStatement();
         pageImages.getResults();
      }
      else {
         pageImages.setViewSpec("cms/SitePageImageView");
         pageImages.setConnection(con);
         pageImages.setColumn("SitePageID", parent.getSitePageID());
         pageImages.setColumn("Name", name);
   		pageImages.sortBy("SortOrder",0);
         pageImages.generateSQLStatement();
         pageImages.getResults();
      }
      if (pageImages.getError().length() > 0) {
         throw new RuntimeException(pageImages.getError());
      }
   }

   public DisplayImage getNextImage() {
      if (pageImages.getNext()) {
         return this;
      }
      else {
         pageImages.close();
         if (parent.getEditMode() == DisplayPage.MODE_EDITING && !printedAddNewText) {
            pageImages.setColumn("SitePageImageID","");
            pageImages.setColumn("Content","Click here to Insert an Image");
            pageImages.setColumn("SortOrder","");
            printedAddNewText = true;
            return this;
         }
         else {
            return null;
         }
      }
   }
   
   public void close() {
      pageImages.close();
   }
   
   public String getWidth() {
      return pageImages.getString("Width");
   }
   
   public String getHeight() {
      return pageImages.getString("Height");
   }
   
   public String getPath() {
      return pageImages.getString("FilePath");
   }
   
   public String getHTMLTag() {

      StringBuffer content = new StringBuffer();
      if (parent.getEditMode() == DisplayPage.MODE_EDITING) {
         content.append("<span class=\"");
         if (getPath().length() == 0) {
            content.append(Site.CSS_STYLE_INSERT);
         }
         else {
            content.append(Site.CSS_STYLE_EDIT);
         }
         content.append("\"><a target=\"_blank\" href=\"");
         
         content.append(parent.getContextPath());
         content.append("/");
         content.append(Site.EDIT_PATH);
         content.append("?SitePageImageID=");
         content.append(pageImages.getString("SitePageImageID"));
         
         if (pageImages.getString("SitePageImageID").length() == 0) {
            content.append("&SitePageID=");
            content.append(parent.getField("SitePageID"));
            content.append("&Name=");
            content.append(name);
         }
         /*if (pageImages.getString("DocumentID").length() == 0) {
            content.append("&SitePageID=");
            content.append(pageImages.getString("SitePageID"));
            content.append("&Name=");
            content.append(name);
         }*/
         content.append("\">");
      }
   
      if (parent.getEditMode() == DisplayPage.MODE_EDITING && getPath().length() == 0) {
         content.append("Click here to insert Image name ");
         content.append(name);
      }
      else if (getPath().length() > 0) {
         if (getPath().endsWith(".swf")) {
            content.append(HTMLUtil.buildFlashTag(parent.getContextPath() + "/" + getPath(), getWidth(), getHeight()));
         }
         else {
            content.append(HTMLUtil.buildImageTag(parent.getContextPath() + "/" + getPath(), getWidth(), getHeight()));
         }
      }
      
      if (parent.getEditMode() == DisplayPage.MODE_EDITING) {
         content.append("</a></span>");
      }
      return content.toString();
   }
   
   /*private String getImageTag() {

      StringBuffer tag = new StringBuffer();

      tag.append("<img src=\"");
      tag.append(parent.getContextPath());
      tag.append("/");
      tag.append();
      tag.append("\" border=\"0\"");
      if (getWidth().length() > 0) {
         tag.append(" width=\"");
         tag.append(getWidth());
         tag.append("\"");
      }
      if (getHeight().length() > 0) {
         tag.append(" height=\"");
         tag.append(getHeight());
         tag.append("\"");
      }
      tag.append("/>");
      
      return tag.toString();
   }
   
   private String getFlashTag() {

      StringBuffer tag = new StringBuffer();
      tag.append("<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0\" width=\"");
      tag.append(getWidth());
      tag.append("\" height=\"");
      tag.append(getHeight());
      tag.append("\">");
      tag.append("<param name=\"movie\" value=\"");

      tag.append(parent.getContextPath());
      tag.append("/");
      tag.append(getPath());
      tag.append("\">");

      tag.append("<param name=\"quality\" value=\"high\">");
      
      tag.append("<embed src=\"");
      tag.append(parent.getContextPath());
      tag.append("/");
      tag.append(getPath());
      tag.append("\"");
   
      tag.append(" quality=\"high\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" width=\"");
      tag.append(getWidth());
      tag.append("\" height=\"");
      tag.append(getHeight());
      tag.append("\"></embed>");
      
      
      tag.append("</object>");
      
      return tag.toString();
   }*/
}