package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class NavigationLink extends RunwayEntity {
      
   public NavigationLink(Connection con) {
      super(con, "cms/SiteNavigationLinkView");
   }
   
   public NavigationLink(Connection con, String navigationLinkID) {
      this(con);
      load(navigationLinkID);
   }
	
   public String getSiteNavigationID() {
      return getField("SiteNavigationID");
   }
	
   public String getSiteNavigationLinkID() {
      return getField("SiteNavigationLinkID");
   }
	
   public String getURL() {
      return getField("URL");
   }
	
   public String getBranchURLPath() {
      return getField("BranchURLPath");
   }
	
   public String getBranchName() {
      return getField("BranchName");
   }
	
   public String getSitePageTypeID() {
      return getField("SitePageTypeID");
   }
	
   public String getSiteBranchID() {
      return getField("SiteBranchID");
   }
   
   public String getSortOrder() {
      return getField("SortOrder");
   }
   
   public String getLabel() {
      return getField("Label");
   }
   
   public String getSectionName() {
      return getField("SectionName");
   }
   
   public String getImageDocumentID() {
      return getField("ImageDocumentID");
   }
   
   public String getImagePath() {
      return getField("ImagePath");
   }
   
   public String getImageName() {
      return getField("ImageName");
   }
   
   public String getRollOverDocumentID() {
      return getField("RollOverDocumentID");
   }
   
   public String getRollOverPath() {
      return getField("RollOverPath");
   }
   
   public String getRollOverName() {
      return getField("RollOverName");
   }
   
   public boolean isLive() {
      return getField("Live").equals("Y");
   }
   
   public boolean isBranchLink() {
      return getField("SiteBranchID").length() > 0;
   }
   
   public boolean useImage() {
      return getField("ImageDocumentID").length() > 0;
   }
   
   public boolean useRollOver() {
      return getField("RollOverDocumentID").length() > 0;
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("NavigationLink");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SiteNavigationLinkID","SYSTEM ERROR - SiteNavigationLinkID");
         val.addMandatoryField("SiteNavigationID","SYSTEM ERROR - SiteNavigationID");

         Validator.addValidator("NavigationLink", val);
      }
      return val;
   }
   

}