package com.sok.runway.cms;

import com.sok.runway.*;
import com.sok.framework.*;

import javax.servlet.http.*;
import javax.servlet.*;
import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class DisplayPage extends Branch {
   
   public static int MODE_LIVE = 0;
   public static int MODE_EDITING = 1;
   public static int MODE_VIEW_EDIT = 2;
   
   
   public static String ROOT_CMS_TOKEN = "/DEFAULT";
   
	public static final String NAV_GRANDPARENT = "Grandparent";
	public static final String NAV_PARENT = "Parent";
	public static final String NAV_SIBLING = "Sibling";
	public static final String NAV_CHILD = "Child";
	public static final String NAV_FIRST_CHILD = "FirstChild";
   
   String contextPath = null;
   String userID = null;
   
   int editMode = MODE_LIVE;
   int masterEditMode = MODE_LIVE;
   
   
   public DisplayPage(Connection con, String contextPath, int editMode, String userID) {
      super(con);
      this.contextPath = contextPath;
      this.userID = userID;
      this.editMode = editMode;
      masterEditMode = editMode;
   }
   
   public boolean load(String pk) {
	   super.load(pk);
       if (isLoaded()) {
           postLoad();
       }
       return isLoaded();
   }
   public void postLoad() {
      if (editMode != MODE_LIVE) {
         if (!isEditor(userID)) {
            this.editMode = MODE_VIEW_EDIT;
         }
      }
   }
   
   public DisplayPage getDisplayPage() {
      DisplayPage newPage = new DisplayPage(getConnection(), contextPath, masterEditMode, userID);
      return newPage;
   }
      
   public DisplayPage getDisplayPage(String branchID) {
      DisplayPage newPage = new DisplayPage(getConnection(), contextPath, masterEditMode, userID);
      newPage.load(branchID);
      return newPage;
   }
   
   public String getContextPath() {
      return contextPath;
   }
   
   public int getEditMode() {
      return editMode;
   }
   
   public Collection getChildDisplayPageIDs() {
      return getBranchIDs(true, getSiteBranchID(), getEditMode() == MODE_LIVE);
   }
   
   public Collection getPageNavigationBranchIDs() {
      if (getNavigationLevel().equals(NAV_GRANDPARENT) || getNavigationLevel().equals(NAV_PARENT)) {
         
         DisplayPage branch = getDisplayPage(getParentSiteBranchID());
         if (getNavigationLevel().equals(NAV_GRANDPARENT)) {
            branch = branch.getDisplayPage(getParentSiteBranchID());
         }
         return branch.getSiblingBranchIDs(true, getEditMode() == MODE_LIVE);
      }
      else if (getNavigationLevel().equals(NAV_SIBLING)) {
         return getSiblingBranchIDs(true, getEditMode() == MODE_LIVE);
      }
      else {
         Collection ids = getChildBranchIDs(true, getEditMode() == MODE_LIVE);
         if (getNavigationLevel().equals(NAV_FIRST_CHILD)) {
            Iterator iter = ids.iterator(); 
            if (iter.hasNext()) {
               DisplayPage branch = getDisplayPage((String)iter.next());
               ids = branch.getChildBranchIDs(true, getEditMode() == MODE_LIVE);
            }
            else {
               ids = new Vector();
            }
         }
         return ids;
      }
   }   
   
   /*public PageSet getNavigationPages() {   
      PageSet childPage = null;
      if (getNavigationLevel().equals(NAV_GRANDPARENT) || getNavigationLevel().equals(NAV_PARENT)) {
         RowBean finder = new RowBean();
         finder.setViewSpec("cms/SiteBranchView");
         finder.setConnection(con);
   	   finder.setColumn("SiteBranchID", getField("ParentSiteBranchID"));
   	   finder.setAction("select");
   	   finder.doAction();
   	   if (finder.getString("ParentSiteBranchID").length() == 0 ) {
            childPage = new PageSet(contextPath, con, getField("ParentSiteBranchID"), masterEditMode, userID);
   	   }
   	   else if (getNavigationLevel().equals(NAV_PARENT)) {
            childPage = new PageSet(contextPath, con, finder.getString("ParentSiteBranchID"), masterEditMode, userID);
   	   }
   	   else {
   	      String branchID = finder.getString("ParentSiteBranchID");
   	      finder.clear();
      	   finder.setColumn("SiteBranchID", branchID);
      	   finder.setAction("select");
      	   finder.doAction();
      	   if (finder.getString("ParentSiteBranchID").length() == 0 ) {
               childPage = new PageSet(contextPath, con, branchID, masterEditMode, userID);
      	   }
      	   else {
               childPage = new PageSet(contextPath, con, finder.getString("ParentSiteBranchID"), masterEditMode, userID);
      	   }
   	   }
      }
      else if (getNavigationLevel().equals(NAV_SIBLING)) {
         childPage = new PageSet(contextPath, con, getField("ParentSiteBranchID"), masterEditMode, userID);
      }
      else {
         childPage = new PageSet(contextPath, con, getField("SiteBranchID"), masterEditMode, userID);
      }
      childPage = childPage.getNextPage();
      
      return childPage;
   }*/
   
   public void setEditMode(int editMode) {
      this.editMode = editMode;
   }
   
   /*public void loadPage(String branchID) {
      
      currentPage.clear();
	   currentPage.setColumn("SiteBranchID", branchID);
	   currentPage.setAction("select");
	   currentPage.doAction();

      return this;
   }*/
   
   /*public Page getCurrentPage(String path) {
      this.con = con;
      
      currentPage.clear();
	   currentPage.setColumn("URLPath", path);
	   if (masterEditMode == MODE_LIVE) {
	      currentPage.setColumn("Live", "Y");
	   }
	   currentPage.retrieveData();
      if (currentPage.getString("SiteBranchID").length() == 0) {
         currentPage.clear();
   	   currentPage.setColumn("URLPath", ROOT_CMS_TOKEN+path);
   	   if (masterEditMode == MODE_LIVE) {
   	      currentPage.setColumn("Live", "Y");
   	   }
   	   currentPage.retrieveData();
      }
	   if (masterEditMode == MODE_EDITING && !currentPage.getString("EditingUserID").equals(userID)) {
	      setEditMode(MODE_LIVE);
	   }
      return this;
   }*/
   
   public DisplaySection getSection(String name) {
      return getSection(name, editMode);
   }
   
   public DisplaySection getSection(String name, int editMode) {
      DisplaySection section = null;
      if (getField("SitePageID").length() > 0 && name != null) {
         section = new DisplaySection(this, getConnection(), name);
         section = section.getNextSection();
         if (section != null) {
            section.close();
         }
      }
      return section;
   }
      
   public DisplaySection getSections(String name) {
      DisplaySection section = null;
      if (getField("SitePageID").length() > 0 && name != null) {
         section = new DisplaySection(this, getConnection(), name);
         section = section.getNextSection();
      }
      return section;
   }
   
   public DisplayImage getImage(String location) {
      return getImage(location, editMode);
   }
     
   public DisplayImage getImage(String location, int editMode) {
      DisplayImage image = null;
      if (getField("SitePageID").length() > 0 && location != null) {
         image = new DisplayImage(this, getConnection(), location);
         image = image.getNextImage();
         if (image != null) {
            image.close();
         }
      }
      return image;
   }
      
   public DisplayImage getImages(String location) {
      DisplayImage image = null;
      if (getField("SitePageID").length() > 0 && location != null) {
         image = new DisplayImage(this, getConnection(), location);
         image = image.getNextImage();
      }
      return image;
   }
   
   /*public Image getImage(int location) {
      Image image = null;
      if (getField("SitePageID").length() > 0) {
         image = new Image(contextPath, getConnection(), getField("SitePageID"), location, editMode);
      }
      return image;
   }*/
   
   public Navigation getSiteNavigation(String name) {
      RowBean navList = new RowBean();
      navList.setViewSpec("cms/SiteNavigationIDView");
      navList.setConnection(getConnection());
      navList.setColumn("SiteID", getSiteID());
      navList.setColumn("Name", name);
      navList.retrieveData();
      
      if (navList.getString("SiteNavigationID").length() > 0) {
         Navigation nav = new Navigation(getConnection(), navList.getString("SiteNavigationID"), getEditMode() == MODE_LIVE);
         return nav;
      }
      else {
         return null;
      }
   }
   
   public Collection getPageDocumentIDs() {
      Collection coll = getPageDocumentIDs(false, getEditMode() == MODE_LIVE);
      if (getEditMode() == MODE_EDITING) {
         coll.add(DisplayDocument.EDIT_DOCUMENT_ID);
      }
      return coll;
   }
   
   public Collection getPageProductIDs() {
      Collection coll = getPageProductIDs(false, getEditMode() == MODE_LIVE);
      if (getEditMode() == MODE_EDITING) {
         coll.add(DisplayProduct.EDIT_PRODUCT_ID);
      }
      return coll;
   }
   
   public boolean isPage() {
      return getField("SitePageID").length() > 0;
   }
   
   public String getLinkFromNavigationLink(NavigationLink navLink) {
      String url = null;
      String label = null;
      String image = null;
      String rollOver = null;
      
      if (navLink.isBranchLink()) {
         DisplayPage page = getDisplayPage(navLink.getSiteBranchID());
         url = contextPath + page.getURLPath() + ".sok";
         
         if (navLink.useImage()) {
            image = contextPath + "/" + navLink.getImagePath();
            if (navLink.useRollOver()) {
               rollOver = contextPath + "/" + navLink.getRollOverPath();
               return HTMLUtil.buildRollOverLink(url, image, rollOver, navLink.getSiteNavigationLinkID());
            }
            else {
               return HTMLUtil.buildImageLink(url, image);
            }
         }
         else if (navLink.getSectionName().length() == 0 ) {
            return page.getPageLinkWithLabel(navLink.getLabel());
         }
         else {
            return page.getPageLink(navLink.getSectionName());
         }
      }
      else {
         url = navLink.getURL();
         if (navLink.useImage()) {
            image = contextPath + "/" + navLink.getImagePath();
            if (navLink.useRollOver()) {
               rollOver = contextPath + "/" + navLink.getRollOverPath();
               return HTMLUtil.buildRollOverLink(url, image, rollOver, navLink.getSiteNavigationLinkID());
            }
            else {
               return HTMLUtil.buildImageLink(url, image);
            }
         }
         else {
            return HTMLUtil.buildLink(navLink.getURL(), navLink.getLabel());
         }
      }
   }
   
   public String getPageLink(String sectionName) {
      DisplaySection linkText = null;
      String label = null;
      if (editMode == MODE_LIVE) {
         linkText = getSection(sectionName,MODE_LIVE);
      }
      else {
         // we only want to display a link here not wrapped in a link
         linkText = getSection(sectionName,MODE_VIEW_EDIT);
      }
      if (linkText != null) {
         label = linkText.getContent(false);
      }
      else {
         label = "unspecified";
      }
      label = getPageLinkWithLabel(label);
      if (editMode == MODE_EDITING) {
         //label = label +  "edit"; //linkText.getContent(true, "Click here to Edit "+sectionName));
      }
      return label;
   }
   
   public String getPageLinkWithLabel(String label) {
      return HTMLUtil.buildLink(contextPath + getURLPath() + ".sok", label);
   }

   
}