package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class URLLink extends RunwayEntity {
   
   public URLLink(Connection con) {
      super(con, "cms/URLLinkView");
   }
      
   public URLLink(Connection con, String urlLinkID) {
      this(con);
      load(urlLinkID);
   }
	
	public Collection getSiteURLLinkIDs(String siteID) {
      ArrayList ids = new ArrayList();
      
      RowSetBean list = new RowSetBean();
      list.setViewSpec("cms/URLLinkIDView");
      list.setConnection(getConnection());
      list.setColumn("SiteID", siteID);
      list.sortBy("URLLinks.Name",0);
      list.generateSQLStatement();
      list.getResults();
      
      if (list.getError().length() > 0) {
         throw new RuntimeException(list.getError());
      }
      
      while (list.getNext()) {
         ids.add(list.getString("URLLinkID"));
      }
      list.close();
      return ids;
   }
   
	public Collection getPageURLLinkIDs(String pageID) {
      ArrayList ids = new ArrayList();
      
      RowSetBean list = new RowSetBean();
      list.setViewSpec("cms/URLLinkIDView");
      list.setConnection(getConnection());
      list.setColumn("URLLinkBranch.SitePageID", pageID);
      
      list.sortBy("URLLinks.Name",0);
      list.generateSQLStatement();
      list.getResults();
      
      if (list.getError().length() > 0) {
         throw new RuntimeException(list.getError());
      }
      
      while (list.getNext()) {
         ids.add(list.getString("URLLinkID"));
      }
      list.close();
      return ids;
   }
	
   public String getURLLinkID() {
      return getField("URLLinkID");
   }
   
   public String getName() {
      return getField("Name");
   }
	
   public String getURL() {
      return getField("URL");
   }
   
   public String getResponse() {
      return getField("Response");
   } 
   
   public String getText() {
      return getField("Text");
   } 
   
   public String getDocumentID() {
      return getField("DocumentID");
   }
   
   public String getSiteID() {
      return getField("SiteID");
   }
   
   public String getImagePath() {
      return getField("ImageFilePath");
   }
   
   public String getLink() {
      if (getDocumentID().length() > 0) {
         return HTMLUtil.buildImageLink(getURL(), getImagePath());
      }
      else {
         return HTMLUtil.buildLink(getURL(), getText());
      }
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("URLLink");
      if (val == null) {
         val = new Validator();

         Validator.addValidator("URLLink", val);
      }
      return val;
   }
   
}