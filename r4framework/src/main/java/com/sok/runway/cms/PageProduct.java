package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.crm.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class PageProduct extends RunwayEntity {
   
   public static final String STATUS_OFFLINE =  "Offline";
   public static final String STATUS_LIVE =  "Live";
   public static final String STATUS_PENDING_LIVE =  "PendingLive";
   public static final String STATUS_PENDING_REMOVAL =  "PendingRemoval";
   
   public PageProduct(Connection con) {
      super(con, "cms/SitePageProductView");
   }
   
   public PageProduct(Connection con, String pageProductID) {
      this(con);
      load(pageProductID);
   }
	
	public static Collection getPageProductIDs(Connection con, String sitePageID) {
      ArrayList ids = new ArrayList();
      
      RowSetBean products = new RowSetBean();
      products.setViewSpec("cms/SitePageProductIDView");
      products.setConnection(con);
      products.setColumn("SitePageID", sitePageID);
   	products.sortBy("SortOrder",0);
      products.generateSQLStatement();
      products.getResults();
      
      if (products.getError().length() > 0) {
         throw new RuntimeException(products.getError());
      }
      
      while (products.getNext()) {
         ids.add(products.getString("SitePageProductID"));
      }
      products.close();
      return ids;
   }
		
   public String getSitePageProductID() {
      return getField("SitePageProductID");
   }
   
   public String getSitePageID() {
      return getField("SitePageID");
   }
   
   public String getSortOrder() {
      return getField("SortOrder");
   }
   
   public String getStatus() {
      return getField("Status");
   }
   
   public String getProductID() {
      return getField("ProductID");
   }
   
   public String getName() {
      return getField("Name");
   }
   
   public String getDescription() {
      return getField("Description");
   }
   
   public String getProductNum() {
      return getField("ProductNum");
   }
   
   public String getTotalCost() {
      return getField("TotalCost");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("PageProduct");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SitePageProductID","SYSTEM ERROR - SitePageProductID");
         val.addMandatoryField("ProductID","SYSTEM ERROR - ProductID");
         val.addMandatoryField("SitePageID","SYSTEM ERROR - SitePageID");
         //val.addMandatoryField("Status","Product must have a status.");
         HashSet set = new HashSet(2);
         set.add(STATUS_OFFLINE);
         set.add(STATUS_LIVE);
         set.add(STATUS_PENDING_LIVE);
         set.add(STATUS_PENDING_REMOVAL);
         val.addRestrictedValueField("Status",set);
         Validator.addValidator("PageProduct", val);
      }
      return val;
   }
   
   public ErrorMap remove() {
      if (getStatus().equals(STATUS_LIVE)) {
         setField("Status", STATUS_PENDING_REMOVAL);
      }
      else if (getStatus().equals(STATUS_PENDING_LIVE)) {
         setField("Status", STATUS_OFFLINE);
      }
      else if (getStatus().equals(STATUS_OFFLINE) || getStatus().equals(STATUS_PENDING_REMOVAL)) {
         return new ErrorMap();
      }
      return super.update();
   }
   
   public ErrorMap restore() {
      if (getStatus().equals(STATUS_OFFLINE)) {
         setField("Status", STATUS_PENDING_LIVE);
      }
      else if (getStatus().equals(STATUS_PENDING_REMOVAL)) {
         setField("Status", STATUS_LIVE);
      }
      else if (getStatus().equals(STATUS_LIVE) || getStatus().equals(STATUS_PENDING_LIVE)) {
         return new ErrorMap();
      }
      return super.update();
   }
   
   public boolean delete() {
      if (getStatus().equals(STATUS_OFFLINE) || getStatus().equals(STATUS_PENDING_LIVE)) {
         return super.delete();
      }
      else {
         return false;
      }
   }
   
}