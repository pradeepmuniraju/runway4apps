package com.sok.runway.cms;

import com.sok.framework.*;

import java.sql.*;
import java.util.*;
import java.io.*;
import org.apache.velocity.app.*;

/**
 * @deprecated CMS2
 */
public class DisplaySection {
   
   RowSetBean pageSections = new RowSetBean();
   DisplayPage parent = null;
   
   String name = null;
   boolean printedAddNewText = false;
      
   public DisplaySection (DisplayPage parent, Connection con, String name) {
      this.name = name;
      this.parent = parent;
      
      if (parent.getEditMode() == DisplayPage.MODE_EDITING || parent.getEditMode() == DisplayPage.MODE_VIEW_EDIT) {
         pageSections.setViewSpec("cms/SitePageSectionEditView");
         pageSections.setConnection(con);
         pageSections.setColumn("SitePageID", parent.getSitePageID());
         pageSections.setColumn("Name", name);
   		pageSections.sortBy("SortOrder",0);
         pageSections.generateSQLStatement();
         pageSections.getResults();
      }
      else {
         pageSections.setViewSpec("cms/SitePageSectionView");
         pageSections.setConnection(con);
         pageSections.setColumn("SitePageID", parent.getSitePageID());
         pageSections.setColumn("Name", name);
   		pageSections.sortBy("SortOrder",0);
         pageSections.generateSQLStatement();
         pageSections.getResults();
      }
      if (pageSections.getError().length() > 0) {
         throw new RuntimeException(pageSections.getError());
      }
   }
   
   public DisplaySection getNextSection() {
      if (pageSections.getNext()) {
         return this;
      }
      else {
         pageSections.close();
         if (parent.getEditMode() == DisplayPage.MODE_EDITING && !printedAddNewText) {
            pageSections.setColumn("SitePageSectionID","");
            pageSections.setColumn("Content","Click here to Insert "+name);
            pageSections.setColumn("SortOrder","");
            printedAddNewText = true;
            return this;
         }
         else {
            return null;
         }
      }
   }
   
   public void close() {
      pageSections.close();
   }
   
   public String getContent() {
      return getContent(true, null);
   }
   
   public String getContent(boolean showEdit) {
      return getContent(showEdit, null);
   }
   
   public String getContent(boolean showEdit, String text) {

      StringBuffer content = new StringBuffer();
      
      if (parent.getEditMode() == DisplayPage.MODE_EDITING && showEdit) {
         content.append("<span class=\"");
         if (printedAddNewText) {
            content.append(Site.CSS_STYLE_INSERT);
         }
         else {
            content.append(Site.CSS_STYLE_EDIT);
         }
         content.append("\"><a target=\"_blank\" href=\"");
         
         content.append(parent.getContextPath());
         content.append("/");
         content.append(Site.EDIT_PATH);
         content.append("?SitePageSectionID=");
         content.append(pageSections.getString("SitePageSectionID"));
         if (pageSections.getString("SitePageSectionID").length() == 0) {
            content.append("&SitePageID=");
            content.append(parent.getField("SitePageID"));
            content.append("&Name=");
            content.append(name);
         }
         content.append("\">");
      }
      if (text == null) {
         content.append(doVelocitySubstitution(pageSections.getString("Content")));
      }
      else {
         content.append(text);
      }
      if (parent.getEditMode() == DisplayPage.MODE_EDITING && showEdit) {
         content.append("</a></span>");
      }
      return content.toString();
   }
   
   public String doVelocitySubstitution(String content) {
      URLLink urlLink = new URLLink(parent.getConnection());
      Iterator iter = urlLink.getPageURLLinkIDs(parent.getSitePageID()).iterator();
      
      RowBean context = new RowBean();
      while (iter.hasNext()) {
         urlLink.load((String)iter.next());
         context.put(urlLink.getName(), urlLink.getLink());
      }
      
      StringWriter text = new StringWriter();
		try{
			VelocityEngine ve = VelocityManager.getEngine();
			ve.evaluate( context, text, "DisplaySection", content);
			text.flush();
			text.close();
		}catch(Exception e){}
		return(text.toString());
   }
   
}