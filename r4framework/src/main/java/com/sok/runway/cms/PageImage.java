package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class PageImage extends RunwayEntity {
      
   public PageImage(Connection con) {
      super(con, "cms/SitePageImageEditView");
   }
   
   public PageImage(Connection con, String pageImageID) {
      this(con);
      load(pageImageID);
   }
   
   public static Collection getPageImageIDs(Connection con, String sitePageID, String sectionName) {
      ArrayList ids = new ArrayList();
      
      RowSetBean sectionList = new RowSetBean();
      sectionList.setViewSpec("cms/SitePageImageEditIDView");
      sectionList.setConnection(con);
      sectionList.setColumn("SitePageID", sitePageID);
      sectionList.setColumn("Name", sectionName);
   	sectionList.sortBy("SortOrder",0);
      sectionList.generateSQLStatement();
      sectionList.getResults();
      
      if (sectionList.getError().length() > 0) {
         throw new RuntimeException(sectionList.getError());
      }
      
      while (sectionList.getNext()) {
         ids.add(sectionList.getString("SitePageImageID"));
      }
      sectionList.close();
      return ids;
   }
   
	
   public String getSitePageImageID() {
      return getField("SitePageImageID");
   }
   
   public String getSitePageID() {
      return getField("SitePageID");
   }
   
   public String getName() {
      return getField("Name");
   }
   
   public String getDocumentID() {
      return getField("DocumentID");
   }
   
   public String getSortOrder() {
      return getField("SortOrder");
   }
   
   public String getFileName() {
      return getField("FileName");
   }
   
   public String getWidth() {
      return getField("Width");
   }
   
   public String getHeight() {
      return getField("Height");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("PageImage");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SitePageImageID","SYSTEM ERROR - SitePageImageID");
         val.addMandatoryField("SitePageID","SYSTEM ERROR - SitePageID");
         val.addMandatoryField("Name","Image must have a name.");

         Validator.addValidator("PageImage", val);
      }
      return val;
   }
   
}