package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class PageSection extends RunwayEntity {
      
   public PageSection(Connection con) {
      super(con, "cms/SitePageSectionEditView");
   }
   
   public PageSection(Connection con, String pageSectionID) {
      this(con);
      load(pageSectionID);
   }
   
   public static Collection getPageSectionIDs(Connection con, String sitePageID, String sectionName) {
      ArrayList ids = new ArrayList();
      
      RowSetBean sectionList = new RowSetBean();
      sectionList.setViewSpec("cms/SitePageSectionEditIDView");
      sectionList.setConnection(con);
      sectionList.setColumn("SitePageID", sitePageID);
      sectionList.setColumn("Name", sectionName);
   	sectionList.sortBy("SortOrder",0);
      sectionList.generateSQLStatement();
      sectionList.getResults();
      
      if (sectionList.getError().length() > 0) {
         throw new RuntimeException(sectionList.getError());
      }
      
      while (sectionList.getNext()) {
         ids.add(sectionList.getString("SitePageSectionID"));
      }
      sectionList.close();
      return ids;
   }
   
	
   public String getSitePageSectionID() {
      return getField("SitePageSectionID");
   }
   
   public String getSitePageID() {
      return getField("SitePageID");
   }
   
   public String getName() {
      return getField("Name");
   }
   
   public String getContent() {
      return getField("Content");
   }
   
   public String getSortOrder() {
      return getField("SortOrder");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("PageSection");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SitePageSectionID","SYSTEM ERROR - SitePageSectionID");
         val.addMandatoryField("SitePageID","SYSTEM ERROR - SitePageID");
         val.addMandatoryField("Name","Section must have a name.");

         Validator.addValidator("PageSection", val);
      }
      return val;
   }
   
}