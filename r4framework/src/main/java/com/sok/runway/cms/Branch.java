package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.crm.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class Branch extends RunwayEntity {
   
   public static String STATUS_NEW = "New";
   public static String STATUS_EDITING = "Editing";
   public static String STATUS_PENDING = "Pending";
   public static String STATUS_APPROVED = "Approved";
   public static String STATUS_LIVE = "Live";
   
   public Branch(Connection con) {
      super(con, "cms/SiteBranchView");
   }
   
   public Branch(Connection con, String branchID) {
      this(con);
      load(branchID);
   }
   
   public Site getSite() {
      return new Site(getConnection(), getSiteID());
   }
   
   public void loadHomeBranch(String siteID) {
      Site site = new Site(getConnection(), siteID);
      load(site.getHomeSiteBranchID());
   }
   
   public void loadBranchByPath(String path) {
      loadFromField("URLPath", path);
   }
   
   public Branch getParentBranch() {
      return new Branch(getConnection(), getParentSiteBranchID());
   }
   
   public Collection getChildBranchIDs() {
      return getBranchIDs(false, getSiteBranchID(), false);
   }
   
   public Collection getChildBranchIDs(boolean pagesOnly, boolean live) {
      return getBranchIDs(pagesOnly, getSiteBranchID(), live);
   }
   
   public Collection getSiblingBranchIDs(boolean pagesOnly, boolean live) {
      return getBranchIDs(pagesOnly, getSiteBranchID(), live);
   }
   
   public Collection getBranchIDs(boolean pagesOnly, String parentBranchID, boolean live) {
      ArrayList ids = new ArrayList();
      
      RowSetBean branchList = new RowSetBean();
      branchList.setViewSpec("cms/SiteBranchIDView");
      branchList.setConnection(getConnection());
      branchList.setColumn("ParentSiteBranchID", parentBranchID);

      if (pagesOnly) {
         branchList.setColumn("SitePageID", "!EMPTY");
      }
      if (live) {
         branchList.setColumn("Live", "Y");
      }
      branchList.sortBy("SiteBranches.SortOrder",0);
      branchList.sortBy("SiteBranches.Name",1);
      branchList.generateSQLStatement();
      branchList.getResults();
      
      if (branchList.getError().length() > 0) {
         throw new RuntimeException(branchList.getError());
      }
      
      while (branchList.getNext()) {
         ids.add(branchList.getString("SiteBranchID"));
      }
      branchList.close();
      return ids;
   }
   
   public String getSiteBranchID() {
      return getField("SiteBranchID");
   }
   
   public String getParentSiteBranchID() {
      return getField("ParentSiteBranchID");
   }
   
   public String getSitePageID() {
      return getField("SitePageID");
   }
   
   public String getSiteID() {
      return getField("SiteID");
   }
   
   public String getURLPath() {
      return getField("URLPath");
   }
   
   public String getSortOrder() {
      return getField("SortOrder");
   }
   
   public String getName() {
      return getField("Name");
   }
   
   public boolean isLive() {
      return getField("Live").equals("Y");
   }
   
   public boolean isPage() {
      return getField("SitePageID").length() > 0;
   }
   public String getSitePageTypeID() {
      return getField("SitePageTypeID");
   }
   public String getFormat() {
      return getField("Format");
   }
   public String getHeader() {
      return getField("Header");
   }
   public String getFooter() {
      return getField("Footer");
   }
   public String getPageType() {
      return getField("PageType");
   }
   public String getNavigation() {
      return getField("Navigation");
   }
   
   public boolean hasProducts() {
      return getField("ProductFlg").equals("Y");
   }
   
   public boolean hasDocuments() {
      return getField("DocumentFlg").equals("Y");
   }
   
   public boolean hasProfiles() {
      return getField("SurveyFlg").equals("Y");
   }
   
   public String getNavigationLevel() {
      return getField("NavigationLevel");
   }
   
   public String getResponse() {
      return getField("Response");
   }
   
   public String getWindowTitle() {
      return getField("WindowTitle");
   }
   
   public boolean isOwner(String userID) {
      return getField("OwnerUserID").equals(userID);
   }
   
   public boolean isEditor(String userID) {
      return getField("EditingUserID").equals(userID);
   }
   
   public String getStatusInput(String userID) {
      StringBuffer buff = new StringBuffer();
      if (!isOwner(userID) && !isEditor(userID)) {
         buff.append(getField("Status"));
      }
      else {
         buff.append("<select name=\"Status\">");
         if (isOwner(userID)) {
            buff.append("<option name=\"");
            buff.append(STATUS_NEW);
            buff.append("\"");
            if (getField("Status").equals(STATUS_NEW)) {
               buff.append(" selected");
            }
            buff.append(">");
            buff.append(STATUS_NEW);
            buff.append("</option>");
         }
         buff.append("<option name=\"");
         buff.append(STATUS_EDITING);
         buff.append("\"");
         if (getField("Status").equals(STATUS_EDITING)) {
            buff.append(" selected");
         }
         buff.append(">");
         buff.append(STATUS_EDITING);
         buff.append("</option>");
         
         buff.append("<option name=\"");
         buff.append(STATUS_PENDING);
         buff.append("\"");
         if (getField("Status").equals(STATUS_PENDING)) {
            buff.append(" selected");
         }
         buff.append(">");
         buff.append(STATUS_PENDING);
         buff.append("</option>");
         
         if (isOwner(userID)) {
            buff.append("<option name=\"");
            buff.append(STATUS_APPROVED);
            buff.append("\"");
            if (getField("Status").equals(STATUS_APPROVED)) {
               buff.append(" selected");
            }
            buff.append(">");
            buff.append(STATUS_APPROVED);
            buff.append("</option>");
            buff.append("<option name=\"");
            buff.append(STATUS_LIVE);
            buff.append("\"");
            if (getField("Status").equals(STATUS_LIVE)) {
               buff.append(" selected");
            }
            buff.append(">");
            buff.append(STATUS_LIVE);
            buff.append("</option>");
         }
         buff.append("</select>");
      }
      return buff.toString();
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("Branch");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SiteBranchID","SYSTEM ERROR - SiteBranchID");
         val.addMandatoryField("SiteID","SYSTEM ERROR - SiteID");
         val.addMandatoryField("URLPath","URLPath must not be blank.");
         val.addMandatoryField("Name","Name must not be blank.");

         Validator.addValidator("Branch", val);
      }
      return val;
   }
   
   
   public void statusToPending(String userID, String comment) {
      if (getField("Status").equals(STATUS_EDITING)) {
         
         Page page = new Page(getConnection(), getSitePageID());
         page.setField("Status", STATUS_PENDING);
         page.setField("EditingUserID", userID);
         page.update();
   
         load(getSiteBranchID());
         
         PageTask task = new PageTask(getConnection());
         task.setField("SitePageID", getSitePageID());
         task.setField("TaskDate", "NOW");
         task.setField("Status", STATUS_PENDING);
         task.setField("UserID", userID);
         task.setField("Comment", comment);
         task.insert();
      }
   }

   public void assignToEditor(String userID, String comment) {
      
      Page page = new Page(getConnection(), getSitePageID());
      page.setField("Status", STATUS_EDITING);
      page.setField("EditingUserID", userID);
      page.update();

      load(getSiteBranchID());
      
      String status = getField("Status");
      if (status.equals(STATUS_LIVE)) {
         executeStatement("DELETE FROM SitePageSectionEdits WHERE SitePageID = '" + getSitePageID() + "';");
         executeStatement("INSERT INTO SitePageSectionEdits SELECT * FROM SitePageSections WHERE SitePageID = '" + getSitePageID() + "';");
         executeStatement("DELETE FROM SitePageImageEdits WHERE SitePageID = '" + getSitePageID() + "';");
         executeStatement("INSERT INTO SitePageImageEdits SELECT * FROM SitePageImages WHERE SitePageID = '" + getSitePageID() + "';");
      }   

      PageTask task = new PageTask(getConnection());
      task.setField("SitePageID", getSitePageID());
      task.setField("TaskDate", "NOW");
      task.setField("Status", STATUS_EDITING);
      task.setField("UserID", userID);
      task.setField("Comment", comment);
      task.insert();
   }

   public void statusToLive(String userID, String comment) {
	   
	   throw new UnsupportedOperationException("just no."); 
	   /*
      String status = getField("Status");
      
      if (status.equals(STATUS_EDITING)
            || status.equals(STATUS_PENDING)
            || status.equals(STATUS_APPROVED)) {
         
         Page page = new Page(getConnection(), getSitePageID());
         page.setField("Status", STATUS_LIVE);
         page.setField("EditingUserID", userID);
         page.update();

         setField("Live", "Y");
         update();
         
         load(getField("SiteBranchID"));
      
         executeStatement("DELETE FROM SitePageSections WHERE SitePageID = '" + getSitePageID() + "';");
         executeStatement("INSERT INTO SitePageSections SELECT * FROM SitePageSectionEdits WHERE SitePageID = '" + getSitePageID() + "';");
         executeStatement("DELETE FROM SitePageImages WHERE SitePageID = '" + getSitePageID() + "';");
         executeStatement("INSERT INTO SitePageImages SELECT * FROM SitePageImageEdits WHERE SitePageID = '" + getSitePageID() + "';");
         
         executeStatement("UPDATE SitePageDocuments SET Status = '" + PageDocument.STATUS_LIVE + "' WHERE Status = '" + PageDocument.STATUS_PENDING_LIVE + "';");
         executeStatement("UPDATE SitePageDocuments SET Status = '" + PageDocument.STATUS_OFFLINE + "' WHERE Status = '" + PageDocument.STATUS_PENDING_REMOVAL + "';");
         
         executeStatement("UPDATE SitePageProducts SET Status = '" + PageProduct.STATUS_LIVE + "' WHERE Status = '" + PageProduct.STATUS_PENDING_LIVE + "';");
         executeStatement("UPDATE SitePageProducts SET Status = '" + PageProduct.STATUS_OFFLINE + "' WHERE Status = '" + PageProduct.STATUS_PENDING_REMOVAL + "';");
         
         PageTask task = new PageTask(getConnection());
         task.setField("SitePageID", getSitePageID());
         task.setField("TaskDate", "NOW");
         task.setField("Status", STATUS_LIVE);
         task.setField("UserID", userID);
         task.setField("Comment", comment);
         task.insert();
         
      }   
      */
      
   }
   
   /*public Collection getLiveImageIDs() {
      ArrayList ids = new ArrayList();
      if (isPage()) {
         RowSetBean images = new RowSetBean();
         images.setViewSpec("cms/SitePageImageIDView");
         images.setConnection(getConnection());
         images.setColumn("SitePageID", getSitePageID());
         images.setColumn("Name", name);
   		images.sortBy("SortOrder",0);
         images.generateSQLStatement();
         images.getResults();
         
         if (images.getError().length() > 0) {
            throw new RuntimeException(images.getError());
         }
         
         while (images.getNext()) {
            ids.add(images.getString("SitePageImageID"));
         }
         images.close();
      }
      return ids;
   }*/
   
   public Collection getPageDocumentIDs(boolean includeAll, boolean liveView) {
	   throw new UnsupportedOperationException("no"); 
	   /* 
      ArrayList ids = new ArrayList();
      if (isPage()) {
         RowSetBean documents = new RowSetBean();
         documents.setViewSpec("cms/SitePageDocumentIDView");
         documents.setConnection(getConnection());
         documents.setColumn("SitePageID", getSitePageID());
         if (!includeAll) {
            if (liveView) {
               documents.setColumn("Status", PageDocument.STATUS_LIVE + "+" + PageDocument.STATUS_PENDING_REMOVAL);
            }
            else {
               documents.setColumn("Status", PageDocument.STATUS_LIVE + "+" + PageDocument.STATUS_PENDING_LIVE);
            }
         }
   		documents.sortBy("SortOrder",0);
         documents.generateSQLStatement();
         documents.getResults();
         
         if (documents.getError().length() > 0) {
            throw new RuntimeException(documents.getError());
         }
         
         while (documents.getNext()) {
            ids.add(documents.getString("SitePageDocumentID"));
         }
         documents.close();
      }
      return ids;
      */
   }
   
   public Collection getPageProductIDs(boolean includeAll, boolean liveView) {
	   throw new UnsupportedOperationException("no"); 
	   /* 
      ArrayList ids = new ArrayList();
      if (isPage()) {
         RowSetBean products = new RowSetBean();
         products.setViewSpec("cms/SitePageProductIDView");
         products.setConnection(getConnection());
         products.setColumn("SitePageID", getSitePageID());
         if (!includeAll) {
            if (liveView) {
               products.setColumn("Status", PageProduct.STATUS_LIVE + "+" + PageProduct.STATUS_PENDING_REMOVAL);
            }
            else {
               products.setColumn("Status", PageProduct.STATUS_LIVE + "+" + PageProduct.STATUS_PENDING_LIVE);
            }
         }
   		products.sortBy("SortOrder",0);
         products.generateSQLStatement();
         products.getResults();
         
         if (products.getError().length() > 0) {
            throw new RuntimeException(products.getError());
         }
         
         while (products.getNext()) {
            ids.add(products.getString("SitePageProductID"));
         }
         products.close();
      }
      return ids;
      */
   }
   
}