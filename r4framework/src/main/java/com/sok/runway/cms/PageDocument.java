package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.crm.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class PageDocument extends RunwayEntity {
   
   public static final String STATUS_OFFLINE =  "Offline";
   public static final String STATUS_LIVE =  "Live";
   public static final String STATUS_PENDING_LIVE =  "PendingLive";
   public static final String STATUS_PENDING_REMOVAL =  "PendingRemoval";
   
   public PageDocument(Connection con) {
      super(con, "cms/SitePageDocumentView");
   }
   
   public PageDocument(Connection con, String pageDocumentID) {
      this(con);
      load(pageDocumentID);
   }
	
	public static Collection getPageDocumentIDs(Connection con, String sitePageID) {
      ArrayList ids = new ArrayList();
      
      RowSetBean documents = new RowSetBean();
      documents.setViewSpec("cms/SitePageDocumentIDView");
      documents.setConnection(con);
      documents.setColumn("SitePageID", sitePageID);
   	documents.sortBy("SortOrder",0);
      documents.generateSQLStatement();
      documents.getResults();
      
      if (documents.getError().length() > 0) {
         throw new RuntimeException(documents.getError());
      }
      
      while (documents.getNext()) {
         ids.add(documents.getString("SitePageDocumentID"));
      }
      documents.close();
      return ids;
   }
	
   public String getSitePageDocumentID() {
      return getField("SitePageDocumentID");
   }
   
   public String getSitePageID() {
      return getField("SitePageID");
   }
   
   public String getSortOrder() {
      return getField("SortOrder");
   }
   
   public String getStatus() {
      return getField("Status");
   }
   
   public String getDocumentID() {
      return getField("DocumentID");
   }
   
   public String getDocumentType() {
      return getField("DocumentType");
   }
   
   public String getFileName() {
      return getField("FileName");
   }
   
   public String getDescription() {
      return getField("Description");
   }
   
   public String getFilePath() {
      return getField("FilePath");
   }
   
   public String getDocumentCategory() {
      return getField("DocumentCategory");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("PageDocument");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SitePageDocumentID","SYSTEM ERROR - SitePageDocumentID");
         val.addMandatoryField("DocumentID","SYSTEM ERROR - DocumentID");
         val.addMandatoryField("SitePageID","SYSTEM ERROR - SitePageID");
         //val.addMandatoryField("Status","Document must have a status.");
         HashSet set = new HashSet(2);
         set.add(STATUS_OFFLINE);
         set.add(STATUS_LIVE);
         set.add(STATUS_PENDING_LIVE);
         set.add(STATUS_PENDING_REMOVAL);
         val.addRestrictedValueField("Status",set);
         Validator.addValidator("PageDocument", val);
      }
      return val;
   }
   
   public ErrorMap remove() {
      if (getStatus().equals(STATUS_LIVE)) {
         setField("Status", STATUS_PENDING_REMOVAL);
      }
      else if (getStatus().equals(STATUS_PENDING_LIVE)) {
         setField("Status", STATUS_OFFLINE);
      }
      else if (getStatus().equals(STATUS_OFFLINE) || getStatus().equals(STATUS_PENDING_REMOVAL)) {
         return new ErrorMap();
      }
      return super.update();
   }
   
   public ErrorMap restore() {
      if (getStatus().equals(STATUS_OFFLINE)) {
         setField("Status", STATUS_PENDING_LIVE);
      }
      else if (getStatus().equals(STATUS_PENDING_REMOVAL)) {
         setField("Status", STATUS_LIVE);
      }
      else if (getStatus().equals(STATUS_LIVE) || getStatus().equals(STATUS_PENDING_LIVE)) {
         return new ErrorMap();
      }
      return super.update();
   }
   
   public boolean delete() {
      if (getStatus().equals(STATUS_OFFLINE) || getStatus().equals(STATUS_PENDING_LIVE)) {
         return super.delete();
      }
      else {
         return false;
      }
   }
   
}