package com.sok.runway.cms;

import com.sok.runway.*;
import com.sok.runway.crm.Product;
import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class DisplayProduct extends PageProduct {
   
   public static final String EDIT_PRODUCT_ID = "EDIT";
   
   private Product product = null;
   private boolean insertMode = false;
   
   DisplayPage parent = null;
   
   public DisplayProduct(DisplayPage parent) {
      super(parent.getConnection());
      setupProduct();
      this.parent = parent;
   }
   
   public DisplayProduct(DisplayPage parent, String pageProductID) {
      super(parent.getConnection(), pageProductID);
      setupProduct();
      this.parent = parent;
   }
   
   private void setupProduct() {
      product = new Product(getConnection());
   }
   
   public boolean load(String pk) {
      if (EDIT_PRODUCT_ID.equals(pk)) {
         clear();
         product.setField("Name","Click here to add Product");
         insertMode = true;
      }
      else {
         super.load(pk);
         product.load(getProductID());
         insertMode = false;
      }
      return isLoaded();
   }
   
   public void clear() {
      super.clear();
      product.clear();
   }
   
   public String getName() {
      if (parent.getEditMode() == DisplayPage.MODE_EDITING) {
         StringBuffer editLink = new StringBuffer();
         
         editLink.append("<span class=\"");
         if (insertMode) {
            editLink.append(Site.CSS_STYLE_INSERT);
         }
         else {
            editLink.append(Site.CSS_STYLE_EDIT);
         }
         editLink.append("\"><a target=\"_blank\" href=\"");
            
         editLink.append(parent.getContextPath());
         editLink.append("/");
         editLink.append(Site.EDIT_PATH);
            
         editLink.append("?SitePageProductID=");
         editLink.append(getSitePageProductID());
         
         if (insertMode) {
            editLink.append("&SitePageID=");
            editLink.append(getSitePageID());
         }
         editLink.append("\">");
         
         if (product.getName().length() == 0) {
            editLink.append("&lt;No Name&gt;");
         }
         else {
            editLink.append(product.getName());
         }
         editLink.append("</a></span>");
   
         return editLink.toString();
      }
      else {
         return product.getName();
      }
   }
   
   public String getDescription() {
      return product.getDescription();
   }
   
   public String getProductNum() {
      return product.getProductNum();
   }
   
   public String getCategory() {
      return product.getCategory();
   }
   
   public String getTotalCost() {
      return String.valueOf(product.getTotalCost());
   }
   
   public String getProfileField(String questionID) {
      // TODO
      return "";//product.getProfileField(questionID);
   }
   
}