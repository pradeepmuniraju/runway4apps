package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class Image extends RunwayEntity {
   
   public Image(Connection con) {
      super(con, "cms/SitePageImageView");
   }
      
   public Image(Connection con, String pageTypeImageID) {
      this(con);
      load(pageTypeImageID);
   }
	
   public String getSitePageImageID() {
      return getField("SitePageImageID");
   }
	
   public String getSitePageID() {
      return getField("SitePageID");
   }
   
   public String getDocumentID() {
      return getField("DocumentID");
   }
   
   public String getName() {
      return getField("Name");
   }
   
   public String getWidth() {
      return getField("Width");
   } 
   
   public String getHeight() {
      return getField("Height");
   } 
   
   public String getSortOrder() {
      return getField("SortOrder");
   } 
   
   public String getPath() {
      return getField("FilePath");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("Image");
      /*if (val == null) {
         val = new Validator();
         val.addMandatoryField("SitePageTypeElementID","SYSTEM ERROR");
         val.addMandatoryField("SitePageTypeID","SYSTEM ERROR");
         val.addMandatoryField("Name","Page Element must have a name.");
         HashSet set = new HashSet(2);
         set.add("Text");
         set.add("Image");
         val.addRestrictedValueField("Type",set);
         Validator.addValidator("PageTypeElement", val);
      }*/
      return val;
   }
   
}