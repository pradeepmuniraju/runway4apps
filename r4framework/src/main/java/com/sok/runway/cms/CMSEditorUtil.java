package com.sok.runway.cms;

import com.sok.framework.*;
import javax.servlet.http.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class CMSEditorUtil
{

   public static final String newtext = "Article Text<br><br><br><br>";
   
   public static RowBean getNewArticle(HttpServletRequest request, RowBean newsletter, ArrayList newslist)
   {
      RowBean news = new RowBean();
        news.put("Title", "New Title");
        
        String nab = (String)request.getAttribute("NewArticleBody"); 
        if(nab != null && nab.length()>0) { 
        	news.put("Overview", nab);
	        news.put("Body", nab);
        } else { 
	        news.put("Overview", newtext);
	        news.put("Body", newtext);
        } 
        news.put("ListImage", "cms/thumbnails/select_image.jpg");
        news.put("Image1", "cms/thumbnails/select_image.jpg");
        news.put("Format", "/cms/newsletters/formats/standard_format");
            
        String cmsid = newsletter.getString("CMSID");
        
        if(cmsid != null)
        {
         news.setToNewID("PageID");
         news.setTableSpec("Pages");
         news.setAction("insert");
         
         int sortnumber = newslist.size();
         
         /* size is not necessarily the highest SortNumber - ie if pages have just been deleted. */ 
         if(newslist.size()>0)
         {
	         try { 
	        	 sortnumber = Integer.parseInt(((RowBean)newslist.get(newslist.size()-1)).getString("SortNumber"));
	        	 sortnumber++;
	         } catch (NumberFormatException nfe) { }
         }
         
         TableBean linkbean = new TableBean();
         linkbean.setRequest(request);
         linkbean.setTableSpec("PageLinks");
         linkbean.setToNewID("LinkID");
         linkbean.setColumn("CMSID",cmsid);
         linkbean.setColumn("ParentID",newsletter.getString("PageID"));
         linkbean.setColumn("Path",newsletter.getString("Path")+"/"+KeyMaker.getTimeInHex());
         //linkbean.setColumn("Active","Inactive");
         linkbean.setColumn("Active","Active"); 
         linkbean.setColumn("SortOrder",String.valueOf(sortnumber)); 
         
         linkbean.setColumn("PageID",news.getString("PageID"));
         linkbean.setColumn("Type","Default");
         linkbean.setColumn("ExternalPath",linkbean.getString("Path"));
         linkbean.setAction("insert");
         linkbean.doAction();
         
         news.setColumn("ExternalPath",linkbean.getString("ExternalPath"));
         
         news.doAction();        
        }
        return(news);
   }

   public static String updateField(RowBean newslettercontext, RowBean newsletter, ArrayList newslist, HttpServletRequest request, String filebase, StringBuffer error)
   {
      String returnvalue = "";
      String name = request.getParameter("-name");
      String value = request.getParameter("-value");
      try {
    	  value = StringUtil.urlDecode(request.getParameter("-value") != null? request.getParameter("-value") : "");
      } catch (Exception e) {
    	  value = request.getParameter("-value");
      }
      
      int width = getInt(request, "-width"); 
      int height = getInt(request, "-height");
            
      if(width > 0 || height >0)
      {
         value = resize(filebase, request, value, width, height);
         returnvalue = value;
      }     
      
      updateField(newslettercontext, newsletter, newslist, request, name, value, error);         
      
      return(returnvalue);
   }
   
   public static void updateField(RowBean newslettercontext, RowBean newsletter, ArrayList newslist, HttpServletRequest request, String name, String value, StringBuffer error)
   {
      int index = getIndex(request);   
            
      if(index>-1)
      {
         if("LinkTemplate".equals(name))
         {
            updateArticleLink(request, newslist, index, name, value, error);
         }
         else
         {
            updateArticle(request, newslist, index, name, value, error);
         }
      }
      else if(index==-100)
      {
         updateNewsletter(request, newsletter, name, value, error);
      }
      else if(index==-200)
      {
         updateCMS(request, newsletter, name, value, error);
      }     
      
   }
   
   public static void updateNewsletter(HttpServletRequest request, RowBean newsletter, String name, String value, StringBuffer error)
   {        
      GenRow row = new GenRow();
      row.setRequest(request);
      row.setTableSpec("Pages");
      row.setParameter(name, value);
      row.setParameter("PageID", newsletter.getString("PageID"));
      row.doAction("update");    
      
      error.append(row.getError());
   }  
   
   public static void updateCMS(HttpServletRequest request, RowBean newsletter, String name, String value, StringBuffer error)
   {        
      GenRow row = new GenRow();
      row.setRequest(request);
      row.setTableSpec("CMS");
      row.setParameter(name, value);
      row.setParameter("CMSID", newsletter.getString("CMSID"));
      row.doAction("update");    
      
      error.append(row.getError());
   }     
   
   public static void updateArticleLink(HttpServletRequest request, ArrayList newslist, int index, String name, String value, StringBuffer error)
   {  
      RowBean article = (RowBean)newslist.get(index);
      
      GenRow row = new GenRow();
      row.setRequest(request);
      row.setTableSpec("PageLinks");
      row.setParameter(name, value);
      row.setParameter("LinkID", article.getString("LinkID"));
      row.doAction("update");
      
      error.append(row.getError());
   }  
   
   public static void updateArticle(HttpServletRequest request, ArrayList newslist, int index, String name, String value, StringBuffer error)
   {  
      RowBean article = (RowBean)newslist.get(index);
      
      GenRow row = new GenRow();
      row.setRequest(request);
      row.setTableSpec("Pages");
      row.setParameter(name, value);
      if(name.equals("Title"))
      {
         row.setParameter("TrackResponse", getTrackResponse(value));
      }
      row.setParameter("PageID", article.getString("PageID"));
      row.doAction("update");
      
      error.append(row.getError());
   }
   
   public static String getTrackResponse(String s)
   {
      StringBuffer sb = new StringBuffer();
      sb.append("Clicked: ");
      sb.append(StringUtil.stripHtml(s));
      if(sb.length()>50)
      {
         sb.delete(46, sb.length());
         sb.append("...");
      }
      System.out.println(s.length());
      System.out.println(sb.length());
      return(sb.toString());
   }
   
   public static void deleteArticle(HttpServletRequest request, ArrayList newslist, StringBuffer error)
   {  
      int index = getIndex(request);

      if(index>-1)
      {  
         RowBean article = (RowBean)newslist.remove(index);
         GenRow row = new GenRow();
         row.setRequest(request);
         row.setTableSpec("Pages");
         row.setParameter("PageID", article.getString("PageID"));
         row.doAction("delete");
         error.append(row.getError());
         
         row.setRequest(request);
         row.setTableSpec("PageLinks");
         row.setParameter("LinkID", article.getString("LinkID"));
         row.doAction("delete");       
         error.append(row.getError());
         
         error.append(article.getString("PageID"));
      }
      else
      {
         error.append("Article Not Found");
      }
   }  
   
   public static void updateSortOrder(HttpServletRequest request, ArrayList newslist, StringBuffer error)
   {
      for(int i=0; i< newslist.size(); i++)
      {
         updateArticleLink(request, newslist, i, "SortOrder", String.valueOf(i), error);
      }
   }
   
   public static void copyArticlesText(HttpServletRequest request, ArrayList newslist, String editmode, StringBuffer error)
   {
      RowBean article = null;
      for(int i=0; i< newslist.size(); i++)
      {
         article = (RowBean)newslist.get(i);

         if(article.getString("Overview").length()==0 || article.getString("Overview").equals(newtext))
         {
            updateArticle(request, newslist, i, "Overview", article.getString("Body"), error);
         }

         if(article.getString("Body").length()==0 || article.getString("Body").equals(newtext))
         {
            updateArticle(request, newslist, i, "Body", article.getString("Overview"), error);
         }

      }
   }
   
   public static void moveupArticle(HttpServletRequest request, ArrayList newslist, StringBuffer error)
   {  
      int index = getIndex(request);

      if(index>0)
      {  
         Object temp = newslist.get(index);
         newslist.set(index, newslist.get(index-1));
         newslist.set(index-1, temp);
         updateSortOrder(request, newslist, error);         
      }
   }
      
   public static void movedownArticle(HttpServletRequest request, ArrayList newslist, StringBuffer error)
   {  
      int index = getIndex(request);

      if(index>-1 && index < newslist.size())
      {  
         Object temp = newslist.get(index);
         newslist.set(index, newslist.get(index+1));
         newslist.set(index+1, temp);
         updateSortOrder(request, newslist, error);      
      }
   }
   
   public static int getInt(HttpServletRequest request, String name)
   {
      String value = request.getParameter(name);   
      int intvalue = -1;
      try{
         intvalue = Integer.parseInt(value);
      }catch(Exception e){}      
      
      return(intvalue);
   }
   
   public static int getIndex(HttpServletRequest request)
   {
      String ref = request.getParameter("-ref");   
      int index = 0;
      if(ref!=null)
      {
         if("newsletter".equals(ref))
         {
            return(-100);
         }
         else if("cms".equals(ref))
         {
            return(-200);
         }        
         else
         {
            try{
               index = Integer.parseInt(ref);
            }catch(Exception e){}      
         }
      }
      return(index-1);
   }
   
   public static String resize(String filebase, HttpServletRequest request, String value, int width, int height)
   {
      String contextpath = filebase;
      String filepath = contextpath + "/" + value;
      ImageUtil image = new ImageUtil(filepath);
      String sizeid = null;
      
      if(width>0 && height>0)
      {
         image.resize(width, height);
         sizeid = new StringBuffer("W").append(String.valueOf(width)).append("xH").append(String.valueOf(height)).toString();
      }
      else if(width>0)
      {
         image.resizeToWidth(width);
         sizeid = new StringBuffer("W").append(String.valueOf(width)).toString();
      }
      else if(height>0)
      {
         image.resizeToHeight(height);
         sizeid = new StringBuffer("H").append(String.valueOf(height)).toString();
      }  
      String newfilepath = getResizedName(value, sizeid);
      image.saveImage(contextpath + "/" + newfilepath);
      return(newfilepath);
   }
   
   public static String getResizedName(String value, String size)
   {
      String suffix = value.substring(value.lastIndexOf('.'), value.length());
      String path = value.substring(0, value.lastIndexOf('.'));
      return(new StringBuffer(path).append(size).append(suffix).toString());
   }
}