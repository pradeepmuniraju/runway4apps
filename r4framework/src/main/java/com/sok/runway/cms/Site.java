package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.crm.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class Site extends RunwayEntity {
      
   public static String EDIT_PATH = "crm/cms2/editcontents.jsp";
   public static String CSS_STYLE_EDIT = "EDITABLE_AREA";
   public static String CSS_STYLE_INSERT = "EDITABLE_AREA_INSERT";
   
   public Site(Connection con) {
      super(con, "cms/SiteView");
   }
   
   public Site(Connection con, String siteID) {
      this(con);
      load(siteID);
   }
   
   public static Collection getSiteIDs(Connection con) {
      ArrayList ids = new ArrayList();
      
      RowSetBean siteList = new RowSetBean();
      siteList.setViewSpec("cms/SiteIDView");
      siteList.setConnection(con);
      siteList.sortBy("Sites.Name",0);
      siteList.generateSQLStatement();
      siteList.getResults();
      
      if (siteList.getError().length() > 0) {
         throw new RuntimeException(siteList.getError());
      }
      
      while (siteList.getNext()) {
         ids.add(siteList.getString("SiteID"));
      }
      siteList.close();
      return ids;
   }
   
   public Collection getRootBranchIDs() {
      
      ArrayList ids = new ArrayList();
      
      RowSetBean branchList = new RowSetBean();
      branchList.setViewSpec("cms/SiteBranchView");
      branchList.setConnection(getConnection());
      branchList.setColumn("SiteID", getSiteID());
      branchList.setColumn("ParentSiteBranchID", "EMPTY+NULL");
      branchList.sortBy("SiteBranches.SortOrder",0);
      branchList.sortBy("SiteBranches.Name",1);
      branchList.generateSQLStatement();
      branchList.getResults();
      
      if (branchList.getError().length() > 0) {
         throw new RuntimeException(branchList.getError());
      }
      
      while (branchList.getNext()) {
         ids.add(branchList.getString("SiteBranchID"));
      }
      branchList.close();
      return ids;
   }
   public String getSiteID() {
      return getField("SiteID");
   }
   
   public String getFolder() {
      return getField("Folder");
   }
   
   public String getName() {
      return getField("Name");
   }
   
   public String getDescription() {
      return getField("Description");
   }
   
   public String getType() {
      return getField("Type");
   }
   
   public String getActive() {
      return getField("Active");
   }
   
   public String getURLPath() {
      return getField("URLPath");
   }
   
   public String getHomeSiteBranchID() {
      return getField("HomeSiteBranchID");
   }
   
   public String getPageMetaID() {
      return getField("PageMetaID");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("Site");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SiteID","SYSTEM ERROR - SiteID");
         val.addMandatoryField("HomeSiteBranchID","SYSTEM ERROR - HomeSiteBranchID");
         val.addMandatoryField("Folder","Folder must not be blank.");
         val.addMandatoryField("Name","Name must not be blank.");
         val.addMandatoryField("Type","Comment must not be blank.");
         val.addMandatoryField("URLPath","URLPath must not be blank.");

         Validator.addValidator("Site", val);
      }
      return val;
   }
   
   /**
    * Overrides parent insert - Creates Site Record and Home Branch Record
    */
   public ErrorMap insert() {
      String homeBranchID = KeyMaker.generate();
      setField("HomeSiteBranchID", homeBranchID);
      
      ErrorMap errors = super.insert();
      
      if (errors.isEmpty()) {
         Branch branch = new Branch(getConnection());
         branch.setField("SiteBranchID",homeBranchID);
         branch.setField("SiteID",getSiteID());
         branch.setField("URLPath","/"+getURLPath());
         branch.setField("Name","Home");
         errors.putAll(branch.insert());
      }
      return errors;
   }
   
   public Collection getPageTypeIDs() {
      return PageType.getPageTypeIDs(getConnection(), getSiteID());
   }
}