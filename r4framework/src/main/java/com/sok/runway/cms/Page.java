package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.crm.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated = CMS2
 */
public class Page extends RunwayEntity {
   
   public Page(Connection con) {
      super(con, "cms/SitePageView");
   }
   
   public Page(Connection con, String pageID) {
      this(con);
      load(pageID);
   }
   
   public String getSitePageID() {
      return getField("SitePageID");
   }
	
   public String getSitePageTypeID() {
      return getField("SitePageTypeID");
   }
   
   public String getResponse() {
      return getField("Response");
   }
   
   public String getOwnerUserID() {
      return getField("OwnerUserID");
   }
   
   public String getStatus() {
      return getField("Status");
   }
   
   public String getEditingUserID() {
      return getField("EditingUserID");
   }
   
   public String getNavigationLevel() {
      return getField("NavigationLevel");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("Page");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SitePageID","SYSTEM ERROR - SitePageID");
         val.addMandatoryField("SitePageTypeID","SYSTEM ERROR - SitePageTypeID");
         val.addMandatoryField("OwnerUserID","Owner must be specified.");
         val.addMandatoryField("Status","Status must not be blank.");

         Validator.addValidator("Page", val);
      }
      return val;
   }
 
   public void attachDocuments(String[] documentIDs) {
      if (documentIDs != null) {
         PageDocument pageDoc = new PageDocument(getConnection());
         for (int i=0; i < documentIDs.length; i++) {
            pageDoc.clear();
            pageDoc.setField("SitePageDocumentID", KeyMaker.generate());
            pageDoc.setField("DocumentID", documentIDs[i]);
            pageDoc.setField("SitePageID", getSitePageID());
            pageDoc.setField("Status", PageDocument.STATUS_PENDING_LIVE);
            ErrorMap errors = pageDoc.insert();
            if (errors.size() > 0) {
               throw new RuntimeException(errors.toString());
            }
         }
      }
   }  
 
   public void attachProducts(String[] productIDs) {
      if (productIDs != null) {
         PageProduct pageProduct = new PageProduct(getConnection());
         for (int i=0; i < productIDs.length; i++) {
            pageProduct.clear();
            pageProduct.setField("SitePageProductID", KeyMaker.generate());
            pageProduct.setField("ProductID", productIDs[i]);
            pageProduct.setField("SitePageID", getSitePageID());
            pageProduct.setField("Status", PageProduct.STATUS_PENDING_LIVE);
            ErrorMap errors = pageProduct.insert();
            if (errors.size() > 0) {
               throw new RuntimeException(errors.toString());
            }
         }
      }
   }  
}