package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class PageTask extends RunwayEntity {
   
   public PageTask(Connection con) {
      super(con, "cms/SitePageTaskView");
   }
   
   public PageTask(Connection con, String pageTaskID) {
      this(con);
      load(pageTaskID);
   }
	
   public String getSitePageTaskID() {
      return getField("SitePageTaskID");
   }
   
   public String getSitePageID() {
      return getField("SitePageID");
   }
   
   public String getTaskDate() {
      return getField("TaskDate");
   }
   
   public String getStatus() {
      return getField("Status");
   }
   
   public String getUserID() {
      return getField("UserID");
   }
   
   public String getComment() {
      return getField("Comment");
   }
   
   public String getDueDate() {
      return getField("DueDate");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("PageTask");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SitePageTaskID","SYSTEM ERROR - SitePageTaskID");
         val.addMandatoryField("SitePageID","SYSTEM ERROR - SitePageID");
         val.addMandatoryField("Task Date","Task Date must not be blank.");
         val.addMandatoryField("Status","Status must not be blank.");
         val.addMandatoryField("UserID","User must not be blank.");
         val.addMandatoryField("Comment","Comment must not be blank.");

         Validator.addValidator("PageTask", val);
      }
      return val;
   }
   
}