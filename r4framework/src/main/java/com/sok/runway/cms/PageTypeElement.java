package com.sok.runway.cms;

import com.sok.framework.*;
import com.sok.runway.*;

import java.sql.*;
import java.util.*;

/**
 * @deprecated CMS2
 */
public class PageTypeElement extends RunwayEntity {
   
   public static final String TYPE_TEXT =  "Text";
   public static final String TYPE_IMAGE =  "Image";
   
   public PageTypeElement(Connection con) {
      super(con, "cms/SitePageTypeElementView");
   }
   
   public PageTypeElement(Connection con, String pageTypeElementID) {
      this(con);
      load(pageTypeElementID);
   }
   
   public static Collection getPageTypeElementIDs(Connection con, String pageTypeID) {
      return getPageTypeElementIDs(con, pageTypeID, null);
   }
   
   public static Collection getPageTypeElementIDs(Connection con, String pageTypeID, String elementType) {
      ArrayList ids = new ArrayList();
      
      RowSetBean pageTypeElementLists = new RowSetBean();
      pageTypeElementLists.setViewSpec("cms/SitePageTypeElementIDView");
      pageTypeElementLists.setConnection(con);
      pageTypeElementLists.setColumn("SitePageTypeID", pageTypeID);
      if (elementType != null) {
         pageTypeElementLists.setColumn("Type", elementType);
      }
   	pageTypeElementLists.sortBy("Type",0);
   	pageTypeElementLists.sortOrder("DESC",0);
   	pageTypeElementLists.sortBy("Name",1);
      pageTypeElementLists.generateSQLStatement();
      pageTypeElementLists.getResults();
      
      if (pageTypeElementLists.getError().length() > 0) {
         throw new RuntimeException(pageTypeElementLists.getError());
      }
      
      while (pageTypeElementLists.getNext()) {
         ids.add(pageTypeElementLists.getString("SitePageTypeElementID"));
      }
      pageTypeElementLists.close();
      return ids;
   }
	
   public String getSitePageTypeElementID() {
      return getField("SitePageTypeElementID");
   }
	
   public String getSitePageTypeID() {
      return getField("SitePageTypeID");
   }
   
   public String getName() {
      return getField("Name");
   }
   
   public String getDescription() {
      return getField("Description");
   }
   
   public String getType() {
      return getField("Type");
   }
   
   public boolean isText() {
      return getField("Type").equals("Text");
   }
   
   public boolean isImage() {
      return getField("Type").equals("Image");
   }
   
   public int getMaxSize() {
      int size = -1;
      try {
         size = Integer.parseInt(getField("MaxSize"));
      }
      catch (NumberFormatException e) {}
      return size;
   }
   
   public boolean supportsMultiple() {
      return getField("Multiple").equals("Y");
   }
   
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("PageTypeElement");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SitePageTypeElementID","SYSTEM ERROR");
         val.addMandatoryField("SitePageTypeID","SYSTEM ERROR");
         val.addMandatoryField("Name","Page Element must have a name.");
         HashSet set = new HashSet(2);
         set.add(TYPE_TEXT);
         set.add(TYPE_IMAGE);
         val.addRestrictedValueField("Type",set);
         Validator.addValidator("PageTypeElement", val);
      }
      return val;
   }
}