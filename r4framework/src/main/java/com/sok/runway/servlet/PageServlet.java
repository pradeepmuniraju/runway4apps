package com.sok.runway.servlet;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;

import com.sok.framework.ActionBean;

import com.sok.runway.*;
import com.sok.runway.security.*;

public class PageServlet extends HttpServlet {

	public static final String DEFAULT_PROPERTIES = "runway";
	public static final String SPECIFIC_PROPERTIES = "specific";
	public static final String ROOT_PATH_PROPERTY = "root";
	public static final String INTERFACE_PATH_NAME = "interfacePath";
	public static final String ROLE_PROPERTY_PATH = "/WEB-INF/roles/";
	private static String propertiesPath = null;

	private static HashMap<String, Properties> roleProperties = new HashMap<String, Properties>();

	public void init(ServletConfig config) {
		try {
			propertiesPath = config.getServletContext().getRealPath(
					ROLE_PROPERTY_PATH);

			loadProperties(DEFAULT_PROPERTIES);
		} catch (Exception e) {
			System.out.print("PageServlet: could not create initialise: ");
			System.out.print(e.getMessage());
		}
	}

	public synchronized static void reloadProperties(String role) {
		PageServlet.roleProperties.remove(role);
	}

	public static Properties getProperties(String role) {
		Properties properties = PageServlet.roleProperties.get(role);

		if (properties == null) {
			properties = loadProperties(role);
		}
		return properties;
	}

	private static synchronized Properties loadProperties(String role) {

		Properties properties = PageServlet.roleProperties.get(role);
		if (properties == null) {
			File defaultProperties = new File(propertiesPath, role
					+ ".properties");

			properties = new Properties();
			try {
				FileInputStream in = new FileInputStream(defaultProperties);
				properties.load(in);
				in.close();
			} catch (Exception e) {
				System.out.print("PageServlet: could not load properties:"
						+ role);
				System.out.print(e.getMessage());
			}

			/*
			 * if using default properties, check if specific default properties
			 * exist
			 */
			if (DEFAULT_PROPERTIES.equals(role)) {
				File specificProperties = new File(propertiesPath,
						SPECIFIC_PROPERTIES + ".properties");
				if (specificProperties.exists()) {
					try {
						FileInputStream in = new FileInputStream(
								specificProperties);
						properties.load(in);
						in.close();
					} catch (Exception e) {
						System.out
								.print("PageServlet: could not load properties:"
										+ role);
						System.out.print(e.getMessage());
					}
				}
			}

			PageServlet.roleProperties.put(role, properties);
		}
		return properties;
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, FileNotFoundException, IOException {
		doRequest(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, FileNotFoundException, IOException {
		doRequest(req, res);
	}

	public void doRequest(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, FileNotFoundException, IOException {

		StringBuffer url = req.getRequestURL();
		int i = url.lastIndexOf("/");
		
		String page = url.substring(i + 1);
		
		String dest = null;
		ItemLists itemlists = null;
		StatusList statuslist = null;
		boolean forward = false;

		if (page.length() != 0) {
			if(!req.isSecure()){
				   //no cache breaks downloading pdf in ie over https
				res.setHeader("Expires","Mon, 26 Jul 1997 05:00:00 GMT" );  // disable IE caching
				res.setHeader("Cache-Control","no-cache, must-revalidate" );
				res.setHeader("Pragma","no-cache" );
				res.setDateHeader ("Expires", 0);
			}

			HttpSession session = req.getSession();

			if (session != null) {
				itemlists = (ItemLists) session.getAttribute("itemlists");
				statuslist = (StatusList) session.getAttribute("statuslist");

				String sqlJndiName = (String) session.getServletContext()
						.getInitParameter("sqlJndiName");

				if (itemlists == null) {
					itemlists = new ItemLists();
					session.setAttribute("itemlists", itemlists);
					itemlists.setJndiName(sqlJndiName);
				}
				if (statuslist == null) {
					statuslist = new StatusList();
					session.setAttribute("statuslist", statuslist);
					statuslist.setJndiName(sqlJndiName);
				}

				itemlists.setConnection(req.getAttribute(ActionBean.CON));
				statuslist.setConnection(req.getAttribute(ActionBean.CON));

				itemlists.setUseDefaults(!page.endsWith(".search"));
			}

			if (page.endsWith(".pdf")) {	
				forward = true;
			} else if (page.endsWith(".csv")) {
				forward = true;
			} else if (page.endsWith(".xls")) {
				forward = true;
			} else if (page.endsWith(".action")) {
				forward = true;
			} else if (page.endsWith(".upload")) {
				forward = true;
			}
			if (!forward) {
				res.setContentType("text/html;charset=UTF-8");
			}

			dest = getMapping(req, page);
		}

		if (dest == null) {
			throw new FileNotFoundException("The " + dest
					+ " page was not found.");
		} else {

			if (forward) {
				req.getRequestDispatcher(dest).forward(req, res);
			} else {
				req.getRequestDispatcher(dest).include(req, res);

				if (req.getAttribute("-redirect") != null) {
					res.sendRedirect((String) req.getAttribute("-redirect"));
				}
			}
		}
	}

	public static String getMapping(HttpServletRequest req, String page) {
		String role = getRolePropertyFile(req);
		return (getMapping(req, role, page));
	}

	public static String getMapping(HttpServletRequest req, String role,
			String page) {
		Properties properties = getProperties(role);
		String dest = null;

		if (properties != null) {
			dest = properties.getProperty(page);
			if (dest == null) {
				properties = getProperties(DEFAULT_PROPERTIES);
				dest = properties.getProperty(page);
			}
		} else {
			properties = getProperties(DEFAULT_PROPERTIES);
			dest = properties.getProperty(page);
		}

		if (dest != null) {
			/*
			 * use request attribute to avoid concurrent request issues when
			 * reusing paths in another role
			 */
			req.getSession().setAttribute(INTERFACE_PATH_NAME,
					properties.getProperty(ROOT_PATH_PROPERTY));
			req.setAttribute(INTERFACE_PATH_NAME, properties
					.getProperty(ROOT_PATH_PROPERTY));
		}

		return (dest);
	}

	private static String getRolePropertyFile(HttpServletRequest request) {

		String rolePropertyFile = null;
		HttpSession session = request.getSession(false);

		if (session != null) {
			UserBean currentuser = (UserBean) session
					.getAttribute(SessionKeys.CURRENT_USER);

			if (currentuser != null) {
				String defaultRoleID = currentuser.getString("DefaultRoleID");

				Map roleMap = currentuser.getUser().getRoles();
				if (roleMap.size() > 0) {
					Role role = null;
					if (defaultRoleID.length() > 0) {
						role = (Role) roleMap.get(defaultRoleID);
					}
					//if it can't find the selected role, use the first one in the list.
					if (role == null) {
						role = (Role) roleMap.values().iterator().next();
					}
					rolePropertyFile = role.getField("RolePropertiesFile");
				}
			}
		}
		if (rolePropertyFile == null || rolePropertyFile.length() == 0) {
			rolePropertyFile = DEFAULT_PROPERTIES;
		}
		return rolePropertyFile;

	}

}