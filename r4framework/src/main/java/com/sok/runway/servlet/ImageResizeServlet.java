package com.sok.runway.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.InitServlet;
import com.sok.framework.tags.ResizeImage;

/**
 * Take a partial file path that includes the real one and resize the image.
 * @author mike
 *
 */
public class ImageResizeServlet extends HttpServlet {

	private static final Logger logger = LoggerFactory.getLogger(ImageResizeServlet.class);
	private static final long serialVersionUID = 1L;

	/**
	 * Takes a path and returns the resized image at that path. 
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		final String newPath = doRequest(req, res);
		if(newPath != null) 
			res.sendRedirect(newPath);
	}
	
	/**
	 * Takes a path and returns a path to a resized image.
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		final String newPath = doRequest(req, res);
		if(newPath != null) { 
			res.setContentType("text/plain");
			res.setContentLength(newPath.length());
			java.io.PrintWriter pw = new java.io.PrintWriter(res.getOutputStream());
			pw.print(newPath);
			pw.flush();
			pw.close();
			res.flushBuffer();
		}
	}
	
	public String doRequest(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		logger.trace("pathInfo: [{}]", req.getPathInfo());
		final String path = req.getPathInfo().substring(1);	
		logger.trace("path: [{}]", path);
		
		final int pi = path.indexOf("/");
		final String params = path.substring(0,pi);
		final String file = path.substring(pi);
		
		logger.trace("params: [{}]", params);
		logger.trace("file: [{}]", file);
		
		final String realFile = InitServlet.getRealPath(file);
		logger.trace("realFile: [{}]", realFile);
		
		File f = new File(realFile);
		
		if(!f.exists()) {
			res.setStatus(404);
			return null;
		}
		
		final int ai = params.indexOf('&');
		final String lw = ai > -1 ? params.substring(0, ai): params;
		final int xi = lw.indexOf("x");
		if(xi > 0) {
			String ws = null, hs = null;
			try {
				ws = lw.substring(0,xi);
				hs = lw.substring(xi+1);
				logger.trace("[{}]x[{}]", ws, hs);
			} catch (Exception e) {
				logger.error("Exception parsing params", e);
			}
			if(StringUtils.isNotBlank(ws) && StringUtils.isNotBlank(hs)) {
				final ResizeImage ri = new ResizeImage();
				ri.setSrc(file);
				ri.setWidth(ws);
				ri.setHeight(hs);
				ri.setTag("false");
				ri.setDebug(String.valueOf(logger.isDebugEnabled()));
				final String newPath = ri.getImage();
				if(logger.isDebugEnabled()) { 
					logger.debug("newPath: [{}]", newPath);
					logger.debug(ri.getDebugString());
				}
				return newPath;
			}
		}
		logger.error("Returning original file for some reason for path [{}]", path);  
		return req.getContextPath() + file;
	}
}
