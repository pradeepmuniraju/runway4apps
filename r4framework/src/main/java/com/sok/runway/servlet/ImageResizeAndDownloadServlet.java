package com.sok.runway.servlet;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.cms.properties.PropertyEntity.ResizeOption;
import com.sok.runway.util.ImageResizeUtil;

/**
 * Downloading Multiple Files As Zip
 * 
 * @author JavaDigest
 */
public class ImageResizeAndDownloadServlet extends HttpServlet {

	private static final long serialVersionUID = -7767828383799037391L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			processRequest(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			processRequest(request, response);
	}

	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// List of files to be downloaded
		List<File> files = new ArrayList<File>();
		
		try {

			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			
			ServletOutputStream out = response.getOutputStream();
			ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(out));

//			DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
//			Date date = new Date();
//			String strDate = dateFormat.format(date);
			
			// Set the content type based to zip
			response.setContentType("Content-type: text/zip");
			response.setHeader("Content-Disposition", "attachment; filename=Product_Images.zip");

			boolean isShowOnPortalOnly = "Y".equalsIgnoreCase(request.getParameter("showOnPortalOnly"));
			boolean isShowOnEmailOnly = "Y".equalsIgnoreCase(request.getParameter("showOnEmailOnly"));
			boolean isShowOnWebsiteOnly = "Y".equalsIgnoreCase(request.getParameter("showOnWebsiteOnly"));
			
			String productID = request.getParameter("ProductID");
			int maxWidth = 0;
			String tmpMaxW = request.getParameter("maxWidth");
			if (StringUtils.isNotBlank(tmpMaxW) && !"null".equalsIgnoreCase(tmpMaxW)) {
				try {
					maxWidth = Integer.parseInt(tmpMaxW);
				}catch(Exception ee) {
					ee.printStackTrace();
					maxWidth = 0;
				}
			}
			int maxHeight = 0;
			String tmpMaxH = request.getParameter("maxHeight");
			if (StringUtils.isNotBlank(tmpMaxH) && !"null".equalsIgnoreCase(tmpMaxH)) {
				try {
					maxHeight = Integer.parseInt(tmpMaxH);
				}catch(Exception ee) {
					ee.printStackTrace();
					maxHeight = 0;
				}
			}
			
			
			String imgPath = "";
			String imgTypeExtension = "";
			String imgFilePath = "";
	
			
			// To get the Floor Plan image
			 GenRow planDataRow = new GenRow();	
			 planDataRow.setViewSpec("properties/ProductPlanView");	
			 planDataRow.setParameter("ProductType", "Home Plan");
			 if (StringUtils.isNotBlank(productID)){
				 planDataRow.setParameter("ProductID", productID);
				 planDataRow.doAction("selectfirst");
			 }
			 System.out.println("ImageResizeAndDownloadServlet ==> Plan Query is:  " + planDataRow.getStatement());
			 String homeDesignProdID = planDataRow.getString("HomeProductID");
			 imgPath = planDataRow.getString("ImagePath");
			 try {
				 String tmpFilePath = InitServlet.getRealPath("/" + imgPath);						
				 if (maxWidth <=0 && maxHeight<=0) {
					 BufferedImage bimg = ImageIO.read(new File(tmpFilePath));
					 maxWidth  = bimg.getWidth();
					 maxHeight = bimg.getHeight();
				 }
			 } catch(Exception ee) {
				 ee.printStackTrace();
			 }
			
			 try {
				 imgTypeExtension = imgPath.substring((imgPath.lastIndexOf(".")+1), imgPath.length());
				 imgFilePath = ImageResizeUtil.imageResizeByHeightWidthAbsPathOutput(imgPath, maxWidth, maxHeight, ResizeOption.Crop, imgTypeExtension);
				 if (StringUtils.isNotBlank(imgFilePath)){
					 files.add(new File(imgFilePath));

					 addFilesInZip (out, zos, files, "Floorplan_");
					 files = new ArrayList<File>();
				 }
			 }
			 catch(Exception ee) {
				 ee.printStackTrace();
			 }
			 
			 
			
			 
			 if (StringUtils.isNotBlank(homeDesignProdID)) {
				 // To get the Document and Images
				 GenRow	 docImgDataRow = new GenRow();
				 docImgDataRow.setViewSpec("LinkedDocumentShortView");
				 docImgDataRow.setColumn("ProductID", homeDesignProdID);

				  if (isShowOnPortalOnly) {
					  docImgDataRow.setColumn("ShowOnPortal", "Y");
				  }

				  if (isShowOnEmailOnly) {
					  docImgDataRow.setColumn("ShowOnEmail", "Y");
				  }
				  
				  if (isShowOnWebsiteOnly) {
					  docImgDataRow.setColumn("ShowOnWebsite", "Y");
				  }
				  
				 docImgDataRow.sortBy("LinkedDocuments.ShowOnGalleryOrder",0);
				 if (docImgDataRow.getString("ProductID").length() > 0) {
					 docImgDataRow.doAction("search");
					 docImgDataRow.getResults(true);
				 }
				 System.out.println("ImageResizeAndDownloadServlet ==> docImgDataRow Query is:  " + docImgDataRow.getStatement());
				while(docImgDataRow.getNext()) {
						imgPath = docImgDataRow.getData("FilePath");
						try {
							String tmpFilePath = InitServlet.getRealPath("/" + imgPath);						
							if (maxWidth <=0 && maxHeight<=0) {
								BufferedImage bimg = ImageIO.read(new File(tmpFilePath));
								maxWidth  = bimg.getWidth();
								maxHeight = bimg.getHeight();
							}
						}
						catch(Exception ee) {
							ee.printStackTrace();
						}
						
						try {
							
							if ("Document".equalsIgnoreCase(docImgDataRow.getData("DocumentSubType")) || imgPath.toLowerCase().endsWith("PDF") || imgPath.toLowerCase().endsWith("doc") || imgPath.toLowerCase().endsWith("docx")) {
								imgFilePath = homePath + "/" + imgPath;
								if (!StringUtil.isBlankOrEmpty(imgFilePath))
								{	
									if(imgFilePath.indexOf(homePath) == 0) {
										imgFilePath = imgFilePath.substring(homePath.length());
									}
									if(imgFilePath.startsWith("/")) {
										imgFilePath = imgFilePath.substring(1);
									}
								}
								imgFilePath = InitServlet.getRealPath("/" + imgFilePath);
								
							} else{
								imgTypeExtension = imgPath.substring((imgPath.lastIndexOf(".")+1), imgPath.length());
								imgFilePath = ImageResizeUtil.imageResizeByHeightWidthAbsPathOutput(imgPath, maxWidth, maxHeight, ResizeOption.Crop, imgTypeExtension);
							}
							
							if (StringUtils.isNotBlank(imgFilePath)) {
								files.add(new File(imgFilePath));
							}
						} 
						catch(Exception ee) {
							ee.printStackTrace();
						}	
				}
				docImgDataRow.close();

				 addFilesInZip (out, zos, files, "Home_Design_");
				 files = new ArrayList<File>();


				
				// To get the Facade Images
				GenRow facadeDataRow = new GenRow();
				//facadeDataRow.setRequest(request);
				facadeDataRow.setViewSpec("ProductLinkedProductView");
				facadeDataRow.setColumn("ProductID", homeDesignProdID);
				facadeDataRow.setColumn("ProductProductsLinkedProduct.ProductType","Home Facade");
				if (facadeDataRow.getString("ProductID").length() > 0) {
					facadeDataRow.doAction("search");
					facadeDataRow.getResults(true);
				}
				System.out.println("ImageResizeAndDownloadServlet ==> facadeDataRow Query is:  " + facadeDataRow.getStatement());
				while(facadeDataRow.getNext()) {
					imgPath = facadeDataRow.getData("ImagePath");
					try {
						String tmpFilePath = InitServlet.getRealPath("/" + imgPath);						
						if (maxWidth <=0 && maxHeight<=0) {
							BufferedImage bimg = ImageIO.read(new File(tmpFilePath));
							maxWidth  = bimg.getWidth();
							maxHeight = bimg.getHeight();
						}
					}
					catch(Exception ee) {
						ee.printStackTrace();
					}
					
					try {
						
						if ("Document".equalsIgnoreCase(facadeDataRow.getData("DocumentSubType")) || imgPath.toLowerCase().endsWith("PDF") || imgPath.toLowerCase().endsWith("doc") || imgPath.toLowerCase().endsWith("docx")) {
							imgFilePath = homePath + "/" + imgPath;
							if (!StringUtil.isBlankOrEmpty(imgFilePath))
							{	
								if(imgFilePath.indexOf(homePath) == 0) {
									imgFilePath = imgFilePath.substring(homePath.length());
								}
								if(imgFilePath.startsWith("/")) {
									imgFilePath = imgFilePath.substring(1);
								}
							}
							imgFilePath = InitServlet.getRealPath("/" + imgFilePath);
						} else {
							imgTypeExtension = imgPath.substring((imgPath.lastIndexOf(".")+1), imgPath.length());
							imgFilePath = ImageResizeUtil.imageResizeByHeightWidthAbsPathOutput(imgPath, maxWidth, maxHeight, ResizeOption.Crop, imgTypeExtension);
						}
						
						if (StringUtils.isNotBlank(imgFilePath)){
							files.add(new File(imgFilePath));
						}
					}
					catch(Exception ee) {
						ee.printStackTrace();
					}
				}
				facadeDataRow.close();
			 
				 addFilesInZip (out, zos, files, "Facade_");
				 files = new ArrayList<File>();

			 } // End of homeDesignProdID 
			 
			
			
			  GenRow linkedImgs = new GenRow();
			  linkedImgs.setRequest(request);
			  linkedImgs.setViewSpec("LinkedDocumentView");
			  linkedImgs.setColumn("DocumentType", "Image");
			  linkedImgs.setColumn("ProductID", productID);
			  //linkedImgs.setColumn("LinkedDocumentDocument.DocumentCategory", "Content");
			  //linkedImgs.setColumn("ShowOnWebsite", "Y");
			  
			  if (isShowOnPortalOnly) {
				  linkedImgs.setColumn("ShowOnPortal", "Y");
			  }

			  if (isShowOnEmailOnly) {
				  linkedImgs.setColumn("ShowOnEmail", "Y");
			  }
			  
			  if (isShowOnWebsiteOnly) {
				  linkedImgs.setColumn("ShowOnWebsite", "Y");
			  }

			  linkedImgs.sortBy("SortOrder", 0);
			  if(productID != null && !"".equalsIgnoreCase(productID)){
					linkedImgs.doAction(ActionBean.SEARCH);
					linkedImgs.getResults(true);
			  }
			  System.out.println("ImageResizeAndDownloadServlet ==> linkedImgs Query is:  " + linkedImgs.getStatement());
			  
			  while(linkedImgs.getNext()){
				  	imgPath = linkedImgs.getData("FilePath");
					try
					{
						String tmpFilePath = InitServlet.getRealPath("/" + imgPath);						
						if (maxWidth <=0 && maxHeight<=0)
						{
							BufferedImage bimg = ImageIO.read(new File(tmpFilePath));
							maxWidth  = bimg.getWidth();
							maxHeight = bimg.getHeight();
						}
					}
					catch(Exception ee) {
						ee.printStackTrace();
					}
					
					try {
						if ("Document".equalsIgnoreCase(linkedImgs.getData("DocumentSubType")) || imgPath.toLowerCase().endsWith(".pdf") || imgPath.toLowerCase().endsWith(".doc") || imgPath.toLowerCase().endsWith(".docx")) {
							imgFilePath = homePath + "/" + imgPath;
							if (!StringUtil.isBlankOrEmpty(imgFilePath))
							{	
								if(imgFilePath.indexOf(homePath) == 0) {
									imgFilePath = imgFilePath.substring(homePath.length());
								}
								if(imgFilePath.startsWith("/")) {
									imgFilePath = imgFilePath.substring(1);
								}
							}
							imgFilePath = InitServlet.getRealPath("/" + imgFilePath);
							
						} else {
							imgTypeExtension = imgPath.substring((imgPath.lastIndexOf(".")+1), imgPath.length());
							imgFilePath = ImageResizeUtil.imageResizeByHeightWidthAbsPathOutput(imgPath, maxWidth, maxHeight, ResizeOption.Crop, imgTypeExtension);
						}
						
						if (StringUtils.isNotBlank(imgFilePath)){
								files.add(new File(imgFilePath));
						}
					}
					catch(Exception ee) {
						ee.printStackTrace();
					}
			  }
			  linkedImgs.close();
			  
			 addFilesInZip (out, zos, files, "");
			 files = new ArrayList<File>();

			  
			zos.close();
		}
		catch(Exception ee) {
				ee.printStackTrace();
		}
		finally {
			if (files != null)
				files = null;
			
			response.flushBuffer();
		}
	}
	
	public static void addFilesInZip (ServletOutputStream out, ZipOutputStream zos, List<File> files, String imageType) {
		
			try {
					int fileCounter = 1;
					for (File tmpFile : files) {
						System.out.println("Adding file " + tmpFile.getName());
						zos.putNextEntry(new ZipEntry(imageType + (fileCounter++) + "_"+ tmpFile.getName()));
			
						// Get the file
						FileInputStream fis = null;
						try {
							fis = new FileInputStream(tmpFile);
			
						} catch (FileNotFoundException fnfe) {
							// If the file does not exists, write an error entry instead of
							// file
							// contents
							zos.write(("ERROR: Could not find file " + tmpFile.getName()).getBytes());
							zos.closeEntry();
							System.out.println("Could not find file " + tmpFile.getAbsolutePath());
							continue;
						}
			
						BufferedInputStream fif = new BufferedInputStream(fis);
			
						// Write the contents of the file
						int data = 0;
						while ((data = fif.read()) != -1) {
							zos.write(data);
						}
						fif.close();
			
						zos.closeEntry();
						//System.out.println("Finished adding file " + tmpFile.getName());
					}
			} catch (Exception ee) {
				System.out.println("Error in Zipping. addFilesInZip " + ee.getMessage());
				ee.printStackTrace();
			}
	}
}