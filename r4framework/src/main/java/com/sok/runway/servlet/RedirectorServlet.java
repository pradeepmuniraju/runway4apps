package com.sok.runway.servlet;

import java.util.HashMap;
import java.util.Properties;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RedirectorServlet extends HttpServlet {

   public static final String DEFAULT_PROPERTIES = "website";
   public static final String ROLE_PROPERTY_PATH = "/WEB-INF/redirector/";
   private String propertiesPath = null;
   
   private static HashMap redirectorProperties = new HashMap();
   
	public void init(ServletConfig config) {
		try {
			propertiesPath = config.getServletContext().getRealPath(ROLE_PROPERTY_PATH);
						
         loadProperties(DEFAULT_PROPERTIES);
		}
		catch(Exception e) {
			System.out.print("PageServlet: could not create initialise: ");
			System.out.print(e.getMessage());
		}
	}

   public synchronized static void reloadProperties(String redirect) {
      redirectorProperties.remove(redirect);
   }
   
   private Properties getProperties(String redirect) {
      Properties properties = (Properties) redirectorProperties.get(redirect);
			
		if (properties == null) {
		   properties = loadProperties(redirect);
		} 
		return properties;
   }
   
   private synchronized Properties loadProperties(String redirect) {

	  Properties properties = (Properties) redirectorProperties.get(redirect);
	  if (properties == null) {
	      File defaultProperties = new File(propertiesPath, redirect + ".properties");
	      
	      properties = new Properties();
	      try {
	         FileInputStream in = new FileInputStream(defaultProperties);
	         properties.load(in);
	         in.close();
	      }
	      catch (Exception e) {
	         System.out.print("PageServlet: could not load properties:" + redirect);
				System.out.print(e.getMessage());
	      }
	      
	      redirectorProperties.put(redirect,properties);
	  } 
      return properties;
   }
   
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, FileNotFoundException, IOException {
	   doRequest(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, FileNotFoundException, IOException {
	   doRequest(req, res);
	}
	
	public void doRequest(HttpServletRequest req, HttpServletResponse res) throws ServletException, FileNotFoundException, IOException {
	   res.setContentType( "text/html" );
	   
		if(!req.isSecure()){
			   //no cache breaks downloading pdf in ie over https
				res.setHeader("Expires","Mon, 26 Jul 1997 05:00:00 GMT" );  // disable IE caching
				res.setHeader("Cache-Control","no-cache, must-revalidate" );
				res.setHeader("Pragma","no-cache" );
				res.setDateHeader ("Expires", 0);
		}
		
		Properties properties = getProperties(DEFAULT_PROPERTIES);
		
		StringBuffer url = req.getRequestURL();
		int i = url.lastIndexOf("/");
		String page = url.substring(i+1);
		String dest = null; 
	
		dest = properties.getProperty(page);
		
		if (dest == null) {
			throw new java.io.FileNotFoundException(); 
		}
		else {
		   //req.getRequestDispatcher(dest).forward(req, res);
			//res.sendRedirect(dest); 
			
			res.setStatus(301);
			res.setHeader( "Location", dest );
			res.setHeader( "Connection", "close" );
			
		}
		/*
		if (req.getAttribute("-redirect") != null) {
		   res.sendRedirect((String)req.getAttribute("-redirect"));
		}
		*/
	}       
}
