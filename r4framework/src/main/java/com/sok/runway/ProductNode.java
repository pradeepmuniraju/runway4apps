package com.sok.runway;

import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.RowSetBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

public class ProductNode {
	private Vector<ProductNode> 	childrenNodes = new Vector<ProductNode>();
	private ProductNode				parentNode = null;
	
	private String					productID = "";
	private String					productProductID = "";
	private String					productType = "";
	private String					productSubType = "";
	private String					category = "";
	private String					description = "";
	private String					name = "";
	private String					totalCost = "";
	private String					estSettlementDate = "";
	
	private boolean					isOptionEnabled = false;
	
	private GenRow					product = new GenRow();
	
	private HttpServletRequest		request;
		
	public ProductNode(HttpServletRequest request, ProductNode parent) {
		parentNode = parent;
		this.request = request;
	}
	
	public void processBean(RowSetBean bean) {
        while(bean.getNext()) { 
        	
        	ProductNode pNode = new ProductNode(request,this);
        	
        	pNode.setBean(bean);
        	
            
            RowSetBean linkProd = new RowSetBean();
            linkProd.setViewSpec("ProductProductLinkedView");
            linkProd.setRequest(request);
            linkProd.setColumn("ProductID",pNode.getProductID());
            linkProd.sortBy("ProductType",0);
            linkProd.sortBy("Name",1);
            linkProd.doAction(GenerationKeys.SEARCH);
            
            linkProd.getResults(true);
            pNode.processBean(linkProd);
            
            linkProd.close();
            
            childrenNodes.add(pNode);
        }

	}
	
	public void setBean(RowSetBean bean) {
		productID = (this.getParentNode() == null || this.getParentNode().getParentNode() == null)? bean.getString("ProductID") : bean.getString("LinkedProductID");
		productType = bean.getString("ProductType");
		productSubType = bean.getString("ProductSubType");
		category = bean.getString("Category");
		productProductID = bean.getString("ProductProductID");
		
		description = bean.getString("Description");
		name = bean.getString("Name");
		totalCost = bean.getString("TotalCost");
		
		estSettlementDate = bean.getString("EstSettlementDate");
		
		if ("ProductProductLinkedView".equals(bean.getViewSpec().getName())) {
			setOptionEnabled("Y".equals(bean.getString("LinkActive")) && "Y".equals(bean.getString("Active")));
		} else {
			setOptionEnabled("Y".equals(bean.getString("Active")));
		}

		setDescription();
	}
	
	protected void setDescription() {
		if ("Land".equals(productType)) {
			setDescriptionLand();
		} else if ("Apartment".equals(productType)) {
			setDescriptionApartment();
		} else if ("Home Plan".equals(productType)) {
			setDescriptionPlan();
		} else if ("House and Land".equals(productType)) {
			setDescriptionHouseLand();
		} else if ("Existing Property".equals(productType)) {
			setDescriptionExistingProperty();
		}
	}
	
	protected void setDescriptionApartment() {
		String description = "";
		try {
			GenRow row = new GenRow();
			row.setViewSpec("properties/ProductApartmentView");
			row.setRequest(request);
			row.setParameter("ProductID", productID);
			row.doAction(GenerationKeys.SELECTFIRST);
			
			product = row;
		} catch (Exception e){}
	}
	
	protected void setDescriptionLand() {
		String description = "";
		try {
			GenRow row = new GenRow();
			row.setViewSpec("properties/ProductLotView");
			row.setRequest(request);
			row.setParameter("ProductID", productID);
			row.doAction(GenerationKeys.SELECTFIRST);
			
			product = row;
		} catch (Exception e){}
	}
	
	protected void setDescriptionPlan() {
		String description = "";
		try {
			GenRow row = new GenRow();
			row.setViewSpec("properties/ProductPlanView");
			row.setRequest(request);
			row.setParameter("ProductID", productID);
			row.doAction(GenerationKeys.SELECTFIRST);
			
			product = row;
		} catch (Exception e){}
	}
	
	protected void setDescriptionHouseLand() {
		String description = "";
		try {
			GenRow row = new GenRow();
			row.setViewSpec("properties/ProductHouseAndLandView");
			row.setRequest(request);
			row.setParameter("ProductID", productID);
			row.doAction(GenerationKeys.SELECTFIRST);

			product = row;
		} catch (Exception e){}
	}
	
	protected void setDescriptionExistingProperty() {
		try {
			GenRow row = new GenRow();
			row.setViewSpec("properties/ProductExistingPropertyView");
			row.setRequest(request);
			row.setParameter("ProductID", productID);
			row.doAction(GenerationKeys.SELECTFIRST);
			row.setParameter("DeveloperName", row.getData("DeveloperCompany"));
			/*
			 * These will be shown as linked products anyway, i think this is a waste.
			GenRow pp = new GenRow();
			try { 
				pp.setRequest(request);
				pp.setViewSpec("ProductProductLinkedView");
				pp.setParameter("ProductID", productID);
				pp.getResults(); 
				while(pp.getNext()) {
					
					if("Land".equals(pp.getData("ProductType"))) {
						row.setParameter("LotProductID", pp.getData("LinkedProductID"));
						row.setParameter("LotTotalCost", pp.getData("TotalCost"));
						row.setParameter("LotNumber", pp.getData("Name"));
						
						GenRow land = new GenRow();
						land.setViewSpec("properties/ProductLotView");
						land.setRequest(request);
						land.setParameter("ProductID", pp.getData("LinkedProductID"));
						land.doAction(GenerationKeys.SELECT);
						
						if(land.isSuccessful()) {
							row.setParameter("StageName", land.getData("StageName"));
							row.setParameter("StageProductID", land.getData("StageProductID"));
							row.setParameter("EstateName", land.getData("EstateName"));
							row.setParameter("EstateProductID", land.getData("EstateProductID"));
						}
					} else if ("Home Plan".equals(pp.getData("ProductType"))) {
						row.setParameter("PlanProductID", pp.getData("LinkedProductID"));
						row.setParameter("PlanTotalCost", pp.getData("TotalCost"));
						row.setParameter("PlanName", pp.getData("Name"));
						
						GenRow plan = new GenRow();
						plan.setViewSpec("properties/ProductPlanView");
						plan.setRequest(request);
						plan.setParameter("ProductID",  pp.getData("LinkedProductID"));
						plan.doAction(GenerationKeys.SELECT);
						if(plan.isSuccessful()) { 
							row.setParameter("RangeName", plan.getData("RangeName"));
							row.setParameter("HomeName", plan.getData("HomeName"));
							row.setParameter("HomeProductID", plan.getData("ProductID"));
						}
					}
				}
			} finally { 
				pp.close();
			}
			*/
			
			

			product = row;
		} catch (Exception e){}
	}
	
	
	
	public GenRow getProduct() {
		return product;
	}
	
	public String getProductID() {
		return productID;
	}
	
	public String getProductType() {
		return productType;
	}

	public Vector<ProductNode> getChildrenNodes() {
		return childrenNodes;
	}

	public void setChildrenNodes(Vector<ProductNode> childrenNodes) {
		this.childrenNodes = childrenNodes;
	}
	
	public int getChildrenNodesCount() {
		return childrenNodes.size();
	}
	
	public ProductNode getChildNode(int index) {
		return childrenNodes.get(index);
	}

	public ProductNode getParentNode() {
		return parentNode;
	}

	public void setParentNode(ProductNode parentNode) {
		this.parentNode = parentNode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public boolean isOptionEnabled() {
		return isOptionEnabled;
	}

	public void setOptionEnabled(boolean isOptionEnabled) {
		this.isOptionEnabled = isOptionEnabled;
	}
	
	public boolean isLastOption() {
		ProductNode node = this;
		
		int place = 0;
		
		while (node.getParentNode() != null) {
			node = node.getParentNode();
			--place;
		}
		
		ProductNode theNode = searchNodes(node);
		
		if (theNode == null || this == theNode) return true;
		
		ProductNode pNode = theNode;
		
		while (pNode.getParentNode() != null) {
			pNode = pNode.getParentNode();
			++place;
		}
		
		if (place <= 0) {
			this.setOptionEnabled(true);
			theNode.setOptionEnabled(false);
			return true;  
		} else {
			this.setOptionEnabled(false);
			theNode.setOptionEnabled(true);
		}
		return false;
	}
	
	protected ProductNode searchNodes(ProductNode nodes) {
		
		for (int n = 0; n < nodes.getChildrenNodesCount(); ++n) {
			ProductNode node = nodes.getChildNode(n);
			if (this.getProductID().equals(node.getProductID()) && this != node) return node;
			if (node.getChildrenNodesCount() > 0) {
				ProductNode theNode = searchNodes(node);
				if (theNode != null && this.getProductID().equals(theNode.getProductID()) && this != theNode) return theNode;
			}
		}
		
		return null;
	}

	public String getSettlementDate() {
		return estSettlementDate;
	}

	public void setSettlementDate(String estSettlementDate) {
		this.estSettlementDate = estSettlementDate;
	}

	public String getProductProductID() {
		return productProductID;
	}

	/**
	 * @return the productSubType
	 */
	public String getProductSubType() {
		return productSubType;
	}

	/**
	 * @param productSubType the productSubType to set
	 */
	public void setProductSubType(String productSubType) {
		this.productSubType = productSubType;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	
}
