package com.sok.runway.crm.interfaces;

import com.sok.runway.crm.*;
import com.sok.runway.security.*;

import java.util.*;
import java.sql.*;

public interface DocumentLinkable extends Linkable {
   
   public Collection<String> getDocumentIDs();   
   
   public Collection<String> getDocumentIDs(Map<String,String> filterParameters);      
   
   public Collection<String> getLinkableDocumentIDs();
   
   public Collection<String> getLinkableDocumentIDs(Map<String,String> filterParameters);
   
   public LinkableDocument getLinkableDocument();
   
   public void linkDocument(String documentID);
}