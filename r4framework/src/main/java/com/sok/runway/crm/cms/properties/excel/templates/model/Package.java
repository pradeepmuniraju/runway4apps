package com.sok.runway.crm.cms.properties.excel.templates.model;

public class Package {
	private String estateID;
	private String estateName;
	private String stageName;
	private String suburb;
	private String lotNumber;
	private String landSize;
	private String size;
	private String facade;
	private String houseName;
	private int beds;
	private int baths;
	private int garage;
	private String sqs;
	
	private double housePrice;	
	private double landPrice;
	private double facadePrice;
	private double totalPrice;
	
	private double siteCosts;
	private double bushfire;
	private double guidelines;
	
	private String status;
	
	private String regionName;
	private String locationName;
	private String groupName = "";
	private String salesRepName = "";
	private String salesAgentName = "";	
	private String exclusiveDate = "";
	
	public String getEstateID() {
		return estateID;
	}
	public void setEstateID(String estateID) {
		this.estateID = estateID;
	}
	public String getEstateName() {
		return estateName;
	}
	public void setEstateName(String estateName) {
		this.estateName = estateName;
	}
	public String getSuburb() {
		return suburb;
	}
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	public String getLotNumber() {
		return lotNumber;
	}
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public String getLandSize() {
		return landSize;
	}
	public void setLandSize(String landSize) {
		this.landSize = landSize;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getFacade() {
		return facade;
	}
	public void setFacade(String facade) {
		this.facade = facade;
	}
	public String getHouseName() {
		return houseName;
	}
	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}
	public int getBeds() {
		return beds;
	}
	public void setBeds(int beds) {
		this.beds = beds;
	}
	public int getBaths() {
		return baths;
	}
	public void setBaths(int baths) {
		this.baths = baths;
	}
	public int getGarage() {
		return garage;
	}
	public void setGarage(int garage) {
		this.garage = garage;
	}
	public String getSqs() {
		return sqs;
	}
	public void setSqs(String sqs) {
		this.sqs = sqs;
	}
	public double getHousePrice() {
		return housePrice;
	}
	public void setHousePrice(double housePrice) {
		this.housePrice = housePrice;
	}
	public double getLandPrice() {
		return landPrice;
	}
	public void setLandPrice(double landPrice) {
		this.landPrice = landPrice;
	}
	public double getFacadePrice() {
		return facadePrice;
	}
	public void setFacadePrice(double facadePrice) {
		this.facadePrice = facadePrice;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public double getSiteCosts() {
		return siteCosts;
	}
	public void setSiteCosts(double siteCosts) {
		this.siteCosts = siteCosts;
	}
	public double getBushfire() {
		return bushfire;
	}
	public void setBushfire(double bushfire) {
		this.bushfire = bushfire;
	}
	public double getGuidelines() {
		return guidelines;
	}
	public void setGuidelines(double guidelines) {
		this.guidelines = guidelines;
	}
	public String getSalesRepName() {
		return salesRepName;
	}
	public void setSalesRepName(String salesRepName) {
		this.salesRepName = salesRepName;
	}
	public String getSalesAgentName() {
		return salesAgentName;
	}
	public void setSalesAgentName(String salesAgentName) {
		this.salesAgentName = salesAgentName;
	}
	public String getExclusiveDate() {
		return exclusiveDate;
	}
	public void setExclusiveDate(String exclusiveDate) {
		this.exclusiveDate = exclusiveDate;
	}
	public String getStageName() {
		return stageName;
	}
	public void setStageName(String stageName) {
		this.stageName = stageName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
}