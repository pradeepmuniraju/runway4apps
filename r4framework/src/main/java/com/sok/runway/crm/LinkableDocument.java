package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;


public abstract class LinkableDocument extends AuditEntity {
   
   private Document document = null;
   
   public LinkableDocument(Connection con, String viewSpec) {
      super(con, viewSpec);
   }
   
   public LinkableDocument(ServletRequest request, String viewSpec) {
      super(request, viewSpec);  
   }
   
   public String getDocumentID() {
      return getField("DocumentID");
   }
   
   public void setDocumentID(String documentID) {
      setField("DocumentID", documentID);
   }
   
   public Document getDocument() {
      if (document == null) {
         document = new Document(getConnection());
         document.setCurrentUser(getCurrentUser());
      }
      if (!document.isLoaded() && getDocumentID().length() != 0) {
         document.load(getDocumentID());
      }
      return document;
   }
   
   public void postLoad() {
      super.postLoad();
      if (isLoaded()) {
         getDocument();
      }
   }
   
   public void clear() {
      super.clear();
      if (document != null) {
         document.clear();
      }
   }
}