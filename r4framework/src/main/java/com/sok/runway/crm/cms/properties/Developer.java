package com.sok.runway.crm.cms.properties;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

public class Developer extends PropertyEntity {
	final PropertyType propertyType = PropertyType.Developer;
	static final String VIEWSPEC = "properties/ProductPlanView";
	private Home home = null;

	public Developer(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE, getProductType());
	}

	public Developer(HttpServletRequest request, String productID) {
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}

	public Developer(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}

	public Developer load(String productID) {
		return (Developer) super.load(productID);
	}

	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
}
