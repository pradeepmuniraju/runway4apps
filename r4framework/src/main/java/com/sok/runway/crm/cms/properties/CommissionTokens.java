/**
 * 
 */
package com.sok.runway.crm.cms.properties;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;

/**
 * @author Puggs
 *
 */
public class CommissionTokens extends HashMap<String, Double> {
	
	private HashMap<String, String> StringMap =  new HashMap<String, String>();
	private static final Logger logger = LoggerFactory.getLogger(CommissionTokens.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String[] types = {"payments","transact","earn","pay"};
	private final String[] headings = {"PaymentIncome","PaymentOutgoing","CommissionIncome","CommissionOutgoing"};
	

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private CommissionTokens() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param initialCapacity
	 */
	@SuppressWarnings("unused")
	private CommissionTokens(int initialCapacity) {
		super(initialCapacity);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param m
	 */
	@SuppressWarnings("unused")
	private CommissionTokens(Map m) {
		super(m);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param initialCapacity
	 * @param loadFactor
	 */
	@SuppressWarnings("unused")
	private CommissionTokens(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
		// TODO Auto-generated constructor stub
	}
	
	public CommissionTokens(String productID, String opportunityID, String paymentCommissionID) {
		super();
		
		load(productID, opportunityID, paymentCommissionID);
	}
	
	public void load(String productID, String opportunityID, String paymentCommissionID) {
		GenRow comms = new GenRow();
		GenRow payments = new GenRow();
		GenRow transactions = new GenRow();

		//double totalAmount = 0, totalPaid = 0;
		//double txntotalPaid = 0;
		
		String key = "";
		
		for (int s = 0; s < types.length; ++s) {
			comms.clear();
			comms.setViewSpec("CommissionView");
			comms.setParameter("OpportunityID", opportunityID);
			comms.setParameter("ProductID", productID);
			comms.setParameter("CommissionType", types[s]);
			if (comms.getString("OpportunityID").length() > 0 && comms.getString("ProductID").length() > 0) {
				comms.doAction("search");
				comms.getResults();
				logger.debug("CommissionView Query {}", comms.getStatement() );
			}
			
			double amount = 0, paid = 0;
			double txnPaid = 0;
			
			while (comms.getNext()) {
				payments.clear();
				payments.setViewSpec("CommissionPaymentsView");
				payments.setConnection(comms.getConnection());
				payments.setParameter("CommissionID", comms.getString("CommissionID"));
				payments.setParameter("Amount", "!0");
				payments.sortBy("SortOrder", 0);
				payments.doAction("search");
				
				payments.getResults();
				
				logger.debug("CommissionPaymentsView Query {}", payments.getStatement() );
				
				while (payments.getNext()) {
					amount += payments.getDouble("Amount");
					paid += payments.getDouble("PaidAmount");
					//totalAmount += payments.getDouble("Amount");
					//totalPaid += payments.getDouble("PaidAmount");
					
					if(payments.getString("CommissionPaymentID").equals(paymentCommissionID)) {
						super.put("This.Amount", Double.valueOf(amount));
						super.put("This.PaidAmount", Double.valueOf(paid));
					}
					
					if (payments.isSet("Description")) {
						double valDue = payments.getDouble("Amount");
						double valPaid =  payments.getDouble("PaidAmount");
						
						key = "Est." + headings[s] + "." + payments.getString("Description").replaceAll(" ", "") + ".Due";
						if (super.containsKey(key)) valDue += super.get(key);
						super.put(key, Double.valueOf(valDue));
						
						logger.trace("Adding key {}, value {}", key , Double.valueOf(valDue) );
						
						key = "Est." + headings[s] + "." + payments.getString("Description").replaceAll(" ", "") + ".Paid";
						if (super.containsKey(key)) valPaid += super.get(key);
						super.put(key, Double.valueOf(valPaid));
						
						logger.trace("Adding key {}, value {}", key , Double.valueOf(valPaid) );
					}
					
					if (payments.isSet("Token")) {
						double valDue = payments.getDouble("Amount");
						double valPaid =  payments.getDouble("PaidAmount");
						
						key = "Est." + headings[s] + "." + payments.getString("Token").replaceAll(" ", "") + ".Due";
						if (super.containsKey(key)) valDue += super.get(key);
						super.put(key, Double.valueOf(valDue));
						
						logger.trace("Adding key {}, value {}", key , Double.valueOf(valDue) );
						
						key = "Est." + headings[s] + "." + payments.getString("Token").replaceAll(" ", "") + ".Paid";
						if (super.containsKey(key)) valPaid += super.get(key);
						super.put(key, Double.valueOf(valPaid));
						
						logger.trace("Adding key {}, value {}", key , Double.valueOf(valPaid) );
					}
					
					if (payments.isSet("PaidMethod")) {
						String method = payments.getString("PaidMethod");
						
						key = "Est." + headings[s] + "." + payments.getString("Token").replaceAll(" ", "") + ".PaidMethod";
						StringMap.put(key, method);
						logger.trace("Adding key {}, value {}", key , method );
					}
					
					if (payments.isSet("PercentageOfPayment")) {
						String method = payments.getString("PercentageOfPayment");
						
						key = "Est." + headings[s] + "." + payments.getString("Token").replaceAll(" ", "") + ".PercentageOfPayment";
						StringMap.put(key, method);
						logger.trace("Adding key {}, value {}", key , method );
					}
					
					if (payments.isSet("Amount")) {
						String method = payments.getData("Amount");
						
						key = "Est." + headings[s] + "." + payments.getString("Token").replaceAll(" ", "") + ".FlatRate";
						StringMap.put(key, method);
						logger.trace("Adding key {}, value {}", key , method );
					}
				}
				
				key = "Est." + headings[s] + ".SubTotal.Due";
				super.put(key, Double.valueOf(amount));
				logger.trace("Adding key {}, value {}", key , Double.valueOf(amount) );
				
				key = "Est." + headings[s] + ".SubTotal.Paid";
				super.put(key, Double.valueOf(paid));
				logger.trace("Adding key {}, value {}", key , Double.valueOf(paid) );
				
				
				// From transactions - Actual
				transactions.clear();
				transactions.setViewSpec("TransactionsView");
				transactions.setConnection(comms.getConnection());
				transactions.setParameter("CommissionID", comms.getString("CommissionID"));
				transactions.doAction("search");					
				transactions.getResults();					
				logger.debug("TransactionsView Query {}", transactions.getStatement() );
				
				while (transactions.getNext()) {
					txnPaid += transactions.getDouble("PaidAmount");
					//txntotalPaid += transactions.getDouble("PaidAmount");
					
					if (transactions.isSet("Description")) {							
						double valPaid =  transactions.getDouble("PaidAmount");
						key = headings[s] + "." + transactions.getString("Description").replaceAll(" ", "") + ".Paid";
						if (super.containsKey(key)) valPaid += super.get(key);
						super.put(key, Double.valueOf(valPaid));
						
						logger.trace("Adding key {}, value {}", key , Double.valueOf(valPaid) );
						logger.debug("Adding key {}, value {}", key , Double.valueOf(valPaid) );
					}
					
					if (transactions.isSet("Token")) {
						double valPaid =  transactions.getDouble("PaidAmount");
						key = headings[s] + "." + transactions.getString("Token").replaceAll(" ", "") + ".Paid";
						if (super.containsKey(key)) valPaid += super.get(key);
						super.put(key, Double.valueOf(valPaid));
						
						logger.trace("Adding key {}, value {}", key , Double.valueOf(valPaid) );
						logger.debug("Adding key {}, value {}", key , Double.valueOf(valPaid) );
					}
					
					if (transactions.isSet("PaidMethod")) {
						String method = transactions.getString("PaidMethod");
						
						key = headings[s] + "." + transactions.getString("Token").replaceAll(" ", "") + ".PaidMethod";
						StringMap.put(key, method);
						logger.trace("Adding key {}, value {}", key , method );
						logger.debug("Adding key {}, value {}", key , method );
					}
					
				}
				
				key = headings[s] + ".SubTotal.Paid";
				super.put(key, Double.valueOf(txnPaid));
				logger.trace("Adding key {}, value {}", key , Double.valueOf(txnPaid) );
				logger.debug("Adding key {}, value {}", key , Double.valueOf(txnPaid) );
			}
			
		}
		comms.close();
		payments.close();
		transactions.close();
		
	}
	
	public String getFromStringMap(String key) {
		return StringMap.get(key);
	}
}