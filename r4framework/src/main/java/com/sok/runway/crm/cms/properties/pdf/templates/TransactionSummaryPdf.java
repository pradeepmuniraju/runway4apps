package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.TransactionInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;

public class TransactionSummaryPdf extends AbstractPDFTemplate implements PDFConstants {

	public static final Logger logger = LoggerFactory.getLogger(LandPropertyListPdf.class);

	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {
		throw new UnsupportedOperationException("Must use writer method if one is defined");
	}

	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document, final PdfWriter writer) throws IOException, DocumentException {

		try {
			renderPDFConcurrent(request, response, document, writer);
			return;
		} catch (ExecutionException ee) {
			logger.error("Execution Exception", ee);
		} catch (InterruptedException ie) {
			logger.error("Execution Interrupted", ie);
		}
	}

	public void renderPDFConcurrent(final HttpServletRequest request, HttpServletResponse response, Document document, final PdfWriter writer) throws IOException, DocumentException, ExecutionException, InterruptedException {

		String trxnID = request.getParameter("TransactionID"); // Parent Transaction ID

		GenRow parentTransactionRow = new GenRow();
		parentTransactionRow.setViewSpec("TransactionsView");
		parentTransactionRow.setParameter("TransactionID", trxnID);
		parentTransactionRow.doAction("selectfirst");
		logger.debug("Parent TransactionsView Query is: " + parentTransactionRow.getStatement());

		TransactionInfo parentInfo = new TransactionInfo();
		
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
			String dateAsString = simpleDateFormat.format(parentTransactionRow.getDate("CreatedDate"));
			parentInfo.setDate(dateAsString);
		} catch (Exception ee) {
			ee.printStackTrace();
			parentInfo.setDate("");
		}
		
		try {
			DateFormat format = DateFormat.getTimeInstance(DateFormat.SHORT);
			parentInfo.setTime(format.format(parentTransactionRow.getDate("CreatedDate"))); // TODO: Please check the parsing logic for 10:45am format
		} catch (Exception ee) {
			ee.printStackTrace();
			parentInfo.setTime("");
		}

		parentInfo.setProcessedBy("Michael Wilson");
		parentInfo.setType("Account Sales commission");
		parentInfo.setTrustTransactionRef(parentTransactionRow.getString("TransactionRef"));
		// parentInfo.setLotNo("1422");
		// parentInfo.setProcessedAmount(parentTransactionRow.getDouble("PaidAmount"));
		// parentInfo.setBalanceAmount(parentTransactionRow.getDouble("BalanceAmount"));
		parentInfo.setReceiptNo(parentTransactionRow.getString("ReceiptNumber"));
		parentInfo.setTotalProcessedAmount("$21,450.00");
		parentInfo.setTotalBalanceAmount("$0.00");

		parentInfo.setProcessedBy(parentTransactionRow.getString("CreatedFirstName") + " " + parentTransactionRow.getString("CreatedLastName"));
		parentInfo.setType(getTransactionType(parentTransactionRow.getString("TransactionType")));
		parentInfo.setTrustTransactionRef(parentTransactionRow.getString("TransactionRef"));
		// parentInfo.setLotNo(parentTransactionRow.getString("ProductName"));
		// parentInfo.setProcessedAmount(parentTransactionRow.getDouble("PaidAmount"));
		// parentInfo.setBalanceAmount(parentTransactionRow.getDouble("BalanceAmount"));
		parentInfo.setReceiptNo(parentTransactionRow.getString("ReceiptNumber"));
		parentInfo.setTotalProcessedAmount(parentTransactionRow.getData("PaidAmount"));
		parentInfo.setTotalBalanceAmount("$0.00");

		document.open();
		setMargins(document);
		renderTransactionSummaryPDF(document, parentInfo, trxnID);
	}

	private String getTransactionType(String txnType) {
		logger.trace("Translating transaction type for {}", txnType);

		if ("Full".equals(txnType)) {
			return "Full Deposit Transfer";
		} else if ("Balance".equals(txnType)) {
			return "Account Sales";
		} else if ("Nil".equals(txnType)) {
			return "Debit Sales";
		}
		return "";
	}

	private void renderTransactionSummaryPDF(com.itextpdf.text.Document document, TransactionInfo parentInfo, String trxnID) throws DocumentException {

		PdfPTable pageTable = new PdfPTable(1);
		pageTable.setWidthPercentage(80f);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPaddingTop(120f);
		pageTable.getDefaultCell().setPaddingBottom(20f);
		pageTable.getDefaultCell().setFixedHeight(0f);
		pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable tmpTbl = new PdfPTable(2);
		tmpTbl.getDefaultCell().setBorder(0);
		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setFixedHeight(0f);

		tmpTbl.getDefaultCell().setColspan(2);

		tmpTbl.getDefaultCell().setPaddingBottom(10f);
		tmpTbl.addCell(new Paragraph("Transaction Summary", PDFUtil.fontArialBlackPt28Bold));
		tmpTbl.getDefaultCell().setPaddingBottom(10f);

		tmpTbl.addCell("Date: " + parentInfo.getDate());		
		tmpTbl.addCell("Time: " + parentInfo.getTime());		
		tmpTbl.addCell("Processed by: " + parentInfo.getProcessedBy());
		tmpTbl.addCell("Type: " + parentInfo.getType());

		tmpTbl.getDefaultCell().setPaddingTop(30f);
		tmpTbl.getDefaultCell().setPaddingBottom(20f);

		Paragraph tmpPara = new Paragraph();
		tmpPara.add(new Chunk("Trust transaction ref: "));
		tmpPara.add(new Chunk(parentInfo.getTrustTransactionRef(), PDFUtil.fontArialBlackPt18Bold));
		tmpTbl.addCell(tmpPara);

		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setPaddingBottom(0f);

		pageTable.addCell(tmpTbl);

		GenRow childRow = new GenRow();
		childRow.setViewSpec("TransactionsView");
		childRow.setParameter("ParentTransactionID", trxnID);
		childRow.doAction("search");
		childRow.getResults(true);
		logger.debug("List TransactionsView Query is: " + childRow.getStatement());

		// TODO: Add logic here to populate the Transactions list.
		ArrayList<TransactionInfo> trnInfoList = new ArrayList<TransactionInfo>();
		while (childRow.getNext()) {

			TransactionInfo tmpInfo = new TransactionInfo();
			// tmpInfo.setProcessedBy("Michael Wilson");
			// tmpInfo.setType("Account Sales commission");
			tmpInfo.setTrustTransactionRef(childRow.getString("TransactionRef"));
			tmpInfo.setLotNo(childRow.getString("ProductName"));
			tmpInfo.setProcessedAmount(childRow.getDouble("PaidAmount"));
			//tmpInfo.setBalanceAmount(childRow.getDouble("BalanceAmount"));
			// tmpInfo.setReceiptNo(childRow.getString("ReceiptNumber"));
			tmpInfo.setTotalProcessedAmount(childRow.getData("PaidAmount"));
			tmpInfo.setTotalBalanceAmount("$0.00");

			trnInfoList.add(tmpInfo);
		}
		childRow.close();

		pageTable.getDefaultCell().setPaddingTop(0f);

		pageTable.addCell(renderTransactions(document, trnInfoList));

		document.add(pageTable);
	}

	private PdfPTable renderTransactions(com.itextpdf.text.Document document, ArrayList<TransactionInfo> trnInfoList) throws DocumentException {

		PdfPTable tmpTbl = new PdfPTable(4);
		tmpTbl.getDefaultCell().setBorder(0);
		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setFixedHeight(0f);
		// tmpTbl.setWidthPercentage(100f);

		double totalProcessedAmount = 0.0;
		double totalBalanceAmount = 0.0;

		if (trnInfoList != null && !trnInfoList.isEmpty()) {

			for (int i = 0; i < trnInfoList.size(); i++) {

				if (i == 0) {
					// Populate the Header as well
					tmpTbl.getDefaultCell().setPaddingTop(10f);
					tmpTbl.getDefaultCell().setPaddingBottom(10f);
					tmpTbl.getDefaultCell().setBorderWidthBottom(0.2f);
					tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
					tmpTbl.addCell(new Paragraph("Lot", PDFUtil.fontArialBlackPt18Bold));
					tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
					tmpTbl.addCell(new Paragraph("Processed", PDFUtil.fontArialBlackPt18Bold));
					tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
					tmpTbl.addCell(new Paragraph("Balance", PDFUtil.fontArialBlackPt18Bold));
					tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
					tmpTbl.addCell(new Paragraph("Receipt No.", PDFUtil.fontArialBlackPt18Bold));
					tmpTbl.getDefaultCell().setBorderWidthBottom(0f);
					tmpTbl.getDefaultCell().setPaddingBottom(0f);
				}

				TransactionInfo trnInfo = trnInfoList.get(i);

				// This is to push down the Totals Row
				if (i == (trnInfoList.size() - 1)) {
					tmpTbl.getDefaultCell().setPaddingBottom(50f);
				}
				tmpTbl.getDefaultCell().setPaddingTop(5f);
				tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				tmpTbl.addCell(trnInfo.getLotNo());
				tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				tmpTbl.addCell(PDFUtil.getString(trnInfo.getProcessedAmount()));

				tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				tmpTbl.addCell(PDFUtil.getString(trnInfo.getBalanceAmount()));
				tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
				tmpTbl.addCell(trnInfo.getReceiptNo());

				totalProcessedAmount += trnInfo.getProcessedAmount();
				totalBalanceAmount += trnInfo.getBalanceAmount();
			}

			// Populate the Header as well
			tmpTbl.getDefaultCell().setPaddingTop(10f);
			tmpTbl.getDefaultCell().setPaddingBottom(10f);
			tmpTbl.getDefaultCell().setBorderWidthTop(0.2f);
			tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			tmpTbl.addCell(new Paragraph("Totals", PDFUtil.fontArialBlackPt18Bold));
			tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			tmpTbl.addCell(new Paragraph(PDFUtil.getString(totalProcessedAmount), PDFUtil.fontArialBlackPt18Bold));
			tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			tmpTbl.addCell(new Paragraph(PDFUtil.getString(totalBalanceAmount), PDFUtil.fontArialBlackPt18Bold));
			tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			tmpTbl.addCell(new Paragraph("", PDFUtil.fontArialBlackPt18Bold));
			tmpTbl.getDefaultCell().setBorderWidthBottom(0f);
			tmpTbl.getDefaultCell().setPaddingBottom(0f);

		}

		return tmpTbl;
	}

	public void setMargins(Document document) {
		document.setMargins(20, 0, 20, 0);
	}

	public String getPdfSimpleName() {
		return "Transaction Summary";
	}

}