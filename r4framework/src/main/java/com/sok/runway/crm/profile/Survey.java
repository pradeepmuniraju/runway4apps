/* ========================================================================= **
 o FILE TYPE        : Java Source File - text/java
 o FILE NAME        : Survey.java
 o DESIGNER(S)      : 
 o CODER(S)         : Irwan Kodradjaja
 o VERSION          : 1.0
 o LAST MODIFIED BY : 
 o LAST MODIFIED ON : 
** ========================================================================= */

/* ======================= ** PACKAGE DECLARATION ** ======================= */
package com.sok.runway.crm.profile;
/* ========================================================================= */

/* ======================== ** IMPORT STATEMENTS ** ======================== */
import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.security.*;
import javax.servlet.ServletRequest;
import java.sql.Connection;
import java.util.*;
/* ========================================================================= */

/* ========================= ** CLASS COMMENTS ** ========================== */
/**
 * A class to represent Runway Surveys/Profiles
 * 
 * @author Irwan
 * @version 0.1
 */
/* ========================================================================= */

/* ====================== ** CLASS DEFINITION ** =========================== */
public class Survey extends SecuredEntity {

   private HashSet mandatoryQuestions = null;
   private boolean cleared = true;
   
	public Survey(Connection con) {
		super(con, "SurveyView");
	}
	
	public Survey(ServletRequest request) {
		super(request, "SurveyView");
		populateFromRequest(request.getParameterMap());
	}
   
	public Survey(Connection con, String surveyID) {
		this(con);
	   load(surveyID);
	}

   public boolean isMandatory(String questionID) {
      if (!isLoaded()) {
         return false;
      }
      else {
         if (mandatoryQuestions == null) {
            mandatoryQuestions = new HashSet();
         }
         if (cleared) {
            GenRow surveyQuestions = new GenRow();
            surveyQuestions.setViewSpec("SurveyQuestionView");
            surveyQuestions.setConnection(getConnection());
            surveyQuestions.setParameter("SurveyID", getPrimaryKey());
            surveyQuestions.doAction(ActionBean.SEARCH);
            surveyQuestions.getResults();
            while (surveyQuestions.getNext()) {
               if (surveyQuestions.getData("Mandatory").startsWith("Y")) {
                  mandatoryQuestions.add(surveyQuestions.getData("QuestionID"));
               }
            }
         }
         return mandatoryQuestions.contains(questionID);
      }
   }
	

   public void clear() {
      super.clear();
      if (mandatoryQuestions != null) {
         mandatoryQuestions.clear();
      }
   }
        
	public Validator getValidator() {
		
		Validator val = Validator.getValidator("Survey");
		
	   if (val == null) {
		   val = new Validator();
		   val.addMandatoryField("SurveyID","SYSTEM ERROR - SurveyID");
		   val.addMandatoryField("GroupID","Survey must have a security group selected");
		   val.addMandatoryField("Name","Question must mave a label");
		   Validator.addValidator("Survey", val);
	   }
	   
	   return val;
	}
	
	/**
	 * Retrieves the QuestionIDs related to a given Survey object.
	 * @return A Collection object containing QuestionID Strings
	 */
	public Collection<String> getQuestionIDs() {
		
		GenRow 				questionBean 	= new GenRow();
		ArrayList<String> questionIDs		= new ArrayList<String>();
		
		questionBean.setViewSpec("SurveyQuestionView");
		questionBean.setParameter("SurveyID", this.getPrimaryKey());
		questionBean.sortBy("SortOrder",0);
		questionBean.doAction(ActionBean.SEARCH);
		questionBean.getResults();
		
		while(questionBean.getNext()) {
			questionIDs.add(questionBean.getData("QuestionID"));
		}
		
		return questionIDs;
		
	}
}