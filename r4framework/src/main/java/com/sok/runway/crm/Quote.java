package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.security.AuditEntity; 
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;

import javax.servlet.ServletRequest;
import java.sql.*;

public class Quote extends AbstractOrder {

   public static final String ENTITY_NAME = "Quote";
   
	public Quote(Connection con) {
		super(con, "OrderView");
	}
	
	public Quote(ServletRequest request) {
		 super(request, "OrderView");
		 populateFromRequest(request.getParameterMap());
	}
	
	public Quote(Connection con, String orderID) {
		this(con);
		load(orderID);
	}

   public String getEntityName() {
	   return ENTITY_NAME;
   }
			
	public Validator getValidator() {
		Validator val = Validator.getValidator("Quote");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("OrderID","SYSTEM ERROR - OrderID");
			val.addMandatoryField("GroupID","Quote must have a security group selected");
			Validator.addValidator("Quote", val);
		}
		return val;
	}
}