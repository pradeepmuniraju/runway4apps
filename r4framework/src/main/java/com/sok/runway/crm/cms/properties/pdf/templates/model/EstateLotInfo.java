package com.sok.runway.crm.cms.properties.pdf.templates.model;

import java.util.ArrayList;

public class EstateLotInfo 
{
		public String currentDate;
		
		public String estateProductID;
		
		public String estateSellingAgentSalesRepName;  // ??????
		public String estateSellingAgentSalesRepPhone;  // ??????
		public String estateName;
		public String estateCity;
		public String estateMelRef;
		
		public String estateDeveloper;
		public String estateAgent;
		
		public String estateLocationName; // ????
		public String estateLocationContactPersonName; // ????
		
		public String estateNotes; // ????
		
		public ArrayList<EstateLotHouseAndLandInfo> estateLotHLInfoList; 	

		
		public ArrayList<EstateLotHouseAndLandInfo> getEstateLotHLInfoList() {
			return estateLotHLInfoList;
		}

		public void setEstateLotHLInfoList(
				ArrayList<EstateLotHouseAndLandInfo> estateLotHLInfoList) {
			this.estateLotHLInfoList = estateLotHLInfoList;
		}

		public String getEstateSellingAgentSalesRepName() {
			return estateSellingAgentSalesRepName;
		}

		public void setEstateSellingAgentSalesRepName(
				String estateSellingAgentSalesRepName) {
			this.estateSellingAgentSalesRepName = estateSellingAgentSalesRepName;
		}

		public String getEstateSellingAgentSalesRepPhone() {
			return estateSellingAgentSalesRepPhone;
		}

		public void setEstateSellingAgentSalesRepPhone(
				String estateSellingAgentSalesRepPhone) {
			this.estateSellingAgentSalesRepPhone = estateSellingAgentSalesRepPhone;
		}

		public String getCurrentDate() {
			return currentDate;
		}

		public void setCurrentDate(String currentDate) {
			this.currentDate = currentDate;
		}

		public String getEstateProductID() {
			return estateProductID;
		}

		public void setEstateProductID(String estateProductID) {
			this.estateProductID = estateProductID;
		}

		public String getEstateName() {
			return estateName;
		}

		public void setEstateName(String estateName) {
			this.estateName = estateName;
		}

		public String getEstateCity() {
			return estateCity;
		}

		public void setEstateCity(String estateCity) {
			this.estateCity = estateCity;
		}

		public String getEstateMelRef() {
			return estateMelRef;
		}

		public void setEstateMelRef(String estateMelRef) {
			this.estateMelRef = estateMelRef;
		}

		public String getEstateDeveloper() {
			return estateDeveloper;
		}

		public void setEstateDeveloper(String estateDeveloper) {
			this.estateDeveloper = estateDeveloper;
		}

		public String getEstateAgent() {
			return estateAgent;
		}

		public void setEstateAgent(String estateAgent) {
			this.estateAgent = estateAgent;
		}

		public String getEstateNotes() {
			return estateNotes;
		}

		public void setEstateNotes(String estateNotes) {
			this.estateNotes = estateNotes;
		}
		
		public String getEstateLocationName() {
			return estateLocationName;
		}

		public void setEstateLocationName(String estateLocationName) {
			this.estateLocationName = estateLocationName;
		}

		public String getEstateLocationContactPersonName() {
			return estateLocationContactPersonName;
		}

		public void setEstateLocationContactPersonName(
				String estateLocationContactPersonName) {
			this.estateLocationContactPersonName = estateLocationContactPersonName;
		}
}