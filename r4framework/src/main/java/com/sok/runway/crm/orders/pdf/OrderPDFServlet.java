package com.sok.runway.crm.orders.pdf;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.cms.properties.pdf.AbstractJasperReport;

public class OrderPDFServlet extends HttpServlet {

	private static final Logger logger = LoggerFactory.getLogger(OrderPDFServlet.class);
	private static final long serialVersionUID = 1L;
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.setAttribute("servletConfig", getServletConfig());//This to work with Jasper
		generate("OrderPdf", request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.setAttribute("servletConfig", getServletConfig());//This to work with Jasper
		generate("OrderPdf", request, response);
	} 
	
	public static final String GENERIC_PACKAGE = "com.sok.runway.crm.orders.pdf.templates.";
	public static final String SPECIFIC_PACKAGE = "com.sok.clients.pdf.templates.";
	public static final String JASPER_PACKAGE = "com.sok.clients.pdf.jasper.report.";
	
	public ServletConfig servletConfig = getServletConfig(); 

	public static void generate(String classname, HttpServletRequest request, HttpServletResponse response) {

		try {
			AbstractOrderPDF template = null;
			AbstractJasperReport jasperRunner = null;

			try {
				jasperRunner = (AbstractJasperReport) (Class.forName(JASPER_PACKAGE + classname).newInstance());
				jasperRunner.generateReport(request, response);
			} catch (ClassNotFoundException c1) {
				try {
					logger.info("Unable to load specific jasper pdf template. Using default " + SPECIFIC_PACKAGE + classname);
					template = (AbstractOrderPDF) (Class.forName(SPECIFIC_PACKAGE + classname).newInstance());
				}
				catch(ClassNotFoundException c2)
				{
					logger.info("Unable to load specific pdf template. Using default " + GENERIC_PACKAGE + classname);
					template = (AbstractOrderPDF) (Class.forName(GENERIC_PACKAGE + classname).newInstance());
				}
				template.processRequest(request, response);
			}

		

		} catch (Exception e) {
			logger.error("Error generating pdf template " + classname, e);
		}
	}
}
