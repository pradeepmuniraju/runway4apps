package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.ProductLotInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.model.Stage;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.MultiLandListByEstateHeader;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;
import com.sok.service.crm.UserService;
import com.sok.service.exception.NotFoundException;

public class MultiLandListPdfByEstate extends AbstractPDFTemplate implements PDFConstants {

		
		private static final Logger logger = LoggerFactory.getLogger(MultiLandListPdfByEstate.class);
		
	    public static float dataCols [] = new float[] { 16f, 16f, 16f, 16f, 16f, 20f}; // 6 columns
		
		public MultiLandListPdfByEstate() {
			logger.debug("Inside Dacland Runway Land List PDF template ");
		}

		@Override
		public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document)
				throws IOException, DocumentException {
			String estateID = request.getParameter("ProductID");
			
			if(StringUtils.isNotBlank(estateID)) {
				String estateLogo = getEstateLogo(estateID, request);		
				if(StringUtils.isNotBlank(estateLogo)) {
					//TODO something to print on pdf
				}
			
				GenRow activeStages = getActiveStages(estateID);
				
				TreeMap<String, Stage> stages = new TreeMap<String, Stage>();
				while(activeStages.getNext()) {
					Stage stage = getLotByStage(estateID,activeStages.getData("ProductID"), request, getPdfSimpleName());
					if(stage != null) {
						stages.put(stage.getStageName(), stage);
					}
				}
				if(stages.isEmpty())
					throw new NotFoundException("No active lots found for estate " + activeStages.getData("ProductID"));
				
				document.open();
				
				
				if(!stages.isEmpty()) {
					Stage tmpStage = stages.firstEntry().getValue();
					MultiLandListByEstateHeader.disclaimerText = tmpStage.getDisclaimer();
					MultiLandListByEstateHeader.dripImage = tmpStage.getDripImage();
				}			
				renderPages(document, stages);
			}
		}
		
		@Override
		public void setMargins(Document document){
			document.setMargins(15, 15, 141, 193); // L R T B
		}

		
		/**
		 * Render PdfTable from ArrayList
		 * @param document
		 * @param building
		 * @param apartmentMap
		 * @throws DocumentException
		 */
		private void renderPages(Document document, Map<String, Stage> stages) throws DocumentException {		
			PdfPTable stageTable = new PdfPTable(1);
			PdfPTable pageBodyTable = new PdfPTable(1);
			PDFUtil.initMultiLandListbyEstateDefaultCell(pageBodyTable);
			pageBodyTable.setWidths(new float[] { 100f });
			int stageCount = 0;
			
			for (Map.Entry<String, Stage> entry : stages.entrySet()) {
				if(stageCount != 0) {
					document.newPage();
					pageBodyTable = new PdfPTable(1);
					PDFUtil.initMultiLandListbyEstateDefaultCell(pageBodyTable);
					pageBodyTable.setWidths(new float[] { 100f });
				}
				Stage stage = entry.getValue();
				stageTable = new PdfPTable(1);
				stageTable = renderUpperLevelTable(stage);
				pageBodyTable.addCell(stageTable);
				stageCount++;
				document.add(pageBodyTable); //Attach to the document
			}
		}

		
		/**
		 * Called by AbstractPDFGenerator to generate header/footer
		 */
		public PdfPageEventHelper getFooter(HttpServletRequest request) {
			return new MultiLandListByEstateHeader(request);
		}
		
		public String getPdfSimpleName() {
			return "Multi Land List PDF";
		}
		
		/**
		 * Render Apartment table
		 * @param building
		 * @param apartments
		 * @return
		 * @throws DocumentException
		 */
		private PdfPTable renderUpperLevelTable(Stage stage) throws DocumentException{
			PdfPTable apartmentTable = new PdfPTable(1);
			PDFUtil.initMultiLandListbyEstateDefaultCell(apartmentTable);			
			//apartmentTable.setWidths(new float[] { 6f, 94f});
			
			apartmentTable.getDefaultCell().setBorder(1);
			apartmentTable.getDefaultCell().setBorderWidth(0.1f);
			apartmentTable.getDefaultCell().setBorderWidthLeft(0.1f);
			apartmentTable.getDefaultCell().setBorderWidthRight(0.1f);
			apartmentTable.getDefaultCell().setBorderWidthTop(0.1f);
			apartmentTable.getDefaultCell().setBorderWidthBottom(0.1f);	
			apartmentTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			apartmentTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPTable apartmentRow = null;
			ArrayList<ProductLotInfo> lots = stage.getLots();
			
			ProductLotInfo lot = null;
			for(int i = 0; i < lots.size(); i++){
				lot = lots.get(i);
				/*if(i == 0){				
					apartmentTable.getDefaultCell().setRowspan(rowSpan);
					apartmentTable.addCell(new Paragraph(stage.getStageName(), PDFUtil.gothamBookPt14));
				}*/
				boolean borderFlag = false;
				if(i == lots.size()-1)
					borderFlag =  true;
				
				apartmentRow = renderLotRow(lot, borderFlag);
				apartmentTable.getDefaultCell().setRowspan(1);
				apartmentTable.addCell(apartmentRow);
			}
			return apartmentTable;		
		}
		
		/**
		 * Render header of apartment table and store it in PDFTable
		 * 
		 * @return
		 * @throws DocumentException
		 */
		public static PdfPTable renderTableHeader() throws DocumentException{
			PdfPTable apartmentTable = new PdfPTable(6);
			PDFUtil.initMultiLandListbyEstateDefaultCell(apartmentTable);
			apartmentTable.setWidths(dataCols);		
			apartmentTable.getDefaultCell().setBorder(1);
			apartmentTable.getDefaultCell().setBorderWidth(0.1f);
			apartmentTable.getDefaultCell().setBorderWidthLeft(0.1f);
			apartmentTable.getDefaultCell().setBorderWidthRight(0.1f);
			apartmentTable.getDefaultCell().setBorderWidthTop(0.1f);		
			apartmentTable.getDefaultCell().setBorderWidthBottom(0.1f);
			apartmentTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			apartmentTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			apartmentTable.getDefaultCell().setPadding(1f);
			apartmentTable.getDefaultCell().setPaddingBottom(2f);
			
			apartmentTable.addCell(new Paragraph(PDFUtil.STAGE, PDFUtil.gothamBookPt16Bold));
			apartmentTable.addCell(new Paragraph(PDFUtil.LOT_NUMBER, PDFUtil.gothamBookPt16Bold));
			apartmentTable.addCell(new Paragraph(PDFUtil.LOT_PRICE, PDFUtil.gothamBookPt16Bold));
			apartmentTable.addCell(new Paragraph(PDFUtil.LOT_SIZE, PDFUtil.gothamBookPt16Bold));
			apartmentTable.addCell(new Paragraph(PDFUtil.LOT_FRONTAGE, PDFUtil.gothamBookPt16Bold));
			apartmentTable.addCell(new Paragraph(PDFUtil.LOT_STATUS, PDFUtil.gothamBookPt16Bold));
			
			//apartmentTable.completeRow();
			return apartmentTable;
		}
		
		/**
		 * Render apartment row
		 * @param lot
		 * @return
		 * @throws DocumentException
		 */
		private PdfPTable renderLotRow(ProductLotInfo lot, boolean borderBottom) throws DocumentException{
			PdfPTable lotRow = new PdfPTable(6);
			lotRow.setWidths(dataCols);
			
			lotRow.getDefaultCell().setBorder(1);		
			lotRow.getDefaultCell().setBorderWidthLeft(0.1f);
			lotRow.getDefaultCell().setBorderWidthRight(0);
			lotRow.getDefaultCell().setBorderWidthBottom(0.1f);
			lotRow.getDefaultCell().setBorderWidthTop(0.1f);
		
			lotRow.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			lotRow.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			lotRow.addCell(new Paragraph(lot.getEstateStageName(), PDFUtil.gothamBookPt16)); //Stage name
			lotRow.addCell(new Paragraph(lot.getLotNumber(), PDFUtil.gothamBookPt16)); //Lot number/name
			
			lotRow.addCell(new Paragraph(lot.getLotPrice(), PDFUtil.gothamBookPt16)); //Lot Price
			lotRow.addCell(new Paragraph(lot.getLotSize(), PDFUtil.gothamBookPt16)); //Lot Size
			
			double frontage;
			if(lot.getLotWidth() > 0) {
				frontage = PDFUtil.formatArea(String.valueOf(lot.getLotWidth()), PDFUtil.DECIMAL_FORMAT);
				lotRow.addCell(new Paragraph(String.valueOf(frontage) + "m", PDFUtil.gothamBookPt16)); //Lot Frontage
			} else  {
				lotRow.addCell(new Paragraph(" ", PDFUtil.gothamBookPt16)); //Lot Frontage
			}
			lotRow.addCell(new Paragraph(lot.getLotStatus(), PDFUtil.gothamBookPt16)); //Lot Status
			
			lotRow.completeRow();
			return lotRow;
		}

		public String getDripCount() {
			return "2";
		}
		
		public Stage getLotByStage(String estateID, String stageID, HttpServletRequest request, String simpleName) throws NotFoundException{
			ProductLotInfo lot = null;
			GenRow genRow = new GenRow();
			
			Stage stage = null;
			
			try { 
				genRow.setRequest(request);
				genRow.setViewSpec("properties/ProductQuickIndexView");
				genRow.setParameter("StageID", stageID);
				genRow.setParameter("ProductType","Land");
				genRow.setParameter("Active", "Y");
				genRow.setParameter("-groupby1", "ProductID");//Need this group by state to remove duplicated records happening in db
				genRow.sortBy("Name",0);
				genRow.setAction(GenerationKeys.SEARCH);
				genRow.doAction();
				genRow.getResults();
				logger.debug("Stage ID: " + stageID);
				logger.debug("Land List Query : " + genRow.getStatement());
				
				//Populate result
				while (genRow.getNext()) {
					lot = getLot(genRow);
					if(lot != null) {
						if(!StringUtil.isBlankOrEmpty(lot.getEstateStageName())) {
							if(stage == null) {
								stage = new Stage();
								stage.setStageName(lot.getEstateStageName());
							} 
							stage.addLot(lot);
						}	
					}
				}
				
				if(stage != null) {
					ProductLotInfo tmp = (ProductLotInfo)stage.getLots().get(0);
					Map<String, String> dm = com.sok.service.crm.cms.properties.DripService.getInstance().getDripMapForTemplates(genRow.getConnection(), UserService.getInstance().getSimpleUser(genRow.getConnection(), "F180D2D08404F1241723606192C82747"), new String[] { estateID, stageID, tmp.getProductID() }, "PDF", simpleName);
					if(StringUtils.isNotBlank(dm.get("Text1"))) { 
						stage.setDisclaimer(dm.get("Text1"));
					}
					if(StringUtils.isNotBlank(dm.get("Image2"))) {
						stage.setDripImage(dm.get("Image2"));
					}
				}
				
			} finally {
				genRow.close();
			}
			return stage;
		}
		
		public GenRow getActiveStages(String estateID) {
			GenRow rows = new GenRow();
			rows.setViewSpec("ProductListView");
			rows.setParameter("ProductType","Stage");
			rows.setParameter("Active","Y");
			rows.setParameter("ProductParentEstateStage.ParentProductID", estateID);
			rows.sortBy("ABS(SUBSTRING(Products.Name,7))", 0);
			rows.sortBy("ABS(Products.Name)",1);
			rows.sortBy("Products.Name",2);
			rows.doAction("search");
			rows.getResults();
			return rows;
		}
		
		
		/**
		 * Helper function used to parse lot from Genrow.
		 * For now, only limited fields are populated
		 * @param genRow
		 * @return ProductLotInfo
		 */
		private ProductLotInfo getLot(GenRow genRow) {
			ProductLotInfo lotInfo = new ProductLotInfo();
			lotInfo.setEstateStageName(genRow.getString("StageName"));
			
			String cost = genRow.getColumn("DripResult");
			try {
				// if the price is $0.00 then use the TotalCost
				if (cost.replaceAll("[0\\.\\$]","").length() == 0) cost = genRow.getColumn("TotalCost");
				if(cost.indexOf(".")>0) { 
					cost = cost.substring(0, cost.indexOf("."));
				}
			} catch(Exception ee) {
				cost = genRow.getColumn("TotalCost");
				ee.printStackTrace();
			}
			if(!cost.contains(","))
				cost = PDFUtil.getIntPrice(Double.parseDouble(cost));
			lotInfo.setLotPrice(cost);
			lotInfo.setLotNumber(genRow.getString("Name"));
			if(StringUtils.isNotBlank(genRow.getString("CurrentStatus"))) {
				String status = genRow.getString("CurrentStatus");
				if(status == null)
					lotInfo.setLotStatus("Sold");
				else
					lotInfo.setLotStatus(status);
			}
				
			GenRow dimensions = getDimensions(genRow.getData("ProductID"));
			lotInfo.setProductID(genRow.getData("ProductID"));
			if(dimensions.getNext())
			{
				if(StringUtils.isNotBlank(dimensions.getString("SQmeters")))
					lotInfo.setLotSize(PDFUtil.formatArea(dimensions.getString("SQmeters"),PDFUtil.DECIMAL_FORMAT) + "m2");
				
				if(StringUtils.isNotBlank(dimensions.getString("Width")))
					lotInfo.setLotWidth(PDFUtil.formatArea(dimensions.getString("Width"),PDFUtil.DECIMAL_FORMAT));
			}
			dimensions.close();
			
			return lotInfo;
		}
		
		private GenRow getDimensions(String lotID) {
			GenRow details = new GenRow();
			details.setTableSpec("properties/LotProductDetails");
			details.setColumn("ProductID", lotID);
			details.setColumn("DetailsType", "Lot");
			details.setParameter("-select0", "*");
			details.sortBy("SortOrder",0);			
			if (details.getString("ProductID").length() > 0) {
				details.doAction("search");
				details.getResults(true);
			}
			return details;
		}
		
		public static String getEstateLogo(String estateID, HttpServletRequest request) {
			GenRow genRow;
			String estateLogo = null;
			
			if (StringUtils.isNotBlank(estateID)) {
				genRow = new GenRow();
				genRow.setViewSpec("ProductView");
				genRow.setParameter("ProductType", "Estate");
				genRow.setParameter("Active", "Y");
				genRow.setParameter("ProductID", estateID);
				genRow.sortBy("Name",0);
				genRow.setAction(GenerationKeys.SELECTFIRST);
				genRow.doAction();
				genRow.getResults();
				
				if(!genRow.isSuccessful()) {
					throw new NotFoundException("Estate not found: " + estateID);
				}
				logger.debug("LandService ==> getEstate Query is:  " + genRow.getData("ProductID") );
				
				if (genRow.getNext() ){
					if(StringUtils.isNotBlank(genRow.getString("ThumbnailImagePath")))
						estateLogo = genRow.getString("ThumbnailImagePath");
				}
			}
			return estateLogo;
		}
		
	}


