package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

public class Apartment extends PropertyEntity {

	final PropertyType propertyType = PropertyType.Apartment;
	static final String VIEWSPEC = "properties/ProductApartmentView";

	public Apartment(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public Apartment(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	
	public Apartment(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	public List<Building> getBuilding() {
		return Collections.emptyList();
	}
	public List<BuildingStage> getStages() {
		return Collections.emptyList();
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
	
	public String getName() {
		  return getString("Name");
		 }
	public String getStreetNumber() {
		  return getString("StreetNumber");
		 }
	
	public String getStreet(){
		return getString("Street");
	}
	
	public String getStreet2(){
		return getString("Street2");
	}

	public String getCity(){
		return getString("City");
	}
	
	public String getState(){
		
		return getString("State");
	}
   
	public String getPostCode(){
		
		return getString("PostCode");
	}
	
	public String getTotalCost(){
		
		return getString("TotalCost");
	}
	
	@Override
	public String[] getDripProductIDs() {
		return new String[]{getString("BuildingProductID"), getString("BuildingStageProductID"), getString(PRODUCTID)};
	}	
	
	public String getCost() { 
		String cost = getColumn("TotalCost");
		if(cost.indexOf(".")>0) { 
			return cost.substring(0, cost.indexOf("."));
		}
		return cost;
	}
	
	public String getDripTotalCost() { 
		String cost = getColumn("DripResult");
		// if the price is $0.00 then use the TotalCost
		if (cost.replaceAll("[0\\.\\$]","").length() == 0) cost = getColumn("TotalCost");
		if(cost.indexOf(".")>0) { 
			return cost.substring(0, cost.indexOf("."));
		}
		return cost;
	}
	
}
