package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import java.io.File;
import java.math.RoundingMode;
import java.net.URI;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.pdf.templates.model.Package;
import com.sok.service.crm.UserService;

public class PDFUtil extends CommonUtil implements PDFConstants {
	private static final Logger logger = LoggerFactory.getLogger(PDFUtil.class);
	
	public static final String website = "www.runway.com.au";
	public static final BaseColor aqua = new BaseColor(72, 71, 88);
	public static final String MAIN_LOGO_IMG = "/specific/images/logolarge.jpg";
	public static final String PIPELINE_LOGO_IMG = "/specific/images/pipeline_logo.jpg";
	public static final String DISCLAIMER_TEXT = "Prices current %1$te %1$tB %1$tY. Price and inclusions subject to change without notice. \u00A9 Copyright. Reproduction in any form either in whole or part is forbidden. All images used are for illustrative purposes only and are artist's impressions. Geographic building restrictions apply.";

	public static final BaseColor pkgHeaderGrey = new BaseColor(53, 52, 53);
	public static final BaseColor regularGrey = new BaseColor(35, 31, 32);
	
	public static final Font detailsFontBlue = FontFactory.getFont(arialFont, fontRatio * 12, aqua);
	public static final Font detailsFontBlack = FontFactory.getFont(arialFont, fontRatio * 16, BaseColor.BLACK);
	public static final Font detailsFontSmall = FontFactory.getFont(arialFont, fontRatio * 9, BaseColor.BLACK);
	public static final Font headerFont = FontFactory.getFont(arialFont, fontRatio * 38, BaseColor.WHITE);
	public static final Font priceFont = FontFactory.getFont(arialFont, fontRatio * 25, Font.BOLD, BaseColor.WHITE);
	public static final Font packageHeaderFont1 = FontFactory.getFont(arialFont, fontRatio * 23, Font.BOLD, BaseColor.WHITE);
	public static final Font packageHeaderFont2 = FontFactory.getFont(arialFont, fontRatio * 16, Font.BOLD, BaseColor.WHITE);

	public static final Font homeRangeFont = FontFactory.getFont(arialFont, fontRatio * 14, BaseColor.BLACK);
	public static final Font homeNameFont = FontFactory.getFont(arialFont, fontRatio * 27, Font.BOLD, aqua);
	public static final Font homeDetailsFont = FontFactory.getFont(arialFont, fontRatio * 12, BaseColor.BLACK);
	public static final Font homePriceFont = FontFactory.getFont(arialFont, fontRatio * 13, BaseColor.BLACK);
	public static final Font fontArialBlackPt28Bold = FontFactory.getFont(arialFont, fontRatio * 28, Font.BOLD, BaseColor.BLACK);
	public static final Font fontArialBlackPt18Bold = FontFactory.getFont(arialFont, fontRatio * 18, Font.BOLD, BaseColor.BLACK);

	public static final Font regularFont = FontFactory.getFont(arialFont, fontRatio * 13, BaseColor.BLACK);
	public static final Font fontArialBlackPt16 = FontFactory.getFont(arialFont, fontRatio * 16, BaseColor.BLACK);
	public static final Font smallFont = FontFactory.getFont(arialFont, fontRatio * 6, BaseColor.BLACK);
	public static final Font s1 = FontFactory.getFont(arialFont, fontRatio * 4, BaseColor.BLACK);
	
	public static final Font gothamBoldPt6RegularGrey =  FontFactory.getFont(gothamFontLight, fontRatio * 6f, Font.NORMAL, regularGrey);

	public static final Font gothamBookPt9RegularGrey =  FontFactory.getFont(gothamFontLight, fontRatio * 9f, Font.NORMAL, regularGrey);
	public static final Font gothamBookPt12RegularGrey =  FontFactory.getFont(gothamFontLight, fontRatio * 12f, Font.NORMAL, regularGrey);
	public static final Font gothamBookPt14RegularGrey =  FontFactory.getFont(gothamFontLight, fontRatio * 14f, Font.NORMAL, regularGrey);

	public static final Font gothamBookPt9BoldGrey =  FontFactory.getFont(gothamFontLight, fontRatio * 9f, Font.BOLD, regularGrey);
	public static final Font gothamBookPt12BoldGrey =  FontFactory.getFont(gothamFontLight, fontRatio * 12f, Font.BOLD, regularGrey);
	public static final Font gothamBookPt14BoldGrey =  FontFactory.getFont(gothamFontLight, fontRatio * 14f, Font.BOLD, regularGrey);
	public static final Font gothamBookPt16 =  FontFactory.getFont(gothamFontBook, fontRatio * 16f, Font.NORMAL, BaseColor.BLACK);
	public static final Font gothamBookPt16Bold =  FontFactory.getFont(gothamFontBook, fontRatio * 16f, Font.BOLD, BaseColor.BLACK);

	private static BaseFont defaultFont = null;
	private static BaseFont m100 = defaultFont, m300 = defaultFont, m500 = defaultFont, m700 = defaultFont, m900 = defaultFont, m700r = defaultFont;
	private static BaseFont h55 = defaultFont, h80 = defaultFont, hnc = defaultFont;
	
	
	// 15-Oct-2012 NI: Added for Henley PDFs.
	private static BaseFont h95 = defaultFont, h45 = defaultFont, h75 = defaultFont, h77 = defaultFont, h97 = defaultFont, h47 = defaultFont, h57 = defaultFont, h65 = defaultFont;
	private static BaseFont avantGardeBook = defaultFont, avantGardeMedium = defaultFont;
	private static BaseFont baskervilleRegular = defaultFont;
	private static BaseFont dinNormal = defaultFont;
	private static BaseFont dinBlack = defaultFont;
	private static BaseFont HelveticaNeueLTHeavy = defaultFont;
	private static BaseFont helvitaRoman = defaultFont;
	private static BaseFont helvitaNeuLtStdCn = defaultFont;
	private static BaseFont helvitaNeuLtStdBldCn = defaultFont;
	
	
	private static BaseFont tradeGothic = defaultFont;

	static {
		try {
			String helvpath = InitServlet.getRealPath() + "/specific/pdf/fonts/HelveticaNeueLTStd/";
			String helvcond = InitServlet.getRealPath() + "/specific/pdf/fonts/HelveticaNeueCondensed/";
			
			String avantGardeBookpath = InitServlet.getRealPath() + "/specific/pdf/fonts/AvantGardeBook/";
			String baskervillepath = InitServlet.getRealPath() + "/specific/pdf/fonts/Baskerville/";
			String tradeGothicPath = InitServlet.getRealPath() + "/specific/pdf/fonts/TradeGothicLTStd/";
			
			String dinFontPath = InitServlet.getRealPath() + "/specific/pdf/fonts/DIN/";
			

			h45 = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-Lt.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			h65 = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-Md.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			h75 = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-Bd.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			
			h95 = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-Blk.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			h47 = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-LtCn.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			
			helvitaRoman = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-Roman.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			helvitaNeuLtStdCn = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-Cn.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			helvitaNeuLtStdBldCn = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-BdCn.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			
			
			
			//h97 = BaseFont.createFont(helvpath + "????????????.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			//h57 = BaseFont.createFont(helvpath + "????????????.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			
			avantGardeBook = BaseFont.createFont(avantGardeBookpath + "AvantGarBoo.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
			avantGardeMedium = BaseFont.createFont(avantGardeBookpath + "AvantGarMed.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
			tradeGothic = BaseFont.createFont(tradeGothicPath + "TradeGothicLTStd-BdCn20.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			h77 = BaseFont.createFont(helvcond + "HelveNeuCon.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			
			dinNormal = BaseFont.createFont(dinFontPath + "DINReg.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
			dinBlack = BaseFont.createFont(dinFontPath + "DINBla.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
			
			//HelveticaNeueLTHeavy = BaseFont.createFont(helvpath + "HelveNeuHeaCon.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
			
			
		} catch (Exception e) {
			System.out.println("Ignoring Error while registering fonts. Error: " + e);
			e.printStackTrace();
		}
	}

	public static final Font tradeGothicPt30White = new Font(tradeGothic, 29.75f, Font.NORMAL, BaseColor.WHITE);
	public static final Font tradeGothicPt38White = new Font(tradeGothic, 37.83f, Font.NORMAL, BaseColor.WHITE);
	public static final Font tradeGothicPt22White = new Font(tradeGothic, 21.62f, Font.NORMAL, BaseColor.WHITE);
	public static final Font tradeGothicPt14White = new Font(tradeGothic, 13.8f, Font.NORMAL, BaseColor.WHITE);
	
	public static final Font helvitaNeue95Pt5White = new Font(h95, fontRatio * 5f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt8White = new Font(h95, fontRatio * 8f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt9White = new Font(h95, fontRatio * 9f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt11White = new Font(h95, fontRatio * 11f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt12White = new Font(h95, fontRatio * 12.1f, Font.NORMAL, BaseColor.WHITE);	
	public static final Font helvitaNeue95Pt16White = new Font(h95, fontRatio * 16.3f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt18White = new Font(h95, fontRatio * 18f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt19White = new Font(h95, fontRatio * 19f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt19_6White = new Font(h95, fontRatio * 19.6f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt20White = new Font(h95, fontRatio * 20f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt20_3White = new Font(h95, fontRatio * 20.3f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt20_5White = new Font(h95, fontRatio * 20.5f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt21White = new Font(h95, fontRatio * 21f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt22White = new Font(h95, fontRatio * 22f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt31White = new Font(h95, fontRatio * 31f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt32White = new Font(h95, fontRatio * 32f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt34White = new Font(h95, fontRatio * 34f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt43White = new Font(h95, fontRatio * 43f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt35White = new Font(h95, fontRatio * 35f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt40White = new Font(h95, fontRatio * 40f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt42White = new Font(h95, fontRatio * 42f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt45White = new Font(h95, fontRatio * 45f, Font.NORMAL, BaseColor.WHITE);
	
	public static final Font helvitaNeue95BoldPt16White = new Font(h95, fontRatio * 16.3f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95BoldPt18White = new Font(h95, fontRatio * 18f, Font.BOLD, BaseColor.WHITE);
	
	
	public static final Font helvitaNeue95Pt20Black = new Font(h95, fontRatio * 20f, Font.NORMAL, BaseColor.BLACK);
	
	public static final Font helvitaNeue65Pt6White = new Font(h65, fontRatio * 6f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt7White = new Font(h65, fontRatio * 7f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt8White = new Font(h65, fontRatio * 8f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt9White = new Font(h65, fontRatio * 9f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt10White = new Font(h65, fontRatio * 10f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt12White = new Font(h65, fontRatio * 12f, Font.NORMAL, BaseColor.WHITE);
	
	public static final Font helvitaNeue65Pt6BoldWhite = new Font(h65, fontRatio * 6f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt8BoldWhite = new Font(h65, fontRatio * 8f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt9BoldWhite = new Font(h65, fontRatio * 9f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt10BoldWhite = new Font(h65, fontRatio * 10f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt12BoldWhite = new Font(h65, fontRatio * 12f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt14BoldWhite = new Font(h65, fontRatio * 14f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue65Pt16BoldWhite = new Font(h65, fontRatio * 16f, Font.BOLD, BaseColor.WHITE);

	
	public static final Font helvitaNeue65Pt8Black = new Font(h65, fontRatio * 8f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue65Pt10Black = new Font(h65, fontRatio * 10f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue65Pt12Black = new Font(h65, fontRatio * 12f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue65Pt13Black = new Font(h65, fontRatio * 13f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue65Pt14Black = new Font(h65, fontRatio * 14f, Font.BOLD, BaseColor.BLACK);
	
	public static final Font helvitaNeue95Pt12BoldBlack = new Font(h95, fontRatio * 12.1f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue95Pt16BoldBlack = new Font(h95, fontRatio * 16f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue95Pt17BoldBlack = new Font(h95, fontRatio * 17f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue95Pt18BoldBlack = new Font(h95, fontRatio * 18f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue95Pt20BoldBlack = new Font(h95, fontRatio * 20f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue95Pt25BoldBlack = new Font(h95, fontRatio * 25f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue95Pt35BoldBlack = new Font(h95, fontRatio * 35f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue95Pt40BoldBlack = new Font(h95, fontRatio * 40f, Font.BOLD, BaseColor.BLACK);
	
	

	public static final Font helvitaNeue95Pt12BoldWhite = new Font(h95, fontRatio * 12.1f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt16BoldWhite = new Font(h95, fontRatio * 16f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt14BoldWhite = new Font(h95, fontRatio * 14f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt17BoldWhite = new Font(h95, fontRatio * 17f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt18BoldWhite = new Font(h95, fontRatio * 18f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt20BoldWhite = new Font(h95, fontRatio * 20f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt22BoldWhite = new Font(h95, fontRatio * 22f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt23BoldWhite = new Font(h95, fontRatio * 23f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt24BoldWhite = new Font(h95, fontRatio * 24f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt25BoldWhite = new Font(h95, fontRatio * 25f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt35BoldWhite = new Font(h95, fontRatio * 35f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt38BoldWhite = new Font(h95, fontRatio * 38f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue95Pt40BoldWhite = new Font(h95, fontRatio * 40f, Font.BOLD, BaseColor.WHITE);
	
	
	public static final Font helvitaNeueCond77NormalPt8White = new Font(h77, fontRatio * 8.5f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt9White = new Font(h77, fontRatio * 9f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt10White = new Font(h77, fontRatio * 10f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt20White = new Font(h77, fontRatio * 20.21f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt20fWhite = new Font(h77, 20.21f, Font.NORMAL, BaseColor.WHITE);
	
	public static final Font helvitaNeueCond77NormalPt30White = new Font(h77, fontRatio * 30f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt32White = new Font(h77, fontRatio * 32f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt34White = new Font(h77, fontRatio * 34f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt35White = new Font(h77, fontRatio * 35f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt36White = new Font(h77, fontRatio * 36f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt37White = new Font(h77, fontRatio * 37f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt38White = new Font(h77, fontRatio * 38f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueCond77NormalPt40White = new Font(h77, fontRatio * 40f, Font.NORMAL, BaseColor.WHITE);
	

	public static final Font helvitaNeueCond77BoldPt30White = new Font(h77, fontRatio * 30f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueCond77BoldPt32White = new Font(h77, fontRatio * 32f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueCond77BoldPt34White = new Font(h77, fontRatio * 34f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueCond77BoldPt35White = new Font(h77, fontRatio * 35f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueCond77BoldPt36White = new Font(h77, fontRatio * 36f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueCond77BoldPt37White = new Font(h77, fontRatio * 37f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueCond77BoldPt38White = new Font(h77, fontRatio * 38f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueCond77BoldPt39White = new Font(h77, fontRatio * 39f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueCond77BoldPt40White = new Font(h77, fontRatio * 40f, Font.BOLD, BaseColor.WHITE);

	public static final Font helvitaNeueLTRomanPt8White = new Font(helvitaRoman, fontRatio * 8.5f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt9White = new Font(helvitaRoman, fontRatio * 9f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt10White = new Font(helvitaRoman, fontRatio * 10f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt10_8White = new Font(helvitaRoman, fontRatio * 10.8f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt11White = new Font(helvitaRoman, fontRatio * 11.6f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt13White = new Font(helvitaRoman, fontRatio * 13f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt12White = new Font(helvitaRoman, fontRatio * 12f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt14White = new Font(helvitaRoman, fontRatio * 14f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt16White = new Font(helvitaRoman, fontRatio * 16f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt17White = new Font(helvitaRoman, fontRatio * 17f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt18White = new Font(helvitaRoman, fontRatio * 18f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt19White = new Font(helvitaRoman, fontRatio * 19f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt20White = new Font(helvitaRoman, fontRatio * 20f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanPt6fWhite = new Font(helvitaRoman, 6.78f, Font.NORMAL, BaseColor.WHITE);


	public static final Font helvitaNeueLTRomanBoldPt18White = new Font(helvitaRoman, fontRatio * 18f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanBoldPt20White = new Font(helvitaRoman, fontRatio * 20f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanBoldPt22White = new Font(helvitaRoman, fontRatio * 22f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueLTRomanBoldPt24White = new Font(helvitaRoman, fontRatio * 24f, Font.BOLD, BaseColor.WHITE);

	
	
	public static final Font helvitaNeue75NormalPt8White = new Font(h75, fontRatio * 8.5f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt9White = new Font(h75, fontRatio * 9f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt10White = new Font(h75, fontRatio * 10f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt11White = new Font(h75, fontRatio * 11.6f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt13White = new Font(h75, fontRatio * 13f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt12White = new Font(h75, fontRatio * 12f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt14White = new Font(h75, fontRatio * 14f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt15White = new Font(h75, fontRatio * 15f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt16White = new Font(h75, fontRatio * 16f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt17White = new Font(h75, fontRatio * 17f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt18White = new Font(h75, fontRatio * 18f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue75NormalPt19White = new Font(h75, fontRatio * 19f, Font.NORMAL, BaseColor.WHITE);
	
	
	public static final Font helvitaNeue75BoldPt6Black = new Font(h75, fontRatio * 6f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt7Black = new Font(h75, fontRatio * 7f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt8Black = new Font(h75, fontRatio * 8f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt9Black = new Font(h75, fontRatio * 9f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt10Black = new Font(h75, fontRatio * 10f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt12Black = new Font(h75, fontRatio * 12f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt13Black = new Font(h75, fontRatio * 13f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt14Black = new Font(h75, fontRatio * 14f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt15Black = new Font(h75, fontRatio * 15f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt16Black = new Font(h75, fontRatio * 16f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt17Black = new Font(h75, fontRatio * 17f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt18Black = new Font(h75, fontRatio * 18f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt20Black = new Font(h75, fontRatio * 20f, Font.BOLD, BaseColor.BLACK);
	public static final Font helvitaNeue75BoldPt25Black = new Font(h75, fontRatio * 25f, Font.BOLD, BaseColor.BLACK);
	
	
	public static final Font helvitaNeue75BoldPt6White = new Font(h75, fontRatio * 6f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt7White = new Font(h75, fontRatio * 7f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt8White = new Font(h75, fontRatio * 8f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt9White = new Font(h75, fontRatio * 9f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt10White = new Font(h75, fontRatio * 10f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt12White = new Font(h75, fontRatio * 12f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt15White = new Font(h75, fontRatio * 15f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt16White = new Font(h75, fontRatio * 16f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt17White = new Font(h75, fontRatio * 17f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt18White = new Font(h75, fontRatio * 18f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt20White = new Font(h75, fontRatio * 20f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt25White = new Font(h75, fontRatio * 25f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue75BoldPt28White = new Font(h75, fontRatio * 28f, Font.BOLD, BaseColor.WHITE);	
	public static final Font helvitaNeue75BoldPt30White = new Font(h75, fontRatio * 30f, Font.BOLD, BaseColor.WHITE);	
	public static final Font helvitaNeue75BoldPt32White = new Font(h75, fontRatio * 32f, Font.BOLD, BaseColor.WHITE);
	
	public static final Font helvitaNeue45LightPt20White = new Font(h45, fontRatio * 20.6f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt18White = new Font(h45, fontRatio * 18f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt16White = new Font(h45, fontRatio * 16f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt15White = new Font(h45, fontRatio * 15f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt8White = new Font(h45, fontRatio * 8f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt9White = new Font(h45, fontRatio * 9f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt9_5White = new Font(h45, fontRatio * 9.5f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt12White = new Font(h45, fontRatio * 12f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt10White = new Font(h45, fontRatio * 10f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt14White = new Font(h45, fontRatio * 14f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt25White = new Font(h45, fontRatio * 25f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt26White = new Font(h45, fontRatio * 26f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt28White = new Font(h45, fontRatio * 28f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt33White = new Font(h45, fontRatio * 33f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt2White = new Font(h45, fontRatio * 2f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt5White = new Font(h45, fontRatio * 5f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt3White = new Font(h45, fontRatio * 3f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt38White = new Font(h45, fontRatio * 38f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt40White = new Font(h45, fontRatio * 40f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt41White = new Font(h45, fontRatio * 41f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt42White = new Font(h45, fontRatio * 42f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt45White = new Font(h45, fontRatio * 45f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue45LightPt48White = new Font(h45, fontRatio * 48f, Font.NORMAL, BaseColor.WHITE);
	
	public static final Font helvitaNeue45LightPt14BoldWhite = new Font(h45, fontRatio * 14f, Font.BOLD, BaseColor.WHITE);

	public static final Font helvitaNeue45LightPt20Black = new Font(h45, fontRatio * 20.6f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt18Black = new Font(h45, fontRatio * 18f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt16Black = new Font(h45, fontRatio * 16f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt15Black = new Font(h45, fontRatio * 15f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt8Black = new Font(h45, fontRatio * 8f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt12Black = new Font(h45, fontRatio * 12f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt10Black = new Font(h45, fontRatio * 10f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt14Black = new Font(h45, fontRatio * 14f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt25Black = new Font(h45, fontRatio * 25f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt28Black = new Font(h45, fontRatio * 28f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt33Black = new Font(h45, fontRatio * 33f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue45LightPt5Black = new Font(h45, fontRatio * 5f, Font.NORMAL, BaseColor.BLACK);

	public static final Font helvitaNeue45LightPt12Red = new Font(h45, fontRatio * 12.6f, Font.NORMAL, BaseColor.RED);
	
	public static final Font helvitaNeue45LightPt12BoldRed = new Font(h45, fontRatio * 12.6f, Font.BOLD, BaseColor.RED);
	public static final Font helvitaNeue45LightPt16BoldRed = new Font(h45, fontRatio * 16f, Font.BOLD, BaseColor.RED);
	public static final Font helvitaNeue45LightPt18BoldRed = new Font(h45, fontRatio * 18f, Font.BOLD, BaseColor.RED);
	public static final Font helvitaNeue45LightPt20BoldRed = new Font(h45, fontRatio * 18f, Font.BOLD, BaseColor.RED);
	
	public static final Font helvitaNeue45LightBoldPt10White = new Font(h45, fontRatio * 10f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt12White = new Font(h45, fontRatio * 12f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt14White = new Font(h45, fontRatio * 14f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt16White = new Font(h45, fontRatio * 16f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt17White = new Font(h45, fontRatio * 17f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt18White = new Font(h45, fontRatio * 18f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt20White = new Font(h45, fontRatio * 20f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt25White = new Font(h45, fontRatio * 25f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt28White = new Font(h45, fontRatio * 28f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt30White = new Font(h45, fontRatio * 30f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt35White = new Font(h45, fontRatio * 35f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt38White = new Font(h45, fontRatio * 38f, Font.BOLD, BaseColor.WHITE);	
	public static final Font helvitaNeue45LightBoldPt40White = new Font(h45, fontRatio * 40f, Font.BOLD, BaseColor.WHITE);	
	public static final Font helvitaNeue45LightBoldPt45White = new Font(h45, fontRatio * 45f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt46White = new Font(h45, fontRatio * 46f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt47White = new Font(h45, fontRatio * 47f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeue45LightBoldPt48White = new Font(h45, fontRatio * 48f, Font.BOLD, BaseColor.WHITE);

	public static final Font helvitaNeueLTHeavyPt40White = new Font(HelveticaNeueLTHeavy, fontRatio * 40f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTHeavyPt42White = new Font(HelveticaNeueLTHeavy, fontRatio * 42f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTHeavyPt44White = new Font(HelveticaNeueLTHeavy, fontRatio * 44f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueLTHeavyPt47White = new Font(HelveticaNeueLTHeavy, fontRatio * 47f, Font.NORMAL, BaseColor.WHITE);
	

	public static final Font helvitaNeueLTHeavyBoldPt40White = new Font(HelveticaNeueLTHeavy, fontRatio * 40f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueLTHeavyBoldPt42White = new Font(HelveticaNeueLTHeavy, fontRatio * 42f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueLTHeavyBoldPt44White = new Font(HelveticaNeueLTHeavy, fontRatio * 44f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueLTHeavyBoldPt47White = new Font(HelveticaNeueLTHeavy, fontRatio * 47f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueLTHeavyBoldPt48White = new Font(HelveticaNeueLTHeavy, fontRatio * 47.5f, Font.BOLD, BaseColor.WHITE);
	public static final Font helvitaNeueLTHeavyBoldPt49White = new Font(HelveticaNeueLTHeavy, fontRatio * 49f, Font.BOLD, BaseColor.WHITE);

	
	public static final Font helvitaNeue47LightCondensePt6Black = new Font(h47, fontRatio * 6.75f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue47LightCondensePt8Black = new Font(h47, fontRatio * 8f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue47LightCondensePt10Black = new Font(h47, fontRatio * 10f, Font.NORMAL, BaseColor.BLACK);
	public static final Font helvitaNeue47LightCondensePt12Black = new Font(h47, fontRatio * 12f, Font.NORMAL, BaseColor.BLACK);
	
	
	public static final Font helvitaNeue47LightCondensePt2White = new Font(h47, fontRatio * 2f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt3White = new Font(h47, fontRatio * 3f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt4White = new Font(h47, fontRatio * 4f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt5White = new Font(h47, fontRatio * 5f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt6White = new Font(h47, fontRatio * 6f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt8White = new Font(h47, fontRatio * 8f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt9White = new Font(h47, fontRatio * 9f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt10White = new Font(h47, fontRatio * 9.8f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt11White = new Font(h47, fontRatio * 11.6f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt12White = new Font(h47, fontRatio * 12f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt37White = new Font(h47, fontRatio * 37f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt40White = new Font(h47, fontRatio * 40f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue47LightCondensePt42White = new Font(h47, fontRatio * 42f, Font.NORMAL, BaseColor.WHITE);

	
	
	public static final Font helvitaNeue57LightStdCondensePt10White = new Font(helvitaNeuLtStdCn, fontRatio * 10f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt15White = new Font(helvitaNeuLtStdCn, fontRatio * 15f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt25White = new Font(helvitaNeuLtStdCn, fontRatio * 25f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt28White = new Font(helvitaNeuLtStdCn, fontRatio * 28f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt30White = new Font(helvitaNeuLtStdCn, fontRatio * 30f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt34White = new Font(helvitaNeuLtStdCn, fontRatio * 34f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt36White = new Font(helvitaNeuLtStdCn, fontRatio * 36f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt38White = new Font(helvitaNeuLtStdCn, fontRatio * 38f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt40White = new Font(helvitaNeuLtStdCn, fontRatio * 40f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt41White = new Font(helvitaNeuLtStdCn, fontRatio * 41f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt41_5White = new Font(helvitaNeuLtStdCn, fontRatio * 41.5f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt42White = new Font(helvitaNeuLtStdCn, fontRatio * 42f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt44White = new Font(helvitaNeuLtStdCn, fontRatio * 44f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue57LightStdCondensePt46White = new Font(helvitaNeuLtStdCn, fontRatio * 46f, Font.NORMAL, BaseColor.WHITE);

	
	
	public static final Font helvitaNeue77LightStdBoldCondensePt36White = new Font(helvitaNeuLtStdBldCn, fontRatio * 36f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue77LightStdBoldCondensePt38White = new Font(helvitaNeuLtStdBldCn, fontRatio * 38f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue77LightStdBoldCondensePt40White = new Font(helvitaNeuLtStdBldCn, fontRatio * 40f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue77LightStdBoldCondensePt42White = new Font(helvitaNeuLtStdBldCn, fontRatio * 42f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue77LightStdBoldCondensePt44White = new Font(helvitaNeuLtStdBldCn, fontRatio * 44f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue77LightStdBoldCondensePt46White = new Font(helvitaNeuLtStdBldCn, fontRatio * 46f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue77LightStdBoldCondensePt16fWhite = new Font(helvitaNeuLtStdBldCn, 16.98f, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeue77LightStdBoldCondensePt38fWhite = new Font(helvitaNeuLtStdBldCn, 38.92f, Font.NORMAL, BaseColor.WHITE);
	
	public static final Font dinBlackPt6NormalWhite = new Font(dinBlack, fontRatio * 6f, Font.NORMAL, BaseColor.WHITE);
	public static final Font dinBlackPt7NormalWhite = new Font(dinBlack, fontRatio * 7f, Font.NORMAL, BaseColor.WHITE);
	public static final Font dinBlackPt8NormalWhite = new Font(dinBlack, fontRatio * 8f, Font.NORMAL, BaseColor.WHITE);
	public static final Font dinBlackPt12NormalWhite = new Font(dinBlack, fontRatio * 12f, Font.NORMAL, BaseColor.WHITE);
	public static final Font dinBlackPt14NormalWhite = new Font(dinBlack, fontRatio * 14f, Font.NORMAL, BaseColor.WHITE);
	public static final Font dinBlackPt16NormalWhite = new Font(dinBlack, fontRatio * 16f, Font.NORMAL, BaseColor.WHITE);
	
	public static final Font dinBlackPt12BoldWhite = new Font(dinBlack, fontRatio * 12f, Font.BOLD, BaseColor.WHITE);
	public static final Font dinBlackPt13BoldWhite = new Font(dinBlack, fontRatio * 13.8f, Font.BOLD, BaseColor.WHITE);
	public static final Font dinBlackPt14BoldWhite = new Font(dinBlack, fontRatio * 14f, Font.BOLD, BaseColor.WHITE);
	public static final Font dinBlackPt15BoldWhite = new Font(dinBlack, fontRatio * 15.8f, Font.BOLD, BaseColor.WHITE);
	public static final Font dinBlackPt16BoldWhite = new Font(dinBlack, fontRatio * 16f, Font.BOLD, BaseColor.WHITE);
	public static final Font dinBlackPt24BoldWhite = new Font(dinBlack, fontRatio * 24f, Font.BOLD, BaseColor.WHITE);
	public static final Font dinBlackPt25BoldWhite = new Font(dinBlack, fontRatio * 25f, Font.BOLD, BaseColor.WHITE);
	
	public static final Font avantGardeBookPt2White = new Font(avantGardeBook, fontRatio * 2f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt4White = new Font(avantGardeBook, fontRatio * 4f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt5White = new Font(avantGardeBook, fontRatio * 5f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt6White = new Font(avantGardeBook, fontRatio * 6f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt7White = new Font(avantGardeBook, fontRatio * 7f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt8White = new Font(avantGardeBook, fontRatio * 8f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt9White = new Font(avantGardeBook, fontRatio * 9f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt10White = new Font(avantGardeBook, fontRatio * 10f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt12White = new Font(avantGardeBook, fontRatio * 12f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt16White = new Font(avantGardeBook, fontRatio * 16f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt20White = new Font(avantGardeBook, fontRatio * 20f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt25White = new Font(avantGardeBook, fontRatio * 25f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt30White = new Font(avantGardeBook, fontRatio * 30f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt35White = new Font(avantGardeBook, fontRatio * 35f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt40White = new Font(avantGardeBook, fontRatio * 40f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt45White = new Font(avantGardeBook, fontRatio * 45f, Font.NORMAL, BaseColor.WHITE);
	public static final Font avantGardeBookPt50White = new Font(avantGardeBook, fontRatio * 50f, Font.NORMAL, BaseColor.WHITE);
	
	public static final Font avantGardeBookPt12Black = new Font(avantGardeBook, fontRatio * 12f, Font.NORMAL, BaseColor.BLACK);
	public static final Font avantGardeBookPt14Black = new Font(avantGardeBook, fontRatio * 14f, Font.NORMAL, BaseColor.BLACK);
	public static final Font avantGardeBookPt18Black = new Font(avantGardeBook, fontRatio * 18f, Font.NORMAL, BaseColor.BLACK);
	public static final Font avantGardeBookPt30Black = new Font(avantGardeBook, fontRatio * 30f, Font.NORMAL, BaseColor.BLACK);
	public static final Font avantGardeBookPt40Black = new Font(avantGardeBook, fontRatio * 40f, Font.NORMAL, BaseColor.BLACK);
	public static final Font avantGardeBookPt45Black = new Font(avantGardeBook, fontRatio * 45f, Font.NORMAL, BaseColor.BLACK);
	
	public static final Font avantGardeBookPt25BoldWhite = new Font(avantGardeBook, fontRatio * 25f, Font.BOLD, BaseColor.WHITE);
	public static final Font avantGardeBookPt35BoldWhite = new Font(avantGardeBook, fontRatio * 35f, Font.BOLD, BaseColor.WHITE);
	public static final Font avantGardeBookPt40BoldWhite = new Font(avantGardeBook, fontRatio * 40f, Font.BOLD, BaseColor.WHITE);
	public static final Font avantGardeBookPt45BoldWhite = new Font(avantGardeBook, fontRatio * 45f, Font.BOLD, BaseColor.WHITE);
	public static final Font avantGardeBookPt50BoldWhite = new Font(avantGardeBook, fontRatio * 50f, Font.BOLD, BaseColor.WHITE);
	
	public static final Font helvitaNeueBoldPt12White = new Font(h75, fontRatio * 12, Font.NORMAL, BaseColor.WHITE);
	public static final Font helvitaNeueBoldPt12Black = new Font(h75, fontRatio * 12, Font.NORMAL, BaseColor.BLACK);
	
	//public static final String QUEST_LOGO = "/specific/pdf/images/quest_logo.png";
	//public static final String QUEST_IMG_BY_BUILDING_HEADER = "/specific/pdf/images/by_building_header.png";
	//public static final String QUEST_IMG_FOOTER = "/specific/pdf/images/pdf_footer.png";
	//public static final String QUEST_IMG_LOGO_TAGLINE = "/specific/pdf/images/quest_logo_tagline.png";
	//public static final String QUEST_IMG_SINGLE_APARTMENT_HEADER = "/specific/pdf/images/single_apartment_header.png";
	//public static final String QUEST_IMG_VARIOUS_BUILDING_HEADER = "/specific/pdf/images/various_buildings_header.png";
	
	public static final String PDF_DETAILS_BY_BUILDING = "Details By Building";
	public static final String PDF_DETAILS_VARIOUS_BUILDINGS = "Various Building Details";
	public static final String PDF_DETAILS_SINGLE_APARTMENT = "Single Apartment Details";
	
	public static final String APARTMENT_DETAILS_HEADING_TEXT = "State of the Art design by Switched-On Group";
	public static final String APARTMENT_DETAILS_DESCRIPTION_TEXT = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
	
	
	public static final DecimalFormat AREA_FMT = new DecimalFormat("#.##m2");
	
	public static final String DECIMAL_FORMAT = "#.#";
	
	/*Column Headers*/
	public static final String STAGE = "Stage";
	public static final String LOT_SIZE = "Lot \nSize";
	public static final String LOT_FRONTAGE = "Lot \nFrontage";
	public static final String LOT_STATUS = "Lot \nStatus";
	public static final String LOT_PRICE = "Lot \nPrice";
	public static final String LOT_NUMBER = "Lot \nNumber";

	public static Paragraph getArea(String prefix, String suffix, String s) {
		return getArea(prefix, suffix, s, detailsFontBlack, detailsFontSmall);
	}

	public static Paragraph getArea(String prefix, String suffix, String s, Font font, Font smallFont) {
		Paragraph p = new Paragraph();
		p.add(new Chunk(prefix, font));
		p.add(new Chunk(s, font));
		p.add(new Chunk("m", font));
		Chunk c = new Chunk("2", smallFont);
		c.setTextRise(4.0f);
		p.add(c);
		p.add(new Chunk(suffix, font));
		return (p);
	}

	public static class Estate {
		public String estateName, estateLogo;
		public java.util.List<Package> packages = new ArrayList<Package>();
		public HashMap<String, Land> lands = new HashMap<String, Land>();
	}

	public static class Home {
		public String name;
		public String price;
		public String rangeName;
		public String bathroomsAsString;
		public int bedrooms, bathrooms, carParks;
		public Range range;
		public java.util.List<Package> packages = new ArrayList<Package>();
	}

	public static class Land {
		public String name, landPrice, estateName, suburb, titleDate;
		public String landImage, estateImage, streetNumber, street, street2, postcode;  
		public double lotSize, width, depth;
		public java.util.List<Package> packages = new ArrayList<Package>();
	}

	public static class PackageNameComparator implements Comparator<Package> {
		public int compare(Package p1, Package p2) {
			return p1.name.compareTo(p2.name);
		}
	}

	public static class EstateComparator implements Comparator {
		public int compare(Object p1, Object p2) {
			String p1Name = ((Package) p1).estateName;
			String p2Name = ((Package) p2).estateName;
			return p1Name.compareTo(p2Name);
		}
	}

	public enum Range {
		AffinityCollection, EssentialSeries, TRange; // Eco for VicUrban

		public static Range fromString(String s) {
			if (StringUtils.isBlank(s))
				return null;

			try {
				return Range.valueOf(s.replaceAll(" ", ""));
			} catch (Exception e) {
				System.out.println(String.format("Unable to load range for '%s'", s));
				return null;
			}
		}

		public BaseColor getColor() {
			return rangeColors[this.ordinal()];
		}

		public String getName() {
			return names[this.ordinal()];
		}

		public String getLogo() {
			return logos[this.ordinal()];
		}

		public String getTick() {
			return ticks[this.ordinal()];
		}

		/* takes a font as defined and creates new font with diff color */
		public Font getInclusionsFont(Font font) {
			return new Font(font.getBaseFont(), font.getSize(), font.getStyle(), getColor());
		}

		private final BaseColor[] rangeColors = { new BaseColor(180, 151, 90), // #b4975a
				new BaseColor(141, 198, 63), // #8dc63f
				new BaseColor(244, 117, 34) // #f47522
		};
		private final String[] logos = { "/specific/pdf/images/affinity_icon.jpg", "/specific/pdf/images/essential_icon.jpg", "/specific/pdf/images/trange_icon.jpg", };
		private final String[] ticks = { "/specific/pdf/images/tick_gold.png", "/specific/pdf/images/tick_green.png", "/specific/pdf/images/tick_orange.png", };
		private final String[] names = { "Affinity Collection", "Essential Series", "TRange", };
	}

	public static Package getPackage(HouseLandPackage product) {
		final Package p = new Package();
		String lotName = product.getLotName();
		p.lotName = lotName;

		if (lotName != null)
			p.lotNo = lotName.toUpperCase().startsWith("LOT") ? lotName.substring(4) : lotName;

		p.filename = StringUtil.ToAlphaNumeric(product.getName());
		if (StringUtils.isBlank(p.filename)) {
			p.filename = "flyer";
		}
		String stageName = product.getStageName();
		if (stageName != null)
			stageName = product.getStageName().toUpperCase().startsWith("STAGE") ? product.getStageName().substring(6) : product.getStageName();

		//p.heroImage = PropertyEntity.resizeImage(product.getImage(), 145, 87, PropertyEntity.ResizeOption.Crop, FORCE_TRUE);
		p.heroImage = InitServlet.getConfig().getServletContext().getInitParameter("URLHome") + "/" +  product.getImage();
		p.estateName = capitalizeFirstLetters(product.getEstateName());
		p.estateLogo = product.getEstateImage();
		p.range = Range.fromString(product.getRangeName());
		p.rangeName = product.getRangeName();
		p.name = product.getName();
		p.price = product.getCost();
		p.carParks = product.getCarParks();
		p.bedrooms = product.getBedrooms();	
		try {
			p.bathrooms = product.getBathrooms();
		}
		catch (NumberFormatException nfe)
		{
			//To avoid throwing an exception
		}
		if(StringUtils.isNotBlank(product.getBathroomsAsString()))
			p.bathroomsAsString = product.getBathroomsAsString().replace(".0", "");
		p.lotSize = product.getLotSize();
		p.stageName = stageName;
		p.suburb = capitalizeFirstLetters(product.getLand() != null ? product.getLand().getString("City") : null);
		p.titleDate = getEmptyStringIfNull(product.getAvailableDate());
		p.landPrice = product.getLandPrice();
		return p;
	}
	
	// for backward compatability
	public static PdfPTable getHeader(Estate e, Home h) throws DocumentException {
		return getHeader(null, e, h);
	}
	
	public static PdfPTable getHeader(HttpServletRequest request, Estate e, Home h) throws DocumentException {
		PdfPCell cell = new PdfPCell();
		PdfPTable headerTable = new PdfPTable(2);
		headerTable.setWidths(new float[] { (185f / 909f), (612f / 909f) });
		headerTable.setWidthPercentage(100f);
		headerTable.getDefaultCell().setPadding(0);
		headerTable.getDefaultCell().setPaddingBottom(11);
		headerTable.getDefaultCell().setPaddingLeft(0);
		headerTable.getDefaultCell().setBorder(0);
		// headerTable.getDefaultCell().setFixedHeight(120f);
		
		String logo = MAIN_LOGO_IMG;
		
		if(request!= null && "yes".equalsIgnoreCase(request.getParameter("portalRequest"))) {
			try {
				//URL fileURL = null;
				//fileURL = request.getServletContext().getResource(PIPELINE_LOGO_IMG);
				//if(fileURL != null) logo = PIPELINE_LOGO_IMG;
	     		File file = new File(InitServlet.getRealPath(request.getContextPath() + "/" + PIPELINE_LOGO_IMG));    		
	     		if(file.exists()) logo = PIPELINE_LOGO_IMG;
			} catch(Exception ex) {
				ex.printStackTrace();
				logger.trace("Exception loading pipeline image {}" , ex.getMessage());
			}
		}
		logger.trace("Logo image is {}" , logo);
		
		String BUILDER_IMG = PropertyEntity.resizeImage(logo, 165, 170, PropertyEntity.ResizeOption.Pad, true, true, 250, 300);
		Image i = createImage(BUILDER_IMG);
		if (i != null) {
			headerTable.addCell(i);
		}

		if (e != null) {
			cell = new PdfPCell();
			if (e.estateLogo != null && (i = createImage(e.estateLogo)) != null) {
				cell = new PdfPCell(i, true);
			} else {
				cell = new PdfPCell(new Paragraph(" estate? " + String.valueOf(e.estateLogo), regularFont));
			}
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setPaddingLeft(230);
			cell.setBorder(0);
			cell.setPaddingTop(35);
			cell.setPaddingBottom(30);
			cell.setPaddingRight(5);
			cell.setFixedHeight(120f);
			headerTable.addCell(cell);
		} else if (h != null) {
			Paragraph p = new Paragraph();
			Chunk homeName = new Chunk(h.name.toUpperCase() + "\n", homeNameFont);

			p.setAlignment(Element.ALIGN_LEFT);
			if (h.rangeName != null)
				p.add(new Chunk(h.rangeName.toUpperCase() + "\n", homeRangeFont));
			else
				p.add(new Chunk("" + "\n", homeRangeFont));
			p.add(new Chunk("\n", smallFont));
			p.add(homeName);
			p.add(new Chunk("\n", smallFont));
			p.add(new Chunk("\n", smallFont));
			p.add(new Chunk("BED  ", homeDetailsFont));
			p.add(new Chunk(h.bedrooms + "   ", detailsFontBlack));
			p.add(new Chunk("GARAGE  ", homeDetailsFont));
			p.add(new Chunk(h.carParks + "   ", detailsFontBlack));
			p.add(new Chunk("BATH  ", homeDetailsFont));
			p.add(new Chunk(h.bathroomsAsString + " \n", detailsFontBlack));
			p.add(new Chunk("\n", smallFont));
			p.add(new Chunk("BASE HOUSE PRICE ", homeDetailsFont));
			p.add(new Chunk(h.price, homePriceFont));

			// Get the width based on the home name
			float width = homeName.getWidthPoint();
			width = width < 120 ? 120 : width;
			PdfPTable homeTable = new PdfPTable(2);
			homeTable.setWidths(new float[] { 400 - width, width });
			homeTable.getDefaultCell().setBorder(1);
			homeTable.getDefaultCell().setPaddingBottom(20);
			homeTable.getDefaultCell().setPaddingRight(10);
			homeTable.getDefaultCell().setNoWrap(true);
			homeTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			homeTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
			homeTable.addCell(BLANK);
			homeTable.addCell(new Paragraph(p));

			headerTable.addCell(homeTable);
		}

		headerTable.getDefaultCell().setFixedHeight(0f);
		headerTable.getDefaultCell().setColspan(2);
		headerTable.getDefaultCell().setPaddingTop(3);
		headerTable.getDefaultCell().setPaddingBottom(9);
		headerTable.getDefaultCell().setPaddingLeft(0);
		headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		headerTable.getDefaultCell().setBackgroundColor(aqua);
		headerTable.addCell(new Paragraph(InitServlet.getSystemParams().getProperty("PDF-HnlTitle", "FIXED PRICE HOUSE AND LAND PACKAGES"), headerFont));
		return headerTable;
	}

	public static PdfPTable buildPackageTable(Package p, boolean estate) throws DocumentException {
		PdfPTable packageRow = new PdfPTable(2);
		packageRow.setWidths(new int[] { 24, 75 });
		packageRow.getDefaultCell().setBorder(0);
		packageRow.getDefaultCell().setPadding(0);
		packageRow.setWidthPercentage(100f);

		Image i = null;
		PdfPCell cell = null;

		String leftImage = estate ? 
				p.heroImage	 : PropertyEntity.resizeImage(p.estateLogo, 140, 70, PropertyEntity.ResizeOption.Pad);

		if (leftImage != null && (i = createImage(leftImage)) != null) {
			cell = new PdfPCell(i, true);
		} else {
			cell = new PdfPCell(new Paragraph("Problem with image" + leftImage, regularFont));
		}

		cell.setBorder(0);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		if (estate) {
			cell.setPadding(0);
		} else {
			cell.setPadding(8);
			cell.setBorderWidthLeft(0.1f);
			cell.setBorderWidthRight(0.1f);
		}

		packageRow.addCell(cell);
		packageRow.addCell(buildDetailsTable(p, estate));
		return packageRow;
	}

	private static PdfPTable buildDetailsTable(Package p, boolean estate) throws DocumentException {
		PdfPTable packageTable = new PdfPTable(9);
		PdfPCell cell = new PdfPCell();
		Image i = null;
		packageTable.setWidthPercentage(100f);
		packageTable.setWidths(new int[] { 5, 9, 32, 13, 5, 8, 5, 5, 5 });

		if (estate && p.range != null) {
			String resizedRangeImage = PropertyEntity.resizeImage(p.range.getLogo(), 55, 55, PropertyEntity.ResizeOption.Fit);
			if (p.range != null && (i = createImage(resizedRangeImage)) != null) {
				cell = new PdfPCell(i, true);
			} else {
				cell = new PdfPCell(new Paragraph("", regularFont));
			}
			cell.setBorder(0);
			cell.setPadding(0);
			cell.setFixedHeight(22.5f);
			cell.setBackgroundColor(pkgHeaderGrey);
			packageTable.addCell(cell);

			packageTable.getDefaultCell().setPadding(0);
			packageTable.getDefaultCell().setColspan(2);
			packageTable.getDefaultCell().setPaddingLeft(20f);

		} else {
			packageTable.getDefaultCell().setPadding(0);
			packageTable.getDefaultCell().setColspan(4);
			packageTable.getDefaultCell().setPaddingLeft(15f);
		}

		packageTable.getDefaultCell().setBorder(0);
		packageTable.getDefaultCell().setPaddingBottom(2f);
		packageTable.getDefaultCell().setFixedHeight(22.5f);
		packageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.getDefaultCell().setBackgroundColor(pkgHeaderGrey);

		if (estate) {
			packageTable.addCell(new Paragraph(p.name.toUpperCase(), packageHeaderFont1));
			packageTable.getDefaultCell().setPaddingLeft(60f);
		} else {
			packageTable.addCell(new Paragraph((p.estateName + ", " + p.suburb).toUpperCase(), packageHeaderFont2));
			packageTable.getDefaultCell().setPaddingLeft(0f);
		}
		packageTable.getDefaultCell().setColspan(6);
		packageTable.addCell(new Paragraph(String.valueOf(p.price), priceFont));

		packageTable.getDefaultCell().setFixedHeight(0f);
		packageTable.getDefaultCell().setColspan(1);
		packageTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		packageTable.getDefaultCell().setPadding(1);
		packageTable.getDefaultCell().setPaddingTop(2);
		packageTable.getDefaultCell().setPaddingLeft(3);
		packageTable.getDefaultCell().setPaddingRight(3);
		packageTable.getDefaultCell().setPaddingBottom(1);
		packageTable.getDefaultCell().setNoWrap(true);

		// Package Details
		// First Row
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("LOT NO.", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(p.lotNo, detailsFontBlack));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("BED", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(String.valueOf(p.bedrooms), detailsFontBlack));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("GARAGE", detailsFontBlue));

		packageTable.getDefaultCell().setPaddingLeft(2);
		packageTable.getDefaultCell().setPaddingRight(2);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(String.valueOf(p.carParks), detailsFontBlack));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("BATH", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(String.valueOf(p.bathroomsAsString), detailsFontBlack));
		packageTable.getDefaultCell().setPaddingLeft(3);
		packageTable.getDefaultCell().setPaddingRight(3);

		// Second Row
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("LOT SIZE", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		Paragraph lotSize = new Paragraph("", detailsFontBlack);
		if (p.lotSize > 0) {
			lotSize.add(getArea("", "", String.valueOf(new Double(p.lotSize).intValue())));
		}
		packageTable.addCell(lotSize);

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("STAGE", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.getDefaultCell().setColspan(5);
		packageTable.addCell(new Paragraph(p.stageName, detailsFontBlack));

		// Third Row
		packageTable.getDefaultCell().setColspan(1);
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);

		if (estate) {
			packageTable.addCell(new Paragraph("ESTATE", detailsFontBlue));
			packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			packageTable.addCell(new Paragraph(p.estateName, detailsFontBlack));
		} else {
			packageTable.addCell(new Paragraph("HOUSE TYPE", detailsFontBlue));
			packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			packageTable.addCell(new Paragraph(p.name, detailsFontBlack));
		}

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("EXPECTED TITLE DATE", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.getDefaultCell().setColspan(5);
		packageTable.addCell(new Paragraph(p.titleDate, detailsFontBlack));

		// Fourth Row
		packageTable.getDefaultCell().setColspan(1);
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);

		if (estate) {
			packageTable.addCell(new Paragraph("SUBURB", detailsFontBlue));
			packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			packageTable.addCell(new Paragraph(p.suburb, detailsFontBlack));
		} else {
			packageTable.addCell(new Paragraph("RANGE", detailsFontBlue));
			packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			packageTable.addCell(new Paragraph(p.rangeName, detailsFontBlack));
		}

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("LAND PRICE", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.getDefaultCell().setColspan(5);
		packageTable.addCell(new Paragraph(p.landPrice, detailsFontBlack));

		packageTable.getDefaultCell().setColspan(9);
		packageTable.addCell(new Paragraph("", detailsFontBlack));

		return packageTable;
	}
	
	public static Package getLandDetailsPackage(com.sok.runway.crm.cms.properties.Land product, String simpleName) {
		final Package p = new Package();

		String productID = product.getString("ProductID");
		
		p.name = product.getString("Name");
		p.lotName = product.getString("Name");
		p.landPrice = product.getColumn("TotalCost");
		
		p.street = product.getString("Street");
		p.street2 = product.getString("Street2");
		p.suburb = product.getString("City");
		p.estateName = product.getString("EstateName");
		p.lotSize = product.getDouble("Area");
		p.lotWidth = product.getDouble("LotWidth");
		p.lotDepth = product.getDouble("LotDepth");
		
		if (product.getString("ImagePath")!=null && !"".equalsIgnoreCase(product.getString("ImagePath")))
		{	
				//p.heroImage = PropertyEntity.resizeImage(product.getString("ImagePath"), 145, 87, PropertyEntity.ResizeOption.Crop, FORCE_TRUE);
				p.heroImage = product.getString("ImagePath");
		}
		else
		{	
				GenRow imagesList = new GenRow();
				imagesList.setViewSpec("LinkedDocumentView");
				imagesList.setParameter("ProductID", productID);
				if (productID!=null && !"".equalsIgnoreCase(productID))
				{	
					imagesList.doAction(GenerationKeys.SEARCH);
					imagesList.getResults();
					imagesList.getNext();
					p.heroImage = imagesList.getString("FilePath");
					imagesList.close();
				}
		}			
		
		p.estateLogo = product.getString("EstateThumbnailImagePath");
		
		/////////////////p.filename = StringUtil.ToAlphaNumeric(product..getName());
		if (StringUtils.isBlank(p.filename)) {
			p.filename = "flyer";
		}
		
		if(simpleName != null) {
			GenRow prodLot = null;
			prodLot = new GenRow();
			prodLot.setViewSpec("properties/ProductLotView");
			prodLot.setParameter("ProductID", product.getString("ProductID"));
			prodLot.setParameter("Status", "Available");
			prodLot.setParameter("Active", "Y");
			
			prodLot.doAction("search");
			prodLot.getResults();
			prodLot.getNext();
			
			String estateID = prodLot.getString("EstateProductID");
			String stageID = prodLot.getString("StageProductID");
			String lotID = product.getString("ProductID");
			prodLot.close();
			
			Map<String, String> dm = com.sok.service.crm.cms.properties.DripService.getInstance().getDripMapForTemplates(product.getConnection(), UserService.getInstance().getSimpleUser(product.getConnection(), "F180D2D08404F1241723606192C82747"), new String[] { estateID, stageID, lotID }, "PDF", simpleName);
			
			//String landDrip4Disclaimer = property.getDripText(request, new String[] { estateID, stageID, lotID, property.getString(PRODUCTID) }, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip
			//disclaimer = property.getDripText(request, new String[] { estateID, stageID, lotID, property.getString(PRODUCTID) }, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip

			p.disclaimer = dm.get("Text4");
		}

		return p;
	}
	
	public static PdfPTable getHeaderForLandPropertyListPdf(Estate e) throws DocumentException {
		PdfPCell cell = new PdfPCell();
		PdfPTable headerTable = new PdfPTable(2);
		headerTable.setWidths(new float[] { (185f / 909f), (612f / 909f) });
		headerTable.setWidthPercentage(100f);
		headerTable.getDefaultCell().setPadding(0);
		headerTable.getDefaultCell().setPaddingBottom(11);
		headerTable.getDefaultCell().setPaddingLeft(0);
		headerTable.getDefaultCell().setBorder(0);
		// headerTable.getDefaultCell().setFixedHeight(120f);
		
		/*String BUILDER_IMG = PropertyEntity.resizeImage(MAIN_LOGO_IMG, 165, 170, PropertyEntity.ResizeOption.Pad, true, true, 250, 300);
		Image i = createImage(BUILDER_IMG);
		if (i != null) {
			headerTable.addCell(i);
		}*/
		
		if (MAIN_LOGO_IMG != null) {
			cell = new PdfPCell();
			
			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			String imgStr = homePath + "/" + MAIN_LOGO_IMG;
			try
			{
				java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
				Image img = CommonUtil.createImage(imgStr);
				img.scaleToFit(140, 65);
				cell = new PdfPCell(img, true);
			}
			catch(Exception ee)
			{
				cell = new PdfPCell(new Paragraph("Problem with the Logo. " + String.valueOf(MAIN_LOGO_IMG), regularFont));
				System.out.println("Exception Occurred. Message is: " + ee.getMessage());
				ee.printStackTrace();
			}		
		}
		else
		{
			cell = new PdfPCell(new Paragraph("Logo is not available.", regularFont));
		}
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setPaddingLeft(10);
		cell.setBorder(0);
		cell.setPaddingTop(10);
		cell.setPaddingBottom(5);
		cell.setPaddingRight(5);
		//cell.setFixedHeight(130f);
		headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		headerTable.addCell(cell);

		
		
		
		/*Image i = null;
		if (e != null) {
			cell = new PdfPCell();
			if (e.estateLogo != null && (i = createImage(e.estateLogo)) != null) {
				cell = new PdfPCell(i, true);
			} else {
				cell = new PdfPCell(new Paragraph(" estate? " + String.valueOf(e.estateLogo), regularFont));
			}
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setPaddingLeft(230);
			cell.setBorder(0);
			cell.setPaddingTop(35);
			cell.setPaddingBottom(30);
			cell.setPaddingRight(5);
			cell.setFixedHeight(120f);
			headerTable.addCell(cell);
		}*/
		
		
		
		if (e.estateLogo != null) {
			cell = new PdfPCell();
			
			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			String imgStr = homePath + "/" + e.estateLogo;
			try
			{
				java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
				Image img = CommonUtil.createImage(imgStr);
				img.scaleToFit(140, 65);
				cell = new PdfPCell(img, true);
			}
			catch(Exception ee)
			{
				cell = new PdfPCell(new Paragraph("Problem with the Estate Logo. " + String.valueOf(e.estateLogo), regularFont));
				System.out.println("Exception Occurred. Message is: " + ee.getMessage());
				ee.printStackTrace();
			}		
		}
		else
		{
			cell = new PdfPCell(new Paragraph("Estate logo is not available", regularFont));
		}
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setPaddingLeft(230);
		cell.setBorder(0);
		cell.setPaddingTop(35);
		cell.setPaddingBottom(30);
		cell.setPaddingRight(5);
		cell.setFixedHeight(120f);
		headerTable.addCell(cell);

		
		
		
		
		

		/*headerTable.getDefaultCell().setFixedHeight(0f);
		headerTable.getDefaultCell().setColspan(2);
		headerTable.getDefaultCell().setPaddingTop(3);
		headerTable.getDefaultCell().setPaddingBottom(9);
		headerTable.getDefaultCell().setPaddingLeft(0);
		headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		headerTable.getDefaultCell().setBackgroundColor(aqua);
		headerTable.addCell(new Paragraph("FIXED PRICE HOUSE AND LAND PACKAGES", headerFont));*/
		
		return headerTable;
	}
	
	
	public static PdfPTable buildPackageTableForLandPropertyListPdf(Package p) throws DocumentException {
		PdfPTable packageRow = new PdfPTable(2);
		packageRow.setWidths(new int[] { 24, 75 });
		packageRow.getDefaultCell().setBorder(0);
		packageRow.getDefaultCell().setPadding(0);
		packageRow.setWidthPercentage(100f);

		Image i = null;
		PdfPCell cell = null;

		String leftImage = PropertyEntity.resizeImage(p.heroImage, 260, 160, PropertyEntity.ResizeOption.Crop, true);
		
		String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
		String imgStr = homePath + "/" + p.heroImage;
		try
		{		
			if (leftImage != null && (i = createImage(leftImage)) != null) {
				java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
				Image img = CommonUtil.createImage(imgStr);
				img.scaleToFit(260, 160);
				cell = new PdfPCell(img, true);
			} else {
				cell = new PdfPCell(new Paragraph("Land Image is not available", regularFont));
			}
			
		}
		catch(Exception ee)
		{
			cell = new PdfPCell(new Paragraph("Problem with the Land Logo. " + String.valueOf(p.heroImage) + ee.getMessage(), regularFont));
			System.out.println("Exception Occurred. Message is: " + ee.getMessage());
			ee.printStackTrace();
		}		
		

		

		cell.setBorder(0);
		cell.setVerticalAlignment(Element.ALIGN_TOP);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setPadding(0);

		packageRow.getDefaultCell().setBorderWidthLeft(0.1f);
		packageRow.getDefaultCell().setBorderWidthRight(0.1f);
		packageRow.addCell(cell);
		packageRow.getDefaultCell().setBorderWidthLeft(0f);
		packageRow.getDefaultCell().setBorderWidthRight(0f);
		packageRow.addCell(buildDetailsTableForLandPropertyListPdf(p));
		return packageRow;
	}
	
	private static PdfPTable buildDetailsTableForLandPropertyListPdf(Package p) throws DocumentException {
		PdfPTable packageTable = new PdfPTable(5);
		PdfPCell cell = new PdfPCell();
		Image i = null;
		packageTable.setWidthPercentage(100f);
		packageTable.setWidths(new int[] { 1, 8, 32, 15, 20 });

		packageTable.getDefaultCell().setPadding(0);
		packageTable.getDefaultCell().setColspan(3);
		packageTable.getDefaultCell().setPaddingLeft(15f);

		packageTable.getDefaultCell().setBorder(0);
		packageTable.getDefaultCell().setPaddingBottom(2f);
		packageTable.getDefaultCell().setFixedHeight(22.5f);
		packageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.getDefaultCell().setBackgroundColor(pkgHeaderGrey);

		packageTable.addCell(new Paragraph("LOT " + p.name.toUpperCase(), packageHeaderFont1));
		packageTable.getDefaultCell().setPaddingLeft(60f);
		
		packageTable.getDefaultCell().setColspan(2);
		packageTable.addCell(new Paragraph(String.valueOf(p.landPrice), priceFont));

		
		packageTable.getDefaultCell().setFixedHeight(0f);
		packageTable.getDefaultCell().setColspan(1);
		packageTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		packageTable.getDefaultCell().setPadding(1);
		packageTable.getDefaultCell().setPaddingTop(2);
		packageTable.getDefaultCell().setPaddingLeft(3);
		packageTable.getDefaultCell().setPaddingRight(3);
		packageTable.getDefaultCell().setPaddingBottom(1);
		packageTable.getDefaultCell().setNoWrap(true);

		// Package Details
		// First Row
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("STREET: ", detailsFontBlue));

		String streetAddr = p.street;
		if (!StringUtil.isBlankOrEmpty(p.street2))
		{
			streetAddr += " " + p.street2;
		}	
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(streetAddr, detailsFontBlack));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("LOT SIZE: ", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		Paragraph lotSize = new Paragraph("", detailsFontBlack);
		if (p.lotSize > 0) {
			lotSize.add(getArea("", "", String.valueOf(new Double(p.lotSize).intValue())));
		}
		packageTable.addCell(lotSize);


		// Second Row
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("SUBURB: ", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		//packageTable.getDefaultCell().setColspan(5);
		packageTable.addCell(new Paragraph(p.suburb, detailsFontBlack));


		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("LOT WIDTH: ", detailsFontBlue));
		
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		Paragraph lotWidth = new Paragraph("", detailsFontBlack);
		if (p.lotWidth > 0) {
			lotWidth.add(getArea("", "", String.valueOf(new Double(p.lotWidth).intValue())));
		}
		packageTable.addCell(lotWidth);
		
		
		// Third Row
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);

		packageTable.addCell(new Paragraph("ESTATE", detailsFontBlue));
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(p.estateName, detailsFontBlack));


		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("LOT DEPTH: ", detailsFontBlue));
		
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		Paragraph lotDepth = new Paragraph("", detailsFontBlack);
		if (p.lotDepth > 0) {
			lotDepth.add(getArea("", "", String.valueOf(new Double(p.lotDepth).intValue())));
		}
		packageTable.addCell(lotDepth);
		
		// Last Empty Row
		packageTable.getDefaultCell().setColspan(5);
		packageTable.addCell(new Paragraph("", detailsFontBlack));

		return packageTable;
	}

	public static Paragraph getAreaWithUnit(String prefix, String suffix, String s, Font font, Font smallFont, String unit, String unitPower) 
	{
		Paragraph p = new Paragraph();
		p.add(new Chunk(prefix, font));
		p.add(new Chunk(s, font));
		p.add(new Chunk(unit, font));
		Chunk c = new Chunk(unitPower, smallFont);
		c.setTextRise(4.0f);
		p.add(c);
		p.add(new Chunk(suffix, font));
		return (p);
	}
	
	public static NumberFormat currency = NumberFormat.getCurrencyInstance();
	public static String getString(Double input) {
		if(input != null) {			 
			return currency.format(input);
		}
		return "";
	}
	
	/**
	 * Use this function to initiate default cell settings, that will be
	 * repeated for every PDFTable
	 * 
	 * @author TP
	 * @param pdfTable
	 * @return
	 */
	public static PdfPTable initMultiLandListbyEstateDefaultCell(PdfPTable pdfTable) {
		pdfTable.getDefaultCell().setBorder(0);
		pdfTable.getDefaultCell().setBorderWidth(0);
		pdfTable.getDefaultCell().setBorderWidthTop(0);
		pdfTable.getDefaultCell().setBorderWidthBottom(0);
		pdfTable.getDefaultCell().setBorderWidthRight(0);
		pdfTable.getDefaultCell().setBorderWidthLeft(0);
		pdfTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		pdfTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		pdfTable.getDefaultCell().setPadding(0);
		pdfTable.setWidthPercentage(100);
		return pdfTable;
	}
	
	public static double formatArea(String areaValue,String format) {
		String formatedArea = null;
		if(StringUtils.isNotBlank(areaValue)) {
			double areaTmp =  Double.parseDouble(areaValue);
			DecimalFormat df = new DecimalFormat(format);
			df.setRoundingMode(RoundingMode.DOWN);
			formatedArea = df.format(areaTmp);
			return new Double(formatedArea).doubleValue();
		}
		return 0.0;
	}
	
	public static String getIntPrice(double num) {
		DecimalFormat df = new DecimalFormat("$###,###,###,###,###");
		String number = df.format(num).toString();
		return number;
	}

	// *******************return drip array****************************
	/**
	 * pass the drip content and token to get a list
	 * 
	 * @param drip
	 * @param token
	 * @return
	 */
	public static ArrayList<String> getDripArray(String drip, String token, int maxItemList) {
		ArrayList<String> al = new ArrayList<String>();
		if(StringUtils.isNotBlank(drip)) {
			StringTokenizer str = new StringTokenizer(drip, token);
			int count = 0;
			while (str.hasMoreElements() && count < maxItemList) {
				String inclusionStr = str.nextElement().toString();
				al.add(inclusionStr);
				count++;
			}
		}
		return al;
	}
	
	@Deprecated
	/**
	 * Use RunwayUtil.updateImage instead of this one
	 * 
	 * @param imagePath
	 * @return
	 */
	public static String updateImage(String imagePath) {
		if (StringUtils.isNotBlank(imagePath) && !imagePath.startsWith("/")) {
			imagePath = "/" + imagePath;
		}
		return imagePath;
	}
	
}