package com.sok.runway.crm.interfaces;

import com.sok.runway.security.User;
import java.sql.Connection;

public interface Linkable {
   
   public boolean load(String id);
   
   public String getField(String fieldName);
   
   public String getPrimaryKey();
   
   public String getPrimaryKeyName();
   
   public User getCurrentUser();
   
   public void setCurrentUser(String userid);
   
   public void setCurrentUser(User user);
   
   public Connection getConnection();
   
   public boolean isLoaded();
   
   public boolean isPersisted(); 
   
   public String getLinkableTitle();
   
   public String getEntityName();
}