package com.sok.runway.crm.cms.properties.excel;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.runway.crm.interfaces.AsyncProcessStatus;

public abstract class AbstractExcelTemplate implements AsyncProcessStatus {

	private static final Logger logger = LoggerFactory.getLogger(AbstractExcelTemplate.class);

	public void setStatus(HttpServletRequest request, String status) {
		setStatus(request, status, -1);
	}

	public void setStatus(HttpServletRequest request, String status, double fraction) {
		logger.debug("setStatus(request, {})", status);
		String statusKey = (String) request.getAttribute("-statusKey");
		if (statusKey == null) {
			logger.debug("no status key from request");
			String fileKey = (String) request.getSession().getAttribute("-statusKey");
			if (fileKey != null) {
				logger.debug("status key from session {} ", fileKey);
				statusKey = fileKey;
			} else {
				logger.error("no status key");
				return;
			}
			request.setAttribute("-statusKey", statusKey);
		}
		logger.debug("using status key {}", statusKey);
		request.getSession().setAttribute(statusKey, makeStatusDiv(status, fraction));
	}

	private String makeStatusDiv(String status, double fraction) {
		if (fraction == -1) {
			return status;
		}
		return new StringBuilder().append(status).append("<div style=\"width:100%; display:block; height: 15px\"><div style=\"width: ").append(fraction * 100).append("%; background-color: red\">&nbsp;</div></div>").toString();
	}

	public void generateExcel(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HSSFWorkbook workbook = null;
		setStatus(request, "Loading the generator", 0.01d);

		java.io.OutputStream os = response.getOutputStream();
		response.setContentType("application/vnd.ms-excel");
		try {

			workbook = new HSSFWorkbook();
			renderExcel(request, response, workbook);
			logger.debug("after renderPDF");
			workbook.write(os);
			os.flush();

		} catch (Exception e) {
			logger.error("Error producing PDF", e);
			try {
				ActionBean.writeStackTrace(e, System.out);
			} catch (Exception e3) {
				System.out.println(ActionBean.writeStackTraceToString(e3));
				System.out.println(ActionBean.writeStackTraceToString(e));
			}

			try {
				if (workbook == null) {
					workbook = new HSSFWorkbook();
				}
				logger.debug("adding error paragraph");

			} catch (Exception ex) {
				System.out.println(ActionBean.writeStackTraceToString(ex));
			}
		} finally {
			if (os != null)
				os.close();
		}
		setStatus(request, "Complete", 1);
	}

	/**
	 * To be overridden for default page event behavior
	 */
	public abstract String renderExcel(HttpServletRequest request, HttpServletResponse response, HSSFWorkbook workbook) throws IOException;

}