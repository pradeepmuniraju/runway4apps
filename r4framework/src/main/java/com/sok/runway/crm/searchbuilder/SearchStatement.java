package com.sok.runway.crm.searchbuilder;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.sok.framework.DisplaySpec;
import com.sok.framework.GenRow;
import com.sok.framework.SpecManager;
import com.sok.runway.model.AbstractModel;

public class SearchStatement extends AbstractModel {
    private String id;
    private String name;
    private String description;
    private SearchType searchType;
    private String createdBy;
    private String createdDate;
    private String modifiedBy;
    private String modifiedDate;
    
    private List<SearchSet> searchSets;
    private DisplaySpec displaySpec;
   
    /**
     * <p>Create a blank Search Statement model.</p>
     */
    public SearchStatement() {
       initialise();
    }
   
    private void initialise() {
        this.id = "";
        this.name = "";
        this.description = "";
        this.searchType = null;
        this.createdBy = "";
        this.createdDate = "";
        this.modifiedBy = "";
        this.modifiedDate = "";        
        this.searchSets = new LinkedList<SearchSet>();
        this.displaySpec = null;
    }
   
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedDate
     */
    public String getModifiedDate() {
        return modifiedDate;
    }

    /**
     * @param modifiedDate the modifiedDate to set
     */
    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    /**
     * @param displaySpec the displaySpec to set
     */
    public void setDisplaySpec(DisplaySpec displaySpec) {
        this.displaySpec = displaySpec;
    }

    /**
     * 
     * @return the search type
     */
    public SearchType getSearchType() {
       return this.searchType;
    }

    /**
     * 
     * @param searchType the searchType to set
     */
    public void setSearchType(String searchType) {
       this.searchType = SearchType.createSearchType(searchType);      
    }
    
    /**
     * 
     * @return the list of search sets
     */
    public List<SearchSet> getSearchSets() {
        return searchSets;
    }

    /**
     * 
     * @param searchSets the list of search sets to set
     */
    public void setSearchSets(List<SearchSet> searchSets) {
       this.searchSets = searchSets;
    }
    
    /**
     * 
     * @param searchSet the searchSet to set
     */
    public void appendSearchSet(SearchSet searchSet) {
       searchSet.setSearchStatement(this);
       this.searchSets.add(searchSet);
    }

    /**
     * <p>Remove the search set with the specified <code>searchSetNo</code>.</p>
     * 
     * @param searchSetNo 
     */
    public void removeSearchSet(String searchSetNo) {
       SearchSet searchSet = getSearchSet(searchSetNo);
       this.searchSets.remove(searchSet);
    }
   
    /**
     * <p>Activate/deactive the search set with the specified <code>searchSetNo</code>.</p>
     * 
     * @param searchSetNo
     */
    public void toggleSearchSet(String searchSetNo) {
       SearchSet searchSet = this.getSearchSet(searchSetNo);
       if(searchSet != null) {
          searchSet.setActive(!searchSet.isActive());
       }
    }
   
    /**
     * 
     * @param searchSetId
     * @return the searchSet with the specified <code>searchSetId</code>
     */
    public SearchSet getSearchSet(String searchSetId) {
       SearchSet foundSearchSet = null;
      
       if(searchSetId != null) {
          boolean isFound = false;
          Iterator<SearchSet> searchSetIter = this.searchSets.iterator();
         
          while(!isFound && searchSetIter.hasNext()) {
             SearchSet searchSet = searchSetIter.next();
            
             if(searchSetId.equals(searchSet.getId())) {
                foundSearchSet = searchSet;
                isFound = true;
             }
          }
       }
      
       return foundSearchSet;
    }
   
    /**
     * 
     * @return the display spec corresponding to the searchType
     */
    public DisplaySpec getDisplaySpec() {
       if(this.displaySpec == null && this.searchType != null) {
          this.displaySpec = SpecManager.getDisplaySpec(this.searchType.getDisplaySpecName());
       }
      
       return displaySpec;
    }
   
    /**
     * 
     * @return
     */
    public GenRow compileSearch() {
       return null; // TODO: implement
    }
   
    /**
     * <p>Clear all search sets and their search criterias.</p>
     */
    public void clear() {
       this.initialise();
    }
    
    public String toJson() {
        // TODO: complete
        StringBuilder json = new StringBuilder();
        json.append("{ id: ").append(enquote(id)).append("}");
        return json.toString();
     }    
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
   
        SearchStatement searchStatement = (SearchStatement) o;
   
        if (id != null ? !id.equals(searchStatement.id) : searchStatement.id != null) return false;
   
        return true;
    }
   
    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
   
    @Override
    public String toString() {
        // TODO: complete
        return "SearchStatement{" +
                "id='" + id + '\'' +
                '}';
    }
}
