package com.sok.runway.crm.cms.properties;

import java.util.Date;
import java.util.HashSet;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.TableSpec;

public class CostUpdateWorker extends Thread {
	private String				productID = "";
	private String 				userID = "";
	public int					max = 0;
	public int					current = 0;
	public boolean				isCompleat = false;
	public double				estimatedRemaining = 0;
	private Date				date = new Date();
	private HashSet<String>		done = new HashSet<String>(); 					

	public CostUpdateWorker(String productID, String userID) {
		this.productID = productID;
		this.userID = userID;

		if (productID != null && productID.length() > 0) this.start();
	}

	public void run() {
		this.setName("Update Task");
		applyToAll("CopiedFromProductID", productID, userID);
		applyToAll("PackageCostProductID", productID, userID);
		isCompleat = true;

	}

	public void applyToAll(String type, String productID, String userid) {
		GenRow pc = new GenRow();
		pc.setViewSpec("ProductProductView");
		GenRow pcp = new GenRow();
		pcp.setTableSpec("Products");
		pcp.setParameter("-select0", "ProductID");
		pcp.setParameter("-select1", "ProductType");
		pcp.setParameter(type,productID);
		pcp.doAction("search");

		pcp.getResults(true);

		GenRow pcc = new GenRow();
		pcc.setTableSpec("PackageCostings");

		StringBuilder sb = new StringBuilder();

		while (pcp.getNext()) {
			if (pcp.getString("ProductID").equals(productID)) continue;
			if (!"Package Cost".endsWith(pcp.getString("ProductType"))) continue;
			if (done.contains(pcp.getString("ProductID"))) continue;
			if (sb.length() > 0) sb.append("+");
			sb.append(pcp.getString("ProductID"));
			done.add(pcp.getString("ProductID"));
		}

		pcp.close();

		if (sb.length() == 0) return;

		pc.setParameter("ProductProductsProduct.ProductType","!House and Land");
		pc.setParameter("LinkedProductID", sb.toString());
		pc.doAction("search");

		pc.getResults(true);

		max += pc.getSize();

		while (pc.getNext()) {
			//pc.getResults(true);

			//System.out.println("Update Worker:\n" + pc.getStatement());

			update("Products", "ProductID", productID, pc.getData("LinkedProductID"),userid);

			pcc.clear();
			pcc.setParameter("-select","*");
			pcc.setParameter("ParentProductID",pc.getData("LinkedProductID"));
			pcc.doAction("selectfirst");
			//System.out.println("PCC " + productID + "  " + pcc.getStatement());
			if (pcc.isSuccessful() || pcc.getString("CostingID").length() > 0 && !pc.getData("LinkedProductID").equals(productID)) {
				pcc.clear();
				pcc.setParameter("ON-ParentProductID",pc.getData("LinkedProductID"));
				pcc.doAction("deleteall");

				update("PackageCostings", "ParentProductID", productID, pc.getData("LinkedProductID"),userid);
			}
			++current;
			Date d = new Date();
			estimatedRemaining = d.getTime() - date.getTime();
			estimatedRemaining = ((double) estimatedRemaining / current) * (max - current) ;

			applyToAll(type, pc.getData("LinkedProductID"), userid);

		}
		pc.close();
	}

	public void update(String tablespecname, String productidfieldname,
			String fromproductid, String toproductid, String userid) {

		if (fromproductid == null || fromproductid.length() == 0 || toproductid == null || toproductid.length() == 0) return;

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);

		//row.setRequest(request);
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow check = new GenRow();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			check.clear();
			check.setTableSpec(tablespecname);
			check.setParameter("-select1", "*");
			check.setParameter(productidfieldname, toproductid);
			check.doAction("selectfirst");

			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.putAllDataAsParameters(row, true);
			if ("Products".equals(tablespecname)) {
				clone.setParameter("ProductType","Package Cost");
			}
			// make sure it is not already there
			if (!check.isSuccessful()
					|| check.getString(row.getTableSpec().getPrimaryKeyName())
					.length() == 0) {
				// if it is a Package Cost then clone that and use the clone
				clone.setParameter("CopiedFromProductID", fromproductid);
				clone.setParameter("PackageCostProductID", fromproductid);
				clone.setParameter(productidfieldname, toproductid);
				clone.createNewID();
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				try {
					clone.doAction("insert");
				} catch (Exception e) {
					System.out.println("Insert Failed " + fromproductid);
				}
			} else {
				clone.remove("CopiedFromProductID");
				clone.remove("PackageCostProductID");
				clone.setParameter(productidfieldname, toproductid);
				clone.setParameter(row.getTableSpec().getPrimaryKeyName(),
						check.getString(row.getTableSpec().getPrimaryKeyName()));
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("ModifiedBy", userid);
				try {
					clone.doAction("update");
				} catch (Exception e) {
					System.out.println("Update Failed " + check.getString(row.getTableSpec().getPrimaryKeyName()));
				}
			}
		}
		row.close();
	}

}
