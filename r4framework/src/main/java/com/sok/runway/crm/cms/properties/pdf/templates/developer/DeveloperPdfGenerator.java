package com.sok.runway.crm.cms.properties.pdf.templates.developer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.DeveloperPDFUtil;
import com.sok.runway.offline.ThreadManager;

public class DeveloperPdfGenerator extends AbstractPDFTemplate {

	private static final Logger logger = LoggerFactory.getLogger(DeveloperPdfGenerator.class);

	public static final String DEV_SPECIFIC_PACKAGE = "com.sok.dev.<$$$>.pdf.templates.";

	private static HashMap<String, String> developerMap = new HashMap<String, String>();

	public static ClassLoader classLoader = null;

	String className = null;

	double currentPercentage = 0f;

	public static boolean loadedJars = false;

	static {
		loadDeveloperJars();
	}
	
	public DeveloperPdfGenerator() {
		super();
	}

	public DeveloperPdfGenerator(String classname) {
		className = classname;
	}

	public static void loadDeveloperJars() {
		if (!loadedJars) {
			try {
				File[] jars = DeveloperPDFUtil.getJarFileNames();

				URL[] urls = new URL[jars.length];

				for (int i = 0; i < jars.length; i++) {
					if (jars[i].getName().endsWith(".jar")) {

						urls[i] = jars[i].toURI().toURL();

					}
				}

				classLoader = new URLClassLoader(urls, Thread.currentThread().getContextClassLoader());

				loadedJars = true;

			} catch (MalformedURLException e) {
				logger.error("Error Loading Developer Jars" + e);
				e.printStackTrace();
			} catch (Exception e) {
				logger.error("Error Loading Developer Jars" + e);
				e.printStackTrace();
			}
		}

	}

	public static Class loadClass(String className) throws Exception {
		return classLoader.loadClass(className);
	}

	// @Override
	public void renderPDF(final HttpServletRequest request, final HttpServletResponse response, final Document document, final PdfWriter writer) throws IOException, DocumentException {
		document.open();

		List<PropertyEntity> properties = (List<PropertyEntity>) request.getAttribute("entityList");
		if (properties == null) {
			setStatus(request, "Loading Records..", 0.02d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			properties = PropertyFactory.getPropertyEntities(request, this);
		}

		setStatus(request, "Loading Records..", currentPercentage);
		double num = new Double(properties.size()), record = 1;

		int size = properties.size();
		List<Future<PdfReader>> readerList = new ArrayList<Future<PdfReader>>(size);

		for (final PropertyEntity pe : properties) {

			if (record % 5 == 0) {
				currentPercentage += record / num;
				setStatus(request, "Loading package information", currentPercentage);
			}
			record++;

			try {
				readerList.add(ThreadManager.startCallable(new Callable<PdfReader>() {
					public PdfReader call() throws IOException, DocumentException {
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						Document document = new com.itextpdf.text.Document();						
						document.setMarginMirroring(true);
						PdfWriter writer = PdfWriter.getInstance(document, baos);
						try {
							invokeDeveloperPdfTemplate(request, document, pe, baos);
						} catch (Exception e) {
							logger.error("error in invoking developer pdf - getting single package", e);
							e.printStackTrace();
						}
						return new PdfReader(baos.toByteArray());
					}

				}));
				/*
				 * } catch (InterruptedException e) { logger.error("PDF Generation Interupted : {}",
				 * e.getMessage()); } catch (ExecutionException e) {
				 * logger.error("error in pdf execution - getting single package", e); }
				 */

			} catch (Exception e) {
				logger.error("error in pdf execution - getting single package", e);
			}
		}

		currentPercentage = 0.6d;
		setStatus(request, "Generating PDF", currentPercentage);

		logger.debug("Rendering pages");
		PdfImportedPage importedPage;
		PdfContentByte cb = writer.getDirectContent();

		for (Future<PdfReader> freader : readerList) {
			try {
				setStatus(request, "Rendering Pages", 0.6 + ((record++ / num) * 0.35));
				PdfReader reader = freader.get();
				int pages = reader.getNumberOfPages();
				for (int i = 1; i <= pages; i++) {

					document.newPage();

					importedPage = writer.getImportedPage(reader, i);
					cb.addTemplate(importedPage, 0, 0);
				}
			} catch (InterruptedException e) {
				logger.error("PDF Generation Interupted : {}", e.getMessage());
			} catch (ExecutionException e) {
				logger.error("error in pdf execution", e);
			}
		}
	}

	private void invokeDeveloperPdfTemplate(HttpServletRequest request, Document document, PropertyEntity property, ByteArrayOutputStream baos) throws Exception {

		if (property == null) {
			throw new RuntimeException("Product was not found");
		}

		if (property.isPropertyType(PropertyType.HouseLandPackage)) {
			String jarName = getJarName(request, property.getString("DeveloperProductID"));

			if (StringUtils.isNotBlank(jarName)) {
				try {
					String fqClassName = DEV_SPECIFIC_PACKAGE.replace("<$$$>", getPackageName(jarName)) + className;

					AbstractDeveloperPdf developerPdf = (AbstractDeveloperPdf) classLoader.loadClass(fqClassName).newInstance();
					developerPdf.setMargins(document);
					
					document.open();
					developerPdf.renderPdf(request, document, property);
					document.close();		
					
					// Overlay for Prestructured PDF's
					developerPdf.setOverlay(request, baos, property);
					
				} catch (ClassNotFoundException e) {
					logMessage("Jar not available. Contact Support", property.getString("ProductID"));
					e.printStackTrace();

				} catch (Exception e) {
					logMessage("Technical error generating pdf for " + property.getString("Name") + ". Contact Support.", property.getString("ProductID"));
					e.printStackTrace();
				}
			}
		} else {
			throw new RuntimeException("Property [" + property.getPropertyType() + "] was not a house and land package");
		}
	}

	private void logMessage(String errorMessage, String property) {
		logger.error("Skipping developer pdf generation for {}, reason ()", property, errorMessage);

	}

	private void renderErrorPage(Document document, String errorMessage) throws DocumentException {
		renderErrorPage(document, errorMessage, null);
	}

	/**
	 * @param request
	 * @param document
	 * @param property
	 * @throws DocumentException
	 */
	private void renderErrorPage(Document document, String errorMessage, Exception e) throws DocumentException {

		PdfPTable pageTable = new PdfPTable(1);
		pageTable.setWidthPercentage(100f);
		pageTable.getDefaultCell().setPadding(10);
		pageTable.getDefaultCell().setBorder(1);

		pageTable.addCell(new Paragraph("Sorry, there was an error generating Developer PDF. More details below."));

		pageTable.addCell(new Paragraph(errorMessage));

		if (e != null) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			pageTable.addCell(new Paragraph(sw.toString()));
		}

		document.add(pageTable);
	}

	public static String getJarName(HttpServletRequest request, String developerID) {
		String jarName = null;

		if (StringUtils.isBlank(developerID)) {
			// System.out.println(classAndMethodName +
			// "Developer is not attached to any Property. Rectify and retry. developerID is: " +
			// developerID);
			logger.trace("Developer is not attached to any Property. Rectify and retry. developerID is: " + developerID);

			return null;
		}

		// developer is attached, check if it has been loaded before
		jarName = developerMap.get(developerID);

		// try to load from db
		if (StringUtils.isBlank(jarName)) {

			GenRow developer = new GenRow();
			developer.setRequest(request);
			developer.setTableSpec("Products");
			developer.setParameter("ProductID", developerID);
			developer.setParameter("-select0", "Project");
			developer.setParameter("-select1", "Name");
			developer.sortBy("Name", 0);
			developer.doAction("selectfirst");

			if (developer.isSuccessful())
				jarName = developer.getString("Project");

			if (StringUtils.isBlank(jarName)) {
				// System.out.println(classAndMethodName + "Developer " +
				// developer.getString("Name") + " is not configured. Please contact support.");
				logger.trace("Developer " + developer.getString("Name") + " is not configured. Please contact support.");
				return null;
			} else {
				developerMap.put(developerID, jarName);
			}
			developer.close();
		}
		return jarName;
	}

	/**
	 * Jar names of format developer-balcon-dev.jar
	 * 
	 * @param jarName
	 * @return
	 */
	private static String getPackageName(String jarName) {
		if (StringUtils.isNotBlank(jarName)) {
			String[] names = jarName.split("-");

			if (names.length > 1)
				return names[1];
		}

		return null;
	}

	@Override
	// Unused
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {

	}
	
	
	public void setMargins(Document document) {
		document.setMargins(0, 0, 0, 0);
		
	}

}
