package com.sok.runway.crm.cms.properties;

import java.util.Collections;
import java.util.List;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

public class Estate extends PropertyEntity {

	final PropertyType propertyType = PropertyType.Estate;
	static final String VIEWSPEC = "ProductView";
	private GenRow display = null;
	public Estate(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter("ProductType",getProductType());
	}
	
	public Estate(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	
	public Estate(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}

	public Estate load(String productID) { 
		return (Estate)super.load(productID);
	}
	
	@Override
	public Object get(String key) { 
		if("estatedisplay".equals(key)) return getDisplay();  
		return super.get(key);
	}
	
	@Override
	public void clear() { 
		display = null;
		super.clear();
	}
	
	public List<Stage> getStages() {
		return Collections.emptyList();
	}
	
	public GenRow getDisplay() { 
		if(display == null && isSuccessful()) { 
			display = getDisplayEntity(getConnection(), getData(PRODUCTID));
		}
		return display;
	}
	
	protected GenRow getDisplayEntity(Connection con, String productid) {
		GenRow row = new GenRow();
		row.setConnection(con);
		row.setViewSpec("DisplayEntityView");
		row.setParameter("EstateProductID", productid); //EstateProductID is in related field in view spec
		row.doAction("selectfirst");

		GenRow parent = new GenRow();
		if (row.isSuccessful()) {
			row.setConnection(con);
			parent.setViewSpec("DisplayEntityView");
			parent.setParameter("ParentDisplayEntityID", row.getData("DisplayEntityID"));
			parent.doAction("selectfirst");
		} 
		row.put("parentdisplay", parent);
		return (row);
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
	
	@Override
	public String[] getDripProductIDs() {
		return new String[]{getString(PRODUCTID)};
	}
	
	public String getLocationID() {
		if(isSet("LocationID")) {
			return getString("LocationID");
		}
		return null;
	}
}
