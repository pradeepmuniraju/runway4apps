package com.sok.runway.crm.searchbuilder.dao;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.searchbuilder.SearchStatement;

public class SearchStatementDAO {
    HttpServletRequest request;

    public SearchStatementDAO(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * <p>Create a new search statement row from the specified 
     * <code>searchStatement</code> model. The primary key of the newly 
     * created search statement row will be set in the model and 
     * returned from this method.</p> 
     * 
     * @param searchStatement the search statement model
     * @return the ID of the newly created searchStatement row
     */
    public String create(SearchStatement searchStatement) {
        GenRow searchStatementRow = createRow();
        if(searchStatement.getId() == null || searchStatement.getId().length() == 0) {
            searchStatementRow.createNewID();
            searchStatement.setId(searchStatementRow.getParameter("SearchStatementID"));
        } else {
            searchStatementRow.setParameter("SearchSetID", searchStatement.getId());
        }        
        populateRow(searchStatementRow, searchStatement);
        searchStatementRow.doAction(GenerationKeys.INSERT);
        searchStatementRow.close();
        
        return searchStatement.getId();
    }
    
    /**
     * <p>Updates the search statement row for the specified 
     * <code>searchStatement</code> model.</p>
     * 
     * @param searchStatement the <code>searchStatement</code> model
     */
    public void save(SearchStatement searchStatement) {
        GenRow searchStatementRow = createRow();
        searchStatementRow.setParameter("SearchSetID", searchStatement.getId());
        populateRow(searchStatementRow, searchStatement);        
        searchStatementRow.doAction(GenerationKeys.UPDATE);
        searchStatementRow.close();
    }
    
    /**
     * <p>Find the <code>SearchStatement</code> model for the 
     * specified <code>id</code>.</p>
     * 
     * @param id of the <code>SearchStatement</code> model to find
     * @return the <code>SearchStatement</code> model for the 
     * specified <code>id</code>
     */
    public SearchStatement findById(String id) {
        SearchStatement searchStatement = new SearchStatement();
        
        GenRow searchStatementRow = createRow();
        searchStatementRow.setParameter("SearchStatementID", id);
        searchStatementRow.setParameter("-select2", "Name");
        searchStatementRow.setParameter("-select3", "Description");
        searchStatementRow.setParameter("-select4", "SearchType");
        searchStatementRow.setParameter("-select5", "CreatedBy");
        searchStatementRow.setParameter("-select6", "CreatedDate");
        searchStatementRow.setParameter("-select7", "ModifiedBy");
        searchStatementRow.setParameter("-select8", "ModifiedDate");
        searchStatementRow.setAction(GenerationKeys.SEARCH);
        searchStatementRow.getResults();
        
        if(searchStatementRow.getNext()) {
            populateSearchStatement(searchStatement, searchStatementRow);
        }
        
        searchStatementRow.close();
        
        return searchStatement;
    }
   
    /**
     * 
     * @return a GenRow instance for the <code>SearchStatements</code> 
     * table spec
     */
    private GenRow createRow() {
        final GenRow searchStatementRow = new GenRow();
        searchStatementRow.setRequest(this.request);
        searchStatementRow.setTableSpec("SearchStatements");
        
        return searchStatementRow;
    }
    
    /**
     * <p>Populate row fields in <code>searchStatementRow</code> from the specified 
     * <code>searchStatement</code> model.</p>
     * 
     * @param searchStatementRow
     * @param searchStatement
     */
    private void populateRow(GenRow searchStatementRow, SearchStatement searchStatement) {
        searchStatementRow.setParameter("Name", searchStatement.getName());
        searchStatementRow.setParameter("Description", searchStatement.getDescription());
        searchStatementRow.setParameter("SearchType", searchStatement.getSearchType().getName());
        searchStatementRow.setParameter("CreatedBy", searchStatement.getCreatedBy());
        searchStatementRow.setParameter("CreatedDate", searchStatement.getCreatedDate());
        searchStatementRow.setParameter("ModifiedBy", searchStatement.getModifiedBy());
        searchStatementRow.setParameter("ModifiedDate", searchStatement.getModifiedDate());
    }
    
    /**
     * <p>Populate the fields in the <code>searchStatement</code> model from the specified 
     * <code>searchStatementRow</code>.</p>
     * 
     * @param searchStatement
     * @param searchStatementRow
     */
    private void populateSearchStatement(SearchStatement searchStatement, GenRow searchStatementRow) {
        searchStatement.setId(searchStatementRow.getString("SearchStatementID"));
        searchStatement.setName(searchStatementRow.getString("Name"));
        searchStatement.setDescription(searchStatementRow.getString("Description"));
        searchStatement.setSearchType(searchStatementRow.getString("SearchType"));
        searchStatement.setCreatedBy(searchStatementRow.getString("CreatedBy"));
        searchStatement.setCreatedDate(searchStatementRow.getString("CreatedDate"));
        searchStatement.setModifiedBy(searchStatementRow.getString("ModifiedBy"));
        searchStatement.setModifiedDate(searchStatementRow.getString("ModifiedDate"));
    }
}
