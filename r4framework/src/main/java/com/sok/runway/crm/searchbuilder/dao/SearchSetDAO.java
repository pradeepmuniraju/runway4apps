package com.sok.runway.crm.searchbuilder.dao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.searchbuilder.SearchSet;

public class SearchSetDAO {
    HttpServletRequest request;

    public SearchSetDAO(HttpServletRequest request) {
        this.request = request;
    }
    
    /**
     * <p>Create a new search set row from the specified 
     * <code>searchSet</code> model. The primary key of the newly 
     * created search set row will be set in the model and 
     * returned from this method.</p> 
     * 
     * @param searchSet the search set model
     * @return the ID of the newly created searchSet row
     */
    public String create(SearchSet searchSet) {
        GenRow searchSetRow = createRow();
        if(searchSet.getId() == null || searchSet.getId().length() == 0) {
            searchSetRow.createNewID();
            searchSet.setId(searchSetRow.getParameter("SearchSetID"));
        } else {
            searchSetRow.setParameter("SearchSetID", searchSet.getId());
        }
        populateRow(searchSetRow, searchSet);
        searchSetRow.doAction(GenerationKeys.INSERT);
        
        return searchSet.getId();
    }
    
    /**
     * <p>Loads the <code>SearchSet</code> model by the specified <code>id</code>.
     * Note that invoking this method <b>will not</b> create an instance of 
     * the parent <code>SearchStatement</code>, nor will it create instances of 
     * the <code>SearchCriteria</code> children.</p>
     * 
     * @param id of the <code>SearchSet</code> row
     * @return the <code>SearchSet</code> model
     */
    public SearchSet findById(String id) {
        SearchSet searchSet = new SearchSet();
        
        GenRow searchSetRow = createRow();
        searchSetRow.setParameter("SearchSetID", id);
        searchSetRow.setParameter("-select1", "SearchStatementID");
        searchSetRow.setParameter("-select2", "GroupID");
        searchSetRow.setParameter("-select3", "StatusID");
        searchSetRow.setParameter("-select4", "RepUserID");
        searchSetRow.setParameter("-select5", "SortOrder");
        searchSetRow.setParameter("-select6", "Active");
        searchSetRow.setParameter("-select7", "Operator");
        searchSetRow.setAction(GenerationKeys.SEARCH);
        searchSetRow.getResults();
        
        if(searchSetRow.getNext()) {
            populateSearchSet(searchSet, searchSetRow);
        }
        
        searchSetRow.close();
        
        return searchSet;
    }
    
    /**
     * <p>Loads all the <code>SearchSet</code> models by the specified <code>id</code>.
     * Note that invoking this method <b>will not</b> create an instance of 
     * the parent <code>SearchStatement</code>, nor will it create instances of 
     * the <code>SearchCriteria</code> children.</p>
     * 
     * @param id of the <code>SearchSet's</code> parent <code>SearchStatement</code> row
     * @return the list of <code>SearchSet</code> models
     */    
    public List<SearchSet> findAllBySearchStatementId(String searchStatementId) {
        List<SearchSet> searchSets = new ArrayList<SearchSet>();
        
        GenRow searchSetRow = createRow();
        searchSetRow.setParameter("SearchStatementID", searchStatementId);
        searchSetRow.setParameter("-select1", "SearchSetID");
        searchSetRow.setParameter("-select2", "GroupID");
        searchSetRow.setParameter("-select3", "StatusID");
        searchSetRow.setParameter("-select4", "RepUserID");
        searchSetRow.setParameter("-select5", "SortOrder");
        searchSetRow.setParameter("-select6", "Active");
        searchSetRow.setParameter("-select7", "Operator");
        searchSetRow.setAction(GenerationKeys.SEARCH);
        searchSetRow.getResults(true);
        
        populateSearchSets(searchSets, searchSetRow);
        
        searchSetRow.close();
        
        return searchSets;
    }
    
    /**
     * <p>Updates the search set row for the specified 
     * <code>searchSet</code> model.</p>
     * 
     * @param searchSet the <code>searchSet</code> model
     */
    public void save(SearchSet searchSet) {
        GenRow searchSetRow = createRow();
        searchSetRow.setParameter("SearchSetID", searchSet.getId());
        populateRow(searchSetRow, searchSet);        
        searchSetRow.doAction(GenerationKeys.UPDATE);
        searchSetRow.close();
    }
    
    /**
     * 
     * @return a GenRow instance for the <code>SearchSets</code> 
     * table spec
     */
    private GenRow createRow() {
        final GenRow searchSetRow = new GenRow();
        searchSetRow.setRequest(this.request);
        searchSetRow.setTableSpec("SearchSets");
        
        return searchSetRow;
    }
    
    /**
     * <p>Populate row fields in <code>searchSetRow</code> from the specified 
     * <code>searchSet</code> model.</p>
     * 
     * @param searchSetRow
     * @param searchSet
     */
    private void populateRow(GenRow searchSetRow, SearchSet searchSet) {
        searchSetRow.setParameter("SearchStatementID", searchSet.getSearchStatementId());
        searchSetRow.setParameter("GroupID", searchSet.getGroupId());
        searchSetRow.setParameter("StatusID", searchSet.getStatusId());
        searchSetRow.setParameter("RepUserID", searchSet.getRepUserId());
        searchSetRow.setParameter("SortOrder", searchSet.getSortOrder());
        searchSetRow.setParameter("Active", String.valueOf(searchSet.isActive()));
        searchSetRow.setParameter("Operator", searchSet.getOperator().toString());
    }
    
    /**
     * <p>Populate the fields in the <code>searchSet</code> model from the specified 
     * <code>searchSetRow</code>.</p>
     * 
     * @param searchSet
     * @param searchSetRow
     */    
    private void populateSearchSet(SearchSet searchSet, GenRow searchSetRow) {
        searchSet.setId(searchSetRow.getString("SearchSetID"));
        searchSet.setSearchStatementId(searchSetRow.getString("SearchStatementID"));
        searchSet.setGroupId(searchSetRow.getString("GroupID"));
        searchSet.setStatusId(searchSetRow.getString("StatusID"));
        searchSet.setRepUserId(searchSetRow.getString("RepUserID"));
        searchSet.setSortOrder(searchSetRow.getString("SortOrder"));
        searchSet.setActive(Boolean.valueOf(searchSetRow.getString("Active")));
        searchSet.setOperator(searchSetRow.getString("Operator"));
    }
    
    /**
     * <p>For each <code>searchSet</code> model in the specified <code>searchSets</code> 
     * list, populate its fields from the specified <code>searchSetRow</code>.</p>
     * 
     * @param searchSets
     * @param searchSetRow
     */
    private void populateSearchSets(List<SearchSet> searchSets, GenRow searchSetRow) {
        while(searchSetRow.getNext()) {
            SearchSet searchSet = new SearchSet();
            populateSearchSet(searchSet, searchSetRow);
            searchSets.add(searchSet);
        }
    }
}
