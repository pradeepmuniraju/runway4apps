package com.sok.runway.crm.searchbuilder;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.sok.framework.DisplaySpec;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;

public class SearchSet implements Serializable { 
    public static final String BEAN_ID = "searchSetBean";
    
    private String id;                              // SearchSetID primary key
    private String searchStatementID;               // parent search statement
    private String groupID;
    private String statusID;
    private String repUserID;
    private String sortOrder;
    private boolean isActive;
    private SearchSetOperator operator;
    
    private SearchStatement searchStatement;        // parent search statement
    private List<SearchCriteria> searchCriterias;   // children search criterias
    
    public SearchSet() {
       this(KeyMaker.generate());
    }
    
    public SearchSet(String id) {
        this.id = id;
        this.searchStatementID = "";
        this.groupID = "";
        this.statusID = "";
        this.repUserID = "";
        this.sortOrder = "";
        this.setActive(true);
        this.setOperator(SearchSetOperator.UNION);
                
        this.searchCriterias = new LinkedList<SearchCriteria>();
    }
    
    /**
     * 
     * @return the id
     */
    public String getId() {
        return this.id;
    }
    
    /**
     * 
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    /**
     * @return the searchStatementID
     */
    public String getSearchStatementId() {
        if(this.searchStatement != null) {
            return this.searchStatement.getId();
        }
        
        return this.searchStatementID;
    }

    /**
     * @param searchStatementID the searchStatementID to set
     */
    public void setSearchStatementId(String searchStatementID) {
        this.searchStatementID = searchStatementID;
    }

    /** 
     * 
     * @return the group id
     */
    public String getGroupId() {
       return groupID;
    }
 
    /**
     * 
     * @param groupID the group id to set
     */
    public void setGroupId(String groupID) {
       this.groupID = groupID;
    }
 
    /**
     * 
     * @return the status id
     */
    public String getStatusId() {
       return statusID;
    }
 
    /**
     * 
     * @param statusID the status id to set
     */
    public void setStatusId(String statusID) {
       this.statusID = statusID;
    }
 
    /** 
     * 
     * @return the rep user id
     */
    public String getRepUserId() {
       return repUserID;
    }
 
    /**
     * 
     * @param repUserID the rep user id to set
     */
    public void setRepUserId(String repUserID) {
       this.repUserID = repUserID;
    }
    
    /**
     * @return the sortOrder
     */
    public String getSortOrder() {
        return sortOrder;
    }

    /**
     * @param sortOrder the sortOrder to set
     */
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * 
     * @return is active flag
     */
    public boolean isActive() {
        return isActive;
    }
  
    /**
     * 
     * @param isActive flat to set
     */
    public void setActive(boolean isActive) {
       this.isActive = isActive;
    }
  
    /**
     * 
     * @return the searchOperator
     */
    public SearchSetOperator getOperator() {
       return operator;
    }
  
    /**
     * 
     * @param searchOperator the search set operator to set
     */
    public void setOperator(SearchSetOperator searchOperator) {
       this.operator = searchOperator;
    }
    
    /**
     * 
     * @param operator the search set operator to set
     */
    public void setOperator(String operator) {
        this.operator = SearchSetOperator.createSearchOperator(operator);
    }

    /**
     * 
     * @return the list of searchCriterias
     */
    public List<SearchCriteria> getSearchCriterias() {
       return searchCriterias;
    }
 
    /**
     * 
     * @param searchCriterias the list of searchCriterias to set
     */
    public void setSearchCriterias(List<SearchCriteria> searchCriterias) {
       this.searchCriterias = searchCriterias;
    }
 
    /**
     * 
     * @return the searchStatement
     */
    public SearchStatement getSearchStatement() {
       return searchStatement;
    }
 
    /**
     * 
     * @param searchStatement the searchStatement to set
     */
    public void setSearchStatement(SearchStatement searchStatement) {
       this.searchStatement = searchStatement;
       this.searchStatementID = searchStatement.getId();
    }
 
    /**
     * <p>Create a new search criteria and append it to this search set.</p>
     * 
     * @return the ID for the newly created searchCriteria
     */
    public String appendNewSearchCriteria() {
       SearchCriteria searchCriteria = new SearchCriteria();
       searchCriteria.setSearchSet(this);
       searchCriteria.setFieldType(FieldType.MODULE);
       searchCriteria.setFieldTypeValue(this.getSearchType().getModuleName());
       this.searchCriterias.add(searchCriteria);
       
       return searchCriteria.getId();
    }
    
    /**
     * 
     * @param id of the searchCriteria
     * @return the searchCriteria for the specified <code>id</code>
     */
    public SearchCriteria getSearchCriteria(String id) {
       SearchCriteria foundSearchCriteria = null;
       boolean isFound = false;
       
       Iterator<SearchCriteria> searchCriteriaIter = this.searchCriterias.iterator();
       while(!isFound && searchCriteriaIter.hasNext()) {
          SearchCriteria searchCriteria = searchCriteriaIter.next();
          if(id.equals(searchCriteria.getId())) {
             foundSearchCriteria = searchCriteria;
             isFound = true;
          }
       }
       
       return foundSearchCriteria;
    }
    
    /**
     * 
     * @return the display spec derived from the search statement
     */
    public DisplaySpec getDisplaySpec() {
       return this.searchStatement.getDisplaySpec();
    }
 
    /**
     * 
     * @return the search type derived from the search statement
     */
    public SearchType getSearchType() {
       return this.searchStatement.getSearchType();
    }
    
    public GenRow compileSearchSet() {
       return null;   // TODO: implement
    }
}
