package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.pdf.templates.BuildingApartmentsPdf;
import com.sok.runway.crm.cms.properties.pdf.templates.model.UserInfo;

public class ApartmentHeaderFooter extends PdfPageEventHelper implements PDFConstants 
{
	public static UserInfo salesRepInfo = new UserInfo();
	public static String pdfType = "";
	
	public ApartmentHeaderFooter(javax.servlet.http.HttpServletRequest request, String pType) 
	{
		this.pdfType =  pType;
		salesRepInfo = getSalesRepDetails (request.getParameter("RepUserID"));
	}

	public void onEndPage(PdfWriter writer, com.itextpdf.text.Document document) 
	{
		try
		{
				PdfPTable pageTable = new PdfPTable(1);
				pageTable.getDefaultCell().setBorder(0);
				pageTable.getDefaultCell().setPadding(0);
				pageTable.setWidthPercentage(100f);
				
				PdfPTable footerTable = getFooter();
				pageTable.setTotalWidth(document.right() - document.left());
				pageTable.addCell(footerTable);
				
				// Header Row ==> pageTable.writeSelectedRows(0, -1, 15, 830, writer.getDirectContent());
				//hdrTable.writeSelectedRows(0,-1,document.left(),document.top() + 10, writer.getDirectContent());
				
				if (!PDFUtil.PDF_DETAILS_SINGLE_APARTMENT.equalsIgnoreCase(pdfType))
				{	
					pageTable.writeSelectedRows(0, -1, 15, 120, writer.getDirectContent());
				}
				else
				{
					pageTable.writeSelectedRows(0, -1, 15, 190, writer.getDirectContent());
				}
		}
		catch(Exception ee)
		{
				System.out.println("Exception Occurred in QuestHeaderFooter.java class. Message is: " + ee.getMessage());
				ee.printStackTrace();
		}
	}
	
	public static PdfPTable getFooter() throws DocumentException 
	{
			PdfPTable footerTable = new PdfPTable(1);
			footerTable.setWidths(new float[] { 612f });
			
			footerTable.getDefaultCell().setPadding(0);
			footerTable.getDefaultCell().setPaddingBottom(4);
			footerTable.getDefaultCell().setPaddingTop(4);
			footerTable.getDefaultCell().setPaddingLeft(0);
			footerTable.getDefaultCell().setBorder(0);
			footerTable.getDefaultCell().setBorderWidth(0f);
			footerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			footerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
	

			String tmpDisclaimer = String.format(PDFUtil.DISCLAIMER_TEXT, new java.util.Date());
			footerTable.getDefaultCell().setColspan(3);
			footerTable.addCell(new Paragraph(tmpDisclaimer, BuildingApartmentsPdf.arialNormalPt12Black));
			footerTable.getDefaultCell().setColspan(1);
			footerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			
			// Footer Row 01
			PdfPTable bodyFooterTbl = new PdfPTable(3);
			bodyFooterTbl.getDefaultCell().setBorder(0);
			bodyFooterTbl.getDefaultCell().setBorderWidth(0f);
			bodyFooterTbl.getDefaultCell().setBorderWidthTop(0f);
			bodyFooterTbl.getDefaultCell().setBorderWidthBottom(0f);
			bodyFooterTbl.getDefaultCell().setColspan(1);
			bodyFooterTbl.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

			//if (!PDFUtil.PDF_DETAILS_SINGLE_APARTMENT.equalsIgnoreCase(pdfType))
			//{
					bodyFooterTbl.setWidths(new float[] { 40f, 35f, 35f});
					
					
					
					//////////////// Ended - Row 01
					
					
					
					

					PdfPTable bodyLeftFooterTbl = new PdfPTable(1);
					bodyLeftFooterTbl.setWidths(new float[] { 100f});
					bodyLeftFooterTbl.getDefaultCell().setBorder(0);
					bodyLeftFooterTbl.getDefaultCell().setBorderWidth(0f);
					bodyLeftFooterTbl.getDefaultCell().setBorderWidthTop(0f);
					bodyLeftFooterTbl.getDefaultCell().setBorderWidthBottom(0f);
					bodyLeftFooterTbl.getDefaultCell().setColspan(1);
					bodyLeftFooterTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
					bodyLeftFooterTbl.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
					bodyLeftFooterTbl.addCell(new Paragraph(salesRepInfo.getFullName(), BuildingApartmentsPdf.arialBoldPt12Black));
					
					String tmpPhoneNos = "";
					if (!StringUtil.isBlankOrEmpty(salesRepInfo.getPhoneNumber()))
					{
						tmpPhoneNos += "T: " + salesRepInfo.getPhoneNumber() + "   ";	
					}	
					
					if (!StringUtil.isBlankOrEmpty(salesRepInfo.getPhoneNumber()))
					{
						tmpPhoneNos +=  "M: " + salesRepInfo.getMobileNumber();
					}
					
					if (!StringUtil.isBlankOrEmpty(tmpPhoneNos))
					{
						bodyLeftFooterTbl.addCell(new Paragraph(tmpPhoneNos, BuildingApartmentsPdf.arialNormalPt12Black));
					}

					if (!StringUtil.isBlankOrEmpty(salesRepInfo.getFaxNumber()))
					{
						bodyLeftFooterTbl.addCell(new Paragraph("F: " + salesRepInfo.getFaxNumber(), BuildingApartmentsPdf.arialNormalPt12Black));
					}

					if (!StringUtil.isBlankOrEmpty(salesRepInfo.getEmail()))
					{
						bodyLeftFooterTbl.addCell(new Paragraph("E: " + salesRepInfo.getEmail(), BuildingApartmentsPdf.arialNormalPt12Black));
					}	

					
					
					PdfPTable bodyMiddleFooterTbl = new PdfPTable(1);
					bodyMiddleFooterTbl.setWidths(new float[] { 100f});
					bodyMiddleFooterTbl.getDefaultCell().setBorder(0);
					bodyMiddleFooterTbl.getDefaultCell().setBorderWidth(0f);
					bodyMiddleFooterTbl.getDefaultCell().setBorderWidthTop(0f);
					bodyMiddleFooterTbl.getDefaultCell().setBorderWidthBottom(0f);
					bodyMiddleFooterTbl.getDefaultCell().setColspan(1);
					bodyMiddleFooterTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
					bodyMiddleFooterTbl.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
					bodyMiddleFooterTbl.addCell(BLANK);
					

					
					GenRow officeAddress = new GenRow();
					officeAddress.setViewSpec("SetupView");
					officeAddress.setParameter("SetupID", "Default");
					officeAddress.doAction(GenerationKeys.SELECTFIRST);

					String compName = officeAddress.getString("Company");
					String street = officeAddress.getString("Street");
					if (StringUtil.isBlankOrEmpty(officeAddress.getString("Street2")))
					{
						street +=  officeAddress.getString("Street2");
					}	 
					
					PdfPTable bodyRightFooterTbl = new PdfPTable(1);
					bodyRightFooterTbl.setWidths(new float[] { 100f});
					bodyRightFooterTbl.getDefaultCell().setBorder(0);
					bodyRightFooterTbl.getDefaultCell().setBorderWidth(0f);
					bodyRightFooterTbl.getDefaultCell().setBorderWidthTop(0f);
					bodyRightFooterTbl.getDefaultCell().setBorderWidthBottom(0f);
					bodyRightFooterTbl.getDefaultCell().setColspan(1);
					bodyRightFooterTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
					bodyRightFooterTbl.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
					bodyRightFooterTbl.addCell(new Paragraph(compName, BuildingApartmentsPdf.arialBoldPt12Black));
					bodyRightFooterTbl.addCell(new Paragraph(street, BuildingApartmentsPdf.arialNormalPt12Black));
					bodyRightFooterTbl.addCell(new Paragraph(officeAddress.getString("City"), BuildingApartmentsPdf.arialNormalPt12Black));
					bodyRightFooterTbl.addCell(new Paragraph(officeAddress.getString("State") + " " + officeAddress.getString("Postcode") + " " +  officeAddress.getString("Country"), BuildingApartmentsPdf.arialNormalPt12Black));
					
					bodyFooterTbl.addCell(bodyLeftFooterTbl);
					bodyFooterTbl.addCell(bodyMiddleFooterTbl);
					bodyFooterTbl.addCell(bodyRightFooterTbl);
			
			footerTable.addCell(bodyFooterTbl); 
			footerTable.completeRow();
			
			
			
			
			
			String website = "";
			if (!StringUtil.isBlankOrEmpty(officeAddress.getString("Web")))
			{
				website = officeAddress.getString("Web");
			}	
			PdfPCell cell = new PdfPCell();
			cell = new PdfPCell(new Paragraph(website, BuildingApartmentsPdf.arialNormalPt14White));
			cell.setBorder(0);
			cell.setPaddingRight(20f);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorderWidth(0f);			
			
		    cell.setBackgroundColor(BuildingApartmentsPdf.greyBGColor);
		    cell.setFixedHeight(30f);

			footerTable.addCell(cell); 
			footerTable.completeRow();
			
			return footerTable;
	}
	
	public UserInfo getSalesRepDetails(String repUserID) 
	{
		UserInfo userInfo =  new UserInfo();
		
		if (repUserID != null && !"".equalsIgnoreCase(repUserID)) 
		{
				GenRow row = new GenRow();
				if (repUserID != null && repUserID.length() != 0) 
				{
					row.setViewSpec("UserView");
					row.setParameter("UserID", repUserID);
				} 
				else 
				{
					row.setViewSpec("SetupView");
					row.setParameter("SetupID", "Default");
				}
				row.doAction("select");
				
				userInfo.setUserID(row.getString("UserID"));
				userInfo.setFirstName(row.getString("FirstName"));
				userInfo.setLastName(row.getString("LastName"));
				userInfo.setMobileNumber(row.getString("Mobile"));
				userInfo.setPhoneNumber(row.getString("Phone"));
				userInfo.setFaxNumber(row.getString("Fax"));
				userInfo.setEmail(getRepEmail(row.getString("Email")));
				
				String locationMudMap = getLocationMudMap(repUserID);
				userInfo.setLocationMudMapPath(locationMudMap);
		}	
			return userInfo;
	}
	
	/* strip out email from "name <email@example.com>" style email fields */
	public String getRepEmail(String email) {
		int s = email.indexOf("<"), e = email.indexOf(">");
		if (s == -1 || e == -1 || e < s)
			return email;
		return email.substring(s + 1, e);
	}
	
	private String getLocationMudMap(String repUserID) 
	{
		    String mudMapPath = "";
			GenRow contactLocations = new GenRow();
			contactLocations.setTableSpec("ContactLocations");
			contactLocations.setColumn("UserID", repUserID);
			
			contactLocations.setParameter("-join0", "ContactLocationLocation.DisplayEntitiesLinkedDocuments.LinkedDocumentImage");		
			contactLocations.setParameter("-select0", "LinkedDocumentImage.FilePath as FilePath");
			contactLocations.setParameter("-select1", "ContactLocationLocation.Name as DisplayCentreName");
	
			contactLocations.doAction("search");
			contactLocations.getResults();
	
			System.out.println("repUserID is: " + repUserID + " ,    MupMap Query is: " + contactLocations.getStatement());
			
			if (contactLocations.getNext()) 
			{
				String tmpMupMapPath = contactLocations.getString("FilePath");
				mudMapPath = tmpMupMapPath.replaceAll(" ", "%20");
			}
			return mudMapPath;
	}
}