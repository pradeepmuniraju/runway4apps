package com.sok.runway.crm.cms.properties;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.RunwayUtil;
import com.sok.framework.generation.GenerationKeys;

public class ExistingProperty extends PropertyEntity {
	private static final Logger logger = LoggerFactory.getLogger(ExistingProperty.class);
	
	final PropertyType propertyType = PropertyType.ExistingProperty;
	static final String VIEWSPEC = "properties/ProductExistingPropertyView";
	
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}

	public ExistingProperty(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE, getProductType());
	}

	public ExistingProperty(HttpServletRequest request, String productID) {
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}

	public ExistingProperty(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	
	public ExistingProperty load(String productID) {
		return (ExistingProperty) super.load(productID);
	}
	
	
	@Override
	public String[] getDripProductIDs() {
		if (StringUtils.isNotBlank(getString(PRODUCTID))) {
			GenRow quick = getProductQuick(getString(PRODUCTID));
			if (quick.isSet("PlanID") && quick.isSet("LotID")) {
				return new String[]{quick.getString("EstateID"), quick.getString("StageID"), quick.getString("LotID"), quick.getString("RangeID"), quick.getString("DesignID"), quick.getString("PlanID"), quick.getString("ProductID")};
			} else if (quick.isSet("PlanID")) {
				return new String[]{quick.getString("RangeID"), quick.getString("DesignID"), quick.getString("PlanID"), quick.getString("ProductID")};
			} else if (quick.isSet("LotID")) {
				return new String[]{quick.getString("EstateID"), quick.getString("StageID"), quick.getString("LotID"), quick.getString(PRODUCTID)};
			}
		}

		return new String[]{getString(PRODUCTID)};
	}
	
	public String getDripTotalCost() { 
		String cost = getColumn("DripResult");
		// if the price is $0.00 then use the TotalCost
		if (cost.replaceAll("[0\\.\\$]","").length() == 0) cost = getColumn("TotalCost");
		if(cost.indexOf(".")>0) { 
			return cost.substring(0, cost.indexOf("."));
		}
		return cost;
	}
	
	public String getCost() { 
		String cost = getColumn("TotalCost");
		if(cost.indexOf(".")>0) { 
			return cost.substring(0, cost.indexOf("."));
		}
		return cost;
	}
	
	public static GenRow getProductQuick(String productID) {
		GenRow row = new GenRow();
		if (StringUtils.isNotBlank(productID)) {
			row.setViewSpec("properties/ProductQuickIndexView");			
			row.setColumn("ProductID", productID);
			row.doAction("selectfirst");
		}
		return row;
	}
	
	
	public GenRow getSalesRep() {
		if(isSuccessful() && isSet(PRODUCTID)) {
			GenRow row = new GenRow(); 
			row.setConnection(getConnection());
			row.setTableSpec("ProductSecurityGroups");
			row.setParameter("ProductID", getString(PRODUCTID));
			row.setParameter("-select0","RepUserID");
			row.setParameter("-select1","ProductSecurityGroupRep.FirstName");
			row.setParameter("-select2","ProductSecurityGroupRep.LastName"); 
			row.setParameter("-select3","ProductSecurityGroupRep.Mobile"); 
			row.setParameter("-select4","ProductSecurityGroupRep.Phone");
			row.setParameter("-select5","ProductSecurityGroupRep.Email");
			row.doAction(GenerationKeys.SELECTFIRST);
			
			if(row.isSuccessful() && row.isSet("FirstName")) {
				return row;
			} 
		}
		return super.getSalesRep();
	}
}
