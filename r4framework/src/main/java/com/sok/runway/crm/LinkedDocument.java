package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;


public class LinkedDocument extends LinkableDocument {

public static final String LINKDOCUMENT_VIEWSPEC = "LinkedDocumentView";
   
   public LinkedDocument(Connection con) {
		super(con, LINKDOCUMENT_VIEWSPEC);
	}
		
	public LinkedDocument(ServletRequest request) {
		super(request, LINKDOCUMENT_VIEWSPEC);
		populateFromRequest(request.getParameterMap());
	}
	   
	public LinkedDocument(Connection con, String linkedDocumentID) {
		this(con);
	   load(linkedDocumentID);
	}
   
   public Validator getValidator() {
		Validator val = Validator.getValidator("LinkedDocuments");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("LinkedDocumentID","SYSTEM ERROR - LinkedDocumentID");
			val.addMandatoryField("DocumentID","SYSTEM ERROR - DocumentID");
			Validator.addValidator("LinkedDocuments", val);
		}
		return val;
   }
}