package com.sok.runway.crm.cms.properties.pdf.advanced;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sog.pdfserver.client.api.request.PDFRequest;
import com.sog.pdfserver.client.api.request.TemplateListTag;
import com.sok.framework.RunwayUtil;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.AdvancedPDFProcessor;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;

public class PDFHouseandlandDetail extends PDFProductDetail {

	public static final Logger logger = LoggerFactory.getLogger(PDFHouseandlandDetail.class);

	@Override
	public void populateSpecificTags(HttpServletRequest request, Map<String,String> tags, List<TemplateListTag> listTags, PropertyEntity property, PDFRequest pdfRequest) {
		String homePath = request.getSession().getServletContext().getInitParameter("URLHome");
		if (property == null) {
			throw new RuntimeException("Product was not found");
		}
		if (!(property.isPropertyType(PropertyType.HouseLandPackage) || property.isPropertyType(PropertyType.HomePlan))) {
			throw new RuntimeException("Property [" + property.getPropertyType() + "] was not a house and land package");
		}
		
		if(property.isPropertyType(PropertyType.HouseLandPackage)) {
			HouseLandPackage product = new HouseLandPackage(property.getConnection(), property.getString("ProductID"));
	
			tags.put("BED", "" + product.getBedrooms());
			tags.put("BATH", "" + product.getBathroomsAsString());
			tags.put("FHB_PRICE", "447,500");
			tags.put("DISCLAIMER", "Package price is based on standard house, facade and builder's preferred siting. Pricing may");
			tags.put("FACADE_IMAGE", AdvancedPDFProcessor.lookUpImage(homePath + RunwayUtil.updateImage(product.getImage())));
		} else if(property.isPropertyType(PropertyType.HomePlan)) {
			
		}
	}
}