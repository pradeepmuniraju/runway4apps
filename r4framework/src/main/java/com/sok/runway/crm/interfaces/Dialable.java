package com.sok.runway.crm.interfaces;

public interface Dialable extends Recordable {
	
	public String getRecordURL(String field, int channel);
	
	public String getRecordURL(String field, int channel, String custom);
	   
	public String getDialURL(String field); 
	
	public String getPhoneURL(String field, int channel);
	
	public String getPhoneURL(String field, int channel,String custom);
}
