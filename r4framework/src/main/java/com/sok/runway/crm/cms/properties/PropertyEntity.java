package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.service.crm.cms.properties.DripService;

public abstract class PropertyEntity extends GenRow {

	private static final Logger logger = LoggerFactory.getLogger(PropertyEntity.class);

	protected static final String PRODUCTID = "ProductID";
	protected static final String PRODUCTTYPE = "ProductType";
	protected static final String LINKED_PRODUCTTYPE = "ProductProductsLinkedProduct.ProductType";

	private HashMap<String, String> dripMap = null;

	DripService ds = new DripService();

	private HashMap<String, Object> productDetailMap = null;

	public boolean isPropertyType(PropertyType type) {
		return getPropertyType().equals(type);
	}

	public abstract PropertyType getPropertyType();

	public String getProductType() {
		if (getPropertyType() == null) return null;
		return getPropertyType().getProductType();
	}

	public PropertyEntity load(String productID) {
		setParameter(PropertyFactory.PRODUCTID, productID);
		if (isSet(PropertyFactory.PRODUCTID)) {
			doAction(getDBAction());
			if (StringUtils.isBlank(getProductType())) {
				success = isSuccessful();
				logger.debug("ProductID supplied {} is not a {}", productID);
			} else if (isSuccessful() && !getProductType().equals(getData(PRODUCTTYPE))) {
				success = false;
				logger.debug("ProductID supplied {} is not a {}", productID, getProductType());
			} else if(!isSuccessful()) {
				logger.debug("ProductID supplied was not found [%1$s]", productID);
				//doAction(GenerationKeys.SEARCH);
				logger.debug(getStatement());
			}
		}
		return this;
	}

	protected String getDBAction() {
		return GenerationKeys.SELECTFIRST;
	}

	protected String getLinkedProductID(Connection con, String productID, String type) {
		GenRow row = new GenRow();
		row.setConnection(con);
		row.setTableSpec("ProductProducts");
		row.setParameter("-select1", "LinkedProductID");
		row.setParameter(PRODUCTID, productID);
		row.setParameter("ProductProductsLinkedProduct.ProductType", type);
		row.doAction(GenerationKeys.SELECTFIRST);
		return (row.getString("LinkedProductID"));
	}

	/**
	 * return the default sales rep override this in child classes to assign particular rep.
	 */
	public GenRow getSalesRep() {
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("SetupView");
		row.setParameter("SetupID", "Default");
		row.doAction("select");
		return row;
	}

	public boolean setSalesRep(String repUserID, String groupID, String currentUser) {

		if (isSet("ProductID")) {
			GenRow reps = new GenRow();
			reps.setConnection(getConnection());
			reps.setViewSpec("ProductSecurityGroupView");
			reps.setParameter("ProductID", getString("ProductID"));
			reps.doAction("search");
			reps.getResults();

			GenRow repUpdate = new GenRow();
			repUpdate.setConnection(getConnection());
			repUpdate.setViewSpec("ProductSecurityGroupView");

			boolean found = false;
			while (reps.getNext()) {
				repUpdate.clear();
				repUpdate.setParameter("ProductSecurityGroupID", reps.getData("ProductSecurityGroupID"));
				if ((repUserID.equals(reps.getData("RepUserID")) || StringUtils.isBlank(reps.getData("RepUserID"))) && groupID.equals(reps.getData("GroupID"))) {
					found = true;
					repUpdate.setParameter("RepUserID", repUserID);
					repUpdate.setParameter("GroupID", groupID);
					repUpdate.doAction(GenerationKeys.UPDATE);
				} else {
					repUpdate.doAction(GenerationKeys.DELETE);
				}
			}
			if (!found) {
				// add new
				repUpdate.clear();
				repUpdate.setParameter("CreatedDate", "NOW");
				repUpdate.setParameter("CreatedBy", currentUser);
				repUpdate.setParameter("ProductSecurityGroupID", KeyMaker.generate());
				repUpdate.setParameter("RepUserID", repUserID);
				repUpdate.setParameter("GroupID", groupID);
				repUpdate.setParameter("ProductID", getString("ProductID"));
				repUpdate.doAction(GenerationKeys.INSERT);
			}
			reps.close();
			return true;
		} else {
			logger.debug("not updating rep as product id was not set");
		}
		return false;
	}

	public Object getProductDetails(String detailType, String field) {
		if (productDetailMap == null)
			populateProductDetailsMap();

		HashMap detailMap = (HashMap<String, Object>) productDetailMap.get(detailType);
		if (detailMap != null) {
			Object o = detailMap.get(field);
			if (o != null)
				return o;

		}
		return null;
	}

	public void populateProductDetailsMap() {
		if (isSet("ProductID")) {
			productDetailMap = new HashMap<String, Object>();

			GenRow details = new GenRow();
			details.setConnection(con);
			details.setTableSpec("properties/ProductDetails");
			details.setParameter("ProductID", getString("ProductID"));
			// details.setParameter("ProductType", "HomePlan");
			details.setParameter("-select", "*");
			details.getResults();

			while (details.getNext()) {
				String type = details.getColumn("DetailsType").replaceAll(" ", "");
				String pType = details.getColumn("ProductType").replaceAll("Details", "");
				HashMap detailMap = new HashMap<String, Object>();

				for (int i = 0; i < details.getTableSpec().getFieldsLength(); i++) {
					String field = details.getTableSpec().getFieldName(i);
					Object o = details.getObject(field);
					if (o != null) {
						detailMap.put(field, o);
						logger.trace("adding {} = {}", field, o);
					}
				}
				productDetailMap.put(type, detailMap);
				productDetailMap.put(pType + "." + type, detailMap);
			}
			details.close();
		} else {
			logger.debug("product id was not set");
		}
	}

	/**
	 * 
	 * @param imagePath
	 * @param width
	 * @param height
	 * @param pad
	 *            (boolean) should this image be padded to the size
	 * @param crop
	 *            (boolean) should this image be cropped to the size If pad and crop are false
	 */

	public enum ResizeOption {
		Pad, Crop, Fit, Trim
	}

	public static String resizeImage(String path, int newWidth, int newHeight, ResizeOption option) {
		return resizeImage(path, newWidth, newHeight, option, false);
	}

	public static String resizeImage(String path, int newWidth, int newHeight, ResizeOption option, String fmt) {
		return resizeImage(path, newWidth, newHeight, option, false, false, 0, 0, fmt);
	}

	public static String resizeImage(String path, int newWidth, int newHeight, ResizeOption option, boolean force) {
		return resizeImage(path, newWidth, newHeight, option, force, false, 0, 0, "png");
	}

	public static String resizeImage(String path, int newWidth, int newHeight, ResizeOption option, boolean force, String fmt) {
		return resizeImage(path, newWidth, newHeight, option, force, false, 0, 0, fmt);
	}

	public static String resizeImage(String path, int newWidth, int newHeight, ResizeOption option, boolean force, boolean highQuality, int blurW, int blurH) {
		return resizeImage(path, newWidth, newHeight, option, force, highQuality, blurW, blurH, "png");
	}

	public static String resizeImage(String path, int newWidth, int newHeight, ResizeOption option, boolean force, boolean highQuality, int blurW, int blurH, String fmt) {
		if (StringUtils.isBlank(path))
			return null;

		String realPath = InitServlet.getRealPath(com.sok.framework.ActionBean._blank);
		String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
		
		java.io.File f0 = null;
		java.io.File f1 = null;
		
		try {
			// old ext, we want to use png
			// path.substring(path.lastIndexOf('.'))

			int lastDot = path.lastIndexOf('.');
			if(lastDot < 0) {
				//we have a bunch of documents with null filenames. 
				return null;
			}

			String newPath = new StringBuilder(100).append(path.substring(0, lastDot).replaceAll(" ", "_"))
					.append("_").append(option.name()).append("_").append(newWidth).append("x").append(newHeight).append(".").append(fmt).toString();
			String tmpPath = new StringBuilder(100).append(path.substring(0, lastDot).replaceAll(" ", "_"))
					.append("_").append(option.name()).append("_").append(newWidth).append("x").append(newHeight).append("_").append(KeyMaker.generate(4)).append("_tmp.").append(fmt).toString();
			String pathAbs = new StringBuilder(realPath).append("/").append(path).toString();
			String tmpAbs = new StringBuilder(realPath).append("/").append(tmpPath).toString();
			String newAbs = new StringBuilder(realPath).append("/").append(newPath).toString();

			f0 = new java.io.File(pathAbs);
			f1 = new java.io.File(newAbs);
			
			if (!f0.exists()) f0 = new java.io.File(pathAbs.replaceAll(" ", "%20"));
			if (!f0.exists()) f0 = new java.io.File(pathAbs.replaceAll(" ", "\\ "));

			if (f0.exists() && (!f1.exists() || force || f1.lastModified() < f0.lastModified())) {
				f1.getParentFile().mkdirs();

				// System.out.println("doing image resize");
				com.sok.framework.ImageUtil img = new com.sok.framework.ImageUtil(pathAbs);

				// DO additional steps if high quality images are required
				if (highQuality) {
					img.setAllowEnlarge(true);
					img.createCompatibleImage();
					logger.trace("Resizing...");
					if (blurW == 0)
						blurW = img.getWidth() + 1000;
					if (blurH == 0)
						blurH = img.getHeight() + 1000;
					img.resizeToFit(blurW, blurH, true, false);
					img.blurImage();
				}

				switch (option) {
				case Pad:
					img.setAllowEnlarge(true);
					img.resizeToFit(newWidth, newHeight, true, false);

					break;
				case Crop:
					img.resizeToFit(newWidth, newHeight, false, true);
					break;
				case Trim:
					img.trim();
					img.createCompatibleImage();
					break;
				case Fit:
				default:
					img.resizeToFit(newWidth, newHeight, false, false);
				}
				img.setFormat(fmt);
				if("jpg".equals(fmt) || "png".equals(fmt)) { 
					img.setJpegQuality(0.95f);
				}
				img.saveImage(tmpAbs);
			}

			// now we have created the temporary file, lets see if it is later than the new file and then rename it
			java.io.File f2 = new java.io.File(tmpAbs);
			if (f2.exists() && (!f1.exists() || force || f1.lastModified() < f2.lastModified())) {
				f1.delete();
				f2.renameTo(f1);
			} else {
				f2.delete();
			}
			return new StringBuilder(homePath).append("/").append(newPath).toString();
		} catch (Exception e) {
			if (f0 == null || !f0.exists()) {
				String file = new StringBuilder(homePath).append("/").append(path).toString();
				logger.error("exception image resize returning filename " + file, e);
				return file;
			}
			String file = new StringBuilder(homePath).append("/").append(f0.getAbsolutePath().replaceFirst(realPath.replaceAll("\\\\", "\\\\\\\\"), "")).toString();
			logger.error("exception image resize returning filename " + file, e);
			
			return file;
		}
	}

	public String getEntityEstateName() {
		if(isPropertyType(PropertyType.HouseLandPackage)) {
			return ((HouseLandPackage)this).getEstateName();
		} 
		if(isPropertyType(PropertyType.Land)) {
			return ((Land)this).getEstateName();
		} 
		if(isPropertyType(PropertyType.Estate)) {
			return ((Estate)this).getString("EstateName");
		} 
		if(isPropertyType(PropertyType.Stage)) {
			return ((Stage)this).getString("EstateName");
		} 
		return null;
	}

	public String getEntityBuildingName() {
		if(isPropertyType(PropertyType.Apartment)) {
			return ((Apartment)this).getString("BuildingName");
		} 
		if(isPropertyType(PropertyType.Building)) {
			return ((Building)this).getString("BuildingName");
		} 
		if(isPropertyType(PropertyType.BuildingStage)) {
			return ((BuildingStage)this).getString("BuildingName");
		} 
		return null;
	}

	public void clearDripMap() {
		if (dripMap != null) dripMap.clear();
		dripMap = null;
	}

	/**
	 * @deprecated
	 */
	public List<String> getPromoImages(final HttpServletRequest request, final String[] productIDs) {
		return getPromoImages(request);
	}

	/**
	 * @deprecated
	 */
	public String getDripText(final HttpServletRequest request, final String[] productIDs, final String template, final String placement) {
		return getDripText(request, template, placement);
	}

	/**
	 * @deprecated
	 */
	public String getDripImage(final HttpServletRequest request, final String[] productIDs, final String template, final String placement) {
		return getDripImage(request, template, placement);
	}

	public List<String> getPromoImages(final HttpServletRequest request) {
		return ds.getDripImages(request, getDripProductIDs(), "PDF", null, null);
	}

	public String getDripText(final HttpServletRequest request, final String template, final String placement) {
		return getDripInfo(request, "Text", "PDF", template, placement);
	}

	public String getDripImage(final HttpServletRequest request, final String template, final String placement) {
		return getDripInfo(request, "Image", "PDF", template, placement);
	}


	public String[] getDripProductIDs() {
		throw new UnsupportedOperationException("Not defined for product type "  + this.getProductType());
	}

	/**
	 * @deprecated
	 */
	public String getDripInfo(final HttpServletRequest request, final String[] productIDs, final String type, final String subType, final String template, final String placement) {
		return getDripInfo(request, type, subType, template, placement);
	}
	
	public String getDripInfo(final HttpServletRequest request, final String type, final String subType, final String template, final String placement) {
		if(logger.isDebugEnabled()) 
			logger.debug("getDripInfo(request,{},{},{},{})", new String[]{ type, subType, template, placement});
		if (dripMap == null)
			dripMap = ds.getDripMapForTemplates(request, getDripProductIDs(), subType, template);

		if(logger.isDebugEnabled()) {
			logger.debug("Have drip map with key length {}", dripMap.keySet().size());
			for(Map.Entry<String, String> me: dripMap.entrySet()) {
				logger.debug("Drip Entry {} = {}", me.getKey(), me.getValue());
			}
		}

		return dripMap.get(type + template + placement);
	}
	
	public String getDripInfo(final Connection conn, final UserBean user, final String type, final String subType, final String template, final String placement) {
		if(logger.isDebugEnabled()) 
			logger.debug("getDripInfo(conn,{},{},{},{})", new String[]{ type, subType, template, placement});
		if (dripMap == null)
			dripMap = ds.getDripMapForTemplates(conn, user, getDripProductIDs(), subType, template);

		if(logger.isDebugEnabled()) {
			logger.debug("Have drip map with key length {}", dripMap.keySet().size());
			for(Map.Entry<String, String> me: dripMap.entrySet()) {
				logger.debug("Drip Entry {} = {}", me.getKey(), me.getValue());
			}
		}

		return dripMap.get(type + template + placement);
	}

	public List<String> getPromoImages(final HttpServletRequest request,final String type) {
		return ds.getDripImages(request, getDripProductIDs(), type, null, null);
	}
	public String getDripImage(final HttpServletRequest request, final String template, final String placement, final String type) {
		return ds.getDripImage(request, getDripProductIDs(), type, template, placement);
	}
	public String getDripText(final HttpServletRequest request, final String template, final String placement, final String type) {
		return ds.getDripText(request, getDripProductIDs(), type, template, placement);
	}

	/**
	 * @deprecated
	 */
	public List<String> getPromoImages(final HttpServletRequest request, final String[] productIDs, final String type) {
		return getPromoImages(request, type);
	}

	/**
	 * @deprecated
	 */
	public String getDripImage(final HttpServletRequest request, final String[] productIDs, final String template, final String placement, final String type) {
		return getDripImage(request, template, placement, type);
	}
	/**
	 * @deprecated
	 */
	public String getDripText(final HttpServletRequest request, final String[] productIDs, final String template, final String placement, final String type) {
		return getDripText(request, template, placement, type);
	}
}