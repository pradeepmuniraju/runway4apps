package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class Brand extends PropertyEntity {

	final PropertyType propertyType = PropertyType.Brand;

	static final String VIEWSPEC = "ProductView";

	public Brand(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public Brand(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}

	public Brand(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	
	@Override
	public Brand load(String productID) {
		return (Brand)super.load(productID);
	}
	public List<BuildingStage> getStages() {
		return Collections.emptyList();
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
}