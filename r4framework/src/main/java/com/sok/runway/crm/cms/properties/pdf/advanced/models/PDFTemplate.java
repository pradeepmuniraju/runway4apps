package com.sok.runway.crm.cms.properties.pdf.advanced.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;

import com.sog.pdfserver.client.api.response.PdfCategory;
import com.sog.pdfserver.client.api.response.TemplateInfo;
import com.sog.pdfserver.client.api.response.TemplateTagInfo;

public class PDFTemplate {
	
	private String	id	= "";
	private String	name	= "";
	private String	description	= "";
	private String	sortType	= "";
	private String	categoryID	= "";
	private String	group	= "";
	private String	product	= "";
	private boolean	defaultTemplate	= false;
	private String	conditions	= "";
	
	private List<TemplateTagInfo> tags = null;
	
	private HashSet<String>	prefixes = null;

	/*
	obj.put("Name", template.getName());
	if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
		obj.put("ID", template.getId());
	else
		obj.put("ID", template.getName());
	obj.put("SortType", template.getProductSortType());
	obj.put("CategoryID", pdfc.getId());
	obj.put("Group", pdfc.getGroup());
	obj.put("Product", pdfc.getProduct());
	obj.put("Default", template.isDefaultTemplate());
	if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
		obj.put("Thumbnail", template.getThumbnail());
	else
		obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
	if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
		obj.put("Description", template.getDescription());
	else
		obj.put("Description", "");

	obj.put("Conditions", "");
	 */
	public PDFTemplate() {
		// TODO Auto-generated constructor stub
	}
	
	public PDFTemplate(PdfCategory pdfc, TemplateInfo template) {
		if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
			id = template.getId();
		else
			id = template.getName();
		
		name = template.getName();
		
		description = template.getDescription();
		sortType  = template.getProductSortType();
		categoryID = pdfc.getId();
		group = pdfc.getGroup();
		product = pdfc.getProduct();
		defaultTemplate = template.isDefaultTemplate();
		
		conditions = template.getSearchMetaText();
		
		tags = template.getTags();
		generatePrefixes();
		
		if ("null".equalsIgnoreCase(description)) description = "";
		if ("null".equalsIgnoreCase(conditions)) conditions = "";
	}

	public PDFTemplate(TemplateInfo template) {
		if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
			id = template.getId();
		else
			id = template.getName();
		
		name = template.getName();
		
		description = template.getDescription();
		sortType  = template.getProductSortType();
		defaultTemplate = template.isDefaultTemplate();
		
		conditions = template.getSearchMetaText();
		
		tags = template.getTags();
		generatePrefixes();
		
		if ("null".equalsIgnoreCase(description)) description = "";
		if ("null".equalsIgnoreCase(conditions)) conditions = "";
	}
	
	private void generatePrefixes() {
		prefixes = new HashSet<>();
		
		if (tags != null && tags.size() > 0) {
			for (int i = 0; i < tags.size(); ++i) {
				TemplateTagInfo tag = tags.get(i);
				String parts[] = tag.getName().split("_");
				if (parts.length > 0) prefixes.add(parts[0] + "_");
				if (parts.length > 1) prefixes.add(parts[0] + "_" + parts[1] + "_");
				if (parts.length > 2) prefixes.add(parts[0] + "_" + parts[1] + "_" + parts[2] + "_");
			}
		}
		
		if (StringUtils.isNotBlank(conditions)) {
			String[] pre = conditions.split(" ");
			if (pre != null && pre.length > 0) {
				for (int i = 0; i < pre.length; ++i) {
					if (StringUtils.isNotBlank(pre[i]) && pre[i].indexOf("_") >= 0) {
						String parts[] = pre[i].split("_");
						if (parts.length > 0) prefixes.add(parts[0] + "_");
						if (parts.length > 1) prefixes.add(parts[0] + "_" + parts[1] + "_");
						if (parts.length > 2) prefixes.add(parts[0] + "_" + parts[1] + "_" + parts[2] + "_");
					}
				}
				
			}
		}
	}
	
	public boolean isUsed(String prefix) {
		if (prefixes == null || StringUtils.isBlank(prefix)) return true;
		
		return prefixes.contains(prefix);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the sortType
	 */
	public String getSortType() {
		return sortType;
	}

	/**
	 * @param sortType the sortType to set
	 */
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	/**
	 * @return the categoryID
	 */
	public String getCategoryID() {
		return categoryID;
	}

	/**
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}

	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(String product) {
		this.product = product;
	}

	/**
	 * @return the defaultTemplate
	 */
	public boolean isDefaultTemplate() {
		return defaultTemplate;
	}

	/**
	 * @param defaultTemplate the defaultTemplate to set
	 */
	public void setDefaultTemplate(boolean defaultTemplate) {
		this.defaultTemplate = defaultTemplate;
	}

	/**
	 * @return the conditions
	 */
	public String getConditions() {
		return conditions;
	}

	/**
	 * @param conditions the conditions to set
	 */
	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	/**
	 * @return the tags
	 */
	public List<TemplateTagInfo> getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(List<TemplateTagInfo> tags) {
		this.tags = tags;
	}

}
