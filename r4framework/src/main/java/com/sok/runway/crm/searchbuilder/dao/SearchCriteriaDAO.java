package com.sok.runway.crm.searchbuilder.dao;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.searchbuilder.SearchCriteria;
import com.sok.runway.crm.searchbuilder.SearchSet;

public class SearchCriteriaDAO {
    HttpServletRequest request;

    public SearchCriteriaDAO(HttpServletRequest request) {
        this.request = request;
    }
    
    /**
     * <p>Create a new search criteria row from the specified 
     * <code>searchCriteria</code> model. The primary key of the newly 
     * created search criteria row will be set in the model and 
     * returned from this method.</p> 
     * 
     * @param searchCriteria the search criteria model
     * @return the ID of the newly created searchCriteria row
     */    
    public String create(SearchCriteria searchCriteria) {
        GenRow searchCriteriaRow = createRow();
        if(searchCriteria.getId() == null || searchCriteria.getId().length() == 0) {
            searchCriteriaRow.createNewID();
            searchCriteria.setId(searchCriteriaRow.getParameter("SearchCriteriaID"));
        } else {
            searchCriteriaRow.setParameter("SearchCriteriaID", searchCriteria.getId());
        }        
        populateRow(searchCriteriaRow, searchCriteria);
        searchCriteriaRow.doAction(GenerationKeys.INSERT);
        
        return searchCriteria.getId();
    }
    
    /**
     * <p>Loads the <code>SearchCriteria</code> model by the specified <code>id</code>.
     * Note that invoking this method <b>will not</b> create an instance of 
     * the parent <code>SearchSet</code> for this <code>SearchCriteria</code> model.</p>
     * 
     * @param id of the <code>SearchCriteria</code> row
     * @return the <code>SearchCriteria</code> model
     */    
    public SearchCriteria findById(String id) {
        SearchCriteria searchCriteria = new SearchCriteria();
        
        GenRow searchCriteriaRow = createRow();
        searchCriteriaRow.setParameter("SearchCriteriaID", id);
        searchCriteriaRow.setParameter("-select1", "SearchSetID");
        searchCriteriaRow.setParameter("-select2", "FieldType");
        searchCriteriaRow.setParameter("-select3", "FieldTypeValue");
        searchCriteriaRow.setParameter("-select4", "FieldName");
        searchCriteriaRow.setParameter("-select5", "FieldValue");
        searchCriteriaRow.setParameter("-select6", "FieldValueText");
        searchCriteriaRow.setParameter("-select7", "FieldValueDate");
        searchCriteriaRow.setParameter("-select8", "FieldValueNumber");
        searchCriteriaRow.setParameter("-select9", "SortOrder");
        searchCriteriaRow.setAction(GenerationKeys.SEARCH);
        searchCriteriaRow.getResults();
        
        if(searchCriteriaRow.getNext()) {
            populateSearchCriteria(searchCriteria, searchCriteriaRow);
        }
        
        searchCriteriaRow.close();
        
        return searchCriteria;
    }
    
    /**
     * <p>Loads the list of <code>SearchCriteria</code> models by the specified 
     * <code>searchSetId</code>. Note that invoking this method <b>will not</b> 
     * create an instance of the parent <code>SearchSet</code>.</p>
     * 
     * @param id of the <code>SearchCriteria</code> row
     * @return the <code>SearchCriteria</code> model
     */     
    public List<SearchCriteria> findAllBySearchSetId(String searchSetId) {
        List<SearchCriteria> searchCriterias = new ArrayList<SearchCriteria>();
        
        GenRow searchCriteriaRow = createRow();
        searchCriteriaRow.setParameter("SearchSetID", searchSetId);
        searchCriteriaRow.setParameter("-select1", "SearchCriteriaID");
        searchCriteriaRow.setParameter("-select2", "FieldType");
        searchCriteriaRow.setParameter("-select3", "FieldTypeValue");
        searchCriteriaRow.setParameter("-select4", "FieldName");
        searchCriteriaRow.setParameter("-select5", "FieldValue");
        searchCriteriaRow.setParameter("-select6", "FieldValueText");
        searchCriteriaRow.setParameter("-select7", "FieldValueDate");
        searchCriteriaRow.setParameter("-select8", "FieldValueNumber");
        searchCriteriaRow.setParameter("-select9", "SortOrder");
        searchCriteriaRow.setAction(GenerationKeys.SEARCH);
        searchCriteriaRow.getResults(true);
        
        populateSearchSets(searchCriterias, searchCriteriaRow);
        
        searchCriteriaRow.close();
        
        return searchCriterias;
    }
    
    /**
     * <p>Updates the search set row for the specified 
     * <code>searchCriteria</code> model.</p>
     * 
     * @param searchCriteria the <code>searchCriteria</code> model
     */
    public void save(SearchCriteria searchCriteria) {
        GenRow searchSetRow = createRow();
        searchSetRow.setParameter("SearchCriteriaID", searchCriteria.getId());
        populateRow(searchSetRow, searchCriteria);        
        searchSetRow.doAction(GenerationKeys.UPDATE);
        searchSetRow.close();
    }    
    
    /**
     * 
     * @return a GenRow instance for the <code>SearchCriterias</code> 
     * table spec
     */
    private GenRow createRow() {
        final GenRow searchSetRow = new GenRow();
        searchSetRow.setRequest(this.request);
        searchSetRow.setTableSpec("SearchCriterias");
        
        return searchSetRow;
    }
    
    /**
     * <p>Populate row fields in <code>searchStatementRow</code> from the specified 
     * <code>searchStatement</code> model.</p>
     * 
     * @param searchStatementRow
     * @param searchStatement
     */
    private void populateRow(GenRow searchCriteriaRow, SearchCriteria searchCriteria) {   
        searchCriteriaRow.setParameter("SearchSetID", searchCriteria.getSearchSetId());
        searchCriteriaRow.setParameter("FieldType", searchCriteria.getFieldType().toString());
        searchCriteriaRow.setParameter("FieldTypeValue", searchCriteria.getFieldTypeValue().toString());
        searchCriteriaRow.setParameter("FieldName", searchCriteria.getFieldNameValue());
        searchCriteriaRow.setParameter("FieldValue", searchCriteria.getFieldValue());
        searchCriteriaRow.setParameter("FieldValueText", searchCriteria.getFieldValueText());
        searchCriteriaRow.setParameter("FieldValueDate", searchCriteria.getFieldValueDate());
        searchCriteriaRow.setParameter("FieldValueNumber", searchCriteria.getFieldValueNumber());
        searchCriteriaRow.setParameter("SortOrder", searchCriteria.getSortOrder());
    }

    /**
     * <p>Populate the fields in the <code>searchCriteria</code> model from the specified 
     * <code>searchCriteriaRow</code>.</p>
     * 
     * @param searchStatement
     * @param searchStatementRow
     */
    private void populateSearchCriteria(SearchCriteria searchCriteria, GenRow searchCriteriaRow) {
        searchCriteria.setId(searchCriteriaRow.getString("SearchCriteriaID"));
        searchCriteria.setSearchSetId(searchCriteriaRow.getString("SearchSetID"));
        searchCriteria.setFieldType(searchCriteriaRow.getString("FieldType"));
        searchCriteria.setFieldTypeValue(searchCriteriaRow.getString("FieldTypeValue"));
        searchCriteria.setFieldNameValue(searchCriteriaRow.getString("FieldName"));
        searchCriteria.setFieldValue(searchCriteriaRow.getString("FieldValue"));
        searchCriteria.setFieldValueText(searchCriteriaRow.getString("FieldValueText"));
        searchCriteria.setFieldValueDate(searchCriteriaRow.getString("FieldValueDate"));
        searchCriteria.setFieldValueNumber(searchCriteriaRow.getString("FieldValueNumber"));
        searchCriteria.setSortOrder(searchCriteriaRow.getString("SortOrder"));
    }
    
    /**
     * <p>For each <code>searchCriteria</code> model in the specified <code>searchCriterias</code> 
     * list, populate its fields from the specified <code>searchCriteriaRow</code>.</p>
     * 
     * @param searchCriterias
     * @param searchCriteriaRow
     */
    private void populateSearchSets(List<SearchCriteria> searchCriterias, GenRow searchCriteriaRow) {
        while(searchCriteriaRow.getNext()) {
            SearchCriteria searchCriteria = new SearchCriteria();
            populateSearchCriteria(searchCriteria, searchCriteriaRow);
            searchCriterias.add(searchCriteria);
        }
    }
}
