package com.sok.runway.crm.cms.properties.pdf.templates.model;

public class LetterOption {

	private String name = "";
	private String description = "";
	private String variant = "";

	private boolean isFinancial = false;

	public LetterOption(String variant, String name, String description, boolean isFinancial) {
		super();
		this.name = name;
		this.description = description;
		this.variant = variant;
		this.isFinancial = isFinancial;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isFinancial() {
		return isFinancial;
	}

	public void setFinancial(boolean isFinancial) {
		this.isFinancial = isFinancial;
	}

}