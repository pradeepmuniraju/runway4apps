package com.sok.runway.crm.cms.properties.pdf.advanced;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sog.pdfserver.client.api.exception.PdfClientValidationException;
import com.sog.pdfserver.client.api.exception.PdfServerConsumeException;
import com.sog.pdfserver.client.api.request.PDFRequest;
import com.sog.pdfserver.client.api.request.PageStaticData;
import com.sog.pdfserver.client.api.request.TemplateListTag;
import com.sok.framework.Conditions;
import com.sok.framework.InitServlet;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.pdf.AdvancedPDFProcessor;
import com.sok.runway.crm.cms.properties.pdf.PropertyPDFServlet;
import com.sok.runway.crm.cms.properties.pdf.advanced.models.PDFTemplate;

/**
 * Default implementation for all products, loaded when specific class for eg:PDFHouseandlandDetail.java is not found
 *
 */
public class PDFProductDetail extends AdvancedPDFProcessor {

	public static final Logger logger = LoggerFactory.getLogger(PDFProductDetail.class);
	
	public void populatePDFRequest(HttpServletRequest request, HttpServletResponse response, PDFRequest pdfRequest) {

		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (List<PropertyEntity>) request.getAttribute("entityList");
		if (properties == null) {
			setStatus(request, "Loading Records..", 0.2d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			properties = PropertyFactory.getBasicEntities(request, this);
		}

		java.util.List<PageStaticData> pageStaticDataList = new java.util.ArrayList<PageStaticData>(properties.size());
		int c = 0;
		setStatus(request, "Generating Tag List..", 0.0d);
		for (PropertyEntity property : properties) {
			if (property == null) {
				throw new RuntimeException("Product was not found");
			}
			
			if (StringUtils.isBlank(pdfRequest.getTemplateId())) {
				JSONObject obj = PropertyPDFServlet.getDefaultPDFTemplate(request, StringUtils.deleteWhitespace(property.getString("ProductType")));
				if (obj == null || !obj.containsKey("ID")) {
					setStatus(request, "redirect:" + InitServlet.getSystemParam("PDF-AdvancedServerFallbackURL"), -1);
					try {
						response.sendRedirect(InitServlet.getSystemParam("PDF-AdvancedServerFallbackURL"));
					} catch (IOException e) {
						e.printStackTrace();
					}
					return;
				}
				if (obj.containsKey("Company")) pdfRequest.setCompanyName((String) obj.get("Company"));
				if (obj.containsKey("ID")) pdfRequest.setTemplateId((String) obj.get("ID"));
				if (obj.containsKey("CategoryID")) pdfRequest.setPdfCategoryId((String) obj.get("CategoryID"));
				if (obj.containsKey("MultiTemplate") && "true".equalsIgnoreCase((String) obj.get("MultiTemplate"))) pdfRequest.setCompanyId(pdfRequest.getTemplateId());
				try {
					loadPDFTemplate(pdfRequest);
				} catch (PdfServerConsumeException e) {
				} catch (PdfClientValidationException e) {
				}
			}

			TreeMap<String,String> tags = new TreeMap<>();
			List<TemplateListTag> listTags = new ArrayList<>();

			templateID =  pdfRequest.getTemplateId();
			
			populateDefaultTags(request, tags, listTags, property);
			populateSpecificTags(request, tags, listTags, property, pdfRequest);
			PageStaticData pageStaticData = new PageStaticData();
			if (super.pdfTemplates.size() > 0) {
				Conditions cons = null;
				for (int x = 0; x < super.pdfTemplates.size(); ++x) {
					PDFTemplate template = super.pdfTemplates.get(x);
					if (StringUtils.isNotBlank(template.getConditions())) {
						if (cons == null) {
								cons = new Conditions(tags);
						}
						if (cons.evaluate(template.getConditions())) {
							pageStaticData.setTemplateId(template.getId());
							pageStaticData.setPdfGroupId(pdfRequest.getTemplateId());
							if (!"true".equalsIgnoreCase(InitServlet.getSystemParam("PDF-AdvancedServerFullTags"))) super.filterTagList(template, tags);
							break;
						}
					}
				}
				if (StringUtils.isBlank(pageStaticData.getTemplateId())) {
					for (int x = 0; x < super.pdfTemplates.size(); ++x) {
						PDFTemplate template = super.pdfTemplates.get(x);
						if (StringUtils.isBlank(template.getConditions())) {
							pageStaticData.setTemplateId(template.getId());
							pageStaticData.setPdfGroupId(pdfRequest.getTemplateId());
							if (!"true".equalsIgnoreCase(InitServlet.getSystemParam("PDF-AdvancedServerFullTags"))) super.filterTagList(template, tags);
							break;
						}
					}
				}
			//} else if (super.pdfTemplates.size() == 1) {
			//	PDFTemplate template = super.pdfTemplates.get(0);
			//	pageStaticData.setTemplateId(template.getId());
			//	pageStaticData.setPdfGroupId(pdfRequest.getTemplateId());
			//	if (!"true".equalsIgnoreCase(InitServlet.getSystemParam("PDF-AdvancedServerFullTags"))) super.filterTagList(template, tags);
			} else {
				pageStaticData.setTemplateId(pdfRequest.getTemplateId());
			}
			
			pageStaticData.setTemplateListTags(listTags);
			pageStaticData.setTemplateTags(tags);

			if (StringUtils.isNotBlank(pageStaticData.getTemplateId())) pageStaticDataList.add(pageStaticData);

			setStatus(request, "Generating Tag List..", (double) (++c) / properties.size());

		}

		pdfRequest.setPageStaticDataList(pageStaticDataList);

		setStatus(request, "Generating Tag List completed", 0.5d);

	}

	public void populateSpecificTags(HttpServletRequest request, Map<String,String> tags, List<TemplateListTag> listTags, PropertyEntity property, PDFRequest pdfRequest) {
		// Child class can override, pdfRequest is passed so that if needed the template name can be overridden by the child class if needed
	}

	@Override
	public int getProcessSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getProcessedCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getErrorCount() {
		// TODO Auto-generated method stub
		return 0;
	}

}