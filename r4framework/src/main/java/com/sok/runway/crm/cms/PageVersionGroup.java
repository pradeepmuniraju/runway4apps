package com.sok.runway.crm.cms;
import com.sok.runway.*;

import com.sok.runway.crm.*;
import com.sok.runway.security.*;
import com.sok.runway.crm.activity.Logable;
import com.sok.runway.crm.factories.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.service.cms.WebsiteService;
import com.sok.service.crm.UserService;

import javax.servlet.ServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.sql.Connection;
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * A class to hold information about a site publication. 
 * The notes will either be the same as on an individual page (if single publish) or entered notes (if bulk publish)
 * 
 * @author Irwan
 * @version 0.1
 */
@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PageVersionGroup extends SecuredEntity implements Logable {
   private static final Logger logger = LoggerFactory.getLogger(PageVersionGroup.class);
   private static final String ENTITY_NAME = "PageVersionGroup";
   
   public PageVersionGroup() {
	   super((Connection) null, "cms3/PageVersionGroupView");
   }
   
	public PageVersionGroup(Connection con) {
		
		super(con, "cms3/PageVersionGroupView");
	}
	
	public PageVersionGroup(ServletRequest request) {
		super(request, "cms3/PageVersionGroupView");
		populateFromRequest(request.getParameterMap());
	}
   
	public PageVersionGroup(Connection con, String pageVersionGroupID) {	
	   this(con);
	   load(pageVersionGroupID);
	}
	
   public String getEntityName() {
	   return ENTITY_NAME;
   }
   
   @Override
   public boolean canEdit() {
	   return WebsiteService.getInstance().canEditWebsite(getConnection(), getCurrentUser(), getField("CMSID"));
   }

	@Override
	public boolean delete() {
		logger.debug("delete()");
		/*
		List<PageContent> list = WebsiteService.getInstance().getPageContents(getConnection(), UserService.getInstance().getSimpleUser(getConnection(), getCurrentUser().getUserID()), getField("CMSID"), getField("LinkID"), getPageID());
		for(PageContent pc: list) {
			pc.setCurrentUser(getCurrentUser());
			pc.setConnection(getConnection());
			logger.debug("pc.delete({})", pc.getPrimaryKey());
			if(!pc.delete()) {
				logger.error("Failed removing Page Content with error " + pc.getRecord().getError());
				return false;
			}
		}
		if(getField("PageMetaID").length()!=0) { 	
			PageMeta pm = new PageMeta(getConnection(), getField("PageMetaID"));
			pm.setCurrentUser(getCurrentUser());
			if(pm.isLoaded() && !pm.delete()) {
				logger.error("Failed removing Page Meta with error " + pm.getRecord().getError());
				return false;
			}
		}
		logger.debug("Content and Meta removed, deleting Page");
		*/
		return super.delete();
	}
	
	
	public Validator getValidator() {
		
		Validator val = Validator.getValidator("Page");
		
	   if (val == null) {
		   val = new Validator();
		   val.addMandatoryField("PageID","SYSTEM ERROR - PageID");
		   val.addMandatoryField("Title","Page must have a Title");
		   Validator.addValidator("Page", val);
	   }
	   
	   return val;
	}
	
}