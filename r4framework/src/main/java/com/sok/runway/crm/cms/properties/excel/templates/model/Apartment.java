package com.sok.runway.crm.cms.properties.excel.templates.model;

public class Apartment {
	private String buildingID;
	private String buildingName;
	private String levelName;
	private String suburb;
	private String lotNumber;
	private String landSize;
	private String size;
	private String internalArea;
	private String externalArea;
	private String facade;
	private String houseName;
	private double beds;
	private double baths;
	private double garage;
	private String sqs;
	
	private String status;
	
	private double housePrice;	
	private double landPrice;
	private double facadePrice;
	private double totalPrice;
	
	private String regionName;
	private String locationName;
	
	private double siteCosts;
	private double bushfire;
	private double guidelines;
	
	private String groupName = "";
	private String salesRepName = "";
	private String salesAgentName = "";	
	private String exclusiveDate = "";
	
	public String getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(String buildingID) {
		this.buildingID = buildingID;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getSuburb() {
		return suburb;
	}
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	public String getApartmentNumber() {
		return lotNumber;
	}
	public void setApartmentNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public String getApartmentSize() {
		return landSize;
	}
	public void setApartmentSize(String landSize) {
		this.landSize = landSize;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getFacade() {
		return facade;
	}
	public void setFacade(String facade) {
		this.facade = facade;
	}
	public String getHouseName() {
		return houseName;
	}
	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}
	public double getBeds() {
		return beds;
	}
	public void setBeds(double beds) {
		this.beds = beds;
	}
	public double getBaths() {
		return baths;
	}
	public void setBaths(double baths) {
		this.baths = baths;
	}
	public double getGarage() {
		return garage;
	}
	public void setGarage(double garage) {
		this.garage = garage;
	}
	public String getSqs() {
		return sqs;
	}
	public void setSqs(String sqs) {
		this.sqs = sqs;
	}
	public double getHousePrice() {
		return housePrice;
	}
	public void setHousePrice(double housePrice) {
		this.housePrice = housePrice;
	}
	public double getApartmentPrice() {
		return landPrice;
	}
	public void setApartmentPrice(double landPrice) {
		this.landPrice = landPrice;
	}
	public double getFacadePrice() {
		return facadePrice;
	}
	public void setFacadePrice(double facadePrice) {
		this.facadePrice = facadePrice;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public double getSiteCosts() {
		return siteCosts;
	}
	public void setSiteCosts(double siteCosts) {
		this.siteCosts = siteCosts;
	}
	public double getBushfire() {
		return bushfire;
	}
	public void setBushfire(double bushfire) {
		this.bushfire = bushfire;
	}
	public double getGuidelines() {
		return guidelines;
	}
	public void setGuidelines(double guidelines) {
		this.guidelines = guidelines;
	}
	public void setInternalArea(String internalArea) {
		this.internalArea = internalArea;
		
	}
	public void setExternalArea(String externalArea) {
		this.externalArea = externalArea;
	}
	public String getInternalArea() {
		return internalArea;
	}
	public String getExternalArea() {
		return externalArea;
	}
	public String getSalesRepName() {
		return salesRepName;
	}
	public void setSalesRepName(String salesRepName) {
		this.salesRepName = salesRepName;
	}
	public String getSalesAgentName() {
		return salesAgentName;
	}
	public void setSalesAgentName(String salesAgentName) {
		this.salesAgentName = salesAgentName;
	}
	public String getExclusiveDate() {
		return exclusiveDate;
	}
	public void setExclusiveDate(String exclusiveDate) {
		this.exclusiveDate = exclusiveDate;
	}
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}