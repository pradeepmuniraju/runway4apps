package com.sok.runway.crm.profile;

import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;

public class Question extends SecuredEntity {
   
   public Question(Connection con) {
      super(con, "QuestionView");
   }
	
	public Question(ServletRequest request) {
	    super(request, "QuestionView");
       populateFromRequest(request.getParameterMap());
	}
   
   public Question(Connection con, String questionID) {
      this(con);
      load(questionID);
   }
        
   public Validator getValidator() {
      Validator val = Validator.getValidator("Question");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("QuestionID","SYSTEM ERROR - QuestionID");
         val.addMandatoryField("GroupID","Question must have a security group selected");
         val.addMandatoryField("Label","Question must mave a label");
         Validator.addValidator("Question", val);
      }
      return val;
   }
}