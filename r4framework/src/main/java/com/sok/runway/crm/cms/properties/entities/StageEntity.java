package com.sok.runway.crm.cms.properties.entities;

import java.sql.Connection;

import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.generation.IllegalConfigurationException;
import com.sok.runway.crm.Product;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.cms.properties.Estate;
import com.sok.runway.externalInterface.beans.cms.properties.Stage;

public class StageEntity  extends Product {
	private static final Logger logger = LoggerFactory.getLogger(StageEntity.class);
	static final String VIEWSPEC = "properties/ProductStageView";
	public static final String ENTITY_NAME = "Stage";
	
	public StageEntity() {
		super(VIEWSPEC, (Connection)null);
	}
	public StageEntity(Connection con) {
		super(VIEWSPEC, con);
	}
	public StageEntity(ServletRequest request) {
		super(VIEWSPEC, request);
	}
	public StageEntity(Connection con, String estateID) {
		super(VIEWSPEC, con);
		loadFinal(estateID);
	}
	public StageEntity(Connection con, Stage e) {
		super(VIEWSPEC, con);
		
		if(StringUtils.isNotBlank(e.getStageID()))
			loadFinal(e.getStageID());
		
		setField("Name", e.getName());
		setField("Description", e.getDescription());
		setField("ProductNum", e.getProductNum());
		
		if(e.getEstate() != null) 
			setField("EstateProductID", e.getEstate().getEstateID());	//nb, this likely won't do anything. 
		BeanFactory.setTableDataFromAddress(e.getAddress(), getRecord(), "");
		
		if(e.getEstSettlementDate() != null)
			setField("EstSettlementDate", e.getEstSettlementDate());
	}
	
	@Override
	public String getEntityName() {
		return ENTITY_NAME;
	}
	
	public void beforeInsert() {
		setNewPrimaryKey();
		super.beforeInsert();
		if(getField("EstateProductID").length()==0) {
			logger.debug(((Object)this).toString());
			throw new RuntimeException("EstateProductID is required for Stage creation");
		}
		insertProductVariation(getConnection(), getField("EstateProductID"), getProductID(), "Estate");
		copy(getConnection(), "ProductSecurityGroups", "ProductID", getField("EstateProductID"), getProductID(), getCurrentUser().getUserID());
	}
	
	@Override
	public void beforeSave() {
		super.beforeSave();
		setField("ProductType","Stage");
	}
	
	@Override
	public void postSave() {
		super.postSave();
		
		GenRow address = new GenRow();
		address.setTableSpec("Addresses");
		address.setParameter("ProductID", getPrimaryKey());
		address.setParameter("-select1","AddressID");
		
		address.doAction(GenerationKeys.SELECTFIRST);
		if(address.isSuccessful()) {
			address.setAction(GenerationKeys.UPDATE);
		} else {
			address.setToNewID("AddressID");
			address.setAction(GenerationKeys.INSERT);
		}
		if(setUpdatedFields(address)) { 
			address.doAction();
		}
	}
	
	private boolean setUpdatedFields(GenRow sub) {
		boolean has = false;
	
		int max = sub.getTableSpec().getFieldsLength() - 1;
		for(int i=0; ; i++) {
			String f = sub.getTableSpec().getFieldName(i);
			if(hasParameter(f) && !"GroupID".equals(f)) { //ProductDetails nor Addresses should have GroupID
				sub.setParameter(f, getField(f));;
				has = true;
			}
			if(i == max)
				break;
		}
		return has; 
	}
	
	/**
	 * @deprectaed
	 */
	public Stage external() {
		return BeanFactory.external(this);
	}
}
