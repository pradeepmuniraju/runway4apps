package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSmartCopy;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyEntity.ResizeOption;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.CommonUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.offline.ThreadManager;

public class WatermarkPdf extends AbstractPDFTemplate implements PDFConstants {

	public static final Logger logger = LoggerFactory.getLogger(WatermarkPdf.class);
	/*************************************/

	//TODO - test without this, it should not be true.
	private static final boolean dev = Boolean.TRUE;

	public static int pageCounter = 1;


	private class SinglePackage {
	}

	void renderPackage(com.itextpdf.text.Document document, SinglePackage p, String homePath) throws DocumentException {}
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {}


	//@Override
	public void renderPDF(final HttpServletRequest request, final HttpServletResponse response, final Document document, final PdfWriter writer) throws IOException, DocumentException
	{
		document.open();	
		final String homePath = request.getSession().getServletContext().getInitParameter("URLHome");


		String docIDs = request.getParameter("DocumentIDs");
		String waterMarkDocID = request.getParameter("WaterMarkID");
		String waterMarkPosition = request.getParameter("Position");
		logger.debug("docIDs: " + docIDs + "  ,  waterMarkDocID: " + waterMarkDocID + "  ,  waterMarkPosition: " + waterMarkPosition);

		final StringTokenizer docIDsTokens = new StringTokenizer(docIDs, ":");
		logger.debug("docIDsTokens.countTokens: " + docIDsTokens.countTokens());

		java.util.List<Future<String>> pList = new java.util.ArrayList<Future<String>>(docIDsTokens.countTokens());

		while (docIDsTokens.hasMoreElements()) {
			final String strDocID = (String)docIDsTokens.nextElement();
			logger.debug("strDocID: " + strDocID);

			if (StringUtils.isNotBlank(strDocID)) {
				pList.add(ThreadManager.startCallable(new Callable<String>() {
					public String call() {
						return strDocID;
					}
				}));
			}
		}

		int size = pList.size();
		logger.debug("starting up pdf page renderers.  size is: " + size);
		List<Future<PdfReader>> readerList = new ArrayList<Future<PdfReader>>(size);
		for (final Future<String> fsp : pList) {
			try {
				final String docID = fsp.get();
				readerList.add(ThreadManager.startCallable(new Callable<PdfReader>() {
					public PdfReader call() throws IOException, DocumentException {
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						Document document = new com.itextpdf.text.Document();
						setMargins(document);
						document.setMarginMirroring(true);
						PdfWriter writer = PdfWriter.getInstance(document, baos);
						document.open();

						//renderPackage(document, p, homePath);
						addImagesOrPDFDoc(request, document, writer, homePath, docID);

						document.close();

						String waterMarkDocID = request.getParameter("WaterMarkID");
						String waterMarkPosition = request.getParameter("Position");
						String waterMarkDocPath = "";
						GenRow grDoc = new GenRow();
						if (StringUtils.isNotBlank(waterMarkDocID)) {
							grDoc.setViewSpec("DocumentView");
							grDoc.setParameter("DocumentID", waterMarkDocID);
							grDoc.doAction(GenerationKeys.SELECTFIRST);
							if (StringUtils.isNotBlank(grDoc.getString("FilePath"))) {
								waterMarkDocPath = grDoc.getString("FilePath");
							} else {
								waterMarkDocPath = "";
							}
						}
						logger.debug("waterMarkDocID: " + waterMarkDocID + " , waterMarkPosition: " + waterMarkPosition + " , waterMarkDocPath: " + waterMarkDocPath);
						if (StringUtils.isNotBlank(waterMarkDocPath)) {
							addPDFWatermark(document, baos, homePath, waterMarkDocPath, waterMarkPosition);
						}
						//removePDFWatermark(baos, homePath, waterMarkDocPath, waterMarkPosition);

						return new PdfReader(baos.toByteArray());
					}
				}));
			} catch (java.util.NoSuchElementException e) {
				logger.debug("NoSuchElementException occurred. Ignored the exception. Message is: " + e.getMessage());
				//e.printStackTrace();
			}
			catch (java.util.concurrent.ExecutionException e) {
				logger.debug("Concurrent Exception occurred. Ignored the exception. Message is: " + e.getMessage());
				//e.printStackTrace();
			}
			catch (InterruptedException e) {
				//logger.error("PDF Generation Interupted : {}" , e.getMessage());
				logger.debug("Interrupted Exception occurred. Ignored the exception. Message is: " + e.getMessage());
				//e.printStackTrace();
			}
		}

		logger.debug("Rendering pages");
		logger.debug("readerList size is: " + readerList.size());
		PdfImportedPage importedPage;
		PdfContentByte cb = writer.getDirectContent();
		for(Future<PdfReader> freader: readerList) {
			try {
				PdfReader reader = freader.get();
				int pages = reader.getNumberOfPages();
				logger.debug("Total pages of Current Reader: " + pages);
				for (int i = 1; i <= pages; i++) {

					importedPage = writer.getImportedPage(reader, i);
					document.setPageSize(new Rectangle(importedPage.getWidth(), importedPage.getHeight()));
					document.newPage();
					cb.addTemplate(importedPage, 0, 0);	
				}
			} 
			catch (java.util.NoSuchElementException e) {
				logger.debug("NoSuchElementException occurred. Ignored the exception. Message is: " + e.getMessage());
			}
			catch (java.util.concurrent.ExecutionException e) {
				logger.debug("Concurrent Exception occurred. Ignored the exception. Message is: " + e.getMessage());
			}
			catch (InterruptedException e) {
				//logger.error("PDF Generation Interupted : {}" , e.getMessage());
				logger.debug("Interrupted Exception occurred. Ignored the exception. Message is: " + e.getMessage());
			}
		}
	}	

	/**
	 * Creates a PDF document.
	 * 
	 * @param source
	 *            the path to the original PDF document
	 * @param destination
	 *            the path to the new PDF document
	 * @throws DocumentException
	 * @throws IOException
	 */
	public void copyPDF(String source, String destination) throws DocumentException, IOException {
		String realPath = source;

		/*
		 * http://itextpdf.com/sandbox/acroforms/reporting/FlattenForm
		 */
		try {
			PdfReader reader = new PdfReader(realPath);
			PdfStamper stamper = new PdfStamper(reader,
					new FileOutputStream(destination));
			stamper.setFormFlattening(true);
			stamper.close();
			reader.close();
		} catch (Exception ee) {
			System.out.println("addExistingPDF. Exception Message is: " + ee.getMessage());
			ee.printStackTrace();
		}
	}

	/* 10-Apr-2014: Alternate implementation technique. Keeping it for reference. Can be deleted later on once current version is tested
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document, PdfWriter writer) throws IOException, DocumentException {

		String homePath = request.getSession().getServletContext().getInitParameter("URLHome");

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		setMargins(document);
		document.setMarginMirroring(true);
		writer = PdfWriter.getInstance(document, baos);

		document.open();
		addImagesAndPDFs(request, document, writer, homePath);

		String waterMarkDocID = request.getParameter("WaterMarkID");
		String waterMarkPosition = request.getParameter("Position");

		String waterMarkDocPath = "";
		GenRow grDoc = new GenRow();
		if (StringUtils.isNotBlank(waterMarkDocID)) {
			grDoc.setViewSpec("DocumentView");
			grDoc.setParameter("DocumentID", waterMarkDocID);
			grDoc.doAction(GenerationKeys.SELECTFIRST);

			if (StringUtils.isNotBlank(grDoc.getString("FilePath"))) {
				waterMarkDocPath = grDoc.getString("FilePath");
			} else {
				waterMarkDocPath = "";
			}
		}
		System.out.println("waterMarkDocID: " + waterMarkDocID + " , waterMarkPosition: " + waterMarkPosition + " , waterMarkDocPath: " + waterMarkDocPath);

		document.close();
		addPDFWatermark(baos, homePath, waterMarkDocPath, waterMarkPosition);

		document.open();
		PdfImportedPage importedPage;
		PdfContentByte cb = writer.getDirectContent();
		PdfReader reader = new PdfReader(baos.toByteArray());
		int pages = reader.getNumberOfPages();
		System.out.println("Test Logs 01 ==> pages: " + pages);
		for (int i = 1; i <= pages; i++) {
			System.out.println("Test Logs 02 ==> ");
			 document.newPage();
			 System.out.println("Test Logs 03 ");
			 importedPage = writer.getImportedPage(reader, i);
			 System.out.println("Test Logs 04 ==> ");
			 cb.addTemplate(importedPage, 0, 0);
			 System.out.println("Test Logs 05 ");
		}
	}*/


	/*
	// This method is not in use now.
	public void addImagesAndPDFs(HttpServletRequest request, com.itextpdf.text.Document document, PdfWriter writer, String homePath) {


		String docIDs = request.getParameter("DocumentIDs");
		String waterMarkDocID = request.getParameter("WaterMarkID");
		String waterMarkPosition = request.getParameter("Position");
		logger.debug("docIDs: " + docIDs + "  ,  waterMarkDocID: " + waterMarkDocID + "  ,  waterMarkPosition: " + waterMarkPosition);

		String strDocID = ""; 
		String strFileName = "";
		String strDocumentType = "";
		String strDocumentSubType = "";
		String strDocPath = "";

		StringTokenizer docIDsTokens = new StringTokenizer(docIDs, ":");
		while (docIDsTokens.hasMoreElements()) {

			strDocID = (String)docIDsTokens.nextElement();
			logger.debug("strDocID: " + strDocID);

			GenRow grDoc = new GenRow();
			if (StringUtils.isNotBlank(strDocID)) {
				grDoc.setViewSpec("DocumentView");
				grDoc.setParameter("DocumentID", strDocID);
				grDoc.doAction(GenerationKeys.SELECTFIRST);

				if (StringUtils.isNotBlank(grDoc.getString("FileName"))) {
					strFileName = grDoc.getString("FileName");
				} else {
					strFileName = "";
				}

				if (StringUtils.isNotBlank(grDoc.getString("DocumentType"))) {
					strDocumentType = grDoc.getString("DocumentType");
				} else {
					strDocumentType = "";
				}

				if (StringUtils.isNotBlank(grDoc.getString("DocumentSubType"))) {
					strDocumentSubType = grDoc.getString("DocumentSubType");
				} else {
					strDocumentSubType = "";
				}

				if (StringUtils.isNotBlank(grDoc.getString("FilePath"))) {
					strDocPath = grDoc.getString("FilePath");
				} else {
					strDocPath = "";
				}

				logger.debug("strDocID: " + strDocID +  " , strFileName: " + strFileName + " , strDocumentType: " + strDocumentType + " , strDocumentSubType: " + strDocumentSubType + "  ,  strDocPath: " + strDocPath);

				if (StringUtils.isNotBlank(strDocPath)) {

					if (strDocPath.toLowerCase().endsWith("pdf")) {

				        // String strPDFPath = "files/test/test_large.pdf";
						addExistingPDF(document, writer, strDocPath);

					} else {

						try {
							PdfPTable pageTable = new PdfPTable(1);
							pageTable.setWidthPercentage(100f);
							pageTable.getDefaultCell().setPadding(0);
							pageTable.getDefaultCell().setBorder(0);
							pageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
							pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

							pageTable.addCell(addNewImageOnPDF(homePath, strDocPath));

							if (pageCounter > 1) {
								document.setPageSize(new Rectangle(PageSize.A4.getWidth(), PageSize.A4.getHeight()));
								document.newPage();
								pageCounter++;
							} else {
								pageCounter++;
							}
							document.add(pageTable);
						} catch (Exception ee) {
							System.out.println("Exception Message is: " + ee.getMessage());
							ee.printStackTrace();
						}
					}
				}
			}
		}

		System.out.println("Total Page count for Final Output PDF: " + writer.getPageNumber());


	}*/


	public void addImagesOrPDFDoc(HttpServletRequest request, com.itextpdf.text.Document document, PdfWriter writer, String homePath, String docID) {

		logger.debug("addImagesOrPDFDoc ==> docID: " + docID);

		String strFileName = "";
		String strDocumentType = "";
		String strDocumentSubType = "";
		String strDocPath = "";

		GenRow grDoc = new GenRow();
		if (StringUtils.isNotBlank(docID)) {
			grDoc.setViewSpec("DocumentView");
			grDoc.setParameter("DocumentID", docID);
			grDoc.doAction(GenerationKeys.SELECTFIRST);

			if (StringUtils.isNotBlank(grDoc.getString("FileName"))) {
				strFileName = grDoc.getString("FileName");
			} else {
				strFileName = "";
			}

			if (StringUtils.isNotBlank(grDoc.getString("DocumentType"))) {
				strDocumentType = grDoc.getString("DocumentType");
			} else {
				strDocumentType = "";
			}

			if (StringUtils.isNotBlank(grDoc.getString("DocumentSubType"))) {
				strDocumentSubType = grDoc.getString("DocumentSubType");
			} else {
				strDocumentSubType = "";
			}

			if (StringUtils.isNotBlank(grDoc.getString("FilePath"))) {
				strDocPath = grDoc.getString("FilePath");
			} else {
				strDocPath = "";
			}

			logger.debug("docID: " + docID +  " , strFileName: " + strFileName + " , strDocumentType: " + strDocumentType + " , strDocumentSubType: " + strDocumentSubType + "  ,  strDocPath: " + strDocPath);

			if (StringUtils.isNotBlank(strDocPath)) {

				if (strDocPath.toLowerCase().endsWith("pdf")) {

					// String strPDFPath = "files/test/test_large.pdf";
					addExistingPDF(document, writer, strDocPath);

				} else {

					try {

						String waterMarkPosition = request.getParameter("Position");

						PdfPTable pageTable = new PdfPTable(1);
						pageTable.setWidthPercentage(100f);
						pageTable.getDefaultCell().setPadding(0);
						pageTable.getDefaultCell().setBorder(0);

						int alignH = Element.ALIGN_CENTER;
						int alignV = Element.ALIGN_BOTTOM;

						if ("TL".equalsIgnoreCase(waterMarkPosition) || "TR".equalsIgnoreCase(waterMarkPosition)) {
							//alignH = Element.ALIGN_TOP;
							alignV = Element.ALIGN_TOP;
						}
						pageTable.getDefaultCell().setVerticalAlignment(alignH);
						pageTable.getDefaultCell().setHorizontalAlignment(alignV);
						//pageTable.getDefaultCell().setBackgroundColor(BaseColor.RED);

						boolean isLandScape = false;
						String imgStr = homePath + "/" + strDocPath;
						Image imgHero = Image.getInstance(InitServlet.getRealPath(strDocPath));
						if (imgHero.getWidth() > imgHero.getHeight()) {
							isLandScape = true;
							logger.debug("Switched to LANDSCAPE Mode  for imgStr: " + imgStr + "  ,  isLandScape: " + isLandScape);
						}

						if (pageCounter > 1) {
							if (isLandScape) {
								// document.setPageSize(new Rectangle(PageSize.A4_LANDSCAPE.getWidth(), PageSize.A4_LANDSCAPE.getHeight()));
								// document.setPageSize(document.getPageSize().rotate());
								document.setPageSize(new Rectangle(842, 595));

							} else {
								document.setPageSize(new Rectangle(PageSize.A4.getWidth(), PageSize.A4.getHeight()));
							}
							document.newPage();
							pageCounter++;
						} else {
							pageCounter++;
						}


						PdfPCell cell = new PdfPCell ();
						cell.setMinimumHeight (document.getPageSize ().getHeight () - 36.0f - 36.0f);
						cell.setVerticalAlignment (alignV);
						if (isLandScape) {
							cell.setFixedHeight(500f);
						} else {
							cell.setFixedHeight(800f);
						}
						cell.setPadding(0);
						cell.setBorder(0);
						cell.addElement (addNewImageOnPDF(document, homePath, strDocPath));

						pageTable.addCell(cell);

						document.add(pageTable);

					} catch (Exception ee) {
						logger.debug("Exception Message is: " + ee.getMessage());
						ee.printStackTrace();
					}
				}
			}
		}

		logger.debug("addImagesOrPDFDoc  ==>  docID is: " + docID + " , Total Pages Added: " + writer.getPageNumber());

	}	


	public PdfPTable addNewImageOnPDF(Document document, String homePath, String imagePath)
	{
		PdfPTable tbl = new PdfPTable(1); 
		try {
			tbl.setWidthPercentage(100f);
			tbl.getDefaultCell().setPadding(20);
			tbl.getDefaultCell().setBorder(0);
			//tbl.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
			//tbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_BOTTOM);
			//tbl.getDefaultCell().setBackgroundColor(BaseColor.GREEN);
			String imgStr = homePath + "/" + imagePath;
			java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));

			try {
				Image imgHero = Image.getInstance(InitServlet.getRealPath(imagePath));
				//Image imgHero = CommonUtil.createImage(PropertyEntity.resizeImage(imagePath, 590, 840, ResizeOption.Fit,"jpg"));
				imgHero.scaleToFit(100, 100);
				tbl.addCell(imgHero);

				/*Image imgHero = Image.getInstance(InitServlet.getRealPath(imagePath));
				float tmpWidth = imgHero.getWidth(); 
				float tmpHeight = imgHero.getHeight();
	      		float ratioW = 595 / tmpWidth;
	      		float ratioY = 842 / tmpHeight;
	      		float ratioWidth = tmpWidth*ratioW;
	            float ratioHeight = tmpHeight*ratioY;
	            System.out.println("addNewImageOnPDF ==> ratioWidth: " + ratioWidth + "  ,  ratioHeight: " + ratioHeight);
	            imgHero = CommonUtil.createImage(PropertyEntity.resizeImage(imagePath, 590, 840, ResizeOption.Trim,"jpg"));
	            imgHero.scaleToFit(400, 400);
				tbl.addCell(imgHero);*/

			} catch(Exception eee) {
				System.out.println("Exception occurred while rendering the Image. Image Path is: " + imgStr  + "    ,  Exception Message is: " + eee.getMessage());
				eee.printStackTrace();
				tbl.addCell(BLANK);
			}
		}
		catch(Exception ee) {
			System.out.println("Exception occurred during rendering of addNewImageOnPDF. Message is: " + ee.getMessage());
			ee.printStackTrace();
		}
		return tbl;
	}


	public  void addExistingPDF(Document document, PdfWriter writer, String pdfFilePath) {

		try {
			String source = InitServlet.getRealPath(pdfFilePath);
			String realPath = InitServlet.getRealPath(pdfFilePath + "_" + KeyMaker.generate(6) + ".pdf");
			copyPDF(source, realPath);
			logger.debug("FilePath 01 is: " + source);

			document.open();
			PdfContentByte cb = writer.getDirectContent();
			try { 
				PdfReader reader = new PdfReader(realPath);
				reader = unlockPdf(reader);
				logger.debug("Reading PDF: " + realPath + "  ,  max is: "+ reader.getNumberOfPages());
				PdfImportedPage page = null;
				for(int i = 1; i<=reader.getNumberOfPages(); i++) {
					page = writer.getImportedPage(reader, i);
					logger.debug("Adding page no: "+ page.getPageNumber() + "  ,  Type: " + page.getType() + "  , page.getWidth: " + page.getWidth() + " , page.getHeight: " + page.getHeight());

					if (pageCounter > 1) {
						document.setPageSize(new Rectangle(page.getWidth(), page.getHeight()));
						document.newPage();
						pageCounter++;
					} else {
						pageCounter++;
					}

					cb.addTemplate(page, 0, 0);
				}
			} catch (Exception ee) {
				logger.error("addExistingPDF. Could not load pdf from path : " + realPath, ee);
				System.out.println("addExistingPDF. Exception Message is: " + ee.getMessage());
				ee.printStackTrace();
			}
			(new File(realPath)).delete();
		} catch (Exception ee) {
			System.out.println("addExistingPDF. Exception Message is: " + ee.getMessage());
			ee.printStackTrace();
		}	
	}	


	public void addPDFWatermark(Document document, ByteArrayOutputStream baos, String homePath, String waterMarkImgPath, String waterMarkPosition)
	{
		try
		{
			PdfReader pdfReader = new PdfReader(baos.toByteArray());
			PdfStamper stamp = new PdfStamper(pdfReader, baos);
			int n = pdfReader.getNumberOfPages();

			int i = 1;
			//PdfContentByte under;
			PdfContentByte over;

			//String imgStr = homePath + "/" + waterMarkImgPath;
			logger.debug("Rendering Watermark Image. Position is: " + waterMarkPosition + " , waterMarkImgPath: " + waterMarkImgPath);
			Image imgWatermark = null;
			try
			{
				//					String tmpImg = PropertyEntity.resizeImage(waterMarkImgPath, 300, 168, PropertyEntity.ResizeOption.Trim, dev);
				//					imgWatermark = Image.getInstance(tmpImg);
				//					imgWatermark.scaleToFit(200, 112);
				//imgWatermark.scaleToFit(225, 126);
				//imgWatermark.scaleToFit(250, 140);

				/*float docPageWidth = document.getPageSize().getWidth();
					float docPageHeight = document.getPageSize().getHeight();
					float startX = 360f + getWidthFraction(docPageWidth); // Default is BR 
					float startY = 80f + getWidthFraction(docPageHeight); // Default is BR
				 */

				imgWatermark = CommonUtil.createImage(PropertyEntity.resizeImage(waterMarkImgPath, 300, 168, PropertyEntity.ResizeOption.Trim, dev));
				imgWatermark.scaleToFit(200, 112);

				float startX = 360f; // Default is BR 
				float startY = 80f; // Default is BR
				if ("BL".equalsIgnoreCase(waterMarkPosition)) {
					startX = 20f; 
					startY = 80f;
				} else if ("TR".equalsIgnoreCase(waterMarkPosition)) {
					startX = 360f; 
					startY = 710f;
				} else if ("TL".equalsIgnoreCase(waterMarkPosition)) {
					startX = 20f; 
					startY = 710f;
				}
				imgWatermark.setAbsolutePosition(startX, startY);

				logger.debug("Rendering Watermark Image. Resized Watermark waterMarkImgPath is: " + waterMarkImgPath  + "     ,    startX: " + startX  + "   ,  startY: " + startY);


			}
			catch(Exception ee)
			{
				imgWatermark = null;
				System.out.println("Exception Occurred. Message is: " + ee.getMessage());
				ee.printStackTrace();
			}		

			while (i <= n) 
			{
				/*
					// Watermark under the existing page
					PdfContentByte cb = stamp.getOverContent(i);
					under = stamp.getUnderContent(i);
					cb.saveState();
					PdfGState gs = new PdfGState();
					gs.setFillOpacity(0.8f);
					under.setGState(gs);
					if (imgWatermark!=null) {	
						under.addImage(imgWatermark);
					}*/

				logger.debug("Rendering Watermark Image on page " + i + "  ,  imgWatermark is: " + imgWatermark);

				// Text over the existing page
				PdfContentByte cb = stamp.getOverContent(i); 
				over = stamp.getOverContent(i);
				cb.saveState();
				PdfGState gs = new PdfGState();
				gs.setFillOpacity(0.7f);
				over.setGState(gs);
				if (imgWatermark!=null) {	
					over.addImage(imgWatermark);
				}	

				cb.restoreState();
				i++;
			}

			stamp.close();
		}
		catch(Exception ee)
		{
			System.out.println("Exception occurred while rendering Overlays. Exception message is: " + ee.getMessage());
			ee.printStackTrace();
		}
	}	

	public static PdfReader unlockPdf(PdfReader reader) { 
		if (reader == null) { 
			return reader; 
		} 
		try { 
			java.lang.reflect.Field f = reader.getClass().getDeclaredField("encrypted"); 
			f.setAccessible(true); 
			f.set(reader, false); 
		} catch (Exception e) { // ignore
			System.out.println("WatermarkPdf.java ==> Exception Occurred while unlocking the PDF. Message is: " + e.getMessage());
			e.printStackTrace();
		} 
		return reader; 
	} 

	public float getWidthFraction (float docPageWidth) {
		float num = 0f;
		if (docPageWidth > PageSize.A4.getWidth()) { 
			num =  docPageWidth - PageSize.A4.getWidth();
		}
		return num;
	}

	public float getHeightFraction (float docPageHeight) {
		float num = 0f;
		if (docPageHeight > PageSize.A4.getHeight()) { 
			num =  docPageHeight - PageSize.A4.getHeight();
		}
		return num;
	}

	public String getPdfSimpleName() {
		return "Watermark PDF";
	}


	public void setMargins(Document document) {
		document.setMargins(0, 0, 0, 0);
	}
}