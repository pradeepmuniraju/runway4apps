package com.sok.runway.crm.cms.properties.pdf;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sog.pdfserver.client.api.request.PDFRequest;
import com.sok.framework.StringUtil;

public class AdvancedPropertyPDFGenerator {
	public static final String GENERIC_PACKAGE = "com.sok.runway.crm.cms.properties.pdf.advanced.";
	public static final String SPECIFIC_PACKAGE = "com.sok.clients.pdf.advanced.";

	public static final String GENERIC_CLASS = "com.sok.runway.crm.cms.properties.pdf.advanced.PDFProduct";// Suffix List or Detail

	private static final Logger logger = LoggerFactory.getLogger(AdvancedPropertyPDFGenerator.class);

	public static void generate(HttpServletRequest request, HttpServletResponse response, PDFRequest pdfRequest) {

		try {
			AdvancedPDFProcessor pdfProcessor = null;

			String pdfGroup = StringUtils.deleteWhitespace(StringUtil.capitalize(StringUtils.defaultIfBlank(request.getParameter("-group"), "DETAIL")));
			String className = StringUtils.deleteWhitespace(("PDF" + StringUtil.capitalize(StringUtils.defaultIfBlank(request.getParameter("-product"), "House And Land")) + pdfGroup));

			logger.debug(" className: " + className);

			try {
				pdfProcessor = (AdvancedPDFProcessor) (Class.forName(SPECIFIC_PACKAGE + className).newInstance());

			} catch (Exception e) {
				logger.error("Unable to load specific pdf template. Using default " + className + ". Exception: " + e);
				try {
					pdfProcessor = (AdvancedPDFProcessor) (Class.forName(GENERIC_PACKAGE + className).newInstance());
				} catch (Exception ex) {
					logger.error("Unable to load generic pdf template. Using default " + className + ". Exception: " + ex);
					pdfProcessor = (AdvancedPDFProcessor) (Class.forName(GENERIC_CLASS + pdfGroup).newInstance()); // PDFProductDetail or PDFProductList
				}
			}

			pdfProcessor.generatePDF(request, response, pdfRequest);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error generating pdf template Exception: " + e);

		}
	}

}