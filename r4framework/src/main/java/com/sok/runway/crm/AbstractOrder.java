package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.runway.crm.factories.*;
import com.sok.runway.security.AuditEntity; 
import com.sok.service.crm.cms.CampaignService;
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;

import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;

import java.sql.*;
import java.util.*;

public abstract class AbstractOrder extends ProfiledEntity implements StatusHistory, DocumentLinkable {

	protected static final String ANSWER_VIEWSPEC = "answers/OrderAnswerView";
	
   public AbstractOrder(Connection con, String viewSpec) {
      super(con, viewSpec);
   }
	
	public AbstractOrder(ServletRequest request, String viewSpec) {
	    super(request, viewSpec);
	}
	
	public String getAnswerViewSpec() {
		return ANSWER_VIEWSPEC;
	}

	public String getStatusHistoryViewName() {
		return "OrderStatusView";
	}

	public String getStatusHistoryID() {
		return getParameter("OrderStatusID");
	}

	public void setStatusHistoryID(String id) {
		setParameter("OrderStatusID", id);
	}

	public String getStatusID() {
		return getParameter("StatusID");
	}
	
	public ErrorMap update() {
		if (getParameter("StatusID") != null && getParameter("StatusID").length() != 0) {
			
			StatusUpdater su = StatusUpdater.getStatusUpdater();
			if (su.isNewStatus(this)) {
				setParameter("OrderStatusID",KeyMaker.generate());
				su.insertStatus(this);
			}
		}
		return super.update();
	}
	
	public ErrorMap insert() {
		JSPLogger.debug(this, "insert()");
		if(StringUtils.isNotBlank(getParameter("ProductID")) && StringUtils.isBlank(getParameter("CampaignID")) && "true".equals(InitServlet.getSystemParam("PropertyMode"))) {
			JSPLogger.debug(this, "have productId={} and no campaign", getParameter("ProductID"));
			GenRow pqi = new GenRow();
			pqi.setConnection(getConnection());
			pqi.setTableSpec("CampaignProducts");
			pqi.setParameter("-select1","CampaignID");
			pqi.setParameter("-select2","ProductID");
			pqi.setParameter("ProductID", getParameter("ProductID"));
			pqi.doAction(GenerationKeys.SELECTFIRST);
			
			if(pqi.isSuccessful() && pqi.isSet("CampaignID")) {
				setParameter("CampaignID", pqi.getData("CampaignID"));
			} else {
				JSPLogger.debug("could not find pqi record with error {}", pqi.getError());
			}
		} else {
			JSPLogger.debug(this, "either no product={} or campaign={}", getParameter("ProductID"), getParameter("CampaignID"));
		}
		
		boolean insertStatus = false;
		if (getParameter("StatusID").length() != 0) {
			if (getParameter("OrderStatusID").length() == 0) {
				setParameter("OrderStatusID",KeyMaker.generate());
			}
			insertStatus = true;
		}
		ErrorMap map = super.insert();
		
		if (map.isEmpty() && insertStatus) {
			StatusUpdater su = StatusUpdater.getStatusUpdater();
			su.insertStatus(this);
		}
		
		return map;
	}
	
	/**
	 * Deletes Child Data of Order after RunwayEntity has removed the order. 
	 * Profile data will be removed by ProfiledEntity. 
	 */
	public boolean delete() {
		if (super.delete()) {
	 		
			GenRow list = new GenRow(); 
	 		list.setTableSpec("OrderItems");
	 		list.setParameter("-select1","OrderItemID");
	 		list.setParameter("-select2","OrderID"); 
	 		list.setConnection(getConnection()); 
	 		list.setParameter(getPrimaryKeyName(),getPrimaryKey());
	 		list.getResults(); 
	 		
	 		AuditEntity child = new OrderItem(getConnection()); 
	 		child.setCurrentUser(getCurrentUser());
	 		
	 		while (list.getNext()) { 
	 			child.load(list.getData("OrderItemID")); 
	 			child.delete(); 
	 		}
			
	 		list.clear(); 
	 		list.setTableSpec("Notes");
	 		list.setParameter("-select1","NoteID");
	 		list.setParameter("-select2","OrderID"); 
	 		list.setParameter(getPrimaryKeyName(),getPrimaryKey());
	 		list.getResults(); 
	 		
	 		child = new Note(getConnection());
	 		child.setCurrentUser(getCurrentUser());
	 		
	 		while (list.getNext()) { 
	 			child.load(list.getData("NoteID")); 
	 			child.delete(); 
	 		}
	 		
	 		GenRow del = new GenRow();
	 		del.setConnection(getConnection()); 
	 		del.setTableSpec("OrderContacts");
	 		del.setParameter(GenerationKeys.UPDATEON + getPrimaryKeyName(), getPrimaryKey());
	 		del.setAction(com.sok.framework.generation.GenerationKeys.DELETEALL); 
	 		del.doAction(); 
			
			del.setTableSpec("OrderContacts");
			del.doAction();
			
			del.setTableSpec("OrderContacts");
			del.doAction();
			
			del.setTableSpec("LinkedDocuments");
			del.doAction();
			
			del.setTableSpec("OrderCompanies");
			del.doAction();
			
			del.setTableSpec("OrderStatus");
			del.doAction();
	 		
	 		return true; 
		}
		return false; 
	} 
	
   /**
    *    Linkable Interface
    */
   public String getLinkableTitle() {      
      if (getField("Name").length() != 0 ) {
         return getField("OrderNum") + " - " + getField("Name");
      }
      else {
         return getField("OrderNum");
      }
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs() {
      return getLinkableDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters);
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs() {
      return getDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
   }     
   
   /**
    *    DocumentLinkable Interface
    */
   public LinkableDocument getLinkableDocument() {
      LinkableDocument doc = new LinkedDocument(getConnection());
      doc.setCurrentUser(getCurrentUser());
      return doc;
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public void linkDocument(String documentID) {
      LinkableDocumentFactory.linkDocument(this, documentID);
   }
}