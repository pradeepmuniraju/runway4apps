package com.sok.runway.crm.searchbuilder;

public class ElementIdUtil {
    private static final String SEPARATOR = "-";
    private static final int PREFIX = 1;
    private static final int FIRST_INDEX = 2;
    private static final int SECOND_INDEX = 3;
    
    /**
     * <p>Creates an ID for an element or form parameter name with the following 
     * format: prefix-firstIndex-secondIndex.</p>
     *
     * @param prefix can be an element ID prefix, or the name of a form parameter
     * @param firstIndex is the first index value to append to the ID
     * @param secondIndex is the second index value to append to the ID
     * @return String with format: prefix-firstIndex-secondIndex
     */
    public static String createID(String prefix, String firstIndex, String secondIndex) {
       return appendIndex(appendIndex(prefix, firstIndex), secondIndex);
    }

    /**
     * <p>Creates an ID for an element or form parameter name with the following 
     * format: prefix-index</p>
     *
     * @param prefix can be an element ID prefix, or the name of a form parameter
     * @param no is the index to append to the ID
     * @return String with format: prefix-index
     */
    public static String createID(String prefix, String index) {
        return appendIndex(prefix, index);
    }
    
    /**
     * <p>Creates an ID for an element or form parameter name with the following 
     * format: prefix-index_1-index_2-...-index_n</p>
     *  
     * @param prefix
     * @param indeces array of indices
     * @return return a string of the form: prefix-index_1-index_2-...-index_n
     */
    public static String createID(String prefix, String[] indeces) {
        String id = "";
        
        if(indeces != null) {
            id = appendIndex(prefix, "");
            for(String index : indeces) {
                id = appendIndex(id, index);
            }
        }
        
        return id;
    }
    
    /**
     * <p>Append <code>index</code> to <code>prefix</code> separated by the 
     * <code>SEPARATOR</code>. 
     */
    private static String appendIndex(String prefix, String index) {
        return (((prefix != null) ? prefix : "") + SEPARATOR + ((index != null) ? index : ""));
    }
    
    /**
     * @param id is in the form: prefix-index_1-index_2-...-index_n
     * @return the sequence of indeces in the id as a String array
     */
    public static String[] parseIndeces(String id) {
        String indeces[] = null;
        
        if(id != null && id.length() > 0) {
            String[] tokens = id.split(SEPARATOR);
            
            int indecesLength = tokens.length - 1;    // Ignore prefix token
            indeces = new String[indecesLength];

            /*
             * tokenIndex starts at 1 because we ignore the prefix token
             */
            for(int indecesIndex = 0, tokenIndex = 1; 
                    indecesIndex < indecesLength; 
                    indecesIndex++, tokenIndex++) {
                indeces[indecesIndex] = tokens[tokenIndex];
            }
        }
        
        return indeces;
    }
    
    /**
     * 
     * @param id of form prefix-index_1-index_2-...-index_n
     * @return the prefix of the id
     */
    public static String parsePrefix(String id) {
        return parseNthToken(id, PREFIX);
    }
    
    /**
     * 
     * @param id of form prefix-index_1-index_2-...-index_n
     * @return the number after the prefix
     */
    public static String parseFirstIndex(String id) {
        return parseNthToken(id, FIRST_INDEX);
    }
    
    /**
     * 
     * @param id of form prefix-index_1-index_2-...-index_n
     * @return the number after the prefix
     */
    public static String parseSecondIndex(String id) {
        return parseNthToken(id, SECOND_INDEX);
    }
    
    /**
     * 
     * @param id of form prefix-index_1-index_2-...-index_n
     * @param tokenNumber the nth token in id (1,2,...,n)
     * @return the tokens in a String array
     */
    public static String parseNthToken(String id, int tokenNumber) {
        String token = "";
        
        if(id != null && id.length() > 0) {
            String[] tokens = id.split(SEPARATOR);
            token = parseNthToken(tokens, tokenNumber);
        }
        
        return token;        
    }
    
    /**
     * 
     * @param tokens a String array of tokens 
     * @param tokenNumber the nth token in id (1,2,...,n)
     * @return the nth token in the tokens String array
     */
    private static String parseNthToken(String[] tokens, int tokenNumber) {
        String token = "";
        
        if(tokens != null && tokenNumber > 0) {
            
            int tokenIndex = tokenNumber - 1;
            
            if(tokenIndex < tokens.length) {
                token = tokens[tokenIndex];
            }
        }
        
        return token;
    }
}
