package com.sok.runway.crm.cms.properties.pdf;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.enums.EnumUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.RunwayUtil;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.excel.PropertyExcelGenerator;
import com.sok.runway.crm.cms.properties.pdf.advanced.models.PDFTemplate;
import com.sok.runway.crm.interfaces.AsyncProcessStatus;
import com.sok.runway.offline.ThreadManager;
import com.sok.framework.ActionBean;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.servlet.ResponseOutputStream;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.FontFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ArrayList; 
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class PropertyPDFServlet extends HttpServlet implements AsyncProcessStatus {

	private static final long serialVersionUID = 1L;
	private static final String PDF_JSP = "PackagePdf";	

	/* */

	private static final Logger logger = LoggerFactory.getLogger(PropertyPDFServlet.class);

	public void setStatus(HttpServletRequest request, String status) {
		setStatus(request, status, -1);
	}

	public void setStatus(HttpServletRequest request, String status, double fraction) {
		logger.debug("setStatus(request, {})", status);
		String statusKey = (String)request.getAttribute("-statusKey");
		if(statusKey == null) {
			logger.debug("no status key from request");
			String fileKey = (String)request.getSession().getAttribute("-statusKey");
			if(fileKey != null) {
				logger.debug("status key from session {} ", fileKey);
				statusKey = fileKey;
			} else {
				logger.error("no status key");
				return;
			}
			request.setAttribute("-statusKey", statusKey);
		}
		logger.debug("using status key {}", statusKey);
		request.getSession().setAttribute(statusKey, makeStatusDiv(status, fraction));
	}

	private String makeStatusDiv(String status, double fraction) {
		if(fraction == -1) {
			return status;
		}
		return new StringBuilder().append(status)
				.append("<div style=\"width:100%; display:block; height: 15px\"><div style=\"width: ").append(fraction * 100)
				.append("%; background-color: red\">&nbsp;</div></div>").toString();
	}
	/* */



	public enum PDFVariant {	 Default /* whatever is in /specific/pdf/packagepdf.jsp */, 
		SinglePackage /* ONE PRODUCT PER PAGE */, 
		MultiPackageEstate /* A LIST OF LOTS, WITH PACKAGE COSTS INVOLVING THAT LOT */, 
		MultiPackageEstateLot ,
		MultiPackageByHomeDesign /* A LIST OF HOME PLANS, WITH PACKAGES THAT INCLUDE THAT PLAN */,
		TwelveHLPackagesPdf,
		PackageBreakdownPdf,
		MultiLandListPdf,
		MultiApartmentListPdf,
		HouseAndLandListPdf,
		Receipt,
		EOIForm,
		HoldForm,
		CongratulatoryLetter,
		PublishListPdf,
		MultiEstateLotListByHnL,
		ExcelPackageReport,
		DeveloperPdf,
		ApartmentsListPdf,
		BuildingApartmentsPdf,
		SingleApartmentPdf,
		ExcelApartmentReport,
		ExcelRealEstateReport,
		ExcelLandReport,
		ExistingPropertyPackagePdf,
		ExistingPropertyListPdf,
		MultiExistingPropertyListPdf,
		MultiLandListPdfByEstate,
		LandPropertyListPdf,
		SinglePackageHQ,
		TwelveHLPackagesPdfHQ,
		ExistingPropertyPackagePdfHQ,
		WatermarkPdf,
		LettersPdf,
		TransactionSummaryPdf,
		ReconcilePdf,
		ErrorPage /* An error pdf, which will render but show a message */; 

	public String getTitle() { 
		return names[this.ordinal()];
	}
	public String getDescription() {
		return desc[this.ordinal()];
	}
	public String getVariant() {
		return defaultVariants[this.ordinal()];
	}

	public static final String[] names = { "Default","Detail","By Estate","By Estate/Builder and Lot","By Plan", "Promotional H&L Packages", "Package Breakdown", "Lot Statistics", "House & Land List", "Detail - HQ", "Promotional H&L Packages - HQ", "Receipt","EOIForm","HoldForm","CongratulatoryLetter", "By Sales Consultant","Excel Report","Developer PDF", "Letters PDF", "Error"};

	public static final String[] desc = { "Default",
		"One product per page with full details",
		"Products listed with key details and thumbnail image, grouped by Estate",
		"Products listed with price only, grouped by Estate/Builder and Lot", 
		"Products listed with key details, grouped by Plan",
		"Two pages PDF with 12 House & Land packages",
		"One product per page with price breakdown",
		"Lot usage details by Estates, showing count of house & land packages",
		"List view of selected House & Land packages",
		"One product per page with full details for High Quality printing",
		"Two pages PDF with 12 House & Land packages for High Quality printing",
		"Receipt for a process",
		"EOI form for a process",
		"Hold Form for a process",
		"Congratulatory Letter for a process",
		"Products grouped by sales consultant, range and design",
		"Product data exported in a excel format",
		"One product per page formatted as specified by developer",
		"Generate the letters on the fly",
	"Error"};

	public static final String[] defaultVariants = { PDF_JSP, PDF_JSP, "MultiEstatePdf", "MultiLandPdf", "MultiHomePdf", "TwelveHLPackagesPdf", "PackageBreakdownPdf", "MultiLandListPdf", "MultiApartmentListPdf", "HouseAndLandListPdf", "ReceiptPdf", "EOIPdf", "HoldFormPdf","CongratulatoryLetterPdf", "PublishListPdf", "MultiLandListPdf", "PackageReport", "DeveloperPdf", "ApartmentsListPdf", "BuildingApartmentsPdf", "SingleApartmentPdf", "ApartmentReport", "RealEstateReport", "LandReport", "ExistingPropertyPackagePdf", "ExistingPropertyListPdf", "MultiExistingPropertyListPdf", "MultiLandListPdfByEstate", "LandPropertyListPdf", "PackagePdfHQ", "TwelveHLPackagesPdfHQ", "ExistingPropertyPackagePdfHQ", "WatermarkPdf", "LettersPdf", "TransactionSummaryPdf","ReconcilePdf", PDF_JSP };

	public static final String[] publishPDFTitles = {"By Sales Consultant","By Estate","By Range"};

	public static final String[] publishPDFDescriptions = {"Products grouped by Sales Consultant, Range and Design", 
		"Products grouped by Estate, Stage, Range and Design",
	"Products grouped by Range, Estate, and Design"};

	public static final String[] publishPDFSortOptions = {"salesRep+range+design", 
		"estate+stage+range+design",
	"range+estate+design"};

	public static final String[] publishPDFGroupOptionsNames = {"Exclusive", "Lot Type","Sales Consultant","Storeys"};
	public static final String[] publishPDFGroupOptions = {"marketingStatus", "lotType","salesRep","storeys"};

	public static PDFVariant[] getVisibleOptions() {
		return new PDFVariant[] { PDFVariant.SinglePackage, PDFVariant.MultiPackageEstate, PDFVariant.MultiPackageEstateLot, PDFVariant.MultiPackageByHomeDesign, PDFVariant.TwelveHLPackagesPdf, PDFVariant.PackageBreakdownPdf, PDFVariant.MultiLandListPdf, PDFVariant.HouseAndLandListPdf, PDFVariant.ExcelPackageReport, PDFVariant.DeveloperPdf };
	}

	public static PDFVariant[] getVisibleOptionsForApartments() {
		return new PDFVariant[] { PDFVariant.SingleApartmentPdf, PDFVariant.BuildingApartmentsPdf, PDFVariant.ApartmentsListPdf, PDFVariant.ExcelApartmentReport };
	}

	public static PDFVariant[] getVisibleOptionsForExistingProperties() {
		return new PDFVariant[] { PDFVariant.ExistingPropertyPackagePdf, PDFVariant.ExistingPropertyListPdf, PDFVariant.ExcelRealEstateReport };
	}

	public static PDFVariant[] getVisibleOptionsForLands() {
		return new PDFVariant[] { PDFVariant.LandPropertyListPdf, PDFVariant.ExcelLandReport };
	}

	public static PDFVariant fromString(String s) {
		if (StringUtils.isBlank(s))
			return Default;
		try {
			return valueOf(s);
		} catch (IllegalArgumentException iae) {
			System.out.format("Unknown PDF Variant - [%1$s] - using default PDF\n", s);
			return Default;
		}
	}
	
		public static boolean isValid(String s) {
			try {
				if (StringUtils.isNotBlank(s)) {
					valueOf(s);
					return true;
				}
			} catch (IllegalArgumentException iae) {
				logger.debug("Unknown PDF Variant - [%1$s] - using default PDF\n", s);
				System.out.format("Unknown PDF Variant - [%1$s] - using default PDF\n", s);
			}
			return false;
		}
	}

	public void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			
			String classAndMethodName = this.getClass() + " processRequest(HttpServletRequest request, HttpServletResponse response) ";
			logger.debug(classAndMethodName + " Started.");

			final String tempFileKey = StringUtils.trimToNull(request.getParameter("fileKey"));
			
			if (tempFileKey != null) {
				logger.debug(classAndMethodName + " tempFileKey is: " + tempFileKey);

				File f = (File) request.getSession().getAttribute(tempFileKey);
				if (f != null && f.exists() && f.length() > 0) {
					String contentFormat = FilenameUtils.isExtension(f.getName(), new String[] {"xls, xlsx"}) ? "application/vnd.ms-excel" : "application/pdf" ;
					FileInputStream fis = new FileInputStream(f);
					response.setContentType(contentFormat);

					DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
					Date date = new Date();
					String strDate = dateFormat.format(date);
					response.setHeader("Content-Type", contentFormat);
					response.setHeader("Content-Disposition", "attachment; filename=\"" + "PropertyDetails_" + strDate + "." + FilenameUtils.getExtension(f.getName()) + "\"");				

					org.apache.commons.io.IOUtils.copy(fis, response.getOutputStream());
					fis.close();
					// request.getSession().removeAttribute(tempFileKey);
					return;
				} else {
					if (f != null)
						System.out.println("PDF: " + f.getAbsolutePath() + " " + f.exists() + " " + f.length());
					throw new ServletException("The file could not be found, perhaps too much time has elapsed since it was generated");
				}
			}
			
			logger.debug(classAndMethodName + " calling setStatus(request, Starting up, 0.01)");
			setStatus(request, "Starting up", 0.01);
			/* the variant of the report, or will default to the legacy implementation */
			final PDFVariant variant = PDFVariant.fromString(request.getParameter("-variant"));
			/* list key relates to a session attribute containing a String[] of ids */
			final String listKey = StringUtils.trimToNull(request.getParameter("-listKey"));
			/* search key relates to a session attribute containing a GenRow */
			final String searchKey = StringUtils.trimToNull(request.getParameter("-searchKey"));
			final boolean isWebsiteRequest = "yes".equalsIgnoreCase(request.getParameter("isWebsiteRequest"));
			final String productID = StringUtils.trimToNull(request.getParameter("ProductID"));
			
			List<PropertyEntity> list = Collections.emptyList();

			//System.out.println("PropertyPDFServlet.java ==> variant.getVariant is: " + variant.getVariant() + " , variant is: " + variant + "  , isWebsiteRequest: " + isWebsiteRequest);

			if ((PDFVariant.MultiEstateLotListByHnL != variant && ( productID == null || !productID.startsWith("-1") ) /* if we pass an actual id or ids */) //"MultiEstateLotListByHnL".equalsIgnoreCase(variant.toString())
					&& !("SingleApartmentPdf".equalsIgnoreCase(variant.toString()) && isWebsiteRequest)
					&& !("WatermarkPdf".equalsIgnoreCase(variant.toString()))
					&& !("TransactionSummaryPdf".equalsIgnoreCase(variant.toString()))
					&& !("ReconcilePdf".equalsIgnoreCase(variant.toString()))
					/*&& !"ApartmentsListPdf".equalsIgnoreCase(variant.toString())
					&& !"BuildingApartmentsPdf".equalsIgnoreCase(variant.toString())
					&& !"SingleApartmentPdf".equalsIgnoreCase(variant.toString())*/ )
			{	
				if (listKey != null) {
					Object o = request.getSession().getAttribute(listKey);
					if (o instanceof String[]) {
						list = PropertyFactory.getPropertyEntities(request, (String[]) o, this);
					}
				}

				if ((list == null || list.size() == 0) && productID != null) {
					/*
					 * System.out.println("loading from request parameter values "); for(String s:
					 * request.getParameterValues("ProductID")) { System.out.println(s); }
					 */
					logger.debug(classAndMethodName + " list is null or blank. calling PropertyFactory.getPropertyEntities(request, this). ProductID is: " + productID);
					
					list = PropertyFactory.getPropertyEntities(request, this);
				}
				if ((list == null || list.size() == 0) && searchKey != null) {
					
					logger.debug(classAndMethodName + " list is null or blank. searchKey is: " + searchKey);
					
					GenRow search = (GenRow) request.getSession().getAttribute(searchKey);
					search.setRequest(request);
					search.setParameter("-select1", "Products.ProductID as altID");
					search.setParameter("-groupby1", "Products.ProductID");
					search.doAction(GenerationKeys.SEARCH);
					search.getResults();
					if (search.getNext()) {
						list = new ArrayList<PropertyEntity>();
						do {
							list.add(PropertyFactory.getPropertyEntity(request, search.getData("ProductID")));
						} while (search.getNext());
					}
					search.close();
				}

				if ( (list == null || list.size() == 0) && StringUtils.isBlank(request.getParameter("OpportunityID")) && StringUtils.isBlank(request.getParameter("TransactionID")) ){
					throw new UnsupportedOperationException("Products or Oppurtunity must be supplied to generate PDF");
				}
			}

			if((PDFVariant.SinglePackage == variant || PDFVariant.DeveloperPdf == variant) && request.getRequestURI().indexOf("propertypdf.zip") != -1) {
				
				logger.debug(classAndMethodName + " Request for the propertypdf.zip");
				
				final ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
				try { 
					logger.debug("doing zip variant");
					Collections.sort(list, new Comparator<PropertyEntity>(){
						public int compare(PropertyEntity p1, PropertyEntity p2) {
							return StringUtils.trimToEmpty(p1.getEntityEstateName()).compareTo(StringUtils.trimToEmpty(p2.getEntityEstateName()));
						}
					});
					List<Future<PDFRecord>> output = new ArrayList<Future<PDFRecord>>(list.size());

					ExecutorService executor = null;
					StringBuilder url = new StringBuilder().append("/property.pdf?");
					for(String key: new String[]{"-variant","GroupID", "RepUserID","DisplayID","useAllocatedRep", "EstateID", "isPortalRequest", "ContactID"}) {
						String val = StringUtils.trimToNull(request.getParameter(key));
						if(val != null) {
							url.append(key).append("=").append(StringUtil.urlEncode(val)).append("&");
						}
					}
					url.append("ProductID=");
					final String urlBase = url.toString();
					final HttpSession session = request.getSession();
					try { 
						int threads = 3;
						try {
							threads = Integer.parseInt(InitServlet.getSystemParam("PropertyPDFThreads", String.valueOf(threads)));
						} catch(NumberFormatException nfe) {}
						executor = ThreadManager.createThreadExecutor("PropertyPDFServlet:createZip", threads);
						
						final boolean singlePdf = !"true".equals(request.getParameter("-estateFolders"));

						for(final PropertyEntity property: list) {
							output.add(executor.submit(new Callable<PDFRecord>() {
								public PDFRecord call() {
									String estate = StringUtils.trimToNull(property.getEntityEstateName());
									StringBuilder file = new StringBuilder();
									if(!singlePdf) {
										if(estate != null) {
											file.append(StringUtil.removeNonFileSystemCharacters(estate).toLowerCase()).append("/");
										}
										file.append(StringUtil.removeNonFileSystemCharacters(property.getString("Name")).toLowerCase()).append("_").append(property.getString("ProductNum")).append("_").append(KeyMaker.generate(5)).append(".pdf");
									}
									String url = urlBase + property.getData("ProductID");
									logger.debug("creating zip entry {} from url {}", file.toString(), url);
									try { 
										ByteArrayOutputStream baos = new ByteArrayOutputStream();
										try {
											ResponseOutputStream ros = new ResponseOutputStream(session, baos, url);
											ros.service(property.getConnection());
										} finally {
											baos.flush();
											baos.close();
										}
										return new PDFRecord(baos.toByteArray(), file.toString());
									} catch (Exception re) {
										logger.error("error creating zip entry from url", re);
										throw new RuntimeException(re);
									}
								}
							}));
						}
						
						// This looks a bit convoluted, but must be done this way so as to pass the doc / cb variables into the actions as they must be final.
						if(singlePdf) {
							out.putNextEntry(new ZipEntry("property.pdf"));
						}
						final Document doc = singlePdf ? new Document(PageSize.A4) : null;
						final PdfWriter writer = doc != null ? getWriter(doc, out) : null;
						if(doc != null) doc.open(); 
						final PdfContentByte cb = writer != null ? writer.getDirectContent() : null; 
						
						Action<PDFRecord> action = singlePdf ? new Action<PDFRecord>(){
							public void act(PDFRecord rec) throws IOException { // add pdf to single doc 
								// Load existing PDF
								PdfReader reader = new PdfReader(rec.bytes);
								for(int i=1; i<=reader.getNumberOfPages(); i++) {
									PdfImportedPage page = writer.getImportedPage(reader, i);
									doc.newPage();
									cb.addTemplate(page, 0, 0);
								}

							}
						} : new Action<PDFRecord>() {
							public void act(PDFRecord rec) throws IOException { // add pdf zip in estate folder
								if(rec != null) {
									out.putNextEntry(new ZipEntry(rec.fileName));
									out.write(rec.bytes);
								}
							}
						};	
						 
						for(Future<PDFRecord> rec: output) {
							PDFRecord tRec = null;
							try {
								tRec = rec.get();
							} catch (InterruptedException e) {
								logger.info("Retrieving PDF Interupted");
							} catch (ExecutionException e) {
								logger.error("Error producing package pdf", e);
							}
							action.act(tRec);
						}
						if(doc != null) { doc.close(); }
					} finally {
						if(executor != null) {
							ThreadManager.shutdownExecutor(executor);
						}
					}
				} finally {
					out.close();
				}
				//if (!response.containsHeader("Content-Disposition")) {
				response.setHeader("Content-Disposition", "attachment; filename=\"propertypdf.zip\"");
				//}
			} else {	// else if not zip 

				/*if (!response.containsHeader("Content-Disposition")) {
					response.setHeader("Content-Disposition", "attachment; filename=\"property.pdf\"");
				}*/

				if(variant.equals(PDFVariant.ExcelPackageReport) || variant.equals(PDFVariant.ExcelApartmentReport)|| variant.equals(PDFVariant.ExcelRealEstateReport) || variant.equals(PDFVariant.ExcelLandReport)) {
					
					logger.debug(classAndMethodName + " Before renderExcel(variant, list, request, response)");
					renderExcel(variant, list, request, response);
					logger.debug(classAndMethodName + " After renderExcel(variant, list, request, response)");
					
				} else {
					
					logger.debug(classAndMethodName + " Before renderPDF(variant, list, request, response)");
					renderPDF(variant, list, request, response);
					logger.debug(classAndMethodName + " After renderPDF(variant, list, request, response)");
				}
			}
		} catch (RuntimeException re) {
			renderError(re, request, response);
			re.printStackTrace();
		}
	}

	/* Essentially a hack as java doesn't do single line try-catch with assignment */ 
	private PdfWriter getWriter(Document doc, OutputStream out) {
		try { 
			return PdfWriter.getInstance(doc, out);
		} catch (DocumentException de) { throw new RuntimeException("Error getting writer", de); } 
	}

	private interface Action<T> {
		public void act(T rec) throws IOException;
	}
	
	private class PDFRecord {
		public byte[] bytes;
		public String fileName;
		public PDFRecord(byte[] bytes, String fileName) {
			this.bytes = bytes;
			this.fileName = fileName;
		}
	}

	/**
	 * Specific implementations should override this method to produce custom output.
	 */
	protected void renderPDF(PDFVariant variant, List<PropertyEntity> list, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// NB, request has RepUserID

		String classAndMethodName = this.getClass() + " renderPDF(PDFVariant variant, List<PropertyEntity> list, HttpServletRequest request, HttpServletResponse response) ";
		logger.debug(classAndMethodName + " Started.");
		
		String template = variant.getVariant();

		logger.debug(classAndMethodName + " template is: " + template);
		
		request.setAttribute("entityList", list);
		request.setAttribute("servletConfig", getServletConfig());//This to work with Jasper
		logger.debug("Loading template {}", template);
		
		logger.debug(classAndMethodName + " Before calling PropertyPDFGenerator.generate(template, request, response)");
		
		// request.getRequestDispatcher(url).forward(request,response);
		PropertyPDFGenerator.generate(template, request, response);
		
		logger.debug(classAndMethodName + " After calling PropertyPDFGenerator.generate(template, request, response)");
		
		logger.debug(classAndMethodName + " Ended.");
	}

	/**
	 * Specific implementations should override this method to produce custom output.
	 */
	protected void renderExcel(PDFVariant variant, List<PropertyEntity> list, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// NB, request has RepUserID

		String classAndMethodName = this.getClass() + " renderExcel(PDFVariant variant, List<PropertyEntity> list, HttpServletRequest request, HttpServletResponse response) ";
		logger.debug(classAndMethodName + " Started.");

		String template = variant.getVariant();

		request.setAttribute("entityList", list);
		logger.debug("Loading template {}", template);
		// request.getRequestDispatcher(url).forward(request,response);
		PropertyExcelGenerator.generate(template, request, response);
		
		logger.debug(classAndMethodName + " Ended.");
	}

	public static void temp() throws IOException {
		File f = File.createTempFile(KeyMaker.generate(), ".pdf");

	}

	public void renderError(Exception e, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String classAndMethodName = this.getClass() + " renderError() ";
		logger.debug(classAndMethodName + " Started.");

		com.itextpdf.text.Document document = new com.itextpdf.text.Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
			document.open();
			Paragraph error = new Paragraph();
			error.add(new Chunk(ActionBean.writeStackTraceToString(e), FontFactory.getFont(FontFactory.HELVETICA, 8)));
			document.add(error);
			document.close();
			writer.close();
		} catch (Exception ex) {
			ActionBean.writeStackTrace(ex, System.out);
			response.sendRedirect("/error.html");
		}
		
		logger.debug(classAndMethodName + " Ended.");
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String classAndMethodName = this.getClass() + " doGet(HttpServletRequest request, HttpServletResponse response) ";
		logger.debug(classAndMethodName + " Before Calling processRequest(request, response).");
		if (shouldRedirectToAdvanced(request)) {
			new AdvancedPDFServlet().doGet(request, response);
		} else {
			processRequest(request, response);
		}

		logger.debug(classAndMethodName + " After Calling processRequest(request, response).");
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String classAndMethodName = this.getClass() + " doPost(HttpServletRequest request, HttpServletResponse response) ";
		logger.debug(classAndMethodName + " Before Calling processRequest(request, response).");

		if (shouldRedirectToAdvanced(request)) {
			new AdvancedPDFServlet().doPost(request, response);
		} else {
			processRequest(request, response);
		}

		logger.debug(classAndMethodName + " After Calling processRequest(request, response).");
	}

	private boolean shouldRedirectToAdvanced(HttpServletRequest request) {
		final String category = request.getParameter("-category");
		final String tempFileKey = StringUtils.trimToNull(request.getParameter("fileKey"));
		String product = StringUtils.trimToNull(request.getParameter("-product"));
		final PDFVariant variant = PDFVariant.fromString(request.getParameter("-variant"));
		logger.debug("category - {}, product - {}" , category, product);
		logger.debug("tempFileKey - {}, variant - {}, product - {}" , tempFileKey, variant);
		
		if (StringUtils.isNotBlank(product))
			product = product.replaceAll(" ", "");
		else
			product = "HouseAndLand";
		
		if (tempFileKey == null) {
			if("Runway".equals(category) || PDFVariant.isValid(request.getParameter("-variant"))) {								
				return false;
			}
			if(PDFVariant.Default == variant) {
				return RunwayUtil.isAdvancedModePDF(request) && StringUtils.isNotBlank(getDefaultPDFTemplateID(request, product));
			}
			return RunwayUtil.isAdvancedModePDF(request);
		}
		return false;
	}
	
	public static String getDefaultPDFTemplateID(HttpServletRequest request, String pType) {
		String defaultTemplateID = "";
		JSONObject resp = new JSONObject();
		AdvancedPDFServlet advPDF = new AdvancedPDFServlet();
		try {
			resp = advPDF.getPDFTemplates(pType, (ArrayList<String>) request.getSession().getAttribute("-printpdfselectedproductids"), true);
			JSONArray ary = (JSONArray) resp.get("Templates");
			for (int y = 0; y < ary.size(); ++y) {
				JSONObject obj = (JSONObject) ary.get(y);
				Boolean isDefault = (Boolean) obj.get("Default");
				if (isDefault && StringUtils.isBlank(request.getParameter("-company"))) {
					defaultTemplateID = (String) obj.get("ID");
					return defaultTemplateID;
				} else if (isDefault && request.getParameter("-company").equals(obj.get("Company"))) {
					defaultTemplateID = (String) obj.get("ID");
					return defaultTemplateID;
				}
				logger.debug("TemplateName {}, isDefault {}", obj.get("Name"), isDefault);
			}

			// try again without default
			for (int y = 0; y < ary.size(); ++y) {
				JSONObject obj = (JSONObject) ary.get(y);
				Boolean isDefault = (Boolean) obj.get("Default");
				if (StringUtils.isBlank(request.getParameter("-company"))) {
					defaultTemplateID = (String) obj.get("ID");
					return defaultTemplateID;
				} else if (request.getParameter("-company").equals(obj.get("Company"))) {
					defaultTemplateID = (String) obj.get("ID");
					return defaultTemplateID;
				}
				logger.debug("TemplateName {}, isDefault {}", obj.get("Name"), isDefault);
			}

		} catch (Exception e) {
			logger.error("Error fetching default template", e.getMessage());
			e.printStackTrace();
		}

		return defaultTemplateID;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject getDefaultPDFTemplate(HttpServletRequest request, String pType) {
		JSONObject resp = new JSONObject();
		AdvancedPDFServlet advPDF = new AdvancedPDFServlet();
		try {
			resp = advPDF.getPDFTemplates(pType, (ArrayList<String>) request.getSession().getAttribute("-printpdfselectedproductids"), true);
			JSONArray ary = (JSONArray) resp.get("Templates");
			for (int y = 0; y < ary.size(); ++y) {
				JSONObject obj = (JSONObject) ary.get(y);
				Boolean isDefault = (Boolean) obj.get("Default");
				if (isDefault && StringUtils.isBlank(request.getParameter("-company"))) {
					return obj;
				} else if (isDefault && request.getParameter("-company").equals(obj.get("Company"))) {
					return obj;
				}
				logger.debug("TemplateName {}, isDefault {}", obj.get("Name"), isDefault);
			}

		} catch (Exception e) {
			logger.error("Error fetching default template", e.getMessage());
			e.printStackTrace();
		}

		return null;
	}

}
