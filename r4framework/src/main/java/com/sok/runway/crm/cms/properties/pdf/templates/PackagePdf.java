package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sok.framework.InitServlet;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.cms.properties.HomePlan;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ListFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil.Range;

public class PackagePdf extends AbstractPDFTemplate implements PDFConstants {
	
	public static final Logger logger = LoggerFactory.getLogger(PackagePdf.class);
	/*** CUSTOM CODE : These parameters are usually customized as per client needs ***/
	private static final Font lotPlanFont = FontFactory.getFont("Arial", 14f, Font.BOLD, BaseColor.WHITE);
	/*************************************/

	public static final String PRODUCTID = "ProductID";

	private static final String BED_IMG = "/specific/pdf/images/icon_bed_white.png";
	private static final String BATH_IMG = "/specific/pdf/images/icon_bath_white.png";
	private static final String CAR_IMG = "/specific/pdf/images/icon_car_white.png";
	private static String LOGO_IMG = PropertyEntity.resizeImage(PDFUtil.MAIN_LOGO_IMG, 400, 264, PropertyEntity.ResizeOption.Pad);

	//TODO - test without this, it should not be true.
	private static final boolean dev = Boolean.TRUE;

	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {
		
		setStatus(request, "Loading properties");
		String homePath = request.getSession().getServletContext().getInitParameter("URLHome");

		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (List<PropertyEntity>)request.getAttribute("entityList"); 
		if(properties == null) {
			setStatus(request,"Loading Records..", 0.02d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			properties = PropertyFactory.getPropertyEntities(request, this);
		}
		
		java.util.List<SinglePackage> pList = new java.util.ArrayList<SinglePackage>(properties.size());
		for (PropertyEntity pe : properties) {
			// ActionBean.connect(p);
			pList.add(getPackage(request, pe));
		}

		setStatus(request, "Rendering Pages");
		
		boolean firstPage = true;
		for (SinglePackage p : pList) {
			if (!firstPage)
				document.newPage();
			else
				firstPage = false;
			renderPackage(document, p, homePath);
		}
	}

	private class SinglePackage {
		String filename, heroImage, planImage, estateLogo, stageName, name, address, price, estateName, suburb, designDetail;
		java.util.List<String> features;
		int bedrooms, bathrooms, carParks;
		String bathroomsStr;
		double lotSize;
		PDFUtil.Range range;
		java.util.List<String> promoImages;
		String dripImg1;
		final String[] inclusions = { "Fixed price site costs and connections", "Developer requirements", "Council requirements", "Minimum 6 Star energy efficiency", "Caesarstone benchtops", "Ceramic floortiles", "Site start guarantee" };
		final String disclaimer = String.format(PDFUtil.DISCLAIMER_TEXT, new java.util.Date());
		String repName, repMobile, repPhone, repEmail;
		String locName, locStreet, locCity, displayHours;
		boolean isHAndLPackage;
		boolean isHome;
	}

	void renderPackage(com.itextpdf.text.Document document, SinglePackage p, String homePath) throws DocumentException {

		if(p.isHome){
			PdfPTable pageTable = new PdfPTable(2);
			pageTable.setWidths(new float[] { (299f / 909f), (612f / 909f) });
			pageTable.setWidthPercentage(100f);
			pageTable.getDefaultCell().setPadding(0);
			pageTable.getDefaultCell().setBorder(0);

			PdfPTable houseInfo = new PdfPTable(1);
			houseInfo.setWidthPercentage(100f);
			houseInfo.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

			houseInfo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			houseInfo.getDefaultCell().setBorder(0);
			houseInfo.getDefaultCell().setPadding(0);

			Image i = PDFUtil.createImage(LOGO_IMG);
			if (i != null) {
				houseInfo.addCell(i);
			}
//			if (p.estateLogo != null && (i = PDFUtil.createImage(p.estateLogo)) != null) {
//				houseInfo.getDefaultCell().setPaddingTop(5);
//				houseInfo.getDefaultCell().setPaddingLeft(20);
//				houseInfo.getDefaultCell().setPaddingRight(20);
//				houseInfo.addCell(i);
//				houseInfo.getDefaultCell().setPaddingLeft(0);
//				houseInfo.getDefaultCell().setPaddingRight(0);
//			} else {
//				houseInfo.addCell(new Paragraph("Problem with estate " + String.valueOf(p.estateLogo), lotDetailFont));
//			}
//			Paragraph details = new Paragraph();
//			details.add(new Chunk("\n", spacerFont));
//			details.add(new Chunk(p.address, lotPlanFontBlack));
//			if (p.lotSize > 0) {
//				addArea(details, "", String.valueOf(new Double(p.lotSize).intValue()), lotDetailFont, lotDetailFontsmall);
//			}
//			if (p.stageName != null) {
//				details.add(new Chunk(" " + p.stageName, lotDetailFont));
//			}
//			details.add(new Chunk("\n", lotDetailFont));
//
//			details.add(new Chunk("\n", spacerFont));
//			details.add(new Chunk(WordUtils.capitalizeFully(p.estateName) + "\n", locationFont));
//			details.add(new Chunk("\n", spacerFont));
//			details.add(new Chunk(WordUtils.capitalizeFully(p.suburb) + "\n", locationFont));
//
//			houseInfo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
//			houseInfo.getDefaultCell().setPaddingLeft(25);
//
//			houseInfo.addCell(details);
			pageTable.addCell(houseInfo);

			if ((i = PDFUtil.createImage(p.heroImage)) != null) {
				// will need to be sized
				PdfPCell heroImageCell = new PdfPCell();		
				heroImageCell.setPadding(0);
				heroImageCell.setBorder(0);
				heroImageCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				heroImageCell.setFixedHeight(280);
				heroImageCell.addElement(i);
				pageTable.addCell(heroImageCell);
				//pageTable.addCell(i);
			} else {
				pageTable.addCell(new Paragraph("Problem with hero " + p.heroImage, lotDetailFont));
			}

//			pageTable.getDefaultCell().setColspan(2);
//			pageTable.getDefaultCell().setPaddingTop(3);
//			pageTable.getDefaultCell().setPaddingLeft(25);
//			pageTable.getDefaultCell().setPaddingBottom(10);
//			pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
//			pageTable.getDefaultCell().setBackgroundColor(PDFUtil.aqua);			
//			pageTable.addCell(new Paragraph("FIXED PRICE HOUSE AND LAND PACKAGE", subHeader));

			pageTable.getDefaultCell().setColspan(2);
			pageTable.getDefaultCell().setPaddingTop(0);
			pageTable.getDefaultCell().setPaddingBottom(0);

			if (p.range != null) {
				pageTable.getDefaultCell().setBackgroundColor(p.range.getColor());
			} else {
				pageTable.getDefaultCell().setBackgroundColor(new BaseColor(102, 102, 102));
			}

			pageTable.getDefaultCell().setColspan(1);
			pageTable.getDefaultCell().setPaddingTop(0f);
			pageTable.addCell(new Paragraph(p.price, singlePriceFont));
			pageTable.getDefaultCell().setPaddingTop(0f);
			pageTable.getDefaultCell().setPaddingLeft(0f);
			houseInfo = new PdfPTable(9);
			houseInfo.setWidthPercentage(100f);
			// houseInfo.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

			houseInfo.setWidths(new float[] { 10, 50, 5, 5, 5, 5, 5, 5, 5 });
			houseInfo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			houseInfo.getDefaultCell().setBorder(0);
			houseInfo.getDefaultCell().setPadding(0);

			/* range image */

			if (p.range != null) {
				pageTable.getDefaultCell().setBackgroundColor(p.range.getColor());
			} else {
				pageTable.getDefaultCell().setBackgroundColor(new BaseColor(102, 102, 102));
			}

			if (p.range != null && (i = PDFUtil.createImage(homePath + p.range.getLogo())) != null) {
				houseInfo.addCell(i);
			} else {
				houseInfo.addCell(new Paragraph(""));
			}
			houseInfo.getDefaultCell().setPaddingLeft(5f);
			houseInfo.getDefaultCell().setPaddingTop(12f);
			houseInfo.getDefaultCell().setPaddingBottom(10f);
			houseInfo.addCell(new Paragraph(p.name.toUpperCase(), lotPlanFont));

			int iPadTop = 3, iPadBottom = 3, iPadRight = 4, iHeight = 12;
			float iPadImg = 12f, iPadText = 11f;
			houseInfo.getDefaultCell().setPaddingTop(iPadText);
			houseInfo.getDefaultCell().setPaddingBottom(15);
			PdfPTable iTable = null;
			if ((i = PDFUtil.createImage(homePath + BED_IMG)) != null) {
				houseInfo.getDefaultCell().setPaddingTop(iPadImg);
				iTable = new PdfPTable(1);
				iTable.getDefaultCell().setBorder(0);
				iTable.getDefaultCell().setPadding(0);
				iTable.getDefaultCell().setPaddingTop(iPadTop);
				iTable.getDefaultCell().setPaddingRight(iPadRight);
				iTable.getDefaultCell().setFixedHeight(iHeight);
				iTable.getDefaultCell().setBorderWidthRight(1f);
				iTable.getDefaultCell().setBorderColorRight(BaseColor.WHITE);
				iTable.addCell(i);
				houseInfo.addCell(iTable);
				houseInfo.getDefaultCell().setPaddingTop(iPadText);
			} else {
				houseInfo.addCell(new Paragraph("Rooms: ", lotPlanFont));
			}
			houseInfo.addCell(new Paragraph(String.valueOf(p.bedrooms), lotPlanFont));
			if ((i = PDFUtil.createImage(homePath + CAR_IMG)) != null) {
				houseInfo.getDefaultCell().setPaddingTop(iPadImg);
				iTable = new PdfPTable(1);
				iTable.getDefaultCell().setBorder(0);
				iTable.getDefaultCell().setPadding(0);
				iTable.getDefaultCell().setPaddingTop(iPadTop);
				iTable.getDefaultCell().setPaddingRight(iPadRight);
				iTable.getDefaultCell().setFixedHeight(iHeight);
				iTable.getDefaultCell().setBorderWidthRight(1f);
				iTable.getDefaultCell().setBorderColorRight(BaseColor.WHITE);
				iTable.addCell(i);
				houseInfo.addCell(iTable);
				houseInfo.getDefaultCell().setPaddingTop(iPadText);
			} else {
				houseInfo.addCell(new Paragraph("Cars: ", lotPlanFont));
			}
			houseInfo.addCell(new Paragraph(String.valueOf(p.carParks), lotPlanFont));
			if ((i = PDFUtil.createImage(homePath + BATH_IMG)) != null) {
				houseInfo.getDefaultCell().setPaddingTop(iPadImg);
				iTable = new PdfPTable(1);
				iTable.getDefaultCell().setBorder(0);
				iTable.getDefaultCell().setPadding(0);
				iTable.getDefaultCell().setPaddingTop(iPadTop);
				iTable.getDefaultCell().setPaddingRight(iPadRight);
				iTable.getDefaultCell().setFixedHeight(iHeight);
				iTable.getDefaultCell().setBorderWidthRight(1f);
				iTable.getDefaultCell().setBorderColorRight(BaseColor.WHITE);
				iTable.addCell(i);
				houseInfo.addCell(iTable);
				houseInfo.getDefaultCell().setPaddingTop(iPadText);
			} else {
				houseInfo.addCell(new Paragraph("Baths: ", lotPlanFont));
			}
			houseInfo.addCell(new Paragraph(String.valueOf(p.bathroomsStr), lotPlanFont));

			houseInfo.addCell(BLANK);

			pageTable.addCell(houseInfo);
			pageTable.getDefaultCell().setPaddingTop(10);
			pageTable.getDefaultCell().setPaddingLeft(12);
			pageTable.getDefaultCell().setPaddingRight(1);
			pageTable.getDefaultCell().setPaddingBottom(0);
			pageTable.getDefaultCell().setBackgroundColor(bottomLeftGrey);

			houseInfo = new PdfPTable(2);
			houseInfo.setWidths(new float[] { 5, 60 });
			houseInfo.getDefaultCell().setBorder(0);
			houseInfo.getDefaultCell().setPaddingLeft(15);
			houseInfo.getDefaultCell().setPaddingBottom(5f);
			houseInfo.getDefaultCell().setColspan(2);
			houseInfo.getDefaultCell().setPaddingRight(10);
			if (p.designDetail != null) {
				houseInfo.getDefaultCell().setLeading(12f, 0f);
				houseInfo.addCell(new Paragraph(p.designDetail, packageDetailFont));

			}
			pageTable.getDefaultCell().setPaddingRight(0);
			houseInfo.getDefaultCell().setPaddingBottom(0f);
			houseInfo.addCell(new Paragraph("PACKAGE\nINCLUSIONS", p.range != null ? p.range.getInclusionsFont(packageInclusionsHeader) : packageInclusionsHeader));
			houseInfo.getDefaultCell().setColspan(1);
			houseInfo.getDefaultCell().setPaddingLeft(2);

			i = p.range != null ? PDFUtil.createImage(homePath + p.range.getTick()) : null;
			houseInfo.getDefaultCell().setPaddingBottom(2f);
			houseInfo.getDefaultCell().setPaddingTop(2f);
			houseInfo.getDefaultCell().setPaddingRight(5f);
			for (String s : p.inclusions) {
				if (i != null) {
					houseInfo.getDefaultCell().setPaddingTop(4f);
					houseInfo.getDefaultCell().setPaddingLeft(2);
					houseInfo.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
					houseInfo.addCell(i);
					houseInfo.getDefaultCell().setPaddingLeft(1);
					houseInfo.getDefaultCell().setPaddingTop(2f);
				} else
					houseInfo.addCell(new Paragraph("\u2022", packageDetailFont)); // bullet
				houseInfo.addCell(new Paragraph(s, packageDetailFont));
			}

			houseInfo.getDefaultCell().setColspan(2);
			houseInfo.getDefaultCell().setPaddingTop(5f);
			houseInfo.addCell(BLANK);

			houseInfo.getDefaultCell().setColspan(2);

			/** CUSTOM CODE : Place holder for DRIPS **/
			String imgStr = "";
			if (p.dripImg1 != null)
			{	
					PdfPCell cell = null;
					 imgStr = homePath + "/" + p.dripImg1;
					try
					{
							URL imgUrl = new URL(imgStr);
							java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
							
							Image dripImg = Image.getInstance(imgUri.toString());
							//dripImg.setAbsolutePosition(180, 87);
							// dripImg.scaleToFit(125, 67);
							dripImg.scaleToFit(170, 90);
							
							dripImg.setBorder(0);
							dripImg.setBorderWidth(0);
							///dripImg.setUseVariableBorders(false);
							
							
							cell = new PdfPCell(dripImg);
							cell.setBorderWidth(0f);
							cell.setBorder(0);
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setPaddingBottom(10);

							houseInfo.getDefaultCell().setPaddingTop(10f);
							houseInfo.getDefaultCell().setPaddingBottom(10f);
							houseInfo.getDefaultCell().setPaddingLeft(10f);
							houseInfo.getDefaultCell().setPaddingRight(10f);
							houseInfo.getDefaultCell().setBackgroundColor(bottomLeftGrey);
							houseInfo.getDefaultCell().setBorder(0);
							houseInfo.getDefaultCell().setBorderWidth(0);
							//dripTable.getDefaultCell().setBorderColor(BaseColor.WHITE);
							//dripTable.getDefaultCell().setBorderWidthRight(20f);
							houseInfo.addCell(cell);
					}
					catch(Exception ee)
					{
							cell = new PdfPCell(new Paragraph("Problem with image" + imgStr ));
							System.out.println("Exception Occurred. Message is: " + ee.getMessage());
							ee.printStackTrace();
					}
			}

			
			/*
			  for (String img : p.promoImages) { if ((i = PDFUtil.createImage(new
			  StringBuilder(100).append(homePath).append("/").append(img).toString())) != null) {
			  houseInfo.getDefaultCell().setPaddingRight(0); houseInfo.addCell(BLANK);
			  houseInfo.getDefaultCell().setPadding(0); houseInfo.getDefaultCell().setPaddingLeft(23);
			  houseInfo.getDefaultCell().setPaddingRight(35); houseInfo.addCell(i); } }
			*/
			

			/*PdfPTable blankTable = new PdfPTable(3);
			blankTable.setWidths(new float[] { 5, 40, 10 });
			blankTable.getDefaultCell().setBorder(0);
			blankTable.getDefaultCell().setPadding(0);
			blankTable.getDefaultCell().setFixedHeight(60f);
			blankTable.getDefaultCell().setBackgroundColor(bottomLeftGrey);
			blankTable.addCell(BLANK);
			blankTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
			blankTable.addCell(BLANK);
			blankTable.getDefaultCell().setBackgroundColor(bottomLeftGrey);
			blankTable.addCell(BLANK);
			houseInfo.getDefaultCell().setPaddingTop(15f);
			houseInfo.addCell(blankTable);*/
			/**********************************/

			houseInfo.getDefaultCell().setColspan(1);
			houseInfo.addCell(BLANK);
			houseInfo.getDefaultCell().setPaddingLeft(0);
			houseInfo.getDefaultCell().setPaddingRight(10);
			houseInfo.getDefaultCell().setPaddingTop(5f);
			houseInfo.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
			houseInfo.getDefaultCell().setPaddingBottom(25f);
			houseInfo.getDefaultCell().setLeading(9f, 0f);
			houseInfo.addCell(new Paragraph(p.disclaimer, disclaimerFont));

			pageTable.addCell(houseInfo);
			/* add the home plan */
			pageTable.getDefaultCell().setPaddingTop(4);
			pageTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
			houseInfo = new PdfPTable(2);
			houseInfo.setWidths(new int[] { 46, 45 });
			houseInfo.getDefaultCell().setBorder(0);
			houseInfo.getDefaultCell().setPadding(0);
			houseInfo.getDefaultCell().setPaddingRight(15f);
			houseInfo.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
			houseInfo.getDefaultCell().setColspan(2);

			if ((i = PDFUtil.createImage(p.planImage)) != null) {
				houseInfo.addCell(i);
			} else {
				houseInfo.addCell(new Paragraph("Sorry, the home plan image could not be displayed"));
			}

			houseInfo.getDefaultCell().setColspan(1);
			houseInfo.getDefaultCell().setPaddingRight(0);
			houseInfo.addCell(ListFooter.getRepParagraph(p.repName, p.repMobile, p.repPhone, p.repEmail));
			houseInfo.addCell(ListFooter.getLocationParagraph(p.locName, p.locStreet, p.locCity, p.displayHours));
			pageTable.getDefaultCell().setPaddingBottom(23.5f);
			pageTable.addCell(houseInfo);
			document.add(pageTable);
		}else if(p.isHAndLPackage){
			PdfPTable pageTable = new PdfPTable(2);
			pageTable.setWidths(new float[] { (299f / 909f), (612f / 909f) });
			pageTable.setWidthPercentage(100f);
			pageTable.getDefaultCell().setPadding(0);
			pageTable.getDefaultCell().setBorder(0);

			PdfPTable houseInfo = new PdfPTable(1);
			houseInfo.setWidthPercentage(100f);
			houseInfo.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

			houseInfo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			houseInfo.getDefaultCell().setBorder(0);
			houseInfo.getDefaultCell().setPadding(0);

			Image i = PDFUtil.createImage(LOGO_IMG);
			if (i != null) {
				houseInfo.addCell(i);
			}
			if (p.estateLogo != null && (i = PDFUtil.createImage(p.estateLogo)) != null) {
				houseInfo.getDefaultCell().setPaddingTop(5);
				houseInfo.getDefaultCell().setPaddingLeft(20);
				houseInfo.getDefaultCell().setPaddingRight(20);
				houseInfo.addCell(i);
				houseInfo.getDefaultCell().setPaddingLeft(0);
				houseInfo.getDefaultCell().setPaddingRight(0);
			} else {
				houseInfo.addCell(new Paragraph("Problem with estate " + String.valueOf(p.estateLogo), lotDetailFont));
			}
			Paragraph details = new Paragraph();
			details.add(new Chunk("\n", spacerFont));
			details.add(new Chunk(p.address, lotPlanFontBlack));
			if (p.lotSize > 0) {
				addArea(details, "", String.valueOf(new Double(p.lotSize).intValue()), lotDetailFont, lotDetailFontsmall);
			}
			if (p.stageName != null) {
				details.add(new Chunk(" " + p.stageName, lotDetailFont));
			}
			details.add(new Chunk("\n", lotDetailFont));

			details.add(new Chunk("\n", spacerFont));
			details.add(new Chunk(WordUtils.capitalizeFully(p.estateName) + "\n", locationFont));
			details.add(new Chunk("\n", spacerFont));
			details.add(new Chunk(WordUtils.capitalizeFully(p.suburb) + "\n", locationFont));

			houseInfo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			houseInfo.getDefaultCell().setPaddingLeft(25);

			houseInfo.addCell(details);
			pageTable.addCell(houseInfo);

			if ((i = PDFUtil.createImage(p.heroImage)) != null) {
				// will need to be sized
				pageTable.addCell(i);
			} else {
				pageTable.addCell(new Paragraph("Problem with hero " + p.heroImage, lotDetailFont));
			}

			pageTable.getDefaultCell().setColspan(2);
			pageTable.getDefaultCell().setPaddingTop(3);
			pageTable.getDefaultCell().setPaddingLeft(25);
			pageTable.getDefaultCell().setPaddingBottom(10);
			pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			pageTable.getDefaultCell().setBackgroundColor(PDFUtil.aqua);
			pageTable.addCell(new Paragraph(InitServlet.getSystemParams().getProperty("PDF-HnlTitle", "FIXED PRICE HOUSE AND LAND PACKAGES"), subHeader));

			pageTable.getDefaultCell().setColspan(2);
			pageTable.getDefaultCell().setPaddingTop(0);
			pageTable.getDefaultCell().setPaddingBottom(0);

			if (p.range != null) {
				pageTable.getDefaultCell().setBackgroundColor(p.range.getColor());
			} else {
				pageTable.getDefaultCell().setBackgroundColor(new BaseColor(102, 102, 102));
			}

			pageTable.getDefaultCell().setColspan(1);
			pageTable.getDefaultCell().setPaddingTop(0f);
			pageTable.addCell(new Paragraph(p.price, singlePriceFont));
			pageTable.getDefaultCell().setPaddingTop(0f);
			pageTable.getDefaultCell().setPaddingLeft(0f);
			houseInfo = new PdfPTable(9);
			houseInfo.setWidthPercentage(100f);
			// houseInfo.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

			houseInfo.setWidths(new float[] { 10, 50, 5, 5, 5, 5, 5, 5, 5 });
			houseInfo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			houseInfo.getDefaultCell().setBorder(0);
			houseInfo.getDefaultCell().setPadding(0);

			/* range image */

			if (p.range != null) {
				pageTable.getDefaultCell().setBackgroundColor(p.range.getColor());
			} else {
				pageTable.getDefaultCell().setBackgroundColor(new BaseColor(102, 102, 102));
			}

			if (p.range != null && (i = PDFUtil.createImage(homePath + p.range.getLogo())) != null) {
				houseInfo.addCell(i);
			} else {
				houseInfo.addCell(new Paragraph(""));
			}
			houseInfo.getDefaultCell().setPaddingLeft(5f);
			houseInfo.getDefaultCell().setPaddingTop(12f);
			houseInfo.getDefaultCell().setPaddingBottom(10f);
			houseInfo.addCell(new Paragraph(p.name.toUpperCase(), lotPlanFont));

			int iPadTop = 3, iPadBottom = 3, iPadRight = 4, iHeight = 12;
			float iPadImg = 12f, iPadText = 11f;
			houseInfo.getDefaultCell().setPaddingTop(iPadText);
			houseInfo.getDefaultCell().setPaddingBottom(15);
			PdfPTable iTable = null;
			if ((i = PDFUtil.createImage(homePath + BED_IMG)) != null) {
				houseInfo.getDefaultCell().setPaddingTop(iPadImg);
				iTable = new PdfPTable(1);
				iTable.getDefaultCell().setBorder(0);
				iTable.getDefaultCell().setPadding(0);
				iTable.getDefaultCell().setPaddingTop(iPadTop);
				iTable.getDefaultCell().setPaddingRight(iPadRight);
				iTable.getDefaultCell().setFixedHeight(iHeight);
				iTable.getDefaultCell().setBorderWidthRight(1f);
				iTable.getDefaultCell().setBorderColorRight(BaseColor.WHITE);
				iTable.addCell(i);
				houseInfo.addCell(iTable);
				houseInfo.getDefaultCell().setPaddingTop(iPadText);
			} else {
				houseInfo.addCell(new Paragraph("Rooms: ", lotPlanFont));
			}
			houseInfo.addCell(new Paragraph(String.valueOf(p.bedrooms), lotPlanFont));
			if ((i = PDFUtil.createImage(homePath + CAR_IMG)) != null) {
				houseInfo.getDefaultCell().setPaddingTop(iPadImg);
				iTable = new PdfPTable(1);
				iTable.getDefaultCell().setBorder(0);
				iTable.getDefaultCell().setPadding(0);
				iTable.getDefaultCell().setPaddingTop(iPadTop);
				iTable.getDefaultCell().setPaddingRight(iPadRight);
				iTable.getDefaultCell().setFixedHeight(iHeight);
				iTable.getDefaultCell().setBorderWidthRight(1f);
				iTable.getDefaultCell().setBorderColorRight(BaseColor.WHITE);
				iTable.addCell(i);
				houseInfo.addCell(iTable);
				houseInfo.getDefaultCell().setPaddingTop(iPadText);
			} else {
				houseInfo.addCell(new Paragraph("Cars: ", lotPlanFont));
			}
			houseInfo.addCell(new Paragraph(String.valueOf(p.carParks), lotPlanFont));
			if ((i = PDFUtil.createImage(homePath + BATH_IMG)) != null) {
				houseInfo.getDefaultCell().setPaddingTop(iPadImg);
				iTable = new PdfPTable(1);
				iTable.getDefaultCell().setBorder(0);
				iTable.getDefaultCell().setPadding(0);
				iTable.getDefaultCell().setPaddingTop(iPadTop);
				iTable.getDefaultCell().setPaddingRight(iPadRight);
				iTable.getDefaultCell().setFixedHeight(iHeight);
				iTable.getDefaultCell().setBorderWidthRight(1f);
				iTable.getDefaultCell().setBorderColorRight(BaseColor.WHITE);
				iTable.addCell(i);
				houseInfo.addCell(iTable);
				houseInfo.getDefaultCell().setPaddingTop(iPadText);
			} else {
				houseInfo.addCell(new Paragraph("Baths: ", lotPlanFont));
			}
			houseInfo.addCell(new Paragraph(String.valueOf(p.bathrooms), lotPlanFont));

			houseInfo.addCell(BLANK);

			pageTable.addCell(houseInfo);
			pageTable.getDefaultCell().setPaddingTop(10);
			pageTable.getDefaultCell().setPaddingLeft(12);
			pageTable.getDefaultCell().setPaddingRight(1);
			pageTable.getDefaultCell().setPaddingBottom(0);
			pageTable.getDefaultCell().setBackgroundColor(bottomLeftGrey);

			houseInfo = new PdfPTable(2);
			houseInfo.setWidths(new float[] { 5, 60 });
			houseInfo.getDefaultCell().setBorder(0);
			houseInfo.getDefaultCell().setPaddingLeft(15);
			houseInfo.getDefaultCell().setPaddingBottom(5f);
			houseInfo.getDefaultCell().setColspan(2);
			houseInfo.getDefaultCell().setPaddingRight(10);
			if (p.designDetail != null) {
				houseInfo.getDefaultCell().setLeading(12f, 0f);
				houseInfo.addCell(new Paragraph(p.designDetail, packageDetailFont));

			}
			pageTable.getDefaultCell().setPaddingRight(0);
			houseInfo.getDefaultCell().setPaddingBottom(0f);
			houseInfo.addCell(new Paragraph("PACKAGE\nINCLUSIONS", p.range != null ? p.range.getInclusionsFont(packageInclusionsHeader) : packageInclusionsHeader));
			houseInfo.getDefaultCell().setColspan(1);
			houseInfo.getDefaultCell().setPaddingLeft(2);

			i = p.range != null ? PDFUtil.createImage(homePath + p.range.getTick()) : null;
			houseInfo.getDefaultCell().setPaddingBottom(2f);
			houseInfo.getDefaultCell().setPaddingTop(2f);
			houseInfo.getDefaultCell().setPaddingRight(5f);
			for (String s : p.inclusions) {
				if (i != null) {
					houseInfo.getDefaultCell().setPaddingTop(4f);
					houseInfo.getDefaultCell().setPaddingLeft(2);
					houseInfo.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
					houseInfo.addCell(i);
					houseInfo.getDefaultCell().setPaddingLeft(1);
					houseInfo.getDefaultCell().setPaddingTop(2f);
				} else
					houseInfo.addCell(new Paragraph("\u2022", packageDetailFont)); // bullet
				houseInfo.addCell(new Paragraph(s, packageDetailFont));
			}

			houseInfo.getDefaultCell().setColspan(2);
			houseInfo.getDefaultCell().setPaddingTop(5f);
			houseInfo.addCell(BLANK);

			houseInfo.getDefaultCell().setColspan(2);

			/** CUSTOM CODE : Place holder for DRIPS **/
			String imgStr = "";
			if (p.dripImg1 != null)
			{	
					PdfPCell cell = null;
					 imgStr = homePath + "/" + p.dripImg1;
					try
					{
							URL imgUrl = new URL(imgStr);
							java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
							
							Image dripImg = Image.getInstance(imgUri.toString());
							//dripImg.setAbsolutePosition(180, 87);
							// dripImg.scaleToFit(125, 67);
							dripImg.scaleToFit(170, 90);
							
							dripImg.setBorder(0);
							dripImg.setBorderWidth(0);
							///dripImg.setUseVariableBorders(false);
							
							
							cell = new PdfPCell(dripImg);
							cell.setBorderWidth(0f);
							cell.setBorder(0);
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setPaddingBottom(10);

							houseInfo.getDefaultCell().setPaddingTop(10f);
							houseInfo.getDefaultCell().setPaddingBottom(10f);
							houseInfo.getDefaultCell().setPaddingLeft(10f);
							houseInfo.getDefaultCell().setPaddingRight(10f);
							houseInfo.getDefaultCell().setBackgroundColor(bottomLeftGrey);
							houseInfo.getDefaultCell().setBorder(0);
							houseInfo.getDefaultCell().setBorderWidth(0);
							//dripTable.getDefaultCell().setBorderColor(BaseColor.WHITE);
							//dripTable.getDefaultCell().setBorderWidthRight(20f);
							houseInfo.addCell(cell);
					}
					catch(Exception ee)
					{
							cell = new PdfPCell(new Paragraph("Problem with image" + imgStr ));
							System.out.println("Exception Occurred. Message is: " + ee.getMessage());
							ee.printStackTrace();
					}
			}

			
			/*
			  for (String img : p.promoImages) { if ((i = PDFUtil.createImage(new
			  StringBuilder(100).append(homePath).append("/").append(img).toString())) != null) {
			  houseInfo.getDefaultCell().setPaddingRight(0); houseInfo.addCell(BLANK);
			  houseInfo.getDefaultCell().setPadding(0); houseInfo.getDefaultCell().setPaddingLeft(23);
			  houseInfo.getDefaultCell().setPaddingRight(35); houseInfo.addCell(i); } }
			*/
			

			/*PdfPTable blankTable = new PdfPTable(3);
			blankTable.setWidths(new float[] { 5, 40, 10 });
			blankTable.getDefaultCell().setBorder(0);
			blankTable.getDefaultCell().setPadding(0);
			blankTable.getDefaultCell().setFixedHeight(60f);
			blankTable.getDefaultCell().setBackgroundColor(bottomLeftGrey);
			blankTable.addCell(BLANK);
			blankTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
			blankTable.addCell(BLANK);
			blankTable.getDefaultCell().setBackgroundColor(bottomLeftGrey);
			blankTable.addCell(BLANK);
			houseInfo.getDefaultCell().setPaddingTop(15f);
			houseInfo.addCell(blankTable);*/
			/**********************************/

			houseInfo.getDefaultCell().setColspan(1);
			houseInfo.addCell(BLANK);
			houseInfo.getDefaultCell().setPaddingLeft(0);
			houseInfo.getDefaultCell().setPaddingRight(10);
			houseInfo.getDefaultCell().setPaddingTop(5f);
			houseInfo.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
			houseInfo.getDefaultCell().setPaddingBottom(25f);
			houseInfo.getDefaultCell().setLeading(9f, 0f);
			houseInfo.addCell(new Paragraph(p.disclaimer, disclaimerFont));

			pageTable.addCell(houseInfo);
			/* add the home plan */
			pageTable.getDefaultCell().setPaddingTop(4);
			pageTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
			houseInfo = new PdfPTable(2);
			houseInfo.setWidths(new int[] { 46, 45 });
			houseInfo.getDefaultCell().setBorder(0);
			houseInfo.getDefaultCell().setPadding(0);
			houseInfo.getDefaultCell().setPaddingRight(15f);
			houseInfo.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
			houseInfo.getDefaultCell().setColspan(2);

			if ((i = PDFUtil.createImage(p.planImage)) != null) {
				houseInfo.addCell(i);
			} else {
				houseInfo.addCell(new Paragraph("Sorry, the home plan image could not be displayed"));
			}

			houseInfo.getDefaultCell().setColspan(1);
			houseInfo.getDefaultCell().setPaddingRight(0);
			houseInfo.addCell(ListFooter.getRepParagraph(p.repName, p.repMobile, p.repPhone, p.repEmail));
			houseInfo.addCell(ListFooter.getLocationParagraph(p.locName, p.locStreet, p.locCity, p.displayHours));
			pageTable.getDefaultCell().setPaddingBottom(23.5f);
			pageTable.addCell(houseInfo);
			document.add(pageTable);
		}
		
	}

	static final BaseColor priceGrey = new BaseColor(68, 68, 70); // #444446
	static final BaseColor featureGrey = new BaseColor(102, 102, 102); // #666666
	static final BaseColor bottomLeftGrey = new BaseColor(53, 52, 53); // #353435

	static final BaseColor featureBG = new BaseColor(238, 239, 241); // #EEEFF1
	
	private static final Font lotPlanFontBlack = FontFactory.getFont("Arial", 14f, Font.BOLD, BaseColor.BLACK);
	
	/* use for area / stage nmae ie (512m .. ) Stage 11 */
	static final Font lotDetailFont = FontFactory.getFont("Arial", 11.0f, BaseColor.BLACK);
	/* use for little 2 in m2 */
	static final Font lotDetailFontsmall = FontFactory.getFont("Arial", 6f, BaseColor.BLACK);

	static final Font spacerFont = FontFactory.getFont("Arial", 3f, BaseColor.WHITE);

	/* use for estate and suburb ie Fairview / Mernda */
	static final Font locationFont = FontFactory.getFont("Arial", 13.5f, BaseColor.BLACK);

	/* FIXED PRICE HOUSE .. etc */
	static final Font subHeader = FontFactory.getFont("Arial", 26.5f, BaseColor.WHITE);
	/* package price */
	static final Font singlePriceFont = FontFactory.getFont("Arial", 33.5f, Font.BOLD, BaseColor.WHITE);

	/* home description, package inclusions */
	static final Font packageDetailFont = FontFactory.getFont("Arial", 9.42f, BaseColor.WHITE);
	/*
	 * not referenced directly, used within the Range enum but here to keep settings in the same
	 * place
	 */
	static final Font packageInclusionsHeader = FontFactory.getFont("Arial", 12f, Font.BOLD, BaseColor.WHITE);

	static final Font disclaimerFont = FontFactory.getFont("Arial", 6.0f, BaseColor.WHITE);

	// private static final DecimalFormat AREA_FMT = new
	// DecimalFormat("#.##m2");
	// private static final DecimalFormat LENGTH_FMT = new
	// DecimalFormat("#.##m");

	public String getShortText(String inputtext, int size) {
		if (inputtext.length() > size) {
			inputtext = inputtext.substring(0, size);
			inputtext = inputtext + "...";
		}
		return (inputtext);
	}

	void insertPage(PdfWriter writer, String file) throws Exception {
		PdfReader reader = new PdfReader(file);
		PdfContentByte cb = writer.getDirectContent();
		PdfImportedPage page = writer.getImportedPage(reader, 1);
		cb.addTemplate(page, 0, 0);
	}

	Paragraph getArea(String prefix, String s) {
		return getArea(prefix, s, lotDetailFont, lotDetailFontsmall);
	}

	Paragraph getArea(String prefix, String s, Font font, Font smallFont) {
		return addArea(new Paragraph(), prefix, s, font, smallFont);
	}

	Paragraph addArea(Paragraph p, String prefix, String s, Font font, Font smallFont) {
		p.add(new Chunk(prefix, font));
		p.add(new Chunk(" (", font));
		p.add(new Chunk(s, font));
		p.add(new Chunk("m", font));
		Chunk c = new Chunk("2", smallFont);
		c.setTextRise(4.0f);
		p.add(c);
		p.add(new Chunk(")", font));
		return (p);
	}

	public SinglePackage getPackage(HttpServletRequest request, PropertyEntity property) {
		// PropertyEntity property =
		// PropertyFactory.getPropertyEntity(request,productID);
		if (property == null) {
			throw new RuntimeException("Product was not found");
		}

		if (property.isPropertyType(PropertyType.HouseLandPackage)) {
			return populateHandLPackage(request, property);
		} else if(property.isPropertyType(PropertyType.HomePlan)){
			return populateHome(request, property);
		} else {
			throw new RuntimeException("Property [" + property.getPropertyType() + "] was not a house and land package");
		}
	}
	
	
	private SinglePackage populateHandLPackage(HttpServletRequest request, PropertyEntity property){
		HouseLandPackage product = (HouseLandPackage) property;

		final SinglePackage p = new SinglePackage();
		p.isHAndLPackage = true;
		p.address = product.getLotName();
		p.filename = StringUtil.ToAlphaNumeric(product.getName());
		if (StringUtils.isBlank(p.filename)) {
			p.filename = "flyer";
		}
		p.heroImage = PropertyEntity.resizeImage(product.getImage(), 605, 425, PropertyEntity.ResizeOption.Crop, dev);
		p.planImage = PropertyEntity.resizeImage(product.getPlanImage(), 705, 717, PropertyEntity.ResizeOption.Pad, dev, true, 0, 0);
		p.estateName = product.getEstateName();
		p.estateLogo = PropertyEntity.resizeImage(product.getEstateImage(), 350, 170, PropertyEntity.ResizeOption.Pad, dev);
		p.range = Range.fromString(product.getRangeName());
		p.name = product.getName();
		p.price = product.getCost();
		p.designDetail = product.getHomePlan() != null ? product.getHomePlan().getString("Description") : null;

		// p.houseInfo.getDefaultCell().setPaddingTop(13f);
		p.carParks = product.getCarParks();
		//p.bedrooms = product.getBedrooms();
		p.bathroomsStr = product.getBathroomsAsString();
		p.features = product.getPlanFeatures();
		p.lotSize = product.getLotSize();
		p.stageName = product.getStageName();
		p.suburb = product.getLand() != null ? product.getLand().getString("City") : null;

		GenRow repDetails = StringUtils.isBlank(request.getParameter("RepUserID")) ? property.getSalesRep() : PDFUtil.getRep(request.getParameter("RepUserID"));
		p.repName = repDetails.getString("FirstName") + " " + repDetails.getString("LastName");
		p.repMobile = repDetails.getString("Mobile");
		p.repPhone = repDetails.getString("Phone");
		p.repEmail = PDFUtil.getRepEmail(repDetails.getString("Email"));

		if (StringUtils.isNotBlank(request.getParameter("DisplayID"))) {
			GenRow displayDetails = PDFUtil.getDisplayLocation(request.getParameter("DisplayID"));
			p.locName = displayDetails.getString("Name");
			p.locStreet = displayDetails.getString("Street");
			p.locCity = displayDetails.getString("City");
			p.displayHours = PDFUtil.getDisplayHours(displayDetails.getString("ProductID"));
		}

		//p.promoImages = property.getDripImage(request, new String[] { rangePID, homePID, property.getString(PRODUCTID) }, getPdfSimpleName(), "2"); // Drip 02 - Value Added Image
		p.dripImg1 = property.getDripImage(request, getPdfSimpleName(), "1"); // Drip 02 - Value Added Image

		return p;
	}
	
	private SinglePackage populateHome(HttpServletRequest request, PropertyEntity property){
		final SinglePackage p = new SinglePackage();
		p.isHome = true;
		HomePlan product = (HomePlan) property;
		
		//p.address = product.getLotName();
		p.filename = StringUtil.ToAlphaNumeric(product.getName());
		if (StringUtils.isBlank(p.filename)) {
			p.filename = "flyer";
		}
		p.heroImage = PropertyEntity.resizeImage(product.getHeroImage(), 605, 425, PropertyEntity.ResizeOption.Crop, dev);
		p.planImage = PropertyEntity.resizeImage(product.getPlanImage(), 705, 717, PropertyEntity.ResizeOption.Pad, dev, true, 0, 0);
		//p.estateName = product.getEstateName();
		//p.estateLogo = PropertyEntity.resizeImage(product.getEstateImage(), 350, 170, PropertyEntity.ResizeOption.Pad, dev);
		p.range = Range.fromString(product.getRangeName());
		p.name = product.getName();
		p.price = product.getCost();
		//p.designDetail = product.getHomePlan() != null ? product.getHomePlan().getString("Description") : null;

		// p.houseInfo.getDefaultCell().setPaddingTop(13f);
		p.carParks = product.getCarParks();
		p.bedrooms = product.getBedrooms();
		p.bathrooms = product.getBathrooms();
		//p.features = product.getPlanFeatures();
		//p.lotSize = product.getLotSize();
		//p.stageName = product.getStageName();
		//p.suburb = product.getLand() != null ? product.getLand().getString("City") : null;

		GenRow repDetails = StringUtils.isBlank(request.getParameter("RepUserID")) ? property.getSalesRep() : PDFUtil.getRep(request.getParameter("RepUserID"));
		p.repName = repDetails.getString("FirstName") + " " + repDetails.getString("LastName");
		p.repMobile = repDetails.getString("Mobile");
		p.repPhone = repDetails.getString("Phone");
		p.repEmail = PDFUtil.getRepEmail(repDetails.getString("Email"));

		if (StringUtils.isNotBlank(request.getParameter("DisplayID"))) {
			GenRow displayDetails = PDFUtil.getDisplayLocation(request.getParameter("DisplayID"));
			p.locName = displayDetails.getString("Name");
			p.locStreet = displayDetails.getString("Street");
			p.locCity = displayDetails.getString("City");
			p.displayHours = PDFUtil.getDisplayHours(displayDetails.getString("ProductID"));
		}

		//p.promoImages = property.getDripImage(request, new String[] { rangePID, homePID, property.getString(PRODUCTID) }, getPdfSimpleName(), "2"); // Drip 02 - Value Added Image
		p.dripImg1 = property.getDripImage(request, getPdfSimpleName(), "1"); // Drip 02 - Value Added Image

		return p;
	}
	

	public String getPdfSimpleName() {
		return "Detail";
	}

	
	public void setMargins(Document document) {
		document.setMargins(0, 0, 0, 0);
	}
}