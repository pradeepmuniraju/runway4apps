package com.sok.runway.crm.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.TemplateURLs;
import com.sok.runway.crm.cms.MasterTemplate;
import com.sok.runway.crm.cms.Template;

/**
 * When time permits, merge legacy uploader into the new
 * TemplateUploader. The names were kept to minimise system
 * changes as much as possible.
 * @author mikey
 * @deprectaed
 */
public class LegacyUploader extends Uploader {

	protected File htmlfile = null;
	protected String htmltext = "";
	protected String html = null;
	protected String bodyhtml = "";
	protected String headerhtml = "";
	protected String footerhtml = "";
	
	//TODO REMOVE
	private final java.io.PrintStream out = System.out; 

	protected int startIndex = -1;
	protected int endIndex = -1;

	protected String	rwid = null;
	protected String	element = null;

	public static enum LastRender {
		EDIT, FINAL
	};

	public static enum ElementType {
		LINK, IMAGE, MAP
	};

	protected LastRender lastRender = null;

	private ArrayList<Map<String, String>> links = null;
	private ArrayList<Map<String, String>> images = null;
	private ArrayList<ImageMap> imagemaps = null;
	
	// Contains an ordered list of link indexes as parsed in the HTML file
	protected Map<String, TemplateLink> templateLinks = null;

	protected static final Pattern START_ANCHOR = Pattern.compile("<[a|A] ");
	protected static final Pattern END_ANCHOR = Pattern.compile("</[a|A]>");

	protected static final Pattern START_IMG = Pattern.compile("<[i|I][m|M][g|G]");

	protected static final Pattern START_MAP = Pattern.compile("<[m|M][a|A][p|P]");
	protected static final Pattern END_MAP = Pattern.compile("</[m|M][a|A][p|P]>");

	protected static final Pattern START_AREA = Pattern.compile("<[a|A][r|R][e|E][a|A]");

	protected static final Pattern END_TAG = Pattern.compile(">");

	protected static final String START_DIV = "<[d|D][i|I][v|V]";
	protected static final String START_TD = "<[t|T][d|D]";
	protected static final String START_P = "<[p|P]";
	
	protected static final String TEMPLATE_START = "<!--TEMPLATE_START-->";
	protected static final String TEMPLATE_END = "<!--TEMPLATE_END-->";
	
	private static final String TEMPLETE_MARKERS_REGEX = "("+TEMPLATE_START+"|"+TEMPLATE_END+")";
	
	protected static final String BODY_START = "<div class=\"bodyArea\">";
	protected static final String BODY_END = "</div>"; 

	protected Template template = null;
	protected MasterTemplate mastertemplate = null;

	protected TemplateURLs templateURLs = null;

	public LegacyUploader() {
	}

	public LegacyUploader(File f, ServletContext sctx) {

		String directory = "/files/templates/" + KeyMaker.generate(5);

		if (doRun(f, sctx != null ? sctx.getRealPath("") : "/tmp", directory)) {
			setUploadSuccess(true);
			runProcess();
		} else {
			debug("upload failed");
		}
	}

	public LastRender getLastRender() {
		return lastRender;
	}

	public void setBodyHTML(String bodyhtml) {
		this.bodyhtml = bodyhtml;
	}

	public String getBodyHTML() {
		return bodyhtml;
	}

	public String getHeaderHTML() {
		return headerhtml;
	}

	public String getFooterHTML() {
		return footerhtml;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	public Template getTemplate() {
		if (template == null) {
			template = new Template((Connection) null);
		}
		return template;
	}
	
	public void setTemplateLink(String linkIndex, TemplateLink templateLink) {
	   templateLinks.put(linkIndex, templateLink);
	}

	public void setMasterTemplate(MasterTemplate mastertemplate) {
		this.mastertemplate = mastertemplate;
	}

	public MasterTemplate getMasterTemplate() {
		if (mastertemplate == null) {
			mastertemplate = new MasterTemplate((Connection) null);
		}
		return mastertemplate;
	}

	public String getHTMLText() {
		return htmltext;
	}

	public String getRenderedHTML() {
		return html.toString();
	}

	protected void runProcess() {
		if (htmlfile == null) {
			setError("The upload did not contain a html or htm file");
			debug("no html file");
		} else {

			links = new ArrayList<Map<String, String>>();
			images = new ArrayList<Map<String, String>>();
			imagemaps = new ArrayList<ImageMap>();
			
			HTMLParser hp = new HTMLParser();
			hp.parse(htmlfile, links, images, imagemaps);

			if (templateURLs != null) {
			   //System.out.println("Loading TemplateURLs");
				for (Map<String, String> me : links) {
					String hr = me.get("href");
					if (hr.startsWith("$")) {
						hr.substring(0);
						if (hr.startsWith("{")) {
							if (!hr.endsWith("}")) {
								continue;
							}
							hr.substring(1, hr.length() - 1);
						}
						else if (hr.startsWith("%7B") && hr.endsWith("%7D")) {
							hr = hr.substring(3, hr.length() - 3);
						}
						TableData rec = templateURLs.getRecord(hr);
						if (rec != null) {
							me.put("-templateURLName", hr);
							me.put("src", rec.getString("URL"));
						}
					}
				}
			}
		}
	}
	
	public void setTemplateURLs(TemplateURLs templateURLs) {
		this.templateURLs = templateURLs;
	} 
	
   public void processTemplateURLs() {
      templateLinks = new HashMap<String, TemplateLink>();
      
      for(int rowIndex = 0; rowIndex < templateURLs.getSize(); rowIndex++) {
         TableData templateUrl = templateURLs.get(rowIndex);
         //final String linkIndex = templateUrl.getString("LinkIndex");
         final String linkResponse = templateUrl.getString("Response");
         final String linkText = templateUrl.getString("LinkText");
         final String linkUrl = templateUrl.getString("URL");
         final String linkTarget = templateUrl.getString("Target");
         final String linkType = templateUrl.getString("LinkTypeDescription");
         final String linkName = templateUrl.getString("Name");  // Token name
         final String cmsid = templateUrl.getString("CMSID");
         final String imageIndex = templateUrl.getString("ImageIndex");
         final String imageAltText = templateUrl.getString("AltImageText");
         final String imageTitle = templateUrl.getString("ImageTitle");
         
         TemplateLink templateLink = new TemplateLink();
         //templateLink.setLinkIndex(linkIndex);
         templateLink.setLinkResponse(linkResponse);
         templateLink.setLinkText(linkText);
         templateLink.setLinkUrl(linkUrl);
         templateLink.setLinkTarget(linkTarget);
         templateLink.setLinkType(linkType);
         templateLink.setLinkName(linkName);  // Token name
         templateLink.setImageIndex(imageIndex);
         templateLink.setImageAltText(imageAltText);
         templateLink.setImageTitle(imageTitle);
         
         if ("Document".equals(linkType) && linkUrl.indexOf("DocumentID=") >= 0) {
        	 String docID = linkUrl.substring(linkUrl.indexOf("DocumentID=") + 11);
        	 if (docID.indexOf("&") >= 0) {
        		 docID = docID.substring(0,docID.indexOf("&"));
        	 }
        	 templateLink.setFileName(loadDocumentFileName(docID));
         }
         
         templateLinks.put(linkName, templateLink);
      }     
   }
   
   public TemplateLink getTemplateLink(String linkIndex) {
      return templateLinks.get(linkIndex);
   }

	public void parseText(StringBuilder text) {

		links = new ArrayList<Map<String, String>>();
		images = new ArrayList<Map<String, String>>();
		imagemaps = new ArrayList<ImageMap>();

		HTMLParser hp = new HTMLParser();
		hp.parse(text, links, images, imagemaps);
	}

   protected void checkFile(ZipEntry ze, File of) {
      // debug(ze.getName() + "\r\n");
      String name = ze.getName();
      //ignore file forks and hidden files
      if(!name.startsWith(".") && !name.startsWith("_")){
         if(!of.isHidden()){
            if (name.toLowerCase().endsWith(".html") || name.toLowerCase().endsWith(".htm")) {
               if(htmlfile==null){
                  htmlfile = of;
               }else{
                  //if there is more than one html file found, try to use the one in outer most folder.
                  int oldpathdepth = StringUtil.occurrencesOf(htmlfile.getAbsolutePath(), File.pathSeparator);
                  int newpathdepth = StringUtil.occurrencesOf(of.getAbsolutePath(), File.pathSeparator);
                  if(newpathdepth < oldpathdepth){
                     htmlfile = of;
                  }
               }
            }
         }
      }
   }

	public ArrayList<Map<String, String>> getLinks() {
		return links;
	}

	public ArrayList<ImageMap> getImageMaps() {
		return imagemaps;
	}

	public ImageMap getImageMap(String id) {
		for (ImageMap im : imagemaps) {
			if (im.id.equals(id)) {
				return im;
			}
		}
		return (ImageMap) null;
	}

	public ArrayList<Map<String, String>> getImages() {
		return images;
	}

	public int getElementIndex(javax.servlet.http.HttpServletRequest request) {
		return getElementIndex(request, "-elementIndex");
	}

	public int getElementIndex(javax.servlet.http.HttpServletRequest request,
			String indexField) {
		int elementIndex = -1;
		String eid = request.getParameter(indexField);
		try {
			elementIndex = Integer.parseInt(eid);
		} catch (NumberFormatException nfe) {
			throw new RuntimeException("invalid number entered " + eid);
		} catch (NullPointerException npe) {
			throw new RuntimeException("no number entered for element index");
		}
		return elementIndex;
	}

	public Map<String, String> getElement(
			javax.servlet.http.HttpServletRequest request) {
		return getElement(request.getParameter("-elementType"),
				getElementIndex(request));
	}

	public Map<String, String> getElement(String type, int elementIndex) {

		if ("link".equals(type)) {
			return links.get(elementIndex);
		} else if ("image".equals(type)) {
			return images.get(elementIndex);
		} else {
			throw new UnsupportedOperationException(
					"could not get element type");
		}
	}

	public boolean doIt = false;

	public static String replaceMatches(String match, String prefix) {

		Pattern p = Pattern.compile("UPLOADER_ID");

		java.util.regex.Matcher ids = p.matcher(match);
		int i = 0;
		String tString = "";

		// excel file splits into document >
		// splits file into document and saves it as a merge file

		while (match.length() != tString.length()) {
			tString = match;
			match = ids.replaceFirst(prefix + "_" + String.valueOf(i));
			i++;
			ids = p.matcher(match);
		}

		return match;
	}

	public class ImageMap {

		public String id;
		private int start_index = -1, end_index = -1;
		private ArrayList<ImageMapItem> items;
		
		public String toString() {
		   return "[ImageMap id: " + id + " start_index: " + start_index + " end_index: " + end_index + "]"; 
		}

		public ImageMap(String id, ArrayList<ImageMapItem> items) {
			this.id = id;
			this.items = items;
		}

		public ImageMap(String id, int start_index, int end_index,
				ArrayList<ImageMapItem> items) {
			this.id = id;
			this.start_index = start_index;
			this.end_index = end_index;
			this.items = items;
		}

		public String generateImageMap() {

			StringBuilder map = new StringBuilder("<map name=\"");
			map.append(id).append("\" id=\"").append(id).append("\">");
			for (ImageMapItem imi : items) {
				imi.generate(map);
			}
			map.append("</map>");
			return map.toString();
		}

		public String generateJS(String arrayName) {
			StringBuilder map = new StringBuilder();
			for (ImageMapItem imi : items) {
				imi.generateJS(map, arrayName, id);
			}
			return map.toString();
		}

		public ArrayList<ImageMapItem> getItems() {
			return items;
		}

		public int getStart() {
			return start_index;
		}

		public int getEnd() {
			return end_index;
		}

		public void parseRequest(HttpServletRequest r) {

			int area = 0;
			ArrayList<ImageMapItem> links = new ArrayList<ImageMapItem>();

			while (TemplateUploader.getVal(r,
					ImageMapItem.aLink + String.valueOf(area)).length() > 0) {
				links.add(new ImageMapItem(r, area));
				area++;
			}
			this.items = links;
		}
	}

	public class ImageMapItem {

		static final String aName = "-areaName@";
		static final String aLink = "-areaLink@";
		static final String aType = "-areaType@";
		static final String aShape = "-areaShape@";
		static final String aTitle = "-areaTitle@";
		static final String aResponse = "-areaResponse@";
		static final String aCoords = "-areaCoords@";
		static final String aTarget = "-areaTarget@";
		static final String aRemove = "-areaRemove@";

		public String name, href, title, type, shape, response, coords, target;

		public ImageMapItem(String name, String href, String type,
				String shape, String title, String response, String coords) {
			this.name = name;
			this.href = href;
			this.type = type;
			this.shape = shape;
			this.title = title;
			this.response = response;
			this.coords = coords;
		}

		public ImageMapItem(Map<String, String> params) {
			this.name = params.get("name");
			if (this.name == null) { // ie, is loaded from existing template
				this.name = "Image" + KeyMaker.generate(4);
			}
			this.href = params.get("href");
			this.type = params.get("type");
			this.shape = params.get("shape");
			this.title = params.get("title");
			this.target = params.get("target");
			this.coords = params.get("coords");
		}

		public ImageMapItem(HttpServletRequest request, int index) {
			this.name = request.getParameter(aName + String.valueOf(index));
			this.href = request.getParameter(aLink + String.valueOf(index));
			this.type = request.getParameter(aType + String.valueOf(index));
			this.shape = request.getParameter(aShape + String.valueOf(index));
			this.title = request.getParameter(aTitle + String.valueOf(index));
			this.response = request.getParameter(aResponse
					+ String.valueOf(index));
			this.coords = request.getParameter(aCoords + String.valueOf(index));
			this.target = request.getParameter(aTarget + String.valueOf(index));
		}

		public void generate(StringBuilder map) {
			map.append("<area href=\"").append(href).append("\"");
			map.append(" shape=\"").append(shape).append("\"");
			map.append(" coords=\"").append(coords).append("\"");
			if (title != null) {
				map.append(" title=\"").append(title).append("\"");
			}
			if (target != null) {
				map.append(" target=\"").append(target).append("\"");
			}
			map.append(" />");
		}

		public void generateJS(StringBuilder map, String arrayName, String mapid) {
			/*
			 * var item = new MapItem('<%=items.name%>', $('#linkURL').val(),
			 * $('#linkType').val(), $('#linkResponse').val(),
			 * $('#linkTarget').val(),coords, linkCoords);
			 * 
			 * var style = { position: 'absolute', left: $('#mapX').val()+'px',
			 * top: $('#mapY').val()+'px', width: $('#mapW').val()+'px', height:
			 * $('#mapH').val()+'px' };
			 * 
			 * mapItems[mapItems.length] = item;
			 */
			String name = "M" + KeyMaker.generate(5);
			map.append(arrayName).append("[").append(arrayName).append(
					".length] = new MapItem('");
			map.append(mapid).append("','");
			map.append(name).append("','");
			map.append(href).append("','");
			map.append(type).append("','");
			map.append(response).append("','");
			map.append(target != null ? target : "_blank").append("','");
			map.append(coords).append("'); \r\n");
		}
		
	}
	
   public static String getVal(HttpServletRequest request, String param) {
      String val = "";
      if(request != null && param != null) {
         String requestVal = request.getParameter(param);
         if(requestVal != null && !requestVal.equals("null")) {
            val = requestVal;
         }
      }
      return val;
   }	

	public static int getInt(HttpServletRequest request, String param) {

		String t = getVal(request, param);
		if (t.length() == 0) {
			return -1;
		}
		try {
			return Integer.parseInt(t);
		} catch (NumberFormatException nfe) {
			return -1;
		}
	}
	
	public Map<String, String> getLinkElement(int index) {
	   return getElementMap(index, "link");
	}
	
	private Map<String, String> getImageElement(int index) {
	   return getElementMap(index, "image");
	}
	
	private Map<String, String> getElementMap(int index, String linkType) {
	   return index >= 0 ? this.getElement(linkType, index) : null;
	}

	public void editImage(HttpServletRequest r) {
		int imageIx = getInt(r, "-imageIndex");
		int linkIx = getInt(r, "-linkIndex");
		
		// System.out.println("-linkURL: " + getVal(r, "-linkURL"));
		// System.out.println("-imageIndex: " + imageIx);
		// System.out.println("-linkIx: " + linkIx);

		Map<String, String> image = getImageElement(imageIx);
		
		if (image == null) {
			throw new RuntimeException("Could not find image at index " + imageIx);
		}
		if (getVal(r, "-imgSrc").length() > 0) {
			image.put("src", "$URLHome/" + getVal(r, "-imgSrc"));
		}
		image.put("alt", getVal(r, "-imgAlt"));
		image.put("title", getVal(r, "-imgTitle"));

		if (getVal(r, "-imgWidth").length() > 0) {
			image.put("width", getVal(r, "-imgWidth"));
		}
		if (getVal(r, "-imgHeight").length() > 0) {
			image.put("height", getVal(r, "-imgHeight"));
		}

		Map<String, String> link = getLinkElement(linkIx);

		if (getVal(r, "-areaLink@0").length() > 0) {

			if (image.get("border") == null) {
				image.put("border", "0"); // explicitly set border attribute
			}

			String mapID = image.get("usemap");

			if (mapID != null && mapID.startsWith("#")) {
				mapID = mapID.substring(1);
			}
			ImageMap im = null;
			boolean newMap = false;
			if (mapID != null) {
				im = this.getImageMap(mapID);
				if (im == null) {
					im = new ImageMap(mapID, new ArrayList<ImageMapItem>());
					newMap = true;
				}
			} else {
				mapID = "M" + KeyMaker.generate(5);
				image.put("usemap", mapID);
				im = new ImageMap(mapID, new ArrayList<ImageMapItem>());
				newMap = true;
			}

			im.parseRequest(r);

			if (newMap) {
				getImageMaps().add(im);
			}

			if (link != null) {
				// System.out.println("removing link" + linkIx + "in place of map " + mapID);
				getLinks().remove(linkIx);
			}
		} else if (getVal(r, "-linkURL").length() > 0) { // has link
			// System.out.println("doing link");
			if (link == null) {
				if (image.get("border") == null) {
					image.put("border", "0"); // explicitly set border attribute
				}

				// System.out.println("adding new link");
				link = new HashMap<String, String>();
				link.put("start_index", image.get("start_index"));
				link.put("end_index", image.get("end_index"));

				int si = Integer.parseInt(link.get("start_index"));

				// System.out.println("si " + si);

				for (int i = 0; i < getLinks().size(); i++) {
					Map<String, String> me = getLinks().get(i);
					int mi = Integer.parseInt(me.get("start_index"));

					// System.out.println("mi " + mi);

					if (mi > si) {
						//System.out.println("inserting link at new position " + i);
						getLinks().add(i, link);
						break;
					}
				}
				// System.out.println("link break");
			}
			link.put("href", getVal(r, "-linkURL"));
			link.put("-linkType", getVal(r, "-linkType"));
			link.put("-linkStafMsg", getVal(r, "-linkStafMsg"));
			link.put("-linkResponse", getVal(r, "-linkResponse"));
			link.put("target", getVal(r, "-linkTarget"));
			link.put("-linkDocURL", getVal(r, "-linkDocURL"));
		}
		
		// Add link to TemplateUploader#templateLinks
		TemplateLink templateLink = getTemplateLink("" + linkIx);
		if(templateLink == null) {
		   templateLink = new TemplateLink();
		}
      templateLink.setLinkUrl(getVal(r, "-linkURL"));
      templateLink.setLinkType(getVal(r, "-linkType"));
      templateLink.setLinkResponse(getVal(r, "-linkResponse"));
      templateLink.setLinkStafMsg(getVal(r, "-linkStafMsg"));
      templateLink.setLinkDocURL(getVal(r, "-linkDocURL"));
      
      setTemplateLink("" + linkIx, templateLink);
	}

	public void editLink(HttpServletRequest request) {

		int linkIx = getInt(request, "-linkIndex");
		Map<String, String> link = linkIx > -1 ? this
				.getElement("link", linkIx) : null;

		if (link == null) {
			throw new RuntimeException("could not find link at index " + linkIx);
		}

		link.put("-linkURL", getVal(request, "-linkURL"));
		link.put("-linkType", getVal(request, "-linkType"));
		link.put("-linkStafMsg", getVal(request, "-linkStafMsg"));
		link.put("-linkResponse", getVal(request, "-linkResponse"));
		link.put("target", getVal(request, "-linkTarget"));
		link.put("-linkDocURL", getVal(request, "-linkDocURL"));
		
      // Add link to TemplateUploader#templateLinks
      TemplateLink templateLink = getTemplateLink("" + linkIx);
      if(templateLink == null) {
         templateLink = new TemplateLink();
      }
      templateLink.setLinkUrl(getVal(request, "-linkURL"));
      templateLink.setLinkType(getVal(request, "-linkType"));
      templateLink.setLinkResponse(getVal(request, "-linkResponse"));
      templateLink.setLinkStafMsg(getVal(request, "-linkStafMsg"));
      templateLink.setLinkDocURL(getVal(request, "-linkDocURL"));
      
      setTemplateLink("" + linkIx, templateLink);
      
		com.sok.Debugger.getDebugger().debug("done editing link");
	}

	public static String parameteriseElements(String match) {

		match = match.replaceAll(START_TD, Matcher
				.quoteReplacement("<td rwid=\"UPLOADER_ID\""));
		match = replaceMatches(match, "TD");
		match = match.replaceAll(START_DIV, Matcher
				.quoteReplacement("<div rwid=\"UPLOADER_ID\""));
		match = replaceMatches(match, "DIV");
		match = match.replaceAll(START_P, Matcher
				.quoteReplacement("<p rwid=\"UPLOADER_ID\""));
		match = replaceMatches(match, "P");
		return match;
	}

	public boolean setElement(String rwid, String elementType, String innerHTML) {
		
		int[] elementIndex = getElementPosition(rwid, elementType);

		StringBuilder repl = new StringBuilder(this.htmltext);
		repl.replace(elementIndex[0], elementIndex[1], innerHTML);

		int oldLen = elementIndex[1] - elementIndex[0];

		int newLen = innerHTML.length();

		int lenDif = newLen - oldLen;

		//System.out.println("Element located at : " + elementIndex[0] + " to "
		//		+ elementIndex[1]);

		if (lenDif != 0) {
			for (Map<String, String> i : getImages()) {
				int index = Integer.parseInt(i.get("start_index"));
				if (index > 0 && index >= elementIndex[0]) {
					i.put("start_index", String.valueOf(index + lenDif));
					int end = Integer.parseInt(i.get("end_index"));

					if (end < elementIndex[1]) {
						parseText(repl);
						return true;
					}
					i.put("end_index", String.valueOf(end + lenDif));
					//System.out.println("Moved "+i.get("src") + " from "+index
					// + " to "+String.valueOf(index + lenDif));
				}
			}
			for (Map<String, String> i : getLinks()) {
				int index = Integer.parseInt(i.get("start_index"));
				if (index > 0 && index >= elementIndex[0]) {
					i.put("start_index", String.valueOf(index + lenDif));
					int end = Integer.parseInt(i.get("end_index"));
					if (end < elementIndex[1]) {
						parseText(repl);
						return true;
					}
					i.put("end_index", String.valueOf(end + lenDif));
				}
			}
			for (ImageMap i : getImageMaps()) {
				if (i.start_index > 0 && i.start_index > elementIndex[0]) {
					i.start_index = i.start_index + lenDif;

					if (i.end_index < elementIndex[1]) {
						parseText(repl);
						return true;
					}
					i.end_index = i.end_index + lenDif;
				}
			}
		}
		this.htmltext = repl.toString();
		
		if(this.startIndex >= elementIndex[0]) { 
			this.startIndex += lenDif; 
		}
		if(this.endIndex >= elementIndex[0]) { 
			this.endIndex += lenDif; 
		}
		return false;
	}

	public void setBody(String rwid, String elementType) {
	 //  this.removeTemplateMarkers();
		if (rwid == null || elementType == null) return;
		this.rwid = rwid;
		this.element = elementType;

		int[] bodyIndex = getElementPosition(rwid, elementType);

		this.startIndex = bodyIndex[0];
		this.endIndex = bodyIndex[1];
		this.bodyhtml = html.substring(this.startIndex, this.endIndex);
	}

	/**
	 *  Remove any pre-existing <!-- TEMPLATE START --> and 
	 *  <!-- TEMPLATE END --> markers that demarcate the template's
	 *  body from the template's header and footer.
    */
	private void removeTemplateMarkers() {
	   if(this.html != null) {
	      this.html = this.html.replaceAll(TEMPLETE_MARKERS_REGEX, "");
	   }
	}
	
	public String getElement(String rwid, String elementType) {

		int[] bodyIndex = getElementPosition(rwid, elementType);
		
		String ret = htmltext.substring(bodyIndex[0], bodyIndex[1]);
		
		String[] list = getDirectoryList();
		if(list != null) { 
			for (String s : list) {
				if (s.indexOf("$") == -1 && s.indexOf("http://") == -1 && s.indexOf("https://") == -1 && s.indexOf("ftp://") == -1 && s.indexOf("mailto:") == -1) {				
					ret = ret.replace("src=\"" + s, "src=\"$URLHome"
							+ getDirectory() + "/" + s);
				}
			}
		} 
		return ret;
	}

	/**
	 * <p>Returns the start and end indeces of the element of <code>elementType</code> and 
	 * <code>ID</code> = <code>rwid</code>.</p> 
	 * 
	 * @param rwid element ID
	 * @param elementType element tag name
	 * @return the start and end indeces of the element of <code>elementType</code> and 
    * <code>ID</code> of <code>rwid</code>.
	 */
	public int[] getElementPosition(String rwid, String elementType) {

		if (!rwid.toLowerCase().startsWith(elementType.toLowerCase())) {
			throw new RuntimeException("Unexpected element ID \"" + rwid + "\" for element type \"" + elementType + "\"");
		}
		String regex = null;

		if ("P".equals(elementType)) {
			regex = START_P;
		} else if ("TD".equals(elementType)) {
			regex = START_TD;
		} else if ("DIV".equals(elementType)) {
			regex = START_DIV;
		} else {
			throw new UnsupportedOperationException("Element type not supported - " + elementType);
		}

		String check = html;
		Pattern p = Pattern.compile(regex);
		int id = Integer.parseInt(rwid.substring(rwid.indexOf("_") + 1));

		Matcher m = p.matcher(check);

		boolean notFound = true;
		int i = 0;
		int startIndex = 0;

		while (m.find() && notFound) {
			if (i++ == id) {
				notFound = false;
				startIndex = m.start();
			}
		}
		startIndex = check.indexOf(">", startIndex) + 1;
		
		check = check.substring(startIndex).toUpperCase();
		
		//System.out.println(check);

		// i = check.indexOf(elementType); //start after the initial element

		int endIndex = check.length();

		String o1 = "<" + elementType;
		String o2 = "</" + elementType;

		i = findLowest(check, o1, o2, 0);

		if (check.charAt(i) == '<' && check.charAt(i + 1) == '/') {
			// found closing bracket on first try
			endIndex = i;
		} else {
			try {
				int depth = 1;
				int revs = 0;

				boolean freePass = true;

				while (freePass || (depth > 0 && revs < 40)) {
					revs++;

					if (freePass) {
						freePass = false;
					}

					//System.out.println(depth + " " + check.substring(i,
					// i+5));
					if ((check.charAt(i) == '<' && (check.charAt(i + 1) == '/') || ("P"
							.equals(elementType) && check.charAt(i + 1) == 'I'))) {
						depth--;
					} else if (check.charAt(i) == '<') {
						depth++;
					} else {
						freePass = true;
						i = findLowest(check, o1, o2, i + 1);
						continue; // skip this one, it's not a html tag
					}

					if (depth == 0) {
						break; // we've found the end.
					}
					// i = check.indexOf(elementType,i+1);
					i = findLowest(check, o1, o2, i + 1);
				}

				if (depth == 0) {
					endIndex = i;
				} else {
					throw new RuntimeException("Could not find closing <" + elementType + "> tag for element \"" + rwid + "\"");
				}
			} catch (RuntimeException re) {
				throw new RuntimeException(re.getMessage()
						+ ": Perhaps the template is missing a closing <" + elementType
						+ "> tag for element \"" + rwid + "\". Please check whether your template's HTML is well formed (all opening tags have closing tags).");
			}
		}
		endIndex += startIndex;

		int[] ret = new int[2];
		ret[0] = startIndex;
		ret[1] = endIndex;

		return ret;

		//System.out.println(startIndex + " " + endIndex);
	}

	protected int findLowest(String input, String o1, String o2, int start) {

		if (!"<TD".equals(o1)) ++start;
		int o1i = input.indexOf(o1, start);
		int o2i = input.indexOf(o2, start);

		if (o2.equals("</P")) {
			// img tag will auto-close a p tag
			int o3i = input.indexOf("<IMG", start);

			if (o3i < o2i) {
				o2i = o3i;
			}
		}

		if (o2i >= 0 && o1i >= 0) {
			return o1i < o2i ? o1i : o2i;
		} else if (o2i > -1) {
			return o2i;
		} else if (o1i > -1) {
			return o1i;
		} else {
			throw new RuntimeException("Cound not find closing tag [debug input: " + input + " o1: " + o1 + " o2: " + o2 + " start: " + start + " o2i: " + o2i + " o1i: " + o1i + "input.length: " + input.length() + " start-text: " + input.substring(start) + "]");
		}
	}

	public void renderHTML() {
		renderHTML(false, false);
	}

	/**
	 * @deprecated
	 */
	public void renderHTML(boolean notUsed, boolean forEdit) {
		renderHTML(forEdit);
	}

	/**
	 * @param forPreview
	 *            - to be removed, but not yet.
	 * @param editMode
	 */
	public void renderHTML(boolean editMode) {
	   // NOTE: Disabled all map element processing, un-comment each map element code snippet to reenable
	   
	   final char[] excludeChars = {' ', '\r', '\n'};
	   
      StringBuilder html = new StringBuilder();

      int linkIndex = 0;
      int imageIndex = 0;
      
      /* DISABLED: map elements //
      int map = -1; // next map number
      int mapIndex = 0; // map index
      if (this.getImageMaps().size() > 0) {
         map = 0;
         mapIndex = this.getImageMaps().get(map).start_index;
         //System.out.println("first map: "+map+" "+mapIndex);
      }
      // END map elements */

      Map<String, String> currentLink = null;
      Map<String, String> currentImage = null;

      ElementType current = null;  // link, image, or map
      int posn = 0;  // character position in HTML

      int iLoc = -1;  // image location in HTML
      int lLoc = -1;  // link location in HTML

      com.sok.Debugger d = com.sok.Debugger.getDebugger();

      while (linkIndex < links.size() || imageIndex < images.size()) {

         /* DISABLED: map elements //
         while (mapIndex == -1 && map < getImageMaps().size()) {
            mapIndex = this.getImageMaps().get(map).start_index;
            if (mapIndex == -1) { // map not specified location
               map++;
            }
         }
         //System.out.println("end skip5");
         if (map >= getImageMaps().size()) {
            map = -1;
         }
         // END map elements */

         if (linkIndex == links.size()) { 
            // only images left, so set iLoc to image's start_index 

            current = ElementType.IMAGE;
            currentImage = images.get(imageIndex);
            iLoc = Integer.parseInt(currentImage.get("start_index"));
            lLoc = -1;

            /* DISABLED: map elements //
            if (map != -1 && mapIndex < iLoc) {
               html.append(htmltext.substring(posn, mapIndex));
               posn = this.getImageMaps().get(map).end_index;
               map++;
               if (map < this.getImageMaps().size()) {
                  mapIndex = this.getImageMaps().get(map).start_index;
               } else {
                  map = -1;
               }
            }
            // END map elements */

         } else if (imageIndex == images.size()) { 
            // only links left, so set lLoc to link's start_index

            current = ElementType.LINK;
            currentLink = links.get(linkIndex);
            lLoc = Integer.parseInt(currentLink.get("start_index"));
            iLoc = -1;

            /* DISABLED: map elements
            if (map != -1 && mapIndex < lLoc) {
               html.append(htmltext.substring(posn, mapIndex));
               posn = this.getImageMaps().get(map).end_index;
               map++;
               if (map < this.getImageMaps().size()) {
                  mapIndex = this.getImageMaps().get(map).start_index;
               } else {
                  map = -1;
               }
            }
            // END map elements */

         } else {
            currentLink = links.get(linkIndex);
            lLoc = Integer.parseInt(currentLink.get("start_index"));
            
            currentImage = images.get(imageIndex);
            iLoc = Integer.parseInt(currentImage.get("start_index"));
            
            d.debug("checking link " + linkIndex + " ix:" + lLoc + "  image "
                  + imageIndex + " ix:" + iLoc);

            if (lLoc <= iLoc) {
               // Assumption: All anchor elements directly precede an image element
               current = ElementType.LINK;
               d.debug("chose link");
            } else {
               current = ElementType.IMAGE;
               d.debug("chose image");
            }

            /* DISABLED: map elements //
            if (map != -1
                  && mapIndex < (Element.IMAGE == current ? iLoc : lLoc)) {
               html.append(htmltext.substring(posn, mapIndex));
               posn = this.getImageMaps().get(map).end_index;
               
               //System.out.println(this.getImageMaps().get(map).start_index + "_" + this.getImageMaps().get(map).end_index);
               //System.out.println("updated posn to "+posn);
               map++;
               if (map < this.getImageMaps().size()) {
                  mapIndex = this.getImageMaps().get(map).start_index;
               } else {
                  map = -1;
               }
            }
            // END map elements */
         }

         int endPosn;

         //System.out.println("_" + posn + "_" + lLoc + "_" + iLoc); 
         String next = null;  // next element code snippet
         StringBuilder nextS = new StringBuilder();

         switch (current) {
         case LINK:
            //System.out.println("Chose Link"); 
            if (startIndex >= posn && startIndex <= lLoc) {
               html.append(htmltext.substring(posn, startIndex));
               html.append(TEMPLATE_START);
               posn = startIndex;
            }
            
            if (endIndex >= posn && endIndex <= lLoc) {
               html.append(htmltext.substring(posn, endIndex));
               html.append(TEMPLATE_END);
               posn = endIndex;
            }

            next = htmltext.substring(posn, lLoc);
            deleteAll(nextS);
            appendChars(nextS, next, excludeChars);

            if (!isBreakElement(nextS)) {
               html.append(htmltext.substring(posn, lLoc));
            }

            //int start2 = Integer.parseInt(currentLink.get("start_index"));
            endPosn = Integer.parseInt(currentLink.get("end_index"));
            
            //System.out.println("LINK"+endIndex + "_" + posn + "_" + lLoc+ "_" + endPosn);
            /*
            if(this.endIndex > 0) { 
               out.println("_"+htmltext.substring(this.startIndex, this.endIndex)+"_");
            }
            */
            //out.println(htmltext.substring(lLoc,end));
            
            String linktext = htmltext.substring(lLoc, endPosn);
            
            Matcher m = START_IMG.matcher(linktext);

            if ((iLoc > -1 && m.find() && ((m.start() + lLoc) == iLoc))
                  || iLoc == lLoc /* inserted link */) {
               // next image is located in the link
               appendImageLink(html, currentImage, imageIndex, currentLink, linkIndex, editMode);
               imageIndex++;
               linkIndex++;
               //System.out.println("Completed image : "+imageIndex);
               //System.out.println("and Completed link : "+linkIndex);
            } else {
               appendLink(html, currentLink, linkIndex, editMode);
               linkIndex++;
               //System.out.println("Completed link : "+linkIndex);
            }

            // html.append("Link :"+htmltext.substring(lLoc,end));

            posn = endPosn;

            break;
         case IMAGE:
            //System.out.println("Chose Image"); 
            if (startIndex >= posn && startIndex <= iLoc) {
               html.append(htmltext.substring(posn, startIndex));
               html.append(TEMPLATE_START);
               posn = startIndex;
            }
                  
            if (endIndex >= posn && endIndex <= iLoc) {
               html.append(htmltext.substring(posn, endIndex));
               html.append(TEMPLATE_END);
               posn = endIndex;
            }
            //System.out.println(this.endIndex);
            //System.out.println(posn + " " + currentImage.get("start_index") + " " + currentImage.get("end_index"));
            next = htmltext.substring(posn, iLoc);
            deleteAll(nextS);            
            appendChars(nextS, next, excludeChars);

            if (!isBreakElement(nextS)) {
               html.append(htmltext.substring(posn, iLoc));
            }

            endPosn = Integer.parseInt(currentImage.get("end_index"));
            /*
             * //System.out.println("P: "+posn+" L: "+iLoc);
             * 
             * 
             * if(iLoc < posn) { html.append("\r\n\r\n");
             * html.append("Image :"+htmltext.substring(iLoc,end));
             * 
             * html.append("\r\n\r\n");
             * html.append("Posn: \r\n"+htmltext.substring(posn));
             * 
             * throw new RuntimeException("fail");
             * 
             * }
             */
            appendImage(html, currentImage, imageIndex, editMode);

            posn = endPosn;
            imageIndex++;
            //System.out.println("Completed image : "+image++);

            break;
         default:
         }
      }

      if (endIndex >= posn) {
          html.append(htmltext.substring(posn, endIndex));
          html.append(TEMPLATE_END);
          posn = endIndex;
       }
      /* DISABLED map elements //
      while (map != -1) {
         html.append(htmltext.substring(posn, mapIndex));
         posn = this.getImageMaps().get(map).end_index;
         map++;
         if (map < this.getImageMaps().size()) {
            mapIndex = this.getImageMaps().get(map).start_index;
         } else {
            map = -1;
         }
      }
      // END map elements */

      html.append(htmltext.substring(posn));

      StringBuilder after = new StringBuilder();
      for (ImageMap e : imagemaps) {
         after.append(e.generateImageMap());
      }

      //System.out.println("appending maps");

      if (html.indexOf("/RESPONSE/") == -1) {
         after
               .append("<img src=\"$URLHome/RESPONSE/${NoteID}/Viewed_Email.gif\" width=\"1\" height=\"1\" />");
      }

      if (html.indexOf("</body") > -1) {
         html.insert(html.indexOf("</body"), after);
      } else {
         html.append(after);
      }

      if (editMode) {
         int templateStart = html.indexOf(TEMPLATE_START);
         int templateEnd = html.indexOf(TEMPLATE_END);

         if (templateStart > 0 && templateEnd > 0) {
            html.insert(templateEnd, "</div>");
            html.insert(templateStart + TEMPLATE_START.length(), "<div id=\"EDITOR_BODY\">");
         }
      }

      if (editMode) {
         this.html = parameteriseElements(html.toString());
         this.html = this.html.replace("<rwdiv", "<div");
         lastRender = LastRender.EDIT;
      } else {
         this.html = html.toString();

         int templateStart = html.indexOf(TEMPLATE_START);
         int templateEnd = html.indexOf(TEMPLATE_END);
 
         if (templateStart > 0 && templateEnd > 0) {
            this.bodyhtml = html.substring(templateStart + TEMPLATE_START.length(), templateEnd);
            this.headerhtml = StringUtil.trim(html.substring(0, templateStart));
            this.footerhtml = StringUtil.trim(html.substring(templateEnd + TEMPLATE_END.length()));
            
            if(this.headerhtml.endsWith(BODY_START)) 
            {
            	this.headerhtml = this.headerhtml.substring(0,this.headerhtml.length()-BODY_START.length());
            	if(this.footerhtml.toLowerCase().startsWith("</div>")) { 
            		/* really, why wouldn't it in this case */
            		this.footerhtml = this.footerhtml.substring(BODY_END.length());
            	}
            }
         } /*
         }
          * else { Whilst the body is required for saving, if we want the
          * html version we need this. Might add a parameter or similar here.
          * throw new RuntimeException("Body has not been defined!"); }
          */
         lastRender = LastRender.FINAL;
      }
   }
	
	/**
	 * 
	 * @param stringBuilder
	 * @return true if stringBuilder contains a &lt;br&gt; or &lt;br/&gt; 
	 */
	private boolean isBreakElement(StringBuilder stringBuilder) {
	   boolean isBreakElement = 
	      (Pattern.matches("<[b|B][r|R][/|]>", stringBuilder) 
            && (stringBuilder.length() == 4 || stringBuilder.length() == 5));

	   return isBreakElement;
	}
	
	/**
	 * Deletes all characters in stringBuilder
	 * 
	 * @param stringBuilder
	 */
	private void deleteAll(StringBuilder stringBuilder) {
	   stringBuilder.delete(0, stringBuilder.length());
	}
	
	/**
	 * Note that stringBuilder is modified when appendChars is invoked.
	 * 
	 * @param stringBuilder all non-excluded characters in string are appended to this stingBuilder
	 * @param string characters to add to stringBuilder
	 * @param excludeCharacters characters to exclude
	 */
	private void appendChars(StringBuilder stringBuilder, String string, char[] excludeCharacters) {
	   boolean excludeChar = false;
      for (int charPosn = 0; charPosn < string.length(); charPosn++) {
         char character = string.charAt(charPosn);

         for(int i = 0; i<excludeCharacters.length; i++) {
            excludeChar = (excludeCharacters[i] == character);
         }
         
         if (!excludeChar) {
            stringBuilder.append(character);
         }
      }
	}
	
	private void appendImageLink(StringBuilder html, 
	      Map<String, String> currentImage, 
	      int imageIndex, 
	      Map<String, String> currentLink, 
	      int linkIndex, 
	      boolean editMode) {
	   if (editMode) {
         html.append("<rwdiv id=\"EDITOR_LINK-").append(linkIndex)
               .append("_IMAGE-").append(imageIndex).append(
                     "\" style=\"display: inline\">");
      }
      
      html.append("<a");
      appendElementAttributes(html, currentLink, !editMode);
      if (editMode) {
         html.append(" onclick=\"return false\"");
      }
      html.append(">");
      
      html.append("<img");
      appendElementAttributes(html, currentImage, !editMode);
      if (editMode && currentImage.get("id") == null) { 
         // generate an ID for use with mapping
         html.append(" id=\"").append("I" + KeyMaker.generate(5))
               .append("\"");
      }
      html.append("/>");
      html.append("</a>");
      if (editMode) {
         html.append("</div>");
      }	   
	}
	
	private void appendLink(StringBuilder html, Map<String, String> currentLink, int linkIndex, boolean editMode) {
      if (editMode) {
         html.append("<rwdiv id=\"EDITOR_LINK-").append(linkIndex)
               .append("\" style=\"display: inline\">");
      }
      html.append("<a");
      appendElementAttributes(html, currentLink, !editMode);
      if (editMode) {
         html.append(" onclick=\"return false\"");
      }
      html.append(">");
      html.append(currentLink.get("link"));
      html.append("</a>");
      if (editMode) {
         html.append("</div>");
      }
	}
	
	private void appendImage(StringBuilder html, Map<String, String> currentImage, int imageIndex, boolean editMode) {
	   if (editMode) {
         html.append("<rwdiv id=\"EDITOR_IMAGE-").append(imageIndex)
               .append("\" style=\"display: inline\">");
      }
      html.append("<img");
      appendElementAttributes(html,currentImage, !editMode);
      if (editMode && currentImage.get("id") == null) { // generate an
                                             // ID for
                                             // use with
                                             // mapping
         html.append(" id=\"").append("I" + KeyMaker.generate(5)).append(
               "\"");
      }
      html.append("/>");
      if (editMode) {
         html.append("</div>");
      }
	}

	/*
	 * context link - for links only start_index end_index
	 */
	public void appendElementAttributes(StringBuilder html, Map<String, String> current, boolean finalise) {
		for (Map.Entry<String, String> me : current.entrySet()) {
		   
			String key = me.getKey();
			if (key.charAt(0) == '-') {
				continue;
			}
			
			if ("html_src".equals(key) && finalise) {
				html.append(" src=\"").append(me.getValue()).append("\"");
			} else if ("html_href".equals(key) && finalise) {
				html.append(" href=\"").append(me.getValue()).append("\"");
				/*
				 * } else if (forPreview && "src".equals(key)) { //assumes
				 * static images html.append(" src=\"");
				 * html.append(me.getValue()).append("\"");
				 */
			} else if ("usemap".equals(key) /* && !me.getValue().startsWith("#") */) {
				html.append(" ").append(key).append("=\"#").append(
						me.getValue()).append("\"");
			} else if (!("context".equals(key) || "link".equals(key)
					|| "start_index".equals(key) || "end_index".equals(key) || (finalise && ("src"
					.equals(key) || "href".equals(key))))) {
				html.append(" ").append(key).append("=\"")
						.append(me.getValue()).append("\"");
			}
		}

		/* if the html_ variants have not been defined, use the originals */
		if (finalise && current.get("src") != null
				&& current.get("html_src") == null) {
			html.append(" src=\"").append(current.get("src")).append("\"");
		}
		if (finalise && current.get("href") != null
				&& current.get("html_href") == null) {
			html.append(" href=\"").append(current.get("href")).append("\"");
		}
		
		//System.out.println();
	}
	
	/**
	 * @author Ulpian Cesana
	 *
	 * <p>Represents a link or image-link. A link is identified by a link index. 
	 * Each TemplateLink maps to a row in the TemplateURLs table.</p>
	 */
	public class TemplateLink {
	   // Link properties
	   private String linkIndex;
	   private String linkUrl;  // Maps to table column TemplateURLs.URL
	   private String linkType;  // Masp to table column TemplateURLs.LinkTypeDescription (Possibile values: Standard, View Online, Unsubscribe, Runway Document, Send to a Friend TODO: Create enum)
	   private String linkResponse; // Maps to table column TemplateURLs.Response
	   private String linkTarget; // Maps to table column TemplateURLs.Target
	   private String linkText;   // Maps to table column TemplateURLs.LinkText
	   private String linkStafMsg;  // Send to a Friend Message, maps to table column MasterTemplates.SendToAFriendMsg
	   private String linkDocURL; // Maps to table column TemplateURLs.URL for documents
	   private String linkFileName;
	   
	   private String linkName;  // Velocity token name, maps to TemplateURLs#Name
	      
	   // Image properties
	   private String imageIndex; // Maps to table column TemplateURLs.ImageIndex
	   private String imageAltText; // Maps to table column TemplateURLs.AltImageText
	   private String imageTitle; // Maps to table column TemplateURLs.ImageTitle
	   private String imageSrc; // TODO: check if attribute is needed or not
	   
	   public TemplateLink() {
	   }

      public String getLinkIndex() {
         return linkIndex;
      }
      
      
      public void parseRequest(HttpServletRequest request) {
    	  this.setLinkUrl(request.getParameter("-linkURL"));
    	  this.setLinkResponse(request.getParameter("-linkResponse")); 
    	  this.setLinkStafMsg(request.getParameter("-linkStafMsg"));
    	  this.setLinkType(request.getParameter("-linkType")); 
    	  this.setLinkTarget(request.getParameter("-linkTarget"));  
    	  if(getVal(request, "-linkDocURL").length() > 0 && "Document".equals(this.getLinkType())){
    	    this.setLinkDocURL(request.getParameter("-linkDocURL"));
    	    this.setLinkUrl(request.getParameter("-linkDocURL"));
            if (this.getLinkUrl().indexOf("DocumentID=") >= 0) {
           	 String docID = this.getLinkUrl().substring(linkUrl.indexOf("DocumentID=") + 11);
           	 if (docID.indexOf("&") >= 0) {
           		 docID = docID.substring(0,docID.indexOf("&"));
           	 }
           	 this.setFileName(loadDocumentFileName(docID));
            }
    	  }
      }
      
      @SuppressWarnings("unchecked")
      public void setJsonParams(JSONObject o) 
      {
    	  o.put("-linkURL", getLinkUrl());
    	  o.put("-linkResponse",getLinkResponse());
    	  o.put("-linkStafMsg", getLinkStafMsg());
    	
    	  /* this is commented out in loadelement, I think it's needed though. */
    	  o.put("-linkType", getLinkType());
    	  
    	  /* probably not needed */
    	  o.put("-linkTarget",getLinkTarget());
    	  o.put("-linkName",getLinkName());
    	  o.put("-linkFileName",getFileName());
      }

      public void setLinkIndex(String linkIndex) {
         this.linkIndex = linkIndex;
      }

      public String getLinkUrl() {
         return linkUrl;
      }

      public void setLinkUrl(String linkUrl) {
         this.linkUrl = linkUrl;
      }

      public String getLinkType() {
         return linkType;
      }

      public void setLinkType(String linkType) {
         this.linkType = linkType;
      }

      public String getLinkResponse() {
         return linkResponse;
      }

      public void setLinkResponse(String linkResponse) {
         this.linkResponse = linkResponse;
      }

      public String getLinkTarget() {
         return linkTarget;
      }

      public void setLinkTarget(String linkTarget) {
         this.linkTarget = linkTarget;
      }
      
      public String getLinkText() {
         return linkText;
      }

      public void setLinkText(String linkText) {
         this.linkText = linkText;
      }

      public String getImageIndex() {
         return imageIndex;
      }

      public void setImageIndex(String imageIndex) {
         this.imageIndex = imageIndex;
      }

      public String getImageAltText() {
         return imageAltText;
      }

      public void setImageAltText(String imageAltText) {
         this.imageAltText = imageAltText;
      }

      public String getImageTitle() {
         return imageTitle;
      }

      public void setImageTitle(String imageTitle) {
         this.imageTitle = imageTitle;
      }

      public String getImageSrc() {
         return imageSrc;
      }

      public void setImageSrc(String imageSrc) {
         this.imageSrc = imageSrc;
      }

      public String getLinkStafMsg() {
         return linkStafMsg;
      }

      public void setLinkStafMsg(String stafMsg) {
         this.linkStafMsg = stafMsg;
      }

      public String getLinkDocURL() {
         return linkDocURL;
      }

      public void setLinkDocURL(String linkDocURL) {
         this.linkDocURL = linkDocURL;
      }

      public String getLinkName() {
         return linkName;
      }

      public void setLinkName(String linkName) {
         this.linkName = linkName;
      }
      public String getFileName() {
          return linkFileName;
       }

       public void setFileName(String fileName) {
          this.linkFileName = fileName;
       }
	}
	
	public class HTMLParser {
	   private static final String NEWLINE = "\r\n";
	   
	   private StringBuilder convertToText(File file) {
	      StringBuilder text  = new StringBuilder();
         
	      if (file.exists()) {
            try {
               BufferedReader reader = new BufferedReader(new FileReader(file));

               String line = null;

               while ((line = reader.readLine()) != null) {
                  text.append(line).append(NEWLINE);
               }

               reader.close();

            } catch (java.io.FileNotFoundException fnfe) {
               debug(fnfe);
               debug("Could not find \"" + file.getName() + "\"");
            } catch (IOException ioe) {
               debug(ioe);
               debug("Could not read \"" + file.getName() + "\"");
            }
         } else {
            debug("Could not find \"" + file.getName() + "\"");
         }
	      
	      return text;
	   }

		public void parse(File file, 
		      ArrayList<Map<String, String>> links,
				ArrayList<Map<String, String>> images,
				Collection<ImageMap> imageMaps) {
		   
		   StringBuilder htmlText = convertToText(file);
		   parse(htmlText, links, images, imageMaps);
		}

		public void parse(StringBuilder html,
				ArrayList<Map<String, String>> links,
				ArrayList<Map<String, String>> images,
				Collection<ImageMap> imageMaps) {
			debug("starting parse");
			htmltext = html.toString();

			Pattern start = null;
			Pattern end = null;

			start = START_ANCHOR;
			end = END_ANCHOR;

			debug("Finding matches for links");

			findMatches(links, html, start, end, true);

			if (links != null) {
				debug("links size " + links.size());
				
				Map<String,String> prev = null; 
				for(Map<String, String> link: links) { 
					if(prev == null) { 
						prev = link; 
					} else { 
						
						int pStart = Integer.parseInt(prev.get("start_index"));
						int pEnd = Integer.parseInt(prev.get("end_index"));
						
						int cStart = Integer.parseInt(link.get("start_index"));
						int cEnd = Integer.parseInt(link.get("end_index"));
						
						//System.out.println("checking "+pStart + "_"+pEnd +"_" + cStart + "_"+cEnd);
						
						if(cStart < pEnd || cEnd < pEnd) { 
							throw new RuntimeException("A Link was found within another link. \r\n The code was: \r\n "+htmltext.substring(pStart,pEnd)+"\r\nThis should be corrected before re-uploading."); 
						}
						prev = link; 
					}
				}
				
				
			}

			start = START_IMG;
			end = END_TAG;

			debug("Finding matches for images");

			findMatches(images, html, start, end, false);

			if (images != null) {
				debug("images size " + images.size());
			}

			findMaps(imageMaps, html);
			
			if(imageMaps != null && links != null) {
				java.util.Iterator<ImageMap> im = imageMaps.iterator();
				if(im.hasNext()) { 
					ImageMap imap = im.next(); 
					
					for(Map<String, String> link: links) { 
						int cStart = Integer.parseInt(link.get("start_index"));
						int cEnd = Integer.parseInt(link.get("end_index"));
						
						while(imap.end_index < cStart && im.hasNext()) { 
							imap = im.next(); 
						}
						if(imap.start_index < cEnd && imap.start_index > cStart) { 
							throw new RuntimeException("An Image Map was found within another link. \r\n The code was: \r\n "+htmltext.substring(cStart,cEnd)+"\r\nThis should be corrected before re-uploading.");
						}
					}		
					
					
				}	
			}
		}

		private Collection<ImageMap> findMaps(Collection<ImageMap> imageMap,
				StringBuilder input) {

			Matcher ms = START_MAP.matcher(input);
			Matcher me = END_MAP.matcher(input);

			//System.out.println(input);

			while (ms.find() && me.find(ms.start())) {

				String mapString = input.substring(ms.start(), me.end());

				int endInit = mapString.indexOf(">") + 1;
				String init = mapString.substring(0, endInit);

				String id = parameters(init, false).get("id");
				if (id == null) {
					continue;
				}

				//System.out.println(id);

				ArrayList<ImageMapItem> items = new ArrayList<ImageMapItem>();

				String areas = input
						.substring(ms.start() + endInit, me.start());

				//System.out.println(areas);

				Matcher areaStart = START_AREA.matcher(areas);
				Matcher areaEnd = END_TAG.matcher(areas);

				while (areaStart.find() && areaEnd.find(areaStart.start())) {

					String area = areas.substring(areaStart.start(), areaEnd
							.end());
					items.add(new ImageMapItem(parameters(area, false)));
				}

				imageMap.add(new ImageMap(id, ms.start(), me.end(), items));
			}

			return imageMap;
		}

		private ArrayList<Map<String, String>> findMatches(
				ArrayList<Map<String, String>> results, StringBuilder input,
				Pattern start, Pattern end, boolean isLink) {

			Matcher ms = start.matcher(input);
			Matcher me = end.matcher(input);

			Map<String, String> params = null;

			while (ms.find() && me.find(ms.start())) {

				String result = input.substring(ms.start(), me.end());
				params = parameters(result, isLink);
				if (isLink) {
					if (!START_IMG.matcher(result).find()) {
						int ci = -1;
						int ce = -1;
						for (int i = (ms.start()); i > 0; i--) {
							String block = input.substring(i - 1, i);
							if (">".equals(block)) {
								ci = i;
								break;
							}
						}
						for (int i = (me.end()); i < input.length(); i++) {
							String block = input.substring(i, i + 1);
							if ("<".equals(block)) {
								ce = i;
								break;
							}
						}
						if (ci > 0 && ce > 0) {
							params.put("context", input.substring(ci, ce));
						}
					}
				}
				params.put("start_index", String.valueOf(ms.start()));
				params.put("end_index", String.valueOf(me.end()));

				results.add(params);
			}

			return results;
		}

		private HashMap<String, String> parameters(String match, boolean isLink) {
			HashMap<String, String> params = new HashMap<String, String>();

			int start = match.indexOf(" ");
			if (start == -1) { // no parameters
				return params;
			}
			int end = (match.indexOf(">") > match.indexOf("/>") && match
					.indexOf("/>") > 0) ? match.indexOf("/>") : match
					.indexOf(">");
			if (end == -1) {
				throw new RuntimeException("Could not find end of html object");
			}
			if (end < start) { // no parameters
				return params;
			}

			debug("\r\n parameters: " + match + "" + start + " " + end);
			String clean = StringUtils.trim(match.substring(start, end));

			int eq = -1;
			int delim = -1;
			StringBuilder name = new StringBuilder();
			StringBuilder param = new StringBuilder();

			for (int i = 0; i < clean.length(); i++) {
				char c = clean.charAt(i);

				if (eq == -1 && c == '=') {
					eq = i;
				} else if (eq == -1 && c != ' ') {
					name.append(c);
				} else if (eq < i && delim == -1 && c != ' ') {
					if ((c == '\'' || c == '\"')) {
						delim = i;
					} else {
						delim = -2;
						param.append(c);
					}
				} else if (((delim > 0 && c != clean.charAt(delim)) || (delim == -2 && c != ' '))) {
					param.append(c);

				} else if ((delim > 0 && c == clean.charAt(delim))
						|| (delim == -2 && c == ' ')) {
					eq = -1;
					delim = -1;

					params.put(name.toString().toLowerCase(), param.toString());
					name.delete(0, name.length());
					param.delete(0, param.length());
				}
			}

			if (isLink) {
				String after = match.substring(end + 1);
				params.put("link", after.substring(0, after.toLowerCase().indexOf("</a>")));
				String href = params.get("href"); 
            
				if (href != null && href.indexOf("optout") > -1) {
					params.put("-linkType", "Unsubscribe");
				} else if (href != null && href.indexOf("notehtmlview") > -1) {
					params.put("-linkType", "ViewOnline");
				} else if (href != null && href.indexOf("staf.sok") > -1) {
				   params.put("-linkType", "SendToAFriend");
				} else if (href != null && href.indexOf("filedownloader.jsp") > -1) {
				   params.put("-linkType", "Document");
				   String documentID = href.substring(href.lastIndexOf("=")+1);
				   String fileName = loadDocumentFileName(documentID);
				   params.put("FileName", fileName);
				} else {
					params.put("-linkType", "Standard");
				}
			} else {
				if (params.get("src") != null) {
					String src = params.get("src");
					if (src.indexOf("$") == -1 && src.indexOf("http://") == -1 && src.indexOf("https://") == -1 && src.indexOf("ftp://") == -1 && src.indexOf("mailto:") == -1) {
						src = "$URLHome" + getDirectory()
								+ ((src.charAt(0) == ('/')) ? "" : "/") + src;
						params.put("src", src);
					}
				}

				if (params.get("usemap") != null) {
					String map = params.get("usemap");
					if (map.length() > 0 && map.startsWith("#")) {
						params.put("usemap", map.substring(1));
					}
				}
			}

			return params;
		}

	}
	
	public boolean hasBody() {
	   return (this.bodyhtml != null && this.bodyhtml.length() > 0);
	}
	
	private String loadDocumentFileName(String documentID) {
	   String fileName = "";
	   if(documentID != null && documentID.length() > 0) {
   	   GenRow document = new GenRow();
   	   document.setTableSpec("Documents");
   	   document.setParameter("DocumentID", documentID);
         document.setParameter("-select0", "FileName");   	   
   	   document.setAction(GenerationKeys.SELECTFIRST);
   	   document.doAction();
   	   if(document.isSuccessful()) {
   	      fileName = document.getColumn("FileName");
   	   }
	   }
	   return fileName;
	}
}
