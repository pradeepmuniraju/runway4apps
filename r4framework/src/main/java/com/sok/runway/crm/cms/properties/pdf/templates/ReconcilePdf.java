package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.ReconcileInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.model.TransactionInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;

public class ReconcilePdf extends AbstractPDFTemplate implements PDFConstants {

	public static final Logger logger = LoggerFactory.getLogger(ReconcilePdf.class);

	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {
		throw new UnsupportedOperationException("Must use writer method if one is defined");
	}

	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document, final PdfWriter writer) throws IOException, DocumentException {

		try {
			renderPDFConcurrent(request, response, document, writer);
			return;
		} catch (ExecutionException ee) {
			logger.error("Execution Exception", ee);
		} catch (InterruptedException ie) {
			logger.error("Execution Interrupted", ie);
		}
	}

	public void renderPDFConcurrent(final HttpServletRequest request, HttpServletResponse response, Document document, final PdfWriter writer) throws IOException, DocumentException, ExecutionException, InterruptedException {

		String reconcileID = request.getParameter("ReconcileID"); // Parent Transaction ID

		GenRow reconcileRow = new GenRow();
		reconcileRow.setViewSpec("ReconcileView");
		reconcileRow.setParameter("ReconcileTransactionID", reconcileID);
		reconcileRow.doAction("selectfirst");
		logger.debug("ReconcileView Query is: " + reconcileRow.getStatement());
		
		GenRow transactionRow = new GenRow();
		transactionRow.setTableSpec("Transactions");
		transactionRow.setParameter("-select0", "PaidOnDate");
		transactionRow.setParameter("TransactionID", reconcileID);
		transactionRow.doAction("selectfirst");
		logger.debug("Transactions Query is: " + transactionRow.getStatement());

		ReconcileInfo reconcileInfo = new ReconcileInfo();
		
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
			String dateAsString = simpleDateFormat.format(reconcileRow.getDate("CreatedDate"));
			reconcileInfo.setDate(dateAsString);
		} catch (Exception ee) {
			ee.printStackTrace();
			reconcileInfo.setDate("");
		}
		
		try {
			DateFormat format = DateFormat.getTimeInstance(DateFormat.SHORT);
			reconcileInfo.setTime(format.format(reconcileRow.getDate("CreatedDate"))); // TODO: Please check the parsing logic for 10:45am format
		} catch (Exception ee) {
			ee.printStackTrace();
			reconcileInfo.setTime("");
		}
		
		reconcileInfo.setAccount(InitServlet.getSystemParam("Trust-AccountName"));
		reconcileInfo.setProcessedBy(reconcileRow.getString("CreatedFirstName") + " " + reconcileRow.getString("CreatedLastName"));
		
		reconcileInfo.setOpeningBalance(reconcileRow.getDouble("OpeningBalance"));
		reconcileInfo.setClearedCreditAmount(reconcileRow.getDouble("ClearedCreditAmount"));
		reconcileInfo.setClearedDebitAmount(reconcileRow.getDouble("ClearedDebitAmount"));
		
		double clearedTransactions = reconcileRow.getDouble("ClearedCreditAmount") + reconcileRow.getDouble("ClearedDebitAmount");		
		reconcileInfo.setClearedTransactions(clearedTransactions);
		
		double clearedBalance = clearedTransactions + reconcileRow.getDouble("OpeningBalance");		
		reconcileInfo.setClearedBalance(clearedBalance);
		
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
			String dateAsString = simpleDateFormat.format(transactionRow.getDate("PaidOnDate"));
			reconcileInfo.setPaidOnDate(dateAsString);
		} catch (Exception ee) {
			ee.printStackTrace();
			reconcileInfo.setPaidOnDate("");
		}
		
		
		reconcileInfo.setUnclearedCreditAmount(reconcileRow.getDouble("UnclearedCreditAmount"));
		reconcileInfo.setUnclearedDebitAmount(reconcileRow.getDouble("UnclearedDebitAmount"));
		
		double unclearedTransactions = reconcileRow.getDouble("UnclearedCreditAmount") + reconcileRow.getDouble("UnclearedDebitAmount");		
		reconcileInfo.setUnclearedTransactions(unclearedTransactions);
		
		double closingBalance = clearedBalance + unclearedTransactions;
		
		reconcileInfo.setClosingBalance(closingBalance);
		
		reconcileInfo.setBankStatementBalance(closingBalance);
		
		reconcileRow.close();
		transactionRow.close();
		document.open();
		setMargins(document);
		renderTransactionSummaryPDF(document, reconcileInfo, reconcileID);
	}

	private String getTransactionType(String txnType) {
		logger.trace("Translating transaction type for {}", txnType);

		if ("Full".equals(txnType)) {
			return "Full Deposit Transfer";
		} else if ("Balance".equals(txnType)) {
			return "Account Sales";
		} else if ("Nil".equals(txnType)) {
			return "Debit Sales";
		}
		return "";
	}

	private void renderTransactionSummaryPDF(com.itextpdf.text.Document document, ReconcileInfo reconcileInfo, String reconcileID) throws DocumentException {

		PdfPTable pageTable = new PdfPTable(1);
		pageTable.setWidthPercentage(80f);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPaddingTop(40f);
		pageTable.getDefaultCell().setPaddingBottom(0f);
		pageTable.getDefaultCell().setFixedHeight(0f);
		pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable tmpTbl = new PdfPTable(2);
		tmpTbl.getDefaultCell().setBorder(0);
		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setFixedHeight(0f);

		tmpTbl.getDefaultCell().setColspan(2);

		tmpTbl.getDefaultCell().setPaddingBottom(10f);
		tmpTbl.addCell(new Paragraph("Bank Trust Account Reconciliation", PDFUtil.fontArialBlackPt28Bold));
		tmpTbl.getDefaultCell().setPaddingBottom(10f);

		tmpTbl.addCell("Date: " + reconcileInfo.getDate());		
		tmpTbl.addCell("Time: " + reconcileInfo.getTime());		
		tmpTbl.addCell("Account: " + reconcileInfo.getAccount());
		tmpTbl.addCell("Processed by: " + reconcileInfo.getProcessedBy());

		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setPaddingBottom(0f);

		pageTable.addCell(tmpTbl);
		
		tmpTbl = new PdfPTable(3);
		tmpTbl.getDefaultCell().setBorder(0);
		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setFixedHeight(0f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.2f);
		
				
		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setPaddingBottom(20f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.0f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.getDefaultCell().setColspan(3);
		tmpTbl.addCell(new Paragraph("-------------------------------------------------- Reconciliation successful -------------------------------------------------", PDFUtil.fontArialBlackPt16));
		
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.getDefaultCell().setPaddingBottom(10f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.2f);
		tmpTbl.addCell("Opening balance("+ reconcileInfo.getPaidOnDate() +")");
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		tmpTbl.addCell(PDFUtil.getString(reconcileInfo.getOpeningBalance()));		
		
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.0f);
		tmpTbl.getDefaultCell().setPaddingBottom(10f);
		tmpTbl.getDefaultCell().setPaddingTop(5f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.addCell("Cleared credits");
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.addCell(PDFUtil.getString(reconcileInfo.getClearedCreditAmount()));
		
		
		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.2f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.addCell("Cleared debits");
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.addCell(PDFUtil.getString(reconcileInfo.getClearedDebitAmount()));
		
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.2f);
		tmpTbl.getDefaultCell().setPaddingTop(5f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.addCell("Cleared transactions");
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.addCell(PDFUtil.getString(reconcileInfo.getClearedTransactions()));
		
		tmpTbl.getDefaultCell().setBorderWidthBottom(2f);
		tmpTbl.getDefaultCell().setPaddingTop(5f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.addCell(new Paragraph("Cleared balance", PDFUtil.fontArialBlackPt18Bold));
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		tmpTbl.addCell(new Paragraph(PDFUtil.getString(reconcileInfo.getClearedBalance()), PDFUtil.fontArialBlackPt18Bold));
		
		tmpTbl.getDefaultCell().setPaddingTop(5f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.0f);
		tmpTbl.getDefaultCell().setPaddingBottom(10f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.addCell("Uncleared credits");
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.addCell(PDFUtil.getString(reconcileInfo.getUnclearedCreditAmount()));
		
		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.2f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.addCell("Uncleared debits");
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.addCell(PDFUtil.getString(reconcileInfo.getUnclearedDebitAmount()));
		
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.2f);
		tmpTbl.getDefaultCell().setPaddingTop(5f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.addCell("Uncleared transactions");
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.addCell(PDFUtil.getString(reconcileInfo.getUnclearedTransactions()));
		
		tmpTbl.getDefaultCell().setBorderWidthBottom(2f);
		tmpTbl.getDefaultCell().setPaddingTop(5f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.addCell(new Paragraph("Closing balance", PDFUtil.fontArialBlackPt18Bold));
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		tmpTbl.addCell(new Paragraph(PDFUtil.getString(reconcileInfo.getClosingBalance()), PDFUtil.fontArialBlackPt18Bold));
		
		tmpTbl.getDefaultCell().setPaddingTop(25f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(2f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.addCell(new Paragraph("Bank statement balance", PDFUtil.fontArialBlackPt18Bold));
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		tmpTbl.addCell(new Paragraph(PDFUtil.getString(reconcileInfo.getBankStatementBalance()), PDFUtil.fontArialBlackPt18Bold));
		
		tmpTbl.getDefaultCell().setPaddingTop(40f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.0f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.getDefaultCell().setColspan(3);
		tmpTbl.addCell(new Paragraph("I verify that the above Trust Account Reconciliation Statement is accurate", PDFUtil.fontArialBlackPt16));
		
		
		tmpTbl.getDefaultCell().setPaddingTop(40f);
		tmpTbl.getDefaultCell().setPaddingBottom(0f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.0f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.getDefaultCell().setColspan(0);
		tmpTbl.addCell(new Paragraph("", PDFUtil.regularFont));
		tmpTbl.addCell(new Paragraph("", PDFUtil.regularFont));
		tmpTbl.addCell(new Paragraph("               /         /          ", PDFUtil.regularFont));
		
		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.0f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.getDefaultCell().setColspan(0);
		tmpTbl.addCell(new Paragraph("____________________________", PDFUtil.regularFont));
		tmpTbl.addCell(new Paragraph("____________________________", PDFUtil.regularFont));
		tmpTbl.addCell(new Paragraph("____________________________", PDFUtil.regularFont));
		

		tmpTbl.getDefaultCell().setBorderWidthBottom(0.0f);
		tmpTbl.getDefaultCell().setPaddingTop(5f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.addCell(new Paragraph("Name", PDFUtil.fontArialBlackPt16));
		tmpTbl.addCell(new Paragraph("Signature", PDFUtil.fontArialBlackPt16));
		tmpTbl.addCell(new Paragraph("Date", PDFUtil.fontArialBlackPt16));
		
		
		tmpTbl.getDefaultCell().setPaddingTop(40f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.0f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.getDefaultCell().setColspan(3);
		tmpTbl.addCell(new Paragraph("__________________________________________", PDFUtil.regularFont));
		
		tmpTbl.getDefaultCell().setPaddingTop(5f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.0f);
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.getDefaultCell().setColspan(3);
		tmpTbl.addCell(new Paragraph("Working Director / Estate Agent / OIEC", PDFUtil.fontArialBlackPt16));
		
		pageTable.addCell(tmpTbl);


		pageTable.getDefaultCell().setPaddingTop(0f);


		document.add(pageTable);
		
		document.newPage();
				
		pageTable = new PdfPTable(1);
		pageTable.setWidthPercentage(80f);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPaddingTop(40f);
		pageTable.getDefaultCell().setPaddingBottom(0f);
		pageTable.getDefaultCell().setFixedHeight(0f);
		pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		tmpTbl = new PdfPTable(2);
		tmpTbl.getDefaultCell().setBorder(0);
		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setFixedHeight(0f);

		tmpTbl.getDefaultCell().setColspan(2);

		tmpTbl.getDefaultCell().setPaddingBottom(10f);
		tmpTbl.addCell(new Paragraph("Bank Trust Account Reconciliation Detail", PDFUtil.fontArialBlackPt28Bold));
		tmpTbl.getDefaultCell().setPaddingBottom(10f);

		tmpTbl.addCell("Date: " + reconcileInfo.getDate());		
		tmpTbl.addCell("Time: " + reconcileInfo.getTime());		
		tmpTbl.addCell("Account: " + reconcileInfo.getAccount());
		tmpTbl.addCell("Processed by: " + reconcileInfo.getProcessedBy());

		tmpTbl.getDefaultCell().setPaddingTop(20f);
		tmpTbl.getDefaultCell().setPaddingBottom(0f);
		
		tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		tmpTbl.getDefaultCell().setColspan(2);
		tmpTbl.addCell(new Paragraph("-------------------------------------------------- Reconciliation successful -------------------------------------------------", PDFUtil.fontArialBlackPt16));

		pageTable.addCell(tmpTbl);
		
		pageTable.setSplitLate(false);
		
		pageTable.addCell(renderTransactions(document, reconcileID, "payments","Cleared credits"));
		
		pageTable.addCell(renderTransactions(document, reconcileID, "transact","Cleared debits"));
		
		pageTable.addCell(renderTransactions(document, reconcileID, "payments","Uncleared credits"));
		
		pageTable.addCell(renderTransactions(document, reconcileID, "transact","Uncleared debits"));
		
		pageTable.getDefaultCell().setPaddingTop(0f);
		
		document.add(pageTable);

	}

	private PdfPTable renderTransactions(com.itextpdf.text.Document document, String reconcileID, String traxType, String tableLabel) throws DocumentException {
		
		String transactionIDs = null;				
		
		PdfPTable tmpTbl = new PdfPTable(5);
		tmpTbl.getDefaultCell().setBorder(0);
		/*tmpTbl.getDefaultCell().setBorderWidthTop(0.5f);
		tmpTbl.getDefaultCell().setBorderWidthBottom(0.5f);
		tmpTbl.getDefaultCell().setBorderWidthLeft(0.5f);
		tmpTbl.getDefaultCell().setBorderWidthRight(0.5f);*/
		tmpTbl.getDefaultCell().setPaddingTop(0f);
		tmpTbl.getDefaultCell().setFixedHeight(0f);
		
		float[] columnWidths = new float[] {15f, 30f, 15f, 20f, 20f};
		tmpTbl.setWidths(columnWidths);
		
		
		GenRow reconcileRow = new GenRow();
		reconcileRow.setViewSpec("ReconcileView");
		reconcileRow.setParameter("ReconcileTransactionID", reconcileID);
		reconcileRow.doAction("selectfirst");
		
		if("Cleared credits".equals(tableLabel) || "Cleared debits".equals(tableLabel)){
			transactionIDs = reconcileRow.getString("ReconciledIDs");
		}else if("Uncleared credits".equals(tableLabel) || "Uncleared debits".equals(tableLabel)){
			transactionIDs = reconcileRow.getString("UnReconciledIDs");
		}
		
		if(transactionIDs != null){
			GenRow clearedCreditsList = new GenRow();
			clearedCreditsList.setViewSpec("TransactionsView");
			clearedCreditsList.setParameter("TransactionID", transactionIDs);
			clearedCreditsList.setParameter("TransactionSubType",traxType);
			clearedCreditsList.doAction("search");
			clearedCreditsList.getResults();
			logger.debug("TransactionsView Query is: " + clearedCreditsList.getStatement());
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
			String paidOnDate = "";
			
			tmpTbl.getDefaultCell().setColspan(5);
			tmpTbl.getDefaultCell().setBorderWidthBottom(0.5f);
			tmpTbl.addCell(tableLabel);
			
			tmpTbl.getDefaultCell().setColspan(0);
			tmpTbl.getDefaultCell().setBorderWidthBottom(0.0f);
			
			while(clearedCreditsList.getNext()){
				
				logger.debug("TransactionsView EstateName is: " + clearedCreditsList.getString("EstateName"));
				
				try {				
					paidOnDate = simpleDateFormat.format(clearedCreditsList.getDate("PaidOnDate"));
				} catch (Exception ee) {
					ee.printStackTrace();
					paidOnDate = "";
				}
				
				tmpTbl.getDefaultCell().setPaddingTop(5f);
				tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				tmpTbl.addCell(paidOnDate);
				tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				tmpTbl.addCell(clearedCreditsList.getString("FirstName") + " " + clearedCreditsList.getString("LastName"));
				tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				tmpTbl.addCell(clearedCreditsList.getString("ProductName") );
				tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				tmpTbl.addCell(clearedCreditsList.getString("EstateName") );
				tmpTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				tmpTbl.addCell(PDFUtil.getString(clearedCreditsList.getDouble("PaidAmount")));
	
	
			}
		}

		return tmpTbl;
	}

	public void setMargins(Document document) {
		document.setMargins(20, 0, 20, 0);
	}

	public String getPdfSimpleName() {
		return "Transaction Summary";
	}

}