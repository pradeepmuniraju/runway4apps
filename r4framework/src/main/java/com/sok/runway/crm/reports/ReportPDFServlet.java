package com.sok.runway.crm.reports;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import java.security.KeyStore;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.servlet.ResponseOutputStream;
import com.sok.runway.autopilot.AutoPilot;
import com.sok.runway.CustomReportBean;
import com.sok.runway.HistoricReport;
import com.sok.runway.ReportBean;
import com.sok.runway.ReportBean.ReportCell;
import com.sok.runway.ReportBean.TitleCell;
import com.sok.runway.UserBean;
import com.sok.service.crm.UserService;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportPDFServlet extends HttpServlet {

	private static final long serialVersionUID = 2L;
	private static final Logger logger = LoggerFactory.getLogger(ReportPDFServlet.class);
	
	public enum GenerationMode {
		PhantomJS,
		Exporter,
		Legacy
	};
	
	public static final GenerationMode MODE = getMode();
	private static final String URL_HOME = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
	private static final String PHANTOMJS_PATH = "/usr/local/bin/phantomjs";
	private static final String PHANTOMJS_FORMAT = "/usr/local/bin/phantomjs /opt/resource/phantomjs/rasterize.js %1$s %2$s";
	private static final String EXPORTER_URL = StringUtils.trimToNull(InitServlet.getSystemParam("exportURL"));
	
	private static GenerationMode getMode() {	
		try { 
			File f = new File(PHANTOMJS_PATH);
			try { 
				if(f.exists()) {
					return GenerationMode.PhantomJS;
				}
			} catch(SecurityException se) {
				logger.error("The JVM does not have permission to access phantomjs", se);
			}
		} catch (Exception e) {
			logger.error("Some other error i'm not seeing", e);
		}
		return GenerationMode.Exporter;
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			final UserBean currentuser = UserService.getInstance().getCurrentUser(request);
			Document document = (Document)request.getAttribute("pdfdocument");
			boolean hasDocument = document != null;
			if(!hasDocument) {
				document = new Document(PageSize.A4.rotate());
				document.setMarginMirroring(true);
				document.setMargins(20, 20, 20, 20);
				PdfWriter.getInstance(document, response.getOutputStream());
				document.open();
			} else {
				document.setPageSize(PageSize.A4.rotate());
			}

			
			String historicReportID = StringUtils.trimToNull(request.getParameter("-historicReportID"));
			String beanID = StringUtils.trimToNull(request.getParameter("-beanID"));
			boolean dontshowchart = "true".equals(request.getAttribute("-dontshowchart"));
			boolean dontshowtable = "true".equals(request.getAttribute("-dontshowtable"));
			ReportBean report = null;
			
			if(historicReportID != null) {
				 //reproducing a historically generated report
			     HistoricReport historicReport = new HistoricReport();
			     historicReport.setTableSpec("HistoricReports");
			     historicReport.setParameter("HistoricReportID", historicReportID);
			     historicReport.setParameter("-select1", "Results");
			     historicReport.setParameter("-select2", "CreatedDate");
			     historicReport.setAction(GenerationKeys.SELECTFIRST);
			     historicReport.doAction();
			     
			     report = historicReport.getCustomReportBean();
			     report.put("contextPath",request.getContextPath());
			}
			if(report == null && beanID != null) {
				GenRow row = new GenRow();
				row.setRequest(request);
				row.setTableSpec("Beans");
				row.setParameter("BeanID", beanID);
				row.setParameter("-select1", "Bean");
				row.doAction(GenerationKeys.SELECTFIRST);

				if (row.isSuccessful()) {
					report = new CustomReportBean();
					report.parseRequest(row.getData("Bean"));
				}
			} 
			
			if(report == null) {
				Paragraph error = new Paragraph();
				error.add(new Chunk("The report could not be found", FontFactory.getFont(FontFactory.HELVETICA, 8)));
				document.add(error);
				return;
			}
			
			PdfPTable aTable = new PdfPTable(2);
			aTable.setWidthPercentage(100f);
			aTable.setWidths(new float[]{5,1});
			aTable.getDefaultCell().setBorderWidth(0);
			aTable.getDefaultCell().setPadding(0);
			aTable.getDefaultCell().setPaddingBottom(10);
			aTable.getDefaultCell().setBorderColor(white);
			aTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			aTable.addCell(new Paragraph(""));
			Paragraph top = new Paragraph();
			top.add(new Chunk("Produced ", FontFactory.getFont(FontFactory.HELVETICA, 8)));
			top.add(new Chunk((currentuser != null ? currentuser.getCurrentDateString() : new Date().toString()), FontFactory.getFont(FontFactory.HELVETICA, 8)));
			top.setAlignment(Paragraph.ALIGN_RIGHT);
			aTable.addCell(top);
			
			if("StatusToStatus".equals(report.getString("-reportType"))) { 
				dontshowchart = true;
			}
			report.parseRequest(request);
			report.doAction();
			request.setAttribute("report", report);
			
			float chartHeight = 0; 
			
			if("true".equals(InitServlet.getSystemParam("ReportPDFGroupLogoEnabled"))) {
				Object o = report.get("GroupID");
				Image i = null;
				if(o instanceof String) {
					String groupID = (String)o; 
					
					GenRow g = new GenRow(); 
					g.setRequest(request); 
					g.setViewSpec("GroupView"); 
					g.setParameter("GroupID", groupID);
					g.doAction(GenerationKeys.SELECT);
					
					if(g.isSet("LogoFilePath")) {
						try {
							i = Image.getInstance(InitServlet.getRealPath("/" + g.getData("LogoFilePath")));
						} catch (DocumentException de) {
							logger.error("Unable to load group image", de);
						}
					} else {
						logger.debug("No group image from query {}", g.getStatement());
					}
					
				} 
				if(i == null) {
					// use runway logo
					try {
						i = Image.getInstance(InitServlet.getRealPath("/specific/images/logolarge.jpg"));
					} catch (DocumentException de) {
						logger.error("Unable to standard runway image", de);
					}
				}
				if(i != null) {
					aTable.addCell(new Paragraph(""));
					aTable.addCell(i);
				}
			}
			document.add(aTable);
			
			if(!dontshowchart) {
				chartHeight = addReportChart(document, report);
			}
			if(!dontshowtable) {
				if(!dontshowchart) {
					document.add(new Paragraph(""));
				}
				addReportTable(document, report, chartHeight);
			}
			if(!hasDocument) {
				/* if this process was spawned from another servlet, don't close the document */
				document.close();
			}
		} catch (DocumentException de) {
			logger.error("Error generating report pdf");
			throw new RuntimeException(de);
		}	
	}
	
	public float addReportChart(Document document, ReportBean report) throws DocumentException {
		Image chartImage = null;
		if(MODE == GenerationMode.PhantomJS) {
			HttpSession session = new com.sok.framework.servlet.HttpSession(InitServlet.getConfig().getServletContext());
			try { 
				logger.debug("Generating report chart via PhantomJS");
				final String fileKey = KeyMaker.generate();
				session.setAttribute(fileKey + "_report", report);
				final String url  = doPhantomJSOutput(report.getConnection(),"/crm/reportgenerator/viewreportgraph.jsp?-sessionKey=" + fileKey, fileKey, session);
				session.setAttribute(fileKey + "_report", null);
				chartImage = Image.getInstance(url); 
			} catch (Exception e) {
				logger.error("Error generating report chart via phantomjs", e);
			}
		}
		if(chartImage == null) {
			//attempt to use the exporter method, so long as we have svg data.
			String svg = StringUtils.trimToNull(report.getString("-svgdata"));
			if(svg != null) { 
				String url = StringUtils.trimToNull(report.getString("-exporturl"));
				if(url == null) {
					if(EXPORTER_URL != null) {
						url = EXPORTER_URL;
					} else {
						try { 
							URL jUrl = new URL(URL_HOME);
							url = new StringBuilder() 
										.append(jUrl.getProtocol()).append("://")			// ie http://
										.append(jUrl.getHost()).append(":").append(jUrl.getPort())	// ie alpha.switched-on.com.au:80
										.append("/exporter/")
										.toString();
							
							logger.debug("Generated {} as Exporter url from URLHome", url);
						} catch (MalformedURLException mue) {
							logger.error("URLHome was malformed, and we cannot generate an exporter path", mue);
						}
					}
				}
				
				if(url != null) {
					try { 
					if(url.startsWith("https")) {
						SSLContext sc = SSLContext.getInstance("SSL");
						sc.init(null, TRUSTING_SSL_MANAGER, new java.security.SecureRandom());
						SSLSocketFactory sslSocketFactory = sc.getSocketFactory();
						HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);
					}
					

					HttpURLConnection con = (HttpURLConnection)new URL(url).openConnection();
					con.setRequestMethod("POST"); 
					con.setRequestProperty("accept","image/png");
					con.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 

					StringBuilder data = new StringBuilder(svg.length() + 50) 	//below should be 39 ish
						.append(URLEncoder.encode("svg","UTF-8")).append("=").append(URLEncoder.encode(svg,"UTF-8"))
						.append("&").append(URLEncoder.encode("type","UTF-8")).append("=").append(URLEncoder.encode("image/png","UTF-8"))
						.append("&").append(URLEncoder.encode("width","UTF-8")).append("=").append(URLEncoder.encode("1200","UTF-8"))
						.append("&").append(URLEncoder.encode("height","UTF-8")).append("=").append(URLEncoder.encode("900","UTF-8"));

					logger.debug("svglength {}, datalength {}", svg.length(), data.length());
					con.setRequestProperty("Content-Length",String.valueOf(data.length())); 

					con.setDoOutput(true); 
					con.setAllowUserInteraction(false);
					OutputStream ost = con.getOutputStream();
					PrintWriter pw = new PrintWriter(ost);
					pw.print(data.toString()); // here we "send" our body!
					pw.flush();
					pw.close();

					String imageFile = "/tmp/" + KeyMaker.generate() + ".png"; 

					BufferedInputStream in = new BufferedInputStream(con.getInputStream());
					FileOutputStream file = new FileOutputStream(imageFile);
					BufferedOutputStream bos = new BufferedOutputStream(file);
					int i;
					while ((i = in.read()) != -1) {
						bos.write(i);
					}
					bos.flush();
					bos.close();
					
					chartImage = Image.getInstance(imageFile);
					} catch (Exception e) {
						logger.error("Error generating report chart via svg exporter", e);
					}
				}
			}
		}
		if(chartImage != null)
		{
			chartImage.setAlignment(Image.MIDDLE);
			//chart.scalePercent(60);
			//document.add(chart);
	
			if (chartImage.getPlainHeight() >= 750f) {
				chartImage.scaleAbsoluteHeight(742f);
			}
			PdfPTable aTable = new PdfPTable(1);
			aTable.setWidthPercentage(100f);
			aTable.getDefaultCell().setBorderWidth(0);
			aTable.getDefaultCell().setPadding(0);
			aTable.getDefaultCell().setBorderColor(white);
			//aTable.getDefaultCell().setBorderColor(black);
			
			chartImage.scaleToFit(document.getPageSize().getWidth()-90, document.getPageSize().getHeight()-90);
			
			aTable.addCell(chartImage);
	
			document.add(aTable);
			
			return chartImage.getHeight();
		}	
		return 0;
	}
	
	private static final TrustManager[] TRUSTING_SSL_MANAGER = new TrustManager[]{
			new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() { return null; }
				public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) { }
				public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException {}
			}
	};
	
	public void addReportTable(Document document, ReportBean report, float chartHeight) throws DocumentException {
		Image tableImage = null;
		if(MODE == GenerationMode.PhantomJS) {
			// Mode is PhantomJS, so attempt to build the report table via that means.
			HttpSession session = new com.sok.framework.servlet.HttpSession(InitServlet.getConfig().getServletContext());
			try { 
				final String fileKey = KeyMaker.generate();
				session.setAttribute(fileKey + "_report", report);
				final String url = doPhantomJSOutput(report.getConnection(),
								"/crm/reportgenerator/viewreportchart.jsp?-sessionKey=" + fileKey, fileKey, session);
				session.setAttribute(fileKey + "_report", null);
				tableImage = Image.getInstance(url); 
			} catch (Exception e) {
				logger.error("Error generating report table via phantomjs", e);
			}
		}
		if(tableImage != null) {
			logger.debug("Report Table generated via PhantomJS, adding to document");
			tableImage.setAlignment(Image.MIDDLE);
			//if (tableImage.getPlainHeight() >= 750f) {
			//	tableImage.scaleAbsoluteHeight(742f);
			//}
			logger.debug("have sizes height={} width={}", tableImage.getPlainHeight(), tableImage.getPlainWidth());
			
			tableImage.scaleToFit(document.getPageSize().getWidth()-90, document.getPageSize().getHeight()-90);
			if(tableImage.getPlainHeight() > tableImage.getPlainWidth()) {
				logger.debug("setting alternate page rotation");
				//portrait 
				document.setPageSize(PageSize.A4);
				document.newPage();
			} /*else if(chartHeight != 0 && (chartHeight + tableImage.getHeight()) > document.getPageSize().getHeight()) {
				//this still isn't right, but close enough
				logger.debug("new Page: chartHeight={}, tableImageHeight={}, pageSize={}", new String[]{String.valueOf(chartHeight), String.valueOf(tableImage.getHeight()), String.valueOf(document.getPageSize().getHeight())});				
				document.newPage();
			}*/
			PdfPTable aTable = new PdfPTable(1);
			aTable.setKeepTogether(true);
			aTable.setWidthPercentage(95f);
			aTable.getDefaultCell().setPadding(0);
			aTable.getDefaultCell().setBorderWidth(0);
			aTable.addCell(new Paragraph(""));
			aTable.addCell(tableImage);
			/* 
			PdfPCell cell = new PdfPCell(tableImage, false);
			cell.setBackgroundColor(BaseColor.WHITE);
			cell.setBorder(0);
			cell.setPadding(0);
			cell.setPaddingRight(5);
			aTable.addCell(cell);
			*/
			document.add(aTable);
		} else {
			logger.debug("Adding legacy report table to document");
			addPDFChartTable(document, report);
		}
	}
	
	
	private static final BaseColor black = new BaseColor(0, 0, 0);
	private static final BaseColor white = new BaseColor(255, 255, 255);
	private static final BaseColor grey = new BaseColor(200, 200, 200);
	private static final BaseColor lightgrey = new BaseColor(245, 245, 245);
	private static final Font font = FontFactory.getFont(FontFactory.HELVETICA, 8);
	private static final Font titlefont = FontFactory.getFont(FontFactory.HELVETICA, 9);
	private static final Font maintitlefont = FontFactory.getFont(FontFactory.HELVETICA, 11f, Font.BOLD, white);
	private static final Font maintitlesmallfont = FontFactory.getFont(FontFactory.HELVETICA, 9f, Font.NORMAL, white);
	
	private void addPDFChartTable(Document d, ReportBean report) throws DocumentException {
		List<List<ReportCell>> chartData = report.getChartData();
		boolean swap =  "true".equals(report.get("-swapsummary"));
		final int cols = swap ? chartData.size() : chartData.get(0).size(); 
		final int rows = swap ? chartData.get(0).size() : chartData.size();
		
		Chunk chunk;
		PdfPCell cell;
	
		PdfPTable aTable = new PdfPTable(cols);
		aTable.setWidthPercentage(95f);
		ChartCellAppender out = new ChartCellAppender(URL_HOME, aTable);
	
		Paragraph subtitle = new Paragraph();
	
		StringBuffer title = new StringBuffer();
		title.append("  ").append(report.getString("Name"));
		title.append(" - ").append(report.getString("Description"));				
		
		chunk = new Chunk(title.toString(), maintitlesmallfont);
		chunk.setTextRise(5.0f);
		subtitle.add(chunk);
		
		cell = new PdfPCell(subtitle);
		cell.setBorderColor(black);
		cell.setBackgroundColor(black);
		cell.setColspan(cols);
		//cell.add(subtitle);
		aTable.addCell(cell);
		
		if (!swap) {
			final int cds =  chartData.get(0).size();
			for (int i = 0; i < chartData.size(); i++) {
				java.util.List<ReportCell> row = chartData.get(i);
				//out.print("<tr>");
				for (int j = 0; j < row.size(); j++) {
					out.render(row.get(j)); 
				}
				out.setFirstRow(false);
				//out.print("</tr>");
				//out.print(div);
			}
		} else if (chartData.size() > 0) {
			for (int i = 0; i < chartData.get(0).size(); i++) {
				//out.print("<tr>");
				for (int j = 0; j < chartData.size(); j++) {
					out.render(chartData.get(j).get(i)); 
				}
				out.setFirstRow(false);
			}
		}
		d.add(aTable);
	}

	private String doPhantomJSOutput(Connection con, String urlPath, String fileKey, HttpSession session) throws Exception {
		ResponseOutputStream responseOS = null;
		FileOutputStream fos = null;

		final String date =  new SimpleDateFormat("yyyy_MM_dd_HH_mm_").format(new Date());
		
		//we're using the autopilot path as it will clean up after itself.
		String htmlfile = new StringBuilder(60)
						.append(AutoPilot.AUTOPILOT_FILE_PATH)
						.append("Report_Chart_")
						.append(date).append(KeyMaker.generate(4)).append(".html").toString();

		String pngfile = new StringBuilder(59)
						.append(AutoPilot.AUTOPILOT_FILE_PATH)
						.append("Report_Chart_")
						.append(date).append(KeyMaker.generate(4)).append(".png").toString();

		logger.trace("HTML [{}], PNG [{}]", htmlfile, pngfile);
		
		String htmlpath = session.getServletContext().getRealPath(htmlfile);
		String pngpath = session.getServletContext().getRealPath(pngfile); 

		String htmlurl = URL_HOME + htmlfile; 
		String imageurl = URL_HOME + pngfile; 
		if (StringUtils.isNotBlank(InitServlet.getSystemParam("PhantomJSURL"))) {
			htmlurl = InitServlet.getSystemParam("PhantomJSURL") + htmlfile;
			imageurl = InitServlet.getSystemParam("PhantomJSURL") + pngfile;
		}
		try {
			File file = new File(htmlpath);
			fos = new java.io.FileOutputStream(file);
			responseOS = new com.sok.framework.servlet.ResponseOutputStream(session, fos, urlPath);
			responseOS.service(con);
			String ex = String.format(PHANTOMJS_FORMAT, htmlurl, pngpath);
			logger.debug("Launching external process [{}]", ex);
			Process p=Runtime.getRuntime().exec(ex); 
			//OutputStream os = p.getOutputStream();
			p.waitFor();  

			session.setAttribute(fileKey, file);

			return imageurl;
		} finally {
			if (fos != null) {
				try {
					fos.flush();
					fos.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			if (responseOS != null) {
				responseOS.close();
			}
		}
	}
	
	private class ChartCellAppender  implements ReportBean.ReportCellRenderer {
		PdfPTable aTable; 
		String urlHome; 
		Chunk chunk;
		PdfPCell cell;
		boolean firstRow = true;
		
		public ChartCellAppender(String urlHome, PdfPTable aTable) {
			this.urlHome = urlHome; 
			this.aTable = aTable; 
		}
		
		public void setFirstRow(boolean firstRow) {
			this.firstRow = firstRow;
		}
		public void render(ReportCell reportCell) { 
			//BlankCell, TotalCell, ValueCell, TitleCell etc.
			Font f = (reportCell instanceof TitleCell) ? titlefont : font;
			String s = reportCell.toString();
			int indent = 0;
			while(s.startsWith("&nbsp;")) {
				s = s.substring(6);
				indent++;
			}
			
			if(s.startsWith("<strong>") && s.endsWith("</strong>")) {
				f = titlefont; 
				s = s.substring(8, s.length() - 9);
			}
			
			indent = indent / 4;
			
			if(reportCell instanceof TitleCell && !firstRow) {
				//s = " " + s;
			}
			
			while(indent > 0) {
				//s = "\t" + s;
				indent--;
			}
			chunk = new Chunk(s, f);
			
			if(reportCell.hasTitle()) { 
				// TODO create a tool-tip (PdfAnnotation) for this 
			}
			if(reportCell.hasLink()) { 
				chunk.setAnchor(urlHome + reportCell.getLink());
			}
			chunk.setTextRise(5.0f);
			
			if(firstRow) { 
				//chunk.setSkew(60f,-15f);
			}

			cell = new PdfPCell(new Paragraph(chunk));
			cell.setBorderWidth(1);
			cell.setBorderColor(black);
			cell.setPaddingTop(10.0f);
			
			cell.setHorizontalAlignment(1);	
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

			cell.setBorderColor(grey);
			cell.setBackgroundColor(lightgrey);
			
			aTable.addCell(cell);
		}
	}
	
}
