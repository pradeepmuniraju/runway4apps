package com.sok.runway.crm;

import com.sok.Debugger;
import com.sok.runway.*;
import com.sok.runway.crm.factories.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.runway.crm.activity.*;
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;

import javax.servlet.ServletRequest;
import java.sql.*;
import java.util.*;

public class Note extends ProfiledEntity implements Logable, DocumentLinkable {

   private static final String ANSWER_VIEWSPEC = "answers/NoteAnswerView";
   public static final String ENTITY_NAME = "Note";
   protected static final String[][] emailheaderfields = { {"FROM","EmailFrom"}, {"TO","EmailTo"}, {"CC","EmailCC"}, {"BCC","EmailBCC"}, {"REPLY_TO","EmailReplyTo"}};

   public Note(Connection con) {
      super(con, "NoteView");
   }
	
	public Note(ServletRequest request) {
	    super(request, "NoteView");
       populateFromRequest(request.getParameterMap());
	}
   
   public Note(Connection con, String NoteID) {
      this(con);
      load(NoteID);
   }
   
   public String getAnswerViewSpec() {
      return ANSWER_VIEWSPEC;
   }

   public String getEntityName() {
	   return ENTITY_NAME;
   }
   
   public void addContactAttendee(String contactID) {
	   addContactAttendee(contactID, null);
   }
   
   public void addUserAttendee(String userID) {
	   addAttendee(userID, true);
   }
   
   /**
    * Added ability to specify how many people are attending. Only added for contacts, not users. 
    */
   public void addContactAttendee(String contactID, Integer attending) {
      addAttendee(contactID, true, attending);
   }
   
   private void addAttendee(String id, boolean isContact) { 
	   addAttendee(id,isContact,null); 
   }
   private void addAttendee(String id, boolean isContact, Integer attending) {
      GenRow attendee = new GenRow();
      attendee.setConnection(getConnection());
      
      if (id != null && id.length() != 0 && getPrimaryKey() != null && getPrimaryKey().length() != 0) {
         if (isContact) {
            attendee.setViewSpec("AttendeeContactView");
            attendee.setParameter("ContactID", id);
         }
         else {
            attendee.setViewSpec("AttendeeUserView");
            attendee.setParameter("UserID", id);
         }  
         attendee.setParameter("NoteID", getPrimaryKey());
         attendee.doAction(SELECTFIRST);
         
         if (attendee.getData("AttendeeID").length() == 0) {
            attendee.clear();
            attendee.setParameter("NoteID", getPrimaryKey());
            if (isContact) {
               attendee.setParameter("ContactID", id);
            }
            else {
               attendee.setParameter("UserID", id);
            }
            if(attending != null) { 
            	attendee.setParameter("Attending",attending); 
            }
            attendee.createNewID();
            attendee.doAction(INSERT);
         }
      }
   }
   
   /**
    * Will be used to save HTMLBody to seperate table after insert. 
    */
   public ErrorMap insert() { 
	   ErrorMap errors = super.insert(); 
	   if(errors.isEmpty()) { 
		   //Create the Email record. 
         if("Email".equals(bean.getString("Type")))
         {
            GenRow email = new GenRow();
            email.setConnection(getConnection()); 
            email.setTableSpec("Emails"); 
            email.setParameter("EmailID", getPrimaryKey());
            copyEmailFields(email);
            email.setAction(GenerationKeys.INSERT);
            email.doAction();     
         }
    
	   }
	   return errors; 
   }
   
   /**
    * Will be used to save HTMLBody to seperate table after insert. 
    */
   public ErrorMap update() { 
	   ErrorMap errors = super.update(); 
	   if(errors.isEmpty()) { 
         
         //Create the Email record.  
         if("Email".equals(bean.getString("Type")))
         {
            GenRow email = new GenRow();
            email.setConnection(getConnection()); 
            email.setTableSpec("Emails"); 
            email.setParameter("EmailID", getPrimaryKey());
            copyEmailFields(email);
            email.setAction(GenerationKeys.UPDATE);
            email.doAction();     
         }

	   }
	   return errors; 
   }
   
   protected void copyEmailFields(GenRow row)
   {
      TableSpec tspec = row.getTableSpec();
      String name = null;
      String value = null;
      for(int i=0; i<tspec.getFieldsLength(); i++)
      {
         name = tspec.getFieldName(i);
         value = bean.getParameter(name);
         if(value!=null)
         {
            row.setParameter(name, value);
         }
      }
      for(int i=0; i< emailheaderfields.length; i++)
      {
         value = bean.getParameter(emailheaderfields[i][0]);
         if(value!=null)
         {
            row.setParameter(emailheaderfields[i][1], value);
         }
      }
   }
   
   public void beforeSave() {
      super.beforeSave();
      
		if(getParameter("NoteDate").length()!=0 && getParameter("NoteTime").length()!=0) {
			StringBuffer notedate = new StringBuffer();
			notedate.append(getParameter("NoteDate"));
			notedate.append(" ");
			notedate.append(getParameter("NoteTime"));
			
			setParameter("NoteDate",notedate.toString());
			if(getParameter("Duration").length()!=0) {
				setParameter("EndDate", UserCalendar.getEndDate(notedate.toString(), getParameter("Duration")));
			}
		}
   }
   
   /**
    * Deletes Child Data of Notes after RunwayEntity has removed the note. 
    * Profile data will be removed by ProfiledEntity. 
    */
   public boolean delete() {
      if (super.delete()) {
    	  GenRow del = new GenRow();
    	  del.setConnection(getConnection()); 
    	  del.setTableSpec("Responses"); 
    	  del.setParameter(GenerationKeys.UPDATEON + getPrimaryKeyName(), getPrimaryKey());
    	  del.setAction(GenerationKeys.DELETEALL); 
    	  del.doAction(); 
    	  
    	  del.setTableSpec("Attendees"); 
    	  del.doAction(); 
    	  
    	  return true; 
      }
      return false; 
   } 
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("Note");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("NoteID","SYSTEM ERROR");
         val.addMandatoryField("GroupID","SYSTEM ERROR");
         Validator.addValidator("Note", val);
      }
      return val;
   }

   /**
    *    Linkable Interface
    */
   public String getLinkableTitle() {
      return getField("Subject");
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs() {
      return getLinkableDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters);
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs() {
      return getDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
   }     
   
   /**
    *    DocumentLinkable Interface
    */
   public LinkableDocument getLinkableDocument() {
      LinkableDocument doc = new LinkedDocument(getConnection());
      doc.setCurrentUser(getCurrentUser());
      return doc;
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public void linkDocument(String documentID) {
      LinkableDocumentFactory.linkDocument(this, documentID);
   }
}