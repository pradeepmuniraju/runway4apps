package com.sok.runway.crm.cms.properties.pdf.templates.model;

import java.util.ArrayList;

/**
 * Value Object created to store Stage/Building details
 *
 */
public class Stage {
	private String stageID;
	
	private String stageName;
	
	private ArrayList<ProductLotInfo> lots;
	
	private String disclaimer;
	
	private String dripImage;
	
	public Stage(){
		lots = new ArrayList<ProductLotInfo>();
	}

	public String getStageID() {
		return stageID;
	}

	public void setStageID(String stageID) {
		this.stageID = stageID;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public ArrayList<ProductLotInfo> getLots() {
		return lots;
	}

	public void setApartments(ArrayList<ProductLotInfo> lots) {
		this.lots = lots;
	}
	
	public void addLot(ProductLotInfo lot){
		lots.add(lot);
	}
	
	public int getLotCount(){
		return lots.size();
	}

	public String getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	public String getDripImage() {
		return dripImage;
	}

	public void setDripImage(String dripImage) {
		this.dripImage = dripImage;
	}
	
}
