package com.sok.runway.crm.cms.properties.pdf.templates.model;

public class ProductLotInfo 
{
	public String lotStatus;  // New, Sold, Not Released	
	public String consultant;
	public String lotNumber;
	public String lotPrice;
	public String exclusiveHold;
	
	public double lotWidth;
	public double lotDepth;
	public String lotSize;
	public String isLotTitled;
	
	public String expectedTO;
	
	public String estateProductID;
	public String estateEstateName;
	public String estateStageName;

	public int brandRange1Count = 0;
	public int brandRange2Count = 0;
	public int brandRange3Count = 0;
	public int brandRange4Count = 0;
	public int brandRange5Count = 0;
	public int brandRange6Count = 0;
	public int brandRange7Count = 0;
	public int brandRange8Count = 0;
	public int brandRange9Count = 0;
	public int brandRange10Count = 0;
	
	public String productID;

	public int getBrandRange1Count() {
		return brandRange1Count;
	}
	public void setBrandRange1Count(int brandRange1Count) {
		this.brandRange1Count = brandRange1Count;
	}
	public int getBrandRange2Count() {
		return brandRange2Count;
	}
	public void setBrandRange2Count(int brandRange2Count) {
		this.brandRange2Count = brandRange2Count;
	}
	public int getBrandRange3Count() {
		return brandRange3Count;
	}
	public void setBrandRange3Count(int brandRange3Count) {
		this.brandRange3Count = brandRange3Count;
	}
	public int getBrandRange4Count() {
		return brandRange4Count;
	}
	public void setBrandRange4Count(int brandRange4Count) {
		this.brandRange4Count = brandRange4Count;
	}
	public int getBrandRange5Count() {
		return brandRange5Count;
	}
	public void setBrandRange5Count(int brandRange5Count) {
		this.brandRange5Count = brandRange5Count;
	}
	public int getBrandRange6Count() {
		return brandRange6Count;
	}
	public void setBrandRange6Count(int brandRange6Count) {
		this.brandRange6Count = brandRange6Count;
	}
	public int getBrandRange7Count() {
		return brandRange7Count;
	}
	public void setBrandRange7Count(int brandRange7Count) {
		this.brandRange7Count = brandRange7Count;
	}
	public int getBrandRange8Count() {
		return brandRange8Count;
	}
	public void setBrandRange8Count(int brandRange8Count) {
		this.brandRange8Count = brandRange8Count;
	}
	public int getBrandRange9Count() {
		return brandRange9Count;
	}
	public void setBrandRange9Count(int brandRange9Count) {
		this.brandRange9Count = brandRange9Count;
	}
	public int getBrandRange10Count() {
		return brandRange10Count;
	}
	public void setBrandRange10Count(int brandRange10Count) {
		this.brandRange10Count = brandRange10Count;
	}
	public String getEstateProductID() {
		return estateProductID;
	}
	public void setEstateProductID(String estateProductID) {
		this.estateProductID = estateProductID;
	}
	public String getEstateEstateName() {
		return estateEstateName;
	}
	public void setEstateEstateName(String estateEstateName) {
		this.estateEstateName = estateEstateName;
	}
	public String getEstateStageName() {
		return estateStageName;
	}
	public void setEstateStageName(String estateStageName) {
		this.estateStageName = estateStageName;
	}
	public String getLotStatus() {
		return lotStatus;
	}
	public void setLotStatus(String lotStatus) {
		this.lotStatus = lotStatus;
	}
	public String getConsultant() {
		return consultant;
	}
	public void setConsultant(String consultant) {
		this.consultant = consultant;
	}
	public String getLotNumber() {
		return lotNumber;
	}
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public String getLotPrice() {
		return lotPrice;
	}
	public void setLotPrice(String lotPrice) {
		this.lotPrice = lotPrice;
	}
	public String getExclusiveHold() {
		return exclusiveHold;
	}
	public void setExclusiveHold(String exclusiveHold) {
		this.exclusiveHold = exclusiveHold;
	}
	public double getLotWidth() {
		return lotWidth;
	}
	public void setLotWidth(double lotWidth) {
		this.lotWidth = lotWidth;
	}
	public double getLotDepth() {
		return lotDepth;
	}
	public void setLotDepth(double lotDepth) {
		this.lotDepth = lotDepth;
	}
	public String getLotSize() {
		return lotSize;
	}
	public void setLotSize(String lotSize) {
		this.lotSize = lotSize;
	}
	public String getExpectedTO() {
		return expectedTO;
	}
	public void setExpectedTO(String expectedTO) {
		this.expectedTO = expectedTO;
	}
	public String getIsLotTitled() {
		return isLotTitled;
	}
	public void setIsLotTitled(String isLotTitled) {
		this.isLotTitled = isLotTitled;
	}
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}

}