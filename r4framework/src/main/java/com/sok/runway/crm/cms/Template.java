package com.sok.runway.crm.cms;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys; 
import com.sok.runway.Validator;
import com.sok.runway.crm.LinkableDocument;
import com.sok.runway.crm.LinkedDocument; 
import com.sok.runway.crm.interfaces.DocumentLinkable;
import com.sok.runway.crm.factories.LinkableDocumentFactory;
import com.sok.runway.security.SecuredEntity;

import java.util.Map;
import java.util.Collection; 
import java.sql.Connection;

import javax.servlet.ServletRequest;

public class Template extends SecuredEntity implements DocumentLinkable {
   
   public static final String ENTITY_NAME = "Template";
   
   public Template(Connection con) {
      super(con, "TemplateView");
   }
	
	public Template(ServletRequest request) {
	    super(request, "TemplateView");
       populateFromRequest(request.getParameterMap());
	}
   
   public Template(Connection con, String contactID) {
      this(con);
      load(contactID);
   }

   public String getEntityName() {
	   return ENTITY_NAME;
   }
   
   /**
    * Deletes Child Data of Templates after RunwayEntity has removed the master template. 
    * 
    */
   public boolean delete() {
      if (super.delete()) {
         
    		GenRow del = new GenRow();
	 		del.setConnection(getConnection());	
    		del.setTableSpec("TemplateURLs");
    		del.setParameter(GenerationKeys.UPDATEON + getPrimaryKeyName(), getPrimaryKey());
    		del.setAction(com.sok.framework.generation.GenerationKeys.DELETEALL); 
    		del.doAction();
    		
    		del.setTableSpec("LinkedDocuments");
    		del.doAction();
    	  
         return true;
      }
      else {
         return false;
      }
   }
        
   public Validator getValidator() {
      Validator val = Validator.getValidator("Template");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("TemplateID","SYSTEM ERROR - TemplateID");
         val.addMandatoryField("GroupID","Template must have a security group selected");
         val.addMandatoryField("TemplateName","Template must have a Name");
         Validator.addValidator("Template", val);
      }
      return val;
   }
   
   
   /**
    *    Linkable Interface
    */
   public String getLinkableTitle() {    
      return getField("TemplateName");
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs() {
      return getLinkableDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters);
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs() {
      return getDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
   }      
   
   /**
    *    DocumentLinkable Interface
    */
   public LinkableDocument getLinkableDocument() {
      LinkableDocument doc = new LinkedDocument(getConnection());
      doc.setCurrentUser(getCurrentUser());
      return doc;
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public void linkDocument(String documentID) {
      LinkableDocumentFactory.linkDocument(this, documentID);
   }
}