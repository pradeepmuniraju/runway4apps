package com.sok.runway.crm.cms.properties.pdf.templates.model;

import java.util.ArrayList;

public class ApartmentInfo {
	
	private String buildingID = ""; 
	private String apartmentID = "";

	private String buildingName = ""; 
	private String apartmentName = "";
	
	private String lotNumber = "";
	private String level = "";
	private String type = "";
	
	private int internalArea;
	private int balconyArea;
	private int totalArea;

	public int getInternalArea() {
		return internalArea;
	}
	public int getBalconyArea() {
		return balconyArea;
	}
	public int getTotalArea() {
		return totalArea;
	}
	public void setInternalArea(int internalArea) {
		this.internalArea = internalArea;
	}
	public void setBalconyArea(int balconyArea) {
		this.balconyArea = balconyArea;
	}
	public void setTotalArea(int totalArea) {
		this.totalArea = totalArea;
	}
	private int carSpaces = 0;
	
	private String price = "";
	private String rent = "";
	private String salesYield = "";
	private String stage = "";
	private String salesStatus = "";
	
	private String heroImage = "";
	
	private String apartmentDetailsText1 = "";
	private String apartmentDetailsText2 = "";

	private ArrayList<String> buildingImagesList = new ArrayList<String>();
	private ArrayList<String> apartmentImagesList = new ArrayList<String>();
	private ArrayList<String> apartmentFloorPlansImagesList = new ArrayList<String>();
	
	public ArrayList<String> getApartmentFloorPlansImagesList() {
		return apartmentFloorPlansImagesList;
	}
	public void setApartmentFloorPlansImagesList(
			ArrayList<String> apartmentFloorPlansImagesList) {
		this.apartmentFloorPlansImagesList = apartmentFloorPlansImagesList;
	}
	public String getApartmentDetailsText1() {
		return apartmentDetailsText1;
	}
	public void setApartmentDetailsText1(String apartmentDetailsText1) {
		this.apartmentDetailsText1 = apartmentDetailsText1;
	}
	public String getApartmentDetailsText2() {
		return apartmentDetailsText2;
	}
	public void setApartmentDetailsText2(String apartmentDetailsText2) {
		this.apartmentDetailsText2 = apartmentDetailsText2;
	}
	public ArrayList<String> getBuildingImagesList() {
		return buildingImagesList;
	}
	public void setBuildingImagesList(ArrayList<String> buildingImagesList) {
		this.buildingImagesList = buildingImagesList;
	}
	public ArrayList<String> getApartmentImagesList() {
		return apartmentImagesList;
	}
	public void setApartmentImagesList(ArrayList<String> apartmentImagesList) {
		this.apartmentImagesList = apartmentImagesList;
	}
	public String getHeroImage() {
		return heroImage;
	}
	public void setHeroImage(String heroImage) {
		this.heroImage = heroImage;
	}
	
	public String getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(String buildingID) {
		this.buildingID = buildingID;
	}
	public String getApartmentID() {
		return apartmentID;
	}
	public void setApartmentID(String apartmentID) {
		this.apartmentID = apartmentID;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getApartmentName() {
		return apartmentName;
	}
	public void setApartmentName(String apartmentName) {
		this.apartmentName = apartmentName;
	}
	public String getLotNumber() {
		return lotNumber;
	}
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getCarSpaces() {
		return carSpaces;
	}
	public void setCarSpaces(int carSpaces) {
		this.carSpaces = carSpaces;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getRent() {
		return rent;
	}
	public void setRent(String rent) {
		this.rent = rent;
	}
	public String getSalesYield() {
		return salesYield;
	}
	public void setSalesYield(String salesYield) {
		this.salesYield = salesYield;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public String getSalesStatus() {
		return salesStatus;
	}
	public void setSalesStatus(String salesStatus) {
		this.salesStatus = salesStatus;
	}
}
