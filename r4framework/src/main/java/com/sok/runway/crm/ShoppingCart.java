package com.sok.runway.crm;

import java.util.*;

public class ShoppingCart extends HashMap<String,Product>
{
	Iterator<String> it = null;
	
	Order order = null;
	Product freight = null;
	boolean completed = false;
	Map<String, Product> promoCodes = new HashMap<String, Product>(); 
	Map<String, Product> freightCharges = new HashMap<String, Product>(); 

	public void setCompleted(boolean b)
	{
		completed = true;
	}

	public boolean getCompleted()
	{
		return(completed);
	}
	
	/**
	 * Remove any associated promo code
	 */
	public Product remove(String id) { 
		promoCodes.remove(id); 
		freightCharges.remove(id); 
		return super.remove(id); 
	}
	
	/**
	 * Promo Codes Stored as RelatedProductID, PromoCode Product
	 */
	public Map<String, Product> getPromoCodeMap() { 
		return promoCodes; 
	}
	
	/**
	 * Promo Codes Stored as RelatedProductID, PromoCode Product
	 */
	public Map<String, Product> getFreightChargesMap() { 
		return freightCharges; 
	}

	public void setOrder(Order hd)
	{
		order = hd;
	}

	public Order getOrder()
	{
		return(order);
	}

	public Product getFreight()
	{
		return(freight);
	}

	public void setFreight(Product freight)
	{
		this.freight = freight;
	}

	public void addTotals()
	{
		double totalcost = 0.0;
		double cost = 0.0;
		double gst = 0.0;
		int qty = 0;
		int currentqty = 0; 
		Iterator<String> products = this.keySet().iterator();
		Iterator<String> codes = promoCodes.keySet().iterator();
		Iterator<String> charges = freightCharges.keySet().iterator(); 
		Product product = new Product(order.getConnection());
		while(products.hasNext() || codes.hasNext() || charges.hasNext())
		{	
			if(products.hasNext()) { 
				product = get(products.next()); 
			} else if (codes.hasNext()) { 
				product = promoCodes.get(codes.next());
			} else if (charges.hasNext()) { 
				product = freightCharges.get(charges.next());
			} 
			
			String q = product.getField("Quantity"); 
			if(q==null || q.equals("")) { 
				product.setField("Quantity","1"); 
				q = "1"; 
			}
			
			currentqty = Integer.parseInt(q);
			cost = cost + (product.getDouble("Cost") * currentqty);
			gst = gst  + (product.getDouble("GST") * currentqty);
			totalcost = totalcost + (product.getDouble("TotalCost") * currentqty);
			qty = qty + currentqty;
		}
		
		if(freight != null) { 
			
			String q = freight.getField("Quantity"); 
			if(q==null || q.equals("")) { 
				freight.setField("Quantity","1"); 
				q = "1"; 
			}
			
			currentqty = Integer.parseInt(q);
			cost = cost + (freight.getDouble("Cost") * currentqty);
			gst = gst  + (freight.getDouble("GST") * currentqty);
			totalcost = totalcost + (freight.getDouble("TotalCost") * currentqty);
			qty = qty + currentqty;

		}
		
		order.setParameter("Cost",new Double(cost));
		order.setParameter("GST",new Double(gst));
		order.setParameter("TotalCost",new Double(totalcost));
		order.setParameter("Quantity",String.valueOf(qty));
	}

	/**
	 * @deprecated
	 */
	static double doubleValue(Object obj)
	{
		if(obj instanceof Double)
		{
			return(((Double)obj).doubleValue());
		}
		else
		{
			return(Double.parseDouble((String)obj));
		}
	}
	
	
	
	/* iteration methods */ 
	public Product getProduct(String key)
	{
		return(get(key));
	}

	public void putProduct(String key, Product bean)
	{
		put(key,bean);
		it = null;
	}

	public boolean hasNextProduct()
	{
		return(it.hasNext());
	}

	public Product nextProduct()
	{
		return(get(it.next()));
	}

	public void resetIterator()
	{
		it = keySet().iterator();
	}

	public Iterator<String> getIterator()
	{
		return(it);
	}

	public void clearIterator()
	{
		it = null;
	}

}