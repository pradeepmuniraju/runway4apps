package com.sok.runway.crm.cms.properties.pdf;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.pdf.PropertyPDFServlet.PDFVariant;
import com.sok.runway.crm.cms.properties.pdf.templates.developer.DeveloperPdfGenerator;

public class PropertyPDFGenerator {
	public static final String GENERIC_PACKAGE = "com.sok.runway.crm.cms.properties.pdf.templates.";
	public static final String GENERIC_JASPER_PACKAGE = "com.sok.runway.crm.cms.properties.pdf.jasper.report.";
	
	public static final String SPECIFIC_PACKAGE = "com.sok.clients.pdf.templates.";
	public static final String DEV_SPECIFIC_PACKAGE = "com.sok.dev.<$$$>.pdf.templates.";

	public static final String JASPER_PACKAGE = "com.sok.clients.pdf.jasper.report.";
	public static final String DEV_JASPER_PACKAGE = "com.sok.dev.<$$$>.pdf.jasper.report.";
	
	private static final Logger logger = LoggerFactory.getLogger(PropertyPDFGenerator.class);
	
	public static String className = "PropertyPDFGenerator.java";
	
	static {
		
		String classAndMethodName = className;
		logger.debug(classAndMethodName + " Before calling DeveloperPdfGenerator.loadDeveloperJars()");
		
		DeveloperPdfGenerator.loadDeveloperJars();
		
		logger.debug(classAndMethodName + " After calling DeveloperPdfGenerator.loadDeveloperJars()");
	}

	public static void generate(String classname, HttpServletRequest request, HttpServletResponse response) {
		try {
			
			String classAndMethodName = className + " generate(String classname, HttpServletRequest request, HttpServletResponse response) ";
			logger.debug(classAndMethodName + " Started.");
			
			Map<String, String> estates = null;
			String developerID = "";
			String packageName = "";
			String estateName = "";
			String specificPackage = "";
			String fqClassName = "";
			
			AbstractPDFTemplate template = null;
			AbstractJasperReport jasperRunner = null;
			try {
				
				final PDFVariant variant = PDFVariant.fromString(request.getParameter("-variant"));
				
				if (variant == PDFVariant.DeveloperPdf) {
					
					logger.debug(classAndMethodName + " variant is DeveloperPdf");

					try {
						String estateID = request.getParameter("EstateID");
						
						logger.debug(classAndMethodName + " estateID is: " + estateID);
						
						if(!StringUtils.isEmpty(estateID)){
							developerID = getDeveloperID(estateID);		
							if(!StringUtils.isEmpty(developerID)){
								estates = getEstateMapByDeveloperID(developerID);
								if(estates != null && estates.size() > 0)
									request.setAttribute("estateMap", estates);
							}	
						}
						
						logger.debug(classAndMethodName + " Before Getting JAR Name. developerID is: " + developerID);
						
						packageName = DeveloperPdfGenerator.getJarName(request, developerID);
						
						logger.debug(classAndMethodName + " packageName is: " + packageName);
						
						if(!StringUtils.isEmpty(packageName)){
							packageName = DEV_JASPER_PACKAGE.replace("<$$$>", getDeveloperJarName(request));
							if(estates.size() > 1){
								estateName = estates.get(estateID);
								specificPackage = packageName + estateName + ".";
								fqClassName = specificPackage + classname;
								
								logger.debug(classAndMethodName + " estateName is: " + estateName + "  specificPackage is: " + specificPackage + " , fqClassName: " + fqClassName);
								
								try{
									
									logger.debug(classAndMethodName + " Before Loading fqClassName: " + fqClassName);
									
									jasperRunner = (AbstractJasperReport) (DeveloperPdfGenerator.loadClass(fqClassName).newInstance());
									
									logger.debug(classAndMethodName + " After Loading fqClassName: " + fqClassName);
									
								} catch (ClassNotFoundException e) {
									logger.debug("Could not load specific package: " + fqClassName);									
									fqClassName = packageName + classname;
									logger.debug("Load generic package instead: " + fqClassName);
									//If specific classes is not found, then load generic developer package
									jasperRunner = (AbstractJasperReport) (DeveloperPdfGenerator.loadClass(fqClassName).newInstance());
								}
							}else{
								
								fqClassName = packageName + classname;
								logger.debug(classAndMethodName + " packageName: " + packageName + " ,  fqClassName: " + fqClassName);

								logger.debug(classAndMethodName + " Before loading Class " + fqClassName);
								jasperRunner = (AbstractJasperReport) (DeveloperPdfGenerator.loadClass(fqClassName).newInstance());
								logger.debug(classAndMethodName + " After loading Class " + fqClassName);
							}
							logger.debug(classAndMethodName + " Setting DeveloperPDF true");
							jasperRunner.setDeveloperPDF(true);
							
							logger.debug(classAndMethodName + " Calling the method jasperRunner.generateReport(request, response)");
							jasperRunner.generateReport(request, response);
							
							logger.debug(classAndMethodName + " Ended.");
							return;
						}						
					} catch (ClassNotFoundException e) { //Neither jasper specific or generic class is found
						e.printStackTrace();
						logger.debug("Error loading jasper developer pdf class: " + fqClassName);
						template = new DeveloperPdfGenerator(classname);
					}
				} else {
					try {
						// Load class from Jasper package first
						logger.debug("Loading jasper specific package class: " + JASPER_PACKAGE + classname);
						jasperRunner = (AbstractJasperReport) (Class.forName(JASPER_PACKAGE + classname).newInstance());
						
						logger.debug(classAndMethodName + " Setting DeveloperPDF false");
						
						jasperRunner.setDeveloperPDF(false);
						
						logger.debug(classAndMethodName + " Before calling jasperRunner.generateReport(request, response)");
						
						jasperRunner.generateReport(request, response);
						
						logger.debug(classAndMethodName + " After calling jasperRunner.generateReport(request, response)");
						
						return;
					} catch (ClassNotFoundException e) {
						// Couldnot load class from Jasper package, then load iText Class
						logger.error("Error loading jasper specific package class: {}", JASPER_PACKAGE + classname);
						template = (AbstractPDFTemplate) (Class.forName(SPECIFIC_PACKAGE + classname).newInstance());
					}
				}
			} catch (Exception e) {
				logger.error("Error loading specific package class: {}", SPECIFIC_PACKAGE + classname);
				logger.error("Unable to load specific pdf template. Using default " + GENERIC_PACKAGE + classname);
				template = (AbstractPDFTemplate) (Class.forName(GENERIC_PACKAGE + classname).newInstance());
			}
			
			logger.debug(classAndMethodName + " Before calling template. generatePDF(request, response)");
			
			template.generatePDF(request, response);
			
			logger.debug(classAndMethodName + " After calling template. generatePDF(request, response)");
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error generating pdf template " + classname + ". Exception: " + e);
		}
	}
	
	public static String getDeveloperJarName(HttpServletRequest request) {
		logger.debug("PropertyPDFGenerator.java  getDeveloperJarName(request) Started");
		Map<String, String> estateNames = null;
		String developerID = "";
		String estateID = request.getParameter("EstateID");		
		if(!StringUtils.isEmpty(estateID)){
			developerID = getDeveloperID(estateID);		
			if(StringUtils.isEmpty(developerID)){
				estateNames = getEstateMapByDeveloperID(developerID);
			}	
		}		
		
		logger.debug("PropertyPDFGenerator.java  getDeveloperJarName(request) will be Ended after making call to getPackageName(). developerID: " + developerID);
		
		return getPackageName(DeveloperPdfGenerator.getJarName(request, developerID));
	}
	
	
	/**
	 * Get DeveloperID
	 * @param request
	 * @return
	 */
	public static String getDeveloperID(String estateID) {
		
		logger.debug("PropertyPDFGenerator.java  getDeveloperID(estateID). Started. estateID is: " + estateID);
		
		String developerID = "";
		GenRow estateGR = new GenRow();
		try {
			estateGR.setViewSpec("properties/ProductEstateView");
			estateGR.setParameter("ProductType", "Estate");
			estateGR.setParameter("ProductID", estateID);
			if (StringUtils.isNotBlank(estateID)) {
				estateGR.setAction(GenerationKeys.SEARCH);
				estateGR.doAction(GenerationKeys.SELECTFIRST);
				developerID = estateGR.getColumn("DeveloperProductID");
			}
		} catch (Exception ee) {
			// System.out.println("Exception occurred while getting DeveloperID of the selected Estate.");
			logger.error("Exception occurred while getting DeveloperID of the selected Estate.");
			ee.printStackTrace();
		}
		logger.trace("INPUT estateID: " + estateID + "  , OUTPUT developerID: " + developerID);
		
		logger.debug("PropertyPDFGenerator.java  getDeveloperID(estateID). Ended. Returning developerID: " + developerID);
		
		return developerID;
	}

	/**
	 * Get Estate name by developerID
	 * @param developerID
	 * @return estateNames
	 */
	public static Map<String, String> getEstateMapByDeveloperID(String developerID){
		
		logger.debug("PropertyPDFGenerator.java  getEstateMapByDeveloperID(String developerID) Started. developerID: " + developerID);
		
		String estateName = "";
		String estateID = "";
		if (StringUtils.isBlank(developerID))
			return null;
		Map<String, String> estates = new HashMap<String, String>();		
		GenRow estateGR = new GenRow();
		try {
			estateGR.setViewSpec("ProductListView");
			estateGR.setParameter("ProductType", "Estate");
			estateGR.setParameter("DeveloperProductID", developerID);
			estateGR.setParameter("-select0", "ProductParentDeveloperEstate.ProductVariationID AS ProductVariationID");
			estateGR.setParameter("ProductParentDeveloperEstate.ParentProductID", developerID);
			estateGR.setParameter("-groupby0","ProductID");
			estateGR.sortBy("Name",0);
			estateGR.doAction("search");
			estateGR.setAction(GenerationKeys.SEARCH);
			estateGR.doAction(GenerationKeys.SEARCH);
			estateGR.getResults();
			while(estateGR.getNext()) {
				if ("true".equals(InitServlet.getSystemParam("PDFDEV-SummaryAsEstateName"))) {
					estateName = estateGR.getData("Summary");
					if(StringUtils.isBlank(estateName))
						estateName = estateGR.getData("Name");
				} else {
					estateName = estateGR.getData("Name");
				}
				estateID = estateGR.getData("ProductID");
				if(!StringUtils.isEmpty(estateName)){
					estateName = StringUtils.deleteWhitespace(estateName);
					estateName = StringUtils.lowerCase(estateName);
					estates.put(estateID, estateName);					
				}	
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		}		
		
		logger.debug("PropertyPDFGenerator.java  getEstateMapByDeveloperID(String developerID) Ended.");
		
		return estates;
	}
	

	/**
	 * Jar names of format developer-balcon-dev.jar
	 * 
	 * @param jarName
	 * @return
	 */
	private static String getPackageName(String jarName) {
		logger.debug("PropertyPDFGenerator.java  getPackageName(jarName). Started. jarName is: " + jarName);
		if (StringUtils.isNotBlank(jarName)) {
			String[] names = jarName.split("-");

			if (names.length > 1) {
				logger.debug("PropertyPDFGenerator.java  getPackageName(jarName). Ended. names[1] is: " + names[1]);
				return names[1];
			}
		}
		
		logger.debug("PropertyPDFGenerator.java  getPackageName(jarName). Ended. and returning empty string");
		
		return "";
	}
}