package com.sok.runway.crm.reports;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.service.crm.UserService;

public class DashboardPDFServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(DashboardPDFServlet.class);
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		final UserBean currentuser = UserService.getInstance().getCurrentUser(request);
		final String dashboardID = StringUtils.trimToNull(request.getParameter("-dashboardID"));
		
		GenRow dashboard = new GenRow();
		Document document = null;
		try {
			logger.debug("Generating dashboard PDF");
			document = new Document(PageSize.A4.rotate(), 20, 20, 20, 20);
			PdfWriter.getInstance(document, response.getOutputStream());
			document.open();
						
			if(dashboardID == null) {
				Paragraph top = new Paragraph();
				top.add(new Chunk("Error - DashBoardID was not provided - ", FontFactory.getFont(FontFactory.HELVETICA, 12)));
				top.add(new Chunk((currentuser != null ? currentuser.getCurrentDateString() : new Date().toString()), FontFactory.getFont(FontFactory.HELVETICA, 8)));
				document.add(top);
				
				return;
			}
			logger.debug("Generating dashboard PDF with DashBoardID=[{}]", dashboardID);
			
			dashboard.setViewSpec("DashBoardItemView");
			dashboard.setRequest(request);
			dashboard.sortBy("DashBoardItems.SortNumber",0);
			dashboard.setParameter("DashBoardID", dashboardID);
			if(logger.isTraceEnabled()) {
				dashboard.doAction(GenerationKeys.SEARCH);
				logger.trace(dashboard.getStatement());
			}
			
			dashboard.getResults();
			if(dashboard.getNext()) {
				long time = logger.isDebugEnabled() ? System.currentTimeMillis() : 0L;
				logger.debug("have dashboard items");
				
				request.setAttribute("pdfdocument", document);
				/*	add this in the reports now.
				Paragraph top = new Paragraph();
				top.add(new Chunk("Produced ", FontFactory.getFont(FontFactory.HELVETICA, 8)));
				top.add(new Chunk(new Date().toString(), FontFactory.getFont(FontFactory.HELVETICA, 8)));
				top.setAlignment(Paragraph.ALIGN_RIGHT);
				document.add(top);
				*/
				boolean first = false;
				//int page = 0, lastpage = 0;
				do {
					//page = document.getPageNumber();
					if(logger.isDebugEnabled()) logger.debug("adding dashboard item for type {} and bean {} and subtype {}", new String[]{dashboard.getData("Type"), dashboard.getData("Name"), dashboard.getData("Subtype")});
					if("Report".equals(dashboard.getData("Type"))) {
						if(first) first = false;
						else {
							document.setPageSize(PageSize.A4.rotate());
							document.newPage();
						}
						/*
						if(page == lastpage)	//if we're on the same page as the previous report, then skip to a new page.
						{
							logger.debug("Adding new page as current page {} equals lastpage {}", page, lastpage);
							document.newPage();
						}
						*/
						//lastpage = document.getPageNumber();
						request.setAttribute("report",null);
						request.setAttribute("-dontshowchart", "Table".equals(dashboard.getData("Subtype")) ? "true" : "false");
						request.setAttribute("-dontshowtable", "Chart".equals(dashboard.getData("Subtype")) ? "true" : "false");
						request.getRequestDispatcher("/report.pdf?-beanID=" + dashboard.getData("BeanID")).include(request, response);
					} else {
						logger.error("Skipping type {} as not yet implemented", dashboard.getData("Type"));
						//other list types not yet implemented.
						/* 
						Paragraph top = new Paragraph();
						top.add(new Chunk("Error - Dashboard was not found - ", FontFactory.getFont(FontFactory.HELVETICA, 12)));
						top.add(new Chunk(new Date().toString(), FontFactory.getFont(FontFactory.HELVETICA, 8)));
						document.add(top);
						*/
					}
					
				} while(dashboard.getNext());
				
				if(logger.isDebugEnabled()) logger.debug("Finished dashboard items in {} ms", System.currentTimeMillis() - time);
			} else {
				Paragraph top = new Paragraph();
				top.add(new Chunk("Error - Dashboard was not found - ", FontFactory.getFont(FontFactory.HELVETICA, 12)));
				top.add(new Chunk((currentuser != null ? currentuser.getCurrentDateString() : new Date().toString()), FontFactory.getFont(FontFactory.HELVETICA, 8)));
				document.add(top);
				return;
			}
		} catch (DocumentException de) {
			throw new RuntimeException(de);
		} finally {
			logger.debug("Closing dashboard");
			dashboard.close();
			if(document != null) {
				logger.debug("Closing document");
				document.close();
			}
		}
	}
}
