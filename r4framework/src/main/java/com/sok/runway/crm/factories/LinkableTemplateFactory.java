package com.sok.runway.crm.factories;

/**
 * Linkable Template Factory
 * Modelled on Linkable Document Factory 
 * @author Michael Dekmetzian (H - 28D)
 * @date 2009-08-24
 */
import com.sok.runway.crm.Contact;
import com.sok.runway.crm.LinkableTemplate; 
import com.sok.runway.crm.LinkedTemplate;
import com.sok.runway.crm.interfaces.TemplateLinkable; 

import com.sok.framework.ActionBean; 
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys; 

import javax.servlet.ServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map; 

public class LinkableTemplateFactory {
   
   public static TemplateLinkable getTemplateLinkable(ServletRequest request, String entityName, String pk) {
      
      TemplateLinkable linkable = null;
      
      if (Contact.ENTITY_NAME.equals(entityName)) {
         linkable = new Contact(request);
         linkable.load(pk);
      }  
      
      if (linkable != null && linkable.isLoaded()) {
         return linkable;
      }
      else {
         return null;
      }
   }
   
   public static Collection<String> getLinkableTemplateIDs(TemplateLinkable docLinkable, Map<String,String> filterParameters) {
      
      ArrayList<String> docIDs = new ArrayList<String>();
      
      if (docLinkable.isLoaded()) {
   		GenRow documents = new GenRow();
   		documents.setConnection(docLinkable.getConnection());
   		documents.setViewSpec("LinkedTemplateView");
   		documents.setParameter(docLinkable.getPrimaryKeyName(), docLinkable.getPrimaryKey());
   			
   		documents.sortBy("LinkedTemplates.TemplateName",0);
   		
   		if (filterParameters != null) {
   		   for (String key : filterParameters.keySet() )
   		   documents.setParameter(key, filterParameters.get(key));
   		}
   		documents.setParameter(GenerationKeys._idonly, GenerationKeys._true);
   		
   		docLinkable.getCurrentUser().setSearchConstraint(documents,"LinkedTemplateTemplate.TemplateGroup");
   		documents.doAction(ActionBean.SEARCH);
   		documents.getResults();
   		while (documents.getNext()) {
   		   docIDs.add(documents.getData("LinkedTemplateID"));
   		}
		}
		return docIDs;
   }
   
   public static boolean linkTemplate(TemplateLinkable templateLinkable, String templateID) {
      if (templateLinkable.isPersisted()) {
         LinkableTemplate linkedTemplate = templateLinkable.getLinkableTemplate();
         
         String[] fields = {"TemplateID","ContactID"}; 
         String[] values = {templateID, templateLinkable.getPrimaryKey()}; 
         linkedTemplate.loadFromField(fields,values);
         if(!linkedTemplate.isLoaded()) { 
	         linkedTemplate.setNewPrimaryKey();
	         linkedTemplate.setField(templateLinkable.getPrimaryKeyName(), templateLinkable.getPrimaryKey());
	         linkedTemplate.setTemplateID(templateID);
	         linkedTemplate.insert();
	         return true;
         } 
      }
      return false;
   }
   public static LinkableTemplate getLinkableTemplate(TemplateLinkable templateLinkable) { 
	   LinkableTemplate template = new LinkedTemplate(templateLinkable.getConnection()); 
	   template.setCurrentUser(templateLinkable.getCurrentUser());
	   return template; 
   }
}