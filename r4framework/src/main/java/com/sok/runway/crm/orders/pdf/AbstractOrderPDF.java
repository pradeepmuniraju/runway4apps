package com.sok.runway.crm.orders.pdf;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

public abstract class AbstractOrderPDF {
	
	public abstract void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;

}
