package com.sok.runway.crm.cms.properties;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

public class PackageCost extends PropertyEntity {

	final PropertyType propertyType = PropertyType.PackageCost;
	static final String VIEWSPEC = "ProductListView";

	public PackageCost(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public PackageCost(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	public PackageCost(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
}
