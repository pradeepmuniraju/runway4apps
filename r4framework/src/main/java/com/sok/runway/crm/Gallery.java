package com.sok.runway.crm;

import java.sql.Connection;

import com.sok.runway.Validator;
import com.sok.runway.security.AuditEntity;

public class Gallery extends AuditEntity {

	public Gallery(Connection con, String galleryID) {
	      super(con, "GalleryView");
	      load(galleryID);
	   }
	
	public String getName() {
		return getField("GalleryName");
	}
	
	@Override
	public Validator getValidator() {
		Validator val = Validator.getValidator("Gallery");
	      if (val == null) {
	         val = new Validator();
	         val.addMandatoryField("GalleryID","SYSTEM ERROR - GalleryID");
	         val.addMandatoryField("Name","Name must not be blank.");

	         Validator.addValidator("Gallery", val);
	      }
	      return val;
	}

}
