/* ========================================================================= **
 o FILE TYPE        : Java Source File - text/java
 o FILE NAME        : Response.java
 o DESIGNER(S)      : 
 o CODER(S)         : 
 o VERSION          : 1.0
 o LAST MODIFIED BY : 
 o LAST MODIFIED ON : 
** ========================================================================= */

/* ======================= ** PACKAGE DECLARATION ** ======================= */
package com.sok.runway.crm;
/* ========================================================================= */

/* ======================== ** IMPORT STATEMENTS ** ======================== */
import com.sok.framework.*;
import com.sok.runway.*;
import com.sok.runway.security.*;
import javax.servlet.ServletRequest;
import java.sql.Connection;
import java.util.*;
/* ========================================================================= */

/* ========================= ** CLASS COMMENTS ** ========================== */
/**
 * Class to hold information about a survey/profile
 * 
 * @author Irwan
 * @version 0.1
 *
 */
/* ========================================================================= */

/* ====================== ** CLASS DEFINITION ** =========================== */
public class Response extends SecuredEntity {
   
	public Response(Connection con) {
		
		super(con, "ResponseView");
	}
	
	public Response(ServletRequest request) {
	    
		super(request, "ResponseView");
		populateFromRequest(request.getParameterMap());
	}
   
	public Response(Connection con, String responseID) {
	   	
		this(con);
	   load(responseID);
	}
        
	public Validator getValidator() {
		
		Validator val = Validator.getValidator("Response");
		
	   if (val == null) {
		   val = new Validator();
		   val.addMandatoryField("ResponseID","SYSTEM ERROR - ResponseID");
		   val.addMandatoryField("GroupID","Response must have a security group set");
		   val.addMandatoryField("Description","Response must have a description");
		   Validator.addValidator("Response", val);
	   }
	   
	   return val;
	}
}