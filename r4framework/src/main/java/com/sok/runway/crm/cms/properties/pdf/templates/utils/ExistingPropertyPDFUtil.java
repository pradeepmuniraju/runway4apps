package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import java.net.URI;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.CommonUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil.Estate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil.Home;
import com.sok.runway.crm.cms.properties.pdf.templates.model.Package;
import com.sok.service.crm.cms.properties.PlanService;

public class ExistingPropertyPDFUtil extends CommonUtil implements PDFConstants {

	public static final String website = "www.runway.com.au";
	public static final BaseColor aqua =  new BaseColor(72, 71, 88);  // Runway Grey ==>  696969 ==>  R: 72   G: 71   B: 88 
	public static final String MAIN_LOGO_IMG = "/specific/images/largelogo.jpg";
	public static String DISCLAIMER_TEXT = "";

	public static final BaseColor pkgHeaderGrey = new BaseColor(105, 105, 105);  // Runway Dark Grey ==>  D3D3D3 ==>  R: 105   G: 105   B: 105

	public static final Font detailsFontBlue = FontFactory.getFont(arialFont, fontRatio * 12, aqua);
	public static final Font detailsFontBlack = FontFactory.getFont(arialFont, fontRatio * 16, BaseColor.BLACK);
	public static final Font detailsFontSmall = FontFactory.getFont(arialFont, fontRatio * 9, BaseColor.BLACK);
	public static final Font headerFont = FontFactory.getFont(arialFont, fontRatio * 38, BaseColor.WHITE);
	public static final Font priceFont = FontFactory.getFont(arialFont, fontRatio * 25, Font.BOLD, BaseColor.WHITE);
	public static final Font packageHeaderFont1 = FontFactory.getFont(arialFont, fontRatio * 23, Font.BOLD, BaseColor.WHITE);
	public static final Font packageHeaderFont2 = FontFactory.getFont(arialFont, fontRatio * 16, Font.BOLD, BaseColor.WHITE);

	public static final Font homeRangeFont = FontFactory.getFont(arialFont, fontRatio * 14, BaseColor.BLACK);
	public static final Font homeNameFont = FontFactory.getFont(arialFont, fontRatio * 27, Font.BOLD, aqua);
	public static final Font homeDetailsFont = FontFactory.getFont(arialFont, fontRatio * 12, BaseColor.BLACK);
	public static final Font homePriceFont = FontFactory.getFont(arialFont, fontRatio * 13, BaseColor.BLACK);

	public static final Font regularFont = FontFactory.getFont(arialFont, fontRatio * 13, BaseColor.BLACK);
	public static final Font smallFont = FontFactory.getFont(arialFont, fontRatio * 6, BaseColor.BLACK);
	public static final Font s1 = FontFactory.getFont(arialFont, fontRatio * 4, BaseColor.BLACK);

	public static final DecimalFormat AREA_FMT = new DecimalFormat("#.##m2");

	public static Paragraph getArea(String prefix, String suffix, String s) {
		return getArea(prefix, suffix, s, detailsFontBlack, detailsFontSmall);
	}

	public static Paragraph getArea(String prefix, String suffix, String s, Font font, Font smallFont) {
		Paragraph p = new Paragraph();
		p.add(new Chunk(prefix, font));
		p.add(new Chunk(s, font));
		p.add(new Chunk("m", font));
		Chunk c = new Chunk("2", smallFont);
		c.setTextRise(4.0f);
		p.add(c);
		p.add(new Chunk(suffix, font));
		return (p);
	}

	public static class Estate {
		public String estateName, estateLogo;
		public java.util.List<Package> packages = new ArrayList<Package>();
		public HashMap<String, Land> lands = new HashMap<String, Land>();
	}

	public static class Land {
		public String name, landPrice, estateName, suburb, titleDate;
		public double lotSize;
		public java.util.List<Package> packages = new ArrayList<Package>();
	}

	public static class PackageNameComparator implements Comparator<Package> {
		public int compare(Package p1, Package p2) {
			return p1.name.compareTo(p2.name);
		}
	}

	public static class EstateComparator implements Comparator {
		public int compare(Object p1, Object p2) {
			String p1Name = ((Package) p1).estateName;
			String p2Name = ((Package) p2).estateName;
			return p1Name.compareTo(p2Name);
		}
	}


	public static Package getPackage(PlanService.ExistingProperty product) {
		final Package p = new Package();
		String lotName = product.Name;
		p.lotName = lotName;

		if (lotName != null)
			p.lotNo = lotName.toUpperCase().startsWith("LOT") ? lotName.substring(4) : lotName;

		p.filename = StringUtil.ToAlphaNumeric(product.Name);
		if (StringUtils.isBlank(p.filename)) {
			p.filename = "flyer";
		}
		
		//String stageName = product..getStageName();
		//if (stageName != null)
		//	stageName = product.getStageName().toUpperCase().startsWith("STAGE") ? product.getStageName().substring(6) : product.getStageName();

		p.heroImage = product.ImagePath; //PropertyEntity.resizeImage(product.ImagePath, 145, 87, PropertyEntity.ResizeOption.Crop, FORCE_TRUE);
		if (StringUtil.isBlankOrEmpty(p.heroImage))
		{
			p.heroImage = "";
		}	
		
		//p.estateName = capitalizeFirstLetters(product.getEstateName());
		//p.estateLogo = product.getEstateImage();
		//p.range = Range.fromString(product.getRangeName());
		//p.rangeName = product.getRangeName();
		
		p.name = product.Name;
		//p.price = product.TotalCost + "";
        p.price = getFormattedAmount(product.TotalCost);

        p.projectName = product.Project;
		
		setProductDetails(product.ProductID, p);
		
		//p.lotSize = product.getLotSize();
		//p.stageName = stageName;
		
		if (product.Address != null)
		{	
			p.street			= product.Address.Street;
			p.street2		= product.Address.Street2;
			p.suburb		= product.Address.City;
			p.state			= product.Address.State;
			p.postCode	= product.Address.Postcode;
		}
		
		return p;
	}

	public static String getFormattedAmount(double totalCost)
	{
		try
		{
	        NumberFormat formatter = new DecimalFormat("###,##0");
	        String strTotalCost = "$"+formatter.format(totalCost);
	        return strTotalCost;
		}
		catch(Exception ee)
		{
			
		}
		return "$";

	}
	
	public static void setProductDetails(String prodID, Package p)
	{
			GenRow details = null;

			details = new GenRow();
			details.setTableSpec("properties/ProductDetails");
			//details.setRequest(request);
			details.setParameter("-select","*");
			details.setColumn("ProductID", prodID);
			details.sortBy("SortOrder",0);
			details.sortBy("DetailsType", 1);

			if (!StringUtil.isBlankOrEmpty(prodID))
			{			
				details.doAction("search");
				details.getResults(true);
			}
			
			Map<String, Object> detailMap = new HashMap<String, Object>();
			
			String tmpKey = "";
			while(details.getNext()) { 
				String type = details.getColumn("DetailsType").replaceAll(" ","");

				for(int i=0; i<details.getTableSpec().getFieldsLength(); i++) { 
					String field = details.getTableSpec().getFieldName(i);
					Object o = details.getObject(field);
					if(o != null) { 
						
						tmpKey = (type+"."+field).trim();
						detailMap.put(tmpKey, String.valueOf(o));
						//out.println("<!--  key:" + tmpKey + ",value:" +  String.valueOf(o) + "-->");
						//out.println("<!-- " + String.format("adding %1$s = %2$s",field, String.valueOf(o)) + "-->");
					}
				}
			}
			details.close();

			
			String strGarage = "0";
			if (detailMap.get("Garage.NumberOf") != null)
				strGarage = detailMap.get("Garage.NumberOf").toString();
			p.carParks = new Double(strGarage).intValue();

			String strBedrooms = "0";
			if (detailMap.get("Bedrooms.NumberOf") != null)
				strBedrooms = detailMap.get("Bedrooms.NumberOf").toString();
			p.bedrooms = new Double(strBedrooms).intValue();

			String strBathrooms = "0";
			if (detailMap.get("Bathrooms.NumberOf") != null)
				strBathrooms = detailMap.get("Bathrooms.NumberOf").toString();
			p.bathrooms = new Double(strBathrooms).intValue();
			p.bathroomsAsString = strBathrooms.replace(".0", "");
	}	
	public static PdfPTable getHeader() throws DocumentException {
		PdfPTable headerTable = new PdfPTable(1);
		headerTable.setWidths(new float[] { 100 });
		headerTable.setWidthPercentage(100f);
		headerTable.getDefaultCell().setPadding(0);
		headerTable.getDefaultCell().setPaddingBottom(11);
		headerTable.getDefaultCell().setPaddingLeft(0);
		headerTable.getDefaultCell().setBorder(0);
		
		PdfPCell cell = new PdfPCell();
		if (MAIN_LOGO_IMG != null) {
			cell = new PdfPCell();
			
			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			String imgStr = homePath + "/" + MAIN_LOGO_IMG;
			try
			{
				Image img = CommonUtil.createImage(imgStr);;
				img.scaleToFit(180, 100);
				cell = new PdfPCell(img, true);
			}
			catch(Exception ee)
			{
				cell = new PdfPCell(new Paragraph("Problem with the Logo. " + String.valueOf(MAIN_LOGO_IMG), regularFont));
				System.out.println("Exception Occurred. Message is: " + ee.getMessage());
				ee.printStackTrace();
			}		
		}
		
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setPaddingLeft(10);
		cell.setBorder(0);
		cell.setPaddingTop(10);
		cell.setPaddingBottom(5);
		cell.setPaddingRight(5);
		headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		headerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		headerTable.addCell(cell);

		return headerTable;
	}
	
	public static PdfPTable buildPackageTable(Package p) throws DocumentException {
		PdfPTable packageRow = new PdfPTable(2);
		packageRow.setWidths(new int[] { 25, 75 });
		packageRow.getDefaultCell().setBorder(0);
		packageRow.getDefaultCell().setPadding(0);
		packageRow.setWidthPercentage(100f);

		Image i = null;
		PdfPCell cell = null;

		String leftImage = p.heroImage; //PropertyEntity.resizeImage(p.heroImage, 140, 70, PropertyEntity.ResizeOption.Pad, true, true, );

		/*if (leftImage != null && (i = createImage(leftImage)) != null) {
			cell = new PdfPCell(i, true);
				
		} else {
			cell = new PdfPCell(new Paragraph("Problem with image" + leftImage, regularFont));
		}*/
		
		String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
		try
		{
			String imgStr = homePath + "/" + p.heroImage;
			if(StringUtils.isBlank(p.heroImage))
			imgStr = homePath + "/" + MAIN_LOGO_IMG;
			Image img = CommonUtil.createImage(imgStr);
			img.scaleToFit(140, 80);
			cell = new PdfPCell(img, true);
		}
		catch(Exception ee)
		{
				cell = new PdfPCell(new Paragraph("Sorry, the image is not available " + leftImage, regularFont));
				System.out.println("Exception Occurred. Message is: " + ee.getMessage());
				ee.printStackTrace();
		}
		
		

		cell.setBorder(0);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setPadding(0);
		//cell.setBorderWidthLeft(0.1f);
		//cell.setBorderWidthRight(0.1f);
	

		packageRow.addCell(cell);
		packageRow.addCell(buildDetailsTable(p));
		return packageRow;
	}

	private static PdfPTable buildDetailsTable(Package p) throws DocumentException {
		PdfPTable packageTable = new PdfPTable(5);
		packageTable.setWidths(new int[] { 2, 15, 30, 15, 15 });
		packageTable.setWidthPercentage(100f);

		packageTable.getDefaultCell().setPadding(0);
		packageTable.getDefaultCell().setColspan(3);
		packageTable.getDefaultCell().setPaddingLeft(15f);


		packageTable.getDefaultCell().setBorder(0);
		packageTable.getDefaultCell().setPaddingBottom(2f);
		packageTable.getDefaultCell().setFixedHeight(22.5f);
		packageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.getDefaultCell().setBackgroundColor(pkgHeaderGrey);

		String tmpStreet = p.street;
		if (!StringUtil.isBlankOrEmpty(p.street2))
		{
			tmpStreet += " " + p.street2;
		}	
		
		packageTable.addCell(new Paragraph(tmpStreet, packageHeaderFont1));
		packageTable.getDefaultCell().setPaddingLeft(60f);

		packageTable.getDefaultCell().setColspan(2);
		packageTable.addCell(new Paragraph(String.valueOf(p.price), priceFont));

		packageTable.getDefaultCell().setFixedHeight(0f);
		packageTable.getDefaultCell().setColspan(1);
		packageTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		packageTable.getDefaultCell().setPadding(1);
		packageTable.getDefaultCell().setPaddingTop(2);
		packageTable.getDefaultCell().setPaddingLeft(3);
		packageTable.getDefaultCell().setPaddingRight(3);
		packageTable.getDefaultCell().setPaddingBottom(1);
		packageTable.getDefaultCell().setNoWrap(true);

		// Package Details
		// First Row
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("SUBURB: ", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(p.suburb, detailsFontBlack));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("BED: ", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(String.valueOf(p.bedrooms), detailsFontBlack));

		packageTable.getDefaultCell().setPaddingLeft(3);
		packageTable.getDefaultCell().setPaddingRight(3);

		// Second Row
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("STATE: ", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(String.valueOf(p.state), detailsFontBlack));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("BATH: ", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(String.valueOf(p.bathroomsAsString), detailsFontBlack));

		// Third Row
		packageTable.getDefaultCell().setColspan(1);
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);

		packageTable.addCell(new Paragraph("POSTCODE: ", detailsFontBlue));
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(p.postCode, detailsFontBlack));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		packageTable.addCell(new Paragraph("GARAGE: ", detailsFontBlue));

		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(String.valueOf(p.carParks), detailsFontBlack));

		// Fourth Row
		packageTable.getDefaultCell().setColspan(1);
		packageTable.addCell(BLANK);
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		packageTable.addCell(new Paragraph("PROJECT NAME: ", detailsFontBlue));
		packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		packageTable.addCell(new Paragraph(p.projectName, detailsFontBlack));
		
		packageTable.addCell(BLANK);
		packageTable.addCell(BLANK);

		return packageTable;
	}
}