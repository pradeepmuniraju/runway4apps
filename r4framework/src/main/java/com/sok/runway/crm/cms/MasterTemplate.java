package com.sok.runway.crm.cms;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.TableBean;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.ErrorMap;
import com.sok.runway.TemplateURLs;
import com.sok.runway.Validator;
import com.sok.runway.crm.LinkableDocument;
import com.sok.runway.crm.LinkedDocument;
import com.sok.runway.crm.factories.LinkableDocumentFactory;
import com.sok.runway.crm.interfaces.DocumentLinkable;
import com.sok.runway.crm.util.LegacyUploader.TemplateLink;
import com.sok.runway.crm.util.LegacyUploader;
import com.sok.runway.crm.util.TemplateUploader;
import com.sok.runway.security.SecuredEntity;

public class MasterTemplate extends SecuredEntity implements DocumentLinkable {
	public static final String ENTITY_NAME = "MasterTemplate";

	/**
	 * @deprecated
	 */
	private static final String LINK_TOKEN_PREFIX = "GenLink";

	private TemplateUploader templateUploader = null;

	public MasterTemplate(Connection con) {
		super(con, "MasterTemplateView");
	}

	public MasterTemplate(ServletRequest request) {
		super(request, "MasterTemplateView");
		populateFromRequest(request.getParameterMap());
	}

	public MasterTemplate(Connection con, String contactID) {
		this(con);
		load(contactID);
	}

	public String getEntityName() {
		return ENTITY_NAME;
	}

	/**
	 * Deletes Child Data of MasterTemplates after RunwayEntity has removed the
	 * master template.
	 * 
	 */
	public boolean delete() {
		if (super.delete()) {
			GenRow del = new GenRow();
			del.setConnection(getConnection());
			del.setTableSpec("TemplateURLs");
			del.setParameter(GenerationKeys.UPDATEON + getPrimaryKeyName(),
					getPrimaryKey());
			del.setAction(com.sok.framework.generation.GenerationKeys.DELETEALL);
			del.doAction();

			del.setTableSpec("LinkedDocuments");
			del.doAction();

			return true;
		} else {
			return false;
		}
	}

	public Validator getValidator() {
		Validator val = Validator.getValidator("MasterTemplate");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("MasterTemplateID",
					"SYSTEM ERROR - MasterTemplateID");
			val.addMandatoryField("GroupID",
					"Master Template must have a security group selected");
			val.addMandatoryField("Name", "Master Template must have a Name");
			val.addMandatoryField("Type", "Master Template must have a Type");
			Validator.addValidator("MasterTemplate", val);
		}
		return val;
	}

	/**
	 * Linkable Interface
	 */

	public String getLinkableTitle() {
		return getField("Name");
	}

	/**
	 * DocumentLinkable Interface
	 */
	public Collection<String> getLinkableDocumentIDs() {
		return getLinkableDocumentIDs(null);
	}

	/**
	 * DocumentLinkable Interface
	 */
	public Collection<String> getLinkableDocumentIDs(
			Map<String, String> filterParameters) {
		return LinkableDocumentFactory.getLinkableDocumentIDs(this,
				filterParameters);
	}

	/**
	 * DocumentLinkable Interface
	 */
	public Collection<String> getDocumentIDs() {
		return getDocumentIDs(null);
	}

	/**
	 * DocumentLinkable Interface
	 */
	public Collection<String> getDocumentIDs(
			Map<String, String> filterParameters) {
		return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
	}

	/**
	 * DocumentLinkable Interface
	 */
	public LinkableDocument getLinkableDocument() {
		LinkableDocument doc = new LinkedDocument(getConnection());
		doc.setCurrentUser(getCurrentUser());
		return doc;
	}

	/**
	 * DocumentLinkable Interface
	 */
	public void linkDocument(String documentID) {
		LinkableDocumentFactory.linkDocument(this, documentID);
	}

	public TemplateUploader getUploader() {
		return templateUploader;
	}

	public void upload(String filePath, ServletContext sctx) {

		File zipFile = new File(sctx.getRealPath("") + "/" + filePath);
		if (!zipFile.exists()) {
			throw new RuntimeException(" file did not exist @ "
					+ zipFile.getAbsolutePath());
		}

		templateUploader = new TemplateUploader(zipFile, sctx);
	}

	public void upload(TableBean docBean, ServletContext sctx) {
		upload(docBean.getString("FilePath"), sctx);
	}

	public void what() {

	}

	public void loadTemplateURLs(ServletContext servletContext) {
		TemplateURLs templateUrls = new TemplateURLs();
		templateUrls.getMasterLinks(this.getField("MasterTemplateID"),
				servletContext);
		templateUploader.setTemplateURLs(templateUrls);
		templateUploader.processTemplateURLs();
	}
	
	protected String fixBody(String text) {
		while (text.length() > 0 && text.toLowerCase().indexOf("<div class=\"bodyarea\">") != -1) {
			int pos = text.toLowerCase().indexOf("<div class=\"bodyarea\">");
			text = text.substring(0,pos) + text.substring(pos + "<div class=\"bodyarea\">".length());
			if (text.toLowerCase().lastIndexOf("</div>") != -1) {
				pos = text.toLowerCase().lastIndexOf("</div>");
				text = text.substring(0,pos) + text.substring(pos + "</div>".length());
			} 
		}
		while (text.length() > 0 && text.toLowerCase().indexOf("<div class=\"bodyarea content\">") != -1) {
			int pos = text.toLowerCase().indexOf("<div class=\"bodyarea\">");
			text = text.substring(0,pos) + text.substring(pos + "<div class=\"bodyarea content\">".length());
			if (text.toLowerCase().lastIndexOf("</div>") != -1) {
				pos = text.toLowerCase().lastIndexOf("</div>");
				text = text.substring(0,pos) + text.substring(pos + "</div>".length());
			} 
		}
		return text;
	}
	public ErrorMap saveTemplateURLs(HttpServletRequest request) {
		List<TemplateLink> updated = templateUploader.getUpdatedLinks();
		for(TemplateLink link : updated)
		{
			String linktype = link.getLinkType(); 

			if ("Standard".equals(linktype)
					|| "Document".equals(linktype)
					|| "SendToAFriend".equals(linktype)) {
				saveTemplateURL(request, link);
			}
		}
		templateUploader.renderHTML(false);
		// templateUploader.setBodyHTML(request.getParameter("-bodyHTML"));

		/* save master template */
		this.setField("DefaultBody", templateUploader.getBodyHTML());
		this.setField("HTMLHeader", templateUploader.getHeaderHTML());
		this.setField("HTMLFooter", templateUploader.getFooterHTML());

		ErrorMap e = null;
		if (this.isLoaded() || this.isPersisted()) {
			e = this.update();
		} else {
			e =  this.insert();
		}
		/* if we need to load the html again, do it 
		 * after we get the content */
		if (request.getParameter("-load") != null
				&& !"".equals(request.getParameter("-load"))
				&& !"null".equals(request.getParameter("-load"))) {
			templateUploader.renderHTML(true);
		} 
		
		return e;
	}

	private void saveTemplateURL(HttpServletRequest request, TemplateLink link) {
		final String masterTemplateID = this.getPrimaryKey();
		GenRow tuv = new GenRow();
		tuv.setRequest(request);
		tuv.setViewSpec("TemplateURLView");
		tuv.clear();
		tuv.setParameter("-select1", "TemplateURLID");
		tuv.setParameter("MasterTemplateID", masterTemplateID);
		// tuv.setParameter("LinkIndex", "" + linkIndex);
		
		String hr = link.getLinkName();
		tuv.setParameter("Name", hr);
		
		tuv.doAction("search"); 
		System.out.println(tuv.getSearchStatement());
		tuv.doAction(GenerationKeys.SELECTFIRST);

		if (!tuv.getString("TemplateURLID").equals("") /* borken? tuv.isSuccessful() */) {
			System.out.println("found, updating"); 
			// TemplateURL exists, so update
			tuv.setParameter("ON-TemplateURLID", tuv.getString("TemplateURLID"));
			tuv.setAction(GenerationKeys.UPDATE);
		} else {
			// otherwise create new TemplateURL
			System.out.println("not found, inserting"); 
			tuv.clear();
			tuv.setToNewID("TemplateURLID");
			tuv.setParameter("MasterTemplateID", masterTemplateID);
			tuv.setAction(GenerationKeys.INSERT);
		}
		tuv.setParameter("Name", link.getLinkName());
		tuv.setParameter("Response", link.getLinkResponse());
		tuv.setParameter("LinkText", link.getLinkText());
		tuv.setParameter("URL", link.getLinkUrl());
		tuv.setParameter("Target",link.getLinkTarget());
		tuv.setParameter("LinkTypeDescription", link.getLinkType());
		tuv.setParameter("LinkType", "T"); /* we're not doing image links for this anymore */
		tuv.doAction();
	}

	public void renderHTML(boolean forEdit) {

		if (templateUploader == null) {
			if (!isLoaded()) {
				throw new RuntimeException("Load Fail - mt must be loaded");
			}

			// TemplateURLs tu = new TemplateURLs();
			// tu.getTemplateLinks(getPrimaryKey(), scontext)

			templateUploader = new TemplateUploader();

			StringBuilder html = new StringBuilder();
			html.append(getField("HTMLHeader"));
			
			html.append(TemplateUploader.TEMPLATE_START);
			if (getField("HTMLHeader").indexOf("bodyArea") == -1) {
				html.append("<div class=\"bodyArea\">");
				if (!fixBody(getField("DefaultBody")).trim().startsWith("<div")) html.append("<div>"); 
			}
			html.append(fixBody(getField("DefaultBody")));
			if (getField("HTMLHeader").indexOf("bodyArea") == -1) {
				if (!fixBody(getField("DefaultBody")).trim().startsWith("<div")) html.append("</div>"); 
				html.append("</div>");
			}
			html.append(TemplateUploader.TEMPLATE_END);
			html.append(ActionBean._space).append(getField("HTMLFooter"));

			templateUploader.parseText(html);
			templateUploader.setBodyHTML(getField("Body"));

		} else {
			StringBuilder html = new StringBuilder();
			html.append(getField("HTMLHeader"));
			html.append("<span style=\"display: none;\">TEMPLATE_START</span>");
			if (getField("HTMLHeader").indexOf("bodyArea") == -1) {
				html.append("<div class=\"bodyArea\">");
				if (!fixBody(getField("DefaultBody")).trim().startsWith("<div")) html.append("<div>"); 
			}
			
			html.append(fixBody(getField("DefaultBody")));
			if (getField("HTMLHeader").indexOf("bodyArea") == -1) {
				if (!fixBody(getField("DefaultBody")).trim().startsWith("<div")) html.append("</div>"); 
				html.append("</div>");
			}
			html.append("<span style=\"display: none;\">TEMPLATE_END</span>");
			html.append(ActionBean._space).append(getField("HTMLFooter"));

			templateUploader.parseText(html);
			templateUploader.setBodyHTML(fixBody(getField("Body")));
		}

		templateUploader.renderHTML(forEdit);
	}

	public boolean hasBody() {
		boolean hasBody = false;

		if (getField("DefaultBody") != null
				&& getField("DefaultBody").length() > 0) {
			hasBody = true;
		}

		return hasBody;
	}
}