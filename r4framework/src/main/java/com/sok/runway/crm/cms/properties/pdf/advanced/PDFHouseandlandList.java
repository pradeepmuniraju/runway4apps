package com.sok.runway.crm.cms.properties.pdf.advanced;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sog.pdfserver.client.api.request.PDFRequest;

public class PDFHouseandlandList extends PDFProductList {

	public static final Logger logger = LoggerFactory.getLogger(PDFHouseandlandList.class);

	@Override
	public void populatePDFRequest(HttpServletRequest request, HttpServletResponse response, PDFRequest pdfRequest) {
		super.populatePDFRequest(request, response, pdfRequest);

	}

}