package com.sok.runway.crm;
/**
 * LinkedTemplate
 * Modelled on LinkedTemplate
 * @author Michael Dekmetzian (H - 28D)
 * @date 2009-08-24
 */
import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;


public class LinkedTemplate extends LinkableTemplate {

public static final String LINKTEMPLATE_VIEWSPEC = "LinkedTemplateView";
   
   public LinkedTemplate(Connection con) {
		super(con, LINKTEMPLATE_VIEWSPEC);
	}
		
	public LinkedTemplate(ServletRequest request) {
		super(request, LINKTEMPLATE_VIEWSPEC);
		populateFromRequest(request.getParameterMap());
	}
	   
	public LinkedTemplate(Connection con, String linkedTemplateID) {
		this(con);
	   load(linkedTemplateID);
	}
   
   public Validator getValidator() {
		Validator val = Validator.getValidator("LinkedTemplates");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("LinkedTemplateID","SYSTEM ERROR - LinkedTemplateID");
			val.addMandatoryField("TemplateID","SYSTEM ERROR - TemplateID");
			Validator.addValidator("LinkedTemplates", val);
		}
		return val;
   }
}