package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;

public class DeveloperPDFUtil {

	private static final Logger logger = LoggerFactory.getLogger(DeveloperPDFUtil.class);

	public static String hasDeveloperPDF(HttpServletRequest request, String productIds) {
		String errorText = "";

		List<String> projectNames = new ArrayList<String>();

		GenRow row = new GenRow();
		row.setRequest(request);
		row.setTableSpec("properties/ProductQuickIndex");
		row.setParameter("ProductID", productIds);
		row.setParameter("-select0", "distinct DeveloperID as DeveloperID");
		row.doAction("search");
		row.getResults();

		while (row.getNext()) {

			if (StringUtils.isNotBlank(row.getString("DeveloperID"))) {
				String projectName = getProjectName(request, row.getString("DeveloperID"));

				if (StringUtils.isBlank(projectName)) {
					errorText = "Sorry, one of the developers is not configured to support developer pdf's. Contact Support";
					break;
				} else {
					projectNames.add(projectName);
				}

			} else {
				errorText = "Sorry, one of the packages is not attached to any developer.";
				break;
			}
		}

		row.close();

		// Check if jars are present
		StringBuffer jarNames = new StringBuffer(100);
		File[] jars = getJarFileNames();
		
		if(jars != null) {
			for (int i = 0; i < jars.length; i++) {
				if (jars[i].getName().endsWith("jar")) {
					jarNames.append(jars[i].getName());
					jarNames.append("+");
				}
			}
	
			for (String projectName : projectNames) {
				if (!jarNames.toString().contains(projectName)) {
					errorText = "Sorry, unable to generate a developer pdf - [jar file is missing]. Contact support";
					break;
				}
			}
		}
		return errorText;
	}

	public static String getProjectName(HttpServletRequest request, String developerID) {
		GenRow row = new GenRow();
		row.setRequest(request);
		row.setTableSpec("Products");
		row.setParameter("ProductID", developerID);
		row.setParameter("-select0", "distinct Project");
		row.sortBy("Name", 0);
		row.doAction("selectfirst");
		String projName = row.getString("Project");

		row.close();

		return projName;
	}

	public static File[] getJarFileNames() {
		String jarDirectory = InitServlet.getRealPath() + "/WEB-INF/lib/developer";
		File devJarDir = new File(jarDirectory);
		File[] jars = devJarDir.listFiles();

		return jars;
	}
}
