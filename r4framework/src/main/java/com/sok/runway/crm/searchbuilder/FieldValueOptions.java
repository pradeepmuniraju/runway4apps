package com.sok.runway.crm.searchbuilder;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.DisplaySpec;
import com.sok.runway.dao.ProfileQuestionDAO;
import com.sok.runway.model.ProfileQuestion;
import com.sok.runway.view.FormFields;
import com.sok.runway.view.ValueList;

public class FieldValueOptions {
    private HttpServletRequest request;
    private SearchCriteria searchCriteria;
    
    public FieldValueOptions(HttpServletRequest request, SearchCriteria searchCriteria) {
        this.request = request;
        this.searchCriteria = searchCriteria;
    }
    
    public Option[] getValueOptions() {
        Option[] valueOptions = null;
        String[] values = null;
        
        FieldType fieldType = this.searchCriteria.getFieldType();
        
        if(fieldType == FieldType.MODULE) {
            DisplaySpec displaySpec = this.searchCriteria.getDisplaySpec();
            String itemName = this.searchCriteria.getFieldNameValue();
            String listType = displaySpec.getItemValuesListType(itemName);
            
            if(listType != null) {
               // If there is a listtype, load the values from the list.
               values = getValuesByListName(listType);
            } else {
               // Otherwise get values from the display spec, if any
               values = displaySpec.getItemValues(itemName);
            }
        } else if(fieldType == FieldType.PROFILE) {
            String questionID = this.searchCriteria.getFieldNameValue();
            values = getQuestionAnswers(questionID);
        }

        valueOptions = new Option[values.length];
        this.populateOptions(values, valueOptions);
        
        return valueOptions;
    }
    
    public Option[] getYesNoOptions() {
        final String[] values = {"Yes", "No"};
        Option[] options = new Option[values.length];
        for(int i = 0; i < values.length; i++) {
            String value = values[i];
            Option option = new Option();
            option.setValue(value);
            option.setText(value);
            option.setTitle(value);
            if(this.searchCriteria.getFieldValue().equals(value)) {
                option.setSelected(true);
            }
            options[i] = option;
        }
        
        return options;
    }
    
    public String getFieldRelationship() {
        String relationship = this.searchCriteria.getSearchType().getName() + "." + this.searchCriteria.getItemFieldName();
        
        return relationship;
    }
     
     public String getFieldInputType() {
         String inputType = FormFields.FIELD_TEXT_TINY;
         
         if(this.searchCriteria.getFieldType() == FieldType.MODULE) {
             String itemName = this.searchCriteria.getFieldNameValue();
             DisplaySpec displaySpec = this.searchCriteria.getDisplaySpec();
             inputType = displaySpec.getItemInputType(itemName); 
         } else if(this.searchCriteria.getFieldType() == FieldType.PROFILE) {
             ProfileQuestionDAO questionDAO = new ProfileQuestionDAO();
             String questionID = this.searchCriteria.getFieldNameValue();
             ProfileQuestion question = questionDAO.findById(request, questionID);
             inputType = question.getInputType();
         }
        
         return inputType;
     }
     
     private String[] getValuesByListName(String listName) {
         String[] values = null;
         ValueList valueList = ValueList.getValueList(listName);
         
         if(valueList != null) {
            values = new String[valueList.size()];
            for(int i=0; i<valueList.size(); i++) {
               values[i] = valueList.getValue(i);
            }
         }
         return values;
     }
     
     private String[] getQuestionAnswers(String questionID) {
         String[] answers = null;
         ProfileQuestionDAO questionDAO = new ProfileQuestionDAO();
         ProfileQuestion question = questionDAO.findById(request, questionID);
         String listName = question.getValueList();
         answers = getValuesByListName(listName);
         
         return answers;
     }
     
     private void populateOptions(String[] values, Option[] options) {
         if(values != null && options != null && options.length >= values.length) {
             for(int i = 0; i < values.length; i++) {
                 String value = values[i];
                 Option option = new Option();
                 option.setValue(value);
                 option.setText(value);
                 option.setTitle(value);
                 if(this.searchCriteria.getFieldValue().equals(value)) {
                     option.setSelected(true);
                 }
                 
                 options[i] = option;
             }
         }
     }
}
