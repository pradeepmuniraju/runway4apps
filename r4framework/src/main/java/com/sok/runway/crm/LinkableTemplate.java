package com.sok.runway.crm;

import com.sok.runway.crm.cms.Template; 
import com.sok.runway.security.AuditEntity;

import javax.servlet.ServletRequest;
import java.sql.Connection;

public abstract class LinkableTemplate extends AuditEntity {
   
   private Template template = null;
   
   public LinkableTemplate(Connection con, String viewSpec) {
      super(con, viewSpec);
   }
   
   public LinkableTemplate(ServletRequest request, String viewSpec) {
      super(request, viewSpec);  
   }
   
   public String getTemplateID() {
      return getField("TemplateID");
   }
   
   public void setTemplateID(String productID) {
      setField("TemplateID", productID);
   }
   
   public Template getTemplate() {
      if (template == null) {
    	  template = new Template(getConnection());
    	  template.setCurrentUser(getCurrentUser());
      }
      if (!template.isLoaded() && getTemplateID().length() != 0) {
    	  template.load(getTemplateID());
      }
      return template;
   }
   
   public void postLoad() {
      super.postLoad();
      if (isLoaded()) {
         getTemplate();
      }
   }
   
   public void clear() {
      super.clear();
      if (template != null) {
    	  template.clear();
      }
   }
}