package com.sok.runway.crm.cms.properties.entities;

import java.sql.Connection;

import javax.servlet.ServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.Product;
import com.sok.runway.externalInterface.beans.cms.properties.HomeDetails;

@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement(name="Plan")
public class PlanEntity extends Product {

	private static final Logger logger = LoggerFactory.getLogger(PlanEntity.class);
	static final String VIEWSPEC = "properties/ProductPlanView";
	public static final String ENTITY_NAME = "Home Plan";
	
	public PlanEntity() {
		super(VIEWSPEC, (Connection)null);
	}
	public PlanEntity(Connection con) {
		super(VIEWSPEC, con);
	}
	public PlanEntity(ServletRequest request) {
		super(VIEWSPEC, request);
	}
	public PlanEntity(Connection con, String planID) {
		super(VIEWSPEC, con);
		super.setParameter("ProductType", ENTITY_NAME);
		loadFinal(planID);
	}
}
