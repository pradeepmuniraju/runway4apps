package com.sok.runway.crm.factories;

import com.sok.runway.crm.interfaces.Dialable;
import com.sok.runway.crm.interfaces.Recordable; 

import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;

public class DialerFactory {
	
   public static String ctiSuffixProp = InitServlet.getSystemParams().getProperty("CTIUrlSuffix"); 
	public static String ctiProp = InitServlet.getSystemParams().getProperty("CTIUrl"); 
	public static String ctiURL = (ctiProp != null && ctiProp.length()>0)?ctiProp:"phone://dial_"; 	
	
	public static String getRecordURL(Recordable rec, String field, int channel, String custom) {

		boolean dial = field != null && rec instanceof Dialable; 
		
		StringBuilder phone = new StringBuilder("phone://").append(dial?"dialstart_":"start_").append(channel); 
		if(dial) { 
			phone.append("_").append(cleanPhone(rec.getField(field)));
		} 
		phone.append("_").append(rec.getPrimaryKeyName()).append("=").append(rec.getPrimaryKey()); 
		phone.append("_UserID=").append(rec.getCurrentUser().getUserID()); 
		phone.append("_FN=").append(cleanName(rec.getLinkableTitle())); 
		if(custom.length()>0) { 
			phone.append("+-+").append(custom); 
		} 
		phone.append("_GroupID=").append(rec.getField("GroupID")); 
		phone.append("_WebAppID=").append(InitServlet.getSystemParams().getProperty("WebAppID")); 

		return phone.toString(); 
	}

	public static String getDialURL(Dialable rec, String field) {
		StringBuilder phone = new StringBuilder(ctiURL).append(cleanPhone(rec.getField(field))); 
		if(ctiSuffixProp!=null){
		   phone.append(ctiSuffixProp);
		}
		return phone.toString(); 
	}
	
	/*
     *  Created By : Puja Shah
     *  Created Date : 31st July 2008
     *  Purpose : To call the appropriate dialer methods bases on the value of CTIRecording- system paramter set.
    */
	public static String getPhoneURL(Dialable drec, String field, int channel, String custom){
		String phone =null;
		String callRecordingType = InitServlet.getSystemParams().getProperty("CTIRecording");
		if ("auto".equals(callRecordingType)) {
			if(custom==null)
				custom = "call+recording";
			phone = getRecordURL(drec,field,channel,custom);
		}else if ("true".equals(callRecordingType)) {
			phone = getDialURL(drec,field);
		}else{
			phone = "phone://"+cleanName(drec.getField(field));
		}
		return phone.toString();
	}
	
	private static String cleanName(String name) {
		return StringUtil.replace(StringUtil.replace(StringUtil.replace(StringUtil.replace(
		StringUtil.replace(name.toString(),"&","and")," ","+"),"_","+"),"'",""),"\"","");  
	}
	
	private static String cleanPhone(String phone) { 
		return StringUtil.replace(StringUtil.replace(StringUtil.replace(phone," ",""),"(",""),")",""); 	
	}
}
