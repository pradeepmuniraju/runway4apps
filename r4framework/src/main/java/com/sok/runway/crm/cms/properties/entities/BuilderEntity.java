package com.sok.runway.crm.cms.properties.entities;

import java.sql.Connection;
import java.util.Collections;

import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.Product;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.cms.properties.Builder;

public class BuilderEntity extends Product {
	private static final Logger logger = LoggerFactory.getLogger(BuilderEntity.class);
	static final String VIEWSPEC = "properties/ProductBuilderView";
	public static final String ENTITY_NAME = "Builder";
	
	public BuilderEntity() {
		super(VIEWSPEC, (Connection)null);
	}
	public BuilderEntity(Connection con) {
		super(VIEWSPEC, con);
	}
	public BuilderEntity(ServletRequest request) {
		super(VIEWSPEC, request);
	}
	public BuilderEntity(Connection con, String builderID) {
		super(VIEWSPEC, con);
		loadFinal(builderID);
	}
	public BuilderEntity(Connection con, Builder e) {
		super(VIEWSPEC, con);
		
		if(StringUtils.isNotBlank(e.getBuilderID()))
			loadFinal(e.getBuilderID());
		
		setField("Name", e.getName());
		setField("Description", e.getDescription());
		setField("ProductNum", e.getProductNum());
		setField("URL", e.getURL());
		BeanFactory.setTableDataFromAddress(e.getAddress(), getRecord(), "");
		setField("ThumbnailImage", e.getThumbnailImage() != null ? e.getThumbnailImage().getDocumentID() : null);
	}
	
	@Override
	public String getEntityName() {
		return ENTITY_NAME;
	}
}
