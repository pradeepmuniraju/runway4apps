package com.sok.runway.crm.cms.properties.excel.templates;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.RunwayUtil;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.excel.AbstractExcelTemplate;
import com.sok.runway.crm.cms.properties.excel.templates.model.Estate;
import com.sok.runway.crm.cms.properties.excel.templates.model.Package;

public class PackageReport extends AbstractExcelTemplate {
	private static final Logger log = LoggerFactory.getLogger(PackageReport.class);

	public final String siteCostsPackageCostCategory = "Site Costs+OH%+Council Requirements";
	public final String councilRequirementsPackageCostCategory = "Council Requirements";
	public final String bushFirePackageCostName = "V - Bushfire BAL12.5";
	public final String developerGuidelinesCategory = "Developer Guildlines";

	public CellStyle defaultHeaderStyle = null;
	public CellStyle defaultDataRowStyle = null;
	public CellStyle defaultDataRowCurrenyStyle = null;
	HSSFWorkbook workbook = null;
	public DataFormat format = null;

	private String errorMessage = "";

	private boolean hasPortalTask = false;
	
	private String portalUserID = "";
	
	private SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy");

	public String renderExcel(HttpServletRequest request, HttpServletResponse response, HSSFWorkbook inworkbook) throws IOException {
		workbook = inworkbook;

		hasPortalTask = "true".equals(request.getParameter("hasPortalFilters"));
		
		portalUserID = request.getParameter("PortalContactID");


		HashMap<String, Estate> estates = getRequestedPackageData(request);

		configureDefaultStyles(workbook);

		String path = getPreformattedExcelFile(request);

		generateReport(workbook, path, estates);

		return errorMessage;
	}

	private void configureDefaultStyles(HSSFWorkbook workbook) {
		format = workbook.createDataFormat();

		defaultHeaderStyle = workbook.createCellStyle();
		defaultHeaderStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
		defaultHeaderStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		defaultDataRowStyle = workbook.createCellStyle();
		defaultDataRowStyle.setFillForegroundColor(HSSFColor.WHITE.index);
		// defaultDataRowStyle.getFont(workbook).setFontName("Calibri");

		defaultDataRowCurrenyStyle = workbook.createCellStyle();
		defaultDataRowCurrenyStyle.setFillForegroundColor(HSSFColor.WHITE.index);
		defaultDataRowCurrenyStyle.setDataFormat(format.getFormat("$#,##0"));

	}

	private HashMap<String, Estate> getRequestedPackageData(HttpServletRequest request) {
		HashMap<String, Estate> estates = new HashMap<String, Estate>();
		
		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (List<PropertyEntity>)request.getAttribute("entityList"); 
		if(properties == null) {
			setStatus(request,"Loading Records..", 0.02d);
			log.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			properties = PropertyFactory.getPropertyEntities(request, this);
		}
		
		GenRow row = new GenRow();
		row.setViewSpec("properties/PackageContactGroupView");
		row.setRequest(request);

		GenRow row2 = new GenRow();
		row2.setViewSpec("ContactGroupView");
		row2.setRequest(request);

		for (PropertyEntity pe : properties) {
			if (hasPortalTask) {
				row.clear();
				row.setParameter("PackageID", pe.getData("ProductID"));
				row.setParameter("-groupby0", "PackageContactID");
				if (StringUtils.isNotBlank(portalUserID)) row.setParameter("ContactID", portalUserID);
				row.sortBy("PackageContactGroups.SortOrder", 0);
				row.doAction("search");
				
				row.getResults();
				
				while (row.getNext()) {
					row2.clear();
					row2.setParameter("ContactID", row.getData("ContactID"));
					if(StringUtils.isNotBlank(pe.getString("GroupID"))) {
						log.debug("Product's GroupID is blank");
						row2.setParameter("GroupID", getProductGroups(request, pe.getData("ProductID"), pe.getString("GroupID")));
					} else {
						// pick group from stage
						log.debug("Picking Product's GroupID from stage");
						row2.setParameter("GroupID", getProductGroups(request, pe.getData("StageProductID"), RunwayUtil.getProduct(request, pe.getString("StageProductID")).getString("GroupID")));
					}
					row2.sortBy("SortOrder", 0);
					
					if (row2.isSet("GroupID")) row2.doAction("selectfirst");
					
					System.out.println(row2.getStatement());
					
					if (row2.isSuccessful() && row2.isSet("RepUserID"))
						processPackage(request, pe, estates, (row2.getString("RepFirstName") + " " + row2.getString("RepLastName")), (row.getString("FirstName") + " " + row.getString("LastName")), row2.getString("Name"), row.getDate("ExclusiveDate") );
					else
						processPackage(request, pe, estates, "No Rep for Group", (row.getString("FirstName") + " " + row.getString("LastName")), pe.getString("SecurityGroupName"), row.getDate("ExclusiveDate") );
				}
				
			} else {
				processPackage(request, pe, estates);
			}
		}
		
		return estates;
	}	

	private String getProductGroups(HttpServletRequest request, String productID, String groupID){
		String groupIDs = groupID;
			
		GenRow productGroup = new GenRow();
		productGroup.setRequest(request);
		productGroup.setTableSpec("ProductSecurityGroups");
		productGroup.setParameter("-select0", "GroupID");
		productGroup.setParameter("ProductID", productID);
		productGroup.setParameter("GroupID", "!NULL+!EMPTY");
		productGroup.doAction("search");
		
		productGroup.getResults();
		
		while (productGroup.getNext()) {
			if (groupIDs.length() > 0) groupIDs += "+";
			groupIDs += productGroup.getData("GroupID");
		}
		
		productGroup.close();
		
		return groupIDs;
	}

	private void processPackage(HttpServletRequest request, PropertyEntity pe, HashMap<String, Estate> estates) {
		processPackage(request, pe, estates, null, null, null, null);
	}
	
	private void processPackage(HttpServletRequest request, PropertyEntity pe, HashMap<String, Estate> estates, String salesRep, String salesAgent, String groupName, Date date) {
		Package p = getPackage(request, pe);
		
		if (StringUtils.isNotBlank(salesRep)) p.setSalesRepName(salesRep);
		if (StringUtils.isNotBlank(salesAgent)) p.setSalesAgentName(salesAgent);
		if (StringUtils.isNotBlank(groupName)) p.setGroupName(groupName);
		if (date != null) {
			p.setExclusiveDate(ddmmyyyy.format(date));
		}
		Estate e = (Estate) estates.get(p.getEstateName());
		if (e == null) {
			e = new Estate();
			e.setEstateName(p.getEstateName());
			estates.put(p.getEstateName(), e);
		}

		e.getHnlPackages().add(p);
		
		GenRow linkedproducts = new GenRow();
		linkedproducts.setRequest(request);
		linkedproducts.setViewSpec("ProductLinkedProductView");
		   
		// Package Costs - common setting
		linkedproducts.setColumn("ProductID", pe.getString("ProductID"));
		linkedproducts.setColumn("ProductProductsLinkedProduct.ProductType", "Package Cost");
		linkedproducts.setColumn("-select0", "Sum(ProductProductsLinkedProduct.TotalCost) AS PackageCost");

		// Site costs
		linkedproducts.setColumn("ProductProductsLinkedProduct.Category", siteCostsPackageCostCategory);
		linkedproducts.setColumn("ProductProductsLinkedProduct.Name", "!" + bushFirePackageCostName);
		linkedproducts.doAction("search");
		linkedproducts.getResults(true);
		log.trace("Query 1 Site: " + linkedproducts.getStatement());

		while (linkedproducts.getNext()) {
			p.setSiteCosts(getPrice(linkedproducts.getData("PackageCost"), ""));
		}

		// Bushfire
		linkedproducts.setColumn("ProductProductsLinkedProduct.Category", councilRequirementsPackageCostCategory);
		linkedproducts.setColumn("ProductProductsLinkedProduct.Name", bushFirePackageCostName);
		linkedproducts.doAction("search");
		linkedproducts.getResults(true);
		log.trace("Query 2 Bushfire: " + linkedproducts.getStatement());

		while (linkedproducts.getNext()) {
			p.setBushfire(getPrice(linkedproducts.getData("PackageCost"), ""));
		}

		// Bushfire
		linkedproducts.setColumn("ProductProductsLinkedProduct.Category", developerGuidelinesCategory);
		linkedproducts.setColumn("ProductProductsLinkedProduct.Name", "");
		linkedproducts.doAction("search");
		linkedproducts.getResults(true);
		log.trace("Query 3 DG: " + linkedproducts.getStatement());

		while (linkedproducts.getNext()) {
			p.setGuidelines(getPrice(linkedproducts.getData("PackageCost"), ""));
		}
		
		linkedproducts.close();
	}

	public Package getPackage(HttpServletRequest request, PropertyEntity property) {
		if (property == null) {
			throw new RuntimeException("Product was not found");
		}
		if (!property.isPropertyType(PropertyType.HouseLandPackage)) {
			throw new RuntimeException("Property [" + property.getPropertyType() + "] was not a house and land package");
		}
		HouseLandPackage product = (HouseLandPackage) property;
		
		Package p = new Package();
		p.setEstateName(capitalizeFirstLetters(product.getEntityEstateName()));
		p.setStatus(product.getString("CurrentStatus"));
		
		p.setRegionName(product.getString("RegionName"));
		p.setLocationName(product.getString("LocationName"));
		
		p.setFacade(getFacadeName(request, property.getString("ProductID")));

		p.setSuburb(product.getCity());
		p.setLotNumber(product.getLotNumber());
		p.setStageName(product.getStageName());
		p.setLandSize(product.getLotWidth() + "m x " + product.getLotDepth() + "m");
		p.setSize(product.getLotSize() + "m2");
		p.setHouseName(product.getName());
		
		p.setBeds(product.getBedrooms());
		p.setBaths(product.getBathrooms());
		p.setGarage(product.getCarParks());
		
		//p.setSqs(datarow.getString("HomeSizeSq"));
		p.setHousePrice(product.getPlanTotalCost());
		p.setLandPrice(product.getLotTotalCost());
		p.setTotalPrice(getPrice(product.getDripTotalCost() , product.getCost()));
		
		return p;
	}

	private void generateReport(HSSFWorkbook workbook, String formatFilePath, HashMap<String, Estate> estates) {
		TreeSet<String> sortedEstates = new TreeSet<String>(estates.keySet());

		HSSFSheet worksheet = renderWorksheet(workbook, "All Estates");
		HSSFSheet formattedSheet = getFormattedSheet(formatFilePath);

		int noOfColumns = renderHeaderRow(workbook, worksheet, 0, formattedSheet);

		int rowCount = 1;

		for (String estateName : sortedEstates) {
			Estate estate = (Estate) estates.get(estateName);

			for (Package p : estate.getHnlPackages()) {
				renderDataRow(workbook, worksheet, p, rowCount++, formattedSheet);
			}

			// autosize columns after all the data rows have been added
			for (int j = 0; j < noOfColumns; j++) {
				worksheet.autoSizeColumn(j);
			}
		}

		for (String estateName : sortedEstates) {
			Estate estate = (Estate) estates.get(estateName);
			worksheet = renderWorksheet(workbook, estate);

			formattedSheet = getFormattedSheet(formatFilePath);

			noOfColumns = renderHeaderRow(workbook, worksheet, 0, formattedSheet);

			rowCount = 1;
			for (Package p : estate.getHnlPackages()) {
				renderDataRow(workbook, worksheet, p, rowCount++, formattedSheet);
			}

			// autosize columns after all the data rows have been added
			for (int j = 0; j < noOfColumns; j++) {
				worksheet.autoSizeColumn(j);
			}
		}
	}

	private HSSFSheet renderWorksheet(HSSFWorkbook workbook, Estate estate) {
		HSSFSheet worksheet = workbook.createSheet(estate.getEstateName());
		return worksheet;
	}

	private HSSFSheet renderWorksheet(HSSFWorkbook workbook, String name) {
		HSSFSheet worksheet = workbook.createSheet(name);
		return worksheet;
	}

	public void renderDataRow(HSSFWorkbook workbook, HSSFSheet worksheet, Package p, int rowNum, HSSFSheet formattedSheet) {
		HSSFRow row = worksheet.createRow((short) rowNum);

		Iterator<Cell> dataRowIterator = null;

		if (formattedSheet != null) {
			dataRowIterator = formattedSheet.getRow(1).cellIterator();
		}

		int i = 0;
		addTextCell(row, dataRowIterator, p.getLotNumber(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getEstateName() + " / " + p.getStageName(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getSuburb(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getRegionName(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getLocationName(), i++, defaultDataRowStyle);

		// dataRowIterator = null;
		addTextCell(row, dataRowIterator, p.getLandSize(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getSize(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getFacade(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getHouseName(), i++, defaultDataRowStyle);
		addNumericCell(row, dataRowIterator, p.getBeds(), i++, defaultDataRowStyle);
		addNumericCell(row, dataRowIterator, p.getBaths(), i++, defaultDataRowStyle);
		addNumericCell(row, dataRowIterator, p.getGarage(), i++, defaultDataRowStyle);
		// addCell(row, dataRowIterator, p.getSqs(), i++, defaultDataRowStyle);
		addCurrencyCell(row, dataRowIterator, p.getHousePrice(), i++, defaultDataRowStyle);
		addCurrencyCell(row, dataRowIterator, p.getSiteCosts(), i++, defaultDataRowStyle);
		addCurrencyCell(row, dataRowIterator, p.getGuidelines(), i++, defaultDataRowStyle);
		addCurrencyCell(row, dataRowIterator, p.getLandPrice(), i++, defaultDataRowStyle);
		// addCurrencyCell(row, dataRowIterator, p.getFacadePrice(), i++, defaultDataRowStyle);
		addCurrencyCell(row, dataRowIterator, p.getBushfire(), i++, defaultDataRowStyle);
		addCurrencyCell(row, dataRowIterator, p.getTotalPrice(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getStatus(), i++, defaultDataRowStyle);
		if (hasPortalTask) {
			addTextCell(row, dataRowIterator, p.getSalesAgentName(), i++, defaultDataRowStyle);
			addTextCell(row, dataRowIterator, p.getSalesRepName(), i++, defaultDataRowStyle);
			addTextCell(row, dataRowIterator, p.getGroupName(), i++, defaultDataRowStyle);
			addTextCell(row, dataRowIterator, p.getExclusiveDate(), i++, defaultDataRowStyle);
		}
	}

	public int renderHeaderRow(HSSFWorkbook workbook, HSSFSheet worksheet, int rowNum, HSSFSheet formattedSheet) {
		HSSFRow row = worksheet.createRow((short) rowNum);

		Iterator<Cell> headerIterator = null;

		if (formattedSheet != null) {
			headerIterator = formattedSheet.getRow(0).cellIterator();
		}

		int i = 0;

		addTextCell(row, headerIterator, "Lot No", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Estate / Stage", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Suburb", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Region", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Location", i++, defaultHeaderStyle);
		// headerIterator = null;
		addTextCell(row, headerIterator, "Land Size", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Size", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Facade", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "House", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Beds", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Baths", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Garage", i++, defaultHeaderStyle);
		// addCell(row, headerIterator, "Sqs", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "House Price", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Site Costs", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Guidelines", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Land Price", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Bushfire", i++, defaultHeaderStyle);
		// addCell(row, headerIterator, "Facade Price", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Total Price", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Status", i++, defaultHeaderStyle);
		if (hasPortalTask) {
			addTextCell(row, headerIterator, "Sales Agent", i++, defaultHeaderStyle);
			addTextCell(row, headerIterator, "Sales Rep", i++, defaultHeaderStyle);
			addTextCell(row, headerIterator, "Group", i++, defaultHeaderStyle);
			addTextCell(row, headerIterator, "Exclusivity", i++, defaultHeaderStyle);
		}

		return i;
	}

	private void addTextCell(HSSFRow row, Iterator<Cell> headerIterator, String cellValue, int cellNum, CellStyle defaultStyle) {
		HSSFCell cellA1 = row.createCell(cellNum);

		if (headerIterator != null && headerIterator.hasNext()) {
			Cell cell = headerIterator.next();

			if (cell != null) {
				CellStyle newStyle = workbook.createCellStyle();
				newStyle.cloneStyleFrom(cell.getCellStyle());
				cellA1.setCellType(cell.getCellType());
				cellA1.setCellStyle(newStyle);
				cellA1.setCellValue(cellValue);
			}
		} else {
			cellA1.setCellStyle(defaultStyle);
			cellA1.setCellType(HSSFCell.CELL_TYPE_STRING);
			cellA1.getCellStyle().setDataFormat((short) 0);
			cellA1.setCellValue(cellValue);
		}
	}

	private void addNumericCell(HSSFRow row, Iterator<Cell> headerIterator, int cellValue, int cellNum, CellStyle defaultStyle) {
		HSSFCell cellA1 = row.createCell(cellNum);

		if (headerIterator != null && headerIterator.hasNext()) {
			Cell cell = headerIterator.next();

			if (cell != null) {
				CellStyle newStyle = workbook.createCellStyle();
				newStyle.cloneStyleFrom(cell.getCellStyle());
				cellA1.setCellType(cell.getCellType());
				cellA1.setCellStyle(newStyle);
				cellA1.setCellValue(cellValue);
			}
		} else {
			cellA1.getCellStyle().setDataFormat(format.getFormat("#"));
			cellA1.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			cellA1.setCellValue(cellValue);
		}

	}

	private void addNumericCell(HSSFRow row, Iterator<Cell> headerIterator, double cellValue, int cellNum, CellStyle defaultStyle) {
		HSSFCell cellA1 = row.createCell(cellNum);

		if (headerIterator != null && headerIterator.hasNext()) {
			Cell cell = headerIterator.next();

			if (cell != null) {
				CellStyle newStyle = workbook.createCellStyle();
				newStyle.cloneStyleFrom(cell.getCellStyle());
				cellA1.setCellType(cell.getCellType());
				cellA1.setCellStyle(newStyle);
				cellA1.setCellValue(cellValue);
			}
		} else {
			cellA1.getCellStyle().setDataFormat(format.getFormat("#"));
			cellA1.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			cellA1.setCellValue(cellValue);
		}

	}

	private void addCurrencyCell(HSSFRow row, Iterator<Cell> headerIterator, double cellValue, int cellNum, CellStyle defaultStyle) {
		HSSFCell cellA1 = row.createCell(cellNum);

		if (headerIterator != null && headerIterator.hasNext()) {
			Cell cell = headerIterator.next();

			if (cell != null) {
				CellStyle newStyle = workbook.createCellStyle();
				newStyle.cloneStyleFrom(cell.getCellStyle());
				cellA1.setCellType(cell.getCellType());
				cellA1.setCellStyle(newStyle);
				cellA1.setCellValue(cellValue);
			}
		} else {
			cellA1.setCellStyle(defaultDataRowCurrenyStyle);
			cellA1.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			cellA1.setCellValue(cellValue);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		PackageReport report = new PackageReport();
		// report.createPackageReport();
		System.out.println("Done");
	}

	private static String capitalizeFirstLetters(String input) {
		if (StringUtils.isEmpty(input))
			return input;
		if (StringUtils.countMatches(input, " ") > 0)
			return WordUtils.capitalize(input.toLowerCase());
		else
			return StringUtils.capitalize(input.toLowerCase());

	}

	private double getPrice(String price, String defaultPrice) {
		if (price.replaceAll("[\\$0\\.\\-]", "").length() == 0) {
			price = defaultPrice;
		}

		if (price != null) {
			try {
				return StringUtil.getRoundedValue(price);

			} catch (Exception e) {
			}
		}
		return 0;
	}

	private HSSFSheet getFormattedSheet(String formatFilePath) {
		if (StringUtils.isNotBlank(formatFilePath)) {
			try {
				FileInputStream file = new FileInputStream(new File(formatFilePath));

				HSSFWorkbook workbook = new HSSFWorkbook(file);
				HSSFSheet sheet = workbook.getSheetAt(0);
				return sheet;
			} catch (Exception e) {
				e.printStackTrace();
				errorMessage = "the excel format file is not as per the specifications, please rectify and retry";
			}
		}
		return null;
	}

	private String getPreformattedExcelFile(HttpServletRequest request) {
		GenRow bean = new GenRow();
		bean.setViewSpec("DocumentView");
		bean.setRequest(request);
		bean.setColumn("DocumentSubType", "PackageReportFormat");
		bean.doAction("selectfirst");

		log.debug("getPreformattedExcelFile Query: " + bean.getStatement());

		if (bean.isSuccessful() && StringUtils.isNotBlank(bean.getString("FilePath"))) {
			String p = "/" + bean.getString("FilePath");
			log.debug("PreformattedExcelFile file path is : " + p);
			return InitServlet.getRealPath(p);
		}
		return null;
	}

	public String getFacadeName(HttpServletRequest request, String productid){
		   GenRow row = new GenRow();
		   row.setRequest(request);
		   row.setViewSpec("ProductLinkedProductView");
		   row.setColumn("ProductID", productid);
		   row.setColumn("ProductProductsLinkedProduct.ProductType","Home Facade");
		   row.sortBy("ProductProductsLinkedProduct.Name",0);
		   if(productid!=null && productid.length()!=0){
		   	row.doAction("selectfirst");
		   }
		   return(row.getString("Name"));
		}

}