package com.sok.runway.crm.cms;

import com.sok.runway.*;
import com.sok.runway.crm.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;


public class PageProduct extends LinkableProduct {

public static final String PAGE_PRODUCT_VIEWSPEC = "PageProductView";
   
   public PageProduct(Connection con) {
		super(con, PAGE_PRODUCT_VIEWSPEC);
	}
		
	public PageProduct(ServletRequest request) {
		super(request, PAGE_PRODUCT_VIEWSPEC);
		populateFromRequest(request.getParameterMap());
	}
	   
	public PageProduct(Connection con, String pageProductID) {
		this(con);
	   load(pageProductID);
	}
   
   public Validator getValidator() {
		Validator val = Validator.getValidator("PageProducts");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("PageProductID","SYSTEM ERROR - PageProductID");
			val.addMandatoryField("ProductID","SYSTEM ERROR - ProductID");
			val.addMandatoryField("PageID","SYSTEM ERROR - ProductID");
			Validator.addValidator("PageProducts", val);
		}
		return val;
   }
}