package com.sok.runway.crm.factories;

import com.sok.runway.crm.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.framework.*;
import java.util.*;

public class AddressFactory {
	
	public static Map<String,String> getAddress(String type, Addressable entity) {
		
		if (type == null) {
			type = ActionBean._blank;
		}
		
		Map<String,String> address = new HashMap<String,String>();
		address.put(Addressable.STREET, entity.getField(type + Addressable.STREET));
		address.put(Addressable.STREET2, entity.getField(type + Addressable.STREET2));
		address.put(Addressable.CITY, entity.getField(type + Addressable.CITY));
		address.put(Addressable.STATE, entity.getField(type + Addressable.STATE));
		address.put(Addressable.POSTCODE, entity.getField(type + Addressable.POSTCODE));
		address.put(Addressable.COUNTRY, entity.getField(type + Addressable.COUNTRY));
		
		return address;
	}
	
	public static void setAddress(String type, Addressable entity, Map<String,String> address) {
		
		if (type == null) {
			type = ActionBean._blank;
		}
		
		entity.setField(type + Addressable.STREET, address.get(Addressable.STREET));
		entity.setField(type + Addressable.STREET2, address.get(Addressable.STREET2));
		entity.setField(type + Addressable.CITY, address.get(Addressable.CITY));
		entity.setField(type + Addressable.STATE, address.get(Addressable.STATE));
		entity.setField(type + Addressable.POSTCODE, address.get(Addressable.POSTCODE));
		entity.setField(type + Addressable.COUNTRY, address.get(Addressable.COUNTRY));
		
	}
	
	public static void updateCompanyAddressFromContact(Contact contact) {
		
		Company company = contact.getCompany();
		if (company != null) {
			company.setAddress(contact.getAddress());
			company.setAddress("Postal", contact.getAddress("Home"));
			company.update();
			
			updateContactAddressesFromCompany(company);
		}
		
	}
	
	public static void updateContactCompanyFromCompany(Company company) { 
		if (company != null) {
			GenRow row = new GenRow();
			row.setTableSpec("Contacts");
			row.setParameter("ON-CompanyID", company.getPrimaryKey()); 
			row.setParameter("Company", company.getField("Company"));
			row.setParameter("ModifiedDate", ActionBean.NOW);
			row.setParameter("ModifiedBy", company.getCurrentUser().getUserID());
			row.doAction(ActionBean.UPDATEALL);
		}
	}
	
	public static void updateContactAddressesFromCompany(Company company) {
		if (company != null) {
			
			String type = "Home";
			String company_type = "Postal";
			
			GenRow row = new GenRow();
			row.setTableSpec("Contacts");
			row.setParameter("ON-CompanyID", company.getPrimaryKey());
			row.setParameter(Addressable.STREET, company.getField(Addressable.STREET));
			row.setParameter(Addressable.STREET2, company.getField(Addressable.STREET2));
			row.setParameter(Addressable.CITY, company.getField(Addressable.CITY));
			row.setParameter(Addressable.STATE, company.getField(Addressable.STATE));
			row.setParameter(Addressable.POSTCODE, company.getField(Addressable.POSTCODE));
			row.setParameter(Addressable.COUNTRY, company.getField(Addressable.COUNTRY));
			
			row.setParameter(type + Addressable.STREET, company.getField(company_type + Addressable.STREET));
			row.setParameter(type + Addressable.STREET2, company.getField(company_type + Addressable.STREET2));
			row.setParameter(type + Addressable.CITY, company.getField(company_type + Addressable.CITY));
			row.setParameter(type + Addressable.STATE, company.getField(company_type + Addressable.STATE));
			row.setParameter(type + Addressable.POSTCODE, company.getField(company_type + Addressable.POSTCODE));
			row.setParameter(type + Addressable.COUNTRY, company.getField(company_type + Addressable.COUNTRY));
			
			row.setParameter("ModifiedDate", ActionBean.NOW);
			row.setParameter("ModifiedBy", company.getCurrentUser().getUserID());
			row.doAction(ActionBean.UPDATEALL);
		}
	}
	
	
}