package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ListFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil.EstateComparator;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil.Home;
import com.sok.runway.crm.cms.properties.pdf.templates.model.Package;

public class MultiHomePdf extends AbstractPDFTemplate implements PDFConstants {

	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException,
			DocumentException {
		
		setStatus(request, "Loading properties");
		java.util.List<PropertyEntity> properties = PropertyFactory.getPropertyEntities(request, this);

		HashMap<String, Home> homes = new HashMap<String, Home>();

		for (PropertyEntity property : properties) {
			if (!property.isPropertyType(PropertyType.HouseLandPackage)) {
				throw new RuntimeException(String.format("ProductID [%1$s] Property [%2$s] is not a house and land package",
						property.getString("ProductID"), property.getPropertyType()));
			}
			HouseLandPackage product = (HouseLandPackage) property;
			Package p = PDFUtil.getPackage(product);

			Home h = (Home) homes.get(p.name);
			if (h == null) {
				h = new PDFUtil.Home();
				h.name = p.name;
				h.price = p.price;
				h.bedrooms = p.bedrooms;
				h.bathrooms = p.bathrooms;
				h.bathroomsAsString = p.bathroomsAsString;
				h.carParks = p.carParks;
				h.range = p.range;
				h.rangeName = p.rangeName;
				homes.put(p.name, h);
			}
			h.packages.add(p);
		}

		setStatus(request, "Rendering Pages");
		renderPages(document, homes);
	}

	private void renderPages(com.itextpdf.text.Document document, HashMap<String, Home> homes) throws DocumentException {
		TreeSet<String> sortedHomes = new TreeSet<String>(homes.keySet());
		boolean addNewPage = false;

		for (String homeName : sortedHomes) {
			if (addNewPage)
				document.newPage();
			else
				addNewPage = true;

			Home home = (Home) homes.get(homeName);
			renderHomePage(document, home);
		}
	}

	private void renderHomePage(com.itextpdf.text.Document document, Home h) throws DocumentException {
		Collections.sort(h.packages, new EstateComparator());
		PdfPTable hdrTable = PDFUtil.getHeader(null, h);

		PdfPTable pageTable = new PdfPTable(1);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.getDefaultCell().setFixedHeight(0f);
		pageTable.setWidthPercentage(100f);

		pageTable.addCell(hdrTable);

		pageTable.getDefaultCell().setFixedHeight(11f);
		pageTable.addCell(BLANK);

		for (int j = 0; j < h.packages.size(); j++) {
			if (j != 0 && j % 6 == 0) {
				document.add(pageTable);
				document.newPage();
				pageTable = new PdfPTable(1);
				pageTable.getDefaultCell().setBorder(0);
				pageTable.getDefaultCell().setPadding(0);
				pageTable.getDefaultCell().setFixedHeight(0f);
				pageTable.setWidthPercentage(100f);

				pageTable.addCell(hdrTable);
				pageTable.getDefaultCell().setFixedHeight(11f);
				pageTable.addCell(BLANK);
			}

			Package p = (Package) h.packages.get(j);
			pageTable.getDefaultCell().setFixedHeight(0f);
			pageTable.getDefaultCell().setBorderWidthTop(0.01f);
			pageTable.getDefaultCell().setBorderWidthBottom(0.01f);
			pageTable.getDefaultCell().setBorderColor(BaseColor.BLACK);
			pageTable.addCell(PDFUtil.buildPackageTable(p, false));

			pageTable.getDefaultCell().setBorder(0);
			pageTable.getDefaultCell().setFixedHeight(6f);
			pageTable.addCell(BLANK);
		}
		document.add(pageTable);
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		String headText = "\nPlease see sales consultant for more packages available";
		return new ListFooter(request, headText);
	}

}