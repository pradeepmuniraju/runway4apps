package com.sok.runway.crm.searchbuilder;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sok.runway.crm.searchbuilder.dao.SearchCriteriaDAO;
import com.sok.runway.crm.searchbuilder.dao.SearchSetDAO;
import com.sok.runway.crm.searchbuilder.dao.SearchStatementDAO;

public class SearchBuilderBean {
    private SearchStatement searchStatement;
   
    public SearchBuilderBean() {
       initialise();
    }
   
    public void initialise() {
       if(this.searchStatement == null) {
          this.searchStatement = new SearchStatement();
       } else {
          this.searchStatement.clear();
       }
    }
   
    /**
     * 
     * @param searchName the searchName to set
     */
    public void setSearchName(String searchName) {
        if(searchName != null && searchName.length() > 0) {
            this.searchStatement.setName(searchName);
        }   
    }
    
    /**
     * 
     * @param searchDescription the search description to set
     */
    public void setSearchDescription(String searchDescription) {
        if(searchDescription != null && searchDescription.length() > 0) {
            this.searchStatement.setDescription(searchDescription);
        }
    }
    
    /**
     * 
     * @param searchType the searchType to set
     */
    public void setSearchType(String searchType) {
        if(searchType != null && searchType.length() > 0) {
            this.searchStatement.setSearchType(searchType);
        }
    }
    
    /**
     * 
     * @return the searchStatement model
     */
    public SearchStatement getSearchStatement() {
        return this.searchStatement;
    }
   
    /**
     * 
     * @param searchSetId
     * @return the searchSet with the specified <code>searchSetId</code>
     */
    public SearchSet getSearchSet(String searchSetId) {
        SearchSet searchSet = this.searchStatement.getSearchSet(searchSetId);

        return searchSet;
    }
   
    /**
     * 
     * @param searchSetId
     * @param searchCriteriaId
     * @return the SearchCriteria for the specified <code>searchSetId</code> and <code>searchCriteriaId</code>
     */
    public SearchCriteria getSearchCriteria(String searchSetId, String searchCriteriaId) {
        SearchCriteria searchCriteria = null;
       
        SearchSet searchSet = this.getSearchSet(searchSetId);
       
        if(searchSet != null) {
           searchCriteria = searchSet.getSearchCriteria(searchCriteriaId);
        }
       
        return searchCriteria;
    }
   
    /**
     * <p>Creates a new <code>SearchSet</code> row with the specified <code>searchSetOperator</code> 
     * and appends it to the <code>searchStatement</code>. The <code>id</code> 
     * of the newly created <code>SearchSet</code> row is returned.</p> 
     * 
     * @param searchSetOperator
     * @return the <code>id</code> of the newly created <code>SearchSet</code> row
     */
    public String appendNewSearchSet(String searchSetOperator) {
        SearchSetOperator operator = SearchSetOperator.createSearchOperator(searchSetOperator);
        SearchSet searchSet = new SearchSet();
        searchSet.setOperator(operator);
        this.searchStatement.appendSearchSet(searchSet);
         
        /* Append a new empty search criteria to this search set */
        searchSet.appendNewSearchCriteria();
        
        return searchSet.getId();
    }
    
    public String appendNewSearchCriteria(String searchSetId) {
        final SearchSet searchSet = this.searchStatement.getSearchSet(searchSetId);
        final String searchCriteriaId = searchSet.appendNewSearchCriteria();

        return searchCriteriaId;
    }
    
    /**
     * 
     * @param request that contains the data source for creating DAOs
     */
    public void saveCreate(HttpServletRequest request) {
        SearchStatementDAO searchStatementDAO = new SearchStatementDAO(request);
        SearchSetDAO searchSetDAO = new SearchSetDAO(request);
        SearchCriteriaDAO searchCriteriaDAO = new SearchCriteriaDAO(request);
        
        /* No SearchStatementId, so create the SearchStatement row */
        searchStatementDAO.create(this.searchStatement);
        
        /* Create SeachSet rows */
        String searchStatementId = this.searchStatement.getId();
        
        for(SearchSet searchSet : searchStatement.getSearchSets()) {
            searchSet.setSearchStatementId(searchStatementId);
            searchSetDAO.create(searchSet);
            
            /* Create SearchCriteria rows */
            String searchSetId = searchSet.getId();
            
            for(SearchCriteria searchCriteria : searchSet.getSearchCriterias()) {
                searchCriteria.setSearchSetId(searchSetId);
                searchCriteriaDAO.create(searchCriteria);
            }
        }
    }
    
    public void save(HttpServletRequest request) {
        SearchStatementDAO searchStatementDAO = new SearchStatementDAO(request);
        searchStatementDAO.save(this.searchStatement);
        // TODO: implement
    }
   
    public void load(HttpServletRequest request, String searchStatementId) {
        this.searchStatement.clear();
        
        /* Load Search Statement */
        SearchStatementDAO searchStatementDAO = new SearchStatementDAO(request);
        this.searchStatement = searchStatementDAO.findById(searchStatementId);
        
        /* Load and wire up Search Sets */
        SearchSetDAO searchSetDAO = new SearchSetDAO(request);
        List<SearchSet> searchSets = searchSetDAO.findAllBySearchStatementId(searchStatementId);
        this.searchStatement.setSearchSets(searchSets);
        
        /* Load and wire up Search Criterias */
        SearchCriteriaDAO searchCriteriaDAO = new SearchCriteriaDAO(request);
        for(SearchSet searchSet : searchSets) {
            searchSet.setSearchStatement(searchStatement);
            
            List<SearchCriteria> searchCriterias = searchCriteriaDAO.findAllBySearchSetId(searchSet.getId());            
            searchSet.setSearchCriterias(searchCriterias);
            
            for(SearchCriteria searchCriteria : searchCriterias) {
                searchCriteria.setSearchSet(searchSet);
            }
        }
    }
}
