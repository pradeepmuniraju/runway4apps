package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.Package;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ListFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil.Estate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil.Land;

public class MultiLandPdf extends AbstractPDFTemplate implements PDFConstants {
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException,
			DocumentException {
		
		setStatus(request, "Loading Properties");
		java.util.List<PropertyEntity> properties = PropertyFactory.getPropertyEntities(request, this);

		HashMap<String, Estate> estates = new HashMap<String, Estate>();

		for (PropertyEntity property : properties) {
			if (!property.isPropertyType(PropertyType.HouseLandPackage)) {
				throw new RuntimeException(String.format("ProductID [%1$s] Property [%2$s] is not a house and land package",
						property.getString("ProductID"), property.getPropertyType()));
			}
			HouseLandPackage product = (HouseLandPackage) property;
			Package p = PDFUtil.getPackage(product);
			
			if (p.estateName == null || p.estateName.length() == 0) p.estateName = "- Missing -";

			Estate e = (Estate) estates.get(p.estateName);
			if (e == null) {
				e = new Estate();
				e.estateName = p.estateName;
				e.estateLogo = PropertyEntity.resizeImage(p.estateLogo, 140, 65, PropertyEntity.ResizeOption.Fit, FORCE_TRUE);
				estates.put(p.estateName, e);
			}

			Land l = (Land) e.lands.get(p.lotName);
			if (l == null) {
				l = new Land();
				l.name = p.lotName;
				l.landPrice = p.landPrice;
				l.estateName = p.estateName;
				l.suburb = p.suburb;
				l.titleDate = p.titleDate;
				l.lotSize = p.lotSize;
				e.lands.put(l.name, l);
			}

			l.packages.add(p);
		}
		setStatus(request, "Rendering Pages");
		renderPages(document, estates);
	}

	private void renderPages(com.itextpdf.text.Document document, HashMap<String, Estate> estates) throws DocumentException {
		TreeSet<String> sortedEstates = new TreeSet<String>(estates.keySet());
		boolean addNewPage = false;

		for (String estateName : sortedEstates) {
			if (addNewPage)
				document.newPage();
			else
				addNewPage = true;

			Estate estate = (Estate) estates.get(estateName);
			renderEstatePage(document, estate);
		}
	}

	private void renderEstatePage(com.itextpdf.text.Document document, Estate e) throws DocumentException {
		PdfPTable hdrTable = PDFUtil.getHeader(e, null);

		PdfPTable pageTable = new PdfPTable(1);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.setWidthPercentage(100f);
		pageTable.addCell(hdrTable);

		TreeSet<String> sortedLands = new TreeSet<String>(e.lands.keySet());

		int count = 1;
		int LANDS_PER_PAGE = 4;

		for (String landName : sortedLands) {
			Land land = (Land) e.lands.get(landName);
			pageTable.getDefaultCell().setPadding(0);
			pageTable.getDefaultCell().setPaddingTop(10);
			pageTable.getDefaultCell().setPaddingBottom(10);
			pageTable.getDefaultCell().setBorder(0);
			pageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
			pageTable.getDefaultCell().setBorderWidthBottom(0.1f);
			PdfPTable landTable = getLandTable(land);
			pageTable.addCell(landTable);

			if (count % LANDS_PER_PAGE == 0 && sortedLands.size() > count) {
				document.add(pageTable);
				document.newPage();
				pageTable = new PdfPTable(1);
				pageTable.getDefaultCell().setBorder(0);
				pageTable.getDefaultCell().setPadding(0);
				pageTable.setWidthPercentage(100f);
				pageTable.addCell(hdrTable);
				LANDS_PER_PAGE = 4;
			}
			count++;
		}

		document.add(pageTable);
	}

	private PdfPTable getLandTable(Land land) throws DocumentException {
		java.util.List<java.util.List<Package>> dividedPackagesList = dividePackages(land.packages);

		PdfPTable landTable = new PdfPTable(4);
		// landTable.setWidthPercentage(100f);
		landTable.setTotalWidth(100f);
		landTable.setWidths(new int[] { 23, 35, 35, 35 });
		landTable.getDefaultCell().setPadding(0);
		landTable.getDefaultCell().setBorder(0);
		landTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
		landTable.getDefaultCell().setMinimumHeight(100f);

		Paragraph p = new Paragraph();
		p.add(new Chunk(land.name + "\n", landNameFont));
		p.add(new Chunk("\n", PDFUtil.s1));
		Paragraph lotSize = PDFUtil.BLANK;
		if (land.lotSize > 0) {
			lotSize = PDFUtil.getArea("(", ")", String.valueOf(new Double(land.lotSize).intValue()));
		}
		p.add(lotSize);
		p.add(new Chunk("\n", PDFUtil.s1));
		p.add(new Chunk((land.estateName) + "\n", landDetailsFont));
		p.add(new Chunk("\n", PDFUtil.s1));
		p.add(new Chunk((land.suburb) + "\n", landDetailsFont));
		p.add(new Chunk("\n", landDetailsFont));
		p.add(new Chunk("\n", PDFUtil.smallFont));
		p.add(new Chunk("Land Price " + land.landPrice + "\n", landDetailsSmallFont));
		p.add(new Chunk("\n", landDetailsFont));
		p.add(new Chunk("Expected Title Date: \n", landDetailsSmallFont));
		p.add(new Chunk("\n", PDFUtil.smallFont));
		if(StringUtils.isNotBlank(land.titleDate))
			p.add(new Chunk(land.titleDate, landDetailsSmallFont));
		else
			p.add(new Chunk("", landDetailsSmallFont));
		
		// First Column
		int cellPadding = 10;
		landTable.addCell(p);
		landTable.getDefaultCell().setPaddingLeft(0);
		landTable.getDefaultCell().setPaddingRight(cellPadding);

		// Add the package columns
		for (int i = 0; i < dividedPackagesList.size(); i++) {
			if (i == 0 || i != dividedPackagesList.size() - 1) {
				landTable.getDefaultCell().setBorderWidthRight(0.1f);
				landTable.getDefaultCell().setBorderColor(PDFUtil.aqua);
			} else {
				landTable.getDefaultCell().setBorder(0);
			}

			landTable.addCell(getPackageTable(dividedPackagesList.get(i)));
			landTable.getDefaultCell().setPaddingLeft(cellPadding);
		}
		landTable.getDefaultCell().setBorder(0);
		// Fill any left over columns with blank
		for (int i = 0; i < 3 - dividedPackagesList.size(); i++) {
			landTable.addCell(BLANK);
		}
		return landTable;
	}

	private PdfPTable getPackageTable(java.util.List<Package> packages) throws DocumentException {
		PdfPTable packageTable = new PdfPTable(2);
		packageTable.setTotalWidth(100f);
		packageTable.setWidths(new int[] { 65, 35 });
		packageTable.getDefaultCell().setBorder(0);
		packageTable.getDefaultCell().setPadding(0);
		packageTable.getDefaultCell().setFixedHeight(13f);
		packageTable.getDefaultCell().setNoWrap(true);
		packageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);

		for (Package p : packages) {
			packageTable.getDefaultCell().setPadding(0);
			packageTable.getDefaultCell().setPaddingRight(3);
			packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			packageTable.addCell(new Paragraph(p.name, packageNameFont));

			packageTable.getDefaultCell().setPadding(0);
			packageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			packageTable.addCell(new Paragraph(p.price, packagePriceFont));
		}
		return packageTable;
	}

	private java.util.List<java.util.List<Package>> dividePackages(java.util.List<Package> packages) {
		java.util.List<java.util.List<Package>> splitPackages = new ArrayList<List<Package>>();
		if (packages != null && packages.size() > 0) {
			int colCapacity = 8;
			int length = packages.size();
			int count = 0;
			int startIndex = 0;

			if (length > 24) {
				colCapacity = getNoOfChunks(length, 3);
			}
			int noOfColumns = getNoOfChunks(length, colCapacity);

			do {
				count++;
				splitPackages.add(packages.subList(startIndex, Math.min(length, count * colCapacity)));
				startIndex = count * colCapacity;
			} while (count < noOfColumns);
		}
		return splitPackages;
	}

	private static int getNoOfChunks(int dataSize, int chunkSize) {
		return dataSize % chunkSize == 0 ? dataSize / chunkSize : (dataSize / chunkSize) + 1;
	}

	// Multi Land
	public static final Font landNameFont = FontFactory.getFont(arialFont, fontRatio * 16, PDFUtil.aqua);
	public static final Font landDetailsFont = FontFactory.getFont(arialFont, fontRatio * 16, BaseColor.BLACK);
	public static final Font landDetailsSmallFont = FontFactory.getFont(arialFont, fontRatio * 12, BaseColor.BLACK);
	public static final Font packageNameFont = FontFactory.getFont(arialFont, fontRatio * 16, BaseColor.BLACK);
	public static final Font packagePriceFont = FontFactory.getFont(arialFont, fontRatio * 15, Font.BOLD, BaseColor.BLACK);

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		String headText = "\nPlease see sales consultant for more details \non these packages in this estate";
		return new ListFooter(request, headText);
	}
}