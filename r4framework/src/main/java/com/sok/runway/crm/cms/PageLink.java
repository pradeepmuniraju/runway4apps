package com.sok.runway.crm.cms;

import com.sok.framework.generation.IllegalConfigurationException;
import com.sok.runway.Validator;
import com.sok.runway.crm.activity.Logable;
import com.sok.runway.security.SecuredEntity;
import com.sok.service.cms.WebsiteService;

import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RunwayEntity object for PageLink
 * 
 * @author Irwan
 * @version 0.1
 * 
 * @version 0.2 - updated for json serialization
 */
@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PageLink extends SecuredEntity implements Logable {
	private static final Logger logger = LoggerFactory.getLogger(PageLink.class);
	public static final String ENTITY_NAME = "Page Link";
	@Override
	public String getEntityName() {
		return ENTITY_NAME;
	}
	
	public PageLink(Connection con) {

		super(con, "cms3/PageLinkEditView");
	}
	
	public PageLink() {
		super((Connection) null, "cms3/PageLinkEditView");
	}

	public PageLink(ServletRequest request) {

		super(request, "cms3/PageLinkEditView");
		populateFromRequest(request.getParameterMap());
	}

	public PageLink(Connection con, String linkID) {

		this(con);
		load(linkID);
	}

	public Validator getValidator() {
		Validator val = Validator.getValidator("PageLink");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("LinkID","SYSTEM ERROR - LinkID");
			val.addMandatoryField("ShortURL","A short url must be supplied");
			Validator.addValidator("PageLink", val);
		}
		return val;
	}

	@Override
	public boolean canCreate() {
		if(StringUtils.isNotBlank(getField("CMSID"))) {
			return canEdit();
		}
		return super.canCreate();
	}
	
	@Override
	public boolean canEdit() {
		if(StringUtils.isNotBlank(getField("CMSID"))) {
			return WebsiteService.getInstance().canEditWebsite(getConnection(), getCurrentUser(), getField("CMSID"));
		}
		return super.canEdit();
	}
	
	@Override
	public boolean canDelete() {
		if(StringUtils.isNotBlank(getField("CMSID"))) {
			//TODO - when versioning is implemented this should only be available to publisher users, unless the page isn't published.
			return canEdit();
		}
		return super.canDelete();
	}
	@Override
	public boolean delete() {
		Page p = new Page(getConnection(), getPageID());
		p.setCurrentUser(getCurrentUser());
		
		if(!p.delete())
		{
			logger.debug("page.delete() failed");
			return false;
		}
		return super.delete();
	}
	
	@JsonProperty("LinkID")
	public String getLinkID() {
		return getField("LinkID");
	}
	@JsonProperty("LinkID")
	public void setLinkID(String LinkID) {
		setField("LinkID", LinkID);
	}
	@JsonProperty("PageID")
	public String getPageID() {
		return getField("PageID");
	}
	@JsonProperty("PageID")
	public void setPageID(String PageID) {
		setField("PageID", PageID);
	}
	@JsonProperty("ParentID")
	public String getParentID() {
		return getField("ParentID");
	}
	@JsonProperty("ParentID")
	public void setParentID(String ParentID) {
		setField("ParentID", ParentID);
	}
	
	@JsonProperty("Path")
	public String getPath() {
		return getField("Path");
	}
	/*	do not allow setting of path
	@JsonProperty("Path")
	public void setPath(String Path) {
		setField("Path", Path);
	}
	 */
	@JsonProperty("CurrentTitle")
	public String getCurrentTitle() {
		return getField("CurrentTitle");
	}
	
	@JsonProperty("CMSID")
	public String getCMSID() {
		return getField("CMSID");
	}
	@JsonProperty("CMSID")
	public void setCMSID(String CMSID) {
		setField("CMSID", CMSID);
	}
	@JsonProperty("ExternalPath")
	public String getExternalPath() {
		return getField("ExternalPath");
	}
	/* do not allow setting of path
	@JsonProperty("ExternalPath")
	public void setExternalPath(String ExternalPath) {
		setField("ExternalPath", ExternalPath);
	}
	 */
	@JsonProperty("ShortURL")
	public String getShortURL() {
		return getField("ShortURL");
	}
	@JsonProperty("ShortURL")
	public void setShortURL(String ShortURL) {
		setField("ShortURL", ShortURL);
	}
	@JsonProperty("SortOrder")
	public Integer getSortOrder() {
		return getInt("SortOrder");
	}
	@JsonProperty("SortOrder")
	public void setSortOrder(int SortOrder) {
		setField("SortOrder", SortOrder);
	}
	
	@JsonProperty("Active")
	public boolean isActive() {
		return "Active".equals(getField("Active")) ? true : false;
	}
	@JsonProperty("Active")
	public void setActive(boolean Active) {
		setField("Active", Active ? "Active" : "Inactive");
	}
/*	TODO - versioning phase.
	@JsonProperty("Deleted")
	public boolean isDeleted() {
		return "Y".equals(getField("Deleted")) ? true : false;
	}
	
	@JsonProperty("Deleted")
	public void setDeleted(boolean Deleted) {
		setField("Deleted", Deleted ? "Y" : "N");
	}
*/	
	@JsonProperty("IncludeInMenu")
	public boolean isIncludeInMenu() {
		return "Y".equals(getField("IncludeInMenu")) ? true : false;
	}
	@JsonProperty("IncludeInMenu")
	public void setIncludeInMenu(boolean IncludeInMenu) {
		setField("IncludeInMenu", IncludeInMenu ? "Y" : "N");
	}
	
	@JsonProperty("ExpiryLinkID")
	public String getExpiryLinkID() {
		return getField("ExpiryLinkID");
	}
	@JsonProperty("ExpiryLinkID")
	public void setExpiryLinkID(String ExpiryLinkID) {
		setField("ExpiryLinkID", ExpiryLinkID);
	}
	@JsonProperty("ExpiryURL")
	public String getExpiryURL() {
		return getField("ExpiryURL");
	}
	@JsonProperty("ExpiryURL")
	public void setExpiryURL(String ExpiryURL) {
		setField("ExpiryURL", ExpiryURL);
	}
	
	@JsonProperty("StartDate")
	public void setStartDateFromUTC(long startDateUTC) {
		logger.debug("setStartDateFromUTC({})", startDateUTC);
		if(startDateUTC > 0)
			setField("StartDate", new Date(startDateUTC));
	}
	
	@JsonProperty("EndDate")
	public void setEndDateFromUTC(long endDateUTC) {
		logger.debug("setEndDatefromUTC({})", endDateUTC);
		if(endDateUTC > 0) 
			setField("EndDate", new Date(endDateUTC));
	}

	@JsonProperty("StartDate")
	public Long getStartDateUTC() {	//for serialization only, not internal use.
		try { 
			Date d = getDate("StartDate");
			if(d != null) return new Long(d.getTime());
		} catch (IllegalConfigurationException iec) { }
		return null;
	}

	@JsonProperty("EndDate")
	public Long getEndDateUTC() {	//for serialization only, not internal use.
		try { 
			Date d = getDate("EndDate");
			if(d != null) return new Long(d.getTime());
		} catch (IllegalConfigurationException iec) { }
		return null;
	}
}