package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.RunwayUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.pdf.AdvancedPDFServlet;
import com.sok.runway.crm.cms.properties.pdf.templates.developer.DeveloperPdfGenerator;
import com.sok.runway.crm.cms.properties.pdf.templates.model.UserInfo;

public class CommonUtil {

	private static final Logger logger = LoggerFactory.getLogger(CommonUtil.class);
	public static String[] fonts = { "Calibri", "Verdana", "Arial", "Helvetica", "sans-serif" };

	public static String font = "Helvetica";
	public static String unfont = "";
	static {
		for (String f : fonts) {
			if (FontFactory.isRegistered(f)) {
				font = f;
				break;
			}
		}
	}

	public static String capitalizeFirstLetters(String input) {
		if (StringUtils.isEmpty(input))
			return input;
		if (StringUtils.countMatches(input, " ") > 0)
			return WordUtils.capitalize(input.toLowerCase());
		else
			return StringUtils.capitalize(input.toLowerCase());

	}

	public static String concat(String[] inputs) {
		StringBuilder output = new StringBuilder(100);
		for (String input : inputs) {
			output.append(input);
			output.append(" ");
		}
		return output.toString();
	}

	public static String getEmptyStringIfNull(String input) {
		if (input == null)
			return "";
		else
			return input;
	}

	private static String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");

	public static Image createImage(String imagepath) {
		if (imagepath == null || imagepath.endsWith("null") || imagepath.length() == 0) {
			return null;
		}
		try {
			imagepath = imagepath.replaceAll("//", "/").replaceFirst(":/", "://"); 
			if (imagepath.startsWith(homePath)) {
				imagepath = imagepath.substring(homePath.length());
				if (!imagepath.startsWith("/"))
					imagepath = "/" + imagepath;
				return Image.getInstance(InitServlet.getRealPath(imagepath));
			} else if (imagepath.startsWith("/")) {
				return Image.getInstance(InitServlet.getRealPath(imagepath));
			}
			if (imagepath.indexOf("\\") == -1)
				return Image.getInstance(imagepath.replaceAll(" ", "%20"));
			else 
				return Image.getInstance(imagepath);
		} catch (java.io.FileNotFoundException nfn) {
			logger.info("File was not found {}", imagepath);
			return null;
		} catch (Exception e) {
			logger.error("Error on image load for : " + imagepath, e);
			return null;
		}
	}

	public static Image createImage(String imagepath, float fitWidth, float fitHeight) {
		if (imagepath == null || imagepath.endsWith("null") || imagepath.length() == 0) {
			return null;
		}
		try {
			Image image = null;
			imagepath = imagepath.replaceAll("//", "/").replaceFirst(":/", "://"); 
			if (imagepath.startsWith(homePath)) {
				imagepath = imagepath.substring(homePath.length());
				if (!imagepath.startsWith("/"))
					imagepath = "/" + imagepath;
				image = Image.getInstance(InitServlet.getRealPath(imagepath));
			} else if (imagepath.startsWith("/")) {
				image = Image.getInstance(InitServlet.getRealPath(imagepath));
			} else {
				if (imagepath.indexOf("\\") == -1)
					image = Image.getInstance(imagepath.replaceAll(" ", "%20"));
				else
					image = Image.getInstance(imagepath);
			}
			image.scaleToFit(fitWidth, fitHeight);
			return image;
		} catch (java.io.FileNotFoundException nfn) {
			logger.info("File was not found {}", imagepath);
			return null;
		} catch (Exception e) {
			logger.error("Error on image load for : " + imagepath, e);
			return null;
		}
	}

	/* strip out email from "name <email@example.com>" style email fields */
	public static String getRepEmail(String email) {
		int s = email.indexOf("<"), e = email.indexOf(">");
		if (s == -1 || e == -1 || e < s)
			return email;
		return email.substring(s + 1, e);
	}

	public static GenRow getDisplayLocation(String displayID) {
		GenRow row = new GenRow();
		if (displayID != null && displayID.length() != 0) {
			row.setViewSpec("DisplayEntityView");
			row.setParameter("DisplayEntityID", displayID);
			row.doAction("select");
		}
		return row;
	}

	public static String getDisplayHours(String companyID) {
		GenRow companybean = new GenRow();
		companybean.setTableSpec("Companies");
		companybean.setParameter("-select13", "Notes");
		companybean.setParameter("CompanyID", companyID);
		companybean.doAction("selectfirst");
		String hours = companybean.getString("Notes");
		return hours;
	}

	public static GenRow getRep(String repUserID) {
		GenRow row = new GenRow();
		if (repUserID != null && repUserID.length() != 0) {
			row.setViewSpec("UserView");
			row.setParameter("UserID", repUserID);
		} else {
			row.setViewSpec("SetupView");
			row.setParameter("SetupID", "Default");
		}
		row.doAction("select");
		return row;
	}

	/**
	 * Runs through the main client jar and the developer jar to list all the supported pdf
	 * templates and their corresponding drip count
	 * 
	 * @return
	 */
	public static ArrayList<String[]> getAllPDFTemplateInfo() {
		ArrayList<String[]> pdfTemplates = new ArrayList<String[]>();

		String mainSpecificJarName = null;
		File mainJarPath = new File(InitServlet.getRealPath() + "/WEB-INF/lib/");
		File developerjarPath = new File(InitServlet.getRealPath() + "/WEB-INF/lib/developer");
		File runwayjarPath = new File(InitServlet.getRealPath() + "/WEB-INF/lib/runway-dev.jar");

		// List pdf templates in main jar eg: Henley-runway.jar
		String[] children = mainJarPath.list();

		if (children != null) {
			for (int i = 0; i < children.length; i++) {
				String filename = children[i];
				if ((filename.endsWith("runway-dev.jar") && !filename.startsWith("runway-dev.jar")) || (filename.endsWith("runway.jar") && !filename.startsWith("runway.jar")))
					mainSpecificJarName = mainJarPath + File.separator + filename;
			}
		}

		String pkgname = "com.sok.clients.pdf";

		if (mainSpecificJarName != null) {
			pkgname = pkgname.replace('.', '/');

			getTemplateInfoForJar(new File(mainSpecificJarName), pdfTemplates, pkgname, false);

		} else {
			// Add all the default templates
			pdfTemplates.add(new String[] { "By Estate", "0" });
			pdfTemplates.add(new String[] { "By Plan", "0" });
			pdfTemplates.add(new String[] { "By Estate and Lot", "0" });
			pdfTemplates.add(new String[] { "Detail", "1" });
			pdfTemplates.add(new String[] { "Real Estate List", "0" });
			pdfTemplates.add(new String[] { "Real Estate Detail", "0" });
			pdfTemplates.add(new String[] { "Land Property List", "0" });
		}
		
		getTemplateInfoForJar(runwayjarPath, pdfTemplates, "com/sok/runway/crm/cms/properties/pdf/templates", false);
		
		pkgname = "com.sok.dev";
		pkgname = pkgname.replace('.', '/');

		// List pdf templates in developer jars eg: developer-peet-dev.jar

		if (developerjarPath.listFiles() != null && developerjarPath.listFiles().length > 0) {
			pdfTemplates.add(new String[] { "Developer Detail", "1" });
		}
		return pdfTemplates;
	}
	
	public static ArrayList<String[]> getAdvancedPDFTemplateInfo() {
		ArrayList<String[]> pdfTemplates = new ArrayList<String[]>();
		if ("true".equals(InitServlet.getSystemParam("PDF-AdvancedServer")) && StringUtils.isNotBlank(InitServlet.getSystemParam("PDF-AdvancedServerHost"))) {
			AdvancedPDFServlet advPDF = new AdvancedPDFServlet();
			try {
				JSONObject json = advPDF.getPDFTemplates("");
				JSONArray ary = (JSONArray) json.get("Templates");
				for(int i =0; i < ary.size(); i++) {
					String id = (String)((JSONObject)ary.get(i)).get("ID");
					String name = (String)((JSONObject)ary.get(i)).get("Name");
					pdfTemplates.add(new String[] { name, id });
				}
				
			} catch (Exception e) {
				logger.error("Unable to contact PDF server {} ", e);
				e.printStackTrace();
			}
		}
		return pdfTemplates;
	}

	private static void getTemplateInfoForJar(File inputJarFile, ArrayList<String[]> pdfTemplates, String pkgname, boolean developerJar) {
		try {
			JarFile jarFile = new JarFile(inputJarFile);
			Enumeration<JarEntry> entries = jarFile.entries();
			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				String entryName = entry.getName();

				// send pkgname if only a particular package needs to be checked, null otherwise

				if (pkgname == null || (entryName.startsWith(pkgname) && entryName.length() > (pkgname.length() + "/".length()))) {
					String className = entryName.replace('/', '.').replace('\\', '.').replace(".class", "");

					try {
						Class c = developerJar ? DeveloperPdfGenerator.loadClass(className) : Class.forName(className);

						Method nameM = c.getDeclaredMethod("getPdfSimpleName");
						Method countM = c.getDeclaredMethod("getDripCount");

						if (nameM != null) {
							try {
								Object obj = c.newInstance();
								String name = (String) nameM.invoke(obj, (Object[]) null);
								String count = (String) countM.invoke(obj, (Object[]) null);
								pdfTemplates.add(new String[] { name, count });
							} catch (Exception e1) {
								logger.error("Object Error " + e1.getMessage());
							}
						}
					} catch (Exception e) {
						// System.out.println("Unable to process className = " + className + e);
					}
				}
			}
		} catch (IOException e) {
			logger.debug("Ignoring error " + e);
		}
	}
	
	public static String getDeveloperPDFLogo(PropertyEntity property) 
	{
		 
		String logoPath = "";
		
		String sysParam = InitServlet.getSystemParam("PDFDEV-BrandLogo");
		
		GenRow logoQuery = new GenRow();
		GenRow linkedDocID = new GenRow();
		
		try
		{
			if(StringUtils.isNotBlank(sysParam)) 
			{
				String prodID = null;
				if(sysParam.equalsIgnoreCase("Brand")) 
					prodID = property.getString("BrandProductID");
				else if(sysParam.equalsIgnoreCase("Range"))
					prodID = property.getString("RangeProductID");
				
				if(StringUtils.isNotBlank(prodID))
				{
					linkedDocID.setTableSpec("Products");
					linkedDocID.setColumn("ProductID", prodID);
					linkedDocID.setParameter("-select0", "ThumbnailImage");
					linkedDocID.doAction(GenerationKeys.SELECTFIRST);
					linkedDocID.getResults();
					System.out.println("get Product:"+linkedDocID.getStatement());
					
					String docID = linkedDocID.getString("ThumbnailImage");
					if(StringUtils.isNotBlank(docID))
					{
						logoQuery.setTableSpec("Documents");
						logoQuery.setParameter("DocumentID",docID);
						logoQuery.setParameter("-select0","FilePath");
						logoQuery.doAction(GenerationKeys.SELECTFIRST);
						String tmpPath = logoQuery.getString("FilePath");
						if(StringUtils.isNotBlank(tmpPath))
							logoPath = tmpPath.replace(" ", "%20");
					}
				}
			}
		}
		catch(Exception ex)
		{
			 ex.printStackTrace() ;
			logger.debug("Exception in retrieveing logo path and falling to the default :" +ex);
			return logoPath;
		}
		finally
		{
			logoQuery.close();
			linkedDocID.close();
		}
		return logoPath;
	}
	
	public static String getDeveloperPDFClientName(HttpServletRequest request, PropertyEntity product) {
		String clientName = InitServlet.getSystemParam("ClientNameForDeveloperPdfs");
		logger.debug("***************** ClientNameForDeveloperPdfs is set to : " + clientName);

		if ("Range".equalsIgnoreCase(clientName)) {
			clientName = product.getString("RangeName");
		} else if ("Brand".equalsIgnoreCase(clientName)) {
			clientName = product.getString("BrandName");
		} else if ("RangeDescription".equalsIgnoreCase(clientName)) {
			String rangeID = product.getString("RangeProductID");
			GenRow range = RunwayUtil.getProduct(request, rangeID);			
			logger.debug("Range Desc:", range.getString("Description"));
			logger.debug("Range Desc Statement :", range.getStatement());
			clientName = range.getString("Description");
		}
		return clientName;
	}
	
	public static UserInfo getProductLinkedSalesRep(String productID) 
	{
		UserInfo userInfo = new UserInfo();

		GenRow row = new GenRow();
		try
		{
			if (productID != null && productID.length() != 0) {
				row.setViewSpec("ProductSecurityGroupView");
				row.setParameter("ProductID", productID);
				row.setParameter("RepUserID", "!EMPTY+NULL");
				row.doAction("selectfirst");
				row.getResults();
				userInfo.setUserID(row.getData("RepUserID"));
				userInfo.setFirstName(row.getString("RepFirstName"));
				userInfo.setLastName(row.getString("RepLastName"));
				userInfo.setPhoneNumber(row.getString("RepPhone"));
				userInfo.setEmail(row.getString("RepEmail"));
				userInfo.setMobileNumber(row.getString("RepMobile"));
			}
		}
		catch(Exception ex)
		{
			 ex.printStackTrace() ;
			logger.debug("Exception in retrieveing Sales Rep Info from ProductSecurityGroupView:" +ex);
		}
		finally
		{
			row.close();
		}
		return userInfo;
	}
	
}
