package com.sok.runway.crm.cms.properties;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

public class Image extends GenRow {
	
	private static final Logger logger = LoggerFactory.getLogger(Image.class);

	final PropertyType propertyType = PropertyType.Facade;
	static final String VIEWSPEC = "LinkedDocumentView";
	final static String LinkedProductID = "LinkedProductID";
	final static String ProductID = "ProductID";
	private Image facade = null;

	public Image(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
	}

	public Image(HttpServletRequest request, String productID) {
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}

	public Image(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}

	public Image load(String productID) {
		setParameter(ProductID, productID);
		//setParameter(PropertyFactory.PRODUCTID, productID);
		if (isSet(ProductID)) {
			doAction(getDBAction());
			if(!isSuccessful()) {
				// if we can't find it view the Product, lets try direct
				remove("ProductID");
				setParameter(LinkedProductID, productID);
				if (isSet(LinkedProductID)) {
					doAction(getDBAction());
					if(!isSuccessful()) {
						logger.debug("ProductID supplied was not found [%1$s]", productID);
						//doAction(GenerationKeys.SEARCH);
						logger.debug(getStatement());
					}
				}
			}
		}
		return this;
	}

	@Override
	public void clear() {
		facade = null;
		super.clear();
	}

	@Override
	public Object get(String key) {
		if ("facade".equals(key))
			return getFacade();
		return super.get(key);
	}

	public Image getFacade() {
		if (facade == null && isSuccessful()) {
			facade = new Image(getConnection()).load(getData("ProductID"));
			if (!facade.isSuccessful()) {
				facade = null;
			}
		}
		return facade;
	}

	public String getFacadeName() {
		return getString("Name");
	}

	protected String getDBAction() {
		return GenerationKeys.SELECTFIRST;
	}
}
