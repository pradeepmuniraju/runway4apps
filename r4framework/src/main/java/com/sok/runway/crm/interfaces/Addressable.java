package com.sok.runway.crm.interfaces;

import com.sok.runway.crm.*;
import java.util.*;

public interface Addressable {
   
   public static final String STREET = "Street";
   public static final String STREET2 = "Street2";
   public static final String CITY = "City";
   public static final String STATE = "State";
   public static final String POSTCODE = "Postcode";
   public static final String COUNTRY = "Country";
   
   public Map getAddress(String type);
   
   public Map getAddress();
   
   public void setAddress(String type, Map address);
   
   public void setAddress(Map address);
   
   public String getField(String Key);
   
   public void setField(String Key, Object value);
}