package com.sok.runway.crm.orders.pdf.templates;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.regex.Matcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrTokenizer;
import org.apache.velocity.app.VelocityEngine;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.ActionBean;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.framework.TableBean;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;
import com.sok.runway.crm.orders.pdf.AbstractOrderPDF;
import com.sok.runway.externalInterface.beans.Order;
import com.sok.runway.externalInterface.beans.QuoteView;
import com.sok.runway.externalInterface.beans.QuoteViewPage;
import com.sok.runway.externalInterface.beans.QuoteViewPageOptions;
import com.sok.runway.externalInterface.beans.QuoteViewPageOptions.DocumentOption;
import com.sok.runway.externalInterface.beans.User;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.OrderAnswer;
import com.sok.runway.externalInterface.beans.shared.OrderContact;
import com.sok.runway.externalInterface.beans.shared.OrderItem;
import com.sok.runway.externalInterface.beans.shared.OrderProfile;
import com.sok.runway.externalInterface.beans.shared.Profile;
import com.sok.runway.externalInterface.resources.UtilityResource.ContactLocation;
import com.sok.runway.offline.ThreadManager;
import com.sok.service.crm.DocumentService;
import com.sok.service.crm.OrderService;
import com.sok.service.crm.ProfileService;
import com.sok.service.crm.QuoteViewService;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.FacadeService;
import com.sok.service.crm.cms.properties.FacadeService.Facade;
import com.sok.service.crm.cms.properties.PlanService;
import com.sok.service.crm.cms.properties.PlanService.HLPackage;
import com.sok.service.crm.cms.properties.PlanService.Plan;

//import jdk.nashorn.internal.ir.WhileNode;


/**
 * Initial version of the split out into client specific and non-specific. 
 * @author mike
 *
 */
public class OrderPdf extends AbstractOrderPDF {

	private static final Logger logger = LoggerFactory.getLogger(OrderPdf.class);
	private static final OrderService os = OrderService.getInstance();
	private static final FacadeService fs = FacadeService.getInstance();
	private static final BaseColor headerBarBackground = new BaseColor(204,208,225);
	private static final BaseColor panelBackground = rgb(227, 228, 230);

	private static final BaseColor costTotalBackground = new BaseColor(196,198,199);

	private static BaseColor rgb(int r, int g, int b) {
		return new BaseColor(r,g,b);
	}

	final DateFormat longdf = new SimpleDateFormat("dd MMMM yyyy");

	private static final Paragraph BLANK = new Paragraph("");
	private static final int designWidth = 867; 
	private final int paddingRight = 15;

	private static BaseFont calibri = null;
	private static BaseFont forzaBold = null; 
	private static BaseFont forzaMedium = null;

	private String pageHeading = "";

	static {
		try {
			String forzaPath = InitServlet.getRealPath("/specific/pdf/fonts/Forza/");
			String calibriPath = InitServlet.getRealPath("/specific/pdf/fonts/Calibri/");

			calibri = BaseFont.createFont(calibriPath + "/Calibri.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
			forzaBold = BaseFont.createFont(forzaPath + "/Forza-Bold.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
			forzaMedium = BaseFont.createFont(forzaPath + "/Forza-Medium.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);

		} catch (Exception e) {
			logger.error("Ignoring Error while registering fonts. Error: ", e);
		}
	}

	/* convert design pixels to whatever this uses */
	private static BaseColor quoteColor = new BaseColor(23, 19, 20);
	private static BaseColor quotePurple = new BaseColor(96, 97, 119);
	private static BaseColor quoteMouve = new BaseColor(129, 127, 164);

	private static Font quotationHeader = new Font(forzaMedium, 16f , Font.NORMAL, quoteColor);
	private static Font quotationSubHeader = new Font(forzaMedium, 16f , Font.NORMAL, BaseColor.WHITE);
	private static final Font subHeader =  new Font(forzaBold, 10f, Font.NORMAL,  quoteColor);
	private static final Font dateText =  new Font(forzaMedium, 10f, Font.NORMAL,  quoteColor);
	private static final Font quoteCoverPageQuotationHeader = new Font(forzaMedium, 24f , Font.NORMAL, quoteColor);

	private static final Font boldFont = new Font(forzaMedium, 15f, Font.NORMAL, quoteColor);

	private static final Font detailsFont = new Font(calibri,  8f, Font.NORMAL, quoteColor);
	private static final Font detailsFontBold = new Font(calibri,  8f, Font.BOLD, quoteColor);

	private static final Font textFont = new Font(calibri,  10f, Font.NORMAL, quoteColor);
	private static final Font textFontBold = new Font(forzaMedium,  14f, Font.NORMAL, quoteColor);

	private static final Font dripTitleFont =  new Font(forzaMedium, 16f, Font.NORMAL, quoteColor);

	private static final Font costDisclaimerFont = new Font(forzaMedium, 12f, Font.NORMAL, quoteColor);
	private static final Font costSubtotalFont =  new Font(forzaMedium, 10f, Font.NORMAL, quoteColor);
	private static final Font costTotalFont =  new Font(calibri, 10f, Font.BOLD, quoteColor);
	private static final Font costTotalFontBold =  new Font(forzaBold, 12f, Font.NORMAL, quoteColor);

	private static final Font footerTitle =  new Font(calibri, 9f, Font.NORMAL, BaseColor.WHITE);
	private static final Font footerContact = new Font(calibri, 9f, Font.NORMAL, BaseColor.WHITE);
	private static final Font footerValidity =  new Font(calibri, 8f, Font.NORMAL, BaseColor.WHITE);

	private static float[] getDesignWidths(float... widths) {
		return getWidths(designWidth, widths);
	}
	private static float[] getWidths(int tableWidth, float... widths) {
		int max = widths.length -1;
		if(max == -1) 
			return widths;

		float[] out = new float[widths.length];
		for(int i=0; ; i++) {
			out[i] = widths[i] / tableWidth; 
			if(i == max) 
				break;
		}
		return out;
	}

	public static Phrase getRepParagraph(String repName) {
		final Phrase p = new Phrase();
		p.add(new Chunk("Quote prepared by - " + repName + "\n", footerTitle));

		return p;
	}
	public static Phrase getRepParagraph(String repMobile, String repPhone, String repEmail) {
		final Phrase p = new Phrase();
		String txt = "";
		if (StringUtils.isNotBlank(repMobile)) {
			txt += "Mobile: " + repMobile;
		} else if (StringUtils.isNotBlank(repPhone)) {
			txt += "Phone: " + repPhone;
		}
		txt += "   " + "Email: " + repEmail;
		p.add(new Chunk(txt.trim(), footerContact));

		return p;
	}

	public static Phrase getValidity(long days, Date expiryDate, QuoteView qView) {
		final DateFormat df = new SimpleDateFormat("dd MMM yyyy");
		StringBuilder validityText = new StringBuilder().append("This quote is valid for ").append(days).append(" days and expires on ").append(df.format(expiryDate)).append(".");

		if(qView.footerText != null && !qView.footerText.equalsIgnoreCase(""))
			validityText = new StringBuilder().append(qView.footerText);

		return new Phrase(validityText.toString() , footerValidity);
	}	

	private void setHeader(PdfPTable table) {
		table.getDefaultCell().setBackgroundColor(panelBackground);
		table.getDefaultCell().setPadding(7);
		table.getDefaultCell().setPaddingTop(3);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorderWidth(0);
	}

	private void setHeaderWithoutBG(PdfPTable table) {
		table.getDefaultCell().setPadding(20);
		//table.getDefaultCell().setPaddingTop(3);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorderWidth(0);
	}

	private void setNormal(PdfPTable table) {
		table.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		table.getDefaultCell().setPadding(20);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorderWidth(0);
	}

	private PdfPTable getHeadingTable(QuoteView qView, Date date, String heading) throws DocumentException {
		PdfPTable headingTable = new PdfPTable(2);
		headingTable.setWidthPercentage(100f);
		headingTable.getDefaultCell().setBorder(0);
		headingTable.setWidths(new float[]{2,1});
		headingTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
		headingTable.getDefaultCell().setPaddingLeft(20);
		headingTable.getDefaultCell().setPaddingRight(20);
		headingTable.getDefaultCell().setPaddingTop(15);
		headingTable.getDefaultCell().setPaddingBottom(0);
		headingTable.addCell(new Paragraph(pageHeading, quotationHeader));
		//headingTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);

		String imagePath = "specific/images/logolarge.jpg";
		if(qView.headerImage != null && !qView.headerImage.equalsIgnoreCase("") && qView.headerImagePath != null && !qView.headerImagePath.equalsIgnoreCase(""))
			imagePath = qView.headerImagePath;
		Image rim = PDFUtil.createImage(PropertyEntity.resizeImage(imagePath, 364, 178, PropertyEntity.ResizeOption.Fit));

		if(rim == null) { 
			headingTable.addCell(BLANK);
		} else { 
			headingTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			headingTable.getDefaultCell().setRowspan(2);
			headingTable.addCell(rim);
			headingTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			headingTable.getDefaultCell().setRowspan(1);
		}
		headingTable.getDefaultCell().setPaddingTop(5);
		headingTable.getDefaultCell().setPaddingBottom(10);
		final DateFormat df = new SimpleDateFormat("dd MMMM yyyy");
		headingTable.addCell(new Paragraph(df.format(date),subHeader));

		headingTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		headingTable.getDefaultCell().setPadding(0);
		headingTable.getDefaultCell().setBorder(0);
		PdfPCell cell = new PdfPCell(BLANK);
		cell.setBorder(0);
		cell.setBackgroundColor(quotePurple);
		cell.setFixedHeight(10f);
		cell.setColspan(2);
		headingTable.addCell(cell); // quote bar*/

		if (StringUtils.isNotBlank(heading)) {
			cell = new PdfPCell(new Phrase(heading, quotationSubHeader));
			cell.setBorder(0);
			cell.setBackgroundColor(quoteMouve);
			cell.setFixedHeight(35f);
			cell.setColspan(2);
			//cell.addElement(new Paragraph(heading));
			cell.setPaddingTop(8);
			cell.setPaddingLeft(20);
			headingTable.addCell(cell); // quote bar*/
		}

		return headingTable;
	}

	private PdfPTable getPlainHeadingTable(String pageHeading) throws DocumentException {
		PdfPTable headingTable = new PdfPTable(2);
		headingTable.setWidthPercentage(100f);
		headingTable.getDefaultCell().setBorder(0);
		headingTable.setWidths(new float[]{2,1});
		headingTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
		headingTable.addCell(new Phrase(pageHeading, quotationHeader));
		headingTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);

		String imagePath = "specific/images/logolarge.jpg";
		Image rim = PDFUtil.createImage(PropertyEntity.resizeImage(imagePath, 364, 178, PropertyEntity.ResizeOption.Fit));

		if(rim == null) { 
			headingTable.addCell(BLANK);
		} else { 
			headingTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			headingTable.addCell(rim);
			headingTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		}
		return headingTable;
	}

	private PdfPTable getHeadingTableLeft(String pageHeading, QuoteView qView) throws DocumentException {
		PdfPTable headingTable = new PdfPTable(2);
		headingTable.setWidthPercentage(100f);
		headingTable.getDefaultCell().setBorder(0);
		headingTable.setWidths(new float[]{1, 2});

		String imagePath = "specific/images/logolarge.jpg";
		if(qView.headerImage != null && !qView.headerImage.equalsIgnoreCase("") && qView.headerImagePath != null && !qView.headerImagePath.equalsIgnoreCase(""))
			imagePath = qView.headerImagePath;

		Image rim = PDFUtil.createImage(PropertyEntity.resizeImage(imagePath, 364, 178, PropertyEntity.ResizeOption.Fit));
		if(rim == null) { 
			headingTable.addCell(BLANK);
		} else {
			headingTable.addCell(rim);
		}
		headingTable.getDefaultCell().setColspan(2);
		headingTable.addCell(BLANK);
		headingTable.getDefaultCell().setColspan(3);
		headingTable.addCell(BLANK);
		headingTable.addCell(new Phrase(pageHeading, quotationHeader));

		return headingTable;
	}

	private void addquoteCoverPage(com.itextpdf.text.Document d, PdfWriter writer, QuoteViewPage qvp, Order order, List<OrderItem> orderItems, com.sok.service.crm.cms.properties.FacadeService.Facade f, ContactLocation orderLocation, Plan p, HLPackage pkg, List<OrderProfile> orderProfiles, QuoteView qView, Map<String, Boolean> customPageOptions) throws DocumentException {
		//addDocumentPages(d, writer, "1", "/specific/pdf/images/quotes/Quote_bg1.pdf");
	}

	private void addCoverPage(com.itextpdf.text.Document d, QuoteViewPage qvp, Order order, List<OrderItem> orderItems, com.sok.service.crm.cms.properties.FacadeService.Facade f, ContactLocation orderLocation, Plan p, HLPackage pkg, List<OrderProfile> orderProfiles, QuoteView qView, Map<String, Boolean> customPageOptions) throws Exception {
		d.newPage();
		List<OrderContact> contactList = order.getLinkedContacts();

		PdfPTable pageTable = new PdfPTable(3);
		pageTable.setWidths(getDesignWidths(474, 32, 361));
		pageTable.setWidthPercentage(100f);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.getDefaultCell().setColspan(3);
		//line break
		//pageTable.addCell(BLANK);

		//pageTable.getDefaultCell().setPaddingLeft(20);
		//pageTable.getDefaultCell().setPaddingRight(20);
		//pageTable.getDefaultCell().setPaddingTop(20);
		//pageTable.getDefaultCell().setPaddingBottom(10);
		//pageTable.getDefaultCell().setColspan(3);

		String quote = getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Build Type", orderProfiles));

		if("HNL".equals(quote)) {
			//innerTable.addCell(new Paragraph("Costs - House and Land",subHeader));	
			quote = "H&L Package";
		} else if("OJ".equals(quote)) {
			quote = "Order Job";
		} else if("KDR".equals(quote)) {
			quote = "Knock Down Rebuild";	
		} else {
			quote = "Unknown";
		}

		try {
		pageHeading = (p != null && p.HomeName != null) ? 
				String.format("Quotation #%1$s\n%2$s - %3$s %4$s", order.getOrderNum(), quote, p.HomeName, p.Name)
				: String.format("Quotation #%1$s - %2$s","Preview","PDF");	


				pageTable.addCell(getHeadingTable(qView, order.getDateOfSale(), null));
		} catch (Exception e) {
			e.printStackTrace();
		}

				PdfPTable hugeImage = null;
				boolean image = false;
				try { 
					Image i = PDFUtil.createImage(PropertyEntity.resizeImage(f.ImagePath, 800, 437, PropertyEntity.ResizeOption.Crop));
					if (i == null) i = PDFUtil.createImage(f.ImagePath, 800, 437);
					if(i != null) {
						hugeImage = new PdfPTable(1);
						hugeImage.getDefaultCell().setBorder(0);
						hugeImage.getDefaultCell().setPadding(0);
						hugeImage.addCell(i);
						/*
				hugeImage.getDefaultCell().setPaddingTop(0);
				hugeImage.addCell(new Phrase("This image is for illustration purposes only", textFont));
						 */
						pageTable.addCell(hugeImage);
						image = true;
						//pageTable.addCell(i);
					}
				} catch (Exception e) {
					logger.error("Error getting facade image", e);
				}
				if(!image) {
					try { 
						Image i = PDFUtil.createImage(PropertyEntity.resizeImage(p.HomeImagePath, 800, 437, PropertyEntity.ResizeOption.Crop));
						if (i == null) i = PDFUtil.createImage(p.HomeImagePath, 800, 437);
						if(i != null) {
							hugeImage = new PdfPTable(1);
							hugeImage.getDefaultCell().setBorder(0);
							hugeImage.getDefaultCell().setPadding(0);
							hugeImage.addCell(i);
							/*
					hugeImage.getDefaultCell().setPaddingTop(5);
					hugeImage.addCell(new Phrase("This image is for illustration purposes only", textFont));
							 */
							pageTable.addCell(hugeImage);
							image = true;
						} 
					} catch (Exception e2) {
						logger.error("Error getting home image", e2);
					}
				}

				if(!image) {
					try { 
						Image i = PDFUtil.createImage(PropertyEntity.resizeImage("crm/orders/views/images/no_image.png", 800, 437, PropertyEntity.ResizeOption.Crop));
						if(i != null) {
							pageTable.addCell(i);
							image = true;
						} 
					} catch (Exception e2) {
						logger.error("Error getting no-image", e2);
					}
				}

				if(!image) {
					pageTable.addCell(BLANK);
				}

				//setHeaderWithoutBG(pageTable);
				//setNormal(pageTable);

				/*
				 *
		pageTable.addCell(new Paragraph("Prepared for",subHeader));

				 */
				PdfPTable innerTable = new PdfPTable(5);
				innerTable.setWidths(getDesignWidths(0, 120, 253, 10, 253));
				innerTable.setWidthPercentage(100f);
				innerTable.getDefaultCell().setPadding(10);
				innerTable.getDefaultCell().setPaddingLeft(0);

				innerTable.getDefaultCell().setBorder(0);
				//innerTable.getDefaultCell().setBackgroundColor(panelBackground);

				innerTable.getDefaultCell().setColspan(5);
				innerTable.getDefaultCell().setBorder(2);
				innerTable.getDefaultCell().setBorderColor(new BaseColor(80,80,80));
				innerTable.addCell(new Paragraph("Prepared for",subHeader));

				innerTable.getDefaultCell().setColspan(1);

				innerTable.getDefaultCell().setBorder(0);


				int ci = 0;

				if(contactList != null) { 
					Collections.sort(contactList, new Comparator<OrderContact>() {
						public int compare(OrderContact o1, OrderContact o2) {
							if("primary".equals(o1.getRole())) {
								return -1;
							}
							return 1;
						}
					});

					OrderContact oc1 = contactList.get(0);
					OrderContact oc2 = null;
					if (contactList.size() > 1) oc2 = contactList.get(1);

					innerTable.getDefaultCell().setPadding(5);
					innerTable.getDefaultCell().setColspan(3);
					innerTable.getDefaultCell().setPaddingLeft(0);
					innerTable.addCell(new Paragraph(oc1.getName(), subHeader));
					innerTable.getDefaultCell().setColspan(1);
					innerTable.getDefaultCell().setPaddingLeft(5);
					innerTable.addCell(BLANK);
					if (oc2 != null)
						innerTable.addCell(new Paragraph(oc2.getName(), subHeader));
					else
						innerTable.addCell(BLANK);

					innerTable.getDefaultCell().setPadding(3);
					innerTable.getDefaultCell().setColspan(2);
					innerTable.getDefaultCell().setPaddingLeft(0);
					innerTable.addCell(new Paragraph("Phone:", detailsFontBold));
					innerTable.getDefaultCell().setColspan(1);
					innerTable.getDefaultCell().setPaddingLeft(5);
					innerTable.addCell(new Paragraph(oc1.getPhone(), detailsFont));
					innerTable.addCell(BLANK);
					if (oc2 != null)
						innerTable.addCell(new Paragraph(oc2.getPhone(), detailsFont));
					else
						innerTable.addCell(BLANK);

					innerTable.getDefaultCell().setColspan(2);
					innerTable.getDefaultCell().setPaddingLeft(0);
					innerTable.addCell(new Paragraph("Mobile:", detailsFontBold));
					innerTable.getDefaultCell().setColspan(1);
					innerTable.getDefaultCell().setPaddingLeft(5);
					innerTable.addCell(new Paragraph(oc1.getPhone(), detailsFont));
					innerTable.addCell(BLANK);
					if (oc2 != null)
						innerTable.addCell(new Paragraph(oc2.getPhone(), detailsFont));
					else
						innerTable.addCell(BLANK);

					innerTable.getDefaultCell().setColspan(2);
					innerTable.getDefaultCell().setPaddingLeft(0);
					innerTable.addCell(new Paragraph("Email:", detailsFontBold));
					innerTable.getDefaultCell().setColspan(1);
					innerTable.getDefaultCell().setPaddingLeft(5);
					innerTable.addCell(new Paragraph(oc1.getMobile(), detailsFont));
					innerTable.addCell(BLANK);
					if (oc2 != null)
						innerTable.addCell(new Paragraph(oc2.getMobile(), detailsFont));
					else
						innerTable.addCell(BLANK);

					innerTable.getDefaultCell().setColspan(2);
					innerTable.getDefaultCell().setPaddingLeft(0);
					innerTable.addCell(new Paragraph("Address:", detailsFontBold));
					innerTable.getDefaultCell().setColspan(1);
					innerTable.getDefaultCell().setPaddingLeft(5);
					innerTable.addCell(new Paragraph(oc1.getPhysicalAddress().getStreet(), detailsFont));
					innerTable.addCell(BLANK);
					if (oc2 != null)
						innerTable.addCell(new Paragraph(oc2.getPhysicalAddress().getStreet(), detailsFont));
					else
						innerTable.addCell(BLANK);

					innerTable.getDefaultCell().setColspan(2);
					innerTable.getDefaultCell().setPaddingLeft(0);
					innerTable.addCell(new Paragraph("Suburb:", detailsFontBold));
					innerTable.getDefaultCell().setColspan(1);
					innerTable.getDefaultCell().setPaddingLeft(5);
					innerTable.addCell(new Paragraph(oc1.getPhysicalAddress().getCity(), detailsFont));
					innerTable.addCell(BLANK);
					if (oc2 != null)
						innerTable.addCell(new Paragraph(oc2.getPhysicalAddress().getCity(), detailsFont));
					else
						innerTable.addCell(BLANK);

					innerTable.getDefaultCell().setColspan(2);
					innerTable.getDefaultCell().setPaddingLeft(0);
					innerTable.addCell(new Paragraph("Postcode:", detailsFontBold));
					innerTable.getDefaultCell().setColspan(1);
					innerTable.getDefaultCell().setPaddingLeft(5);
					innerTable.addCell(new Paragraph(oc1.getPhysicalAddress().getPostcode(), detailsFont));
					innerTable.addCell(BLANK);
					if (oc2 != null)
						innerTable.addCell(new Paragraph(oc2.getPhysicalAddress().getPostcode(), detailsFont));
					else
						innerTable.addCell(BLANK);
					pageTable.getDefaultCell().setColspan(1);
					pageTable.getDefaultCell().setPaddingLeft(20);
					pageTable.addCell(innerTable);

				}

				pageTable.getDefaultCell().setColspan(1);
				pageTable.getDefaultCell().setPaddingTop(20);
				pageTable.addCell(BLANK);
				pageTable.getDefaultCell().setPaddingTop(0);
				pageTable.getDefaultCell().setColspan(1);

				innerTable = new PdfPTable(3);
				//innerTable.setWidths(getWidths(225, 12, 90, 127));
				innerTable.setWidths(getWidths(225, 12, 100, 117));
				innerTable.setWidthPercentage(100f);
				innerTable.getDefaultCell().setPadding(10);

				innerTable.getDefaultCell().setColspan(3);
				innerTable.getDefaultCell().setBorder(2);
				innerTable.getDefaultCell().setBorderColor(new BaseColor(80,80,80));
				innerTable.addCell(new Paragraph("Price",subHeader));

				innerTable.getDefaultCell().setColspan(1);

				innerTable.getDefaultCell().setBorder(0);

				QuoteViewPageOptions po = qvp.SelectedOptions;

				List<SummaryCost> list = new ArrayList<SummaryCost>();
				List<SummaryCost> landSubList = new ArrayList<SummaryCost>();
				List<SummaryCost> postList = new ArrayList<SummaryCost>();

				try {
					if(po.Baseprice != null && po.Baseprice.pdf) {
						double basePrice = getBasePrice(orderItems, p, pkg), lotCost = 0;
						if(qvp.SelectedOptions != null && qvp.SelectedOptions.SeparateLand != null && qvp.SelectedOptions.SeparateLand.BooleanValue == true) {
							if(pkg.LotCost != 0) {
								lotCost = pkg.LotCost * 1.1;
								basePrice -= lotCost;
							}
						}
						//list.add(new SummaryCost("Base Price:", basePrice));
						if("HNL".equals(getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Build Type", orderProfiles)))) 
							list.add(new SummaryCost("H&L Package Price:", basePrice));
						else
							list.add(new SummaryCost("House Price:", basePrice));

						try {
							if(lotCost != 0) 
							{
								list.add(new SummaryCost("Land", lotCost));
								if(pkg.PackageCosts != null) {
									for(int z = 0 ; z < pkg.PackageCosts.size(); z++) {
										String categoryName = pkg.PackageCosts.get(z).Category;
										if(categoryName!=null && customPageOptions!=null && customPageOptions.get(categoryName) != null && customPageOptions.get(categoryName).booleanValue())
											landSubList.add(new SummaryCost(categoryName, "Included"));
									}
								}
							}
						} catch (Exception ee) {
							ee.printStackTrace();
						}
					}
				}catch(Exception ee){
					ee.printStackTrace();
				}

				try {
					if(po.Options != null && po.Options.pdf) {
						list.add(new SummaryCost("Options:", getOptionsTotal(orderItems)));
						double ec = getEstateCovenantPrice(orderItems);
						if(ec > 0) {
							list.add(new SummaryCost("Estate Covenant Requirements", 0));	
						}
					}
				}catch(Exception ee){
					ee.printStackTrace();
				}

				try {
					if(po.CustomVariations != null && po.CustomVariations.pdf) {
						list.add(new SummaryCost("Non Standard Variations:", getCustomTotal(orderItems)));
					}
				}catch(Exception ee){
					ee.printStackTrace();
				}

				try {
					if(po.Facade != null && po.Facade.pdf) {
						OrderItem facade = getFacadeItem(orderItems);
						if(facade != null) 
							list.add(new SummaryCost("Facade - " + facade.getName(), facade.getTotalCost()));
					}
				}catch(Exception ee){
					ee.printStackTrace();
				}

				JSONObject dripContent = null;
				JSONParser jp = new JSONParser();
				Object o = null; String t = null;


				if(po.DRIPS != null && po.DRIPS.pdf){
					List<OrderItem> dripItems = getDripItems(orderItems);
					double postDrip = 0, dripTotal = 0; 
					for(OrderItem item: dripItems) {
						//get drip items, check those that have a cover defined.
						if(item.isIncludeSummary() && StringUtils.isNotBlank(item.getMetaData())) {
							try { 
								Object obj = jp.parse(item.getMetaData());
								if(obj instanceof JSONObject) {
									dripContent = (JSONObject)obj;
								} else {
									logger.error("Got a different instance than expected for MetaData {} in OrderItemID=[{}]", obj.getClass().getName(), item.getOrderItemID());
									continue;
								}
							} catch (ParseException pe) {
								logger.error("Error parsing Drip Meta Data", pe);
								/* attempt to load from drip ? */
								continue;
							}
							String cover = null;

							if((o = dripContent.get("Cover")) != null && o instanceof String && StringUtils.isNotBlank(t = (String)o)) {
								cover = t;
							} 
							if("Post Net".equals(item.getPricingType())) {
								if(cover != null) {
									postList.add(new SummaryCost(t, item.getGST() + item.getCost()));
								} else {
									postDrip += item.getGST() + item.getCost();
								}
							} else if("Inc Net".equals(item.getPricingType())) {
								if(cover != null) {
									list.add(new SummaryCost(t, item.getTotalCost()));
								} else {
									dripTotal += item.getTotalCost();
								}
							} else {	//inc base
								if(cover != null && !"Not Shown".equals(item.getPricingType())) {
									list.add(new SummaryCost(t, 0));
								}
								//base price calc is done seperately in pdf
							}
						}
					}
					/*
			for(PDFDripInfo pdi: dripSummaryList) {
				if(pdi.Cover != null) {
					dripTotal -= pdi.includePrice ? 0 : pdi.cost;
					//OrderItem i = this.getOrderItemByProductId(orderItems, pdi.)
					list.add(new SummaryCost(pdi.Cover, pdi.includePrice ? 0 : pdi.cost));
				}				
			}
					 */
					if(dripTotal != 0) { 
						list.add(new SummaryCost("Promotion", dripTotal));
					}
					if(postDrip != 0) {
						postList.add(new SummaryCost("Promotion", postDrip));
					}
				}

				final List<String> financialSort = getFinancialsSort(qvp.QuoteViewPageID, orderItems);

				/* we dont want them sorted 
		Collections.sort(list, new Comparator<SummaryCost>() {
			public int compare(SummaryCost o1, SummaryCost o2) {
				int ix1 = financialSort.indexOf(o1.title);
				int ix2 = financialSort.indexOf(o2.title);
				if(ix2 == -1) {
					return -1;
				}
				if(ix1 == -1) {
					return 1;
				}
				return (ix1 < ix2) ? -1 : 1; 
			}
		}); */

				Collections.sort(landSubList, new Comparator<SummaryCost>() {
					public int compare(SummaryCost o1, SummaryCost o2) {
						int ix1 = financialSort.indexOf(o1.title);
						int ix2 = financialSort.indexOf(o2.title);
						if(ix2 == -1) {
							return -1;
						}
						if(ix1 == -1) {
							return 1;
						}
						return (ix1 < ix2) ? -1 : 1; 
					}
				});

				if(!postList.isEmpty()) { 

					final List<String> postFinancialSort = getFinancialsSort(qvp.QuoteViewPageID, orderItems);

					if(!postFinancialSort.isEmpty()) { 

						Collections.sort(postList, new Comparator<SummaryCost>() {
							public int compare(SummaryCost o1, SummaryCost o2) {
								int ix1 = postFinancialSort.indexOf(o1.title);
								int ix2 = postFinancialSort.indexOf(o2.title);
								if(ix2 == -1) {
									return -1;
								}
								if(ix1 == -1) {
									return 1;
								}
								return (ix1 < ix2) ? -1 : 1; 
							}
						});
					}
				} 
				NumberFormat mf = NumberFormat.getCurrencyInstance();
				mf.setMaximumFractionDigits(0);

				//innerTable.getDefaultCell().setBackgroundColor(panelBackground);
				innerTable.getDefaultCell().setColspan(1);
				innerTable.getDefaultCell().setPaddingTop(5);
				innerTable.getDefaultCell().setPaddingBottom(7);

				int i = 0;
				double tc = 0.0;
				for(SummaryCost sc: list) {

					//innerTable.addCell(BLANK);
					innerTable.getDefaultCell().setColspan(2);
					innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
					innerTable.addCell(new Paragraph(sc.title, textFont));
					innerTable.getDefaultCell().setColspan(1);
					innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
					innerTable.addCell(new Paragraph(formatMoney(sc.cost, mf), textFont));
					//innerTable.addCell(BLANK);
					if(tc == 0d) {
						innerTable.getDefaultCell().setPaddingTop(0);
					}
					tc += sc.cost;
					if("Land".equalsIgnoreCase(sc.title))
					{
						for(SummaryCost landList: landSubList) 
						{
							//innerTable.addCell(BLANK);
							innerTable.getDefaultCell().setColspan(2);
							innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
							innerTable.addCell(new Paragraph(landList.title, textFont));
							innerTable.getDefaultCell().setColspan(1);
							innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
							innerTable.addCell(new Paragraph("Included", textFont));
							//innerTable.addCell(BLANK);
						}
					}
				}

				//innerTable.getDefaultCell().setBackgroundColor(panelBackground);
				//innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setColspan(2);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);		
				innerTable.addCell(new Paragraph("Sub-Total:", textFont));
				innerTable.getDefaultCell().setColspan(1);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				innerTable.addCell(new Paragraph(formatMoney(tc, mf), textFont));
				//innerTable.addCell(BLANK);

				//innerTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
				innerTable.getDefaultCell().setColspan(3);
				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setColspan(1);

				//innerTable.getDefaultCell().setBackgroundColor(costTotalBackground);

				innerTable.getDefaultCell().setPaddingTop(5);
				innerTable.getDefaultCell().setPaddingBottom(5);

				innerTable.getDefaultCell().setColspan(2);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				innerTable.addCell(new Paragraph("Total", costTotalFontBold));
				innerTable.getDefaultCell().setColspan(1);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				innerTable.addCell(new Paragraph(formatMoney(order.getTotalCost(), mf), costTotalFont));
				innerTable.addCell(BLANK);

				//innerTable.getDefaultCell().setBackgroundColor(panelBackground);
				for(SummaryCost sc: postList) {
					//innerTable.addCell(BLANK);
					innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
					innerTable.addCell(new Paragraph(sc.title, textFont));
					innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
					innerTable.addCell(new Paragraph(formatMoney(sc.cost, mf), textFont));
					//innerTable.addCell(BLANK);
					if(tc == 0d) {
						innerTable.getDefaultCell().setPaddingTop(0);
					}
				}

				innerTable.getDefaultCell().setColspan(3);
				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setColspan(1);

				innerTable.getDefaultCell().setColspan(3);
				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setColspan(1);

				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

				pageTable.getDefaultCell().setBackgroundColor(panelBackground);
				pageTable.getDefaultCell().setRowspan(1);
				pageTable.getDefaultCell().setPaddingLeft(20);
				pageTable.getDefaultCell().setPaddingRight(20);
				pageTable.addCell(innerTable);

				pageTable.getDefaultCell().setRowspan(1);
				pageTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

				innerTable = new PdfPTable(6);
				innerTable.setWidths(getDesignWidths(0, 150, 203, 40, 150, 203));
				innerTable.setWidthPercentage(100f);
				innerTable.getDefaultCell().setPadding(10);
				innerTable.getDefaultCell().setPaddingLeft(0);

				innerTable.getDefaultCell().setBorder(0);
				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setColspan(2);
				innerTable.getDefaultCell().setBorder(2);
				innerTable.getDefaultCell().setBorderColor(new BaseColor(80,80,80));
				innerTable.addCell(new Paragraph("Land",subHeader));

				innerTable.getDefaultCell().setBorder(0);
				innerTable.getDefaultCell().setColspan(1);
				innerTable.addCell(BLANK);

				innerTable.getDefaultCell().setColspan(2);
				innerTable.getDefaultCell().setBorder(2);
				innerTable.getDefaultCell().setBorderColor(new BaseColor(80,80,80));
				innerTable.addCell(new Paragraph("Home",subHeader));

				innerTable.getDefaultCell().setBorder(0);
				innerTable.getDefaultCell().setColspan(1);
				innerTable.getDefaultCell().setPadding(3);

				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph("Lot/Street No:",detailsFontBold));
				innerTable.addCell(new Paragraph(orderLocation.StreetNumber,detailsFont));
				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph("Range:",detailsFontBold));
				innerTable.addCell(new Paragraph(p.RangeName,detailsFont));

				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph("Street:",detailsFontBold));
				innerTable.addCell(new Paragraph(orderLocation.Street,detailsFont));
				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph("Design:",detailsFontBold));
				innerTable.addCell(new Paragraph(p.HomeName, detailsFont));

				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph("Suburb:",detailsFontBold));
				innerTable.addCell(new Paragraph(orderLocation.City,detailsFont));
				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph("Plan (size):",detailsFontBold));
				innerTable.addCell(new Paragraph(p.Name, detailsFont));

				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph("Postcode:",detailsFontBold));
				innerTable.addCell(new Paragraph(orderLocation.Postcode,detailsFont));
				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph("Facade:",detailsFontBold));
				innerTable.addCell(new Paragraph(f.Name,detailsFont));

				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph("Estate:",detailsFontBold));
				innerTable.addCell(new Paragraph(pkg.EstateName,detailsFont));
				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph("Build Region:",detailsFontBold));
				innerTable.addCell(new Paragraph(pkg.RegionName,detailsFont));

				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);

				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);

				pageTable.getDefaultCell().setColspan(1);
				pageTable.addCell(innerTable);
				pageTable.addCell(BLANK);

				innerTable = new PdfPTable(3);
				//innerTable.setWidths(getWidths(225, 12, 90, 127));
				innerTable.setWidths(getWidths(225, 12, 100, 117));
				innerTable.setWidthPercentage(100f);
				innerTable.getDefaultCell().setPadding(10);

				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

				innerTable.getDefaultCell().setColspan(3);
				innerTable.getDefaultCell().setBorder(2);
				innerTable.getDefaultCell().setBorderColor(new BaseColor(80,80,80));
				innerTable.addCell(new Paragraph("Comments",subHeader));

				innerTable.getDefaultCell().setBorder(0);

				innerTable.getDefaultCell().setPaddingBottom(20);
				innerTable.addCell(new Paragraph(order.getNotes(), detailsFont));

				innerTable.getDefaultCell().setPadding(10);
				//innerTable.getDefaultCell().setColspan(3);
				//innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setColspan(1);

				innerTable.getDefaultCell().setColspan(3);
				innerTable.getDefaultCell().setBorder(2);
				innerTable.getDefaultCell().setBorderColor(new BaseColor(80,80,80));
				innerTable.addCell(new Paragraph("Signature:",textFont));

				innerTable.getDefaultCell().setBorder(0);
				innerTable.getDefaultCell().setColspan(3);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setColspan(1);

				pageTable.getDefaultCell().setBackgroundColor(panelBackground);
				pageTable.getDefaultCell().setRowspan(2);
				pageTable.getDefaultCell().setPaddingLeft(20);
				pageTable.getDefaultCell().setPaddingRight(20);
				pageTable.addCell(innerTable);
				//pageTable.addCell(BLANK);

				pageTable.getDefaultCell().setRowspan(1);
				pageTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

				pageTable.addCell(BLANK);

				pageTable.addCell(BLANK);
				pageTable.addCell(BLANK);
				pageTable.addCell(BLANK);


				d.add(pageTable);
	}
	/*
	private void blah() {
		String ssm = getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Site Start Month", orderProfiles)),
				ssy = getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Site Start Year", orderProfiles)),
				siteStart = null;
		if(ssm != null && ssy != null) {
			siteStart = new StringBuilder().append(ssm).append(" ").append(ssy).toString();
		}

		final String[] ldt = { "Lot/Street No:", "Street:","Suburb:","Postcode:","Estate", "Build Region" }; 
		final String[] ldd = { orderLocation.StreetNumber, orderLocation.Street, orderLocation.City, orderLocation.Postcode, pkg.EstateName, pkg.RegionName
		};

		if(ldd[0] != null && ldd[0].toUpperCase().startsWith("LOT ") && ldd[0].length()>4) {
			ldd[0] = ldd[0].substring(4);
		}

		innerTable.getDefaultCell().setPaddingTop(8);
		innerTable.getDefaultCell().setPaddingBottom(4);

		for(int ii=0; ii < ldt.length; ++ii) {
			if(ldd[ii] != null) {
				innerTable.addCell(BLANK);
				innerTable.addCell(new Paragraph(ldt[ii], textFont));
				innerTable.addCell(new Paragraph(ldd[ii], textFont));
			}
			if(ii == 0) {
				innerTable.getDefaultCell().setPaddingTop(3);		
			}
		}
		pageTable.addCell(innerTable);
		pageTable.addCell(BLANK);

		//pageTable.getDefaultCell().setBackgroundColor(headerBarBackground);

		innerTable = new PdfPTable(3);
		innerTable.setWidths(getWidths(225, 12, 100, 117));
		innerTable.setWidthPercentage(100f);
		innerTable.getDefaultCell().setBorder(0);
		innerTable.getDefaultCell().setColspan(3);

		setHeaderWithoutBG(innerTable);
		innerTable.addCell(new Paragraph("Home Details",subHeader));
		setNormal(innerTable);
		innerTable.getDefaultCell().setBackgroundColor(panelBackground);
		innerTable.getDefaultCell().setColspan(1);

		String[] hdt = { "Range Name:", "Design:", "Plan (Size):", "Facade:" }; 
		String[] hdd = { p.RangeName, 
				p.HomeName, p.Name, f.Name };

		boolean regionPricing = "true".equals(InitServlet.getSystemParam("RegionPricingEnabled"));
		if(regionPricing && StringUtils.isNotBlank(order.getProductType()) && order.getProductType().equalsIgnoreCase("Home Plan") && StringUtils.isNotBlank(order.getRegionName())) {
			String[] hdtTmp = { "Range Name:", "Design:", "Plan (size):", "Facade:" }; 
			hdt = hdtTmp;

			String[] hddTmp =  { p.RangeName, p.HomeName, p.Name, f.Name };
			hdd = hddTmp;

			hddTmp = null;
			hdtTmp = null;
		}

		innerTable.getDefaultCell().setPaddingTop(8);
		innerTable.getDefaultCell().setPaddingBottom(4);

		for(int ii=0; ii < hdt.length; ++ii) {
			innerTable.addCell(BLANK);
			innerTable.addCell(new Paragraph(hdt[ii], textFont));
			innerTable.addCell(new Paragraph(hdd[ii], textFont));
			if(ii == 0) {
				innerTable.getDefaultCell().setPaddingTop(3);		
			}
		}
		pageTable.addCell(innerTable);

		pageTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		pageTable.addCell(BLANK);

		pageTable.getDefaultCell().setColspan(5);
		//TODO padding test 
		pageTable.getDefaultCell().setPaddingTop(10);
		pageTable.addCell(new Paragraph(" "));
		pageTable.getDefaultCell().setPaddingTop(10);
		pageTable.getDefaultCell().setColspan(3);

		innerTable = new PdfPTable(4);
		innerTable.setWidths(getWidths(473, 12, 270 , 179, 12));
		innerTable.setWidthPercentage(100f);
		innerTable.getDefaultCell().setBorder(0);
		innerTable.getDefaultCell().setColspan(2);

		setHeaderWithoutBG(innerTable);

		String type = getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Build Type", orderProfiles));

		if("HNL".equals(type)) {
			//innerTable.addCell(new Paragraph("Costs - House and Land",subHeader));	
			innerTable.addCell(new Paragraph("Price",subHeader));
		} else if("OJ".equals(type)) {
			//innerTable.addCell(new Paragraph("Costs - Order Job",subHeader));
			innerTable.addCell(new Paragraph("Price",subHeader));
		} else if("KDR".equals(type)) {
			innerTable.addCell(new Paragraph("Costs - Knockdown Rebuild",subHeader));	
		} else {
			//innerTable.addCell(new Paragraph("Costs - Order",subHeader));
			innerTable.addCell(new Paragraph("Price",subHeader));
		}

		innerTable.getDefaultCell().setPadding(0);
		innerTable.getDefaultCell().setPaddingTop(1);
		innerTable.getDefaultCell().setPaddingRight(3);
		innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		//Paragraph pa= new Paragraph("(All prices are estimates only.\nPrices may vary without notice.)", costDisclaimerFont);
		Paragraph pa= new Paragraph("", costDisclaimerFont);
		//pa.add(Chunk.NEWLINE);
		//pa.add(new Chunk("Prices may vary without notice", costDisclaimerFont));
		innerTable.addCell(pa);
		innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

		setNormal(innerTable);
		innerTable.getDefaultCell().setBackgroundColor(panelBackground);

		pageTable.addCell(innerTable);
		pageTable.getDefaultCell().setColspan(1);
		pageTable.addCell(BLANK);

		innerTable = new PdfPTable(1);
		innerTable.setWidthPercentage(100f);
		innerTable.getDefaultCell().setBorder(0);

		setHeaderWithoutBG(innerTable);
		innerTable.addCell(new Paragraph("Comments:",subHeader));
		setNormal(innerTable);
		innerTable.getDefaultCell().setBackgroundColor(panelBackground);
		innerTable.getDefaultCell().setPaddingTop(5);

		innerTable.getDefaultCell().setPaddingLeft(8);
		innerTable.addCell(new Paragraph(order.getNotes(), textFont));
		pageTable.addCell(innerTable);

		pageTable.getDefaultCell().setColspan(5);
	} */
	/*
	private static class PDHFooter extends PdfPageEventHelper {

		User rep; 
		Order order;
		Plan plan;

		public PDHFooter (User rep, Order order, Plan plan) {
			this.rep = rep;
			this.order = order;
			this.plan = plan;
		}

		public void onEndPage(PdfWriter writer, com.itextpdf.text.Document document) {
			PdfPTable pageTable = new PdfPTable(5);
			try {
				pageTable.setWidths(getDesignWidths(226, 22, 226, 32, 361));
				pageTable.setTotalWidth(556f);
				pageTable.setLockedWidth(true);
				pageTable.getDefaultCell().setPadding(10);
				pageTable.getDefaultCell().setBorder(0);
				long days = (order.getOutcomeDateLong() - order.getDateOfSaleLong()) / ( 1000 * 60 * 60 * 24);

				pageTable.getDefaultCell().setPadding(7);
				pageTable.getDefaultCell().setBackgroundColor(headerBlue);
				pageTable.getDefaultCell().setColspan(3);
				pageTable.addCell(getRepParagraph(rep.getName(), rep.getMobile(), rep.getPhone(), rep.getEmail(), days, order.getOutcomeDate()));

				pageTable.getDefaultCell().setColspan(2);

				Image rim = null;
				if(plan.RangeName != null) {
					if(plan.RangeName.startsWith("Prestige")) {
						rim = PDFUtil.createImage(PropertyEntity.resizeImage("specific/pdf/images/prestige_brand_logo.png", 362, 144, PropertyEntity.ResizeOption.Fit));
					} else if(plan.RangeName.startsWith("Lifestyle")) {
						rim = PDFUtil.createImage(PropertyEntity.resizeImage("specific/pdf/images/lifestyle_brand_logo.png", 362, 144, PropertyEntity.ResizeOption.Fit));
					} else if(plan.RangeName.startsWith("Access")) {
						rim = PDFUtil.createImage(PropertyEntity.resizeImage("specific/pdf/images/access_brand_logo.png", 362, 144, PropertyEntity.ResizeOption.Fit));
					}
				}
				if(rim != null) {
					PdfPTable innerTable = new PdfPTable(2);
					innerTable.setWidths(getWidths(381, 200, 181));
					innerTable.setWidthPercentage(100f);
					innerTable.getDefaultCell().setBorder(0);
					innerTable.addCell(BLANK);
					innerTable.addCell(rim);
					pageTable.addCell(innerTable);

				} else {
					pageTable.addCell(BLANK);
				}
				pageTable.writeSelectedRows(0, -1, 20, 80, writer.getDirectContent());
			} catch (DocumentException de) {
				throw new ExceptionConverter(de);
			}
		}
	}
	 */

	private String getAnswer(OrderAnswer oa) {
		if(oa == null)
			return null;
		return StringUtils.trimToNull(oa.getAnswer());
	}
	/* 
	 * Convenience method, but number formats should be re-used within the same thread wherever possible.
	 */
	private String formatMoney(final double d) {
		return formatMoney(d, null);
	}

	private String formatMoney(final double d, NumberFormat mf) {
		if(mf == null) {
			mf = NumberFormat.getCurrencyInstance();
			mf.setMaximumFractionDigits(0);
		}
		if(d == 0d) {
			return "Inc";
		}
		return mf.format(d);
	}

	private List<String> getFinancialsSort(final String pageID, final List<OrderItem> items) {
		final String key = "Cover-Financials-" + pageID;
		for(OrderItem oi: items) {
			if(key.equals(oi.getCategory())) {
				try { 
					Object o = JSONValue.parse(oi.getDescription());
					if(o instanceof JSONArray) {
						final JSONArray arr = (JSONArray)o;
						final List<String> ret = new ArrayList<String>();
						for(Object item: arr) {
							ret.add(String.valueOf(item));
						}
						return ret;
					} else {
						logger.error("had some other object? {} {}", o.getClass().getName(), o.toString());
					}
				} catch (RuntimeException re) {
					logger.error("Error parsing from financial sort", re);
				}
			}
		}
		return Collections.emptyList();
	}

	private List<String> getPostFinancialsSort(final String pageID, final List<OrderItem> items) {
		final String key = "Cover-PostFinancials-" + pageID;
		for(OrderItem oi: items) {
			if(key.equals(oi.getCategory())) {
				try { 
					Object o = JSONValue.parse(oi.getDescription());
					if(o instanceof JSONArray) {
						final JSONArray arr = (JSONArray)o;
						final List<String> ret = new ArrayList<String>();
						for(Object item: arr) {
							ret.add(String.valueOf(item));
						}
						return ret;
					} else {
						logger.error("had some other object? {} {}", o.getClass().getName(), o.toString());
					}
				} catch (RuntimeException re) {
					logger.error("Error parsing from financial sort", re);
				}
			}
		}
		return Collections.emptyList();
	}

	private static class SummaryCost {
		String title;
		double cost; 
		String costText;

		public SummaryCost (String title, double cost) {
			this.title = title;
			this.cost = cost;
		}


		public SummaryCost (String title, String costText) {
			this.title = title;
			this.cost = -1;
			this.costText = costText;
		}
	}

	private double getBasePrice(List<OrderItem> items, Plan p, HLPackage pkg) {
		double total = pkg != null ? pkg.TotalCost : p.TotalCost;
		if(items == null || items.isEmpty())
			return total;

		total = 0d;
		for(OrderItem oi: items) {
			if(oi.getCategory() != null && oi.getCategory().startsWith("Custom") && oi.isIncludePrice()) {
				total += oi.getTotalCost();
			} else if ("Package".equals(oi.getCategory())) { 
				total += oi.getTotalCost();
			} else if("Plan".equals(oi.getCategory())) {
				total += oi.getTotalCost();
			} else if("VPB".equals(oi.getCategory()) && "Estate Covenant Requirements".equals(oi.getItemGroup()) && oi.isIncludePrice()) {
				total += oi.getTotalCost();
			} else if("DRIP".equals(oi.getCategory()) && (oi.isIncludePrice() || ("Inc Base".equals(oi.getPricingType()) || "Not Shown".equals(oi.getPricingType())))) {
				total += oi.getTotalCost();
			}
		}
		return total;
	}

	private double getEstateCovenantPrice(List<OrderItem> items) {
		double total = 0d;
		for(OrderItem oi: items) {
			if("VPB".equals(oi.getCategory()) && "Estate Covenant Requirements".equals(oi.getItemGroup()) && oi.isIncludePrice()) {
				total += oi.getTotalCost();
			}
		}
		return total;
	}


	private double getCustomTotal(List<OrderItem> items) {
		if(items == null || items.isEmpty())
			return 0;
		double total = 0d;
		for(OrderItem oi: items) {
			if(oi.getCategory() != null && oi.getCategory().startsWith("Custom") && !oi.isIncludePrice()) {
				total += oi.getTotalCost();
			}
		}
		return total;
	}

	private OrderItem getOpenOrderItem(List<OrderItem> items, QuoteViewPage page) {
		String key = "Open-" + page.QuoteViewPageID;
		for(OrderItem oi: items) {
			if(key.equals(oi.getCategory())) {
				return oi;
			}
		}
		return null;
	}

	private List<OrderItem> getDripItems(List<OrderItem> items) {
		List<OrderItem> dripList = new ArrayList<OrderItem>(5);
		for(OrderItem oi: items) {
			if("DRIP".equals(oi.getCategory())) {
				dripList.add(oi);
			}
		}
		return dripList;
	}

	/*
	private double getDripTotal(List<OrderItem> items) {
		if(items == null || items.isEmpty())
			return 0;
		double total = 0d;
		for(OrderItem oi: items) {
			if("DRIP".equals(oi.getCategory()) && !oi.isIncludePrice()) {
				total += oi.getTotalCost();
			}
		}
		return total;
	}
	 */
	private double getOptionsTotal(List<OrderItem> items) {
		if(items == null || items.isEmpty())
			return 0;
		double total = 0d;
		for(OrderItem oi: items) {
			if("VPB".equals(oi.getCategory()) && !("Estate Covenant Requirements".equals(oi.getItemGroup()) && oi.isIncludePrice())) {
				total += oi.getTotalCost();
			}
		}
		return total;
	}

	private OrderItem getFacadeItem(List<OrderItem> list) {
		for(OrderItem oi: list) {
			if("Facade".equals(oi.getCategory())) {
				return oi;
			}
		}
		return null;
	}

	private OrderItem getPlanItem(List<OrderItem> list) {
		for(OrderItem oi: list) {
			if("Plan".equals(oi.getCategory())) {
				return oi;
			}
		}
		return null;
	}

	private OrderItem getPackageItem(List<OrderItem> list) {
		for(OrderItem oi: list) {
			if("Package".equals(oi.getCategory())) {
				return oi;
			}
		}
		return null;
	}
	private OrderItem getOrderItemByProductId(List<OrderItem> list, String productID) {
		return getOrderItemByProductId(list, productID, null);
	}

	private OrderItem getOrderItemByProductId(List<OrderItem> list, String productID, String name) {
		if(StringUtils.isBlank(productID)) {
			return null;
		}
		for(OrderItem item: list) {
			if(productID.equals(item.getProductID()) && (name == null || name.equals(item.getName()))) {
				return item;
			}
		}
		return null;
	}
	private OrderItem getSummaryOrderItemByProductId(List<OrderItem> list, String productID) {
		if(StringUtils.isBlank(productID)) {
			return null;
		}
		for(OrderItem item: list) {
			if(productID.equals(item.getProductID()) && item.isIncludeSummary()) {
				return item;
			}
		}
		return null;
	}	


	public void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		//final String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
		com.itextpdf.text.Document document = null;
		java.io.ByteArrayOutputStream baos = null;
		PdfStamper stamp = null;
		PdfReader reader = null;
		//need to grab the outputstream before doing anything else. 
		java.io.OutputStream outputStream = response.getOutputStream();
		//now we can set the content type 
		response.setContentType("application/pdf");
		try {

			final String orderID = request.getParameter("OrderID");
			final UserBean user = UserService.getInstance().getCurrentUser(request);
			final Connection con = ActionBean.getConnection(request);

			//Order order = os.getOrder(request, orderID);
			//List<OrderItem> orderItems = os.getOrderItems(request, orderID);
			//List<ContactLocation> lcl = UtilityResource.getInstance().getOrderLocations(request, orderID);
			//List<OrderProfile> orderProfiles = OrderResource.getInstance().getOrderProfile(request, orderID);

			final boolean preview = "true".equals(request.getParameter("-preview"));
			final String quoteViewID = StringUtils.trimToNull(request.getParameter("QuoteViewID"));

			QuoteView qv = preview ? QuoteViewService.getInstance().getQuoteView(con, user, quoteViewID) : null;
			Future<Order> fOrder = ThreadManager.startCallable(new Callable<Order>() {
				public Order call() {
					try {
						return os.getOrder(con, user, orderID);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}
			});
			Future<List<OrderItem>> fOrderItems = preview ? null : ThreadManager.startCallable(new Callable<List<OrderItem>>() {
				public List<OrderItem> call() {
					try {
						return os.getOrderItems(con, user, orderID);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}
			});
			Future<List<ContactLocation>> fContactLocation = preview ? null : ThreadManager.startCallable(new Callable<List<ContactLocation>>() {
				public List<ContactLocation> call() {
					try {
						return os.getOrderLocations(con, user, orderID);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}
			});
			Future<List<OrderProfile>> fOrderProfiles = preview ? null : ThreadManager.startCallable(new Callable<List<OrderProfile>>() {
				public List<OrderProfile> call() {
					try {
						final List<Profile> pl = ProfileService.getInstance().getProfiles(con, user, ProfileService.ProfileType.ORDER, orderID);
						final List<OrderProfile> cpl = new ArrayList<OrderProfile>(pl.size());
						for(Profile p: pl) {
							cpl.add((OrderProfile)p);
						}
						return cpl;
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}
			});

			List<OrderItem> orderItems = preview ? Arrays.asList(qv.OrderTemplate.getOrderItems()) :  fOrderItems.get();

			final OrderItem facadeItem = preview ? null : getFacadeItem(orderItems);
			final OrderItem planItem = preview ? null : getPlanItem(orderItems);
			final OrderItem packageItem = preview ? null : getPackageItem(orderItems);

			//Facade f = fs.getFacade(request, facadeItem.getProductID());
			//Plan plan = PlanService.getInstance().getPlan(request, planItem.getProductID());

			Future<Facade> fFacade = preview  ? null : ThreadManager.startCallable(new Callable<Facade>() {
				public Facade call() {
					try {
						return fs.getFacade(con, user, facadeItem.getProductID());
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}
			});

			Future<Plan> fPlan = preview || planItem == null ? null :  ThreadManager.startCallable(new Callable<Plan>() {
				public Plan call() {
					try {
						return PlanService.getInstance().getPlan(con, user, planItem.getProductID());
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}
			});
			Future<HLPackage> fPackage = preview || packageItem == null ? null :  ThreadManager.startCallable(new Callable<HLPackage>() {
				public HLPackage call() {
					try {
						return PlanService.getInstance().getPackage(con, user, packageItem.getProductID());
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}
			});
			final Order order = preview ? qv.OrderTemplate : fOrder.get();

			// Populating Quote View looking for header image and footer text 
			if(qv == null && order.getQuoteViewID() != null && !order.getQuoteViewID().equalsIgnoreCase("")) 
				qv = QuoteViewService.getInstance().getQuoteView(con, user, order.getQuoteViewID());

			//QuoteView qv = QuoteViewService.getInstance().getQuoteView(con, user, order.getQuoteViewID());
			//List<QuoteViewPage> qvpl = QuoteViewService.getInstance().getQuoteViewPages(request, qv.QuoteViewID);
			//User rep = UserResource.getInstance().getUser(request, order.getRepUserID());
			/*
        	Future<QuoteView> fQuoteView = ThreadManager.startCallable(new Callable<QuoteView>() {
        		public QuoteView call() {
        			return QuoteViewService.getInstance().getQuoteView(con, user, order.getQuoteViewID());
        		}
        	});
			 */
			Future<List<QuoteViewPage>> fQuoteViewPages = ThreadManager.startCallable(new Callable<List<QuoteViewPage>>() {
				public List<QuoteViewPage> call() {
					try {
						return QuoteViewService.getInstance().getQuoteViewPages(con, user, preview ? quoteViewID  : order.getQuoteViewID());
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}
			});
			Future<User> fRepUser = ThreadManager.startCallable(new Callable<User>() {
				public User call() {
					try {
						return UserService.getInstance().getUser(con, user, preview ? user.getUserID() : order.getRepUserID());
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}
			});
			Facade f = preview || facadeItem == null ? new Facade() : fFacade.get();
			final HLPackage pkg = preview || packageItem == null ? new HLPackage() : fPackage.get();
			if(packageItem != null) {
				fPlan = ThreadManager.startCallable(new Callable<Plan>() {
					public Plan call() {
						try {
							return PlanService.getInstance().getPlan(con, user, pkg.PlanProductID);
						} catch (Exception e) {
							e.printStackTrace();
						}
						return null;
					}
				});
			}
			final Plan plan = fPlan == null ? new Plan() : fPlan.get();

			User rep = fRepUser.get();
			List<OrderProfile> orderProfiles = preview ? null : fOrderProfiles.get();
			List<ContactLocation> lcl = preview ? new ArrayList<ContactLocation>(0) : fContactLocation.get();
			if(!preview && lcl.isEmpty()) {
				throw new RuntimeException("No location defined, this order is incomplete");
			}

			//uncomment if required.
			//QuoteView qv = fQuoteView.get();
			List<QuoteViewPage> qvpl = fQuoteViewPages.get();

			Map<String, Future<com.sok.runway.externalInterface.beans.Document>> docMap = new HashMap<String, Future<com.sok.runway.externalInterface.beans.Document>>();

			for(QuoteViewPage qvp: qvpl) {
				if ("Document".equals(qvp.PageTypeName) && qvp.Active) {
					final DocumentOption sd = qvp.SelectedOptions != null ? qvp.SelectedOptions.SelectedDocument : null;
					if(sd != null && StringUtils.isNotBlank(sd.DocumentID)) {
						docMap.put(sd.DocumentID, ThreadManager.startCallable(new Callable<com.sok.runway.externalInterface.beans.Document>() {
							public com.sok.runway.externalInterface.beans.Document call() {
								try {
									return DocumentService.getInstance().getDocument(con, user, sd.DocumentID);
								} catch (Exception e) {
									e.printStackTrace();
								}
								return null;
							}
						}));
					} else {
						/* make the page inactive so we don't check this later. */ 
						qvp.Active = false;
					}
				}
			}
			//final List<OrderItem> dripItems = getDripItems(orderItems);
			/*
			 * 
			 * We're now storing the drip info inside the order item MetaData, so this is not required.
        	Future<List<PDFDripInfo>> fDripList =  ThreadManager.startCallable(new Callable<List<PDFDripInfo>>() {
        		public List<PDFDripInfo> call() {
        			DripCollection dc = null;
        			logger.debug("testing drip");
        			if(planItem != null) {
        				logger.debug("testing planItem");
        				List<String> list = new ArrayList<String>();
        				if(StringUtils.isNotBlank(plan.RangeProductID)) {
        					list.add(plan.RangeProductID);
        				}
        				if(StringUtils.isNotBlank(plan.HomeProductID)) {
        					list.add(plan.HomeProductID);
        				}
        				if(StringUtils.isNotBlank(plan.ProductID)) {
        					list.add(plan.ProductID);
        				}
        				if(list.isEmpty()) return Collections.emptyList();

        				String[] productIds = new String[list.size()];
        				int ix = 0;
        				for(String s: list) {
        					productIds[ix++] = s;
        				}
        				dc =  DripService.getInstance().getAllDrips(con, user, productIds, new java.util.Date());
        				PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(con, productIds[productIds.length - 1]);
        				DripService.getInstance().populateDripInfo(con, dc, mainProduct, "Quotes", preview ? quoteViewID : order.getQuoteViewID(), null);
        			} else if(packageItem != null) {      
        				logger.debug("testing planItem {}, {}", "Quotes",  preview ? quoteViewID : order.getQuoteViewID());
        				dc =  DripService.getInstance().getAllDrips(con, user, new String[]{packageItem.getProductID(), packageItem.getProductID(), packageItem.getProductID()}, new java.util.Date());
        				PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(con, packageItem.getProductID());
        				DripService.getInstance().populateDripInfo(con, dc, mainProduct, "Quotes",  preview ? quoteViewID : order.getQuoteViewID(), null);
        			} else {
        				logger.debug("no plan or package item");
        				return Collections.emptyList();
        			}

        			List<PDFDripInfo> dripInfo = new ArrayList<PDFDripInfo>();
        			for(DripProduct dp: dc.allDrips) {
        				OrderItem item = getSummaryOrderItemByProductId(dripItems, dp.ProductID);
        				logger.debug("checking {}", dp.Name);
        				if(item != null) {
        					logger.debug("item was in the list of drips");
        					// it has one of the items
        					if(dp.DripInfo != null && dp.DripInfo.ContentList != null) {
        						logger.debug("item had content");
        						PDFDripInfo pdi = new PDFDripInfo();
        						pdi.cost = item.getTotalCost();
        						pdi.includePrice = item.isIncludePrice();
        						pdi.items = new ArrayList<String>();

        						if(!dp.DripInfo.ContentList.isEmpty()) {
        							for(DripService.DripContent dripContent: dp.DripInfo.ContentList) {
        								if(dripContent.ContentType == DripService.DripContentType.Text) {
        									if("Value".equals(dripContent.Placement)) {
        										if(getOrderItemByProductId(dripItems, dp.ProductID, dripContent.Description) != null) {
        											pdi.items.add(dripContent.Description);
            									}	
        									} else if ("Heading".equals(dripContent.Placement)) {
        										pdi.Name = dripContent.Description;
        									} else if ("Description".equals(dripContent.Placement)) {
        										pdi.Description = dripContent.Description;
        									} else if("Cover".equals(dripContent.Placement)) {
        										pdi.Cover = dripContent.Description;
        									}
        								} else if(dripContent.ContentType == DripService.DripContentType.Image && "Image".equals(dripContent.Placement)){
        									logger.debug("Should be setting image path {}", dripContent.ImagePath);
        									pdi.ImagePath = dripContent.ImagePath;
        								}	
        							}
        							dripInfo.add(pdi);
        						}
         					} else {
        						logger.debug("drip info was null for product {}", dp.ProductID);
        					}
        				} else {
        					logger.debug("could not find order item for product {}", dp.ProductID);
        				}
        			}
        			return dripInfo;
        		}
        	});
			 */
			logger.debug("Have QVPL with {} pages", qvpl.size());
			//ProfileService.getInstance().

			/* construct document */
			document = new com.itextpdf.text.Document();
			document.setMargins(0,0,0,0);
			document.setMarginMirroring(true);
			baos = new java.io.ByteArrayOutputStream();
			PdfWriter writer = PdfWriter.getInstance(document, baos);

			//writer.setPageEvent(new PDHFooter(rep, order, plan));


			if(preview) {
				order.setDateOfSale(new java.util.Date());	
				order.setOutcomeDate(new java.util.Date(order.getDateOfSale().getTime() + (1000L * 60L * 60L * 24L * 30L)));	
			}

			TableData openContext = null;
			document.open();
			final String selectedPage = StringUtils.trimToNull(request.getParameter("QuoteViewPageID")); 
			List<Integer> footerList = new ArrayList<Integer>();
			int lastPage = 0;

			String type = getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Build Type", orderProfiles));
			logger.debug("Order type - {}", type);
			for(QuoteViewPage qvp: qvpl) {
				logger.debug(qvp.PageTypeName);
				logger.debug("qvp.DisplayKDR - {}", qvp.DisplayKDR);

				if((qvp.Active && qvp.DisplayPDF) || qvp.QuoteViewPageID.equals(selectedPage)) { 
					if("Cover Page".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						logger.debug("Adding cover page");
						Map<String, Boolean> pageOptions = null;
						QuoteViewPageOptions customOptions = null;
						for(QuoteViewPage tmpBean: qvpl) {
							if("Custom".equals(tmpBean.PageTypeName))
							{
								customOptions = tmpBean.SelectedOptions;
								pageOptions = customOptions.PageOptions;
							}
						}
						//addquoteCoverPage(document, writer, qvp, order, orderItems, f, preview ? new ContactLocation() : lcl.get(0), plan, pkg, orderProfiles, qv, pageOptions);
						addCoverPage(document, qvp, order, orderItems, f, preview ? new ContactLocation() : lcl.get(0), plan, pkg, orderProfiles, qv, pageOptions);
						footerList.add(writer.getPageNumber());
						int page = writer.getPageNumber();
						logger.debug("Last: {}, thisPage: {}", lastPage, page);
						for(int i = (lastPage +1); ;i++) { 
							footerList.add(i);
							if(i >= page) 
								break;
						}
						lastPage = page;
					} else if ("Document".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID)) 
							&& ( ("HNL".equals(type) && qvp.DisplayHNL) || ("OJ".equals(type) && qvp.DisplayOJ) || ("KDR".equals(type) && qvp.DisplayKDR) ) ) {
						DocumentOption sd = qvp.SelectedOptions.SelectedDocument;
						addDocumentPages(document, writer, sd.Pages, docMap.get(sd.DocumentID).get().getFilePath());
						lastPage = writer.getPageNumber();
					} else if ("VPB".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						boolean footerFlag = addOptionPages(document, order, orderItems, qv);
						if(footerFlag)
						{
							int page = writer.getPageNumber();
							logger.debug("Last: {}, thisPage: {}", lastPage, page);
							for(int i = (lastPage +1); ;i++) { 
								footerList.add(i);
								if(i >= page) 
									break;
							}
							lastPage = page;
						}
					} else if ("DRIP".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {

						//List<PDFDripInfo> driplist = fDripList.get();
						/* needs to have :
						 * included drip items with image and or text 
						 * 
						 * plus
						 * optional selected drip items with image and or text. 
						 */
						boolean footerFlag = addDripPage(document, qvp, order, orderItems, qv);
						if(footerFlag)
						{
							int page = writer.getPageNumber();
							logger.debug("Last: {}, thisPage: {}", lastPage, page);
							for(int i = (lastPage +1); ;i++) { 
								footerList.add(i);
								if(i >= page) 
									break;
							}
							lastPage = page;
						}
					} else if ("Custom".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						boolean footerFlag = addCustomPages(document, order, orderItems, qvp, qv);

						if(footerFlag)
						{
							int page = writer.getPageNumber();
							logger.debug("Last: {}, thisPage: {}", lastPage, page);
							for(int i = (lastPage +1); ;i++) { 
								footerList.add(i);
								if(i >= page) 
									break;
							}
							lastPage = page;
						}
	            	} else if ("Summary".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
	            		addSummaryPage(document, qvp, order, orderItems, qv);
	            		int page = writer.getPageNumber();
	            		logger.debug("Last: {}, thisPage: {}", lastPage, page);
	            		if(page > lastPage) {
		            		for(int i = (lastPage +1); ;i++) { 
		            			footerList.add(i);
		            			if(i >= page) 
		            				break;
		            		}
	            		}
	            		lastPage = page;
					} else if("Open".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						if(openContext == null) {
							openContext = new TableBean();
							List<OrderContact> lc = order.getLinkedContacts();
							int size = lc.size();
							if(size != 0) {
								openContext.put("FirstName", lc.get(0).getFirstName());
								openContext.put("LastName", lc.get(0).getLastName());
								Address a = lc.get(0).getPhysicalAddress();
								if(a != null) { 
									openContext.put("Street", a.getStreet());
									openContext.put("Street2", a.getStreet2());
									openContext.put("City", a.getCity());
									openContext.put("State", a.getState());
									openContext.put("Postcode", a.getPostcode());
									openContext.put("Country", a.getCountry());
								}
								openContext.put("Email", lc.get(0).getEmail());
								openContext.put("Phone",  lc.get(0).getPhone());
								openContext.put("Mobile",  lc.get(0).getMobile());
							}
							if(size > 1) {
								openContext.put("SecondaryFirstName", lc.get(1).getFirstName());
								openContext.put("SecondaryLastName", lc.get(1).getLastName());
								Address a = lc.get(1).getPhysicalAddress();
								if(a != null) { 
									openContext.put("SecondaryStreet", a.getStreet());
									openContext.put("SecondaryStreet2", a.getStreet2());
									openContext.put("SecondaryCity", a.getCity());
									openContext.put("SecondaryState", a.getState());
									openContext.put("SecondaryPostcode", a.getPostcode());
									openContext.put("SecondaryCountry", a.getCountry());
								}
								openContext.put("SecondaryEmail", lc.get(1).getEmail());
								openContext.put("SecondaryPhone",  lc.get(1).getPhone());
								openContext.put("SecondaryMobile",  lc.get(1).getMobile());
							}

							openContext.put("ContactName", order.getContactName());
							openContext.put("RepName", order.getRepName());

							UserBean orderRep = UserService.getInstance().getSimpleUser(con, order.getRepUserID());
							for(String key: new String[]{"FirstName","LastName","Street","City","Postcode","State","Country","Email","Phone"}) {
								openContext.put("Rep" + key, orderRep.getString(key));
							}
						}
						addOpenPage(document, qvp, orderItems, openContext, qv); 
						int page = writer.getPageNumber();
						logger.debug("Last: {}, thisPage: {}", lastPage, page);
						for(int i = (lastPage +1); ;i++) { 
							footerList.add(i);
							if(i >= page) 
								break;
						}
						lastPage = page;
					} else if("Floorplan".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						this.addFloorplanPage(document, qvp, plan.ImagePath);
						int page = writer.getPageNumber();
						logger.debug("Last: {}, thisPage: {}", lastPage, page);
						for(int i = (lastPage +1); ;i++) { 
							footerList.add(i);
							if(i >= page) 
								break;
						}
						lastPage = page;
					}
				}
			} 

			/*this.addTNCPage(document, order);
    		int page = writer.getPageNumber();
    		logger.debug("Last: {}, thisPage: {}", lastPage, page);
    		for(int i = (lastPage +1); ;i++) { 
    			footerList.add(i);
    			if(i >= page) 
    				break;
    		}
    		lastPage = page; */

			document.close();
			document = null;
			reader = new PdfReader(baos.toByteArray());

			stamp = new PdfStamper(reader, response.getOutputStream());
			response.setHeader("Content-Type", "application/pdf");

			long days = (order.getOutcomeDateLong() - order.getDateOfSaleLong()) / ( 1000 * 60 * 60 * 24);

			PdfPTable hugeImage = new PdfPTable(1);
			hugeImage.setWidths(getDesignWidths(800));
			hugeImage.setTotalWidth(800);
			hugeImage.getDefaultCell().setBorder(0);
			hugeImage.getDefaultCell().setPadding(0);
			hugeImage.getDefaultCell().setFixedHeight(60);
			hugeImage.getDefaultCell().setBackgroundColor(quotePurple);
			try { 
				Image i = PDFUtil.createImage(PropertyEntity.resizeImage(InitServlet.getRealPath("/specific/images/quoteFooter.jpg"), 800, 70, PropertyEntity.ResizeOption.Crop));
				if (i == null) i = PDFUtil.createImage(InitServlet.getRealPath("/specific/images/quoteFooter.jpg"), 800, 70);
				if(i != null) {
					hugeImage.addCell(i);
				} else {
					hugeImage.addCell(BLANK);
				}
			} catch (Exception e) {}

			for(Integer i: footerList) {
				logger.debug("adding footer on page {}", i);
				//we'll stamp on top.
				//if( i != 1 ) { // As first page does not have a footer for FH
				hugeImage.writeSelectedRows(0, -1, 0, 60, stamp.getOverContent(i));
				//}
				Phrase p1 = getRepParagraph(rep.getName());
				Phrase p2 = getRepParagraph(rep.getMobile(), rep.getPhone(), rep.getEmail());
				Phrase p3 = getValidity(days, order.getOutcomeDate(), qv);

				// we need to add overlays for first page
				if(i == 1) {
					setquoteCoverPageOverlays(stamp.getOverContent(i), order, plan, pkg, type);
				}
				setquoteOtherPageOverlays(stamp.getOverContent(i), p1, p2, p3);
			}	

		} catch (Exception e) {
			logger.error("Exception in order pdf", e);
			try {
				if(document == null) { 
					document = new com.itextpdf.text.Document();
					document.setMargins(0,0,0,0);
					document.setMarginMirroring(true);
					PdfWriter writer = PdfWriter.getInstance(document, outputStream);
					document.open();
				}
				Paragraph error = new Paragraph();
				error.add(new Chunk(ActionBean
						.writeStackTraceToString(e), FontFactory
						.getFont(FontFactory.HELVETICA, 8)));
				document.add(error);
			} catch (Exception ex) {
				System.out.println(ActionBean
						.writeStackTraceToString(ex));
			}
		} finally {
			if (document != null) {
				document.close();
			}
			if(reader != null) { 
				reader.close();
			}
			if(stamp != null) { 
				try { stamp.close(); } catch (DocumentException de) { }
			}
		}
	}	

	/**
	 * @param stamp
	 * @param pageTable
	 */
	private void setquoteOtherPageOverlays(PdfContentByte backCb, Phrase p1, Phrase p2, Phrase p3) {
		PdfGState gs1 = new PdfGState();
		gs1.setFillOpacity(0.6f);			
		//backCb.saveState();	 
		//backCb.setGState(gs1);
		//backCb.resetRGBColorFill();
		//backCb.restoreState();

		int newY = 42;

		ColumnText.showTextAligned(backCb, Element.ALIGN_LEFT, p1, 20, newY, 0);
		ColumnText.showTextAligned(backCb, Element.ALIGN_LEFT, p2, 20, newY - 12, 0);
		ColumnText.showTextAligned(backCb, Element.ALIGN_LEFT, p3, 20, newY - 26, 0);
	}

	private void addTNCPage(Document document, Order order) throws DocumentException {
		logger.debug("Adding T&C page");

		document.newPage();
		PdfPTable itemTable = new PdfPTable(2);
		itemTable.setWidthPercentage(100f);
		itemTable.getDefaultCell().setPadding(0);
		itemTable.getDefaultCell().setBorder(0);
		itemTable.getDefaultCell().setColspan(2);
		itemTable.getDefaultCell().setPaddingTop(15);
		itemTable.getDefaultCell().setPaddingBottom(15);
		itemTable.addCell(getPlainHeadingTable("Terms & Conditions"));

		setHeaderWithoutBG(itemTable);
		itemTable.getDefaultCell().setPaddingLeft(7);
		itemTable.getDefaultCell().setColspan(1);
		setNormal(itemTable);
		itemTable.getDefaultCell().setPaddingBottom(3);
		itemTable.addCell(BLANK);
		itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);;
		itemTable.addCell(new Paragraph(longdf.format(order.getDateOfSale()), subHeader)); // date
		itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		itemTable.getDefaultCell().setPaddingBottom(0);
		itemTable.getDefaultCell().setColspan(2);
		PdfPCell cell = new PdfPCell(BLANK);
		cell.setBackgroundColor(quotePurple);
		cell.setFixedHeight(10f);
		cell.setColspan(2);
		itemTable.addCell(cell); // quote bar*/

		setNormal(itemTable);
		itemTable.getDefaultCell().setPaddingBottom(20);
		itemTable.addCell(new Paragraph("\n\n TERMS & CONDITIONS GOES HERE", textFont));

		// getSignedTable(document);
		itemTable.addCell(new Paragraph("  Signed Client:....................................  Date:  /  /  ", textFont));
		itemTable.addCell(new Paragraph("  Signed Client:....................................  Date:  /  /  ", textFont));

		document.add(itemTable);

	}
	private void getSignedTable(Document document) {
		PdfPTable itemTable = new PdfPTable(2);
		itemTable.setWidthPercentage(100f);
		itemTable.getDefaultCell().setPadding(0);
		itemTable.getDefaultCell().setBorder(0);
		itemTable.getDefaultCell().setPaddingTop(15);
		itemTable.getDefaultCell().setPaddingBottom(15);
		itemTable.addCell(new Paragraph("Signed Client:"));
	}
	private void setquoteCoverPageOverlays(PdfContentByte backCb, Order order, Plan plan, HLPackage pkg, String type) {

		PdfGState gs1 = new PdfGState();
		gs1.setFillOpacity(0.6f);			
		backCb.saveState();	 
		backCb.setGState(gs1);
		backCb.setRGBColorFill(16,16,16);
		backCb.rectangle(0, 440, 200, 20);
		backCb.fill();
		backCb.resetRGBColorFill();
		backCb.restoreState();


		int newY = 447;
		int tmpY = 10;

		ColumnText.showTextAligned(backCb, Element.ALIGN_LEFT, new Phrase("Image for illustration purposes only", PDFUtil.helvitaNeue75NormalPt12White), 20, newY, 0);

		/*
		final DateFormat df = new SimpleDateFormat("dd MMMM yyyy");

		Phrase tmpPhrase = new Phrase();
		tmpPhrase.add(new Chunk(df.format(order.getDateOfSale()), dateText));
		ColumnText.showTextAligned(over, Element.ALIGN_LEFT, tmpPhrase, 485, 730, 0); // DATE

		tmpPhrase = new Phrase();
		List<OrderContact> contactList = order.getLinkedContacts();
		tmpPhrase.add(new Chunk(String.format("%1$s %2$s", contactList.get(0).getTitle(), contactList.get(0).getName()), quoteCoverPageQuotationHeader));
		ColumnText.showTextAligned(over, Element.ALIGN_LEFT, tmpPhrase, 50, 640, 0); // Quotation

		String quoteType = "OJ".equals(type)? "Order Job" : "";
		if("HNL".equals(type)) {
			quoteType = "H&L Package";
		}
		tmpPhrase = new Phrase();
		tmpPhrase.add(new Chunk(String.format("Preliminary Quotation (%1$s) - %2$s", order.getOrderNum(), quoteType), quoteCoverPageQuotationHeader));
		ColumnText.showTextAligned(over, Element.ALIGN_LEFT, tmpPhrase, 50, 610, 0); // Quotation

		tmpPhrase = new Phrase();
		tmpPhrase.add(new Chunk(plan.HomeName + " " + plan.Name, quoteCoverPageQuotationHeader));
		ColumnText.showTextAligned(over, Element.ALIGN_LEFT, tmpPhrase, 50, 580, 0); // Plan Name
		 */
	}

	private static final VelocityEngine ve;
	static {
		try { 
			ve = VelocityManager.getEngine();
		} catch (Exception e) {
			logger.error("Unable to get Velocity Engine", e);
			throw new RuntimeException(e);
		}
	}
	private String evaluate(String velocity, TableData context) {
		if(logger.isDebugEnabled()) {
			logger.debug(velocity);
			for(Object o: context.getKeys()) {
				logger.debug("{}={}",o, context.get(o));
			}
		}
		StringWriter sw = new StringWriter();
		try {
			ve.evaluate(context, sw, this.getClass().getName(), velocity);
			sw.flush();
			sw.close();
		} catch (Exception e) {
			logger.error("Error evaluating field", e);
			return velocity;
		}
		return (sw.toString());
	}


	private void addOpenPage(Document document, QuoteViewPage qvp, List<OrderItem> orderItems, TableData ctx, QuoteView qView) throws DocumentException {
		OrderItem item = this.getOpenOrderItem(orderItems, qvp);

		document.newPage();

		PdfPTable itemTable = new PdfPTable(1);
		itemTable.setWidthPercentage(100f);
		itemTable.getDefaultCell().setPadding(0);
		itemTable.getDefaultCell().setBorder(0);

		itemTable.getDefaultCell().setColspan(5);
		itemTable.addCell(getHeadingTableLeft(qvp.PageName, qView));

		itemTable.getDefaultCell().setPaddingTop(15);
		itemTable.getDefaultCell().setPaddingBottom(15);

		itemTable.getDefaultCell().setPaddingLeft(60);
		itemTable.getDefaultCell().setPaddingRight(60);


		itemTable.getDefaultCell().setColspan(0);

		itemTable.addCell(BLANK);

		if(item != null) {
			String text = item.getDescription();
			if(text != null) {
				PdfPCell c = new PdfPCell();	//need to use seperate cell to use leading for some reason.
				c.setPaddingLeft(60);
				c.setPaddingRight(60);
				c.setBorder(0);
				c.addElement(getText(evaluate(text, ctx), textFont, textFontBold));

				itemTable.addCell(c);
			} else {
				itemTable.addCell(new Phrase("This page is intentionally left blank", textFont));
			}
		} else {
			itemTable.addCell(new Phrase("This page is intentionally left blank", textFont));
		}
		document.add(itemTable);
	}


	private Phrase getText(String text, Font normFont, Font boldFont) {

		String noComments = text.replaceAll("(?s)<!--.*?-->", "");	//remove html comments, such as added in by word
		//System.out.println(noComments);
		String switchBr = StringUtil.replace(noComments, "<br />", "<br/>");	//the regex matches on <br/>, ck adds <br />
		//System.out.println(switchBr);
		Matcher matcher = StringUtil.newlineregex.matcher(switchBr);
		String newString = matcher.replaceAll("");
		//System.out.println(newString);
		matcher = StringUtil.pbrregex.matcher(newString);
		newString = matcher.replaceAll("\r\n");
		//System.out.println(newString);
		StrTokenizer st = new StrTokenizer(newString, "<strong>");
		String init = st.nextToken();

		Phrase p = new Phrase(13, new Chunk(StringEscapeUtils.unescapeHtml(StringUtil.stripHtml(init)), normFont));

		while(st.hasNext()) {
			String s = st.nextToken();
			if(s.length() > 0) { 
				StrTokenizer cl = new StrTokenizer(s, "</strong>");
				if(cl.hasNext()) {
					String bold = StringUtil.stripHtml(cl.nextToken());
					if(bold.length()> 0) { 
						p.add(new Chunk(StringEscapeUtils.unescapeHtml(bold), boldFont));
						//System.out.print("=======");
						//System.out.print(StringEscapeUtils.unescapeHtml(StringUtil.stripHtml(bold)));
						//System.out.print("=======");
					}
					while(cl.hasNext()) {
						String norm = StringUtil.stripHtml(cl.nextToken());
						if(norm.length() > 0) { 
							p.add(new Chunk(StringEscapeUtils.unescapeHtml(norm), normFont));
							//System.out.print(StringEscapeUtils.unescapeHtml(norm));
						}
					}
				}
			} 
		}
		return p;
	}

	private void addFloorplanPage(Document document, QuoteViewPage qvp, String path) throws DocumentException {
		logger.debug("Adding floorplan page with path {}", path);
		document.newPage();

		PdfPTable itemTable = new PdfPTable(1);
		itemTable.setWidthPercentage(100f);
		itemTable.getDefaultCell().setPadding(0);
		itemTable.getDefaultCell().setBorder(0);

		itemTable.getDefaultCell().setColspan(5);
		itemTable.getDefaultCell().setPaddingTop(15);
		itemTable.getDefaultCell().setPaddingBottom(15);

		//itemTable.addCell(getHeadingTable(qvp.PageName));
		itemTable.addCell(new Phrase(qvp.PageName, quotationHeader));
		itemTable.getDefaultCell().setColspan(0);

		itemTable.addCell(BLANK);

		try {
			String realPath = InitServlet.getRealPath("/" + path);
			Image rim = Image.getInstance((InitServlet.getRealPath("/" + path)));
			rim.scaleToFit(600f, 650f);

			PdfPCell cell = new PdfPCell(rim, true);
			cell.setFixedHeight(650f);
			cell.setBorder(0);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);

			logger.debug("Floorplan realpath: {} was null {}", realPath, rim == null);
			itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			itemTable.addCell(cell);
		} catch (Exception e) {
			itemTable.addCell(new Phrase("Sorry, we were unable to find a suitable floorplan image."));
			logger.error("Error adding pdf image " , e);
		}
		document.add(itemTable);
	}

	private boolean addDripPage(Document document, QuoteViewPage qvp, Order order, List<OrderItem> orderItems, QuoteView qView) throws DocumentException {
		List<OrderItem> dripItems = getDripItems(orderItems);
		if(logger.isDebugEnabled()) logger.debug("addDripPage(document, QuoteViewPage, DripItems({})", dripItems.size());
		boolean checkFlag = false;

		for(OrderItem pdi: dripItems) {
			if(StringUtils.isNotBlank(pdi.getMetaData()) && !"{}".equalsIgnoreCase(pdi.getMetaData())) {
				checkFlag = true;
			}
		}

		if(checkFlag)
		{
			document.newPage();
			PdfPTable itemTable = new PdfPTable(2);
			itemTable.setWidthPercentage(100f);
			itemTable.getDefaultCell().setPadding(0);
			itemTable.getDefaultCell().setBorder(0);
			itemTable.getDefaultCell().setColspan(2);
			itemTable.addCell(getHeadingTable(qView, order.getDateOfSale(), "Promitions"));

			itemTable.getDefaultCell().setPaddingTop(15);
			itemTable.getDefaultCell().setPaddingBottom(15);
			itemTable.getDefaultCell().setPaddingLeft(20);
			itemTable.getDefaultCell().setPaddingRight(20);


			setHeaderWithoutBG(itemTable);
			itemTable.getDefaultCell().setPaddingLeft(7);			
			itemTable.getDefaultCell().setColspan(1);
			setNormal(itemTable);
			/*
			itemTable.getDefaultCell().setPaddingBottom(3);
			itemTable.addCell(new Phrase("Selected Promotions", subHeader));
			itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);;
			itemTable.addCell(new Paragraph(longdf.format(order.getDateOfSale()),subHeader)); // date
			itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			itemTable.getDefaultCell().setPaddingBottom(0);
			itemTable.getDefaultCell().setColspan(2);
			itemTable.getDefaultCell().setBorder(0);
			PdfPCell cell = new PdfPCell(BLANK);
			cell.setBackgroundColor(quotePurple);
			cell.setFixedHeight(10f);
			cell.setColspan(2);
			itemTable.addCell(cell); // quote bar
			 */
			setNormal(itemTable);			
			itemTable.addCell(BLANK);
			itemTable.getDefaultCell().setBackgroundColor(panelBackground);

			itemTable.getDefaultCell().setPaddingLeft(7);

			boolean first = true;
			JSONObject dripContent = null;

			JSONParser jp = new JSONParser();

			for(OrderItem pdi: dripItems) {
				if(!pdi.isIncludeSummary()) {
					//items that are child items will be include summary = N
					logger.debug("skipping item {} as include summary = N", pdi.getOrderItemID());
					continue;
				}
				if(StringUtils.isBlank(pdi.getMetaData())) {
					logger.error("Drip Item meta data was blank for OrderItemID=[{}]", pdi.getOrderItemID());
					continue;
				}
				try { 
					Object obj = jp.parse(pdi.getMetaData());
					if(obj instanceof JSONObject) {
						dripContent = (JSONObject)obj;
					} else {
						logger.error("Got a different instance than expected for MetaData {} in OrderItemID=[{}]", obj.getClass().getName(), pdi.getOrderItemID());
						continue;
					}
				} catch (ParseException pe) {
					logger.error("Error parsing Drip Meta Data", pe);
					/* attempt to load from drip ? */
					continue;
				}
				logger.debug("Processing DRIP Item for PDF Content OrderItemID=[{}], Description=[{}]", pdi.getOrderItemID(), pdi.getDescription());

				if(first) {
					first = false;
				} else {
					//TODO draw the line here.
				}

				itemTable.getDefaultCell().setPaddingTop(20);

				Object o = null; String t = null;

				if((o = dripContent.get("Name")) != null && o instanceof String && StringUtils.isNotBlank(t = (String)o)) {
					itemTable.addCell(new Phrase(t, dripTitleFont));
					logger.debug("OrderItemID=[{}] Heading: {}",pdi.getOrderItemID(), t);
				}
				if((o = dripContent.get("ImagePath")) != null && o instanceof String && StringUtils.isNotBlank(t = (String)o)) {
					logger.debug("OrderItemID=[{}] ImagePath: {}",pdi.getOrderItemID(), t);
					try {
						Image rim = Image.getInstance((InitServlet.getRealPath("/" + t)));
						//Image rim = PDFUtil.createImage(InitServlet.getRealPath("/" + t)); //PropertyEntity.resizeImage(pdi.ImagePath, 1400,	250, PropertyEntity.ResizeOption.Fit));		
						itemTable.getDefaultCell().setPaddingRight(100);
						itemTable.addCell(rim);
					} catch (Exception e) {
						logger.error("Error adding pdf image " , e);
					}
				}

				itemTable.getDefaultCell().setPaddingRight(0);
				if((o = dripContent.get("Description")) != null && o instanceof String && StringUtils.isNotBlank(t = (String)o)) {
					logger.debug("OrderItemID=[{}] Description: {}",pdi.getOrderItemID(), t);
					itemTable.addCell(new Phrase(t, textFont));
				}
				itemTable.getDefaultCell().setPaddingTop(10);
				JSONArray items = null;
				if((o = dripContent.get("Items")) != null && o instanceof JSONArray && !(items = (JSONArray)o).isEmpty()) {
					for(Object so: items) {
						if(so instanceof JSONObject) {	//original implementation used this
							JSONObject itemObj = (JSONObject)so;
							//what we're expecting. 
							if((o = itemObj.get("Description")) != null && o instanceof String && StringUtils.isNotBlank(t = (String)o)) {
								//now, see if this exists as an order item. 
								if(hasSelectedPromoItem(dripItems, pdi.getProductID(), t)) {
									logger.debug("OrderItemID=[{}] SelectedItem: {}",pdi.getOrderItemID(), t);
									itemTable.addCell(new Phrase(t, textFont));
								}
							}
						} else if(so instanceof String && StringUtils.isNotBlank(t = (String)so)) {
							//this is how the conversion process does it. 
							if(hasSelectedPromoItem(dripItems, pdi.getProductID(), t)) {
								logger.debug("OrderItemID=[{}] SelectedItem: {}",pdi.getOrderItemID(), t);
								itemTable.addCell(new Phrase(t, textFont));
							}
						}
					}
				}
			}
			document.add(itemTable);
		}
		return checkFlag;
	}

	private boolean hasSelectedPromoItem(List<OrderItem> dripItems, String productID, String t) {
		logger.debug("hasSelectedPromoItem(List, {}, {})", productID, t);
		if(dripItems != null && StringUtils.isNotBlank(productID) && StringUtils.isNotBlank(t)) {
			for(OrderItem oi: dripItems) {
				if(!oi.isIncludeSummary() && productID.equals(oi.getProductID()) && t.equals(oi.getDescription())) {
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * This has been removed as we will now store the drip info in the order item. 
	private void addDripPage(Document document, QuoteViewPage qvp, List<PDFDripInfo> list) throws DocumentException {

		document.newPage();
		PdfPTable itemTable = new PdfPTable(1);
		itemTable.setWidthPercentage(100f);
		itemTable.getDefaultCell().setPadding(0);
		itemTable.getDefaultCell().setBorder(0);
		itemTable.getDefaultCell().setPaddingTop(15);
		itemTable.getDefaultCell().setPaddingBottom(15);
		itemTable.addCell(getHeadingTable(qvp.PageName));

		setHeader(itemTable);
		itemTable.getDefaultCell().setPaddingLeft(7);
		itemTable.addCell(new Phrase("Selected Promotions", subHeader));
		setNormal(itemTable);
		itemTable.getDefaultCell().setBackgroundColor(panelBackground);
		itemTable.addCell(BLANK);
		logger.debug("drip list size: {}", list.size());
		itemTable.getDefaultCell().setPaddingLeft(7);

		boolean first = true;

		for(PDFDripInfo pdi: list) {
			if(first) {
				first = false;
			} else {
				//TODO draw the line here.

			}
			itemTable.getDefaultCell().setPaddingTop(20);

			if(pdi.Name != null) { 
				itemTable.addCell(new Phrase(pdi.Name, dripTitleFont));
			}
			if(pdi.ImagePath != null) {
				try {
					Image rim = PDFUtil.createImage(InitServlet.getRealPath("/" + pdi.ImagePath)); //PropertyEntity.resizeImage(pdi.ImagePath, 1400,	250, PropertyEntity.ResizeOption.Fit));		
					itemTable.getDefaultCell().setPaddingRight(100);
					itemTable.addCell(rim);
				} catch (Exception e) {
					logger.error("Error adding pdf image " , e);
				}
			}

			itemTable.getDefaultCell().setPaddingRight(0);
			if(pdi.Description != null) {
				itemTable.addCell(new Phrase(pdi.Description, textFont));
			}
			itemTable.getDefaultCell().setPaddingTop(10);
			if(!pdi.items.isEmpty()) {
				for(String s: pdi.items) {
					itemTable.addCell(new Phrase(s, textFont));
				}
			}
		}
		document.add(itemTable);
	}
	 */
	private static class PDFDripInfo {
		public String Cover = null;
		public String Name;
		public boolean includePrice = false;
		public String Description;
		public String ImagePath;
		public List<String> items = Collections.emptyList();
		public double cost = 0d;

		@Override
		public String toString() {
			return String.format("%1$s %2$s %3$s %4$s ", Cover, Name, Description, ImagePath);
		}
	}

	private boolean addOptionPages(Document document, Order order, List<OrderItem> orderItems, QuoteView qView) throws DocumentException {
		int size = orderItems.size() - 4;
		List<OrderItem> vpbItems = new ArrayList<OrderItem>(size > 0 ? size : 0);	//at least that many will be plans, facade, open, cover 
		for(OrderItem item: orderItems) {
			if("VPB".equals(item.getCategory())) { 
				vpbItems.add(item);
			}
		}
		Collections.sort(vpbItems, new Comparator<OrderItem>() {
			public int compare(OrderItem oi1, OrderItem oi2) {
				if(oi2.getItemGroupSort() == oi1.getItemGroupSort()) {
					if(oi2.getSortNumber() < oi1.getSortNumber()) {
						return 1;
					} else if (oi2.getSortNumber() == oi1.getSortNumber()) {
						return 0;
					}
					return -1;
				}
				if(oi2.getItemGroupSort() < oi1.getItemGroupSort()) {
					return 1;
				} 
				return -1;
			}
			public boolean equals(OrderItem oi1, OrderItem oi2) {
				return oi2.getItemGroupSort() == oi1.getItemGroupSort() && oi2.getSortNumber() == oi1.getSortNumber();
			}
		});
		if(vpbItems.size() > 0)
		{
			document.newPage();
			addItemPages(document, order, vpbItems, "Selections", qView);
			return true;
		}
		return false;
	} 

	private boolean addCustomPages(Document document, Order order, List<OrderItem> orderItems, QuoteViewPage qvp, QuoteView qView) throws DocumentException {
		String customCat = "Custom-" + qvp.QuoteViewPageID, customName = qvp.PageName;
		if(StringUtils.isBlank(customCat)) {
			return false;
		}
		int size = orderItems.size() - 4;
		List<OrderItem> customItems = new ArrayList<OrderItem>(size > 0 ? size : 0);	//at least that many will be plans, facade, open, cover 
		for(OrderItem item: orderItems) {
			if(customCat.equals(item.getCategory())) { 
				customItems.add(item);
			}
		}
		Collections.sort(customItems, new Comparator<OrderItem>() {
			public int compare(OrderItem oi1, OrderItem oi2) {
				if(oi2.getItemGroupSort() == oi1.getItemGroupSort()) {
					if(oi2.getSortNumber() < oi1.getSortNumber()) {
						return 1;
					} else if (oi2.getSortNumber() == oi1.getSortNumber()) {
						return 0;
					} 
					return -1;
				}
				if(oi2.getItemGroupSort() < oi1.getItemGroupSort()) {
					return 1;
				} 
				return -1;
			}
			//I don't think this is right - method desc would suggest that this method would never get called and 
			//is for the comparator itself, not the items. 
			//public abstract boolean equals(java.lang.Object arg0);
			//public boolean equals(OrderItem oi1, OrderItem oi2) {
			//	return oi2.getItemGroupSort() == oi1.getItemGroupSort() && oi2.getSortNumber() == oi1.getSortNumber();
			//}
		});
		if(customItems.size() > 0)
		{
			document.newPage();
			addItemPages(document, order, customItems, customName, qView);
			return true;
		}
		return false;
	} 

	private void addItemPages(Document document, Order order, List<OrderItem> orderItems, String pageHeading, QuoteView qView) throws DocumentException {

		String groupName = null;
		final float[] widths = getDesignWidths(20, 604,  98, 5, 143, 20);
		PdfPTable itemTable = null;
		boolean first = false;
		boolean newPage = true;
		double groupTotal = 0;
		double optionTotal = 0;
		NumberFormat mf = NumberFormat.getCurrencyInstance();
		mf.setMaximumFractionDigits(0);
		
		int lines = 0;

		for(OrderItem item: orderItems) {
			++lines;
			if(groupName == null || !groupName.equals(item.getItemGroup())) {
				if(itemTable != null) {
					itemTable.getDefaultCell().setPaddingTop(10);
					itemTable.getDefaultCell().setPaddingBottom(10);
					/* do subtotal row */
					itemTable.addCell(BLANK);
					itemTable.addCell(new Phrase("SUB TOTAL", costSubtotalFont));
					itemTable.addCell(BLANK);
					itemTable.addCell(BLANK);
					itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
					itemTable.addCell(new Phrase(formatMoney(groupTotal, mf), costSubtotalFont));
					itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

					itemTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
					itemTable.addCell(BLANK);
					itemTable.getDefaultCell().setColspan(widths.length);
					itemTable.addCell(BLANK);

					optionTotal += groupTotal;
					groupTotal = 0;
					document.add(itemTable);

					itemTable = null;
					groupName = null;
					
					lines += 2;
				}
				if("PAGE_BREAK".equals(item.getName()) || lines > 35) {
					document.newPage();
					newPage = true;
					lines = 0;
					continue;
					/* do new heading? */
				}
				groupName = item.getItemGroup();
				itemTable = new PdfPTable(widths.length);
				itemTable.setWidths(widths);
				itemTable.setWidthPercentage(100f);
				itemTable.getDefaultCell().setPadding(0);
				itemTable.getDefaultCell().setBorder(0);

				if(newPage || lines >= 39) {
					itemTable.getDefaultCell().setColspan(widths.length);
					itemTable.addCell(getHeadingTable(qView, order.getDateOfSale(), pageHeading));
					itemTable.addCell(new Paragraph(" "));
					itemTable.getDefaultCell().setPaddingTop(10);
					itemTable.getDefaultCell().setPaddingLeft(20);
					itemTable.getDefaultCell().setPaddingRight(20);
					itemTable.getDefaultCell().setColspan(1);
					newPage = false;
					lines = 0;
				}

				setHeaderWithoutBG(itemTable);
				itemTable.addCell(BLANK);
				itemTable.getDefaultCell().setBackgroundColor(panelBackground);
				itemTable.getDefaultCell().setPaddingLeft(20);
				itemTable.addCell(new Phrase(groupName, subHeader));
				itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
				itemTable.addCell(new Phrase("Qty", subHeader));
				//itemTable.addCell(new Phrase("Unit", subHeader));
				itemTable.addCell(BLANK);
				itemTable.addCell(new Phrase("Total Cost", subHeader));
				itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				itemTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
				itemTable.addCell(BLANK);
				setNormal(itemTable);
				itemTable.getDefaultCell().setPaddingTop(5);
				itemTable.getDefaultCell().setPaddingBottom(5);
				first = true;
				lines += 3;
			}
			if("PAGE_BREAK".equals(item.getName())) {
				if(itemTable != null) {
					document.add(itemTable);
				}
				lines = 0;
				document.newPage();
				itemTable = new PdfPTable(widths.length);
				itemTable.setWidths(widths);
				itemTable.setWidthPercentage(100f);
				itemTable.getDefaultCell().setPadding(0);
				itemTable.getDefaultCell().setBorder(0);

				if(groupName != null) {
					setHeader(itemTable);
					itemTable.addCell(BLANK);
					itemTable.getDefaultCell().setBackgroundColor(panelBackground);
					itemTable.getDefaultCell().setPaddingLeft(0);
					itemTable.addCell(new Phrase(groupName, subHeader));
					itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
					itemTable.addCell(new Phrase("Qty", subHeader));
					//itemTable.addCell(new Phrase("Unit", subHeader));
					itemTable.addCell(BLANK);
					itemTable.addCell(new Phrase("Total Cost", subHeader));
					itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
					itemTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
					itemTable.addCell(BLANK);
					setNormal(itemTable);
					itemTable.getDefaultCell().setPaddingTop(5);
					itemTable.getDefaultCell().setPaddingBottom(5);
					first = true;
					lines += 2;
				}
				newPage = true;
				continue;
				/* do new heading? */
			}

			if(!skipItemDetails(pageHeading, groupName)) {
				if(StringUtils.isNotBlank(item.getExtraText())) {
					itemTable.getDefaultCell().setPaddingBottom(0);
				}
				itemTable.addCell(BLANK);
				itemTable.addCell(new Phrase(item.getDescription(), textFont));
				//POSS CHANGES HERE, QTY Formatting.
				itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
				itemTable.addCell(new Phrase(String.valueOf(item.getQuantity()), textFont));
				//try {
				//	itemTable.addCell(new Phrase(item.getItemCost() != 0 ? formatMoney(( item.getItemCost() * item.getQuantity()), mf) : "", textFont));
				//} catch (Exception e){
				itemTable.addCell(BLANK);
				//}
				itemTable.addCell(new Phrase(formatMoney(item.getTotalCost(), mf), textFont));
				itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				itemTable.addCell(BLANK);


				if(StringUtils.isNotBlank(item.getExtraText())) {
					itemTable.getDefaultCell().setPaddingTop(0);
					itemTable.getDefaultCell().setPaddingBottom(0);
					if(first) {
						itemTable.getDefaultCell().setPaddingBottom(10);
					}
					itemTable.addCell(BLANK);
					itemTable.addCell(new Phrase(item.getExtraText(), textFont));
					//POSS CHANGES HERE, QTY Formatting.
					itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
					itemTable.addCell(BLANK);
					itemTable.addCell(BLANK);
					itemTable.addCell(BLANK);
					itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

					itemTable.getDefaultCell().setPaddingTop(10);
					itemTable.getDefaultCell().setPaddingBottom(10);
				}
			}
			groupTotal += item.getTotalCost();
			if(first) {
				itemTable.getDefaultCell().setPaddingTop(0);
				first = false;
			}
		}

		/* this can happen if a page break is left at the end */
		if(itemTable == null) {
			itemTable = new PdfPTable(widths.length);
			itemTable.setWidths(widths);
			itemTable.setWidthPercentage(100f);
			itemTable.getDefaultCell().setPadding(0);
			itemTable.getDefaultCell().setBorder(0);
		}

		if(groupName != null) {
			itemTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
			itemTable.getDefaultCell().setPaddingTop(10);
			itemTable.getDefaultCell().setPaddingBottom(10);
			/* end group */
			itemTable.addCell(BLANK);
			itemTable.addCell(new Phrase("SUB TOTAL", costSubtotalFont));
			itemTable.addCell(BLANK);
			itemTable.addCell(BLANK);
			itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			itemTable.addCell(new Phrase(formatMoney(groupTotal, mf), costSubtotalFont));
			itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			itemTable.addCell(BLANK);

			optionTotal += groupTotal;
		}
		//itemTable.getDefaultCell().setColspan(5);
		//itemTable.addCell(BLANK);
		itemTable.getDefaultCell().setColspan(1);
		itemTable.addCell(BLANK);
		itemTable.getDefaultCell().setBackgroundColor(costTotalBackground);
		itemTable.addCell(new Phrase("TOTAL", costTotalFontBold));
		itemTable.addCell(BLANK);
		itemTable.addCell(BLANK);
		itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		itemTable.addCell(new Phrase(formatMoney(optionTotal, mf), costTotalFontBold));
		itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		itemTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		itemTable.addCell(BLANK);

		document.add(itemTable);

	}

	private boolean skipItemDetails(String pageHeading, String groupName) {
		boolean skip = false;
		if (StringUtils.isNotBlank(pageHeading) && StringUtils.isNotBlank(groupName)) {
			String[] headings = new String[]{"Fixed Costs"};
			String[] subHeadings = new String[]{"Site Costs", "Council"};

			skip = ArrayUtils.contains(headings, pageHeading) && ArrayUtils.contains(subHeadings, groupName);

		}
		logger.debug("pageHeading - {}, groupName - {}", pageHeading, groupName);
		logger.debug("Skip - {}", skip);
		return skip;
	}

	private void addSummaryPage(Document document, QuoteViewPage qvp, Order order, List<OrderItem> orderItems, QuoteView qView) throws DocumentException {

		JSONObject summaryMeta = null;
		String key = "Summary-" + qvp.QuoteViewPageID;
		for(OrderItem oi: orderItems) {
			if(key.equals(oi.getCategory())) {
				try {
					summaryMeta = (JSONObject)JSONValue.parseWithException(oi.getMetaData());
				} catch (ParseException pe) {
					logger.error("Was not able to parse Summary meta data", pe);
					return;
				} catch (ClassCastException cce) {
					logger.error("Some other object was stored in meta, expecting object [{}]", oi.getMetaData());
					return;
				}
				break;
			}
		}
		if(summaryMeta == null) { 
			logger.debug("no summary order item found for key {}, skipping page.", key);
			return;
		}
		document.newPage();
		
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMaximumFractionDigits(0);
		final float[] widths = getDesignWidths(20, 604,  98, 143, 20);


		PdfPTable itemTable = new PdfPTable(2);
		itemTable.setWidthPercentage(100f);
		itemTable.getDefaultCell().setPadding(0);
		itemTable.getDefaultCell().setBorder(0);
		itemTable.getDefaultCell().setColspan(2);
		itemTable.addCell(getHeadingTable(qView, order.getDateOfSale(), "Summary"));

		itemTable.getDefaultCell().setPaddingTop(15);
		itemTable.getDefaultCell().setPaddingBottom(15);
		itemTable.getDefaultCell().setPaddingLeft(20);
		itemTable.getDefaultCell().setPaddingRight(20);


		setHeaderWithoutBG(itemTable);
		itemTable.getDefaultCell().setPaddingLeft(7);			
		itemTable.getDefaultCell().setColspan(1);
		setNormal(itemTable);

		document.add(itemTable);
		
		PdfPTable innerTable = new PdfPTable(widths.length);
		innerTable.setWidths(widths);
		innerTable.setWidthPercentage(100f);
		innerTable.getDefaultCell().setBorder(0);
		innerTable.getDefaultCell().setColspan(2);

		innerTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		innerTable.getDefaultCell().setColspan(widths.length);
		innerTable.getDefaultCell().setPaddingTop(5);
		innerTable.getDefaultCell().setPaddingBottom(7);
		innerTable.addCell(BLANK);

		innerTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		innerTable.getDefaultCell().setColspan(1);
		innerTable.getDefaultCell().setPaddingTop(5);
		innerTable.getDefaultCell().setPaddingBottom(7);

	    JSONArray items = (JSONArray)(summaryMeta.containsKey("items") ? summaryMeta.get("items") : null);
	    
	    Object oc = (summaryMeta.containsKey("TotalCost") ? summaryMeta.get("TotalCost") : null);
	    Number tc = null;
	    if(oc != null && oc instanceof Number) tc = (Number)oc;

	    if(items != null) {
	    	for(Object o: items) {
	    		JSONObject item = (JSONObject)o;

				setHeaderWithoutBG(innerTable);
				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setBackgroundColor(panelBackground);
				innerTable.getDefaultCell().setPaddingLeft(20);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				innerTable.addCell(new Paragraph(String.valueOf(item.get("Name")), subHeader));
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
				//innerTable.addCell(new Phrase("", subHeader));
				//itemTable.addCell(new Phrase("Unit", subHeader));
				innerTable.addCell(BLANK);
				innerTable.addCell(new Phrase("Total Cost", subHeader));
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				innerTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
				innerTable.addCell(BLANK);
	    		
	    		JSONArray groups = (JSONArray)(item.containsKey("Groups") ? item.get("Groups") : null);

	    		Number itemcost = (Number)(item.containsKey("TotalCost") ? item.get("TotalCost") : null);

	    		if(groups != null) {
	    			for(Object o2: groups) {
	    				JSONObject gi = (JSONObject)o2;
	    				Number groupcost = (Number)(gi.containsKey("TotalCost") ? gi.get("TotalCost") : null);
	    				Boolean showSummary = (Boolean)(gi.containsKey("ShowSubtotal") ? gi.get("ShowSubtotal") : false);

	    				setNormal(innerTable);

	    				innerTable.getDefaultCell().setPaddingTop(5);
	    	    		innerTable.getDefaultCell().setPaddingBottom(5);
	    	    		
	    				innerTable.addCell(BLANK);
	    				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
	    				innerTable.addCell(new Paragraph(String.valueOf( gi.get("Name")), textFont)); 
	    				innerTable.addCell(BLANK);
	    				if(showSummary) {
	    					innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
	    					innerTable.getDefaultCell().setPaddingRight(paddingRight);
	    					innerTable.addCell(new Paragraph(formatMoney(groupcost.doubleValue(), nf), textFont));
	    					innerTable.getDefaultCell().setPaddingRight(0);
	    				} else {
	    					innerTable.addCell(BLANK);
	    				}
	        			innerTable.addCell(BLANK);
	    			}
	    		}
				setNormal(innerTable);
    			innerTable.addCell(BLANK);
    			innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
    			innerTable.getDefaultCell().setPaddingRight(paddingRight);
    			innerTable.addCell(BLANK);
    			innerTable.addCell(BLANK);
    			innerTable.addCell(new Paragraph(formatMoney(itemcost.doubleValue(), nf), costSubtotalFont));
    			innerTable.getDefaultCell().setPaddingRight(0);
    			innerTable.addCell(BLANK);
	    	}
	    }
	    if(tc != null) {
	    	innerTable.addCell(BLANK);
	    	innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
    		innerTable.addCell(new Paragraph("TOTAL", costTotalFont));
	    	innerTable.addCell(BLANK);
    		innerTable.getDefaultCell().setPaddingRight(paddingRight);
    		innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
	    	innerTable.addCell(new Paragraph(formatMoney(tc.doubleValue(), nf), costTotalFont));
	    	innerTable.getDefaultCell().setPaddingRight(0);
	    	innerTable.addCell(BLANK);
	    }
	    
	    document.add(innerTable);
	}
	
	private void addDocumentPages(Document document, PdfWriter writer, String pageRangeStr, String filePath) {
		String[] pageRange = StringUtils.trimToEmpty(pageRangeStr).split("-"); 
		if(pageRange.length == 0) 
			return;
		if(pageRange.length == 1) {
			if(StringUtils.isBlank(pageRange[0])) {
				return;
			}
		} 
		int[] intRange = new int[2];
		try { 
			intRange[0] = Integer.parseInt(pageRange[0]);
			intRange[1] = pageRange.length == 1 ? Integer.parseInt(pageRange[0]) : Integer.parseInt(pageRange[1]);
		} catch (NumberFormatException nfe) {
			logger.error("Error parsing page range [" + pageRangeStr + "]", nfe);
			intRange = new int[]{1,1};
		}
		final String realPath = InitServlet.getRealPath(filePath);
		logger.debug("Adding pages [{}] from document at {}", pageRangeStr, realPath);

		PdfContentByte cb = writer.getDirectContent();
		try { 
			// Load existing PDF
			PdfReader reader = new PdfReader(realPath);
			int max = reader.getNumberOfPages();
			PdfImportedPage page = null;
			for(int i = intRange[0]; ; i++) {
				page = writer.getImportedPage(reader, i); 
				document.newPage();
				cb.addTemplate(page, 0, 0);

				if(i == intRange[1]) 
					break;
				if(i == max) {
					logger.debug("Reader did not have any more pages Pages: {}, Req {}", intRange[1], max);
					break;
				}
			}
		} catch (IOException ioe) {
			logger.error("Could not load pdf from path : " + realPath, ioe);
		}


	}
}

