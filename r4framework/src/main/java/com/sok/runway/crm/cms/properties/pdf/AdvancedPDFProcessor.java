package com.sok.runway.crm.cms.properties.pdf;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.DocumentException;
import com.sog.pdfserver.client.api.exception.PdfClientValidationException;
import com.sog.pdfserver.client.api.exception.PdfServerConsumeException;
import com.sog.pdfserver.client.api.request.PDFRequest;
import com.sog.pdfserver.client.api.request.TemplateListTag;
import com.sog.pdfserver.client.api.response.PdfGenerateResponse;
import com.sog.pdfserver.client.api.response.SearchTemplateResponse;
import com.sog.pdfserver.client.api.response.TemplateInfo;
import com.sog.pdfserver.client.api.response.TemplateTagInfo;
import com.sog.pdfserver.client.api.service.ConsumerFactory;
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.MailerBean;
import com.sok.framework.RowBean;
import com.sok.framework.RunwayUtil;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.TableSpec;
import com.sok.framework.VelocityManager;
import com.sok.framework.ViewSpec;
import com.sok.runway.OfflineProcess;
import com.sok.runway.SalesRepBean;
import com.sok.runway.URLFilter;
import com.sok.runway.UserBean;
import com.sok.runway.autopilot.AutoPilot;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.pdf.advanced.models.PDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.advanced.wrapper.AdvancedTagUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.CommonPDFUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.CommonUtil;
import com.sok.runway.crm.interfaces.AsyncProcessStatus;
import com.sok.runway.offline.rpmManager.RPMtask;
import com.sok.service.crm.cms.properties.DripService;
import com.sok.service.crm.cms.properties.DripService.DripCollection;

public abstract class AdvancedPDFProcessor extends OfflineProcess implements AsyncProcessStatus {
	public static String PREFIX_REQUEST = "Request_";
	public static String PREFIX_PRODUCT = "Product_";
	public static String PREFIX_QUICK_INDEX = "Quick_";

	public static String PREFIX_PLAN = "Plan_";
	public static String PREFIX_DESIGN = "Design_";
	public static String PREFIX_RANGE = "Range_";
	public static String PREFIX_BRAND = "Brand_";
	public static String PREFIX_BUILDER = "Builder_";
	public static String PREFIX_FACADE = "Facade_";

	public static String PREFIX_LOT = "Lot_";
	public static String PREFIX_STAGE = "Stage_";
	public static String PREFIX_ESTATE = "Estate_";
	public static String PREFIX_DEVELOPER = "Developer_";

	public static String PREFIX_DIMENSIONS = "Dim_";
	public static String PREFIX_SALESREP_DETAILS = "Rep_";
	public static String PREFIX_DISPLAY_DETAILS = "Display_";
	public static String PREFIX_DOCUMENTS = "Docs_";	

	private static final String file_Date_Format = "yyyy_MM_dd_HH_mm";

	private UserBean user = null;
	private String email = "";

	private String pdfURL = null;
	private long pdfLen = -1;

	private int c = 0;

	private final long TIME = 3 * 60 * 1000; 

	private static final String DEFAULT_VELOCITY_PATH = "/cms/emails/offlinealerts/";

	protected String alerttext = "generic/generic_text.vt";
	protected String alerthtml = "generic/generic.vt";

	protected String emailalertsurlhome = null;

	private SimpleDateFormat hhmm = new SimpleDateFormat("HH:mm");
	private SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd MMM yyyy");

	protected String templateID = null;

	protected List<PDFTemplate> pdfTemplates = new ArrayList<PDFTemplate>();

	private static final Logger logger = LoggerFactory.getLogger(AdvancedPDFProcessor.class);

	private static final NumberFormat currancyFull = NumberFormat.getCurrencyInstance();
	private static final NumberFormat currancyShort = NumberFormat.getCurrencyInstance();

	private static HashMap<String,String> imageMap = new HashMap<String, String>();

	public void setStatus(HttpServletRequest request, String status) {
		setStatus(request, status, -1);
	}

	public void setStatus(HttpServletRequest request, String status, double fraction) {

		String classAndMethodName = this.getClass() + " setStatus(request, status, fraction) ";
		logger.debug(classAndMethodName + " Started.");

		logger.debug("setStatus(request, {})", status);
		String statusKey = (String) request.getAttribute("-statusKey");
		if (statusKey == null) {
			logger.debug("no status key from request");
			String fileKey = (String) request.getSession().getAttribute("-statusKey");
			if (fileKey != null) {
				logger.debug("status key from session {} ", fileKey);
				statusKey = fileKey;
			} else {
				logger.error("no status key");
				return;
			}
			request.setAttribute("-statusKey", statusKey);
		}
		logger.debug("using status key {}", statusKey);
		request.getSession().setAttribute(statusKey, makeStatusDiv(status, fraction));

		logger.debug(classAndMethodName + " Ended.");
	}

	private String makeStatusDiv(String status, double fraction) {

		String classAndMethodName = this.getClass() + " makeStatusDiv(status, fraction) ";
		logger.debug(classAndMethodName + " Started.");

		if (fraction == -1) {
			logger.debug(classAndMethodName + " Ended. fraction is -1 ");
			return status;
		}

		logger.debug(classAndMethodName + " Ended.");

		StringBuilder sb = new StringBuilder().append(status);

		if (fraction >= 0) sb.append("<div style=\"width:90%; display:block; height: 15px\"><div style=\"width: ").append(fraction * 100).append("%; background-color: red\">&nbsp;</div></div>").toString();

		return sb.toString();
	}

	public void generatePDF(HttpServletRequest request, HttpServletResponse response, PDFRequest pdfRequest) throws IOException, DocumentException {

		String classAndMethodName = this.getClass() + " generatePDF(HttpServletRequest request, HttpServletResponse response) ";
		logger.debug(classAndMethodName + " Started.");
		setStatus(request, "Loading templates", 0.01d);
		logger.debug("Prepare PDF data");
		try {
			loadPDFTemplate(pdfRequest);
		} catch (PdfServerConsumeException e1) {
			setStatus(request, "failed", -1);
			logger.error("Failed to load templates PdfServerConsumeException {}", e1.getMessage());
			return;
		} catch (PdfClientValidationException e1) {
			setStatus(request, "failed", -1);
			logger.error("Failed PDF to load templates PdfClientValidationException {}", e1.getMessage());
			return;
		} catch (Exception e1) {
			setStatus(request, "failed", -1);
			logger.error("Failed PDF to load templates Exception {}", e1.getMessage());
			URLFilter.exceptionLogger(this, request, e1);
			return;
		}
		setStatus(request, "Loading the properties", 0.02d);
		populatePDFRequest(request, response, pdfRequest);

		if ((pdfRequest.getPageStaticDataList() == null || pdfRequest.getPageStaticDataList().size() == 0) && 
				(pdfRequest.getGroupDataList() == null || pdfRequest.getGroupDataList().size() == 0)) {
			setStatus(request, "nopages", -1);
			logger.error("Failed PDF to Generate PDF, no pages {}", InitServlet.getSystemParam("PDF-AdvancedServerHost"));
			return;
		}

		try {
			ConsumerFactory consumerFactory = ConsumerFactory.getFactory(InitServlet.getSystemParam("PDF-AdvancedServerHost"));
			setStatus(request, "Sending PDF", 0.0d); // Generating PDF - 60%
			logger.debug("Sending pdfRequest {}", pdfRequest.toString());
			PdfGenerateResponse pdfResponse = consumerFactory.getPdfProcessor().process(pdfRequest);
			logger.error("pdfResponse : ", pdfResponse);
			String statusKey = (String) request.getAttribute("-statusKey");
			String downloadURL = pdfResponse.getDownloadURL();
			request.getSession().setAttribute(statusKey + ":filename", downloadURL);
			logger.error("downloadURL is {}", downloadURL);
			if (StringUtils.isNotBlank(downloadURL)) {
				setStatus(request, "Downloading PDF", 0.0d);
				logger.debug("Fetching downloadURL {}", downloadURL);
				// wait 5 seconds before we start checking
				try {
					Thread.sleep(5000);
				} catch (Exception e) {}
				fetchPDFFile(request, response, downloadURL);
			} else {
				setStatus(request, "failed", -1);
				logger.debug("Failed PDF Generate PDF {}", InitServlet.getSystemParam("PDF-AdvancedServerHost"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			setStatus(request, "failed", -1);
			logger.error("Failed PDF to Generate PDF Exception {}", e.getMessage());
		}

		//setStatus(request, "Complete", 1);

		logger.debug(classAndMethodName + " Ended.");
	}

	private void fetchPDFFile(HttpServletRequest request, HttpServletResponse response, String downloadURL) throws IOException {


		// need to grab the outputstream before doing anything else.
		java.io.OutputStream stream = response.getOutputStream();
		// now we can set the content type
		response.setContentType("application/pdf");
		int c500 = 0;
		try {

			if (StringUtils.isNotBlank(downloadURL)) {
				logger.debug("Connecting to downloadURL {}", downloadURL);

				int responseCode = 0;
				// do this a few times so we can wait...
				Date start = new Date();
				Date end = new Date();
				end.setTime(start.getTime() + TIME);
				while (end.after(new Date())) {
					if (c > 0)
						setStatus(request, "Downloading PDF (retry " + c + ")", (double) ((new Date()).getTime() - start.getTime()) / TIME);
					else 
						setStatus(request, "Downloading PDF", (double) ((new Date()).getTime() - start.getTime()) / TIME);
					URL u = new URL(downloadURL);
					HttpURLConnection huc = (HttpURLConnection) u.openConnection();
					huc.setRequestMethod("GET");
					huc.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.0.13) Gecko/2009073021 Runway/4.2.0.2");
					huc.connect();
					if ((responseCode = huc.getResponseCode()) == 200) break;
					if (responseCode == 500) {
						logger.error("Failed PDF, 500 error {}", c500);
						if (++c500 > 5) {
							user = (UserBean) request.getAttribute("currentuser");
							if (user == null && StringUtils.isNotBlank(InitServlet.getSystemParam("PDF-AdvancedServerFallbackURL"))) {
								response.sendRedirect(InitServlet.getSystemParam("PDF-AdvancedServerFallbackURL"));
								setStatus(request, "redirect:" + InitServlet.getSystemParam("PDF-AdvancedServerFallbackURL"), -1);
							}
							setStatus(request, "failed", -1);
							return;
						}

					}
					logger.error("Sleeping to connect to downloadURL {} {}", responseCode, downloadURL);
					try {
						Thread.sleep(1000);
					} catch (Exception e) {}
				}

				if (responseCode == 200) {
					logger.debug("Connected to downloadURL {}", downloadURL);
					BufferedInputStream buf = null;
					try {
						int readBytes = 0;
						final InputStream input = new URL(downloadURL).openStream();
						buf = new BufferedInputStream(input);

						// read from the file; write to the ServletOutputStream
						while ((readBytes = buf.read()) != -1)
							stream.write(readBytes);
					} catch (IOException ioe) {
						throw new ServletException(ioe.getMessage());
					} finally {
						if (stream != null)
							stream.close();
						if (buf != null)
							buf.close();
					}
					setStatus(request, "complete", -1);
					return;
				} else {
					user = (UserBean) request.getAttribute("currentuser");
					if (user != null) email = user.getEmail();

					if (++c <= 5 && StringUtils.isBlank(email)) {
						logger.error("Failed PDF, no email trying again {}", c);
						fetchPDFFile(request, response, downloadURL);
						return;
					} else if (StringUtils.isNotBlank(email)) {
						logger.error("Failed PDF, sending email to {}", email);
						pdfURL = downloadURL;
						setStatus(request, "emailed", -1);
						start();
						return;
					} else if (StringUtils.isNotBlank(InitServlet.getSystemParam("PDF-AdvancedServerFallbackURL"))) {
						logger.error("Failed PDF, no email trying redirect to {}", InitServlet.getSystemParam("PDF-AdvancedServerFallbackURL"));
						setStatus(request, "redirect:" + InitServlet.getSystemParam("PDF-AdvancedServerFallbackURL"), -1);
						response.sendRedirect(InitServlet.getSystemParam("PDF-AdvancedServerFallbackURL"));
						return;
					} else {
						logger.error("Failed PDF, nothing we can do");
						setStatus(request, "failed", -1);
						return;
					}
				}

			}
		} catch (Exception e) {
			setStatus(request, "failed", -1);
		}
	}

	public static void addTag(String homePath, Map<String,String> tags, String prefix, String field, String value) {
		if (StringUtils.isNotBlank(field) && StringUtils.isNotBlank(value)) {
			String tagValue = formatTagValue(value, field, homePath);
			tags.put(prefix + field, tagValue);

			if (value.trim().length() == value.trim().replaceAll("[^0-9\\$\\.\\-\\,]", "").length()) {
				try {
					double d = Double.parseDouble(value.replaceAll("[^0-9\\.\\-]", ""));

					tagValue = formatTagValue(currancyFull.format(d), field, homePath);
					tags.put(prefix + field + "_CurrencyFull", tagValue);

					currancyShort.setMaximumFractionDigits(0);
					tagValue = formatTagValue(currancyShort.format(StringUtil.getRoundedValue(d)), field, homePath);
					tags.put(prefix + field + "_CurrencyShort", tagValue);

					tagValue = formatTagValue(String.valueOf(StringUtil.getRoundedValue(d)), field, homePath);
					tags.put(prefix + field + "_Int", tagValue);

					String srt = String.valueOf(d);
					if (srt.endsWith(".0") || srt.endsWith(".00")) srt = srt.substring(0, srt.indexOf("."));
					tagValue = formatTagValue(srt, field, homePath);
					tags.put(prefix + field + "_Short", tagValue);
				} catch (Exception e) {}
			}
		}
	}

	public static void convertRow2Tags(String homePath, GenRow row, Map<String,String> tags, String prefix) {
		if (row == null || tags == null || !row.isSuccessful())
			return;


		if (row.getViewSpec() != null) {
			ViewSpec vs = row.getViewSpec();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				if (!vs.getLocalItemName(i).endsWith("ID") && row.getData(vs.getLocalItemName(i)) != null) {
					addTag(homePath, tags, prefix, vs.getLocalItemName(i), row.getData(vs.getLocalItemName(i)));
					/*
					String tagValue = formatTagValue(row.getColumn(vs.getLocalItemName(i)), vs.getLocalItemName(i), homePath);
					tags.add(AdvancedTagUtil.createTag(prefix + vs.getLocalItemName(i), tagValue));
					 */
					logger.trace("convertRow2Tags Local : " + (prefix + vs.getLocalItemName(i)) + " = " + row.getData(vs.getLocalItemName(i)));
				}
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				if (!vs.getRelatedItemName(i).endsWith("ID") && row.getData(vs.getRelatedItemName(i)) != null) {
					addTag(homePath, tags, prefix, vs.getRelatedItemName(i), row.getData(vs.getRelatedItemName(i)));
					/*
					String tagValue = formatTagValue(row.getColumn(vs.getRelatedItemName(i)), vs.getRelatedItemName(i), homePath);
					tags.add(AdvancedTagUtil.createTag(prefix + vs.getRelatedItemName(i), tagValue));
					 */
					logger.trace("convertRow2Tags Related : " + (prefix + vs.getRelatedItemName(i)) + " = " + row.getData(vs.getRelatedItemName(i)));
				}
			}
		} else if (row.getTableSpec() != null) {
			TableSpec ts = row.getTableSpec();
			for (int i = 0; i < ts.getFieldsLength(); ++i) {
				if (!ts.getFieldName(i).endsWith("ID") && row.getData(ts.getFieldName(i)) != null) {
					addTag(homePath, tags, prefix, ts.getFieldName(i), row.getData(ts.getFieldName(i)));
					/*
					String tagValue = formatTagValue(row.getColumn(ts.getFieldName(i)), ts.getFieldName(i), homePath);
					tags.add(AdvancedTagUtil.createTag(prefix + ts.getFieldName(i), tagValue));
					 */
					logger.trace("convertRow2Tags Local : " + (prefix + ts.getFieldName(i)) + " = " + row.getData(ts.getFieldName(i)));
				}
			}
		}

	}
	public static void populateProductDetailsTags(HttpServletRequest request, PropertyEntity property, Map<String,String> tags, String prefix) {
		if (StringUtils.isNotBlank(property.getString("ProductID"))) {

			String homePath = getHomePath(request);

			GenRow details = new GenRow();
			details.setRequest(request);
			details.setTableSpec("properties/ProductDetails");
			details.setParameter("ProductID", property.getString("ProductID"));
			details.setParameter("-select", "*");
			details.getResults();

			while (details.getNext()) {
				String type = details.getColumn("DetailsType").replaceAll("[^a-zA-Z0-9]", "");
				String pType = details.getColumn("ProductType").replaceAll("Details", "");

				for (int i = 0; i < details.getTableSpec().getFieldsLength(); i++) {
					String field = details.getTableSpec().getFieldName(i);
					Object o = details.getObject(field);
					if (o != null) {
						addTag(homePath, tags, prefix + type + "_", field, o.toString());
						addTag(homePath, tags, prefix + pType + "_" + type + "_", field, o.toString());
						//tags.put(prefix + type + "_" + field, o.toString()); // Eg: DIM_Activity_SQmeters
						//tags.put(prefix + pType + "_" + type + "_" + field, o.toString());
						// logger.debug("adding {} = {}", field, o);
					}
				}
			}
			details.close();
		} else {
			logger.debug("product id was not set");
		}
	}

	/**
	 * To be overridden for default page event behavior
	 */
	public void populateDefaultTags(HttpServletRequest request, Map<String,String> tags, List<TemplateListTag> listTags, PropertyEntity property) {
		// Populate from appropriate property entity, DRIPS
		// convertRow2Tags(getHomePath(request), property, tags, PREFIX_PRODUCT);
		String homePath = getHomePath(request);
		
		Enumeration<String> param = request.getParameterNames();
		while (param.hasMoreElements()) {
			String s = (String) param.nextElement();
			try {
				tags.put(PREFIX_REQUEST + s.replaceAll("[^a-zA-Z0-9]", ""), request.getParameter(s));
			} catch (Exception e1) {
				try {
					String[] v = request.getParameterValues(s);
					for (int c = 0; c < v.length; ++c) tags.put(PREFIX_REQUEST + s.replaceAll("[^a-zA-Z0-9]", "") + c + "_", v[c]);
				} catch (Exception e2) {
				}
			}
		}
		

		addTag(homePath, tags, "Source_", "Type", "Runway");
		addTag(homePath, tags, "Source_", "Name", InitServlet.getSystemParam("RunwayName"));

		tags.put("Current_Date", RunwayUtil.getCurDateInServerTimeZone(RunwayUtil.ddmmyyyy));
		tags.put("Current_Time", RunwayUtil.getCurDateInServerTimeZone(new SimpleDateFormat("HH:mm:ss")));
		tags.put("Current_DateTime", RunwayUtil.getCurDateInServerTimeZone(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")));

		GenRow quickRow = RunwayUtil.getProductQuick(request, property.getString("ProductID"));

		// we need to make sure that drips are not out of date. This must be done first
		if ("House and Land".equals(quickRow.getString("ProductType")) || "Home Plan".equals(quickRow.getString("ProductType")) || "Existing Property".equals(quickRow.getString("ProductType"))) {
			if ("Y".equals(quickRow.getString("Active")) && quickRow.getString("CurrentStatusID").equals(InitServlet.getSystemParam("ProductAvailableStatus"))) {
				SimpleDateFormat ldf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				boolean doDRIP = false;
				if (StringUtils.isBlank(quickRow.getString("DripModifiedDate"))) {
					doDRIP = true;
				} else if (StringUtils.isNotBlank(quickRow.getString("ModifiedDate"))) {
					try {
						if (ldf.parse(quickRow.getString("ModifiedDate")).getTime() > ldf.parse(quickRow.getString("DripModifiedDate")).getTime()) doDRIP = true;
					} catch (Exception e) {
						doDRIP = true;
					}
				}
				if (doDRIP) {
					DripCollection dc = null;
					if ("House and Land".equals(quickRow.getString("ProductType")) || "Existing Property".equals(quickRow.getString("ProductType")))
						dc = (new DripService()).getAllDrips(request, new String[] {quickRow.getString("ProductID"), quickRow.getString("ProductID"), quickRow.getString("ProductID")}, new Date());
					else
						dc = (new DripService()).getAllDrips(request, new String[] {quickRow.getString("RangeID"), quickRow.getString("DesignID"), quickRow.getString("ProductID")}, new Date());
					double drips = dc.includedDripAmount;
					try {
						double result = drips + Double.parseDouble(quickRow.getString("TotalCost"));

						//fix it for next list
						GenRow update = new GenRow();
						update.setTableSpec("Products");
						update.setRequest(request);
						update.setParameter("ProductID", quickRow.getString("ProductID"));
						update.setParameter("DripCost", String.valueOf(drips));
						update.setParameter("DripResult", String.valueOf(result));
						update.setParameter("DripModifiedDate", "NOW");
						update.doAction("update");

						RPMtask.getInstance().doIndexRefreash("update", quickRow.getString("ProductID"), quickRow.getString("ProductType"));

						quickRow = RunwayUtil.getProductQuick(request, property.getString("ProductID"));
					} catch (Exception e) {}
				}
			}
		}    		

		// For home plan, also add the facade name tokens
		if ("Home Plan".equals(quickRow.getString("ProductType"))) {
			addDefaultFacadeTokens(request, tags, homePath, quickRow.getString("DesignID"), quickRow.getString("ProductID"), quickRow.getDouble("TotalCost"));
			if (isPrefixUsed(PREFIX_FACADE)) addAllFacadeTokens(request, tags, homePath, quickRow.getString("DesignID"), quickRow.getString("ProductID"), quickRow.getDouble("TotalCost"));
		}

		// For H&L, also add the facade name tokens
		if ("House and Land".equals(quickRow.getString("ProductType"))) {
			addDefaultFacadeTokens(request, tags, homePath, quickRow.getString("DesignID"), quickRow.getString("CopiedFromID"), 0);
			if (isPrefixUsed(PREFIX_FACADE)) addAllFacadeTokens(request, tags, homePath, quickRow.getString("DesignID"), quickRow.getString("CopiedFromID"), 0);
			try {
				double dHome = quickRow.getDouble("TotalCost") - quickRow.getDouble("LotPrice");
				addTag(homePath, tags, PREFIX_QUICK_INDEX, "HomeTotalCost", "" + dHome);
			} catch (Exception e) {
			}
		}

		if (isPrefixUsed(PREFIX_PRODUCT)) {
			GenRow productRow = RunwayUtil.getProductRow(request, property.getString("ProductID"));
			convertRow2Tags(homePath, productRow, tags, PREFIX_PRODUCT);
			addTag(homePath, tags, PREFIX_PRODUCT, "DisplayPrice", productRow.getString("DisplayPrice"));
		}

		if (isPrefixUsed(PREFIX_QUICK_INDEX)) {
			convertRow2Tags(homePath, quickRow, tags, PREFIX_QUICK_INDEX);
			addTag(homePath, tags, PREFIX_QUICK_INDEX, "DisplayPrice", quickRow.getString("DisplayPrice"));
		}

		if (isPrefixUsed(PREFIX_PLAN)) 
			convertRow2Tags(homePath, RunwayUtil.getProductRow(request, quickRow.getString("PlanID")), tags, PREFIX_PLAN);
		if (isPrefixUsed(PREFIX_DESIGN))
			convertRow2Tags(homePath, RunwayUtil.getProductRow(request, quickRow.getString("DesignID")), tags, PREFIX_DESIGN);
		if (isPrefixUsed(PREFIX_RANGE)) 
			convertRow2Tags(homePath, RunwayUtil.getProductRow(request, quickRow.getString("RangeID")), tags, PREFIX_RANGE);
		if ("true".equals(InitServlet.getSystemParam("useBrand")) && isPrefixUsed(PREFIX_BRAND))
			convertRow2Tags(homePath, RunwayUtil.getProductRow(request, quickRow.getString("BuilderID")), tags, PREFIX_BRAND);
		if (isPrefixUsed(PREFIX_BUILDER))
			convertRow2Tags(homePath, RunwayUtil.getProductRow(request, quickRow.getString("BuilderProductID")), tags, PREFIX_BUILDER);

		if (isPrefixUsed(PREFIX_LOT))
			convertRow2Tags(homePath, RunwayUtil.getProductRow(request, quickRow.getString("LotID")), tags, PREFIX_LOT);
		if (isPrefixUsed(PREFIX_STAGE))
			convertRow2Tags(homePath, RunwayUtil.getProductRow(request, quickRow.getString("StageID")), tags, PREFIX_STAGE);
		if (isPrefixUsed(PREFIX_ESTATE))
			convertRow2Tags(homePath, RunwayUtil.getProductRow(request, quickRow.getString("EstateID")), tags, PREFIX_ESTATE);
		if (isPrefixUsed(PREFIX_DEVELOPER) || isPrefixUsed("DeveloperPDF_")) {
			convertRow2Tags(homePath, RunwayUtil.getProductRow(request, quickRow.getString("DeveloperID")), tags, PREFIX_DEVELOPER);

			addTag(homePath, tags, "DeveloperPDF_", "Logo", tags.get(InitServlet.getSystemParam("PDF-AdvancedServerDeveloperPDFLogo") + "_ImagePath"));
			addTag(homePath, tags, "DeveloperPDF_", "Name", tags.get(InitServlet.getSystemParam("PDF-AdvancedServerDeveloperPDFBuilder") + "_Description"));

			if (!tags.containsKey("DeveloperPDF_Logo") || tags.get("DeveloperPDF_Logo") == null) addTag(homePath, tags, "DeveloperPDF_", "Logo", tags.get(InitServlet.getSystemParam("PDF-AdvancedServerDeveloperPDFLogo") + "_ThumbnailImagePath"));
			if (!tags.containsKey("DeveloperPDF_Name") || tags.get("DeveloperPDF_Name") == null) addTag(homePath, tags, "DeveloperPDF_", "Name", tags.get(InitServlet.getSystemParam("PDF-AdvancedServerDeveloperPDFBuilder") + "_Name"));
			if (!tags.containsKey("DeveloperPDF_Name") || tags.get("DeveloperPDF_Name") == null) addTag(homePath, tags, "DeveloperPDF_", "Name", InitServlet.getSystemParam("PDF-AdvancedServerDeveloperPDFBuilder"));
		}

		if (isPrefixUsed(PREFIX_DIMENSIONS))
			populateProductDetailsTags(request, property, tags, PREFIX_DIMENSIONS);

		//if (StringUtils.isBlank(templateID)) templateID = getPdfSimpleName(request, property.getString("ProductType"));
		logger.debug("getPdfSimpleName {}", templateID);

		// Default FHB price is considered to be DRIP 6 - create specific if for other DRIP positions
		try {
			String netAmount = property.getDripInfo(request, "", "PDF", "", "DripResultRaw");
			tags.put("NetAmount", StringUtil.getRoundedFormattedValue(netAmount, false));

			String fhbDiscount = property.getDripText(request, templateID, "6");
			logger.debug("fhbDiscount {}", fhbDiscount);
			if (StringUtils.isNotBlank(fhbDiscount)) {
				String fhbPrice = getFHBPrice(netAmount, fhbDiscount);
				tags.put("FHBPrice", fhbPrice);
				logger.debug("fhbPrice {}", fhbPrice);
			}
		} catch(Exception e) {
			e.printStackTrace();
			logger.debug("Error while calculating FHB Price {} with {}" , e.getMessage(), property.getString("ProductID"));
		}
		logger.debug("HomePath {}", homePath);

		try {
			for (int i = 1; i < 17; i++) {
				if (isPrefixUsed("DRIP_TEXT_") || isPrefixUsed("INCLUSION_LIST_")) {
					String dripText = property.getDripText(request, templateID, "" + i);
					if (StringUtils.isBlank(dripText)) dripText = property.getDripText(request, "AnyPDF", "" + i);
					logger.debug("Loading dripText {}, {}", i, dripText);
					if (StringUtils.isNotBlank(dripText) && isPrefixUsed("DRIP_TEXT_" + i + "_")) {
						tags.put("DRIP_TEXT_" + i, dripText);
						listTags.add(AdvancedTagUtil.createTag("INCLUSION_LIST_" + i, DripService.getDripArray(dripText, "\r\n", 100)));
					} else if (StringUtils.isNotBlank(dripText)) {
						listTags.add(AdvancedTagUtil.createTag("INCLUSION_LIST_" + i, DripService.getDripArray(dripText, "\r\n", 100)));
					}
				}
				if (isPrefixUsed("DRIP_IMAGE_" + i + "_")) {
					String dripImage = property.getDripImage(request, templateID, "" + i);
					if (StringUtils.isBlank(dripImage)) dripImage = property.getDripImage(request, "AnyPDF", "" + i);
					logger.debug("Loading dripImage {}, {}", i, dripImage);
					if (StringUtils.isNotBlank(dripImage)) {
						dripImage = lookUpImage(homePath + RunwayUtil.updateImage(dripImage));
						tags.put("DRIP_IMAGE_" + i, dripImage);
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			logger.debug("Error while doing Drip Info {} with {}" , e.getMessage(), property.getString("ProductID"));
		}

		String pdfRep = StringUtils.defaultIfEmpty(request.getParameter("-pdfRep"), "");
		String pdfLocation = request.getParameter("-pdfLocation");
		logger.trace("Loading pdfRep is {}", pdfRep);

		// If estate contact is present, else product rep
		if (pdfRep.equals("EstateContact") && "House and Land".equals(quickRow.getString("ProductType"))) {
			logger.trace("Loading estate rep for {}", quickRow.getString("EstateID"));

			/*SalesRepBean estateRep = CommonPDFUtil.getEstateContactDetails(quickRow.getString("EstateID"), request);
			if(estateRep != null) {
				logger.trace("Loading estate rep is {}", estateRep.getRepUserID());
				GenRow repDetails = CommonUtil.getRep(estateRep.getRepUserID());
				convertRow2Tags(homePath, repDetails, tags, PREFIX_SALESREP_DETAILS);
			}*/

			GenRow contactRow = CommonPDFUtil.getEstateLinkedContacts(quickRow.getString("EstateID"), request);
			String contactID = request.getParameter("ContactID");
			if (contactRow != null) {
				contactID = contactRow.getString("ContactID");
			} else {
				// No Estate contact found, default to product contact
				pdfRep = "Product";
			}
			if (StringUtils.isNotBlank(contactID)) {
				GenRow estateContactDetails = RunwayUtil.getContact(request, contactID);
				convertRow2Tags(homePath, estateContactDetails, tags, PREFIX_SALESREP_DETAILS);
			}			 
		}

		if (pdfRep.equals("Product")) {
			String repUserID = findRepForProduct(property.getData("ProductID"), request.getParameter("GroupID"));
			if (StringUtils.isBlank(repUserID)) repUserID = request.getParameter("RepUserID");
			if (("default".equals(repUserID) || StringUtils.isBlank(repUserID)) && request.getSession().getAttribute(ActionBean.currentuser) != null) repUserID = ((UserBean) request.getSession().getAttribute(ActionBean.currentuser)).getUserID();

			if (StringUtils.isNotBlank(repUserID) && isPrefixUsed(PREFIX_SALESREP_DETAILS)) {
				GenRow repDetails = CommonUtil.getRep(repUserID);
				convertRow2Tags(homePath, repDetails, tags, PREFIX_SALESREP_DETAILS);
			}
		} else {
			String repUserID = request.getParameter("RepUserID");
			if ("default".equals(repUserID)) repUserID = "";
			if (StringUtils.isBlank(repUserID)) repUserID = findRepForProduct(property.getData("ProductID"), request.getParameter("GroupID"));
			if (StringUtils.isBlank(repUserID) && request.getSession().getAttribute(ActionBean.currentuser) != null) repUserID = ((UserBean) request.getSession().getAttribute(ActionBean.currentuser)).getUserID();

			if (StringUtils.isNotBlank(repUserID) && isPrefixUsed(PREFIX_SALESREP_DETAILS)) {
				GenRow repDetails = CommonUtil.getRep(repUserID);
				convertRow2Tags(homePath, repDetails, tags, PREFIX_SALESREP_DETAILS);
			}
		}
		if ("Location".equals(pdfLocation)) {
			String location = quickRow.getString("LocationID");
			if (StringUtils.isBlank(location)) location = quickRow.getString("RegionID");
			if (StringUtils.isBlank(location)) location = request.getParameter("DisplayID");

			if (StringUtils.isNotBlank(location) && isPrefixUsed(PREFIX_DISPLAY_DETAILS)) {
				GenRow displayDetails = CommonUtil.getDisplayLocation(location);
				convertRow2Tags(homePath, displayDetails, tags, PREFIX_DISPLAY_DETAILS);
			}
		} else {
			String location = request.getParameter("DisplayID");
			if (StringUtils.isBlank(location)) location = quickRow.getString("LocationID");
			if (StringUtils.isBlank(location)) location = quickRow.getString("RegionID");

			if (StringUtils.isNotBlank(location) && isPrefixUsed(PREFIX_DISPLAY_DETAILS)) {
				GenRow displayDetails = CommonUtil.getDisplayLocation(location);
				convertRow2Tags(homePath, displayDetails, tags, PREFIX_DISPLAY_DETAILS);
			}
		}

		// Inclusion list 1, by default DRIP 1 is considered inclusion
		//if (isPrefixUsed("INCLUSION_LIST_")) {
		//	for (int d = 1; d < 15; ++d) {
		//		String txt = property.getDripText(request, templateID, "" + d);
		//		if (StringUtils.isNotBlank(txt)) listTags.add(AdvancedTagUtil.createTag("INCLUSION_LIST_" + d, DripService.getDripArray(txt, "\r\n", 100)));
		//	}
		//}

		// Documents
		if (isPrefixUsed(PREFIX_PRODUCT + "Image_")) {
			GenRow estateDocs = getDocumentsRow(request, quickRow.getString("ProductID"));
			while (estateDocs.getNext()) {
				if (StringUtils.isNotBlank(estateDocs.getString("DocumentSubType"))) {
					convertRow2Tags(homePath, estateDocs, tags, PREFIX_PRODUCT + "Image_" + estateDocs.getString("DocumentSubType").replaceAll("[^a-zA-Z0-9]", "") + "_");
				} else {
					convertRow2Tags(homePath, estateDocs, tags, PREFIX_PRODUCT + "Image_");
				}
			}
		}

		if (isPrefixUsed(PREFIX_ESTATE + "Image_")) {
			GenRow estateDocs = getDocumentsRow(request, quickRow.getString("EstateID"));
			while (estateDocs.getNext()) {
				if (StringUtils.isNotBlank(estateDocs.getString("DocumentSubType"))) {
					convertRow2Tags(homePath, estateDocs, tags, PREFIX_ESTATE + "Image_" + estateDocs.getString("DocumentSubType").replaceAll("[^a-zA-Z0-9]", "") + "_");
				} else {
					convertRow2Tags(homePath, estateDocs, tags, PREFIX_ESTATE + "Image_");
				}
			}
		}

		if (isPrefixUsed(PREFIX_PLAN + "Image_")) {
			GenRow planDocs = getDocumentsRow(request, quickRow.getString("PlanID"));
			while (planDocs.getNext()) {
				if (StringUtils.isNotBlank(planDocs.getString("DocumentSubType"))) {
					convertRow2Tags(homePath, planDocs, tags, PREFIX_PLAN + "Image_" + planDocs.getString("DocumentSubType").replaceAll("[^a-zA-Z0-9]", "") + "_");
				} else {
					convertRow2Tags(homePath, planDocs, tags, PREFIX_PLAN + "Image_");
				}
			}
		}

		if (isPrefixUsed(PREFIX_DESIGN + "Image_")) {
			GenRow designDocs = getDocumentsRow(request, quickRow.getString("DesignID"));
			while (designDocs.getNext()) {
				if (StringUtils.isNotBlank(designDocs.getString("DocumentSubType"))) {
					convertRow2Tags(homePath, designDocs, tags, PREFIX_DESIGN + "Image_" + designDocs.getString("DocumentSubType").replaceAll("[^a-zA-Z0-9]", "") + "_");
				} else {
					convertRow2Tags(homePath, designDocs, tags, PREFIX_DESIGN + "Image_");
				}
			}
		}

		if (isPrefixUsed(PREFIX_RANGE + "Image_")) {
			GenRow rangeDocs = getDocumentsRow(request, quickRow.getString("RangeID"));
			while (rangeDocs.getNext()) {
				if (StringUtils.isNotBlank(rangeDocs.getString("DocumentSubType"))) {
					convertRow2Tags(homePath, rangeDocs, tags, PREFIX_RANGE + "Image_" + rangeDocs.getString("DocumentSubType").replaceAll("[^a-zA-Z0-9]", "") + "_");
				} else {
					convertRow2Tags(homePath, rangeDocs, tags, PREFIX_RANGE + "Image_");
				}
			}
		}

		if (isPrefixUsed(PREFIX_BRAND + "Image_")) {
			GenRow brandDocs = getDocumentsRow(request, quickRow.getString("BuilderID"));
			while (brandDocs.getNext()) {
				if (StringUtils.isNotBlank(brandDocs.getString("DocumentSubType"))) {
					convertRow2Tags(homePath, brandDocs, tags, PREFIX_BRAND + "Image_" + brandDocs.getString("DocumentSubType").replaceAll("[^a-zA-Z0-9]", "") + "_");
				} else {
					convertRow2Tags(homePath, brandDocs, tags, PREFIX_BRAND + "Image_");
				}
			}
		}
	}
	private String getFHBPrice(String totalCost, String fhbDiscount) {
		double dNet = Double.parseDouble(totalCost.replaceAll("[^0-9\\-\\.]", ""));
		double dFHB = Double.parseDouble(fhbDiscount.replaceAll("[^0-9\\-\\.]", ""));

		return StringUtil.getRoundedFormattedValue(dNet + dFHB, false);
	}
	/**
	 * IMPORTANT: This method must be overwritten if needed to provide brand specific DRIPs
	 * 
	 * @param request
	 * @return
	 */
	protected String getPdfSimpleName(HttpServletRequest request, String pType) {
		if (pdfTemplates != null && pdfTemplates.size() > 0) {
			return pdfTemplates.get(0).getId();
		}
		logger.debug("PDF simple name, variant {}, ptype {}", request.getParameter("-variant"), pType);
		return StringUtils.defaultIfBlank(request.getParameter("-variant"), PropertyPDFServlet.getDefaultPDFTemplateID(request, StringUtils.deleteWhitespace(pType)));
	}

	public static String getHomePath(HttpServletRequest request) {
		return request.getSession().getServletContext().getInitParameter("URLHome");
	}

	/**
	 * To be overridden for default page event behavior
	 */
	public abstract void populatePDFRequest(HttpServletRequest request, HttpServletResponse response, PDFRequest pdfRequest);

	public static GenRow getDocumentsRow(HttpServletRequest request, String productID) {
		GenRow documents = new GenRow();
		if (StringUtils.isNotBlank(productID)) {
			documents.setRequest(request);
			documents.setParameter("ProductID", productID);
			documents.setViewSpec("LinkedDocumentView");
			documents.doAction("search");
			documents.getResults();
		}
		return documents;
	}

	/* strip out email from "name <email@example.com>" style email fields */
	public static String getFormattedEmail(String email) {
		int s = email.indexOf("<"), e = email.indexOf(">");
		if (s == -1 || e == -1 || e < s)
			return email;
		return email.substring(s + 1, e);
	}

	public static String formatTagValue(String tagValue, String tagName, String homePath) {
		try {
			if (StringUtils.isNotBlank(tagValue)) {
				tagValue = tagName.contains("Email") ? getFormattedEmail(tagValue) : tagValue;
				//tagValue = tagName.contains("Cost") ? StringUtil.getRoundedFormattedValue(tagValue, false) : tagValue;
				tagValue = tagName.contains("ImagePath") || tagName.contains("FilePath") ? lookUpImage(homePath + RunwayUtil.updateImage(tagValue)) : tagValue;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while formatting {} - {}", tagName, e.getMessage());
		}
		return tagValue;
	}

	@Override
	public void execute() {
		StringBuffer fileName = new StringBuffer();
		fileName.append("property_");
		DateFormat dateFormat = new SimpleDateFormat(file_Date_Format);
		fileName.append(dateFormat.format(new java.util.Date()))
		.append("_").append(KeyMaker.generate(5));	      
		fileName.append(".pdf");

		File temp = new File(new File(InitServlet.getRealPath(AutoPilot.AUTOPILOT_FILE_PATH)), fileName.toString());

		try {
			if (StringUtils.isNotBlank(pdfURL)) {
				logger.debug("Connecting to downloadURL {}", pdfURL);

				int responseCode = 0;
				// do this a few times so we can wait...
				for (int x = 0; x < 3600; ++x) {
					URL u = new URL(pdfURL);
					HttpURLConnection huc = (HttpURLConnection) u.openConnection();
					huc.setRequestMethod("GET");
					huc.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.0.13) Gecko/2009073021 Runway/4.2.0.2");
					huc.connect();
					if ((responseCode = huc.getResponseCode()) == 200) break;
					//if (responseCode == 500) break;
					logger.error("Sleeping to connect to downloadURL {} {}", responseCode, pdfURL);
					Thread.sleep(1000);
				}

				if (responseCode == 200) {
					logger.debug("Connected to downloadURL {}", pdfURL);
					BufferedInputStream buf = null;
					FileOutputStream fos = null;
					try {
						int readBytes = 0;
						final InputStream input = new URL(pdfURL).openStream();
						buf = new BufferedInputStream(input);
						fos = new FileOutputStream(temp);

						// read from the file; write to the ServletOutputStream
						while ((readBytes = buf.read()) != -1)
							fos.write(readBytes);
					} catch (IOException ioe) {
						throw new ServletException(ioe.getMessage());
					} finally {
						if (fos != null)
							fos.close();
						if (buf != null)
							buf.close();
					}
					if(temp.length() == 0) {
						logger.debug("file len was 0 for file " + AutoPilot.AUTOPILOT_FILE_PATH + fileName);
						throw new RuntimeException("Failed to output file");
					}
					pdfLen = temp.length();
					pdfURL = InitServlet.getSystemParams().getProperty("URLHome")  + AutoPilot.AUTOPILOT_FILE_PATH + fileName;

					logger.info("PDF Result: {} {}", pdfLen, pdfURL);

					sendPDFEmail();
				} else {
					pdfURL = null;
					logger.debug("Connected to downloadURL failed {}", pdfURL);
				}

			}
		} catch (Exception e) {

		}
	}

	public void loadPDFTemplate(PDFRequest pdfRequest) throws PdfServerConsumeException, PdfClientValidationException {
		logger.error("loadPDFTemplate {}", pdfRequest.toString());
		if (pdfRequest == null || StringUtils.isBlank(pdfRequest.getTemplateId())) return;
		try {
			ConsumerFactory consumerFactory = ConsumerFactory.getFactory(InitServlet.getSystemParam("PDF-AdvancedServerHost"));
			SearchTemplateResponse response = null;
			if (pdfRequest.getTemplateId().equalsIgnoreCase(pdfRequest.getCompanyId())) {
				// if the Template and Category IDs are the same then its a Group PDF ID
				response = consumerFactory.getCompanyService().searchTemplates(pdfRequest.getCompanyName(), pdfRequest.getPdfCategoryId(), pdfRequest.getTemplateId());
				List<TemplateInfo> templates = response.getTemplates();
				for (int x = 0; x < templates.size(); ++x) {
					pdfTemplates.add(new PDFTemplate(templates.get(x)));
				}
				pdfRequest.setCompanyId("");
			} else {
				response = consumerFactory.getCompanyService().searchTemplate(pdfRequest.getCompanyName(), pdfRequest.getPdfCategoryId(), pdfRequest.getTemplateId());
				if (response.getTemplateInfo() != null) pdfTemplates.add(new PDFTemplate(response.getTemplateInfo()));
			}
		} catch (Exception e) {
			URLFilter.exceptionLogger(this, null, e);
			throw e;
		}
	}

	public void filterTagList(PDFTemplate template, Map<String,String> tags) {

		List<TemplateTagInfo> valid = template.getTags();

		if (valid == null || valid.size() == 0) return;

		Set<String> set = new HashSet<String>();

		for (int x = 0; x < valid.size(); ++x) {
			set.add(valid.get(x).getName());
		}

		Object[] keys = tags.keySet().toArray();

		for (int s = 0; s < keys.length; ++s) {
			if (!set.contains(keys[s])) tags.remove(keys[s]);
		}
	}

	public boolean isPrefixUsed(String prefix) {
		if (pdfTemplates == null || pdfTemplates.size() == 0) return true;

		if ("true".equals(InitServlet.getSystemParam("PDF-AdvancedServerFullTags")) ||
				"true".equals(InitServlet.getSystemParam("PDF-AdvancedServerFullListTags"))) return true;

		for (int p = 0; p < pdfTemplates.size(); ++p) {
			if (pdfTemplates.get(p).isUsed(prefix)) return true;
		}

		return false;
	}

	protected String findRepForProduct(String productID, String groupID) {
		if (StringUtils.isBlank(productID)) return "";

		GenRow row = new GenRow();
		row.setTableSpec("ProductSecurityGroups");
		row.setParameter("-select0", "RepUserID");
		row.setParameter("ProductID", productID);
		row.setParameter("RepUserID", "!NULL;!EMPTY");
		if (StringUtils.isNotBlank(groupID)) row.setParameter("GroupID", groupID);
		row.doAction("selectfirst");

		if (StringUtils.isNotBlank(row.getData("RepUserID"))) return row.getData("RepUserID");

		row.remove("GroupID");
		row.doAction("selectfirst");

		return row.getData("RepUserID");
	}

	public static String lookUpImage(String img) {
		if (StringUtils.isBlank(img)) return "";

		if (imageMap.containsKey(img)) return imageMap.get(img);

		GenRow row = new GenRow();
		row.setTableSpec("MigratedResources");
		row.setParameter("-select0", "dest");
		row.setParameter("source", img);
		row.doAction("selectfirst");

		if (StringUtils.isNotBlank(row.getString("dest"))) {
			imageMap.put(img, row.getString("dest"));

			return row.getString("dest");
		}

		return img;
	}

	public String getStatusMailBody() {

		StringBuilder status = new StringBuilder()
				.append(" Runway offline process - PDF Generation Notification ")
				.append(new SimpleDateFormat("d MMM yyyy - h:mma").format(new java.util.Date()))
				.append("<br>\r\n<br>\r\n");

		if(pdfURL == null) {
			status.append("Sorry, we weren't able to make this pdf. Please advise the support team");
		} else {
			status.append("Your pdf has been generated and is available at the following url : <br>\r\n<br>\r\n").append(pdfURL);
		}
		return status.toString();
	}

	public String getStatusMailSubject() {
		return "PDF Generation Notification";
	}

	public String getStatusMailRecipient() {
		return user.getEmail();
	}
	private void sendPDFEmail() {
		logger.debug("Sending end email to user - {} about pdf - {}", user.getUserID(), pdfURL);

		if (!user.isSet("Email")) return;

		sendEmail(user.getString("Email"), getStatusMailSubject(), getStatusMailBody());
	}

	protected void sendEmail(String to, String subject, String msg) {
		try {

			Date d = new Date();
			RowBean context = new RowBean();
			context.put("URLHome", InitServlet.getSystemParams().getProperty("URLHome"));
			context.put("EmailAlertsURLHome", emailalertsurlhome);
			context.put("alert", to);
			context.put("body", msg);
			context.put("subject", subject);
			context.put("date", ddmmyyyy.format(d) + ", " + hhmm.format(d));

			String text = generateEmail(context, alerttext);
			String html = generateEmail(context, alerthtml);
			//String text = generateEmail(context, StringUtil.convertHtmlToText(msg));
			//String html = generateEmail(context, msg);

			ServletConfig sc = InitServlet.getConfig();
			if (sc != null && to.length() != 0) {
				MailerBean mailer = new MailerBean();
				mailer.setServletContext(sc.getServletContext());
				mailer.setFrom(InitServlet.getSystemParams().getProperty("DefaultEmail"));
				mailer.setTo(to);
				mailer.setSubject(subject);
				mailer.setTextbody(text);
				mailer.setHtmlbody(html);
				String response = mailer.getResponse();
			}

		} catch (Exception e) {
			logger.debug("Email of PDF Error {} " , e);
		}
	}

	protected String generateEmail(TableData context, String script) {
		StringWriter text = new StringWriter();
		try {
			StringBuffer roottemplate = new StringBuffer();
			roottemplate.append("#parse(\"");
			roottemplate.append(DEFAULT_VELOCITY_PATH);
			roottemplate.append(script);
			roottemplate.append("\")");

			VelocityEngine ve = VelocityManager.getEngine();
			ve.evaluate(context, text, "StatusAlerts", roottemplate.toString());

			text.flush();
			text.close();

		} catch (Exception e) {
			logger.debug("Email of PDF Generation Error {} " , e);
		}
		return (text.toString());
	}

	private void addDefaultFacadeTokens(HttpServletRequest request, Map<String,String> tags, String homePath, String designID, String planID, double baseCost) {
		logger.debug("Adding facade tokens for {} and {}", designID, planID);
		String facadeName = "", imagePath = "", total_cost = "", homeface_cost = "";
		GenRow row = new GenRow();
		row.setRequest(request);
		row.setViewSpec("ProductLinkedProductView");
		row.setParameter("ProductID", planID);
		row.setParameter("ProductProductsLinkedProduct.ProductType", "Home Facade");
		row.setParameter("IsExclusive", "Y");
		row.setParameter("ProductProductsLinkedProduct.Active", "Y");
		//row.sortBy("ProductProductsLinkedProduct.Name", 0);
		if (StringUtils.isNotBlank(planID)) {		
			row.doAction("selectfirst");
			facadeName = row.getString("Name");
			imagePath = row.getString("ImagePath");
			total_cost = row.getString("TotalCost");
			homeface_cost = "" + (baseCost + row.getData("TotalCost"));
			addTag(homePath, tags, PREFIX_PRODUCT, "DefaultPlanFacadeName", row.getData("Name"));
			addTag(homePath, tags, PREFIX_PRODUCT, "DefaultPlanFacadeImagePath", row.getData("ImagePath"));
			addTag(homePath, tags, PREFIX_PRODUCT, "DefaultPlanFacadeTotalCost", row.getData("TotalCost"));
			addTag(homePath, tags, PREFIX_PRODUCT, "DefaultPlanFacadeHomeFacadeTotalCost", "" + (baseCost + row.getDouble("TotalCost")));

			logger.trace("Adding DefaultPlanFacadeName {}", row.getStatement());
			logger.debug("Adding DefaultPlanFacadeName {}", facadeName);
		}
		if (StringUtils.isNotBlank(designID)) {
			row.setParameter("ProductID", designID);
			row.doAction("selectfirst");
			facadeName = StringUtils.isBlank(facadeName) ? row.getString("Name") : facadeName;
			imagePath = StringUtils.isBlank(imagePath) ? row.getString("Name") : imagePath;
			total_cost = StringUtils.isBlank(total_cost) ? row.getString("TotalCost") : total_cost;
			homeface_cost = StringUtils.isBlank(homeface_cost) ? "" + (baseCost + row.getData("TotalCost")) : homeface_cost;
			addTag(homePath, tags, PREFIX_PRODUCT, "DefaultDesignFacadeName", row.getData("Name"));
			addTag(homePath, tags, PREFIX_PRODUCT, "DefaultDesignFacadeImagePath", row.getData("ImagePath"));
			addTag(homePath, tags, PREFIX_PRODUCT, "DefaultDesignFacadeTotalCost", row.getData("TotalCost"));
			addTag(homePath, tags, PREFIX_PRODUCT, "DefaultDesignFacadeHomeFacadeTotalCost", "" + (baseCost + row.getDouble("TotalCost")));

			logger.trace("Adding DefaultDesignFacadeName {}", row.getStatement());
			logger.debug("Adding DefaultDesignFacadeName {}", row.getData("Name"));
		}

		addTag(homePath, tags, PREFIX_PRODUCT, "DefaultFacadeName", facadeName);
		addTag(homePath, tags, PREFIX_PRODUCT, "DefaultFacadeImagePath", imagePath);
		addTag(homePath, tags, PREFIX_PRODUCT, "DefaultFacadeTotalCost", total_cost);
		addTag(homePath, tags, PREFIX_PRODUCT, "DefaultFacadeHomeFacadeTotalCost", homeface_cost);
		logger.debug("Adding DefaultFacadeName {}", facadeName);
	}

	private void addAllFacadeTokens(HttpServletRequest request, Map<String,String> tags, String homePath, String designID, String planID, double basePrice) {
		logger.debug("Adding facade tokens for {} and {}", designID, planID);
		int c = 0;
		GenRow row = new GenRow();
		row.setRequest(request);
		row.setViewSpec("ProductLinkedProductView");
		row.setParameter("ProductID", planID);
		row.setParameter("ProductProductsLinkedProduct.ProductType", "Home Facade");
		row.setParameter("ProductProductsLinkedProduct.Active", "Y");
		//row.sortBy("ProductProductsLinkedProduct.Name", 0);
		if (StringUtils.isNotBlank(planID)) {		
			row.doAction("search");

			row.getResults();

			while (row.getNext()) {
				++c;
				addTag(homePath, tags, PREFIX_FACADE + c + "_", "Name", row.getData("Name"));
				addTag(homePath, tags, PREFIX_FACADE + c + "_", "ImagePath", row.getData("ImagePath"));
				addTag(homePath, tags, PREFIX_FACADE + c + "_", "TotalCost", row.getData("TotalCost"));
				addTag(homePath, tags, PREFIX_FACADE + c + "_", "HomeFacadeTotalCost", "" + (basePrice + row.getDouble("TotalCost")));
			}
		}
		if (StringUtils.isNotBlank(designID)) {
			row.setParameter("ProductID", designID);
			row.doAction("search");

			row.getResults();

			while (row.getNext()) {
				++c;
				addTag(homePath, tags, PREFIX_FACADE + c + "_", "Name", row.getData("Name"));
				addTag(homePath, tags, PREFIX_FACADE + c + "_", "ImagePath", row.getData("ImagePath"));
				addTag(homePath, tags, PREFIX_FACADE + c + "_", "TotalCost", row.getData("TotalCost"));
				addTag(homePath, tags, PREFIX_FACADE + c + "_", "HomeFacadeTotalCost", "" + (basePrice + row.getDouble("TotalCost")));
			}
		}
		row.close();
	}

	public boolean sendEmail() {
		return false;
	}

	public static void clearImageMap() {
		imageMap = new HashMap<String,String>();
	}

}