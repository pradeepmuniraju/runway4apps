package com.sok.runway.crm.cms.properties.excel.templates.model;

public class RealEstate {
	private String estateID;
	private String estateName;
	private String stageName;
	private String suburb;
	private String lotNumber;
	private String landSize;
	private String size;
	private String internalArea;
	private String externalArea;
	private String facade;
	private String houseName;
	private double beds;
	private double baths;
	private double garage;
	private String sqs;

	private String status;

	private double housePrice;
	private double landPrice;
	private double facadePrice;
	private double totalPrice;

	private String regionName;
	private String locationName;

	private String groupName = "";
	private String salesRepName = "";
	private String salesAgentName = "";
	private String exclusiveDate = "";
	public String getEstateID() {
		return estateID;
	}
	public void setEstateID(String estateID) {
		this.estateID = estateID;
	}
	public String getEstateName() {
		return estateName;
	}
	public void setEstateName(String estateName) {
		this.estateName = estateName;
	}
	public String getStageName() {
		return stageName;
	}
	public void setStageName(String stageName) {
		this.stageName = stageName;
	}
	public String getSuburb() {
		return suburb;
	}
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	public String getLotNumber() {
		return lotNumber;
	}
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public String getLandSize() {
		return landSize;
	}
	public void setLandSize(String landSize) {
		this.landSize = landSize;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getInternalArea() {
		return internalArea;
	}
	public void setInternalArea(String internalArea) {
		this.internalArea = internalArea;
	}
	public String getExternalArea() {
		return externalArea;
	}
	public void setExternalArea(String externalArea) {
		this.externalArea = externalArea;
	}
	public String getFacade() {
		return facade;
	}
	public void setFacade(String facade) {
		this.facade = facade;
	}
	public String getHouseName() {
		return houseName;
	}
	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}
	public double getBeds() {
		return beds;
	}
	public void setBeds(double beds) {
		this.beds = beds;
	}
	public double getBaths() {
		return baths;
	}
	public void setBaths(double baths) {
		this.baths = baths;
	}
	public double getGarage() {
		return garage;
	}
	public void setGarage(double garage) {
		this.garage = garage;
	}
	public String getSqs() {
		return sqs;
	}
	public void setSqs(String sqs) {
		this.sqs = sqs;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getHousePrice() {
		return housePrice;
	}
	public void setHousePrice(double housePrice) {
		this.housePrice = housePrice;
	}
	public double getLandPrice() {
		return landPrice;
	}
	public void setLandPrice(double landPrice) {
		this.landPrice = landPrice;
	}
	public double getFacadePrice() {
		return facadePrice;
	}
	public void setFacadePrice(double facadePrice) {
		this.facadePrice = facadePrice;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getSalesRepName() {
		return salesRepName;
	}
	public void setSalesRepName(String salesRepName) {
		this.salesRepName = salesRepName;
	}
	public String getSalesAgentName() {
		return salesAgentName;
	}
	public void setSalesAgentName(String salesAgentName) {
		this.salesAgentName = salesAgentName;
	}
	public String getExclusiveDate() {
		return exclusiveDate;
	}
	public void setExclusiveDate(String exclusiveDate) {
		this.exclusiveDate = exclusiveDate;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}