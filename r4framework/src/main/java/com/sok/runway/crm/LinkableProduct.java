package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;


public abstract class LinkableProduct extends AuditEntity {
   
   private Product product = null;
   
   public LinkableProduct(Connection con, String viewSpec) {
      super(con, viewSpec);
   }
   
   public LinkableProduct(ServletRequest request, String viewSpec) {
      super(request, viewSpec);  
   }
   
   public String getProductID() {
      return getField("ProductID");
   }
   
   public void setProductID(String productID) {
      setField("ProductID", productID);
   }
   
   public Product getProduct() {
      if (product == null) {
         product = new Product(getConnection());
         product.setCurrentUser(getCurrentUser());
      }
      if (!product.isLoaded() && getProductID().length() != 0) {
         product.load(getProductID());
      }
      return product;
   }
   
   public void postLoad() {
      super.postLoad();
      if (isLoaded()) {
         getProduct();
      }
   }
   
   public void clear() {
      super.clear();
      if (product != null) {
         product.clear();
      }
   }
}