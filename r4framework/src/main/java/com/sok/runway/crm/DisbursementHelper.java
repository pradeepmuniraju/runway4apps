package com.sok.runway.crm;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.RunwayUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.JSPLogger;

public class DisbursementHelper {
	
	// All Disbursement keys
	public String DIS_KEY_FULL_DEPOSIT_TRANSFER = "Full";
	public String DIS_KEY_ACCOUNT_SALES = "Balance";
	public String DIS_KEY_DEBIT_SALES = "Nil";
	
	// All Disbursement labels
	public String DIS_LABEL_FULL_DEPOSIT_TRANSFER = "Full Deposit Transfer";
	public String DIS_LABEL_ACCOUNT_SALES = "Account Sales";
	public String DIS_LABEL_DEBIT_SALES = "Debit Sales";
	
	// All token Keys
	public String DIS_TOKEN_KEY_VENDOR_PAYMENT = "Type.VendorPayment";	
	public String DIS_TOKEN_KEY_VENDOR_NOTICE = "Type.VendorNotice";	
	public String DIS_TOKEN_KEY_GROSS_COMMISSION = "Type.Commission";
	public String DIS_TOKEN_KEY_PARTIAL_GROSS_COMMISSION = "Type.PartialCommission";
	
	// All token labels
	public String DIS_TOKEN_LABEL_VENDOR_PAYMENT = "Vendor payment";	
	public String DIS_TOKEN_LABEL_VENDOR_NOTICE = "Vendor notice";	
	public String DIS_TOKEN_LABEL_GROSS_COMMISSION = "Gross commission";
	public String DIS_TOKEN_LABEL_PARTIAL_GROSS_COMMISSION = "Partial Gross commission";
	
	// From closed payments
	public String Letter_Subtype_ContractCoverLettertoVendor = "ContractCoverLettertoVendor";
	
	// From closed disbursements
	public String Letter_Subtype_ACSalesCreditVendorSolicitorEFT = "ACSalesCreditVendorSolicitorEFT";
	public String Letter_Subtype_ACSalesDebitVendorSolicitor = "ACSalesDebitVendorSolicitor";
	public String Letter_Subtype_DepositTransferEFTFullDepositTaxInvoiceLater = "DepositTransferEFTFullDepositTaxInvoiceLater";
	
	// From closed commissions
	public String Letter_Subtype_TaxInvoice = "TaxInvoice";
	
	public String[] getDisbursementKeys() {
		return new String[] {DIS_KEY_FULL_DEPOSIT_TRANSFER, DIS_KEY_ACCOUNT_SALES, DIS_KEY_DEBIT_SALES};
	}
	
	public String[] getDisbursementLabels() {
		return new String[] {DIS_LABEL_FULL_DEPOSIT_TRANSFER, DIS_LABEL_ACCOUNT_SALES, DIS_LABEL_DEBIT_SALES};
	}
	
	public String[] getDisbursementTokenKeys() {
		return new String[] {DIS_TOKEN_KEY_VENDOR_PAYMENT, DIS_TOKEN_KEY_VENDOR_NOTICE, DIS_TOKEN_KEY_GROSS_COMMISSION, DIS_TOKEN_KEY_PARTIAL_GROSS_COMMISSION};
	}
	
	public String[] getDisbursementTokenLabels() {
		return new String[] {DIS_TOKEN_LABEL_VENDOR_PAYMENT, DIS_TOKEN_LABEL_VENDOR_NOTICE, DIS_TOKEN_LABEL_GROSS_COMMISSION,DIS_TOKEN_LABEL_PARTIAL_GROSS_COMMISSION};
	}
	
	public String getDisbursementTypeLabel(String disbursementKey) {
		HashMap<String, String> disburementMap = new HashMap<String, String>();	
		disburementMap.put(DIS_KEY_DEBIT_SALES, DIS_LABEL_DEBIT_SALES);
		disburementMap.put(DIS_KEY_ACCOUNT_SALES, DIS_LABEL_ACCOUNT_SALES);
		disburementMap.put(DIS_KEY_FULL_DEPOSIT_TRANSFER, DIS_LABEL_FULL_DEPOSIT_TRANSFER);
		
		return StringUtils.defaultString(disburementMap.get(disbursementKey));
	}
	
	 
	
	public String getDisbursementTypeOptions(HttpServletRequest request, String selectedOption) {
		JSPLogger.debug(this, "selectedOption {}", selectedOption);
		String text = "";
				
		int i = 0;
		String[] labels = getDisbursementLabels();
		for(String key : getDisbursementKeys()) {	
			String selected = key.equals(selectedOption) ? "selected" : "";
			JSPLogger.debug(this, "key {} selected {}", key, selected);
			
			text += "<option value='" + key + "'" + selected+ ">" + labels[i] + "</option>";
			i++;
		}
		return text;
	}
	
	public String getDisbursementTokensOptions(HttpServletRequest request, String selectedOption) {
		String text = "";
		/*
		GenRow lists = new GenRow();
		lists.setViewSpec("ListItemView");
		lists.setRequest(request);
		lists.setColumn("ListItemItemList.ListName", "PaymentsTokens");
		lists.sortBy("ItemLabel",0);
		lists.doAction(ActionBean.SEARCH);
		lists.getResults();
		
		while (lists.getNext()) {
			text += "<option value='" + lists.getString("ItemValue") + "'>" + lists.getString("ItemLabel") + "</option>";
		}
		lists.close();
		*/
		int i = 0;
		String[] labels = getDisbursementTokenLabels();
		for(String key : getDisbursementTokenKeys()) {	
			String selected = key.equals(selectedOption) ? "selecetd" : "";
			text += "<option value='" + key + "'" + selected+ ">" + labels[i] + "</option>";
			i++;
		}
		return text;
	}
	
	public double getEarnedGrossCommission(HttpServletRequest request, String opportunityID) {
	 	GenRow paymentTemplates = new GenRow();
		paymentTemplates.setRequest(request);
		paymentTemplates.setViewSpec("CommissionView");
		paymentTemplates.setParameter("OpportunityID", opportunityID);
		paymentTemplates.setParameter("CommissionType", "pay");
		paymentTemplates.setParameter("-select0", "SUM(TotalAmount) AS GrossCommission");			
		paymentTemplates.doAction("selectfirst");
		
		return paymentTemplates.getDouble("GrossCommission");
	 }	
	 
	public double getDeposit(HttpServletRequest request, String opportunityID) {
	 	GenRow paymentTemplates = new GenRow();
		paymentTemplates.setRequest(request);
		paymentTemplates.setViewSpec("CommissionView");
		paymentTemplates.setParameter("OpportunityID", opportunityID);
		paymentTemplates.setParameter("CommissionType", "payments");
		paymentTemplates.setParameter("-select0", "SUM(TotalAmount) AS GrossDeposit");			
		paymentTemplates.doAction("selectfirst");
		
		return paymentTemplates.getDouble("GrossDeposit");
	 }	
	
	 public void createDisbursements(HttpServletRequest request, String opportunityID, String productID) {
		if (StringUtils.isBlank(opportunityID)) return;
		
		// Select the Payment template who have the transaction Type set
		GenRow paymentTemplates = new GenRow();
		paymentTemplates.setRequest(request);
		paymentTemplates.setViewSpec("CommissionView");
		paymentTemplates.setParameter("CommissionType", "payments");
		paymentTemplates.setParameter("TransactionType", "!NULL;!EMPTY");
		paymentTemplates.setParameter("OpportunityID", opportunityID);
		paymentTemplates.doAction(GenerationKeys.SEARCH);
		paymentTemplates.getResults();
		
		if (paymentTemplates.getNext()) {
			JSPLogger.debug(this, "Inserting lines for txn type {}", paymentTemplates.getData("TransactionType"));
			JSPLogger.debug(this, "Inserting lines for CommissionID  {}", paymentTemplates.getData("LinkedCommissionID"));
			
			// FULL
			if(DIS_KEY_FULL_DEPOSIT_TRANSFER.equals(paymentTemplates.getData("TransactionType"))) {
				insertDisbursements(request, paymentTemplates, paymentTemplates.getDouble("TotalAmount"), DIS_TOKEN_LABEL_VENDOR_PAYMENT, "0", DIS_TOKEN_KEY_VENDOR_PAYMENT, null);
			
			} else if(DIS_KEY_ACCOUNT_SALES.equals(paymentTemplates.getData("TransactionType"))) {
				// Account Sales
				double grossCommission = getEarnedGrossCommission(request, opportunityID);
				JSPLogger.debug(this, "Disbursement {}", paymentTemplates.getDouble("TotalAmount") - grossCommission);
				JSPLogger.debug(this, "grossCommission {}", grossCommission);
				
				insertDisbursements(request, paymentTemplates, paymentTemplates.getDouble("TotalAmount") - grossCommission, DIS_TOKEN_LABEL_VENDOR_PAYMENT, "0", DIS_TOKEN_KEY_VENDOR_PAYMENT, null);
				insertDisbursements(request, paymentTemplates, grossCommission, DIS_TOKEN_LABEL_GROSS_COMMISSION, "1", DIS_TOKEN_KEY_GROSS_COMMISSION, getSettlementDate(request, productID));
				
			} else if(DIS_KEY_DEBIT_SALES.equals(paymentTemplates.getData("TransactionType"))) {
				// Debit Sales
				double deposit = getDeposit(request, opportunityID);
				JSPLogger.debug(this, "Deposit {}", deposit);
				
				insertDisbursements(request, paymentTemplates, 0, DIS_TOKEN_LABEL_VENDOR_NOTICE, "0", DIS_TOKEN_KEY_VENDOR_NOTICE, null);
				insertDisbursements(request, paymentTemplates, deposit, DIS_TOKEN_LABEL_PARTIAL_GROSS_COMMISSION, "1", DIS_TOKEN_KEY_PARTIAL_GROSS_COMMISSION, getSettlementDate(request, productID));
			}
		}
	}
	
	 
	public String getSettlementDate(HttpServletRequest request, String productID) {
		GenRow lotDetails = new GenRow();
	 	lotDetails.setRequest(request);
	 	lotDetails.setViewSpec("ProductView");
	 	lotDetails.setParameter("ProductID", productID);
	    lotDetails.doAction("selectfirst");
	    return StringUtils.isBlank(lotDetails.getString("ActSettlementDate")) ?  lotDetails.getData("EstSettlementDate") : lotDetails.getData("ActSettlementDate");
	}
	
	public void insertDisbursements(HttpServletRequest request, GenRow paymentRow, double amount, String description, String sortOrder, String token, String date) {
		boolean useDashboard = "true".equals(InitServlet.getSystemParams().getProperty("UsePaymentsDashboard"));
		try {
			String commPaymentID = KeyMaker.generate();
			
			GenRow disbursementBucket = new GenRow();
			disbursementBucket.setTableSpec("CommissionPayments");
			disbursementBucket.setRequest(request);
			disbursementBucket.setColumn("CommissionPaymentID" , commPaymentID);
			disbursementBucket.setColumn("CommissionID", paymentRow.getString("LinkedCommissionID"));
			disbursementBucket.setColumn("Description", description);
			disbursementBucket.setParameter("Amount", amount);
			disbursementBucket.setParameter("DueAmount", amount);
			//disbursementBucket.setColumn("PercentageOfPayment", cItems.getString("Percent"));
			//disbursementBucket.setColumn("PayByDays", cItems.getString("DueDays"));
			//disbursementBucket.setColumn("PayByStatus", cItems.getString("DueStatus"));
			disbursementBucket.setColumn("SortOrder", sortOrder);
			disbursementBucket.setColumn("Editable", "Y");
			//disbursementBucket.setColumn("EditableDescription", cItems.getString("EditableDescription"));
			//disbursementBucket.setColumn("CommissionTemplateItemID", cItems.getString("CommissionTemplateItemID"));
			disbursementBucket.setColumn("Token", token);    				
			disbursementBucket.setColumn("IsTemplate", useDashboard ? "Y" : "N");
			disbursementBucket.setColumn("Completed", "N");
			
			
			if(date != null) {
				Date fromDate = RunwayUtil.getDateObject(date, RunwayUtil.ddmmyyyy);
				disbursementBucket.setParameter("PayByDate", fromDate);
			}
			
			disbursementBucket.doAction(GenerationKeys.INSERT);
			JSPLogger.debug(this, "Inserted {} {}", description, commPaymentID);
		} catch(Exception e) {
			JSPLogger.debug(this, "Error when inserting commissions ");
			e.printStackTrace();
		}
	}
	
	public double[] calculateCreditDebit (GenRow transactions) {
		double creditAmount = 0, debitAmount = 0;
		if(StringUtils.isNotBlank(transactions.getString("TransactionSubType"))) {
			HashMap<String, Boolean> creditMap = new HashMap<String, Boolean>();
			creditMap.put("payments" , true);
			creditMap.put("transact" , false);
			
			Boolean isCredit = creditMap.get(transactions.getString("TransactionSubType"));
			if(isCredit == null) {
				isCredit = true;
				JSPLogger.debug(this,"TransactionSubType is null {} ", transactions.getString("TransactionSubType"));
			}
			
			double paidAmount = Math.abs(transactions.getDouble("PaidAmount"));
			
			if(transactions.getDouble("PaidAmount") >= 0) {
				if (isCredit) {
					creditAmount = paidAmount;
				} else {
					debitAmount = paidAmount;
				}
			} else {
				if (isCredit) {
					debitAmount = paidAmount;
				} else {
					creditAmount = paidAmount;
				}
			}
		}
		return new double[]{creditAmount, debitAmount};
	}
	
	public double getOpeningBalance(HttpServletRequest request) {
		GenRow row = new GenRow();
		row.setRequest(request);
		row.setTableSpec("SystemParameters");	
		row.setParameter("Name", "Trust-StatementBalance");
		row.setParameter("-select0", "*");
		row.doAction("selectfirst");
		
		String openingBalance = StringUtils.defaultIfBlank(row.getString("Value"), "");
		openingBalance = openingBalance.replaceAll("[^0-9\\.]","");
		try {
			return Double.parseDouble(openingBalance);
		}catch(Exception e){
			JSPLogger.debug(this,"openingBalance from SystemParameters is {} ", openingBalance);
			return 0;
		}
	}
	
	
	public void updateOpeningBalance(HttpServletRequest request, double newOpeningBalance) {
		GenRow row = new GenRow();
		row.setRequest(request);
		row.setTableSpec("SystemParameters");	
		row.setParameter("ON-Name", "Trust-StatementBalance");
		row.setParameter("Value", newOpeningBalance);
		row.doAction("updateall");
		
		JSPLogger.debug(this,"Updated openingBalance in SystemParameters as {} ", newOpeningBalance);
	}
	
	
	public String[] getLetterVariant(String commissionType, String transactionType, String token) {
		String title = "";
		String subVariant = "";
		DisbursementHelper hlpr = new DisbursementHelper();
		
		if("payments".equals(commissionType)) {
			title = "Contract cover letter to vendor";
			subVariant =  Letter_Subtype_ContractCoverLettertoVendor;
			
		} else if("transact".equals(commissionType)) {
			
			if(hlpr.DIS_TOKEN_KEY_VENDOR_PAYMENT.equals(token) && hlpr.DIS_KEY_FULL_DEPOSIT_TRANSFER.equals(transactionType) ) {
				title = "Full Deposit Letter";
    			subVariant = Letter_Subtype_DepositTransferEFTFullDepositTaxInvoiceLater; // full vendor payment
    			
			} else if(hlpr.DIS_TOKEN_KEY_VENDOR_PAYMENT.equals(token) && hlpr.DIS_KEY_ACCOUNT_SALES.equals(transactionType) ) {
				title = "AC Sales Credit Letter";
        		subVariant = Letter_Subtype_ACSalesCreditVendorSolicitorEFT; // account sales vendor payment
    			
    		} else if(hlpr.DIS_TOKEN_KEY_VENDOR_NOTICE.equals(token)) {
    			title = "AC Sales Debit Letter";
    			subVariant = Letter_Subtype_ACSalesDebitVendorSolicitor;
    		}
			
        } else if("pay".equals(commissionType)) {
        	title = "Tax Invoice";
        	subVariant =  Letter_Subtype_TaxInvoice;
		}
		
		return new String[] {subVariant, title};
	}
	
	public GenRow getLandProduct(HttpServletRequest request, String productID) {
		GenRow row = new GenRow();
		if (StringUtils.isNotBlank(productID)) {
			row.setViewSpec("{properties/}ProductLotView");
			row.setRequest(request);
			row.setColumn("ProductID", productID);
			row.setColumn("ProductType", "Land");
			row.doAction("selectfirst");
		}
		return row;
	}
	
	/* Returns GenRow if any process level templates are applied  */
    public GenRow getProcessTemplates(HttpServletRequest request, String processID, String productID, String groupID) {
    	JSPLogger.debug(this, "processID {}, productID {}", processID, productID);
    	JSPLogger.debug(this, "groupID {}", groupID);
    	
    	GenRow product = com.sok.runway.crm.Product.loadProduct(productID);
    	
        GenRow processBean = new GenRow();
        processBean.setRequest(request);
        processBean.setViewSpec("ProcessProductTemplateView");
        processBean.setParameter("ProcessID", processID);
        processBean.setParameter("CommissionID", "!NULL;!EMPTY");
        processBean.setParameter("GroupID", groupID);
        processBean.setParameter("ApplyTo|1","Product:" + product.getString("ProductType"));
        
		if ("Land".equals(product.getString("ProductType")) || "House and Land".equals(product.getString("ProductType"))) {
			processBean.addParameter("ApplyTo|1","NULL+EMPTY");
			if ("House and Land".equals(product.getString("ProductType"))) {
				processBean.addParameter("ApplyTo^5|1","ProductType:Land,Product:" + product.getString("ProductType"));
				processBean.addParameter("ApplyToID^5|1",product.getString("LotProductID"));
			} else {
				processBean.addParameter("ApplyTo^5|1","ProductType:Land,Product:" + product.getString("ProductType"));
				processBean.addParameter("ApplyToID^5|1",product.getString("ProductID"));
			}
			processBean.addParameter("ApplyTo^6|1","ProductType:Stage,Product:" + product.getString("ProductType"));
			processBean.addParameter("ApplyToID^6|1",product.getString("StageProductID"));
			processBean.addParameter("ApplyTo^7|1","ProductType:Estate,Product:" + product.getString("ProductType"));
			processBean.addParameter("ApplyToID^7|1",product.getString("EstateProductID"));
			processBean.addParameter("ApplyTo^8|1","ProductType:Developer,Product:" + product.getString("ProductType"));
			processBean.addParameter("ApplyToID^8|1",product.getString("DeveloperProductID"));
		}
		
		if ("Home Plan".equals(product.getString("ProductType")) || "House and Land".equals(product.getString("ProductType"))) {
			processBean.addParameter("ApplyTo|1","NULL+EMPTY");
			if ("House and Land".equals(product.getString("ProductType"))) {
				processBean.addParameter("ApplyTo^9|1","ProductType:Home Plan,Product:" + product.getString("ProductType"));
				processBean.addParameter("ApplyToID^9|1",product.getString("CopyPlanID"));
			} else {
				processBean.addParameter("ApplyTo^9|1","ProductType:Home Plan,Product:" + product.getString("ProductType"));
				processBean.addParameter("ApplyToID^9|1",product.getString("ProductID"));
			}
			processBean.addParameter("ApplyTo^10|1","ProductType:Home,Product:" + product.getString("ProductType"));
			processBean.addParameter("ApplyToID^10|1",product.getString("HomeProductID"));
			processBean.addParameter("ApplyTo^11|1","ProductType:Home Range,Product:" + product.getString("ProductType"));
			processBean.addParameter("ApplyToID^11|1",product.getString("RangeProductID"));
			processBean.addParameter("ApplyTo^12|1","ProductType:Brand,Product:" + product.getString("ProductType"));
			processBean.addParameter("ApplyToID^12|1",product.getString("BrandProductID"));
		}
		
		if ("Apartment".equals(product.getString("ProductType"))) {
			processBean.addParameter("ApplyTo|1","NULL+EMPTY");
			processBean.addParameter("ApplyTo^5|1","ProductType:Apartment,Product:" + product.getString("ProductType"));
			processBean.addParameter("ApplyToID^5|1",product.getString("ProductID"));
			processBean.addParameter("ApplyTo^6|1","ProductType:Building Stage,Product:" + product.getString("ProductType"));
			processBean.addParameter("ApplyToID^6|1",product.getString("BuildingStageProductID"));
			processBean.addParameter("ApplyTo^7|1","ProductType:Building,Product:" + product.getString("ProductType"));
			processBean.addParameter("ApplyToID^7|1",product.getString("BuildingProductID"));
			processBean.addParameter("ApplyTo^8|1","ProductType:Building Developer,Product:" + product.getString("ProductType"));
			processBean.addParameter("ApplyToID^8|1",product.getString("BuildingDeveloperID"));
		}
		
         if(StringUtils.isNotBlank(processID)) {
        	processBean.doAction(GenerationKeys.SEARCH);
        	processBean.getResults(true);
         }
        
        JSPLogger.debug(this, "getProcessTemplates {}", processBean.getStatement());
        
        if(processBean.getSize() > 0)        
        	return processBean;
        
        processBean.close();
        
        return null;
    }
}