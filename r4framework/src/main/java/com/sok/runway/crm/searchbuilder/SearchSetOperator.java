package com.sok.runway.crm.searchbuilder;

public enum SearchSetOperator {
   UNION ("Union"),
   INTERSECT ("Intersect"),
   SUBTRACT ("Subtract");
   
   private final String operator;
   
   SearchSetOperator(String operator) {
      this.operator = operator;
   }
   
   public String toString() {
      return this.operator;
   }
   
   public static String[] valueStrings() {
      SearchSetOperator[] operators = SearchSetOperator.values();
      
      String[] valueStrings = new String[operators.length];
      for(int i=0; i < operators.length; i++) {
         valueStrings[i] = operators[i].toString();
      }
      
      return valueStrings;
   }
   
   public static SearchSetOperator createSearchOperator(String name) {
      SearchSetOperator operators = null;
      if(name != null) {
         if(name.equalsIgnoreCase(SearchSetOperator.UNION.toString())) {
            operators = SearchSetOperator.UNION;
         } else if(name.equalsIgnoreCase(SearchSetOperator.INTERSECT.toString())) {
            operators = SearchSetOperator.INTERSECT;
         } else if(name.equalsIgnoreCase(SearchSetOperator.SUBTRACT.toString())) {
            operators = SearchSetOperator.SUBTRACT;
         }
      }
      
      return operators;
   }
}
