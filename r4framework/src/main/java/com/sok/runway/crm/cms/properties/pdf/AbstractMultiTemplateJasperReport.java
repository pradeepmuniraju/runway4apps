package com.sok.runway.crm.cms.properties.pdf;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.LocalJasperReportsContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.cms.properties.pdf.templates.developer.DeveloperPdfGenerator;
import com.sok.runway.crm.interfaces.AsyncProcessStatus;

/**
 * @author Tuan Pham
 * @version 1.0
 * 
 * This class used to generate multiple-template PDF.
 * Extended class needs to override function prepareDataSources() to return HashMap<$reportPath, $List<DataSource>)
 * File path should be relative path. E.g: /files/jasper/HamlanHomes.jrxml
 * Please refer to PackagePdf.java in Hamlan project for more information.
 */
public abstract class AbstractMultiTemplateJasperReport  extends AbstractJasperReport implements AsyncProcessStatus {
	protected static final Logger logger = LoggerFactory.getLogger(AbstractMultiTemplateJasperReport.class);
	protected static final String TEMPLATE_PREFIX = "developer_";
	protected String templateURI;
	protected HashMap<String, JRDataSource> dataSources;
	protected HashMap<String, Object> parameters;
	
	/**
	 * This function will return specific template file by estate name in format "developer_${estateName}.jrxml"
	 * @return specificTemplatePath
	 */
	private String getDeveloperTemplate(HttpServletRequest request){
		
		String classAndMethodName = this.getClass() + " getDeveloperTemplate(request) ";
		logger.debug(classAndMethodName + " Started.");
		
		String templatePath = "";		
		String estateID = "";
		String estateName = "";
		Map<String, String> estates = null;		
		
		//Override default template by specific template file if estateMap found 
		if(request.getAttribute("estateMap") != null){
			estates = (HashMap<String, String>) request.getAttribute("estateMap");
			estateID = request.getParameter("EstateID");
			estateName = estates.get(estateID);
			if(!StringUtils.isEmpty(estateName)){
				templatePath = File.separatorChar + TEMPLATE_PREFIX + estateName + REPORT_EXTENSION;
			}
		}
		
		logger.debug(classAndMethodName + " Ended. templatePath is: " + templatePath);
		
		return templatePath;
	}
	
	
	/**
	 * This is the main function to generate report.
	 * Report data has to be prepared and store in JRDataSource type.
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public void generateReport(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String classAndMethodName = this.getClass() + " generateReport(request, response) ";
		logger.debug(classAndMethodName + " Started.");

		ServletConfig servletConfig = (ServletConfig)request.getAttribute("servletConfig");		
		String templatePath = ""; 				
	    JRDataSource dataSource = null;
		ServletOutputStream servletOutputStream = null;		
		ClassLoader originalLoader = null;
		List<JasperPrint> jasperPrints = new ArrayList<JasperPrint>();//Init list of JasperPrint to hold multiple template files
		
		logger.debug(classAndMethodName + " Calling setStatus(request, Prepare PDF date, 0.2d)");
		
		setStatus(request, "Prepare PDF data", 0.2d); //20% complete
		
		try{
			if(DeveloperPdfGenerator.loadedJars && isDeveloperPDF()){
				
				logger.debug(classAndMethodName + "Setting originalLoader and setContextClassLoader");

				originalLoader = Thread.currentThread().getContextClassLoader();
			    Thread.currentThread().setContextClassLoader(DeveloperPdfGenerator.classLoader);
			}
			try {	
				
				logger.debug(classAndMethodName + "Calling prepareDataSources(request, response)");
				
				//Prepare report data
				dataSources = prepareDataSources(request, response);	
				
				logger.debug(classAndMethodName + "Calling createReportParameters(request, response)");
				
				//Override if you want to provide report params 
				parameters = createReportParameters(request, response);
				
				logger.debug(classAndMethodName + "Calling setStatus(request, Compile PDF template, 0.5d)");
				
				setStatus(request, "Compile PDF template", 0.5d); //50% complete
					
				LocalJasperReportsContext ctx = iniJasperReportsContext(request);
		    	JasperFillManager fillmgr = JasperFillManager.getInstance(ctx);				
				
		    	logger.debug(classAndMethodName + "Before setting up jasperPrints");
		    	
				for (Map.Entry<String,JRDataSource> entry : dataSources.entrySet()) {
					dataSource = entry.getValue();
					templatePath = (isDeveloperPDF())? getDeveloperTemplate(request) :  entry.getKey();
				    templatePath = servletConfig.getServletContext().getRealPath(templatePath);
			    	JasperReport jasperReport = loadReport(templatePath);
			    	JasperPrint jasperPrint = fillmgr.fill(jasperReport, parameters, dataSource);
			    	jasperPrints.add(jasperPrint);//Add to JasperPrint list				    
				}
				
				logger.debug(classAndMethodName + "After setting up jasperPrints");
				
				logger.debug(classAndMethodName + "Setting setStatus(request, Filling PDF data, 0.8d)");
				
				setStatus(request, "Filling PDF data", 0.8d); //80% complete
				
				logger.debug(classAndMethodName + "Before Create a blank template to concatenate other template");
				
		    	//Create a blank template to concatenate other template
		    	String filename = servletConfig.getServletContext().getRealPath("specs/blank.jrxml");
		        JasperReport blankReport = loadReport(filename);
		        JasperPrint mainJasperPrint = fillmgr.fill(blankReport, parameters, new JRBeanCollectionDataSource(Collections.EMPTY_LIST));
		        for(JasperPrint print : jasperPrints){
		        	for (Object page : print.getPages()){
						mainJasperPrint.addPage((JRPrintPage)page);
					}
		        }
		        
		        logger.debug(classAndMethodName + "After Create a blank template to concatenate other template");
		        
		        logger.debug(classAndMethodName + "Before exportToPdfStream");
		    	JasperExportManager exmgr = JasperExportManager.getInstance(ctx);
		    	exmgr.exportToPdfStream(mainJasperPrint,  response.getOutputStream());
		    	logger.debug(classAndMethodName + "After exportToPdfStream");
		    	
		    	logger.debug(classAndMethodName + "Before postReportProcess(request, response, exmgr, mainJasperPrint)");
		    	postReportProcess(request, response, exmgr, mainJasperPrint);
		    	logger.debug(classAndMethodName + "After postReportProcess(request, response, exmgr, mainJasperPrint)");
		    	
		    } finally {
		    	
		    	logger.debug(classAndMethodName + "Inside Finally. Before calling setContextClassLoader(originalLoader)");
		    	
		    	if(DeveloperPdfGenerator.loadedJars && isDeveloperPDF())
		    		Thread.currentThread().setContextClassLoader(originalLoader);
		    	
		    	logger.debug(classAndMethodName + "Inside Finally. After calling setContextClassLoader(originalLoader)");
		    }
		}catch(JRException e){
			// display error 
			setStatus(request, "Error: - " + e.getMessage(), 0.8d);
			
			e.printStackTrace();
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
			logger.debug(classAndMethodName + "Inside Outer Finally Started. Closing Outputstreams");
			
			if(servletOutputStream != null){
				servletOutputStream.flush();
				servletOutputStream.close();
			}
			
			logger.debug(classAndMethodName + "Inside Outer Finally Ended");
		}
	}
	
	@Override
	protected  HashMap<String, Object> createReportParameters(HttpServletRequest request, HttpServletResponse response){
		
		String classAndMethodName = this.getClass() + " createReportParameters(request, response) ";
		logger.debug(classAndMethodName + " Started and will end adter calling createParameters(request, response).");

		return createParameters(request, response);
	}
	
	@Override
	protected JRDataSource prepareReportDataSource(HttpServletRequest request, HttpServletResponse response){
		
		String classAndMethodName = this.getClass() + " prepareReportDataSource(request, response) ";
		logger.debug(classAndMethodName + " Started. It is returning NULL.");

		return null;
    }
	
	/**
	 * To be overridden for default page event behavior
	 */
	protected abstract HashMap<String, JRDataSource> prepareDataSources(HttpServletRequest request, HttpServletResponse response);
	
	protected abstract HashMap<String, Object> createParameters(HttpServletRequest request, HttpServletResponse response);
	
	protected void postReportProcess(HttpServletRequest request, HttpServletResponse response, JasperExportManager exmgr, JasperPrint mainJasperPrint) throws JRException, IOException {
		// Implement this child class if you want to gain access to the generated PDF report
	}
}