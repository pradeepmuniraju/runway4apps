package com.sok.runway.crm.cms.properties.publishing;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;

public class PublishService {
	private static final Logger logger = LoggerFactory.getLogger(PublishService.class);

	public static boolean canPublishToWeb(HttpServletRequest request, String productID) {
		
		String isControlledPublish = InitServlet.getSystemParams().getProperty("ControlledPublish");
		
		if(! "true".equals(isControlledPublish)){
			logger.debug("Not a ControlledPublish. Publishing product - " + productID);
			return true;
		}
		
		GenRow productPublishing = new GenRow();
		
		productPublishing.parseRequest(request);
		productPublishing.setViewSpec("properties/ProductPublishingView");
		productPublishing.setParameter("ProductID", productID);
		productPublishing.setParameter("PublishMethod", PublishMethod.Website.getName());
		productPublishing.setParameter("StartDate", "<NOW");
		productPublishing.setParameter("EndDate", ">NOW+NULL");
		
		
		productPublishing.doAction("selectfirst");
		logger.debug("Productpublish statement : " + productPublishing.getStatement());
		
		logger.debug("Product available for publishing : " + productPublishing.isSuccessful());
		
		if (productPublishing.isSuccessful()) {
			return true;
		} else {
			return false;
		}
	}
}