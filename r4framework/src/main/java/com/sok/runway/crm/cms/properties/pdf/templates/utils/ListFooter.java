package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import org.apache.commons.lang.StringUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

public class ListFooter extends PdfPageEventHelper implements PDFConstants {
	public static String headText = "\nPlease see sales consultant for more \n packages available in this estate";

	String repName, repMobile, repPhone, repEmail, webSiteRequest;
	String locName, locStreet, locCity, displayHours, disclaimerText = null;

	public ListFooter(javax.servlet.http.HttpServletRequest request) {
		GenRow repDetails = PDFUtil.getRep(request.getParameter("RepUserID"));
		repName = repDetails.getString("FirstName") + " " + repDetails.getString("LastName");
		repMobile = repDetails.getString("Mobile");
		repPhone = repDetails.getString("Phone");
		repEmail = PDFUtil.getRepEmail(repDetails.getString("Email"));

		GenRow displayDetails = PDFUtil.getDisplayLocation(request.getParameter("DisplayID"));
		locName = displayDetails.getString("Name");
		locStreet = displayDetails.getString("Street");
		locCity = displayDetails.getString("City");
		displayHours = PDFUtil.getDisplayHours(displayDetails.getString("ProductID"));
	}

	/*public ListFooter(javax.servlet.http.HttpServletRequest request, String text) {
		this(request);
		headText = text;
	}*/
	
	public ListFooter(javax.servlet.http.HttpServletRequest request, String text) {
		this(request);
		headText = text;

		Object disc = request.getAttribute("-listFooterDisclaimer");
		if(disc != null && disc instanceof String) {
			disclaimerText = (String)disc;
		}
	}
	public ListFooter(javax.servlet.http.HttpServletRequest request, String text, String disc) {
		this(request);
		if(text != null)
			headText = text;
		if(disc != null) 
			disclaimerText = disc;
	}	

	public void onEndPage(PdfWriter writer, com.itextpdf.text.Document document) {
		PdfPTable fTable = new PdfPTable(3);
		try {
			fTable.setWidths(new int[] { 40, 45, 48 });
			fTable.setTotalWidth(566f);
			fTable.setLockedWidth(true);
			fTable.getDefaultCell().setBorder(0);
			fTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);

			// Footer Heading
			/*fTable.addCell(new Paragraph());
			fTable.getDefaultCell().setColspan(2);

			Paragraph p = new Paragraph(headText, fHeadFont);
			fTable.addCell(p);*/

			// Disclaimer
			String tmpDisclaimer = disclaimerText != null ? disclaimerText : PDFUtil.DISCLAIMER_TEXT;
			final String disclaimer = String.format(tmpDisclaimer, new java.util.Date());
			fTable.getDefaultCell().setColspan(1);
			fTable.getDefaultCell().setFixedHeight(100f);
			fTable.getDefaultCell().setLeading(8f, 0f);
			fTable.getDefaultCell().setPaddingRight(15f);
			Paragraph p = new Paragraph(disclaimer, fDisclaimerFont);
			fTable.addCell(p);

			// Contact
			fTable.addCell(getRepParagraph(repName, repMobile, repPhone, repEmail));
			// Location Details
			fTable.addCell(getLocationParagraph(locName, locStreet, locCity, displayHours));
			fTable.writeSelectedRows(0, -1, 35, 160, writer.getDirectContent());
		} catch (DocumentException de) {
			throw new ExceptionConverter(de);
		}
	}

	public static Paragraph getRepParagraph(String repName, String repMobile, String repPhone, String repEmail) {
		final Paragraph p = new Paragraph();
		p.add(Chunk.NEWLINE);
		p.add(new Chunk("CONTACT\n", fContactTitleFont));
		p.add(new Chunk("\n", smallFont));
		p.add(new Chunk(repName + "\n", fRepNameFont));
		p.add(new Chunk("\n", smallFont));
		if (StringUtils.isNotBlank(repMobile)) {
			p.add(new Chunk("M ", fContactFont));
			p.add(new Chunk(repMobile + "    ", fPhoneFont));
		}
		if (StringUtils.isNotBlank(repPhone)) {
			p.add(new Chunk("T ", fContactFont));
			p.add(new Chunk(repPhone, fPhoneFont));
		}
		p.add(Chunk.NEWLINE);
		p.add(new Chunk("\n", smallFont));
		p.add(new Chunk("E ", fContactFont));
		p.add(new Chunk(repEmail, fEmailFont));
		return p;
	}

	public static Paragraph getLocationParagraph(String name, String street, String city, String hours) {
		String address = "";

		if (StringUtils.isNotBlank(street) && StringUtils.isNotBlank(city))
			address = street + ", " + city;
		else
			address = PDFUtil.getEmptyStringIfNull(street) + PDFUtil.getEmptyStringIfNull(city);

		int startTime = 11, endTime = 5;
		try {
			if(name != null){
				name = name.replace(",", " at");
			}
			
			if (StringUtils.isNotBlank(hours)) {
				// DB Format is 11:00 am - 5:00 pm.
				// Display Format 11am-5pm
				String[] timings = hours.split("-");
				startTime = Integer.parseInt((timings[0].split(":")[0]).trim());
				endTime = Integer.parseInt((timings[1].split(":")[0]).trim());
			}
		} catch (Exception e) {
			System.out.println("Ignoring Error :" + e + " during parsing visit timings. Using default 11am - 5 pm");
			// DO nothing
		}
		hours = "Open 7 days a week from " + startTime + "am-" + endTime + "pm";

		
		GenRow officeAddress = new GenRow();
		officeAddress.setViewSpec("SetupView");
		officeAddress.setParameter("SetupID", "Default");
		///officeAddress.setParameter("SetupID", "0O1E2O5H1D865O2H5Z300B2Z6P6F");  // this is used for Riviera Homes
		officeAddress.doAction(GenerationKeys.SELECTFIRST);
		String websiteURL = officeAddress.getString("Web");
		officeAddress.close();
		
		
		final Paragraph p = new Paragraph();
		p.add(new Chunk("\n"));
		if (name != null)
			p.add(new Chunk("OR VISIT ", fVisitFont));
		p.add(new Chunk(PDFUtil.getEmptyStringIfNull(name) + "\n", fLocationFont));
		p.add(new Chunk("\n", s1));
		p.add(new Chunk(PDFUtil.capitalizeFirstLetters(PDFUtil.getEmptyStringIfNull(address)) + "\n", fLocationFont));
		p.add(new Chunk("\n", s1));
		//p.add(new Chunk(hours + "\n", fTimingsFont));
		p.add(new Chunk("\n", s1));
		p.add(new Chunk(websiteURL, fWebsiteFont));
		return p;
	}

	public static final Font smallFont = FontFactory.getFont(arialFont, fontRatio * 6, BaseColor.BLACK);
	public static final Font s1 = FontFactory.getFont(arialFont, fontRatio * 4, BaseColor.BLACK);

	public static final Font fHeadFont = FontFactory.getFont(arialFont, fontRatio * 23, BaseColor.BLACK);
	public static final Font fDisclaimerFont = FontFactory.getFont(arialFont, fontRatio * 10, BaseColor.BLACK);
	public static final Font fContactTitleFont = FontFactory.getFont(arialFont, fontRatio * 22, Font.BOLD, BaseColor.BLACK);
	public static final Font fRepNameFont = FontFactory.getFont(arialFont, fontRatio * 22, PDFUtil.aqua);
	public static final Font fPhoneFont = FontFactory.getFont(arialFont, fontRatio * 18, PDFUtil.aqua);
	public static final Font fEmailFont = FontFactory.getFont(arialFont, fontRatio * 16, PDFUtil.aqua);
	public static final Font fContactFont = FontFactory.getFont(arialFont, fontRatio * 14, BaseColor.BLACK);
	public static final Font fVisitFont = FontFactory.getFont(arialFont, fontRatio * 16, Font.BOLD, BaseColor.BLACK);
	public static final Font fLocationFont = FontFactory.getFont(arialFont, fontRatio * 16, BaseColor.BLACK);
	public static final Font fTimingsFont = FontFactory.getFont(arialFont, fontRatio * 15, Font.BOLD, BaseColor.BLACK);
	public static final Font fWebsiteFont = FontFactory.getFont(arialFont, fontRatio * 20, Font.BOLD, PDFUtil.aqua);
}