package com.sok.runway.crm.cms.properties;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

public class HomeRange extends PropertyEntity {

	final PropertyType propertyType = PropertyType.HomeRange;
	static final String VIEWSPEC = "properties/ProductHomeRangeView";

	public HomeRange(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public HomeRange(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	
	public HomeRange(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	public HomeRange load(String productID) {
		return (HomeRange)super.load(productID);
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
}