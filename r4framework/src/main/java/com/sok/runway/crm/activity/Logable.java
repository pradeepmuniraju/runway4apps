package com.sok.runway.crm.activity;

import com.sok.runway.security.*;

import java.sql.*;

public interface Logable {
   
   public static final String ACTIVITY_DELETE = "Deleted";
   public static final String ACTIVITY_INSERT = "Inserted";
   public static final String ACTIVITY_UPDATE = "Updated";
   
   
   public User getCurrentUser();
   
   public Connection getConnection();
   
   public String getEntityName();
   
   public String getPrimaryKey();
   
}