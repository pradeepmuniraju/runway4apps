package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import com.sok.runway.UserBean;
import com.sok.runway.crm.Product;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.PlanService;
import com.sok.service.crm.cms.properties.PlanService.Plan;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

public class Home  extends PropertyEntity {

	final PropertyType propertyType = PropertyType.Home;
	static final String VIEWSPEC = "properties/ProductHomeView";
	private HomeRange homeRange = null;
	private Brand homeBrand = null;
	
	private UserService us = UserService.getInstance();
	
	private PlanService planService = PlanService.getInstance();
	
	public Home(Connection con) { 
		setConnection(con);
		setViewSpec("properties/ProductHomeView");
		setParameter(PRODUCTTYPE,getProductType());
	}
	public Home(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	
	public Home(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	@Override
	public Home load(String productID) {
		return (Home)super.load(productID);
	}

	public List<HomePlan> getHomePlans() {
		return Collections.emptyList();
	}
	
	public List<HomeOption> getHomeOptions() { 
		return Collections.emptyList();
	}
	
	protected double getHomeDetail(HttpServletRequest request,
			String productID, String type, String field) {
		GenRow row = new GenRow();
		row.setRequest(request);
		row.setTableSpec("properties/ProductDetails");
		row.setParameter("ProductID", productID);
		row.setParameter("DetailsType", type);
		row.setParameter("ProductType", "HomePlan");
		row.setParameter("-select1", field);
		if (row.getString("ProductID").length() > 0) row.doAction("selectfirst");

		if (row.isSuccessful()) {
			try {
				return row.getDouble(field);
			} catch (Exception e)  {
				e.printStackTrace();
			}
		}
		return 0.0d;
	}
	
	
	/**
	 * If you use this you must close the facades! 
	 * @return
	 */
	public Facade getFacades(boolean showInactive) {
		Facade facade = new Facade(getConnection());
		if(isSuccessful() && isSet(PRODUCTID)) {
			facade.setParameter(PRODUCTID, getData(PRODUCTID));
			if(!showInactive) {
				facade.setParameter("Active", "Y");
				facade.setParameter("ProductProductsLinkedProduct.Active", "Y");
			}
			facade.doAction(GenerationKeys.SEARCH);
			facade.getResults(true);
		}
		return facade;
	}
	public Facade getFacades() {
		return getFacades(false);
	}
	
	/**
	 *
	 * @return
	 */
	public List<com.sok.runway.externalInterface.beans.cms.properties.Plan> getPlans(boolean showInactive) {
		return planService.getHomePlans(getConnection(), this.getData(PRODUCTID), showInactive);
		/*
		Plan plans = new Plan(getConnection());
		if(isSuccessful() && isSet(PRODUCTID)) {
			plans.setParameter(PRODUCTID, getData(PRODUCTID));
			if(!showInactive) {
				plans.setParameter("Active", "Y");
				//plans.setParameter("ProductProductsLinkedProduct.Active", "Y");
			}
			plans.doAction(GenerationKeys.SEARCH);
			plans.getResults(true);
		}
		return facade;
		*/
	}

	/**
	 *
	 * @return
	 */
	public List<com.sok.runway.externalInterface.beans.cms.properties.PlanCheck> getPlansCheck(boolean showInactive) {
		return planService.getHomePlansCheck(getConnection(), this.getData(PRODUCTID), showInactive);
		/*
		Plan plans = new Plan(getConnection());
		if(isSuccessful() && isSet(PRODUCTID)) {
			plans.setParameter(PRODUCTID, getData(PRODUCTID));
			if(!showInactive) {
				plans.setParameter("Active", "Y");
				//plans.setParameter("ProductProductsLinkedProduct.Active", "Y");
			}
			plans.doAction(GenerationKeys.SEARCH);
			plans.getResults(true);
		}
		return facade;
		*/
	}
	public List<com.sok.runway.externalInterface.beans.cms.properties.Plan> getPlans() {
		return getPlans(false);
	}
	
	@Override
	public Object get(String key) { 
		if("rangeproduct".equals(key)) return getHomeRange();
		return super.get(key);
	}
	
	public HomeRange getHomeRange() {
		if(homeRange == null && isSuccessful()) { 
			homeRange = new HomeRange(getConnection()).load(getData("RangeProductID"));
			if(!homeRange.isSuccessful()) {
				homeRange = null;
			}
		}
		return homeRange;
	}
	
	public Brand getHomeBrand() {
		if(homeBrand == null && isSuccessful()) { 
			homeBrand = new Brand(getConnection()).load(getData("BrandProductID"));
			if(!homeBrand.isSuccessful()) {
				homeBrand = null;
			}
		}
		return homeBrand;
	}
	
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
}
