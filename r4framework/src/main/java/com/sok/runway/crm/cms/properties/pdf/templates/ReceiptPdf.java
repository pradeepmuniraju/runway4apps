package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.text.NumberFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.runway.crm.cms.properties.HomePlan;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;

public class ReceiptPdf extends AbstractPDFTemplate implements PDFConstants {
	ReceiptInfo info = new ReceiptInfo();

	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {
		ReceiptInfo info = getReceiptInfo(request);

		PdfPTable pageTable = new PdfPTable(1);
		pageTable.setWidths(new float[] { (345f / 909f) });
		pageTable.setWidthPercentage(100f);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.getDefaultCell().setBorder(0);

		pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		pageTable.getDefaultCell().setFixedHeight(100f);
		pageTable.getDefaultCell().setPaddingTop(50);
		pageTable.addCell(PDFUtil.createImage(request.getSession().getServletContext().getInitParameter("URLHome") + "/" + info.rangeLogo, 30, 10));

		pageTable.getDefaultCell().setFixedHeight(0f);
		pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		pageTable.addCell(new Paragraph("Customer Receipt", headerFont));

		pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		pageTable.addCell(getReceiptTable(info));

		// pageTable.getDefaultCell().setPaddingTop(248);
		// pageTable.addCell(getFooterTable(info));

		document.add(pageTable);
	}

	private PdfPTable getReceiptTable(ReceiptInfo info) throws DocumentException {
		PdfPTable receiptTable = new PdfPTable(3);
		receiptTable.setWidths(new int[] { 4, 10, 7 });
		receiptTable.setWidthPercentage(100f);
		receiptTable.getDefaultCell().setPadding(0);
		receiptTable.getDefaultCell().setPaddingTop(3);
		receiptTable.getDefaultCell().setBorder(0);

		// Received from details
		receiptTable.addCell(new Paragraph("Received from: ", detailsFontBold));
		receiptTable.addCell(new Paragraph(info.fromName, detailsFont));

		Paragraph p = new Paragraph();
		p.add(new Chunk("Date:  ", detailsFontBold));
		p.add(new Chunk(info.receiptDate, detailsFont));
		receiptTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		receiptTable.addCell(p);

		receiptTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		receiptTable.addCell(BLANK);
		receiptTable.addCell(new Paragraph(info.fromAddress1, detailsFont));
		receiptTable.addCell(BLANK);

		receiptTable.addCell(BLANK);
		receiptTable.addCell(new Paragraph(info.fromAddress2, detailsFont));
		receiptTable.addCell(BLANK);

		receiptTable.getDefaultCell().setPaddingTop(20);
		receiptTable.addCell(new Paragraph("Job Number:", detailsFontBold));
		receiptTable.addCell(new Paragraph(info.jobNumber, detailsFont));
		receiptTable.addCell(BLANK);

		receiptTable.addCell(new Paragraph("Building at:", detailsFontBold));
		receiptTable.addCell(new Paragraph(info.buildAddress1, detailsFont));
		receiptTable.addCell(BLANK);

		receiptTable.getDefaultCell().setPaddingTop(3);
		receiptTable.addCell(BLANK);
		receiptTable.addCell(new Paragraph(info.buildAddress2, detailsFont));
		receiptTable.addCell(BLANK);

		receiptTable.getDefaultCell().setPaddingTop(20);
		receiptTable.addCell(new Paragraph("Stage:", detailsFontBold));
		receiptTable.addCell(new Paragraph(info.stage, detailsFont));
		receiptTable.addCell(BLANK);

		receiptTable.getDefaultCell().setPaddingTop(20);
		receiptTable.addCell(new Paragraph("Amount:", detailsFontBold));
		receiptTable.addCell(new Paragraph(info.amount, detailsFont));
		receiptTable.addCell(BLANK);

		receiptTable.getDefaultCell().setPaddingTop(20);
		receiptTable.addCell(new Paragraph("Received by:", detailsFontBold));
		p = new Paragraph();
		p.add(new Chunk("_________________________  ", detailsFont));
		p.add(new Chunk("  Print Name:  ", detailsFontBold));
		p.add(new Chunk(info.printName, detailsFont));
		receiptTable.getDefaultCell().setColspan(2);
		receiptTable.addCell(p);

		receiptTable.getDefaultCell().setPaddingTop(1);
		receiptTable.getDefaultCell().setColspan(1);
		receiptTable.addCell(BLANK);
		receiptTable.getDefaultCell().setColspan(1);
		receiptTable.getDefaultCell().setPaddingLeft(45);
		receiptTable.addCell(new Paragraph("(Signature)", smallFont));
		receiptTable.addCell(BLANK);
		
		receiptTable.getDefaultCell().setPadding(0);
		receiptTable.getDefaultCell().setPaddingTop(35);		
		receiptTable.getDefaultCell().setColspan(3);
		
		String receiptDisclaimerText = InitServlet.getSystemParams().getProperty("ReceiptDisclaimerText");
		if(StringUtils.isNotBlank(receiptDisclaimerText)){
			receiptTable.addCell(new Paragraph(receiptDisclaimerText, mediumFont));
		}else{
		receiptTable.addCell(new Paragraph("* Please note that for payments made by direct deposit, cheque or credit card, this receipt is not valid until funds clear into the "
				+ StringUtils.defaultString(InitServlet.getSystemParam("ClientNameForDeveloperPdfs")) + " bank account", mediumFont));
		}
		return receiptTable;
	}

	private PdfPTable getFooterTable(ReceiptInfo info) throws DocumentException {
		PdfPTable footerTable = new PdfPTable(1);
		footerTable.setWidths(new int[] { 200 });
		footerTable.setTotalWidth(200f);
		footerTable.setWidthPercentage(200f);
		footerTable.getDefaultCell().setPadding(0);
		footerTable.getDefaultCell().setPaddingTop(3);
		footerTable.getDefaultCell().setBorder(0);

		// Received from details
		footerTable.addCell(new Paragraph(info.regionName, detailsFontBold));
		footerTable.getDefaultCell().setPaddingTop(3);
		footerTable.addCell(new Paragraph(info.companyName, detailsFontBold));
		footerTable.addCell(new Paragraph(info.abn, detailsFont));
		footerTable.getDefaultCell().setPaddingTop(10);
		footerTable.addCell(new Paragraph(info.companyAddress, detailsFont));

		Paragraph p = new Paragraph();
		p.add(new Chunk(info.phone, detailsFont));
		p.add(new Chunk(info.fax, detailsFont));
		footerTable.getDefaultCell().setPaddingTop(10);
		footerTable.addCell(p);

		return footerTable;
	}

	private ReceiptInfo getReceiptInfo(HttpServletRequest request) {
		String amountQid = request.getParameter("AmountQID");
		GenRow depositDetails = new GenRow();
		depositDetails.setViewSpec("OpportunityView");
		depositDetails.parseRequest(request);

		depositDetails.setParameter("-join0", "OpportunityOrders");
		depositDetails.setParameter("-join1", "OpportunityOrders.OrderContactLocations.ContactLocationAddress");
		depositDetails.setParameter("-select0", "OpportunityProcessStage.ProcessStageStage.Name as Status");
		depositDetails.setParameter("-select1", "OpportunityOrders.OrderNum as OrderNum");
		depositDetails.setParameter("-select2", "OpportunityOrders.OrderContactLocations.ContactLocationAddress.StreetNumber as BuildStreetNumber");
		depositDetails.setParameter("-select3", "OpportunityOrders.OrderContactLocations.ContactLocationAddress.Street as BuildStreet");
		depositDetails.setParameter("-select4", "OpportunityOrders.OrderContactLocations.ContactLocationAddress.City as BuildCity");
		depositDetails.setParameter("-select5", "OpportunityOrders.OrderContactLocations.ContactLocationAddress.Postcode as BuildPostcode");
		depositDetails.setParameter("-select6", "OpportunityOrders.OrderContactLocations.ContactLocationAddress.State as BuildState");
		depositDetails.setParameter("-select7", "OpportunityOrders.ProductID as OrderProductID");
		depositDetails.setParameter("-select8", "OpportunityReferenceProduct.ProductAddress.StreetNumber as RefBuildStreetNumber");
		depositDetails.setParameter("-select9", "OpportunityReferenceProduct.ProductAddress.Street as RefBuildStreet");
		depositDetails.setParameter("-select10", "OpportunityReferenceProduct.ProductAddress..City as RefBuildCity");
		depositDetails.setParameter("-select11", "OpportunityReferenceProduct.ProductAddress..Postcode as RefBuildPostcode");

		depositDetails.doAction("selectfirst");
		System.out.println(depositDetails.getStatement());

		depositDetails.getResults();
		if (depositDetails.isSuccessful()) {
			info.fromName = depositDetails.getString("Title") + " " + depositDetails.getString("FirstName") + " " + depositDetails.getString("LastName");
			info.fromAddress1 = depositDetails.getString("ContactStreet") + " " + depositDetails.getString("ContactStreet2");
			info.fromAddress2 = depositDetails.getString("ContactCity") + " " + depositDetails.getString("ContactState") + " " + depositDetails.getString("ContactPostcode");
			info.receiptDate = getProcessAnswer(depositDetails.getString("OpportunityID"), findQuestion("Payment Date"));
			info.jobNumber = depositDetails.getString("OrderNum");
			info.stage = depositDetails.getString("Status");
			info.printName = new StringBuilder(depositDetails.getString("RepFirstName")).append(" ").append(depositDetails.getString("RepLastName")).toString();
			
			info.buildAddress1 = (depositDetails.getString("BuildStreetNumber") + " " + depositDetails.getString("BuildStreet")).trim();
			info.buildAddress2 = (depositDetails.getString("BuildCity") + " " + depositDetails.getString("BuildState") + " " + depositDetails.getString("BuildPostcode")).trim();

			if (info.buildAddress1.length() == 0) {
				info.buildAddress1 = (depositDetails.getString("RefBuildStreetNumber") + " " + depositDetails.getString("RefBuildStreet")).trim();
				info.buildAddress2 = (depositDetails.getString("RefBuildCity") + " " + depositDetails.getString("RefBuildState") + " " + depositDetails.getString("RefBuildPostcode")).trim();
			}


			info.amount = getProcessAnswer(depositDetails.getString("OpportunityID"), findQuestion("PNetAmount"));
			if (StringUtils.isNotBlank(info.amount))
				info.amount = currency.format(Double.parseDouble(info.amount));

			PropertyEntity product = PropertyFactory.getPropertyEntity(request, depositDetails.isSet("OrderProductID")? depositDetails.getString("OrderProductID") : depositDetails.getString("ReferenceProductID"));

			if (product.isPropertyType(PropertyType.HouseLandPackage)) {
				info.rangeLogo = ((HouseLandPackage) product).getRange().getString("ThumbnailImagePath");
			} else if (product.isPropertyType(PropertyType.HomePlan)) {
				info.rangeLogo = ((HomePlan) product).getHomeRange().getString("ThumbnailImagePath");
			}
		}
		return info;
	}

	private class ReceiptInfo {
		String rangeLogo;
		String fromName, fromAddress1, fromAddress2;
		String receiptDate, jobNumber, buildAddress1, buildAddress2, stage;
		String amount, printName;
		String regionName, companyName, abn, companyAddress, phone, fax;
	}

	public Boolean isVisible() {
		return false;
	}

	public class ReceiptFooter extends PdfPageEventHelper implements PDFConstants {

		public ReceiptFooter(HttpServletRequest request) {
			String orderID = getOrderID(request.getParameter("OpportunityID"));
			String regionProductNum = getOrderAnswer(orderID, findQuestion("Build Region"));

			String regionID = "";

			GenRow setUpDetails = new GenRow();

			GenRow products = new GenRow();
			products.setTableSpec("DisplayEntities");
			products.setRequest(request);
			products.setParameter("DisplayEntityProduct.ProductNum", regionProductNum);
			products.setParameter("-join0", "DisplayEntityProduct");
			products.setParameter("-select0", "DisplayEntityID");
			if (StringUtils.isNotBlank(regionProductNum)) {
				products.doAction("selectfirst");
				//products.getResults();
				if (products.isSuccessful()) {
					regionID = products.getString("DisplayEntityID");
				}
	
				// Footer information
				setUpDetails.setViewSpec("SetupView");
				setUpDetails.setRequest(request);
				setUpDetails.setParameter("DisplayEntityID", regionID);
				setUpDetails.setParameter("-join0", "SetupDisplayEntities");
				setUpDetails.setParameter("-select0", "SetupDisplayEntities.Name as Name");
	
				setUpDetails.doAction("selectfirst");
				System.out.println(setUpDetails.getStatement());
			}
			//setUpDetails.getResults();
			if (setUpDetails.isSuccessful()) {
				info.regionName = setUpDetails.getString("Name");
				info.companyName = setUpDetails.getString("Footer");
				info.abn = new StringBuilder("ABN ").append(setUpDetails.getString("ABN")).toString();
				info.companyAddress = PDFUtil.concat(new String[] { setUpDetails.getString("Street"), setUpDetails.getString("Street2"), setUpDetails.getString("City"), setUpDetails.getString("State"), setUpDetails.getString("Postcode") });
				info.phone = new StringBuilder("T     ").append(setUpDetails.getString("Phone")).toString();
				info.fax = new StringBuilder("       F     ").append(setUpDetails.getString("Fax")).toString();
			}

		}

		public void onEndPage(PdfWriter writer, com.itextpdf.text.Document document) {
			try {
				PdfPTable footerTable = getFooterTable(info);
				footerTable.writeSelectedRows(0, -1, 40, 100, writer.getDirectContent());
			} catch (Exception e) {
				System.out.println("Exception Occurred while prcocessing footer. Message: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		return new ReceiptFooter(request);
	}

	private String getOrderID(String opportunityID) {
		GenRow row = new GenRow();
		row.setTableSpec("Opportunities");
		row.setParameter("OpportunityID", opportunityID);
		row.setParameter("-select0", "DefaultOrderID");
		if (opportunityID != null)
			row.doAction("selectfirst");

		if (row.isSuccessful())
			return row.getString("DefaultOrderID");
		return null; // used in getProcessAnswer / getOrderAnswer
	}

	private String findQuestion(String question) {
		GenRow row = new GenRow();
		row.setTableSpec("Questions");
		row.setParameter("-select0", "QuestionID");
		row.setParameter("ShortLabel", question);
		row.doAction("selectfirst");

		if (row.isSuccessful())
			return row.getString("QuestionID");
		return null; // used in getProcessAnswer / getOrderAnswer
	}

	private String getOrderAnswer(String orderID, String questionID) {
		GenRow row = new GenRow();
		row.setTableSpec("answers/OrderAnswers");
		row.setParameter("-select0", "Answer");
		row.setParameter("QuestionID", questionID);
		row.setParameter("OrderID", orderID);
		if (orderID != null && questionID != null)
			row.doAction("selectfirst");

		return row.getString("Answer");
	}

	private String getProcessAnswer(String opportunityID, String questionID) {
		GenRow row = new GenRow();
		row.setTableSpec("answers/OpportunityAnswers");
		row.setParameter("-select0", "Answer");
		row.setParameter("QuestionID", questionID);
		row.setParameter("OpportunityID", opportunityID);
		if (opportunityID != null && questionID != null)
			row.doAction("selectfirst");

		return row.getString("Answer");
	}

	public static final Font headerFont = FontFactory.getFont(arialFont, fontRatio * 22, Font.BOLD, BaseColor.BLACK);
	public static final Font detailsFont = FontFactory.getFont(arialFont, fontRatio * 15, BaseColor.BLACK);
	public static final Font detailsFontBold = FontFactory.getFont(arialFont, fontRatio * 14, Font.BOLD, BaseColor.BLACK);
	public static final Font smallFont = FontFactory.getFont(arialFont, fontRatio * 12, BaseColor.BLACK);
	public static final Font mediumFont = FontFactory.getFont(arialFont, fontRatio * 13, BaseColor.BLACK);

	private final NumberFormat currency = NumberFormat.getCurrencyInstance();
}