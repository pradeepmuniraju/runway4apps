package com.sok.runway.crm.cms.properties.entities;

import java.sql.Connection;

import javax.servlet.ServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.Product;

public class RealEstateEntity extends Product {
	private static final Logger logger = LoggerFactory.getLogger(RealEstateEntity.class);
	
	static final String VIEWSPEC = "properties/ProductExistingPropertyView";
	public static final String ENTITY_NAME = "Existing Property"; // future change to "Real Estate"
	
	public RealEstateEntity() {
		super(VIEWSPEC, (Connection)null);
	}
	public RealEstateEntity(Connection con) {
		super(VIEWSPEC, con);
	}
	public RealEstateEntity(ServletRequest request) {
		super(VIEWSPEC, request);
	}
	public RealEstateEntity(Connection con, String realEstateID) {
		super(VIEWSPEC, con);
		loadFinal(realEstateID);
	}
	@Override
	public String getEntityName() {
		return ENTITY_NAME;
	}
	
	
	
	
	@Override
	public void postSave() {
		super.postSave();
		
		GenRow address = new GenRow();
		address.setTableSpec("Addresses");
		address.setParameter("ProductID", getPrimaryKey());
		address.setParameter("-select1","AddressID");
		
		address.doAction(GenerationKeys.SELECTFIRST);
		if(address.isSuccessful()) {
			address.setAction(GenerationKeys.UPDATE);
		} else {
			address.setToNewID("AddressID");
			address.setAction(GenerationKeys.INSERT);
		}
		if(setUpdatedFields(address)) { 
			address.doAction();
		}
	}
	
	private boolean setUpdatedFields(GenRow sub) {
		boolean has = false;
	
		int max = sub.getTableSpec().getFieldsLength() - 1;
		for(int i=0; ; i++) {
			String f = sub.getTableSpec().getFieldName(i);
			if(hasParameter(f) && !"GroupID".equals(f)) { //ProductDetails nor Addresses should have GroupID 
				sub.setParameter(f, getField(f));;
				has = true;
			}
			if(i == max)
				break;
		}
		return has; 
	}
}
