package com.sok.runway.crm.cms.properties;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.TableSpec;

public class DripUpdateWorker extends Thread {
	private String				productID = "";
	private String 				userID = "";
	public int					max = 0;
	public int					current = 0;
	public boolean				isCompleat = false;
	public double				estimatedRemaining = 0;
	private Date				date = new Date();
	
	private Map<String, GenRow> productMap = new HashMap<String, GenRow>();
	private Map<String, ArrayList<Map<String,String>>>  costingsMap = new HashMap<String, ArrayList<Map<String,String>>>();
	private Map<String, ArrayList<Map<String,String>>>  linkMap = new HashMap<String, ArrayList<Map<String,String>>>();
	private Map<String, Map<String,String>>  documentMap = new HashMap<String, Map<String,String>>();
	private Map<String, String>  costingsLinks = new HashMap<String, String>();
	
	private HashSet<String>		done = new HashSet<String>(); 					
	
	public DripUpdateWorker(String productID, String userID) {
		this.productID = productID;
		this.userID = userID;
		
		productMap = new HashMap<String, GenRow>();
		costingsMap = new HashMap<String, ArrayList<Map<String,String>>>();
		linkMap = new HashMap<String, ArrayList<Map<String,String>>>();
		documentMap = new HashMap<String, Map<String,String>>();
		costingsLinks = new HashMap<String, String>();

		if (productID != null && productID.length() > 0) this.start();
	}
	
	public void run() {
	   this.setName("Update Task");
	   try {
		   applyToAll("CopiedFromProductID", productID, userID);
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
	   try {
		   applyToAll("PackageCostProductID", productID, userID);
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
	   current = max;
	   isCompleat = true;

	}
	protected void applyToAll(String type, String productID, String userid) {
		if (StringUtils.isBlank(type) || StringUtils.isBlank(productID)) return;
		GenRow pc = new GenRow();
		pc.setViewSpec("ProductProductView");
		GenRow pcp = new GenRow();
		pcp.setTableSpec("Products");
		pcp.setParameter("-select0", "ProductID");
		pcp.setParameter("-select1", "ProductType");
		pcp.setParameter(type,productID);
		pcp.doAction("search");
		
		pcp.getResults(true);
		
		GenRow pcc = new GenRow();
		pcc.setTableSpec("PackageCostings");
		
		GenRow dcc = new GenRow();
		dcc.setTableSpec("LinkedDocuments");
	
		StringBuilder sb = new StringBuilder();

		while (pcp.getNext()) {
			if (pcp.getString("ProductID").equals(productID)) continue;
			if (!"Drip".endsWith(pcp.getString("ProductType"))) continue;
			if (done.contains(pcp.getString("ProductID"))) continue;
			if (sb.length() > 0) sb.append("+");
			sb.append(pcp.getString("ProductID"));
			done.add(pcp.getString("ProductID"));
		}
		
		pcp.close();
		
		if (sb.length() == 0) return;
		
		pc.setParameter("ProductProductsProduct.ProductType","!House and Land");
		pc.setParameter("LinkedProductID", sb.toString());
		pc.doAction("search");
		
		pc.getResults(true);
		
		max += pc.getSize();
			
		while (pc.getNext()) {
			update("Products", "ProductID", productID, pc.getData("LinkedProductID"),userid);
			
			pcc.clear();
			pcc.setParameter("-select","*");
			pcc.setParameter("ParentProductID",pc.getData("LinkedProductID"));
			pcc.doAction("selectfirst");
			//System.out.println("PCC " + productID + "  " + pcc.getStatement());
			if (pcc.isSuccessful() || pcc.getString("CostingID").length() > 0 && !pc.getData("LinkedProductID").equals(productID)) {
				pcc.clear();
				pcc.setParameter("ON-ParentProductID",pc.getData("LinkedProductID"));
				pcc.doAction("deleteall");

				try {
					pcc.clear();
					pcc.setParameter("ON-ParentProductID",pc.getData("LinkedProductID"));
					if (pc.getData("LinkedProductID").length() > 0)  pcc.doAction("deleteall");
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {
					dcc.clear();
					dcc.setParameter("ON-ProductID",pc.getData("LinkedProductID"));
					if (pc.getData("LinkedProductID").length() > 0) dcc.doAction("deleteall");
				} catch (Exception e) {
					e.printStackTrace();
				}

				copyDripCostingsCached(productID,  pc.getData("LinkedProductID"), userid);
				copyDripContentCached(productID,  pc.getData("LinkedProductID"), userid);
			}

			++current;
			Date d = new Date();
			estimatedRemaining = d.getTime() - date.getTime();
			estimatedRemaining = ((double) estimatedRemaining / current) * (max - current) ;

			applyToAll(type, pc.getData("LinkedProductID"), userid);
		}
		
		pc.close();
	}
	
	public void update(String tablespecname, String productidfieldname, String fromproductid, String toproductid, String userid) {
		
		if (fromproductid == null || fromproductid.length() == 0 || toproductid == null || toproductid.length() == 0) return;

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);

		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow check = new GenRow();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			check.clear();
			check.setTableSpec(tablespecname);
			check.setParameter("-select1", "*");
			check.setParameter(productidfieldname, toproductid);
			check.doAction("selectfirst");

			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.putAllDataAsParameters(row, true);
			if ("Products".equals(tablespecname)) {
				clone.setParameter("ProductType","Drip");
			}
			// make sure it is not already there
			if (!check.isSuccessful() || check.getString(row.getTableSpec().getPrimaryKeyName()).length() == 0) {
				// if it is a Package Cost then clone that and use the clone
				clone.setParameter("CopiedFromProductID", fromproductid);
				clone.setParameter("PackageCostProductID", fromproductid);
				clone.setParameter(productidfieldname, toproductid);
				clone.createNewID();
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				try {
					clone.doAction("insert");
				} catch (Exception e) {
					System.out.println("Insert Failed " + fromproductid);
				}
			} else {
				clone.remove("CopiedFromProductID");
				clone.remove("PackageCostProductID");
				clone.setParameter(productidfieldname, toproductid);
				clone.setParameter(row.getTableSpec().getPrimaryKeyName(),
					check.getString(row.getTableSpec().getPrimaryKeyName()));
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("ModifiedBy", userid);
				try {
					clone.doAction("update");
				} catch (Exception e) {
					System.out.println("Update Failed " + check.getString(row.getTableSpec().getPrimaryKeyName()));
				}
			}
		}
		row.close();
	}
	
	public String copyDripCostingsCached(String fromproductid, String toproductid, String userid) {
		ArrayList<Map<String,String>> costing = costingsMap.get(fromproductid);
		if (costing != null) {
			HashMap<String,String> keys = new HashMap<String,String>();
			for (Map<String,String> keyValue : costing) {
				if (keyValue != null && keyValue.size() > 0) {
					keys.put(keyValue.get("CostingID"), KeyMaker.generate());
				}
			}

			GenRow clone = new GenRow();
			clone.setTableSpec("PackageCostings");
			for (Map<String,String> keyValue : costing) {
				if (keyValue != null && keyValue.size() > 0) {
					clone.putAll(keyValue);
					clone.setParameter("CreatedDate", "NOW");
					clone.setParameter("ModifiedDate", "NOW");
					clone.setParameter("CreatedBy", userid);
					clone.setParameter("ModifiedBy", userid);
					clone.setParameter("ParentProductID", toproductid);
					clone.setParameter("CostingID", keys.get(keyValue.get("CostingID")));
					if (keyValue.get("ParentID") != null && keyValue.get("ParentID").length() > 0) clone.setParameter("ParentID", keys.get(keyValue.get("ParentID")));
					try {
						clone.doAction("insert");
					} catch (Exception e) {
						clone.clearStatement();
						clone.setParameter("ParentProductID", toproductid);
						clone.doAction("update");
//						logger.error("error in {} of {}","copyDripCostings",e.getMessage());
					}
				}
			}
		} else {
			HashMap<String,String> keys = new HashMap<String,String>();
			costing = new ArrayList<Map<String,String>>();
			GenRow row = new GenRow();
			row.setTableSpec("PackageCostings");

			TableSpec ts = row.getTableSpec();

			row.setParameter("-select1", "*");
			row.setParameter("ParentProductID", fromproductid);
			row.doAction(ActionBean.SEARCH);
			row.getResults();

			while (row.getNext()) {
				keys.put(row.getString("CostingID"), KeyMaker.generate());
				costing.add(row.getDataMap());
			}
			costingsMap.put(fromproductid, costing);
			
			row.close();
			
			GenRow clone = new GenRow();
			clone.setTableSpec("PackageCostings");
			for (Map<String,String> keyValue : costing) {
				if (keyValue != null && keyValue.size() > 0) {
					clone.putAll(keyValue);
					clone.setParameter("CreatedDate", "NOW");
					clone.setParameter("ModifiedDate", "NOW");
					clone.setParameter("CreatedBy", userid);
					clone.setParameter("ModifiedBy", userid);
					clone.setParameter("ParentProductID", toproductid);
					clone.setParameter("CostingID", keys.get(keyValue.get("CostingID")));
					if (keyValue.get("ParentID") != null && keyValue.get("ParentID").length() > 0) clone.setParameter("ParentID", keys.get(keyValue.get("ParentID")));
					try {
						clone.doAction("insert");
					} catch (Exception e) {
						clone.clearStatement();
						clone.setParameter("ParentProductID", toproductid);
						clone.doAction("update");
//						logger.error("error in {} of {}","copyDripCostings",e.getMessage());
					}
				}
			}
		}

		return "";
	}

	public void copyDripContentCached(String fromProductID, String toProductID, String userid) {
		ArrayList<Map<String,String>> links = linkMap.get(fromProductID);
		if (links != null) {
			GenRow clone = new GenRow();
			clone.setTableSpec("LinkedDocuments");

			GenRow bean = new GenRow();
			bean.setViewSpec("DocumentView");

			
			for (Map<String,String> keyValue : links) {
				if (keyValue != null && keyValue.size() > 0) {
					Map<String,String> docs = documentMap.get(keyValue.get("DocumentID"));
							
					String newDocumentID = KeyMaker.generate();
	
					bean.clear();
					bean.putAll(docs);
					bean.setColumn("DocumentID",newDocumentID);		
					// for some stupid reason we are getting , in intagers
					bean.setParameter("FileSize",bean.getString("FileSize").replaceAll("[^0-9]",""));
					bean.setAction("insert");
					bean.doAction();
	
					clone.putAll(keyValue);
					clone.setParameter("CreatedDate", "NOW");
					clone.setParameter("ModifiedDate", "NOW");
					clone.setParameter("CreatedBy", userid);
					clone.setParameter("ModifiedBy", userid);
					clone.setColumn("LinkedDocumentID",KeyMaker.generate());
					clone.setColumn("CreatedDate","NOW");
					clone.setColumn("CreatedBy",userid);
					clone.setColumn("DocumentID",newDocumentID);
					clone.setColumn("ContentID",KeyMaker.generate());
					clone.setColumn("ProductID",toProductID);
					try {
						clone.doAction("insert");
					} catch (Exception e) {
						clone.clearStatement();
						clone.setParameter("ParentProductID", toProductID);
						clone.doAction("update");
//						logger.error("error in {} of {}","copyDripCostings",e.getMessage());
					}
				}
			}
		} else {
			links = new ArrayList<Map<String,String>>();
			GenRow row = new GenRow();
			row.setTableSpec("LinkedDocuments");

			row.setParameter("-select1", "*");
			row.setParameter("ProductID", fromProductID);
			row.doAction(ActionBean.SEARCH);
			row.getResults();

			while (row.getNext()) {
				links.add(row.getDataMap());
			}
			linkMap.put(fromProductID, links);
			
			row.close();
			
			GenRow clone = new GenRow();
			clone.setTableSpec("LinkedDocuments");

			GenRow bean = new GenRow();
			bean.setTableSpec("Documents");

			GenRow insert = new GenRow();
			insert.setTableSpec("Documents");

			
			for (Map<String,String> keyValue : links) {
				if (keyValue != null && keyValue.size() > 0) {
					Map<String,String> docs = documentMap.get(keyValue.get("DocumentID"));
					if (docs == null || docs.size() == 0) {
						bean.clear();
						bean.setTableSpec("Documents");
						bean.setParameter("DocumentID",keyValue.get("DocumentID"));
						bean.setParameter("-select0","*");
						bean.setParameter("-top","1");
						bean.doAction("search");
						
						bean.getResults();
						
						bean.getNext();
						
						docs = bean.getDataMap();
						
						documentMap.put(keyValue.get("DocumentID"), docs);
						
					}
					if (docs != null && docs.size() > 0) {
						String newDocumentID = KeyMaker.generate();
		
						insert.clear();
						insert.putAll(docs);
						insert.setColumn("DocumentID",newDocumentID);
						// for some stupid reason we are getting , in intagers
						insert.setParameter("FileSize",insert.getString("FileSize").replaceAll("[^0-9]",""));
						insert.setAction("insert");
						insert.doAction();
		
						clone.putAll(keyValue);
						clone.setParameter("CreatedDate", "NOW");
						clone.setParameter("ModifiedDate", "NOW");
						clone.setParameter("CreatedBy", userid);
						clone.setParameter("ModifiedBy", userid);
						clone.setColumn("LinkedDocumentID",KeyMaker.generate());
						clone.setColumn("CreatedDate","NOW");
						clone.setColumn("CreatedBy",userid);
						clone.setColumn("DocumentID",newDocumentID);
						clone.setColumn("ContentID",KeyMaker.generate());
						clone.setColumn("ProductID",toProductID);
						try {
							clone.doAction("insert");
						} catch (Exception e) {
							clone.clearStatement();
							clone.setParameter("ParentProductID", toProductID);
							clone.doAction("update");
//							logger.error("error in {} of {}","copyDripCostings",e.getMessage());
						}
					}
				}
			}
		}
	}
	
}
