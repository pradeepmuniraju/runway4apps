package com.sok.runway.crm.cms.properties.pdf.advanced.wrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sog.pdfserver.client.api.request.TemplateListTag;
import com.sog.pdfserver.client.api.request.PairTag;
import com.sog.pdfserver.client.api.request.TemplateTagStyle;

public class AdvancedTagUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(AdvancedTagUtil.class);
	
	public static PairTag createTag(String tagName, String tagValue) {
		logger.debug("Tag [{}] = [{}]", tagName, tagValue);
		return createTag(tagName, tagValue, null, null);
	}

	public static PairTag createTag(String tagName, String tagValue, String font, String color) {
		PairTag tag = new PairTag();
		tag.setName(tagName);
		tag.setValuePair(createValuePair(tagName, tagValue));

		/*
		if (StringUtils.isNotBlank(font) || StringUtils.isNotBlank(color)) {
			TemplateTagStyle stylePrice = new TemplateTagStyle();
			if (StringUtils.isNotBlank(font))
				stylePrice.setColor(font);
			if (StringUtils.isNotBlank(color))
				stylePrice.setColor(color);
			tag.setValuePair(tag.getValuePair().putAll(createValuePair(key, stylePrice)))();
		}
		*/
		return tag;

	}
	
	public static TemplateListTag createTag(String tagName, List<String> tagValue) {
		return createTag(tagName, tagValue, null, null);
	}

	public static TemplateListTag createTag(String tagName, List<String> tagValue, String font, String color) {
		TemplateListTag tag = new TemplateListTag();
		tag.setName(tagName);
		tag.setValues(tagValue);

		if (StringUtils.isNotBlank(font) || StringUtils.isNotBlank(color)) {
			TemplateTagStyle stylePrice = new TemplateTagStyle();
			if (StringUtils.isNotBlank(font))
				stylePrice.setColor(font);
			if (StringUtils.isNotBlank(color))
				stylePrice.setColor(color);
			//tag.setStyle(stylePrice);
		}

		return tag;

	}
	
	public static Map<String,String> createValuePair(String key, String value) {
		HashMap<String,String> map = new HashMap<String,String>();
		
		map.put(key, value);
		
		return map;
	}
}
