/**
 * 
 */
package com.sok.runway.crm;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.offline.rpmManager.RPMtask;
import com.sok.runway.security.User;

/**
 * @author dion
 *
 */
public class ProductDelete {
	
	private static ActivityLogger activityLogger = ActivityLogger.getActivityLogger();
	public static HashMap<String,ProductDelete>	masterDelete = new HashMap<String,ProductDelete>(); 
	private User user;
	
	private String							id;
	
	private ArrayList<String>				processedIDs = new ArrayList<String>();
	private ArrayList<String>				productIDs = new ArrayList<String>();
	private ProductDeleteSimulation			simulation = null;
	private HashMap<String,ProductDelete>	childProducts = new HashMap<String,ProductDelete>(); 
	private HashMap<String,ProductDelete>	parentProducts = new HashMap<String,ProductDelete>(); 
	
	public HashMap<String,HashMap>			productList = new HashMap<String,HashMap>();
	
	private HashMap<String,Integer>			statusList = new HashMap<String,Integer>();
	
	private GenRow							product = null;

	private boolean							debug = false;
	
	private int								current = 0;

	private Connection 						con = null;
	private Context							ctx = null;
	private String 							dbconn = null;
	
	private boolean							isDeleteDone = false;

	/**
	 * @param productID - single product that needs to be deleted
	 */
	public ProductDelete(String id, String productID) {

		this.id = id;

		if (!masterDelete.containsKey(id)) masterDelete.put(id, this);

		this.productIDs.add(productID);
		
		if (!getMasterDelete(id).productList.containsKey(id)) {
			getMasterDelete(id).productList.put(id, (new HashMap<String,ProductDelete>()));
		}
	}

	/**
	 * @param productIDs - a List for products that needs to be deleted
	 */
	public ProductDelete(String id, ArrayList<String> productIDs) {

		this.id = id;

		if (!masterDelete.containsKey(id)) masterDelete.put(id, this);

		this.productIDs.addAll(productIDs);

		if (!getMasterDelete(id).productList.containsKey(id)) {
			getMasterDelete(id).productList.put(id, (new HashMap<String,ProductDelete>()));
		}
	}

	/**
	 * @param productIDs - a List for products that needs to be deleted
	 */
	public ProductDelete(String id, String[] productIDs) {
		this.id = id;
		
		if (!masterDelete.containsKey(id)) masterDelete.put(id, this);

		for (int s = 0; productIDs != null && s < productIDs.length; ++s) this.productIDs.add(productIDs[s]); 

		if (!getMasterDelete(id).productList.containsKey(id)) {
			getMasterDelete(id).productList.put(id, (new HashMap<String,ProductDelete>()));
		}
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public static ProductDelete getMasterDelete(String id) {
		return masterDelete.get(id);
	}
	
	public void simulateDelete(HttpServletRequest request, int level) {
		isDeleteDone = false;
		if (productIDs.size() == 1) {
			if (!isAlreadyProcessed(productIDs.get(0))) {
				if (!getMasterDelete(id).processedIDs.contains(productIDs.get(0))) getMasterDelete(id).processedIDs.add(productIDs.get(0));
				if (level >= 0) {
					simulateDelete(request,productIDs.get(0),level);
					//simulateChild(request,productIDs.get(0),level);
					//simulateParent(request,productIDs.get(0),1);
				} else {
					simulateDelete(request,productIDs.get(0),1);
					//simulateParent(request,productIDs.get(0),1);
				}
			}
			addProcessedItem(productIDs.get(0));
		} else if (productIDs.size() > 1) {
			for (current = 0; current < productIDs.size(); ++current){
				(new ProductDelete(id, productIDs.get(current))).simulateDelete(request, level);
			}
		}
		try {
			if (con != null && !con.isClosed()) {
				con.close();
			}
		} catch (Exception e) {
			System.out.println("Count not close connection " + e.getMessage());
		}
	}
	
	public void simulateDelete(int level) {
		isDeleteDone = false;
		if (productIDs.size() == 1) {
			if (!isAlreadyProcessed(productIDs.get(0))) {
				if (!getMasterDelete(id).processedIDs.contains(productIDs.get(0))) getMasterDelete(id).processedIDs.add(productIDs.get(0));
				if (level >= 0) {
					simulateDelete(productIDs.get(0),level);
					//simulateChild(productIDs.get(0),level);
					//simulateParent(productIDs.get(0),1);
				} else {
					simulateDelete(productIDs.get(0),1);
					//simulateParent(productIDs.get(0),1);
				}
			}
			addProcessedItem(productIDs.get(0));
		} else if (productIDs.size() > 1) {
			for (current = 0; current < productIDs.size(); ++current){
				(new ProductDelete(id, productIDs.get(current))).simulateDelete(level);
			}
		}
		try {
			if (con != null && !con.isClosed()) {
				con.close();
			}
		} catch (Exception e) {
			System.out.println("Count not close connection " + e.getMessage());
		}
	}

	private boolean simulateParent(String productID, int level) {
		isDeleteDone = false;
		boolean noParent = true;
		if (productID == null || productID.length() > 0) return false;
		if (level > 0) {
			// find any children related to this product
			GenRow parentProduct = new GenRow();
			parentProduct.setViewSpec("ProductProductView");
			parentProduct.setConnection(getConnection());
			parentProduct.setColumn("LinkedProductID|1", productID);
			parentProduct.doAction("search");
			parentProduct.getResults();
			
			ArrayList<String> ppArray = new ArrayList<String>();
			while (parentProduct.getNext()) {
				ppArray.add(parentProduct.getString("ProductID"));
			}
			parentProduct.close();

			for (int p = 0; p < ppArray.size(); ++p) {
				if (!parentProducts.containsKey(ppArray.get(p)) && !childProducts.containsKey(ppArray.get(p))) {
					GenRow product = loadProductByID(ppArray.get(p));
					ProductDelete pd = new ProductDelete(id, ppArray.get(p));
					if (!pd.isAlreadyProcessed(ppArray.get(p))) {
						pd.addProcessedItem(ppArray.get(p));
						if ("House and Land".equalsIgnoreCase(product.getString("ProductType"))) {
							if (!pd.simulateDelete(ppArray.get(p),(level)) ||
								!pd.simulateChild(ppArray.get(p), (level + 1))) {
								parentProducts.put(ppArray.get(p), pd);
								noParent = false;
							}
						} else {
							String productType = product.getString("ProductType");
							if (!"House and Land".equalsIgnoreCase(productType) && !"Stage".equalsIgnoreCase(productType) && !"Estate".equalsIgnoreCase(productType)
									 && !"Home".equalsIgnoreCase(productType) && !"Home Range".equalsIgnoreCase(productType)) {
								pd.simulateParent(ppArray.get(p), level - 1);
							}
							parentProducts.put(ppArray.get(p), pd);
						}
					}
				}
			}
		}
		

		if (debug) System.out.println("simulateParent " + level + " " + productID + " " + noParent);

		return noParent;
		
	}

	private boolean simulateDelete(String productID, int level) {
		isDeleteDone = false;
		if (!getMasterDelete(id).processedIDs.contains(productID) && getMasterDelete(id).productIDs.contains(productID)) getMasterDelete(id).processedIDs.add(productID);

		if (product == null) product = loadProductByID(productID);
		if (simulation == null) simulation = new ProductDeleteSimulation();
		
		if (getMasterDelete(id).statusList != null) {
			HashMap<String,Integer> statusL = getMasterDelete(id).statusList;
			Integer i = statusL.get(product.getString("CurrentStatusID"));
			if (i == null) i = new Integer(0);
			++i;
			statusL.put(product.getString("CurrentStatusID"), i);
		}
		
		simulation.setStatus(product.getString("CurrentStatusID"));
		// first search for any processes that have this product in them;
		GenRow process = new GenRow();
		process.setViewSpec("OpportunityView");
		process.setConnection(getConnection());
		process.setColumn("ProductID|1", productID);
		process.setColumn("ReferenceProductID|1", productID);
		process.doAction("search");
		process.getResults();
		
		while (process.getNext()) {
			simulation.addProcess(process.getString("OpportunityID"));
		}
		process.close();
		
		GenRow linkedProcess = new GenRow();
		linkedProcess.setViewSpec("LinkedProductListView");
		linkedProcess.setConnection(getConnection());
		linkedProcess.setColumn("ProductID", productID);
		linkedProcess.doAction("search");
		linkedProcess.getResults();
		
		while (linkedProcess.getNext()) {
			simulation.addProcess(linkedProcess.getString("OpportunityID"));
		}
		linkedProcess.close();
		
		if ("Land".equals(product.getString("ProductType"))) {
			GenRow linkedProduct = new GenRow();
			linkedProduct.setViewSpec("ProductLinkedProductView");
			linkedProduct.setConnection(getConnection());
			linkedProduct.setColumn("LinkedProductID", productID);
			linkedProduct.doAction("search");
			linkedProduct.getResults();
			
			while (linkedProduct.getNext()) {
				simulation.addProduct(linkedProduct.getString("ProductID"));
			}
			linkedProduct.close();
		} else if (!"Land".equals(product.getString("ProductType")) && !"House and Land".equals(product.getString("ProductType")) && !"Home Plan".equals(product.getString("ProductType"))) {
			GenRow linkedProduct = new GenRow();
			linkedProduct.setViewSpec("ProductLinkedProductView");
			linkedProduct.setConnection(getConnection());
			linkedProduct.setColumn("ProductID", productID);
			linkedProduct.doAction("search");
			linkedProduct.getResults();
			
			while (linkedProduct.getNext()) {
				simulation.addProduct(linkedProduct.getString("LinkedProductID"));
			}
			linkedProduct.close();
		}

		// find any commissions related to this product
		GenRow commission = new GenRow();
		commission.setViewSpec("CommissionListView");
		commission.setConnection(getConnection());
		commission.setColumn("ProductID|1", productID);
		commission.doAction("search");
		commission.getResults();
		
		while (commission.getNext()) {
			simulation.addCommission(commission.getString("CommissionID"));
		}
		commission.close();
		
		// find any orders related to this product
		GenRow order = new GenRow();
		order.setViewSpec("OrderItemsView");
		order.setConnection(getConnection());
		order.setColumn("ProductID|1", productID);
		order.doAction("search");
		order.getResults();
		
		while (order.getNext()) {
			simulation.addQuote(order.getString("OrderID"));
		}
		order.close();
		
		if (debug) System.out.println("simulateDelete " + level + " " + productID + " " + simulation.canBeDelete());
		
		return simulation.canBeDelete();
	}

	private GenRow loadProductByID(String productID) {
		   GenRow product = new GenRow();
		   product.setConnection(getConnection());
		   product.setViewSpec("ProductView");
		   product.setParameter("ProductID", productID);
		   //product.setParameter("-select","*");
		   product.setAction("selectfirst");
		   if (productID == null || productID.length() > 0) product.doAction();
		   
		   return product;
	}

	private boolean simulateChild(String productID, int level) {
		boolean noChild = true;
		if (level > 8) return noChild;
		
		// If this is a stage or estate then there will variations
		GenRow childVariation = new GenRow();
		childVariation.setTableSpec("ProductVariations");
		childVariation.setConnection(getConnection());
		childVariation.setParameter("-select", "*");
		childVariation.setColumn("ParentProductID|1", productID);
		if (productID == null || productID.length() > 0) {
			childVariation.doAction("search");
			childVariation.getResults();
		}
		
		ArrayList<String> cvArray = new ArrayList<String>(); 
		while (childVariation.getNext()) {
			cvArray.add(childVariation.getString("ChildProductID"));
		}
		childVariation.close();

		for (int a = 0; a < cvArray.size(); ++a) {
			if (!childProducts.containsKey(cvArray.get(a))) {
				ProductDelete pd = new ProductDelete(id,cvArray.get(a));
				if (!pd.isAlreadyProcessed(cvArray.get(a))) {
					pd.addProcessedItem(cvArray.get(a));
					if (!pd.simulateDelete(cvArray.get(a), (level + 1)) ||
						!pd.simulateParent(cvArray.get(a), (level - 1)) ||
						!pd.simulateChild(cvArray.get(a), (level + 1))) {
						childProducts.put(cvArray.get(a), pd);
						noChild = false;
					}
				}
			}
		}

		GenRow childProduct = new GenRow();
		childProduct.setViewSpec("ProductProductView");
		childProduct.setConnection(getConnection());
		childProduct.setColumn("ProductID|1", productID);
		if (productID == null || productID.length() > 0) {
			childProduct.doAction("search");
			childProduct.getResults();
		}
		
		ArrayList<String> cpArray = new ArrayList<String>(); 
		while (childProduct.getNext()) {
			cpArray.add(childProduct.getString("LinkedProductID"));
		}
		childProduct.close();
		
		for (int a = 0; a < cpArray.size(); ++a) {
			if (!childProducts.containsKey(cpArray.get(a))) {
				ProductDelete pd = new ProductDelete(id,cpArray.get(a));
				if (!pd.isAlreadyProcessed(cpArray.get(a))) {
					pd.addProcessedItem(cpArray.get(a));
					if (!pd.simulateDelete(cpArray.get(a), (level + 1)) ||
						!pd.simulateChild(cpArray.get(a), (level + 1)) ||
						!pd.simulateParent(cpArray.get(a), level - 1)) {
						childProducts.put(cpArray.get(a), pd);
						noChild = false;
					}
				}
			}
		}
		if (debug) System.out.println("simulateChild " + level + " " + productID + " " + noChild);

		return noChild;
	}

	protected boolean isAlreadyProcessed(String productID) {
		HashMap thisMap = getMasterDelete(id).productList.get(id);
		if (thisMap == null) {
			thisMap = new HashMap<String,ProductDelete>();
			getMasterDelete(id).productList.put(id, thisMap);
		}
		
		return thisMap.containsKey(productID);
	}
	
	protected void addProcessedItem(String productID) {
		HashMap thisMap = getMasterDelete(id).productList.get(id);
		if (thisMap == null) {
			thisMap = new HashMap<String,ProductDelete>();
		}
		thisMap.put(productID,this);
		getMasterDelete(id).productList.put(id, thisMap);
	}
	
	public int getProcessedCount() {
		return getMasterDelete(id).processedIDs.size();
	}
	
	protected boolean simulateDelete(HttpServletRequest request, String productID, int level) {
		if (product == null) product = loadProductByID(request, productID);
		if (simulation == null) simulation = new ProductDeleteSimulation();
		
		if (getMasterDelete(id).statusList != null) {
			HashMap<String,Integer> statusL = getMasterDelete(id).statusList;
			Integer i = statusL.get(product.getString("CurrentStatusID"));
			if (i == null) i = new Integer(0);
			++i;
			statusL.put(product.getString("CurrentStatusID"), i);
		}
		
		simulation.setStatus(product.getString("CurrentStatusID"));

		// first search for any processes that have this product in them;
		GenRow process = new GenRow();
		process.setViewSpec("OpportunityView");
		process.setRequest(request);
		process.setColumn("ProductID|1", productID);
		process.setColumn("ReferenceProductID|1", productID);
		if (productID == null || productID.length() > 0) {
			process.doAction("search");
			process.getResults();
		}
		
		while (process.getNext()) {
			simulation.addProcess(process.getString("OpportunityID"));
		}
		process.close();
		
		GenRow linkedProcess = new GenRow();
		linkedProcess.setViewSpec("LinkedProductListView");
		linkedProcess.setRequest(request);
		linkedProcess.setColumn("ProductID", productID);
		if (productID == null || productID.length() > 0) {
			linkedProcess.doAction("search");
			linkedProcess.getResults();
		}
		
		while (linkedProcess.getNext()) {
			simulation.addProcess(linkedProcess.getString("OpportunityID"));
		}
		linkedProcess.close();
		
		// find any commissions related to this product
		GenRow commission = new GenRow();
		commission.setViewSpec("CommissionListView");
		commission.setRequest(request);
		commission.setColumn("ProductID|1", productID);
		if (productID == null || productID.length() > 0) {
			commission.doAction("search");
			commission.getResults();
		}
		
		while (commission.getNext()) {
			simulation.addCommission(commission.getString("CommissionID"));
		}
		commission.close();
		
		// find any orders related to this product
		GenRow order = new GenRow();
		order.setViewSpec("OrderItemsView");
		order.setRequest(request);
		order.setColumn("ProductID|1", productID);
		if (productID == null || productID.length() > 0) {
			order.doAction("search");
			order.getResults();
		}
		
		while (order.getNext()) {
			simulation.addQuote(order.getString("OrderID"));
		}
		order.close();
		
		if (debug) System.out.println("simulateDelete " + level + " " + productID + " " + simulation.canBeDelete());
		
		return simulation.canBeDelete();
	}
	
	protected boolean simulateChild(HttpServletRequest request, String productID, int level) {
		boolean noChild = true;
		if (level > 8) return noChild;
		
		// If this is a stage or estate then there will variations
		GenRow childVariation = new GenRow();
		childVariation.setTableSpec("ProductVariations");
		childVariation.setRequest(request);
		childVariation.setParameter("-select", "*");
		childVariation.setColumn("ParentProductID|1", productID);
		if (productID == null || productID.length() > 0) {
			childVariation.doAction("search");
			childVariation.getResults();
		}
		
		ArrayList<String> cvArray = new ArrayList<String>(); 
		while (childVariation.getNext()) {
			cvArray.add(childVariation.getString("ChildProductID"));
		}
		childVariation.close();

		for (int a = 0; a < cvArray.size(); ++a) {
			if (!childProducts.containsKey(cvArray.get(a))) {
				ProductDelete pd = new ProductDelete(id,cvArray.get(a));
				if (!pd.isAlreadyProcessed(cvArray.get(a))) {
					pd.addProcessedItem(cvArray.get(a));
					if (!pd.simulateDelete(cvArray.get(a), (level + 1)) ||
						!pd.simulateParent(cvArray.get(a), (level - 1)) ||
						!pd.simulateChild(cvArray.get(a), (level + 1))) {
						childProducts.put(cvArray.get(a), pd);
						noChild = false;
					}
				}
			}
		}

		GenRow childProduct = new GenRow();
		childProduct.setViewSpec("ProductProductView");
		childProduct.setRequest(request);
		childProduct.setColumn("ProductID|1", productID);
		if (productID == null || productID.length() > 0) {
			childProduct.doAction("search");
			childProduct.getResults();
		}
		
		ArrayList<String> cpArray = new ArrayList<String>(); 
		while (childProduct.getNext()) {
			cpArray.add(childProduct.getString("LinkedProductID"));
		}
		childProduct.close();
		
		for (int a = 0; a < cpArray.size(); ++a) {
			if (!childProducts.containsKey(cpArray.get(a))) {
				ProductDelete pd = new ProductDelete(id,cpArray.get(a));
				if (!pd.isAlreadyProcessed(cpArray.get(a))) {
					pd.addProcessedItem(cpArray.get(a));
					if (!pd.simulateDelete(cpArray.get(a), (level + 1)) ||
						!pd.simulateChild(cpArray.get(a), (level + 1)) ||
						!pd.simulateParent(cpArray.get(a), level - 1)) {
						childProducts.put(cpArray.get(a), pd);
						noChild = false;
					}
				}
			}
		}
		
		if (debug) System.out.println("simulateChild " + level + " " + productID + " " + noChild);

		return noChild;
	}
	
	protected boolean simulateParent(HttpServletRequest request, String productID, int level) {
		boolean noParent = true;
		if (level > 0) {
			// find any children related to this product
			GenRow parentProduct = new GenRow();
			parentProduct.setViewSpec("ProductProductView");
			parentProduct.setRequest(request);
			parentProduct.setColumn("LinkedProductID|1", productID);
			if (productID == null || productID.length() > 0) {
				parentProduct.doAction("search");
				parentProduct.getResults();
			}
			
			ArrayList<String> ppArray = new ArrayList<String>();
			while (parentProduct.getNext()) {
				ppArray.add(parentProduct.getString("ProductID"));
			}
			parentProduct.close();

			for (int p = 0; p < ppArray.size(); ++p) {
				if (!parentProducts.containsKey(ppArray.get(p)) && !childProducts.containsKey(ppArray.get(p))) {
					GenRow product = loadProductByID(ppArray.get(p));
					ProductDelete pd = new ProductDelete(id, ppArray.get(p));
					if (!pd.isAlreadyProcessed(ppArray.get(p))) {
						pd.addProcessedItem(ppArray.get(p));
						if ("House and Land".equalsIgnoreCase(product.getString("ProductType"))) {
							if (!pd.simulateDelete(ppArray.get(p),(level)) ||
								!pd.simulateChild(ppArray.get(p), (level + 1))) {
								parentProducts.put(ppArray.get(p), pd);
								noParent = false;
							}
						} else {
							String productType = product.getString("ProductType");
							if (!"House and Land".equalsIgnoreCase(productType) && !"Stage".equalsIgnoreCase(productType) && !"Estate".equalsIgnoreCase(productType)
									 && !"Home".equalsIgnoreCase(productType) && !"Home Range".equalsIgnoreCase(productType)) {
								pd.simulateParent(ppArray.get(p), 1);
							}
							parentProducts.put(ppArray.get(p), pd);
						}
					}
				}
			}
		}

		if (debug) System.out.println("simulateParent " + level + " " + productID + " " + noParent);

		return noParent;
		
	}
	
	public int getProductCount() {
		HashMap thisMap = getMasterDelete(id).productList.get(id);
		if (thisMap != null) {
			return thisMap.size();
		}
		return -1;
	}

	public void doActualDelete(String productID, String pType, Set<String> deletable) {
		isDeleteDone = false;
		current = 0;
		Collection<ProductDelete> collection = this.getProductList();
		if (collection != null) {
			Iterator<ProductDelete> it = collection.iterator();
			
			System.out.println("Number of Products to Delete " + collection.size());

			while (it.hasNext()) {
				ProductDelete pd = it.next();
				GenRow prod = null;
				try {
					if (pd.canBeDeleted(deletable) && (prod = pd.getProduct()) != null) {
						String type = prod.getString("ProductType");
						if (pType.length() > 0) {
							if (pType.equalsIgnoreCase(type)) {
								String prodID = prod.getString("ProductID");
								if (productID.indexOf(prodID) >= 0) {
									prod.setConnection(getConnection());
									if (prod.getString("ProductID").length() > 0) {
										prod.doAction("delete");
										if (RPMtask.getInstance() != null) RPMtask.getInstance().doIndexRefreash("delete", prodID, type);
									}
									if (getMasterDelete(id).processedIDs != null) {
										getMasterDelete(id).processedIDs.remove(prodID);
									}
									
									activityLogger.logActivity(user, getConnection(), pType, prodID, ActivityLogger.ActionType.Deleted.name());
	
									GenRow ProductDetails = new GenRow();
									ProductDetails.setTableSpec("properties/ProductDetails");
									ProductDetails.setConnection(getConnection());
									ProductDetails.setColumn("ON-ProductID", prodID);
									if (ProductDetails.getString("ON-ProductID").length() > 0) ProductDetails.doAction("deleteall");
	
									GenRow childVariation = new GenRow();
									childVariation.setTableSpec("ProductVariations");
									childVariation.setConnection(getConnection());
									childVariation.setParameter("-select", "*");
									childVariation.setColumn("ON-ParentProductID|1", prodID);
									childVariation.setColumn("ON-ChildProductID|1", prodID);
									if (childVariation.getString("ON-ParentProductID|1").length() > 0 && childVariation.getString("ON-ChildProductID|1").length() > 0) 
										childVariation.doAction("deleteall");
	
									GenRow childProduct = new GenRow();
									childProduct.setTableSpec("ProductProducts");
									childProduct.setConnection(getConnection());
									childProduct.setColumn("ON-ProductID|1", prodID);
									childProduct.setColumn("ON-LinkedProductID|1", prodID);
									if (childProduct.getString("ON-ProductID|1").length() > 0 && childProduct.getString("ON-LinkedProductID|1").length() > 0) 
										childProduct.doAction("deleteall");
	
									System.out.println("Deleted Product " + prodID);
									
									GenRow row = new GenRow();
									row.setTableSpec("Activities");
									row.setConnection(getConnection());
									row.setParameter("RecordID", prodID);
									row.setParameter("RecordType","Product");
									row.setParameter("ActionType", "Deleted");
									row.setParameter("CreatedDate", "NOW");
									row.setParameter("UserID", user.getUserID());
									row.setParameter("ActivityID", KeyMaker.generate());
									row.doAction("insert");
								}
							}
						} else {
							if ("Developer".equalsIgnoreCase(type) || "Brand".equalsIgnoreCase(type) || "Estate".equalsIgnoreCase(type) || "Stage".equalsIgnoreCase(type) ||
									"Land".equalsIgnoreCase(type) || "Home Range".equalsIgnoreCase(type) || "Home Plan".equalsIgnoreCase(type) ||
									"Building Developer".equalsIgnoreCase(type) || "Building".equalsIgnoreCase(type) || "Building Stage".equalsIgnoreCase(type) || "Apartment".equalsIgnoreCase(type) ||
									"House and Land".equalsIgnoreCase(type) || "Home".equalsIgnoreCase(type) || "Existing Property".equalsIgnoreCase(type) || "Builder".equalsIgnoreCase(type)) {
								String prodID = prod.getString("ProductID");
								prod.setConnection(getConnection());
								if (prod.getString("ProductID").length() > 0) {
									prod.doAction("delete");
									if (RPMtask.getInstance() != null) RPMtask.getInstance().doIndexRefreash("delete", prodID, type);
								}
								
								activityLogger.logActivity(user, getConnection(), type, prodID, ActivityLogger.ActionType.Deleted.name());
								
								if (getMasterDelete(id).processedIDs != null) {
									getMasterDelete(id).processedIDs.remove(prodID);
								}
	
								GenRow ProductDetails = new GenRow();
								ProductDetails.setTableSpec("properties/ProductDetails");
								ProductDetails.setConnection(getConnection());
								ProductDetails.setColumn("ON-ProductID", prodID);
								if (ProductDetails.getString("ON-ProductID").length() > 0) ProductDetails.doAction("deleteall");
	
								GenRow childVariation = new GenRow();
								childVariation.setTableSpec("ProductVariations");
								childVariation.setConnection(getConnection());
								childVariation.setParameter("-select", "*");
								childVariation.setColumn("ON-ParentProductID|1", prodID);
								childVariation.setColumn("ON-ChildProductID|1", prodID);
								if (childVariation.getString("ON-ParentProductID|1").length() > 0 && childVariation.getString("ON-ChildProductID|1").length() > 0) 
									childVariation.doAction("deleteall");
	
								GenRow childProduct = new GenRow();
								childProduct.setTableSpec("ProductProducts");
								childProduct.setConnection(getConnection());
								childProduct.setColumn("ON-ProductID|1", prodID);
								childProduct.setColumn("ON-LinkedProductID|1", prodID);
								if (childProduct.getString("ON-ProductID|1").length() > 0 && childProduct.getString("ON-LinkedProductID|1").length() > 0) 
									childProduct.doAction("deleteall");
								
								
								GenRow relatedProducts = new GenRow();
								relatedProducts.setTableSpec("RelatedProducts");
								relatedProducts.setConnection(getConnection());
								relatedProducts.setColumn("ON-ProductID|1", prodID);
								relatedProducts.setColumn("ON-LinkedProductID|1", prodID);
								if (relatedProducts.getString("ON-ProductID|1").length() > 0 && relatedProducts.getString("ON-LinkedProductID|1").length() > 0) 
									relatedProducts.doAction("deleteall");
	
								System.out.println("Deleted Product " + prodID);
								
								GenRow row = new GenRow();
								row.setTableSpec("Activities");
								row.setConnection(getConnection());
								row.setParameter("RecordID", prodID);
								row.setParameter("RecordType","Product");
								row.setParameter("ActionType", "Deleted");
								row.setParameter("CreatedDate", "NOW");
								row.setParameter("UserID", user.getUserID());
								row.setParameter("ActivityID", KeyMaker.generate());
								row.doAction("insert");
							}
						}
					}
					++current;
				} catch (Exception e) {
					System.out.println("Exception in delete, moving on");
					e.printStackTrace();
				}
			}
		}
		System.out.println("Number of Products Deleted " + current);
		try {
			if (con != null && !con.isClosed()) {
				con.close();
			}
		} catch (Exception e) {
			System.out.println("Count not close connection " + e.getMessage());
		}
		isDeleteDone = true;
	}
	
	public boolean isDeleteDone() {
		return isDeleteDone;
	}
	
	public boolean canBeDeleted(Set<String> deletable) {
		if (simulation != null) {
			// can this one be deleted?
			if (deletable != null && deletable.size() > 0 && !deletable.contains(simulation.getStatus())) return false;
			if (!simulation.canBeDelete() && deletable == null) return false;
			
			// can all of the children be deleted
			Collection<ProductDelete> collection = childProducts.values();
			Iterator<ProductDelete> i = collection.iterator();
			
			while (i.hasNext()) {
				ProductDelete pd = i.next();
				if (!pd.canBeDeleted(deletable)) return false;
			}
			
			// can all the parents be deleted?
			collection = parentProducts.values();
			i = collection.iterator();
			
			while (i.hasNext()) {
				ProductDelete pd = i.next();
				if (!pd.canBeDeleted(deletable)) return false;
			}
	
			return true;
		} else {
			HashMap<String,ProductDelete> thisMap = getMasterDelete(id).productList.get(id);
			if (thisMap != null) {
				for (int p = 0; p < productIDs.size(); ++p) {
					ProductDelete pDel = thisMap.get(productIDs.get(p));
					if (pDel != null && pDel.hasSimulation() && !pDel.canBeDeleted(deletable)) return false;
				}
			}
			return true;
		}
	}
	
	public boolean hasSimulation() {
		return simulation != null;
	}
	
	private GenRow loadProductByID(HttpServletRequest request, String productID) {
	   GenRow product = new GenRow();
	   product.setRequest(request);
	   product.setViewSpec("ProductView");
	   product.setParameter("ProductID", productID);
	   //product.setParameter("-select","*");
		if (productID == null || productID.length() > 0) {
		   product.setAction("selectfirst");
		   product.doAction();
		}
	   
	   return product;
	}

	public Collection<ProductDelete> getProductList() {
		HashMap<String,ProductDelete> theMap = getMasterDelete(id).productList.get(id);
		if (theMap != null) {
			return theMap.values();
		}
		
		return null;
	}
	
	public GenRow getProduct() {
		return product;
	}
	
	public Set getProcesses() {
		return simulation.getProcesses();
	}
	
	public Set getCommissions() {
		return simulation.getCommissions();
	}
	
	public Set getQuotes() {
		return simulation.getQuotes();
	}
	
	public int getProcessCount() {
		return simulation.getProcessCount();
	}

	public int getCommissionCount() {
		return simulation.getCommissionCount();
	}

	public int getQuoteCount() {
		return simulation.getQuoteCount();
	}
	
	public String getID() {
		return id;
	}
	
	public static HashMap<String,ProductDelete> getProductList(String id) {
		return getMasterDelete(id).productList.get(id);
	}

	public int getDeleted() {
		if (getMasterDelete(id).processedIDs == null || getMasterDelete(id).productIDs == null) return 0;
		return getMasterDelete(id).productIDs.size() - getMasterDelete(id).processedIDs.size();
	}
	public int getCurrent() {
		if (getMasterDelete(id).processedIDs == null) return 0;
		return getMasterDelete(id).processedIDs.size();
	}
	
	public int getMax() {
		if (getMasterDelete(id).productIDs == null) return 0;
		return getMasterDelete(id).productIDs.size();
	}
	public Connection getConnection()
	{
		try
		{
			if(con == null || con.isClosed()) {
				if(ctx == null)
				{
					ctx = new InitialContext();
				}
	
				DataSource ds = null;
				try
				{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource) envContext.lookup(dbconn);
				}
				catch(Exception e)
				{
				}
				if(ds == null)
				{
					ds = (DataSource) ctx.lookup(dbconn);
				}
				con = ds.getConnection();
			}
		}
		catch(Exception e)
		{
		}
		finally
		{
			try
			{
				ctx.close();
				ctx = null;
			}
			catch(Exception e)
			{
			}
		}
		
		return con;
	}

	public HashMap<String, Integer> getStatusList() {
		return statusList;
	}
}
