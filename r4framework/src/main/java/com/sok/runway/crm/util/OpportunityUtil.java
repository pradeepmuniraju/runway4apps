package com.sok.runway.crm.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.framework.TableSpec;
import com.sok.framework.ViewSpec;
import com.sok.framework.generation.GenerationKeys;

public class OpportunityUtil {

	private OpportunityUtil() {
		// TODO Auto-generated constructor stub
	}

	public static void loadProductDetailsTokens(HttpServletRequest request, Map<String,String> specificcontext, String productID) {
		String ignorelist = "ProductDetailsID,ProductID,CopiedFromProductID,ProductType,DetailsType,SortOrder";

		GenRow product = new GenRow();
		product.setTableSpec("properties/ProductDetails");
		product.setRequest(request);
		product.setParameter("-select0","*");
		product.setParameter("ProductID", productID);
		product.doAction("search");

		product.getResults();

		TableSpec ts = product.getTableSpec();

		while (product.getNext()) {

			for (int i = 0; i < ts.getFieldsLength(); ++i) {
				if (ignorelist.indexOf(ts.getFieldName(i)) == -1) specificcontext.put("Product_Details_" + product.getString("DetailsType").replaceAll("[^a-zA-Z0-9]", "") + "_" + ts.getFieldName(i), product.getString(ts.getFieldName(i)));
			}
		}

		product.close();

	}

	public static void loadProductTokens(HttpServletRequest request, Map<String,String> specificcontext, String productID) {
		GenRow find = new GenRow();
		find.setTableSpec("Products");
		find.setRequest(request);
		find.setParameter("-select0", "ProductType");
		find.setParameter("ProductID", productID);
		find.doAction("selectfirst");

		GenRow product = new GenRow();
		product.setViewSpec("ProductView");
		product.setRequest(request);
		product.setParameter("ProductID", productID);
		product.setParameter("ProductType", find.getString("ProductType"));


		if ("House and Land".equals(find.getString("ProductType"))) {
			product.setViewSpec("properties/ProductHouseAndLandView");
		} else if ("Land".equals(find.getString("ProductType"))) {
			product.setViewSpec("properties/ProductLotView");
		} else if ("Home Plan".equals(find.getString("ProductType"))) {
			product.setViewSpec("properties/ProductPlanView");
		} else if ("Apartment".equals(find.getString("ProductType"))) {
			product.setViewSpec("properties/ProductApartmentView");
		} else if ("Existing Property".equals(find.getString("ProductType"))) {
			product.setViewSpec("properties/ProductExistingPropertyView");
		}

		product.doAction("selectfirst");

		if (product.isSuccessful()) {
			ViewSpec vs = product.getViewSpec();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put("Product_" + vs.getLocalItemName(i), product.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("Product_" + vs.getRelatedItemName(i), product.getString(vs.getRelatedItemName(i)));
			}
		}
		
		GenRow quickIndex = new GenRow();
		quickIndex.setViewSpec("{properties/}ProductQuickIndexView");
		quickIndex.setRequest(request);
		quickIndex.setParameter("ProductID", productID);
		quickIndex.doAction("selectfirst");
		
		if (quickIndex.isSuccessful()) {
			ViewSpec vs = quickIndex.getViewSpec();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put("ProductQuick_" + vs.getLocalItemName(i), quickIndex.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("ProductQuick_" + vs.getRelatedItemName(i), quickIndex.getString(vs.getRelatedItemName(i)));
			}
			
			String fullName = "";
			if ("Land".equals(quickIndex.getString("ProductType"))) {
				if (!quickIndex.getString("Name").startsWith("Lot") && !quickIndex.getString("Name").startsWith("Block")) fullName = "Lot ";
				fullName += quickIndex.getString("Name") + " " + quickIndex.getString("StageName") + " " + quickIndex.getString("EstateName"); 
			} else if ("Home Plan".equals(quickIndex.getString("ProductType"))) {
				if (!quickIndex.getString("Name").startsWith("Lot") && !quickIndex.getString("Name").startsWith("Block")) fullName = "Lot ";
				fullName += quickIndex.getString("DesignName") + " " + quickIndex.getString("Name") + ", " + quickIndex.getString("RangeName"); 
			} else if ("Apartment".equals(quickIndex.getString("ProductType"))) {
				if (!quickIndex.getString("Name").startsWith("Apartment")) fullName = "Apartment ";
				fullName += quickIndex.getString("Name") + " Level " + quickIndex.getString("StageName") + ", " + quickIndex.getString("EstateName"); 
			} else {
				fullName = quickIndex.getString("Name");
			}

			specificcontext.put("ProductQuick_FullName", fullName);
		}
		
		if (product.isSet("RegionID")) loadLocationTokens(request, specificcontext, product.getString("RegionID"), "Product_Region_");
		if (product.isSet("LocationID")) loadLocationTokens(request, specificcontext, product.getString("LocationID"), "Product_Location_");
		
		GenRow addressRow = new GenRow();
		addressRow.setRequest(request);
		addressRow.setTableSpec("Addresses");
		addressRow.setColumn("ProductID", productID);
		addressRow.setParameter("-select1", "AddressID");
		addressRow.setParameter("-select2", "ProductID");
		addressRow.setParameter("-select3", "LocationID");
		addressRow.setParameter("-select4", "GroupID");
		addressRow.setParameter("-select5", "StreetNumber");
		addressRow.setParameter("-select6", "Street");
		addressRow.setParameter("-select7", "Street2");
		addressRow.setParameter("-select8", "City");
		addressRow.setParameter("-select9", "State");
		addressRow.setParameter("-select10", "Postcode");
		addressRow.setParameter("-select11", "Region");
		addressRow.setParameter("-select12", "Country");
		addressRow.setParameter("-select13", "UseMap");
		addressRow.setParameter("-select14", "Estate");
		addressRow.setParameter("-select15", "Latitude");
		addressRow.setParameter("-select16", "Longitude");
		addressRow.setParameter("-select17", "MapRef");

		if(addressRow.isSet("ProductID")) { 
			addressRow.doAction(GenerationKeys.SELECTFIRST);
		} 
		
		String street1 = addressRow.getString("Street");
		String street2 = addressRow.getString("Street2");
		String city = addressRow.getString("City");
		String state = addressRow.getString("State");
		String postcode = addressRow.getString("Postcode");
		String region = addressRow.getString("Region");
		String country = addressRow.getString("Country");
		String mapRef = addressRow.getString("MapRef");

		String fullAddress = ""; 
		if (!StringUtil.isBlankOrEmpty(street1))
		{
			fullAddress += street1;
		}

		if (!StringUtil.isBlankOrEmpty(street2))
		{
			fullAddress += " " + street2;
		}

		if (!StringUtil.isBlankOrEmpty(city))
		{
			if (StringUtil.isBlankOrEmpty(fullAddress))
			{
				fullAddress += city;
			}
			else
			{	
				fullAddress += ", " + city;
			}
		}

		if (!StringUtil.isBlankOrEmpty(state))
		{
			fullAddress += ", " + state;
		}

		if (!StringUtil.isBlankOrEmpty(postcode))
		{
			fullAddress += " " + postcode;
		}

		specificcontext.put("Product_Street", street1);
		specificcontext.put("Product_Street2", street2);
		specificcontext.put("Product_City", city);
		specificcontext.put("Product_Postcode", postcode);
		specificcontext.put("Product_State", state);
		specificcontext.put("Product_Country", country);

		specificcontext.put("Product_FullAddress", fullAddress);
	}
	
 	public static void loadOpportunityTokens(HttpServletRequest request, Map<String,String> specificcontext, String opportunityID) {
		GenRow opportunity = new GenRow();
		opportunity.setViewSpec("OpportunityView");
		opportunity.setRequest(request);
		opportunity.setParameter("OpportunityID", opportunityID);
		opportunity.doAction("selectfirst");

		if (opportunity.isSuccessful()) {
			ViewSpec vs = opportunity.getViewSpec();

			String table = vs.getMainTableName();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				if("date".equalsIgnoreCase(vs.getLocalItemFormatType(i))) {
					specificcontext.put("Process_" + vs.getLocalItemName(i), opportunity.getData(vs.getLocalItemName(i)));
				} else {
					specificcontext.put("Process_" + vs.getLocalItemName(i), opportunity.getString(vs.getLocalItemName(i)));
				}
				
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("Process_" + vs.getRelatedItemName(i), opportunity.getString(vs.getRelatedItemName(i)));
			}
		}
		
		if (opportunity.isSet("RegionID")) loadLocationTokens(request, specificcontext, opportunity.getString("RegionID"), "Process_Region_");
		if (opportunity.isSet("LocationID")) loadLocationTokens(request, specificcontext, opportunity.getString("LocationID"), "Process_Location_");
		

	}
 	
	public static void loadLocationTokens(HttpServletRequest request, Map<String,String> specificcontext, String locationID, String key) {
		GenRow row = new GenRow();
		row.setViewSpec("DisplayEntityView");
		row.setRequest(request);
		row.setParameter("DisplayEntityID", locationID);
		row.doAction("selectfirst");
		
		specificcontext.put(key + "Name", row.getString("Name"));
		specificcontext.put(key + "Type", row.getString("Name"));
		specificcontext.put(key + "GroupName", row.getString("GroupName"));
		specificcontext.put(key + "UniqueID", row.getString("ProductNumber"));
		specificcontext.put(key + "Phone", row.getString("Phone"));
		specificcontext.put(key + "Email", row.getString("Email"));
		specificcontext.put(key + "Web", row.getString("Web"));
		specificcontext.put(key + "DisplayHours", row.getString("DisplayHours"));

		String street1 = row.getString("Street");
		String street2 = row.getString("Street2");
		String city = row.getString("City");
		String state = row.getString("State");
		String postcode = row.getString("Postcode");
		String region = row.getString("Region");
		String country = row.getString("Country");
		String mapRef = row.getString("MapRef");

		String fullAddress = ""; 
		if (!StringUtil.isBlankOrEmpty(street1))
		{
			fullAddress += street1;
		}

		if (!StringUtil.isBlankOrEmpty(street2))
		{
			fullAddress += " " + street2;
		}

		if (!StringUtil.isBlankOrEmpty(city))
		{
			if (StringUtil.isBlankOrEmpty(fullAddress))
			{
				fullAddress += city;
			}
			else
			{	
				fullAddress += ", " + city;
			}
		}

		if (!StringUtil.isBlankOrEmpty(state))
		{
			fullAddress += ", " + state;
		}

		if (!StringUtil.isBlankOrEmpty(postcode))
		{
			fullAddress += " " + postcode;
		}

		specificcontext.put(key + "Street", street1);
		specificcontext.put(key + "Street2", street2);
		specificcontext.put(key + "City", city);
		specificcontext.put(key + "Postcode", postcode);
		specificcontext.put(key + "State", state);
		specificcontext.put(key + "Country", country);

		specificcontext.put(key + "FullAddress", fullAddress);
		
		GenRow companyLinkedContacts = new GenRow();
		companyLinkedContacts.setViewSpec("CompanyContactLinkView");
		companyLinkedContacts.setConnection(row.getConnection());
		companyLinkedContacts.setParameter("CompanyID", row.getString("ProductID"));
		companyLinkedContacts.sortBy("RepAllocation", 0);
		if (companyLinkedContacts.isSet("CompanyID")) {
			companyLinkedContacts.doAction("search");
			
			companyLinkedContacts.getResults();
			
			StringBuilder emails = new StringBuilder();
			
			int c = 1;
			
			while (companyLinkedContacts.getNext()) {

				specificcontext.put(key + "Staff_" + c + "_FirstName", companyLinkedContacts.getString("FirstName"));
				specificcontext.put(key + "Staff_" + c + "_LastName", companyLinkedContacts.getString("LastName"));
				specificcontext.put(key + "Staff_" + c + "_Phone", companyLinkedContacts.getString("Phone"));
				specificcontext.put(key + "Staff_" + c + "_Mobile", companyLinkedContacts.getString("Mobile"));
				specificcontext.put(key + "Staff_" + c + "_Email", companyLinkedContacts.getString("Email"));
				
				if ("Y".equals(companyLinkedContacts.getString("ToFlag")) || "Y".equals(companyLinkedContacts.getString("CCFlag")) || "Y".equals(companyLinkedContacts.getString("BCCFlag"))) {
					specificcontext.put(key + "Staff_" + c + "_DistributeTo", "Y");
					if (companyLinkedContacts.isSet("Email")) {
						if (emails.length() > 0) emails.append(", ");
						emails.append(companyLinkedContacts.getString("Email"));
					}
				}
				
			}
			
			specificcontext.put(key + "RepEmails", emails.toString());
		}
		
		companyLinkedContacts.close();
		row.close();
	}

	public static void LoadProductAnswers(HttpServletRequest request, Map<String,String> specificcontext, String key, String productID) {
		GenRow row = new GenRow();
		row.setViewSpec("answers/ProductAnswerView");
		row.setRequest(request);
		row.setParameter("ProductID", productID);
		row.doAction("search");

		row.getResults();

		while (row.getNext()) {
			if (row.isSet("Answer")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_Answer", row.getString("Answer"));
			if (row.isSet("AnswerText")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerText", row.getString("AnswerText"));
			if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerNumber", row.getString("AnswerNumber"));
			if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerCurrency", row.getString("AnswerCurrency"));
			if (row.isSet("AnswerDate")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerDate", row.getString("AnswerDate"));
			if (row.isSet("OtherAnswer")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_OtherAnswer", row.getString("OtherAnswer"));
			if (row.isSet("QuestionShortLabel")) {
				if (row.isSet("Answer")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_Answer", row.getString("Answer"));
				if (row.isSet("AnswerText")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerText", row.getString("AnswerText"));
				if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerNumber", row.getString("AnswerNumber"));
				if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerCurrency", row.getString("AnswerCurrency"));
				if (row.isSet("AnswerDate")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerDate", row.getString("AnswerDate"));
				if (row.isSet("OtherAnswer")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_OtherAnswer", row.getString("OtherAnswer"));
			}
		}
		
		row.close();
	}

 	public static void loadOpportunityLinkedContactTokens(HttpServletRequest request, Map<String,String> specificcontext, String opportunityID) {
		GenRow opportunity = new GenRow();
		opportunity.setViewSpec("OpportunityContactsListView");
		opportunity.setRequest(request);
		opportunity.setParameter("OpportunityID", opportunityID);
		opportunity.doAction("search");
		opportunity.getResults();

		while (opportunity.getNext()) {
			String role = opportunity.getString("Role").replaceAll(" ", "_");
			ViewSpec vs = opportunity.getViewSpec();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put("Process_Contact_" + role + "_" + vs.getLocalItemName(i), opportunity.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("Process_Contact_" + role + "_" + vs.getRelatedItemName(i), opportunity.getString(vs.getRelatedItemName(i)));
			}
			
			loadContactGroupTokens(request, specificcontext, opportunity.getString("ContactID"), "Process_Contact_" + role + "_");
		}
		
		opportunity.close();
	}
	
	public static void loadContactGroupTokens(HttpServletRequest request, Map<String,String> specificcontext, String contactID, String key) {
		List<String> groupNames = new ArrayList<String>();
		GenRow cg = new GenRow();
		cg.setViewSpec("ContactGroupView");
		cg.setRequest(request);
		cg.setParameter("ContactID", contactID);
		cg.sortBy("SortOrder", 0);
		cg.doAction("search");
		cg.getResults();

		ViewSpec vs = cg.getViewSpec();

		if (cg.getNext()) {			
			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put(key + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
				specificcontext.put(key + "Default_" + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put(key + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
				specificcontext.put(key + "Default_" + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
			}
			do {
				groupNames.add(cg.getString("Name"));
				String group = cg.getString("Name").replaceAll(" ", "_");

				for (int i = 0; i < vs.getLocalLength(); ++i) {
					specificcontext.put(key + group + "_" + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
				}
				for (int i = 0; i < vs.getRelatedLength(); ++i) {
					specificcontext.put(key + group + "_" + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
				}
			} while (cg.getNext());
		}
		
		specificcontext.put("GroupNames", StringUtils.join(groupNames, ", "));
		cg.close();

	}

	/** Token is going to be like Process_Company_<Role>_<ViewSpecColumnName>
	 * @param opportunityID
	 */
	public static void loadOpportunityLinkedCompanyTokens(HttpServletRequest request, Map<String,String> specificcontext, String opportunityID) {
		GenRow opportunity = new GenRow();
		opportunity.setViewSpec("OpportunityCompanyListView");
		opportunity.setRequest(request);
		opportunity.setParameter("OpportunityID", opportunityID);
		opportunity.doAction("search");
		opportunity.getResults();

		while (opportunity.getNext()) {
			String role = opportunity.getString("Role").replaceAll(" ", "_");
			ViewSpec vs = opportunity.getViewSpec();

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put("Process_Company_" + role + "_" + vs.getLocalItemName(i), opportunity.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("Process_Company_" + role + "_" + vs.getRelatedItemName(i), opportunity.getString(vs.getRelatedItemName(i)));
			}
			
			loadCompanyGroupTokens(request, specificcontext, opportunity.getString("CompanyID"), "Process_Company_" + role + "_" );
		}
		
		opportunity.close();

	}

	public static void loadCompanyGroupTokens(HttpServletRequest request, Map<String,String> specificcontext, String companyID, String key) {
		GenRow cg = new GenRow();
		cg.setViewSpec("CompanyGroupView");
		cg.setRequest(request);
		cg.setParameter("CompanyID", companyID);
		cg.sortBy("SortOrder", 0);
		cg.doAction("search");
		cg.getResults();

		ViewSpec vs = cg.getViewSpec();

		if (cg.getNext()) {
			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put(key + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
				specificcontext.put(key + "Default_" + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put(key + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
				specificcontext.put(key + "Default_" + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
			}
			do {
				String group = cg.getString("Name").replaceAll(" ", "_");

				for (int i = 0; i < vs.getLocalLength(); ++i) {
					specificcontext.put(key + group + "_" + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
				}
				for (int i = 0; i < vs.getRelatedLength(); ++i) {
					specificcontext.put(key + group + "_" + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
				}
			} while (cg.getNext());
		}
		
		cg.close();

	}

	public static void loadOpportunityStageTokens(HttpServletRequest request, Map<String,String> specificcontext, String opportunityID, String processStageID) {
		GenRow opportunity = new GenRow();
		opportunity.setViewSpec("OpportunityStageStageView");
		opportunity.setRequest(request);
		opportunity.setParameter("OpportunityID", opportunityID);
		opportunity.doAction("search");

		opportunity.getResults();

		ViewSpec vs = opportunity.getViewSpec();

		while (opportunity.getNext()) {

			for (int i = 0; i < vs.getLocalLength(); ++i) {
				specificcontext.put("Process_" + opportunity.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_" + vs.getLocalItemName(i), opportunity.getString(vs.getLocalItemName(i)));
				if (opportunity.getString("ProcessStageID").equals(processStageID))
					specificcontext.put("Process_CurrentStage_" + vs.getLocalItemName(i), opportunity.getString(vs.getLocalItemName(i)));

			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				specificcontext.put("Process_" + opportunity.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_" + vs.getRelatedItemName(i), opportunity.getString(vs.getRelatedItemName(i)));
				if (opportunity.getString("ProcessStageID").equals(processStageID))
					specificcontext.put("Process_CurrentStage_" + vs.getRelatedItemName(i), opportunity.getString(vs.getRelatedItemName(i)));
			}

			LoadOpportunityStageAnswers(request, specificcontext, "Process_" + opportunity.getString("Name").replaceAll("[^a-zA-Z0-9]", ""), opportunityID, opportunity.getString("ProcessStageID"));
			if (opportunity.getString("ProcessStageID").equals(processStageID)) {
				LoadOpportunityStageAnswers(request, specificcontext, "Process_CurrentStage", opportunityID, opportunity.getString("ProcessStageID"));
			}
		}

		LoadOpportunityStageAnswers(request, specificcontext, "Process_AllStages", opportunityID, "NULL;EMPTY");

		opportunity.close();

	}

	public static void LoadOpportunityStageAnswers(HttpServletRequest request, Map<String,String> specificcontext, String key, String opportunityID, String processStageID) {
		GenRow row = new GenRow();
		row.setViewSpec("answers/OpportunityAnswerView");
		row.setRequest(request);
		row.setParameter("OpportunityID", opportunityID);
		row.setParameter("ProcessStageID", processStageID);
		row.doAction("search");

		row.getResults();

		while (row.getNext()) {
			if (row.isSet("Answer")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_Answer", row.getString("Answer"));
			if (row.isSet("AnswerText")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerText", row.getString("AnswerText"));
			if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerNumber", row.getString("AnswerNumber"));
			if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerCurrency", row.getString("AnswerCurrency"));
			if (row.isSet("AnswerDate")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerDate", row.getString("AnswerDate"));
			if (row.isSet("OtherAnswer")) specificcontext.put(key + "_Question_" + row.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_OtherAnswer", row.getString("OtherAnswer"));
			if (row.isSet("QuestionShortLabel")) {
				if (row.isSet("Answer")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_Answer", row.getString("Answer"));
				if (row.isSet("AnswerText")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerText", row.getString("AnswerText"));
				if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerNumber", row.getString("AnswerNumber"));
				if (row.isSet("AnswerNumber")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerCurrency", row.getString("AnswerCurrency"));
				if (row.isSet("AnswerDate")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_AnswerDate", row.getString("AnswerDate"));
				if (row.isSet("OtherAnswer")) specificcontext.put(key + "_Question_" + row.getString("QuestionShortLabel").replaceAll("[^a-zA-Z0-9]", "") + "_OtherAnswer", row.getString("OtherAnswer"));
			}
		}
		
		row.close();
	}

}
