package com.sok.runway.crm.interfaces;

import com.sok.runway.crm.*;
import com.sok.runway.security.*;

import java.util.*;
import java.sql.*;

public interface ProductLinkable extends Linkable {
   
   public Collection<String> getLinkableProductIDs();
   
   public Collection<String> getLinkableProductIDs(Map<String,String> filterParameters);
   
   public LinkableProduct getLinkableProduct();
   
   public void linkProduct(String productID);
}