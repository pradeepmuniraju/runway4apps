package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.framework.*;

import java.sql.*;
import java.util.*;
import javax.servlet.ServletRequest;

public abstract class RunwayAction extends RunwayEntity {
   
   public static final String ACTION_SURVEY = "ACTION_SURVEY";
	public static final String ACTION_QUESTION = "ACTION_QUESTION";
	public static final String ACTION_EMAIL = "ACTION_EMAIL";
	public static final String ACTION_SMS = "ACTION_SMS";
	public static final String ACTION_COMPANY_STATUS = "ACTION_COMPANY_STATUS";
	public static final String ACTION_CONTACT_STATUS = "ACTION_CONTACT_STATUS";
	public static final String ACTION_CAMPAIGN_STATUS = "ACTION_CAMPAIGN_STATUS";
	public static final String ACTION_NOTE = "ACTION_NOTE";
	
	public static final ActionType[] ACTION_TYPES = {
	            new ActionType(ACTION_SURVEY, "Complete Survey"),
	            new ActionType(ACTION_QUESTION, "Answer Question"),
	            new ActionType(ACTION_EMAIL, "Send Email"),
	            new ActionType(ACTION_SMS, "Send SMS"),
	            new ActionType(ACTION_COMPANY_STATUS, "Update Company Status"),
	            new ActionType(ACTION_CONTACT_STATUS, "Update Contact Status"),
	            new ActionType(ACTION_CAMPAIGN_STATUS, "Update Campaign Status"),
	            new ActionType(ACTION_NOTE, "Create Note")
	            };
	
	public static class ActionType {
	   public String key;
	   public String display;
	   
	   public ActionType(String key, String display) {
	      this.key = key;
	      this.display = display;
	   }
	   
	   public boolean equals(String key) {
	      return key.equals(key);
	   }
	}
	
   private RowBean actionParameters = null;
   
   public RunwayAction(Object con, String viewSpec) {
      super(con, viewSpec);
   }
   
   public RunwayAction(ServletRequest request, String viewSpec) {
      super(request, viewSpec);
   }

   public RunwayAction(Connection con, String viewSpec) {
      super(con, viewSpec);
   }
   
   private RowBean getActionParameters() {
      if (actionParameters == null) {
         this.actionParameters = new RowBean();
         actionParameters.setViewSpec("ContactView");
      }
      return this.actionParameters;
   }
   
   public void setActionParameter(String key, String value) {
      getActionParameters().setColumn(key, value);
   }
   
   public String getActionParameter(String key) {
      return getActionParameters().getColumn(key);
   }
   
   public void beforeSave() {
      setField("ActionParameters", getActionParameters().toString());
   }

   
}