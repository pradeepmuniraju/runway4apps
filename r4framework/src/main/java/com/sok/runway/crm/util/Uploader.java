package com.sok.runway.crm.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.servlet.ServletContext;

import com.sok.Debugger;
import com.sok.framework.KeyMaker;

public abstract class Uploader {

	private Debugger deb =  new Debugger();
	private String majorError = null; 
	private File newdir; 
	private String directory = "";
	private boolean uploadSuccess = false; 
	private String basepath = ""; 
	
	{ 
		deb.setDebug(true); 
	}
	
	public String getBasepath() {
		return basepath;
	}
	
	public String getDirectory() {
		return directory;
	}
	
	public void debug(String s) { 
		deb.debug(s); 
	}
	public void debug(Exception e) { 
		deb.debug(e); 
	}
	
	public String getErrors() {  
		return majorError + /*"<!-- "  + */deb.getMessages()/*+  "-->" */; 
	}
	
	public void setError(String error) { 
		majorError = error; 
	}
	
	/** 
	 * no arg constructor useful for debugging. 
	 */
	public Uploader() { 
		
	}
	
	protected void setUploadSuccess(boolean uploadSuccess) { 
		this.uploadSuccess = uploadSuccess; 
	}
	
	public Uploader(File f, String basepath, String dir) {
		
		uploadSuccess = doRun(f,basepath, dir); 
	}
	
	public boolean doRun(File zipFile, String basepath, String directory) { 
		boolean ok = true; 
		if(zipFile.exists()) { 
			try { 
				this.directory = directory;
				this.basepath = basepath;
				newdir = new File(basepath + this.directory);
				
				if(!newdir.exists()) { 
					newdir.mkdirs();
				} 
				//System.out.println("done making directories :"+newdir.getAbsolutePath()); 
				ok = extract(zipFile, newdir);
				
			} catch (IOException ioe) { 
				debug(ioe);
				setError("Uploaded file failed to open");
			}
		} else {
			setError("Upload file did not exist"); 
		}
		
		if(!ok) { 
			this.basepath = ""; 
			this.directory = ""; 
			setError("The Upload did not complete successfully");
		} 
		
		return ok; 
	}
	
	public boolean getUploadSuccess() { 
		return uploadSuccess; 
	}
	
	protected boolean extract(File f, File newdir) throws IOException { 
		ZipFile zf = new ZipFile(f); 
		
		//debug("starting"); 
		
		for (Enumeration<? extends ZipEntry> zfe = zf.entries() ; zfe.hasMoreElements() ;) {
			ZipEntry ze = zfe.nextElement();
			
			if(!ze.isDirectory()) {
				if(!ze.getName().endsWith("Thumbs.db")) {
					//debug(ze.getName()); 
					
					if(ze.getName().indexOf("/")> -1) { 
						String[] ds = ze.getName().split("/");
						File base = newdir; 
						for(int i=0; i<(ds.length-1); i++) { 
							base = new File(base, ds[i]);
							if(!base.exists()) { 
								base.mkdir(); 
								//deb.debug("making dir : "+base.getAbsolutePath()+ " = "+base.mkdir());
							}
						}
					}
					
					File of = new File(newdir, ze.getName()); 
					
					if(of.exists()) {  
						of.delete(); 
						//debug("deleting : "+ze.getName()+ " = "+of.delete()); 
					}
					InputStream is = zf.getInputStream(ze);
					//debug("Available: "+is.available()); 

					int BUFFER = 2048; 
					
					FileOutputStream fos = new FileOutputStream(of);
					BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);
					
					int count = 0; 
					byte[] data = new byte[BUFFER]; 
					
					while ((count = is.read(data, 0, BUFFER)) != -1) {
						   //System.out.write(x);
						   dest.write(data, 0, count);
						}

					dest.flush(); 
					dest.close();
					is.close();
					
					boolean success = (of.exists() && of.length() > 0); 
					
					if(!success) { 
						//debug("failed on "+of.getAbsolutePath()+" exists "+of.exists() + " " + of.length()); 
						return false; 
					}
					
					checkFile(ze, of); 
				} 
			}
		}
		return true;
	}
	
	protected abstract void checkFile(ZipEntry ze, File of);
	
	public String[] getDirectoryList() { 
		File f = new File(getBasepath() + getDirectory());
		return f.list(new java.io.FilenameFilter() {
			public boolean accept(File dir, String name) {
				return new File(dir, name).isDirectory();
			}
		});
		
	}
}
