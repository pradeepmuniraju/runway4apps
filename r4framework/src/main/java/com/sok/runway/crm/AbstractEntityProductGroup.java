package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.runway.crm.factories.*;
import com.sok.runway.campaigns.*;
import com.sok.runway.security.*;
import com.sok.runway.crm.activity.*;
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys; 


import javax.servlet.ServletRequest;
import java.util.*;
import java.sql.*;

public abstract class AbstractEntityProductGroup extends ProfiledEntity implements StatusHistory {
      
   public AbstractEntityProductGroup(Connection con, String viewSpec) {
      super(con, viewSpec);
   }
	
	public AbstractEntityProductGroup(ServletRequest request, String viewSpec) {
	    super(request, viewSpec);
	}
   
   public abstract void setStatusHistoryID(String id);

   public String getStatusID() {
      return getParameter("StatusID");
   }
   
   public ErrorMap update() {
      if (getStatusID() != null && getStatusID().length() != 0) {
         
         StatusUpdater su = StatusUpdater.getStatusUpdater();
         if (su.isNewStatus(this)) {
            setStatusHistoryID(KeyMaker.generate());
            su.insertStatus(this);
         }
      }
      
      return super.update();
   }
   
   public ErrorMap insert() {
      boolean insertStatus = false;
      if (getStatusID().length() != 0) {
         if (getStatusHistoryID().length() == 0) {
            setStatusHistoryID(KeyMaker.generate());
         }
         insertStatus = true;
      }
      ErrorMap map = super.insert();
      
      if (map.isEmpty()) {
         
         if (insertStatus) {
            StatusUpdater su = StatusUpdater.getStatusUpdater();
            su.insertStatus(this);
         }
      }
      
      return map;
   }
}