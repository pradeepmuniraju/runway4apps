package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.service.crm.cms.properties.PackageService;

public class Land extends PropertyEntity {

	private static final Logger logger = LoggerFactory.getLogger(PropertyEntity.class);
	
	final PropertyType propertyType = PropertyType.Land;
	static final String VIEWSPEC = "properties/ProductLotView";
	private Estate estate = null;
	private Stage stage = null;
	public Land(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public Land(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	
	public Land(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	
	@Override
	public Land load(String productID) { 
		return (Land)super.load(productID);
	}
	
	@Override
	public void clear() { 
		stage = null;
		estate = null;
		super.clear();
	}
	
	@Override
	public Object get(String key) { 
		if("estateproduct".equals(key)) return getEstate();
		if("stageproduct".equals(key)) return getStage();
		return super.get(key);
	}
	
	public String getLotNumber() { 
		return getString("Name");
	}
	
	public String getLotName() { 
		return getString("Name").toUpperCase().startsWith("LOT")? getString("Name") :"Lot " + getString("Name");
	}
	
	public double getArea() { 
		return getDouble("Area");
	}
	
	public double getLotWidth() { 
		return getDouble("LotWidth");
	}
	
	public double getLotDepth() { 
		return getDouble("LotDepth");
	}
	
	public String getEstateName() { 
		if(getEstate() != null) { 
			return estate.getString("Name");
		}
		return null;
	}
	
	public String getCity(){
		return getString("City");
	}
	
	public String getEstateImage() { 
		logger.trace("checking estate image"); 
		if(getEstate() != null) {
			logger.trace("have estate"); 
			if(StringUtils.isNotBlank(estate.getString("ThumbnailImagePath"))) {
				return estate.getString("ThumbnailImagePath");
			}
		}
		return null;
	}
	
	/**
	 * Added for Develoepr Pdfs for Henley
	 * @return String
	 */
	public String getEstateWebURL() { 
		logger.trace("checking estate URL"); 
		if(getEstate() != null) {
			logger.trace("have estate"); 
			if(StringUtils.isNotBlank(estate.getString("URL"))) {
				return estate.getString("URL");
			}
		}
		return null;
	}
	
	/**
	 * Added for Develoepr Pdfs for Henley
	 * @return String
	 */
	public String getEstateID() { 
		logger.trace("checking estate ID , which is EstateProductID in ProductLotView"); 
		if(getEstate() != null) {
			logger.trace("have estate"); 
			if(StringUtils.isNotBlank(estate.getString("ProductID"))) {
				return estate.getString("ProductID");
			}
		}
		return null;
	}
	
	/**
	 * Added for JGKing Pdfs
	 * @return String
	 */
	public String getEstateLocationID() { 
		logger.trace("retrieving estate Location ID , which is LocationID in ProductLotView"); 
		if(getEstate() != null) {
			logger.trace("have estate"); 
			if(StringUtils.isNotBlank(estate.getString("LocationID"))) {
				return estate.getString("LocationID");
			}
		}
		return null;
	}
	

	/**
	 * Added for PorterDavis PDFs
	 * @return
	 */
	public String getEstateSuburb() { 
		logger.trace("checking estate Suburb "); 
		if(getEstate() != null) {
			logger.trace("have estate"); 
			if(StringUtils.isNotBlank(estate.getString("City"))) {
				return estate.getString("City");
			}
		}
		return null;
	}

	/**
	 * Added for PorterDavis PDFs
	 * @return
	 */
	public String getEstateMapRef() { 
		logger.trace("checking estate Map Ref"); 

		/*if(getEstate() != null) {
			System.out.println("have estate"); 
			if(StringUtils.isNotBlank(estate.getString("MapRef"))) {
				return estate.getString("MapRef");
			}
		}*/

		if(StringUtils.isNotBlank(getString("MapRef"))) {
			return getString("MapRef");
		}

		return null;
	}

	
	
	public Estate getEstate() {
		if(estate == null && isSuccessful() && isSet("EstateProductID")) {
			//System.out.println("checking estate"); 
			estate = new Estate(getConnection()).load(getData("EstateProductID"));
			if(!estate.isSuccessful()) {
				logger.warn("estate couldn't be loaded {}", getData("EstateProductID")); 
				estate = null;
			}
		} 
		return estate;
	}
	
	public String getStageName() { 
		return getString("StageName");
	}
	
	@Override
	public GenRow getSalesRep() { 
		if(isSuccessful() && isSet(PRODUCTID)) {
			GenRow row = new GenRow(); 
			row.setConnection(getConnection());
			row.setTableSpec("ProductSecurityGroups");
			row.setParameter("ProductID", getString(PRODUCTID));
			row.setParameter("-select0","RepUserID");
			row.setParameter("-select1","ProductSecurityGroupRep.FirstName");
			row.setParameter("-select2","ProductSecurityGroupRep.LastName"); 
			row.setParameter("-select3","ProductSecurityGroupRep.Mobile"); 
			row.setParameter("-select4","ProductSecurityGroupRep.Phone");
			row.setParameter("-select5","ProductSecurityGroupRep.Email");
			row.doAction(GenerationKeys.SELECTFIRST);
			
			if(row.isSuccessful() && row.isSet("FirstName")) {
				return row;
			} 
		}
		return super.getSalesRep();
	}
	
	public String getPrice() { 
		String cost = getColumn("TotalCost");
		if(cost.indexOf(".")>0) { 
			return cost.substring(0, cost.indexOf("."));
		}
		return cost;
	}
	
	public Stage getStage() {
		if(stage == null && isSuccessful() && isSet("StageProductID")) {
			stage = new Stage(getConnection()).load(getData("StageProductID"));
			if(!stage.isSuccessful()) { 
				stage = null;
			}
		}
		return stage;
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
	public String getLocationID() {
		if(isSet("LocationID")) {
			return getString("LocationID");
		}
		if(getStage() != null) {
			return stage.getLocationID();
		}
		return null;
	}
	
	
	public String getDripTotalCost() { 
		String cost = getColumn("DripResult");
		// if the price is $0.00 then use the TotalCost
		if (cost.replaceAll("[0\\.\\$]","").length() == 0) cost = getColumn("TotalCost");
		if(cost.indexOf(".")>0) { 
			return cost.substring(0, cost.indexOf("."));
		}
		return cost;
	}
	
	public String getPackageCount() {
		String c = "0";
		if(StringUtils.isNotBlank(PRODUCTID)) {
			GenRow row = PackageService.getInstance().getPlanSearch(null, null, "LotID=" + PRODUCTID, true);
			row.doAction(GenerationKeys.SEARCH);
			row.getResults(true);
			
			logger.debug("getPackageCount {} ", row.getStatement());
			
			if(row.isSuccessful()) {
				c = Integer.toString(row.getSize());
			} 
			
			row.close();
		}
		return c;
	}
	

	@Override
	public String[] getDripProductIDs() {
		return new String[]{getString("EstateProductID"), getString("StageProductID"), getString(PRODUCTID)};
	}	
}
