package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.cms.properties.Land;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.Package;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ListFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil.Estate;
import com.sok.runway.offline.ThreadManager;

public class LandPropertyListPdf extends AbstractPDFTemplate implements PDFConstants {
	
	public static final Logger logger = LoggerFactory.getLogger(LandPropertyListPdf.class); 
	
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException,
		DocumentException {
		throw new UnsupportedOperationException("Must use writer method if one is defined");
	}
	
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document, final PdfWriter writer) throws IOException,
			DocumentException {
		
		try {
			renderPDFConcurrent(request, response, document, writer);
			return;
		} catch (ExecutionException ee) {
			logger.error("Execution Exception", ee);
		} catch (InterruptedException ie) {
			logger.error("Execution Interrupted", ie);
		}
		
		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (List<PropertyEntity>)request.getAttribute("entityList"); 
		if(properties == null) {
			setStatus(request,"Loading Records..", 0.02d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			properties = PropertyFactory.getPropertyEntities(request, this);
		}
		
		HashMap<String, Estate> estates = new HashMap<String, Estate>();
		
		double num = new Double(properties.size()), record = 1;
		for (PropertyEntity property : properties) {
			if(record % 5 == 0) {
				setStatus(request, "Loading package information" , record / num);
			}
			record++;
			
			if (!property.isPropertyType(PropertyType.Land)) {
				throw new RuntimeException(String.format("ProductID [%1$s] Property [%2$s] is not a Land",
						property.getString("ProductID"), property.getPropertyType()));
			}
			Land product = (Land) property;
			Package p = PDFUtil.getLandDetailsPackage(product, getPdfSimpleName());

			if (p.estateName == null || p.estateName.length() == 0) p.estateName = "- Missing -";
			Estate e = (Estate) estates.get(p.estateName);
			if (e == null) {
				e = new PDFUtil.Estate();
				e.estateName = p.estateName;
				// 16-Nov-2012 NI: e.estateLogo = PropertyEntity.resizeImage(p.estateLogo, 140, 65, PropertyEntity.ResizeOption.Fit);
				e.estateLogo = p.estateLogo;
				estates.put(p.estateName, e);
			}
			e.packages.add(p);
			
			
			//String disclaimer = property.getDripText(request, new String[] { property.getString(PRODUCTID) }, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip
			/*String disclaimer = disclaimer = property.getDripText(request, new String[] { product.getString("EstateProductID"), product.getString("StageProductID"), product.getString("ProductID") }, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip;
			if (StringUtil.isBlankOrEmpty(PDFUtil.DISCLAIMER_TEXT))
			{
				disclaimer = property.getDripText(request, new String[] { product.getString("EstateProductID"), product.getString("StageProductID"), product.getString("ProductID") }, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip
				
				ExistingPropertyPDFUtil.DISCLAIMER_TEXT = disclaimer; 
				if (StringUtil.isBlankOrEmpty(ExistingPropertyPDFUtil.DISCLAIMER_TEXT))
					ExistingPropertyPDFUtil.DISCLAIMER_TEXT = "";
			}*/	
			
		}
		setStatus(request, "Generating PDF", 0.8d);
		
		renderPages(document, estates);
	}

	private void renderPages(com.itextpdf.text.Document document, HashMap<String, Estate> estates) throws DocumentException {
		TreeSet<String> sortedEstates = new TreeSet<String>(estates.keySet());
		boolean addNewPage = false;

		for (String estateName : sortedEstates) {
			if (addNewPage)
				document.newPage();
			else
				addNewPage = true;

			Estate estate = (Estate) estates.get(estateName);
			renderEstatePage(document, estate);
		}
	}

	private void renderEstatePage(com.itextpdf.text.Document document, Estate e) throws DocumentException {
		Collections.sort(e.packages, new PDFUtil.PackageNameComparator());
		PdfPTable hdrTable = PDFUtil.getHeaderForLandPropertyListPdf(e);

		PdfPTable pageTable = new PdfPTable(1);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.getDefaultCell().setFixedHeight(0f);
		pageTable.setWidthPercentage(100f);

		pageTable.addCell(hdrTable);

		pageTable.getDefaultCell().setFixedHeight(11f);
		pageTable.addCell(BLANK);

		for (int j = 0; j < e.packages.size(); j++) {
			if (j != 0 && j % 6 == 0) {
				document.add(pageTable);
				document.newPage();
				pageTable = new PdfPTable(1);
				pageTable.getDefaultCell().setBorder(0);
				pageTable.getDefaultCell().setPadding(0);
				pageTable.getDefaultCell().setFixedHeight(0f);
				pageTable.setWidthPercentage(100f);

				pageTable.addCell(hdrTable);
				pageTable.getDefaultCell().setFixedHeight(11f);
				pageTable.addCell(BLANK);
			}

			pageTable.getDefaultCell().setFixedHeight(0f);
			pageTable.getDefaultCell().setBorderWidthTop(0.01f);
			pageTable.getDefaultCell().setBorderWidthBottom(0.01f);
			pageTable.getDefaultCell().setBorderColor(BaseColor.BLACK);

			Package p = (Package) e.packages.get(j);
			pageTable.addCell(PDFUtil.buildPackageTableForLandPropertyListPdf(p));

			pageTable.getDefaultCell().setBorder(0);
			pageTable.getDefaultCell().setFixedHeight(6f);
			pageTable.addCell(BLANK);

		}

		document.add(pageTable);
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		return new ListFooter(request);
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request, String header, String disclaimer) {
		return new ListFooter(request, header, disclaimer);
	}
	
	public void renderPDFConcurrent(final HttpServletRequest request, HttpServletResponse response, Document document, final PdfWriter writer) throws IOException,
			DocumentException, ExecutionException, InterruptedException {
		
		List<PropertyEntity> properties = (List<PropertyEntity>)request.getAttribute("entityList"); 
		if(properties == null) {
			setStatus(request,"Loading Records..", 0.02d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			PropertyFactory.getPropertyEntities(request, this);
		}
		
		final int size = properties.size();
		double num = new Double(size), record = 1;
		final List<Future<Package>> packageList = new ArrayList<Future<Package>>(size);
		
		for (final PropertyEntity property : properties) {
			if(record % 5 == 0) {
				setStatus(request, "Loading package information" , record / num);
			}
			record++;
			if (!property.isPropertyType(PropertyType.Land)) {
				throw new RuntimeException(String.format("ProductID [%1$s] Property [%2$s] is not a Land",
						property.getString("ProductID"), property.getPropertyType()));
			}
			final Land product = (Land) property;
			
			packageList.add(ThreadManager.startCallable(new Callable<Package>() {
				public Package call() {
					return PDFUtil.getLandDetailsPackage(product, getPdfSimpleName());
				}
			}));
		}
		
		Map<String, EstateTableView> etvMap = new HashMap<String, EstateTableView>();
		for(Future<Package> fp: packageList) {
			Package p = fp.get();
			EstateTableView e = etvMap.containsKey(p.estateName) ? etvMap.get(p.estateName) : null;
			if(e == null) {
				e = new EstateTableView(p);
				etvMap.put(p.estateName, e);
			}
			e.addPackage(p);
		}
		final TreeSet<EstateTableView> sortedEstates = new TreeSet<EstateTableView>(etvMap.values());
		boolean addNewPage = false;

		List<Future<PdfReader>> readerList = new ArrayList<Future<PdfReader>>(size);
		for (final EstateTableView estate : sortedEstates) {
			readerList.add(ThreadManager.startCallable(new Callable<PdfReader>() {
				public PdfReader call() throws IOException, DocumentException {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					Document document = new com.itextpdf.text.Document();
					setMargins(document);
					document.setMarginMirroring(true);
					PdfWriter writer = PdfWriter.getInstance(document, baos);

					writer.setPageEvent(getFooter(request, null, estate.estateDisclaimer));
					document.open();
					try {
						estate.render(document);
					} catch (InterruptedException e) {
						logger.error("PDF Generation Interupted : {}" , e.getMessage());
					} catch (ExecutionException e) {
						logger.error("error in pdf execution - getting single package", e);
					}

					document.close();
					return new PdfReader(baos.toByteArray());
				}
			}));
		}
		document.open();
		logger.debug("Rendering pages");
		PdfImportedPage importedPage;
		PdfContentByte cb = writer.getDirectContent();
		for(Future<PdfReader> freader: readerList) {
			try {
				setStatus(request, "Rendering Pages", 0.6 + (( record++ / num) * 0.35));
				PdfReader reader = freader.get();
				int pages = reader.getNumberOfPages();
				for (int i = 1; i <= pages; i++) {
					 document.newPage();
					 importedPage = writer.getImportedPage(reader, i);
					 cb.addTemplate(importedPage, 0, 0);	
				}
			} catch (InterruptedException e) {
				logger.error("PDF Generation Interupted : {}" , e.getMessage());
			} catch (ExecutionException e) {
				logger.error("error in pdf execution", e);
			}
		}
	}
	
	private static class EstateTableView implements Comparable<EstateTableView> {
		private final String estateName;
		private final String estateDisclaimer;
		private final TreeSet<PackageTableView> packageTables = new TreeSet<PackageTableView>();
		private Future<PdfPTable> estateHeader;
		public EstateTableView(final Package p) {
			if (p.estateName == null || p.estateName.length() == 0) p.estateName = "- Missing -";
			estateName = p.estateName;
			estateDisclaimer = p.disclaimer;
			estateHeader = ThreadManager.startCallable(new Callable<PdfPTable>() {
				public PdfPTable call() throws DocumentException {
					return PDFUtil.getHeaderForLandPropertyListPdf(getEstate(p));
				}
				private Estate getEstate(Package p) {
					Estate e = new PDFUtil.Estate();
					e.estateName = p.estateName;
					// 16-Nov-2012 NI: e.estateLogo = PropertyEntity.resizeImage(p.estateLogo, 140, 65, PropertyEntity.ResizeOption.Fit);
					e.estateLogo = p.estateLogo;
					return e;
				}
			});
		}
		public void addPackage(final Package p) throws DocumentException {
			packageTables.add(new PackageTableView(p));
		}
		
		public void render(Document document) throws DocumentException, ExecutionException, InterruptedException { 
			final PdfPTable hdrTable = estateHeader.get();
			PdfPTable pageTable = new PdfPTable(1);
			pageTable.getDefaultCell().setBorder(0);
			pageTable.getDefaultCell().setPadding(0);
			pageTable.getDefaultCell().setFixedHeight(0f);
			pageTable.setWidthPercentage(100f);

			pageTable.addCell(hdrTable);
			pageTable.getDefaultCell().setFixedHeight(11f);
			pageTable.addCell(BLANK);
			
			int i=0;
			for(PackageTableView ptv: packageTables) {
				if (i != 0 && i % 6 == 0) {
					document.add(pageTable);
					document.newPage();
					pageTable = new PdfPTable(1);
					pageTable.getDefaultCell().setBorder(0);
					pageTable.getDefaultCell().setPadding(0);
					pageTable.getDefaultCell().setFixedHeight(0f);
					pageTable.setWidthPercentage(100f);

					pageTable.addCell(hdrTable);
					pageTable.getDefaultCell().setFixedHeight(11f);
					pageTable.addCell(BLANK);
				}
				pageTable.getDefaultCell().setFixedHeight(0f);
				pageTable.getDefaultCell().setBorderWidthTop(0.01f);
				pageTable.getDefaultCell().setBorderWidthBottom(0.01f);
				pageTable.getDefaultCell().setBorderColor(BaseColor.BLACK);
				pageTable.addCell(ptv.table.get());
				pageTable.getDefaultCell().setBorder(0);
				pageTable.getDefaultCell().setFixedHeight(6f);
				pageTable.addCell(BLANK);
				i++;
			}
			document.add(pageTable);
		}
		@Override
		public int compareTo(EstateTableView arg1) {
			return this.estateName.compareTo(arg1.estateName);
		}
	}
	
	private static class PackageTableView implements Comparable<PackageTableView> {
		public final String name;
		public final Future<PdfPTable> table;
		public PackageTableView(final Package p) throws DocumentException {
			this.name = p.name  + p.lotNo;
			this.table = ThreadManager.startCallable(new Callable<PdfPTable>() {
				public PdfPTable call() throws DocumentException {
					return PDFUtil.buildPackageTableForLandPropertyListPdf(p);
				}
			});
		}
		@Override
		public int compareTo(PackageTableView arg0) {
			return this.name.compareTo(arg0.name);
		}
	}
	
	public String getPdfSimpleName() {
		return "Land Property List";
	}
	
}