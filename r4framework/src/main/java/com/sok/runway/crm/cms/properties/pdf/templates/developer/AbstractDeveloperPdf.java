package com.sok.runway.crm.cms.properties.pdf.templates.developer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;

public abstract class AbstractDeveloperPdf extends AbstractPDFTemplate {

	/** override this for a single package developer pdf
	 * @param request
	 * @param document
	 * @param property
	 * @throws DocumentException
	 */
	public void renderPdf(HttpServletRequest request, Document document, PropertyEntity property) throws DocumentException {
	};
	
	
	/** for future use: override this for a list developer pdf
	 * @param request
	 * @param document
	 * @param property
	 * @throws DocumentException
	 */
	public void renderPdf(HttpServletRequest request, Document document, List<PropertyEntity> properties) throws DocumentException {
	};

	
	@Override
	// Unused
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {

	}
	
	/**
	 * This method will be implemented only if the implementing class needs to add overlays to 
	 * pdf page. Override this method in the implementation class and setOverlay method gets
	 * called from within {@link DeveloperPdfGenerator}/invokeDeveloperPdfTemplate() method.
	 * 
	 * @param request
	 * @param baos
	 * @param property
	 */
	public void setOverlay(HttpServletRequest request, ByteArrayOutputStream baos, PropertyEntity property){}
	

}