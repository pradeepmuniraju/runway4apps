/**
 * 
 */
package com.sok.runway.crm.cms.properties.pdf;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.sog.pdfserver.client.api.request.CompanyInfoRequest;
import com.sog.pdfserver.client.api.request.PDFRequest;
import com.sog.pdfserver.client.api.response.CompanyInfoResponse;
import com.sog.pdfserver.client.api.response.PdfCategory;
import com.sog.pdfserver.client.api.response.TemplateGroup;
import com.sog.pdfserver.client.api.response.TemplateInfo;
import com.sog.pdfserver.client.api.service.CompanyService;
import com.sog.pdfserver.client.api.service.ConsumerFactory;
import com.sok.framework.ActionBean;
import com.sok.framework.Conditions;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.RunwayUtil;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.servlet.ResponseOutputStream;
import com.sok.runway.URLFilter;
import com.sok.runway.crm.Product;
import com.sok.runway.crm.cms.properties.Basic;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.interfaces.AsyncProcessStatus;
import com.sok.runway.offline.ThreadManager;

/**
 * @author Dion Chapman
 *
 */
public class AdvancedPDFServlet extends HttpServlet implements AsyncProcessStatus {
	private static final Logger logger = LoggerFactory.getLogger(AdvancedPDFServlet.class);

	private String host = "";
	private String company = "";
	private String developer = "";
	/**
	 * @throws Exception
	 * 
	 * 
	 */
	public AdvancedPDFServlet() {
		host = InitServlet.getSystemParam("PDF-AdvancedServerHost");
		company = InitServlet.getSystemParam("PDF-AdvancedServerCompanyName");
		developer = InitServlet.getSystemParam("PDF-AdvancedServerDeveloperName");
	}

	public void setStatus(HttpServletRequest request, String status) {
		setStatus(request, status, -1);
	}

	public void setStatus(HttpServletRequest request, String status, double fraction) {
		logger.debug("setStatus(request, {})", status);
		String statusKey = (String) request.getAttribute("-statusKey");
		if (statusKey == null) {
			logger.debug("no status key from request");
			String fileKey = (String) request.getSession().getAttribute("-statusKey");
			if (fileKey != null) {
				logger.debug("status key from session {} ", fileKey);
				statusKey = fileKey;
			} else {
				logger.error("no status key");
				return;
			}
			request.setAttribute("-statusKey", statusKey);
		}
		logger.debug("using status key {}", statusKey);
		request.getSession().setAttribute(statusKey, makeStatusDiv(status, fraction));
	}

	private String makeStatusDiv(String status, double fraction) {
		if (fraction == -1) {
			return status;
		}
		return new StringBuilder().append(status).append("<div style=\"width:100%; display:block; height: 15px\"><div style=\"width: ").append(fraction * 100).append("%; background-color: red\">&nbsp;</div></div>").toString();
	}
	/* */

	public JSONObject getPDFTemplates(String type) throws Exception {
		return getPDFTemplates(type, new ArrayList<String>(), false);
	}
	public JSONObject getPDFTemplates(String type, ArrayList<String> products) throws Exception {
		return getPDFTemplates(type, products, false);
	}
	public JSONObject getPDFTemplates(String type, String opportunityID, String viewStageID) throws Exception {
		return getPDFTemplates(type, opportunityID, viewStageID, false);
	}
	public JSONObject getPDFTemplates(String type, boolean showInvisible) throws Exception {
		return getPDFTemplates(type, new ArrayList<String>(), showInvisible);
	}
	public JSONObject getPDFTemplates(String type, ArrayList<String> products, boolean showInvisible) throws Exception {
		host = InitServlet.getSystemParam("PDF-AdvancedServerHost");
		company = InitServlet.getSystemParam("PDF-AdvancedServerCompanyName");
		developer = InitServlet.getSystemParam("PDF-AdvancedServerDeveloperName");

		if (StringUtils.isBlank(host)) {
			throw new Exception("PDF-AdvanceServerHost is blank, please configure the address for the PDF server");
		}
		//if (StringUtils.isBlank(company)) {
		//	throw new Exception("PDF-AdvanceServerCompanyName is blank, please configure the company for the PDF server");
		//}

		JSONObject json = new JSONObject();

		JSONArray ary = new JSONArray();

		ConsumerFactory cf = ConsumerFactory.getFactory(host);
		CompanyService cs = cf.getCompanyService();

		HashSet<String> set = new HashSet<String>();
		if (StringUtils.isNotBlank(type)) {
			String[] types = type.split(" ");

			if (types.length == 0)
				types = type.split("\\+");

			for (int t = 0; t < types.length; ++t)
				set.add(types[t].toLowerCase());
		}

		if (StringUtils.isNotBlank(company)) {
			CompanyInfoRequest ciRequest = new CompanyInfoRequest(company);

			CompanyInfoResponse ciResponse = cs.getCompanyInfo(ciRequest);

			if (ciResponse != null) {
				List<PdfCategory> categories = ciResponse.getCategoryList();

				for (int x = 0; x < categories.size(); ++x) {
					PdfCategory pdfc = categories.get(x);
					if (set.size() == 0 || set.contains(StringUtils.deleteWhitespace(pdfc.getProduct().toLowerCase()))
							|| set.contains(StringUtils.deleteWhitespace(pdfc.getProduct().toLowerCase() + pdfc.getGroup().toLowerCase())) ) {
						List<TemplateGroup> groups = pdfc.getTemplateGroups();
						for (int y = 0; y < groups.size(); ++y) {
							TemplateGroup template = groups.get(y);
							JSONObject obj = new JSONObject();
							obj.put("Name", template.getName());
							if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
								obj.put("ID", template.getId());
							else
								obj.put("ID", template.getName());
							obj.put("CategoryID", pdfc.getId());
							obj.put("SortType", "");
							obj.put("Group", pdfc.getGroup());
							obj.put("Product", pdfc.getProduct());
							obj.put("Default", template.isDefaultGroup());
							if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
								obj.put("Thumbnail", template.getThumbnail());
							else
								obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
							if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
								obj.put("Description", template.getDescription());
							else
								obj.put("Description", "");

							obj.put("Conditions", "");
							obj.put("MultiTemplate", "true");
							obj.put("Company", company);

							ary.add(obj);
						}
						List<TemplateInfo> templates = pdfc.getTemplates();
						for (int y = 0; y < templates.size(); ++y) {
							TemplateInfo template = templates.get(y);
							if (!template.isInvisibleTemplate() || showInvisible) {
								String conditions = template.getSearchMetaText();
								if (products != null && products.size() > 0 && StringUtils.isNotBlank(conditions) && !"null".equalsIgnoreCase(conditions)) {
									for (int p = 0; p < Math.min(products.size(), 2); ++p) {
										Conditions cond = new Conditions(products.get(p));
										if (cond.evaluate(conditions)) {
											JSONObject obj = new JSONObject();
											obj.put("Name", template.getName());
											if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
												obj.put("ID", template.getId());
											else
												obj.put("ID", template.getName());
											obj.put("SortType", template.getProductSortType());
											obj.put("CategoryID", pdfc.getId());
											obj.put("Group", pdfc.getGroup());
											obj.put("Product", pdfc.getProduct());
											obj.put("Default", template.isDefaultTemplate());
											if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
												obj.put("Thumbnail", template.getThumbnail());
											else
												obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
											if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
												obj.put("Description", template.getDescription());
											else
												obj.put("Description", "");

											obj.put("Conditions", conditions);
											obj.put("MultiTemplate", "false");
											obj.put("Company", company);

											ary.add(obj);

											break;
										}
									}
								} else {
									JSONObject obj = new JSONObject();
									obj.put("Name", template.getName());
									if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
										obj.put("ID", template.getId());
									else
										obj.put("ID", template.getName());
									obj.put("SortType", template.getProductSortType());
									obj.put("CategoryID", pdfc.getId());
									obj.put("Group", pdfc.getGroup());
									obj.put("Product", pdfc.getProduct());
									obj.put("Default", template.isDefaultTemplate());
									if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
										obj.put("Thumbnail", template.getThumbnail());
									else
										obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
									if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
										obj.put("Description", template.getDescription());
									else
										obj.put("Description", "");

									obj.put("Conditions", "");
									obj.put("MultiTemplate", "false");
									obj.put("Company", company);

									ary.add(obj);
								}
							}
						}
					}
				}
			}
		}

		if (StringUtils.isNotBlank(developer)) {
			CompanyInfoRequest ciRequest = new CompanyInfoRequest(developer);

			CompanyInfoResponse ciResponse = cs.getCompanyInfo(ciRequest);

			if (ciResponse != null) {
				List<PdfCategory> categories = ciResponse.getCategoryList();

				for (int x = 0; x < categories.size(); ++x) {
					PdfCategory pdfc = categories.get(x);
					if (set.size() == 0 || set.contains(StringUtils.deleteWhitespace(pdfc.getProduct().toLowerCase()))) {
						List<TemplateGroup> groups = pdfc.getTemplateGroups();
						for (int y = 0; y < groups.size(); ++y) {
							TemplateGroup template = groups.get(y);
							JSONObject obj = new JSONObject();
							obj.put("Name", template.getName());
							if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
								obj.put("ID", template.getId());
							else
								obj.put("ID", template.getName());
							obj.put("CategoryID", pdfc.getId());
							obj.put("SortType", "");
							obj.put("Group", pdfc.getGroup());
							obj.put("Product", pdfc.getProduct());
							obj.put("Default", template.isDefaultGroup());
							if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
								obj.put("Thumbnail", template.getThumbnail());
							else
								obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
							if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
								obj.put("Description", template.getDescription());
							else
								obj.put("Description", "");

							obj.put("Conditions", "");
							obj.put("MultiTemplate", "true");
							obj.put("Company", developer);

							ary.add(obj);
						}
						List<TemplateInfo> templates = pdfc.getTemplates();
						for (int y = 0; y < templates.size(); ++y) {
							TemplateInfo template = templates.get(y);
							if (!template.isInvisibleTemplate() || (showInvisible && template.isInvisibleTemplate())) {
								String conditions = template.getSearchMetaText();
								if (products != null && products.size() > 0 && StringUtils.isNotBlank(conditions) && !"null".equalsIgnoreCase(conditions)) {
									for (int p = 0; p < Math.min(products.size(), 10); ++p) {
										Conditions cond = new Conditions(products.get(p));
										if (cond.evaluate(conditions)) {
											JSONObject obj = new JSONObject();
											obj.put("Name", template.getName());
											if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
												obj.put("ID", template.getId());
											else
												obj.put("ID", template.getName());
											obj.put("SortType", template.getProductSortType());
											obj.put("CategoryID", pdfc.getId());
											obj.put("Group", pdfc.getGroup());
											obj.put("Product", pdfc.getProduct());
											obj.put("Default", template.isDefaultTemplate());
											if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
												obj.put("Thumbnail", template.getThumbnail());
											else
												obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
											if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
												obj.put("Description", template.getDescription());
											else
												obj.put("Description", "");

											obj.put("Conditions", conditions);
											obj.put("MultiTemplate", "false");
											obj.put("Company", developer);

											ary.add(obj);

											break;
										}
									}
								} else {
									JSONObject obj = new JSONObject();
									obj.put("Name", template.getName());
									if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
										obj.put("ID", template.getId());
									else
										obj.put("ID", template.getName());
									obj.put("SortType", template.getProductSortType());
									obj.put("CategoryID", pdfc.getId());
									obj.put("Group", pdfc.getGroup());
									obj.put("Product", pdfc.getProduct());
									obj.put("Default", template.isDefaultTemplate());
									if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
										obj.put("Thumbnail", template.getThumbnail());
									else
										obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
									if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
										obj.put("Description", template.getDescription());
									else
										obj.put("Description", "");

									obj.put("Conditions", "");
									obj.put("MultiTemplate", "false");
									obj.put("Company", developer);

									ary.add(obj);
								}
							}
						}
					}
				}
			}
		}

		json.put("Templates", ary);

		// System.out.println(json.toJSONString());

		return json;

	}

	public JSONObject getPDFTemplates(String type, String opportunityID, String viewStageID, boolean showInvisible) throws Exception {
		host = InitServlet.getSystemParam("PDF-AdvancedServerHost");
		company = InitServlet.getSystemParam("PDF-AdvancedServerCompanyName");
		developer = InitServlet.getSystemParam("PDF-AdvancedServerDeveloperName");

		if (StringUtils.isBlank(host)) {
			throw new Exception("PDF-AdvanceServerHost is blank, please configure the address for the PDF server");
		}
		//if (StringUtils.isBlank(company)) {
		//	throw new Exception("PDF-AdvanceServerCompanyName is blank, please configure the company for the PDF server");
		//}

		JSONObject json = new JSONObject();

		JSONArray ary = new JSONArray();

		ConsumerFactory cf = ConsumerFactory.getFactory(host);
		CompanyService cs = cf.getCompanyService();

		HashSet<String> set = new HashSet<String>();
		if (StringUtils.isNotBlank(type)) {
			String[] types = type.split(" ");

			if (types.length == 0)
				types = type.split("\\+");

			for (int t = 0; t < types.length; ++t)
				set.add(types[t].toLowerCase());
		}

		Conditions cond = null;

		if (StringUtils.isNotBlank(company)) {
			CompanyInfoRequest ciRequest = new CompanyInfoRequest(company);

			CompanyInfoResponse ciResponse = cs.getCompanyInfo(ciRequest);

			if (ciResponse != null) {
				List<PdfCategory> categories = ciResponse.getCategoryList();

				for (int x = 0; x < categories.size(); ++x) {
					PdfCategory pdfc = categories.get(x);
					if (set.size() == 0 || set.contains(StringUtils.deleteWhitespace(pdfc.getProduct().toLowerCase()))
							|| set.contains(StringUtils.deleteWhitespace(pdfc.getProduct().toLowerCase() + pdfc.getGroup().toLowerCase())) ) {
						List<TemplateGroup> groups = pdfc.getTemplateGroups();
						for (int y = 0; y < groups.size(); ++y) {
							TemplateGroup template = groups.get(y);
							JSONObject obj = new JSONObject();
							obj.put("Name", template.getName());
							if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
								obj.put("ID", template.getId());
							else
								obj.put("ID", template.getName());
							obj.put("CategoryID", pdfc.getId());
							obj.put("SortType", "");
							obj.put("Group", pdfc.getGroup());
							obj.put("Product", pdfc.getProduct());
							obj.put("Default", template.isDefaultGroup());
							if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
								obj.put("Thumbnail", template.getThumbnail());
							else
								obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
							if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
								obj.put("Description", template.getDescription());
							else
								obj.put("Description", "");

							obj.put("Conditions", "");
							obj.put("MultiTemplate", "true");
							obj.put("Company", company);

							ary.add(obj);
						}
						List<TemplateInfo> templates = pdfc.getTemplates();
						for (int y = 0; y < templates.size(); ++y) {
							TemplateInfo template = templates.get(y);
							if (!template.isInvisibleTemplate() || showInvisible) {
								String conditions = template.getSearchMetaText();
								if (StringUtils.isNotBlank(opportunityID) && StringUtils.isNotBlank(conditions) && !"null".equalsIgnoreCase(conditions)) {
									if (cond == null) {
										String[] ids = getOpportunityIDs(opportunityID);
										cond = new Conditions(ids[0], ids[1], ids[2], viewStageID);
									}
									if (cond.evaluate(conditions)) {
										JSONObject obj = new JSONObject();
										obj.put("Name", template.getName());
										if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
											obj.put("ID", template.getId());
										else
											obj.put("ID", template.getName());
										obj.put("SortType", template.getProductSortType());
										obj.put("CategoryID", pdfc.getId());
										obj.put("Group", pdfc.getGroup());
										obj.put("Product", pdfc.getProduct());
										obj.put("Default", template.isDefaultTemplate());
										if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
											obj.put("Thumbnail", template.getThumbnail());
										else
											obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
										if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
											obj.put("Description", template.getDescription());
										else
											obj.put("Description", "");

										obj.put("Conditions", conditions);
										obj.put("MultiTemplate", "false");
										obj.put("Company", company);

										ary.add(obj);
									}
								} else {
									JSONObject obj = new JSONObject();
									obj.put("Name", template.getName());
									if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
										obj.put("ID", template.getId());
									else
										obj.put("ID", template.getName());
									obj.put("SortType", template.getProductSortType());
									obj.put("CategoryID", pdfc.getId());
									obj.put("Group", pdfc.getGroup());
									obj.put("Product", pdfc.getProduct());
									obj.put("Default", template.isDefaultTemplate());
									if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
										obj.put("Thumbnail", template.getThumbnail());
									else
										obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
									if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
										obj.put("Description", template.getDescription());
									else
										obj.put("Description", "");

									obj.put("Conditions", "");
									obj.put("MultiTemplate", "false");
									obj.put("Company", company);

									ary.add(obj);
								}
							}
						}
					}
				}
			}
		}

		if (StringUtils.isNotBlank(developer)) {
			CompanyInfoRequest ciRequest = new CompanyInfoRequest(developer);

			CompanyInfoResponse ciResponse = cs.getCompanyInfo(ciRequest);

			if (ciResponse != null) {
				List<PdfCategory> categories = ciResponse.getCategoryList();

				for (int x = 0; x < categories.size(); ++x) {
					PdfCategory pdfc = categories.get(x);
					if (set.size() == 0 || set.contains(StringUtils.deleteWhitespace(pdfc.getProduct().toLowerCase()))) {
						List<TemplateGroup> groups = pdfc.getTemplateGroups();
						for (int y = 0; y < groups.size(); ++y) {
							TemplateGroup template = groups.get(y);
							JSONObject obj = new JSONObject();
							obj.put("Name", template.getName());
							if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
								obj.put("ID", template.getId());
							else
								obj.put("ID", template.getName());
							obj.put("CategoryID", pdfc.getId());
							obj.put("SortType", "");
							obj.put("Group", pdfc.getGroup());
							obj.put("Product", pdfc.getProduct());
							obj.put("Default", template.isDefaultGroup());
							if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
								obj.put("Thumbnail", template.getThumbnail());
							else
								obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
							if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
								obj.put("Description", template.getDescription());
							else
								obj.put("Description", "");

							obj.put("Conditions", "");
							obj.put("MultiTemplate", "true");
							obj.put("Company", developer);

							ary.add(obj);
						}
						List<TemplateInfo> templates = pdfc.getTemplates();
						for (int y = 0; y < templates.size(); ++y) {
							TemplateInfo template = templates.get(y);
							if (!template.isInvisibleTemplate() || (showInvisible && template.isInvisibleTemplate())) {
								String conditions = template.getSearchMetaText();
								if (StringUtils.isNotBlank(opportunityID) && StringUtils.isNotBlank(conditions) && !"null".equalsIgnoreCase(conditions)) {
									if (cond == null) {
										String[] ids = getOpportunityIDs(opportunityID);
										cond = new Conditions(ids[0], ids[1], ids[2], viewStageID);
									}
									if (cond.evaluate(conditions)) {
										JSONObject obj = new JSONObject();
										obj.put("Name", template.getName());
										if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
											obj.put("ID", template.getId());
										else
											obj.put("ID", template.getName());
										obj.put("SortType", template.getProductSortType());
										obj.put("CategoryID", pdfc.getId());
										obj.put("Group", pdfc.getGroup());
										obj.put("Product", pdfc.getProduct());
										obj.put("Default", template.isDefaultTemplate());
										if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
											obj.put("Thumbnail", template.getThumbnail());
										else
											obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
										if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
											obj.put("Description", template.getDescription());
										else
											obj.put("Description", "");

										obj.put("Conditions", conditions);
										obj.put("MultiTemplate", "false");
										obj.put("Company", developer);

										ary.add(obj);
									}
								} else {
									JSONObject obj = new JSONObject();
									obj.put("Name", template.getName());
									if (StringUtils.isNotBlank(template.getId()) && !"null".equals(template.getId()))
										obj.put("ID", template.getId());
									else
										obj.put("ID", template.getName());
									obj.put("SortType", template.getProductSortType());
									obj.put("CategoryID", pdfc.getId());
									obj.put("Group", pdfc.getGroup());
									obj.put("Product", pdfc.getProduct());
									obj.put("Default", template.isDefaultTemplate());
									if (StringUtils.isNotBlank(template.getThumbnail()) && !"null".equals(template.getThumbnail()))
										obj.put("Thumbnail", template.getThumbnail());
									else
										obj.put("Thumbnail", "/crm/cms/templates/images_cms/no_preview_thumb.jpg");
									if (StringUtils.isNotBlank(template.getDescription()) && !"null".equals(template.getDescription()))
										obj.put("Description", template.getDescription());
									else
										obj.put("Description", "");

									obj.put("Conditions", "");
									obj.put("MultiTemplate", "false");
									obj.put("Company", developer);

									ary.add(obj);
								}
							}
						}
					}
				}
			}
		}

		json.put("Templates", ary);

		// System.out.println(json.toJSONString());

		return json;

	}

	private String[] getOpportunityIDs(String opportunityID) {
		if (StringUtils.isBlank(opportunityID)) return new String[3];

		GenRow row = new GenRow();
		row.setTableSpec("Opportunities");
		row.setParameter("-select0", "DefaultOrderID");
		row.setParameter("-select1", "ReferenceProductID");
		row.setParameter("OpportunityID", opportunityID);

		row.doAction("selectfirst");

		return new String[]{row.getString("ReferenceProductID"), row.getString("DefaultOrderID"), opportunityID};
	}

	/**
	 * @param host
	 *            the host to set for the PDF Server
	 */
	public void setHost(String host) {
		this.host = host;
	}

	public void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {

			String classAndMethodName = this.getClass() + " processRequest(HttpServletRequest request, HttpServletResponse response) ";
			logger.debug(classAndMethodName + " Started.");

			final String tempFileKey = StringUtils.trimToNull(request.getParameter("fileKey"));

			if (tempFileKey != null) {
				logger.debug(classAndMethodName + " tempFileKey is: " + tempFileKey);

				File f = (File) request.getSession().getAttribute(tempFileKey);
				if (f != null && f.exists() && f.length() > 0) {
					String contentFormat = FilenameUtils.isExtension(f.getName(), new String[]{"xls, xlsx"}) ? "application/vnd.ms-excel" : "application/pdf";
					FileInputStream fis = new FileInputStream(f);
					response.setContentType(contentFormat);

					DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
					Date date = new Date();
					String strDate = dateFormat.format(date);
					response.setHeader("Content-Type", contentFormat);
					response.setHeader("Content-Disposition", "attachment; filename=\"" + "PropertyDetails_" + strDate + "." + FilenameUtils.getExtension(f.getName()) + "\"");

					org.apache.commons.io.IOUtils.copy(fis, response.getOutputStream());
					fis.close();
					return;
				} else {
					if (f != null)
						System.out.println("PDF: " + f.getAbsolutePath() + " " + f.exists() + " " + f.length());
					throw new ServletException("The file could not be found, perhaps too much time has elapsed since it was generated");
				}
			}

			logger.debug(classAndMethodName + " calling setStatus(request, Starting up, 0.01)");
			setStatus(request, "Starting up", 0.01);
			/* the variant of the report, or will default to the legacy implementation */
			/* list key relates to a session attribute containing a String[] of ids */
			final String listKey = StringUtils.trimToNull(request.getParameter("-listKey"));
			/* search key relates to a session attribute containing a GenRow */
			final String searchKey = StringUtils.trimToNull(request.getParameter("-searchKey"));
			final String productID = StringUtils.trimToNull(request.getParameter("ProductID"));

			List<PropertyEntity> list = Collections.emptyList();

			// System.out.println("PropertyPDFServlet.java ==> variant.getVariant is: " + variant.getVariant() + " , variant is: " + variant + "  , isWebsiteRequest: " + isWebsiteRequest);

			if (listKey != null) {
				Object o = request.getSession().getAttribute(listKey);
				if (o instanceof String[]) {
					try {
						list = PropertyFactory.getBasicEntities(request, (String[]) o, this);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}

			/*
			 * System.out.println("loading from request parameter values "); for(String s: request.getParameterValues("ProductID")) { System.out.println(s); }
			 */
			if ((list == null || list.size() == 0)) {
				if ("-1".equals(productID) && StringUtils.isNotBlank(request.getParameter("EstateID"))) {
					logger.debug(classAndMethodName + " list is null or blank. calling PropertyFactory.getBasicEntitiesForEstate(request, this). EstateID is: " + request.getParameter("EstateID"));

					list = PropertyFactory.getBasicEntitiesForEstate(request, this);

					ArrayList<String> properties = new ArrayList<String>();

					for (int x = 0; x < Math.min(list.size(), 5); ++x) {
						properties.add(list.get(x).getString("ProductID"));
					}

					JSONObject json = getPDFTemplates(request.getParameter("-product") + request.getParameter("-group"), properties, true);

					if (json != null && json.containsKey("Templates")) {
						JSONArray arry = (JSONArray) json.get("Templates");

						if (arry.size() == 1 && ((JSONObject) arry.get(0)).containsKey("ID") && ((JSONObject) arry.get(0)).containsKey("CategoryID")) {
							request.setAttribute("-variant", (String) ((JSONObject) arry.get(0)).get("ID")); 
							request.setAttribute("-category", (String) ((JSONObject) arry.get(0)).get("CategoryID")); 
						}
					}
				} else if (productID != null) {
					logger.debug(classAndMethodName + " list is null or blank. calling PropertyFactory.getPropertyEntities(request, this). ProductID is: " + productID);

					list = PropertyFactory.getBasicEntities(request, this);
				}
			}
			if ((list == null || list.size() == 0) && searchKey != null) {

				logger.debug(classAndMethodName + " list is null or blank. searchKey is: " + searchKey);

				GenRow search = (GenRow) request.getSession().getAttribute(searchKey);
				search.setRequest(request);
				search.setParameter("-select1", "Products.ProductID as altID");
				search.setParameter("-groupby1", "Products.ProductID");
				search.doAction(GenerationKeys.SEARCH);
				search.getResults();
				if (search.getNext()) {
					list = new ArrayList<PropertyEntity>();
					do {
						list.add(new Basic(request, search.getData("ProductID")));
					} while (search.getNext());
				}
				search.close();
			}

			if ((list == null || list.size() == 0) && StringUtils.isBlank(request.getParameter("OpportunityID")) && StringUtils.isBlank(request.getParameter("TransactionID"))) {
				throw new UnsupportedOperationException("Products or Oppurtunity must be supplied to generate PDF");
			}
			logger.error("URI " + request.getRequestURI());

			if (request.getRequestURI().indexOf("apropertypdf.zip") == -1 && request.getRequestURI().indexOf("propertypdf.zip") != -1) {

				logger.debug(classAndMethodName + " Request for the propertypdf.zip");

				final ZipOutputStream out = new ZipOutputStream(response.getOutputStream());
				try {
					logger.debug("doing zip variant");
					Collections.sort(list, new Comparator<PropertyEntity>() {
						public int compare(PropertyEntity p1, PropertyEntity p2) {
							return StringUtils.trimToEmpty(p1.getEntityEstateName()).compareTo(StringUtils.trimToEmpty(p2.getEntityEstateName()));
						}
					});
					List<Future<PDFRecord>> output = new ArrayList<Future<PDFRecord>>(list.size());

					ExecutorService executor = null;
					StringBuilder url = new StringBuilder().append("/property.pdf?");
					for (String key : new String[]{"-variant", "-category", "-group", "-product", "-sorttype", "-company", "-deliverymethod", "-deliverymethod", "-pdfLocation", "-pdfRep", "-variantname", "GroupID", "RepUserID", "DisplayID", "useAllocatedRep", "EstateID", "isPortalRequest", "ContactID"}) {
						String val = StringUtils.trimToNull(request.getParameter(key));
						if (val != null) {
							url.append(key).append("=").append(StringUtil.urlEncode(val)).append("&");
						}
					}
					url.append("ProductID=");
					final String urlBase = url.toString();
					final HttpSession session = request.getSession();
					try {
						int threads = 3;
						try {
							threads = Integer.parseInt(InitServlet.getSystemParam("PropertyPDFThreads", String.valueOf(threads)));
						} catch (NumberFormatException nfe) {
						}
						executor = ThreadManager.createThreadExecutor("PropertyPDFServlet:createZip", threads);

						final boolean singlePdf = !"true".equals(request.getParameter("-estateFolders"));

						for (final PropertyEntity property : list) {
							output.add(executor.submit(new Callable<PDFRecord>() {
								public PDFRecord call() {
									PropertyEntity pe = PropertyFactory.getPropertyEntity(property.getConnection(), property.getString("ProductID"));
									String estate = StringUtils.trimToNull(pe.getEntityEstateName());
									StringBuilder file = new StringBuilder();
									if (!singlePdf) {
										if (estate != null) {
											file.append(StringUtil.removeNonFileSystemCharacters(estate).toLowerCase()).append("/");
										}
										file.append(StringUtil.removeNonFileSystemCharacters(pe.getString("Name")).toLowerCase()).append("_").append(property.getString("ProductNum")).append("_").append(KeyMaker.generate(5)).append(".pdf");
									}
									String url = urlBase + pe.getData("ProductID");
									logger.debug("creating zip entry {} from url {}", file.toString(), url);
									try {
										ByteArrayOutputStream baos = new ByteArrayOutputStream();
										try {
											ResponseOutputStream ros = new ResponseOutputStream(session, baos, url);
											ros.service(pe.getConnection());
										} finally {
											baos.flush();
											baos.close();
										}
										return new PDFRecord(baos.toByteArray(), file.toString());
									} catch (Exception re) {
										logger.error("error creating zip entry from url", re);
										throw new RuntimeException(re);
									}
								}
							}));
						}

						// This looks a bit convoluted, but must be done this way so as to pass the doc / cb variables into the actions as they must be final.
						if (singlePdf) {
							out.putNextEntry(new ZipEntry("property.pdf"));
						}
						final Document doc = singlePdf ? new Document(PageSize.A4) : null;
						final PdfWriter writer = doc != null ? getWriter(doc, out) : null;
						if (doc != null)
							doc.open();
						final PdfContentByte cb = writer != null ? writer.getDirectContent() : null;

						Action<PDFRecord> action = singlePdf ? new Action<PDFRecord>() {
							public void act(PDFRecord rec) throws IOException { // add pdf to single doc
								// Load existing PDF
								PdfReader reader = new PdfReader(rec.bytes);
								for (int i = 1; i <= reader.getNumberOfPages(); i++) {
									PdfImportedPage page = writer.getImportedPage(reader, i);
									doc.newPage();
									cb.addTemplate(page, 0, 0);
								}

							}
						} : new Action<PDFRecord>() {
							public void act(PDFRecord rec) throws IOException { // add pdf zip in estate folder
								if (rec != null) {
									out.putNextEntry(new ZipEntry(rec.fileName));
									out.write(rec.bytes);
								}
							}
						};

						for (Future<PDFRecord> rec : output) {
							PDFRecord tRec = null;
							try {
								tRec = rec.get();
							} catch (InterruptedException e) {
								logger.info("Retrieving PDF Interupted");
							} catch (ExecutionException e) {
								logger.error("Error producing package pdf", e);
							}
							action.act(tRec);
						}
						if (doc != null) {
							doc.close();
						}
					} finally {
						if (executor != null) {
							ThreadManager.shutdownExecutor(executor);
						}
					}
				} finally {
					out.close();
				}
				// if (!response.containsHeader("Content-Disposition")) {
				response.setHeader("Content-Disposition", "attachment; filename=\"propertypdf.zip\"");
				// }
			} else { // else if not zip

				/*
				 * if (!response.containsHeader("Content-Disposition")) { response.setHeader("Content-Disposition", "attachment; filename=\"property.pdf\""); }
				 */
				//
				// if (variant.equals(PDFVariant.ExcelPackageReport) || variant.equals(PDFVariant.ExcelApartmentReport) || variant.equals(PDFVariant.ExcelRealEstateReport) || variant.equals(PDFVariant.ExcelLandReport)) {
				//
				// logger.debug(classAndMethodName + " Before renderExcel(variant, list, request, response)");
				// // renderExcel(variant, list, request, response);
				// logger.debug(classAndMethodName + " After renderExcel(variant, list, request, response)");
				//
				// } else {
				//
				// logger.debug(classAndMethodName + " Before renderPDF(variant, list, request, response)");
				// renderPDF(variant, list, request, response);
				// logger.debug(classAndMethodName + " After renderPDF(variant, list, request, response)");
				// }
				renderPDF(list, request, response);
			}
		} catch (RuntimeException re) {
			renderError(re, request, response);
			re.printStackTrace();
		} catch (Exception e) {
			renderError(e, request, response);
			e.printStackTrace();
		}
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	public void renderError(Exception e, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String classAndMethodName = this.getClass() + " renderError() ";
		logger.debug(classAndMethodName + " Started.");

		com.itextpdf.text.Document document = new com.itextpdf.text.Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
			document.open();
			Paragraph error = new Paragraph();
			error.add(new Chunk(ActionBean.writeStackTraceToString(e), FontFactory.getFont(FontFactory.HELVETICA, 8)));
			document.add(error);
			document.close();
			writer.close();
		} catch (Exception ex) {
			ActionBean.writeStackTrace(ex, System.out);
			response.sendRedirect("/error.html");
		}

		logger.debug(classAndMethodName + " Ended.");
	}

	private class PDFRecord {
		public byte[] bytes;
		public String fileName;
		public PDFRecord(byte[] bytes, String fileName) {
			this.bytes = bytes;
			this.fileName = fileName;
		}
	}

	/* Essentially a hack as java doesn't do single line try-catch with assignment */
	private PdfWriter getWriter(Document doc, OutputStream out) {
		try {
			return PdfWriter.getInstance(doc, out);
		} catch (DocumentException de) {
			throw new RuntimeException("Error getting writer", de);
		}
	}

	private interface Action<T> {
		public void act(T rec) throws IOException;
	}

	protected void renderPDF(List<PropertyEntity> list, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String classAndMethodName = this.getClass() + " renderPDF(PDFVariant variant, List<PropertyEntity> list, HttpServletRequest request, HttpServletResponse response) ";
		logger.debug(classAndMethodName + " Started.");

		request.setAttribute("entityList", list);
		request.setAttribute("servletConfig", getServletConfig());// This to work with Jasper

		logger.debug(classAndMethodName + " Before calling PropertyPDFGenerator.generate(template, request, response)");

		try {
			PDFRequest pdfRequest = new PDFRequest();
			pdfRequest.setPdfGenMode("SYNC");
			pdfRequest.setCompanyName(company);
			if (StringUtils.isNotBlank(request.getParameter("-company")) && !"null".equals(request.getParameter("-company"))) {
				pdfRequest.setCompanyName(request.getParameter("-company"));
			}

			if (StringUtils.isNotBlank(request.getParameter("-variant")) && !"null".equals(request.getParameter("-variant"))) {
				pdfRequest.setTemplateId(request.getParameter("-variant"));
			} else if (StringUtils.isNotBlank((String) request.getAttribute("-variant")) && !"null".equals((String) request.getAttribute("-variant"))) {
				pdfRequest.setTemplateId((String) request.getAttribute("-variant"));
			}

			if (StringUtils.isNotBlank(request.getParameter("-variantname")) && !"null".equals(request.getParameter("-variantname"))) {
				pdfRequest.setTemplateName(request.getParameter("-variantname")); // For future enhancements
			}

			if (StringUtils.isNotBlank(request.getParameter("-category")) && !"null".equals(request.getParameter("-category"))) {
				pdfRequest.setPdfCategoryId(request.getParameter("-category")); // For future enhancements
			} else if (StringUtils.isNotBlank((String) request.getAttribute("-category")) && !"null".equals((String) request.getAttribute("-category"))) {
				pdfRequest.setPdfCategoryId((String) request.getAttribute("-category")); // For future enhancements
			} 
			if (StringUtils.isNotBlank(request.getParameter("-multitemplate")) && !"null".equals(request.getParameter("-multitemplate"))) {
				if ("true".equalsIgnoreCase(request.getParameter("-multitemplate"))) pdfRequest.setCompanyId(pdfRequest.getTemplateId()); // For future enhancements
			}
			pdfRequest.setExternalRequestId(KeyMaker.generate());

			AdvancedPropertyPDFGenerator.generate(request, response, pdfRequest);
			//setStatus(request, "Complete", 1);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error ");
		}

		logger.debug(classAndMethodName + " After calling PropertyPDFGenerator.generate(template, request, response)");

		logger.debug(classAndMethodName + " Ended.");
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		host = InitServlet.getSystemParam("PDF-AdvancedServerHost");
		company = InitServlet.getSystemParam("PDF-AdvancedServerCompanyName");
		developer = InitServlet.getSystemParam("PDF-AdvancedServerDeveloperName");

		URLFilter.exceptionLogger(this, request, null);

		String classAndMethodName = this.getClass() + " doGet(HttpServletRequest request, HttpServletResponse response) ";
		logger.debug(classAndMethodName + " Before Calling processRequest(request, response).");

		processRequest(request, response);

		logger.debug(classAndMethodName + " After Calling processRequest(request, response).");
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		host = InitServlet.getSystemParam("PDF-AdvancedServerHost");
		company = InitServlet.getSystemParam("PDF-AdvancedServerCompanyName");
		developer = InitServlet.getSystemParam("PDF-AdvancedServerDeveloperName");

		URLFilter.exceptionLogger(this, request, null);

		String classAndMethodName = this.getClass() + " doPost(HttpServletRequest request, HttpServletResponse response) ";
		logger.debug(classAndMethodName + " Before Calling processRequest(request, response).");

		processRequest(request, response);

		logger.debug(classAndMethodName + " After Calling processRequest(request, response).");
	}
}