package com.sok.runway.crm.cms.properties.excel.templates;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.RunwayUtil;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.excel.AbstractExcelTemplate;
import com.sok.runway.crm.cms.properties.excel.templates.model.Building;
import com.sok.runway.crm.cms.properties.excel.templates.model.Apartment;

public class ApartmentReport extends AbstractExcelTemplate {
	private static final Logger log = LoggerFactory.getLogger(ApartmentReport.class);

	public final String siteCostsApartmentCostCategory = "Site Costs+OH%+Council Requirements";
	public final String councilRequirementsApartmentCostCategory = "Council Requirements";
	public final String bushFireApartmentCostName = "V - Bushfire BAL12.5";
	public final String developerGuidelinesCategory = "Developer Guildlines";

	public CellStyle defaultHeaderStyle = null;
	public CellStyle defaultDataRowStyle = null;
	public CellStyle defaultDataRowCurrenyStyle = null;
	HSSFWorkbook workbook = null;
	public DataFormat format = null;

	private String errorMessage = "";
	
	private boolean hasPortalTask = false;
	
	private String portalUserID = "";
	
	private SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy");

	public String renderExcel(HttpServletRequest request, HttpServletResponse response, HSSFWorkbook inworkbook) throws IOException {
		workbook = inworkbook;
		
		hasPortalTask = "true".equals(request.getParameter("hasPortalFilters"));
		
		portalUserID = request.getParameter("PortalContactID");

		HashMap<String, Building> buildings = getRequestedApartmentData(request);

		configureDefaultStyles(workbook);

		String path = getPreformattedExcelFile(request);

		generateReport(workbook, path, buildings);

		return errorMessage;
	}

	private void configureDefaultStyles(HSSFWorkbook workbook) {
		format = workbook.createDataFormat();

		defaultHeaderStyle = workbook.createCellStyle();
		defaultHeaderStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
		defaultHeaderStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		defaultDataRowStyle = workbook.createCellStyle();
		defaultDataRowStyle.setFillForegroundColor(HSSFColor.WHITE.index);
		// defaultDataRowStyle.getFont(workbook).setFontName("Calibri");

		defaultDataRowCurrenyStyle = workbook.createCellStyle();
		defaultDataRowCurrenyStyle.setFillForegroundColor(HSSFColor.WHITE.index);
		defaultDataRowCurrenyStyle.setDataFormat(format.getFormat("$#,##0"));

	}

	private HashMap<String, Building> getRequestedApartmentData(HttpServletRequest request) {
		HashMap<String, Building> buildings = new HashMap<String, Building>();
		
		List<PropertyEntity> properties = (List<PropertyEntity>)request.getAttribute("entityList"); 
		if(properties == null) {
			setStatus(request,"Loading Records..", 0.02d);
			log.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			properties = PropertyFactory.getPropertyEntities(request, this);
		}
		
		GenRow row = new GenRow();
		row.setViewSpec("properties/ApartmentContactGroupView");
		row.setRequest(request);

		GenRow row2 = new GenRow();
		row2.setViewSpec("ContactGroupView");
		row2.setRequest(request);

		for (PropertyEntity pe : properties) {
			if (hasPortalTask) {
				row.clear();
				row.setParameter("BuildingID", pe.getData("ProductID"));
				row.setParameter("-groupby0", "ApartmentContactID");
				if (StringUtils.isNotBlank(portalUserID)) row.setParameter("ContactID", portalUserID);
				row.sortBy("ApartmentContactGroups.SortOrder", 0);
				row.doAction("search");
				
				row.getResults();
				
				while (row.getNext()) {
					row2.clear();
					row2.setParameter("ContactID", row.getData("ContactID"));
					if(StringUtils.isNotBlank(pe.getString("GroupID"))) {
						log.debug("Product's GroupID is blank");
						row2.setParameter("GroupID", getProductGroups(request, pe.getData("ProductID"), pe.getString("GroupID")));
					} else {
						// pick group from stage
						log.debug("Picking Product's GroupID from stage");
						row2.setParameter("GroupID", getProductGroups(request, pe.getData("BuildingStageProductID"), RunwayUtil.getProduct(request, pe.getString("BuildingStageProductID")).getString("GroupID")));
					}
					if (row2.isSet("GroupID")) row2.doAction("selectfirst");
					
					if (row2.isSuccessful() && row2.isSet("RepUserID"))
						processApartment(request, pe, buildings, (row2.getString("RepFirstName") + " " + row2.getString("RepLastName")), (row.getString("FirstName") + " " + row.getString("LastName")), row2.getString("Name"), row.getDate("ExclusiveDate") );
					else
						processApartment(request, pe, buildings, "No Rep for Group", (row.getString("FirstName") + " " + row.getString("LastName")), pe.getString("SecurityGroupName"), row.getDate("ExclusiveDate") );
				}
				
			} else {
				processApartment(request, pe, buildings);
			}
		}
		
		//linkedproducts.close();
		return buildings;
	}
	
	private String getProductGroups(HttpServletRequest request, String productID, String groupID){
		String groupIDs = groupID;
			
		GenRow productGroup = new GenRow();
		productGroup.setRequest(request);
		productGroup.setTableSpec("ProductSecurityGroups");
		productGroup.setParameter("-select0", "GroupID");
		productGroup.setParameter("ProductID", productID);
		productGroup.setParameter("GroupID", "!NULL+!EMPTY");
		productGroup.doAction("search");
		
		productGroup.getResults();
		
		while (productGroup.getNext()) {
			if (groupIDs.length() > 0) groupIDs += "+";
			groupIDs += productGroup.getData("GroupID");
		}
		
		productGroup.close();
		
		return groupIDs;
	}

	private void processApartment(HttpServletRequest request, PropertyEntity pe, HashMap<String, Building> buildings) {
		processApartment(request, pe, buildings, null, null, null, null);
	}
	
	private void processApartment(HttpServletRequest request, PropertyEntity pe, HashMap<String, Building> buildings, String salesRep, String salesAgent, String groupName, Date date) {
		Apartment p = getApartment(request, pe);
		
		if (StringUtils.isNotBlank(salesRep)) p.setSalesRepName(salesRep);
		if (StringUtils.isNotBlank(salesAgent)) p.setSalesAgentName(salesAgent);
		if (StringUtils.isNotBlank(groupName)) p.setGroupName(groupName);
		if (date != null) {
			p.setExclusiveDate(ddmmyyyy.format(date));
		}
		
		Building e = (Building) buildings.get(p.getBuildingName());
		if (e == null) {
			e = new Building();
			e.setBuildingName(p.getBuildingName());
			buildings.put(p.getBuildingName(), e);
		}

		e.getApartments().add(p);
	}

	public Apartment getApartment(HttpServletRequest request, PropertyEntity property) {
		if (property == null) {
			throw new RuntimeException("Product was not found");
		}
		if (!property.isPropertyType(PropertyType.Apartment)) {
			throw new RuntimeException("Property [" + property.getPropertyType() + "] was not a house and land package");
		}
		com.sok.runway.crm.cms.properties.Apartment product = (com.sok.runway.crm.cms.properties.Apartment) property;
		
		//System.out.println(product.getStatement());
		
		Apartment p = new Apartment();
		p.setBuildingName(capitalizeFirstLetters(product.getEntityBuildingName()));
		p.setLevelName(capitalizeFirstLetters(product.getString("BuildingStageName")));
		
		p.setStatus(product.getString("CurrentStatus"));
		
		p.setRegionName(product.getString("RegionName"));
		p.setLocationName(product.getString("LocationName"));
		
		p.setSuburb(product.getCity());
		p.setApartmentNumber(product.getName());
		if ((StringUtils.isNotBlank(product.getString("LotWidth")) && !"0".equals(product.getString("LotWidth"))) && (StringUtils.isNotBlank(product.getString("LotDepth")) && !"0".equals(product.getString("LotDepth"))))
				p.setApartmentSize(product.getString("Width") + "m x " + product.getString("Depth") + "m");
		else if (StringUtils.isNotBlank(product.getString("LotWidth")) && !"0".equals(product.getString("LotWidth")))
			p.setApartmentSize(product.getString("Width") + "m wide");
 
		if (StringUtils.isNotBlank(product.getString("Area")) && !"0".equals(product.getString("Area")))
			p.setSize(product.getString("Area") + "m2");
		else if (StringUtils.isNotBlank(product.getString("HomeSize")) && !"0".equals(product.getString("HomeSize")))
			p.setSize(product.getString("HomeSize") + "m2");
		else if ((StringUtils.isNotBlank(product.getString("InternalArea")) && !"0".equals(product.getString("InternalArea")))  || (StringUtils.isNotBlank(product.getString("ExternalArea")) && !"0".equals(product.getString("ExternalArea"))))
			p.setSize((product.getDouble("InternalArea") + product.getDouble("ExternalArea")) + "m2");
		
		if (StringUtils.isNotBlank(product.getString("InternalArea")) && !"0".equals(product.getString("InternalArea")))
			p.setInternalArea(product.getString("InternalArea") + "m2");
		if (StringUtils.isNotBlank(product.getString("ExternalArea")) && !"0".equals(product.getString("ExternalArea")))
			p.setExternalArea(product.getString("ExternalArea") + "m2");
 
		p.setHouseName(product.getName());
		
		p.setBeds(product.getDouble("Bedrooms"));
		p.setBaths(product.getDouble("Bathrooms"));
		p.setGarage(product.getDouble("CarParks"));
		
		//p.setSqs(datarow.getString("HomeSizeSq"));
		p.setTotalPrice(getPrice(product.getDripTotalCost() , product.getCost()));
		
		return p;
	}

	private void generateReport(HSSFWorkbook workbook, String formatFilePath, HashMap<String, Building> buildings) {
		TreeSet<String> sortedBuildings = new TreeSet<String>(buildings.keySet());

		HSSFSheet worksheet = renderWorksheet(workbook, "All Buildings");
		HSSFSheet formattedSheet = getFormattedSheet(formatFilePath);

		int noOfColumns = renderHeaderRow(workbook, worksheet, 0, formattedSheet);

		int rowCount = 1;

		for (String buildingName : sortedBuildings) {
			Building building = (Building) buildings.get(buildingName);

			for (Apartment p : building.getApartments()) {
				renderDataRow(workbook, worksheet, p, rowCount++, formattedSheet);
			}

			// autosize columns after all the data rows have been added
			for (int j = 0; j < noOfColumns; j++) {
				worksheet.autoSizeColumn(j);
			}
		}

		for (String buildingName : sortedBuildings) {
			Building building = (Building) buildings.get(buildingName);
			worksheet = renderWorksheet(workbook, building);

			formattedSheet = getFormattedSheet(formatFilePath);

			noOfColumns = renderHeaderRow(workbook, worksheet, 0, formattedSheet);

			rowCount = 1;
			for (Apartment p : building.getApartments()) {
				renderDataRow(workbook, worksheet, p, rowCount++, formattedSheet);
			}

			// autosize columns after all the data rows have been added
			for (int j = 0; j < noOfColumns; j++) {
				worksheet.autoSizeColumn(j);
			}
		}
	}

	private HSSFSheet renderWorksheet(HSSFWorkbook workbook, Building building) {
		HSSFSheet worksheet = workbook.createSheet(building.getBuildingName());
		return worksheet;
	}

	private HSSFSheet renderWorksheet(HSSFWorkbook workbook, String name) {
		HSSFSheet worksheet = workbook.createSheet(name);
		return worksheet;
	}

	public void renderDataRow(HSSFWorkbook workbook, HSSFSheet worksheet, Apartment p, int rowNum, HSSFSheet formattedSheet) {
		HSSFRow row = worksheet.createRow((short) rowNum);

		Iterator<Cell> dataRowIterator = null;

		if (formattedSheet != null) {
			dataRowIterator = formattedSheet.getRow(1).cellIterator();
		}

		int i = 0;
		addTextCell(row, dataRowIterator, p.getApartmentNumber(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getBuildingName() + " / " + p.getLevelName(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getSuburb(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getRegionName(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getLocationName(), i++, defaultDataRowStyle);

		// dataRowIterator = null;
		addTextCell(row, dataRowIterator, p.getSize(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getInternalArea(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getExternalArea(), i++, defaultDataRowStyle);
		addNumericCell(row, dataRowIterator, p.getBeds(), i++, defaultDataRowStyle);
		addNumericCell(row, dataRowIterator, p.getBaths(), i++, defaultDataRowStyle);
		addNumericCell(row, dataRowIterator, p.getGarage(), i++, defaultDataRowStyle);
		// addCell(row, dataRowIterator, p.getSqs(), i++, defaultDataRowStyle);
		/*
		addCurrencyCell(row, dataRowIterator, p.getSiteCosts(), i++, defaultDataRowStyle);
		addCurrencyCell(row, dataRowIterator, p.getGuidelines(), i++, defaultDataRowStyle);
		addCurrencyCell(row, dataRowIterator, p.getBushfire(), i++, defaultDataRowStyle);
		*/
		// addCurrencyCell(row, dataRowIterator, p.getFacadePrice(), i++, defaultDataRowStyle);
		addCurrencyCell(row, dataRowIterator, p.getTotalPrice(), i++, defaultDataRowStyle);
		addTextCell(row, dataRowIterator, p.getStatus(), i++, defaultDataRowStyle);
		if (hasPortalTask) {
			addTextCell(row, dataRowIterator, p.getSalesAgentName(), i++, defaultDataRowStyle);
			addTextCell(row, dataRowIterator, p.getSalesRepName(), i++, defaultDataRowStyle);
			addTextCell(row, dataRowIterator, p.getGroupName(), i++, defaultDataRowStyle);
			addTextCell(row, dataRowIterator, p.getExclusiveDate(), i++, defaultDataRowStyle);
		}
	}

	public int renderHeaderRow(HSSFWorkbook workbook, HSSFSheet worksheet, int rowNum, HSSFSheet formattedSheet) {
		HSSFRow row = worksheet.createRow((short) rowNum);

		Iterator<Cell> headerIterator = null;

		if (formattedSheet != null) {
			headerIterator = formattedSheet.getRow(0).cellIterator();
		}

		int i = 0;

		addTextCell(row, headerIterator, "Apartment No", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Building / Level", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Suburb", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Region", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Location", i++, defaultHeaderStyle);
		// headerIterator = null;
		addTextCell(row, headerIterator, "Size", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Interal Area", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "External Area", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Beds", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Baths", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Garage", i++, defaultHeaderStyle);
		// addCell(row, headerIterator, "Sqs", i++, defaultHeaderStyle);
		/*
		addTextCell(row, headerIterator, "Site Costs", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Guidelines", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Bushfire", i++, defaultHeaderStyle);
		*/
		// addCell(row, headerIterator, "Facade Price", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Total Price", i++, defaultHeaderStyle);
		addTextCell(row, headerIterator, "Status", i++, defaultHeaderStyle);
		if (hasPortalTask) {
			addTextCell(row, headerIterator, "Sales Agent", i++, defaultHeaderStyle);
			addTextCell(row, headerIterator, "Sales Rep", i++, defaultHeaderStyle);
			addTextCell(row, headerIterator, "Group", i++, defaultHeaderStyle);
			addTextCell(row, headerIterator, "Exclusivity", i++, defaultHeaderStyle);
		}

		return i;
	}

	private void addTextCell(HSSFRow row, Iterator<Cell> headerIterator, String cellValue, int cellNum, CellStyle defaultStyle) {
		HSSFCell cellA1 = row.createCell(cellNum);

		if (headerIterator != null && headerIterator.hasNext()) {
			Cell cell = headerIterator.next();

			if (cell != null) {
				CellStyle newStyle = workbook.createCellStyle();
				newStyle.cloneStyleFrom(cell.getCellStyle());
				cellA1.setCellType(cell.getCellType());
				cellA1.setCellStyle(newStyle);
				cellA1.setCellValue(cellValue);
			}
		} else {
			cellA1.setCellStyle(defaultStyle);
			cellA1.setCellType(HSSFCell.CELL_TYPE_STRING);
			cellA1.getCellStyle().setDataFormat((short) 0);
			cellA1.setCellValue(cellValue);
		}
	}

	private void addNumericCell(HSSFRow row, Iterator<Cell> headerIterator, int cellValue, int cellNum, CellStyle defaultStyle) {
		HSSFCell cellA1 = row.createCell(cellNum);

		if (headerIterator != null && headerIterator.hasNext()) {
			Cell cell = headerIterator.next();

			if (cell != null) {
				CellStyle newStyle = workbook.createCellStyle();
				newStyle.cloneStyleFrom(cell.getCellStyle());
				cellA1.setCellType(cell.getCellType());
				cellA1.setCellStyle(newStyle);
				cellA1.setCellValue(cellValue);
			}
		} else {
			cellA1.getCellStyle().setDataFormat(format.getFormat("#"));
			cellA1.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			cellA1.setCellValue(cellValue);
		}

	}

	private void addNumericCell(HSSFRow row, Iterator<Cell> headerIterator, double cellValue, int cellNum, CellStyle defaultStyle) {
		HSSFCell cellA1 = row.createCell(cellNum);

		if (headerIterator != null && headerIterator.hasNext()) {
			Cell cell = headerIterator.next();

			if (cell != null) {
				CellStyle newStyle = workbook.createCellStyle();
				newStyle.cloneStyleFrom(cell.getCellStyle());
				cellA1.setCellType(cell.getCellType());
				cellA1.setCellStyle(newStyle);
				cellA1.setCellValue(cellValue);
			}
		} else {
			cellA1.getCellStyle().setDataFormat(format.getFormat("#"));
			cellA1.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			cellA1.setCellValue(cellValue);
		}

	}

	private void addCurrencyCell(HSSFRow row, Iterator<Cell> headerIterator, double cellValue, int cellNum, CellStyle defaultStyle) {
		HSSFCell cellA1 = row.createCell(cellNum);

		if (headerIterator != null && headerIterator.hasNext()) {
			Cell cell = headerIterator.next();

			if (cell != null) {
				CellStyle newStyle = workbook.createCellStyle();
				newStyle.cloneStyleFrom(cell.getCellStyle());
				cellA1.setCellType(cell.getCellType());
				cellA1.setCellStyle(newStyle);
				cellA1.setCellValue(cellValue);
			}
		} else {
			cellA1.setCellStyle(defaultDataRowCurrenyStyle);
			cellA1.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			cellA1.setCellValue(cellValue);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ApartmentReport report = new ApartmentReport();
		// report.createApartmentReport();
		System.out.println("Done");
	}

	private static String capitalizeFirstLetters(String input) {
		if (StringUtils.isEmpty(input))
			return input;
		if (StringUtils.countMatches(input, " ") > 0)
			return WordUtils.capitalize(input.toLowerCase());
		else
			return StringUtils.capitalize(input.toLowerCase());

	}

	private double getPrice(String price, String defaultPrice) {
		if (price.replaceAll("[\\$0\\.\\-]", "").length() == 0) {
			price = defaultPrice;
		}

		if (price != null) {
			try {
				return StringUtil.getRoundedValue(price);

			} catch (Exception e) {
			}
		}
		return 0;
	}

	private HSSFSheet getFormattedSheet(String formatFilePath) {
		if (StringUtils.isNotBlank(formatFilePath)) {
			try {
				FileInputStream file = new FileInputStream(new File(formatFilePath));

				HSSFWorkbook workbook = new HSSFWorkbook(file);
				HSSFSheet sheet = workbook.getSheetAt(0);
				return sheet;
			} catch (Exception e) {
				e.printStackTrace();
				errorMessage = "the excel format file is not as per the specifications, please rectify and retry";
			}
		}
		return null;
	}

	private String getPreformattedExcelFile(HttpServletRequest request) {
		GenRow bean = new GenRow();
		bean.setViewSpec("DocumentView");
		bean.setRequest(request);
		bean.setColumn("DocumentSubType", "ApartmentReportFormat");
		bean.doAction("selectfirst");

		log.debug("getPreformattedExcelFile Query: " + bean.getStatement());

		if (bean.isSuccessful() && StringUtils.isNotBlank(bean.getString("FilePath"))) {
			String p = "/" + bean.getString("FilePath");
			log.debug("PreformattedExcelFile file path is : " + p);
			return InitServlet.getRealPath(p);
		}
		return null;
	}
}