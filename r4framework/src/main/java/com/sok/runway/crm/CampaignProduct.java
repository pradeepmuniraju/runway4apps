package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;


public class CampaignProduct extends LinkableProduct {

public static final String CAMPAIGN_PRODUCT_VIEWSPEC = "CampaignProductView";
   
   public CampaignProduct(Connection con) {
		super(con, CAMPAIGN_PRODUCT_VIEWSPEC);
	}
		
	public CampaignProduct(ServletRequest request) {
		super(request, CAMPAIGN_PRODUCT_VIEWSPEC);
		populateFromRequest(request.getParameterMap());
	}
	   
	public CampaignProduct(Connection con, String campaignProductID) {
		this(con);
	   load(campaignProductID);
	}
   
   public Validator getValidator() {
		Validator val = Validator.getValidator("CampaignProducts");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("CampaignProductID","SYSTEM ERROR - CampaignProductID");
			val.addMandatoryField("ProductID","SYSTEM ERROR - ProductID");
			val.addMandatoryField("CampaignID","SYSTEM ERROR - ProductID");
			Validator.addValidator("CampaignProducts", val);
		}
		return val;
   }
}