package com.sok.runway.crm.searchbuilder;

public class Select {
    private String id;
    private String name;
    private String className;
    private String title;
    private Option[] options;
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }
    
    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }
    
    /**
     * @return the options
     */
    public Option[] getOptions() {
        return options;
    }
    
    /**
     * @param options the options to set
     */
    public void setOptions(Option[] options) {
        this.options = options;
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String toString() {
        return "<select>";
    }
}
