package com.sok.runway.crm.searchbuilder;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.DisplaySpec;
import com.sok.framework.SpecManager;
import com.sok.runway.dao.ProfileQuestionDAO;
import com.sok.runway.model.ProfileQuestion;

public class FieldNameOptions {
    private HttpServletRequest request;
    private SearchCriteria searchCriteria;
    
    public FieldNameOptions(HttpServletRequest request, SearchCriteria searchCriteria) {
        this.request = request;
        this.searchCriteria = searchCriteria;
    }
    
    public Option[] getNameOptions() {
        Option[] nameOptions = null;
        FieldType fieldType = this.searchCriteria.getFieldType();
        String fieldTypeValue = this.searchCriteria.getFieldTypeValue();
        
        if(fieldType == FieldType.MODULE) {
            if(fieldTypeValue.equals(this.searchCriteria.getSearchType().getModuleName())) {
                nameOptions = getNameOptionsForModule(this.searchCriteria.getDisplaySpec());
            } else {
                nameOptions = getNameOptionsForLinkedModule(fieldTypeValue);
            }
        } else if(fieldType == FieldType.PROFILE) {
            nameOptions = getNameOptionsForProfile(fieldTypeValue);
        }
        
        return nameOptions;      
    }
     
    private Option[] getNameOptionsForModule(DisplaySpec displaySpec) {
        Option[] nameOptions = null;
        
        String[] itemNames = displaySpec.getItemNames();
        
        if(itemNames != null) {
           nameOptions = new Option[itemNames.length];
           
           for(int i = 0; i < itemNames.length; i++) {
              String itemName = itemNames[i];
              String itemFieldName = displaySpec.getItemFieldName(itemName);
              String itemFieldLabel = displaySpec.getItemLabel(itemName);
              
              Option nameOption = new Option();
              nameOption.setValue(itemName);
              nameOption.setText(itemFieldLabel);
              nameOption.setTitle(itemFieldLabel);
              if(this.searchCriteria.getFieldNameValue().equals(itemName)) {
                  nameOption.setSelected(true);
              }
              
              nameOptions[i] = nameOption;
           }
        }
        
        return nameOptions;
    }
    
    private Option[] getNameOptionsForLinkedModule(String relatedDisplayName) {
        DisplaySpec displaySpec = SpecManager.getDisplaySpec(relatedDisplayName);
        
        return getNameOptionsForModule(displaySpec);
    }
    
    private Option[] getNameOptionsForProfile(String profileID) {
        Option[] nameOptions = null;
        /* Get profile questions */
        ProfileQuestionDAO profileQuestionDAO = new ProfileQuestionDAO();
        List<ProfileQuestion> profileQuestions = profileQuestionDAO.findAllByProfileID(request, profileID);
        
        nameOptions = new Option[profileQuestions.size()];
        int index = 0;
        for(ProfileQuestion profileQuestion : profileQuestions) {
            Option option = new Option();
            option.setValue(profileQuestion.getId());
            option.setText(profileQuestion.getLabel());
            option.setTitle(profileQuestion.getLabel());
            if(this.searchCriteria.getFieldNameValue().equals(profileQuestion.getId())) {
                option.setSelected(true);
            }
            nameOptions[index] = option;
            
            index++;
        }
        
        return nameOptions;
    }
}
