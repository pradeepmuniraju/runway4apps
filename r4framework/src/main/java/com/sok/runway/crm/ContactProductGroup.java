package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.campaigns.*;
import com.sok.runway.security.*;
import com.sok.runway.crm.activity.*;
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys; 


import javax.servlet.ServletRequest;
import java.util.*;
import java.sql.*;

public class ContactProductGroup extends AbstractEntityProductGroup {

   private static final String ANSWER_VIEWSPEC = "answers/ContactProductGroupAnswerView";
   private static final String ENTITY_NAME = "ContactProductGroup";
   
   private static final String[] COMPOSITE_KEY_FIELDS = {"ProductGroupID", "ContactID"};
      
   public ContactProductGroup(Connection con) {
      super(con, "ContactProductGroupView");
   }
	
	public ContactProductGroup(ServletRequest request) {
	    super(request, "ContactProductGroupView");
       populateFromRequest(request.getParameterMap());
	}
   
   public ContactProductGroup(Connection con, String contactProductGroupID) {
      this(con);
      load(contactProductGroupID);
   }

   public boolean load(String contactID, String productGroupID) {
      clear();
      if (productGroupID != null && productGroupID.length()!= 0
            && contactID != null && contactID.length()!= 0) {
               
         String[] fieldValues = {productGroupID, contactID};
         
         loadFromField(COMPOSITE_KEY_FIELDS, fieldValues);
         return isLoaded(); 
      } 
      return isLoaded(); 
   }
   
   public String getEntityName() {
	   return ENTITY_NAME;
   }
      
   public String getAnswerViewSpec() {
      return ANSWER_VIEWSPEC;
   }

   public String getStatusHistoryViewName() {
      return "ContactPGStatusView";
   }

   public String getStatusHistoryID() {
      return getParameter("ContactPGStatusID");
   }

   public void setStatusHistoryID(String id) {
      setParameter("ContactPGStatusID", id);
   }   
           
   public Validator getValidator() {
      Validator val = Validator.getValidator("ContactProductGroup");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("ContactProductGroupID","SYSTEM ERROR - ContactProductGroupID");
         val.addMandatoryField("ContactID","SYSTEM ERROR - ContactID");
         val.addMandatoryField("ProductGroupID","SYSTEM ERROR - ProductGroupID");
         val.addMandatoryField("ContactPGStatusID","Product Group must have a status selected");
         Validator.addValidator("ContactProductGroup", val);
      }
      return val;
   }
}