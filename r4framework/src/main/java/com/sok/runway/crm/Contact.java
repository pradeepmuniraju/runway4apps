package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.campaigns.*;
import com.sok.runway.offline.ThreadManager;
import com.sok.runway.security.*;
import com.sok.runway.crm.activity.*;
import com.sok.runway.crm.factories.AddressFactory;
import com.sok.runway.crm.factories.LinkableTemplateFactory;
import com.sok.runway.crm.factories.StatusUpdater;
import com.sok.runway.crm.factories.LinkableDocumentFactory; 
import com.sok.runway.crm.interfaces.*;
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys; 


import javax.servlet.ServletRequest;
import java.util.*;
import java.sql.*;

public class Contact extends ProfiledEntity implements StatusHistory, Addressable, Logable, DocumentLinkable, TemplateLinkable {

   private static final String ANSWER_VIEWSPEC = "answers/ContactAnswerView";
   public static final String UPDATE_COMPANY_CONTACT_ADDRESS = "-updateCompanyContactAddress";
   public static final String ENTITY_NAME = "Contact";
      
   public static final String GROUPS_RELATION = "GroupContactGroup";   
   
   public Contact(Connection con) {
      super(con, "ContactView");
      super.setGroupsRelation(GROUPS_RELATION);
   }
	
	public Contact(ServletRequest request) {
	    super(request, "ContactView");
	    super.setGroupsRelation(GROUPS_RELATION);
       populateFromRequest(request.getParameterMap());
	}
   
   public Contact(Connection con, String contactID) {
      this(con);
      load(contactID);
   }

   public String getEntityName() {
	   return ENTITY_NAME;
   }
   
   public boolean loadUniqueContact(String firstName, String lastName, String email) {
      return loadUniqueContact(firstName, lastName, email, null);
   }
   
   public boolean loadUniqueContact(String firstName, String lastName, String email, String groupID) {
      clear();
      if (firstName != null && firstName.length()!= 0
            && lastName != null && lastName.length()!= 0
            && email != null && email.length()!= 0) {
               
         String sectionCode = ActionBean._percent;
         if (groupID != null && groupID.length() != 0) {
            Group group = new Group(getConnection(), groupID);
            if (group.isLoaded()) {
               sectionCode = group.getSectionCode() + ActionBean._percent;
            }
         }
         String[] uniqueFields = {"FirstName", "LastName", "Email", "ContactGroup.SectionCode"};
         String[] fieldValues = {firstName, lastName, email, sectionCode};
         
         loadFromField(uniqueFields, fieldValues);
      }
      return isLoaded(); 
   }
   
   public static String getDefaultGroupID(ServletRequest request, String contactID)
   {
	   return getDefaultGroupID(ActionBean.getConnection(request), contactID);
   }
   
   public static String getDefaultGroupID(Connection con, String contactID)
   {
	   GenRow row = new GenRow(); 
	   row.setConnection(con); 
	   row.setParameter("ContactID",contactID);
	   row.setTableSpec("ContactGroups");
	   row.setParameter("-select1", "GroupID");
	   row.setParameter("-sort1","SortOrder");
	   row.doAction(GenerationKeys.SELECTFIRST);
	   
	   return row.getData("GroupID");
   }
   
   public static GenRow getContactGroup(Connection con, String contactID, String groupID)
   {
	   GenRow row = new GenRow(); 
	   row.setConnection(con); 
	   row.setViewSpec("ContactGroupView");
	   row.setParameter("ContactID",contactID);
	   row.setParameter("GroupID",groupID);
	   row.doAction(GenerationKeys.SELECTFIRST);
	   
	   if(row.isSuccessful()) 
		   return row;
	   return null;
   }
   
   public Company getCompany() {
      Company company = null;
      if (getField("CompanyID").length() != 0) {
         company = new Company(getConnection());
         
         if (company.load(getField("CompanyID"))) {
            company.setCurrentUser(getCurrentUser());
         }
         else {
            company = null;
         }
      }
      return company;
   }
   
   public ContactProductGroup getContactProductGroup(String productGroupID) {
      ContactProductGroup contactProductGroup = null;
      if (productGroupID != null && productGroupID.length() != 0) {
         contactProductGroup = new ContactProductGroup(getConnection());
         
         if (contactProductGroup.load(getPrimaryKey(), productGroupID)) {
            contactProductGroup.setCurrentUser(getCurrentUser());
         }
         else {
            contactProductGroup = null;
         }
      }
      return contactProductGroup;
   }
   
   public CampaignApplication getCampaignApplication(String campaignID) {
      CampaignApplication campaignApplication = null;
      if (isLoaded() && campaignID.length() != 0) {
         campaignApplication = new CampaignApplication(getConnection());
         campaignApplication.loadContact(campaignID, getPrimaryKey());
         if (!campaignApplication.isLoaded()) {
            campaignApplication = null;
         }
      }
      return campaignApplication;
   }
      
   public String getAnswerViewSpec() {
      return ANSWER_VIEWSPEC;
   }

   public String getStatusHistoryViewName() {
      return "ContactStatusView";
   }

   public String getStatusHistoryID() {
      return getParameter("ContactStatusID");
   }

   public void setStatusHistoryID(String id) {
	   setParameter("ContactStatusID", id);
   }

   public String getStatusID() {
      return getParameter("StatusID");
   }
   
   /* @type = "Home" or "" */
   public Map getAddress(String type) {
      return AddressFactory.getAddress(type, this);
   }
   
   public Map getAddress() {
      return AddressFactory.getAddress(null, this);
   }
   
   /* @type = "Home" or "" */
   public void setAddress(String type, Map address) {
      AddressFactory.setAddress(type, this, address);
   }
   
   public void setAddress(Map address) {
      AddressFactory.setAddress(null, this, address);
   }
   
   public ErrorMap update() {
      if (getStatusID() != null && getStatusID().length() != 0) {
         
         StatusUpdater su = StatusUpdater.getStatusUpdater();
         if (su.isNewStatus(this)) {
            setParameter("ContactStatusID",KeyMaker.generate());
            su.insertStatus(this);
            setParameter("QualifiedDate","NOW");
            setParameter("QualifiedBy",getCurrentUser().getUserID());
         }
      }
      if ("true".equals(getParameter(UPDATE_COMPANY_CONTACT_ADDRESS))) {
         AddressFactory.updateCompanyAddressFromContact(this);
      }
      ErrorMap e = super.update();
      checkEmail();
      return e; 
   }
   
   public ErrorMap insert() {
      boolean insertStatus = false;
      if (getStatusID().length() != 0) {
         if (getStatusHistoryID().length() == 0) {
            setParameter("ContactStatusID",KeyMaker.generate());
         }
         setParameter("QualifiedDate","NOW");
         setParameter("QualifiedBy",getCurrentUser().getUserID());
         insertStatus = true;
      }
      ErrorMap map = super.insert();
      
      if (map.isEmpty()) {
         if (insertStatus) {
            StatusUpdater su = StatusUpdater.getStatusUpdater();
            su.insertStatus(this);
         }
         /* may need to be updated to insert group status also. */
         
      }
      checkEmail(); 
      
      return map;
   }
   
   /**
    * Deletes Child Data of Contacts after RunwayEntity has removed the note. 
    * Profile data will be removed by ProfiledEntity. 
    */
   public boolean delete() {
      if (super.delete()) {
    		GenRow del = new GenRow();
	 		del.setConnection(getConnection());		 		
	 		del.setTableSpec("ContactEvents"); 
	 		del.setParameter(GenerationKeys.UPDATEON +  "ContactEventCampaign.ContactID", getPrimaryKey()); 
			del.setAction(com.sok.framework.generation.GenerationKeys.DELETEALL); 
	 		del.doAction(); 

    		del.clear();     		
    		del.setTableSpec("ContactCampaigns");
    		del.setParameter(GenerationKeys.UPDATEON + getPrimaryKeyName(), getPrimaryKey());
    		del.setAction(com.sok.framework.generation.GenerationKeys.DELETEALL); 
    		del.doAction();
    		
    		del.setTableSpec("ContactStatus");
    		del.doAction();
    		
    		del.setTableSpec("OpportunityContacts");
    		del.doAction();

    		del.setTableSpec("ContactProductGroups");
    		del.doAction();
    		
    		del.setTableSpec("LinkedDocuments");
    		del.doAction();
    		
    		del.setTableSpec("Attendees");
    		del.doAction();

    		del.setTableSpec("ContactProducts");
    		del.doAction();
    		
    		/* deleting note will also delete responses and note profile data */ 
    		
    		GenRow notes = new GenRow(); 
    		notes.setViewSpec("NoteIDView"); 
    		notes.setConnection(getConnection()); 
    		notes.setParameter(getPrimaryKeyName(), getPrimaryKey());
    		notes.getResults(); 
    		
    		AuditEntity child = new Note(getConnection()); 
    		child.setCurrentUser(getCurrentUser());
    		
    		while(notes.getNext()) { 
    			child.load(notes.getData("NoteID")); 
    			child.delete(); 
    		}    		
    		
    		del.clear();
    		del.setTableSpec("ContactLinks");
    		del.setAction(GenerationKeys.DELETEALL);
    		del.setParameter(GenerationKeys.UPDATEON + "LinkerID", getPrimaryKey());
    		del.doAction();

    		del.clear();
    		del.setTableSpec("ContactLinks");
    		del.setParameter(GenerationKeys.UPDATEON + "LinkeeID", getPrimaryKey());
    		del.setAction(GenerationKeys.DELETEALL);
    		del.doAction();
    	  
         return true;
      }
      else {
         return false;
      }
   }
        
   public Validator getValidator() {
      Validator val = Validator.getValidator("Contact");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("ContactID","SYSTEM ERROR - ContactID");
         val.addMandatoryField("ContactStatusID","Contact must have a status selected");
         val.addMandatoryField("GroupID","Contact must have a security group selected");
         Validator.addValidator("Contact", val);
      }
      return val;
   }
   
   
   /**
    *    Linkable Interface
    */
   public String getLinkableTitle() {    
      if (getField("FirstName").length() != 0 ) {
         return getField("FirstName") + " " + getField("LastName");
      }
      else {
         return getField("LastName");
      }
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs() {
      return getLinkableDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs() {
      return getDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
   }      
    
   /**
    *    DocumentLinkable Interface
    */
   public LinkableDocument getLinkableDocument() {
      LinkableDocument doc = new LinkedDocument(getConnection());
      doc.setCurrentUser(getCurrentUser());
      return doc;
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public void linkDocument(String documentID) {
      LinkableDocumentFactory.linkDocument(this, documentID);
   }
   
   public void checkEmail() {
	   
	   Thread t = new Thread(new EmailTest(this),"Contact-EmailTestThread-"+ThreadManager.getNextThreadId());
	   try { t.setPriority(Thread.MIN_PRIORITY); } catch (Exception e) { } 
	   t.start();   
   }

   private class EmailTest implements Runnable { 
	   
	   Contact c = null; 
	   String pk = null; 
	   String email = null; 
	   GenRow upd; 
	   
	   private EmailTest(Contact c) { 
		   	this.c = c;
		   	this.pk = c.getPrimaryKey();
		   	this.email = c.getField("Email"); 
	   }
	   
	   
	   private void update(String status) { 
		   /* set in object in case it is being passed around */ 
		   if(pk.equals(c.getPrimaryKey())) { 
			   c.setField("EmailStatus",status);
		   }
		   
		   upd = new GenRow();
		   upd.setTableSpec("Contacts"); 
		   upd.setParameter("ContactID",pk);
		   upd.setParameter("EmailStatus",status); 
		   upd.doAction(GenerationKeys.UPDATE); 
	   }

	   public void run() { 
		   if(getField("Email").length()>0) {
			   EmailValidationBean evb = new EmailValidationBean(); 
			   evb.setEmailAddress(email); 
			   int result = evb.getValidateEmail();
			   
			   if(result == EmailValidationBean.VALID || result == EmailValidationBean.INVALID 
					   || result == EmailValidationBean.INVALIDDOMAIN || result == EmailValidationBean.INVALIDFORMAT)
				{ 
				   update(EmailValidationBean.CODES[result]);
				}
			   /*
				else if(result == EmailValidationBean.ERROR || result == EmailValidationBean.ERROR_USER)
				{ 
					//do not update as an error occurred
				}
		   		*/
		   } else {
			   //cleanup
			   update("EMPTY");  
		   }
	   }
   }

	public LinkableTemplate getLinkableTemplate() {
		return LinkableTemplateFactory.getLinkableTemplate(this);	
	}
	
	public Collection<String> getLinkableTemplateIDs() {
		return getLinkableTemplateIDs(null);
	}
	
	public Collection<String> getLinkableTemplateIDs(
			Map<String, String> filterParameters) {
		return LinkableTemplateFactory.getLinkableTemplateIDs(this, filterParameters);
	}
	
	public boolean linkTemplate(String templateID) {
		return LinkableTemplateFactory.linkTemplate(this, templateID);
	}
}