package com.sok.runway.crm.searchbuilder;

import java.io.Serializable;

import com.sok.framework.DisplaySpec;
import com.sok.framework.KeyMaker;

public class SearchCriteria implements Serializable {
    public static final String BEAN_ID = "searchCriteriaBean";
    
    private String id;              // SearchCriteriaID primary key
    private String searchSetId;     // parent SearchSetID
    
    /* FieldType */
    private FieldType fieldType;    // module or profile field type
    private String fieldTypeValue;  // module name (if Module field type) or profile ID (if Profile field type)
    
    /* Field Name */
    private String fieldNameValue;  // item name from display spec (if Module field type), or question ID (if Profile field type)

    /* Field Value */
    private String fieldValue;
    private String fieldValueText;
    private String fieldValueDate;
    private String fieldValueNumber;
    
    private String sortOrder;

    private SearchSet searchSet;    // parent search set
   
    public SearchCriteria() {
       this(KeyMaker.generate());
    }
   
    public SearchCriteria(String id) {
       this.id = id;
       this.searchSetId = "";
       this.fieldType = null;
       this.fieldTypeValue = "";
       this.fieldNameValue = "";
       this.fieldValue = "";
       this.fieldValueText = "";
       this.fieldValueDate = "";
       this.fieldValueNumber = "";       
       this.sortOrder = "";
       this.searchSet = null;
    }
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    /**
     * @return the searchSetId
     */
    public String getSearchSetId() {
        if(this.searchSet != null) {
            return this.searchSet.getId();
        }
        
        return this.searchSetId;
    }

    /**
     * @param searchSetId the searchSetId to set
     */
    public void setSearchSetId(String searchSetId) {
        this.searchSetId = searchSetId;
    }

    /**
     * 
     * @param fieldType the field type to set
     */
    public void setFieldType(FieldType fieldType) {
       this.fieldType = fieldType;
    }
    
    /**
     * 
     * @param fieldType the field type to set
     */
    public void setFieldType(String fieldType) {
        this.fieldType = FieldType.createFieldType(fieldType);
    }
   
    /**
     * 
     * @return the fieldType
     */
    public FieldType getFieldType() {
       return this.fieldType;
    }
    
    /**
     * 
     * @return the the module name (if fieldType is "Module") 
     * or ProfileID (if the fieldType is "Profile")
     */
    public String getFieldTypeValue() {
        return fieldTypeValue;
    }

    /**
     * <p>fieldNameValue can be a module name (if fieldType is "Module"), 
     * or ProfileID (if the fieldType is "Profile").</p>
     * 
     * @param fieldNameValue the Module or ProfileID to set
     */
    public void setFieldTypeValue(String fieldTypeValue) {
        this.fieldTypeValue = fieldTypeValue;
    }

    /**
     * 
     * @return the display spec's item name (if fieldType is "Module"), 
     * or QuestionID (if the fieldType is "Profile")
     */
    public String getFieldNameValue() {
       return fieldNameValue;
    }

    /**
     * <p>fieldNameValue can be a display spec's item name (if fieldType is "Module"), 
     * or QuestionID (if the fieldType is "Profile").</p>
     * 
     * @param fieldNameValue the fieldNameValue to set
     */
    public void setFieldNameValue(String fieldNameValue) {
       this.fieldNameValue = fieldNameValue;
    }
    
    /**
     * 
     * @return the field value
     */
    public String getFieldValue() {
       return fieldValue;
    }

    /**
     * 
     * @param value the field value to set
     */
    public void setFieldValue(String value) {
       this.fieldValue = value;
    }
    
    /**
     * @return the fieldValueText
     */
    public String getFieldValueText() {
        return fieldValueText;
    }

    /**
     * @param fieldValueText the fieldValueText to set
     */
    public void setFieldValueText(String fieldValueText) {
        this.fieldValueText = fieldValueText;
    }

    /**
     * @return the fieldValueDate
     */
    public String getFieldValueDate() {
        return fieldValueDate;
    }

    /**
     * @param fieldValueDate the fieldValueDate to set
     */
    public void setFieldValueDate(String fieldValueDate) {
        this.fieldValueDate = fieldValueDate;
    }

    /**
     * @return the fieldValueNumber
     */
    public String getFieldValueNumber() {
        return fieldValueNumber;
    }

    /**
     * @param fieldValueNumber the fieldValueNumber to set
     */
    public void setFieldValueNumber(String fieldValueNumber) {
        this.fieldValueNumber = fieldValueNumber;
    }
    
    /**
     * <p>A helper method to set all field value attributes at once.</p>
     * 
     * @param fieldValue the fieldValue to set
     * @param fieldValueText the fieldValueText to set
     * @param fieldValueDate the fieldValueDate to set
     * @param fieldValueNumber the fieldValueNumber to set
     */
    public void setFieldValue(String fieldValue, String fieldValueText, String fieldValueDate, String fieldValueNumber) {
        setFieldValue((fieldValue != null ? fieldValue : ""));
        setFieldValueText((fieldValueText != null ? fieldValueText : ""));
        setFieldValueDate((fieldValueDate != null ? fieldValueDate : ""));
        setFieldValueNumber((fieldValueNumber != null ? fieldValueNumber : ""));
    }
    
    public String getSortOrder() {
        return this.sortOrder;
    }
    
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
    
    public String getItemFieldName() {
        return this.getDisplaySpec().getItemFieldName(this.fieldNameValue);
    }

    public String getLabel() {
        return this.getDisplaySpec().getItemLabel(this.fieldNameValue);
    }
    
    public SearchSet getSearchSet() {
        return searchSet;
    }
    
    public SearchType getSearchType() {
        return this.searchSet.getSearchType();
     }
    
    public void setSearchSet(SearchSet searchSet) {
       this.searchSet = searchSet;
       this.searchSetId = searchSet.getId();
    }
    
    public DisplaySpec getDisplaySpec() {
       return this.searchSet.getDisplaySpec();
    }
}
