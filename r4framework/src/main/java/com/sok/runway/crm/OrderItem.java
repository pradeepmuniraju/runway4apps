package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import javax.servlet.ServletRequest;
import java.sql.*;

public class OrderItem extends ProfiledEntity {

	private static final String ANSWER_VIEWSPEC = "answers/OrderItemAnswerView";

	public OrderItem(Connection con) {
		super(con, "OrderItemsView");
	}

	public OrderItem(ServletRequest request) {
		 super(request, "OrderItemsView");
		 populateFromRequest(request.getParameterMap());
	}

	public OrderItem(Connection con, String orderItemID) {
		this(con);
		load(orderItemID);
	}

	public String getAnswerViewSpec() {
		return ANSWER_VIEWSPEC;
	}
	
	public Validator getValidator() {
		Validator val = Validator.getValidator("OrderItems");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("OrderItemID","SYSTEM ERROR - OrderItemID");
			val.addMandatoryField("OrderID","Order Item must have an order");
			Validator.addValidator("OrderItems", val);
		}
		return val;
	}
}