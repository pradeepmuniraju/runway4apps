/* ========================================================================= **
 o FILE TYPE        : Java Source File - text/java
 o FILE NAME        : ProfileFactory.java
 o DESIGNER(S)      : 
 o CODER(S)         : Irwan Kodradjaja
 o VERSION          : 1.0
 o LAST MODIFIED BY : 
 o LAST MODIFIED ON : 
 ** ======================================================================== */

/* ======================= ** PACKAGE DECLARATION ** ======================= */
package com.sok.runway.crm.profile;

/* ========================================================================= */

/* ======================== ** IMPORT STATEMENTS ** ======================== */
import com.sok.framework.*;
import com.sok.runway.crm.*;
import com.sok.runway.security.SecurityFactory;
import java.util.*;

/* ========================================================================= */

/* ========================= ** CLASS COMMENTS ** ========================== */
/**
 * Factory class to retrieve surveys related to a Contact object
 * 
 * @author Irwan
 * @version 0.1
 * 
 */
/* ========================================================================= */

/* ====================== ** CLASS DEFINITION ** =========================== */
public class ProfileFactory {
	
	/**
	 * Holds the class used by this profile Factory.
	 * Default: this
	 */
	private static ProfileFactory profileFactory = null;
	
	/**
	 * Retrieves profile factory object
	 * @return 
	 */
	public static ProfileFactory getProfileFactory() {
		
		if (profileFactory == null) {
			profileFactory = new ProfileFactory();
		}
		
		return profileFactory;
	}

	public static void setProfileFactory(ProfileFactory factory) {
		
		profileFactory = factory;
	}

	public ProfileFactory() {
		
		ProfileFactory.profileFactory = this;
	}
	/**
	 * Retrieves SurveyIDs relevant to a given ProfiledEntity object.
	 * @param contactObj
	 * @return
	 */
	public Collection<String> getSurveyIDsForEntity(ProfiledEntity profiledObj) {
		
		return getSurveyIDsForEntity(profiledObj, false);
	}
	
	/**
	 * Retrieves SurveyIDs relevant to a given ProfiledEntity object.
	 * 
	 * @param 	profiledObj 	The target object. Currently handled: Contact, Company, and Note.
	 * @param 	forViewEdit 	A filter for retrieved surveys.
	
	 * @return 	Null if object type is not currently handled, else a Collection object (ArrayList)
	 * 			containing Strings of SurveyID.
	 */
	public Collection<String> getSurveyIDsForEntity(ProfiledEntity profiledObj, boolean forViewEdit) {
		
		GenRow 				surveyBean 	= new GenRow();
		ArrayList<String> surveyIDs 	= null;

		surveyBean.setViewSpec("SurveyView");
		surveyBean.setConnection(profiledObj.getConnection());
		
		boolean matched = false; /* flag to indicate if surveys can be obtained for the given profiledObj type */
		
		if(profiledObj instanceof Company) // Company
		{
			matched = true;
			
			if(profiledObj.getField("Type").length() != 0)
				surveyBean.setParameter("CompanyType", "All+"+profiledObj.getField("Type"));
			else 
				surveyBean.setParameter("CompanyType", "All");
			
			if(forViewEdit) surveyBean.setParameter("DisplayInCompanyView", "Y");
		}
		else if(profiledObj instanceof Contact) // Contact
		{
			matched = true;
			
			if(profiledObj.getField("Type").length() != 0)
				surveyBean.setParameter("ContactType", "All+"+profiledObj.getField("Type"));
			else 
				surveyBean.setParameter("ContactType", "All");
			
			if(forViewEdit) surveyBean.setParameter("DisplayInContactView", "Y");
		}
		else if(profiledObj instanceof Note) // Note
		{
			matched = true;
			
			if(profiledObj.getField("Type").length() != 0)
				surveyBean.setParameter("NoteType", "All+"+profiledObj.getField("Type"));
			else 
				surveyBean.setParameter("NoteType", "All");
			
			if(forViewEdit) surveyBean.setParameter("NoteProfile", "Y");
		}
		else if(profiledObj instanceof Product) // Product
		{
			matched = true;

			if(profiledObj.getField("ProductType").length() != 0)
				surveyBean.setParameter("ProductType", "All+"+profiledObj.getField("ProductType"));
			else 
				surveyBean.setParameter("ProductType", "All");
			
			if(forViewEdit) surveyBean.setParameter("ProductProfile", "Y");
		}
		else if(profiledObj instanceof Opportunity) // Opportunity
		{
			matched = true;
			surveyBean.setParameter("AllOpportunities", "Y");
		}
		else if(profiledObj instanceof ProductGroup) // ProductGroup
		{
			matched = true;
			surveyBean.setParameter("ProductGroupProfile", "Y");
		}
		
		// TODO: handle other ProfiledEntity types
		
		if (matched)
		{
			surveyIDs 	= new ArrayList<String>();
			
			surveyBean.setParameter("Disabled", "N+EMPTY+NULL");
			profiledObj.getCurrentUser().setSearchConstraint(surveyBean,"SurveyGroup");
			surveyBean.doAction(ActionBean.SEARCH);
			surveyBean.getResults();
						
			while (surveyBean.getNext())
			{
				surveyIDs.add(surveyBean.getString("SurveyID"));
			}
		}
		
		return surveyIDs;

	}
}