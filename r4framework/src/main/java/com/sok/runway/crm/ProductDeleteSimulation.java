/**
 * 
 */
package com.sok.runway.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author dion
 *
 */
public class ProductDeleteSimulation {

	private Set<String>				processList = new HashSet<String>();
	private Set<String>				commissionList = new HashSet<String>();
	private Set<String>				quoteList = new HashSet<String>();
	private Set<String>				parentList = new HashSet<String>();
	
	private String					statusID = "";
	
	/**
	 * 
	 */
	public ProductDeleteSimulation() {
		// TODO Auto-generated constructor stub
	}
	
	public void addProcess(String opportunityID) {
		if (opportunityID != null && opportunityID.length() > 0) processList.add(opportunityID);
	}

	public boolean addProduct(String productID) {
			return parentList.add(productID);
	}

	public void addCommission(String commissionID) {
		if (commissionID != null && commissionID.length() > 0) commissionList.add(commissionID);
	}
	
	public void addQuote(String orderID) {
		if (orderID != null && orderID.length() > 0) quoteList.add(orderID);
	}
	
	public boolean canBeDelete() {
		return processList.size() == 0 && commissionList.size() == 0 && quoteList.size() == 0 &&  parentList.size() == 0;
	}
	
	public Set<String> getProcesses() {
		return processList;
	}
	
	public int getProcessCount() {
		return processList.size();
	}
	
	public Set<String> getCommissions() {
		return commissionList;
	}
	
	public int getCommissionCount() {
		return commissionList.size();
	}
	
	public Set<String> getQuotes() {
		return quoteList;
	}
	
	public int getQuoteCount() {
		return quoteList.size();
	}

	public void setStatus(String statusID) {
		this.statusID = statusID;
	}
	
	public String getStatus() {
		return statusID;
	}
}
