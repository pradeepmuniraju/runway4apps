package com.sok.runway.crm.cms.properties.pdf.templates.model;

import java.util.ArrayList;

public class BuildingInfo {
	
	private String buildingID = ""; 
	private String apartmentID = "";

	private String buildingName = ""; 
	private String apartmentName = "";
	
	ArrayList<ApartmentInfo> apartments = new ArrayList<ApartmentInfo> ();

	public String getBuildingID() {
		return buildingID;
	}

	public void setBuildingID(String buildingID) {
		this.buildingID = buildingID;
	}

	public String getApartmentID() {
		return apartmentID;
	}

	public void setApartmentID(String apartmentID) {
		this.apartmentID = apartmentID;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getApartmentName() {
		return apartmentName;
	}

	public void setApartmentName(String apartmentName) {
		this.apartmentName = apartmentName;
	}

	public ArrayList<ApartmentInfo> getApartments() {
		return apartments;
	}

	public void setApartments(ArrayList<ApartmentInfo> apartments) {
		this.apartments = apartments;
	}

}
