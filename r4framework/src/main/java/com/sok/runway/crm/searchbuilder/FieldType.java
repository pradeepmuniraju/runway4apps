package com.sok.runway.crm.searchbuilder;

public enum FieldType {   
   MODULE ("Module"),
   PROFILE ("Profile");
   
   private final String name;
   
   FieldType(String name) {
      this.name = name;
   }
   
   public String toString() {
      return this.name;
   }
   
   public static String[] valueStrings() {
      FieldType[] fieldTypes = FieldType.values();
      
      String[] valueStrings = new String[fieldTypes.length];
      for(int i=0; i < fieldTypes.length; i++) {
         valueStrings[i] = fieldTypes[i].toString();
      }
      
      return valueStrings;
   }
   
   public static FieldType createFieldType(String name) {
      FieldType fieldType = null;
      if(name != null) {
         if(name.equalsIgnoreCase(FieldType.MODULE.toString())) {
            fieldType = FieldType.MODULE;
         } else if(name.equalsIgnoreCase(FieldType.PROFILE.toString())) {
            fieldType = FieldType.PROFILE;
         }
      }
      
      return fieldType;
   }
}
