package com.sok.runway.crm.cms.properties.pdf.templates.model;

public class ReconcileInfo {

	public String reconcileTransactionID;
	public String date;
	public String time;
	public String account;
	public String processedBy;
	public double openingBalance;
	public String paidOnDate;
	public double clearedCreditAmount;	
	public double clearedDebitAmount;	
	public double clearedTransactions;
	public double clearedBalance;
	public double unclearedCreditAmount;
	public double unclearedDebitAmount;
	public double unclearedTransactions;
	public double closingBalance;
	public double bankStatementBalance;
	
	
	public String getReconcileTransactionID() {
		return reconcileTransactionID;
	}
	public void setReconcileTransactionID(String reconcileTransactionID) {
		this.reconcileTransactionID = reconcileTransactionID;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getProcessedBy() {
		return processedBy;
	}
	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}
	public double getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(double openingBalance) {
		this.openingBalance = openingBalance;
	}
	public String getPaidOnDate() {
		return paidOnDate;
	}
	public void setPaidOnDate(String paidOnDate) {
		this.paidOnDate = paidOnDate;
	}
	public double getClearedCreditAmount() {
		return clearedCreditAmount;
	}
	public void setClearedCreditAmount(double clearedCreditAmount) {
		this.clearedCreditAmount = clearedCreditAmount;
	}
	public double getClearedDebitAmount() {
		return clearedDebitAmount;
	}
	public void setClearedDebitAmount(double clearedDebitAmount) {
		this.clearedDebitAmount = clearedDebitAmount;
	}
	public double getClearedTransactions() {
		return clearedTransactions;
	}
	public void setClearedTransactions(double clearedTransactions) {
		this.clearedTransactions = clearedTransactions;
	}
	public double getClearedBalance() {
		return clearedBalance;
	}
	public void setClearedBalance(double clearedBalance) {
		this.clearedBalance = clearedBalance;
	}
	public double getUnclearedCreditAmount() {
		return unclearedCreditAmount;
	}
	public void setUnclearedCreditAmount(double unclearedCreditAmount) {
		this.unclearedCreditAmount = unclearedCreditAmount;
	}
	public double getUnclearedDebitAmount() {
		return unclearedDebitAmount;
	}
	public void setUnclearedDebitAmount(double unclearedDebitAmount) {
		this.unclearedDebitAmount = unclearedDebitAmount;
	}
	public double getUnclearedTransactions() {
		return unclearedTransactions;
	}
	public void setUnclearedTransactions(double unclearedTransactions) {
		this.unclearedTransactions = unclearedTransactions;
	}
	public double getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(double closingBalance) {
		this.closingBalance = closingBalance;
	}		
	public double getBankStatementBalance() {
		return bankStatementBalance;
	}
	public void setBankStatementBalance(double bankStatementBalance) {
		this.bankStatementBalance = bankStatementBalance;
	}
	
}