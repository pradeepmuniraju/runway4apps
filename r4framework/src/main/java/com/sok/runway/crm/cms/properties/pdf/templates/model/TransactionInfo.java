package com.sok.runway.crm.cms.properties.pdf.templates.model;

public class TransactionInfo {

	public String date;
	public String time;
	public String processedBy;
	public String type;
	
	public String trustTransactionRef;
	
	public String lotNo;
	public double processedAmount;
	public double balanceAmount;
	public String receiptNo;
	
	public String totalProcessedAmount;
	public String totalBalanceAmount;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getProcessedBy() {
		return processedBy;
	}
	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTrustTransactionRef() {
		return trustTransactionRef;
	}
	public void setTrustTransactionRef(String trustTransactionRef) {
		this.trustTransactionRef = trustTransactionRef;
	}
	public String getLotNo() {
		return lotNo;
	}
	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}
	public double getProcessedAmount() {
		return processedAmount;
	}
	public void setProcessedAmount(double processedAmount) {
		this.processedAmount = processedAmount;
	}
	public double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public String getTotalProcessedAmount() {
		return totalProcessedAmount;
	}
	public void setTotalProcessedAmount(String totalProcessedAmount) {
		this.totalProcessedAmount = totalProcessedAmount;
	}
	public String getTotalBalanceAmount() {
		return totalBalanceAmount;
	}
	public void setTotalBalanceAmount(String totalBalanceAmount) {
		this.totalBalanceAmount = totalBalanceAmount;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
}