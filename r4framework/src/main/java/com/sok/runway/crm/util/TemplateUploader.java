package com.sok.runway.crm.util;

import java.io.File;
import java.util.zip.ZipEntry;

import com.sok.framework.ActionBean;
import com.sok.framework.KeyMaker;
import com.sok.framework.RunwayUtil;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.runway.crm.util.LegacyUploader.LastRender;
import com.sok.runway.crm.util.LegacyUploader.TemplateLink;

import org.json.simple.JSONObject;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import java.io.StringReader;
import java.io.IOException;
import java.io.StringWriter;

import nu.validator.htmlparser.common.XmlViolationPolicy;
import nu.validator.htmlparser.dom.Dom2Sax;
import nu.validator.htmlparser.dom.HtmlDocumentBuilder;
import nu.validator.htmlparser.sax.HtmlParser;
//import nu.validator.htmlparser.sax.HtmlSerializer;
import nu.validator.htmlparser.test.SystemErrErrorHandler;
import nu.validator.htmlparser.test.TreeDumpContentHandler;
import nu.validator.saxtree.TreeParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class TemplateUploader extends LegacyUploader {

	/* the file uploaded from uploader */
	//private File htmlfile = null;
	private Document doc = null;
	private HtmlDocumentBuilder builder = null;
	
	private String directory = "";
	
	public static final String TEMPLATE_START = "<span style=\"display: none;\">TEMPLATE_START</span>";
	public static final String TEMPLATE_END = "<span style=\"display: none;\">TEMPLATE_END</span>";
	
	public TemplateUploader() {
		if(builder == null)
		{
			builder = new HtmlDocumentBuilder();
		}
	}
	
	public TemplateUploader(File f, ServletContext sctx) {
		// this can't be done is super as when it gets back it clears the doc
		//super(f, sctx);
		
		/* XmlViolationPolicy.ALLOW */
		directory = "/files/templates/" + KeyMaker.generate(5);

		if (doRun(f, sctx != null ? sctx.getRealPath("") : "/tmp", directory)) {
			setUploadSuccess(true);
			runProcess();
		} else {
			debug("upload failed");
		}

		if(builder == null)
		{
			builder = new HtmlDocumentBuilder();
		}
	}

	protected String cleanHTML(File f) throws FileNotFoundException, IOException, SAXException
	{
		return cleanHTML(new InputSource(new FileReader(f)));
	}
	
	protected String cleanHTML(String s) throws IOException, SAXException
	{
		return cleanHTML(new InputSource(new StringReader(s))); 
	}
	
	protected String cleanHTML(InputSource is) throws FileNotFoundException, IOException, SAXException 
	{
		StringWriter out = new StringWriter();
        ContentHandler serializer = new HTMLSerializer(out);
		HtmlParser parser = new HtmlParser(XmlViolationPolicy.ALLOW);
        parser.setErrorHandler(new SystemErrErrorHandler());
        parser.setContentHandler(serializer);
        parser.setProperty("http://xml.org/sax/properties/lexical-handler",
                serializer);
        parser.parse(is);
        out.flush();
        out.close();
        
        return out.toString();
	}
	
	
	protected void runProcess() {
		if (htmlfile == null) {
			setError("The upload did not contain a html or htm file");
			debug("no html file");
		} else {
			/* can't do this in the constructor as the super constructor 
			 * gets run first */

			if(builder == null)
			{
				builder = new HtmlDocumentBuilder();
			}
			try {
				doc = builder.parse(htmlfile);
				
				/*
				 * Process TemplateURLs from anchors
				 */

				// for each a, attr data-link-id data-href-orig
				
				NodeList nl = doc.getElementsByTagName("a");
				
				Node n = null; 
				Element e = null;
				for(int i=0; i<nl.getLength(); i++)
				{
					n = nl.item(i);
					if(n instanceof Element)
					{
						e = (Element)n;
						String hr = e.getAttribute("href");
						if (hr.startsWith("$")) {
							hr.substring(0);
							if (hr.startsWith("{")) {
								if (!hr.endsWith("}")) {
									continue;
								}
								hr.substring(1, hr.length() - 1);
							}
							TableData rec = templateURLs.getRecord(hr);
							if (rec != null) {
								e.setAttribute("data-link-id", rec.getString("TemplateURLID"));
								e.setAttribute("data-href-orig", rec.getString("URL"));
								e.setAttribute("target",rec.getString("Target"));
							} 
						} else { 
							e.setAttribute("data-href-orig", hr);
						}
					}
				}
			/*
			 * Process TemplateURLs from anchors
			 */

			// for each a, attr data-link-id data-href-orig
			
			nl = doc.getElementsByTagName("img");
			
			n = null; 
			e = null;
			for(int i=0; i<nl.getLength(); i++)
			{
				n = nl.item(i);
				if(n instanceof Element)
				{
					e = (Element)n;
					String hr = e.getAttribute("src");
					if (!hr.startsWith("$") && !hr.toLowerCase().startsWith("http://") && !hr.toLowerCase().startsWith("https://")) {
						e.setAttribute("data-link-id", "" + i);
						e.setAttribute("data-href-orig", hr);
						String link = "${URLHome}" + directory;
						if (!link.endsWith("/") && !hr.startsWith("/")) link += "/";
						link += hr;
						e.setAttribute("src", link);
					} else { 
						e.setAttribute("data-src-orig", hr);
					}
				}
			}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw new RuntimeException("Import File not found", e);
			} catch (SAXException e) {
				e.printStackTrace();
				throw new RuntimeException("HTML Parsing Failed", e);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("IOException", e);
			}
		}
	}

	public void parseText(StringBuilder text) {
	/*	
		try { 
			File t = new File("//tmp//tmp.html");
			if(t.exists()) {
				t.delete();
			}
			FileWriter f = new FileWriter(t);
			f.write(text.toString());
			f.flush();
			f.close();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	*/	
		
		try {
			doc = builder.parse(new InputSource(new StringReader(text
					.toString().replaceAll("&#", "&amp;#"))));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("IOException");
		} catch (SAXException se) {
			se.printStackTrace();
			throw new RuntimeException("HTML Barsing Failed", se);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	public ArrayList<Map<String, String>> getLinks() {
		NodeList nl = getLinkElements();
		if (nl != null)
			return getElementListMap(nl);
		return new ArrayList<Map<String, String>>();
	}

	public ArrayList<Map<String, String>> getImages() {
		NodeList nl = getImageElements();
		if (nl != null)
			return getElementListMap(nl);
		return new ArrayList<Map<String, String>>();
	}

	private ArrayList<Map<String, String>> getElementListMap(NodeList nl) {
		final ArrayList<Map<String, String>> elements = new ArrayList<Map<String, String>>();
		Map<String, String> attrs = null;
		if (nl != null) {
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				if (n instanceof org.w3c.dom.Element) {
					org.w3c.dom.NamedNodeMap nm = ((org.w3c.dom.Element) n)
							.getAttributes();
					attrs = new HashMap<String, String>();
					for (int j = 0; j < nm.getLength(); j++) {
						Node a = nm.item(j);
						attrs.put(a.getNodeName(), a.getNodeValue());
					}
					elements.add(attrs);
				}
			}
		}
		return elements;
	}

	public int getElementIndex(javax.servlet.http.HttpServletRequest request,
			String indexField) {
		throw new RuntimeException("Not supported for HTML Uploader");
	}

	public Map<String, String> getElement(String type, int elementIndex) {
		throw new RuntimeException("Not supported for HTML Uploader");
	}

	//
	private static String HTML_HEAD = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><body><div id=\"PARSE_\">";
	private static String HTML_END = "</div></body></html>";

	public boolean setElement(String rwid, String elementType, String innerHTML) {
		Element e = getHTMLElement(rwid, elementType);
		if (e == null)
			return false;

		/* innerHTML should replace the contents of the Dom Doc */
		if (innerHTML != null && innerHTML.length() > 0) {
			// need to remove any encoding as it cause problems
			innerHTML = StringUtil.urlDecode(innerHTML);
		}
		try {
			/*
			 * DocumentFragment frag = builder.parse(new InputSource(new StringReader(
					HTML_HEAD + innerHTML + HTML_END)));
					There's probably a better way to do this, but I don't have 
					javadocs nor internet so lets just do a quick hack.
			 * 
			 */
			Document frag = builder.parse(new InputSource(new StringReader(
					HTML_HEAD + innerHTML + HTML_END)));
			Element holder = frag.getElementById("PARSE_");
			
			e.setTextContent("");
			for (int i = 0; i < e.getChildNodes().getLength(); i++) {
				e.removeChild(e.getChildNodes().item(i));
			}
			for (int i = 0; i < holder.getChildNodes().getLength(); i++) {
				Node n = holder.getChildNodes().item(i).cloneNode(true);
				doc.adoptNode(n);
				e.appendChild(n);
			}
			return true;
		} catch (SAXException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return false;
	}

	private static void parameteriseElements(NodeList nl, boolean editMode)
	{
		Node n = null; 
		Element e = null;
		for(int i=0; i<nl.getLength(); i++)
		{
			n = nl.item(i);
			if(n instanceof Element)
			{
				e = (Element)n;
				
				if(editMode)
				{
					e.setAttribute("data-rwid", e.getNodeName()+"_"+i);
				} else {
					if(e.hasAttribute("data-rwid"))
					{
						e.removeAttribute("data-rwid");
					}
				}
			}
		}
	}
	
	private static void parameteriseElements(Document doc, boolean editMode)
	{
		parameteriseElements(doc.getElementsByTagName("div"),editMode);
		parameteriseElements(doc.getElementsByTagName("td"),editMode);
		parameteriseElements(doc.getElementsByTagName("p"),editMode);
		parameteriseElements(doc.getElementsByTagName("a"),editMode);
		parameteriseElements(doc.getElementsByTagName("img"),editMode);
	}
	
	public void renderHTML(boolean editMode) {
		/* kill or add attributes for data-rwid="*" */
		parameteriseElements(doc, editMode);
		
		StringWriter sw = new StringWriter();

      //Original
      //HtmlSerializer hs = new HtmlSerializer(sw);
      HTMLSerializer hs = new HTMLSerializer(sw);
		StringBuilder html = null;
		try { 
			new Dom2Sax(hs, hs).parse(doc);
		}
		catch (SAXException se) 
		{
			se.printStackTrace();
		} 
		finally
		{
			sw.flush(); 
			try {
				sw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			html = new StringBuilder(RunwayUtil.encodeWindows1252(sw.toString())); 
		}
		if (editMode) {
			lastRender = LastRender.EDIT;
			/*
	         int templateStart = html.indexOf(TEMPLATE_START);
	         int templateEnd = html.indexOf(TEMPLATE_END);

	         if (templateStart > 0 && templateEnd > 0) {
	            html.insert(templateEnd, "</div>");
	            html.insert(templateStart + TEMPLATE_START.length(), "<div id=\"EDITOR_BODY\">");
	         }
	         */
		} else {
			
		   if (html.indexOf("/RESPONSE/") == -1) {
			   
			   if(html.indexOf("</body>")>-1)
			   {
				   html.insert(html.indexOf("</body>"), "<img src=\"$URLHome/RESPONSE/${NoteID}/Viewed_Email.gif\" width=\"1\" height=\"1\" />");
			   } else { 
				   html.append("<img src=\"$URLHome/RESPONSE/${NoteID}/Viewed_Email.gif\" width=\"1\" height=\"1\" />");
			   }
		    }

		   this.html = html.toString();
		   if (rwid != null && element != null) {
			   setBody(rwid, element);
   		   	   if (endIndex >= 0 && html.indexOf(TEMPLATE_END) == -1) html.insert(endIndex, TEMPLATE_END);
			   if (startIndex >= 0 && html.indexOf(TEMPLATE_START) == -1) html.insert(startIndex, TEMPLATE_START);
		   }
		   	
			int templateStart = html.indexOf(TEMPLATE_START);
			int templateEnd = html.indexOf(TEMPLATE_END); 
			
			//System.out.print(html);
			
			if (templateStart > 0 && templateEnd > 0) {
				this.bodyhtml = html.substring(
						templateStart + TEMPLATE_START.length(), templateEnd);
				this.headerhtml = StringUtil.trim(html.substring(0,
						templateStart));
				this.footerhtml = StringUtil.trim(html.substring(templateEnd
						+ TEMPLATE_END.length()));

				if (this.headerhtml.endsWith(BODY_START)) {
					this.headerhtml = this.headerhtml.substring(0,
							this.headerhtml.length() - BODY_START.length());
					if (this.footerhtml.startsWith("</div>")) {
						/* really, why wouldn't it in this case */
						this.footerhtml = this.footerhtml.substring(BODY_END
								.length());
					}
				}
			} else {
				throw new RuntimeException("template start/end not found");
			}

			lastRender = LastRender.FINAL;
		}
		if(html != null)
			this.html = html.toString();
	}

	private Element getHTMLElement(String rwid, String elementType) {
		rwid = rwid.toLowerCase();
		elementType = elementType.toLowerCase();
		
		if (!rwid.startsWith(elementType)) {
			throw new RuntimeException("Unexpected element ID \"" + rwid
					+ "\" for element type \"" + elementType + "\"");
		}
		NodeList nl = null;
		if ("p".equals(elementType)) {
			nl = doc.getElementsByTagName("p");
		} else if ("td".equals(elementType)) {
			nl = doc.getElementsByTagName("td");
		} else if ("div".equals(elementType)) {
			nl = doc.getElementsByTagName("div");
		} else {
			throw new UnsupportedOperationException(
					"Element type not supported - " + elementType);
		}
		if (nl != null) {
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				if (n instanceof org.w3c.dom.Element) {
					org.w3c.dom.Element e = (org.w3c.dom.Element) n;
					if (rwid.equals(e.getAttribute("data-rwid"))) {
						return e;
					}
				}
			}
		}
		return (Element) null;
	}

	/**
	 * <p>Returns the start and end indeces of the element of <code>elementType</code> and 
	 * <code>ID</code> = <code>rwid</code>.</p> 
	 * 
	 * @param rwid element ID
	 * @param elementType element tag name
	 * @return the start and end indeces of the element of <code>elementType</code> and 
    * <code>ID</code> of <code>rwid</code>.
	 */
	public int[] getElementPosition(String rwid, String elementType) {

		if (!rwid.toLowerCase().startsWith(elementType.toLowerCase())) {
			throw new RuntimeException("Unexpected element ID \"" + rwid + "\" for element type \"" + elementType + "\"");
		}
		String regex = null;

		if ("P".equals(elementType)) {
			regex = START_P;
		} else if ("TD".equals(elementType)) {
			regex = START_TD;
		} else if ("DIV".equals(elementType)) {
			regex = START_DIV;
		} else {
			throw new UnsupportedOperationException("Element type not supported - " + elementType);
		}

		String check = html;
		Pattern p = Pattern.compile(regex);
		int id = Integer.parseInt(rwid.substring(rwid.indexOf("_") + 1));

		Matcher m = p.matcher(check);

		boolean notFound = true;
		int i = 0;
		startIndex = 0;

		while (m.find() && notFound) {
			if (i++ == id) {
				notFound = false;
				startIndex = m.start();
			}
		}
		// put it after TD only or before the others
		if ("TD".equals(elementType)) startIndex = check.indexOf(">", startIndex) + 1;
		
		check = check.substring(startIndex).toUpperCase();
		
		//System.out.println(check);

		// i = check.indexOf(elementType); //start after the initial element

		endIndex = check.length();

		String o1 = "<" + elementType;
		String o2 = "</" + elementType;

		i = findLowest(check, o1, o2, 0);

		if (check.charAt(i) == '<' && check.charAt(i + 1) == '/') {
			// found closing bracket on first try
			endIndex = i;
		} else {
			try {
				int depth = 1;
				int revs = 0;

				boolean freePass = true;

				while (freePass || (depth > 0 && revs < 40)) {
					revs++;

					if (freePass) {
						freePass = false;
					}

					//System.out.println(depth + " " + check.substring(i,
					// i+5));
					if ((check.charAt(i) == '<' && (check.charAt(i + 1) == '/') || ("P"
							.equals(elementType) && check.charAt(i + 1) == 'I'))) {
						depth--;
					} else if (check.charAt(i) == '<') {
						depth++;
					} else {
						freePass = true;
						i = findLowest(check, o1, o2, i + 1);
						continue; // skip this one, it's not a html tag
					}

					if (depth == 0) {
						break; // we've found the end.
					}
					// i = check.indexOf(elementType,i+1);
					i = findLowest(check, o1, o2, i + 1);
				}

				if (depth == 0) {
					endIndex = i;
				} else {
					throw new RuntimeException("Could not find closing <" + elementType + "> tag for element \"" + rwid + "\"");
				}
			} catch (RuntimeException re) {
				throw new RuntimeException(re.getMessage()
						+ ": Perhaps the template is missing a closing <" + elementType
						+ "> tag for element \"" + rwid + "\". Please check whether your template's HTML is well formed (all opening tags have closing tags).");
			}
		}
		endIndex += startIndex;

		int[] ret = new int[2];
		ret[0] = startIndex;
		ret[1] = endIndex;

		return ret;

		//System.out.println(startIndex + " " + endIndex);
	}

	public NodeList getLinkElements() {
		if (doc != null)
			return doc.getElementsByTagName("a");
		return null;
	}

	public NodeList getImageElements() {
		if (doc != null)
			return doc.getElementsByTagName("img");
		return null;
	}

	public NodeList getMapElements() {
		if (doc != null)
			return doc.getElementsByTagName("map");
		return null;
	}
	
	@Override
	public void editImage(HttpServletRequest r)
	{
		String rwID = getVal(r, "image-rwid"); 
		Element e = this.getDomElement(rwID, "image");
		if(e == null)
		{
			throw new RuntimeException("could not find image at index " + rwID);
		}
		if (getVal(r, "-imgSrc").length() > 0) {
			String imgSRC = getVal(r, "-imgSrc").toLowerCase();
			if (!imgSRC.startsWith("http://") && !imgSRC.startsWith("https://")) {
				e.setAttribute("src", "$URLHome/" + getVal(r, "-imgSrc"));
			} else {
				e.setAttribute("src", getVal(r, "-imgSrc"));
			}
		}
		e.setAttribute("alt", getVal(r, "-imgAlt"));
		e.setAttribute("title", getVal(r, "-imgTitle"));
		if (getVal(r, "-imgWidth").length() > 0) {	
			e.setAttribute("width", getVal(r, "-imgWidth"));
		}
		if (getVal(r, "-imgHeight").length() > 0) {
			e.setAttribute("height", getVal(r, "-imgHeight"));
		}
		if(!e.hasAttribute("border"))
		{
			e.setAttribute("border", "0");
		}
		
		if (getVal(r, "-linkURL").length() > 0 || getVal(r, "-linkDocURL").length() > 0) { // has link
			editLink(r);
		}
	}
	public List<TemplateLink> updated = new ArrayList<TemplateLink>();
	
	public void editLink(HttpServletRequest r)
	{
		String rwID = getVal(r, "link-rwid"); 
		Element e = this.getDomElement(rwID, "link");
		if(e == null)
		{
			throw new RuntimeException("could not find link at index " + rwID);
		} 	
		String linkName = getVal(r,"-linkName");
		
		if("".equals(linkName) && e.getAttribute("href").startsWith("$"))
		{
			linkName = e.getAttribute("href").substring(1);
			if(linkName.startsWith("{") && linkName.endsWith("}")) { 
				linkName = linkName.substring(1,linkName.length()-1);
			}
		}
		
		TemplateLink link = null;
		if(!"".equals(linkName))
		{
			link = getTemplateLink(linkName); 
		} 
		if(link == null)
		{
			link = new TemplateLink();
			// valocity tokens can't start with a number, so we use T + random
			link.setLinkName("T" + KeyMaker.generate(5));
		}
		link.parseRequest(r);
		e.setAttribute("data-href-orig",link.getLinkUrl());
		e.setAttribute("href","${" + link.getLinkName() + "}");
		e.setAttribute("data-type",link.getLinkType());
		e.setAttribute("target",link.getLinkTarget());
		
		if("ViewOnline".equals(link.getLinkType())) 
		{
			e.setAttribute("href", "$URLHome/cms/emails/shared/notehtmlview.jsp?ID=${NoteID}&version=html");
		} 
		else if ("SendToAFriend".equals(link.getLinkType()))
		{
			String href = e.getAttribute("href"); 
			if (href == null || href.length() == 0
					|| href.charAt(0) == '#') {
				e.setAttribute("href","$URLHome/system/staf.sok?ID=${NoteID}");
			}
		} else if ("Unsubscribe".equals(link.getLinkType())) { 
			e.setAttribute("href","$URLHome/cms/optout/optout_sendemail.jsp?NoteID=${NoteID}");		
		} else { 
			updated.add(link);
		}
	}

	public List<TemplateLink> getUpdatedLinks()
	{
		return updated; 
	}
	

    public TemplateLink getTemplateLink(String linkIndex) {
		   for(TemplateLink t: updated)
		   {
			   if(linkIndex.equals(t.getLinkName()))
				   return t;
		   }
	      return templateLinks.get(linkIndex);
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getElementJson(String rwid, String elementType)
	{ 
		Element e = getDomElement(rwid, elementType);
		JSONObject obj = new JSONObject();
		if(e == null)
		{
			obj.put("status","failed"); 
			obj.put("message", "Element not found");
			return obj;
		}		
		
		NamedNodeMap atts = e.getAttributes(); 
		
		for(int i=0; i<atts.getLength(); i++)
		{
			obj.put(atts.item(i).getNodeName(), replaceurl(atts.item(i).getNodeValue()));
		}
		
		if("link".equals(elementType)
				&& obj.get("href").toString().startsWith("$")) { 
			String hr = obj.get("href").toString();
			hr = hr.substring(1);
			if (hr.startsWith("{") && hr.endsWith("}")) {
				hr = hr.substring(1, hr.length() - 1);
			} else if (hr.startsWith("%7B") && hr.endsWith("%7D")) {
				hr = hr.substring(3, hr.length() - 3);
				e.setAttribute("href", "${" + hr + "}");
			}
			
			TemplateLink t = getTemplateLink(hr);
			if(t != null)
			{
				t.setJsonParams(obj);
			} else { 
				System.out.println("template link coulnd't be found for " + hr);
			}
		} else if("link".equals(elementType)
				&& !obj.containsKey("data-href-orig"))
		{
			obj.put("data-href-orig", obj.get("href"));
		}	
		return obj;		
	}
	
	private String replaceurl(String ret) { 
		ret = StringUtil.replace(ret,"$URLHome","");
		ret = StringUtil.replace(ret,"${URLHome}","");

		return ret; 
	}
	
	/**
	 * matches a dom element to a runway id. 
	 * *NOTE* this won't give you anything if you've done a final 
	 * render as that process will actually clear the IDs for output.
	 * If this becomes an issue for the html editor (it shouldn't, 
	 * as tehy'll be ignored on reparse) then remove them via js or 
	 * regex. 
	 * @param rwid
	 * @param elementType
	 * @return
	 */
	public Element getDomElement(String rwid, String elementType)
	{
		NodeList nl = null; 
		if("link".equals(elementType))
		{
			nl = doc.getElementsByTagName("a");
		} 
		else if ("image".equals(elementType)) 
		{
			nl = doc.getElementsByTagName("img");
		}
		for(int i=0; i<nl.getLength(); i++)
		{
			if(nl.item(i) instanceof Element)
			{
				if(rwid.equals(((Element)nl.item(i)).getAttribute("data-rwid"))) {
					return (Element)nl.item(i);
				} 
			}
		}
		
		return (Element)null;
	}
	
	public static void main(String[] args) 
	{      

      try { 
         HtmlDocumentBuilder builder = new HtmlDocumentBuilder();
         Document doc = builder.parse(new File("/usr/local/tomcat/webapps/runway4/test.html"));
         StringWriter sw = new StringWriter();
         HTMLSerializer hs = new HTMLSerializer(sw);
         new Dom2Sax(hs, hs).parse(doc);
         sw.flush(); 
         sw.close();
         System.out.println(sw.toString());
      }
      catch (SAXException se) 
      {
         se.printStackTrace(); 
      }
      catch(IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      finally
      {
 
      }

	}
	
}
