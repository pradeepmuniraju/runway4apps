package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.runway.crm.cms.properties.pdf.templates.PublishListPdf;

public class PublishListHeaderFooter extends PdfPageEventHelper implements PDFConstants {

	public void onEndPage(PdfWriter writer, com.itextpdf.text.Document document) {

		try {
			PdfPTable pageTable = new PdfPTable(1);
			pageTable.getDefaultCell().setBorder(0);
			pageTable.getDefaultCell().setPadding(0);
			pageTable.setWidthPercentage(100f);

			PdfPTable hdrTable = PublishListPdf.getHeader();
			pageTable.setTotalWidth(document.right() - document.left());
			pageTable.addCell(hdrTable);

			pageTable.writeSelectedRows(0, -1, 15, 832, writer.getDirectContent());

		} catch (Exception ee) {
			System.out.println("Exception Occurred in PublishListHeaderFooter.java class. Message is: " + ee.getMessage());
			ee.printStackTrace();
		}
	}
}