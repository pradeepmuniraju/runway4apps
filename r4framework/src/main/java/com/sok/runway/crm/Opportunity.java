package com.sok.runway.crm;

import com.sok.runway.*; 
import com.sok.runway.crm.interfaces.*;
import com.sok.runway.crm.factories.*;
import com.sok.runway.crm.activity.*; 
import com.sok.runway.security.AuditEntity; 
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;

import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;

import java.sql.*;
import java.util.*;

public class Opportunity extends ProfiledEntity implements StatusHistory, Logable, DocumentLinkable {

   private static final String ANSWER_VIEWSPEC = "answers/OpportunityAnswerView";
   private static final String STATUS_ID = "OpportunityStatusID"; 
   protected static final String VIEW_SPEC = "OpportunityView";
   public static final String ENTITY_NAME = "Opportunity";
   
   public Opportunity(Connection con) {
      super(con, VIEW_SPEC);
   }
	
	public Opportunity(ServletRequest request) {
	    this(request, VIEW_SPEC);
	}
	
   public Opportunity(ServletRequest request, String viewspec) {
      super(request, viewspec);
      populateFromRequest(request.getParameterMap());
  }	
   
   public Opportunity(Connection con, String opportunityID) {
      this(con);
      load(opportunityID);
   }
   
   public String getAnswerViewSpec() {
      return ANSWER_VIEWSPEC;
   }

   public String getEntityName() {
	   return ENTITY_NAME;
   }

   public String getStatusHistoryViewName() {
      return "OpportunityStatusView";
   }

   public String getStatusHistoryID() {
      return getParameter(STATUS_ID);
   }

	public void setStatusHistoryID(String id) {
		setParameter(STATUS_ID, id);
	}

   public String getStatusID() {
      return getParameter("StatusID");
   }
   
   public ErrorMap update() {
      if (getStatusID() != null && getStatusID().length() != 0) {
         
         StatusUpdater su = StatusUpdater.getStatusUpdater();
         if (su.isNewStatus(this)) {
        	   setStatusHistoryID(KeyMaker.generate());
            su.insertStatus(this);
         }
      }
      return super.update();
   }
   
   public ErrorMap insert() {
	   if(StringUtils.isNotBlank(getParameter("ReferenceProductID")) && StringUtils.isBlank(getParameter("CampaignID")) && "true".equals(InitServlet.getSystemParam("PropertyMode"))) {
		   GenRow pqi = new GenRow();
		   pqi.setConnection(getConnection());
		   pqi.setTableSpec("properties/ProductQuickIndex");
		   pqi.setParameter("-select1","CampaignID");
		   pqi.setParameter("-select2","ProductID");
		   pqi.setParameter("ProductID", getParameter("ReferenceProductID"));
		   pqi.doAction(GenerationKeys.SELECTFIRST);

		   if(pqi.isSuccessful() && pqi.isSet("CampaignID")) {
			   setParameter("CampaignID", pqi.getData("CampaignID"));
		   }
	   }
      boolean insertStatus = false;
      if (getStatusID().length() != 0) {
         if (getStatusHistoryID().length() == 0) {
         	setStatusHistoryID(KeyMaker.generate());
         }
         insertStatus = true;
      }
      
      ErrorMap map = super.insert();
      
      createOpportunityStages();
      
      if (map.isEmpty()) {
         if (insertStatus) {
            StatusUpdater su = StatusUpdater.getStatusUpdater();
            su.insertStatus(this);
         }
      }
      
      return map;
   }
   
   public void createOpportunityStages() {
   }
   
   /**
    * Deletes Child Data of Opportunity after RunwayEntity has removed the note. 
    * Profile data will be removed by ProfiledEntity. 
    */
   public boolean delete() {
      if (super.delete()) {
    	  GenRow list = new GenRow(); 
    	  list.setTableSpec("OpportunityStages");
    	  list.setParameter("-select1","OpportunityStageID");
    	  list.setParameter("-select2","OpportunityID"); 
    	  list.setConnection(getConnection()); 
    	  list.setParameter(getPrimaryKeyName(),getPrimaryKey());
    	  list.getResults(); 
    	  
    	  AuditEntity child = new OpportunityStage(getConnection());
    	  child.setCurrentUser(getCurrentUser());
    	  
    	  while (list.getNext()) { 
    		  child.load(list.getData("OpportunityStageID")); 
    		  child.delete(); 
    	  }
    	  
    	  list.clear(); 
    	  list.setTableSpec("Notes");
    	  list.setParameter("-select1","NoteID");
    	  list.setParameter("-select2","OpportunityID"); 
    	  list.setParameter(getPrimaryKeyName(),getPrimaryKey());
    	  list.getResults(); 
    	  
    	  child = new Note(getConnection());
    	  child.setCurrentUser(getCurrentUser());
    	  
    	  while (list.getNext()) { 
    		  child.load(list.getData("NoteID")); 
    		  child.delete(); 
    	  }
    	  
    	  GenRow del = new GenRow();
    	  del.setConnection(getConnection()); 
    	  del.setParameter(GenerationKeys.UPDATEON + getPrimaryKeyName(), getPrimaryKey());
    	  del.setAction(com.sok.framework.generation.GenerationKeys.DELETEALL);
    	  
    	  del.setTableSpec("LinkedDocuments"); 
    	  del.doAction(); 
    			
    	  del.setTableSpec("OpportunityCompanies");
    	  del.doAction();

    	  del.setTableSpec("RecurringDates");
    	  del.doAction();

    	  del.setTableSpec("OpportunityContacts");
    	  del.doAction();

    	  del.setTableSpec("OpportunityProductGroups");
    	  del.doAction();
    		
    	  del.setTableSpec("Orders");
    	  del.doAction();
    		
    	  del.setTableSpec("LinkedProducts");
    	  del.doAction();
    		
    	  del.setTableSpec("OpportunityStatus");
    	  del.doAction();

    	  del.setTableSpec("Rois");
    	  del.doAction();
    	  
    	  del.setTableSpec("RecurringDates");
    	  del.doAction();
    	  
    	  del.setTableSpec("Commissions");
    	  del.doAction();
    	  
    	  return true; 
      }
      return false; 
   } 
        
   public Validator getValidator() {
      Validator val = Validator.getValidator("Opportunity");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("OpportunityID","SYSTEM ERROR - OpportunityID");
         val.addMandatoryField("GroupID","Opportunity must have a security group selected");
         Validator.addValidator("Opportunity", val);
      }
      return val;
   }
   
   /**
    *    Linkable Interface
    */
   public String getLinkableTitle() {
      return getField("Name");
   }

   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs() {
      return getLinkableDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters);
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs() {
      return getDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
   }     
   
   /**
    *    DocumentLinkable Interface
    */
   public LinkableDocument getLinkableDocument() {
      LinkableDocument doc = new LinkedDocument(getConnection());
      doc.setCurrentUser(getCurrentUser());
      return doc;
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public void linkDocument(String documentID) {
      LinkableDocumentFactory.linkDocument(this, documentID);
   }
}