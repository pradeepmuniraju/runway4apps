package com.sok.runway.crm.cms.properties.entities;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.Product;
import com.sok.runway.crm.cms.properties.Facade;
import com.sok.runway.crm.cms.properties.PackageCost;
import com.sok.runway.externalInterface.beans.cms.properties.SimpleFacade;
import com.sok.service.crm.cms.properties.PlanService.HLPackageCost;

public class HomeEntity extends Product {

	private static final Logger logger = LoggerFactory
			.getLogger(HomeEntity.class);
	static final String VIEWSPEC = "properties/ProductHomeView";
	public static final String ENTITY_NAME = "Home";
	
	private PlanEntity plan = null;

	public HomeEntity() {
		super(VIEWSPEC, (Connection) null);
	}

	public HomeEntity(Connection con) {
		super(VIEWSPEC, con);
	}

	public HomeEntity(ServletRequest request) {
		super(VIEWSPEC, request);
	}

	public HomeEntity(Connection con, String productID) {
		super(VIEWSPEC, con);
		super.setParameter("ProductType", ENTITY_NAME);
		loadFinal(productID);
	}

	@Override
	public String getEntityName() {
		return ENTITY_NAME;
	}
	
	public SimpleFacade getFacade() {
		if(isLoaded() && isSet("ProductID")) {
			Facade f = new Facade(getConnection());
			f.setParameter("ProductID", getField("ProductID"));
			if(logger.isDebugEnabled()) { 
				f.doAction(GenerationKeys.SEARCH);
				logger.debug(f.getStatement());
			}
			f.doAction(GenerationKeys.SELECTFIRST);
			if(f.isSuccessful()) {
				SimpleFacade sf = new SimpleFacade(){};
				sf.setFacadeID(f.getData("ProductID"));
				sf.setName(f.getData("Name"));
				return sf; 
			}
		}
		return null;
	}
	/*
	public LotEntity getLot() {
		
	}
	public PlanEntity getPlan() {
		
	}
	*/
	private List<HLPackageCost> packageCosts = null;
	public List<HLPackageCost> getPackageCosts() {

		if (packageCosts == null) {
			if (getPrimaryKey().length() == 0) {
				return Collections.emptyList();
			}
			synchronized (this) {
				PackageCost pc = new PackageCost(getConnection());
				try {
					pc.setParameter("-join1", "ProductsLinkedProductProduct"); // inner
																				// join
					pc.setParameter("ProductsLinkedProductProduct.ProductID",
							getPrimaryKey());
					if (logger.isDebugEnabled()) {
						pc.doAction(GenerationKeys.SEARCH);
						logger.debug(pc.getStatement());
					}
					pc.getResults(true);
					if (pc.getNext()) {
						packageCosts = new ArrayList<HLPackageCost>(
								pc.getSize());
						do {
							packageCosts.add(new HLPackageCost(pc));
						} while (pc.getNext());
					} else {
						packageCosts = Collections.emptyList();
					}
				} finally {
					pc.close();
				}
			}
		}
		return packageCosts;
	}

	public void setPlanEntity(PlanEntity plan) {
		this.plan = plan;
	}
	
	public PlanEntity getPlanEntity() {
		return plan;
	}
	
	/*
	 * private Facade facade = null;
	 * 
	 * @XmlElement(name="FacadeID") private String getFacadeID() { if(facade
	 * == null) { facade = getFacade(); } if(facade == null) return null; return
	 * facade.getFacadeID(); }
	 * 
	 * public Facade getFacade() {
	 * 
	 * 
	 * 
	 * }
	 */
}
