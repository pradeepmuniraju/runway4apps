package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.security.*;
import com.sok.framework.*;
import javax.servlet.ServletRequest;
import java.sql.*;
import java.util.*;

public class Setup extends AuditEntity {
   
   public static final String DEFAULT_SETUP_ID = "DEFAULT";
   
   public Setup(Connection con) {
      super(con, "SetupView");
   }
	
	public Setup(ServletRequest request) {
	    super(request, "SetupView");
       populateFromRequest(request.getParameterMap());
	}
	
   public Setup(Connection con, String setupID) {
      this(con);
      load(setupID);
   }
      
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("Setup");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("SetupID","SYSTEM ERROR - SetupID");
         val.addMandatoryField("Company","Company must not be blank.");

         Validator.addValidator("Setup", val);
      }
      return val;
   }
   
}