package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ExistingPropertyListFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ExistingPropertyPDFUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.model.Package;
import com.sok.service.crm.cms.properties.PlanService;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExistingPropertyListPdf extends AbstractPDFTemplate implements PDFConstants {
	
	public static final Logger logger = LoggerFactory.getLogger(ExistingPropertyListPdf.class); 
	
	public static final String PRODUCTID = "ProductID";
	
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException,
			DocumentException {
		
		/*try {
			renderPDFConcurrent(request, response, document);
			return;
		} catch (ExecutionException ee) {
			logger.error("Execution Exception", ee);
		} catch (InterruptedException ie) {
			logger.error("Execution Interrupted", ie);
		}*/
		
		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (List<PropertyEntity>)request.getAttribute("entityList"); 
		if(properties == null) {
			setStatus(request,"Loading Records..", 0.02d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			properties = PropertyFactory.getPropertyEntities(request, this);
		}
		
		// HashMap<String, Estate> estates = new HashMap<String, Estate>();
		ArrayList<Package> epList = new ArrayList<Package>();
		
		double num = new Double(properties.size()), record = 1;
		for (PropertyEntity property : properties) {
			if(record % 5 == 0) {
				setStatus(request, "Loading package information" , record / num);
			}
			record++;
			
			if (!property.isPropertyType(PropertyType.ExistingProperty)) {
				throw new RuntimeException(String.format("ProductID [%1$s] Property [%2$s] is not an Existing Property",
						property.getString("ProductID"), property.getPropertyType()));
			}
			final PlanService.ExistingProperty product = new PlanService().getExistingProperty(request, property.getString(PRODUCTID));
			Package p = ExistingPropertyPDFUtil.getPackage(product);

			/*Estate e = (Estate) estates.get(p.estateName);
			if (e == null) {
				e = new ExistingPropertyPDFUtil.Estate();
				e.estateName = p.estateName;
				e.estateLogo = PropertyEntity.resizeImage(p.estateLogo, 140, 65, PropertyEntity.ResizeOption.Fit);
				estates.put(p.estateName, e);
			}*/
			
			epList.add(p);
			
			//String disclaimer = property.getDripText(request, new String[] { property.getString(PRODUCTID) }, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip
			String disclaimer = "";
			if (StringUtil.isBlankOrEmpty(ExistingPropertyPDFUtil.DISCLAIMER_TEXT))
			{
				disclaimer = property.getDripText(request, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip
				
				ExistingPropertyPDFUtil.DISCLAIMER_TEXT = disclaimer; 
				if (StringUtil.isBlankOrEmpty(ExistingPropertyPDFUtil.DISCLAIMER_TEXT))
					ExistingPropertyPDFUtil.DISCLAIMER_TEXT = "";
			}	
		}
		setStatus(request, "Generating PDF", 0.8d);
		
		renderPages(document, epList);
	}

	private void renderPages(com.itextpdf.text.Document document, ArrayList<Package> epList) throws DocumentException {
			//TreeSet<String> sortedEstates = new TreeSet<String>(epList.keySet());
			//boolean addNewPage = false;
	
			/*for (String estateName : sortedEstates) {
				if (addNewPage)
					document.newPage();
				else
					addNewPage = true;
	
				Estate estate = (Estate) estates.get(estateName);
				renderEstatePage(document, estate);
			}*/

			PdfPTable hdrTable = ExistingPropertyPDFUtil.getHeader();

			PdfPTable pageTable = new PdfPTable(1);
			pageTable.setWidthPercentage(100f);
			pageTable.getDefaultCell().setBorder(0);
			pageTable.getDefaultCell().setPadding(0);
			pageTable.getDefaultCell().setFixedHeight(50f);
			pageTable.addCell(hdrTable);
			
			pageTable.getDefaultCell().setFixedHeight(20f);
			pageTable.addCell(BLANK);

			for (int i=0; i<epList.size(); i++)
			{
					if ( i!=0 && ((i%6)==0) ) 
					{
						document.add(pageTable);
						document.newPage();
						pageTable = new PdfPTable(1);
						pageTable.getDefaultCell().setBorder(0);
						pageTable.getDefaultCell().setPadding(0);
						pageTable.getDefaultCell().setFixedHeight(50f);
						pageTable.setWidthPercentage(100f);
	
						pageTable.addCell(hdrTable);
						pageTable.getDefaultCell().setFixedHeight(11f);
						pageTable.addCell(BLANK);
					}

					pageTable.getDefaultCell().setFixedHeight(0f);
					pageTable.getDefaultCell().setBorderWidthTop(0.01f);
					pageTable.getDefaultCell().setBorderWidthBottom(0.0f);
					pageTable.getDefaultCell().setBorderColor(BaseColor.BLACK);

					Package p = (Package) epList.get(i);
					pageTable.addCell(ExistingPropertyPDFUtil.buildPackageTable(p));

					pageTable.getDefaultCell().setBorder(0);
					pageTable.getDefaultCell().setFixedHeight(20f);
					pageTable.addCell(BLANK);
			}
			document.add(pageTable);
	}


	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		return new ExistingPropertyListFooter(request);
	}
	
	/*public void renderPDFConcurrent(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException,
			DocumentException, ExecutionException, InterruptedException {
		
		List<PropertyEntity> properties = (List<PropertyEntity>)request.getAttribute("entityList"); 
		if(properties == null) {
			setStatus(request,"Loading Records..", 0.02d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			PropertyFactory.getPropertyEntities(request, this);
		}
		
		final int size = properties.size();
		double num = new Double(size), record = 1;
		final List<Future<Package>> packageList = new ArrayList<Future<Package>>(size);
		
		for (final PropertyEntity property : properties) {
			if(record % 5 == 0) {
				setStatus(request, "Loading package information" , record / num);
			}
			record++;
			if (!property.isPropertyType(PropertyType.ExistingProperty)) {
				throw new RuntimeException(String.format("ProductID [%1$s] Property [%2$s] is not an Existing Property",
						property.getString("ProductID"), property.getPropertyType()));
			}
			
			//final ExistingProperty product = (ExistingProperty) property;
			final PlanService.ExistingProperty product = new PlanService().getExistingProperty(request, property.getString(PRODUCTID));			
			

			//String disclaimer = property.getDripText(request, new String[] { property.getString(PRODUCTID) }, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip
			String disclaimer = "";
			if (StringUtil.isBlankOrEmpty(disclaimer))
			{	
				disclaimer = property.getDripText(request, new String[] { property.getString(PRODUCTID), property.getString(PRODUCTID), property.getString(PRODUCTID) }, getPdfSimpleName(), "4"); // Drip 01 - First Home Buyer Price
				
				ExistingPropertyPDFUtil.DISCLAIMER_TEXT = disclaimer; 
				if (StringUtil.isBlankOrEmpty(ExistingPropertyPDFUtil.DISCLAIMER_TEXT))
					ExistingPropertyPDFUtil.DISCLAIMER_TEXT = "";
			}	

			
			
			packageList.add(ThreadManager.startCallable(new Callable<Package>() {
				public Package call() {
					return ExistingPropertyPDFUtil.getPackage(product);
				}
			}));
		}
		
		Map<String, EstateTableView> etvMap = new HashMap<String, EstateTableView>();
		for(Future<Package> fp: packageList) {
			Package p = fp.get();
			EstateTableView e = etvMap.containsKey(p.estateName) ? etvMap.get(p.estateName) : null;
			if(e == null) {
				e = new EstateTableView(p);
				etvMap.put(p.estateName, e);
			}
			e.addPackage(p);
		}
		final TreeSet<EstateTableView> sortedEstates = new TreeSet<EstateTableView>(etvMap.values());
		boolean addNewPage = false;

		for (EstateTableView estate : sortedEstates) {
			if (addNewPage)
				document.newPage();
			else
				addNewPage = true;
			estate.render(document);
		}
	}*/
	
	/*private static class EstateTableView implements Comparable<EstateTableView> {
		private final String estateName;
		private final TreeSet<PackageTableView> packageTables = new TreeSet<PackageTableView>();
		private Future<PdfPTable> estateHeader;
		public EstateTableView(final Package p) {
			if (p.estateName != null)
			{	
				estateName = p.estateName;
			}
			else
			{
				estateName = "";
			}
			
			estateHeader = ThreadManager.startCallable(new Callable<PdfPTable>() {
				public PdfPTable call() throws DocumentException {
					return ExistingPropertyPDFUtil.getHeader();
				}
			});
		}
		public void addPackage(final Package p) throws DocumentException {
			packageTables.add(new PackageTableView(p));
		}
		
		public void render(Document document) throws DocumentException, ExecutionException, InterruptedException { 
			final PdfPTable hdrTable = estateHeader.get();
			PdfPTable pageTable = new PdfPTable(1);
			pageTable.getDefaultCell().setBorder(0);
			pageTable.getDefaultCell().setPadding(0);
			pageTable.getDefaultCell().setFixedHeight(50f);
			pageTable.setWidthPercentage(100f);

			pageTable.addCell(hdrTable);
			pageTable.getDefaultCell().setFixedHeight(11f);
			pageTable.addCell(BLANK);
			
			int i=0;
			for(PackageTableView ptv: packageTables) {
				if (i != 0 && i % 6 == 0) {
					document.add(pageTable);
					document.newPage();
					pageTable = new PdfPTable(1);
					pageTable.getDefaultCell().setBorder(0);
					pageTable.getDefaultCell().setPadding(0);
					pageTable.getDefaultCell().setFixedHeight(0f);
					pageTable.setWidthPercentage(100f);

					pageTable.addCell(hdrTable);
					pageTable.getDefaultCell().setFixedHeight(11f);
					pageTable.addCell(BLANK);
				}
				pageTable.getDefaultCell().setFixedHeight(0f);
				pageTable.getDefaultCell().setBorderWidthTop(0.01f);
				pageTable.getDefaultCell().setBorderWidthBottom(0.01f);
				pageTable.getDefaultCell().setBorderColor(BaseColor.BLACK);
				pageTable.addCell(ptv.table.get());
				pageTable.getDefaultCell().setBorder(0);
				pageTable.getDefaultCell().setFixedHeight(6f);
				pageTable.addCell(BLANK);
				i++;
			}
			document.add(pageTable);
		}
		@Override
		public int compareTo(EstateTableView arg1) {
			return this.estateName.compareTo(arg1.estateName);
		}
	}
	
	private static class PackageTableView implements Comparable<PackageTableView> {
		public final String name;
		public final Future<PdfPTable> table;
		public PackageTableView(final Package p) throws DocumentException {
			this.name = p.name  + p.lotNo;
			this.table = ThreadManager.startCallable(new Callable<PdfPTable>() {
				public PdfPTable call() throws DocumentException {
					return ExistingPropertyPDFUtil.buildPackageTable(p);
				}
			});
		}
		@Override
		public int compareTo(PackageTableView arg0) {
			return this.name.compareTo(arg0.name);
		}
	}*/
	
	public String getPdfSimpleName() {
		return "Real Estate List";
	}
	
	public String getDripCount() {
		return "2";
	}
	
}