package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ExistingPropertyListFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ExistingPropertyPDFUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.PlanService;

public class ExistingPropertyPackagePdf extends AbstractPDFTemplate implements PDFConstants {
	
	public static final Logger logger = LoggerFactory.getLogger(ExistingPropertyPackagePdf.class);
	/*** CUSTOM CODE : These parameters are usually customized as per client needs ***/
	private static final Font lotPlanFont = FontFactory.getFont("Arial", 14f, Font.BOLD, BaseColor.WHITE);
	/*************************************/

	public static final String PRODUCTID = "ProductID";

	private static final String BED_IMG = "/specific/pdf/images/icon_bed_white.png";
	private static final String BATH_IMG = "/specific/pdf/images/icon_bath_white.png";
	private static final String CAR_IMG = "/specific/pdf/images/icon_car_white.png";
	private static String LOGO_IMG = PropertyEntity.resizeImage(ExistingPropertyPDFUtil.MAIN_LOGO_IMG, 400, 264, PropertyEntity.ResizeOption.Pad);

	//TODO - test without this, it should not be true.
	private static final boolean dev = Boolean.TRUE;

	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {
		
		setStatus(request, "Loading properties");
		String homePath = request.getSession().getServletContext().getInitParameter("URLHome");

		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (List<PropertyEntity>)request.getAttribute("entityList"); 
		if(properties == null) {
			setStatus(request,"Loading Records..", 0.02d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			properties = PropertyFactory.getPropertyEntities(request, this);
		}
		
		java.util.List<SinglePackage> pList = new java.util.ArrayList<SinglePackage>(properties.size());
		for (PropertyEntity pe : properties) {
			// ActionBean.connect(p);
			pList.add(getPackage(request, pe));
		}

		setStatus(request, "Rendering Pages");
		
		boolean firstPage = true;
		for (SinglePackage p : pList) {
			if (!firstPage)
				document.newPage();
			else
				firstPage = false;
			renderPackage(document, p, homePath);
		}
	}

	private class SinglePackage {
		String filename, heroImage, planImage, projectName, name; 
		String AddressID, Street, Street2, City, State, Postcode;
		String price;

		int bedrooms, bathrooms, carParks;
		
		/*String Name, ProductID, ProductType, ProductSubType, PricingType, Image, ImagePath, ThumbnailImage, ThumbnailImagePath, Summary, PublishDescription, MarketingStatus, PublishingStatus, 
		Vendor, VendorDisplay, VendorPhone, VendorEmail, VendorType, SellingAgent, SellingAgentDisplay, SellingAgentPhone, SellingAgentEmail, 
		PlanProductID, LotProductID, 
		CurrentStatus, CurrentStatusID,
		SellingAgentContactID, SellingAgentContact;
		double PlanTotalCost, LotTotalCost, TotalCost, DripCost, DripResult;
		//boolean Active;
		List<ProductDetail> ProductDetails = Collections.emptyList();*/
		
		
		String dripImg1;
		//String[] inclusions = { "Test Inclusion 01", "Test Inclusion 02", "Test Inclusion 03", "Test Inclusion 04", "Test Inclusion 05", "Test Inclusion 06", "Test Inclusion 07" };
		String existingProdHeading, designDetail;
		String[] inclusions = { };
		String disclaimer = String.format(ExistingPropertyPDFUtil.DISCLAIMER_TEXT, new java.util.Date());
		String repName, repMobile, repPhone, repEmail;
		String locName, locStreet, locCity, displayHours;
	}

	void renderPackage(com.itextpdf.text.Document document, SinglePackage p, String homePath) throws DocumentException {

		PdfPTable pageTable = new PdfPTable(2);
		pageTable.setWidths(new float[] { (299f / 909f), (612f / 909f) });
		pageTable.setWidthPercentage(100f);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.getDefaultCell().setBorder(0);

		PdfPTable houseInfo = new PdfPTable(1);
		houseInfo.setWidthPercentage(100f);
		houseInfo.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

		houseInfo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		houseInfo.getDefaultCell().setBorder(0);
		houseInfo.getDefaultCell().setPadding(0);

		Image i = ExistingPropertyPDFUtil.createImage(LOGO_IMG);
		if (i != null) {
			houseInfo.addCell(i);
		}
		
		/*if (p.estateLogo != null && (i = ExistingPropertyPDFUtil.createImage(p.estateLogo)) != null) {
			houseInfo.getDefaultCell().setPaddingTop(5);
			houseInfo.getDefaultCell().setPaddingLeft(20);
			houseInfo.getDefaultCell().setPaddingRight(20);
			houseInfo.addCell(i);
			houseInfo.getDefaultCell().setPaddingLeft(0);
			houseInfo.getDefaultCell().setPaddingRight(0);
		} else {
			houseInfo.addCell(new Paragraph("Problem with estate " + String.valueOf(p.estateLogo), lotDetailFont));
		}*/
		houseInfo.addCell(new Paragraph("", lotDetailFont));
		
		Paragraph details = new Paragraph();
		details.add(new Chunk("\n", spacerFont));
		String tmpStreet = p.Street;
		if (!StringUtil.isBlankOrEmpty(p.Street2))
		{
			tmpStreet += " " + p.Street2;
		}	
		details.add(new Chunk(tmpStreet, locationFont));
		details.add(new Chunk("\n", locationFont));
		details.add(new Chunk(p.City + ", " + p.State + " " + p.Postcode + "\n", locationFont));

		houseInfo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		houseInfo.getDefaultCell().setPaddingLeft(10);

		houseInfo.addCell(details);
		pageTable.addCell(houseInfo);

		pageTable.getDefaultCell().setFixedHeight(280f);  
		if ((i = ExistingPropertyPDFUtil.createImage(p.heroImage)) != null) {
			// will need to be sized
			pageTable.addCell(i);
		} 
		else 
		{
			pageTable.addCell(new Paragraph("Sorry, the Facade image is not available " + p.heroImage, lotDetailFont));
		}
		pageTable.getDefaultCell().setFixedHeight(0f);
		
		pageTable.getDefaultCell().setColspan(2);
		pageTable.getDefaultCell().setPaddingTop(3);
		pageTable.getDefaultCell().setPaddingLeft(25);
		pageTable.getDefaultCell().setPaddingBottom(10);
		pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		pageTable.getDefaultCell().setBackgroundColor(ExistingPropertyPDFUtil.aqua);
		pageTable.getDefaultCell().setFixedHeight(40f);
		pageTable.addCell(new Paragraph(p.existingProdHeading, subHeader));
		pageTable.getDefaultCell().setFixedHeight(0f);
		
		pageTable.getDefaultCell().setColspan(2);
		pageTable.getDefaultCell().setPaddingTop(0);
		pageTable.getDefaultCell().setPaddingBottom(0);

		pageTable.getDefaultCell().setBackgroundColor(ExistingPropertyPDFUtil.pkgHeaderGrey);

		pageTable.getDefaultCell().setColspan(1);
		pageTable.getDefaultCell().setPaddingTop(0f);
		pageTable.addCell(new Paragraph(p.price, singlePriceFont));
		pageTable.getDefaultCell().setPaddingTop(0f);
		pageTable.getDefaultCell().setPaddingLeft(0f);
		houseInfo = new PdfPTable(9);
		houseInfo.setWidthPercentage(100f);
		// houseInfo.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

		houseInfo.setWidths(new float[] { 10, 50, 5, 5, 5, 5, 5, 5, 5 });
		houseInfo.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		houseInfo.getDefaultCell().setBorder(0);
		houseInfo.getDefaultCell().setPadding(0);

		pageTable.getDefaultCell().setBackgroundColor(ExistingPropertyPDFUtil.pkgHeaderGrey);

		houseInfo.addCell(new Paragraph(""));
		
		
		houseInfo.getDefaultCell().setPaddingLeft(5f);
		houseInfo.getDefaultCell().setPaddingTop(12f);
		houseInfo.getDefaultCell().setPaddingBottom(10f);
		houseInfo.addCell(new Paragraph(p.name.toUpperCase(), lotPlanFont));

		int iPadTop = 3, iPadBottom = 3, iPadRight = 4, iHeight = 12;
		float iPadImg = 12f, iPadText = 11f;
		houseInfo.getDefaultCell().setPaddingTop(iPadText);
		houseInfo.getDefaultCell().setPaddingBottom(15);
		PdfPTable iTable = null;
		if ((i = ExistingPropertyPDFUtil.createImage(homePath + BED_IMG)) != null) {
			houseInfo.getDefaultCell().setPaddingTop(iPadImg);
			iTable = new PdfPTable(1);
			iTable.getDefaultCell().setBorder(0);
			iTable.getDefaultCell().setPadding(0);
			iTable.getDefaultCell().setPaddingTop(iPadTop);
			iTable.getDefaultCell().setPaddingRight(iPadRight);
			iTable.getDefaultCell().setFixedHeight(iHeight);
			iTable.getDefaultCell().setBorderWidthRight(1f);
			iTable.getDefaultCell().setBorderColorRight(BaseColor.WHITE);
			iTable.addCell(i);
			houseInfo.addCell(iTable);
			houseInfo.getDefaultCell().setPaddingTop(iPadText);
		} else {
			houseInfo.addCell(new Paragraph("Rooms: ", lotPlanFont));
		}
		houseInfo.addCell(new Paragraph(String.valueOf(p.bedrooms), lotPlanFont));
		if ((i = ExistingPropertyPDFUtil.createImage(homePath + CAR_IMG)) != null) {
			houseInfo.getDefaultCell().setPaddingTop(iPadImg);
			iTable = new PdfPTable(1);
			iTable.getDefaultCell().setBorder(0);
			iTable.getDefaultCell().setPadding(0);
			iTable.getDefaultCell().setPaddingTop(iPadTop);
			iTable.getDefaultCell().setPaddingRight(iPadRight);
			iTable.getDefaultCell().setFixedHeight(iHeight);
			iTable.getDefaultCell().setBorderWidthRight(1f);
			iTable.getDefaultCell().setBorderColorRight(BaseColor.WHITE);
			iTable.addCell(i);
			houseInfo.addCell(iTable);
			houseInfo.getDefaultCell().setPaddingTop(iPadText);
		} else {
			houseInfo.addCell(new Paragraph("Cars: ", lotPlanFont));
		}
		houseInfo.addCell(new Paragraph(String.valueOf(p.carParks), lotPlanFont));
		if ((i = ExistingPropertyPDFUtil.createImage(homePath + BATH_IMG)) != null) {
			houseInfo.getDefaultCell().setPaddingTop(iPadImg);
			iTable = new PdfPTable(1);
			iTable.getDefaultCell().setBorder(0);
			iTable.getDefaultCell().setPadding(0);
			iTable.getDefaultCell().setPaddingTop(iPadTop);
			iTable.getDefaultCell().setPaddingRight(iPadRight);
			iTable.getDefaultCell().setFixedHeight(iHeight);
			iTable.getDefaultCell().setBorderWidthRight(1f);
			iTable.getDefaultCell().setBorderColorRight(BaseColor.WHITE);
			iTable.addCell(i);
			houseInfo.addCell(iTable);
			houseInfo.getDefaultCell().setPaddingTop(iPadText);
		} else {
			houseInfo.addCell(new Paragraph("Baths: ", lotPlanFont));
		}
		houseInfo.addCell(new Paragraph(String.valueOf(p.bathrooms), lotPlanFont));

		houseInfo.addCell(BLANK);

		pageTable.addCell(houseInfo);
		pageTable.getDefaultCell().setPaddingTop(10);
		pageTable.getDefaultCell().setPaddingLeft(12);
		pageTable.getDefaultCell().setPaddingRight(1);
		pageTable.getDefaultCell().setPaddingBottom(0);
		pageTable.getDefaultCell().setBackgroundColor(bottomLeftGrey);

		houseInfo = new PdfPTable(2);
		houseInfo.setWidths(new float[] { 5, 60 });
		houseInfo.getDefaultCell().setBorder(0);
		houseInfo.getDefaultCell().setPaddingLeft(15);
		houseInfo.getDefaultCell().setPaddingBottom(5f);
		houseInfo.getDefaultCell().setColspan(2);
		houseInfo.getDefaultCell().setPaddingRight(10);

		houseInfo.getDefaultCell().setLeading(12f, 0f);
		houseInfo.addCell(new Paragraph(p.designDetail, packageDetailFont));
		houseInfo.addCell(BLANK);
		
		
		
		pageTable.getDefaultCell().setFixedHeight(480f);  // This will be used to make sure that in all cases contents are generated in one-page only.
		pageTable.getDefaultCell().setPaddingRight(0);
		houseInfo.getDefaultCell().setPaddingBottom(0f);
		houseInfo.addCell(new Paragraph("INCLUSIONS", packageInclusionsHeader));
		houseInfo.getDefaultCell().setColspan(1);
		houseInfo.getDefaultCell().setPaddingLeft(2);

		i = null;
		houseInfo.getDefaultCell().setPaddingBottom(2f);
		houseInfo.getDefaultCell().setPaddingTop(2f);
		houseInfo.getDefaultCell().setPaddingRight(5f);
		for (String s : p.inclusions) {
			if (i != null) {
				houseInfo.getDefaultCell().setPaddingTop(4f);
				houseInfo.getDefaultCell().setPaddingLeft(2);
				houseInfo.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
				houseInfo.addCell(i);
				houseInfo.getDefaultCell().setPaddingLeft(1);
				houseInfo.getDefaultCell().setPaddingTop(2f);
			} else
				houseInfo.addCell(new Paragraph("\u2022", packageDetailFont)); // bullet
			houseInfo.addCell(new Paragraph(s, packageDetailFont));
		}

		houseInfo.getDefaultCell().setColspan(2);
		houseInfo.getDefaultCell().setPaddingTop(5f);
		houseInfo.addCell(BLANK);

		houseInfo.getDefaultCell().setColspan(2);

		/** CUSTOM CODE : Place holder for DRIPS **/
		String imgStr = "";
		if (p.dripImg1 != null)
		{	
				PdfPCell cell = null;
				 imgStr = homePath + "/" + p.dripImg1;
				try
				{
						URL imgUrl = new URL(imgStr);
						java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
						
						Image dripImg = Image.getInstance(imgUri.toString());
						//dripImg.setAbsolutePosition(180, 87);
						// dripImg.scaleToFit(125, 67);
						dripImg.scaleToFit(170, 90);
						
						dripImg.setBorder(0);
						dripImg.setBorderWidth(0);
						///dripImg.setUseVariableBorders(false);
						
						
						cell = new PdfPCell(dripImg);
						cell.setBorderWidth(0f);
						cell.setBorder(0);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						cell.setPaddingBottom(10);

						houseInfo.getDefaultCell().setPaddingTop(10f);
						houseInfo.getDefaultCell().setPaddingBottom(10f);
						houseInfo.getDefaultCell().setPaddingLeft(10f);
						houseInfo.getDefaultCell().setPaddingRight(10f);
						houseInfo.getDefaultCell().setBackgroundColor(bottomLeftGrey);
						houseInfo.getDefaultCell().setBorder(0);
						houseInfo.getDefaultCell().setBorderWidth(0);
						//dripTable.getDefaultCell().setBorderColor(BaseColor.WHITE);
						//dripTable.getDefaultCell().setBorderWidthRight(20f);
						houseInfo.addCell(cell);
				}
				catch(Exception ee)
				{
						cell = new PdfPCell(new Paragraph("Problem with image" + imgStr ));
						System.out.println("Exception Occurred. Message is: " + ee.getMessage());
						ee.printStackTrace();
				}
		}

		
		/*
		  for (String img : p.promoImages) { if ((i = ExistingPropertyPDFUtil.createImage(new
		  StringBuilder(100).append(homePath).append("/").append(img).toString())) != null) {
		  houseInfo.getDefaultCell().setPaddingRight(0); houseInfo.addCell(BLANK);
		  houseInfo.getDefaultCell().setPadding(0); houseInfo.getDefaultCell().setPaddingLeft(23);
		  houseInfo.getDefaultCell().setPaddingRight(35); houseInfo.addCell(i); } }
		*/
		

		/*PdfPTable blankTable = new PdfPTable(3);
		blankTable.setWidths(new float[] { 5, 40, 10 });
		blankTable.getDefaultCell().setBorder(0);
		blankTable.getDefaultCell().setPadding(0);
		blankTable.getDefaultCell().setFixedHeight(60f);
		blankTable.getDefaultCell().setBackgroundColor(bottomLeftGrey);
		blankTable.addCell(BLANK);
		blankTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		blankTable.addCell(BLANK);
		blankTable.getDefaultCell().setBackgroundColor(bottomLeftGrey);
		blankTable.addCell(BLANK);
		houseInfo.getDefaultCell().setPaddingTop(15f);
		houseInfo.addCell(blankTable);*/
		/**********************************/

		houseInfo.getDefaultCell().setColspan(1);
		houseInfo.addCell(BLANK);
		houseInfo.getDefaultCell().setPaddingLeft(0);
		houseInfo.getDefaultCell().setPaddingRight(10);
		houseInfo.getDefaultCell().setPaddingTop(5f);
		houseInfo.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
		houseInfo.getDefaultCell().setPaddingBottom(25f);
		houseInfo.getDefaultCell().setLeading(9f, 0f);
		houseInfo.addCell(new Paragraph(p.disclaimer, disclaimerFont));

		pageTable.addCell(houseInfo);
		/* add the home plan */
		pageTable.getDefaultCell().setPaddingTop(4);
		pageTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		houseInfo = new PdfPTable(2);
		houseInfo.setWidths(new int[] { 46, 45 });
		houseInfo.getDefaultCell().setBorder(0);
		houseInfo.getDefaultCell().setPadding(0);
		houseInfo.getDefaultCell().setPaddingRight(15f);
		houseInfo.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
		houseInfo.getDefaultCell().setColspan(2);

		if ((i = ExistingPropertyPDFUtil.createImage(p.planImage)) != null) {
			houseInfo.addCell(i);
		} else {
			houseInfo.addCell(new Paragraph("Sorry, the Floor Plan image is not available"));
		}

		houseInfo.getDefaultCell().setColspan(1);
		houseInfo.getDefaultCell().setPaddingRight(0);
		houseInfo.addCell(ExistingPropertyListFooter.getRepParagraph(p.repName, p.repMobile, p.repPhone, p.repEmail));
		houseInfo.addCell(ExistingPropertyListFooter.getLocationParagraph(p.locName, p.locStreet, p.locCity, p.displayHours));
		pageTable.getDefaultCell().setPaddingBottom(23.5f);
		pageTable.addCell(houseInfo);
		document.add(pageTable);
	}

	static final BaseColor priceGrey = new BaseColor(68, 68, 70); // #444446
	static final BaseColor featureGrey = new BaseColor(102, 102, 102); // #666666
	static final BaseColor bottomLeftGrey = new BaseColor(53, 52, 53); // #353435

	static final BaseColor featureBG = new BaseColor(238, 239, 241); // #EEEFF1
	
	private static final Font lotPlanFontBlack = FontFactory.getFont("Arial", 14f, Font.BOLD, BaseColor.BLACK);
	
	/* use for area / stage nmae ie (512m .. ) Stage 11 */
	static final Font lotDetailFont = FontFactory.getFont("Arial", 11.0f, BaseColor.BLACK);
	/* use for little 2 in m2 */
	static final Font lotDetailFontsmall = FontFactory.getFont("Arial", 6f, BaseColor.BLACK);

	static final Font spacerFont = FontFactory.getFont("Arial", 3f, BaseColor.WHITE);

	/* use for estate and suburb ie Fairview / Mernda */
	static final Font locationFont = FontFactory.getFont("Arial", 13.5f, BaseColor.BLACK);

	/* FIXED PRICE HOUSE .. etc */
	static final Font subHeader = FontFactory.getFont("Arial", 26.5f, BaseColor.WHITE);
	/* package price */
	static final Font singlePriceFont = FontFactory.getFont("Arial", 33.5f, Font.BOLD, BaseColor.WHITE);

	/* home description, package inclusions */
	static final Font packageDetailFont = FontFactory.getFont("Arial", 9.42f, BaseColor.WHITE);
	/*
	 * not referenced directly, used within the Range enum but here to keep settings in the same
	 * place
	 */
	static final Font packageInclusionsHeader = FontFactory.getFont("Arial", 12f, Font.BOLD, BaseColor.WHITE);

	static final Font disclaimerFont = FontFactory.getFont("Arial", 6.0f, BaseColor.WHITE);

	// private static final DecimalFormat AREA_FMT = new
	// DecimalFormat("#.##m2");
	// private static final DecimalFormat LENGTH_FMT = new
	// DecimalFormat("#.##m");

	public String getShortText(String inputtext, int size) {
		if (inputtext.length() > size) {
			inputtext = inputtext.substring(0, size);
			inputtext = inputtext + "...";
		}
		return (inputtext);
	}

	void insertPage(PdfWriter writer, String file) throws Exception {
		PdfReader reader = new PdfReader(file);
		PdfContentByte cb = writer.getDirectContent();
		PdfImportedPage page = writer.getImportedPage(reader, 1);
		cb.addTemplate(page, 0, 0);
	}

	Paragraph getArea(String prefix, String s) {
		return getArea(prefix, s, lotDetailFont, lotDetailFontsmall);
	}

	Paragraph getArea(String prefix, String s, Font font, Font smallFont) {
		return addArea(new Paragraph(), prefix, s, font, smallFont);
	}

	Paragraph addArea(Paragraph p, String prefix, String s, Font font, Font smallFont) {
		p.add(new Chunk(prefix, font));
		p.add(new Chunk(" (", font));
		p.add(new Chunk(s, font));
		p.add(new Chunk("m", font));
		Chunk c = new Chunk("2", smallFont);
		c.setTextRise(4.0f);
		p.add(c);
		p.add(new Chunk(")", font));
		return (p);
	}

	public SinglePackage getPackage(HttpServletRequest request, PropertyEntity property) {
		// PropertyEntity property =
		// PropertyFactory.getPropertyEntity(request,productID);
		if (property == null) {
			throw new RuntimeException("Product was not found");
		}
		if (!property.isPropertyType(PropertyType.ExistingProperty)) {
			throw new RuntimeException("Property [" + property.getPropertyType() + "] was not an Existing Property");
		}
		
		UserBean tmpUser = UserService.getInstance().getSimpleUser(property.getConnection(), "F180D2D08404F1241723606192C82747");
		//ExistingProperty product = (ExistingProperty) property;
		PlanService.ExistingProperty product = new PlanService().getExistingProperty(property.getConnection(), tmpUser, property.getString(PRODUCTID));
		// 02-Oct-2012 NI: PlanService.ExistingProperty product = new PlanService().getExistingProperty(request, property.getString(PRODUCTID));

		final SinglePackage p = new SinglePackage();
		p.filename = StringUtil.ToAlphaNumeric(product.Name);
		if (StringUtils.isBlank(p.filename)) {
			p.filename = "flyer";
		}
		p.heroImage = PropertyEntity.resizeImage(product.ImagePath, 605, 425, PropertyEntity.ResizeOption.Crop, dev);
		if (StringUtil.isBlankOrEmpty(p.heroImage))
		{
			p.heroImage = "";
		}	
		
		p.planImage = PropertyEntity.resizeImage(product.ThumbnailImagePath, 705, 650, PropertyEntity.ResizeOption.Pad, dev, true, 0, 0);
		if (StringUtil.isBlankOrEmpty(p.planImage))
		{
			p.planImage = "";
		}	
		
		p.projectName = product.Name;
		
		p.name = product.Name;
		
		
		String existingProdHeading = "";
		String dripdetail = "";
		String inclusions = "";
		String disclaimer = ""; 
				
		if (!StringUtil.isBlankOrEmpty(product.PlanProductID))
		{
				p.price = property.getDripText(request, getPdfSimpleName(), "NetAmount"); // Internal Drip - Drip Price.
				existingProdHeading = property.getDripText(request, getPdfSimpleName(), "1");  // Drip 01 - Existing Product Heading
				dripdetail = property.getDripText(request, getPdfSimpleName(), "2");  // Drip 02 - Design Details / Marketing description.
				inclusions = property.getDripText(request, getPdfSimpleName(), "3");   // Drip 03 - Inclusions Drip
				disclaimer = property.getDripText(request, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip
		}
		else if (!StringUtil.isBlankOrEmpty(product.LotProductID))
		{
				p.price = property.getDripText(request, getPdfSimpleName(), "NetAmount"); // Internal Drip - Drip Price.
				existingProdHeading = property.getDripText(request, getPdfSimpleName(), "1");  // Drip 01 - Existing Product Heading
				dripdetail = property.getDripText(request, getPdfSimpleName(), "2");  // Drip 02 - Design Details / Marketing description.
				inclusions = property.getDripText(request, getPdfSimpleName(), "3");   // Drip 03 - Inclusions Drip
				disclaimer = property.getDripText(request, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip
		}
		else
		{
				p.price = property.getDripText(request, getPdfSimpleName(), "NetAmount"); // Internal Drip - Drip Price.
				existingProdHeading = property.getDripText(request, getPdfSimpleName(), "1");  // Drip 01 - Existing Product Heading
				dripdetail = property.getDripText(request, getPdfSimpleName(), "2");  // Drip 02 - Design Details / Marketing description.
				inclusions = property.getDripText(request, getPdfSimpleName(), "3");   // Drip 03 - Inclusions Drip
				disclaimer = property.getDripText(request, getPdfSimpleName(), "4"); // Drip 04 - Disclaimer Drip
		}	
			
		
		if(StringUtils.isBlank(p.price)) 
		{
			p.price = ExistingPropertyPDFUtil.getFormattedAmount(product.TotalCost);
		}
		if( p.price == null ) 
		{
			p.price = "";
		}

		if(StringUtils.isNotBlank(existingProdHeading)) {
			p.existingProdHeading = existingProdHeading;
		}
		
		if(StringUtils.isNotBlank(dripdetail)) {
			p.designDetail = dripdetail;
		}
		
		if(StringUtils.isNotBlank(inclusions)) {
			p.inclusions = inclusions.split("\n");
		}

		p.disclaimer = disclaimer;
		if (StringUtil.isBlankOrEmpty(p.disclaimer))
			p.disclaimer = "";
		
		
		
		if (product.Address != null)
		{	
			p.Street			= product.Address.Street;
			p.Street2		= product.Address.Street2;
			p.City				= product.Address.City;
			p.State			= product.Address.State;
			p.Postcode	= product.Address.Postcode;
		}
		
		// p.houseInfo.getDefaultCell().setPaddingTop(13f);
		//List<ProductDetail> ProductDetails = product.ProductDetails; 
		//logger.debug("Size of details array: {}", ProductDetails.size());
		
		setProductDetails(request, property.getString(PRODUCTID), p);
		
		
		GenRow repDetails = StringUtils.isBlank(request.getParameter("RepUserID")) ? property.getSalesRep() : ExistingPropertyPDFUtil.getRep(request.getParameter("RepUserID"));
		p.repName = repDetails.getString("FirstName") + " " + repDetails.getString("LastName");
		p.repMobile = repDetails.getString("Mobile");
		p.repPhone = repDetails.getString("Phone");
		p.repEmail = ExistingPropertyPDFUtil.getRepEmail(repDetails.getString("Email"));

		if (StringUtils.isNotBlank(request.getParameter("DisplayID"))) {
			GenRow displayDetails = ExistingPropertyPDFUtil.getDisplayLocation(request.getParameter("DisplayID"));
			p.locName = displayDetails.getString("Name");
			p.locStreet = displayDetails.getString("Street");
			p.locCity = displayDetails.getString("City");
			p.displayHours = ExistingPropertyPDFUtil.getDisplayHours(displayDetails.getString("ProductID"));
		}

		//p.promoImages = property.getDripImage(request, getPdfSimpleName(), "2"); // Drip 02 - Value Added Image
		p.dripImg1 = property.getDripImage(request,getPdfSimpleName(), "1"); // Drip 02 - Value Added Image

		return p;
	}
	
		
	public void getFullAddress()
	{
	}
	
	public void setProductDetails(HttpServletRequest request, String prodID, SinglePackage p)
	{
			GenRow details = null;

			details = new GenRow();
			details.setTableSpec("properties/ProductDetails");
			details.setRequest(request);
			details.setParameter("-select","*");
			details.setColumn("ProductID", prodID);
			details.sortBy("SortOrder",0);
			details.sortBy("DetailsType", 1);

			if (!StringUtil.isBlankOrEmpty(prodID))
			{			
				details.doAction("search");
				details.getResults(true);
			}
			
			Map<String, Object> detailMap = new HashMap<String, Object>();
			
			String tmpKey = "";
			while(details.getNext()) { 
				String type = details.getColumn("DetailsType").replaceAll(" ","");

				for(int i=0; i<details.getTableSpec().getFieldsLength(); i++) { 
					String field = details.getTableSpec().getFieldName(i);
					Object o = details.getObject(field);
					if(o != null) { 
						
						tmpKey = (type+"."+field).trim();
						detailMap.put(tmpKey, String.valueOf(o));
						//out.println("<!--  key:" + tmpKey + ",value:" +  String.valueOf(o) + "-->");
						//out.println("<!-- " + String.format("adding %1$s = %2$s",field, String.valueOf(o)) + "-->");
					}
				}
			}
			details.close();

			
			String strGarage = "0";
			if (detailMap.get("Garage.NumberOf") != null)
				strGarage = detailMap.get("Garage.NumberOf").toString();
			p.carParks = new Double(strGarage).intValue();

			String strBedrooms = "0";
			if (detailMap.get("Bedrooms.NumberOf") != null)
				strBedrooms = detailMap.get("Bedrooms.NumberOf").toString();
			p.bedrooms = new Double(strBedrooms).intValue();

			String strBathrooms = "0";
			if (detailMap.get("Bathrooms.NumberOf") != null)
				strBathrooms = detailMap.get("Bathrooms.NumberOf").toString();
			p.bathrooms = new Double(strBathrooms).intValue();
	}
	
	
	

	public String getPdfSimpleName() {
		return "Real Estate Detail";
	}

	
	public void setMargins(Document document) {
		document.setMargins(0, 0, 0, 0);
	}
}