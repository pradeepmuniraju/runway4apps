package com.sok.runway.crm.reports;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.DateFormat; 
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.StringUtil;
import com.sok.runway.ReportBean;
import com.sok.runway.CustomReportBean;

import org.apache.commons.lang.StringUtils;
/**
 * ReportBean designed to generate custom tabular data combining several reportspecs to compare
 * two different status' and the state of their parent records. 
 * 
 * Alternatively to compare 'record creation' with a particular status. 
 * 
 * @author Michael Dekmetzian
 *
 */
public class StatusToStatusDurationReportBean extends CustomReportBean {
	private static final long serialVersionUID = 1L;
	Logger logger = LoggerFactory.getLogger(StatusToStatusDurationReportBean.class);
	
	ReportBean timeTaken = null;
	ReportBean otherStatus = null;
	
	//can't be static as DateFormat is not thread-safe.
	private DateFormat monthFormat = new SimpleDateFormat("MMMM");
	private DateFormat dayFormat = new SimpleDateFormat("EEE dd");
	private enum RangeType { DAY, WEEK, MONTH, QUARTER, NONE	};
	private enum Entity { CONTACT, PROCESS, PRODUCT, PAYMENT; 
		public void setEntityParams(ReportBean bean) {
			bean.put("entity",this.name());			
		}
	};
	
	private static final String rangeBlock = "<span title=\"%1$s\">%2$s</span>";
	public String formatRange(String title, Date d) {
		if(d == null || range == null) return title;
			switch(range) {
			case DAY: 
				return String.format(rangeBlock, title, dayFormat.format(d));
			case WEEK:
				return title;
			case MONTH:
				return String.format(rangeBlock, title, monthFormat.format(d));
			case QUARTER:
				return title;
			default:
				return title;
		}
	}
	
	public TitleCell formatRangeTitle(String title, Date d) {
		if(d == null || range == null) return new TitleCell(title);
			switch(range) {
			case DAY: 
				return new TitleCell(dayFormat.format(d), title);
			case MONTH:
				return new TitleCell(monthFormat.format(d), title);
			case WEEK:				
			case QUARTER:
			default:
				return new TitleCell(title);
		}
	}
		
	private RangeType range = RangeType.NONE;
	private Entity entity = Entity.CONTACT;
	
	public StatusToStatusDurationReportBean() { 
		super();
		put(ReportBean.reportspec,"ReportSetStatusToStatusDuration");
		put("-swapsummary","false");
		put("Interval", "monthly");
		timeTaken = new SpecialReportBean();
		timeTaken.put(ReportBean.reportspec,"ReportSetStatusToStatusDurationAverage");
		timeTaken.put(ReportBean.reportspecright, "ReportSetStatusToStatusChanged");
	}
	
	public void clear() {
		
		super.clear();
		put(ReportBean.reportspec,"ReportSetStatusToStatusDuration");
		put("-swapsummary","false");
		
		timeTaken = new SpecialReportBean();
		timeTaken.put(ReportBean.reportspec,"ReportSetStatusToStatusDurationAverage");
		timeTaken.put(ReportBean.reportspecright, "ReportSetStatusToStatusChanged");
	}
	
	public void setRequest(HttpServletRequest request) {
		super.setRequest(request);
		timeTaken.setRequest(request);
	}
	
	public void setDebug(boolean debug) {
		super.setDebug(debug);
		timeTaken.setDebug(debug);
	}
	
	private static final String[] keysToCopy = new String[]{"EntryStatusID", "CompletionStatusID", "GroupID", "RepUserID", "-begin", "-end","contextPath","RightGroupID","CommissionTemplateID","entity"};
	
	public void setup() {
		put("-cinterval",null);
		put("-rowtrigger","");
		put("-righttrigger","");
		put("-rowtrigger","");
		put("datefield","");
		range = RangeType.NONE;
		entity = Entity.CONTACT;
		if("Product".equals(get("Entity"))) { 
			entity = Entity.PRODUCT;
			put(ReportBean.reportspecright,"ReportSetProductStatus");
		} else if("Process".equals(get("Entity"))) { 
			entity = Entity.PROCESS;
			put(ReportBean.reportspecright,"ReportSetProcessStatus");
			put("datefield", "OpportunityStatus.CreatedDate");
			put("searchdatefield", "OpportunityStatus%23R.CreatedDate");
		} else if("Payment".equals(get("Entity"))) { 
			entity = Entity.PAYMENT; 
			put(ReportBean.reportspecright,"ReportSetPayments");
			put("PaymentDate", "CommissionPayments.PaidOnDate" );
			put("StatusValue","paid");
			put("PaymentCountField","Opportunities.OpportunityID");
		} else {
			entity = Entity.CONTACT;
			put(ReportBean.reportspecright,"ReportSetContactStatusHistory");
		}
		
		entity.setEntityParams(this);
		entity.setEntityParams(timeTaken);
		
		String iv = getString("Interval");
		if("quarterly".equals(iv)) {
			put("-cinterval","quarterrange");
			range = RangeType.QUARTER;
		} else if ("monthly".equals(iv)) {
			put("-cinterval","monthrange");
			range = RangeType.MONTH;
		} else if ("weekly".equals(iv)) {
			put("-cinterval","weekrange");
			range = RangeType.WEEK;
		} else if ("daily".equals(iv)) {
			put("-cinterval","dayrange");
			range = RangeType.DAY;
		} else { 
			put("-cinterval","monthrange");
			range = RangeType.MONTH;
			put("Interval", "monthly");
		}

		if(getString("-cinterval").length()>0) { 
			final String cinterval = getString("-cinterval"); 
			setParameter("-coltrigger","c" + cinterval);
			setParameter("-rowtrigger", cinterval);
			setParameter("-righttrigger","");
			
			timeTaken.setParameter("-rowtrigger", cinterval);
		}
		final String statusID = entity == Entity.CONTACT ? "GroupStatusID" : (entity == Entity.PAYMENT ? "PaymentDescription" : "StatusID");
		
		remove("ContactGroupID");
		
		if(getString("EntryStatusID").length()>0) { 
			put(statusID,getString("EntryStatusID"));
			remove("ContactsSearch");
			remove("ContactsQuery");
		} else {
			switch(entity) {
				case PRODUCT: 
					put(ReportBean.reportspecright,"ReportSetProduct");
					break; 
				case CONTACT: 
					put(ReportBean.reportspecright,"ReportSetContactCreated");
					/*
					put("ContactsSearch",new StringBuilder(410)
							.append(" NOT EXISTS (SELECT ExStatusList.StatusID FROM StatusList as ExStatusList ") 
							.append("INNER JOIN GroupStatus AS ExGroupStatus ON ExStatusList.StatusID = ExGroupStatus.StatusID ") 
							.append("INNER JOIN ContactGroups AS ExContactGroups ON ExGroupStatus.ContactGroupID = ExContactGroups.ContactGroupID ")
							.append("WHERE ExStatusList.Status = 'Linked Contact' AND Contacts.ContactID = ExContactGroups.ContactID AND ExContactGroups.GroupID = '")
							.append(get("GroupID")).append("' )").toString()); 
					//put("ContactsSearch"," not exists (select StatusList.StatusID from StatusList inner join GroupStatus as ExGroupStatus on StatusList.StatusID = ExGroupStatus.StatusID where Status = 'Linked Contact' and ContactGroups.ContactGroupID = ExGroupStatus.ContactGroupID )");
					put("ContactsQuery","ContactContactGroups%23EX.ContactGroupGroupStatus.ContactGroupStatusList.Status=Linked Contact&-ContactContactGroups%23EX=omit");
					*/
					break;
				case PROCESS: 
					put("datefield", "Opportunities.CreatedDate");
					put("searchdatefield", "CreatedDate");
					put(ReportBean.reportspecright,"ReportSetProcess");
					break;
				case PAYMENT: 
					put("PaymentDate", "Opportunities.CreatedDate" );
					break;
					default: 
				}	
		}
		
		put(_rowtrigger, get(rowtrigger));
		put(_coltrigger, get(coltrigger));
		put(_righttrigger, get(righttrigger));
		
		put("RightGroupID",get("GroupID"));
		for(String key : keysToCopy) {
			logger.debug("Putting value [{}] in [{}]", get(key), key); 
			timeTaken.put(key, get(key));
		}
		super.setup();
		timeTaken.setup();
		
		com.sok.Debugger.getDebugger().debug("Following output is the StatusToStatusReportBean");
		com.sok.Debugger.getDebugger().debug(this.toString());
		com.sok.Debugger.getDebugger().debug("Following output is the SecondaryBean");
		com.sok.Debugger.getDebugger().debug(timeTaken.toString()); 
	}
	
	@Override
	protected void generateSearchBeanStatement(String searchtype,
			String constraintRelation) {
		super.generateSearchBeanStatement(searchtype, constraintRelation);
		/* if values are null, will remain as null */
		timeTaken.put(searchtype + "Search",get(searchtype + "Search"));
		timeTaken.put(searchtype + "Query",get(searchtype + "Query"));
		
		/* we want to set this only in the contact search beans, which are in the main reportbean */
		if(!isSet("EntryStatusID") && entity == Entity.CONTACT) {
			StringBuilder search = new StringBuilder(410)
				.append(" NOT EXISTS (SELECT ExStatusList.StatusID FROM StatusList as ExStatusList ") 
				.append("INNER JOIN GroupStatus AS ExGroupStatus ON ExStatusList.StatusID = ExGroupStatus.StatusID ") 
				.append("INNER JOIN ContactGroups AS ExContactGroups ON ExGroupStatus.ContactGroupID = ExContactGroups.ContactGroupID ")
				.append("WHERE ExStatusList.Status = 'Linked Contact' AND Contacts.ContactID = ExContactGroups.ContactID AND ExContactGroups.GroupID = '")
				.append(get("GroupID")).append("' )");
			StringBuilder query = new StringBuilder("ContactContactGroups%23EX.ContactGroupGroupStatus.ContactGroupStatusList.Status=Linked Contact&-ContactContactGroups%23EX=omit");
			
			if(isSet(searchtype + "Search")) {
				search.append(" AND ");
				search.append(getString(searchtype + "Search"));
			}
			if(isSet(searchtype + "Query")) {
				query.append("&");
				query.append(getString(searchtype + "Query"));
			}
			setColumn(searchtype + "Search",search.toString());
			setColumn(searchtype + "Query",query.toString());
		}
	}

	@Override
	public String getReportType() {

		switch(entity) { 
			case CONTACT:
				return "Contacts";
			case PRODUCT: 
				return "Products";
			case PROCESS:
			case PAYMENT:
				return "Opportunities";
			default:
				return null;
		}
	}
	
	@Override
	public void doAction() {
		com.sok.Debugger.getDebugger().debug("Following output is the StatusToStatusReportBean");
		super.doAction();
		com.sok.Debugger.getDebugger().debug("Following output is the SecondaryBean");
		timeTaken.doAction();
	}
	
	@Override
	public String getError() { 
		return super.getError() + timeTaken.getError();
	}
	
	public ReportBean getTT() { return timeTaken; }
	
	@Override
	public List<List<ReportCell>> getChartData() {
		final BlankCell blankCell = new BlankCell();
		int cols = this.getColumnCount();
		int rows = this.getRowCount();
		//int right = this.getRightCount();
		
		NumberFormat nf = NumberFormat.getPercentInstance();
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		
		List<List<ReportCell>> chartData = new LinkedList<List<ReportCell>>();
		/* produce a an array of tabular type data */
		List<ReportCell> rowData = null;
		
		if (this.hasRowTitles()) {
			/* first row */
			rowData = new LinkedList<ReportCell>();
			chartData.add(rowData);
			rowData.add(blankCell);

			/* row titles */
			for (int j = 0; j < rows; j++) {
				rowData.add(formatRangeTitle(this.getRowTitle(j), getDate(rowvars[1][j])));
			}
			rowData.add(new TitleCell(this.getSummaryLabel()));
			
			rowData.add(blankCell);
			rowData.add(blankCell);
			rowData.add(blankCell);
			rowData.add(blankCell);
			rowData.add(blankCell);
			rowData.add(blankCell);
		} 
		
		if(rows != cols) { 
			logger.debug(String.format("rows [%2$d] and cols [%3$d] are equal [%1$b]", rows == cols, rows, cols));
		}
		
		final int tTotal = 0; 
		final int rTotal = 1;
		final int oTotal = 2;
		final int lTotal = 3;
		int[] totals = { 0,0,0,0 };
		double dAv = 0d;
		
		/* do row for contact status from column data */
		if(getReportSpec() != null) { 
			// ie the ContactStatusReport is set up.
			rowData = new LinkedList<ReportCell>();
			chartData.add(rowData);
			rowData.add(blankCell);
			
			for (int j = 0; j < rows; j++) {
				rowData.add(buildCellValue( j, 0, ListType.Right));
			}
			rowData.add(new TotalCell(this.getRightRowSummary(0)));
			
			rowData.add(new TitleCell("Rem","Number of records which have remained at the initial status"));
			rowData.add(new TitleCell("Rem %","Percentage of records which have remained at the initial status").setStyle("percentSpan"));
			rowData.add(new TitleCell("Other", "Number of records which have since to another status"));
			rowData.add(new TitleCell("Other %","Percentage of records which have since to another status").setStyle("percentSpan"));
			rowData.add(new TitleCell("Total","The total number of records for this period"));
			rowData.add(new TitleCell("Avg Time Taken", "The average time taken to go from the initial state to the completion status"));
		}
		
		/* do custom report data for status duration, with total on right. */
		int other = 0;
		int rowTotal = 0;
		for(int i=0; i< rows; i++) { 
			rowData = new LinkedList<ReportCell>();
			chartData.add(rowData);
			
			//rowData.add(getRowTitle(i));
			rowData.add(formatRangeTitle(this.getRowTitle(i), getDate(rowvars[1][i])));
			int val = 0; 
			for(int j=0; j<cols; j++) {
				if(i<=j) { 
				rowData.add(buildCellValue(i, j, ListType.Col));
				try {
					val+= Integer.parseInt(this.getValue(i,  j, ListType.Col));
				} catch (NumberFormatException e) { 
					logger.error("failed to parse number " + getValue(i, j, ListType.Col), e);
				}
				} else {
					rowData.add(blankCell);
				}
			}
			rowData.add(new TotalCell(this.getColumnSummary(i)));
			
			try {
				rowTotal = Integer.parseInt(this.getColumnSummary(i));
				totals[tTotal]+=rowTotal;
			} catch (Exception e) { rowTotal = 0; }
			
			//logger.debug(getRowSummary(i) + " vs " + val);
			
			try {
				other = Integer.parseInt(timeTaken.getItem(i, 0, ListType.Right));
				totals[oTotal]+= other;
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				other = 0;
			}
			
			int c = 4;
			try {
				String cs = StringUtil.replace(getRightSummary(i),",","");
				int csi = new Double(cs).intValue();
				int rem = csi - val - other;
				if(rem < 0) rem = 0;
				rowData.add(new ValueCell(String.valueOf(rem)));
				totals[rTotal]+= rem;
				c--;
				
				double remp = (rem == 0) ? 0 : ((double)rem)/csi;
				rowData.add(new ValueCell(nf.format(remp)).setStyle("percentSpan"));
				//rowData.add(new StringBuilder("<span class=\"percentSpan\">").append(nf.format(remp)).append("</span>").toString()); 
				
				c--;
				rowData.add(new ValueCell(String.valueOf(other)));
				c--;
				double otherp = other == 0 ? 0 : ((double)other)/csi;  
				//rowData.add(new StringBuilder("<span class=\"percentSpan\">").append(nf.format(otherp)).append("</span>").toString());
				rowData.add(new ValueCell(nf.format(otherp)).setStyle("percentSpan"));
				
			} catch (Exception e) {
				for(int ei = 0; ei< c; ei++) rowData.add(new ErrorCell(e));
			}
			
			rowData.add(buildCellValue( i, 0, ListType.Right));
			try { 
				totals[lTotal] += Integer.parseInt(this.getItem(i, 0, ListType.Right));
			} catch (Exception e) { }
			
			/* add the time taken data onto the end of the row */
			try {
				String v = StringUtils.trimToNull(buildCellHTML(timeTaken, i, 0, ListType.Col));
				rowData.add(v != null ? new ValueCell(v +" days") : blankCell);
				
				try {
					dAv += (Double.parseDouble(timeTaken.getItem(i,0,ListType.Col)) * rowTotal);
				} catch (NumberFormatException nfe) { } 
				
			} catch (Exception e) { 
				logger.error("failed outputting avg", e);
				//ActionBean.writeStackTraceToWriter(e, System.err);
				rowData.add(new ErrorCell(e));
			}
		}
		rowData = new LinkedList<ReportCell>();
		chartData.add(rowData);
		rowData.add(blankCell);
		for(int i=0; i<cols; i++) { 
			rowData.add(blankCell);
		}
		rowData.add(new TotalCell(String.valueOf(totals[tTotal])));
		rowData.add(new ValueCell(String.valueOf(totals[rTotal])));
		rowData.add(blankCell);
		rowData.add(new ValueCell(String.valueOf(totals[oTotal])));
		rowData.add(blankCell);
		rowData.add(new ValueCell(String.valueOf(totals[lTotal])));
		if(dAv > 0 && totals[tTotal] > 0) { 
			try { 
			Double df = dAv/(double)totals[tTotal]; 
			//StringBuilder av = new StringBuilder(50).append("dav [").append(dAv).append("] rows [").append(rows).append("] val [").append(df).append("] days");
			//System.out.println(av.toString());
			rowData.add(new ValueCell(String.format("%1$d days", Math.round(df))));
			} catch (Exception e) { 
				try {
					com.sok.framework.ActionBean.writeStackTrace(e, System.out);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				rowData.add(new ErrorCell(e));
			}
		}
		return chartData;
	}
	
	/**
	 * We want to override the monthly intervals 
	 */
	@Override
	public String getNextDate(Calendar cal, int type) {
		if(type != Calendar.MONTH) return super.getNextDate(cal,type);
		
			StringBuffer sb = new StringBuffer();
			sb.append(getDateString(cal));

			if (type != Calendar.DAY_OF_MONTH) {
				sb.append(" - ");
			}
			cal.add(type, 1);
			cal.set(Calendar.DAY_OF_MONTH, 1);

			if (type != Calendar.DAY_OF_MONTH) {
				cal.add(Calendar.DAY_OF_MONTH, -1);
				sb.append(getDateString(cal));
				cal.add(Calendar.DAY_OF_MONTH, 1);
			}

			return (sb.toString());
		} 
	
	public static class SpecialReportBean extends CustomReportBean {
		/**
		 * We want to override the monthly intervals 
		 */
		@Override
		public String getNextDate(Calendar cal, int type) {
			if(type != Calendar.MONTH) return super.getNextDate(cal,type);
			
				StringBuffer sb = new StringBuffer();
				sb.append(getDateString(cal));

				if (type != Calendar.DAY_OF_MONTH) {
					sb.append(" - ");
				}
				cal.add(type, 1);
				cal.set(Calendar.DAY_OF_MONTH, 1);

				if (type != Calendar.DAY_OF_MONTH) {
					cal.add(Calendar.DAY_OF_MONTH, -1);
					sb.append(getDateString(cal));
					cal.add(Calendar.DAY_OF_MONTH, 1);
				}

				return (sb.toString());
			} 
	}
}
