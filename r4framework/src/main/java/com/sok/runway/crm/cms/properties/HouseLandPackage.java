package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

public class HouseLandPackage extends PropertyEntity {

	private static final Logger logger = LoggerFactory.getLogger(HouseLandPackage.class);
	
	final PropertyType propertyType = PropertyType.HouseLandPackage;
	static final String VIEWSPEC = "properties/ProductHouseAndLandEntityView";
	private HomePlan plan = null;
	private Land land = null;
	private Facade facade = null;
	private String estateStreet =  null;
	private String estateCity =  null;
	
	static final String LOTPRODUCTID = "LotProductID";
	static final String PLANPRODUCTID = "PlanProductID";
	static final String LINKEDPRODUCTID = "LinkedProductID";
	static final String ESTATEID = "EstateProductID";
	
	public HouseLandPackage(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public HouseLandPackage(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	
	public HouseLandPackage(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	@Override
	public HouseLandPackage load(String productID) { 
		return (HouseLandPackage)super.load(productID);
	}
	
	@Override
	public Object get(String key) { 
		if("lotproduct".equals(key)) return getLand();
		if("planproduct".equals(key)) return getHomePlan();
		return super.get(key);
	}
	
	@Override
	public void clear() { 
		land = null;
		plan = null;
		super.clear();
	}
	
	public String getName() { 
		if(getHomePlan() != null) { 
			return plan.getName();
		}
		return ActionBean._blank;
	}
	
	public String getLotNumber() { 
		if(getLand() != null) { 
			return land.getLotNumber();
		}
		return ActionBean._blank;
	}
	
	public String getLotName() { 
		if(getLand() != null) { 
			return land.getLotName();
		}
		return ActionBean._blank;
	}
	public double getLotCost() {
		if(getLand() != null) { 
			return land.getDouble("Cost"); 
		}
		return 0;
	}
	public double getLotTotalCost() {
		if(getLand() != null) { 
			return land.getDouble("TotalCost"); 
		}
		return 0;
	}
	
	public double getLotSize() { 
		if(getLand() != null) { 
			return land.getArea(); 
		}
		return 0d; 
	}
	
	public double getLotWidth() { 
		if(getLand() != null) { 
			return land.getLotWidth(); 
		}
		return 0d; 
	}
	
	public double getLotDepth() { 
		if(getLand() != null) { 
			return land.getLotDepth();
		}
		return 0d; 
	}
	
	public String getLotType() { 
		if(getLand() != null) { 
			return land.getString("ProductSubType");
		}
		return null; 
	}
	
	public String getPublishingStatus() {		
		return StringUtils.defaultIfEmpty(getString("PublishingStatus"),"To be approved");
	}
	
	public GenRow getDisplay() { 		
		if(getHomePlan() != null) { 
				if(getLand() != null) {
					Estate e = land.getEstate();
					if(e != null) { 
						return plan.getDisplay(e.getString(PRODUCTID));
					}
				}
				return plan.getDisplay();
		}
		return null;
	}
	
	public GenRow getSalesRep() {
		if(isSuccessful() && isSet(PRODUCTID)) {
			GenRow row = new GenRow(); 
			row.setConnection(getConnection());
			row.setTableSpec("ProductSecurityGroups");
			row.setParameter("ProductID", getString(PRODUCTID));
			row.setParameter("-select0","RepUserID");
			row.setParameter("-select1","ProductSecurityGroupRep.FirstName");
			row.setParameter("-select2","ProductSecurityGroupRep.LastName"); 
			row.setParameter("-select3","ProductSecurityGroupRep.Mobile"); 
			row.setParameter("-select4","ProductSecurityGroupRep.Phone");
			row.setParameter("-select5","ProductSecurityGroupRep.Email");
			row.doAction(GenerationKeys.SELECTFIRST);
			
			if(row.isSuccessful() && row.isSet("FirstName")) {
				return row;
			} 
		}
		return super.getSalesRep();
	}
	
	public String getStageName() { 
		if(getLand() != null) { 
			return land.getStageName();
		}
		return null;
	}

	public Stage getStage() { 
		if(getLand() != null) { 
			return land.getStage();
		}
		return null;
	}
	
	public String getEstateName()  {
		if(getLand() != null) { 
			return land.getEstateName();
		}
		return null;
	}
	
	public String getEstateImage() { 
		if(getLand() != null) { 
			return land.getEstateImage();
		}
		return null;
	}
	
	public String getEstateWebURL() { 
		if(getLand() != null) { 
			return land.getEstateWebURL();
		}
		return null;
	}
	
	public String getEstateID() { 
		if(getLand() != null) { 
			return land.getEstateID();
		}
		return null;
	}
	
	public String getEstateLocationId(){
		if(getLand() != null) { 
			return land.getEstateLocationID();
		}
		return null;
	}
	
	
	public String getMarketingStatus() { 
		if(getLand() != null) { 
			return land.getString("MarketingStatus");
		}
		return null;
	}
	
	public String getLandPrice() { 
	    if(getLand() != null) { 
    		return land.getPrice();
	    }
	    return null;
	}
	
	public String getCost() { 
		String cost = getColumn("TotalCost");
		if(cost.indexOf(".")>0) { 
			return cost.substring(0, cost.indexOf("."));
		}
		return cost;
	}
	
	public String getDripTotalCost() { 
		String cost = getColumn("DripResult");
		// if the price is $0.00 then use the TotalCost
		if (cost.replaceAll("[0\\.\\$]","").length() == 0) cost = getColumn("TotalCost");
		if(cost.indexOf(".")>0) { 
			return cost.substring(0, cost.indexOf("."));
		}
		return cost;
	}
	
	@Override
	public String[] getDripProductIDs() {
		return new String[]{getString(PRODUCTID), getString(PRODUCTID), getString(PRODUCTID)};
	}

	public String getAvailableDate() {
	    	if(getHomePlan() != null) { 
	    	    	if(plan.getDate("AvailableDate")!= null ){
        	    		SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");		
        	    		String availDate = sdf.format(plan.getDate("AvailableDate"));
        	    		return availDate;
	    	    	}
	    	}
	    	return null;
	}

	public String getLandAvailableDate() {
    	if(getLand() != null) { 
    	    	if(land.getDate("AvailableDate")!= null ){
    	    		SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");		
    	    		String availDate = sdf.format(land.getDate("AvailableDate"));
    	    		return availDate;
    	    	}
    	}
    	return null;
}

	public int getBedrooms() { 
		if(getHomePlan() != null) { 
			return plan.getInt("Bedrooms");
		}
		return -1;
	}
	
	public int getBathrooms() { 
		if(getHomePlan() != null) { 
			return plan.getInt("Bathrooms");
		}
		return -1;
	}
	
	public String getBathroomsAsString() { 
		if(getHomePlan() != null) { 
			return plan.getString("Bathrooms");
		}
		return "0";
	}
	
	public int getCarParks() { 
		if(getHomePlan() != null) { 
			return plan.getInt("CarParks");
		}
		return -1;
	}
	
	public int getStudy() { 
		if(getHomePlan() != null) { 
			return plan.getStudy();
		}
		return -1;
	}
	
	public String getStorey() { 
		if(getHomePlan() != null) { 
			return plan.getStorey();
		}
		return null;
	}
	
	public String getImage() { 
		return getString("ImagePath");
	}
	
	public String getImageDescription() { 
		return getString("ImageDescription");
	}
	
	/**
	 * This method actually returns LandWidth of the land.
	 * This LandWidth is used as Frontage in Develoepr Pdfs. This method
	 * return LandWidth from HomeDetails table #land
	 */
	public String getHomeLandWidth(){
		return getString("LandWidth");
	}
	
	public String getPlanImage() { 
		if(getHomePlan() != null) { 
			return plan.getString("ImagePath");
		}
		return null;
	}
	
	
	
	public String getPlanName() { 
		if(getHomePlan() != null) { 
			return plan.getString("Name");
		}
		return null;
	}
	public double getPlanCost() { 
		if(getHomePlan() != null) { 
			return plan.getDouble("Cost");
		}
		return 0d;
	}
	public double getPlanTotalCost() { 
		if(getHomePlan() != null) { 
			return plan.getDouble("TotalCost");
		}
		return 0d;
	}
	
	public List<String> getPlanFeatures() { 
		if(getHomePlan() != null) { 
			return plan.getFeatureList();
		}
		return Collections.emptyList();
	}
	
	public String getRangeName() {
		if(getRange() != null) {
			logger.trace("hnl have range {}", getData(PRODUCTID));
			return getRange().getString("Name");
		} else {
			logger.warn("hnl no range found {}", getData(PRODUCTID));
		}
		return null;
	}
	
	public HomeRange getRange() { 
		if(getHomePlan() != null) { 
			logger.trace("hnl have home plan");
			return plan.getHomeRange();
		}
		return null;
	}
	
	public String getFacadeName() {
		if(getFacade() != null) {
			logger.trace("hnl have facade {}", getData(PRODUCTID));
			return getFacade().getString("Name");
		} else {
			logger.warn("hnl no facade found {}", getData(PRODUCTID));
		}
		return null;
	}
	
	public String getFacadeDescription() {
		if(getFacade() != null) {
			logger.trace("hnl have facade {}", getData(PRODUCTID));
			return getFacade().getString("Description");
		} else {
			logger.warn("hnl no facade found {}", getData(PRODUCTID));
		}
		return null;
	}
	
	public Land getLand() {
		logger.trace("loading lot with id [{}]",getData(LOTPRODUCTID));
		if(land == null && isSuccessful() && isSet(LOTPRODUCTID)) {
			land = new Land(getConnection()).load( getData(LOTPRODUCTID)); //getLinkedProductID(getConnection(), getData(PRODUCTID), PropertyType.Land.getProductType()));
			if(!land.isSuccessful()) {
				logger.trace("loading lot was not sucessful with error {}", land.getError());
				land = null;
			}
		}
		return land;
	}
	
	public String getHomeName() {
		if(getHomePlan() != null) {
			return plan.getHomeName();
		}
		return null; 
	}
	
	public HomePlan getHomePlan() {
		//System.out.format("loading plan with id [%1$s]\n",getData(PLANPRODUCTID));
		if(plan == null && isSuccessful() && isSet(PLANPRODUCTID)) {
			plan = new HomePlan(getConnection()).load(getData(PLANPRODUCTID)); //getLinkedProductID(getConnection(),  getData(PRODUCTID), PropertyType.HomePlan.getProductType()));
			if(!plan.isSuccessful()) {
				plan = null;
			}
		}
		return plan;
	}
	
	public Facade getFacade() {
		if(facade == null && isSuccessful() && isSet(PRODUCTID)) {
			facade = new Facade(getConnection()).load(getData(PRODUCTID));
			if(!facade.isSuccessful()) {
				facade = null;
			}
		}
		return facade;
	}
	
	/**
	 * @author nawaz
	 * @return
	 */
	public double getHomeSizeSq() {
		
		try
		{
			if(getHomePlan() != null) { 
				return plan.getDouble("HomeSizeSq");
			}
		}
		catch(Exception ee)
		{
			logger.error("Exception occurred in getHomeSize() method. Message is: {}",ee.getMessage());
		}
		return -1;
	}
	
	/**
	 * @author nawaz
	 * @return
	 */
	public double getHomeSize() {
		
		try
		{
			if(getHomePlan() != null) { 
				return plan.getDouble("HomeSize");
			}
		}
		catch(Exception ee)
		{
			logger.error("Exception occurred in getHomeSize() method. Message is: {}",ee.getMessage());
		}
		return -1;
	}

	/**
	 * @author nawaz
	 * @return
	 */
	public double getHomeArea() {
		
		try
		{
			if(getHomePlan() != null) { 
				return plan.getDouble("HomeArea");
			}
		}
		catch(Exception ee)
		{
			logger.error("Exception occurred in getHomeArea() method. Message is: {}",ee.getMessage());
		}
		return -1;
	}

	
	/**
	 * @author nawaz
	 * @return
	 */
	public String getStreet() {
		try
		{
			if(getLand() != null) { 
				return land.getString("Street"); 
			}
		}	
		catch(Exception ee)
		{
			logger.warn("Exception occurred in getStreet() method. Message is: {}",ee.getMessage());
		}
		
		return "";
	}

	public String getAddress() {
		try
		{
			if(getLand() != null) { 
				if(StringUtils.isNotBlank(land.getString("StreetNumber"))) {
					return new StringBuilder().append(land.getString("StreetNumber")).append(", ").append(land.getString("Street")).append(", ").append(land.getString("City")).toString();
				} else {
					return new StringBuilder().append(land.getString("Street")).append(", ").append(land.getString("City")).toString();
				}
			}
		}	
		catch(Exception ee)
		{
			logger.warn("Exception occurred in getStreet() method. Message is: {}",ee.getMessage());
		}
		
		return "";
	}
	
	/**
	 * This method returns a combination of Streetnumberr and streetname or
	 * if streetnumber is not available then lotno + streetname, if lotno is available
	 * getString(Name) is the lotno.
	 * @param isLotNoAttach
	 * @return streetNumber/lotno streetName
	 */
	public String getStreet(boolean isLotNoAttach){
		StringBuilder lotAddressBuilder =  new StringBuilder();
		try
		{	
			if(getLand() != null){
				if(land.getString("StreetNumber") != null && land.getString("StreetNumber").length() > 0){
					lotAddressBuilder.append(land.getString("StreetNumber"));
				} else if(isLotNoAttach && land.getString("Name") != null && land.getString("Name").length() > 0){
					lotAddressBuilder.append("Lot " + land.getString("Name"));
				}
				
				if(land.getString("Street") != null){
					if(lotAddressBuilder.length() > 0){
						lotAddressBuilder.append(", " + land.getString("Street"));
					}else{
						lotAddressBuilder.append(land.getString("Street"));
					}
				}
				
//				if(land.getString("City") != null){
//					if(lotAddressBuilder.length() > 0){
//						lotAddressBuilder.append(", " + land.getString("City"));
//					}else{
//						lotAddressBuilder.append(land.getString("City"));
//					}
//				}
			}
		}	
		catch(Exception ee)
		{
			logger.debug("Exception occurred in getAddress() method. Message is: {}",ee.getMessage());
		}
		return lotAddressBuilder.toString(); 
	}
	
	
	public String getCity() {
		try
		{
			if(getLand() != null) { 
				return land.getString("City"); 
			}
		}	
		catch(Exception ee)
		{
			logger.warn("Exception occurred in getStreet() method. Message is: {}",ee.getMessage());
		}
		
		return "";
	}
	
	/**
	 * @author nawaz
	 * @return
	 */
	public double getLivingArea() {
		try
		{
			if(getHomePlan() != null) { 
				return plan.getDouble("LivingArea");  // TODO: Need attention here.
			}
		}
		catch(Exception ee)
		{
			logger.warn("Exception occurred in getLivingArea() method. Message is: {}",ee.getMessage());
		}

		return -1;
	}

	/**
	 * @author nawaz
	 * @return
	 */
	public double getGarage() { 
		try
		{
			if(getHomePlan() != null) { 
				return plan.getDouble("Garage");
			}
		}
		catch(Exception ee)
		{
			logger.warn("Exception occurred in getGarage() method. Message is: {}",ee.getMessage());
		}
		
		return -1;
	}
	

	/**
	 * @author nawaz
	 * @return
	 */
	public String getEstateSuburb() {
		try
		{
			if(getLand() != null) { 
				return land.getEstateSuburb();
			}
		}	
		catch(Exception ee)
		{
			logger.warn("Exception occurred in getEstateSuburb() method. Message is: {}",ee.getMessage());
		}
		
		return "";
	}

	/**
	 * @author nawaz
	 * @return
	 */
	public String getEstateMapRef() {
		try
		{
			if(getLand() != null) { 
				return land.getEstateMapRef();
			}
		}	
		catch(Exception ee)
		{
			logger.warn("Exception occurred in getEstateMapRef() method. Message is: {}",ee.getMessage());
		}
		
		return "";
	}
	
	
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
	
	/**
	 * @author kavitha
	 * @return
	 */
	public String getLocationAddress() {
		try
		{
			if(isSuccessful() && isSet(ESTATEID)) {
				GenRow row = new GenRow(); 
				//row.setConnection(getConnection());
				row.setViewSpec("ProductView");
				String estateID =  getString(ESTATEID);
				if (estateID != null && estateID.length() != 0) {
					row.setParameter("ProductID", estateID);
					row.doAction(GenerationKeys.SELECTFIRST);
				}
				if(row.isSuccessful() && row.isSet("Street") && row.isSet("City")) {
					StringBuffer estateAddressBuilder = new StringBuffer();
					estateAddressBuilder.append(row.getString("StreetNumber"));
					estateAddressBuilder.append(" ");
					estateAddressBuilder.append(row.getString("Street"));
					estateAddressBuilder.append(",");
					estateStreet = estateAddressBuilder.toString();
					estateCity = row.getString("City");
				} 
			}
		}	
		catch(Exception ee)
		{
			logger.warn("Exception occurred in getLocationAddress() method. Message is: {}",ee.getMessage());
		}
		return "";
	}
	
	public String getEstateStreet() {
		if(estateStreet != null)
			return estateStreet;
		else {
			getLocationAddress();
		}
		return estateStreet;	
	}
	
	public String getEstateCity() {
		if(estateCity != null)
			return estateCity;
		else {
			getLocationAddress();
		}
		return estateCity;	
	}
	
	public String getPricingType() {
		String pricingType = getString("PricingType");
		if(pricingType != null && !pricingType.equalsIgnoreCase(""))
			return pricingType;
		else
			return "Price";
	}
	
	public String getDisplayFlag() { 
		return getString("DisplayFlag");
	}
	
} 