package com.sok.runway.crm.interfaces;

import javax.servlet.http.HttpServletRequest;

public interface AsyncProcessStatus {

	/* implementation example in AbstractPDFTemplate */
	public void setStatus(HttpServletRequest request, String status);
	public void setStatus(HttpServletRequest request, String status, double percentage);
}
