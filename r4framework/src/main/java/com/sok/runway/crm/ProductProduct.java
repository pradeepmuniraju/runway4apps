package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;


public class ProductProduct extends LinkableProduct {

   public static final String PRODUCT_PRODUCT_VIEWSPEC = "ProductProductView";
   
   public ProductProduct(Connection con) {
		super(con, PRODUCT_PRODUCT_VIEWSPEC);
	}
		
	public ProductProduct(ServletRequest request) {
		super(request, PRODUCT_PRODUCT_VIEWSPEC);
		populateFromRequest(request.getParameterMap());
	}
	   
	public ProductProduct(Connection con, String productProductID) {
		this(con);
	   load(productProductID);
	}
   
   public Validator getValidator() {
		Validator val = Validator.getValidator("ProductProducts");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("ProductProductID","SYSTEM ERROR - ProductProductID");
			val.addMandatoryField("ProductID","SYSTEM ERROR - ProductID");
			val.addMandatoryField("ProductID","SYSTEM ERROR - ProductID");
			Validator.addValidator("ProductProducts", val);
		}
		return val;
   }
}