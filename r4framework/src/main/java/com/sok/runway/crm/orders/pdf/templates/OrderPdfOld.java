package com.sok.runway.crm.orders.pdf.templates;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.regex.Matcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrTokenizer;
import org.apache.velocity.app.VelocityEngine;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.ActionBean;
import com.sok.framework.Conditions;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.framework.TableBean;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;
import com.sok.runway.crm.orders.pdf.AbstractOrderPDF;
import com.sok.runway.externalInterface.beans.Order;
import com.sok.runway.externalInterface.beans.QuoteView;
import com.sok.runway.externalInterface.beans.QuoteViewPage;
import com.sok.runway.externalInterface.beans.QuoteViewPageOptions;
import com.sok.runway.externalInterface.beans.QuoteViewPageOptions.DocumentOption;
import com.sok.runway.externalInterface.beans.User;
import com.sok.runway.externalInterface.beans.shared.Address;
import com.sok.runway.externalInterface.beans.shared.OrderAnswer;
import com.sok.runway.externalInterface.beans.shared.OrderContact;
import com.sok.runway.externalInterface.beans.shared.OrderItem;
import com.sok.runway.externalInterface.beans.shared.OrderProfile;
import com.sok.runway.externalInterface.beans.shared.Profile;
import com.sok.runway.externalInterface.resources.UtilityResource.ContactLocation;
import com.sok.runway.offline.ThreadManager;
import com.sok.service.crm.DocumentService;
import com.sok.service.crm.OrderService;
import com.sok.service.crm.ProfileService;
import com.sok.service.crm.QuoteViewService;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.FacadeService;
import com.sok.service.crm.cms.properties.FacadeService.Facade;
import com.sok.service.crm.cms.properties.PlanService;
import com.sok.service.crm.cms.properties.PlanService.HLPackage;
import com.sok.service.crm.cms.properties.PlanService.Plan;


/**
 * Initial version of the split out into client specific and non-specific. 
 * @author mike
 *
 */
public class OrderPdfOld extends AbstractOrderPDF {

	private static final Logger logger = LoggerFactory.getLogger(OrderPdfOld.class);
	private static final OrderService os = OrderService.getInstance();
	private static final FacadeService fs = FacadeService.getInstance();
	private static final BaseColor headerBarBackground = new BaseColor(204,208,225);
	private static final BaseColor panelBackground = rgb(243,243,243);
	private static final BaseColor costTotalBackground = new BaseColor(212,229,247);

	private static BaseColor rgb(int r, int g, int b) {
		return new BaseColor(r,g,b);
	}

	private static final Paragraph BLANK = new Paragraph("");
	private static final int designWidth = 867; 

	//private static BaseFont m100 = null; 
	private static BaseFont m300 = null; 
	private static BaseFont m500 = null; 
	private static BaseFont m700 = null;
	//private static BaseFont m900 = null;

	//private static BaseFont h45 = null; 
	private static BaseFont h55 = null; 
	private static BaseFont h75 = null;
	private static BaseFont h80 = null;

	static {
		try {
			String museopath = InitServlet.getRealPath("/specific/pdf/fonts/Museo/");
			String helvpath = InitServlet.getRealPath("/specific/pdf/fonts/HelveticaNeueLTStd/");

			//m100 = BaseFont.createFont(museopath + "/Museo_Slab_100.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			m300 = BaseFont.createFont(museopath + "/Museo_Slab_300.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			m500 = BaseFont.createFont(museopath + "/Museo_Slab_500.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			m700 = BaseFont.createFont(museopath + "/Museo_Slab_700.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			//m900 = BaseFont.createFont(museopath + "/Museo_Slab_900.otf", BaseFont.CP1252, BaseFont.EMBEDDED);

			//h45 = BaseFont.createFont(helvpath + "/HelveticaNeueLTStd-Lt.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			h55 = BaseFont.createFont(helvpath + "/HelveticaNeueLTStd-Roman.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			h75 = BaseFont.createFont(helvpath + "/HelveticaNeueLTStd-Bd.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			h80 = BaseFont.createFont(helvpath + "/HelveNeuMed.ttf", BaseFont.CP1252, BaseFont.EMBEDDED); 

		} catch (Exception e) {
			logger.error("Ignoring Error while registering fonts. Error: ", e);
			//if(m100 == null) m100 = BaseFont.g
		}
	}

	/* convert design pixels to whatever this uses */
	private static float fontRatio = 0.638f;

	private static Font quotationHeader = new Font(m500, fontRatio * 30f , Font.NORMAL, BaseColor.BLACK);
	private static final Font subHeader =  new Font(m500, fontRatio * 17f, Font.NORMAL,  new BaseColor(51,51,51));

	private static final Font boldFont = new Font(h80, fontRatio *15f, Font.NORMAL, BaseColor.BLACK);

	private static final Font textFont = new Font(h55,  fontRatio *13f, Font.NORMAL, BaseColor.BLACK);
	private static final Font textFontBold = new Font(h80,  fontRatio *14f, Font.NORMAL, BaseColor.BLACK);

	private static final Font dripTitleFont =  new Font(h75, fontRatio *16f, Font.NORMAL, BaseColor.BLACK);

	private static final Font costDisclaimerFont = new Font(h55, fontRatio *12f, Font.NORMAL, new BaseColor(51, 51, 51));
	private static final Font costSubtotalFont =  new Font(h75, fontRatio *13f, Font.NORMAL, BaseColor.BLACK);
	private static final Font costTotalFont =  new Font(h75, fontRatio *15f, Font.NORMAL, BaseColor.BLACK);

	private static final Font footerTitle =  new Font(m700, fontRatio *15f, Font.NORMAL, new BaseColor(51, 51, 51));
	private static final Font footerContact = new Font(m500,fontRatio *15f, Font.NORMAL, new BaseColor(51, 51, 51));
	private static final Font footerValidity =  new Font(m300, fontRatio *13f, Font.NORMAL, new BaseColor(51, 51, 51));

	private static float[] getDesignWidths(float... widths) {
		return getWidths(designWidth, widths);
	}
	private static float[] getWidths(int tableWidth, float... widths) {
		int max = widths.length -1;
		if(max == -1) 
			return widths;

		float[] out = new float[widths.length];
		for(int i=0; ; i++) {
			out[i] = widths[i] / tableWidth; 
			if(i == max) 
				break;
		}
		return out;
	}

	public static Paragraph getRepParagraph(String repName, String repMobile, String repPhone, String repEmail) {
		final Paragraph p = new Paragraph();
		p.add(new Chunk("Quote prepared by - " + repName, footerTitle));
		if (StringUtils.isNotBlank(repPhone)) {
			p.add(new Chunk(new StringBuilder().append("   Phone: ").append(repPhone).append("  ").toString(), footerContact));
		}
		if (StringUtils.isNotBlank(repMobile)) {
			p.add(new Chunk("    Mobile: ", footerContact));
			p.add(new Chunk(repMobile + "    ", footerContact));
		}
		p.add(new Chunk("    Email: " + repEmail, footerContact));
		return p;
	}

	public static Paragraph getValidity(long days, Date expiryDate, QuoteView qView) {
		final DateFormat df = new SimpleDateFormat("dd MMM yyyy");
		StringBuilder validityText = new StringBuilder().append("This quote is valid for ").append(days).append(" days and expires on ").append(df.format(expiryDate)).append(".");

		if(qView.footerText != null && !qView.footerText.equalsIgnoreCase(""))
			validityText = new StringBuilder().append(qView.footerText);

		return new Paragraph(validityText.toString() , footerValidity);
	}	

	private void setHeader(PdfPTable table) {
		table.getDefaultCell().setBackgroundColor(headerBarBackground);
		table.getDefaultCell().setPadding(7);
		table.getDefaultCell().setPaddingTop(3);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorderWidth(0);
	}
	private void setNormal(PdfPTable table) {
		table.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		table.getDefaultCell().setPadding(0);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorderWidth(0);
	}

	private PdfPTable getHeadingTable(String pageHeading, QuoteView qView) throws DocumentException {
		PdfPTable headingTable = new PdfPTable(2);
		headingTable.setWidthPercentage(100f);
		headingTable.getDefaultCell().setBorder(0);
		headingTable.setWidths(new float[]{2,1});
		headingTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);
		headingTable.addCell(new Phrase(pageHeading, quotationHeader));
		headingTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);

		String imagePath = "specific/images/logolarge.jpg";
		if(qView.headerImage != null && !qView.headerImage.equalsIgnoreCase("") && qView.headerImagePath != null && !qView.headerImagePath.equalsIgnoreCase(""))
			imagePath = qView.headerImagePath;
		Image rim = PDFUtil.createImage(PropertyEntity.resizeImage(imagePath, 364, 178, PropertyEntity.ResizeOption.Fit));

		if(rim == null) { 
			headingTable.addCell(BLANK);
		} else { 
			headingTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			headingTable.addCell(rim);
			headingTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		}
		return headingTable;
	}

	private PdfPTable getHeadingTableLeft(String pageHeading, QuoteView qView) throws DocumentException {
		PdfPTable headingTable = new PdfPTable(2);
		headingTable.setWidthPercentage(100f);
		headingTable.getDefaultCell().setBorder(0);
		headingTable.setWidths(new float[]{1, 2});

		String imagePath = "specific/images/logolarge.jpg";
		if(qView.headerImage != null && !qView.headerImage.equalsIgnoreCase("") && qView.headerImagePath != null && !qView.headerImagePath.equalsIgnoreCase(""))
			imagePath = qView.headerImagePath;

		Image rim = PDFUtil.createImage(PropertyEntity.resizeImage(imagePath, 364, 178, PropertyEntity.ResizeOption.Fit));
		if(rim == null) { 
			headingTable.addCell(BLANK);
		} else {
			headingTable.addCell(rim);
		}
		headingTable.getDefaultCell().setColspan(2);
		headingTable.addCell(BLANK);
		headingTable.getDefaultCell().setColspan(3);
		headingTable.addCell(BLANK);
		headingTable.addCell(new Phrase(pageHeading, quotationHeader));

		return headingTable;
	}


	private void addCoverPage(com.itextpdf.text.Document d, QuoteViewPage qvp, Order order, List<OrderItem> orderItems, com.sok.service.crm.cms.properties.FacadeService.Facade f, ContactLocation orderLocation, Plan p, HLPackage pkg, List<OrderProfile> orderProfiles, QuoteView qView, Map<String, Boolean> customPageOptions) throws DocumentException {
		d.newPage();
		List<OrderContact> contactList = order.getLinkedContacts();

		PdfPTable pageTable = new PdfPTable(5);
		pageTable.setWidths(getDesignWidths(226, 22, 226, 32, 361));
		pageTable.setWidthPercentage(100f);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.getDefaultCell().setColspan(5);
		//line break
		pageTable.addCell(BLANK);

		pageTable.getDefaultCell().setPaddingLeft(10);
		pageTable.getDefaultCell().setPaddingBottom(10);
		pageTable.getDefaultCell().setColspan(5);

		String pageHeading = (p != null && p.HomeName != null) ? 
				String.format("Quotation (%1$s) %2$s - %3$s", order.getOrderNum(), p.HomeName, p.Name)
				: String.format("Quotation %1$s - %2$s","Preview","PDF");	


				pageTable.addCell(getHeadingTable(pageHeading, qView));

				//line break
				pageTable.addCell(new Paragraph(" ", textFont));
				pageTable.getDefaultCell().setColspan(4);
				setHeader(pageTable);
				pageTable.addCell(new Paragraph("For",subHeader));
				pageTable.getDefaultCell().setColspan(1);

				pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);

				final DateFormat df = new SimpleDateFormat("dd MMM yyyy");
				pageTable.addCell(new Paragraph(df.format(order.getDateOfSale()),subHeader));
				pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				pageTable.getDefaultCell().setColspan(5);
				setNormal(pageTable);

				PdfPTable innerTable = new PdfPTable(4);
				innerTable.setWidths(getDesignWidths(12, 303, 12, 503));
				innerTable.setWidthPercentage(100f);
				innerTable.getDefaultCell().setPadding(0);
				innerTable.getDefaultCell().setBorder(0);
				innerTable.getDefaultCell().setBackgroundColor(panelBackground);

				int ci = 0;

				if(contactList != null) { 
					Collections.sort(contactList, new Comparator<OrderContact>() {
						public int compare(OrderContact o1, OrderContact o2) {
							if("primary".equals(o1.getRole())) {
								return -1;
							}
							return 1;
						}
					});


					for(OrderContact oc: contactList) {
						if(StringUtils.isBlank(oc.getContactID()))
						{
							continue;
						}
						innerTable.addCell(BLANK);

						PdfPTable contactTable = new PdfPTable(2);
						contactTable.getDefaultCell().setPadding(0);
						contactTable.getDefaultCell().setBorder(0);
						contactTable.setWidths(getWidths(303, 83, 202));
						contactTable.getDefaultCell().setColspan(2);

						contactTable.getDefaultCell().setPaddingTop(8);
						contactTable.getDefaultCell().setPaddingBottom(3);

						contactTable.addCell(new Paragraph(oc.getName(), boldFont));
						contactTable.getDefaultCell().setColspan(1);

						contactTable.getDefaultCell().setPaddingTop(2);
						contactTable.getDefaultCell().setPaddingBottom(4);

						contactTable.addCell(new Paragraph("Phone", textFont));
						contactTable.addCell(new Paragraph(oc.getPhone(), textFont));

						contactTable.addCell(new Paragraph("Mobile:", textFont));
						contactTable.addCell(new Paragraph(oc.getMobile(), textFont));
						contactTable.addCell(new Paragraph("Email:", textFont));
						contactTable.addCell(new Paragraph(oc.getEmail(), textFont));
						if(oc.getPhysicalAddress() != null) {
							contactTable.addCell(new Paragraph("Address:", textFont));
							contactTable.addCell(new Paragraph(oc.getPhysicalAddress().getStreet(), textFont));
							contactTable.addCell(new Paragraph("Suburb:", textFont));
							contactTable.addCell(new Paragraph(oc.getPhysicalAddress().getCity(), textFont));
							contactTable.addCell(new Paragraph("Postcode:", textFont));
							contactTable.addCell(new Paragraph(oc.getPhysicalAddress().getPostcode(), textFont));	
						}
						innerTable.addCell(contactTable);
						ci++;
						if(ci == 2) 
							break;
					}
					while(ci < 2) {
						innerTable.addCell(BLANK);
						innerTable.addCell(BLANK);
						ci++;
					}
					pageTable.addCell(innerTable);
				} 
				pageTable.getDefaultCell().setColspan(5);
				pageTable.getDefaultCell().setPaddingTop(20);
				pageTable.addCell(new Paragraph(" ", textFont));
				pageTable.getDefaultCell().setPaddingTop(0);
				pageTable.getDefaultCell().setColspan(1);

				innerTable = new PdfPTable(3);
				innerTable.setWidths(getWidths(225, 12, 83, 130));
				innerTable.setWidthPercentage(100f);
				innerTable.getDefaultCell().setPadding(0);
				innerTable.getDefaultCell().setBorder(0);
				innerTable.getDefaultCell().setColspan(3);

				setHeader(innerTable);
				innerTable.addCell(new Paragraph("Land Details",subHeader));
				setNormal(innerTable);
				innerTable.getDefaultCell().setBackgroundColor(panelBackground);
				innerTable.getDefaultCell().setColspan(1);

				String ssm = getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Site Start Month", orderProfiles)),
						ssy = getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Site Start Year", orderProfiles)),
						siteStart = null;
				if(ssm != null && ssy != null) {
					siteStart = new StringBuilder().append(ssm).append(" ").append(ssy).toString();
				}

				final String[] ldt = { "Lot/Street No:", "Street:","Suburb:","Postcode:","Estate","Stage","Title Date", "Site Start" }; 
				final String[] ldd = { orderLocation.StreetNumber, orderLocation.Street, orderLocation.City, orderLocation.Postcode, 
						getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Estate", orderProfiles)),
						getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Stage", orderProfiles)),
						getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Title Date", orderProfiles)),
						siteStart
				};

				if(ldd[0] != null && ldd[0].toUpperCase().startsWith("LOT ") && ldd[0].length()>4) {
					ldd[0] = ldd[0].substring(4);
				}

				innerTable.getDefaultCell().setPaddingTop(8);
				innerTable.getDefaultCell().setPaddingBottom(4);

				int max = ldt.length -1;
				for(int i=0; ; i++) {
					if(ldd[i] != null) { 
						innerTable.addCell(BLANK);
						innerTable.addCell(new Paragraph(ldt[i], textFont));
						innerTable.addCell(new Paragraph(ldd[i], textFont));
					}
					if(i == 0) {
						innerTable.getDefaultCell().setPaddingTop(3);		
					}
					if(i == max) 
						break;
				}
				pageTable.addCell(innerTable);
				pageTable.addCell(BLANK);

				pageTable.getDefaultCell().setBackgroundColor(headerBarBackground);

				innerTable = new PdfPTable(3);
				innerTable.setWidths(getWidths(225, 12, 83, 130));
				innerTable.setWidthPercentage(100f);
				innerTable.getDefaultCell().setBorder(0);
				innerTable.getDefaultCell().setColspan(3);

				setHeader(innerTable);
				innerTable.addCell(new Paragraph("Home Details",subHeader));
				setNormal(innerTable);
				innerTable.getDefaultCell().setBackgroundColor(panelBackground);
				innerTable.getDefaultCell().setColspan(1);

				String br = getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Build Region", orderProfiles)),
						brName = null;
				if(br != null) {
					com.sok.runway.externalInterface.beans.ItemList il = ProfileService.getInstance().getItemListByName("SAPplant");
					for(com.sok.runway.externalInterface.beans.shared.ListItem li: il.getListItems()) {
						if(br.equals(li.getItemValue())) {
							brName = li.getItemLabel();
							break;
						}
					}
				}

				String[] hdt = { /* "Display:", */"Range (Brand):", "Build Region:", "Design:", "Plan (Size):", "Facade:" }; 
				String[] hdd = { p.RangeName, 
						brName, 
						p.HomeName, p.Name, f.Name };

				boolean regionPricing = "true".equals(InitServlet.getSystemParam("RegionPricingEnabled"));
				if(regionPricing && StringUtils.isNotBlank(order.getProductType()) && order.getProductType().equalsIgnoreCase("Home Plan") && StringUtils.isNotBlank(order.getRegionName())) {
					String[] hdtTmp = { /* "Display:", */"Range (Brand):", "Build Region:", "Design:", "Plan (Size):", "Facade:", "Region:" }; 
					hdt = hdtTmp;

					String[] hddTmp =  { p.RangeName, brName, p.HomeName, p.Name, f.Name, order.getRegionName() };
					hdd = hddTmp;

					hddTmp = null;
					hdtTmp = null;
				}

				max = hdt.length -1;

				innerTable.getDefaultCell().setPaddingTop(8);
				innerTable.getDefaultCell().setPaddingBottom(4);

				for(int i=0; ; i++) {
					innerTable.addCell(BLANK);
					innerTable.addCell(new Paragraph(hdt[i], textFont));
					innerTable.addCell(new Paragraph(hdd[i], textFont));
					if(i == 0) {
						innerTable.getDefaultCell().setPaddingTop(3);		
					}
					if(i == max) 
						break;
				}
				pageTable.addCell(innerTable);

				pageTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
				pageTable.addCell(BLANK);


				boolean image = false;
				try { 
					Image i = PDFUtil.createImage(PropertyEntity.resizeImage(f.ImagePath, 361, 210, PropertyEntity.ResizeOption.Crop));
					if(i != null) {
						innerTable = new PdfPTable(1);
						innerTable.getDefaultCell().setBorder(0);
						innerTable.getDefaultCell().setPadding(0);
						innerTable.addCell(i);
						innerTable.getDefaultCell().setPaddingTop(5);
						innerTable.addCell(new Phrase("This image is for illustration purposes only", textFont));
						pageTable.addCell(innerTable);
						image = true;
						//pageTable.addCell(i);
					}
				} catch (Exception e) {
					logger.error("Error getting facade image", e);
				}
				if(!image) {
					try { 
						Image i = PDFUtil.createImage(PropertyEntity.resizeImage(p.HomeImagePath, 361, 210, PropertyEntity.ResizeOption.Crop));
						if(i != null) {
							innerTable = new PdfPTable(1);
							innerTable.getDefaultCell().setBorder(0);
							innerTable.getDefaultCell().setPadding(0);
							innerTable.addCell(i);
							innerTable.getDefaultCell().setPaddingTop(5);
							innerTable.addCell(new Phrase("This image is for illustration purposes only", textFont));
							pageTable.addCell(innerTable);
							image = true;
						} 
					} catch (Exception e2) {
						logger.error("Error getting home image", e2);
					}
				}

				if(!image) {
					try { 
						Image i = PDFUtil.createImage(PropertyEntity.resizeImage("crm/orders/views/images/no_image.png", 361, 210, PropertyEntity.ResizeOption.Crop));
						if(i != null) {
							pageTable.addCell(i);
							image = true;
						} 
					} catch (Exception e2) {
						logger.error("Error getting no-image", e2);
					}
				}

				if(!image) {
					pageTable.addCell(BLANK);
				}

				pageTable.getDefaultCell().setColspan(5);
				//TODO padding test 
				pageTable.getDefaultCell().setPaddingTop(10);
				pageTable.addCell(new Paragraph(" "));
				pageTable.getDefaultCell().setPaddingTop(10);
				pageTable.getDefaultCell().setColspan(3);

				innerTable = new PdfPTable(4);
				innerTable.setWidths(getWidths(473, 12, 270 , 179, 12));
				innerTable.setWidthPercentage(100f);
				innerTable.getDefaultCell().setBorder(0);
				innerTable.getDefaultCell().setColspan(2);

				setHeader(innerTable);

				String type = getAnswer(ProfileService.getOrderProfileAnswerByQuestionName("Build Type", orderProfiles));

				if("HNL".equals(type)) {
					if (!"false".equals(InitServlet.getSystemParam("Quote-ShowLandInPDF"))) {
						innerTable.addCell(new Paragraph("Costs - House and Land",subHeader));	
					} else {
						innerTable.addCell(new Paragraph("Costs - Home",subHeader));	
					}
				} else if("OJ".equals(type)) {
					innerTable.addCell(new Paragraph("Costs - Order Job",subHeader));	
				} else if("KDR".equals(type)) {
					innerTable.addCell(new Paragraph("Costs - Knockdown Rebuild",subHeader));	
				} else {
					innerTable.addCell(new Paragraph("Costs - Order",subHeader));
				}

				innerTable.getDefaultCell().setPadding(0);
				innerTable.getDefaultCell().setPaddingTop(1);
				innerTable.getDefaultCell().setPaddingRight(3);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				Paragraph pa= new Paragraph("(All prices are estimates only.\nPrices may vary without notice.)", costDisclaimerFont);
				//pa.add(Chunk.NEWLINE);
				//pa.add(new Chunk("Prices may vary without notice", costDisclaimerFont));
				innerTable.addCell(pa);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

				setNormal(innerTable);
				innerTable.getDefaultCell().setBackgroundColor(panelBackground);

				QuoteViewPageOptions po = qvp.SelectedOptions;

				List<SummaryCost> list = new ArrayList<SummaryCost>();
				List<SummaryCost> landSubList = new ArrayList<SummaryCost>();
				List<SummaryCost> postList = new ArrayList<SummaryCost>();

				try {
					if(po.Baseprice != null && po.Baseprice.pdf) {
						double basePrice = getBasePrice(orderItems, p, pkg), lotCost = 0;
						if (!"false".equals(InitServlet.getSystemParam("Quote-ShowLandInPDF"))) {
							if(qvp.SelectedOptions != null && qvp.SelectedOptions.SeparateLand != null && qvp.SelectedOptions.SeparateLand.BooleanValue == true) {
								if(pkg.LotCost != 0) {
									lotCost = pkg.LotCost * 1.1;
									basePrice -= lotCost;
								}
							}
							list.add(new SummaryCost("Base Price", basePrice));
							try {
								if(lotCost != 0) 
								{
									list.add(new SummaryCost("Land", lotCost));
									if(pkg.PackageCosts != null) {
										for(int z = 0 ; z < pkg.PackageCosts.size(); z++) {
											String categoryName = pkg.PackageCosts.get(z).Category;
											if(categoryName!=null && customPageOptions!=null && customPageOptions.get(categoryName) != null && customPageOptions.get(categoryName).booleanValue())
												landSubList.add(new SummaryCost(categoryName, "Included"));
										}
									}
								}
							} catch (Exception ee) {
								ee.printStackTrace();
							}
						} else {
							list.add(new SummaryCost("Retail List Price", basePrice));
						}
					}
				}catch(Exception ee){
					ee.printStackTrace();
				}

				try {
					if(po.Facade != null && po.Facade.pdf) {
						OrderItem facade = getFacadeItem(orderItems);
						if(facade != null) 
							list.add(new SummaryCost("Facade", facade.getTotalCost()));
					}
				}catch(Exception ee){
					ee.printStackTrace();
				}

				try {
					if(po.CustomVariations != null && po.CustomVariations.pdf) {
						list.add(new SummaryCost("Custom Variations", getCustomTotal(orderItems)));
					}
				}catch(Exception ee){
					ee.printStackTrace();
				}

				try {
					if(po.Options != null && po.Options.pdf) {
						list.add(new SummaryCost("Options", getOptionsTotal(orderItems)));
						double ec = getEstateCovenantPrice(orderItems);
						if(ec > 0) {
							list.add(new SummaryCost("Estate Covenant Requirements", 0));	
						}
					}
				}catch(Exception ee){
					ee.printStackTrace();
				}

				JSONObject dripContent = null;
				JSONParser jp = new JSONParser();
				Object o = null; String t = null;


				if(po.DRIPS != null && po.DRIPS.pdf){
					List<OrderItem> dripItems = getDripItems(orderItems);
					double postDrip = 0, dripTotal = 0; 
					for(OrderItem item: dripItems) {
						//get drip items, check those that have a cover defined.
						if(item.isIncludeSummary() && StringUtils.isNotBlank(item.getMetaData())) {
							try { 
								Object obj = jp.parse(item.getMetaData());
								if(obj instanceof JSONObject) {
									dripContent = (JSONObject)obj;
								} else {
									logger.error("Got a different instance than expected for MetaData {} in OrderItemID=[{}]", obj.getClass().getName(), item.getOrderItemID());
									continue;
								}
							} catch (ParseException pe) {
								logger.error("Error parsing Drip Meta Data", pe);
								/* attempt to load from drip ? */
								continue;
							}
							String cover = null;

							if((o = dripContent.get("Cover")) != null && o instanceof String && StringUtils.isNotBlank(t = (String)o)) {
								cover = t;
							} 
							if("Post Net".equals(item.getPricingType())) {
								if(cover != null) {
									postList.add(new SummaryCost(t, item.getGST() + item.getCost()));
								} else {
									postDrip += item.getGST() + item.getCost();
								}
							} else if("Inc Net".equals(item.getPricingType())) {
								if(cover != null) {
									list.add(new SummaryCost(t, item.getTotalCost()));
								} else {
									dripTotal += item.getTotalCost();
								}
							} else {	//inc base
								if(cover != null && !"Not Shown".equals(item.getPricingType())) {
									list.add(new SummaryCost(t, 0));
								}
								//base price calc is done seperately in pdf
							}
						}
					}
					/*
			for(PDFDripInfo pdi: dripSummaryList) {
				if(pdi.Cover != null) {
					dripTotal -= pdi.includePrice ? 0 : pdi.cost;
					//OrderItem i = this.getOrderItemByProductId(orderItems, pdi.)
					list.add(new SummaryCost(pdi.Cover, pdi.includePrice ? 0 : pdi.cost));
				}				
			}
					 */
					if(dripTotal != 0) { 
						list.add(new SummaryCost("Promotion", dripTotal));
					}
					if(postDrip != 0) {
						postList.add(new SummaryCost("Promotion", postDrip));
					}
				}

				final List<String> financialSort = getFinancialsSort(qvp.QuoteViewPageID, orderItems);

				Collections.sort(list, new Comparator<SummaryCost>() {
					public int compare(SummaryCost o1, SummaryCost o2) {
						int ix1 = financialSort.indexOf(o1.title);
						int ix2 = financialSort.indexOf(o2.title);
						if(ix2 == -1) {
							return -1;
						}
						if(ix1 == -1) {
							return 1;
						}
						return (ix1 < ix2) ? -1 : 1; 
					}
				});

				Collections.sort(landSubList, new Comparator<SummaryCost>() {
					public int compare(SummaryCost o1, SummaryCost o2) {
						int ix1 = financialSort.indexOf(o1.title);
						int ix2 = financialSort.indexOf(o2.title);
						if(ix2 == -1) {
							return -1;
						}
						if(ix1 == -1) {
							return 1;
						}
						return (ix1 < ix2) ? -1 : 1; 
					}
				});

				if(!postList.isEmpty()) { 

					final List<String> postFinancialSort = getFinancialsSort(qvp.QuoteViewPageID, orderItems);

					if(!postFinancialSort.isEmpty()) { 

						Collections.sort(postList, new Comparator<SummaryCost>() {
							public int compare(SummaryCost o1, SummaryCost o2) {
								int ix1 = postFinancialSort.indexOf(o1.title);
								int ix2 = postFinancialSort.indexOf(o2.title);
								if(ix2 == -1) {
									return -1;
								}
								if(ix1 == -1) {
									return 1;
								}
								return (ix1 < ix2) ? -1 : 1; 
							}
						});
					}
				} 
				NumberFormat mf = NumberFormat.getCurrencyInstance();
				mf.setMaximumFractionDigits(0);

				innerTable.getDefaultCell().setBackgroundColor(panelBackground);
				innerTable.getDefaultCell().setColspan(1);
				innerTable.getDefaultCell().setPaddingTop(5);
				innerTable.getDefaultCell().setPaddingBottom(7);

				double tc = 0.0;
				for(SummaryCost sc: list) {
					innerTable.addCell(BLANK);
					innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
					innerTable.addCell(new Paragraph(sc.title, textFont));
					innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
					innerTable.addCell(new Paragraph(formatMoney(sc.cost, mf), textFont));
					innerTable.addCell(BLANK);
					if(tc == 0d) {
						innerTable.getDefaultCell().setPaddingTop(0);
					}
					tc += sc.cost;
					if (!"false".equals(InitServlet.getSystemParam("Quote-ShowLandInPDF"))) {
						if("Land".equalsIgnoreCase(sc.title))
						{
							for(SummaryCost landList: landSubList) 
							{
								innerTable.addCell(BLANK);
								innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
								innerTable.addCell(new Paragraph(landList.title, textFont));
								innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
								innerTable.addCell(new Paragraph("Included", textFont));
								innerTable.addCell(BLANK);
							}
						}
					}
				}

				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				innerTable.addCell(new Paragraph("Sub Total", costSubtotalFont));
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				innerTable.addCell(new Paragraph(formatMoney(tc, mf), costSubtotalFont));
				innerTable.addCell(BLANK);


				innerTable.getDefaultCell().setColspan(4);
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setColspan(1);

				innerTable.getDefaultCell().setBackgroundColor(costTotalBackground);
				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

				innerTable.getDefaultCell().setPaddingTop(5);
				innerTable.getDefaultCell().setPaddingBottom(5);

				innerTable.addCell(new Paragraph("TOTAL", costTotalFont));
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				innerTable.addCell(new Paragraph(formatMoney(order.getTotalCost(), mf), costTotalFont));
				innerTable.addCell(BLANK);

				innerTable.getDefaultCell().setBackgroundColor(panelBackground);
				for(SummaryCost sc: postList) {
					innerTable.addCell(BLANK);
					innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
					innerTable.addCell(new Paragraph(sc.title, textFont));
					innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
					innerTable.addCell(new Paragraph(formatMoney(sc.cost, mf), textFont));
					innerTable.addCell(BLANK);
					if(tc == 0d) {
						innerTable.getDefaultCell().setPaddingTop(0);
					}
				}

				pageTable.addCell(innerTable);
				pageTable.getDefaultCell().setColspan(1);
				pageTable.addCell(BLANK);

				innerTable = new PdfPTable(1);
				innerTable.setWidthPercentage(100f);
				innerTable.getDefaultCell().setBorder(0);

				setHeader(innerTable);
				innerTable.addCell(new Paragraph("Comments",subHeader));
				setNormal(innerTable);
				innerTable.getDefaultCell().setBackgroundColor(panelBackground);
				innerTable.getDefaultCell().setPaddingTop(5);

				innerTable.getDefaultCell().setPaddingLeft(8);
				innerTable.addCell(new Paragraph(order.getNotes(), textFont));
				pageTable.addCell(innerTable);

				pageTable.getDefaultCell().setColspan(5);

				d.add(pageTable);
	}
	/*
	private static class PDHFooter extends PdfPageEventHelper {

		User rep; 
		Order order;
		Plan plan;

		public PDHFooter (User rep, Order order, Plan plan) {
			this.rep = rep;
			this.order = order;
			this.plan = plan;
		}

		public void onEndPage(PdfWriter writer, com.itextpdf.text.Document document) {
			PdfPTable pageTable = new PdfPTable(5);
			try {
				pageTable.setWidths(getDesignWidths(226, 22, 226, 32, 361));
				pageTable.setTotalWidth(556f);
				pageTable.setLockedWidth(true);
				pageTable.getDefaultCell().setPadding(10);
				pageTable.getDefaultCell().setBorder(0);
				long days = (order.getOutcomeDateLong() - order.getDateOfSaleLong()) / ( 1000 * 60 * 60 * 24);

				pageTable.getDefaultCell().setPadding(7);
				pageTable.getDefaultCell().setBackgroundColor(headerBlue);
				pageTable.getDefaultCell().setColspan(3);
				pageTable.addCell(getRepParagraph(rep.getName(), rep.getMobile(), rep.getPhone(), rep.getEmail(), days, order.getOutcomeDate()));

				pageTable.getDefaultCell().setColspan(2);

				Image rim = null;
				if(plan.RangeName != null) {
					if(plan.RangeName.startsWith("Prestige")) {
						rim = PDFUtil.createImage(PropertyEntity.resizeImage("specific/pdf/images/prestige_brand_logo.png", 362, 144, PropertyEntity.ResizeOption.Fit));
					} else if(plan.RangeName.startsWith("Lifestyle")) {
						rim = PDFUtil.createImage(PropertyEntity.resizeImage("specific/pdf/images/lifestyle_brand_logo.png", 362, 144, PropertyEntity.ResizeOption.Fit));
					} else if(plan.RangeName.startsWith("Access")) {
						rim = PDFUtil.createImage(PropertyEntity.resizeImage("specific/pdf/images/access_brand_logo.png", 362, 144, PropertyEntity.ResizeOption.Fit));
					}
				}
				if(rim != null) {
					PdfPTable innerTable = new PdfPTable(2);
					innerTable.setWidths(getWidths(381, 200, 181));
					innerTable.setWidthPercentage(100f);
					innerTable.getDefaultCell().setBorder(0);
					innerTable.addCell(BLANK);
					innerTable.addCell(rim);
					pageTable.addCell(innerTable);

				} else {
					pageTable.addCell(BLANK);
				}
				pageTable.writeSelectedRows(0, -1, 20, 80, writer.getDirectContent());
			} catch (DocumentException de) {
				throw new ExceptionConverter(de);
			}
		}
	}
	 */

	private double getItemCosts(List<OrderItem> orderItems, String category) {
		double groupTotal = 0;
		for(OrderItem item: orderItems) {
			logger.debug("OrderItem : {} = {}", item.getOrderItemID(), item.getCategory());
			if(StringUtils.isNotBlank(category) && category.equals(item.getCategory())) {
				logger.debug("Cost of orderitem added {}" , item.getTotalCost());
				groupTotal += item.getTotalCost();
				logger.debug("Total {}" , groupTotal);
			}
		}

		return groupTotal;
	}
	private String getAnswer(OrderAnswer oa) {
		if(oa == null)
			return null;
		return StringUtils.trimToNull(oa.getAnswer());
	}
	/* 
	 * Convenience method, but number formats should be re-used within the same thread wherever possible.
	 */
	private String formatMoney(final double d) {
		return formatMoney(d, null);
	}

	private String formatMoney(final double d, NumberFormat mf) {
		if(mf == null) {
			mf = NumberFormat.getCurrencyInstance();
			mf.setMaximumFractionDigits(0);
		}
		if(d == 0d) {
			return "Included";
		}
		return mf.format(d);
	}

	private List<String> getFinancialsSort(final String pageID, final List<OrderItem> items) {
		final String key = "Cover-Financials-" + pageID;
		for(OrderItem oi: items) {
			if(key.equals(oi.getCategory())) {
				try { 
					Object o = JSONValue.parse(oi.getDescription());
					if(o instanceof JSONArray) {
						final JSONArray arr = (JSONArray)o;
						final List<String> ret = new ArrayList<String>();
						for(Object item: arr) {
							ret.add(String.valueOf(item));
						}
						return ret;
					} else {
						logger.error("had some other object? {} {}", o.getClass().getName(), o.toString());
					}
				} catch (RuntimeException re) {
					logger.error("Error parsing from financial sort", re);
				}
			}
		}
		return Collections.emptyList();
	}

	private List<String> getPostFinancialsSort(final String pageID, final List<OrderItem> items) {
		final String key = "Cover-PostFinancials-" + pageID;
		for(OrderItem oi: items) {
			if(key.equals(oi.getCategory())) {
				try { 
					Object o = JSONValue.parse(oi.getDescription());
					if(o instanceof JSONArray) {
						final JSONArray arr = (JSONArray)o;
						final List<String> ret = new ArrayList<String>();
						for(Object item: arr) {
							ret.add(String.valueOf(item));
						}
						return ret;
					} else {
						logger.error("had some other object? {} {}", o.getClass().getName(), o.toString());
					}
				} catch (RuntimeException re) {
					logger.error("Error parsing from financial sort", re);
				}
			}
		}
		return Collections.emptyList();
	}

	private static class SummaryCost {
		String title;
		double cost; 
		String costText;

		public SummaryCost (String title, double cost) {
			this.title = title;
			this.cost = cost;
		}


		public SummaryCost (String title, String costText) {
			this.title = title;
			this.cost = -1;
			this.costText = costText;
		}
	}

	private double getBasePrice(List<OrderItem> items, Plan p, HLPackage pkg) {
		double total = pkg != null ? pkg.TotalCost : p.TotalCost;
		if(items == null || items.isEmpty())
			return total;

		total = 0d;
		for(OrderItem oi: items) {
			if(oi.getCategory() != null && oi.getCategory().startsWith("Custom") && oi.isIncludePrice()) {
				total += oi.getTotalCost();
			} else if ("Package".equals(oi.getCategory())) { 
				total += oi.getTotalCost();
			} else if("Plan".equals(oi.getCategory())) {
				total += oi.getTotalCost();
			} else if("VPB".equals(oi.getCategory()) && "Estate Covenant Requirements".equals(oi.getItemGroup()) && oi.isIncludePrice()) {
				total += oi.getTotalCost();
			} else if("DRIP".equals(oi.getCategory()) && (oi.isIncludePrice() || ("Inc Base".equals(oi.getPricingType()) || "Not Shown".equals(oi.getPricingType())))) {
				total += oi.getTotalCost();
			}
		}
		return total;
	}

	private double getEstateCovenantPrice(List<OrderItem> items) {
		double total = 0d;
		for(OrderItem oi: items) {
			if("VPB".equals(oi.getCategory()) && "Estate Covenant Requirements".equals(oi.getItemGroup()) && oi.isIncludePrice()) {
				total += oi.getTotalCost();
			}
		}
		return total;
	}


	private double getCustomTotal(List<OrderItem> items) {
		if(items == null || items.isEmpty())
			return 0;
		double total = 0d;
		for(OrderItem oi: items) {
			if(oi.getCategory() != null && oi.getCategory().startsWith("Custom") && !oi.isIncludePrice()) {
				total += oi.getTotalCost();
			}
		}
		return total;
	}

	private OrderItem getOpenOrderItem(List<OrderItem> items, QuoteViewPage page) {
		String key = "Open-" + page.QuoteViewPageID;
		for(OrderItem oi: items) {
			if(key.equals(oi.getCategory())) {
				return oi;
			}
		}
		return null;
	}

	private List<OrderItem> getDripItems(List<OrderItem> items) {
		List<OrderItem> dripList = new ArrayList<OrderItem>(5);
		for(OrderItem oi: items) {
			if("DRIP".equals(oi.getCategory())) {
				dripList.add(oi);
			}
		}
		return dripList;
	}

	/*
	private double getDripTotal(List<OrderItem> items) {
		if(items == null || items.isEmpty())
			return 0;
		double total = 0d;
		for(OrderItem oi: items) {
			if("DRIP".equals(oi.getCategory()) && !oi.isIncludePrice()) {
				total += oi.getTotalCost();
			}
		}
		return total;
	}
	 */
	private double getOptionsTotal(List<OrderItem> items) {
		if(items == null || items.isEmpty())
			return 0;
		double total = 0d;
		for(OrderItem oi: items) {
			if("VPB".equals(oi.getCategory()) && !("Estate Covenant Requirements".equals(oi.getItemGroup()) && oi.isIncludePrice())) {
				total += oi.getTotalCost();
			}
		}
		return total;
	}

	private OrderItem getFacadeItem(List<OrderItem> list) {
		for(OrderItem oi: list) {
			if("Facade".equals(oi.getCategory())) {
				return oi;
			}
		}
		return null;
	}

	private OrderItem getPlanItem(List<OrderItem> list) {
		for(OrderItem oi: list) {
			if("Plan".equals(oi.getCategory())) {
				return oi;
			}
		}
		return null;
	}

	private OrderItem getPackageItem(List<OrderItem> list) {
		for(OrderItem oi: list) {
			if("Package".equals(oi.getCategory())) {
				return oi;
			}
		}
		return null;
	}
	private OrderItem getOrderItemByProductId(List<OrderItem> list, String productID) {
		return getOrderItemByProductId(list, productID, null);
	}

	private OrderItem getOrderItemByProductId(List<OrderItem> list, String productID, String name) {
		if(StringUtils.isBlank(productID)) {
			return null;
		}
		for(OrderItem item: list) {
			if(productID.equals(item.getProductID()) && (name == null || name.equals(item.getName()))) {
				return item;
			}
		}
		return null;
	}
	private OrderItem getSummaryOrderItemByProductId(List<OrderItem> list, String productID) {
		if(StringUtils.isBlank(productID)) {
			return null;
		}
		for(OrderItem item: list) {
			if(productID.equals(item.getProductID()) && item.isIncludeSummary()) {
				return item;
			}
		}
		return null;
	}	


	public void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		//final String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
		com.itextpdf.text.Document document = null;
		java.io.ByteArrayOutputStream baos = null;
		PdfStamper stamp = null;
		PdfReader reader = null;
		//need to grab the outputstream before doing anything else. 
		java.io.OutputStream outputStream = response.getOutputStream();
		//now we can set the content type 
		response.setContentType("application/pdf");
		try {

			final String orderID = request.getParameter("OrderID");
			final UserBean user = UserService.getInstance().getCurrentUser(request);
			final Connection con = ActionBean.getConnection(request);

			//Order order = os.getOrder(request, orderID);
			//List<OrderItem> orderItems = os.getOrderItems(request, orderID);
			//List<ContactLocation> lcl = UtilityResource.getInstance().getOrderLocations(request, orderID);
			//List<OrderProfile> orderProfiles = OrderResource.getInstance().getOrderProfile(request, orderID);

			final boolean preview = "true".equals(request.getParameter("-preview"));
			final String quoteViewID = StringUtils.trimToNull(request.getParameter("QuoteViewID"));

			QuoteView qv = preview ? QuoteViewService.getInstance().getQuoteView(con, user, quoteViewID) : null;
			Future<Order> fOrder = ThreadManager.startCallable(new Callable<Order>() {
				public Order call() {
					return os.getOrder(con, user, orderID);
				}
			});
			Future<List<OrderItem>> fOrderItems = preview ? null : ThreadManager.startCallable(new Callable<List<OrderItem>>() {
				public List<OrderItem> call() {
					return os.getOrderItems(con, user, orderID);
				}
			});
			Future<List<ContactLocation>> fContactLocation = preview ? null : ThreadManager.startCallable(new Callable<List<ContactLocation>>() {
				public List<ContactLocation> call() {
					return os.getOrderLocations(con, user, orderID);
				}
			});
			Future<List<OrderProfile>> fOrderProfiles = preview ? null : ThreadManager.startCallable(new Callable<List<OrderProfile>>() {
				public List<OrderProfile> call() {
					final List<Profile> pl = ProfileService.getInstance().getProfiles(con, user, ProfileService.ProfileType.ORDER, orderID);
					final List<OrderProfile> cpl = new ArrayList<OrderProfile>(pl.size());
					for(Profile p: pl) {
						cpl.add((OrderProfile)p);
					}
					return cpl;
				}
			});

			List<OrderItem> orderItems = preview ? Arrays.asList(qv.OrderTemplate.getOrderItems()) :  fOrderItems.get();

			final OrderItem facadeItem = preview ? null : getFacadeItem(orderItems);
			final OrderItem planItem = preview ? null : getPlanItem(orderItems);
			final OrderItem packageItem = preview ? null : getPackageItem(orderItems);

			//Facade f = fs.getFacade(request, facadeItem.getProductID());
			//Plan plan = PlanService.getInstance().getPlan(request, planItem.getProductID());

			Future<Facade> fFacade = preview  ? null : ThreadManager.startCallable(new Callable<Facade>() {
				public Facade call() {
					return fs.getFacade(con, user, facadeItem.getProductID());
				}
			});

			Future<Plan> fPlan = preview || planItem == null ? null :  ThreadManager.startCallable(new Callable<Plan>() {
				public Plan call() {
					return PlanService.getInstance().getPlan(con, user, planItem.getProductID());
				}
			});
			Future<HLPackage> fPackage = preview || packageItem == null ? null :  ThreadManager.startCallable(new Callable<HLPackage>() {
				public HLPackage call() {
					return PlanService.getInstance().getPackage(con, user, packageItem.getProductID());
				}
			});
			final Order order = preview ? qv.OrderTemplate : fOrder.get();
			/*
			if (fPackage != null) {
				HLPackage hlp = fPackage.get();
				if (hlp != null && StringUtils.isNotBlank(hlp.EstateProductID)) {
					PDFDao dao = new PDFDao();
					imageHeader = dao.getProductImage(hlp.EstateProductID, "Header");
					imageFooter = dao.getProductImage(hlp.EstateProductID, "Footer");
				}

				HouseLandPackage hl = new HouseLandPackage(con, packageItem.getProductID());
				Object tmp = hl.getProductDetails("ExternalColour", "Alphavalue");
				externalColour = String.valueOf(tmp == null ? "" : tmp);

				tmp = hl.getProductDetails("InternalColour", "Alphavalue");
				internalColour = String.valueOf(tmp == null ? "" : tmp);
			}
			 */
			// Populating Quote View looking for header image and footer text 
			if(qv == null && order.getQuoteViewID() != null && !order.getQuoteViewID().equalsIgnoreCase("")) 
				qv = QuoteViewService.getInstance().getQuoteView(con, user, order.getQuoteViewID());

			//QuoteView qv = QuoteViewService.getInstance().getQuoteView(con, user, order.getQuoteViewID());
			//List<QuoteViewPage> qvpl = QuoteViewService.getInstance().getQuoteViewPages(request, qv.QuoteViewID);
			//User rep = UserResource.getInstance().getUser(request, order.getRepUserID());
			/*
        	Future<QuoteView> fQuoteView = ThreadManager.startCallable(new Callable<QuoteView>() {
        		public QuoteView call() {
        			return QuoteViewService.getInstance().getQuoteView(con, user, order.getQuoteViewID());
        		}
        	});
			 */
			Future<List<QuoteViewPage>> fQuoteViewPages = ThreadManager.startCallable(new Callable<List<QuoteViewPage>>() {
				public List<QuoteViewPage> call() {
					return QuoteViewService.getInstance().getQuoteViewPages(con, user, preview ? quoteViewID  : order.getQuoteViewID());
				}
			});
			Future<User> fRepUser = ThreadManager.startCallable(new Callable<User>() {
				public User call() {
					return UserService.getInstance().getUser(con, user, preview ? user.getUserID() : order.getRepUserID());
				}
			});
			Facade f = preview || facadeItem == null ? new Facade() : fFacade.get();
			final HLPackage pkg = preview || packageItem == null ? new HLPackage() : fPackage.get();
			if(packageItem != null) {
				fPlan = ThreadManager.startCallable(new Callable<Plan>() {
					public Plan call() {
						return PlanService.getInstance().getPlan(con, user, pkg.PlanProductID);
					}
				});
			}
			final Plan plan = fPlan == null ? new Plan() : fPlan.get();

			User rep = fRepUser.get();
			List<OrderProfile> orderProfiles = preview ? null : fOrderProfiles.get();
			List<ContactLocation> lcl = preview ? new ArrayList<ContactLocation>(0) : fContactLocation.get();
			if(!preview && lcl.isEmpty()) {
				throw new RuntimeException("No location defined, this order is incomplete");
			}

			//uncomment if required.
			//QuoteView qv = fQuoteView.get();
			List<QuoteViewPage> qvpl = fQuoteViewPages.get();

			Map<String, Future<com.sok.runway.externalInterface.beans.Document>> docMap = new HashMap<String, Future<com.sok.runway.externalInterface.beans.Document>>();

			for(QuoteViewPage qvp: qvpl) {
				if ("Document".equals(qvp.PageTypeName)) {
					final DocumentOption sd = qvp.SelectedOptions != null ? qvp.SelectedOptions.SelectedDocument : null;
					if(sd != null && StringUtils.isNotBlank(sd.DocumentID)) {
						docMap.put(sd.DocumentID, ThreadManager.startCallable(new Callable<com.sok.runway.externalInterface.beans.Document>() {
							public com.sok.runway.externalInterface.beans.Document call() {
								return DocumentService.getInstance().getDocument(con, user, sd.DocumentID);
							}
						}));
					} else {
						/* make the page inactive so we don't check this later. */ 
						qvp.Active = false;
					}
				}
			}
			//final List<OrderItem> dripItems = getDripItems(orderItems);
			/*
			 * 
			 * We're now storing the drip info inside the order item MetaData, so this is not required.
        	Future<List<PDFDripInfo>> fDripList =  ThreadManager.startCallable(new Callable<List<PDFDripInfo>>() {
        		public List<PDFDripInfo> call() {
        			DripCollection dc = null;
        			logger.debug("testing drip");
        			if(planItem != null) {
        				logger.debug("testing planItem");
        				List<String> list = new ArrayList<String>();
        				if(StringUtils.isNotBlank(plan.RangeProductID)) {
        					list.add(plan.RangeProductID);
        				}
        				if(StringUtils.isNotBlank(plan.HomeProductID)) {
        					list.add(plan.HomeProductID);
        				}
        				if(StringUtils.isNotBlank(plan.ProductID)) {
        					list.add(plan.ProductID);
        				}
        				if(list.isEmpty()) return Collections.emptyList();

        				String[] productIds = new String[list.size()];
        				int ix = 0;
        				for(String s: list) {
        					productIds[ix++] = s;
        				}
        				dc =  DripService.getInstance().getAllDrips(con, user, productIds, new java.util.Date());
        				PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(con, productIds[productIds.length - 1]);
        				DripService.getInstance().populateDripInfo(con, dc, mainProduct, "Quotes", preview ? quoteViewID : order.getQuoteViewID(), null);
        			} else if(packageItem != null) {      
        				logger.debug("testing planItem {}, {}", "Quotes",  preview ? quoteViewID : order.getQuoteViewID());
        				dc =  DripService.getInstance().getAllDrips(con, user, new String[]{packageItem.getProductID(), packageItem.getProductID(), packageItem.getProductID()}, new java.util.Date());
        				PropertyEntity mainProduct = PropertyFactory.getPropertyEntity(con, packageItem.getProductID());
        				DripService.getInstance().populateDripInfo(con, dc, mainProduct, "Quotes",  preview ? quoteViewID : order.getQuoteViewID(), null);
        			} else {
        				logger.debug("no plan or package item");
        				return Collections.emptyList();
        			}

        			List<PDFDripInfo> dripInfo = new ArrayList<PDFDripInfo>();
        			for(DripProduct dp: dc.allDrips) {
        				OrderItem item = getSummaryOrderItemByProductId(dripItems, dp.ProductID);
        				logger.debug("checking {}", dp.Name);
        				if(item != null) {
        					logger.debug("item was in the list of drips");
        					// it has one of the items
        					if(dp.DripInfo != null && dp.DripInfo.ContentList != null) {
        						logger.debug("item had content");
        						PDFDripInfo pdi = new PDFDripInfo();
        						pdi.cost = item.getTotalCost();
        						pdi.includePrice = item.isIncludePrice();
        						pdi.items = new ArrayList<String>();

        						if(!dp.DripInfo.ContentList.isEmpty()) {
        							for(DripService.DripContent dripContent: dp.DripInfo.ContentList) {
        								if(dripContent.ContentType == DripService.DripContentType.Text) {
        									if("Value".equals(dripContent.Placement)) {
        										if(getOrderItemByProductId(dripItems, dp.ProductID, dripContent.Description) != null) {
        											pdi.items.add(dripContent.Description);
            									}	
        									} else if ("Heading".equals(dripContent.Placement)) {
        										pdi.Name = dripContent.Description;
        									} else if ("Description".equals(dripContent.Placement)) {
        										pdi.Description = dripContent.Description;
        									} else if("Cover".equals(dripContent.Placement)) {
        										pdi.Cover = dripContent.Description;
        									}
        								} else if(dripContent.ContentType == DripService.DripContentType.Image && "Image".equals(dripContent.Placement)){
        									logger.debug("Should be setting image path {}", dripContent.ImagePath);
        									pdi.ImagePath = dripContent.ImagePath;
        								}	
        							}
        							dripInfo.add(pdi);
        						}
         					} else {
        						logger.debug("drip info was null for product {}", dp.ProductID);
        					}
        				} else {
        					logger.debug("could not find order item for product {}", dp.ProductID);
        				}
        			}
        			return dripInfo;
        		}
        	});
			 */
			logger.debug("Have QVPL with {} pages", qvpl.size());
			//ProfileService.getInstance().

			/* construct document */
			document = new com.itextpdf.text.Document();
			document.setMargins(20,20,20,80);
			document.setMarginMirroring(true);
			baos = new java.io.ByteArrayOutputStream();
			PdfWriter writer = PdfWriter.getInstance(document, baos);

			//writer.setPageEvent(new PDHFooter(rep, order, plan));

			Conditions prodConditionValues = new Conditions(order.getProductID(), order.getOrderID());
			if(preview) {
				order.setDateOfSale(new java.util.Date());	
				order.setOutcomeDate(new java.util.Date(order.getDateOfSale().getTime() + (1000L * 60L * 60L * 24L * 30L)));	
			}

			TableData openContext = null;
			document.open();
			final String selectedPage = StringUtils.trimToNull(request.getParameter("QuoteViewPageID")); 
			logger.debug("selectedPage : {}" , selectedPage);
			List<Integer> footerList = new ArrayList<Integer>();
			List<Integer> pageNumberList = new ArrayList<Integer>();
			int lastPage = 0;
			for(QuoteViewPage qvp: qvpl) {
				logger.debug("PageTypeName : {}, qvp.QuoteViewPageID : {}" , qvp.PageTypeName, qvp.QuoteViewPageID);
				if((qvp.Active && qvp.DisplayPDF) || qvp.QuoteViewPageID.equals(selectedPage)) { 
					if("Cover Page".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						logger.debug("Adding cover page");
						Map<String, Boolean> pageOptions = null;
						QuoteViewPageOptions customOptions = null;
						for(QuoteViewPage tmpBean: qvpl) {
							if("Custom".equals(tmpBean.PageTypeName))
							{
								customOptions = tmpBean.SelectedOptions;
								pageOptions = customOptions.PageOptions;
							}
						}
						addCoverPage(document, qvp, order, orderItems, f, preview ? new ContactLocation() : lcl.get(0), plan, pkg, orderProfiles, qv, pageOptions);
						footerList.add(writer.getPageNumber());
						pageNumberList.add(writer.getPageNumber());
						int page = writer.getPageNumber();
						logger.debug("Last: {}, thisPage: {}", lastPage, page);
						for(int i = (lastPage +1); ;i++) { 
							footerList.add(i);
							pageNumberList.add(i);
							if(i >= page) 
								break;
						}
						lastPage = page;
					} else if ("Document".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						DocumentOption sd = qvp.SelectedOptions.SelectedDocument;
						boolean insertDocument = prodConditionValues.evaluate(qvp.Conditions);
						if(insertDocument)
						{
							addDocumentPages(document, writer, sd, docMap.get(sd.DocumentID).get());
							int page = writer.getPageNumber();
							logger.debug("Last: {}, thisPage: {}", lastPage, page);
							for(int i = (lastPage +1); ;i++) { 
								pageNumberList.add(i);
								if(i >= page) 
									break;
							}
							lastPage = page;
						}
					} else if ("VPB".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						boolean footerFlag = addOptionPages(document, orderItems, qv);
						if(footerFlag)
						{
							int page = writer.getPageNumber();
							logger.debug("Last: {}, thisPage: {}", lastPage, page);
							for(int i = (lastPage +1); ;i++) { 
								footerList.add(i);
								pageNumberList.add(i);
								if(i >= page) 
									break;
							}
							lastPage = page;
						}
					} else if ("DRIP".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {

						//List<PDFDripInfo> driplist = fDripList.get();
						/* needs to have :
						 * included drip items with image and or text 
						 * 
						 * plus
						 * optional selected drip items with image and or text. 
						 */
						boolean footerFlag = addDripPage(document, qvp, orderItems, qv);
						if(footerFlag)
						{
							int page = writer.getPageNumber();
							logger.debug("Last: {}, thisPage: {}", lastPage, page);
							for(int i = (lastPage +1); ;i++) { 
								footerList.add(i);
								pageNumberList.add(i);
								if(i >= page) 
									break;
							}
							lastPage = page;
						}
					} else if ("Custom".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						boolean footerFlag = addCustomPages(document, orderItems, qvp, qv);

						if(footerFlag)
						{
							int page = writer.getPageNumber();
							logger.debug("Last: {}, thisPage: {}", lastPage, page);
							for(int i = (lastPage +1); ;i++) { 
								footerList.add(i);
								pageNumberList.add(i);
								if(i >= page) 
									break;
							}
							lastPage = page;
						}
					} else if("Open".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						if(openContext == null) {
							openContext = new TableBean();
							List<OrderContact> lc = order.getLinkedContacts();
							int size = lc.size();
							if(size != 0) {
								openContext.put("FirstName", lc.get(0).getFirstName());
								openContext.put("LastName", lc.get(0).getLastName());
								Address a = lc.get(0).getPhysicalAddress();
								if(a != null) { 
									openContext.put("Street", a.getStreet());
									openContext.put("Street2", a.getStreet2());
									openContext.put("City", a.getCity());
									openContext.put("State", a.getState());
									openContext.put("Postcode", a.getPostcode());
									openContext.put("Country", a.getCountry());
								}
								openContext.put("Email", lc.get(0).getEmail());
								openContext.put("Phone",  lc.get(0).getPhone());
								openContext.put("Mobile",  lc.get(0).getMobile());
							}
							if(size > 1) {
								openContext.put("SecondaryFirstName", lc.get(1).getFirstName());
								openContext.put("SecondaryLastName", lc.get(1).getLastName());
								Address a = lc.get(1).getPhysicalAddress();
								if(a != null) { 
									openContext.put("SecondaryStreet", a.getStreet());
									openContext.put("SecondaryStreet2", a.getStreet2());
									openContext.put("SecondaryCity", a.getCity());
									openContext.put("SecondaryState", a.getState());
									openContext.put("SecondaryPostcode", a.getPostcode());
									openContext.put("SecondaryCountry", a.getCountry());
								}
								openContext.put("SecondaryEmail", lc.get(1).getEmail());
								openContext.put("SecondaryPhone",  lc.get(1).getPhone());
								openContext.put("SecondaryMobile",  lc.get(1).getMobile());
							}

							openContext.put("ContactName", order.getContactName());
							openContext.put("RepName", order.getRepName());

							UserBean orderRep = UserService.getInstance().getSimpleUser(con, order.getRepUserID());
							for(String key: new String[]{"FirstName","LastName","Street","City","Postcode","State","Country","Email","Phone"}) {
								openContext.put("Rep" + key, orderRep.getString(key));
							}
						}
						addOpenPage(document, qvp, orderItems, openContext, qv); 
						int page = writer.getPageNumber();
						logger.debug("Last: {}, thisPage: {}", lastPage, page);
						for(int i = (lastPage +1); ;i++) { 
							footerList.add(i);
							pageNumberList.add(i);
							if(i >= page) 
								break;
						}
						lastPage = page;
					} else if ("Summary".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						addSummaryPage(document, qvp, orderItems);
						int page = writer.getPageNumber();
						logger.debug("Last: {}, thisPage: {}", lastPage, page);
						if(page > lastPage) {
							for(int i = (lastPage +1); ;i++) { 
								footerList.add(i);
								if(i >= page) 
									break;
							}
						}
						lastPage = page;
					} else if("Floorplan".equals(qvp.PageTypeName) && (selectedPage == null || selectedPage.equals(qvp.QuoteViewPageID))) {
						this.addFloorplanPage(document, qvp, plan.ImagePath);
						int page = writer.getPageNumber();
						logger.debug("Last: {}, thisPage: {}", lastPage, page);
						for(int i = (lastPage +1); ;i++) { 
							footerList.add(i);
							pageNumberList.add(i);
							if(i >= page) 
								break;
						}
						lastPage = page;
					}
				}
			} 
			document.close();
			document = null;
			reader = new PdfReader(baos.toByteArray());

			stamp = new PdfStamper(reader, response.getOutputStream());
			response.setHeader("Content-Type", "application/pdf");


			PdfPTable pageTable = new PdfPTable(5);
			pageTable.setWidths(getDesignWidths(226, 22, 226, 32, 361));
			pageTable.setTotalWidth(556f);
			pageTable.setLockedWidth(true);
			pageTable.getDefaultCell().setPadding(10);
			pageTable.getDefaultCell().setBorder(0);
			pageTable.getDefaultCell().setPadding(7);
			pageTable.getDefaultCell().setColspan(5);


			setHeader(pageTable);
			pageTable.addCell(getRepParagraph(rep.getName(), rep.getMobile(), rep.getPhone(), rep.getEmail()));

			long days = (order.getOutcomeDateLong() - order.getDateOfSaleLong()) / ( 1000 * 60 * 60 * 24);
			setNormal(pageTable);
			pageTable.getDefaultCell().setBackgroundColor(panelBackground);
			pageTable.getDefaultCell().setPadding(4);
			pageTable.getDefaultCell().setPaddingBottom(8);
			pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			pageTable.addCell(getValidity(days, order.getOutcomeDate(), qv));
			/*
			if (StringUtils.isNotBlank(imageFooter)) {
				pageTable.getDefaultCell().setPaddingTop(0);
				pageTable.getDefaultCell().setPadding(0);
				pageTable.addCell(getEstateFooter());
			}
			 */
			/*
			PdfPTable pageNos = new PdfPTable(1);
			pageNos.setTotalWidth(556f);
			pageNos.setLockedWidth(true);
			pageNos.getDefaultCell().setPadding(0);
			pageNos.getDefaultCell().setBorder(0);
			 */


			for(Integer i: footerList) {
				logger.debug("adding footer on page {}", i);
				//we'll stamp on top.
				pageTable.writeSelectedRows(0, -1, 20, 50, stamp.getOverContent(i));
			}

		} catch (Exception e) {
			logger.error("Exception in order pdf", e);
			try {
				if(document == null) { 
					document = new com.itextpdf.text.Document();
					document.setMargins(0,0,0,0);
					document.setMarginMirroring(true);
					PdfWriter writer = PdfWriter.getInstance(document, outputStream);
					document.open();
				}
				Paragraph error = new Paragraph();
				error.add(new Chunk(ActionBean
						.writeStackTraceToString(e), FontFactory
						.getFont(FontFactory.HELVETICA, 8)));
				document.add(error);
			} catch (Exception ex) {
				System.out.println(ActionBean
						.writeStackTraceToString(ex));
			}
		} finally {
			if (document != null) {
				document.close();
			}
			if(reader != null) { 
				reader.close();
			}
			if(stamp != null) { 
				try { stamp.close(); } catch (DocumentException de) { }
			}
		}
	}	

	private static final VelocityEngine ve;
	static {
		try { 
			ve = VelocityManager.getEngine();
		} catch (Exception e) {
			logger.error("Unable to get Velocity Engine", e);
			throw new RuntimeException(e);
		}
	}
	private String evaluate(String velocity, TableData context) {
		if(logger.isDebugEnabled()) {
			logger.debug(velocity);
			for(Object o: context.getKeys()) {
				logger.debug("{}={}",o, context.get(o));
			}
		}
		StringWriter sw = new StringWriter();
		try {
			ve.evaluate(context, sw, this.getClass().getName(), velocity);
			sw.flush();
			sw.close();
		} catch (Exception e) {
			logger.error("Error evaluating field", e);
			return velocity;
		}
		return (sw.toString());
	}


	private void addOpenPage(Document document, QuoteViewPage qvp, List<OrderItem> orderItems, TableData ctx, QuoteView qView) throws DocumentException {
		OrderItem item = this.getOpenOrderItem(orderItems, qvp);

		document.newPage();

		PdfPTable itemTable = new PdfPTable(1);
		itemTable.setWidthPercentage(100f);
		itemTable.getDefaultCell().setPadding(0);
		itemTable.getDefaultCell().setBorder(0);

		itemTable.getDefaultCell().setColspan(5);
		itemTable.getDefaultCell().setPaddingTop(15);
		itemTable.getDefaultCell().setPaddingBottom(15);

		itemTable.getDefaultCell().setPaddingLeft(60);
		itemTable.getDefaultCell().setPaddingRight(60);

		itemTable.addCell(getHeadingTableLeft(qvp.PageName, qView));

		itemTable.getDefaultCell().setColspan(0);

		itemTable.addCell(BLANK);

		if(item != null) {
			String text = item.getDescription();
			if(text != null) {
				PdfPCell c = new PdfPCell();	//need to use seperate cell to use leading for some reason.
				c.setPaddingLeft(60);
				c.setPaddingRight(60);
				c.setBorder(0);
				c.addElement(getText(evaluate(text, ctx), textFont, textFontBold));

				itemTable.addCell(c);
			} else {
				itemTable.addCell(new Phrase("This page is intentionally left blank", textFont));
			}
		} else {
			itemTable.addCell(new Phrase("This page is intentionally left blank", textFont));
		}
		document.add(itemTable);
	}


	private Phrase getText(String text, Font normFont, Font boldFont) {

		String noComments = text.replaceAll("(?s)<!--.*?-->", "");	//remove html comments, such as added in by word
		//System.out.println(noComments);
		String switchBr = StringUtil.replace(noComments, "<br />", "<br/>");	//the regex matches on <br/>, ck adds <br />
		//System.out.println(switchBr);
		Matcher matcher = StringUtil.newlineregex.matcher(switchBr);
		String newString = matcher.replaceAll("");
		//System.out.println(newString);
		matcher = StringUtil.pbrregex.matcher(newString);
		newString = matcher.replaceAll("\r\n");
		//System.out.println(newString);
		StrTokenizer st = new StrTokenizer(newString, "<strong>");
		String init = st.nextToken();

		Phrase p = new Phrase(13, new Chunk(StringEscapeUtils.unescapeHtml(StringUtil.stripHtml(init)), normFont));

		while(st.hasNext()) {
			String s = st.nextToken();
			if(s.length() > 0) { 
				StrTokenizer cl = new StrTokenizer(s, "</strong>");
				if(cl.hasNext()) {
					String bold = StringUtil.stripHtml(cl.nextToken());
					if(bold.length()> 0) { 
						p.add(new Chunk(StringEscapeUtils.unescapeHtml(bold), boldFont));
						//System.out.print("=======");
						//System.out.print(StringEscapeUtils.unescapeHtml(StringUtil.stripHtml(bold)));
						//System.out.print("=======");
					}
					while(cl.hasNext()) {
						String norm = StringUtil.stripHtml(cl.nextToken());
						if(norm.length() > 0) { 
							p.add(new Chunk(StringEscapeUtils.unescapeHtml(norm), normFont));
							//System.out.print(StringEscapeUtils.unescapeHtml(norm));
						}
					}
				}
			} 
		}
		return p;
	}

	private void addFloorplanPage(Document document, QuoteViewPage qvp, String path) throws DocumentException {
		logger.debug("Adding floorplan page with path {}", path);
		document.newPage();

		PdfPTable itemTable = new PdfPTable(1);
		itemTable.setWidthPercentage(100f);
		itemTable.getDefaultCell().setPadding(0);
		itemTable.getDefaultCell().setBorder(0);

		itemTable.getDefaultCell().setColspan(5);
		itemTable.getDefaultCell().setPaddingTop(15);
		itemTable.getDefaultCell().setPaddingBottom(15);

		//itemTable.addCell(getHeadingTable(qvp.PageName));
		itemTable.addCell(new Phrase(qvp.PageName, quotationHeader));
		itemTable.getDefaultCell().setColspan(0);

		itemTable.addCell(BLANK);

		try {
			String realPath = InitServlet.getRealPath("/" + path);
			Image rim = Image.getInstance((InitServlet.getRealPath("/" + path)));
			rim.scaleToFit(600f, 650f);

			PdfPCell cell = new PdfPCell(rim, true);
			cell.setFixedHeight(650f);
			cell.setBorder(0);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);

			logger.debug("Floorplan realpath: {} was null {}", realPath, rim == null);
			itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			itemTable.addCell(cell);
		} catch (Exception e) {
			itemTable.addCell(new Phrase("Sorry, we were unable to find a suitable floorplan image."));
			logger.error("Error adding pdf image " , e);
		}
		document.add(itemTable);
	}

	private boolean addDripPage(Document document, QuoteViewPage qvp, List<OrderItem> orderItems, QuoteView qView) throws DocumentException {
		List<OrderItem> dripItems = getDripItems(orderItems);
		if(logger.isDebugEnabled()) logger.debug("addDripPage(document, QuoteViewPage, DripItems({})", dripItems.size());
		boolean checkFlag = false;

		for(OrderItem pdi: dripItems) {
			if(StringUtils.isNotBlank(pdi.getMetaData()) && !"{}".equalsIgnoreCase(pdi.getMetaData())) {
				checkFlag = true;
			}
		}

		if(checkFlag)
		{
			document.newPage();
			PdfPTable itemTable = new PdfPTable(1);
			itemTable.setWidthPercentage(100f);
			itemTable.getDefaultCell().setPadding(0);
			itemTable.getDefaultCell().setBorder(0);
			itemTable.getDefaultCell().setPaddingTop(15);
			itemTable.getDefaultCell().setPaddingBottom(15);
			itemTable.addCell(getHeadingTable(qvp.PageName, qView));

			setHeader(itemTable);
			itemTable.getDefaultCell().setPaddingLeft(7);
			itemTable.addCell(new Phrase("Selected Promotions", subHeader));
			setNormal(itemTable);
			itemTable.getDefaultCell().setBackgroundColor(panelBackground);
			itemTable.addCell(BLANK);

			itemTable.getDefaultCell().setPaddingLeft(7);

			boolean first = true;
			JSONObject dripContent = null;

			JSONParser jp = new JSONParser();

			for(OrderItem pdi: dripItems) {
				if(!pdi.isIncludeSummary()) {
					//items that are child items will be include summary = N
					logger.debug("skipping item {} as include summary = N", pdi.getOrderItemID());
					continue;
				}
				if(StringUtils.isBlank(pdi.getMetaData())) {
					logger.error("Drip Item meta data was blank for OrderItemID=[{}]", pdi.getOrderItemID());
					continue;
				}
				try { 
					Object obj = jp.parse(pdi.getMetaData());
					if(obj instanceof JSONObject) {
						dripContent = (JSONObject)obj;
					} else {
						logger.error("Got a different instance than expected for MetaData {} in OrderItemID=[{}]", obj.getClass().getName(), pdi.getOrderItemID());
						continue;
					}
				} catch (ParseException pe) {
					logger.error("Error parsing Drip Meta Data", pe);
					/* attempt to load from drip ? */
					continue;
				}
				logger.debug("Processing DRIP Item for PDF Content OrderItemID=[{}], Description=[{}]", pdi.getOrderItemID(), pdi.getDescription());

				if(first) {
					first = false;
				} else {
					//TODO draw the line here.
				}

				itemTable.getDefaultCell().setPaddingTop(20);

				Object o = null; String t = null;

				if((o = dripContent.get("Name")) != null && o instanceof String && StringUtils.isNotBlank(t = (String)o)) {
					itemTable.addCell(new Phrase(t, dripTitleFont));
					logger.debug("OrderItemID=[{}] Heading: {}",pdi.getOrderItemID(), t);
				}
				if((o = dripContent.get("ImagePath")) != null && o instanceof String && StringUtils.isNotBlank(t = (String)o)) {
					logger.debug("OrderItemID=[{}] ImagePath: {}",pdi.getOrderItemID(), t);
					try {
						Image rim = Image.getInstance((InitServlet.getRealPath("/" + t)));
						//Image rim = PDFUtil.createImage(InitServlet.getRealPath("/" + t)); //PropertyEntity.resizeImage(pdi.ImagePath, 1400,	250, PropertyEntity.ResizeOption.Fit));		
						itemTable.getDefaultCell().setPaddingRight(100);
						itemTable.addCell(rim);
					} catch (Exception e) {
						logger.error("Error adding pdf image " , e);
					}
				}

				itemTable.getDefaultCell().setPaddingRight(0);
				if((o = dripContent.get("Description")) != null && o instanceof String && StringUtils.isNotBlank(t = (String)o)) {
					logger.debug("OrderItemID=[{}] Description: {}",pdi.getOrderItemID(), t);
					itemTable.addCell(new Phrase(t, textFont));
				}
				itemTable.getDefaultCell().setPaddingTop(10);
				JSONArray items = null;
				if((o = dripContent.get("Items")) != null && o instanceof JSONArray && !(items = (JSONArray)o).isEmpty()) {
					for(Object so: items) {
						if(so instanceof JSONObject) {	//original implementation used this
							JSONObject itemObj = (JSONObject)so;
							//what we're expecting. 
							if((o = itemObj.get("Description")) != null && o instanceof String && StringUtils.isNotBlank(t = (String)o)) {
								//now, see if this exists as an order item. 
								if(hasSelectedPromoItem(dripItems, pdi.getProductID(), t)) {
									logger.debug("OrderItemID=[{}] SelectedItem: {}",pdi.getOrderItemID(), t);
									itemTable.addCell(new Phrase(t, textFont));
								}
							}
						} else if(so instanceof String && StringUtils.isNotBlank(t = (String)so)) {
							//this is how the conversion process does it. 
							if(hasSelectedPromoItem(dripItems, pdi.getProductID(), t)) {
								logger.debug("OrderItemID=[{}] SelectedItem: {}",pdi.getOrderItemID(), t);
								itemTable.addCell(new Phrase(t, textFont));
							}
						}
					}
				}
			}
			document.add(itemTable);
		}
		return checkFlag;
	}

	private boolean hasSelectedPromoItem(List<OrderItem> dripItems, String productID, String t) {
		logger.debug("hasSelectedPromoItem(List, {}, {})", productID, t);
		if(dripItems != null && StringUtils.isNotBlank(productID) && StringUtils.isNotBlank(t)) {
			for(OrderItem oi: dripItems) {
				if(!oi.isIncludeSummary() && productID.equals(oi.getProductID()) && t.equals(oi.getDescription())) {
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * This has been removed as we will now store the drip info in the order item. 
	private void addDripPage(Document document, QuoteViewPage qvp, List<PDFDripInfo> list) throws DocumentException {

		document.newPage();
		PdfPTable itemTable = new PdfPTable(1);
		itemTable.setWidthPercentage(100f);
		itemTable.getDefaultCell().setPadding(0);
		itemTable.getDefaultCell().setBorder(0);
		itemTable.getDefaultCell().setPaddingTop(15);
		itemTable.getDefaultCell().setPaddingBottom(15);
		itemTable.addCell(getHeadingTable(qvp.PageName));

		setHeader(itemTable);
		itemTable.getDefaultCell().setPaddingLeft(7);
		itemTable.addCell(new Phrase("Selected Promotions", subHeader));
		setNormal(itemTable);
		itemTable.getDefaultCell().setBackgroundColor(panelBackground);
		itemTable.addCell(BLANK);
		logger.debug("drip list size: {}", list.size());
		itemTable.getDefaultCell().setPaddingLeft(7);

		boolean first = true;

		for(PDFDripInfo pdi: list) {
			if(first) {
				first = false;
			} else {
				//TODO draw the line here.

			}
			itemTable.getDefaultCell().setPaddingTop(20);

			if(pdi.Name != null) { 
				itemTable.addCell(new Phrase(pdi.Name, dripTitleFont));
			}
			if(pdi.ImagePath != null) {
				try {
					Image rim = PDFUtil.createImage(InitServlet.getRealPath("/" + pdi.ImagePath)); //PropertyEntity.resizeImage(pdi.ImagePath, 1400,	250, PropertyEntity.ResizeOption.Fit));		
					itemTable.getDefaultCell().setPaddingRight(100);
					itemTable.addCell(rim);
				} catch (Exception e) {
					logger.error("Error adding pdf image " , e);
				}
			}

			itemTable.getDefaultCell().setPaddingRight(0);
			if(pdi.Description != null) {
				itemTable.addCell(new Phrase(pdi.Description, textFont));
			}
			itemTable.getDefaultCell().setPaddingTop(10);
			if(!pdi.items.isEmpty()) {
				for(String s: pdi.items) {
					itemTable.addCell(new Phrase(s, textFont));
				}
			}
		}
		document.add(itemTable);
	}
	 */
	private static class PDFDripInfo {
		public String Cover = null;
		public String Name;
		public boolean includePrice = false;
		public String Description;
		public String ImagePath;
		public List<String> items = Collections.emptyList();
		public double cost = 0d;

		@Override
		public String toString() {
			return String.format("%1$s %2$s %3$s %4$s ", Cover, Name, Description, ImagePath);
		}
	}

	private boolean addOptionPages(Document document, List<OrderItem> orderItems, QuoteView qView) throws DocumentException {
		int size = orderItems.size() - 4;
		List<OrderItem> vpbItems = new ArrayList<OrderItem>(size > 0 ? size : 0);	//at least that many will be plans, facade, open, cover 
		for(OrderItem item: orderItems) {
			if("VPB".equals(item.getCategory())) { 
				vpbItems.add(item);
			}
		}
		Collections.sort(vpbItems, new Comparator<OrderItem>() {
			public int compare(OrderItem oi1, OrderItem oi2) {
				if(oi2.getItemGroupSort() == oi1.getItemGroupSort()) {
					if(oi2.getSortNumber() < oi1.getSortNumber()) {
						return 1;
					} else if (oi2.getSortNumber() == oi1.getSortNumber()) {
						return 0;
					}
					return -1;
				}
				if(oi2.getItemGroupSort() < oi1.getItemGroupSort()) {
					return 1;
				} 
				return -1;
			}
			public boolean equals(OrderItem oi1, OrderItem oi2) {
				return oi2.getItemGroupSort() == oi1.getItemGroupSort() && oi2.getSortNumber() == oi1.getSortNumber();
			}
		});
		if(vpbItems.size() > 0)
		{
			document.newPage();
			addItemPages(document, vpbItems, "Options", qView);
			return true;
		}
		return false;
	} 

	private boolean addCustomPages(Document document, List<OrderItem> orderItems, QuoteViewPage qvp, QuoteView qView) throws DocumentException {
		String customCat = "Custom-" + qvp.QuoteViewPageID, customName = qvp.PageName;
		if(StringUtils.isBlank(customCat)) {
			return false;
		}
		int size = orderItems.size() - 4;
		List<OrderItem> customItems = new ArrayList<OrderItem>(size > 0 ? size : 0);	//at least that many will be plans, facade, open, cover 
		for(OrderItem item: orderItems) {
			if(customCat.equals(item.getCategory())) { 
				customItems.add(item);
			}
		}
		Collections.sort(customItems, new Comparator<OrderItem>() {
			public int compare(OrderItem oi1, OrderItem oi2) {
				if(oi2.getItemGroupSort() == oi1.getItemGroupSort()) {
					if(oi2.getSortNumber() < oi1.getSortNumber()) {
						return 1;
					} else if (oi2.getSortNumber() == oi1.getSortNumber()) {
						return 0;
					} 
					return -1;
				}
				if(oi2.getItemGroupSort() < oi1.getItemGroupSort()) {
					return 1;
				} 
				return -1;
			}
			//I don't think this is right - method desc would suggest that this method would never get called and 
			//is for the comparator itself, not the items. 
			//public abstract boolean equals(java.lang.Object arg0);
			//public boolean equals(OrderItem oi1, OrderItem oi2) {
			//	return oi2.getItemGroupSort() == oi1.getItemGroupSort() && oi2.getSortNumber() == oi1.getSortNumber();
			//}
		});
		if(customItems.size() > 0)
		{
			document.newPage();
			addItemPages(document, customItems, customName, qView);
			return true;
		}
		return false;
	} 

	private void addItemPages(Document document, List<OrderItem> orderItems, String pageHeading, QuoteView qView) throws DocumentException {

		String groupName = null;
		final float[] widths = getDesignWidths(12, 604,  78, 80, 93);
		PdfPTable itemTable = null;
		boolean first = false;
		boolean newPage = true;
		double groupTotal = 0;
		double optionTotal = 0;
		NumberFormat mf = NumberFormat.getCurrencyInstance();
		mf.setMaximumFractionDigits(0);

		for(OrderItem item: orderItems) {
			if(groupName == null || !groupName.equals(item.getItemGroup())) {
				if(itemTable != null) {
					itemTable.getDefaultCell().setPaddingTop(15);
					itemTable.getDefaultCell().setPaddingBottom(15);
					/* do subtotal row */
					itemTable.addCell(BLANK);
					itemTable.addCell(new Phrase("SUB TOTAL", costSubtotalFont));
					itemTable.addCell(BLANK);
					itemTable.addCell(BLANK);
					itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
					itemTable.addCell(new Phrase(formatMoney(groupTotal, mf), costSubtotalFont));
					itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

					itemTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
					itemTable.getDefaultCell().setColspan(5);
					itemTable.addCell(BLANK);

					optionTotal += groupTotal;
					groupTotal = 0;
					document.add(itemTable);

					itemTable = null;
					groupName = null;
				}
				if("PAGE_BREAK".equals(item.getName())) {
					document.newPage();
					newPage = true;
					continue;
					/* do new heading? */
				}
				groupName = item.getItemGroup();
				itemTable = new PdfPTable(5);
				itemTable.setWidths(widths);
				itemTable.setWidthPercentage(100f);
				itemTable.getDefaultCell().setPadding(0);
				itemTable.getDefaultCell().setBorder(0);

				if(newPage) {
					itemTable.getDefaultCell().setColspan(5);
					itemTable.getDefaultCell().setPaddingTop(15);
					itemTable.getDefaultCell().setPaddingBottom(15);

					//itemTable.addCell(new Phrase(pageHeading, quotationHeader));
					itemTable.addCell(getHeadingTable(pageHeading, qView));
					itemTable.getDefaultCell().setColspan(1);
					newPage = false;
				}

				setHeader(itemTable);
				itemTable.addCell(BLANK);
				itemTable.getDefaultCell().setPaddingLeft(0);
				itemTable.addCell(new Phrase(groupName, subHeader));
				itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
				itemTable.addCell(new Phrase("Qty", subHeader));
				itemTable.addCell(new Phrase("Unit", subHeader));
				itemTable.addCell(new Phrase("Inc GST", subHeader));
				itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				setNormal(itemTable);
				itemTable.getDefaultCell().setBackgroundColor(panelBackground);
				itemTable.getDefaultCell().setPaddingTop(5);
				itemTable.getDefaultCell().setPaddingBottom(5);
				first = true;
			}
			if("PAGE_BREAK".equals(item.getName())) {
				if(itemTable != null) {
					document.add(itemTable);
				}
				document.newPage();
				itemTable = new PdfPTable(5);
				itemTable.setWidths(widths);
				itemTable.setWidthPercentage(100f);
				itemTable.getDefaultCell().setPadding(0);
				itemTable.getDefaultCell().setBorder(0);

				itemTable.getDefaultCell().setColspan(5);
				itemTable.getDefaultCell().setPaddingTop(15);
				itemTable.getDefaultCell().setPaddingBottom(15);
				itemTable.addCell(new Phrase(pageHeading, quotationHeader));
				itemTable.getDefaultCell().setColspan(0);
				if(groupName != null) {
					setHeader(itemTable);
					itemTable.addCell(BLANK);
					itemTable.getDefaultCell().setPaddingLeft(0);
					itemTable.addCell(new Phrase(groupName + " cont...", subHeader));
					itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
					itemTable.addCell(new Phrase("Qty", subHeader));
					itemTable.addCell(new Phrase("Unit", subHeader));
					itemTable.addCell(new Phrase("Inc GST", subHeader));
					itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
					setNormal(itemTable);
					itemTable.getDefaultCell().setBackgroundColor(panelBackground);
					itemTable.getDefaultCell().setPaddingTop(5);
					itemTable.getDefaultCell().setPaddingBottom(5);
					first = true;
				}
				continue;
				/* do new heading? */
			}
			itemTable.addCell(BLANK);
			itemTable.addCell(new Phrase(item.getDescription(), textFont));
			//POSS CHANGES HERE, QTY Formatting.
			itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			itemTable.addCell(new Phrase(String.valueOf(item.getQuantity()), textFont));
			itemTable.addCell(new Phrase(item.getItemCost() != 0 ? formatMoney(item.getItemCost(), mf) : "", textFont));
			itemTable.addCell(new Phrase(formatMoney(item.getTotalCost(), mf), textFont));
			itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

			groupTotal += item.getTotalCost();
			if(first) {
				itemTable.getDefaultCell().setPaddingTop(0);
				first = false;
			}
		}

		/* this can happen if a page break is left at the end */
		if(itemTable == null) {
			itemTable = new PdfPTable(5);
			itemTable.setWidths(widths);
			itemTable.setWidthPercentage(100f);
			itemTable.getDefaultCell().setPadding(0);
			itemTable.getDefaultCell().setBorder(0);
		}

		if(groupName != null) {
			itemTable.getDefaultCell().setBackgroundColor(panelBackground);
			itemTable.getDefaultCell().setPaddingTop(10);
			itemTable.getDefaultCell().setPaddingBottom(10);
			/* end group */
			itemTable.addCell(BLANK);
			itemTable.addCell(new Phrase("SUB TOTAL", costSubtotalFont));
			itemTable.addCell(BLANK);
			itemTable.addCell(BLANK);
			itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			itemTable.addCell(new Phrase(formatMoney(groupTotal, mf), costSubtotalFont));
			itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

			optionTotal += groupTotal;
		}
		itemTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		itemTable.getDefaultCell().setColspan(5);
		itemTable.addCell(BLANK);
		itemTable.getDefaultCell().setColspan(1);
		itemTable.addCell(BLANK);
		itemTable.addCell(new Phrase("TOTAL", costTotalFont));
		itemTable.addCell(BLANK);
		itemTable.addCell(BLANK);
		itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		itemTable.addCell(new Phrase(formatMoney(optionTotal, mf), costTotalFont));
		itemTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

		document.add(itemTable);
	}

	private void addSummaryPage(Document document, QuoteViewPage qvp, List<OrderItem> orderItems) throws DocumentException {

		JSONObject summaryMeta = null;
		String key = "Summary-" + qvp.QuoteViewPageID;
		for(OrderItem oi: orderItems) {
			if(key.equals(oi.getCategory())) {
				try {
					summaryMeta = (JSONObject)JSONValue.parseWithException(oi.getMetaData());
				} catch (ParseException pe) {
					logger.error("Was not able to parse Summary meta data", pe);
					return;
				} catch (ClassCastException cce) {
					logger.error("Some other object was stored in meta, expecting object [{}]", oi.getMetaData());
					return;
				}
				break;
			}
		}
		if(summaryMeta == null) { 
			logger.debug("no summary order item found for key {}, skipping page.", key);
			return;
		}
		document.newPage();

		PdfPTable itemTable = null;
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMaximumFractionDigits(0);
		final float[] widths = getDesignWidths(12, 604,  78, 80, 93);

		itemTable = new PdfPTable(5);
		itemTable.setWidths(widths);
		itemTable.setWidthPercentage(100f);
		itemTable.getDefaultCell().setPadding(0);
		itemTable.getDefaultCell().setBorder(0);

		itemTable.getDefaultCell().setColspan(5);
		itemTable.getDefaultCell().setPaddingTop(15);
		itemTable.getDefaultCell().setPaddingBottom(15);
		itemTable.addCell(new Phrase(qvp.PageName, quotationHeader));
		itemTable.getDefaultCell().setColspan(0);

		document.add(itemTable);

		PdfPTable innerTable = new PdfPTable(3);
		innerTable.setWidths(getDesignWidths( 12, 762, 93));
		innerTable.setWidthPercentage(100f);
		innerTable.getDefaultCell().setBorder(0);
		innerTable.getDefaultCell().setColspan(2);

		innerTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		innerTable.getDefaultCell().setColspan(1);
		innerTable.getDefaultCell().setPaddingTop(5);
		innerTable.getDefaultCell().setPaddingBottom(7);

		JSONArray items = (JSONArray)(summaryMeta.containsKey("items") ? summaryMeta.get("items") : null);

		Object oc = (summaryMeta.containsKey("TotalCost") ? summaryMeta.get("TotalCost") : null);
		Number tc = null;
		if(oc != null && oc instanceof Number) tc = (Number)oc;

		if(items != null) {
			for(Object o: items) {
				JSONObject item = (JSONObject)o;

				setHeader(innerTable);
				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				innerTable.addCell(new Paragraph(String.valueOf(item.get("Name")), subHeader)); 
				innerTable.addCell(BLANK);
				setNormal(innerTable);
				innerTable.getDefaultCell().setPaddingTop(5);
				innerTable.getDefaultCell().setPaddingBottom(5);

				JSONArray groups = (JSONArray)(item.containsKey("Groups") ? item.get("Groups") : null);

				Number itemcost = (Number)(item.containsKey("TotalCost") ? item.get("TotalCost") : null);

				if(groups != null) {
					for(Object o2: groups) {
						JSONObject gi = (JSONObject)o2;
						Number groupcost = (Number)(gi.containsKey("TotalCost") ? gi.get("TotalCost") : null);
						Boolean showSummary = (Boolean)(gi.containsKey("ShowSubtotal") ? gi.get("ShowSubtotal") : false);

						innerTable.addCell(BLANK);
						innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
						innerTable.addCell(new Paragraph(String.valueOf( gi.get("Name")), textFont)); 
						if(showSummary) {
							innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
							innerTable.getDefaultCell().setPaddingRight(12);
							innerTable.addCell(new Paragraph(formatMoney(groupcost.doubleValue(), nf), textFont));
							innerTable.getDefaultCell().setPaddingRight(0);
						} else {
							innerTable.addCell(BLANK);
						}
					}
				}
				innerTable.addCell(BLANK);
				innerTable.addCell(BLANK);
				innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				innerTable.getDefaultCell().setPaddingRight(12);
				innerTable.addCell(new Paragraph(formatMoney(itemcost.doubleValue(), nf), costSubtotalFont));
				innerTable.getDefaultCell().setPaddingRight(0);
			}
		}
		if(tc != null) {
			innerTable.addCell(BLANK);
			innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			innerTable.addCell(new Paragraph("TOTAL", costTotalFont));
			innerTable.getDefaultCell().setPaddingRight(12);
			innerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			innerTable.addCell(new Paragraph(formatMoney(tc.doubleValue(), nf), costTotalFont));
			innerTable.getDefaultCell().setPaddingRight(0);
		}

		document.add(innerTable);
	}

	private void addDocumentPages(Document document, PdfWriter writer, DocumentOption sd, com.sok.runway.externalInterface.beans.Document runwayDocument) {
		String[] pageRange = StringUtils.trimToEmpty(sd.Pages).split("-"); 
		if(pageRange.length == 0) 
			return;
		if(pageRange.length == 1) {
			if(StringUtils.isBlank(pageRange[0])) {
				return;
			}
		} 
		int[] intRange = new int[2];
		try { 
			intRange[0] = Integer.parseInt(pageRange[0]);
			intRange[1] = pageRange.length == 1 ? Integer.parseInt(pageRange[0]) : Integer.parseInt(pageRange[1]);
		} catch (NumberFormatException nfe) {
			logger.error("Error parsing page range [" + sd.Pages + "]", nfe);
			intRange = new int[]{1,1};
		}
		final String realPath = InitServlet.getRealPath(runwayDocument.getFilePath());
		logger.debug("Adding pages [{}] from document at {}", sd.Pages, realPath);

		PdfContentByte cb = writer.getDirectContent();
		try { 
			// Load existing PDF
			PdfReader reader = new PdfReader(realPath);
			int max = reader.getNumberOfPages();
			PdfImportedPage page = null;
			for(int i = intRange[0]; ; i++) {
				page = writer.getImportedPage(reader, i); 
				document.newPage();
				cb.addTemplate(page, 0, 0);

				if(i == intRange[1]) 
					break;
				if(i == max) {
					logger.debug("Reader did not have any more pages Pages: {}, Req {}", intRange[1], max);
					break;
				}
			}
		} catch (IOException ioe) {
			logger.error("Could not load pdf from path : " + realPath, ioe);
		}


	}
}

