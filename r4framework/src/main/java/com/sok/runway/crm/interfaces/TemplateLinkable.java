package com.sok.runway.crm.interfaces;
 
/**
 * LinkableTemplate interface 
 * Modelled on LinkableDocument 
 * @author Michael Dekmetzian (H - 28D)
 * @date 2009-08-24
 */
import com.sok.runway.crm.LinkableTemplate;

import java.util.Collection;
import java.util.Map;

public interface TemplateLinkable extends Linkable {
   
   public Collection<String> getLinkableTemplateIDs();
   
   public Collection<String> getLinkableTemplateIDs(Map<String,String> filterParameters);
   
   public LinkableTemplate getLinkableTemplate();
   
   public boolean linkTemplate(String templateID);
}