package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.GenRow;

public class Basic extends PropertyEntity {

	private PropertyType propertyType = null;

	static final String VIEWSPEC = "ProductQuickIndexView";

	public Basic(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
	}
	public Basic(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}

	public Basic(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	
	@Override
	public Basic load(String productID) {
		super.load(productID);
		
		propertyType =  PropertyType.getPropertyType(this.getString(PropertyEntity.PRODUCTTYPE));
		
		return this;
	}
	
	@Override
	public String[] getDripProductIDs() {
		if (propertyType != null && propertyType == PropertyType.HouseLandPackage)
			return new String[]{getString(PRODUCTID), getString(PRODUCTID), getString(PRODUCTID)};

		if (propertyType != null && propertyType == PropertyType.HomePlan) {
			return new String[]{getString("RangeID"), getString("DesignID"), getString(PRODUCTID)};
		}

		if (propertyType != null && (propertyType == PropertyType.Apartment || propertyType == PropertyType.Land)) {
			return new String[]{getString("EstateID"), getString("StageID"), getString(PRODUCTID)};
		}		

		if (propertyType != null && propertyType == PropertyType.ExistingProperty) {
			if (StringUtils.isNotBlank(getString(PRODUCTID))) {
				GenRow quick = ExistingProperty.getProductQuick(getString(PRODUCTID));
				if (quick.isSet("PlanID") && quick.isSet("LotID")) {
					return new String[]{quick.getString("EstateID"), quick.getString("StageID"), quick.getString("LotID"), quick.getString("RangeID"), quick.getString("DesignID"), quick.getString("PlanID"), quick.getString("ProductID")};
				} else if (quick.isSet("PlanID")) {
					return new String[]{quick.getString("RangeID"), quick.getString("DesignID"), quick.getString("PlanID"), quick.getString("ProductID")};
				} else if (quick.isSet("LotID")) {
					return new String[]{quick.getString("EstateID"), quick.getString("StageID"), quick.getString("LotID"), quick.getString(PRODUCTID)};
				}
			}

			return new String[]{getString(PRODUCTID)};
		
		}

		throw new UnsupportedOperationException("Not defined for product type "  + this.getProductType());
	}

	public List<BuildingStage> getStages() {
		return Collections.emptyList();
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
}