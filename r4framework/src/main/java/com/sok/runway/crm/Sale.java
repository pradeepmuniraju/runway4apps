package com.sok.runway.crm;

import com.sok.runway.*;

import javax.servlet.ServletRequest;
import java.sql.*;

public class Sale extends AbstractOrder {

	public static final String ENTITY_NAME = "Sales";

	public Sale(Connection con) {
		super(con, "OrderView");
	}

	public Sale(ServletRequest request) {
		 super(request, "OrderView");
		 populateFromRequest(request.getParameterMap());
	}

	public Sale(Connection con, String orderID) {
		this(con);
		load(orderID);
	}

    public String getEntityName() {
	    return ENTITY_NAME;
    }

	public Validator getValidator() {
		Validator val = Validator.getValidator("Sale");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("OrderID","SYSTEM ERROR - OrderID");
			val.addMandatoryField("GroupID","Order must have a security group selected");
			Validator.addValidator("Sale", val);
		}
		return val;
	}
}