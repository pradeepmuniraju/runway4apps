package com.sok.runway.crm.cms;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.sok.runway.Validator;
import com.sok.runway.crm.activity.Logable;
import com.sok.runway.security.AuditEntity;

@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PageContent extends AuditEntity implements Logable {

	private static final String VIEW_NAME = "cms3/PageContentView";
	private static final String ENTITY_NAME = "PageContentItem";
	
	public PageContent() {
		this((Connection)null);
	}
	
	public PageContent(Connection con) {
		super(con, VIEW_NAME);
	}
	
	public PageContent(Connection con, String pageContentID) {
		super(con, VIEW_NAME);
		load(pageContentID);
	}
	
	public PageContent(HttpServletRequest request) {
		super(request, VIEW_NAME);
		populateFromRequest(request.getParameterMap());
	}
	
	@Override
	public String getEntityName() {
		return ENTITY_NAME;
    }	
	
	@Override
	public Validator getValidator() {
		Validator val = Validator.getValidator(ENTITY_NAME);

		if (val == null) {
			val = new Validator();
			val.addMandatoryField("PageContentID","SYSTEM ERROR - PageContentID");
			val.addMandatoryField("PageID","SYSTEM ERROR - PageID");
			Validator.addValidator(ENTITY_NAME, val);
		}

		return val;
	}
	
	@JsonProperty("PageContentID")
	public String getPageContentID() {
		return getField("PageContentID");
	}
	@JsonProperty("PageContentID")
	public void setPageContentID(String PageContentID) {
		setField("PageContentID", PageContentID);
	}
	@JsonProperty("PageID")
	public String getPageID() {
		return getField("PageID");
	}
	@JsonProperty("PageID")
	public void setPageID(String PageID) {
		setField("PageID", PageID);
	}
	@JsonProperty("ContentType")
	public String getContentType() {
		return getField("ContentType");
	}
	@JsonProperty("ContentType")
	public void setContentType(String ContentType) {
		setField("ContentType", ContentType);
	}
	@JsonProperty("ContentField")
	public String getContentField() {
		return getField("ContentField");
	}
	@JsonProperty("ContentField")
	public void setContentField(String ContentField) {
		setField("ContentField", ContentField);
	}
	@JsonProperty("Content")
	public String getContent() {
		return getField("Content");
	}
	@JsonProperty("Content")
	public void setContent(String Content) {
		setField("Content", Content);
	}
	@JsonProperty("ContentData")
	public String getContentData() {
		return getField("ContentData");
	}
	@JsonProperty("ContentData")
	public void setContentData(String ContentData) {
		setField("ContentData", ContentData);
	}
	@JsonProperty("MasterPageID")
	public String getMasterPageID() {
		return getField("MasterPageID");
	}
	@JsonProperty("MasterPageID")
	public void setMasterPageID(String MasterPageID) {
		setField("MasterPageID", MasterPageID);
	}

}
