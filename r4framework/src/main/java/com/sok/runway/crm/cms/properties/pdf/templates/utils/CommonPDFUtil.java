package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import javax.servlet.http.HttpServletRequest;


import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.SalesRepBean;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.pdf.templates.model.UserInfo;

public class CommonPDFUtil extends CommonUtil{
	
	private static final Logger logger = LoggerFactory.getLogger(CommonPDFUtil.class);
	
	public static String getBuilderName(PropertyEntity property) 
	{
		String builderName = "";
		String sysParam = InitServlet.getSystemParam("PDFDEV-BuilderName");
		try
		{
			if(StringUtils.isNotBlank(sysParam)) 
			{
				if(sysParam.equalsIgnoreCase("Builder")) 
					builderName = property.getString("BuilderName");
				if(sysParam.equalsIgnoreCase("Brand")) 
					builderName = property.getString("BrandName");
				else if(sysParam.equalsIgnoreCase("Range"))
					builderName = property.getString("RangeName");
				else if(sysParam.equalsIgnoreCase("RangeDescription"))
					builderName = property.getString("RangeDescription");
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace() ;
			logger.debug("Exception in retrieveing builder name based on system parameter DeveloperPDFBuilderName:" +ex);
			return builderName;
		}
		return builderName;
	}
	
	/**
	 * Get contactID 
	 * @param estateID
	 * @param request
	 * @return
	 */
	public static String getContactIDByEstateID(String estateID, HttpServletRequest request){
		String contactID = "";
		GenRow estateContact = new GenRow(); 
		estateContact.setRequest(request); 
		estateContact.setViewSpec("properties/EstateContactView"); 
		estateContact.setParameter("EstateID", estateID); 
		estateContact.setParameter("DefaultLink", "Y");
		estateContact.doAction(GenerationKeys.SELECTFIRST);			
		contactID = estateContact.getString("ContactID");
		return contactID;
	}
	
	public static GenRow getEstateLinkedContacts(String estateID, HttpServletRequest request){
		 GenRow contactRow = null;
		 if(StringUtils.isNotBlank(estateID))
		 {
			String contactId = getContactIDByEstateID(estateID, request);
			if(StringUtils.isNotBlank(contactId))
			{
				contactRow = getContactDetails(contactId, request);
			}
		 }
		return contactRow;
	}
	
	public static boolean useAsterisk() {
		boolean useAsterick = false;
		String sysParam = InitServlet.getSystemParam("PDFDEV-ShowAsteriskWithPrice");
		if(StringUtils.isNotBlank(sysParam) && "true".equals(sysParam))
			useAsterick = true;
		return useAsterick;
	}
	
	
	public static boolean showSalesRep() {
		boolean showRep = true;
		String sysParam = InitServlet.getSystemParam("PDFDEV-ShowSalesRep");
		if(StringUtils.isNotBlank(sysParam) && "false".equals(sysParam))
			showRep = false;
		return showRep;
	}
	
	public static String getMobileOrPhone() 
	{
		String checkFlag = "Mobile";
		String sysParam = InitServlet.getSystemParam("PDFDEV-MobileOrPhone");
		if(StringUtils.isNotBlank(sysParam)) 
		{
			if(sysParam.equalsIgnoreCase("phone")) 
				checkFlag = "Phone";
		}
		return checkFlag;
	}
	
	public static GenRow getContactDetails(String contactID, HttpServletRequest request)
	{
		GenRow contactRow = null;
		contactRow = new GenRow();
		contactRow.setRequest(request);
		contactRow.setViewSpec("ContactView");
		contactRow.setParameter("ContactID", contactID);
		contactRow.doAction(GenerationKeys.SELECTFIRST);
		contactRow.getResults();
		return contactRow;
	}
	
	public static SalesRepBean loadSalesRepDetails(HttpServletRequest request, PropertyEntity product){
		SalesRepBean salesRep = new SalesRepBean();
		GenRow repDetails = null;
		String repID = request.getParameter("RepUserID");
		
		UserInfo salesRepBean = getProductLinkedSalesRep(product.getString("ProductID"));
		salesRep.setRepUserID(salesRepBean.getUserID());
		salesRep.setRepName(salesRepBean.getFirstName() + " " + salesRepBean.getLastName());
		salesRep.setRepMobile(salesRepBean.getMobileNumber());
		salesRep.setRepPhone(salesRepBean.getPhoneNumber());
		salesRep.setRepEmail(salesRepBean.getEmail());
		
		String phoneValue = getMobileOrPhone();
		
		// if(StringUtils.isBlank(salesRep.getRepName()) && StringUtils.isNotBlank(repID)){
		if(StringUtils.isNotBlank(repID)){
			repDetails = PDFUtil.getRep(repID);
			salesRep.setRepUserID(repID);
			salesRep.setRepName(repDetails.getString("FirstName") + " " + repDetails.getString("LastName"));
			if(phoneValue.equalsIgnoreCase("Phone"))
				salesRep.setRepPhone(repDetails.getString("Phone"));
			else
				salesRep.setRepPhone(repDetails.getString("Mobile"));
			salesRep.setRepMobile(repDetails.getString("Mobile"));
			
			salesRep.setRepEmail(PDFUtil.getRepEmail(repDetails.getString("Email")));
		} else{
			repID = salesRep.getRepUserID();
		}
		
		if(StringUtils.isNotBlank(salesRep.getRepName())){
			if(StringUtils.isNotBlank(repID))
			{
				GenRow userRow = new GenRow();
				userRow.setRequest(request);
				userRow.setViewSpec("UserView");
				userRow.setParameter("UserID", repID);
				userRow.doAction(GenerationKeys.SELECTFIRST);
				userRow.getResults();
				
				if(userRow.isSuccessful())
				{
					String address = "";
					String tmpStr = "";
					if(userRow.isSuccessful()){
						tmpStr = userRow.getString("Street");
						if(StringUtils.isNotBlank(tmpStr)){
							address = tmpStr;
						}
						
						// As requested by Dwayne to stop showing such address without proper address
						if (StringUtils.isNotBlank(address)) {
							tmpStr = userRow.getString("City");
							if(StringUtils.isNotBlank(tmpStr) && StringUtils.isNotBlank(address)){
								address = address+", "+tmpStr   ;
							}
							
							tmpStr = userRow.getString("State");
							if(StringUtils.isNotBlank(tmpStr)){
								address = address+", "+tmpStr   ;
							}
							
							tmpStr = userRow.getString("Postcode");
							if(StringUtils.isNotBlank(tmpStr)){
								address = address+", "+tmpStr   ;
							}
						}
						salesRep.setAddress(address);
					}
				}
			}
		}
		
		return salesRep;
	}
	
	
	 public static SalesRepBean getEstateContactDetails(String estateID, HttpServletRequest request)
	 {
		 SalesRepBean salesRep = null;
		 GenRow contactRow = getEstateLinkedContacts(estateID, request);
		 if(contactRow == null)
		 {
			 String contactID = request.getParameter("ContactID");
			 if(StringUtils.isNotBlank(contactID))
				 contactRow = getContactDetails(contactID, request);
		 }
		 if(contactRow != null && contactRow.isSuccessful())
		 {
			String phoneValue = getMobileOrPhone();
			
			String name = contactRow.getString("FirstName") + " " + contactRow.getString("LastName");				
			String repMobile = contactRow.getString("Mobile");
			String repPhone = contactRow.getString("Phone");
			salesRep = new SalesRepBean();
			salesRep.setRepName(name);
			if(phoneValue.equalsIgnoreCase("Phone"))
				salesRep.setRepPhone(repPhone);
			else
				salesRep.setRepPhone(repMobile);
			
			salesRep.setRepMobile(repMobile);

			String address = "";
			String tmpStr = "";
			if(contactRow.isSuccessful()){
				tmpStr = contactRow.getString("Street");
				if(StringUtils.isNotBlank(tmpStr)){
					address = tmpStr;
				}
				
				tmpStr = contactRow.getString("City");
				if(StringUtils.isNotBlank(tmpStr) && StringUtils.isNotBlank(address)){
					address = address+", "+tmpStr   ;
				}
				
				tmpStr = contactRow.getString("State");
				if(StringUtils.isNotBlank(tmpStr)){
					address = address+", "+tmpStr   ;
				}
				
				tmpStr = contactRow.getString("Postcode");
				if(StringUtils.isNotBlank(tmpStr)){
					address = address+", "+tmpStr   ;
				}
				salesRep.setAddress(address);
			}
		 }
		 return salesRep;
	 }
	 
	 public static GenRow getLocationDetails(String locationId) 
	 {
		GenRow row = new GenRow();
		if(locationId != null && !locationId.equalsIgnoreCase("")) 
		{
			row.setViewSpec("DisplayEntityView");				
			row.setParameter("DisplayEntityID", locationId);
			row.doAction(GenerationKeys.SELECTFIRST);
		}
		return row;
	 }

	 public static String getFinalFacadeDisclaimer(String dripDisc, String facadeDesc) {
			String tmpFacadeDisclaimer = "";
			
			if (StringUtils.isNotBlank(dripDisc)) {
				tmpFacadeDisclaimer += dripDisc;
			}
			
			if (StringUtils.isNotBlank(facadeDesc)) {
				if (StringUtils.isNotBlank(tmpFacadeDisclaimer)) {
					tmpFacadeDisclaimer += " ";
				}
				tmpFacadeDisclaimer += facadeDesc;
			}
			
			return tmpFacadeDisclaimer;
	 }
}