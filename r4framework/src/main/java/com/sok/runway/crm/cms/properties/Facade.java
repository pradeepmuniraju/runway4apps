package com.sok.runway.crm.cms.properties;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.generation.GenerationKeys;

public class Facade extends PropertyEntity {
	
	private static final Logger logger = LoggerFactory.getLogger(Facade.class);

	final PropertyType propertyType = PropertyType.Facade;
	static final String VIEWSPEC = "ProductLinkedProductView";
	final static String LinkedProductID = "LinkedProductID";
	final static String ProductID = "ProductID";
	private Facade facade = null;

	public Facade(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(LINKED_PRODUCTTYPE, getProductType());
	}

	public Facade(HttpServletRequest request, String productID) {
		setViewSpec(VIEWSPEC);
		setRequest(request);
		setParameter(LINKED_PRODUCTTYPE, getProductType());
		load(productID);
	}

	public Facade(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		setParameter(LINKED_PRODUCTTYPE, getProductType());
		load(productID);
	}

	@Override
	public Facade load(String productID) {
		setParameter(ProductID, productID);
		//setParameter(PropertyFactory.PRODUCTID, productID);
		if (isSet(ProductID)) {
			doAction(getDBAction());
			if (isSuccessful() && !getProductType().equals(getData(PRODUCTTYPE))) {
				success = false;
				logger.debug("ProductID supplied {} is not a {}", productID, getProductType());

			} else if(!isSuccessful()) {
				// if we can't find it view the Product, lets try direct
				remove("ProductID");
				setParameter(LinkedProductID, productID);
				if (isSet(LinkedProductID)) {
					doAction(getDBAction());
					if (isSuccessful() && !getProductType().equals(getData(PRODUCTTYPE))) {
						success = false;
						logger.debug("ProductID supplied {} is not a {}", productID, getProductType());

					} else if(!isSuccessful()) {
						logger.debug("ProductID supplied was not found [%1$s]", productID);
						//doAction(GenerationKeys.SEARCH);
						logger.debug(getStatement());
					}
				}
			}
		}
		return this;
	}

	@Override
	public void clear() {
		facade = null;
		super.clear();
	}

	@Override
	public Object get(String key) {
		if ("facade".equals(key))
			return getFacade();
		return super.get(key);
	}

	public Facade getFacade() {
		if (facade == null && isSuccessful()) {
			facade = new Facade(getConnection()).load(getData("ProductID"));
			if (!facade.isSuccessful()) {
				facade = null;
			}
		}
		return facade;
	}

	public String getFacadeName() {
		return getString("Name");
	}

	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}

	protected String getDBAction() {
		return GenerationKeys.SELECTFIRST;
	}
}
