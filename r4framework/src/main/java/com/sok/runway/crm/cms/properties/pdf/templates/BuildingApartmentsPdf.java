package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.ApartmentInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ApartmentHeaderFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;

public class BuildingApartmentsPdf extends AbstractPDFTemplate implements PDFConstants {
	
	public static final BaseColor blackBGColor = new BaseColor(0, 0, 0); // 000000
	public static final BaseColor whiteBGColor = new BaseColor(255, 255, 255); // FFFFFF
	public static final BaseColor greyBGColor = new BaseColor(51, 51, 51); // #333333
	public static final BaseColor lightGreyBGColor = new BaseColor(205, 206, 208); // #CDCED0
	

	private static BaseFont defaultFont = null;
	private static BaseFont arialNormal = defaultFont, arialBold = defaultFont, arialItalic = defaultFont, arialBoldItalic = defaultFont;
	static {
		try {
			String arialPath = InitServlet.getRealPath() + "/specific/pdf/fonts/Arial/";

			arialNormal = BaseFont.createFont(arialPath + "arial.ttf", BaseFont.CP1252, BaseFont.EMBEDDED); 
			arialBold = BaseFont.createFont(arialPath + "arialbd.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
			
			arialBoldItalic = BaseFont.createFont(arialPath + "arialbi.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
			arialItalic = BaseFont.createFont(arialPath + "ariali.ttf", BaseFont.CP1252, BaseFont.EMBEDDED);
			
			System.out.println("Fonts registered successfully");
		} catch (Exception e) {
			System.out.println("Ignoring Error while registering fonts. Error: " + e);
			e.printStackTrace();
		}
	}
	
	public static final Font arialNormalPt10Black = new Font(arialNormal, fontRatio * 10, Font.NORMAL, blackBGColor);
	public static final Font arialNormalPt12Black = new Font(arialNormal, fontRatio * 12, Font.NORMAL, blackBGColor);
	public static final Font arialNormalPt14Black = new Font(arialNormal, fontRatio * 14, Font.NORMAL, blackBGColor);
	public static final Font arialNormalPt16Black = new Font(arialNormal, fontRatio * 16, Font.NORMAL, blackBGColor);
	public static final Font arialNormalPt17Black = new Font(arialNormal, fontRatio * 17.5f, Font.NORMAL, blackBGColor);
	public static final Font arialNormalPt18Black = new Font(arialNormal, fontRatio * 18, Font.NORMAL, blackBGColor);
	public static final Font arialNormalPt20Black = new Font(arialNormal, fontRatio * 20, Font.NORMAL, blackBGColor);
	
	public static final Font arialBoldPt12Black = new Font(arialNormal, fontRatio * 12, Font.BOLD, blackBGColor);
	public static final Font arialBoldPt14Black = new Font(arialNormal, fontRatio * 14, Font.BOLD, blackBGColor);
	public static final Font arialBoldPt15Black = new Font(arialNormal, fontRatio * 15, Font.BOLD, blackBGColor);
	public static final Font arialBoldPt16Black = new Font(arialNormal, fontRatio * 16, Font.BOLD, blackBGColor);
	public static final Font arialBoldPt17Black = new Font(arialNormal, fontRatio * 17, Font.BOLD, blackBGColor);
	public static final Font arialBoldPt18Black = new Font(arialNormal, fontRatio * 18, Font.BOLD, blackBGColor);
	public static final Font arialBoldPt20Black = new Font(arialNormal, fontRatio * 20, Font.BOLD, blackBGColor);
	public static final Font arialBoldPt25Black = new Font(arialNormal, fontRatio * 25, Font.BOLD, blackBGColor);
	public static final Font arialBoldPt30Black = new Font(arialNormal, fontRatio * 30, Font.BOLD, blackBGColor);
	
	public static final Font arialNormalPt14White = new Font(arialNormal, fontRatio * 14, Font.NORMAL, whiteBGColor);
	public static final Font arialNormalPt20White = new Font(arialNormal, fontRatio * 20, Font.NORMAL, whiteBGColor);
	public static final Font arialNormalPt14Grey = new Font(arialNormal, fontRatio * 14, Font.NORMAL, greyBGColor);
	public static final Font arialNormalPt15Grey = new Font(arialNormal, fontRatio * 15, Font.NORMAL, greyBGColor);
	public static final Font arialNormalPt16Grey = new Font(arialNormal, fontRatio * 16, Font.NORMAL, greyBGColor);
	public static final Font arialNormalPt17Grey = new Font(arialNormal, fontRatio * 17.5f, Font.NORMAL, greyBGColor);
	public static final Font arialNormalPt18Grey = new Font(arialNormal, fontRatio * 18, Font.NORMAL, greyBGColor);
		
	
	
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {

		ArrayList<ApartmentInfo> apartmentsList = getBuildingsApartmentsList(request, PDFUtil.PDF_DETAILS_BY_BUILDING);

		renderPages(document, apartmentsList);
	}

	@Override
	public void setMargins(Document document) 
	{
		//document.setMargins(15, 30, 10, 0); // L R T B
		document.setMargins(15, 15, 15, 0); // L R T B
	}

	private void renderPages(com.itextpdf.text.Document document, ArrayList<ApartmentInfo> apartmentInfoList) throws DocumentException 
	{
		renderApartmentsPage(document, apartmentInfoList);
	}

	private void renderApartmentsPage(com.itextpdf.text.Document document, ArrayList<ApartmentInfo> apartmentInfoList) throws DocumentException
	{
		PdfPTable pageTable = new PdfPTable(1);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setBorderWidth(0f);
		pageTable.getDefaultCell().setBorderWidthTop(0f);
		pageTable.getDefaultCell().setBorderWidthBottom(0f);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.setWidthPercentage(100f);

		if (apartmentInfoList!=null && !apartmentInfoList.isEmpty())
		{
				// Adding Header
				ApartmentInfo firstApartmentInfo = apartmentInfoList.get(0);
				pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				pageTable.addCell(getHeaderTable(firstApartmentInfo, getPdfSimpleName())); 

				int pageBreaker = 0;
				for (int j = 0; j < apartmentInfoList.size(); j++) 
				{
						if (j != 0 && ( (pageBreaker%6==0) || (j==3))) 
						{
							document.add(pageTable);
							document.newPage();
							
							pageTable = new PdfPTable(1);
							pageTable.getDefaultCell().setBorder(0);
							pageTable.getDefaultCell().setPadding(0);
							pageTable.getDefaultCell().setFixedHeight(0f);
							pageTable.setWidthPercentage(100f);
							
							// Adding Header
							pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
							pageTable.addCell(getHeaderTable(firstApartmentInfo, getPdfSimpleName()));
							
							pageBreaker = 0;
						}
						
						if (j == 0)
						{
							PdfPTable tblTopRowForBodyTableFirstPage = getTopRowForBodyTableFirstPage(firstApartmentInfo);
							pageTable.addCell(tblTopRowForBodyTableFirstPage);
						}	
						pageTable.getDefaultCell().setPadding(0);
						pageTable.getDefaultCell().setPaddingTop(8);
						pageTable.getDefaultCell().setPaddingBottom(8);
						pageTable.getDefaultCell().setBorder(0);
						pageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
						
						ApartmentInfo apartmentInfo = apartmentInfoList.get(j);
						if (apartmentInfo!=null)
						{	
							PdfPTable bodyTable = getBodyTableRow(apartmentInfo, getPdfSimpleName());
							pageTable.addCell(bodyTable);
						}	
						
						pageBreaker++;
				}
		}		
		document.add(pageTable);
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		return new ApartmentHeaderFooter(request, PDFUtil.PDF_DETAILS_BY_BUILDING);
	}
	
	public String getPdfSimpleName() {
		return PDFUtil.PDF_DETAILS_BY_BUILDING;
	}
	
	public String getDripCount() {
		return "0";
	}
	
	

	
	public static PdfPTable getHeaderTable(ApartmentInfo apartmentInfo, String pdfType) throws DocumentException 
	{
			PdfPTable hdrTable = new PdfPTable(1);
			hdrTable.setWidths(new float[] { 100f});
			hdrTable.getDefaultCell().setPadding(0);
			hdrTable.getDefaultCell().setPaddingBottom(5);
			hdrTable.getDefaultCell().setPaddingTop(5);
			hdrTable.getDefaultCell().setPaddingLeft(0);
			hdrTable.getDefaultCell().setBorder(0);
			hdrTable.getDefaultCell().setBorderWidth(0f);
			hdrTable.getDefaultCell().setBorderWidthTop(0f);
			hdrTable.getDefaultCell().setBorderWidthBottom(0f);
			hdrTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			hdrTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			hdrTable.setWidthPercentage(100f);
			
			// Header Row 01
			PdfPCell headerCell = new PdfPCell();

			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			String imgStr = homePath + "/" + PDFUtil.MAIN_LOGO_IMG;
			/*try
			{
					java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
					Image imgHero = Image.getInstance(imgUri.toString());
				    imgHero.scaleToFit(550, 50);
				    headerCell = new PdfPCell(imgHero);
			}
			catch(Exception ee)
			{
					headerCell = new PdfPCell(new Paragraph("Problem with image" + imgStr, arialNormalPt17Black));
					System.out.println("Exception Occurred. Message is: " + ee.getMessage());
					ee.printStackTrace();
			}

			headerCell.setBorder(0);
			headerCell.setBorderWidth(0f);
			hdrTable.addCell(headerCell);*/
			
			// Header Row 01
			headerCell.setBackgroundColor(PDFUtil.pkgHeaderGrey);
			headerCell.setFixedHeight(30f);
			headerCell.setBorder(0);
			headerCell.setBorderWidth(0f);
			hdrTable.addCell(headerCell);			
			
			 
			// Header Row 02
			PdfPTable taglineTbl = new PdfPTable(2);
			taglineTbl.setWidths(new float[] { 70f, 30f});
			taglineTbl.getDefaultCell().setPadding(0);
			taglineTbl.getDefaultCell().setPaddingBottom(3);
			taglineTbl.getDefaultCell().setPaddingTop(3);
			taglineTbl.getDefaultCell().setPaddingLeft(0);
			taglineTbl.getDefaultCell().setBorder(0);
			taglineTbl.getDefaultCell().setBorderWidth(0f);
			taglineTbl.getDefaultCell().setBorderWidthTop(0f);
			taglineTbl.getDefaultCell().setBorderWidthBottom(0f);
			taglineTbl.getDefaultCell().setBorderWidthRight(0f);
			taglineTbl.getDefaultCell().setBorderWidthLeft(0f);
			taglineTbl.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			taglineTbl.setWidthPercentage(100f);
			
			String buildingName = "";
			if (apartmentInfo != null)
			{
				buildingName =	apartmentInfo.getBuildingName();
				
				if (PDFUtil.PDF_DETAILS_SINGLE_APARTMENT.equalsIgnoreCase(pdfType))
				{	
					buildingName +=	" - Lot " + apartmentInfo.getApartmentName();
				}	
			}
	
			taglineTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			taglineTbl.addCell(new Paragraph(buildingName, arialBoldPt30Black));
			taglineTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			
			headerCell = new PdfPCell();
			imgStr = homePath + "/" + PDFUtil.MAIN_LOGO_IMG;
			try
			{
					java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
					Image imgHero = Image.getInstance(imgUri.toString());
				    imgHero.scaleToFit(400, 50);
				    headerCell = new PdfPCell(imgHero);
			}
			catch(Exception ee)
			{
					headerCell = new PdfPCell(new Paragraph("Problem with image" + imgStr, arialNormalPt17Black));
					System.out.println("Exception Occurred. Message is: " + ee.getMessage());
					ee.printStackTrace();
			}
			headerCell.setBorder(0);
			headerCell.setBorderWidth(0f);
			headerCell.setBorderWidthRight(0f);
			headerCell.setBorderWidthLeft(0f);
			headerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			taglineTbl.addCell(headerCell);

			hdrTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			hdrTable.addCell(taglineTbl);
			return hdrTable;
	}	
	
	public static PdfPTable getBodyTableRow(ApartmentInfo apartmentInfo, String pdfType) throws DocumentException 
	{
			PdfPTable bodyTable = new PdfPTable(2);
			bodyTable.setWidths(new float[] { 25f, 75f});
			bodyTable.getDefaultCell().setPadding(0);
			bodyTable.getDefaultCell().setPaddingBottom(1);
			bodyTable.getDefaultCell().setPaddingTop(1);
			bodyTable.getDefaultCell().setPaddingLeft(0);
			bodyTable.getDefaultCell().setBorder(0);
			bodyTable.getDefaultCell().setBorderWidth(0f);
			bodyTable.getDefaultCell().setBorderWidthTop(0f);
			bodyTable.getDefaultCell().setBorderWidthBottom(0f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			bodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			bodyTable.setWidthPercentage(100f);
			
			
			// Body Row 01
			PdfPTable bodyHeadingRowTbl = new PdfPTable(2);
			bodyHeadingRowTbl.setWidths(new float[] { 50f, 50f});
			bodyHeadingRowTbl.getDefaultCell().setBorder(0);
			bodyHeadingRowTbl.getDefaultCell().setBorderWidth(0f);
			bodyHeadingRowTbl.getDefaultCell().setBorderWidthTop(0f);
			bodyHeadingRowTbl.getDefaultCell().setBorderWidthBottom(0f);
			bodyHeadingRowTbl.getDefaultCell().setColspan(1);
			bodyHeadingRowTbl.getDefaultCell().setPaddingLeft(10f);
			bodyHeadingRowTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			bodyHeadingRowTbl.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			bodyHeadingRowTbl.getDefaultCell().setBackgroundColor(greyBGColor);
			
			if (PDFUtil.PDF_DETAILS_BY_BUILDING.equalsIgnoreCase(pdfType))
			{
				bodyHeadingRowTbl.addCell(new Paragraph("Apartment" + "  " + apartmentInfo.getApartmentName(), arialNormalPt20White));	
			}
			else
			{
				bodyHeadingRowTbl.addCell(new Paragraph(apartmentInfo.getBuildingName() + " - Apartment " + apartmentInfo.getApartmentName(), BuildingApartmentsPdf.arialNormalPt20White));	
			}	
			
			bodyHeadingRowTbl.getDefaultCell().setColspan(1);
			bodyHeadingRowTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			bodyHeadingRowTbl.getDefaultCell().setPaddingRight(10f);
			bodyHeadingRowTbl.addCell(new Paragraph(apartmentInfo.getPrice(), arialNormalPt20White));
			bodyHeadingRowTbl.completeRow();
			bodyHeadingRowTbl.getDefaultCell().setColspan(1);
			bodyHeadingRowTbl.getDefaultCell().setPaddingLeft(0f);
			bodyHeadingRowTbl.getDefaultCell().setPaddingRight(0f);
			bodyHeadingRowTbl.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			
			bodyTable.getDefaultCell().setColspan(2);
			bodyTable.getDefaultCell().setBackgroundColor(greyBGColor);
			bodyTable.addCell(bodyHeadingRowTbl);
			bodyTable.getDefaultCell().setBackgroundColor(lightGreyBGColor);
			bodyTable.getDefaultCell().setColspan(1);
			
			
			PdfPCell cell = new PdfPCell();
			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			String hImage = apartmentInfo.getHeroImage();
			
			/*if (PDFUtil.PDF_DETAILS_BY_BUILDING.equalsIgnoreCase(pdfType))
			{
				ArrayList<String> fpImgList =  apartmentInfo.getApartmentFloorPlansImagesList();
				if (fpImgList!=null && !fpImgList.isEmpty())
				{	
					hImage = fpImgList.get(0);
				}	
			}*/
			
			String imgStr = homePath + "/" + hImage;
			try
			{
					java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
					
					if (hImage!=null && !"".equalsIgnoreCase(hImage))
					{	
						Image imgHero = Image.getInstance(imgUri.toString());
						if (PDFUtil.PDF_DETAILS_BY_BUILDING.equalsIgnoreCase(pdfType))
						{	
								imgHero.scaleToFit(140, 76.3f);  // 1000 x 1000
						}
						else
						{
								imgHero.scaleToFit(132, 76.3f);  // 1000 x 667
						}	
						cell = new PdfPCell(imgHero);
					}
					else
					{
						//cell = new PdfPCell(new Paragraph("Image Doesn't exist", arialNormalPt17Black));
					}
			}
			catch(Exception ee)
			{
					cell = new PdfPCell(new Paragraph("Problem with image" + imgStr, arialNormalPt17Black));
					System.out.println("Exception Occurred. Message is: " + ee.getMessage());
					ee.printStackTrace();
			}
			cell.setBackgroundColor(lightGreyBGColor);
			cell.setBorder(0);
			cell.setBorderWidth(0f);
			bodyTable.addCell(cell);
			 


			PdfPTable rightBodyTable = new PdfPTable(3);
			rightBodyTable.setWidths(new float[] { 34f, 30f, 36f});
			rightBodyTable.getDefaultCell().setPadding(0);
			rightBodyTable.getDefaultCell().setPaddingBottom(2);
			rightBodyTable.getDefaultCell().setPaddingTop(2);
			rightBodyTable.getDefaultCell().setPaddingLeft(4);
			rightBodyTable.getDefaultCell().setBorder(0);
			rightBodyTable.getDefaultCell().setBorderWidth(0f);
			rightBodyTable.getDefaultCell().setBorderWidthTop(0f);
			rightBodyTable.getDefaultCell().setBorderWidthBottom(0f);
			rightBodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_LEFT);
			rightBodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			rightBodyTable.getDefaultCell().setBackgroundColor(lightGreyBGColor);
			rightBodyTable.getDefaultCell().setColspan(1);

			PdfPTable rightTable1 = new PdfPTable(2);
			rightTable1.setWidths(new float[] {45, 55f});
			rightTable1.getDefaultCell().setPadding(0);
			rightTable1.getDefaultCell().setPaddingBottom(3);
			rightTable1.getDefaultCell().setPaddingTop(2);
			rightTable1.getDefaultCell().setPaddingLeft(2);
			rightTable1.getDefaultCell().setBorder(0);
			rightTable1.getDefaultCell().setBorderWidth(0f);
			rightTable1.getDefaultCell().setBorderWidthTop(0f);
			rightTable1.getDefaultCell().setBorderWidthBottom(0f);
			rightTable1.getDefaultCell().setVerticalAlignment(Element.ALIGN_LEFT);
			rightTable1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			rightTable1.getDefaultCell().setBackgroundColor(lightGreyBGColor);
			rightTable1.getDefaultCell().setColspan(1);

			rightTable1.addCell(new Paragraph("Apartment #:", arialBoldPt15Black));
			rightTable1.addCell(new Paragraph(apartmentInfo.getLotNumber(), arialNormalPt15Grey));

			rightTable1.addCell(new Paragraph("Level:", arialBoldPt15Black));
			rightTable1.addCell(new Paragraph(apartmentInfo.getLevel(), arialNormalPt15Grey));

			
			String finalTypeStr = "";
			String typeDesc = apartmentInfo.getType();
			StringTokenizer st = new StringTokenizer(typeDesc);
			int worldCounter = 0;
			while (st.hasMoreElements()) 
			{
				worldCounter ++;
				finalTypeStr += st.nextElement() + " ";
				
				if (worldCounter > 9)
				{	
					finalTypeStr = finalTypeStr.trim() + "...";
					break;
				}	
			}
			rightTable1.addCell(new Paragraph("Type:", arialBoldPt15Black));
			rightTable1.addCell(new Paragraph(finalTypeStr, arialNormalPt15Grey));
			
			rightBodyTable.addCell(rightTable1);


			PdfPTable rightTable2 = new PdfPTable(2);
			rightTable2.setWidths(new float[] {70, 30f});
			rightTable2.getDefaultCell().setPadding(0);
			rightTable2.getDefaultCell().setPaddingBottom(2);
			rightTable2.getDefaultCell().setPaddingTop(2);
			rightTable2.getDefaultCell().setPaddingLeft(2);
			rightTable2.getDefaultCell().setBorder(0);
			rightTable2.getDefaultCell().setBorderWidth(0f);
			rightTable2.getDefaultCell().setBorderWidthTop(0f);
			rightTable2.getDefaultCell().setBorderWidthBottom(0f);
			rightTable2.getDefaultCell().setVerticalAlignment(Element.ALIGN_LEFT);
			rightTable2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			rightTable2.getDefaultCell().setBackgroundColor(lightGreyBGColor);
			rightTable2.getDefaultCell().setColspan(1);

			Paragraph p = new Paragraph();
			p.add(new Chunk("Internal Area (m", arialBoldPt15Black));
			Chunk c = new Chunk("2", arialBoldPt12Black);
			c.setTextRise(4.0f);
			p.add(c);
			p.add(new Chunk("):", arialBoldPt15Black));
			rightTable2.addCell(p);
			rightTable2.addCell(new Paragraph("" + apartmentInfo.getInternalArea(), arialNormalPt15Grey));

			p = new Paragraph();
			p.add(new Chunk("Balcony Area (m", arialBoldPt15Black));
			c = new Chunk("2", arialBoldPt12Black);
			c.setTextRise(4.0f);
			p.add(c);
			p.add(new Chunk("):", arialBoldPt15Black));
			rightTable2.addCell(p);
			rightTable2.addCell(new Paragraph("" + apartmentInfo.getBalconyArea(), arialNormalPt15Grey));
			
			p = new Paragraph();
			p.add(new Chunk("Total Area (m", arialBoldPt15Black));
			c = new Chunk("2", arialBoldPt12Black);
			c.setTextRise(4.0f);
			p.add(c);
			p.add(new Chunk("):", arialBoldPt15Black));
			rightTable2.addCell(p);
			rightTable2.addCell(new Paragraph("" + apartmentInfo.getTotalArea(), arialNormalPt15Grey));

			rightTable2.addCell(new Paragraph("Car Spaces:", arialBoldPt15Black));
			rightTable2.addCell(new Paragraph("" + apartmentInfo.getCarSpaces(), arialNormalPt15Grey));

			rightBodyTable.addCell(rightTable2);


			PdfPTable rightTable3 = new PdfPTable(2);
			rightTable3.setWidths(new float[] {40, 60});
			rightTable3.getDefaultCell().setPadding(0);
			rightTable3.getDefaultCell().setPaddingBottom(2);
			rightTable3.getDefaultCell().setPaddingTop(2);
			rightTable3.getDefaultCell().setPaddingLeft(2);
			rightTable3.getDefaultCell().setBorder(0);
			rightTable3.getDefaultCell().setBorderWidth(0f);
			rightTable3.getDefaultCell().setBorderWidthTop(0f);
			rightTable3.getDefaultCell().setBorderWidthBottom(0f);
			rightTable3.getDefaultCell().setVerticalAlignment(Element.ALIGN_LEFT);
			rightTable3.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			rightTable3.getDefaultCell().setBackgroundColor(lightGreyBGColor);
			rightTable3.getDefaultCell().setColspan(1);
			
			rightTable3.addCell(new Paragraph("Rent:", arialBoldPt15Black));
			rightTable3.addCell(new Paragraph("" + apartmentInfo.getRent(), arialNormalPt15Grey));

			rightTable3.addCell(new Paragraph("Sales Yield:", arialBoldPt15Black));
			rightTable3.addCell(new Paragraph("" + apartmentInfo.getSalesYield(), arialNormalPt15Grey));

			rightTable3.addCell(new Paragraph("Stage:", arialBoldPt15Black));
			rightTable3.addCell(new Paragraph("" + apartmentInfo.getStage(), arialNormalPt15Grey));

			rightTable3.addCell(new Paragraph("Sales Status:", arialBoldPt15Black));
			rightTable3.addCell(new Paragraph("" + apartmentInfo.getSalesStatus(), arialNormalPt15Grey));

			rightBodyTable.addCell(rightTable3);

			
			bodyTable.addCell(rightBodyTable);
			return bodyTable;
	}
	
	
	public PdfPTable getSecondRowTable(ApartmentInfo apartmentInfo) throws DocumentException 
	{
			PdfPTable bodyTable = new PdfPTable(1);
			bodyTable.setWidths(new float[] { 100f});
			bodyTable.getDefaultCell().setPadding(0);
			bodyTable.getDefaultCell().setPaddingBottom(2);
			bodyTable.getDefaultCell().setPaddingTop(2);
			bodyTable.getDefaultCell().setPaddingLeft(0);
			bodyTable.getDefaultCell().setBorder(0);
			bodyTable.getDefaultCell().setBorderWidth(0f);
			bodyTable.getDefaultCell().setBorderWidthTop(0f);
			bodyTable.getDefaultCell().setBorderWidthBottom(0f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			bodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			bodyTable.setWidthPercentage(100f);
			//bodyTable.getDefaultCell().setMinimumHeight(600f);

			Paragraph pHeading =  new Paragraph(apartmentInfo.getApartmentDetailsText1(), BuildingApartmentsPdf.arialBoldPt20Black);
			Paragraph pDescription =  new Paragraph(apartmentInfo.getApartmentDetailsText2(), BuildingApartmentsPdf.arialNormalPt14Black);

			bodyTable.addCell(pHeading);
			bodyTable.addCell(pDescription);
			
			return bodyTable;
	}
	
	public PdfPTable getTopRowForBodyTableFirstPage(ApartmentInfo apartmentInfo) throws DocumentException 
	{
			PdfPTable bodyTable = new PdfPTable(2);
			bodyTable.setWidths(new float[] { 50f, 50f});
			bodyTable.getDefaultCell().setPadding(0);
			bodyTable.getDefaultCell().setPaddingBottom(3);
			bodyTable.getDefaultCell().setPaddingTop(3);
			bodyTable.getDefaultCell().setPaddingLeft(0);
			bodyTable.getDefaultCell().setBorder(0);
			bodyTable.getDefaultCell().setBorderWidth(0f);
			bodyTable.getDefaultCell().setBorderWidthTop(0f);
			bodyTable.getDefaultCell().setBorderWidthBottom(0f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			bodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			bodyTable.setWidthPercentage(100f);

			bodyTable.getDefaultCell().setColspan(1);
			bodyTable.getDefaultCell().setMinimumHeight(600f);
			
			String leftImg = "";
			String rightImg = "";
			if (apartmentInfo != null && apartmentInfo.getBuildingImagesList() != null && !apartmentInfo.getBuildingImagesList().isEmpty())
			{
					ArrayList<String> tmpImgList = apartmentInfo.getBuildingImagesList();
					
					if (tmpImgList.size() >=1)
					{
						leftImg = tmpImgList.get(0);
					}	
					
					if (tmpImgList.size() >=2)
					{	
						rightImg = tmpImgList.get(1);
					}			
				
			}	
			
			// Left Cell
			PdfPCell cellLeft = new PdfPCell();
			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			String imgStr = homePath + "/" + leftImg;
			try
			{
					java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
					
					if (leftImg!=null && !"".equalsIgnoreCase(leftImg))
					{	
						Image imgHero = Image.getInstance(imgUri.toString());
						//imgHero.scaleToFit(304, 109.2f); // Perfect for 750 x 299
						imgHero.scaleToFit(274f, 259.2f); // Perfect for 1000 x 594
						cellLeft = new PdfPCell(imgHero);
					}
					else
					{
						cellLeft = new PdfPCell(new Paragraph("Image Doesn't exist", arialNormalPt17Black));
					}
			}
			catch(Exception ee)
			{
				cellLeft = new PdfPCell(new Paragraph("Problem with image" + imgStr, arialNormalPt17Black));
					System.out.println("Exception Occurred. Message is: " + ee.getMessage());
					ee.printStackTrace();
			}
			cellLeft.setBorder(0);
			cellLeft.setBorderWidth(0f);
			bodyTable.addCell(cellLeft);
			 
			// Right Cell
			PdfPCell cellRight = new PdfPCell();
			imgStr = homePath + "/" + rightImg;
			try
			{
					java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
					
					if (rightImg!=null && !"".equalsIgnoreCase(rightImg))
					{	
						Image imgHero = Image.getInstance(imgUri.toString());
						//imgHero.scaleAbsoluteHeight(250);
						//imgHero.scaleToFit(304, 109.2f); // Perfect for 750 x 299
						imgHero.scaleToFit(274f, 260.2f); // Perfect for 1000 x 594
						cellRight = new PdfPCell(imgHero);
					}
					else
					{
						cellRight = new PdfPCell(new Paragraph("Image Doesn't exist", arialNormalPt17Black));
					}
			}
			catch(Exception ee)
			{
					cellRight = new PdfPCell(new Paragraph("Problem with image" + imgStr, arialNormalPt17Black));
					System.out.println("Exception Occurred. Message is: " + ee.getMessage());
					ee.printStackTrace();
			}
			cellRight.setBorder(0);
			cellRight.setBorderWidth(0f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			bodyTable.addCell(cellRight);

			bodyTable.getDefaultCell().setMinimumHeight(0f);
			bodyTable.getDefaultCell().setColspan(2);
			PdfPTable descriptionTbl = getSecondRowTable(apartmentInfo);
			bodyTable.addCell(descriptionTbl);

			return bodyTable;
	}
	

	public static ArrayList<ApartmentInfo> getBuildingsApartmentsList(HttpServletRequest request, String pdfType)
	{
			String productIDsStr = "";
			int indx = 0;
			java.util.List<PropertyEntity> properties = PropertyFactory.getPropertyEntities(request);
			for (PropertyEntity pe : properties) 
			{
				if (indx > 0)
				{
					productIDsStr += "+";
				}
				productIDsStr += pe.getData("ProductID");
				
				indx++;
			}
			
			String isWebsiteRequest = request.getParameter("isWebsiteRequest");
			if ("yes".equalsIgnoreCase(isWebsiteRequest))
				productIDsStr = request.getParameter("ProductID");
			System.out.println("productIDsStr: " + productIDsStr  + "  , isWebsiteRequest: " + isWebsiteRequest);
			
			/*GenRow search = (GenRow) request.getSession().getAttribute(searchKey);
			search.setRequest(request);
			search.setParameter("-select1", "Products.ProductID as altID");
			search.setParameter("-groupby1", "Products.ProductID");
			search.doAction(GenerationKeys.SEARCH);
			search.getResults();
			
			if (search.getNext()) 
			{
				list = new ArrayList<PropertyEntity>();
				do 
				{
					list.add(PropertyFactory.getPropertyEntity(request, search.getData("ProductID")));
				}	
			}	*/
		
			ArrayList<ApartmentInfo> apartmentList = new ArrayList<ApartmentInfo> ();
			
			GenRow aprtmtQuery = new GenRow(); 
			aprtmtQuery.setRequest(request);
			aprtmtQuery.setViewSpec("properties/ProductApartmentView");
			aprtmtQuery.setParameter("-select1", "Products.ProductID as AptProductID");
			// 23-July-2012 NI: As it can be building as well aprtmtQuery.setParameter("ProductType", "Apartment");
			aprtmtQuery.sortBy("BuildingName",0);
			aprtmtQuery.sortBy("Name",1);
			aprtmtQuery.setAction(GenerationKeys.SEARCH);

			GenRow imagesList = new GenRow();
			String productIDs =  productIDsStr;  // request.getParameter("ProductID");
			
			if (productIDs!=null && !"".equalsIgnoreCase(productIDs))
			{
					/*if (PDFUtil.PDF_DETAILS_BY_BUILDING.equalsIgnoreCase(pdfType) || PDFUtil.PDF_DETAILS_VARIOUS_BUILDINGS.equalsIgnoreCase(pdfType))
					{	
						// 19-July-2012 NI: Commented as we only have apartmentIDs.    //aprtmtQuery.setParameter("ProductID", productIDs);							

						String strBuildingIDs = getBuildingIDsOfApartments(productIDs, request);
						aprtmtQuery.setParameter("ProductParentBuildingStageApartment.ProductVariationBuildingStageApartment.ProductParentBuildingBuildingStage.ProductVariationBuildingBuildingStage.ProductID", strBuildingIDs);
					}
					else if (PDFUtil.PDF_DETAILS_SINGLE_APARTMENT.equalsIgnoreCase(pdfType))
					{
							aprtmtQuery.setParameter("ProductID", productIDs);
					}*/
				
					aprtmtQuery.setParameter("ProductID", productIDs);
					
					aprtmtQuery.doAction();
					aprtmtQuery.getResults();
			}
			
			System.out.println("pdfType is: " + pdfType + "   ,  Database Query is: " + aprtmtQuery.getStatement());

			if(!aprtmtQuery.isSuccessful()) 
			{ 
				throw new RuntimeException("Apartment records are not found");
			}


			ArrayList<String> imgList = new ArrayList<String>();
			String imgPath = "";
			/*while (imagesList.getNext())  
			{
					imgPath = imagesList.getString("FilePath");
					if (imgPath!=null && !"".equalsIgnoreCase(imgPath))
					{	
						imgList.add(imgPath);
					}	
			}
			imagesList.close();*/
			
			Map<String, Object> dimensionsMap = new HashMap<String, Object>();
			while (aprtmtQuery.getNext() ) 
			{
					// Getting the Dimenstions
					dimensionsMap = getDimensions(aprtmtQuery.getString("AptProductID"));
					
					String levels = "0";
					if (dimensionsMap.get("Levels.NumberOf") != null)
							levels = dimensionsMap.get("Levels.NumberOf").toString();
					int intLevels = new Double(levels).intValue();
					
					String apartmentArea = "0";
					if (dimensionsMap.get("FloorArea.SQmeters") != null)
						apartmentArea = dimensionsMap.get("FloorArea.SQmeters").toString();
					double doubleApartmentArea = Double.parseDouble(apartmentArea);
							
					String balconyArea = "0";
					if (dimensionsMap.get("Balcony.SQmeters") != null)
						balconyArea = dimensionsMap.get("Balcony.SQmeters").toString();
					double doubleBalconyArea = Double.parseDouble(balconyArea);
					
					double dTotalArea = 0;
					dTotalArea = doubleApartmentArea + doubleBalconyArea; 

					String carSpaces = "0";
					if (dimensionsMap.get("CarParks.NumberOf") != null)
						carSpaces = dimensionsMap.get("CarParks.NumberOf").toString();
					int intCarSpaces = new Double(carSpaces).intValue();
					
					
					ApartmentInfo aprmtInfo = new ApartmentInfo();
					aprmtInfo.setBuildingID(aprtmtQuery.getString("BuildingProductID")); 
					aprmtInfo.setApartmentID(aprtmtQuery.getString("AptProductID"));

					aprmtInfo.setBuildingName(aprtmtQuery.getString("BuildingName")); 
					aprmtInfo.setApartmentName(aprtmtQuery.getString("Name"));
						
					aprmtInfo.setLotNumber(aprtmtQuery.getString("Name"));
					aprmtInfo.setLevel(intLevels + "");  // Previously using: aprtmtQuery.getString("StockLevel")
					//aprmtInfo.setType(aprtmtQuery.getString("Description"));
					aprmtInfo.setType(aprtmtQuery.getString("ProductSubType"));
						
					if (doubleApartmentArea > 0)
					{	
						aprmtInfo.setInternalArea(new Double(doubleApartmentArea).intValue());  // aprtmtQuery.getDouble("Area")
					}
					
					if (doubleBalconyArea > 0)
					{	
						aprmtInfo.setBalconyArea(new Double(doubleBalconyArea).intValue());
					}
					
					if (dTotalArea > 0)
					{	
						aprmtInfo.setTotalArea(new Double(dTotalArea).intValue());
					}	
					
					if (intCarSpaces > 0)
					{
						aprmtInfo.setCarSpaces(intCarSpaces);	
					}	
						
					aprmtInfo.setPrice(aprtmtQuery.getData("TotalCost"));
					aprmtInfo.setRent(aprtmtQuery.getData("TotalRent"));
					aprmtInfo.setSalesYield(aprtmtQuery.getString("SalesYield") + "%");
					aprmtInfo.setStage(aprtmtQuery.getString("BuildingStageName"));
					aprmtInfo.setSalesStatus(aprtmtQuery.getString("CurrentStatus"));
					
					//aprmtInfo.setApartmentDetailsText1(PDFUtil.APARTMENT_DETAILS_HEADING_TEXT);
					//aprmtInfo.setApartmentDetailsText2(PDFUtil.APARTMENT_DETAILS_DESCRIPTION_TEXT);

					aprmtInfo.setApartmentDetailsText1(aprtmtQuery.getString("Summary"));
					aprmtInfo.setApartmentDetailsText2(aprtmtQuery.getString("Description"));
					
					
					String buildingID = aprtmtQuery.getString("BuildingProductID"); 
					String apartmentID = aprtmtQuery.getString("AptProductID");
					
					imagesList = new GenRow();
					imagesList.setViewSpec("LinkedDocumentView");
					imagesList.setParameter("ProductID", buildingID);
					if (buildingID!=null && !"".equalsIgnoreCase(buildingID))
					{	
						imagesList.doAction(GenerationKeys.SEARCH);
						imagesList.getResults();
					}
					ArrayList<String> buildingImgList = new ArrayList<String>();
					imgPath = aprtmtQuery.getString("BuildingThumbnailImagePath");
					if (imgPath!=null && !"".equalsIgnoreCase(imgPath))
					{	
						buildingImgList.add(imgPath);
					}
					imgPath = "";
					while (imagesList.getNext())  
					{
							imgPath = imagesList.getString("FilePath");
							if (imgPath!=null && !"".equalsIgnoreCase(imgPath))
							{	
								buildingImgList.add(imgPath);
							}	
					}
					imagesList.close();
					System.out.println("Building Images Query is:  " + imagesList.getStatement()); 

					
					
					imagesList = new GenRow();
					imagesList.setViewSpec("LinkedDocumentView");
					imagesList.setParameter("ProductID", apartmentID);
					if (apartmentID!=null && !"".equalsIgnoreCase(apartmentID))
					{	
						imagesList.doAction(GenerationKeys.SEARCH);
						imagesList.getResults();
					}
					
					ArrayList<String> apartmentImgList = new ArrayList<String>();
					ArrayList<String> aptFloorPlanImgList = new ArrayList<String>();
					imgPath = "";
					String imgType = "";
					while (imagesList.getNext())  
					{
							imgPath = imagesList.getString("FilePath");
							imgType = imagesList.getString("DocumentSubType");
							if (imgPath!=null && !"".equalsIgnoreCase(imgPath))
							{
								if ("Floor Plan".equalsIgnoreCase(imgType))
								{
									aptFloorPlanImgList.add(imgPath);	
								}
								else
								{
									apartmentImgList.add(imgPath);	
								}
								
							}	
					}
					imagesList.close();
					System.out.println("Apartments Images Query is:  " + imagesList.getStatement());

					
					aprmtInfo.setBuildingImagesList(buildingImgList);
					aprmtInfo.setApartmentImagesList(apartmentImgList);
					aprmtInfo.setApartmentFloorPlansImagesList(aptFloorPlanImgList);
					
					//Hero Image is set as thumbnail image else main image or else the floorplan image if available.
					String heroImg = "";
					if(StringUtils.isNotBlank(aprtmtQuery.getString("ThumbnailImagePath")))
						heroImg = aprtmtQuery.getString("ThumbnailImagePath");
					else if(StringUtils.isNotBlank(aprtmtQuery.getString("ImagePath")))
						heroImg = aprtmtQuery.getString("ImagePath");
					
					if (PDFUtil.PDF_DETAILS_BY_BUILDING.equalsIgnoreCase(pdfType))
					{
						ArrayList<String> fpImgList =  aprmtInfo.getApartmentFloorPlansImagesList();
						if (StringUtils.isBlank(heroImg) && fpImgList!=null && !fpImgList.isEmpty())
						{	
							heroImg = fpImgList.get(0);
						}	
					}	
					
					// aprmtInfo.setHeroImage(aprtmtQuery.getString("Image"));
					aprmtInfo.setHeroImage(heroImg);

					
					apartmentList.add(aprmtInfo);

					/*apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);
					apartmentList.add(aprmtInfo);*/
			}
			aprtmtQuery.close();
			
			System.out.println("apartmentList Size is: " + apartmentList.size());
			
			return apartmentList;
	}	
	
	
	public static String getBuildingIDsOfApartments(String aprtmtIDs, HttpServletRequest request) 
	{
			String buildingIDs = "";
			GenRow aprtmtQuery = new GenRow(); 
			aprtmtQuery.setRequest(request);
			aprtmtQuery.setViewSpec("properties/ProductApartmentView");
			//aprtmtQuery.setParameter("ProductType", "Apartment");
			aprtmtQuery.setAction(GenerationKeys.SEARCH);
			aprtmtQuery.setParameter("ProductID", aprtmtIDs);
	
			aprtmtQuery.doAction();
			aprtmtQuery.getResults();
			
			System.out.println("getBuildingIDsOfApartments Query is: " + aprtmtQuery.getStatement());
			
			int i=0;
			while (aprtmtQuery.getNext() ) 
			{
				if (i > 0)
				{
					buildingIDs += "+";
				}	
				buildingIDs += aprtmtQuery.getString("BuildingProductID");
				
				i++;
			}
			aprtmtQuery.close();
		
		System.out.println("buildingIDs String is: " + buildingIDs);

		return buildingIDs;
	}	
	
	public static Map<String, Object> getDimensions(String productID) 
	{
		GenRow details = new GenRow();
		details.setTableSpec("properties/ProductDetails");
		details.setParameter("-select", "*");
		details.setColumn("ProductID", productID);
		//details.setColumn("DetailsType", "Bedroom %");
		details.sortBy("ProductType", 0);
		details.sortBy("SortOrder", 1);
		details.sortBy("DetailsType", 2);
		if (details.getString("ProductID").length() > 0) {
			details.doAction("search");
			details.getResults(true);
		}
		
		
		Map<String, Object> detailMap = new HashMap<String, Object>();
		String tmpKey = "";
		while(details.getNext()) 
		{ 
			String type = details.getColumn("DetailsType").replaceAll(" ","");

			for(int i=0; i<details.getTableSpec().getFieldsLength(); i++) 
			{ 
					String field = details.getTableSpec().getFieldName(i);
					Object o = details.getObject(field);
					if(o != null) 
					{ 
						tmpKey = (type+"."+field).trim();
						detailMap.put(tmpKey, String.valueOf(o));
						
						// System.out.println("tmpKey:  " + tmpKey  + "    ,  value: "  + String.valueOf(o));
					}
			}
		}
		details.close();

		return detailMap;
	}	
}