package com.sok.runway.crm.cms.properties.excel.templates.model;

import java.util.ArrayList;
import java.util.List;

public class Estate {
	private List<Lot> lots = new ArrayList<Lot>();
	private List<Package> hnlPackages = new ArrayList<Package>();
	private List<RealEstate> realEstates = new ArrayList<RealEstate>();
	
	
	private String estateName;

	public List<Package> getHnlPackages() {
		return hnlPackages;
	}

	public void setHnlPackages(List<Package> hnlPackages) {
		this.hnlPackages = hnlPackages;
	}

	public String getEstateName() {
		return estateName;
	}

	public void setEstateName(String estateName) {
		this.estateName = estateName;
	}

	public List<RealEstate> getRealEstates() {
		return realEstates;
	}

	public void setRealEstates(List<RealEstate> realEstates) {
		this.realEstates = realEstates;
	}

	public List<Lot> getLots() {
		return lots;
	}

	public void setLots(List<Lot> lots) {
		this.lots = lots;
	}
}
