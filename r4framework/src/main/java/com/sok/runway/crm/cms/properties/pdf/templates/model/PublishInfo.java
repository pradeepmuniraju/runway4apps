package com.sok.runway.crm.cms.properties.pdf.templates.model;

public class PublishInfo {
	public String salesRep;
	public String range;
	public String design;
	public String estate;
	public String stage;
	public String marketingStatus;
	public String lotType;
	public String storeys;

	public String homePlanName;
	public String address;
	public String facade;
	public String width;
	public String depth;
	public String area;
	public String price;
	public String status;
	public String rea;
	public String domain;
	public String mypackage;
	public String website;
	public String print;
	public String hnl;

	public String getSalesRep() {
		return salesRep;
	}

	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getMarketingStatus() {
		return marketingStatus;
	}

	public void setMarketingStatus(String marketingStatus) {
		this.marketingStatus = marketingStatus;
	}

	public String getLotType() {
		return lotType;
	}

	public void setLotType(String lotType) {
		this.lotType = lotType;
	}

	public String getStoreys() {
		return storeys;
	}

	public void setStoreys(String storeys) {
		this.storeys = storeys;
	}

	public String getHomePlanName() {
		return homePlanName;
	}

	public void setHomePlanName(String homePlanName) {
		this.homePlanName = homePlanName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFacade() {
		return facade;
	}

	public void setFacade(String facade) {
		this.facade = facade;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getDepth() {
		return depth;
	}

	public void setDepth(String depth) {
		this.depth = depth;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHnl() {
		return hnl;
	}

	public void setHnl(String hnl) {
		this.hnl = hnl;
	}
	
	public String getRea() {
		return rea;
	}

	public void setRea(String rea) {
		this.rea = rea;
	}
	
	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getMypackage() {
		return mypackage;
	}

	public void setMypackage(String mypackage) {
		this.mypackage = mypackage;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getPrint() {
		return print;
	}

	public void setPrint(String print) {
		this.print = print;
	}
}