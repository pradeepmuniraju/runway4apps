package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.EstateLotHouseAndLandInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.model.EstateLotInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.model.ProductLotInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.model.RangeBrandInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.MultiEstateLotHeader;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;

public class MultiLandListPdf extends AbstractPDFTemplate implements PDFConstants 
{
	public static final BaseColor aqua = new BaseColor(0, 166, 162);
	
	public static final BaseColor blueColor = new BaseColor(0, 42, 84);  //  #002A53
	public static final BaseColor normalRowFontColor = new BaseColor(51, 51, 51); // #333333
	
	//public static final BaseColor skyBlueColor = new BaseColor(204, 242, 238); // CCF2EE       // Notes:
	public static final BaseColor skyBlueColor = new BaseColor(102, 102, 102); // ##75726F
	
	public static final BaseColor whiteColor = new BaseColor(255, 255, 255); // FFFFFF       
	public static final BaseColor blackColor = new BaseColor(0, 0, 0); // 000000
	
	public static final BaseColor darkGreyBGColor = new BaseColor(153, 153, 153); // #75726F
	public static final BaseColor greyBGColor = new BaseColor(221, 221, 221); // DDDDDD
	
	
	public static String rangeType = ""; // NI: 
	
	public static final String MAIN_LOGO_IMG = "/specific/pdf/images/fixed_price_stack_w_background.png";
	public static final String DISCLAIMER_TEXT = "Prices based on standard home, standard facade, builder's preferred siting and current promotion. See sales consultant for current promotion details. First Home Buyer price less First Home Owner Grant subject to SRO eligibility criteria. Photos for illustration purposes only. Guaranteed site start subject to land title & finance availability. 18, 20 & 23 weeks completion based on single storey construction subject to home range. 25 & 30 week completion based on standard double storey construction subject to home range. Build times include variations and geographic zones apply. Rent maximum $250.00 per week. Minimum lot width & depth may vary due to Council requirements & estate guidelines. House size based on standard facade. This flyer is for illustration purposes and should be used as a guide only. Refer to actual working drawings. Effective: ";

	
	public static final String BED_IMG = "/specific/pdf/images/pdf_icon_bed.png";
	public static final String BATH_IMG = "/specific/pdf/images/pdf_icon_bath.png";
	public static final String CAR_IMG = "/specific/pdf/images/pdf_icon_car.png";
	public static final String STUDY_IMG = "/specific/pdf/images/pdf_icon_study.png";
	public static final String BASIN_IMG = "/specific/pdf/images/pdf_icon_basin.png";
						    
	public static final BaseColor pkgHeaderGrey = new BaseColor(53, 52, 53);
	
	
	
/*	private static final BaseColor darkblueBG = new BaseColor(0, 7, 28); 
	private static final BaseColor darkblueFont = new BaseColor(0, 42, 84);  ==> blueColor  ==> #002A53
	private static final BaseColor lightblueBG = new BaseColor(0, 165, 229);
*/	
	
	// 30-May-2012 NI: Added for Embedded fonts.
	private static BaseFont defaultFont = null;
	private static BaseFont m100 = defaultFont, m300 = defaultFont, m500 = defaultFont, m700 = defaultFont, m900 = defaultFont;
	private static BaseFont h45 = defaultFont, h55 = defaultFont, h75 = defaultFont, h80 = defaultFont;

	static {
		try {
			String museopath = InitServlet.getRealPath() + "/specific/pdf/fonts/Museo/";
			String helvpath = InitServlet.getRealPath() + "/specific/pdf/fonts/HelveticaNeueLTStd/";

			m100 = BaseFont.createFont(museopath + "Museo_Slab_100.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			m300 = BaseFont.createFont(museopath + "Museo_Slab_300.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			m500 = BaseFont.createFont(museopath + "Museo_Slab_500.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			m700 = BaseFont.createFont(museopath + "Museo_Slab_700.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			m900 = BaseFont.createFont(museopath + "Museo_Slab_900.otf", BaseFont.CP1252, BaseFont.EMBEDDED);

			h45 = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-Lt.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			h55 = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-Roman.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			h75 = BaseFont.createFont(helvpath + "HelveticaNeueLTStd-Bd.otf", BaseFont.CP1252, BaseFont.EMBEDDED);
			
			h80 = BaseFont.createFont(helvpath + "HelveNeuMed.ttf", BaseFont.CP1252, BaseFont.EMBEDDED); 
			/*Helvetica Neue Medium
			13pt
			#333*/
			
		} catch (Exception e) {
			System.out.println("Ignoring Error while registering fonts. Error: " + e);
			e.printStackTrace();
		}
	}

	public static final Font helvitaNeuePt11Black = new Font(h80, fontRatio * 11, Font.NORMAL, blueColor.BLACK);
	public static final Font helvitaNeuePt11White = new Font(h80, fontRatio * 11, Font.NORMAL, blueColor.WHITE);

	public static final Font helvitaNeueMediumPt9White = new Font(h80, fontRatio * 9, Font.NORMAL, blueColor.WHITE);
	
	public static final Font helvitaNeueRomanPt8Blue = new Font(h45, fontRatio * 8, Font.NORMAL, blueColor);
	public static final Font helvitaNeueRomanPt12Blue = new Font(h45, fontRatio * 12, Font.NORMAL, blueColor);
	
	public static final Font helvitaNeueBoldPt12Blue = new Font(h75, fontRatio * 12, Font.NORMAL, blueColor);
	public static final Font helvitaNeueBoldPt8Blue = new Font(h75, fontRatio * 8, Font.NORMAL, blueColor);
	
	public static final Font helvitaNeueRomanPt14Blue = new Font(h55, fontRatio * 14, Font.NORMAL, blueColor);
	public static final Font helvitaNeueRomanPt15Blue = new Font(h55, fontRatio * 15, Font.NORMAL, blueColor);
	public static final Font helvitaNeueRomanPt16Blue = new Font(h55, fontRatio * 16, Font.NORMAL, blueColor);

	public static final Font museoSlab300Pt8Blue = new Font(m300, fontRatio * 8, Font.NORMAL, blueColor);
	public static final Font museoSlab300Pt11Blue = new Font(m300, fontRatio * 11, Font.NORMAL, blueColor);
	public static final Font museoSlab300Pt13Blue = new Font(m300, fontRatio * 13, Font.NORMAL, blueColor);
	public static final Font museoSlab300Pt14Blue = new Font(m300, fontRatio * 14, Font.NORMAL, blueColor);
	
	public static final Font museoSlab500Pt34Blue = new Font(m500, fontRatio * 34, Font.NORMAL, blueColor);
	public static final Font museoSlab500Pt25Blue = new Font(m500, fontRatio * 25, Font.NORMAL, blueColor);
	public static final Font museoSlab500Pt23Blue = new Font(m500, fontRatio * 23, Font.NORMAL, blueColor);
	public static final Font museoSlab500Pt17Blue = new Font(m500, fontRatio * 17, Font.NORMAL, blueColor);
	
	public static final Font museoSlab900Pt16Blue = new Font(m900, fontRatio * 16, Font.NORMAL, blueColor);
	public static final Font museoSlab900Pt23Blue = new Font(m900, fontRatio * 23, Font.NORMAL, blueColor);
	
	public static final Font helvitaNeueRomanPt12Grey = new Font(h55, fontRatio * 12, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueRomanPt8Grey = new Font(h55, fontRatio * 8, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueBoldPt12Grey = new Font(h75, fontRatio * 12, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueBoldPt8Grey = new Font(h75, fontRatio * 8, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueBoldPt6Grey = new Font(h75, fontRatio * 6, Font.NORMAL, normalRowFontColor);
	
	public static final Font helvitaNeueRomanPt11Grey = new Font(h55, fontRatio * 11, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueRomanPt13Grey = new Font(h55, fontRatio * 13, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueRomanPt15Grey = new Font(h55, fontRatio * 15, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueRomanPt17Grey = new Font(h55, fontRatio * 17, Font.NORMAL, normalRowFontColor);
	
	//public static final Font helvitaNeueMediumPt9Grey = new Font(h80, fontRatio * 9, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueMediumPt10Grey = new Font(h80, fontRatio * 10, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueMediumPt11Grey = new Font(h80, fontRatio * 11, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueMediumPt13Grey = new Font(h80, fontRatio * 13, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueMediumPt15Grey = new Font(h80, fontRatio * 15, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueMediumPt17Grey = new Font(h80, fontRatio * 17, Font.NORMAL, normalRowFontColor);
	public static final Font helvitaNeueMediumPt19Grey = new Font(h80, fontRatio * 19, Font.NORMAL, normalRowFontColor);
	
	public static final Font helvitaNeueMediumPt11White = new Font(h80, fontRatio * 11, Font.NORMAL, blueColor.WHITE);
	public static final Font helvitaNeueMediumPt17White = new Font(h80, fontRatio * 17, Font.NORMAL, blueColor.WHITE);
	
	public static final Font helvitaNeueBoldPt8White = new Font(h75, fontRatio * 8, Font.NORMAL, blueColor.WHITE);
	public static final Font helvitaNeueRomanPt8White = new Font(h55, fontRatio * 8, Font.NORMAL, blueColor.WHITE);
	public static final Font helvitaNeueRomanPt12White = new Font(h55, fontRatio * 12, Font.NORMAL, blueColor.WHITE);
	public static final Font helvitaNeueBoldPt12White = new Font(h75, fontRatio * 12, Font.NORMAL, blueColor.WHITE);
	
	public static ArrayList<RangeBrandInfo> rangeBrandList = new ArrayList<RangeBrandInfo>();
	
	double currentPercentage = 0f;
	
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException 
	{
		StringBuilder productIDsStr = new StringBuilder();
		// this is so inefficient, load all this data to get just the IDs
		//java.util.List<PropertyEntity> properties = PropertyFactory.getPropertyEntities(request);
		boolean listAllProducts = "-1".equals(request.getParameter("ProductID"));
		String[] id = request.getParameterValues("ProductID");
		if (id == null || id.length == 0) {
			String pID = (String) request.getSession().getAttribute("productIDs");
			if (StringUtils.isNotBlank(pID)) {
				id = pID.split("\\+");
			}
		}
		if (id != null && id.length > 0) {
			for (int p = 0; p < id.length; ++p) 
			{
				if (StringUtils.isNotBlank(id[p]))
				{
					if (productIDsStr.length() > 0) productIDsStr.append("+");
					if(StringUtils.isNotBlank(id[p]) && !listAllProducts)
						productIDsStr.append(id[p]);
	
				}
			}
		} else {
			String ID = request.getParameter("ProductID");
			if (StringUtils.isNotBlank(ID)) productIDsStr.append(ID);
		}
		
		setStatus(request,"Loading Ranges...", 0.1);
		
		if (productIDsStr.length() == 0)
		{	
			rangeBrandList = getAllActiveRanges (request);
		}
		else
		{
			rangeBrandList = getActiveRangesByProductIDs (request, productIDsStr.toString());
		}	
		
		setStatus(request,"Loading Estates...", 0.3);
		
		GenRow estateQuery = new GenRow(); 
		estateQuery.setRequest(request);
		estateQuery.setViewSpec("ProductView");
		estateQuery.setParameter("ProductType", "Estate");
		estateQuery.setParameter("Active", "Y");
		estateQuery.setParameter("-join0", "SelllingAgentCompanyName");  
		estateQuery.setParameter("-join1", "DeveloperProductName");
		
		estateQuery.setParameter("-join2", "EstateLocation");
		
		estateQuery.setParameter("-select0", "SelllingAgentCompanyName.Company as SellingAgentName");
		estateQuery.setParameter("-select1", "DeveloperProductName.Name as DeveloperProductName");
		
		estateQuery.setParameter("-select2", "ProductDisplayEntity.Name as LocationName");
		estateQuery.setParameter("-select3", "ProductDisplayEntity.DisplayEntityID as DisplayEntityID");
		
		estateQuery.sortBy("Name",0);
		estateQuery.setAction(GenerationKeys.SEARCH);
		estateQuery.doAction();
		estateQuery.getResults();
		if(!estateQuery.isSuccessful()) { 
			throw new RuntimeException("Estates records not found");
		}
		System.out.println("estates query is: " + estateQuery.getStatement());

		setStatus(request,"Loading and linking House and Land Packages with Lots and Estates...", 0.4);
		
		GenRow houseAndLandView = new GenRow(); 
		houseAndLandView.setRequest(request);
		houseAndLandView.setViewSpec("properties/ProductHouseAndLandView");
		houseAndLandView.setParameter("ProductType", "House and Land");
		houseAndLandView.setParameter("ProductID", productIDsStr.toString());
		houseAndLandView.setParameter("Active", "Y");
		houseAndLandView.sortBy("EstateName",0);
		houseAndLandView.sortBy("StageName",1);
		houseAndLandView.sortBy("LotName",2);
		if (productIDsStr.length() > 0 || listAllProducts) {
			houseAndLandView.doAction(GenerationKeys.SEARCH);
			houseAndLandView.getResults();
		}
		System.out.println("ProductHouseAndLandView query is: " + houseAndLandView.getStatement());
		
		ArrayList<EstateLotHouseAndLandInfo> tmphlList = new ArrayList<EstateLotHouseAndLandInfo>();
		while (houseAndLandView.getNext() ) 
		{
				EstateLotHouseAndLandInfo hlInfo = new EstateLotHouseAndLandInfo();
				
				hlInfo.setEstateProductID(houseAndLandView.getString("EstateProductID"));
				hlInfo.setEstateEstateName(houseAndLandView.getString("EstateName"));
				hlInfo.setEstateStageName(houseAndLandView.getString("StageName"));
				hlInfo.setLotNumber(houseAndLandView.getString("LotName"));
				
				/*hlInfo.setLotStatus(houseAndLandView.getString("CurrentStatus"));  // e.g.; New, Sold, Not Released	
				hlInfo.setConsultant(houseAndLandView.getString("LotConsultant"));  // TODO
				hlInfo.setLotPrice(houseAndLandView.getString("LotTotalCost"));
				hlInfo.setExclusiveHold(houseAndLandView.getString("LotMarketingStatus"));
				hlInfo.setLotWidth(houseAndLandView.getDouble("LotWidth"));
				hlInfo.setLotDepth(houseAndLandView.getDouble("LotDepth"));
				hlInfo.setLotSize(houseAndLandView.getDouble("CanFitOnFrontage"));   // TODO
				hlInfo.setExpectedTO(houseAndLandView.getString("LotTitleMonth") + " " + houseAndLandView.getString("LotTitleYear"));
				*/
				
				hlInfo.setRange(houseAndLandView.getString("RangeName"));
				
				for (int r=0; r<rangeBrandList.size(); r++)
				{
					RangeBrandInfo rangeInfo = rangeBrandList.get(r);
					
					if (rangeInfo!=null)
					{	
							if (rangeInfo.getRangeName().equalsIgnoreCase(houseAndLandView.getString("RangeName")))
							{
								if (r==0)
								{	
									hlInfo.setIsRangeBrand1("Y");
								}
								else if (r==1)
								{
									hlInfo.setIsRangeBrand2("Y");
								}
								else if (r==2)
								{
									hlInfo.setIsRangeBrand3("Y");
								}	
								else if (r==3)
								{
									hlInfo.setIsRangeBrand4("Y");
								}	
								else if (r==4)
								{
									hlInfo.setIsRangeBrand5("Y");
								}	
								else if (r==5)
								{
									hlInfo.setIsRangeBrand6("Y");
								}	
								else if (r==6)
								{
									hlInfo.setIsRangeBrand7("Y");
								}	
								else if (r==7)
								{
									hlInfo.setIsRangeBrand8("Y");
								}	
								else if (r==8)
								{
									hlInfo.setIsRangeBrand9("Y");
								}	
								else if (r==9)
								{
									hlInfo.setIsRangeBrand10("Y");
								}
								
								break;
							}
					}	
				}	
				
				tmphlList.add(hlInfo);
		}

		HashMap<String, ProductLotInfo> lotHash = populateProductLotHashMap(request, tmphlList);
		
		setStatus(request,"Linking Brands and Ranges...", 0.7);
		
		ArrayList<EstateLotInfo> estateLotInfoList = getEstatesHouseLandData(estateQuery, tmphlList, request);
		
		setStatus(request,"Rendering the PDF...", 0.8);
		
		renderPages(document, estateLotInfoList, lotHash);
		
		setStatus(request,"Finalizing the output...", 0.9);
	}
	
	
	public ArrayList<RangeBrandInfo> getAllActiveRanges (HttpServletRequest request)
	{
		 	ArrayList<RangeBrandInfo> rangeInfoList = new ArrayList<RangeBrandInfo>();
		 	
			GenRow rangeQuery = new GenRow(); 
			rangeQuery.setRequest(request);
			rangeQuery.setViewSpec("ProductView");
			rangeQuery.setParameter("ProductType", "Home Range");
			rangeQuery.setParameter("Active", "Y");
			rangeQuery.sortBy("Name",0);
			rangeQuery.setAction(GenerationKeys.SEARCH);
			rangeQuery.doAction();
			rangeQuery.getResults();
			
			if(!rangeQuery.isSuccessful()) 
			{ 
				throw new RuntimeException("No Active Brand / Range available");
			}
			System.out.println("Brand - Range query is: " + rangeQuery.getStatement());

			RangeBrandInfo rangeInfo = null;
			while (rangeQuery.getNext() ) 
			{
				rangeInfo = new RangeBrandInfo();
				
				rangeInfo.setRangeID(rangeQuery.getString("ProductID"));
				rangeInfo.setRangeName(rangeQuery.getString("Name"));
				rangeInfo.setRangeColour(rangeQuery.getString("RangeColorHex"));
				
				rangeInfoList.add(rangeInfo);
			}
			rangeQuery.close();
		 	
		 	return rangeInfoList;
	}
	
	
	public ArrayList<RangeBrandInfo> getActiveRangesByProductIDs (HttpServletRequest request, String productIDsStr)
	{
			GenRow rangeQuery = new GenRow(); 
			rangeQuery.setRequest(request);
			rangeQuery.setViewSpec("properties/ProductHouseAndLandReport");
//			rangeQuery.setParameter("ProductType", "Home Range");
			rangeQuery.setParameter("ProductID", productIDsStr);
// 29-Jan-2013 NI: 			rangeQuery.setParameter("Active", "Y");
//			rangeQuery.sortBy("RangeName",0);
			rangeQuery.setAction(GenerationKeys.SEARCH);
			rangeQuery.doAction();
			rangeQuery.getResults();
			
			if(!rangeQuery.isSuccessful()) 
			{ 
				throw new RuntimeException("No Active Brand / Range available");
			}
			System.out.println("Brand - Range query is: " + rangeQuery.getStatement());

			
			String rangeIDsStr = "";
			int indx = 0;
			while (rangeQuery.getNext() ) 
			{
					if (indx > 0)
					{
						rangeIDsStr += "+";
					}
					rangeIDsStr += rangeQuery.getString("RangeProductID");
					indx++;			
			}
			rangeQuery.close();
		 	
			
			
		 	ArrayList<RangeBrandInfo> rangeInfoList = new ArrayList<RangeBrandInfo>();
			rangeQuery = new GenRow(); 
			rangeQuery.setRequest(request);
			rangeQuery.setViewSpec("ProductView");
			rangeQuery.setParameter("-select1", "Products.ProductID as ProductRangeID");
			rangeQuery.setParameter("ProductType", "Home Range");
			rangeQuery.setParameter("ProductID", rangeIDsStr);
// 29-Jan-2013 NI: 						rangeQuery.setParameter("Active", "Y");
			rangeQuery.sortBy("Name",0);
			rangeQuery.setAction(GenerationKeys.SEARCH);
			rangeQuery.doAction();
			rangeQuery.getResults();
			
			if(!rangeQuery.isSuccessful()) 
			{ 
				throw new RuntimeException("No Active Brand / Range available");
			}
			System.out.println("Brand - Range query is: " + rangeQuery.getStatement());

			RangeBrandInfo rangeInfo = null;
			while (rangeQuery.getNext() ) 
			{
				rangeInfo = new RangeBrandInfo();
			
				if (!isValueAlreadyExist (rangeInfoList, rangeQuery.getString("ProductRangeID")))
				{	
					rangeInfo.setRangeID(rangeQuery.getString("ProductRangeID"));
					rangeInfo.setRangeName(rangeQuery.getString("Name"));
					rangeInfo.setRangeColour(rangeQuery.getString("RangeColorHex"));
				
					rangeInfoList.add(rangeInfo);
				}	
			}
			rangeQuery.close();

			
			System.out.println("rangeInfoList size is: " + rangeInfoList.size());
			
			
		 	return rangeInfoList;
	}	

	
	/*public ArrayList<RangeBrandInfo> getActiveRangesByProductIDs (HttpServletRequest request, String productIDsStr)
	{
		 	ArrayList<RangeBrandInfo> rangeInfoList = new ArrayList<RangeBrandInfo>();
		 	
			GenRow rangeQuery = new GenRow(); 
			rangeQuery.setRequest(request);
			rangeQuery.setViewSpec("properties/ProductHouseAndLandView");
//			rangeQuery.setParameter("ProductType", "Home Range");
			rangeQuery.setParameter("ProductID", productIDsStr);
			rangeQuery.setParameter("Active", "Y");
			rangeQuery.sortBy("RangeName",0);
			rangeQuery.setAction(GenerationKeys.SEARCH);
			rangeQuery.doAction();
			rangeQuery.getResults();
			
			if(!rangeQuery.isSuccessful()) 
			{ 
				throw new RuntimeException("No Active Brand / Range available");
			}
			System.out.println("Brand - Range query is: " + rangeQuery.getStatement());

			RangeBrandInfo rangeInfo = null;
			while (rangeQuery.getNext() ) 
			{
				rangeInfo = new RangeBrandInfo();
				
				if (!isValueAlreadyExist (rangeInfoList, rangeQuery.getString("RangeProductID")))
				{	
					rangeInfo.setRangeID(rangeQuery.getString("RangeProductID"));
					rangeInfo.setRangeName(rangeQuery.getString("RangeName"));
					rangeInfo.setRangeColour(rangeQuery.getString("RangeColorHex"));
					rangeInfoList.add(rangeInfo);
				}	
			}
			rangeQuery.close();
		 	
		 	return rangeInfoList;
	}	*/
	
	boolean isValueAlreadyExist(ArrayList<RangeBrandInfo> rangeList, String rangeID) 
	{
		if (rangeList != null && !StringUtil.isBlankOrEmpty(rangeID))
		{	
		    for (int i=0; i<rangeList.size(); i++) 
		    {
		    	RangeBrandInfo rangeInfo = rangeList.get(i);
		    	//System.out.println("rangeID: " + rangeID + " ,  rangeInfo.getRangeID: " + rangeInfo.getRangeID());
		        if (rangeID.equalsIgnoreCase(rangeInfo.getRangeID())) 
		        {
		            return true;
		        }
		    }
		}    
	    return false;
	}

	@Override
	public void setMargins(Document document) 
	{
		document.setMargins(15, 30, 100, 0); // L R T B
	}
	
	private void renderPages(com.itextpdf.text.Document document, ArrayList<EstateLotInfo> estateLotInfoList, HashMap<String, ProductLotInfo> lotHash) throws DocumentException {
		// TreeSet<String> sortedEstates = new TreeSet<String>(estates.keySet());
		//boolean addNewPage = false;
		
		renderEstatePage(document, estateLotInfoList, lotHash);

	}

	private void renderEstatePage(com.itextpdf.text.Document document, ArrayList<EstateLotInfo> estateLotInfoList, HashMap<String, ProductLotInfo> lotHash) throws DocumentException 
	{
		PdfPTable pageTable = new PdfPTable(1);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.setWidthPercentage(100f);
		
		/*PdfPTable topBlankTable = new PdfPTable(15);
		topBlankTable.getDefaultCell().setPadding(0);
		topBlankTable.getDefaultCell().setBorder(0);
		topBlankTable.getDefaultCell().setBorderWidth(0);
		topBlankTable.getDefaultCell().setColspan(15);
		topBlankTable.getDefaultCell().setBackgroundColor(whiteColor);
		topBlankTable.addCell(new Paragraph("", helvitaNeueMediumPt11Grey));
		topBlankTable.completeRow();

		pageTable.getDefaultCell().setPadding(0);
		pageTable.getDefaultCell().setPaddingTop(120);
		pageTable.getDefaultCell().setPaddingBottom(0);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
		pageTable.addCell(topBlankTable);*/
		
		
		if (estateLotInfoList != null) 
		{
				pageTable.getDefaultCell().setPadding(0);
				pageTable.getDefaultCell().setPaddingTop(10);
				pageTable.getDefaultCell().setPaddingBottom(10);
				pageTable.getDefaultCell().setBorder(0);
				pageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
				
				for (int i=0; i<estateLotInfoList.size(); i++)
				{
						EstateLotInfo e = estateLotInfoList.get(i);
						if (e!=null)
						{	
							if (e.getEstateLotHLInfoList() != null && e.getEstateLotHLInfoList().size() > 0)
							{
								PdfPTable bodyTable = getBodyTable(e, lotHash);
								pageTable.addCell(bodyTable);
							}	
						}	
				}	
		}

		document.add(pageTable);
	}



	private static int getNoOfChunks(int dataSize, int chunkSize) {
		return dataSize % chunkSize == 0 ? dataSize / chunkSize : (dataSize / chunkSize) + 1;
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		//String headText = "\nFooter text will come over here...";
		//String pdfType = "byLand";
		return new MultiEstateLotHeader();
	}
	
	public String getPdfSimpleName() {
		return "Multi Estate Lot List PDF";
	}
	
	public static ArrayList<EstateLotInfo>  getEstatesHouseLandData(GenRow estateQuery, ArrayList<EstateLotHouseAndLandInfo> tmphlList, HttpServletRequest request) 
	{
			ArrayList<EstateLotInfo> estateLotInfoList = new ArrayList <EstateLotInfo>();  // Final List

			ArrayList<EstateLotHouseAndLandInfo> hlInfoList;
			EstateLotInfo estateLotInfo; 
			
			while (estateQuery.getNext() ) 
			{
					hlInfoList = new ArrayList <EstateLotHouseAndLandInfo>();
					estateLotInfo = new EstateLotInfo();
					
					Calendar currentDate = Calendar.getInstance();
					SimpleDateFormat formatter =  new SimpleDateFormat("dd/MM/yyyy");
					String dateNow = formatter.format(currentDate.getTime());
					estateLotInfo.setCurrentDate(dateNow);
					
					estateLotInfo.setEstateProductID(estateQuery.getString("ProductID"));
					estateLotInfo.setEstateName(estateQuery.getString("Name"));
					estateLotInfo.setEstateCity(estateQuery.getString("City"));
					estateLotInfo.setEstateMelRef(estateQuery.getString("MapRef"));
					estateLotInfo.setEstateDeveloper(estateQuery.getString("DeveloperProductName"));
					estateLotInfo.setEstateAgent(estateQuery.getString("SellingAgentName"));
					estateLotInfo.setEstateNotes(estateQuery.getString("Notes"));  // TODO: Notes ??
					
					/*estateLotInfo.setEstateContactPerson(estateQuery.getString("OwnFirstName") + " " + estateQuery.getString("OwnLastName"));
					estateLotInfo.setEstateModifierName(estateQuery.getString("ModFirstName") + " " + estateQuery.getString("ModLastName"));
					estateLotInfo.setEstateModifierNumber(estateQuery.getString("ModiferNumber")); // TODO: Modifer Number ??*/
					
					///////////////////////// estateQuery.setParameter("-select7", "SelllingAgentCompanyName.Company as LocationContactPerson");

					
					String tmpVendorID = estateQuery.getString("Vendor");
					if (tmpVendorID!=null && !"".equalsIgnoreCase(tmpVendorID))
					{	
							GenRow seller = new GenRow();
							seller.setTableSpec("Contacts");
							seller.setRequest(request);
							seller.setParameter("-select0", "ContactID");
							seller.setParameter("-select1", "FirstName");
							seller.setParameter("-select2", "LastName");
							seller.setParameter("-select3", "Phone");
							seller.setParameter("-select4", "Mobile");
							seller.setColumn("ContactID", tmpVendorID);	
							seller.doAction("search");
							seller.getResults(true);
								
							while (seller.getNext()) 
							{
									String tmpRepName = seller.getString("FirstName") + " " + seller.getString("LastName");
									estateLotInfo.setEstateSellingAgentSalesRepName(tmpRepName);
									
									String tmpRepPhoneNo =  "";
									if (seller.getString("Phone") != null && !"".equalsIgnoreCase(seller.getString("Phone")))
									{
										tmpRepPhoneNo = seller.getString("Phone"); 
									}
									if (tmpRepPhoneNo==null || "".equalsIgnoreCase(tmpRepPhoneNo))
									{
										if (seller.getString("Mobile") != null && !"".equalsIgnoreCase(seller.getString("Mobile")))
										{
											tmpRepPhoneNo = seller.getString("Mobile"); 
										}
									}	
									estateLotInfo.setEstateSellingAgentSalesRepPhone(tmpRepPhoneNo);
									break;
							}
							seller.close();
					}
					else
					{	
							String tmpSellingAgentID = estateQuery.getString("SellingAgent");
							//System.out.println("tmpSellingAgentID is: " + tmpSellingAgentID);
							if (tmpSellingAgentID!=null && !"".equalsIgnoreCase(tmpSellingAgentID))
							{	
									GenRow seller = new GenRow();
									seller.setTableSpec("Contacts");
									seller.setRequest(request);
									seller.setParameter("-select0", "ContactID");
									seller.setParameter("-select1", "FirstName");
									seller.setParameter("-select2", "LastName");
									seller.setParameter("-select3", "Phone");
									seller.setParameter("-select4", "Mobile");
									//seller.setParameter("-groupby0","ContactID");
									//seller.sortBy("LastName",0);
									//seller.sortBy("FirstName",1);
									seller.setColumn("CompanyID", tmpSellingAgentID);	
									seller.doAction("search");
									seller.getResults(true);
										
									//System.out.println("RepNames Query is: "+ seller.getStatement());
									
									while (seller.getNext()) 
									{
											String tmpRepName = seller.getString("FirstName") + " " + seller.getString("LastName");
											estateLotInfo.setEstateSellingAgentSalesRepName(tmpRepName);
											
											String tmpRepPhoneNo =  "";
											if (seller.getString("Phone") != null && !"".equalsIgnoreCase(seller.getString("Phone")))
											{
												tmpRepPhoneNo = seller.getString("Phone"); 
											}
											if (tmpRepPhoneNo==null || "".equalsIgnoreCase(tmpRepPhoneNo))
											{
												if (seller.getString("Mobile") != null && !"".equalsIgnoreCase(seller.getString("Mobile")))
												{
													tmpRepPhoneNo = seller.getString("Mobile"); 
												}
											}	
											estateLotInfo.setEstateSellingAgentSalesRepPhone(tmpRepPhoneNo);
											break;
									}
									seller.close();
												
		
									//GenRow companyLinkedContacts = new GenRow();
									//companyLinkedContacts.setViewSpec("CompanyContactLinkView");
									//companyLinkedContacts.setRequest(request);
									//companyLinkedContacts.setParameter("CompanyID", tmpSellingAgentID);
									//companyLinkedContacts.doAction("search");
									//companyLinkedContacts.getResults();
									
									//System.out.println("companyLinkedContacts Query is: "+ companyLinkedContacts.getStatement());
									
									//while (companyLinkedContacts.getNext()) 
									//{
									//		String tmpLctnPrsn = companyLinkedContacts.getString("FirstName") + " " + companyLinkedContacts.getString("LastName");
									//		estateLotInfo.setEstateLocationContactPersonName(tmpLctnPrsn);
									//		break;
									//}
									//companyLinkedContacts.close();
							}
					}
					
					
					/*String tmpVendorID = estateQuery.getString("SellingAgent");
					if (tmpVendorID!=null && !"".equalsIgnoreCase(tmpVendorID))
					{	
								GenRow row = new GenRow();
								row.setRequest(request);
								row.setViewSpec("UserView");
								row.setParameter("UserID",tmpVendorID);
								row.doAction("selectfirst");

								String tmpRepName = row.getString("FirstName") + " " + row.getString("LastName");
								estateLotInfo.setEstateSellingAgentSalesRepName(tmpRepName);
								
								String tmpRepPhoneNo =  "";
								if (row.getString("Phone") != null && !"".equalsIgnoreCase(row.getString("Phone")))
								{
									tmpRepPhoneNo = row.getString("Phone"); 
								}
								if (tmpRepPhoneNo==null || "".equalsIgnoreCase(tmpRepPhoneNo))
								{
									if (row.getString("Mobile") != null && !"".equalsIgnoreCase(row.getString("Mobile")))
									{
										tmpRepPhoneNo = row.getString("Mobile"); 
									}
								}	
								estateLotInfo.setEstateSellingAgentSalesRepPhone(tmpRepPhoneNo);
								
								row.close();
					}*/					
					
					
					String tmpDsplyEntyID = estateQuery.getString("DisplayEntityID");
					if (tmpDsplyEntyID!=null && !"".equalsIgnoreCase(tmpDsplyEntyID))
					{
							GenRow childLocations = new GenRow();
							childLocations.setTableSpec("DisplayEntities");
							childLocations.setRequest(request);
							childLocations.setParameter("-select0","DisplayEntityID");
							childLocations.setParameter("-select1","Name");
							childLocations.setParameter("-select2","ProductID");
							// childLocations.setParameter("ParentDisplayEntityID", tmpDsplyEntyID);
							childLocations.setParameter("DisplayEntityID", tmpDsplyEntyID);
							childLocations.doAction("search");
							childLocations.getResults();
							//System.out.println("childLocations Query is: "+ childLocations.getStatement());
							while(childLocations.getNext())
							{
									GenRow companyLinkedContacts = new GenRow();
									companyLinkedContacts.setViewSpec("CompanyContactLinkView");
									companyLinkedContacts.setRequest(request);
									
								    companyLinkedContacts.setParameter("CompanyID",childLocations.getString("ProductID"));
									companyLinkedContacts.doAction("search");
									companyLinkedContacts.getResults();
									//System.out.println("companyLinkedContacts Query is: "+ companyLinkedContacts.getStatement());
									
									String locationStaffs = "";
									while (companyLinkedContacts.getNext()) 
									{
										if (!"".equalsIgnoreCase(locationStaffs))
										{
											locationStaffs += " , ";
										}	
										locationStaffs += companyLinkedContacts.getString("FirstName") + " " + companyLinkedContacts.getString("LastName");
									}
									estateLotInfo.setEstateLocationContactPersonName(locationStaffs);
									companyLinkedContacts.close();

									break;
							}
							childLocations.close();
					}	
					
					estateLotInfo.setEstateLocationName(estateQuery.getString("LocationName"));
					
					for (int i=0; i<tmphlList.size(); i++)
					{
							EstateLotHouseAndLandInfo tmpInfo = tmphlList.get(i);
							if (tmpInfo!=null && tmpInfo.getEstateProductID().equalsIgnoreCase(estateQuery.getString("ProductID")))
							{
									EstateLotHouseAndLandInfo hlInfo = new EstateLotHouseAndLandInfo();
									
									hlInfo.setEstateProductID(tmpInfo.getEstateProductID());
									hlInfo.setEstateEstateName(tmpInfo.getEstateEstateName());
									hlInfo.setEstateStageName(tmpInfo.getEstateStageName());
									hlInfo.setLotNumber(tmpInfo.getLotNumber());

									/*hlInfo.setLotStatus(tmpInfo.getLotStatus());	
									hlInfo.setConsultant(tmpInfo.getConsultant());
									hlInfo.setLotPrice(tmpInfo.getLotPrice());
									hlInfo.setExclusiveHold(tmpInfo.getExclusiveHold());
									hlInfo.setLotWidth(tmpInfo.getLotWidth());
									hlInfo.setLotDepth(tmpInfo.getLotDepth());
									hlInfo.setLotSize(tmpInfo.getLotSize());
									hlInfo.setExpectedTO(tmpInfo.getExpectedTO());*/
									
									hlInfo.setRange(tmpInfo.getRange());
									
									hlInfo.setIsRangeBrand1(tmpInfo.getIsRangeBrand1());
									hlInfo.setIsRangeBrand2(tmpInfo.getIsRangeBrand2());
									hlInfo.setIsRangeBrand3(tmpInfo.getIsRangeBrand3());
									hlInfo.setIsRangeBrand4(tmpInfo.getIsRangeBrand4());
									hlInfo.setIsRangeBrand5(tmpInfo.getIsRangeBrand5());
									hlInfo.setIsRangeBrand6(tmpInfo.getIsRangeBrand6());
									hlInfo.setIsRangeBrand7(tmpInfo.getIsRangeBrand7());
									hlInfo.setIsRangeBrand8(tmpInfo.getIsRangeBrand8());
									hlInfo.setIsRangeBrand9(tmpInfo.getIsRangeBrand9());
									hlInfo.setIsRangeBrand10(tmpInfo.getIsRangeBrand10());
									
									hlInfoList.add(hlInfo);
							}
					}
					estateLotInfo.setEstateLotHLInfoList(hlInfoList);

					estateLotInfoList.add(estateLotInfo);
			}		
		return estateLotInfoList;
	}
	
	
	public static PdfPTable getHeader() throws DocumentException 
	{
			int fixedColumns = 8;
			int totalColumns = fixedColumns + rangeBrandList.size();
			
			PdfPTable headerTable = new PdfPTable(totalColumns);
			float fWidths [] = new float [totalColumns];
			
			int tmpCounter = 0;
			fWidths[tmpCounter++] = 30f; 
			fWidths[tmpCounter++] = 30f;
			fWidths[tmpCounter++] = 40f;
			fWidths[tmpCounter++] = 30f;
			fWidths[tmpCounter++] = 30f;
			fWidths[tmpCounter++] = 30f;
			fWidths[tmpCounter++] = 30f;
			fWidths[tmpCounter++] = 20f;
			for (int r=0; r<rangeBrandList.size(); r++)
			{
				fWidths[tmpCounter++] = 10f;
			}
			headerTable.setWidths(fWidths);
			
			
			headerTable.getDefaultCell().setPadding(0);
			headerTable.getDefaultCell().setPaddingBottom(4);
			headerTable.getDefaultCell().setPaddingTop(4);
			headerTable.getDefaultCell().setPaddingLeft(0);
			headerTable.getDefaultCell().setBorder(0);
			headerTable.getDefaultCell().setBorderWidth(0.0f);
			// headerTable.getDefaultCell().setRight(500);
			//headerTable.getDefaultCell().setBackgroundColor(aqua); //todo
			headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			headerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
	
			// Row 01
			headerTable.getDefaultCell().setColspan(4);
			headerTable.getDefaultCell().setBackgroundColor(blackColor);
			headerTable.addCell(new Paragraph("Land List", helvitaNeueMediumPt17White));
			
			Calendar currentDate = Calendar.getInstance();
			SimpleDateFormat formatter =  new SimpleDateFormat("dd/MM/yyyy");
			String dateNow = formatter.format(currentDate.getTime());
	
			
			headerTable.getDefaultCell().setBorder(0);
			headerTable.getDefaultCell().setBorderWidth(0f);
			headerTable.getDefaultCell().setBorderWidthLeft(0f);
			headerTable.getDefaultCell().setBorderWidthRight(0f);
			headerTable.getDefaultCell().setBorderWidthTop(0f);
			headerTable.getDefaultCell().setBorderWidthBottom(0f);
			headerTable.getDefaultCell().setColspan(3);
			headerTable.getDefaultCell().setBackgroundColor(blackColor);
			headerTable.addCell(new Paragraph("Week ending: " + dateNow, helvitaNeueMediumPt17White));
			
			headerTable.getDefaultCell().setColspan(1);
			headerTable.getDefaultCell().setBackgroundColor(whiteColor);
			headerTable.addCell(BLANK);
	
			////headerTable.getDefaultCell().setRowspan(3);
			
			
			for (int r=0; r<rangeBrandList.size(); r++)
			{
				RangeBrandInfo rbInfo = rangeBrandList.get(r);
				
				Font tmpFont = null;
				if (rbInfo.getRangeColour() != null && !"".equalsIgnoreCase(rbInfo.getRangeColour()) && !"#FFFFFF".equalsIgnoreCase(rbInfo.getRangeColour()))
				{
					tmpFont = new Font(helvitaNeueBoldPt12White);
				}
				else
				{
					tmpFont = new Font(helvitaNeueBoldPt12Grey);
				}	
				
				PdfPCell verticalCell = new PdfPCell(new Paragraph(rbInfo.getRangeName(), tmpFont));
				verticalCell.setRotation(90);
				verticalCell.setRowspan(5);
				verticalCell.setRotation(90);
				verticalCell.setBackgroundColor(hex2Rgb(rbInfo.getRangeColour()));
				//headerTable.getDefaultCell().setFixedHeight(30);
				headerTable.addCell(verticalCell);
			}
			headerTable.completeRow();
	

			// Row 02
			headerTable.getDefaultCell().setColspan(fixedColumns);
			headerTable.getDefaultCell().setBackgroundColor(whiteColor);
			headerTable.getDefaultCell().setPaddingBottom(10);
			headerTable.getDefaultCell().setPaddingTop(10);
			headerTable.addCell(BLANK);
			headerTable.getDefaultCell().setPaddingBottom(0);
			headerTable.getDefaultCell().setPaddingTop(0);
			headerTable.completeRow();
			
			// Row 03
			/*PdfPTable legendTable = new PdfPTable(2);
			legendTable.setWidths(new float[] { 80f, 120f });
			legendTable.getDefaultCell().setPadding(0);
			legendTable.getDefaultCell().setPaddingBottom(2);
			legendTable.getDefaultCell().setPaddingLeft(0);
			legendTable.getDefaultCell().setBorder(1);
			legendTable.getDefaultCell().setBorderWidth(0.1f);
			legendTable.getDefaultCell().setBorderWidthLeft(0.1f);
			legendTable.getDefaultCell().setBorderWidthRight(0.1f);
			legendTable.getDefaultCell().setBorderWidthTop(0.1f);
			legendTable.getDefaultCell().setBorderWidthBottom(0.1f);
			legendTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			legendTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
	
	
			legendTable.getDefaultCell().setRowspan(3);
			legendTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
			legendTable.addCell(new Paragraph("Legend", museoSlab500Pt17Blue));
			legendTable.getDefaultCell().setRowspan(1);
			
			//legendTable.getDefaultCell().setBackgroundColor(porterDavisSkyBlueColor);
			//legendTable.addCell(new Paragraph("Lot (New)", helvitaNeueMediumPt11Grey));
			
			legendTable.getDefaultCell().setBackgroundColor(porterDavisAccessRed);
			legendTable.addCell(new Paragraph("Access Suitable", helvitaNeueMediumPt11Grey));
			
			//legendTable.getDefaultCell().setBackgroundColor(porterDavisYellowColor);
			//legendTable.addCell(new Paragraph("Lot (Sold)", helvitaNeueMediumPt11Grey));
			
			legendTable.getDefaultCell().setBackgroundColor(porterDavisLifestyleGreen);
			legendTable.addCell(new Paragraph("Lifestyle Suitable", helvitaNeueMediumPt11Grey));
	
			//legendTable.getDefaultCell().setBackgroundColor(porterDavisPrestigeOrange);
			//legendTable.addCell(new Paragraph("Lot (Not released)", helvitaNeueMediumPt11Grey));
			
			legendTable.getDefaultCell().setBackgroundColor(porterDavisPrestigeOrange);
			legendTable.addCell(new Paragraph("Prestige Suitable", helvitaNeueMediumPt11Grey));
			
			headerTable.getDefaultCell().setColspan(5);
			headerTable.getDefaultCell().setBorder(0);
			headerTable.addCell(legendTable);
			*/
			headerTable.getDefaultCell().setColspan(1);
			headerTable.addCell(BLANK); // Column 1st
			headerTable.addCell(BLANK);
			headerTable.addCell(BLANK);
			headerTable.addCell(BLANK);
			headerTable.addCell(BLANK);
			headerTable.addCell(BLANK); // Column 6th
			headerTable.addCell(BLANK);
			headerTable.addCell(BLANK); // column 8th
			headerTable.completeRow();
	
			// Row 04
			headerTable.getDefaultCell().setColspan(fixedColumns);
			headerTable.getDefaultCell().setBackgroundColor(whiteColor);
			headerTable.getDefaultCell().setPaddingBottom(20);
			headerTable.getDefaultCell().setPaddingTop(20);
			headerTable.addCell(BLANK);
			headerTable.getDefaultCell().setPaddingBottom(0);
			headerTable.getDefaultCell().setPaddingTop(0);
			headerTable.completeRow();
			
			// Row 05
			headerTable.getDefaultCell().setColspan(1);
			
			headerTable.getDefaultCell().setBackgroundColor(greyBGColor);
			headerTable.getDefaultCell().setBorder(1);
			//headerTable.getDefaultCell().setBorderWidth(0.1f);
			headerTable.getDefaultCell().setBorderWidthLeft(0.1f);
			headerTable.getDefaultCell().setBorderWidthRight(0.1f);
			//headerTable.getDefaultCell().setBorderWidthTop(0.1f);
			//headerTable.getDefaultCell().setBorderWidthBottom(0.1f);
			headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			headerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			//headerTable.addCell(new Paragraph("", helvitaNeueRomanPt13Grey));
			headerTable.addCell(new Paragraph("Lot No.", helvitaNeueRomanPt13Grey));
			headerTable.addCell(new Paragraph("Price", helvitaNeueRomanPt13Grey));
			headerTable.addCell(new Paragraph("Exc. Hold", helvitaNeueRomanPt13Grey));
			headerTable.addCell(new Paragraph("Lot Width", helvitaNeueRomanPt13Grey));
			headerTable.addCell(new Paragraph("Lot Depth", helvitaNeueRomanPt13Grey));
			headerTable.addCell(new Paragraph("Lot Size", helvitaNeueRomanPt13Grey));
			headerTable.addCell(new Paragraph("Exp. T/O", helvitaNeueRomanPt13Grey));
			headerTable.addCell(new Paragraph("Titled", helvitaNeueRomanPt13Grey));
			headerTable.getDefaultCell().setColspan(1);
			
			/*
			headerTable.getDefaultCell().setBorder(0);
			headerTable.getDefaultCell().setBorderWidth(0f);
			headerTable.getDefaultCell().setBorderWidthLeft(0f);
			headerTable.getDefaultCell().setBorderWidthRight(0f);
			headerTable.getDefaultCell().setBorderWidthTop(0f);
			headerTable.getDefaultCell().setBorderWidthBottom(0f);
			headerTable.getDefaultCell().setBackgroundColor(whiteColor);
			headerTable.addCell(BLANK);
			*/
			
			headerTable.completeRow();
			return headerTable;
	}
	
	
	private PdfPTable getBodyTable(EstateLotInfo e, HashMap<String, ProductLotInfo> lotHash) throws DocumentException 
	{
			int fixedColumns = 8;
			int totalColumns = fixedColumns + rangeBrandList.size();
			
			float fWidths [] = new float [totalColumns];
			int tmpCounter = 0;
			fWidths[tmpCounter++] = 30f; 
			fWidths[tmpCounter++] = 30f;
			fWidths[tmpCounter++] = 40f;
			fWidths[tmpCounter++] = 30f;
			fWidths[tmpCounter++] = 30f;
			fWidths[tmpCounter++] = 30f;
			fWidths[tmpCounter++] = 30f;
			fWidths[tmpCounter++] = 20f;
			for (int r=0; r<rangeBrandList.size(); r++)
			{
				fWidths[tmpCounter++] = 10f;
			}
		
			PdfPCell cell = new PdfPCell();
			PdfPTable bodyTable = new PdfPTable(totalColumns);
			bodyTable.setWidths(fWidths);
	
			bodyTable.getDefaultCell().setPadding(0);
			bodyTable.getDefaultCell().setPaddingBottom(3);
			bodyTable.getDefaultCell().setPaddingTop(3);
			bodyTable.getDefaultCell().setPaddingLeft(0);
			bodyTable.getDefaultCell().setBorder(1);
			bodyTable.getDefaultCell().setBorderWidth(0.1f);
			bodyTable.getDefaultCell().setBorderWidthTop(0.1f);
			bodyTable.getDefaultCell().setBorderWidthBottom(0.1f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			bodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
	
			// Body Row 01
			PdfPTable estateHeadingTable = new PdfPTable(2);
			estateHeadingTable.setWidths(new float[] { 5f, 20f});
	
			estateHeadingTable.getDefaultCell().setPadding(0);
			estateHeadingTable.getDefaultCell().setBorder(0);
			estateHeadingTable.getDefaultCell().setPaddingTop(2);
			estateHeadingTable.getDefaultCell().setPaddingBottom(2);
			estateHeadingTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			estateHeadingTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			estateHeadingTable.getDefaultCell().setBackgroundColor(darkGreyBGColor);
			estateHeadingTable.addCell(new Paragraph("", helvitaNeueMediumPt19Grey));

			String  tmpStr = e.getEstateName();
			/*PdfPCell estHeadName = new PdfPCell();
			estHeadName.setPadding(0);
			estHeadName.setBorder(0);
			estHeadName.setBackgroundColor(darkGreyBGColor);
			estHeadName.addElement(new Chunk(tmpStr, helvitaNeueMediumPt19Grey));*/

			String tmpEstateDetStr = "";
			if (e.getEstateCity()!=null && !"".equalsIgnoreCase(e.getEstateCity()))
			{
				tmpEstateDetStr += " @ " + e.getEstateCity();	
			}	
			
			if (e.getEstateMelRef() != null && !"".equalsIgnoreCase(e.getEstateMelRef()))
			{
				tmpEstateDetStr += " (" + e.getEstateMelRef() + ")";	
			}	
			//estHeadName.addElement(new Chunk(tmpEstateNameStr, helvitaNeueMediumPt15Grey));
			
			
			Paragraph p = new Paragraph(); 
			p.add(new Chunk(tmpStr, helvitaNeueMediumPt19Grey));
			p.add(new Chunk(tmpEstateDetStr, helvitaNeueMediumPt15Grey));
			estateHeadingTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			estateHeadingTable.addCell(p);
			estateHeadingTable.completeRow();
			
			bodyTable.getDefaultCell().setBackgroundColor(darkGreyBGColor);
			bodyTable.getDefaultCell().setColspan(1);
			bodyTable.addCell(BLANK);

			bodyTable.getDefaultCell().setBackgroundColor(darkGreyBGColor);
			bodyTable.getDefaultCell().setColspan((totalColumns-1));
			bodyTable.addCell(estateHeadingTable);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			bodyTable.completeRow();

			// Body Row 02
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			bodyTable.getDefaultCell().setBackgroundColor(skyBlueColor);
			bodyTable.getDefaultCell().setColspan(1);
			bodyTable.getDefaultCell().setPaddingLeft(5);
			tmpStr = "";
			if (e.getEstateDeveloper() !=null && !"".equalsIgnoreCase(e.getEstateDeveloper()))
			{
				tmpStr = "Dev: " + e.getEstateDeveloper();
			}	
			bodyTable.addCell(new Paragraph(tmpStr, helvitaNeueMediumPt9White));

			bodyTable.getDefaultCell().setPaddingLeft(0);
			bodyTable.getDefaultCell().setColspan(2);
			tmpStr = "";
			if (e.getEstateAgent() !=null && !"".equalsIgnoreCase(e.getEstateAgent()))
			{
				tmpStr = "Agent: " + e.getEstateAgent();
			}	
			bodyTable.addCell(new Paragraph(tmpStr, helvitaNeueMediumPt9White));


			bodyTable.getDefaultCell().setColspan(4);
			tmpStr = "";
			if (e.getEstateSellingAgentSalesRepName() != null && !"".equalsIgnoreCase(e.getEstateSellingAgentSalesRepName()))
			{
				tmpStr = e.getEstateSellingAgentSalesRepName(); 
			}

			if (e.getEstateSellingAgentSalesRepPhone() != null && !"".equalsIgnoreCase(e.getEstateSellingAgentSalesRepPhone()))
			{
				tmpStr += "  " + e.getEstateSellingAgentSalesRepPhone(); 
			}
			bodyTable.addCell(new Paragraph(tmpStr, helvitaNeueMediumPt9White));
			

			bodyTable.getDefaultCell().setColspan((totalColumns-7));
			tmpStr = "";
			/*if (e.getEstateLocationName() !=null && !"".equalsIgnoreCase(e.getEstateLocationName()))
			{
				tmpStr = e.getEstateLocationName();
			}*/
			if (e.getEstateLocationContactPersonName() !=null && !"".equalsIgnoreCase(e.getEstateLocationContactPersonName()))
			{
				tmpStr +=  "   " + e.getEstateLocationContactPersonName();
			}
			if (tmpStr!=null && !"".equalsIgnoreCase(tmpStr))
			{	
				bodyTable.addCell(new Paragraph("Updates: " + tmpStr, helvitaNeueMediumPt9White));
			}
			else
			{
				bodyTable.addCell(new Paragraph(" ", helvitaNeueMediumPt9White));
			}	
			bodyTable.completeRow();
			
			
			
			// Body Row 03
			/*bodyTable.getDefaultCell().setColspan(15);
			bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
			bodyTable.getDefaultCell().setPaddingBottom(5);
			//bodyTable.getDefaultCell().setPaddingTop(5);
			bodyTable.addCell(BLANK);
			bodyTable.getDefaultCell().setPaddingBottom(0);
			bodyTable.getDefaultCell().setPaddingTop(0);
			bodyTable.completeRow();
			*/
			
			
			// Body Row 04
			bodyTable.getDefaultCell().setBorderWidthLeft(0.1f);
			bodyTable.getDefaultCell().setBorderWidthRight(0.1f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			if (e.getEstateLotHLInfoList()!=null)
			{
					String previousStage  = "";
					String currentStage = "";
					
					String previousLot  = "";
					String currentLot = "";
					
					int totalCountRange1 = 0;
					int totalCountRange2 = 0;
					int totalCountRange3 = 0;
					int totalCountRange4 = 0;
					int totalCountRange5 = 0;
					int totalCountRange6 = 0;
					int totalCountRange7 = 0;
					int totalCountRange8	= 0;
					int totalCountRange9	= 0;
					int totalCountRange10	= 0;					

					for (int i=0; i< e.getEstateLotHLInfoList().size(); i++)
					{	
							EstateLotHouseAndLandInfo hlInfo = e.getEstateLotHLInfoList().get(i);
							
							if (hlInfo!=null)
							{
									currentStage = hlInfo.getEstateStageName();
									if (!currentStage.equalsIgnoreCase(previousStage))
									{
											// Body Row 05 ==> This is the Stage Heading ROW
											bodyTable.getDefaultCell().setColspan(totalColumns);
											bodyTable.getDefaultCell().setBackgroundColor(greyBGColor);
											bodyTable.addCell(new Paragraph(currentStage, helvitaNeueMediumPt15Grey));
											bodyTable.completeRow();
											
											previousLot  = ""; // Making Previous Lot value as empty
									}
									previousStage = hlInfo.getEstateStageName();

									
									currentLot = hlInfo.getLotNumber();
									if (!currentLot.equalsIgnoreCase(previousLot))
									{
											bodyTable.getDefaultCell().setColspan(1);
											bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
											//bodyTable.addCell(new Paragraph("", helvitaNeueMediumPt11Grey)); // Added RW 19.06
		
											String finalLotKey =  hlInfo.getEstateProductID() + "-" + hlInfo.getEstateEstateName() + "-"  + hlInfo.getEstateStageName() + "-" + hlInfo.getLotNumber();
											ProductLotInfo lotInfo = lotHash.get(finalLotKey);
											if (lotInfo!=null)
											{
													bodyTable.addCell(new Paragraph(lotInfo.getLotNumber(), helvitaNeueMediumPt11Grey));
													bodyTable.addCell(new Paragraph(lotInfo.getLotPrice(), helvitaNeueMediumPt11Grey));
													bodyTable.addCell(new Paragraph(lotInfo.getExclusiveHold(), helvitaNeueMediumPt11Grey));
													bodyTable.addCell(new Paragraph(lotInfo.getLotWidth() + "", helvitaNeueMediumPt11Grey));
													bodyTable.addCell(new Paragraph(lotInfo.getLotDepth() + "", helvitaNeueMediumPt11Grey));
													bodyTable.addCell(new Paragraph(lotInfo.getLotSize() + "", helvitaNeueMediumPt11Grey));
													bodyTable.addCell(new Paragraph(lotInfo.getExpectedTO(), helvitaNeueMediumPt11Grey));
													bodyTable.addCell(new Paragraph(lotInfo.getIsLotTitled(), helvitaNeueMediumPt11Grey));
													//bodyTable.addCell(BLANK);
													
													for (int r=0; r<rangeBrandList.size(); r++)
													{
															RangeBrandInfo rangeInfo = rangeBrandList.get(r);
															BaseColor rgbColor = hex2Rgb(rangeInfo.getRangeColour());
															
															Font tmpFont = null;
															if (rangeInfo.getRangeColour() != null && !"".equalsIgnoreCase(rangeInfo.getRangeColour()) && !"#FFFFFF".equalsIgnoreCase(rangeInfo.getRangeColour()))
															{
																tmpFont = new Font(helvitaNeueMediumPt11White);
															}
															else
															{
																tmpFont = new Font(helvitaNeueMediumPt11Grey);
															}	

															
															if (r==0)
															{	
																	if (lotInfo.getBrandRange1Count() > 0)
																	{	
																		bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
																		bodyTable.addCell(new Paragraph(lotInfo.getBrandRange1Count() + "", tmpFont));
																		
																		totalCountRange1 += lotInfo.getBrandRange1Count();
																	}
																	else
																	{
																		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
																		bodyTable.addCell(BLANK);
																	}
															} 
															else if (r==1)
															{	
																	if (lotInfo.getBrandRange2Count() > 0)
																	{	
																		bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
																		bodyTable.addCell(new Paragraph(lotInfo.getBrandRange2Count() + "", tmpFont));
																		
																		totalCountRange2 += lotInfo.getBrandRange2Count();
																	}
																	else
																	{
																		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
																		bodyTable.addCell(BLANK);
																	}
															}
															else if (r==2)
															{	
																	if (lotInfo.getBrandRange3Count() > 0)
																	{	
																		bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
																		bodyTable.addCell(new Paragraph(lotInfo.getBrandRange3Count() + "", tmpFont));
																		
																		totalCountRange3 += lotInfo.getBrandRange3Count();
																	}
																	else
																	{
																		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
																		bodyTable.addCell(BLANK);
																	}
															}
															else if (r==3)
															{	
																	if (lotInfo.getBrandRange4Count() > 0)
																	{	
																		bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
																		bodyTable.addCell(new Paragraph(lotInfo.getBrandRange4Count() + "", tmpFont));
																		
																		totalCountRange4 += lotInfo.getBrandRange4Count();
																	}
																	else
																	{
																		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
																		bodyTable.addCell(BLANK);
																	}
															}
															else if (r==4)
															{	
																	if (lotInfo.getBrandRange5Count() > 0)
																	{	
																		bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
																		bodyTable.addCell(new Paragraph(lotInfo.getBrandRange5Count() + "", tmpFont));
																		
																		totalCountRange5 += lotInfo.getBrandRange5Count();
																	}
																	else
																	{
																		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
																		bodyTable.addCell(BLANK);
																	}
															}
															else if (r==5)
															{	
																	if (lotInfo.getBrandRange6Count() > 0)
																	{	
																		bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
																		bodyTable.addCell(new Paragraph(lotInfo.getBrandRange6Count() + "", tmpFont));
																		
																		totalCountRange6 += lotInfo.getBrandRange6Count();
																	}
																	else
																	{
																		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
																		bodyTable.addCell(BLANK);
																	}
															}
															else if (r==6)
															{	
																	if (lotInfo.getBrandRange7Count() > 0)
																	{	
																		bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
																		bodyTable.addCell(new Paragraph(lotInfo.getBrandRange7Count() + "", tmpFont));
																		
																		totalCountRange7 += lotInfo.getBrandRange7Count();
																	}
																	else
																	{
																		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
																		bodyTable.addCell(BLANK);
																	}
															}
															else if (r==7)
															{	
																	if (lotInfo.getBrandRange8Count() > 0)
																	{	
																		bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
																		bodyTable.addCell(new Paragraph(lotInfo.getBrandRange8Count() + "", tmpFont));
																		
																		totalCountRange8 += lotInfo.getBrandRange8Count();
																	}
																	else
																	{
																		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
																		bodyTable.addCell(BLANK);
																	}
															}
															else if (r==8)
															{	
																	if (lotInfo.getBrandRange9Count() > 0)
																	{	
																		bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
																		bodyTable.addCell(new Paragraph(lotInfo.getBrandRange9Count() + "", tmpFont));
																		
																		totalCountRange9 += lotInfo.getBrandRange9Count();
																	}
																	else
																	{
																		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
																		bodyTable.addCell(BLANK);
																	}
															}
															else if (r==9)
															{	
																	if (lotInfo.getBrandRange10Count() > 0)
																	{	
																		bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
																		bodyTable.addCell(new Paragraph(lotInfo.getBrandRange10Count() + "", tmpFont));
																		
																		totalCountRange10 += lotInfo.getBrandRange10Count();
																	}
																	else
																	{
																		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
																		bodyTable.addCell(BLANK);
																	}
															}
													}
											}
											else
											{
													bodyTable.addCell(BLANK);
													bodyTable.addCell(BLANK);
													bodyTable.addCell(BLANK);
													bodyTable.addCell(BLANK);
													bodyTable.addCell(BLANK);
													bodyTable.addCell(BLANK);
													bodyTable.addCell(BLANK);
													bodyTable.addCell(BLANK);
													
													for (int r=0; r<rangeBrandList.size(); r++)
													{
															bodyTable.addCell(BLANK); 
													}		
											}
											bodyTable.completeRow();
											previousLot  = hlInfo.getLotNumber();
									}	
									
							}		
					}
					
					// 29-Jan-2013 NI: [Started] Adding the Estate Level Total Row
					bodyTable.getDefaultCell().setColspan(8);
					bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
					bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
					bodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
					bodyTable.addCell(new Paragraph("Total", new Font(helvitaNeueBoldPt12Grey)));
					bodyTable.getDefaultCell().setColspan(1);
					
					for (int r=0; r<rangeBrandList.size(); r++)
					{
						
						RangeBrandInfo rangeInfo = rangeBrandList.get(r);
						BaseColor rgbColor = hex2Rgb(rangeInfo.getRangeColour());
						
						Font tmpFont = null;
						if (rangeInfo.getRangeColour() != null && !"".equalsIgnoreCase(rangeInfo.getRangeColour()) && !"#FFFFFF".equalsIgnoreCase(rangeInfo.getRangeColour()))
						{
							tmpFont = new Font(helvitaNeueMediumPt11White);
						}
						else
						{
							tmpFont = new Font(helvitaNeueMediumPt11Grey);
						}												
						
						///bodyTable.addCell(BLANK);
							
						if (r==0)
						{	
							bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
							bodyTable.addCell(new Paragraph(totalCountRange1 + "", tmpFont));
						} 
						else if (r==1)
						{	
							bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
							bodyTable.addCell(new Paragraph(totalCountRange2 + "", tmpFont));
						}
						else if (r==2)
						{	
							bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
							bodyTable.addCell(new Paragraph(totalCountRange3 + "", tmpFont));
						}
						else if (r==3)
						{	
							bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
							bodyTable.addCell(new Paragraph(totalCountRange4 + "", tmpFont));
						}
						else if (r==4)
						{	
							bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
							bodyTable.addCell(new Paragraph(totalCountRange5 + "", tmpFont));
						}
						else if (r==5)
						{	
							bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
							bodyTable.addCell(new Paragraph(totalCountRange6 + "", tmpFont));
						}
						else if (r==6)
						{	
							bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
							bodyTable.addCell(new Paragraph(totalCountRange7 + "", tmpFont));
						}
						else if (r==7)
						{	
							bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
							bodyTable.addCell(new Paragraph(totalCountRange8 + "", tmpFont));
						}
						else if (r==8)
						{	
							bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
							bodyTable.addCell(new Paragraph(totalCountRange9 + "", tmpFont));
						}
						else if (r==9)
						{	
							bodyTable.getDefaultCell().setBackgroundColor(rgbColor);
							bodyTable.addCell(new Paragraph(totalCountRange10 + "", tmpFont));
						}
					}
					bodyTable.completeRow();
					// 29-Jan-2013 NI: [Ended] Adding the Estate Level Total Row					
			}			
			
			return bodyTable;
	}
	
	public HashMap<String, ProductLotInfo> populateProductLotHashMap(HttpServletRequest request, ArrayList<EstateLotHouseAndLandInfo> hlList)
	{
			HashMap<String, ProductLotInfo> lotHash = new HashMap<String, ProductLotInfo> ();
			
			GenRow lotQuery = new GenRow(); 
			lotQuery.setRequest(request);
			lotQuery.setViewSpec("properties/ProductLotView");
			lotQuery.setParameter("ProductType", "Land");
			lotQuery.setParameter("Active", "Y");
			lotQuery.setAction(GenerationKeys.SEARCH);
			lotQuery.doAction();
			lotQuery.getResults();
			if(!lotQuery.isSuccessful()) { 
				throw new RuntimeException("LOT records not found");
			}
			System.out.println("Product LOTs query is: " + lotQuery.getStatement());
			

			String hlKey = "";
			String lotKey = "";
			
			while (lotQuery.getNext() ) 
			{
					ProductLotInfo  lotInfo = new ProductLotInfo();
					
					lotInfo.setEstateProductID(lotQuery.getString("EstateProductID"));
					lotInfo.setEstateEstateName(lotQuery.getString("EstateName"));
					lotInfo.setEstateStageName(lotQuery.getString("StageName"));
					
					lotInfo.setLotStatus(lotQuery.getString("CurrentStatus"));  // e.g.; New, Sold, Not Released	
					lotInfo.setConsultant(lotQuery.getString("LotConsultant"));  // TODO
					lotInfo.setLotNumber(lotQuery.getString("Name"));
					
					if ("Y".equalsIgnoreCase(lotQuery.getString("IsLotTitled")))
					{
						lotInfo.setIsLotTitled(lotQuery.getString("IsLotTitled"));
					}	
					
			        NumberFormat formatter = new DecimalFormat("###,##0");
			        double totalCost = 	lotQuery.getDouble("TotalCost");
			        String strTotalCost = "$"+formatter.format(totalCost);
					lotInfo.setLotPrice(strTotalCost);
					
					lotInfo.setExclusiveHold(lotQuery.getString("MarketingStatus"));
	
					lotInfo.setLotWidth(lotQuery.getDouble("LotWidth"));
					lotInfo.setLotDepth(lotQuery.getDouble("LotDepth"));
					
			        double totalArea = 	lotQuery.getDouble("Area");
			        String strTotalArea = formatter.format(totalArea);
					lotInfo.setLotSize(strTotalArea); 
					
					String expTO = lotQuery.getString("LotTitleMonth"); 
					if (expTO!=null && expTO.length()>=3)
					{
						expTO = expTO.substring(0, 3);
					}
					expTO += " " + lotQuery.getString("LotTitleYear");
					lotInfo.setExpectedTO(expTO);
					
					lotKey = lotInfo.getEstateProductID() + "-" + lotInfo.getEstateEstateName() + "-" + lotInfo.getEstateStageName() + "-" + lotInfo.getLotNumber();  
					
					/*boolean doLogging = false;
					if ("Highlands MC".equalsIgnoreCase(lotInfo.getEstateEstateName()))
					{
						System.out.println("lotKey is: "  + lotKey);
						doLogging = true;
					}*/	
					
					
					int rangeBrand1Count = 0;
					int rangeBrand2Count = 0;
					int rangeBrand3Count = 0;
					int rangeBrand4Count = 0;
					int rangeBrand5Count = 0;
					int rangeBrand6Count = 0;
					int rangeBrand7Count = 0;
					int rangeBrand8Count = 0;
					int rangeBrand9Count = 0;
					int rangeBrand10Count = 0;

					for (int i=0; i<hlList.size(); i++)
					{
							EstateLotHouseAndLandInfo hlInfo = hlList.get(i);
							if (hlInfo!=null)
							{
									hlKey = hlInfo.getEstateProductID() + "-" + hlInfo.getEstateEstateName() + "-" + hlInfo.getEstateStageName() + "-" + hlInfo.getLotNumber();
									
									/*if (doLogging)
									{
										System.out.println("hlKey is: "  + lotKey  + "                  , hlInfo.getIsAccess: " + hlInfo.getIsAccess() + " , hlInfo.getIsAccessTerrace: " + hlInfo.getIsAccessTerrace() 
												+ " , hlInfo.getIsLifestyle: " + hlInfo.getIsLifestyle() + " , hlInfo.getIsLifestyleTerrace: " + hlInfo.getIsLifestyleTerrace() 
												+ " , hlInfo.getIsPrestigeSingleStorey: " + hlInfo.getIsPrestigeSingleStorey() + " , hlInfo.getIsPrestigeDoubleStorey: " + hlInfo.getIsPrestigeDoubleStorey());
									}*/
									
									
									if (lotKey.equalsIgnoreCase(hlKey))
									{
											if ("Y".equalsIgnoreCase( hlInfo.getIsRangeBrand1()))
											{
												rangeBrand1Count++;
											}		 
											else if ("Y".equalsIgnoreCase( hlInfo.getIsRangeBrand2()))
											{
												rangeBrand2Count++;
											}
											else if ("Y".equalsIgnoreCase( hlInfo.getIsRangeBrand3()))
											{
												rangeBrand3Count++;
											}
											else if ("Y".equalsIgnoreCase( hlInfo.getIsRangeBrand4()))
											{
												rangeBrand4Count++;
											}
											else if ("Y".equalsIgnoreCase( hlInfo.getIsRangeBrand5()))
											{
												rangeBrand5Count++;
											}
											else if ("Y".equalsIgnoreCase( hlInfo.getIsRangeBrand6()))
											{
												rangeBrand6Count++;
											}
											else if ("Y".equalsIgnoreCase( hlInfo.getIsRangeBrand7()))
											{
												rangeBrand7Count++;
											}
											else if ("Y".equalsIgnoreCase( hlInfo.getIsRangeBrand8()))
											{
												rangeBrand8Count++;
											}
											else if ("Y".equalsIgnoreCase( hlInfo.getIsRangeBrand9()))
											{
												rangeBrand9Count++;
											}
											else if ("Y".equalsIgnoreCase( hlInfo.getIsRangeBrand10()))
											{
												rangeBrand10Count++;
											}
									}
							}	
					}	
					
					lotInfo.setBrandRange1Count(rangeBrand1Count);
					lotInfo.setBrandRange2Count(rangeBrand2Count);
					lotInfo.setBrandRange3Count(rangeBrand3Count);
					lotInfo.setBrandRange4Count(rangeBrand4Count);
					lotInfo.setBrandRange5Count(rangeBrand5Count);
					lotInfo.setBrandRange6Count(rangeBrand6Count);
					lotInfo.setBrandRange7Count(rangeBrand7Count);
					lotInfo.setBrandRange8Count(rangeBrand8Count);
					lotInfo.setBrandRange9Count(rangeBrand9Count);
					lotInfo.setBrandRange10Count(rangeBrand10Count);
					
					String finalLotKey =  lotInfo.getEstateProductID() + "-" + lotInfo.getEstateEstateName() + "-"  + lotInfo.getEstateStageName() + "-" + lotInfo.getLotNumber(); 
					lotHash.put(finalLotKey, lotInfo);
			}
			
			return lotHash;
	}
	
	public static BaseColor hex2Rgb(String colorStr) 
	{
			BaseColor rgbColor = new BaseColor(255, 255, 255); // FFFFFF
			//BaseColor rgbColor = new BaseColor(0, 0, 0); // BLACK
			if (colorStr!=null && colorStr.length() >= 7)
			{
				rgbColor = new BaseColor(Integer.valueOf(colorStr.substring( 1, 3 ), 16 ).intValue(), Integer.valueOf( colorStr.substring( 3, 5 ), 16 ).intValue(), Integer.valueOf( colorStr.substring( 5, 7 ), 16).intValue());
			}
			return rgbColor;
	}
	
	public static String getRedfromHex(String colorStr) {
		if (colorStr == null || colorStr.length() < 7)
		{
			return "";
		}	
		return Integer.valueOf(colorStr.substring( 1, 3 ), 16 ).toString();
	}
	
	public static String getGreenfromHex(String colorStr) {
		if (colorStr == null || colorStr.length() < 7)
		{
			return "";
		}	
		return Integer.valueOf( colorStr.substring( 3, 5 ), 16).toString();
	}
	
	public static String getBluefromHex(String colorStr) {
		if (colorStr == null || colorStr.length() < 7)
		{
			return "";
		}	
		return (Integer.valueOf(colorStr.substring( 5, 7 ), 16 )).toString();
	}
	
}