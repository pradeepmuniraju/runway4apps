package com.sok.runway.crm.factories;

import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.sok.runway.crm.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.framework.*;

public class StatusUpdater {
   
   private static StatusUpdater statusUpdater = null;
   
   public static StatusUpdater getStatusUpdater() {
      if (statusUpdater == null) {
         load();
      }
      return statusUpdater;
   }
   
   private static synchronized void load(){
      if (statusUpdater == null) {    
         statusUpdater = new StatusUpdater();
      }
   }
   
   public StatusUpdater() {
      StatusUpdater.statusUpdater = this;
   }
   
   public void insertStatus(StatusHistory statusedEntity) {
      if (statusedEntity.getStatusID() != null && statusedEntity.getStatusID().length() > 0) {
         GenRow statusBean = new GenRow();
         statusBean.setConnection(statusedEntity.getConnection());
         statusBean.setViewSpec(statusedEntity.getStatusHistoryViewName());
         
         statusBean.setParameter(statusBean.getTableSpec().getPrimaryKeyName(),statusedEntity.getStatusHistoryID());
         statusBean.setParameter(statusedEntity.getPrimaryKeyName(),statusedEntity.getPrimaryKey());
         statusBean.setParameter("StatusID",statusedEntity.getStatusID());
         statusBean.setParameter("CreatedDate","NOW");
         statusBean.setParameter("CreatedBy",statusedEntity.getCurrentUser().getPrimaryKey());
         statusBean.doAction(ActionBean.INSERT);
      }
   }   
   
   public boolean isNewStatus(StatusHistory statusedEntity) {
      if (statusedEntity.getStatusID() != null && statusedEntity.getStatusID().length() != 0) {
         GenRow statusBean = new GenRow();
         statusBean.setConnection(statusedEntity.getConnection());
         statusBean.setViewSpec(statusedEntity.getViewSpec());
         statusBean.setColumn(statusedEntity.getPrimaryKeyName(), statusedEntity.getPrimaryKey());
         statusBean.doAction(ActionBean.SELECT);
         
         return !statusBean.getData("CurrentStatusID").equals(statusedEntity.getStatusID());
      }
      return false;
   }
   
   public static String insertGroupStatus(Connection con, String type, String relatedgroupid, String groupid, String statusid, String userid)
   {
          GenRow status = new GenRow();
          status.setConnection(con);
          status.setTableSpec("GroupStatus");
          status.createNewID();
          status.setParameter(type, relatedgroupid); 
          status.setParameter("StatusID", statusid);
          status.setParameter("CreatedDate", "NOW");
          status.setParameter("CreatedBy", userid);
          setAlerts(con, status, statusid, groupid);
          status.doAction(ActionBean.INSERT);
          
          return status.getString("GroupStatusID");
   } 
   
   public static void updateContactQualifiedDetails(Connection con, TableData row, String userid)
   {
       GenRow contactGroups = new GenRow();
       contactGroups.setConnection(con);
       contactGroups.setTableSpec("ContactGroups");
       contactGroups.setParameter("ContactGroupID",row.getString("ContactGroupID"));
       contactGroups.setParameter("-select0","ContactID");
       contactGroups.setAction(ActionBean.SELECT);
       contactGroups.doAction();
      
      GenRow contacUpdate = new GenRow();
      contacUpdate.setConnection(con);
      contacUpdate.setViewSpec("ContactView");
      contacUpdate.setParameter("ContactID",contactGroups.getData("ContactID"));
      contacUpdate.setParameter("QualifiedDate", "NOW");
      contacUpdate.setParameter("QualifiedBy", userid);
      contacUpdate.setAction(ActionBean.UPDATE);
      contacUpdate.doAction();
   }

   public static void setAlerts(Connection con, GenRow status, String statusid, String groupid)
   {
      GenRow alert = getAlert(con, groupid, statusid);
      if(alert.isSuccessful())
      {
         int daysfromdeadline = alert.getInt("DaysFromDeadline");
         if(daysfromdeadline>=0)
         {
            Date deadlinedate = getAlertDate(daysfromdeadline, new Date());
            status.setParameter("DeadlineDate",deadlinedate);
            int daysfromalert = alert.getInt("DaysFromAlert");
            if(daysfromalert>=0)
            {
               status.setParameter("AlertDate",getAlertDate(0 - daysfromalert, deadlinedate));
            } 
         }
      
      }
   }
   
   public static void setAlertsForProcess(Connection con, GenRow status, String alertid)
   {
      GenRow alert = getStatusAlert(con, alertid);
      if(alert.isSuccessful())
      {
         int daysfromdeadline = alert.getInt("DaysFromDeadline");
         if(daysfromdeadline>=0)
         {
            Date deadlinedate = getAlertDate(daysfromdeadline, new Date());
            status.setParameter("DeadlineDate",deadlinedate);
            int daysfromalert = alert.getInt("DaysFromAlert");
            if(daysfromalert>=0)
            {
               status.setParameter("AlertDate",getAlertDate(0 - daysfromalert, deadlinedate));
            } 
         }
      
      }
   }
   
   public static Date getAlertDate(int alertdays, Date date)
   {
     Calendar deadline  = new GregorianCalendar();
     deadline.setTime(date);
     deadline.add(Calendar.DAY_OF_YEAR, alertdays);
     return(deadline.getTime());
   }
   
   public static GenRow getAlert(Connection con, String groupid, String statusid)
   {
     GenRow alertdays = new GenRow();
     alertdays.setConnection(con);
     alertdays.setViewSpec("StatusAlertView");
     alertdays.setParameter("StatusAlertLinkedStatus.LinkedStatusesGroup.GroupID", groupid);
     alertdays.setParameter("StatusAlertLinkedStatus.StatusID", statusid);
     alertdays.doAction("selectfirst");
     return(alertdays);
   }     

   public static GenRow getStatusAlert(Connection con, String alertid)
   {
     GenRow alertdays = new GenRow();
     alertdays.setConnection(con);
     alertdays.setViewSpec("StatusAlertView");
     alertdays.setParameter("StatusAlertID", alertid);
     alertdays.doAction("selectfirst");
     return(alertdays);
   }     
   
}
   
   
      
   
   
   