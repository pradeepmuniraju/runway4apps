package com.sok.runway.crm.cms.properties.pdf.templates.model;

/***
 * @author nawaz
 */

public class UserInfo 
{
		private String userID = "";
		private String firstName = "";
		private String lastName = "";
		private String phoneNumber = "";
		private String mobileNumber = "";
		private String faxNumber = "";
		private String email = "";
		private String locationMudMapPath = "";
		
		public String getLocationMudMapPath() {
			return locationMudMapPath;
		}
		public void setLocationMudMapPath(String locationMudMapPath) {
			this.locationMudMapPath = locationMudMapPath;
		}
		public String getUserID() {
			return userID;
		}
		public void setUserID(String userID) {
			this.userID = userID;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public String getMobileNumber() {
			return mobileNumber;
		}
		public void setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
		}
		public String getFaxNumber() {
			return faxNumber;
		}
		public void setFaxNumber(String faxNumber) {
			this.faxNumber = faxNumber;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}

		public String getFullName() 
		{
			String name = "";
			if (firstName != null && !"".equalsIgnoreCase(firstName))
			{
					name = firstName;
			}	

			if (lastName != null && !"".equalsIgnoreCase(lastName))
			{
					name += " " + lastName;
			}	
			
			return name;
		}

}
