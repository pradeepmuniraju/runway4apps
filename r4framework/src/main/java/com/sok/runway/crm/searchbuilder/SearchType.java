package com.sok.runway.crm.searchbuilder;

import com.sok.runway.crm.profile.ProfileTypes;

public enum SearchType {
   COMPANIES ("Companies", "CompanyDisplay", ProfileTypes.PROFILE_TYPE_COMPANY),
   CONTACTS ("Contacts", "ContactDisplay", ProfileTypes.PROFILE_TYPE_CONTACT);
   
   private final String name;
   private final String displaySpecName;
   private final String profileType;
   
   SearchType(String name, String displaySpecName, String profileType) {
      this.name = name;
      this.displaySpecName = displaySpecName;
      this.profileType = profileType;
   }
   
   /**
    * @return the name of this SearchType
    */
   public String toString() {
      return this.name;
   }

   /**
    * @return the name of this SearchType
    */
   public String getName() {
      return this.name;
   }
   
   /**
    * 
    * @return profile type name
    */
   public String getProfileType() {
       return this.profileType;
   }

   /**
    * @return the display spec name associated with this SearchType
    */
   public String getDisplaySpecName() {
      return this.displaySpecName;
   }
   
   /**
    * 
    * @return the module name (term before the display spec name)
    */
   public String getModuleName() {
       return this.getDisplaySpecName().replaceFirst("Display", "");
   }

   /**
    * 
    * @return the names of all search types as an array of Stings
    */
   public static String[] valueStrings() {
      SearchType[] searchTypes = SearchType.values();
      
      String[] valueStrings = new String[searchTypes.length];
      for(int i=0; i < searchTypes.length; i++) {
         valueStrings[i] = searchTypes[i].toString();
      }
      
      return valueStrings;
   }
   
   /**
    * 
    * @param name of the search type
    * @return a SearchType enum that corresponds to the specified name
    */
   public static SearchType createSearchType(String name) {
      SearchType searchType = null;
      if(name != null) {
         if(name.equalsIgnoreCase(SearchType.COMPANIES.toString())) {
            searchType = SearchType.COMPANIES;
         } else if(name.equalsIgnoreCase(SearchType.CONTACTS.toString())) {
            searchType = SearchType.CONTACTS;
         }
      }
      
      return searchType;
   }
}
