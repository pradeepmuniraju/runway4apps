package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.ApartmentInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ApartmentHeaderFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;

public class ApartmentsListPdf extends AbstractPDFTemplate implements PDFConstants {

	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {

		ArrayList<ApartmentInfo> apartmentsList = BuildingApartmentsPdf.getBuildingsApartmentsList(request, PDFUtil.PDF_DETAILS_VARIOUS_BUILDINGS);

		renderPages(document, apartmentsList);
	}

	@Override
	public void setMargins(Document document) 
	{
		//document.setMargins(15, 30, 10, 0); // L R T B
		document.setMargins(15, 15, 15, 0); // L R T B
	}

	private void renderPages(com.itextpdf.text.Document document, ArrayList<ApartmentInfo> apartmentInfoList) throws DocumentException 
	{
		renderApartmentsPage(document, apartmentInfoList);
	}

	private void renderApartmentsPage(com.itextpdf.text.Document document, ArrayList<ApartmentInfo> apartmentInfoList) throws DocumentException
	{
		PdfPTable pageTable = new PdfPTable(1);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setBorderWidth(0f);
		pageTable.getDefaultCell().setBorderWidthTop(0f);
		pageTable.getDefaultCell().setBorderWidthBottom(0f);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.setWidthPercentage(100f);
		
		if (apartmentInfoList!=null)
		{
				// Adding Header
				ApartmentInfo apartmentInfo = apartmentInfoList.get(0);
				pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				pageTable.addCell(BuildingApartmentsPdf.getHeaderTable(apartmentInfo, getPdfSimpleName())); 
			

				for (int j = 0; j < apartmentInfoList.size(); j++) 
				{
						apartmentInfo = apartmentInfoList.get(j);

						if (j != 0 && j % 5 == 0) 
						{
							document.add(pageTable);
							document.newPage();
							
							pageTable = new PdfPTable(1);
							pageTable.getDefaultCell().setBorder(0);
							pageTable.getDefaultCell().setPadding(0);
							pageTable.getDefaultCell().setFixedHeight(0f);
							pageTable.setWidthPercentage(100f);
							
							// Adding Header
							pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
							pageTable.addCell(BuildingApartmentsPdf.getHeaderTable(apartmentInfo, getPdfSimpleName())); 

						}
		
						pageTable.getDefaultCell().setPadding(0);
						pageTable.getDefaultCell().setPaddingTop(8);
						pageTable.getDefaultCell().setPaddingBottom(8);
						pageTable.getDefaultCell().setBorder(0);
						pageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
						
						 apartmentInfo = apartmentInfoList.get(j);
						if (apartmentInfo!=null)
						{	
							PdfPTable bodyTable = BuildingApartmentsPdf.getBodyTableRow(apartmentInfo, getPdfSimpleName());
							pageTable.addCell(bodyTable);
						}	
				}
		}		
		document.add(pageTable);
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		return new ApartmentHeaderFooter(request, PDFUtil.PDF_DETAILS_VARIOUS_BUILDINGS);
	}
	
	public String getPdfSimpleName() {
		return PDFUtil.PDF_DETAILS_VARIOUS_BUILDINGS;
	}
}