package com.sok.runway.crm.interfaces;

import com.sok.framework.ViewSpec;
import com.sok.runway.crm.*;
import com.sok.runway.security.*;

public interface StatusHistory {
   
   public String getStatusHistoryViewName();  
   
   public String getStatusID();  
   
   public String getStatusHistoryID();  
   public void setStatusHistoryID(String id);  
   
   public ViewSpec getViewSpec();
      
   public String getPrimaryKey();  
   
   public String getPrimaryKeyName();  
   
   public User getCurrentUser();  
   
   public java.sql.Connection getConnection();  
}