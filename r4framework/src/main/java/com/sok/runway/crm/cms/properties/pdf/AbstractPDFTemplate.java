package com.sok.runway.crm.cms.properties.pdf;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.ActionBean;
import com.sok.runway.crm.interfaces.AsyncProcessStatus;

public abstract class AbstractPDFTemplate implements AsyncProcessStatus {

	private static final Logger logger = LoggerFactory.getLogger(AbstractPDFTemplate.class);
	
	public void setStatus(HttpServletRequest request, String status) {
		setStatus(request, status, -1);
	}
	
	public void setStatus(HttpServletRequest request, String status, double fraction) {
		
		String classAndMethodName = this.getClass() + " setStatus(request, status, fraction) ";
		logger.debug(classAndMethodName + " Started.");
		
		logger.debug("setStatus(request, {})", status);
		String statusKey = (String)request.getAttribute("-statusKey");
		if(statusKey == null) {
			logger.debug("no status key from request");
			String fileKey = (String)request.getSession().getAttribute("-statusKey");
			if(fileKey != null) {
				logger.debug("status key from session {} ", fileKey);
				statusKey = fileKey;
			} else {
				logger.error("no status key");
				return;
			}
			request.setAttribute("-statusKey", statusKey);
		}
		logger.debug("using status key {}", statusKey);
		request.getSession().setAttribute(statusKey, makeStatusDiv(status, fraction));
		
		logger.debug(classAndMethodName + " Ended.");
	}
	
	private String makeStatusDiv(String status, double fraction) {
		
		String classAndMethodName = this.getClass() + " makeStatusDiv(status, fraction) ";
		logger.debug(classAndMethodName + " Started.");
		
		if(fraction == -1) {
			logger.debug(classAndMethodName + " Ended. fraction is -1 ");
			return status;
		}
		
		logger.debug(classAndMethodName + " Ended.");
		
		return new StringBuilder().append(status)
				.append("<div style=\"width:100%; display:block; height: 15px\"><div style=\"width: ").append(fraction * 100)
				.append("%; background-color: red\">&nbsp;</div></div>").toString();
	}
	
	public void generatePDF(HttpServletRequest request, HttpServletResponse response) throws IOException, DocumentException {
		
		String classAndMethodName = this.getClass() + " generatePDF(HttpServletRequest request, HttpServletResponse response) ";
		logger.debug(classAndMethodName + " Started.");
		
		Document document = null;
		setStatus(request, "Loading the generator", 0.01d);
		
		// need to grab the outputstream before doing anything else.
		java.io.OutputStream os = response.getOutputStream();
		// now we can set the content type
		response.setContentType("application/pdf");
		try {
			
			document = new com.itextpdf.text.Document();
			if(StringUtils.isNotBlank(request.getParameter("landscape")) && "true".equalsIgnoreCase(request.getParameter("landscape"))) {
				document.setPageSize(PageSize.A4.rotate());
			}
			setMargins(document);
			document.setMarginMirroring(true);
			PdfWriter writer = PdfWriter.getInstance(document, os);
			renderPDF(request, response, document, writer);
			logger.debug("after renderPDF");

		} catch (Exception e) {
			logger.error("Error producing PDF", e);
			try {
				ActionBean.writeStackTrace(e, System.out);
			} catch (Exception e3) {
				System.out.println(ActionBean.writeStackTraceToString(e3));
				System.out.println(ActionBean.writeStackTraceToString(e));
			}

			try {
				if (document == null) {
					document = new com.itextpdf.text.Document();
					document.setMargins(0, 0, 0, 0);
					document.setMarginMirroring(true);
					PdfWriter writer = PdfWriter.getInstance(document, os);
					document.open();
				}
				logger.debug("adding error paragraph");
				Paragraph error = new Paragraph();
				error.add(new Chunk(ActionBean.writeStackTraceToString(e), FontFactory.getFont(FontFactory.HELVETICA, 8)));
				document.add(error);
			} catch (Exception ex) {
				System.out.println(ActionBean.writeStackTraceToString(ex));
			}
		} finally {
			if (document != null && document.isOpen()) {
				document.close();
			}
		}
		//setStatus(request, "Complete", 1);
		
		logger.debug(classAndMethodName + " Ended.");
	}

	public void setMargins(Document document) {
		document.setMargins(35, 30, 0, 100);
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		return null;
	}
	
	public String getPdfSimpleName() {
		return "SimpleName";
	}
	
	public String getDripCount(){
		return "0";
	}
	
	/**
	 * Default implementation adds the footer to each page before calling renderPDF
	 */
	public void renderPDF(final HttpServletRequest request, final HttpServletResponse response, final Document document, final PdfWriter writer) throws IOException, DocumentException
	{
		String classAndMethodName = this.getClass() + " renderPDF() ";
		logger.debug(classAndMethodName + " Started.");

		if (getFooter(request) != null)
			writer.setPageEvent(getFooter(request));

		logger.debug(classAndMethodName + " Opening Document.");
		document.open();
		
		logger.debug(classAndMethodName + " Calling renderPDF(request, response, document).");
		renderPDF(request, response, document);
		
		logger.debug(classAndMethodName + " Ended.");
	}

	/**
	 * To be overridden for default page event behavior
	 */
	public abstract void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException;

}