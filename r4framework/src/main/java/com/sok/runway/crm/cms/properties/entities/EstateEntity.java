package com.sok.runway.crm.cms.properties.entities;

import java.sql.Connection;
import java.util.Collections;

import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.Product;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.cms.properties.Estate;

public class EstateEntity extends Product {
	private static final Logger logger = LoggerFactory.getLogger(EstateEntity.class);
	static final String VIEWSPEC = "properties/ProductEstateView";
	public static final String ENTITY_NAME = "Estate";
	
	public EstateEntity() {
		super(VIEWSPEC, (Connection)null);
	}
	public EstateEntity(Connection con) {
		super(VIEWSPEC, con);
	}
	public EstateEntity(ServletRequest request) {
		super(VIEWSPEC, request);
	}
	public EstateEntity(Connection con, String estateID) {
		super(VIEWSPEC, con);
		loadFinal(estateID);
	}
	public EstateEntity(Connection con, Estate e) {
		super(VIEWSPEC, con);
		
		if(StringUtils.isNotBlank(e.getEstateID()))
			loadFinal(e.getEstateID());
		
		setField("Name", e.getName());
		setField("Description", e.getDescription());
		setField("ProductNum", e.getProductNum());
		setField("URL", e.getURL());
		BeanFactory.setTableDataFromAddress(e.getAddress(), getRecord(), "");
		setField("ThumbnailImage", e.getThumbnailImage() != null ? e.getThumbnailImage().getDocumentID() : null);
	}
	
	@Override
	public String getEntityName() {
		return ENTITY_NAME;
	}
}
