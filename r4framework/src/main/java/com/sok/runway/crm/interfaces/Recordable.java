package com.sok.runway.crm.interfaces;

public interface Recordable extends Linkable {
	
	public String getRecordURL(int channel);
	
	public String getRecordURL(int channel, String custom);
	
}
