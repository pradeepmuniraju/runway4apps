package com.sok.runway.crm.util;

import java.io.File;
import java.util.zip.ZipEntry;

import javax.servlet.http.HttpServletRequest;
import com.sok.runway.crm.Document; 
import com.sok.runway.crm.cms.MasterTemplate;
import com.sok.runway.crm.cms.Template;
import com.sok.runway.crm.interfaces.DocumentLinkable;

/**
 * Document Uploader, for uploading bulk images / other documnents into a specified directory
 * @author mikey
 *
 */
public class DocumentUploader extends Uploader {

	private String appPath; 
	private Document d = null; 
	private DocumentLinkable dl = null; 
	private HttpServletRequest request; 
	
	public void upload(File f, HttpServletRequest request, String directory) { 
		this.request = request; 
		
		appPath = request!=null?request.getSession().getServletContext().getRealPath(""):"";
		doRun(f,appPath,directory); 
	}
	protected void checkFile(ZipEntry ze, File of) {
		String fullPath = of.getAbsolutePath(); 
		String filePath = fullPath.substring(appPath.length()); 
		
		if(request != null) {
			
			if(d == null) { 
				d = new Document(request); 
			} else { 
				d.clear(); 
				d.populate(request.getParameterMap()); 
			}
			
			if(dl == null) { 
				if(request.getParameter("TemplateID") != null) { 
					 dl = new Template(request); 
				} else if (request.getParameter("MasterTemplateID") != null) {
					 dl = new MasterTemplate(request); 
				} 
			} 
		
			d.setField("FilePath", filePath); 
			d.setField("FileName", of.getName()); 
			d.setField("FileSize", ze.getSize()); 
			
			/*
			 * GroupID, ContentType, ContentSubType, ProductGroupID could be passed thru via request 
			 */
			
			d.insert(); 
			
			if(dl != null && dl.isLoaded()) { 	
				dl.linkDocument(d.getPrimaryKey());
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DocumentUploader dl = new DocumentUploader();
		File f = new File("/home/mikey/Desktop/bsedm/bsedm.zip"); 
		if(!f.exists()) { 
			throw new IllegalArgumentException("file does not exist"); 
		}
		dl.upload(f, (HttpServletRequest)null, "/home/mikey/Desktop/bsedm/unzip"); 
		System.out.println("done"); 
		System.out.println(dl.getErrors());
	}

}
