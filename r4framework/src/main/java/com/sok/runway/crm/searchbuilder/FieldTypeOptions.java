package com.sok.runway.crm.searchbuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.DisplaySpec;
import com.sok.runway.dao.GroupDAO;
import com.sok.runway.dao.ProfileDAO;
import com.sok.runway.model.Group;
import com.sok.runway.model.Profile;

public class FieldTypeOptions {
    private HttpServletRequest request;
    private SearchCriteria searchCriteria;
    
    public FieldTypeOptions(HttpServletRequest request, SearchCriteria searchCriteria) {
        this.request = request;
        this.searchCriteria = searchCriteria;
    }
    
    public Option[] getModuleOptions() {
       Option[] moduleOptions = null;
       
       DisplaySpec displaySpec = this.searchCriteria.getDisplaySpec();
       String[] relatedDisplayNames = displaySpec.getRelatedDisplayNames();
       int moduleOptionsLength = relatedDisplayNames.length + 1;   // Contacts/Company module plus all linked modules
       moduleOptions = new Option[moduleOptionsLength];
       
       String fieldTypeValue = this.searchCriteria.getFieldTypeValue();
       
       /* Add Company/Contacts module */
       String module = this.searchCriteria.getSearchType().getModuleName();
       Option moduleOption = new Option();
       moduleOption.setValue("Module-" + module);
       moduleOption.setText(module);
       moduleOption.setTitle(module);
       if(fieldTypeValue.equalsIgnoreCase(module)) {
           moduleOption.setSelected(true);
       }
       
       int index = 0;
       moduleOptions[index] = moduleOption;
       
       /* Add linked modules */
       for(String relatedDisplayName : relatedDisplayNames) {
          index++;
         
          module = displaySpec.getRelatedDisplayLabel(relatedDisplayName);

          moduleOption = new Option();
          moduleOption.setValue("Module-" + relatedDisplayName);
          moduleOption.setText(module);
          if(fieldTypeValue.equalsIgnoreCase(module)) {
              moduleOption.setSelected(true);
          }

          moduleOptions[index] = moduleOption;
       }
       
       return moduleOptions;
    }
    
    public Option[] getFieldProfileOptions(String userID) {
       GroupDAO groupDao = new GroupDAO(request);
       List<Group> groups = groupDao.findAllByUserID(userID);
       
       ProfileDAO profileDao = new ProfileDAO();
       SearchType searchType = this.searchCriteria.getSearchType();
       Set<Profile> profiles = new HashSet<Profile>();
       
       for(Group group : groups) {
           List<Profile> groupProfiles = profileDao.findAllByGroupIDAndProfileType(
                   request, group.getId(), searchType.getProfileType());
           profiles.addAll(groupProfiles);
       }
       
       Option[] options = new Option[profiles.size()];
       int index = 0;
       for(Profile profile : profiles) {
           Option option = new Option();
           option.setValue("Profile-" + profile.getId());
           option.setText(profile.getName());
           option.setTitle(profile.getName());
           if(this.searchCriteria.getFieldTypeValue().equals(profile.getId())) {
               option.setSelected(true);
           }
           options[index] = option;
           
           index++;
       }
       
       return options;
    }
}
