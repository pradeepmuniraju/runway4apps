package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;

public class HomePlan extends PropertyEntity {
	private static final Logger logger = LoggerFactory.getLogger(HomePlan.class);
	
	final PropertyType propertyType = PropertyType.HomePlan;
	static final String VIEWSPEC = "properties/ProductPlanView";
	private Home home = null;
	private Facade facade = null;

	public HomePlan(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE, getProductType());
	}

	public HomePlan(HttpServletRequest request, String productID) {
		setViewSpec(VIEWSPEC);
		setRequest(request);
		setParameter(PRODUCTTYPE, getProductType());
		load(productID);
	}

	public HomePlan(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		setParameter(PRODUCTTYPE, getProductType());
		load(productID);
	}

	public HomePlan load(String productID) {
		return (HomePlan) super.load(productID);
	}

	public List<String> getFeatureList() {
		HomePlanFeature hpf = getFeatures();
		List<String> features = Collections.emptyList();
		if (hpf.getNext()) {
			features = new ArrayList<String>();
			do {
				features.add(hpf.getData("Name"));
			} while (hpf.getNext());
		}
		hpf.close();
		return features;
	}

	@Override
	public String[] getDripProductIDs() {
		return new String[]{getString("RangeProductID"), getString("HomeProductID"), getString(PRODUCTID)};
	}	
	
	public String getHomeProductId(){
		return getString("HomeProductID");
	}
	
	public String getName() {
		return isSuccessful() ? new StringBuilder(getColumn("HomeName")).append(" ").append(getColumn("Name")).toString() : ActionBean._blank;
	}
	
	public String getHomeName() {
		return isSuccessful() ? getString("HomeName"): ActionBean._blank;
	}
	
	public String getPlanName() {
		return isSuccessful() ? getString("Name"): ActionBean._blank;
	}

	public Facade getFacade() {
		if(facade == null && isSuccessful() && isSet(PRODUCTID)) {
			facade = new Facade(getConnection()).load(getData(PRODUCTID));
			if(!facade.isSuccessful()) {
				facade = null;
			}
		}
		return facade;
	}
	
	public String getFacadeName() {
		if(getFacade() != null) {
			logger.trace("hnl have facade {}", getData(PRODUCTID));
			return getFacade().getString("Name");
		} else {
			logger.warn("hnl no facade found {}", getData(PRODUCTID));
		}
		return null;
	}
	
	public String getFacadeDescription() {
		if(getFacade() != null) {
			logger.trace("hnl have facade {}", getData(PRODUCTID));
			return getFacade().getString("Description");
		} else {
			logger.warn("hnl no facade found {}", getData(PRODUCTID));
		}
		return null;
	}
	
	
	/**
	 * Returns a HomePlanFeature (GenRow) as an iterable list. You *MUST* close this object when you
	 * are finished with it.
	 * 
	 * @return
	 */
	public HomePlanFeature getFeatures() {
		final HomePlanFeature features = new HomePlanFeature(getConnection());
		if (isSuccessful() && StringUtils.isNotBlank(getData(PRODUCTID))) {
			features.setParameter("ProductsLinkedProductProduct.ProductID", getData(PRODUCTID));
			features.setParameter("Active", "Y");
			features.setParameter("-sort1", "Name");
			features.setAction(GenerationKeys.SEARCH);
			features.getResults();
		}
		return features;
	}

	@Override
	public Object get(String key) {
		if ("homeproduct".equals(key))
			return getHome();
		return super.get(key);
	}

	public Home getHome() {
		if (home == null && isSuccessful()) {
			home = new Home(getConnection()).load(getData("HomeProductID"));
			if (!home.isSuccessful()) {
				home = null;
			}
		}
		return home;
	}

	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}

	public HomeRange getHomeRange() {
		if (getHome() != null) {
			return home.getHomeRange();
		}
		return null;
	}

	public String getPlanImage() {
		return getString("ImagePath");
	}

	public String getHeroImage() {
		if (getHome() != null) {
			return home.getString("ImagePath");
		}
		return null;
	}

	public String getRangeName() {
		if (getHomeRange() != null) {
			return getHomeRange().getString("Name");
		} else {
			logger.warn("no range found {}", getData(PRODUCTID));
		}
		return null;
	}

	public String getCost() { 
		String cost = getColumn("TotalCost");
		if(cost.indexOf(".")>0) { 
			return cost.substring(0, cost.indexOf("."));
		}
		return cost;
	}
	
	public String getDripTotalCost() { 
		String cost = getColumn("DripResult");
		// if the price is $0.00 then use the TotalCost
		if (cost.replaceAll("[0\\.\\$]","").length() == 0) cost = getColumn("TotalCost");
		if(cost.indexOf(".")>0) { 
			return cost.substring(0, cost.indexOf("."));
		}
		return cost;
	}

	public int getBedrooms() {
		return getInt("Bedrooms");
	}

	public int getBathrooms() {
		return getInt("Bathrooms");
	}

	public int getCarParks() {
		return getInt("CarParks");
	}

	public int getStudy() {
		if (StringUtils.isNotEmpty(getString("Study")))
			return new Double(getDouble("Study")).intValue();
		return -1;
	}
	
	public String getStorey() {
		return getString("Storey");
	}

	public GenRow getDisplay() {
		return getDisplay(null);
	}
	
	public String getHomeSizeSq(){
		return getString("HomeSizeSq");
	}

	public GenRow getDisplay(String estateID) {
		if (isSuccessful() && isSet(PRODUCTID)) {
			GenRow products = new GenRow();
			products.setConnection(getConnection());
			products.setViewSpec("properties/ProductHouseAndLandView");
			products.setParameter("ProductProductProducts#House.ProductProductsLinkedProductHouse.ProductParentHomeDesign.ProductVariationHomeDesign.ProductID", getString(PRODUCTID));
			if (StringUtils.isNotBlank(estateID)) {
				products.setParameter("ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ProductID", estateID);
			}
			products.setParameter("DisplayFlag", "Y");
			products.doAction(GenerationKeys.SELECTFIRST);
			if (products.isSuccessful()) {
				return products;
			}
		}
		return null;
	}

	/**
	 * If you use this you must close the facades! 
	 * @return
	 */
	public Facade getFacades(boolean showInactive) {
		Facade facade = new Facade(getConnection());
		if(isSuccessful() && isSet(PRODUCTID)) {
			facade.setParameter(PRODUCTID, getData(PRODUCTID));
			if(!showInactive) {
				facade.setParameter("Active", "Y");
				facade.setParameter("ProductProductsLinkedProduct.Active", "Y");
			}
			facade.doAction(GenerationKeys.SEARCH);
			facade.getResults(true);
		}
		return facade;
	}
	public Facade getFacades() {
		return getFacades(false);
	}
	
}
