package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import javax.servlet.ServletRequest;
import java.sql.*;

public class OpportunityStage extends ProfiledEntity {
	
	private static final String ANSWER_VIEWSPEC = "answers/OpportunityStageAnswerView";

	public OpportunityStage(Connection con) {
		super(con, "OpportunityStageStageView");
	}

	public OpportunityStage(ServletRequest request) {
		 super(request, "OpportunityStageStageView");
		 populateFromRequest(request.getParameterMap());
	}

	public OpportunityStage(Connection con, String opportunityStageID) {
		this(con);
		load(opportunityStageID);
	}

	public String getAnswerViewSpec() {
		return ANSWER_VIEWSPEC;
	}

	public Validator getValidator() {
		Validator val = Validator.getValidator("OpportunityStage");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("OpportunityStageID","SYSTEM ERROR - OpportunityStageID");
			val.addMandatoryField("OpportunityID","Opportunity Stage must have an opportunity");
			Validator.addValidator("OpportunityStage", val);
		}
		return val;
	}
}