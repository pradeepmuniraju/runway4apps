package com.sok.runway.crm;

import java.util.ArrayList;
import java.util.Collection;

public class PublisherList {

	public static final String REALESTATE = "realestate.com.au";
	public static final String MYPACKAGE = "mypackage.com.au";
	public static final String HOUSEANDLAND = "houseandland.com.au";
	public static final String DOMAIN = "domain.com.au";
	public static final String WEBSITE = "Website";
	public static final String HAL = "House and Land Packages";
	public static final String HS = "homesales.com.au";
	public static final String TM = "trademe.co.nz";
	public static final String REIWA = "reiwa.com.au";
	public static final String MEHOUSE = "mehouse.com.au";
	public static final String AH = "allhomes.com.au";
	public static final String SENIORSHOUSINGONLINE = "Seniors Housing Online";
	public static final String NEWHOMESGUIDE = "newhomesguide.com.au"; 
	public static final String PORTAL = "Runway Portal";
	public static final String ALL = "All";
	
	private static final String REQUEST_DELIM = "+";
	
	private static final Collection<String> publisherList;
	
	private PublisherList() {
		
	}
	
	static {
		publisherList = new ArrayList<String>();
		publisherList.add(HAL);
		publisherList.add(REALESTATE);
		publisherList.add(MYPACKAGE);
		publisherList.add(HOUSEANDLAND);
		publisherList.add(DOMAIN);
		publisherList.add(HS);
		publisherList.add(TM);
		publisherList.add(AH);
		publisherList.add(SENIORSHOUSINGONLINE);
		publisherList.add(REIWA);
		publisherList.add(NEWHOMESGUIDE);
		publisherList.add(WEBSITE);
		publisherList.add(PORTAL);
		publisherList.add(ALL);
	}
	
	public static  Collection<String> getPublisherList() {
		return publisherList;
	}
	
	public static String toRequestALLString() {
		StringBuilder buf = new StringBuilder(REQUEST_DELIM);
		buf.append(HAL);
		buf.append(REQUEST_DELIM);
		buf.append(REALESTATE);
		buf.append(REQUEST_DELIM);
		buf.append(MYPACKAGE);
		buf.append(REQUEST_DELIM);
		buf.append(HOUSEANDLAND);
		buf.append(REQUEST_DELIM);
		buf.append(DOMAIN);
		buf.append(REQUEST_DELIM);
		buf.append(HS);
		buf.append(REQUEST_DELIM);
		buf.append(TM);
		buf.append(REQUEST_DELIM);
		buf.append(AH);
		buf.append(REQUEST_DELIM);
		buf.append(SENIORSHOUSINGONLINE);
		buf.append(REQUEST_DELIM);
		buf.append(NEWHOMESGUIDE);
		buf.append(REQUEST_DELIM);
		buf.append(WEBSITE);
		buf.append(REQUEST_DELIM);
		buf.append(PORTAL);
		
		return buf.toString();
	}
}
