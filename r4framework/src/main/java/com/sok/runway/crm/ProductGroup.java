package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.security.*;
import com.sok.framework.*;

import com.sok.runway.crm.activity.*;
import javax.servlet.ServletRequest;
import java.sql.*;
import java.util.*;

public class ProductGroup extends ProfiledEntity implements Logable {

	private static final String ENTITY_NAME = "ProductGroup";
	private static final String ANSWER_VIEWSPEC = "answers/ProductGroupAnswerView";
	
	/** 
    * Instantiate without loading specific Product Group.
    * @param con
    */
   public ProductGroup(Connection con) {
      super(con, "ProductGroupView");
   }
	/**
	 * Instantiate and load ProductGroupID contained within HTTP Request parameter.
	 * @param request
	 */
	public ProductGroup(ServletRequest request) {
	    super(request, "ProductGroupView");
       populateFromRequest(request.getParameterMap());
	}
	
	/** 
	 * Instantiate and load specified ProductGroupID
	 * @param con
	 * @param productGroupID
	 */
   public ProductGroup(Connection con, String productGroupID) {
      this(con);
      load(productGroupID);
   }
   
   /**
    * Retrieves entity name for this object as defined in the class.
    */
   public String getEntityName() {
	   return ENTITY_NAME;
   }
   
   /**
    * Retrieves the Validator object for this class.
    * Required details for ProductGroup: ProductGroupID, GroupName.
    */
   public Validator getValidator() {
      Validator val = Validator.getValidator("ProductGroup");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("ProductGroupID","SYSTEM ERROR - ProductGroupID");
         val.addMandatoryField("GroupName","Group Name must not be blank.");

         Validator.addValidator("ProductGroup", val);
      }
      return val;
   }
   
	public String getAnswerViewSpec() {
		return ANSWER_VIEWSPEC; 
	}
}