package com.sok.runway.crm.cms.properties.entities;

import java.sql.Connection;

import javax.servlet.ServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.Product;
import com.sok.runway.externalInterface.BeanFactory;
import com.sok.runway.externalInterface.beans.cms.properties.Estate;
import com.sok.runway.externalInterface.beans.cms.properties.HomeDetails;
import com.sok.runway.externalInterface.beans.cms.properties.Lot;
import com.sok.runway.externalInterface.beans.cms.properties.LegacyLot;
import com.sok.runway.externalInterface.beans.cms.properties.LotStatusHistory;
import com.sok.runway.externalInterface.beans.cms.properties.SimpleEstate;
import com.sok.runway.externalInterface.beans.cms.properties.SimpleStage;
import com.sok.runway.externalInterface.beans.cms.properties.Stage;
import com.sok.runway.externalInterface.beans.shared.Address;

@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement(name="Lot")
public class LotEntity extends Product {
	
	private static final Logger logger = LoggerFactory.getLogger(LotEntity.class);
	static final String VIEWSPEC = "properties/ProductLotView";
	public static final String ENTITY_NAME = "Lot";
	
	public LotEntity() {
		super(VIEWSPEC, (Connection)null);
	}
	public LotEntity(Connection con) {
		super(VIEWSPEC, con);
	}
	public LotEntity(ServletRequest request) {
		super(VIEWSPEC, request);
	}
	public LotEntity(Connection con, String lotID) {
		super(VIEWSPEC, con);
		loadFinal(lotID);
	}
	
	public LotEntity(Connection con, LegacyLot l) {
		super(VIEWSPEC, con);
		
		logger.debug(((Object)this).toString());
		if(StringUtils.isNotBlank(l.getProductID())) {
			load(l.getProductID());
			logger.debug("Load Lot from ID={} isLoaded={}", l.getProductID(), this.isLoaded());
		} else if(StringUtils.isNotBlank(l.getName()) && StringUtils.isNotBlank(l.getStageProductID())) {
			loadFromField(new String[]{"Name", "ProductParentStageLot.ProductVariationStageLot.ProductID"}, new String[]{l.getName(),l.getStageProductID()});
			logger.debug("Load Lot from Name={} and StageProductID={} isLoaded={}", new String[]{l.getName(), l.getStageProductID(), String.valueOf(isLoaded())});
		}
		
		if(StringUtils.isNotBlank(l.getName()))
			setField("Name", l.getName());
		if(StringUtils.isNotBlank(l.getProductNum()))
			setField("ProductNum", l.getProductNum());
		
		if(StringUtils.isNotBlank(l.getEstateProductID()))
			setField("EstateProductID", l.getEstateProductID());
		if(StringUtils.isNotBlank(l.getEstateName()))
			setField("EstateName", l.getEstateName());
		
		if(StringUtils.isNotBlank(l.getStageProductID()))
			setField("StageProductID", l.getStageProductID());
		else 
			logger.warn("no stageproductid from legacy lot");
		
		logger.debug("Set StageProductID to {}", getField("StageProductID"));
		
		if(StringUtils.isNotBlank(l.getStageName()))
			setField("StageName", l.getStageName());
		
		if(StringUtils.isNotBlank(l.getRegionID()))
			setField("RegionID", l.getRegionID());
		if(StringUtils.isNotBlank(l.getLocationID()))
			setField("LocationID", l.getLocationID());
		
		BeanFactory.setTableDataFromAddress(l.getAddress(), getRecord(), "");
		setDetails(l.getDetails());
		setField("Cost", l.getCost());
		setField("GST", l.getGST());
		setField("TotalCost", l.getTotalCost());
		
		if(StringUtils.isNotBlank(l.getTitleMonth()))
			setField("TitleMonth", l.getTitleMonth());
		if(StringUtils.isNotBlank(l.getTitleYear()))
			setField("TitleYear", l.getTitleYear());

		if(StringUtils.isNotBlank(l.getStatusID()))
			setStatusID(l.getStatusID());
		if(StringUtils.isNotBlank(l.getSubType()))
			setField("ProductSubType", l.getSubType());
	}
	
	public LotEntity(Connection con, Lot l) {
		super(VIEWSPEC, con);
		
		if(StringUtils.isNotBlank(l.getLotID()))
			load(l.getLotID());
		
		SimpleStage ss = l.getStage();
		SimpleEstate se = l.getStage() != null ? l.getStage().getEstate() : null;
		
		if(StringUtils.isNotBlank(l.getName()) && ss != null && StringUtils.isNotBlank(ss.getStageID()))
			loadFromField(new String[]{"Name", "ProductParentStageLot.ProductVariationStageLot.ProductID"}, new String[]{l.getName(), ss.getStageID()});
		
		if(StringUtils.isNotBlank(l.getName()))
			setField("Name", l.getName());
		if(StringUtils.isNotBlank(l.getProductNum()))
			setField("ProductNum", l.getProductNum());
		
		if(se != null) { 
			if(StringUtils.isNotBlank(se.getEstateID()))
				setField("EstateProductID", se.getEstateID());
			if(StringUtils.isNotBlank(se.getName()))
				setField("EstateName", se.getName());
		}
		if(ss != null) { 
			if(StringUtils.isNotBlank(ss.getStageID()))
				setField("StageProductID", ss.getStageID());
			if(StringUtils.isNotBlank(ss.getName()))
				setField("StageName", ss.getName());
		}
		
		if(StringUtils.isNotBlank(l.getRegionID()))
			setField("RegionID", l.getRegionID());
		if(StringUtils.isNotBlank(l.getLocationID()))
			setField("LocationID", l.getLocationID());
		
		BeanFactory.setTableDataFromAddress(l.getAddress(), getRecord(), "");
		setField("Cost", l.getCost());
		setField("GST", l.getGST());
		setField("TotalCost", l.getTotalCost());
		
		if(StringUtils.isNotBlank(l.getTitleMonth()))
			setField("TitleMonth", l.getTitleMonth());
		if(StringUtils.isNotBlank(l.getTitleYear()))
			setField("TitleYear", l.getTitleYear());
		//setStatus(l.getStatus());
		
		if(l.getCurrentStatus() != null) { 
			LotStatusHistory s = l.getCurrentStatus();
			if(StringUtils.isNotBlank(s.getStatusID()))
				setStatusID(s.getStatusID());
		}

		if(StringUtils.isNotBlank(l.getSubType()))
			setField("ProductSubType", l.getSubType());
	}
	
	@Override
	public String getEntityName() {
		return ENTITY_NAME;
	}

	@Override
	public void beforeInsert() {
		logger.debug("beforeInsert()");
		//the below requires this, and beforeInsert doesn't set it.
		setNewPrimaryKey();
		super.beforeInsert();
		logger.debug("set ProudctNum");
		if(getField("ProductNum").length()==0) {
			setField("ProductNum", KeyMaker.getRandomNumberString(6));
		}
		if(getField("StageProductID").length()==0) {
			logger.debug(((Object)this).toString());
			throw new RuntimeException("StageProductID is required for Lot creation");
		}
		logger.debug("insertProductVariation(con, {}, {}, Stage)", getField("StageProductID"), getProductID());
		insertProductVariation(getConnection(), getField("StageProductID"), getProductID(), "Stage");
		logger.debug("copy(con, ProductSecrurityGroups, ProductID, {}, {}, user", getField("StageProductID"), getProductID());
		copy(getConnection(), "ProductSecurityGroups", "ProductID", getField("StageProductID"), getProductID(), getCurrentUser().getUserID());
		logger.debug("endBeforeInsert");
	}
	

	@JsonProperty("Details")
	public void setDetails(HomeDetails details) {
		setField("Bedrooms",details.Bedrooms);
		setField("Bathrooms",details.Bathrooms);
		setField("HomeSize",details.HomeSize);
		setField("HomeSizeSq",details.HomeSizeSq);
		setField("LotWidth",details.LotWidth);
		setField("LotDepth",details.LotDepth);
		setField("Garage",details.Garage);
		setField("Study",details.Study);
		setField("Dimensions",details.Dimensions);
		setField("Area",details.Area);
		setField("CarParks",details.CarParks);
		setField("CanFitOnDepth",details.CanFitOnDepth);
		setField("CanFitOnFrontage",details.CanFitOnFrontage);
		setField("WillFitHouseSize",details.WillFitHouseSize);
		setField("LotTitleMonth",details.LotTitleMonth);
		setField("LotTitleYear",details.LotTitleYear);
		setField("IsLotTitled",details.IsLotTitled);
		setField("FitsArea",details.FitsArea);
		setField("FitsFrontage",details.FitsFrontage);
		setField("FitsDepth",details.FitsDepth);
		setField("FitsBuildWidth",details.FitsBuildWidth);
		setField("FitsBuildDepth",details.FitsBuildDepth);
		setField("Perimeter",details.Perimeter);
		setField("Storey",details.Storey);
		setField("GroundFloorArea",details.GroundFloorArea);
		setField("FirstFloorWindows",details.FirstFloorWindows);
		setField("EavesFacade",details.EavesFacade);
		setField("EavesGround",details.EavesGround);
		setField("EavesFirst",details.EavesFirst);
		setField("LeftSetback",details.LeftSetback);
		setField("RightSetback",details.RightSetback);
		setField("FrontSetback",details.FrontSetback);
		setField("RearSetback",details.RearSetback);
		setField("BuildWidth",details.BuildWidth);
		setField("BuildDepth",details.BuildDepth);
	}
	public HomeDetails getDetails() {
		return new HomeDetails(getRecord());
	}
	
	@Override
	public void beforeSave() {
		super.beforeSave();
		setField("ProductType","Land");
	}
	
	@Override
	public void postSave() {
		super.postSave();
		
		GenRow homeDetails = new GenRow();
		homeDetails.setTableSpec("properties/HomeDetails");
		homeDetails.setParameter("-select1","ProductID");
		homeDetails.setParameter("ProductID", getPrimaryKey());
		homeDetails.doAction(GenerationKeys.SELECT);
		homeDetails.setAction(homeDetails.isSuccessful() ? GenerationKeys.UPDATE: GenerationKeys.INSERT);
		if(setUpdatedFields(homeDetails)) {
			homeDetails.doAction();
		}
		
		GenRow address = new GenRow();
		address.setTableSpec("Addresses");
		address.setParameter("ProductID", getPrimaryKey());
		address.setParameter("-select1","AddressID");
		
		address.doAction(GenerationKeys.SELECTFIRST);
		if(address.isSuccessful()) {
			address.setAction(GenerationKeys.UPDATE);
		} else {
			address.setToNewID("AddressID");
			address.setAction(GenerationKeys.INSERT);
		}
		if(setUpdatedFields(address)) { 
			address.doAction();
		}
	}
	
	private boolean setUpdatedFields(GenRow sub) {
		boolean has = false;
	
		int max = sub.getTableSpec().getFieldsLength() - 1;
		for(int i=0; ; i++) {
			String f = sub.getTableSpec().getFieldName(i);
			if(hasParameter(f) && !"GroupID".equals(f)) { //ProductDetails nor Addresses should have GroupID 
				sub.setParameter(f, getField(f));;
				has = true;
			}
			if(i == max)
				break;
		}
		return has; 
	}
}
