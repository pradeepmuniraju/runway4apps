/**
 * 
 */
package com.sok.runway.crm;

import java.sql.Connection;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.runway.view.crm.ProductView;

/**
 * @author dion
 *
 */
public class ProductDeleteReport extends Thread {

	//private ProductView 	product = null;
	private GenRow			requestrow = null;

	private ProductDelete	productDel = null;
	
	private String			masterID = "";


	private Connection		con = null;
	private Context			ctx = null;
	private String 			dbconn = null;


	private String 			productID = "";
	private String 			pType = "";

	private int 			estate = 0;
	private int 			stage = 0;
	private int 			brand = 0;
	private int 			range = 0;
	private int 			handl = 0;
	private int 			land = 0;
	private int 			home = 0;
	private int 			process = 0;
	private int 			commissions = 0;
	private int 			quote = 0;

	private int 			canDelete = 0;

	private boolean 		canBeDeleted = true;

	private String 			dString = "";
	
	private boolean			isComplete = false;

	public ProductDeleteReport(HttpServletRequest request, String productID) {
		//product = new ProductView(request);

		requestrow = new GenRow();
		requestrow.parseRequest(request);

		masterID = requestrow.getString("DeleteID");
		
		this.productID = productID;

		pType = requestrow.getString("ProductType");

		//con = requestrow.getConnection();

		this.setName("delete " + pType);

		this.start();
	}


	public ProductDeleteReport(HttpServletRequest request, GenRow requestrow, String productID) {
		///product = new ProductView(request);

		if(requestrow == null) {
			requestrow = new GenRow();
			requestrow.parseRequest(request);
		}
		this.requestrow = requestrow;

		masterID = requestrow.getString("DeleteID");

		this.productID = productID;

		pType = requestrow.getString("ProductType");

		//con = requestrow.getConnection();

		this.setName("delete " + pType);
		this.start();
	}


	public Connection getConnection() throws Exception
	{
		if(con == null || con.isClosed())
		{
			con = ActionBean.connect();
		}
		return con;
	}

	public void run() {
		isComplete = false;
		try {
			con = getConnection();
		} catch (Exception e) {
			
		}

		String productType = "";

		if (productID != null && productID.indexOf(" ") >= 0) productID = productID.replaceAll(" ","\\+");
		if (productID.length() > 0) {
			if (masterID.length() == 0) masterID = KeyMaker.generate();
			if (productID.indexOf("+") >= 0) {
				productDel = new ProductDelete(masterID,productID.split("\\+"));
			} else {
				productDel = new ProductDelete(masterID,productID);
			}
			productDel.simulateDelete(1);

			canBeDeleted =  productDel.canBeDeleted(null);

			if (productDel.getProduct() != null) {
				productType = productDel.getProduct().getString("ProductType");
			}

			Collection<ProductDelete> collection = productDel.getProductList();
			if (collection != null) {
				Iterator<ProductDelete> it = collection.iterator();

				Set<String> processList = new HashSet<String>();
				Set<String> commissionList = new HashSet<String>();
				Set<String> quoteList = new HashSet<String>();

				ProductDelete pd = null;

				while (it.hasNext()) {
					try {
						pd = it.next();
					} catch (Exception e) {
						continue;
					}
					if (!pd.canBeDeleted(null)) {
						GenRow row = pd.getProduct();
						if (row != null) {
							String type = row.getString("ProductType");
							if ("Brand".equalsIgnoreCase(type)) ++brand;
							if ("Estate".equalsIgnoreCase(type)) ++estate;
							if ("Stage".equalsIgnoreCase(type)) ++stage;
							if ("Land".equalsIgnoreCase(type)) ++land;
							if ("Home Range".equalsIgnoreCase(type)) ++range;
							if ("Home Plan".equalsIgnoreCase(type)) ++home;
							if ("House and Land".equalsIgnoreCase(type)) ++handl;
						}
						processList.addAll(pd.getProcesses());
						commissionList.addAll(pd.getCommissions());
						quoteList.addAll(pd.getQuotes());
					} else {
						GenRow row = pd.getProduct();
						if (row != null) {
							String type = row.getString("ProductType");
							if (pType.length() > 0) {
								if (pType.equalsIgnoreCase(type)) ++canDelete;  
							} else {
								if ("Brand".equalsIgnoreCase(type) || "Estate".equalsIgnoreCase(type) || "Stage".equalsIgnoreCase(type) ||
										"Land".equalsIgnoreCase(type) || "Home Range".equalsIgnoreCase(type) || "Home Plan".equalsIgnoreCase(type) || 
										"House and Land".equalsIgnoreCase(type)) {
									++canDelete;
								}
							}
						}
					}
				}

				process += processList.size();
				commissions += commissionList.size();
				quote += quoteList.size();
			}

		}

		if (process > 0) {
			dString += "Processes";
		}

		if (commissions > 0) {
			if (dString.length() > 0) dString += ", ";
			dString += "Commissions, Payments";
		}

		if (quote > 0) {
			if (dString.length() > 0) dString += ", ";
			dString += "Quotes";
		}

		if (dString.length() > 0) {
			if (dString.lastIndexOf(", ") >= 0) {
				dString = dString.substring(0,dString.lastIndexOf(", ")) + " and" + dString.substring(dString.lastIndexOf(", ") + 1);  
			}
		}

		isComplete = true;
	}


	public int getEstate() {
		return estate;
	}


	public int getStage() {
		return stage;
	}


	public int getBrand() {
		return brand;
	}


	public int getRange() {
		return range;
	}


	public int getHouseLand() {
		return handl;
	}


	public int getLand() {
		return land;
	}


	public int getHome() {
		return home;
	}


	public int getProcess() {
		return process;
	}


	public int getCommissions() {
		return commissions;
	}


	public int getQuote() {
		return quote;
	}


	public int canDelete() {
		return canDelete;
	}


	public boolean canBeDeleted() {
		return canBeDeleted;
	}


	public String getProductType() {
		return pType;
	}

	public String getMasterID() {
		if (productDel == null) return "";
		return productDel.getID();
	}

	public int getMax() {
		if (productDel == null) return 0;
		return productDel.getMax();
	}

	public int getCurrent() {
		if (productDel == null) return 0;
		return productDel.getProcessedCount();
	}

	public String getInfoString() {
		return dString;
	}


	public boolean isComplete() {
		return isComplete;
	}


	public String getProductID() {
		return productID;
	}


	public void setProductID(String productID) {
		this.productID = productID;
	}


	public ProductDelete getProductDelete() {
		return productDel;
	}
	
	public HashMap<String,Integer> getStatusList() {
		if (productDel == null) return null;
		return productDel.getStatusList();
	}
}
