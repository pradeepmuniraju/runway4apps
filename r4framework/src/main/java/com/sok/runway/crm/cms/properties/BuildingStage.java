package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class BuildingStage  extends PropertyEntity {

	final PropertyType propertyType = PropertyType.BuildingStage;
	static final String VIEWSPEC = "properties/ProductBuildingStageView";

	public BuildingStage(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public BuildingStage(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	public BuildingStage(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	public List<BuildingStage> getStage() {
		return Collections.emptyList();
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
}
