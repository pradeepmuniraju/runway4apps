package com.sok.runway.crm.cms.properties.pdf.advanced;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sog.pdfserver.client.api.request.GroupData;
import com.sog.pdfserver.client.api.request.PDFRequest;
import com.sog.pdfserver.client.api.request.PageStaticData;
import com.sog.pdfserver.client.api.request.PairTag;
import com.sog.pdfserver.client.api.request.RowData;
import com.sog.pdfserver.client.api.request.TemplateListTag;
import com.sok.framework.Conditions;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.RunwayUtil;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.pdf.AdvancedPDFProcessor;
import com.sok.runway.crm.cms.properties.pdf.advanced.models.PDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;
import com.sog.pdfserver.client.api.exception.PdfClientValidationException;
import com.sog.pdfserver.client.api.exception.PdfServerConsumeException;


/**
 * Default implementation for all products, loaded when specific class for eg:PDFHouseandlandDetail.java is not found
 *
 */
public class PDFProductList extends AdvancedPDFProcessor {

	public static final Logger logger = LoggerFactory.getLogger(PDFProductList.class);

	public void populatePDFRequest(HttpServletRequest request, HttpServletResponse response, PDFRequest pdfRequest) {
		String SORT_TYPE = StringUtils.defaultIfBlank(request.getParameter("-sorttype"), "BY_NONE");

		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (List<PropertyEntity>) request.getAttribute("entityList");
		if (properties == null) {
			setStatus(request, "Loading Records..", 0.2d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			properties = PropertyFactory.getPropertyEntities(request, this);
		}
		
		String productType = "";
		if (properties != null && properties.size() > 0) productType = properties.get(0).getProductType();
		if (StringUtils.isNotBlank(productType)) 
			productType = productType.toUpperCase().replaceAll(" ", "");
		else
			productType = "";
		
		if (!"BY_NONE".equals(SORT_TYPE)) SORT_TYPE = productType + "_" + SORT_TYPE;
		
		templateID =  pdfRequest.getTemplateId();
		
		try {
			loadPDFTemplate(pdfRequest);
		} catch (PdfServerConsumeException e) {
		} catch (PdfClientValidationException e) {
		}
		
		List<GroupData> groupDataList = new ArrayList<>();
		pdfRequest.setGroupDataList(groupDataList);

		TreeMap<String, PDFProductList.SortedBean> sortedBeanMap = sortProperties(request, properties, SORT_TYPE);

		TreeSet<String> sortedSet = new TreeSet<String>(sortedBeanMap.keySet());

		int c = 0;
		int index = 0;
		setStatus(request, "Generating Tag List..", 0.0d);
		for (String sortName : sortedSet) {
			PDFProductList.SortedBean sortedBean = sortedBeanMap.get(sortName);

			List<RowData> rowDataList = new ArrayList<>();
			// For By Estate and By Plan

			if (sortedBean.children != null && !sortedBean.children.isEmpty()) {
				TreeSet<String> childSortedSet = new TreeSet<String>(sortedBean.children.keySet());

				for (String childSortName : childSortedSet) {
					PDFProductList.SortedBean childSortedBean = sortedBean.children.get(childSortName);
					for (PropertyEntity property : childSortedBean.property) {
						RowData rowData = new RowData();
						HashMap<String,String> rowTags = new HashMap<String,String>();
						List<TemplateListTag> listTags = new ArrayList<>(); // Unused currently
						populateDefaultTags(request, rowTags, listTags, property);
						populateSpecificTags(request, rowTags, listTags, property, pdfRequest);
						
						logger.debug("Requested template {} with ID {} " , pdfRequest.getTemplateName() ,pdfRequest.getTemplateId());
						if (super.pdfTemplates.size() > 0) {
							for (int x = 0; x < super.pdfTemplates.size(); ++x) {							
								PDFTemplate template = super.pdfTemplates.get(x);
								logger.debug("Processing template {} with ID {} " , template.getName(), template.getId());
								
								if (StringUtils.isNotBlank(pdfRequest.getTemplateId()) && pdfRequest.getTemplateId().equals(template.getId())) {
									logger.debug("No. of tags {} " , template.getTags().size());
									if (!"true".equalsIgnoreCase(InitServlet.getSystemParam("PDF-AdvancedServerFullListTags")))
										super.filterTagList(template, rowTags);
									break;
								}
							} 
						}
	
						rowData.setName("Item" + index++);
//						System.out.println(property.getString("EstateName") + "," + property.getString("StageName") + "," + property.getString("LotName"));
//						System.out.println(property.getString("RangeName") + "," + property.getString("HomePlanName"));
						rowData.setLotGroup(property.getString("EstateName") + "," + property.getString("StageName") + "," + property.getString("LotName"));
						rowData.setPlanGroup(property.getString("RangeName") + "," + property.getString("HomePlanName"));
						rowData.setTags(rowTags);

						HashMap<String, String> valuePairs = new HashMap<>();
						HashMap<String, String> valuePairsF = new HashMap<>();
						HashMap<String, String> valuePairsTC = new HashMap<>();
						HashMap<String, String> valuePairsLot = new HashMap<>();
						HashMap<String, String> valuePairsD = new HashMap<>();
	
						List<PairTag> pairTags = new ArrayList<>();
						PairTag pairTag = new PairTag();
						pairTag.setName("LOT_ESTATE_HOUSE_DESIGN");
	
						PairTag pairTagF = new PairTag();
						pairTagF.setName("LOT_ESTATE_HOUSE_DESIGN_FORMATTED");
	
						PairTag pairTagTC = new PairTag();
						pairTagTC.setName("LOT_ESTATE_HOUSE_DESIGN_TOTAL_COST");
	
						PairTag pairTagLot = new PairTag();
						pairTagLot.setName("LOT_ESTATE_HOUSE_LOT_NAME");
	
						PairTag pairTagD = new PairTag();
						pairTagD.setName("LOT_ESTATE_HOUSE_DESIGN_NAME");
	
						for (PropertyEntity childProperty : childSortedBean.property) {
							// Pair tags
							valuePairs.put(childProperty.getName(), childProperty.getColumn("DripResult"));
							valuePairsF.put(childProperty.getName(), StringUtil.getRoundedFormattedValue(childProperty.getColumn("DripResult"), false));
							valuePairsTC.put(childProperty.getName(), StringUtil.getRoundedFormattedValue(childProperty.getColumn("TotalCost"), false));
							valuePairsLot.put(childProperty.getName(), childProperty.getColumn("LotName"));
							valuePairsD.put(childProperty.getName(), childProperty.getColumn("DesignName"));
						}
	
						pairTag.setValuePair(valuePairs);
						pairTagF.setValuePair(valuePairsF);
						pairTagTC.setValuePair(valuePairsTC);
						pairTagLot.setValuePair(valuePairsLot);
						pairTagD.setValuePair(valuePairsD);
	
						pairTags.add(pairTag);
						pairTags.add(pairTagF);
						pairTags.add(pairTagTC);
						pairTags.add(pairTagLot);
						pairTags.add(pairTagD);
	
						rowData.setPairTags(pairTags);
						rowDataList.add(rowData);
					}
				}
			} else { 
				for (PropertyEntity childProperty : sortedBean.property) {
					RowData rowData = new RowData();
					HashMap<String,String> rowTags = new HashMap<String,String>();
					List<TemplateListTag> listTags = new ArrayList<>(); // Unused currently
					populateDefaultTags(request, rowTags, listTags, childProperty);
					populateSpecificTags(request, rowTags, listTags, childProperty, pdfRequest);
					
					logger.debug("Requested template {} with ID {} " , pdfRequest.getTemplateName() ,pdfRequest.getTemplateId());
					if (super.pdfTemplates.size() > 0) {
						for (int x = 0; x < super.pdfTemplates.size(); ++x) {							
							PDFTemplate template = super.pdfTemplates.get(x);
							logger.debug("Processing template {} with ID {} " , template.getName(), template.getId());
							
							if (StringUtils.isNotBlank(pdfRequest.getTemplateId()) && pdfRequest.getTemplateId().equals(template.getId())) {
								logger.debug("No. of tags {} " , template.getTags().size());
								if (!"true".equalsIgnoreCase(InitServlet.getSystemParam("PDF-AdvancedServerFullListTags")))
									super.filterTagList(template, rowTags);
								break;
							}
						}
					}
					rowData.setName("Item" + index++);
//					System.out.println(childProperty.getString("EstateName") + "," + childProperty.getString("StageName") + "," + childProperty.getString("LotName"));
//					System.out.println(childProperty.getString("RangeName") + "," + childProperty.getString("DesignName") + "," + childProperty.getString("PlanName"));
					rowData.setLotGroup(childProperty.getString("EstateName") + "," + childProperty.getString("StageName") + "," + childProperty.getString("LotName"));
					rowData.setPlanGroup(childProperty.getString("RangeName") + "," + childProperty.getString("HomePlanName"));
					rowData.setTags(rowTags);
					
					rowDataList.add(rowData);
				}
			}

			// Page specfic header and footer
			HashMap<String,String> groupTags = new HashMap<String,String>();

			// Header
			GenRow headerPropertyRow = RunwayUtil.getProductRow(request, sortedBean.sortID);
			convertRow2Tags(getHomePath(request), headerPropertyRow, groupTags, PREFIX_PRODUCT);

			// Footer
			GenRow repDetails = PDFUtil.getRep(request.getParameter("RepUserID"));
			GenRow displayDetails = PDFUtil.getDisplayLocation(request.getParameter("DisplayID"));

			convertRow2Tags(getHomePath(request), repDetails, groupTags, PREFIX_SALESREP_DETAILS);
			convertRow2Tags(getHomePath(request), displayDetails, groupTags, PREFIX_DISPLAY_DETAILS);

			PageStaticData pageStaticData = new PageStaticData();
			pageStaticData.setTemplateTags(groupTags);
			pageStaticData.setTemplateId(pdfRequest.getTemplateId());
			pageStaticData.setPdfGroupId(pdfRequest.getTemplateId());

			GroupData groupData = new GroupData();
			groupData.setPageStaticData(pageStaticData);
			groupData.setRowDataList(rowDataList);

			logger.debug("Group Data {}", groupData);

			groupDataList.add(groupData);

			setStatus(request, "Generating Tag List..", (double) (++c) / properties.size());
		}

		setStatus(request, "Loading tags completed", 0.5d);

	}

	public TreeMap<String, PDFProductList.SortedBean> sortProperties(HttpServletRequest request, List<PropertyEntity> properties, String sortType) {
		ConfigBean configBean = getConfigSortBean(sortType);

		TreeMap<String, PDFProductList.SortedBean> sortedBeanMap = new TreeMap<String, PDFProductList.SortedBean>();

		for (PropertyEntity property : properties) {
			if (property == null) {
				throw new RuntimeException("Product was not found");
			}

			String sortName = getSortColumnValues(property, configBean.sortName);
			PDFProductList.SortedBean sortedBean = (PDFProductList.SortedBean) sortedBeanMap.get(sortName);
			if (sortedBean == null) {
				sortedBean = new PDFProductList.SortedBean(property.getString(configBean.sortID), sortName);
				sortedBeanMap.put(sortName, sortedBean);
			}

			if (StringUtils.isNotBlank(configBean.childSortName)) {
				String childSortName = getSortColumnValues(property, configBean.childSortName);

				SortedBean childBean = (SortedBean) sortedBean.children.get(childSortName);
				if (childBean == null) {
					childBean = new PDFProductList.SortedBean(property.getString(configBean.childSortID), childSortName);
					sortedBean.children.put(childSortName, childBean);
					if ("ProductID".equals(configBean.childSortID))
						sortedBean.property.add(property); // This is the land property in case of BYESTATE/BUILDERANDLOT
					else 
						sortedBean.property.add(PropertyFactory.getPropertyEntity(request, property.getString(configBean.childSortID))); // This is the land property in case of BYESTATE/BUILDERANDLOT
				}

				childBean.property.add(property);
			} else {
				// Do this only if there is no second level sorting
				sortedBean.property.add(property);
			}
		}

		return sortedBeanMap;
	}
	private String getSortColumnValues(PropertyEntity property, String sortName) {
		if (StringUtils.isBlank(sortName))
			return "NOT SORTED";

		String[] sortColumns = sortName.split(":");
		String[] sortColumnValues = new String[sortColumns.length];

		int i = 0;
		for (String col : sortColumns) {
			sortColumnValues[i++] = property.getString(col);
			logger.debug(property.getString(col));
		}

		return StringUtils.join(sortColumnValues, " ");
	}

	public void populateSpecificTags(HttpServletRequest request, Map<String,String> tags, List<TemplateListTag> listTags, PropertyEntity property, PDFRequest pdfRequest) {
		// Child class can override, pdfRequest is passed so that if needed the template name can be overridden by the child class if needed
	}

	public static class SortedBean {
		public String sortID, sortName;
		public java.util.List<PropertyEntity> property = new ArrayList<PropertyEntity>(); // List of all child properties
		public TreeMap<String, SortedBean> children = new TreeMap<String, SortedBean>(); // Map of all child sortedbeans

		public SortedBean(String sortID, String sortName) {
			this.sortID = sortID;
			this.sortName = sortName;
		}

		@Override
		public String toString() {
			return StringUtils.join(new String[]{sortID, sortName}, "-");
		}
	}

	public static class ConfigBean {
		public String sortID, sortName, childSortID, childSortName;

		public ConfigBean(String sortID, String sortName) {
			this.sortID = sortID;
			this.sortName = sortName;
		}

		public ConfigBean(String sortID, String sortName, String childSortID, String childSortName) {
			this.sortID = sortID;
			this.sortName = sortName;
			this.childSortID = childSortID;
			this.childSortName = childSortName;
		}

	}

	public ConfigBean getConfigSortBean(String sortType) {
		HashMap<String, ConfigBean> SortMap = new HashMap<String, ConfigBean>();
		SortMap.put("BY_NONE", new ConfigBean(null, null));
		SortMap.put("HOUSEANDLAND_BY_PLAN", new ConfigBean("RangeID", "RangeName", "ProductID", "HomePlanName"));
		SortMap.put("HOUSEANDLAND_BY_ESTATE", new ConfigBean("EstateID", "EstateName"));
		SortMap.put("HOUSEANDLAND_BY_ESTATE_AND_LOT", new ConfigBean("EstateID", "EstateName", "LotID", "LotName"));
		SortMap.put("HOUSEANDLAND_BY_ESTATE_AND_LOT_HNL", new ConfigBean("EstateID", "EstateName", "ProductID", "LotName"));
		SortMap.put("HOUSEANDLAND_BY_PRICE", new ConfigBean("EstateID", "EstateName", "ProductID", "DripResult"));
		SortMap.put("HOUSEANDLAND_BY_DESIGN_NAME", new ConfigBean("RangeID", "RangeName", "ProductID", "DesignName"));
		SortMap.put("HOUSEANDLAND_BY_ESTATE_STAGE_LOT", new ConfigBean("EstateID", "EstateName", "ProductID", "StageName:LotName"));
		SortMap.put("HOMEPLAN_BY_PLAN", new ConfigBean("RangeID", "RangeName", "PlanID", "HomePlanName"));
		SortMap.put("HOMEPLAN_BY_PRICE", new ConfigBean("RangeID", "RangeName", "PlanID", "TotalCost"));
		SortMap.put("LAND_BY_ESTATE", new ConfigBean("EstateID", "EstateName"));
		SortMap.put("LAND_BY_ESTATE_AND_LOT", new ConfigBean("EstateID", "EstateName", "ProductID", "Name"));

		SortMap.put("LAND_BY_ESTATE_STAGE_LOT", new ConfigBean("EstateID", "EstateName", "ProductID", "Name"));
		SortMap.put("LAND_BY_PRICE", new ConfigBean("EstateID", "EstateName", "ProductID", "TotalCost"));

		logger.debug("sortType - {}", sortType);
		logger.debug("Sort - {}", SortMap.get(sortType).toString());

		return SortMap.get(sortType);
	}

	@Override
	public int getProcessSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getProcessedCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getErrorCount() {
		// TODO Auto-generated method stub
		return 0;
	}
}