package com.sok.runway.crm.cms;

import java.sql.Connection;

import javax.servlet.ServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.runway.Validator;
import com.sok.runway.crm.activity.Logable;
import com.sok.runway.security.AuditEntity;

@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown=true)
public class PageMeta extends AuditEntity implements Logable {
	
	public static final String ENTITY_NAME = "Page Meta";
	@Override
	public String getEntityName() {
		return ENTITY_NAME;
	}
	
	public PageMeta(Connection con) {
		super(con, "PageMetaView");
	}
	public PageMeta() {
		super((Connection) null, "PageMetaView");
	}
	public PageMeta(ServletRequest request) {
		super(request, "PageMetaView");
		populateFromRequest(request.getParameterMap());
	}
	public PageMeta(Connection con, String linkID) {
		this(con);
		load(linkID);
	}
	public Validator getValidator() {
		Validator val = Validator.getValidator("PageMeta");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("PageMetaID","SYSTEM ERROR - PageMetaID");
			Validator.addValidator("PageMeta", val);
		}
		return val;
	}
	
	@JsonProperty("PageMetaID")
	public String getPageMetaID() {
		return getField("PageMetaID");
	}
	@JsonProperty("PageMetaID")
	public void setPageMetaID(String PageMetaID) {
		setField("PageMetaID", PageMetaID);
	}
	@JsonProperty("KeyWords")
	public String getKeyWords() {
		return getField("KeyWords");
	}
	@JsonProperty("KeyWords")
	public void setKeyWords(String KeyWords) {
		setField("KeyWords", KeyWords);
	}
	@JsonProperty("Rating")
	public String getRating() {
		return getField("Rating");
	}
	@JsonProperty("Rating")
	public void setRating(String Rating) {
		setField("Rating", Rating);
	}
	@JsonProperty("Robots")
	public String getRobots() {
		return getField("Robots");
	}
	@JsonProperty("Robots")
	public void setRobots(String Robots) {
		setField("Robots", Robots);
	}
	@JsonProperty("RevisitAfter")
	public String getRevisitAfter() {
		return getField("RevisitAfter");
	}
	@JsonProperty("RevisitAfter")
	public void setRevisitAfter(String RevisitAfter) {
		setField("RevisitAfter", RevisitAfter);
	}
	@JsonProperty("Description")
	public String getDescription() {
		return getField("Description");
	}
	@JsonProperty("Description")
	public void setDescription(String Description) {
		setField("Description", Description);
	}
}
