package com.sok.runway.crm.cms.properties.excel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PropertyExcelGenerator {
	public static final String GENERIC_PACKAGE = "com.sok.runway.crm.cms.properties.excel.templates.";
	public static final String SPECIFIC_PACKAGE = "com.sok.clients.report.";

	public static void generate(String classname, HttpServletRequest request, HttpServletResponse response) {

		try {
			AbstractExcelTemplate template = null;

			try {
				template = (AbstractExcelTemplate) (Class.forName(SPECIFIC_PACKAGE + classname).newInstance());

			} catch (Exception e) {
				System.out.println("Unable to load specific pdf template. Using default " + classname + ". Exception: " + e);
				template = (AbstractExcelTemplate) (Class.forName(GENERIC_PACKAGE + classname).newInstance());
			}

			template.generateExcel(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error generating pdf template " + classname + ". Exception: " + e);
		}
	}
}