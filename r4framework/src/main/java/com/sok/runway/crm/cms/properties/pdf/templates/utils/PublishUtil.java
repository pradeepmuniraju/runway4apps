package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.templates.model.PublishInfo;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;

public class PublishUtil extends CommonUtil implements PDFConstants {

	private static final Logger logger = LoggerFactory.getLogger(PublishUtil.class);

	public static List<PublishInfo> getPublishBean(HttpServletRequest request) {

		List<PropertyEntity> properties = PropertyFactory.getPropertyEntities(request);

		List<PublishInfo> publishInfoList = new ArrayList<PublishInfo>();
		try {
			for (PropertyEntity property : properties) {

				if (!property.isPropertyType(PropertyType.HouseLandPackage)) {
					System.out.println(String.format("ProductID [%1$s] Property [%2$s] is not a house and land package", property.getString("ProductID"), property.getPropertyType()));
					continue;
				}

				PublishInfo info = new PublishInfo();

				HouseLandPackage product = (HouseLandPackage) property;

				info.address = capitalizeFirstLetters(product.getAddress());
				info.facade = product.getFacadeName();
				info.homePlanName = product.getName();
				info.price = product.getDripTotalCost();
				info.status = product.getPublishingStatus();

				info.range = product.getRangeName();
				info.design = product.getHomeName();
				info.estate = product.getEstateName();
				info.stage = product.getStageName();
				info.marketingStatus = product.getMarketingStatus();
				info.lotType = product.getLotType();
				info.storeys = product.getStorey();

				info.area = Integer.toString(new Double(product.getLotSize()).intValue());
				info.width = Integer.toString(new Double(product.getLotWidth()).intValue());
				info.depth = Integer.toString(new Double(product.getLotDepth()).intValue());

				fillPublishDetails(request, property.getString("ProductID"), info);
				
				fixNonNullableFields(info);
				
				publishInfoList.add(info);
			}
		} catch (Exception e) {
			logger.debug("Error in getPublishBean" + e);
		}
		return publishInfoList;
	}

	private static void fillPublishDetails(HttpServletRequest request, String productID, PublishInfo info) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		GenRow productPublishing = new GenRow();
		productPublishing.setRequest(request);
		productPublishing.setViewSpec("properties/ProductPublishingView");
		productPublishing.setParameter("ProductID", productID);
		productPublishing.setParameter("-join0", "ProductPublishingProductSecurityGroups.ProductSecurityGroupRep");
		productPublishing.setParameter("-select0", "ProductPublishingProductSecurityGroups.ProductSecurityGroupRep.FirstName as RepFirstName");
		productPublishing.setParameter("-select1", "ProductPublishingProductSecurityGroups.ProductSecurityGroupRep.LastName as RepLastName");
		productPublishing.setParameter("-select2", "ProductPublishingProductSecurityGroups.RepUserID as RepUserID");
		productPublishing.setParameter("-select3", "DATEDIFF( NOW(), FirstPublishDate) as Age");
		productPublishing.doAction("search");
		productPublishing.getResults();

		while (productPublishing.getNext()) {
			// Need not be done inside a while loop, but want to avoid firing separate queries
			if (StringUtils.isBlank(productPublishing.getString("RepUserID")))
				info.salesRep = "Unclaimed";
			else
				info.salesRep = productPublishing.getString("RepFirstName") + " " + productPublishing.getString("RepLastName");

			PublishMethod pm = PublishMethod.getFromPublishMethodName(productPublishing.getString("PublishMethod"));
			try {
				PropertyUtils.setProperty(info, pm.getBeanName(), productPublishing.getString("Age"));
			} catch (NoSuchMethodException e) {
				logger.error("Ignoring Error in setting property to getPublishBean" + e);
			}

		}
	}

	private static void fixNonNullableFields(PublishInfo info) {
		if (StringUtils.isBlank(info.salesRep))
			info.salesRep = "Unclaimed";
		
		if (StringUtils.isBlank(info.marketingStatus))
			info.marketingStatus = "Unspecified MarketingStatus";
		
		if (StringUtils.isBlank(info.lotType))
			info.lotType = "Unspecified Lot Type";
	}
}
