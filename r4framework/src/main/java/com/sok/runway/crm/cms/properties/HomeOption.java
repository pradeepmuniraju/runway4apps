package com.sok.runway.crm.cms.properties;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

public class HomeOption extends PropertyEntity {

	final PropertyType propertyType = PropertyType.HomeOption;
	static final String VIEWSPEC = "properties/ProductPlanFeatureView";

	public HomeOption(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public HomeOption(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	public HomeOption(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
}
