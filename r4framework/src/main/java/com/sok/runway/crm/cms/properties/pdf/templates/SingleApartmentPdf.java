package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.ApartmentInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ApartmentHeaderFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;

public class SingleApartmentPdf extends AbstractPDFTemplate implements PDFConstants {
	
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {

		ArrayList<ApartmentInfo> apartmentsList = BuildingApartmentsPdf.getBuildingsApartmentsList(request, PDFUtil.PDF_DETAILS_SINGLE_APARTMENT);

		renderPages(document, apartmentsList);
	}

	@Override
	public void setMargins(Document document) 
	{
		document.setMargins(15, 15, 15, 0); // L R T B
	}

	private void renderPages(com.itextpdf.text.Document document, ArrayList<ApartmentInfo> apartmentInfoList) throws DocumentException 
	{
		renderApartmentsPage(document, apartmentInfoList);
	}

	private void renderApartmentsPage(com.itextpdf.text.Document document, ArrayList<ApartmentInfo> apartmentInfoList) throws DocumentException
	{
		PdfPTable pageTable = new PdfPTable(1);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setBorderWidth(0f);
		pageTable.getDefaultCell().setBorderWidthTop(0f);
		pageTable.getDefaultCell().setBorderWidthBottom(0f);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.setWidthPercentage(100f);

		if (apartmentInfoList!=null && !apartmentInfoList.isEmpty())
		{
				// Adding Header
				ApartmentInfo apartmentInfo = apartmentInfoList.get(0);
				pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
				pageTable.addCell(BuildingApartmentsPdf.getHeaderTable(apartmentInfo, getPdfSimpleName())); 

				int pageBreaker = 0;
				for (int j = 0; j < apartmentInfoList.size(); j++) 
				{
						apartmentInfo = apartmentInfoList.get(j);

						if (j != 0 && pageBreaker%1==0) 
						{
							document.add(pageTable);
							document.newPage();
							
							pageTable = new PdfPTable(1);
							pageTable.getDefaultCell().setBorder(0);
							pageTable.getDefaultCell().setPadding(0);
							pageTable.getDefaultCell().setFixedHeight(0f);
							pageTable.setWidthPercentage(100f);
							
							// Adding Header
							pageTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
							pageTable.addCell(BuildingApartmentsPdf.getHeaderTable(apartmentInfo, getPdfSimpleName()));
							
							pageBreaker = 0;
						}

						pageTable.getDefaultCell().setPadding(0);
						pageTable.getDefaultCell().setPaddingTop(10);  // Important Padding Field.
						pageTable.getDefaultCell().setPaddingBottom(10);
						pageTable.getDefaultCell().setBorder(0);
						pageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
						
						PdfPTable tblFirstRow = getFirstRowTable(apartmentInfo);
						pageTable.addCell(tblFirstRow);
						
						PdfPTable tblSecondRow = getSecondRowTable(apartmentInfo);
						pageTable.addCell(tblSecondRow);
						
						PdfPTable tblThirdRow = getThirdRowTable(apartmentInfo);
						pageTable.addCell(tblThirdRow);

						/*ApartmentInfo apartmentInfo = apartmentInfoList.get(j);
						if (apartmentInfo!=null)
						{	
							PdfPTable bodyTable = getBodyTableRow(apartmentInfo);
							pageTable.addCell(bodyTable);
						}*/	
						
						pageBreaker++;
				}
		}		
		document.add(pageTable);
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		return new ApartmentHeaderFooter(request, PDFUtil.PDF_DETAILS_SINGLE_APARTMENT);
	}
	
	public String getPdfSimpleName() {
		return PDFUtil.PDF_DETAILS_SINGLE_APARTMENT;
	}
	
	public PdfPTable getBodyTableRow(ApartmentInfo apartmentInfo) throws DocumentException 
	{
			PdfPTable rightBodyTable = new PdfPTable(2);
			rightBodyTable.setWidths(new float[] { 43f, 57f});
			rightBodyTable.setWidthPercentage(100f);
			rightBodyTable.getDefaultCell().setPadding(0);
			rightBodyTable.getDefaultCell().setPaddingBottom(3);
			rightBodyTable.getDefaultCell().setPaddingTop(2);
			rightBodyTable.getDefaultCell().setPaddingLeft(10);
			rightBodyTable.getDefaultCell().setBorder(0);
			rightBodyTable.getDefaultCell().setBorderWidth(0f);
			rightBodyTable.getDefaultCell().setBorderWidthTop(0f);
			rightBodyTable.getDefaultCell().setBorderWidthBottom(0f);
			rightBodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_LEFT);
			rightBodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			rightBodyTable.getDefaultCell().setBackgroundColor(BuildingApartmentsPdf.lightGreyBGColor);
			rightBodyTable.getDefaultCell().setColspan(1);

			// Body Row 01
			rightBodyTable.addCell(new Paragraph("Lot #:", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(new Paragraph(apartmentInfo.getLotNumber(), BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();

			// Body Row 02
			rightBodyTable.addCell(new Paragraph("Level:", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(new Paragraph(apartmentInfo.getLevel(), BuildingApartmentsPdf.arialNormalPt15Grey));

			
			// Body Row 03
			String finalTypeStr = "";
			String typeDesc = apartmentInfo.getType();
			StringTokenizer st = new StringTokenizer(typeDesc);
			int worldCounter = 0;
			while (st.hasMoreElements()) 
			{
				worldCounter ++;
				finalTypeStr += st.nextElement() + " ";
				
				if (worldCounter > 7)
				{	
					finalTypeStr = finalTypeStr.trim() + "...";
					break;
				}	
			}
			rightBodyTable.addCell(new Paragraph("Type:", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(new Paragraph(finalTypeStr, BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();
			
			Paragraph p = new Paragraph();
			p.add(new Chunk("Internal Area (m", BuildingApartmentsPdf.arialBoldPt15Black));
			Chunk c = new Chunk("2", BuildingApartmentsPdf.arialBoldPt12Black);
			c.setTextRise(4.0f);
			p.add(c);
			p.add(new Chunk("):", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(p);
			rightBodyTable.addCell(new Paragraph("" + apartmentInfo.getInternalArea(), BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();
			
			p = new Paragraph();
			p.add(new Chunk("Balcony Area (m", BuildingApartmentsPdf.arialBoldPt15Black));
			c = new Chunk("2", BuildingApartmentsPdf.arialBoldPt12Black);
			c.setTextRise(4.0f);
			p.add(c);
			p.add(new Chunk("):", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(p);
			rightBodyTable.addCell(new Paragraph("" + apartmentInfo.getBalconyArea(), BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();
			
			p = new Paragraph();
			p.add(new Chunk("Total Area (m", BuildingApartmentsPdf.arialBoldPt15Black));
			c = new Chunk("2", BuildingApartmentsPdf.arialBoldPt12Black);
			c.setTextRise(4.0f);
			p.add(c);
			p.add(new Chunk("):", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(p);
			rightBodyTable.addCell(new Paragraph("" + apartmentInfo.getTotalArea(), BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();
			
			rightBodyTable.addCell(new Paragraph("Car Spaces:", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(new Paragraph("" + apartmentInfo.getCarSpaces(), BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();
			
			rightBodyTable.addCell(new Paragraph("Price:", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(new Paragraph("" + apartmentInfo.getPrice(), BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();
			
			rightBodyTable.addCell(new Paragraph("Rent:", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(new Paragraph("" + apartmentInfo.getRent(), BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();
			
			rightBodyTable.addCell(new Paragraph("Sales Yield:", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(new Paragraph("" + apartmentInfo.getSalesYield(),BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();
			
			rightBodyTable.addCell(new Paragraph("Stage:", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(new Paragraph("" + apartmentInfo.getStage(), BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();
			
			rightBodyTable.addCell(new Paragraph("Sales Status:", BuildingApartmentsPdf.arialBoldPt15Black));
			rightBodyTable.addCell(new Paragraph("" + apartmentInfo.getSalesStatus(), BuildingApartmentsPdf.arialNormalPt15Grey));
			rightBodyTable.completeRow();

			return rightBodyTable;
	}
	
	
	public PdfPTable getFirstRowTable(ApartmentInfo apartmentInfo) throws DocumentException 
	{
			PdfPTable bodyTable = new PdfPTable(2);
			bodyTable.setWidths(new float[] { 61.2f, 38.8f});
			bodyTable.getDefaultCell().setPadding(0);
			bodyTable.getDefaultCell().setPaddingBottom(3);
			bodyTable.getDefaultCell().setPaddingTop(10);
			bodyTable.getDefaultCell().setPaddingLeft(0);
			bodyTable.getDefaultCell().setBorder(0);
			bodyTable.getDefaultCell().setBorderWidth(0f);
			bodyTable.getDefaultCell().setBorderWidthTop(0f);
			bodyTable.getDefaultCell().setBorderWidthBottom(0f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			bodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			bodyTable.setWidthPercentage(100f);
			//bodyTable.getDefaultCell().setMinimumHeight(600f);

			String leftImg = "";
			if (apartmentInfo != null && apartmentInfo.getBuildingImagesList() != null && !apartmentInfo.getBuildingImagesList().isEmpty())
			{
					ArrayList<String> tmpImgList = apartmentInfo.getBuildingImagesList();
					
					if (tmpImgList.size() >=1)
					{
						leftImg = tmpImgList.get(0);
					}	
			}	
			
			// Left Cell
			PdfPCell cellLeft = new PdfPCell();
			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			String imgStr = homePath + "/" + leftImg;
			try
			{
					java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
					
					if (leftImg!=null && !"".equalsIgnoreCase(leftImg))
					{	
						Image imgHero = Image.getInstance(imgUri.toString());
						//imgHero.scaleToFit(304, 109.2f); // Perfect for 750 x 299
						imgHero.scaleToFit(340f, 359.6f); // Perfect for 1000 x 594
						cellLeft = new PdfPCell(imgHero);
					}
					else
					{
						cellLeft = new PdfPCell(new Paragraph("Image Doesn't exist", BuildingApartmentsPdf.arialNormalPt16Black));
					}
			}
			catch(Exception ee)
			{
				cellLeft = new PdfPCell(new Paragraph("Problem with image" + imgStr, BuildingApartmentsPdf.arialNormalPt16Black));
					System.out.println("Exception Occurred. Message is: " + ee.getMessage());
					ee.printStackTrace();
			}
			cellLeft.setBorder(0);
			cellLeft.setBorderWidth(0f);
			bodyTable.addCell(cellLeft);
			 
			// Right Cell
			PdfPCell cellRight = new PdfPCell();
			try
			{
					PdfPTable tblRight = getBodyTableRow (apartmentInfo);
					if (tblRight!=null)
					{	
						cellRight = new PdfPCell(tblRight);
					}
					else
					{
						cellRight = new PdfPCell(new Paragraph("Information is not available.", BuildingApartmentsPdf.arialNormalPt16Black));
					}
			}
			catch(Exception ee)
			{
					cellRight = new PdfPCell(new Paragraph("Problem with Apartment Details" + imgStr, BuildingApartmentsPdf.arialNormalPt16Black));
					System.out.println("Exception Occurred. Message is: " + ee.getMessage());
					ee.printStackTrace();
			}
			cellRight.setBorder(0);
			cellRight.setBorderWidth(0f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			bodyTable.addCell(cellRight);

			return bodyTable;
	}
	
	public PdfPTable getSecondRowTable(ApartmentInfo apartmentInfo) throws DocumentException 
	{
			PdfPTable bodyTable = new PdfPTable(1);
			bodyTable.setWidths(new float[] { 100f});
			bodyTable.getDefaultCell().setPadding(0);
			bodyTable.getDefaultCell().setPaddingBottom(3);
			bodyTable.getDefaultCell().setPaddingTop(10);
			bodyTable.getDefaultCell().setPaddingLeft(0);
			bodyTable.getDefaultCell().setBorder(0);
			bodyTable.getDefaultCell().setBorderWidth(0f);
			bodyTable.getDefaultCell().setBorderWidthTop(0f);
			bodyTable.getDefaultCell().setBorderWidthBottom(0f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			bodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			bodyTable.setWidthPercentage(100f);
			//bodyTable.getDefaultCell().setMinimumHeight(600f);

			Paragraph pHeading =  new Paragraph(apartmentInfo.getApartmentDetailsText1(), BuildingApartmentsPdf.arialBoldPt25Black);
			Paragraph pDescription =  new Paragraph(apartmentInfo.getApartmentDetailsText2(), BuildingApartmentsPdf.arialNormalPt16Black);

			bodyTable.addCell(pHeading);
			bodyTable.addCell(pDescription);
			
			return bodyTable;
	}
	
	
	public PdfPTable getThirdRowTable(ApartmentInfo apartmentInfo) throws DocumentException 
	{
			PdfPTable bodyTable = new PdfPTable(2);
			bodyTable.setWidths(new float[] { 50f, 50f});
			bodyTable.getDefaultCell().setPadding(0);
			bodyTable.getDefaultCell().setPaddingBottom(3);
			bodyTable.getDefaultCell().setPaddingTop(10);
			bodyTable.getDefaultCell().setPaddingLeft(0);
			bodyTable.getDefaultCell().setBorder(0);
			bodyTable.getDefaultCell().setBorderWidth(0f);
			bodyTable.getDefaultCell().setBorderWidthTop(0f);
			bodyTable.getDefaultCell().setBorderWidthBottom(0f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			bodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			bodyTable.setWidthPercentage(100f);
			//bodyTable.getDefaultCell().setMinimumHeight(600f);

			String leftImg = "";
			String rightImg = "";
			
			boolean isFloorPlanAvailable = false;
			if (apartmentInfo != null && apartmentInfo.getApartmentFloorPlansImagesList() != null && !apartmentInfo.getApartmentFloorPlansImagesList().isEmpty())
			{
					ArrayList<String> tmpImgList = apartmentInfo.getApartmentFloorPlansImagesList();
					
					if (tmpImgList.size() >=1)
					{
						leftImg = tmpImgList.get(0);
						isFloorPlanAvailable = true;
					}	
			}
			
			if (apartmentInfo != null && apartmentInfo.getApartmentImagesList() != null && !apartmentInfo.getApartmentImagesList().isEmpty())
			{
					ArrayList<String> tmpImgList = apartmentInfo.getApartmentImagesList();
					
					if (isFloorPlanAvailable && leftImg!=null && !"".equalsIgnoreCase(leftImg))
					{
							if (tmpImgList.size() >=1)
							{
								rightImg = tmpImgList.get(0);
							}	
					}
					else
					{
							if (tmpImgList.size() >=1)
							{
								leftImg = tmpImgList.get(0);
							}	

							if (tmpImgList.size() >=2)
							{	
								rightImg = tmpImgList.get(1);
							}			
					}	
			}
			
			// Left Cell
			PdfPCell cellLeft = new PdfPCell();
			String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
			String imgStr = homePath + "/" + leftImg;
			try
			{
					java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
					
					if (leftImg!=null && !"".equalsIgnoreCase(leftImg))
					{	
						Image imgHero = Image.getInstance(imgUri.toString());
						//imgHero.scaleToFit(304, 109.2f); // Perfect for 750 x 299
						//imgHero.scaleToFit(374f, 185f); // Perfect for 1000 x 1000
						imgHero.scaleToFit(272f, 260.2f); // Perfect for 1000 x 666
						cellLeft = new PdfPCell(imgHero);
					}
					else
					{
						cellLeft = new PdfPCell(new Paragraph("Image Doesn't exist",BuildingApartmentsPdf.arialNormalPt17Black));
					}
			}
			catch(Exception ee)
			{
				cellLeft = new PdfPCell(new Paragraph("Problem with image" + imgStr, BuildingApartmentsPdf.arialNormalPt17Black));
					System.out.println("Exception Occurred. Message is: " + ee.getMessage());
					ee.printStackTrace();
			}
			cellLeft.setBorder(0);
			cellLeft.setBorderWidth(0f);
			bodyTable.addCell(cellLeft);
			 
			// Right Cell
			PdfPCell cellRight = new PdfPCell();
			imgStr = homePath + "/" + rightImg;
			try
			{
					java.net.URI imgUri = new URI(imgStr.replace(" ", "%20"));
					
					if (rightImg!=null && !"".equalsIgnoreCase(rightImg))
					{	
						Image imgHero = Image.getInstance(imgUri.toString());
						//imgHero.scaleAbsoluteHeight(250);
						//imgHero.scaleToFit(304, 109.2f); // Perfect for 750 x 299
						imgHero.scaleToFit(272f, 260.2f); // Perfect for 1000 x 666
						cellRight = new PdfPCell(imgHero);
					}
					else
					{
						cellRight = new PdfPCell(new Paragraph("Image Doesn't exist", BuildingApartmentsPdf.arialNormalPt17Black));
					}
			}
			catch(Exception ee)
			{
					cellRight = new PdfPCell(new Paragraph("Problem with image" + imgStr, BuildingApartmentsPdf.arialNormalPt17Black));
					System.out.println("Exception Occurred. Message is: " + ee.getMessage());
					ee.printStackTrace();
			}
			cellRight.setBorder(0);
			cellRight.setBorderWidth(0f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
			bodyTable.addCell(cellRight);

			return bodyTable;
	}
}