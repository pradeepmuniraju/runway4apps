package com.sok.runway.crm.cms;
import com.sok.framework.StringUtil;
import com.sok.runway.*;

import com.sok.runway.crm.*;
import com.sok.runway.security.*;
import com.sok.runway.crm.activity.Logable;
import com.sok.runway.crm.factories.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.service.cms.WebsiteService;
import com.sok.service.crm.UserService;

import javax.servlet.ServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.sql.Connection;
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Class to hold information about a CMS page
 * 
 * @author Irwan
 * @version 0.1
 * @version 0.2	- 	added support for json serialization, default constructor
 * @version 0.3	- 	deletions will now delete child objects 
 */
@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Page extends SecuredEntity implements DocumentLinkable, Logable {
   private static final Logger logger = LoggerFactory.getLogger(Page.class);
   public static final String ENTITY_NAME = "Page";
   
   public Page() {
	   super((Connection) null, "PageTableView");
   }
   
	public Page(Connection con) {
		
		super(con, "PageTableView");
	}
	
	public Page(ServletRequest request) {
	    
		super(request, "PageTableView");
		populateFromRequest(request.getParameterMap());
	}
   
	public Page(Connection con, String pageID) {
	   	
		this(con);
	   load(pageID);
	}
	
	public void load(String pageID, String cmsID) {
		loadFromField(new String[]{"CMSID","PageID"}, new String[]{cmsID, pageID});
	}
	
   public String getEntityName() {
	   return ENTITY_NAME;
   }
   
   @Override
   public boolean canEdit() {
	   return WebsiteService.getInstance().canEditWebsite(getConnection(), getCurrentUser(), getField("CMSID"));
   }

	@Override
	public boolean delete() {
		logger.debug("delete()");
		List<PageContent> list = WebsiteService.getInstance().getPageContents(getConnection(), UserService.getInstance().getSimpleUser(getConnection(), getCurrentUser().getUserID()), getField("CMSID"), getPageID());
		for(PageContent pc: list) {
			pc.setCurrentUser(getCurrentUser());
			pc.setConnection(getConnection());
			logger.debug("pc.delete({})", pc.getPrimaryKey());
			if(!pc.delete()) {
				logger.error("Failed removing Page Content with error " + pc.getRecord().getError());
				return false;
			}
		}
		if(getField("PageMetaID").length()!=0) { 	
			PageMeta pm = new PageMeta(getConnection(), getField("PageMetaID"));
			pm.setCurrentUser(getCurrentUser());
			if(pm.isLoaded() && !pm.delete()) {
				logger.error("Failed removing Page Meta with error " + pm.getRecord().getError());
				return false;
			}
		}
		logger.debug("Content and Meta removed, deleting Page");
		return super.delete();
	}
	
	
	public Validator getValidator() {
		
		Validator val = Validator.getValidator("Page");
		
	   if (val == null) {
		   val = new Validator();
		   val.addMandatoryField("PageID","SYSTEM ERROR - PageID");
		   val.addMandatoryField("Title","Page must have a Title");
		   Validator.addValidator("Page", val);
	   }
	   
	   return val;
	}
	
   /**
    *    Linkable Interface
    */
   public String getLinkableTitle() {      
      return getField("Title");
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs() {
      return getLinkableDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters);
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs() {
      return getDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
   }     
   
   /**
    *    DocumentLinkable Interface
    */
   public LinkableDocument getLinkableDocument() {
      LinkableDocument doc = new LinkedDocument(getConnection());
      doc.setCurrentUser(getCurrentUser());
      return doc;
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public void linkDocument(String documentID) {
      LinkableDocumentFactory.linkDocument(this, documentID);
   }

   @JsonProperty("PageID")
   public String getPageID() {
	   return getField("PageID");
   }
   @JsonProperty("PageID")
   public void setPageID(String PageID) {
	   setField("PageID", PageID);
   }
   @JsonProperty("PageVersionID")
   public String getPageVersionID() {
	   return getField("PageVersionID");
   }
   @JsonProperty("PageVersionID")
   public void setPageVersionID(String PageVersionID) {
	   setField("PageVersionID", PageVersionID);
   }
   @JsonProperty("PageMasterID")
   public String getPageMasterID() {
	   return getField("PageMasterID");
   }
   @JsonProperty("PageMasterID")
   public void setPageMasterID(String PageMasterID) {
	   setField("PageMasterID", PageMasterID);
   }
   @JsonProperty("PageMetaID")
   public String getPageMetaID() {
	   return getField("PageMetaID");
   }
   @JsonProperty("PageMetaID")
   public void setPageMetaID(String PageMetaID) {
	   setField("PageMetaID", PageMetaID);
   }
   @JsonProperty("Title")
   public String getTitle() {
	   return getField("Title");
   }
   @JsonProperty("Title")
   public void setTitle(String Title) {
	   setField("Title", StringUtil.stripHtml(Title));
   }
   @JsonProperty("WindowTitle")
   public String getWindowTitle() {
	   return getField("WindowTitle");
   }
   @JsonProperty("WindowTitle")
   public void setWindowTitle(String WindowTitle) {
	   setField("WindowTitle", StringUtil.stripHtml(WindowTitle));
   }
   @JsonProperty("Body")
   public String getBody() {
	   return getField("Body");
   }
   @JsonProperty("Body")
   public void setBody(String Body) {
	   setField("Body", Body);
   }
   @JsonProperty("Image")
   public String getImage() {
	   return getField("Image");
   }
   @JsonProperty("Image")
   public void setImage(String Image) {
	   setField("Image", Image);
   }
   @JsonProperty("ImageCaption")
   public String getImageCaption() {
	   return getField("ImageCaption");
   }
   @JsonProperty("ImageCaption")
   public void setImageCaption(String ImageCaption) {
	   setField("ImageCaption", ImageCaption);
   }
   @JsonProperty("Overview")
   public String getOverview() {
	   return getField("Overview");
   }
   @JsonProperty("Overview")
   public void setOverview(String Overview) {
	   setField("Overview", Overview);
   }
   @JsonProperty("ListImage")
   public String getListImage() {
	   return getField("ListImage");
   }
   @JsonProperty("ListImage")
   public void setListImage(String ListImage) {
	   setField("ListImage", ListImage);
   }
   @JsonProperty("Image1")
   public String getImage1() {
	   return getField("Image1");
   }
   @JsonProperty("Image1")
   public void setImage1(String Image1) {
	   setField("Image1", Image1);
   }
   @JsonProperty("Image2")
   public String getImage2() {
	   return getField("Image2");
   }
   @JsonProperty("Image2")
   public void setImage2(String Image2) {
	   setField("Image2", Image2);
   }
   @JsonProperty("Image3")
   public String getImage3() {
	   return getField("Image3");
   }
   @JsonProperty("Image3")
   public void setImage3(String Image3) {
	   setField("Image3", Image3);
   }
   @JsonProperty("Image4")
   public String getImage4() {
	   return getField("Image4");
   }
   @JsonProperty("Image4")
   public void setImage4(String Image4) {
	   setField("Image4", Image4);
   }
   @JsonProperty("Image1Caption")
   public String getImage1Caption() {
	   return getField("Image1Caption");
   }
   @JsonProperty("Image1Caption")
   public void setImage1Caption(String Image1Caption) {
	   setField("Image1Caption", Image1Caption);
   }
   @JsonProperty("Image2Caption")
   public String getImage2Caption() {
	   return getField("Image2Caption");
   }
   @JsonProperty("Image2Caption")
   public void setImage2Caption(String Image2Caption) {
	   setField("Image2Caption", Image2Caption);
   }
   @JsonProperty("Image3Caption")
   public String getImage3Caption() {
	   return getField("Image3Caption");
   }
   @JsonProperty("Image3Caption")
   public void setImage3Caption(String Image3Caption) {
	   setField("Image3Caption", Image3Caption);
   }
   @JsonProperty("Image4Caption")
   public String getImage4Caption() {
	   return getField("Image4Caption");
   }
   @JsonProperty("Image4Caption")
   public void setImage4Caption(String Image4Caption) {
	   setField("Image4Caption", Image4Caption);
   }
	
	@JsonProperty("Header")
	public String getHeader() {
		return getField("Header");
	}
	@JsonProperty("Header")
	public void setHeader(String Header) {
		setField("Header", Header);
	}
	@JsonProperty("Format")
	public String getFormat() {
		return getField("Format");
	}
	@JsonProperty("Format")
	public void setFormat(String Format) {
		setField("Format", Format);
	}
	@JsonProperty("Navigation")
	public String getNavigation() {
		return getField("Navigation");
	}
	@JsonProperty("Navigation")
	public void setNavigation(String Navigation) {
		setField("Navigation", Navigation);
	}
	@JsonProperty("NavigationLevel")
	public String getNavigationLevel() {
		return getField("NavigationLevel");
	}
	@JsonProperty("NavigationLevel")
	public void setNavigationLevel(String NavigationLevel) {
		setField("NavigationLevel", NavigationLevel);
	}
	@JsonProperty("PageType")
	public String getPageType() {
		return getField("PageType");
	}
	@JsonProperty("PageType")
	public void setPageType(String PageType) {
		setField("PageType", PageType);
	}
	@JsonProperty("Footer")
	public String getFooter() {
		return getField("Footer");
	}
	@JsonProperty("Footer")
	public void setFooter(String Footer) {
		setField("Footer", Footer);
	}
}