package com.sok.runway.crm.cms.properties.pdf.templates.model;

import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil.Range;

public class Package {
	public String filename, heroImage, lotNo, lotName;
	public String estateName;
	public String estateLogo;
	public String suburb;
	public String titleDate;
	public String stageName;
	public String name;
	public String price;
	public String landPrice;
	public String rangeName;
	public String bathroomsAsString;
	public int bedrooms, bathrooms, carParks;
	public int powderRoom;
	public double lotSize;
	public Range range;
	
	// Properties added for Existing Property List PDF
	public String street;
	public String street2;
	public String state;
	public String postCode;
	
	public String homeArea;
	public String balconyArea;
	public String courtyardArea;
	
	public String projectName;
	
	// Parameters added for land Property List PDF	
	public String disclaimer;
	public double lotWidth, lotDepth;
	
}