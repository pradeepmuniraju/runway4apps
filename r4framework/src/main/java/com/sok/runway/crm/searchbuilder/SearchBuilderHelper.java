package com.sok.runway.crm.searchbuilder;

import javax.servlet.http.HttpServletRequest;

public class SearchBuilderHelper {
    public static SearchCriteria getSearchCriteriaFromRequest(HttpServletRequest request, SearchBuilderBean searchBuilderBean) {
        SearchCriteria searchCriteria = null;
        
        String searchSetNo = request.getParameter("-SearchSetNo");
        String searchCriteriaNo = request.getParameter("-SearchCriteriaNo");
        
        if((searchSetNo != null && searchSetNo.length() > 0) 
                && (searchCriteriaNo != null && searchCriteriaNo.length() > 0)) {
            searchCriteria = searchBuilderBean.getSearchCriteria(searchCriteriaNo, searchCriteriaNo);
        }
            
        if(searchCriteria == null) {    
            searchCriteria = (SearchCriteria) request.getAttribute(SearchCriteria.BEAN_ID);
        }
        
        return searchCriteria;
    }
}
