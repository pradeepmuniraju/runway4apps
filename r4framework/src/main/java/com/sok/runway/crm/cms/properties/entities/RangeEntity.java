package com.sok.runway.crm.cms.properties.entities;

import java.sql.Connection;

import javax.servlet.ServletRequest;
import com.sok.runway.crm.Product;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

public class RangeEntity extends Product {

	//private static final Logger logger = LoggerFactory.getLogger(RangeEntity.class);
	static final String VIEWSPEC = "ProductView";
	public static final String ENTITY_NAME = "Home Range";

	public RangeEntity() {
		super(VIEWSPEC, (Connection) null);
		super.setParameter("ProductType", ENTITY_NAME);
	}

	public RangeEntity(Connection con) {
		super(VIEWSPEC, con);
		super.setParameter("ProductType", ENTITY_NAME);
	}

	public RangeEntity(ServletRequest request) {
		super(VIEWSPEC, request);
		super.setParameter("ProductType", ENTITY_NAME);
	}

	public RangeEntity(Connection con, String productID) {
		super(VIEWSPEC, con);
		super.setParameter("ProductType", ENTITY_NAME);
		loadFinal(productID);
	}

	@Override
	public String getEntityName() {
		return ENTITY_NAME;
	}
}
