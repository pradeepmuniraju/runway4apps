package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class Building extends PropertyEntity {

	final PropertyType propertyType = PropertyType.Building;

	static final String VIEWSPEC = "ProductView";

	public Building(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public Building(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}

	public Building(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	public List<BuildingStage> getStages() {
		return Collections.emptyList();
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
	
	@Override
	public String[] getDripProductIDs() {
		return new String[]{getString(PRODUCTID)};
	}
}
