package com.sok.runway.crm.cms.properties.pdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.LocalJasperReportsContext;
import net.sf.jasperreports.engine.util.SimpleFileResolver;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.runway.crm.cms.properties.pdf.templates.developer.DeveloperPdfGenerator;
import com.sok.runway.crm.interfaces.AsyncProcessStatus;

public abstract class AbstractJasperReport implements AsyncProcessStatus {

	protected static final Logger logger = LoggerFactory.getLogger(AbstractJasperReport.class);
	protected static final String TEMPLATE_PREFIX = "developer_";
	protected final String IMAGE_PATH = "/files/";//Path to images folder.
	public final String REPORT_EXTENSION = ".jrxml";
	public final String BINARY_EXTENSION = ".jasper";	
	
	public final String DEVELOPER_DRIP_TEMPLATE = "Developer Detail";
	public final String DETAIL_DRIP_TEMPLATE = "Detail";
	
	protected String templateURI;
	protected String reportURI;  //Relative path of containing folder
	protected JRDataSource dataSource;
	protected HashMap<String, Object> parameters;
	private boolean developerPDF = true;
	
	public void setStatus(HttpServletRequest request, String status) {

		String classAndMethodName = this.getClass() + " setStatus(request, status) ";
		logger.debug(classAndMethodName + " Started.");

		setStatus(request, status, -1);
		
		logger.debug(classAndMethodName + " Ended.");
	}
	
	public void setStatus(HttpServletRequest request, String status, double fraction) {
		
		String classAndMethodName = this.getClass() + " setStatus(request, status, fraction) ";
		logger.debug(classAndMethodName + " Started.");

		logger.debug("setStatus(request, {})", status);
		String statusKey = (String)request.getAttribute("-statusKey");
		if(statusKey == null) {
			logger.debug("no status key from request");
			String fileKey = (String)request.getSession().getAttribute("-statusKey");
			if(fileKey != null) {
				logger.debug("status key from session {} ", fileKey);
				statusKey = fileKey;
			} else {
				logger.error("no status key");
				
				logger.debug(classAndMethodName + " No Status Key Found. Returning from here.");
				
				return;
			}
			request.setAttribute("-statusKey", statusKey);
		}
		logger.debug("using status key {}", statusKey);
		request.getSession().setAttribute(statusKey, makeStatusDiv(status, fraction));
		
		logger.debug(classAndMethodName + " Ended.");
	}
	
	private String makeStatusDiv(String status, double fraction) {
		
		String classAndMethodName = this.getClass() + " makeStatusDiv(fraction) ";
		logger.debug(classAndMethodName + " Started.");
		
		if(fraction == -1) {
			logger.debug(classAndMethodName + " fraction is -1. Ending the method here.");
			return status;
		}
		
		logger.debug(classAndMethodName + " Ended.");
		
		return new StringBuilder().append(status)
				.append("<div style=\"width:100%; display:block; height: 15px\"><div style=\"width: ").append(fraction * 100)
				.append("%; background-color: red\">&nbsp;</div></div>").toString();
	}
	
	
	/**
	 * This function will return specific template file in name "developer_${estateName}.jrxml"
	 * @return specificTemplatePath
	 */
	private File getTemplateFile(HttpServletRequest request){
		
		String classAndMethodName = this.getClass() + " getTemplateFile(request) ";
		logger.debug(classAndMethodName + " Started.");
		
		String specificTemplatePath = "";
		String estateID = "";
		String estateName = "";
		Map<String, String> estates = null;
		File templateFile = null;
		
		//Get default template file 
		ServletContext context = request.getSession().getServletContext();
		String templateFilename = context.getRealPath(templateURI);
		
		logger.debug(classAndMethodName + " templateURI is: " + templateURI);
		
		templateFile = new File(templateFilename);
		//Check whether specific template file exists for specific realestate
		estateID = request.getParameter("EstateID");
		
		logger.debug(classAndMethodName + " estateID is: " + estateID);
		
		if(request.getAttribute("estateMap") != null){
			estates = (HashMap<String, String>) request.getAttribute("estateMap");
			estateName = estates.get(estateID);
			if(!StringUtils.isEmpty(estateName) && templateFile.exists()){
				File parentFolder = templateFile.getParentFile();				
				specificTemplatePath = parentFolder.getAbsolutePath() + File.separatorChar + TEMPLATE_PREFIX + estateName + REPORT_EXTENSION;

				logger.debug(classAndMethodName + " specificTemplatePath is: " + specificTemplatePath);
				
				File specificTemplateFile = new File(specificTemplatePath);
				if(specificTemplateFile.exists())
					templateFile =  specificTemplateFile;
			}
		}
		
		logger.debug(classAndMethodName + " Ended.");
		
		return templateFile;
	}
	
	/**
	 * This function will be used to extract relative file path from File object.
	 * Need to be called before filling the Jasper template 
	 * @param templateFile
	 * @return
	 */
	private String getRelativePath(File templateFile){
		
		String classAndMethodName = this.getClass() + " getRelativePath(templateFile) ";
		logger.debug(classAndMethodName + " Started.");

		String relativePath = "";		
		String parentPath = StringUtils.substring(templateURI, 0, StringUtils.lastIndexOf(templateURI, "/"));
		relativePath += parentPath + "/" + templateFile.getName();
		
		logger.debug(classAndMethodName + " relativePath is: " + relativePath);
		logger.debug(classAndMethodName + " Ended.");
		
		return relativePath;
	}
	
	/**
	 * This is the main function to generate report.
	 * Report data has to be prepared and store in JRDataSource type.
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public void generateReport(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		String classAndMethodName = this.getClass() + " generateReport(request, response) ";
		logger.debug(classAndMethodName + " Started.");

		ServletConfig servletConfig = (ServletConfig)request.getAttribute("servletConfig");		
		ServletOutputStream servletOutputStream = null;
		InputStream reportStream = null;
		ClassLoader originalLoader = null;
		
		try{
			
			logger.debug(classAndMethodName + " Before checking loadedJARs and isDeveloperPDF.");
			if(DeveloperPdfGenerator.loadedJars && isDeveloperPDF()){
				originalLoader = Thread.currentThread().getContextClassLoader();
			    Thread.currentThread().setContextClassLoader(DeveloperPdfGenerator.classLoader);
			}
			
			logger.debug(classAndMethodName + " After checking loadedJARs and isDeveloperPDF.");
			
			try {
				/* Get real path of source/destination file,
				 * then compile template to generate Jasper binary file.
				 * Both source and dest file must use absolute path.
				 * Eg: c:/tools/tomcat/webapps/runway4/common/${project_name}/resources/*.jrxml/jasper.
				 * The report should be compiled only if the template file is newer than binary file to improve the performance. 
				 */
				
				logger.debug(classAndMethodName + " calling setStatus(request, Prepare PDF data, 0.2d)");
				
				setStatus(request, "Prepare PDF data", 0.2d); //20% complete		
				
				logger.debug(classAndMethodName + " calling prepareReportDataSource(request, response)");
				
				//Prepare report data
				dataSource = prepareReportDataSource(request, response);
				
				logger.debug(classAndMethodName + " calling createReportParameters(request, response)");
				
				//Override if you want to provide report params 
				parameters = createReportParameters(request, response);
				
				logger.debug(classAndMethodName + " calling setStatus(request, Compile PDF template, 0.5d)");
				
				setStatus(request, "Compile PDF template", 0.5d); //50% complete		
				File templateFile = getTemplateFile(request);
				
				logger.debug(classAndMethodName + " creating jasperFile. absolutePath is: " + templateFile.getAbsolutePath() + "  , REPORT_EXTENSION is: " + REPORT_EXTENSION + "  , BINARY_EXTENSION: " + BINARY_EXTENSION);
				
				File jasperFile = new File( StringUtils.replace(templateFile.getAbsolutePath(), REPORT_EXTENSION, BINARY_EXTENSION));
				if(!jasperFile.exists() || jasperFile.lastModified() < templateFile.lastModified()){
					logger.debug(classAndMethodName + " No jasperFile existed. So BEFORE compiling report to File. Compile jrxml to binary format.");
					
					JasperCompileManager.compileReportToFile(templateFile.getAbsolutePath(), jasperFile.getAbsolutePath());//Compile jrxml to binary format
					
					logger.debug(classAndMethodName + " No jasperFile existed. AFTER compiling report to File. Compile jrxml to binary format.");
					
				}
				
				logger.debug(classAndMethodName + " calling setStatus(request, Filling PDF data, 0.8d)");
				
		    	setStatus(request, "Filling PDF data", 0.8d); //80% complete
		    	//getResourceAsStream only works with relative path. So it's required to convert the absolute path 
		    	String relativeTemplateURI = getRelativePath(templateFile);
		    	
		    	String binaryFilePath = StringUtils.replace(relativeTemplateURI, REPORT_EXTENSION, BINARY_EXTENSION);
		    	
		    	logger.debug(classAndMethodName + " Before Load binary file into report stream. binaryFilePath is: " + binaryFilePath);

		    	reportStream = servletConfig.getServletContext().getResourceAsStream(binaryFilePath);//Load binary file into report stream
		    	JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportStream);
		    	
		    	logger.debug(classAndMethodName + " After Load binary file into report stream. binaryFilePath is: " + binaryFilePath);
		    	
		    	LocalJasperReportsContext ctx = iniJasperReportsContext(request);
		    	JasperFillManager fillmgr = JasperFillManager.getInstance(ctx);
		    	
		    	logger.debug(classAndMethodName + " Before Fill the report with data from dataSource");
		    	
		    	JasperPrint jasperPrint = fillmgr.fill(jasperReport, parameters, dataSource);//Fill the report with data from dataSource
		    	
		    	logger.debug(classAndMethodName + " After Fill the report with data from dataSource");
		    	
		    	JasperExportManager exmgr = JasperExportManager.getInstance(ctx);
		    	
		    	logger.debug(classAndMethodName + " Before exmgr.exportToPdfStream()");
		    	
		    	exmgr.exportToPdfStream(jasperPrint,  response.getOutputStream());
		    	
		    	logger.debug(classAndMethodName + " After exmgr.exportToPdfStream()");
		    	
		    	logger.debug(classAndMethodName + "Before postReportProcess(request, response, exmgr, mainJasperPrint)");
		    	postReportProcess(request, response, exmgr, jasperPrint);
		    	logger.debug(classAndMethodName + "After postReportProcess(request, response, exmgr, mainJasperPrint)");
		    	
		    } finally {
		    	
		    	logger.debug(classAndMethodName + " Before finally block execution");
		    	
		    	if(DeveloperPdfGenerator.loadedJars && isDeveloperPDF())
		    		Thread.currentThread().setContextClassLoader(originalLoader);
		    	
		    	logger.debug(classAndMethodName + " After finally block execution");
		    }
		}catch(JRException e){
			// display error
			setStatus(request, "Error: - " + e.getMessage(), 0.8d);
			
			e.printStackTrace();
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
			logger.debug(classAndMethodName + " Outer finally block started.");
			
			if(servletOutputStream != null){
				servletOutputStream.flush();
				servletOutputStream.close();
			}
			
			logger.debug(classAndMethodName + " Outer finally block ended.");
		}
		
		logger.debug(classAndMethodName + " Ended.");
	}
	
	/**
	 * @return the developerPDF
	 */
	public boolean isDeveloperPDF() {
		return developerPDF;
	}

	/**
	 * @param developerPDF the developerPDF to set
	 */
	public void setDeveloperPDF(boolean developerPDF) {
		this.developerPDF = developerPDF;
	}
	
	/**
	 * Initiate JasperReportContext and set FileResolver to the context.
	 * This step is essential to allow image to be load from relative path 
	 * without changing file path to server path
	 * 
	 * @param request
	 * @return
	 * @throws FileNotFoundException
	 */
	protected LocalJasperReportsContext iniJasperReportsContext(HttpServletRequest request) throws FileNotFoundException{
		
		String classAndMethodName = this.getClass() + " iniJasperReportsContext(request) ";
		logger.debug(classAndMethodName + " Started.");
		
		ServletContext context = request.getSession().getServletContext();
		//REPORT_FILE_RESOLVER needs to be passed in to load images using relative path
		String reportsDirPath = context.getRealPath(IMAGE_PATH);
		File reportsDir = new File(reportsDirPath);
		if (!reportsDir.exists()) {
		    throw new FileNotFoundException(String.valueOf(reportsDir));
		}
		
		logger.debug(classAndMethodName + " LocalJasperReportsContext setting LocalJasperReportsContext and setFileResolver");
		
    	LocalJasperReportsContext ctx = new LocalJasperReportsContext(DefaultJasperReportsContext.getInstance());
    	ctx.setClassLoader(getClass().getClassLoader());
    	ctx.setFileResolver(new SimpleFileResolver(reportsDir));
    	
    	logger.debug(classAndMethodName + " Ended.");
    	
    	return ctx;
	}
	
	
	protected JasperReport loadReport(String templateFilename) throws Exception
	{
		String classAndMethodName = this.getClass() + " loadReport(templateFilename) ";
		logger.debug(classAndMethodName + " Started.");

		File templateFile = new File(templateFilename);
		File jasperFile = new File(templateFilename.replace(REPORT_EXTENSION, BINARY_EXTENSION));
		if (!jasperFile.exists() || jasperFile.lastModified() < templateFile.lastModified())
		{
			JasperCompileManager.compileReportToFile(templateFile.getAbsolutePath(), jasperFile.getAbsolutePath());
		}
		
		logger.debug(classAndMethodName + " It will end after loading jasperFile: " + jasperFile);
		
        return (JasperReport)JRLoader.loadObject(jasperFile);
	}	
	
	/**
	 * This function will be used in Drip Configuration page to load Drip template in Template Dropbox.
	 * As developer sometimes forgets to implement this, I put the default implementation here.
	 * 
	 * @return
	 */
	public String getPdfSimpleName() {		
		return (developerPDF)? DEVELOPER_DRIP_TEMPLATE: DETAIL_DRIP_TEMPLATE;
	}
	
	/**
	 * This function will be used in Drip Configuration page to load Drip template in Template Dropbox.
	 * 
	 * @return
	 */
	public String getDripCount() {
		return "0";
	}	
	
	/**
	 * To be overridden for default page event behavior
	 */
	protected abstract JRDataSource prepareReportDataSource(HttpServletRequest request, HttpServletResponse response);

	protected abstract HashMap<String, Object> createReportParameters(HttpServletRequest request, HttpServletResponse response);
	
	protected void postReportProcess(HttpServletRequest request, HttpServletResponse response, JasperExportManager exmgr, JasperPrint mainJasperPrint) throws JRException, IOException {
		// Implement this child class if you want to gain access to the generated PDF report
	}
}