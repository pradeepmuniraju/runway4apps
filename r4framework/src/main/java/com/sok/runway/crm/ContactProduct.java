package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.security.*;

import javax.servlet.ServletRequest;
import java.sql.Connection;


public class ContactProduct extends LinkableProduct {

public static final String CONTACT_PRODUCT_VIEWSPEC = "ContactProductView";
   
   public ContactProduct(Connection con) {
		super(con, CONTACT_PRODUCT_VIEWSPEC);
	}
		
	public ContactProduct(ServletRequest request) {
		super(request, CONTACT_PRODUCT_VIEWSPEC);
		populateFromRequest(request.getParameterMap());
	}
	   
	public ContactProduct(Connection con, String contactProductID) {
		this(con);
	   load(contactProductID);
	}
   
   public Validator getValidator() {
		Validator val = Validator.getValidator("ContactProducts");
		if (val == null) {
			val = new Validator();
			val.addMandatoryField("ContactProductID","SYSTEM ERROR - ContactProductID");
			val.addMandatoryField("ProductID","SYSTEM ERROR - ProductID");
			val.addMandatoryField("ContactID","SYSTEM ERROR - ProductID");
			Validator.addValidator("ContactProducts", val);
		}
		return val;
   }
}