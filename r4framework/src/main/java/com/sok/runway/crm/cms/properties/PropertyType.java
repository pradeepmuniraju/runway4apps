package com.sok.runway.crm.cms.properties;

import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

public enum PropertyType { Apartment, Brand, Building, BuildingStage, Home, HomeOption, 
									HomePlan, HomePlanFeature, HomeRange, HouseLandPackage, 
									Estate, Land, Stage, Facade, PackageCost, Unknown, Developer, ExistingProperty, Range; 
	
	private static final Logger logger = LoggerFactory.getLogger(PropertyType.class);

	public static PropertyType getPropertyType(String propertyType) {
		logger.debug("PropertyType checking [{}]", propertyType);
		if(StringUtils.isBlank(propertyType)) return PropertyType.Unknown;
		
		if("Home Plan Feature".equals(propertyType)) {
			logger.trace("Was {}", HomePlanFeature.name());
			return HomePlanFeature;
		} else if ("House and Land".equals(propertyType)) {
			logger.trace("Was {}", HouseLandPackage.name());
			return HouseLandPackage;
		} else if ("Home Facade".equals(propertyType)) {
			logger.trace("Was {}", Facade.name());
			return Facade;
		} else {
			try {
				PropertyType pt = valueOf(propertyType.replaceAll(" ",""));
				logger.trace("Was {}", pt.name());	
				return pt;
			} catch (IllegalArgumentException iae) { 
				logger.error("PropertyType [{}] as not found", propertyType);
				return Unknown;
			}
		}
	}
	public String getProductType() { 
		switch(this) {
			case BuildingStage:
				return "Building Stage";
			case HomeOption:
				return "Home Option";
			case HomePlan:
				return "Home Plan";
			case Range:
				return "Home Range";
			case HomePlanFeature:
				return "Plan Feature";
			case HomeRange:
				return "Home Range";
			case HouseLandPackage:
				return "House and Land";
			case Facade:
				return "Home Facade";
			case PackageCost:
				return "Package Cost";
			case ExistingProperty: 
				return "Existing Property";
			default:
				return this.name();
		}
	}
}
