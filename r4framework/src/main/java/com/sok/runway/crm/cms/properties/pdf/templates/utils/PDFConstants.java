package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import com.itextpdf.text.Paragraph;

public interface PDFConstants {
	public static String arialFont = "Arial";
	public static String gothamFontBook = "Gotham-Book.ttf";
	public static String gothamFontLight = "Gotham-Light.ttf";
	public static final float fontRatio = 0.63f;// relative size of the web version in pixels to the
												// pdf version.
	public static final Paragraph BLANK = new Paragraph("");
	public static final boolean FORCE_TRUE = false;

}
