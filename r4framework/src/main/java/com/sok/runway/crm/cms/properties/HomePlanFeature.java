package com.sok.runway.crm.cms.properties;

import javax.servlet.http.HttpServletRequest;

import java.sql.Connection;
import com.sok.framework.generation.GenerationKeys;

public class HomePlanFeature extends PropertyEntity {

	final PropertyType propertyType = PropertyType.HomePlanFeature;
	static final String VIEWSPEC = "properties/ProductPlanFeatureView";

	public HomePlanFeature(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public HomePlanFeature(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	public HomePlanFeature(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
}
