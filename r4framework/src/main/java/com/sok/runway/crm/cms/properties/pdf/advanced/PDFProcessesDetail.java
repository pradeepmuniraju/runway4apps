/**
 * 
 */
package com.sok.runway.crm.cms.properties.pdf.advanced;

import java.text.NumberFormat;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.sog.pdfserver.client.api.request.PDFRequest;
import com.sog.pdfserver.client.api.request.PageStaticData;
import com.sog.pdfserver.client.api.request.TemplateListTag;
import com.sok.framework.GenRow;
import com.sok.framework.RunwayUtil;
import com.sok.framework.TableData;
import com.sok.runway.crm.cms.properties.PropertyEntity;

/**
 * @author Dion Chapman
 *
 */
public class PDFProcessesDetail extends PDFProductDetail {

	public static String PREFIX_PROCESS = "Process_";
	
	/**
	 * 
	 */
	public PDFProcessesDetail() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void populateSpecificTags(HttpServletRequest request, Map<String,String> tags, List<TemplateListTag> listTags, PropertyEntity property, PDFRequest pdfRequest) {
		// Child class can override, pdfRequest is passed so that if needed the template name can be overridden by the child class if needed
		//super.populateSpecificTags(request, tags, listTags, property, pdfRequest);
		
		String opportunityID = request.getParameter("OpportunityID");
		
		if (StringUtils.isBlank(opportunityID)) {
			throw  new RuntimeException("Opportunity was not found");
		}

		java.util.List<PageStaticData> pageStaticDataList = new java.util.ArrayList<PageStaticData>();

		GenRow opp = new GenRow();
		opp.setViewSpec("OpportunityView");
		opp.setRequest(request);
		opp.setParameter("OpportunityID", opportunityID);
		opp.doAction("selectfirst");

		convertRow2Tags(getHomePath(request), opp, tags, PREFIX_PROCESS);

		GenRow answers = new GenRow();
		answers.setRequest(request);
		
		answers.setViewSpec("answers/ContactAnswerSurveyView");
		if (opp.isSet("ContactID")) {
			GenRow primaryContact = getContact(request, opp.getString("ContactID"));
			convertRow2Tags(getHomePath(request), primaryContact, tags, PREFIX_PROCESS + "Contact_");
			loadLinkedtags(primaryContact, tags, PREFIX_PROCESS + "Contact_");
			
			GenRow link = new GenRow();
			link.setViewSpec("LinkeeContactListView");
			link.setRequest(request);
			link.setParameter("LinkerID", opp.getString("ContactID"));
			link.setParameter("LinkedIn", "Y");
			link.doAction("search");
			link.getResults();

			while (link.getNext()) {
				if (link.getString("LinkeeID").length() > 0) {
					GenRow contact = getContact(request, link.getString("LinkeeID"));
					loadLinkedtags(contact, tags, PREFIX_PROCESS + "Contact_");
					convertRow2Tags(getHomePath(request), contact, tags, PREFIX_PROCESS + "Contact_" + link.getString("LinkType").replaceAll("[^a-zA-Z0-9]", "") + "_");
				}
			}
			link.close();
			
			answers.clear();
			answers.setParameter("ContactID", opp.getString("ContactID"));
			answers.doAction("search");

			answers.getResults();

			while (answers.getNext()) {
				convertRow2Tags(getHomePath(request), answers, tags, PREFIX_PROCESS + "Contact_" + answers.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
			}
			
		}
		if (opp.isSet("RepUserID")) convertRow2Tags(getHomePath(request), RunwayUtil.getUser(request, opp.getString("RepUserID")), tags, PREFIX_PROCESS + "Rep_");
		
		GenRow stage = new GenRow();
		stage.setViewSpec("OpportunityStageStageView");
		stage.setRequest(request);
		stage.setParameter("OpportunityID", opportunityID);
		
		stage.doAction("search");
		
		stage.getResults();
		
		answers.setViewSpec("answers/OpportunityAnswerView");
		
		
		while (stage.getNext()) {
			answers.clear();
			answers.setParameter("OpportunityID", opportunityID);
			answers.setParameter("ProcessStageID", stage.getString("ProcessStageID"));
			answers.doAction("search");

			answers.getResults();

			while (answers.getNext()) {
				convertRow2Tags(getHomePath(request), answers, tags, PREFIX_PROCESS + stage.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
			}
			
			convertRow2Tags(getHomePath(request), stage, tags, PREFIX_PROCESS + stage.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_");
			if (opp.getString("ProcessStageID").equals(stage.getString("ProcessStageID"))) {
				convertRow2Tags(getHomePath(request), stage, tags, PREFIX_PROCESS + "CurrentStage_");

				answers.getResults();

				while (answers.getNext()) {
					convertRow2Tags(getHomePath(request), answers, tags, PREFIX_PROCESS + "CurrentStage_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
				}
			}
		}
		
		answers.close();
		
		if (StringUtils.isNotBlank(request.getParameter("ViewStageID"))) {
			stage.setParameter("ProcessStageID", request.getParameter("ViewStageID"));
		
			stage.doAction("selectfirst");
		
			convertRow2Tags(getHomePath(request), stage, tags, PREFIX_PROCESS +  "CurrentViewStage_");
		}
		
		stage.close();
		
		GenRow link = new GenRow();
		link.setTableSpec("OpportunityContacts");
		link.setRequest(request);
		link.setParameter("-select0", "ContactID");
		link.setParameter("-select1", "Role");
		link.setParameter("OpportunityID", opportunityID);
		
		link.doAction("search");
		
		link.getResults();
		
		answers.setViewSpec("answers/ContactAnswerSurveyView");
		while (link.getNext()) {
			if (link.isSet("ContactID")) {
				convertRow2Tags(getHomePath(request), getContact(request, link.getString("ContactID")), tags, PREFIX_PROCESS + "Contact_" + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + "_");

				answers.clear();
				answers.setParameter("ContactID", link.getString("ContactID"));
				answers.doAction("search");

				answers.getResults();

				while (answers.getNext()) {
					convertRow2Tags(getHomePath(request), answers, tags, PREFIX_PROCESS + "Contact_" + link.getString("Role").replaceAll(" ", "_") + "_" + answers.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
				}
			}
		}
		
		link.clear();
		link.setTableSpec("OpportunityCompanies");
		link.setRequest(request);
		link.setParameter("-select0", "CompanyID");
		link.setParameter("-select1", "Role");
		link.setParameter("OpportunityID", opportunityID);
		
		link.doAction("search");
		
		link.getResults();
		
		answers.setViewSpec("answers/CompanyAnswerSurveyView");
		while (link.getNext()) {
			if (link.isSet("CompanyID")) {
				convertRow2Tags(getHomePath(request), getCompany(request, link.getString("CompanyID")), tags, PREFIX_PROCESS + "Company_" + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + "_");

				answers.clear();
				answers.setParameter("CompanyID", link.getString("CompanyID"));
				answers.doAction("search");

				answers.getResults();

				while (answers.getNext()) {
					convertRow2Tags(getHomePath(request), answers, tags, PREFIX_PROCESS + "Company_" + link.getString("Role").replaceAll("[^a-zA-Z0-9]", "") + "_" + answers.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
				}
			}
		}
		
		link.close();

		if (StringUtils.isNotBlank(request.getParameter("ContactID"))) {
			convertRow2Tags(getHomePath(request), getContact(request, request.getParameter("ContactID")), tags, PREFIX_PROCESS + "Contact_Selected_");

			answers.clear();
			answers.setParameter("ContactID", opp.getString("ContactID"));
			answers.doAction("search");

			answers.getResults();

			while (answers.getNext()) {
				convertRow2Tags(getHomePath(request), answers, tags, PREFIX_PROCESS + "Contact_Selected_" + answers.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Question_" + answers.getString("QuestionLabel").replaceAll("[^a-zA-Z0-9]", "") + "_");
			}
		}
		
		getPayments(request, tags, listTags, property, pdfRequest);
		
		setStatus(request, "Loading tags completed", 0.5d);
	}
	
	private GenRow getContact(HttpServletRequest request, String contactID) {
		GenRow row = new GenRow();
		
		if (StringUtils.isBlank(contactID)) return row;
		
		row.setViewSpec("ContactView");
		row.setRequest(request);
		row.setParameter("ContactID", contactID);
		
		row.doAction("selectfirst");
		
		return row;
	}
	
	private GenRow getCompany(HttpServletRequest request, String companyID) {
		GenRow row = new GenRow();
		
		if (StringUtils.isBlank(companyID)) return row;
		
		row.setViewSpec("CompanyView");
		row.setRequest(request);
		row.setParameter("CompanyID", companyID);
		
		row.doAction("selectfirst");
		
		return row;
	}
	
	private void getPayments(HttpServletRequest request, Map<String,String> tags, List<TemplateListTag> listTags, PropertyEntity property, PDFRequest pdfRequest) {
		String opportunityID = request.getParameter("OpportunityID");
		
		if (StringUtils.isBlank(opportunityID)) return;
		
      	GenRow comms = new GenRow();
      	comms.setViewSpec("CommissionView");
      	comms.setRequest(request);
      	/*
      	if (request.getParameter("CommissionName") != null && request.getParameter("CommissionName").length() > 0) {
      		payments.setColumn(commissionSortLong,"%" + request.getParameter("CommissionName") + "%");
      	}
      	if (request.getParameter("CommissionRole") != null && request.getParameter("CommissionRole").length() > 0) {
      		payments.setColumn("LinkType",request.getParameter("CommissionRole"));
      	}*/
      	comms.setColumn("OpportunityID", opportunityID);
		if (request.getParameter("commissionID") != null && request.getParameter("commissionID").length() > 0) {
			comms.setColumn("CommissionID",request.getParameter("commissionID"));
		}
		comms.setColumn("CommissionType","payments");
      	//payments.sortBy(commissionSort,0);
      	//payments.setParameter("-groupby0",commissionGroup);
		comms.doAction("search");
		comms.getResults();
		
		GenRow payments = new GenRow();
		payments.setViewSpec("CommissionPaymentsView");
		payments.setRequest(request);
		
      	while (comms.getNext()) {
      		double totalDue = 0, totalPaid = 0;
      		payments.clear();
      		payments.setParameter("CommissionID", comms.getString("CommissionID"));
      		payments.sortBy("SortOrder", 0);
      		payments.doAction("search");
      		
      		payments.getResults();
      		
      		while (payments.getNext()) {
				convertRow2Tags(getHomePath(request), payments, tags, PREFIX_PROCESS + "Payments_" + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_Payment_" + payments.getString("Description").replaceAll("[^a-zA-Z0-9]", "") + "_");
				convertRow2Tags(getHomePath(request), payments, tags, PREFIX_PROCESS + "Payments_AnyTemplate_Payment_" + payments.getString("Description").replaceAll("[^a-zA-Z0-9]", "") + "_");
				
				totalDue += payments.getDouble("DueAmount");
				totalPaid += payments.getDouble("PaidAmount");
      		}
      		
      		addTag(getHomePath(request), tags, PREFIX_PROCESS + "Payments_" + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_", "TotalPaid", "" + totalPaid);
      		addTag(getHomePath(request), tags, PREFIX_PROCESS + "Payments_" + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_", "TotalDue", "" + totalDue);
      		addTag(getHomePath(request), tags, PREFIX_PROCESS + "Payments_" + comms.getString("Name").replaceAll("[^a-zA-Z0-9]", "") + "_", "TotalOutStanding", "" + (totalDue - totalPaid));

      		addTag(getHomePath(request), tags, PREFIX_PROCESS + "Payments_AnyTemplate_", "TotalPaid", "" + totalPaid);
      		addTag(getHomePath(request), tags, PREFIX_PROCESS + "Payments_AnyTemplate_", "TotalDue", "" + totalDue);
      		addTag(getHomePath(request), tags, PREFIX_PROCESS + "Payments_AnyTemplate_", "TotalOutStanding", "" + (totalDue - totalPaid));
      	}
      	comms.close();
      	payments.close();
	}
	
	private String[]		tokens = {"Title","FirstName","LastName","Salutation","Company"};
	protected void loadLinkedtags(GenRow linkedcontext, Map<String,String> tags, String pretext)
	{
		if (!linkedcontext.isSet("ContactID")) return;
		
		for (int t = 0; t < tokens.length; ++t) {
			if (StringUtils.isBlank(tags.get(pretext + "Linked" + tokens[t]))) {
				tags.put(pretext + "Linked" + tokens[t], linkedcontext.getString(tokens[t]));
			} else {
				String value = (String) tags.get(pretext + "Linked" + tokens[t]);
				value = value.replaceAll(" and ",", ") + " and " + linkedcontext.get(tokens[t]);
				if (value.endsWith(" and ")) value = value.replaceAll(" and ", "");
				tags.put(pretext + "Linked" + tokens[t], value);
			}
		}

		if (StringUtils.isBlank(tags.get(pretext + "LinkedNames"))) {
			tags.put(pretext + "LinkedNames", (linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim());
		} else {
			String value = (String) tags.get(pretext + "LinkedNames");
			value = value.replaceAll(" and ",", ") + " and " + (linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim();
			if (value.endsWith(" and ")) value = value.replaceAll(" and ", "");
			tags.put(pretext + "LinkedNames", value);
		}

		if (StringUtils.isBlank(tags.get(pretext + "LinkedFormalNames"))) {
			tags.put(pretext + "LinkedFormalNames", (linkedcontext.get("Title") + " " + linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim());
		} else {
			String value = (String) tags.get(pretext + "LinkedFormalNames");
			value = value.replaceAll(" and ",", ") + " and " + (linkedcontext.get("Title") + " " + linkedcontext.get("FirstName") + " " + linkedcontext.get("LastName")).trim();
			if (value.endsWith(" and ")) value = value.replaceAll(" and ", "");
			tags.put(pretext + "LinkedFormalNames", value);
		}
	}
	
}
