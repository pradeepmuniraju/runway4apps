package com.sok.runway.crm;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Holiday extends GregorianCalendar { 
	
	public static final int WORLD = 42;
	public static final int AUSTRALIA = 1;
	public static final int UK = 2;
	
	public static final String STATE_ALL = "ALL"; 
	public static final String AUS_VIC = "VIC";
	public static final String AUS_NSW = "NSW";
	public static final String AUS_QLD = "QLD";
	public static final String AUS_NT = "NT";
	public static final String AUS_WA = "WA";
	public static final String AUS_SA = "SA";
	public static final String AUS_TAS = "TAS";
	public static final String AUS_ACT = "ACT";
	
	public static final String _blank = "";
	
	private int country; 
	private String state; 
	private String except; 
	
	/**
	 * AUS - STATE_ALL = NSW/VIC
	 * MUST BE IN CHRONOLOGICAL ORDER
	 * MONTH SPECIFIED 1 = JAN, 12 = DEC FOR EASE OF USE
	 * http://www.ocpe.nt.gov.au/legislation/holidays
	 * http://www.industrialrelations.nsw.gov.au/holidays/index.html
	 * http://www.wageline.qld.gov.au/holidayslist/index.html
	 * http://www.safework.sa.gov.au/show_page.jsp?id=2483#item33650
	 * http://www.docep.wa.gov.au/LabourRelations/Content/Wages%20and%20Leave/Holidays/index.htm
	 * http://www.wst.tas.gov.au/employment_info/public_holidays
	 * http://www.cmd.act.gov.au/holidays
	 */
	public static final Holiday[] HOLIDAYS  = {
		
		/* 2008 */ 
		
		new Holiday(WORLD, STATE_ALL,2008, 1, 1), /* new years day */ 
		new Holiday(AUS_TAS,2008, 1, 9), /* devonport cup (TAS) */
		new Holiday(STATE_ALL,2008, 1, 28), /* australia day */ 
		new Holiday(AUS_TAS,2008, 2, 11), /* hobart regatta (TAS) */
		new Holiday(AUS_TAS,2008, 2, 27), /* launceston cup (TAS) */
		new Holiday(AUS_WA,2008, 3, 3), /* labour day (WA) */
		new Holiday(AUS_TAS,2008, 3, 4), /* king island show (TAS) */
		new Holiday(AUS_TAS,2008, 3, 10), /* eight hours day (TAS) */
		new Holiday(AUS_VIC,2008, 3, 10), /* vic labour day */
		new Holiday(AUS_ACT,2008, 3, 10), /* canberra day (ACT) */
		new Holiday(AUS_SA,2008, 3, 10), /* cup day (SA) */ 
		new Holiday(WORLD,STATE_ALL,2008, 3, 21), /* good friday (easter sat skipped as we don't count weekends anyway) */ 
		new Holiday(WORLD,STATE_ALL,2008, 3, 24), /* easter monday */
		new Holiday(STATE_ALL,2008, 4, 25), /* ANZAC DAY */ 
		new Holiday(AUS_NT,2008, 5, 5), /* may day (NT) */
		new Holiday(AUS_QLD,2008, 5, 5), /* labour day (QLD) */
		new Holiday(UK,STATE_ALL,2008, 5, 5), /* early may bank holiday */
		new Holiday(UK,STATE_ALL,2008, 5, 25), /* spring bank holiday */
		new Holiday(AUS_WA,2008, 6, 2), /* foundation day (WA) */
		new Holiday(STATE_ALL,2008, 6, 9, AUS_WA), /* queens bday */
		new Holiday(AUS_NSW,2008, 8, 4), /* nsw bank holiday */
		new Holiday(AUS_NT,2008, 8, 4), /* picnic day (NT) */
		new Holiday(AUS_QLD,2008, 8, 13), /* show day (QLD/brisbane) */
		new Holiday(UK,STATE_ALL,2008, 8, 25), /* Summer bank holiday */
		new Holiday(AUS_WA,2008, 9, 29), /* Queens Birthday (WA) */
		new Holiday(AUS_TAS,2008, 10, 3), /* burnie show(TAS) */
		new Holiday(AUS_NSW,2008, 10, 6), /* nsw labour day */
        new Holiday(AUS_SA,2008, 10, 6), /* sa labour day */
        new Holiday(AUS_ACT,2008, 10, 6), /* labour day (ACT) */
        new Holiday(AUS_TAS,2008, 10, 9), /* launceston show(TAS) */
        new Holiday(AUS_TAS,2008, 10, 17), /* flinders island show(TAS) */
        new Holiday(AUS_TAS,2008, 10, 23), /* hobart show(TAS) */
        new Holiday(AUS_TAS,2008, 11, 3), /* recreation day northern tas (TAS) */
		new Holiday(AUS_VIC,2008, 11, 4), /* cup day */
		new Holiday(AUS_ACT,2008, 11, 4), /* family and community day (ACT) */ 
		new Holiday(AUS_TAS,2008, 11, 28), /* devonport show(TAS) */
		new Holiday(WORLD, STATE_ALL,2008, 12, 25), /* xmas day */ 
		new Holiday(STATE_ALL,2008, 12, 26), /* boxing day 08 */ 
		
		/* 2009 */ 
		
		new Holiday(STATE_ALL,2009, 1, 1), /* new years day */ 
		new Holiday(AUS_TAS,2009, 1, 7), /* devonport cup (TAS) */
		new Holiday(STATE_ALL,2009, 1, 26), /* australia day */ 
		new Holiday(AUS_TAS,2009, 2, 9), /* hobart regatta (TAS) */
		new Holiday(AUS_TAS,2009, 2, 25), /* launceston cup (TAS) */
		new Holiday(AUS_VIC,2009, 3, 2), /* labour day (WA) */
		new Holiday(AUS_TAS,2009, 3, 3), /* king island show (TAS) */
		new Holiday(AUS_TAS,2009, 3, 9), /* eight hours day (TAS) */
		new Holiday(AUS_VIC,2009, 3, 9), /* labour day */
		new Holiday(AUS_ACT,2009, 3, 9), /* canberra day (ACT) */ 
		new Holiday(AUS_SA,2009, 3, 9), /* cup day (SA) */ 
		new Holiday(STATE_ALL,2009, 4, 10), /* good friday (easter sat skipped as we don't count weekends anyway) */ 
		new Holiday(STATE_ALL,2009, 4, 13), /* easter monday */ 
		new Holiday(AUS_WA,2009, 4, 27), /* ANZAC DAY Holiday*/ 
		new Holiday(UK,STATE_ALL,2009, 5, 4), /* early may bank holiday */
		new Holiday(AUS_QLD,2009, 5, 4), /* labour day (QLD) */
		new Holiday(AUS_NT,2009, 5, 4), /* may day (NT) */
		new Holiday(UK,STATE_ALL,2009, 5, 25), /* spring bank holiday */
		new Holiday(AUS_WA,2009, 6, 1), /* foundation day (WA) */
		new Holiday(STATE_ALL,2009, 6, 8, AUS_WA), /* queens bday */ 
		new Holiday(AUS_NSW,2009, 8, 3), /* nsw bank holiday */
		new Holiday(AUS_NT,2009, 8, 3), /* picnic day (NT) */
		new Holiday(AUS_NT,2009, 8, 12), /* show day (QLD/brisbane) */
		new Holiday(UK,STATE_ALL,2009, 8, 31), /* Summer bank holiday */
		new Holiday(AUS_WA,2009, 9, 28), /* Queens Birthday (WA) */
		new Holiday(AUS_TAS,2009, 10, 2), /* burnie show(TAS) */ 
		new Holiday(AUS_NSW,2009, 10, 5), /* nsw labour day */
		new Holiday(AUS_SA,2009, 10, 5), /* labour day (SA)*/
		new Holiday(AUS_ACT,2009, 10, 5), /* labour day (ACT)*/
		new Holiday(AUS_TAS,2009, 10, 8), /* launceston show(TAS) */
		new Holiday(AUS_TAS,2009, 10, 16), /* flinders island show (TAS) */
        new Holiday(AUS_TAS,2009, 10, 22), /* hobart show(TAS) */
        new Holiday(AUS_TAS,2009, 11, 2), /* recreation day northern tas (TAS) */
		new Holiday(AUS_VIC,2009, 11, 3), /* cup day */
		new Holiday(AUS_VIC,2009, 11, 3), /* family day (ACT) */ 
		new Holiday(AUS_TAS,2009, 11, 27), /* devonport show(TAS) */
		new Holiday(WORLD, STATE_ALL,2009, 12, 25), /* xmas day */
		new Holiday(AUS_SA,2009, 12, 28), /* proclamation day (SA) */
		new Holiday(AUS_NT,2009, 12, 28), /* boxing day (NT) */
		new Holiday(AUS_TAS,2009, 12, 28), /* boxing day (TAS) */
		new Holiday(AUS_ACT,2009, 12, 28), /* boxing day (ACT) */
		new Holiday(AUS_WA,2009, 12, 28) /* boxing day (WA) */ 
		};
	
	public Holiday (String state, int year, int month, int day) { 
		this(AUSTRALIA,state, year, month, day, null);
	}
	
	public Holiday (String state, int year, int month, int day, String except) { 
		this(AUSTRALIA,state, year, month, day, except);
	}
	
	public Holiday (int country, String state, int year, int month, int day) {
		this(country,state, year, month, day, null);
	}
	
	public Holiday (int country, String state, int year, int month, int day, String except) {
		super(year, (month-1), day, 0, 0); 
		this.country = country; 
		this.state = state; 
		this.except = except; 
	} 
	
	public String getState() { 
		return state; 
	}

	public String getExcept() { 
		return except!=null?except:_blank; 
	}

	public int getCountry() { 
		return country; 
	}

	public static boolean isBusinessDay(String state, int country, Calendar c) { 
		return !(isWeekend(c) || isHoliday(state, country, c)); 
	} 
	
	public static boolean isHoliday(String state, Calendar c) { 
		return isHoliday(state, AUSTRALIA, c); 
	}

	public static boolean isHoliday(String state, int country, Calendar c) {
		
		for(Holiday hol:HOLIDAYS) {
			if((country == hol.getCountry() || hol.getCountry() == WORLD) && ((state.equals(hol.getState()) || STATE_ALL.equals(hol.getState())) && !state.equals(hol.getExcept()))) { 

				if(c.getTimeInMillis() >=hol.getTimeInMillis() && c.getTimeInMillis() <=(hol.getTimeInMillis()+86400000)) { 
					return true; 
				} else if (hol.getTimeInMillis() > c.getTimeInMillis()) {
					return false; 
				}
			}
		} 
		return false; 
	}
	
	public static boolean isWeekend(Calendar c) { 
		return (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);
	}
}
