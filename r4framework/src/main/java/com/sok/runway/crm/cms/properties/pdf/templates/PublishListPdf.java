package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.collections.comparators.NullComparator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.PublishInfo;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PublishListHeaderFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PublishUtil;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;

public class PublishListPdf extends AbstractPDFTemplate implements PDFConstants {
	private static final Logger logger = LoggerFactory.getLogger(PublishListPdf.class);

	public static final BaseColor blueColor = new BaseColor(0, 42, 84);
	public static final BaseColor normalRowFontColor = new BaseColor(51, 51, 51);

	public static final BaseColor whiteColor = new BaseColor(255, 255, 255);
	public static final BaseColor blackColor = new BaseColor(0, 0, 0);

	public static final BaseColor firstHeaderColor = new BaseColor(43, 43, 43); // #2B2B2B
	public static final BaseColor secondHeaderColor = new BaseColor(153, 153, 153);
	public static final BaseColor thirdHeaderColor = new BaseColor(196, 196, 196); // #C4C4C4
	public static final BaseColor fourthHeaderColor = new BaseColor(221, 221, 221);
	public static final BaseColor fifthHeaderColor = new BaseColor(204, 242, 238);

	public static final Font arialPt10Grey = FontFactory.getFont("Arial", fontRatio * 10, normalRowFontColor);
	public static final Font arialPt11Grey = FontFactory.getFont("Arial", fontRatio * 11, normalRowFontColor);
	public static final Font arialPt13Grey = FontFactory.getFont("Arial", fontRatio * 13, normalRowFontColor);
	public static final Font arialPt15Grey = FontFactory.getFont("Arial", fontRatio * 15, normalRowFontColor);
	public static final Font arialMediumPt17Grey = FontFactory.getFont("Arial", fontRatio * 17, normalRowFontColor);
	public static final Font arialPt19Grey = FontFactory.getFont("Arial", fontRatio * 19, normalRowFontColor);

	public static final Font arialPt14White = FontFactory.getFont("Arial", fontRatio * 14, whiteColor);
	public static final Font arialPt17White = FontFactory.getFont("Arial", fontRatio * 17, whiteColor);

	public static List<String> sortOrderList = null;
	public static PublishMethod[] pmList;

	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException, DocumentException {
		logger.debug("renderPDF(request, {})");

		List<PublishInfo> publishInfoList = PublishUtil.getPublishBean(request);

		pmList = PublishMethod.getVisibleMethods();

		try {

			String sortOption = request.getParameter("-sortOption").replaceAll(" ", "+");
			String groupBy = request.getParameter("-groupBy");
			
			if(StringUtils.isNotBlank(groupBy)) {
				sortOption = groupBy + "+" + sortOption;
			}
			
			String[] sortArray = sortOption.split("\\+");
			
			sortOrderList = Arrays.asList(sortArray);

		} catch (Exception e) {
			logger.debug("Error while determining the sort order, using default sort order. Exception is " + e);
			sortOrderList = Arrays.asList(new String[] { "salesRep", "salesRep", "range", "design" });
		}
		logger.debug("sortOrderList is " + sortOrderList.toString());

		ComparatorChain comparatorChain = new ComparatorChain();
		for (String orderParam : sortOrderList) {
			comparatorChain.addComparator(new BeanComparator(orderParam, new NullComparator(true)));
		}

		Collections.sort(publishInfoList, comparatorChain);

		renderPages(document, publishInfoList);
	}

	@Override
	public void setMargins(Document document) {
		document.setMargins(15, 30, 100, 0); // L R T B
	}

	private void renderPages(com.itextpdf.text.Document document, List<PublishInfo> publishInfoList) throws DocumentException {
		PdfPTable pageTable = new PdfPTable(1);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.setWidthPercentage(100f);

		if (publishInfoList != null) {
			pageTable.getDefaultCell().setPaddingTop(10);
			pageTable.getDefaultCell().setPaddingBottom(10);
			pageTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

			int fixedColumns = 8;
			int totalColumns = fixedColumns + pmList.length;
			PdfPTable bodyTable = new PdfPTable(totalColumns);
			bodyTable.setWidths(getTableWidths(totalColumns));

			bodyTable.getDefaultCell().setPadding(0);
			bodyTable.getDefaultCell().setPaddingBottom(3);
			bodyTable.getDefaultCell().setPaddingTop(3);
			bodyTable.getDefaultCell().setPaddingLeft(0);
			bodyTable.getDefaultCell().setBorder(1);
			bodyTable.getDefaultCell().setBorderWidth(0.1f);
			bodyTable.getDefaultCell().setBorderWidthTop(0.1f);
			bodyTable.getDefaultCell().setBorderWidthBottom(0.1f);
			bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			bodyTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

			for (int i = 0; i < publishInfoList.size(); i++) {
				PublishInfo prevInfo;

				if (i == 0) {
					prevInfo = new PublishInfo();
				} else {
					prevInfo = publishInfoList.get(i - 1);
				}

				addContentToBodyTable(bodyTable, publishInfoList.get(i), prevInfo, i, totalColumns);
			}
			pageTable.addCell(bodyTable);
		}

		document.add(pageTable);
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		return new PublishListHeaderFooter();
	}

	public String getPdfSimpleName() {
		return "Publish List PDF";
	}

	public static float[] getTableWidths(int totalColumns) {
		float fWidths[] = new float[totalColumns];

		int tmpCounter = 0;
		fWidths[tmpCounter++] = 40f;
		fWidths[tmpCounter++] = 65f;
		fWidths[tmpCounter++] = 40f;
		fWidths[tmpCounter++] = 15f; // Width
		fWidths[tmpCounter++] = 15f;
		fWidths[tmpCounter++] = 20f;
		fWidths[tmpCounter++] = 25f; // price
		fWidths[tmpCounter++] = 35f; // status
		for (int r = 0; r < pmList.length; r++) {
			fWidths[tmpCounter++] = 10f;
		}
		return fWidths;
	}

	public static PdfPTable getHeader() throws DocumentException {
		int fixedColumns = 8;
		int totalColumns = fixedColumns + pmList.length;

		PdfPTable headerTable = new PdfPTable(totalColumns);
		headerTable.setWidths(getTableWidths(totalColumns));

		headerTable.getDefaultCell().setPadding(0);
		headerTable.getDefaultCell().setPaddingBottom(4);
		headerTable.getDefaultCell().setPaddingTop(4);
		headerTable.getDefaultCell().setPaddingLeft(0);
		headerTable.getDefaultCell().setBorder(0);
		headerTable.getDefaultCell().setBorderWidth(0.0f);
		headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

		// Row 01
		headerTable.getDefaultCell().setColspan(3);
		headerTable.getDefaultCell().setBackgroundColor(blackColor);
		headerTable.addCell(new Paragraph("Publish List", arialPt17White));

		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateNow = formatter.format(currentDate.getTime());

		headerTable.getDefaultCell().setBorder(0);
		headerTable.getDefaultCell().setBorderWidth(0f);
		headerTable.getDefaultCell().setBorderWidthLeft(0f);
		headerTable.getDefaultCell().setBorderWidthRight(0f);
		headerTable.getDefaultCell().setBorderWidthTop(0f);
		headerTable.getDefaultCell().setBorderWidthBottom(0f);
		headerTable.getDefaultCell().setColspan(4);
		headerTable.getDefaultCell().setBackgroundColor(blackColor);
		headerTable.addCell(new Paragraph("Week ending: " + dateNow, arialPt17White));

		headerTable.getDefaultCell().setColspan(1);
		headerTable.getDefaultCell().setBackgroundColor(whiteColor);
		headerTable.addCell(BLANK);

		for (int r = 0; r < pmList.length; r++) {
			PublishMethod pm = pmList[r];
			PdfPCell verticalCell = new PdfPCell(new Paragraph(pm.getName(), new Font(arialPt14White)));
			verticalCell.setRotation(90);
			verticalCell.setRowspan(5);
			verticalCell.setRotation(90);
			verticalCell.setBackgroundColor(pm.getColor());
			headerTable.addCell(verticalCell);
		}
		headerTable.completeRow();

		// Row 02
		headerTable.getDefaultCell().setColspan(fixedColumns);
		headerTable.getDefaultCell().setBackgroundColor(whiteColor);
		headerTable.getDefaultCell().setPaddingBottom(10);
		headerTable.getDefaultCell().setPaddingTop(10);
		headerTable.addCell(BLANK);
		headerTable.getDefaultCell().setPaddingBottom(0);
		headerTable.getDefaultCell().setPaddingTop(0);
		headerTable.completeRow();

		headerTable.getDefaultCell().setColspan(1);
		headerTable.addCell(BLANK); // Column 1st
		headerTable.addCell(BLANK);
		headerTable.addCell(BLANK);
		headerTable.addCell(BLANK);
		headerTable.addCell(BLANK);
		headerTable.addCell(BLANK); // Column 6th
		headerTable.addCell(BLANK);
		headerTable.addCell(BLANK); // column 8th
		headerTable.completeRow();

		// Row 04
		headerTable.getDefaultCell().setColspan(fixedColumns);
		headerTable.getDefaultCell().setBackgroundColor(whiteColor);
		headerTable.getDefaultCell().setPaddingBottom(20);
		headerTable.getDefaultCell().setPaddingTop(20);
		headerTable.addCell(BLANK);
		headerTable.getDefaultCell().setPaddingBottom(0);
		headerTable.getDefaultCell().setPaddingTop(0);
		headerTable.completeRow();

		// Row 05
		headerTable.getDefaultCell().setColspan(1);
		headerTable.getDefaultCell().setPaddingBottom(4);
		headerTable.getDefaultCell().setPaddingTop(2);
		headerTable.getDefaultCell().setBackgroundColor(thirdHeaderColor);
		headerTable.getDefaultCell().setBorder(1);
		headerTable.getDefaultCell().setBorderWidthLeft(0.1f);
		headerTable.getDefaultCell().setBorderWidthRight(0.1f);
		headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

		headerTable.addCell(new Paragraph("Home/Plan", arialPt13Grey));
		headerTable.addCell(new Paragraph("Address", arialPt13Grey));
		headerTable.addCell(new Paragraph("Facade", arialPt13Grey));
		headerTable.addCell(new Paragraph("Width", arialPt13Grey));
		headerTable.addCell(new Paragraph("Depth", arialPt13Grey));
		headerTable.addCell(new Paragraph("Area", arialPt13Grey));
		headerTable.addCell(new Paragraph("Price", arialPt13Grey));
		headerTable.addCell(new Paragraph("Status", arialPt13Grey));
		headerTable.getDefaultCell().setColspan(1);

		headerTable.completeRow();
		return headerTable;
	}

	private void addContentToBodyTable(PdfPTable bodyTable, PublishInfo info, PublishInfo previousInfo, int index, int totalColumns) throws DocumentException {

		try {
			boolean complete = false;
			int i = 0;

			while (!complete && i < sortOrderList.size()) {
				String sortOrderBeanName = sortOrderList.get(i);

				String currentValue = getProperty(info, sortOrderBeanName);
				String prevValue = getProperty(previousInfo, sortOrderBeanName);

				if (i == 0 && !currentValue.equalsIgnoreCase(prevValue)) {

					if (index != 0)
						addBlankRow(bodyTable, totalColumns);

					addFirstHeader(bodyTable, info, sortOrderBeanName, totalColumns);
					addSecondHeader(bodyTable, info, sortOrderBeanName, totalColumns);
					addThirdHeader(bodyTable, info, sortOrderBeanName, totalColumns);
					addFourthHeader(bodyTable, info, sortOrderBeanName, totalColumns);
					addFifthHeader(bodyTable, info, sortOrderBeanName, totalColumns);

					complete = true;

				} else if (i == 1 && !currentValue.equalsIgnoreCase(prevValue)) {
					addSecondHeader(bodyTable, info, sortOrderBeanName, totalColumns);
					addThirdHeader(bodyTable, info, sortOrderBeanName, totalColumns);
					addFourthHeader(bodyTable, info, sortOrderBeanName, totalColumns);
					addFifthHeader(bodyTable, info, sortOrderBeanName, totalColumns);

					complete = true;

				} else if (i == 2 && !currentValue.equalsIgnoreCase(prevValue)) {
					addThirdHeader(bodyTable, info, sortOrderBeanName, totalColumns);
					addFourthHeader(bodyTable, info, sortOrderBeanName, totalColumns);
					addFifthHeader(bodyTable, info, sortOrderBeanName, totalColumns);

					complete = true;

				} else if (i == 3 && !currentValue.equalsIgnoreCase(prevValue)) {
					addFourthHeader(bodyTable, info, sortOrderBeanName, totalColumns);
					addFifthHeader(bodyTable, info, sortOrderBeanName, totalColumns);

					complete = true;
				} else if (i == 4 && !currentValue.equalsIgnoreCase(prevValue)) {
					addFifthHeader(bodyTable, info, sortOrderBeanName, totalColumns);

					complete = true;
				}
				i++;
			}

			addPackageRow(bodyTable, info, totalColumns);

		} catch (Exception e) {
			logger.debug("Error in addContentToBodyTable");
			throw new DocumentException("Error in addContentToBodyTable" + e);
		}
		// return bodyTable;
	}

	private void addBlankRow(PdfPTable bodyTable, int totalColumns) throws Exception {
		bodyTable.getDefaultCell().setBorder(0);
		bodyTable.getDefaultCell().setBorderWidth(0f);
		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
		bodyTable.getDefaultCell().setColspan(totalColumns);
		bodyTable.getDefaultCell().setFixedHeight(30f);
		bodyTable.addCell(BLANK);
		bodyTable.completeRow();

		// Reset
		bodyTable.getDefaultCell().setBorder(1);
		bodyTable.getDefaultCell().setBorderWidth(0.1f);
		bodyTable.getDefaultCell().setBorderWidthTop(0.1f);
		bodyTable.getDefaultCell().setBorderWidthBottom(0.1f);
		bodyTable.getDefaultCell().setFixedHeight(0f);
	}

	private void addFirstHeader(PdfPTable bodyTable, PublishInfo info, String sortOrderBeanName, int totalColumns) throws Exception {

		if (sortOrderList.size() < 1)
			return;

		Paragraph p = new Paragraph();
		p.add(new Chunk((getProperty(info, sortOrderList.get(0))), arialPt17White));
		initHeader(bodyTable, totalColumns, firstHeaderColor);
		bodyTable.addCell(p);
		bodyTable.completeRow();
	}

	private void addSecondHeader(PdfPTable bodyTable, PublishInfo info, String sortOrderBeanName, int totalColumns) throws Exception {

		if (sortOrderList.size() < 2)
			return;

		Paragraph p = new Paragraph();
		p.add(new Chunk((getProperty(info, sortOrderList.get(1))), arialMediumPt17Grey));
		initHeader(bodyTable, totalColumns, secondHeaderColor);
		bodyTable.addCell(p);
		bodyTable.completeRow();
	}

	private void addThirdHeader(PdfPTable bodyTable, PublishInfo info, String sortOrderBeanName, int totalColumns) throws Exception {

		if (sortOrderList.size() < 3)
			return;
		else if (sortOrderList.size() == 3) {
			// This is the last header, add borders
			toggleBorder(bodyTable, true);
		}

		Paragraph p = new Paragraph();
		p.add(new Chunk((getProperty(info, sortOrderList.get(2))), arialPt15Grey));
		initHeader(bodyTable, totalColumns, thirdHeaderColor);
		bodyTable.addCell(p);
		bodyTable.completeRow();
		
		toggleBorder(bodyTable, false);
	}

	private void addFourthHeader(PdfPTable bodyTable, PublishInfo info, String sortOrderBeanName, int totalColumns) throws Exception {
		if (sortOrderList.size() < 4)
			return;
		else if (sortOrderList.size() == 4) {
			// This is the last header, add borders
			toggleBorder(bodyTable, true);
		}

		Paragraph p = new Paragraph();
		p.add(new Chunk((getProperty(info, sortOrderList.get(3))), arialPt13Grey));
		initHeader(bodyTable, totalColumns, fourthHeaderColor);
		bodyTable.addCell(p);
		bodyTable.completeRow();

		toggleBorder(bodyTable, false);
	}

	private void addFifthHeader(PdfPTable bodyTable, PublishInfo info, String sortOrderBeanName, int totalColumns) throws Exception {

		if (sortOrderList.size() < 5)
			return;

		Paragraph p = new Paragraph();
		p.add(new Chunk((getProperty(info, sortOrderList.get(4))), arialPt13Grey));
		initHeader(bodyTable, totalColumns, fifthHeaderColor);
		toggleBorder(bodyTable, true);
		bodyTable.addCell(p);
		bodyTable.completeRow();

		bodyTable.getDefaultCell().setBorder(1);
		bodyTable.getDefaultCell().setBorderWidthLeft(0.1f);
		bodyTable.getDefaultCell().setBorderWidthRight(0.1f);

		toggleBorder(bodyTable, false);
	}

	private void addPackageRow(PdfPTable bodyTable, PublishInfo info, int totalColumns) throws Exception {
		bodyTable.getDefaultCell().setColspan(1);
		bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
		bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

		bodyTable.getDefaultCell().setBorderWidthLeft(0.1f);
		bodyTable.getDefaultCell().setBorderWidthRight(0.1f);
		bodyTable.getDefaultCell().setBorderWidthTop(0.1f);
		bodyTable.getDefaultCell().setBorderWidthBottom(0.1f);

		bodyTable.addCell(new Paragraph(info.getHomePlanName(), arialPt13Grey));
		bodyTable.addCell(new Paragraph(info.getAddress(), arialPt11Grey));
		bodyTable.addCell(new Paragraph(info.getFacade(), arialPt11Grey));
		bodyTable.addCell(new Paragraph(info.getWidth() + "", arialPt11Grey));
		bodyTable.addCell(new Paragraph(info.getDepth() + "", arialPt11Grey));
		bodyTable.addCell(new Paragraph(info.getArea() + "", arialPt11Grey));
		bodyTable.addCell(new Paragraph(info.getPrice(), arialPt11Grey));
		bodyTable.addCell(new Paragraph(info.getStatus(), arialPt11Grey));

		for (int r = 0; r < pmList.length; r++) {
			String value = getProperty(info, pmList[r].getBeanName());

			if (StringUtils.isNotBlank(value)) {
				bodyTable.getDefaultCell().setBackgroundColor(pmList[r].getColor());
				bodyTable.addCell(new Paragraph(getProperty(info, pmList[r].getBeanName()), arialPt14White));
			} else {
				bodyTable.getDefaultCell().setBackgroundColor(whiteColor);
				bodyTable.addCell(new Paragraph(getProperty(info, pmList[r].getBeanName()), arialPt11Grey));
			}
			
		}

		bodyTable.completeRow();
	}

	private void toggleBorder(PdfPTable bodyTable, boolean add) {
		if (add) {
			bodyTable.getDefaultCell().setBorder(1);
			bodyTable.getDefaultCell().setBorderWidthLeft(0.1f);
			bodyTable.getDefaultCell().setBorderWidthRight(0.1f);
		} else {
			bodyTable.getDefaultCell().setBorder(0);
			bodyTable.getDefaultCell().setBorderWidthLeft(0f);
			bodyTable.getDefaultCell().setBorderWidthRight(0f);
		}
	}
	
	private void initHeader(PdfPTable bodyTable, int totalColumns, BaseColor bgColor) {
		bodyTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		bodyTable.getDefaultCell().setPaddingLeft(5);
		bodyTable.getDefaultCell().setBackgroundColor(bgColor);
		bodyTable.getDefaultCell().setColspan((totalColumns));
	}
	
	public static String getProperty(PublishInfo bean, String propertyName) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		String value = "";
		try {
			value = (String) PropertyUtils.getProperty(bean, propertyName);
			if (StringUtils.isBlank(value)) {
				return "";
			}
		} catch (NoSuchMethodException e) {
			logger.error("Ignoring Error in getting property from PublishBean" + e);
		} catch (IllegalAccessException e) {
			logger.error("Ignoring Error in getting property from PublishBean" + e);
		} catch (InvocationTargetException e) {
			logger.error("Ignoring Error in getting property from PublishBean" + e);
		}
		return value;
	}
}