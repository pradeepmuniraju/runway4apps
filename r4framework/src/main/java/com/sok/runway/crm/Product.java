package com.sok.runway.crm;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.RowSetBean;
import com.sok.framework.TableSpec;
import com.sok.runway.ErrorMap;
import com.sok.runway.RunwayEntity;
import com.sok.runway.Validator;
import com.sok.runway.crm.activity.Logable;
import com.sok.runway.crm.factories.LinkableDocumentFactory;
import com.sok.runway.crm.factories.StatusUpdater;
import com.sok.runway.crm.interfaces.DocumentLinkable;
import com.sok.runway.crm.interfaces.StatusHistory;
import com.sok.runway.offline.rpmManager.RPMtask;

public class Product extends ProfiledEntity implements StatusHistory, Logable, DocumentLinkable {
	private static final Logger logger = LoggerFactory.getLogger(Product.class);
   private static final String ANSWER_VIEWSPEC = "answers/ProductAnswerView";
   public static final String ENTITY_NAME = "Product";
   
   public static final String GROUPS_RELATION = "GroupProductSecurityGroup";  
   

   
   public Product(Connection con) {
      super(con, "ProductView");
      super.setGroupsRelation(GROUPS_RELATION);
   }
	
	public Product(ServletRequest request) {
	    super(request, "ProductView");
	    super.setGroupsRelation(GROUPS_RELATION);
       populateFromRequest(request.getParameterMap());
	}
	
   public Product(Connection con, String productID) {
      this(con);
      super.setGroupsRelation(GROUPS_RELATION);
      load(productID);
   }
   
   protected Product(String viewSpec, Connection con) {
	   super(con, viewSpec);
	   super.setGroupsRelation(GROUPS_RELATION);
   }
   
   protected Product(String viewspec, ServletRequest request) {
       super(request, viewspec);
       super.setGroupsRelation(GROUPS_RELATION);
       populateFromRequest(request.getParameterMap());
   }
   
   protected Product(String viewspec, Connection con, String productID) {
      super(con, viewspec);
      super.setGroupsRelation(GROUPS_RELATION);
      load(productID);
   }  
   
   public String getAnswerViewSpec() {
      return ANSWER_VIEWSPEC;
   }

   public String getEntityName() {
	   return ENTITY_NAME;
   }
   
   public void dupe() {
      super.dupe();
      setParameter("ProductStatusID", ActionBean._blank);
      setParameter("StatusID", getField("CurrentStatusID"));
   }
   
   public ErrorMap update() {
      if (getParameter("StatusID") != null && getParameter("StatusID").length() != 0) {
         
         StatusUpdater su = StatusUpdater.getStatusUpdater();
         if (su.isNewStatus(this)) {
            setParameter("ProductStatusID",KeyMaker.generate());
            su.insertStatus(this);
         }
      }
      return super.update();
   }   
   
   public ErrorMap insert() {
      boolean insertStatus = false;
      if (getParameter("StatusID").length() != 0) {
         if (getParameter("ProductStatusID").length() == 0) {
            setParameter("ProductStatusID",KeyMaker.generate());
         }
         setParameter("QualifiedDate","NOW");
         setParameter("QualifiedBy",getCurrentUser().getUserID());
         insertStatus = true;
      }
      ErrorMap map = super.insert();
      
      if (map.isEmpty()) {
         if (insertStatus) {
            StatusUpdater su = StatusUpdater.getStatusUpdater();
            su.insertStatus(this);
         }
      }
      
      return map;
   }   
   
   public Collection findProducts(Map requestMap) {
      return findProducts(getConnection(), requestMap);
   }
   
   public static Collection findProducts(Connection con, Map requestMap) {
      ArrayList ids = new ArrayList();
      
      RowSetBean products = new RowSetBean();
      products.setViewSpec("ProductIDView");
      products.setConnection(con);
      Iterator iter = requestMap.keySet().iterator();
      String key = null;
      while (iter.hasNext()) {
         key = (String)iter.next();
         products.setColumn(key, buildValueFromArray((String[])requestMap.get(key)));
      }
      products.generateSQLStatement();
      products.getResults();
      
      if (products.getError().length() > 0) {
         throw new RuntimeException(products.getError());
      }
      
      while (products.getNext()) {
         ids.add(products.getString("ProductID"));
      }
      products.close();
      return ids;
   }
   
	
	public String getProductID() {
		return getField("ProductID");
	}
	
	public void setProductID(String ProductID) {
		setField("ProductID", ProductID);
	}
   
	@XmlElement(name="Name")
	public String getName() {
		return getField("Name");
	}

	public void setName(String Name) {
		setField("Name", Name);
	}
   
	@XmlElement(name="Description")
	public String getDescription() {
		return getField("Description");
	}

	public void setDescription(String Description) {
		setField("Description", Description);
	}
   
	@XmlElement(name="ProductNum")
	public String getProductNum() {
		return getField("ProductNum");
	}

	public void setProductNum(String ProductNum) {
		setField("ProductNum", ProductNum);
	}
   
   public String getCategory() {
      return getField("Category");
   }
	
    @XmlElement(name="Cost")
	public Double getCost()
	{
		return getDouble("Cost");
	}	

	public void setCost(double cost)
	{
		setField("Cost", cost);
	}
	
	@XmlElement(name="GST")
	public Double getGST()
	{
		return getDouble("GST");
	}	

	public void setGST(double gst)
	{
		setField("GST", gst);
	}

	@XmlElement(name="TotalCost")
	public Double getTotalCost() {
		return getDouble("TotalCost");
	}

	public void setTotalCost(double totalcost)
	{
		setField("TotalCost", totalcost);
	}	

	public String getProductType() {
		return getField("ProductType");
	}

	@JsonProperty("Active")
	public String getActive() {
		if(getField("Active").length()!=0)
			return getField("Active");
		return "N";
	}
	@JsonProperty("Active")
	public void setActive(String Active) {
		setField("Active", Active);
	}

	//@XmlElement(name = "Status")
	public String getStatus() {
		return getField("Status");
	}
	//@XmlElement(name = "StatusID")
	public String getCurrentStatusID() {
		return getField("CurrentStatusID");
	}
	//@XmlElement(name = "StatusID")
	public void setStatusID(String StatusID) {	// the status history updater will check StatusID
		setField("StatusID", StatusID);
	}
   
	//@XmlElement(name = "SubType")
	public String getSubType() {
		return getField("ProductSubType");
	}

	public void setSubType(String SubType) {
		setField("ProductSubType", SubType);
	}	

	@XmlElement(name = "Image")
	public Document getImage() {
		if(isSet("Image")) return new Document(getField("Image"), getField("ImageName"), getField("ImagePath"));
		return null;
	}
	
	@XmlElement(name = "Thumbnail")
	public Document getThumbnail() {
		if(isSet("ThumbnailImage")) return new Document(getField("ThumbnailImage"), getField("ThumbnailImageName"), getField("ThumbnailImagePath"));
		return null;
	}
	
   public Validator getValidator() {
      Validator val = Validator.getValidator("Product");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("ProductID","SYSTEM ERROR - ProductID");
         val.addMandatoryField("GroupID","Product must have a security group selected");
         val.addMandatoryField("Name","Name must not be blank.");

         Validator.addValidator("Product", val);
      }
      return val;
   }
   
   public String getStatusHistoryViewName() {
      return "ProductStatusView";
   }

   public String getStatusHistoryID() {
      return getParameter("ProductStatusID");
   }

   public void setStatusHistoryID(String id) {
	   setParameter("ProductStatusID", id);
   }
   
   /**
    * This actually returns a statusID that has been updated, not retreived.
    */
   public String getStatusID() {
      return getParameter("StatusID");
   }   
   
   /**
    * Deletes Child Data of Products after RunwayEntity has removed the note. 
    * Profile data will be removed by ProfiledEntity. 
    */
   public boolean delete() {

      if (getPrimaryKey().length() > 0) {
         try {
       		GenRow del = new GenRow();
   	 		del.setConnection(getConnection());		 		
   	 		del.setTableSpec("LinkedDocuments"); 
   	 		del.setParameter(RunwayEntity.UPDATEON +  getPrimaryKeyName(), getPrimaryKey()); 
   	 		del.setAction(RunwayEntity.DELETEALL); 
   	 		del.doAction(); 
   	 		
   	 		del.setTableSpec("CampaignProducts"); 
   	 		del.doAction(); 
   	 		
   	 		del.setTableSpec("properties/ProductQuickIndex"); 
   	 		del.doAction(); 
   	 		
   	 		del.setTableSpec("PageProducts"); 
   	 		del.doAction(); 
   	 		
   	 		del.setTableSpec("ContactProducts"); 
   	 		del.doAction(); 
   	 		
   	 		del.setTableSpec("ProductProducts"); 
   	 		del.doAction(); 
   	 		
   	 		del.setTableSpec("properties/ProductDetails"); 
   	 		del.doAction(); 
   	 		
   	 		del.setTableSpec("properties/HomeDetails"); 
   	 		del.doAction(); 
   	 		
   	 		del.setTableSpec("ROIElements"); 
   	 		del.doAction(); 
   	 		
   	 		del.clear();
   	 		del.setTableSpec("ProductProducts"); 
   	 		del.setParameter(RunwayEntity.UPDATEON +  "LinkedProductID", getPrimaryKey()); 
   	 		del.setAction(RunwayEntity.DELETEALL); 
   	 		del.doAction(); 
   	 		
   	 		del.clear();
   	 		del.setConnection(getConnection());		 		
   	 		del.setViewSpec("OpportunityIDView"); 
   	 		del.setParameter(getPrimaryKeyName(), getPrimaryKey());
   	 		del.getResults(); 
   	 		
   	 		Opportunity opportunity = new Opportunity(getConnection());
   	 		
   	 		while (del.getNext()) {
   	 		   opportunity.setPrimaryKey(del.getData(opportunity.getPrimaryKeyName()));
   	 		   
   	 		   if (!opportunity.delete()) {
   	 		      return false;
   	 		   }
   	 		}
   	 		return super.delete();
   	 	}
   	 	catch (Exception e) {
   	 	   logger.error("Exception while deleting product {}. Exeception" + getPrimaryKey(), e);
   	 	   return false;
   	 	}
	 	}
	 	return false;
   }
	
   /**
    *    Linkable Interface
    */
   public String getLinkableTitle() {      
      if (getField("ProductNum").length() != 0 ) {
         return getField("ProductNum") + " - " + getField("Name");
      }
      else {
         return getField("Name");
      }
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs() {
      return getLinkableDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs() {
      return getDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
   }   
    
   /**
    *    DocumentLinkable Interface
    */
   public LinkableDocument getLinkableDocument() {
      LinkableDocument doc = new LinkedDocument(getConnection());
      doc.setCurrentUser(getCurrentUser());
      return doc;
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public void linkDocument(String documentID) {
      LinkableDocumentFactory.linkDocument(this, documentID);
   }
   
   public Collection<String> getGalleryIDs(Map<String, String> filterParameters) {
	   return LinkableDocumentFactory.getGalleryIDs(this, filterParameters);
   }
   
   public Collection<String> getGalleryIDs() {
	   return getGalleryIDs(null);
   }
   
   public Map<String, String> getLinkedDocumentIDsAndGalleryIDs(Map<String, String> filterParameters) {
	   return  LinkableDocumentFactory.getLinkedDocumentIDsAndGalleryIDs(this, filterParameters);
   }
   
   public Collection<String> getLinkableDocumentIDsWithOutGalleryOnly() {
	   Map<String, String> filterParameters = new HashMap<String, String>();
	   filterParameters.put("ShowOnGalleryOnly", "N");
	   return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDsWithGalleryShowOrder(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters, "LinkedDocumentID", false, true);
   }
   
	
	protected void insertProductVariation(Connection con, String parentproductid, String childproductid, String type){
	   if (parentproductid == null || parentproductid.length() < 5 || childproductid == null || childproductid.length() < 5) return;
	   GenRow productvariation = new GenRow();
	   productvariation.setConnection(con);
	   productvariation.setTableSpec("ProductVariations");
	   productvariation.setParameter("ParentProductID", parentproductid);
	   productvariation.setParameter("ChildProductID", childproductid);
	   
	   if(productvariation.isSet("ParentProductID") && productvariation.isSet("ChildProductID")) { 
		   productvariation.setParameter("Type", type);
		   productvariation.createNewID();
		   productvariation.doAction("insert");
	   } 
	}
	protected String copy(Connection con, String tablespecname, String productidfieldname, String fromproductid, String toproductid, String userid)
	{
	   GenRow row = new GenRow();
	   row.setTableSpec(tablespecname);
	   
	   TableSpec ts = row.getTableSpec();
	   
	   row.setConnection(con);
	   row.setParameter("-select1", "*");
	   row.setParameter(productidfieldname, fromproductid);
	   
	   if(row.isSet(productidfieldname)) { 
		   row.doAction(ActionBean.SEARCH);
		   row.getResults();
	   }
	   
	   GenRow clone = new GenRow();
	   while (row.getNext()) {
	          clone.clear();
		      clone.setTableSpec(tablespecname);
		      clone.setConnection(con);
		      clone.putAllDataAsParameters(row);
		      if("Products".equals(row.getTableSpec().getTableName()))
		      {
		    	 if (row.getString("Name").indexOf("copy") == -1) clone.setParameter("Name",row.getString("Name") + " copy");
		         clone.setParameter("ProductType", "Land");
		    	 clone.setParameter("CopiedFromProductID",fromproductid);
		    	 clone.setParameter("Category","");
		         clone.setParameter("UpdateByProcess", "Y");
		      }
		      clone.setParameter("CreatedDate", "NOW");
		      clone.setParameter("ModifiedDate", "NOW");
		      clone.setParameter("CreatedBy", userid);
		      clone.setParameter("ModifiedBy", userid);
		      clone.remove(row.getTableSpec().getPrimaryKeyName());
		      clone.createNewID();
		      clone.setParameter(productidfieldname, toproductid);
		      clone.doAction("insert");
	   }
	   row.close();
	   return(clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	/**
	 * Loads a GenRow for the Product, it will detect the type and use the correct view
	 * 
	 * @param productID
	 * @return
	 */
	public static GenRow loadProduct(String productID) {
		GenRow row = new GenRow();
		if (StringUtils.isBlank(productID)) return row;
		
		row.setTableSpec("Products");
		row.setParameter("-select0", "ProductType");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");
		
		String productType = row.getString("ProductType");
		row = new GenRow();
		
		if ("House and Land".equals(productType)) {
			row.setViewSpec("properties/ProductHouseAndLandView");
		} else if ("Home Plan".equals(productType)) {
			row.setViewSpec("properties/ProductPlanView");
		} else if ("Home".equals(productType)) {
			row.setViewSpec("properties/ProductHomeView");
		} else if ("Home Range".equals(productType)) {
			row.setViewSpec("ProductView");
		} else if ("Land".equals(productType)) {
			row.setViewSpec("properties/ProductLotView");
		} else if ("Stage".equals(productType)) {
			row.setViewSpec("properties/ProductStageView");
		} else if ("Estate".equals(productType)) {
			row.setViewSpec("properties/ProductEstateView");
		} else if ("Apartment".equals(productType)) {
			row.setViewSpec("properties/ProductApartmentView");
		} else if ("Building Stage".equals(productType)) {
			row.setViewSpec("properties/ProductBuildingStageView");
		} else if ("Building".equals(productType)) {
			row.setViewSpec("properties/ProductBuildingView");
		} else if ("Existing Properties".equals(productType)) {
			row.setViewSpec("properties/ProductExistingPropertyView");
		} else {
			row.setViewSpec("ProductView");
			row.setParameter("ProductID", productID);
			row.doAction("selectfirst");
			return row;
		}
		
		row.setParameter("ProductID", productID);
		row.setParameter("ProductType", productType);
		
		row.doAction("selectfirst");
		
		return row;
	}
	
	public static void touch(String productID, String userID) {
		if (StringUtils.isBlank(productID) || StringUtils.isBlank(userID)) return;
		
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setParameter("ProductID", productID);
		row.setParameter("ModifiedBy", userID);
		row.setParameter("ModifiedDate", "NOW");
		row.doAction("update");
		
		RPMtask.getInstance().doIndexRefreash("update", productID);
	}
}