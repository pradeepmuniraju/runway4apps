package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.runway.crm.factories.AddressFactory;
import com.sok.runway.crm.factories.LinkableDocumentFactory;
import com.sok.runway.crm.factories.StatusUpdater;
import com.sok.runway.crm.activity.*;
import com.sok.runway.security.AuditEntity; 
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import javax.servlet.ServletRequest;
import java.util.*;
import java.sql.*;

public class Company extends ProfiledEntity implements StatusHistory, Addressable, Logable, DocumentLinkable {

   private static final String ANSWER_VIEWSPEC = "{answers/}CompanyAnswerView";
   public static final String ENTITY_NAME = "Company";
	   
   public static final String GROUPS_RELATION = "GroupCompanyGroup";     
   
   public Company(Connection con) {
      super(con, "CompanyView");
      super.setGroupsRelation(GROUPS_RELATION);
   }
	
	public Company(ServletRequest request) {
	    super(request, "CompanyView");
	      super.setGroupsRelation(GROUPS_RELATION);
       populateFromRequest(request.getParameterMap());
	}
   
   public Company(Connection con, String companyID) {
      this(con);
      load(companyID);
   }
   
   public String getAnswerViewSpec() {
      return ANSWER_VIEWSPEC;
   }

   public String getEntityName() {
	   return ENTITY_NAME;
   }

   public String getStatusHistoryViewName() {
      return "CompanyStatusView";
   }

   public String getStatusHistoryID() {
      return getParameter("CompanyStatusID");
   }

   public void setStatusHistoryID(String id) {
	   setParameter("CompanyStatusID", id);
   }

   public String getStatusID() {
      return getParameter("StatusID");
   }
   
   /* @type = "Postal" or "" */
   public Map getAddress(String type) {
      return AddressFactory.getAddress(type, this);
   }
   
   public Map getAddress() {
      return AddressFactory.getAddress(null, this);
   }
   
   /* @type = "Postal" or "" */
   public void setAddress(String type, Map address) {
      AddressFactory.setAddress(type, this, address);
   }
   
   public void setAddress(Map address) {
      AddressFactory.setAddress(null, this, address);
   }
   
   
   
   /**
    * Search and delete all Notes associated with this object - based on 
    * the attribute name returned by getPrimaryKeyName().
    */
   public void deleteAllNotes() 
   {
   	Note 		note 			= new Note(getConnection());
   	GenRow 	noteBean 	= new GenRow();
   	
   	note.setCurrentUser(getCurrentUser());
   	
   	noteBean.setConnection(getConnection());
   	noteBean.setViewSpec("NoteIDView");
   	noteBean.setParameter(getPrimaryKeyName(), getPrimaryKey());
   	noteBean.doAction(ActionBean.SEARCH);
   	noteBean.getResults();
   	
   	while (noteBean.getNext()) {
   		note.load(noteBean.getData("NoteID"));
   		note.delete();
   	}
   }
   
   /**
    * Search and delete all Contacts associated with this object - based on 
    * the attribute name returned by getPrimaryKeyName().
    */
   public void deleteAllContacts() 
   {
   	Contact 	contact 			= new Contact(getConnection());
   	GenRow 	contactBean 	= new GenRow();
   	
   	contact.setCurrentUser(getCurrentUser());
   	
   	contactBean.setConnection(getConnection());
   	contactBean.setViewSpec("ContactIDView");
   	contactBean.setParameter(getPrimaryKeyName(), getPrimaryKey());
   	contactBean.doAction(ActionBean.SEARCH);
   	contactBean.getResults();
   	
   	while (contactBean.getNext()) {
   		contact.load(contactBean.getData("ContactID"));
   		contact.delete();
   	}
   }
   
   /**
    * Delete this Company and its associated Notes, AND also all Contacts 
    * assigned to it (Contacts having the CompanyID to be deleted).
    * @return boolean true if the delete was successful
    */
   public boolean delete()
   {
   	return this.delete(true);
   }
   
   /**
    * Delete this Company and its associated Notes, with the option of including
    * or excluding all Contacts associated with it.
    * @param deleteContacts
    * @return boolean true if the delete was successful
    */
   public boolean delete(boolean deleteContacts)
   {
      if (super.delete())
      {
      	deleteAllNotes();
      	
      	if (deleteContacts) 
      	{
      		deleteAllContacts();
      	}
      	
      	GenRow list = new GenRow(); 
 		list.setConnection(getConnection()); 
 		list.setTableSpec("Opportunities");
 		list.setParameter("-select1","OpportunityID");
 		list.setParameter("-select2","CompanyID"); 
 		list.setParameter(getPrimaryKeyName(),getPrimaryKey());
 		list.getResults(); 
 		
 		AuditEntity child = new Opportunity(getConnection()); 
 		child.setCurrentUser(getCurrentUser());
 		
 		while (list.getNext()) { 
 			child.load(list.getData("OpportunityID")); 
 			child.delete(); 
 		}

 		list.clear(); 
 		list.setTableSpec("Orders");
 		list.setParameter("-select1","OrderID");
 		list.setParameter("-select2","CompanyID"); 
 		list.setParameter(getPrimaryKeyName(),getPrimaryKey());
 		list.getResults(); 
 		
 		child = new Order(getConnection()); 
 		child.setCurrentUser(getCurrentUser());
 		
 		while (list.getNext()) { 
 			child.load(list.getData("OrderID")); 
 			child.delete(); 
 		}	 					
 		
 		/* delete all stragglers */ 
 		GenRow del = new GenRow();
 		del.setConnection(getConnection());		 		
 		del.setTableSpec("ContactEvents"); 
		del.setParameter(GenerationKeys.UPDATEON + "ContactEventCampaign.CompanyID", getPrimaryKey());
		del.setAction(com.sok.framework.generation.GenerationKeys.DELETEALL); 
 		del.doAction(); 
 		
		del.clear(); 
		del.setTableSpec("CompanyStatus"); 
		del.setParameter(GenerationKeys.UPDATEON + getPrimaryKeyName(), getPrimaryKey());
		del.setAction(com.sok.framework.generation.GenerationKeys.DELETEALL); 
		del.doAction(); 

		del.setTableSpec("ContactCampaigns");
		del.doAction();
 		
		del.setTableSpec("OpportunityCompanies");
 		del.doAction();

 		del.setTableSpec("CompanyProductGroups");
 		del.doAction();

 		del.setTableSpec("LinkedDocuments");
 		del.doAction();

 		del.clear();
 		del.setTableSpec("CompanyLinks");
 		del.setAction(GenerationKeys.DELETEALL);
		del.setParameter(GenerationKeys.UPDATEON + "LinkerID", getPrimaryKey());
 		del.doAction();

 		del.clear();
 		del.setTableSpec("CompanyLinks");
 		del.setAction("deleteall");
		del.setParameter(GenerationKeys.UPDATEON + "LinkeeID", getPrimaryKey());
 		del.doAction();
      	
         return true;
      }
      else 
      {
         return false;
      }
   }
   
   public ErrorMap update() {
	  
	  if ("offline".equals(getParameter("updateContactCompany"))) { 
		  AddressFactory.updateContactCompanyFromCompany(this); 
	  }
	  if ("offline".equals(getParameter("updateContactAddresses"))){ 
		  AddressFactory.updateContactAddressesFromCompany(this); 		   	
	  }

      if (getParameter("StatusID") != null && getParameter("StatusID").length() != 0) {
         
         StatusUpdater su = StatusUpdater.getStatusUpdater();
         if (su.isNewStatus(this)) {
            setParameter("CompanyStatusID",KeyMaker.generate());
            su.insertStatus(this);
            setParameter("QualifiedDate","NOW");
            setParameter("QualifiedBy",getCurrentUser().getUserID());
         }
      }
      return super.update();
   }
   
   public ErrorMap insert() {
      boolean insertStatus = false;
      if (getParameter("StatusID").length() != 0) {
         if (getParameter("CompanyStatusID").length() == 0) {
            setParameter("CompanyStatusID",KeyMaker.generate());
         }
         setParameter("QualifiedDate","NOW");
         setParameter("QualifiedBy",getCurrentUser().getUserID());
         insertStatus = true;
      }
      ErrorMap map = super.insert();
      
      if (map.isEmpty()) {
         if (insertStatus) {
            StatusUpdater su = StatusUpdater.getStatusUpdater();
            su.insertStatus(this);
         }
      }
      
      return map;
   }
        
   public Validator getValidator() {
      Validator val = Validator.getValidator("Company");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("CompanyID","SYSTEM ERROR");
         val.addMandatoryField("GroupID","SYSTEM ERROR");
         Validator.addValidator("Company", val);
      }
      return val;
   }
   
   /**
    *    Linkable Interface
    */
   public String getLinkableTitle() {
      return getField("Company");
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs() {
      return getLinkableDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters);
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs() {
      return getDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
   }      
   
   /**
    *    DocumentLinkable Interface
    */
   public LinkableDocument getLinkableDocument() {
      LinkableDocument doc = new LinkedDocument(getConnection());
      doc.setCurrentUser(getCurrentUser());
      return doc;
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public void linkDocument(String documentID) {
      LinkableDocumentFactory.linkDocument(this, documentID);
   }
}