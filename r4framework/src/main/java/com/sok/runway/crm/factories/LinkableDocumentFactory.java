package com.sok.runway.crm.factories;

import com.sok.runway.crm.*;
import com.sok.runway.campaigns.*;
import com.sok.runway.crm.interfaces.*;
import com.sok.runway.crm.admin.*;
import com.sok.runway.crm.cms.*;
import com.sok.runway.security.*;
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys; 

import javax.servlet.ServletRequest;

import java.util.*;
import java.sql.*;

public class LinkableDocumentFactory {
   
   public static DocumentLinkable getDocumentLinkable(ServletRequest request, String entityName, String pk) {
      
      DocumentLinkable linkable = null;
      
      if (Contact.ENTITY_NAME.equals(entityName)) {
         linkable = new Contact(request);
         linkable.load(pk);
      }
      else if (Company.ENTITY_NAME.equals(entityName)) {
         linkable = new Company(request);
         linkable.load(pk);
      }
      else if (Campaign.ENTITY_NAME.equals(entityName)) {
         linkable = new Campaign(request);
         linkable.load(pk);
      }
      else if (Quote.ENTITY_NAME.equals(entityName)) {
         linkable = new Quote(request);
         linkable.load(pk);
      }
      else if (Note.ENTITY_NAME.equals(entityName)) {
         linkable = new Note(request);
         linkable.load(pk);
      }
      else if (Opportunity.ENTITY_NAME.equals(entityName)) {
         linkable = new Opportunity(request);
         linkable.load(pk);
      }
      else if (Page.ENTITY_NAME.equals(entityName)) {
         linkable = new Page(request);
         linkable.load(pk);
      }
      else if (Stage.ENTITY_NAME.equals(entityName)) {
         linkable = new Stage(request);
         linkable.load(pk);
      }
      else if (Product.ENTITY_NAME.equals(entityName)) {
         linkable = new Product(request);
         linkable.load(pk);
      }
      else if (MasterTemplate.ENTITY_NAME.equals(entityName)) {
         linkable = new MasterTemplate(request);
         linkable.load(pk);
      }
      else if (Template.ENTITY_NAME.equals(entityName)) {
         linkable = new Template(request);
         linkable.load(pk);
      }
      
      if (linkable != null && linkable.isLoaded()) {
         return linkable;
      }
      else {
         return null;
      }
   }
   
   public static Collection<String> getLinkableDocumentIDs(DocumentLinkable docLinkable, Map<String,String> filterParameters) {
      return(getLinkableDocumentIDs(docLinkable, filterParameters, "LinkedDocumentID"));
   }
   
   public static Collection<String> getDocumentIDs(DocumentLinkable docLinkable, Map<String,String> filterParameters) {
      return(getLinkableDocumentIDs(docLinkable, filterParameters, "DocumentID"));
   }
   
   public static Collection<String> getGalleryIDs(DocumentLinkable docLinkable, Map<String, String> filterParameters) {
	   return (getLinkableDocumentIDs(docLinkable, filterParameters, "GalleryID"));
   }
   
   public static Collection<String> getLinkableDocumentIDs(DocumentLinkable docLinkable, Map<String,String> filterParameters, String returnvalue) {
	   return getLinkableDocumentIDs(docLinkable, filterParameters, returnvalue, false);
   }
   
   public static Collection<String> getLinkableDocumentIDs(DocumentLinkable docLinkable, Map<String,String> filterParameters, String returnvalue, boolean appendGalleryID) {
	   return getLinkableDocumentIDs(docLinkable, filterParameters, returnvalue, false, false);
   }
   
   public static Collection<String> getLinkableDocumentIDs(DocumentLinkable docLinkable, Map<String,String> filterParameters, String returnvalue, boolean appendGalleryID, boolean useGalleryShowOrder) {
      ArrayList<String> docIDs = new ArrayList<String>();
      
      if (docLinkable.isLoaded()) {
         GenRow documents = new GenRow();
         documents.setConnection(docLinkable.getConnection());
         documents.setViewSpec("LinkedDocumentShortView");
         documents.setParameter(docLinkable.getPrimaryKeyName(), docLinkable.getPrimaryKey());
         if ((!StringUtil.isBlankOrEmpty(returnvalue)) && (!documents.getTableSpec().getPrimaryKeyName().equalsIgnoreCase(returnvalue))
        		 && (!docLinkable.getPrimaryKeyName().equalsIgnoreCase(returnvalue))) {
        	 documents.setParameter("-select1", returnvalue);
         }
         if ((appendGalleryID) 
        	&& (documents.getParameter("GalleryID") == null || documents.getParameter("GalleryID").length() == 0)) {
        	 documents.setParameter("GalleryID", "NULL+EMPTY");
         }
            
         if (useGalleryShowOrder) {
        	 documents.sortBy("LinkedDocuments.ShowOnGalleryOrder",0);
        	 documents.sortOrder("ASC", 0);
        	 documents.sortBy("LinkedDocuments.SortOrder",1);
        	 documents.sortBy("LinkedDocuments.CreatedDate",2);
        	 documents.sortOrder("DESC",2);
        	 documents.sortBy("LinkedDocumentDocument.FileName",3);
         } else {
        	 documents.sortBy("LinkedDocuments.SortOrder",0);
        	 documents.sortBy("LinkedDocuments.CreatedDate",1);
        	 documents.sortOrder("DESC",1);
        	 documents.sortBy("LinkedDocumentDocument.FileName",2);
         }
         
         if (filterParameters != null) {
            for (String key : filterParameters.keySet() )
            	if ("ShowOnGalleryOnly".equalsIgnoreCase(key)) {
            		String param = filterParameters.get(key);
            		if ("Y".equalsIgnoreCase(param)) {
            			documents.setParameter(key, filterParameters.get(key));
            		} else {
            			documents.setParameter(key, "NULL+EMPTY+N");
            		}
            	} else {
            		documents.setParameter(key, filterParameters.get(key));
            	}
         } else {
             docLinkable.getCurrentUser().setSearchConstraint(documents,"LinkedDocumentDocument.DocumentGroup");
         }
         documents.setParameter(GenerationKeys._idonly, GenerationKeys._true);
         
         if (documents.getString("ProductID").length() > 0 && documents.getString("OrderID").length() == 0) documents.setParameter("OrderID", "NULL+EMPTY");

         documents.setParameter("-groupby0", "DocumentID");
         
         documents.doAction(ActionBean.SEARCH);
         documents.getResults();
         
         while (documents.getNext()) {
            docIDs.add(documents.getData(returnvalue));
         }
         documents.close();
      }
      return docIDs;
   }   
   
   public static void linkDocument(DocumentLinkable docLinkable, String documentID) {
      if (docLinkable.isPersisted()) {
         LinkableDocument linkedDoc = docLinkable.getLinkableDocument();
         linkedDoc.setNewPrimaryKey();
         linkedDoc.setField(docLinkable.getPrimaryKeyName(), docLinkable.getPrimaryKey());
         linkedDoc.setDocumentID(documentID);
         linkedDoc.insert();
      }
   }
   
   
   public static Map<String, String> getLinkedDocumentIDsAndGalleryIDs(DocumentLinkable docLinkable, Map<String,String> filterParameters) {
	   HashMap<String, String> docIDs = new HashMap<String, String>();
	   
	   if (docLinkable.isLoaded()) {
	         GenRow documents = new GenRow();
	         documents.setConnection(docLinkable.getConnection());
	         documents.setViewSpec("LinkedDocumentView");
	         documents.setParameter(docLinkable.getPrimaryKeyName(), docLinkable.getPrimaryKey());

	         documents.sortBy("LinkedDocuments.SortOrder",0);
	         documents.sortBy("LinkedDocuments.CreatedDate",1);
	         documents.sortOrder("DESC",1);
	         documents.sortBy("LinkedDocumentDocument.FileName",2);
	         
	         if (filterParameters != null) {
	            for (String key : filterParameters.keySet() )
	            documents.setParameter(key, filterParameters.get(key));
	         }

	         if (documents.getParameter("GalleryID") == null || documents.getParameter("GalleryID").length() == 0) {
	        	 documents.setParameter("GalleryID", "NULL+EMPTY");
		         docLinkable.getCurrentUser().setSearchConstraint(documents,"LinkedDocumentDocument.DocumentGroup");
	         }

	         //documents.setParameter(GenerationKeys._idonly, GenerationKeys._true);
	         
	         if (documents.getString("ProductID").length() > 0 && documents.getString("OrderID").length() == 0) documents.setParameter("OrderID", "NULL+EMPTY");

	         documents.doAction(ActionBean.SEARCH);
	         documents.getResults();

	         while (documents.getNext()) {
	            docIDs.put(documents.getData("LinkedDocumentID"), documents.getData("GalleryID"));
	         }
	         documents.close();
	      }
	   
	   return docIDs;
   }
   
}