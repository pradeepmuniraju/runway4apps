/*
 * Copyright (c) 2007 Henri Sivonen
 * Copyright (c) 2008-2011 Mozilla Foundation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a 
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 */

package com.sok.runway.crm.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Arrays;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;

public class HTMLSerializer implements ContentHandler, LexicalHandler {

    private static final String[] VOID_ELEMENTS = { "area", "base", "basefont",
            "bgsound", "br", "col", "command", "embed", "frame", "hr", "img",
            "input", "keygen", "link", "meta", "param", "source", "track",
            "wbr" };

    private static final String[] NON_ESCAPING = { "iframe", "noembed",
            "noframes", "noscript", "plaintext", "script", "style", "xmp" };

    private static Writer wrap(OutputStream out) {
        try {
            return new OutputStreamWriter(out, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private int ignoreLevel = 0;

    private int escapeLevel = 0;

    private final Writer writer;

    public HTMLSerializer(OutputStream out) {
        this(wrap(out));
    }

    public HTMLSerializer(Writer out) {
        this.writer = out;
    }

    public void characters(char[] ch, int start, int length)
            throws SAXException {
        try {
            if (escapeLevel > 0) {
                writer.write(ch, start, length);
            } else {
                for (int i = start; i < start + length; i++) {
                    char c = ch[i];
                    switch (c) {
                        case '<':
                            writer.write("&lt;");	// less than angle bracket
                            break;
                        case '>':
                            writer.write("&gt;");  /// greater than angle bracket
                            break;
                        case '&':
                        	if (i < (start + length - 1) && ch[i + 1] == '#')
                        		writer.write("&"); // need to keep the &# as is
                        	else if (i < (start + length - 5) && ch[i + 1] == 'b' && ch[i + 2] == 'a' && ch[i + 3] == 'l' && ch[i + 4] == 'l' && ch[i + 5] == ';')
                        		writer.write("&"); // need to keep the &# as is
                        	else
                        		writer.write("&amp;"); // ampersand
                            break;
                        case '\u00A0':
                            writer.write("&nbsp;");   // space 
                            break;
                        default:
                            writer.write(c);
                            break;
                    }
                }
           }
        } catch (IOException e) {
            throw new SAXException(e);
        }
    }

    public void endDocument() throws SAXException {
        try {
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new SAXException(e);
        }
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        if (escapeLevel > 0) {
            escapeLevel--;
        }
        if (ignoreLevel > 0) {
            ignoreLevel--;
        } else {
            try {
                writer.write('<');
                writer.write('/');
                writer.write(localName);
                writer.write('>');
            } catch (IOException e) {
                throw new SAXException(e);
            }
        }
    }

    public void ignorableWhitespace(char[] ch, int start, int length)
            throws SAXException {
        characters(ch, start, length);
    }

    public void processingInstruction(String target, String data)
            throws SAXException {
    }

    public void setDocumentLocator(Locator locator) {
    }

    public void startDocument() throws SAXException {
        try {
            writer.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
        } catch (IOException e) {
            throw new SAXException(e);
        }
    }

    public void startElement(String uri, String localName, String qName,
            Attributes atts) throws SAXException {
        if (escapeLevel > 0) {
            escapeLevel++;
        }
        boolean xhtml = "http://www.w3.org/1999/xhtml".equals(uri);
        if (ignoreLevel > 0
                || !(xhtml || "http://www.w3.org/2000/svg".equals(uri) || "http://www.w3.org/1998/Math/MathML".equals(uri))) {
            ignoreLevel++;
            return;
        }
        try {
            writer.write('<');
            writer.write(localName);
            for (int i = 0; i < atts.getLength(); i++) {
                String attUri = atts.getURI(i);
                String attLocal = atts.getLocalName(i);
                if (attUri.length() == 0) {
                    writer.write(' ');
                } else if (!xhtml
                        && "http://www.w3.org/1999/xlink".equals(attUri)) {
                    writer.write(" xlink:");
                } else if ("http://www.w3.org/XML/1998/namespace".equals(attUri)) {
                    if (xhtml) {
                        if ("lang".equals(attLocal)) {
                            writer.write(' ');
                        } else {
                            continue;
                        }
                    } else {
                        writer.write(" xml:");
                    }
                } else {
                    continue;
                }
                if(atts.getLocalName(i)==null)
                	writer.write(atts.getQName(i));
                else
                	writer.write(atts.getLocalName(i));
                
                writer.write('=');
                writer.write('"');
                if(!"data-href-orig".equals(atts.getLocalName(i)) && !"href".equals(atts.getLocalName(i)) && !"src".equals(atts.getLocalName(i)) && !"action".equals(atts.getLocalName(i))){
                   String val = atts.getValue(i);
                   for (int j = 0; j < val.length(); j++) {
                       char c = val.charAt(j);
                       switch (c) {
                           case '"':
                               writer.write("&quot;");
                               break;
                           case '&':
                           	if (j < (val.length() - 1) && val.charAt(i + 1) == '#')
                        		writer.write("&"); // need to keep the &# as is
                        	else if (j < (val.length() - 5) && val.charAt(j + 1) == 'b' && val.charAt(j + 2) == 'a' && val.charAt(j + 3) == 'l' && val.charAt(j + 4) == 'l' && val.charAt(j + 5) == ';')
                        		writer.write("&"); // need to keep the &# as is
                        	else
                               writer.write("&amp;");
                               break;
                           case '\u00A0':
                               writer.write("&nbsp;");
                               break;
                           default:
                               writer.write(c);
                               break;
                       }
                   }
                }
                else{
                   writer.write(atts.getValue(i));
                }
                writer.write('"');
            }
            writer.write('>');
            if (Arrays.binarySearch(VOID_ELEMENTS, localName) > -1) {
                ignoreLevel++;
                return;
            }
            if ("pre".equals(localName) || "textarea".equals(localName)
                    || "listing".equals(localName)) {
                writer.write('\n');
            }
            if (escapeLevel == 0
                    && Arrays.binarySearch(NON_ESCAPING, localName) > -1) {
                escapeLevel = 1;
            }
        } catch (IOException e) {
            throw new SAXException(e);
        }
    }

    public void comment(char[] ch, int start, int length) throws SAXException {
        if (ignoreLevel > 0 || escapeLevel > 0) {
            return;
        }
        try {
            writer.write("<!--");
            writer.write(ch, start, length);
            writer.write("-->");
        } catch (IOException e) {
            throw new SAXException(e);
        }
    }

    public void endCDATA() throws SAXException {
    }

    public void endDTD() throws SAXException {
    }

    public void endEntity(String name) throws SAXException {
    }

    public void startCDATA() throws SAXException {
    }

    public void startDTD(String name, String publicId, String systemId)
            throws SAXException {
    }

    public void startEntity(String name) throws SAXException {
    }

    public void startPrefixMapping(String prefix, String uri)
            throws SAXException {
    }

    public void endPrefixMapping(String prefix) throws SAXException {
    }

    public void skippedEntity(String name) throws SAXException {
    }

}
