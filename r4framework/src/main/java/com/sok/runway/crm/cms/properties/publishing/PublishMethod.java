package com.sok.runway.crm.cms.properties.publishing;

import com.itextpdf.text.BaseColor;
import com.sok.framework.InitServlet;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public enum PublishMethod {
	REA, Domain, MyPackage, Hnl, HomeSales, Trademe, AllHomes, Reiwa, MeHouse, SeniorsHousingOnline, NewHomesGuide, Website, Print;
	
	//public static final String NHG_HOST = "https://truss.dev.nhg.net.au"; // For dev only
	public static final String NHG_HOST = "https://truss.newhomesguide.com.au"; // Live
	public static final String TRADEME_HOST = "https://api.trademe.co.nz";
	private static final Logger logger = LoggerFactory.getLogger(PublishMethod.class);
	
	public String getName() {
		return names[this.ordinal()];
	}
	
	public String getPublisher() {
		return publishers[this.ordinal()];
	}
	
	public String getFeedName() {
		return feedNames[this.ordinal()];
	}

	public String getShortName() {
		return shortNames[this.ordinal()];
	}
	
	public String getBeanName() {
		return beanNames[this.ordinal()];
	}
	
	public BaseColor getColor() {
		return colors[this.ordinal()];
	}
	
	public boolean hasFeed() {
		return feedHost[this.ordinal()] != null;
	}	

	public String getAgentIDKey() {
		return getName() + "ID";
	}	
	
	public String getBranchIDKey() {
		return getName() + "BranchID";
	}
	
	public String getProjectCodeKey() {
		return getName() + "ProjectCode";
	}
	
	public String getListingIDKey() {
		return getName() + "ListingID";
	}
	
	public String getSystemParameterKey() {
		return getName().toLowerCase();
	}
	
	
	public boolean isFeedConfigured() { 
		return hasFeed() && isPublishMethodConfigured(this);
	}
	
	public String getSpecFolder() {
		return feedSpec[this.ordinal()];
	}
	
	public String getFeedHost() { 
		String host = InitServlet.getSystemParams().getProperty(names[this.ordinal()]+ "Host");
		logger.debug("host from sys params is {}", host);
		
		if(StringUtils.isBlank(host)){
			host = feedHost[this.ordinal()];
		}
		logger.debug("Feed host is ", host);
		return host;
	}
	
	public static PublishMethod[] getVisibleMethods() {
		String visibleMethodsSetting = InitServlet.getSystemParams().getProperty("PublishMethods");		

		if (StringUtils.isNotBlank(visibleMethodsSetting)) {
			List<PublishMethod> visibleMethods = new ArrayList<PublishMethod>();

			for (PublishMethod pm : PublishMethod.values()) {
				if (visibleMethodsSetting.contains(pm.name())) {
					visibleMethods.add(pm);
				}
			}
			PublishMethod[] visibleMethodsArray = new PublishMethod[visibleMethods.size()];
			visibleMethods.toArray(visibleMethodsArray);

			return visibleMethodsArray;
		} else {
			return PublishMethod.values();
		}
	}

	public static PublishMethod[] getMultiAccountMethods() {
		String multiAccountString = InitServlet.getSystemParams().getProperty("Publish-MultiAccountMethods");

		List<PublishMethod> visibleMultiAccountMethods = new ArrayList<PublishMethod>();

		if (StringUtils.isNotBlank(multiAccountString)) {
			for (PublishMethod pm : getVisibleMethods()) {
				if (multiAccountString.contains(pm.name())) {
					visibleMultiAccountMethods.add(pm);
				}
			}
		}

		PublishMethod[] visibleMultiAccountMethodsArray = new PublishMethod[visibleMultiAccountMethods.size()];
		visibleMultiAccountMethods.toArray(visibleMultiAccountMethodsArray);

		return visibleMultiAccountMethodsArray;
	}
	
	public static PublishMethod[] getLegacyIDMethods() {
		String legacyString = "REA"; // Only rea at this point in time

		List<PublishMethod> visibleLegacyIDMethods = new ArrayList<PublishMethod>();

		if (StringUtils.isNotBlank(legacyString)) {
			for (PublishMethod pm : getVisibleMethods()) {
				if (legacyString.contains(pm.name())) {
					visibleLegacyIDMethods.add(pm);
				}
			}
		}

		PublishMethod[] visibleLegacyIDArray = new PublishMethod[visibleLegacyIDMethods.size()];
		visibleLegacyIDMethods.toArray(visibleLegacyIDArray);

		return visibleLegacyIDArray;
	}
	
	
	/**
	 * all publish methods which have details that can be configured
	 * 
	 * @return
	 */
	public static PublishMethod[] getConfigurableMethods() {
		PublishMethod[] pmList = getVisibleMethods();
		pmList = (PublishMethod[]) ArrayUtils.removeElement(pmList, Print);
		pmList = (PublishMethod[]) ArrayUtils.removeElement(pmList, Website);
		
		return pmList;
	}
	
	/**
	 * all publish methods which have the lot limit applied
	 * 
	 * @return
	 */
	public static PublishMethod[] getVisibleLotLimitMethods() {
		PublishMethod[] lotLimitMethods = {REA, HomeSales};
		
		String visibleMethodsSetting = InitServlet.getSystemParams().getProperty("PublishMethods");
		
		if (StringUtils.isNotBlank(visibleMethodsSetting)) {
			List<PublishMethod> visibleLotLimitMethods = new ArrayList<PublishMethod>();

			for (PublishMethod pm : lotLimitMethods) {
				if (visibleMethodsSetting.contains(pm.name())) {
					visibleLotLimitMethods.add(pm);
				}
			}
			PublishMethod[] visibleLotLimitMethodsArray = new PublishMethod[visibleLotLimitMethods.size()];
			visibleLotLimitMethods.toArray(visibleLotLimitMethodsArray);

			return visibleLotLimitMethodsArray;
		} else {
			return lotLimitMethods;
		}
	}
	
	public static PublishMethod getFromFeedName(String name) { 
		if(name == null) throw new IllegalArgumentException("name cannot be null");
		for(PublishMethod pm: values()) { 
			if(pm.hasFeed() && pm.getFeedName().equals(name)) {
				return pm;
			}
		}
		throw new IllegalArgumentException("name did not match any registered feeds");
	}
	
	public static PublishMethod getFromPublishMethodName(String name) { 
		if(name == null) throw new IllegalArgumentException("name cannot be null");
		
		try {
			return PublishMethod.valueOf(name.replaceAll(" ", ""));
		} catch (Exception e) {
			System.out.println(String.format("Unable to load PublishMethod for '%s'", name));
		}
		
		throw new IllegalArgumentException("name did not match any registered publish methods");
	}
	
	public static boolean isFeedEnabled(PublishMethod pm) {
		PublishMethod[] visibleMethods = getVisibleMethods();

		for(PublishMethod vpm : visibleMethods) {
			if(pm == vpm) return true;
		}
		return false;
	}

	private static boolean isPublishMethodConfigured(PublishMethod pm) {
		logger.trace("Checking if isConfigured for {}" , pm);
		switch(pm) {
			case REA:
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("realestateUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("realestatePassword")));
			case Domain:
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("domainUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("domainPassword")));
			case MyPackage:
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("mypackageUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("mypackagePassword")));
			case Hnl:
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("hnlUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("hnlPassword")));
			case HomeSales:
				logger.trace("HomeSales user [{}], password [{}]", InitServlet.getSystemParam("homesalesUser"), InitServlet.getSystemParam("homesalesPassword"));
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("homesalesUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("homesalesPassword")));
			case Trademe:
				logger.trace("Trade user [{}], password [{}]", InitServlet.getSystemParam("trademeKey"), InitServlet.getSystemParam("trademeSecret"));
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("trademeKey")) && StringUtils.isNotBlank(InitServlet.getSystemParam("trademeSecret")) && StringUtils.isNotBlank(InitServlet.getSystemParam("trademeToken")) && StringUtils.isNotBlank(InitServlet.getSystemParam("trademeTokenSecret")));
			case AllHomes:
				logger.trace("AllHomes user [{}], password [{}]", InitServlet.getSystemParam("allhomesUser"), InitServlet.getSystemParam("allhomesPassword"));
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("allhomesUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("allhomesPassword")));
			case Reiwa:
				logger.trace("Reiwa user [{}], password [{}]", InitServlet.getSystemParam("reiwaUser"), InitServlet.getSystemParam("reiwaPassword"));
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("reiwaUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("reiwaPassword")));
			case MeHouse:
				logger.trace("MeHouse user [{}], password [{}]", InitServlet.getSystemParam("mehouseUser"), InitServlet.getSystemParam("mehousePassword"));
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("mehouseUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("mehousePassword")));
			case SeniorsHousingOnline:
				logger.trace("SeniorsHousingOnline user [{}], password [{}]", InitServlet.getSystemParam("SeniorsHousingOnlineUser"), InitServlet.getSystemParam("seniorshousingonlinePassword"));
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("seniorshousingonlineUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("seniorshousingonlinePassword")));
			case NewHomesGuide:
				logger.trace("NewHomesGuide user [{}], password [{}]", InitServlet.getSystemParam("newHomesGuideUser"), InitServlet.getSystemParam("newHomesGuidePassword"));
				return (StringUtils.isNotBlank(InitServlet.getSystemParam("newHomesGuideUser")) && StringUtils.isNotBlank(InitServlet.getSystemParam("newHomesGuidePassword")));
		}
		return false;

	}

	private static final String[] feedNames = {
		"RealEstate.com.au",
		"Domain",
		"My Package",
		"houseandland.com.au",
		"homesales.com.au",
		"trademe.co.nz",
		"allhomes.com.au",
		"Reiwa.com.au",
		"MeHouse.com.au",
		"SeniorsHousingOnline.com.au",
		"newhomesguide.com.au",
		"My Website",
		"Print"
	};
	
	private static final String[] feedSpec = {
		"realestateau",
		"domainau",
		"mypackageau",
		"hnlau",
		"homesalesau",
		"trademe",
		"allhomesau",
		"reiwaau",
		"mehouseau",
		"seniorshousingonlineau",
		"newhomesguideau",
		"website",
		"print"
	};
	
	private static final String[] shortNames = {
		"REA",
		"DM",
		"MP",
		"HNL",
		"HS",
		"TM",
		"AH",
		"REI",
		"ME",
		"SHO",
		"NHG",
		"Web",
		"P"
	};
	
	private static final String[] feedHost = {
		"reaxml.realestate.com.au",
		//"ftp.domain.com.au",
		"dolphin.fairfax.com.au",
		"ftp.mypackage.com.au",
		"feeder.houseandland.com.au",
		"feeds.homesales.com.au",
		TRADEME_HOST,
		"manage.allhomes.com.au",
		"ftp.reiwa.com.au",
		"ftp.mehouse.com.au",
		"xml.seniorshousingonline.com.au",
		NHG_HOST,
		null,
		null
	};
	
	private static final String[] names = {
		"REA",
		"Domain",
		"MyPackage",
		"Hnl",
		"HomeSales",
		"Trademe",
		"AllHomes",
		"Reiwa",
		"MeHouse",
		"SeniorsHousingOnline",
		"NewHomesGuide",
		"Website",
		"Print"
	};
	
	private static final String[] publishers = {
		"realestate.com.au",
		"domain.com.au",
		"mypackage.com.au",
		"houseandland.com.au",
		"homesales.com.au",
		"trademe.co.nz",
		"allhomes.com.au",
		"reiwa.com.au",
		"mehouse.com.au",
		"seniorshousingonline.com.au",
		"newhomesguide.com.au",
		"Website",
		null
	};
	
	private static final BaseColor[] colors = {
		new BaseColor(202, 0, 29),
		new BaseColor(109, 153, 42),
		new BaseColor(0, 77, 156),
		new BaseColor(34, 172, 223),
		new BaseColor(169, 0, 97),
		new BaseColor(255, 227, 116),
		new BaseColor(227, 6, 19),
		new BaseColor(0, 166, 162),
		new BaseColor(0, 166, 162),
		new BaseColor(0, 166, 162),
		new BaseColor(0, 173, 239),
		new BaseColor(0, 166, 162),
		new BaseColor(0, 100, 162)
	};
	
	private static final String[] beanNames = {
		"rea",
		"domain",
		"mypackage",
		"hnl",
		"homesales",
		"trademe",
		"allhomes",
		"reiwa",
		"mehouse",
		"seniorshousingonline",
		"newhomesguide",
		"website",
		"print"
	};
}