package com.sok.runway.crm.searchbuilder;

public class SearchTypeOptions {
    private SearchStatement searchStatement;
    
    public SearchTypeOptions(SearchStatement searchStatement) {
        this.searchStatement = searchStatement;
    }
    
    public Option[] getSearchOptions() {
        String[] searchTypes = SearchType.valueStrings();
        Option[] options = new Option[searchTypes.length];
        
        for(int i = 0; i < searchTypes.length; i++) {
            String searchType = searchTypes[i];
            Option option = new Option();
            option.setValue(searchType);
            option.setText(searchType);
            
            if(this.searchStatement != null) {
                SearchType selectedSearchType = this.searchStatement.getSearchType();
                if(selectedSearchType != null && 
                        searchType.equalsIgnoreCase(selectedSearchType.toString())) {
                    option.setSelected(true);
                }
            }
            
            options[i] = option;
        }
        
        return options;
    }
}
