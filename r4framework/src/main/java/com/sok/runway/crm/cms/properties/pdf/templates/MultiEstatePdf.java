package com.sok.runway.crm.cms.properties.pdf.templates;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.sok.runway.crm.cms.properties.HouseLandPackage;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.pdf.AbstractPDFTemplate;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.ListFooter;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFConstants;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil;
import com.sok.runway.crm.cms.properties.pdf.templates.utils.PDFUtil.Estate;
import com.sok.runway.crm.cms.properties.pdf.templates.model.Package;
import com.sok.runway.offline.ThreadManager;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultiEstatePdf extends AbstractPDFTemplate implements PDFConstants {
	
	public static final Logger logger = LoggerFactory.getLogger(MultiEstatePdf.class);
	
	
	public void renderPDF(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException,
			DocumentException {
		try {
			renderPDFConcurrent(request, response, document);
			return;
		} catch (ExecutionException ee) {
			logger.error("Execution Exception", ee);
		} catch (InterruptedException ie) {
			logger.error("Execution Interrupted", ie);
		}
		
		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (List<PropertyEntity>)request.getAttribute("entityList"); 
		if(properties == null) {
			setStatus(request,"Loading Records..", 0.02d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			properties = PropertyFactory.getPropertyEntities(request, this);
		}
		
		HashMap<String, Estate> estates = new HashMap<String, Estate>();
		
		double num = new Double(properties.size()), record = 1;
		for (PropertyEntity property : properties) {
			if(record % 5 == 0) {
				setStatus(request, "Loading package information" , record / num);
			}
			record++;
			
			if (!property.isPropertyType(PropertyType.HouseLandPackage)) {
				throw new RuntimeException(String.format("ProductID [%1$s] Property [%2$s] is not a house and land package",
						property.getString("ProductID"), property.getPropertyType()));
			}
			HouseLandPackage product = (HouseLandPackage) property;
			Package p = PDFUtil.getPackage(product);

			Estate e = (Estate) estates.get(p.estateName);
			if (e == null) {
				e = new PDFUtil.Estate();
				e.estateName = p.estateName;
				e.estateLogo = PropertyEntity.resizeImage(p.estateLogo, 140, 65, PropertyEntity.ResizeOption.Fit);
				estates.put(p.estateName, e);
			}
			e.packages.add(p);
		}
		setStatus(request, "Generating PDF", 0.8d);
		
		renderPages(request, document, estates);
	}

	private void renderPages(HttpServletRequest request, com.itextpdf.text.Document document, HashMap<String, Estate> estates) throws DocumentException {
		TreeSet<String> sortedEstates = new TreeSet<String>(estates.keySet());
		boolean addNewPage = false;

		for (String estateName : sortedEstates) {
			if (addNewPage)
				document.newPage();
			else
				addNewPage = true;

			Estate estate = (Estate) estates.get(estateName);
			renderEstatePage(request, document, estate);
		}
	}

	private void renderEstatePage(HttpServletRequest request, com.itextpdf.text.Document document, Estate e) throws DocumentException {
		Collections.sort(e.packages, new PDFUtil.PackageNameComparator());
		PdfPTable hdrTable = PDFUtil.getHeader(request, e, null);

		PdfPTable pageTable = new PdfPTable(1);
		pageTable.getDefaultCell().setBorder(0);
		pageTable.getDefaultCell().setPadding(0);
		pageTable.getDefaultCell().setFixedHeight(0f);
		pageTable.setWidthPercentage(100f);

		pageTable.addCell(hdrTable);

		pageTable.getDefaultCell().setFixedHeight(15f);
		pageTable.addCell(BLANK);

		for (int j = 0; j < e.packages.size(); j++) {
			if (j != 0 && j % 5 == 0) {
				document.add(pageTable);
				document.newPage();
				pageTable = new PdfPTable(1);
				pageTable.getDefaultCell().setBorder(0);
				pageTable.getDefaultCell().setPadding(0);
				pageTable.getDefaultCell().setFixedHeight(0f);
				pageTable.setWidthPercentage(100f);

				pageTable.addCell(hdrTable);
				pageTable.getDefaultCell().setFixedHeight(15f);
				pageTable.addCell(BLANK);
			}

			pageTable.getDefaultCell().setFixedHeight(0f);
			pageTable.getDefaultCell().setBorderWidthTop(0.01f);
			pageTable.getDefaultCell().setBorderWidthBottom(0.01f);
			pageTable.getDefaultCell().setBorderColor(BaseColor.BLACK);

			Package p = (Package) e.packages.get(j);
			pageTable.addCell(PDFUtil.buildPackageTable(p, true));

			pageTable.getDefaultCell().setBorder(0);
			pageTable.getDefaultCell().setFixedHeight(10f);
			pageTable.addCell(BLANK);

		}

		document.add(pageTable);
	}

	public PdfPageEventHelper getFooter(HttpServletRequest request) {
		return new ListFooter(request);
	}
	
	public void renderPDFConcurrent(HttpServletRequest request, HttpServletResponse response, Document document) throws IOException,
			DocumentException, ExecutionException, InterruptedException {
		
		List<PropertyEntity> properties = (List<PropertyEntity>)request.getAttribute("entityList"); 
		if(properties == null) {
			setStatus(request,"Loading Records..", 0.02d);
			logger.error("RELOADING PROPERTY ENTITIES SHOULD ALREADY BE LOADED");
			PropertyFactory.getPropertyEntities(request, this);
		}
		
		final int size = properties.size();
		double num = new Double(size), record = 1;
		final List<Future<Package>> packageList = new ArrayList<Future<Package>>(size);
		
		for (final PropertyEntity property : properties) {
			if(record % 5 == 0) {
				setStatus(request, "Loading package information" , record / num);
			}
			record++;
			if (!property.isPropertyType(PropertyType.HouseLandPackage)) {
				throw new RuntimeException(String.format("ProductID [%1$s] Property [%2$s] is not a house and land package",
						property.getString("ProductID"), property.getPropertyType()));
			}
			final HouseLandPackage product = (HouseLandPackage) property;
			
			packageList.add(ThreadManager.startCallable(new Callable<Package>() {
				public Package call() {
					return PDFUtil.getPackage(product);
				}
			}));
		}
		
		Map<String, EstateTableView> etvMap = new HashMap<String, EstateTableView>();
		for(Future<Package> fp: packageList) {
			Package p = fp.get();
			EstateTableView e = etvMap.containsKey(p.estateName) ? etvMap.get(p.estateName) : null;
			if(e == null) {
				e = new EstateTableView(request, p);
				etvMap.put(p.estateName, e);
			}
			e.addPackage(p);
		}
		final TreeSet<EstateTableView> sortedEstates = new TreeSet<EstateTableView>(etvMap.values());
		boolean addNewPage = false;

		for (EstateTableView estate : sortedEstates) {
			if (addNewPage)
				document.newPage();
			else
				addNewPage = true;
			estate.render(document);
		}
	}
	
	private static class EstateTableView implements Comparable<EstateTableView> {
		private final String estateName;
		private final TreeSet<PackageTableView> packageTables = new TreeSet<PackageTableView>();
		private Future<PdfPTable> estateHeader;
		public EstateTableView(final HttpServletRequest request, final Package p) {
			estateName = p.estateName;
			estateHeader = ThreadManager.startCallable(new Callable<PdfPTable>() {
				public PdfPTable call() throws DocumentException {
					return PDFUtil.getHeader(request, getEstate(p), null);
				}
				private Estate getEstate(Package p) {
					Estate e = new PDFUtil.Estate();
					e.estateName = p.estateName;
					e.estateLogo = PropertyEntity.resizeImage(p.estateLogo, 140, 65, PropertyEntity.ResizeOption.Fit);
					return e;
				}
			});
		}
		public void addPackage(final Package p) throws DocumentException {
			packageTables.add(new PackageTableView(p));
		}
		
		public void render(Document document) throws DocumentException, ExecutionException, InterruptedException { 
			final PdfPTable hdrTable = estateHeader.get();
			PdfPTable pageTable = new PdfPTable(1);
			pageTable.getDefaultCell().setBorder(0);
			pageTable.getDefaultCell().setPadding(0);
			pageTable.getDefaultCell().setFixedHeight(0f);
			pageTable.setWidthPercentage(100f);

			pageTable.addCell(hdrTable);
			pageTable.getDefaultCell().setFixedHeight(15f);
			pageTable.addCell(BLANK);
			
			int i=0;
			for(PackageTableView ptv: packageTables) {
				if (i != 0 && i % 5 == 0) {
					document.add(pageTable);
					document.newPage();
					pageTable = new PdfPTable(1);
					pageTable.getDefaultCell().setBorder(0);
					pageTable.getDefaultCell().setPadding(0);
					pageTable.getDefaultCell().setFixedHeight(0f);
					pageTable.setWidthPercentage(100f);

					pageTable.addCell(hdrTable);
					pageTable.getDefaultCell().setFixedHeight(15f);
					pageTable.addCell(BLANK);
				}
				pageTable.getDefaultCell().setFixedHeight(0f);
				pageTable.getDefaultCell().setBorderWidthTop(0.01f);
				pageTable.getDefaultCell().setBorderWidthBottom(0.01f);
				pageTable.getDefaultCell().setBorderColor(BaseColor.BLACK);
				pageTable.addCell(ptv.table.get());
				pageTable.getDefaultCell().setBorder(0);
				pageTable.getDefaultCell().setFixedHeight(10f);
				pageTable.addCell(BLANK);
				i++;
			}
			document.add(pageTable);
		}
		@Override
		public int compareTo(EstateTableView arg1) {
			return this.estateName.compareTo(arg1.estateName);
		}
	}
	
	private static class PackageTableView implements Comparable<PackageTableView> {
		public final String name;
		public final Future<PdfPTable> table;
		public PackageTableView(final Package p) throws DocumentException {
			this.name = p.name  + p.lotNo;
			this.table = ThreadManager.startCallable(new Callable<PdfPTable>() {
				public PdfPTable call() throws DocumentException {
					return PDFUtil.buildPackageTable(p, true);
				}
			});
		}
		@Override
		public int compareTo(PackageTableView arg0) {
			return this.name.compareTo(arg0.name);
		}
	}
}