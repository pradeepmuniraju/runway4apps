package com.sok.runway.crm.admin;

import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys; 
import com.sok.runway.*;
import com.sok.runway.crm.*;
import com.sok.runway.security.*;
import com.sok.runway.crm.factories.*;
import com.sok.runway.crm.interfaces.*;
import javax.servlet.ServletRequest;
import java.sql.Connection;
import java.util.*;


public class Stage extends SecuredEntity implements DocumentLinkable {

   public static final String ENTITY_NAME = "Stage";
   
	public Stage(Connection con) {
		
		super(con, "StageView");
	}
	
	public Stage(ServletRequest request) {
	    
		super(request, "StageView");
		populateFromRequest(request.getParameterMap());
	}
   
	public Stage(Connection con, String stageID) {
	   	
		this(con);
	   load(stageID);
	}

   public String getEntityName() {
	   return ENTITY_NAME;
   }
        
	public Validator getValidator() {
		
		Validator val = Validator.getValidator("Stage");
		
	   if (val == null) {
		   val = new Validator();
		   val.addMandatoryField("StageID","SYSTEM ERROR - StageID");
		   val.addMandatoryField("Name","Stage must have a Name");
		   Validator.addValidator("Stage", val);
	   }
	   
	   return val;
	}
	
   /**
    *    Linkable Interface
    */
   public String getLinkableTitle() {      
      return getField("Name");
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs() {
      return getLinkableDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getLinkableDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getLinkableDocumentIDs(this, filterParameters);
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs() {
      return getDocumentIDs(null);
   }
   
   /**
    *    DocumentLinkable Interface
    */
   public Collection<String> getDocumentIDs(Map<String,String> filterParameters) {
      return LinkableDocumentFactory.getDocumentIDs(this, filterParameters);
   }      
   
   /**
    *    DocumentLinkable Interface
    */
   public LinkableDocument getLinkableDocument() {
      LinkableDocument doc = new LinkedDocument(getConnection());
      doc.setCurrentUser(getCurrentUser());
      return doc;
   }
    
   /**
    *    DocumentLinkable Interface
    */
   public void linkDocument(String documentID) {
      LinkableDocumentFactory.linkDocument(this, documentID);
   }
}