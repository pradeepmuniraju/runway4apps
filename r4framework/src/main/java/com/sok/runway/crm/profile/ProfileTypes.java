package com.sok.runway.crm.profile;

public interface ProfileTypes {
   public static final String PROFILE_TYPE_CHECKLIST = "CheckListProfile";
   public static final String PROFILE_TYPE_COMPANY = "CompanyProfile";
   public static final String PROFILE_TYPE_CONTACT = "ContactProfile";
   public static final String PROFILE_TYPE_ITEM_LIST = "ItemListProfile";
   public static final String PROFILE_TYPE_NOTE = "NoteProfile";
   public static final String PROFILE_TYPE_OPPORUTNITY = "OpportunityProfile";
   public static final String PROFILE_TYPE_ORDER = "OrderProfile";
   public static final String PROFILE_TYPE_ORDER_ITEM = "OrderItemProfile";
   //public static final String PROFILE_TYPE_PIPELINE = "ProcessProfile";
   public static final String PROFILE_TYPE_PRODUCT = "ProductProfile";
   
   public static final String[] ALL_PROFILE_TYPES = {
      PROFILE_TYPE_CHECKLIST,
      PROFILE_TYPE_COMPANY, 
      PROFILE_TYPE_CONTACT,
      PROFILE_TYPE_ITEM_LIST,
      PROFILE_TYPE_NOTE,
      PROFILE_TYPE_OPPORUTNITY,
      PROFILE_TYPE_ORDER,
      PROFILE_TYPE_ORDER_ITEM,
      //PROFILE_TYPE_PIPELINE,
      PROFILE_TYPE_PRODUCT};
}
