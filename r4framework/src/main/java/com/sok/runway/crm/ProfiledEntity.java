package com.sok.runway.crm;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletRequest;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.SpecManager;
import com.sok.runway.ErrorMap;
import com.sok.runway.crm.profile.ProfileFactory;
import com.sok.runway.security.SecuredEntity;

public abstract class ProfiledEntity extends SecuredEntity {
   
   public static final String PROFILE_PREFIX = "QUESTIONID-";
   public static final String QUANTIFIER_SEPERATOR = "::";
   
   public static final int FIELD_TYPE_STRING = 0;
   public static final int FIELD_TYPE_DATE = 1;
   public static final int FIELD_TYPE_NUMBER = 2;
   public static final int FIELD_TYPE_CURRENCY = 3;
   
   private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
   
   private GenRow questionAnswers = null;
   private boolean profilesCleared = true;
   
   private HashMap<String, ProfileAnswer> savedAnswers = null;
   private HashMap<String, ProfileAnswer> updatedProfileAnswers = null;
   private HashMap<String, QuestionMetaData> questionMetaData = null;
   
   private Answer answer = null;
   
   public ProfiledEntity(Connection con, String viewSpec) {
      super(con, viewSpec);
   }
	
	public ProfiledEntity(ServletRequest request, String viewSpec) {
	    super(request, viewSpec);
	}
   
   public abstract String getAnswerViewSpec(); 
   
   public void clear() {
      super.clear();
      clearProfiles();
   }
   
   public void dupe() {
	  loadProfiles();
	  if (savedAnswers != null) {
	      if (updatedProfileAnswers == null) {
	          updatedProfileAnswers = new HashMap<String, ProfileAnswer>();
	      }
		  updatedProfileAnswers.putAll(savedAnswers);
        savedAnswers.clear();
	  }
	  super.dupe();
   }
   
   
   public void clearProfiles() {
      if (savedAnswers != null) {
         savedAnswers.clear();
      }
      if (updatedProfileAnswers != null) {
         updatedProfileAnswers.clear();
      }
      profilesCleared = true;
   }
   
   public void loadProfiles() {
      if (isPersisted()) {
         if (profilesCleared && getPrimaryKey().length() != 0) {
            if (questionAnswers == null) {
               questionAnswers = new GenRow();
            }
            if (savedAnswers == null) {
               savedAnswers = new HashMap<String, ProfileAnswer>();
            }
            questionAnswers.clear();
            questionAnswers.setConnection(getConnection());
            questionAnswers.setViewSpec(getAnswerViewSpec());
            questionAnswers.setColumn(getPrimaryKeyName(), getPrimaryKey());
            questionAnswers.doAction(ActionBean.SEARCH);
            questionAnswers.getResults();
            while (questionAnswers.getNext()) {
               savedAnswers.put(questionAnswers.getData("QuestionID"), new ProfileAnswer(questionAnswers.getData("AnswerText") , 
                                                                                          questionAnswers.getDate("AnswerDate"), 
                                                                                          questionAnswers.getDouble("AnswerNumber"),
                                                                                          /*questionAnswers.getData("InputType"),
                                                                                          questionAnswers.getData("Label"),
                                                                                          questionAnswers.getData("ValueList"),*/
                                                                                          questionAnswers.getInt("Quantifier")));
            }
            questionAnswers.close();
            profilesCleared = false;
         }
      }
   }
   
   private HashMap<String, QuestionMetaData> getProfileMetaData() {
      if (questionMetaData == null) {
         questionMetaData = new HashMap<String, QuestionMetaData>();
      
         ProfileFactory factory = ProfileFactory.getProfileFactory();
         
         Collection<String> surveyIDs = factory.getSurveyIDsForEntity(this);
         
         if (surveyIDs != null) {
            GenRow questions = new GenRow();
            questions.setConnection(getConnection());
            questions.setViewSpec("QuestionView");
            
            for (String sID : surveyIDs) {
               questions.setParameter("QuestionSurveyQuestion.SurveyID", sID);
               questions.doAction(ActionBean.SEARCH);
               questions.getResults();
               
               while (questions.getNext()) {
                  
                  questionMetaData.put(questions.getData("QuestionID"), new QuestionMetaData( questions.getData("InputType"),
                                                                                             questions.getData("Label"),
                                                                                             questions.getData("ValueList")
                                                                                          ));
               }
            }
            questions.close();
         }
      }
      return questionMetaData;
   }
   
   /*public abstract void something(TableData survey) {
      
      survey.setParameter();
   }*/
   
   private ProfileAnswer getAnswerForQuestion(String questionID) {
      loadProfiles();
      if (savedAnswers != null) {
         return (ProfileAnswer)savedAnswers.get(questionID);
      }
      else {
         return null;
      }
   }

   /**
    * Takes a questionID and returns the InputType of the answer
    * @param questionID
    * @return InputType
    * @deprecated
    */
   public String getInputType(String questionID) {      
      return getProfileInputType(questionID);
   }

   /**
    * Takes a questionID and returns the InputType of the answer
    * @param questionID
    * @return InputType
    */
   public String getProfileInputType(String questionID) {    
      QuestionMetaData metaData = getProfileMetaData().get(questionID);
      if (metaData != null) {
         return metaData.getInputType();
      }
      else {
         return null;
      }
   }

   /**
    * Takes a questionID and returns the Label for the answer
    * @param questionID
    * @return Label
    */
   public String getProfileLabel(String questionID) {
      QuestionMetaData metaData = getProfileMetaData().get(questionID);
      if (metaData != null) {
         return metaData.getLabel();
      }
      else {
         return null;
      }
   }
   /**
    * Takes a questionID and returns the ValueList for the answer
    * @param questionID
    * @return ValueList
    */
   public String getProfileValueListName(String questionID) {
      QuestionMetaData metaData = getProfileMetaData().get(questionID);
      if (metaData != null) {
         return metaData.getValueListName();
      }
      else {
         return null;
      }
   }
   
   public Number getProfileNumber(String questionID) {
      ProfileAnswer theAnswer = getAnswerForQuestion(questionID);
      if (theAnswer != null) {
         return theAnswer.getDouble();
      }
      return null;
   }
   
   public int getProfileInt(String questionID) {
      Number num = getProfileNumber(questionID);
      if (num != null) {
         return num.intValue();
      }
      return 0;
   }
   
   public long getProfileLong(String questionID) {
      Number num = getProfileNumber(questionID);
      if (num != null) {
         return num.longValue();
      }
      return 0l;
   }
   
   public float getProfileFloat(String questionID) {
      Number num = getProfileNumber(questionID);
      if (num != null) {
         return num.floatValue();
      }
      return 0f;
   }
   
   public double getProfileDouble(String questionID) {
      Number num = getProfileNumber(questionID);
      if (num != null) {
         return num.doubleValue();
      }
      return 0d;
   }
   
   public Date getProfileDate(String questionID) {
      ProfileAnswer theAnswer = getAnswerForQuestion(questionID);
      if (theAnswer != null) {
         return theAnswer.getDate();
      }
      else {
         return null;
      }
   }
   
   public String getProfileField(String questionID) {
      return getProfileField(questionID, FIELD_TYPE_STRING);
   }
   
   public String getProfileField(String questionID, int displayType) {
      Map<String, ProfileAnswer> updatedAnswers = getUpdatedAnswers();
      String answer = null;
      if (updatedAnswers != null) {
         ProfileAnswer newAnswer = (ProfileAnswer)updatedAnswers.get(questionID);
         if (newAnswer != null) {
            answer = newAnswer.getAnswer();
         }
      }
      if (answer == null) {
         ProfileAnswer theAnswer = getAnswerForQuestion(questionID);
         if (theAnswer != null) {
            answer = theAnswer.getAnswer(displayType);
         }
      }
      if (answer == null) {
         answer = ActionBean._blank;
      }
      return answer;
   }
   
   public int getProfileQuantifier(String questionID) {
      Map<String, ProfileAnswer> updatedAnswers = getUpdatedAnswers();
      int answer = -1;
      if (updatedAnswers != null) {
         ProfileAnswer newAnswer = (ProfileAnswer)updatedAnswers.get(questionID);
         if (newAnswer != null) {
            answer = newAnswer.getQuantifier();
         }
      }
      if (answer == -1) {
         ProfileAnswer theAnswer = getAnswerForQuestion(questionID);
         if (theAnswer != null) {
            answer = theAnswer.getQuantifier();
         }
      }
      if (answer == -1) {
         answer = 0;
      }
      return answer;
   }
   
   public void setProfileField(String questionID, String value) {
      setProfileField(questionID, value, 0);
   }
   
   public void setProfileField(String questionID, String value, int quantifier) {
      if (updatedProfileAnswers == null) {
         updatedProfileAnswers = new HashMap<String, ProfileAnswer>();
      }
      updatedProfileAnswers.put(questionID, new ProfileAnswer(value, quantifier));
   }
   
   
   public void populateFromRequest(Map map, String prefix) {
      super.populateFromRequest(map, prefix);
      if (prefix != null) {
         String prefixToken = prefix + "@" + PROFILE_PREFIX;
         
         Iterator iter = map.keySet().iterator();
         String key = null;
         String[] values = null;
         int quantifier = 0;
         String value = null;
         while (iter.hasNext()) {
            key = (String)iter.next();
            
            if (key.startsWith(prefixToken)) {
               values = (String[])map.get(key);
               
               if (values.length == 1) {
                  quantifier = getQuantifier(values[0]);
                  value = stripQuantifier(values[0]);
                  setProfileField(key.substring(prefixToken.length()), value, quantifier);
               }
               else {
                  StringBuffer val = new StringBuffer();
                  quantifier = 0;
                  for (int i=0; i < values.length; i++) {
                     quantifier = quantifier + getQuantifier(values[i]);
                     value = stripQuantifier(values[i]);
                     if (value.length() > 0) {
                        if (val.length() > 0) {
                           val.append("+");
                        }
                        val.append(value);
                     }
                  }
                  setProfileField(key.substring(prefixToken.length()), val.toString(), quantifier);
               }
            }
         }
         
      }
   }
      
   public void populateFromRequest(Map map, boolean load) {
      super.populateFromRequest(map, load);
      
      Iterator iter = map.keySet().iterator();
      String key = null;
      String[] values = null;
      int quantifier = 0;
      String value = null;
      while (iter.hasNext()) {
         key = (String)iter.next();
         
         if (key.startsWith(PROFILE_PREFIX)) {
            values = (String[])map.get(key);
            if (values.length == 1) {
               quantifier = getQuantifier(values[0]);
               value = stripQuantifier(values[0]);
               setProfileField(key.substring(PROFILE_PREFIX.length()), value, quantifier);
            }
            else {
               StringBuffer val = new StringBuffer();
               quantifier = 0;
               for (int i=0; i < values.length; i++) {
                  quantifier = quantifier + getQuantifier(values[i]);
                  value = stripQuantifier(values[i]);
                  if (value.length() > 0) {
                     if (val.length() > 0) {
                        val.append("+");
                     }
                     val.append(value);
                  }
               }
               setProfileField(key.substring(PROFILE_PREFIX.length()), val.toString(), quantifier);
            }
         }
      }
      
   }
   
   private String stripQuantifier(String value) {
      if (value.indexOf(QUANTIFIER_SEPERATOR) >= 0 ) {
         return value.substring(0, value.indexOf(QUANTIFIER_SEPERATOR));
      }
      else {
         return value;
      }
   }
   
   private int getQuantifier(String value) {
      int qt = 0;
      if (value.indexOf(QUANTIFIER_SEPERATOR) >= 0 ) {
         String quantifier = value.substring(value.indexOf(QUANTIFIER_SEPERATOR)+2);
         try {
            qt = Integer.parseInt(quantifier);
         }
         catch (Exception e) {}
      }
      return qt;
   }
   
   public HashMap<String, ProfileAnswer> getUpdatedAnswers() {
      return updatedProfileAnswers;
   }
   
   private void saveAnswer(String questionID) {
      if (answer == null) {
         answer = new Answer(getConnection(), getAnswerViewSpec());
         answer.setCurrentUser(getCurrentUser());
      } else { 
    	  /* ensures we have a current connection */ 
    	  answer.setConnection(getConnection());
      }
      answer.clear();
      
      int quantifier = 0;
      
      ProfileAnswer theAnswer = getAnswerForQuestion(questionID);   
      if (theAnswer != null) {
         quantifier = theAnswer.getQuantifier();
      }
      ProfileAnswer newAnswer = (ProfileAnswer)updatedProfileAnswers.get(questionID);
      
      answer.loadAnswer(questionID, getPrimaryKey(), getPrimaryKeyName());
      answer.setParameter("Answer", newAnswer.getShortAnswer());
      answer.setParameter("AnswerText", newAnswer.getAnswer());
      
      if (quantifier > 0 && quantifier < newAnswer.getQuantifier()) {
         answer.setParameter("Quantifier", String.valueOf(quantifier));
      }
      else {
         answer.setParameter("Quantifier", String.valueOf(newAnswer.getQuantifier()));
      }
      
      if (isDate(newAnswer.getAnswer())) {
         answer.setParameter("AnswerDate", newAnswer.getAnswer());
      }
      else if (isNumeric(newAnswer.getAnswer())) {
         answer.setParameter("AnswerNumber", newAnswer.getAnswer());
      }
      
      if (theAnswer == null) {
         answer.insert();
      }
      else {
         // Only update Answer if it has changed
         String currentAnswer = theAnswer.getAnswer();
         if (currentAnswer == null || !currentAnswer.equals(newAnswer.getAnswer())) {
            answer.update();
         }
      }
      if (savedAnswers == null) {
          savedAnswers = new HashMap<String, ProfileAnswer>();
      }
      savedAnswers.put(questionID, new ProfileAnswer(answer.getField("AnswerText") , newAnswer.getQuantifier()));
   }
   
   public ErrorMap update() {
      ErrorMap map = super.update();
      
      updateProfileAnswers();
      
      return map;
   }
   
   public void updateProfileAnswers() {
      if (updatedProfileAnswers != null) {
         Iterator<String> iter = updatedProfileAnswers.keySet().iterator();
         int i = 0;
         while (iter.hasNext()) {
        	 i++;
        	 String questionID = (String)iter.next();
            saveAnswer(questionID);
         }
      }
   }
   
   public ErrorMap insert() {
      ErrorMap map = super.insert();
      
      if (map.isEmpty()) {
         updateProfileAnswers();
      }
      return map;
   }
   
   /**
    * Invokes delete() from parent class (SecuredEntity) then, if 
    * successful, deletes all Answers related this object using GenRow.
    */
   public boolean delete() {
      if (super.delete())  {
      	GenRow deleteObj = new GenRow();
      	
      	deleteObj.setConnection(getConnection());
      	deleteObj.setViewSpec(getAnswerViewSpec());
      	deleteObj.setParameter(ActionBean.UPDATEON + getPrimaryKeyName(), getPrimaryKey());
      	deleteObj.doAction(ActionBean.DELETEALL);
      	
         return true;
      }
      else  {
         return false;
      }
   }
      
   private boolean isNumeric(String value) {
      try {
         Double.parseDouble(value);
         return true;
      }
      catch (Exception e) {
         return false;
      }
   }
   
   private synchronized boolean isDate(String value) {
      try {
         dateFormat.parse(value);
         return true;
      }
      catch (Exception e) {
         return false;
      }
   }
   
   private class QuestionMetaData {
      String inputType = null;
      String label = null;
      String valueListName = null;
      
      public QuestionMetaData (String inputType, String label, String valueListName) {
         this.inputType = inputType;
         this.label = label;
         this.valueListName = valueListName;
      }
      
      public String getInputType() {
         return inputType;
      }
      
      public String getLabel() {
         return label;
      }
      
      public String getValueListName() {
         return valueListName;
      }
   }
   
   private class ProfileAnswer {
      int quantifier = 0;
      String theAnswer = null;
      Date dateAnswer = null;
      Double doubleAnswer = null;
      /*String inputType = null;
      String label = null;
      String valueList = null;
      */
      
      public ProfileAnswer (String theAnswer, int quantifier) {
         this.theAnswer = theAnswer;
         this.quantifier = quantifier;
      }
      
      //public ProfileAnswer (String theAnswer, Date dateAnswer, double doubleAnswer, String inputType, String label, String valueList, int quantifier) {
      public ProfileAnswer (String theAnswer, Date dateAnswer, double doubleAnswer, int quantifier) {
         this.theAnswer = theAnswer;
         this.dateAnswer = dateAnswer;
         this.doubleAnswer = new Double(doubleAnswer);
         /*this.inputType = inputType;
         this.label = label;
         this.valueList = valueList;*/
         this.quantifier = quantifier;
      }
         
      public String getAnswer(int type) {
         if (type == FIELD_TYPE_DATE) {
            String format = SpecManager.getViewSpec(getAnswerViewSpec()).getFormatForName("AnswerDate");
            if (format == null) {
               return theAnswer;
            }
            else {
               return ActionBean.formatValue(null, format, null, dateAnswer, null, null);
            }
         }
         else if (type == FIELD_TYPE_NUMBER) {
            String format = SpecManager.getViewSpec(getAnswerViewSpec()).getFormatForName("AnswerNumber");
            if (format == null) {
               return theAnswer;
            }
            else {
               return ActionBean.formatValue(null, format, null, doubleAnswer, null, null);
            }
         }
         else if (type == FIELD_TYPE_CURRENCY) {
            String format = SpecManager.getViewSpec(getAnswerViewSpec()).getFormatForName("AnswerCurrency");
            if (format == null) {
               return theAnswer;
            }
            else {
               return ActionBean.formatValue(null, format, ActionBean.TYPE_CURRENCY, doubleAnswer, null, null);
            }
         }
         else {
            return theAnswer;
         }
      }
      
      public String getShortAnswer() {
    	  if(theAnswer != null) { 
    		  return theAnswer.length()>200?theAnswer.substring(0,200):theAnswer;
    	  }
    	  return theAnswer;
      }
      
      public String getAnswer() { 
    	  return theAnswer; 
      }
      
      public Date getDate() {
         return dateAnswer;
      }
      
      public Double getDouble() {
         return doubleAnswer;
      }
      
      public int getQuantifier() {
         return quantifier;
      }
      
      /*public String getInputType() {
         return inputType;
      }
      
      public String getLabel() {
         return label;
      }
      
      public String getValueList() {
         return valueList;
      }*/
   }
}