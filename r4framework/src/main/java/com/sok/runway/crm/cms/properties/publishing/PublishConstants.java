package com.sok.runway.crm.cms.properties.publishing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by pradeeprunway on 20/04/2017.
 */
public class PublishConstants {

    private final static Logger logger = LoggerFactory.getLogger(PublishConstants.class);

    private static Map<String, String> PUBLISH_OPTIONS = new HashMap<String,String>();
    static {
        PUBLISH_OPTIONS.put("Approved", "grey-tick");
        PUBLISH_OPTIONS.put("Published", "green-tick");
        PUBLISH_OPTIONS.put("Pre-release", "yellow-exclaim");
        PUBLISH_OPTIONS.put("Unpublished", "grey-exclaim");
        PUBLISH_OPTIONS.put("To be approved", "grey-exclaim");
        PUBLISH_OPTIONS.put("To be published", "grey-exclaim");
        PUBLISH_OPTIONS.put("Invalid", "red-exclaim");
        PUBLISH_OPTIONS.put("Content Required", "red-exclaim");
        PUBLISH_OPTIONS.put("Removing", "yellow-tick");
        PUBLISH_OPTIONS.put("Expired", "green-minus");
    }

    public static Set<String> getPublishOptions() {
        return PUBLISH_OPTIONS.keySet();
    }
    /* may want to change this implementation later, so lets keep it here */
    public static String getPublishOptionClass(String s) {
        return PUBLISH_OPTIONS.get(s);
    }

}
