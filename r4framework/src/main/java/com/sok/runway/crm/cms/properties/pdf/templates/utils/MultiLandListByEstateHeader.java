package com.sok.runway.crm.cms.properties.pdf.templates.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.sok.framework.InitServlet;
import com.sok.runway.crm.cms.properties.pdf.templates.MultiLandListPdfByEstate;

public class MultiLandListByEstateHeader extends PdfPageEventHelper implements PDFConstants {
	static Logger logger;
	
	public static String disclaimerText;
	private static int pageNumber = 0;
	String estateLogo;
	public static String dripImage;

	public MultiLandListByEstateHeader(HttpServletRequest request){
		logger = LoggerFactory.getLogger(getClass());
		String estateID = request.getParameter("ProductID");
		System.out.println("Estate ID is: " + estateID);
		
		if(!StringUtils.equals("", estateID) && !StringUtils.equals("-1", estateID)){
			estateLogo = MultiLandListPdfByEstate.getEstateLogo(estateID, request);	
		}
	}
	
	public void onStartPage(PdfWriter writer, Document document) {
		logger.debug("onStartPage");
	}
	
	/**
	 * Called when a page is finished, just before being written to the document.
	 */
	public void onEndPage(PdfWriter writer, Document document) {		
		try
		{
			pageNumber = writer.getPageNumber();
			logger.debug("pageNumber: " + pageNumber);
			System.out.println("pageNumber: " + pageNumber);
			
			//Render page header
			PdfPTable pageTable = new PdfPTable(1);
			PDFUtil.initMultiLandListbyEstateDefaultCell(pageTable);
			
			PdfPTable contentTbl;
			if(StringUtils.isNotBlank(estateLogo)) {
				contentTbl = renderPageHeader(estateLogo);
				pageTable.setTotalWidth(document.right() - document.left());
				pageTable.addCell(contentTbl);		
			}
			
			PdfPTable apartmentHeader;			
			apartmentHeader = MultiLandListPdfByEstate.renderTableHeader();
			pageTable.addCell(apartmentHeader);			
			pageTable.writeSelectedRows(0, -1, 15, 858, writer.getDirectContent());
			
			//Render page footer
			pageTable = new PdfPTable(1);
			PDFUtil.initMultiLandListbyEstateDefaultCell(pageTable);
			contentTbl = renderPageFooter();
			pageTable.setTotalWidth(document.right() - document.left());
			pageTable.addCell(contentTbl);	
			pageTable.writeSelectedRows(0, -1, 15, 192, writer.getDirectContent());	//Relocate to the bottom of the page
			//renderPageFooter(document);
		}catch(Exception ee)
		{
			logger.debug(ee.getMessage() + ": could not generate page header/footer");
		}
	}
	
	/**
	 * Generate page header 
	 * Simply render the logo and the client address.
	 * @param String estateLogo
	 * @param pdfType
	 * @return
	 * @throws DocumentException
	 */
	public PdfPTable renderPageHeader(String estateLogo) throws DocumentException 
	{
		String homePath = InitServlet.getConfig().getServletContext().getInitParameter("URLHome");
		PdfPTable hdrTable = new PdfPTable(1);		
		PDFUtil.initMultiLandListbyEstateDefaultCell(hdrTable);
		hdrTable.setWidths(new float[] { 100f});
		hdrTable.getDefaultCell().setBorderColor(BaseColor.GREEN);
		hdrTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		hdrTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

		//Generate Estate logo
		PdfPCell headerCell = new PdfPCell();
		try{
			Image imgHero = Image.getInstance(InitServlet.getRealPath(estateLogo));				
			imgHero.scaleToFit(750, 75);
			headerCell = new PdfPCell(imgHero);
		} catch(Exception ee){
			headerCell = new PdfPCell(BLANK);
			logger.debug("Exception Occurred. Message is: " + ee.getMessage());				
		}
		headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerCell.setBorder(0);
		headerCell.setPaddingTop(20f);
		
		Calendar currentDate = Calendar.getInstance(); //Get today date 
		SimpleDateFormat formatter =  new SimpleDateFormat("dd-MM-yyyy");
		String dateNow = formatter.format(currentDate.getTime()); //Convert to string
		
		hdrTable.addCell(headerCell); //Add logo to header table
		
		hdrTable.getDefaultCell().setFixedHeight(15f);
		hdrTable.addCell(BLANK);
		//headerCell = new PdfPCell();
		hdrTable.addCell(new Paragraph("Price as at "+dateNow,  PDFUtil.gothamBookPt16)); //followed by created date
		hdrTable.getDefaultCell().setFixedHeight(10f);
		hdrTable.addCell(BLANK);
		return hdrTable;
	}

	
	public PdfPTable renderPageFooter() throws DocumentException 
	{
		PdfPTable hdrTable = new PdfPTable(1);		
		
		String disclaimer = MultiLandListByEstateHeader.disclaimerText;
		if(StringUtils.isBlank(disclaimer))
			disclaimer = "";
		
		hdrTable.getDefaultCell().setFixedHeight(25f);
		hdrTable.getDefaultCell().setBorder(0);

		hdrTable.addCell(new Paragraph(disclaimer,  PDFUtil.gothamBookPt9RegularGrey)); 
		hdrTable.getDefaultCell().setFixedHeight(5f);
		hdrTable.addCell(BLANK);
		
		PDFUtil.initMultiLandListbyEstateDefaultCell(hdrTable);
		hdrTable.setWidths(new float[] { 100f});
		hdrTable.getDefaultCell().setBorderColor(BaseColor.GREEN);
		hdrTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		hdrTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

		PdfPCell headerCell = new PdfPCell();
		try{
			Image imgHero = Image.getInstance(InitServlet.getRealPath(MultiLandListByEstateHeader.dripImage));				
			//imgHero.scaleAbsolute(964, 161);
			headerCell = new PdfPCell(imgHero);
		} catch(Exception ee){
			headerCell = new PdfPCell(new Paragraph(BLANK));
			logger.debug("Exception Occurred. Message is: " + ee.getMessage());				
		}
		headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerCell.setBorder(0);
		hdrTable.getDefaultCell().setPaddingBottom(20f);
		hdrTable.addCell(headerCell); //Add logo to header table
 		return hdrTable;
	}
	
}
