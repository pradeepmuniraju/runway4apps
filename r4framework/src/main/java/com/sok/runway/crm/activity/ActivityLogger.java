package com.sok.runway.crm.activity;

import java.sql.Connection;

import com.sok.runway.offline.ThreadManager;
import com.sok.runway.security.*;
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.sok.runway.crm.Product;

/**
 * Update to remove single-threaded operation.
 * @author mike
 *
 */
public class ActivityLogger {
   private static final Logger logger = LoggerFactory.getLogger(ActivityLogger.class);
   private static ActivityLogger activityLogger = new ActivityLogger();
   public ActivityLogger() { }
   public static ActivityLogger getActivityLogger() {
      //if (activityLogger == null) {
      //  loadLogger();
      //}
      return activityLogger;
   }
   
   public enum ActionType {
	   Inserted, Updated, Deleted, Unknown;
	   private static String[] actions = { "insert", "update", "delete", "--"};
	   public static ActionType getFromString(String s) {
		   for(int i=0; i< values().length; i++) {
			   if(actions[i].equals(s) || values()[i].name().equals(s)) {
				   return values()[i];
			   }
		   }
		   logger.warn("Unknown action encountered =[{}]", s);
		   return Unknown;
	   }
   }
   
   /**
    * Not sure why you'd ever want to do this.
    * @deprecated
    */
   public static void setActivityLogger(ActivityLogger activityLogger) {
      ActivityLogger.activityLogger = activityLogger;
   }
   
   
   public void logActivity(final Logable loggableEntity, final String actionType) {
	   logActivity(loggableEntity, ActionType.getFromString(actionType));
   } 
   public void logActivity(final Logable loggableEntity, final ActionType actionType) {
      ThreadManager.startThread(new Runnable() {
    	  public void run() {
    		  String recordType = "";
    		  if(loggableEntity instanceof Product) {
    			  recordType = ((Product) loggableEntity).getField("ProductType");
    			  if(StringUtils.isNotBlank(recordType))
    				  recordType = " - " + recordType;
    		  }
    		  
    		  final User user = loggableEntity.getCurrentUser();
    	      final GenRow activityLog = new GenRow();
    	      activityLog.setViewSpec("ActivityView");
    	      activityLog.setParameter("Location", user.getIPAddress());
    	      activityLog.setParameter("UserID", user.getUserID());
    	      //moved to solve problems with using closed request connections 
    	      //could have used a check for isClosed, this less expensive.
    	      //activityLog.setConnection(loggableEntity.getConnection());
    	      ActionBean.connect(activityLog);
    	      activityLog.setCloseConnection(true);
    	      activityLog.setParameter("RecordType", loggableEntity.getEntityName() + recordType);    	      
    	      activityLog.setParameter("ActionType", actionType.name());
    	      activityLog.setParameter("CreatedDate", "NOW");
    	      activityLog.setParameter("RecordID", loggableEntity.getPrimaryKey());
    	      activityLog.createNewID();
    	      activityLog.doAction(GenerationKeys.INSERT);
    	      activityLog.setConnection((java.sql.Connection)null);
    	  }
      },"logActivity-");      
   }
   
   public void logActivity(User currentuser, Connection conn, String entityName, String primaryKey, String actionType) {
	   logActivity(new LoggableEntity(currentuser, conn, entityName, primaryKey), actionType);
   }

   public static class LoggableEntity implements Logable {
		private final User user;
		private final Connection conn; 
		private final String entityName;
		private final String primaryKey;
		public LoggableEntity(User currentuser, Connection conn, String entityName, String primaryKey) {
			this.user = currentuser;
			this.conn = conn;
			this.entityName = entityName;
			this.primaryKey = primaryKey;
		}
		   
		@Override
		public User getCurrentUser() {
			return user;
		}
	
		@Override
		public Connection getConnection() {
			return conn;
		}
	
		@Override
		public String getEntityName() {
			return entityName;
		}
	
		@Override
		public String getPrimaryKey() {
			return primaryKey;
		}
   }
}