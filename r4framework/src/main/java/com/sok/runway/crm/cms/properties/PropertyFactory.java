package com.sok.runway.crm.cms.properties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
//import java.util.concurrent.Callable;
import java.util.TreeSet;
import java.sql.Connection;
import javax.servlet.http.HttpServletRequest; 

//import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.interfaces.AsyncProcessStatus;
//import com.sok.runway.offline.ThreadManager;
//import com.sok.runway.util.FutureList;
import com.sok.service.crm.UserService;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

import org.apache.commons.lang.StringUtils;

public class PropertyFactory {
	private static final Logger logger = LoggerFactory.getLogger(PropertyFactory.class);
	public static final String PRODUCTID = "ProductID";
	public static PropertyEntity getPropertyEntity(HttpServletRequest request) {
		return getPropertyEntity(request, request.getParameter("ProductID"));
	}
	
	/**
	 * @deprecated
	 */
	public static List<PropertyEntity> getPropertyEntities(HttpServletRequest request, String productID/* unused at present.. maybe to change */) {
		return getPropertyEntities(request);
	}
	
	public static List<PropertyEntity> getPropertyEntities(HttpServletRequest request) {
		return getPropertyEntities(request, (AsyncProcessStatus)null);
	}

	public static List<PropertyEntity> getPropertyEntities(HttpServletRequest request, AsyncProcessStatus statusable) {

		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (java.util.List<PropertyEntity>)request.getAttribute("entityList");
		if(properties == null) {
			String[] productIDs = request.getParameterValues(PRODUCTID);
			if(productIDs == null) {
				throw new RuntimeException("no properties found");
			}
			if(productIDs.length == 1 && productIDs[0].indexOf('+')>-1) {
				productIDs = productIDs[0].split("\\+");
			} else if(productIDs.length == 1 && productIDs[0].indexOf(':')>-1) {
				productIDs = productIDs[0].split("\\:");
			} else if (productIDs.length==0 || (productIDs.length == 1 && StringUtils.isBlank(productIDs[0]))) {
				throw new RuntimeException("Product ID was not specified");
			}
			properties = getPropertyEntities(request, productIDs, statusable);
		}
	    return properties;
	}
	
	public static List<PropertyEntity> getBasicEntities(HttpServletRequest request, AsyncProcessStatus statusable) {

		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (java.util.List<PropertyEntity>)request.getAttribute("entityList");
		if(properties == null) {
			String[] productIDs = request.getParameterValues(PRODUCTID);
			if(productIDs == null) {
				throw new RuntimeException("no properties found");
			}
			if(productIDs.length == 1 && productIDs[0].indexOf('+')>-1) {
				productIDs = productIDs[0].split("\\+");
			} else if(productIDs.length == 1 && productIDs[0].indexOf(':')>-1) {
				productIDs = productIDs[0].split("\\:");
			} else if (productIDs.length==0 || (productIDs.length == 1 && StringUtils.isBlank(productIDs[0]))) {
				throw new RuntimeException("Product ID was not specified");
			}
			properties = getBasicEntities(request, productIDs, statusable);
		}
	    return properties;
	}
	
	public static List<PropertyEntity> getBasicEntitiesForEstate(HttpServletRequest request, AsyncProcessStatus statusable) {

		@SuppressWarnings("unchecked")
		List<PropertyEntity> properties = (java.util.List<PropertyEntity>)request.getAttribute("entityList");
		if(properties == null) {
			String estateID = request.getParameter("EstateID");
			if(StringUtils.isBlank(estateID)) {
				throw new RuntimeException("no estate found");
			}
			ArrayList<String> list = new ArrayList<>();
			GenRow row = new GenRow();
			row.setViewSpec("properties/ProductQuickIndexView");
			row.setRequest(request);
			if ("-1".equals(estateID)) {
				row.sortBy("EstateName", 0);
				row.sortBy("ABS(ProductQuickIndex.StageName)",1);
				row.sortBy("ProductQuickIndex.StageName",2);
				row.sortBy("ABS(ProductQuickIndex.LotName)",3);
				row.sortBy("ProductQuickIndex.LotName",4);
				row.sortBy("Name", 5);
			} else {
				row.setParameter("EstateID", estateID);
				row.sortBy("ABS(ProductQuickIndex.StageName)",0);
				row.sortBy("ProductQuickIndex.StageName",1);
				row.sortBy("ABS(ProductQuickIndex.LotName)",2);
				row.sortBy("ProductQuickIndex.LotName",3);
				row.sortBy("Name", 4);
			}
			row.setParameter("ProductType", request.getParameter("ProductType"));
			row.setParameter("Active", "Y");
			row.setParameter("QuickIndexCurrentStatus.ProductStatusList.StatusID", InitServlet.getSystemParam("ProductAvailableStatus"));
			if (StringUtils.isBlank(row.getString("ProductType"))) row.setParameter("ProductType", request.getParameter("-product"));
 
			
			row.doAction("search");
			
			row.getResults();
			
			while (row.getNext()) {
				list.add(row.getString("ProductID"));
			}
			
			row.close();			
			
			properties = getBasicEntities(request, (String[]) list.toArray(new String[list.size()]), statusable);
		}
	    return properties;
	}
	
	public static List<PropertyEntity> getPropertyEntities(HttpServletRequest request, String[] productIDs) {
		return getPropertyEntities(request, productIDs,(AsyncProcessStatus) null);
	}
	
	public static List<PropertyEntity> getPropertyEntities(HttpServletRequest request, String[] productIDs, AsyncProcessStatus statusable) {
		if(productIDs == null || productIDs.length==0) return Collections.emptyList();
		if (productIDs.length == 1 && (productIDs[0].indexOf("+") >= 0 || productIDs[0].indexOf("#") >= 0)) {
			if (productIDs[0].indexOf("+") >= 0) productIDs = productIDs[0].split("\\+");
			else if (productIDs[0].indexOf("#") >= 0) productIDs = productIDs[0].split("#");
		}
		Set<String> IDs = new HashSet<String>(productIDs.length);
		for(String s: productIDs) {
			IDs.add(s);
		}
		List<PropertyEntity> properties = new java.util.ArrayList<PropertyEntity>(IDs.size());
		
		double num = new Double(productIDs.length), record = 1;
		for(String pId: IDs) { 
			if(statusable != null) {
				if(record % 5 == 0) {
					statusable.setStatus(request, "Loading properties" , record / num);
				}
				record++;
			}
			properties.add(PropertyFactory.getPropertyEntity(request,pId));
		}
		/*
		final UserBean user = UserService.getInstance().getCurrentUser(request);
		FutureList<PropertyEntity> properties = new FutureList<PropertyEntity>(productIDs.length);
		final Connection con = ActionBean.getConnection(request);
		double num = new Double(productIDs.length), record = 1;
		for(final String pId: productIDs) { 
			if(statusable != null) {
				if(record % 5 == 0) {
					statusable.setStatus(request, "Loading properties" , record / num);
				}
				record++;
			}
			properties.add(ThreadManager.startCallable(new Callable<PropertyEntity>() {
				public PropertyEntity call() {
					PropertyEntity pe =  PropertyFactory.getPropertyEntity(con, pId);
					if(pe.isPropertyType(PropertyType.HouseLandPackage)) {
						HouseLandPackage hlp = (HouseLandPackage)pe; 
						hlp.getLand();
						hlp.getRangeName();
					}
					return pe;
				}
			}));
		}
		*/
		return properties; 
	}
	
	public static List<PropertyEntity> getBasicEntities(HttpServletRequest request, String[] productIDs, AsyncProcessStatus statusable) {
		if(productIDs == null || productIDs.length==0) return Collections.emptyList();
		if (productIDs.length == 1 && (productIDs[0].indexOf("+") >= 0 || productIDs[0].indexOf("#") >= 0)) {
			if (productIDs[0].indexOf("+") >= 0) productIDs = productIDs[0].split("\\+");
			else if (productIDs[0].indexOf("#") >= 0) productIDs = productIDs[0].split("#");
		}
		//Set<String> IDs = new TreeSet<String>();
		//for(String s: productIDs) {
		//	IDs.add(s);
		//}
		List<PropertyEntity> properties = new java.util.ArrayList<PropertyEntity>();
		
		double num = new Double(productIDs.length), record = 1;
		for(int p = 0; p < productIDs.length; ++p) { 
			if(statusable != null) {
				if(record % 5 == 0) {
					statusable.setStatus(request, "Loading properties" , record / num);
				}
				record++;
			}
			properties.add(new Basic(request,productIDs[p]));
		}
		return properties; 
	}
	
	public static PropertyEntity getPropertyEntity(HttpServletRequest request, String productID) {
		GenRow check = new GenRow(); 
		check.setTableSpec("Products");
		check.setRequest(request);
		check.setParameter("-select1",PRODUCTID);
		check.setParameter("-select2","ProductType");
		check.setParameter("ProductID",productID);
		if(!check.isSet(PRODUCTID)) {
			throw new RuntimeException("Product ID was not specified");
		}
		check.doAction(GenerationKeys.SELECT);
		if(!check.isSuccessful()) { 
			throw new RuntimeException("Product record was not found");
		}
		
		PropertyType propertyType = PropertyType.getPropertyType(check.getData("ProductType"));
		switch(propertyType) {
			case Apartment: 
				return new Apartment(request, check.getData(PRODUCTID));
			case Brand: 
				return new Brand(request, check.getData(PRODUCTID));
			case Building: 
				return new Building(request, check.getData(PRODUCTID));
			case BuildingStage: 
				return new BuildingStage(request, check.getData(PRODUCTID));
			case Home: 
				return new Home(request, check.getData(PRODUCTID));
			case HomeOption: 
				return new HomeOption(request, check.getData(PRODUCTID));
			case HomePlan: 
				return new HomePlan(request, check.getData(PRODUCTID));
			case HomePlanFeature: 
				return new HomePlanFeature(request, check.getData(PRODUCTID));
			case HomeRange: 
				return new HomeRange(request, check.getData(PRODUCTID));
			case HouseLandPackage: 
				return new HouseLandPackage(request, check.getData(PRODUCTID));
			case Estate: 
				return new Estate(request, check.getData(PRODUCTID));
			case Land: 
				return new Land(request, check.getData(PRODUCTID));
			case Stage: 
				return new Stage(request, check.getData(PRODUCTID));
			case Developer:
				return new Developer(request, check.getData(PRODUCTID));
			case ExistingProperty:
				return new ExistingProperty(request, check.getData(PRODUCTID));
		}
		return null;
	}
	
	public static PropertyEntity getPropertyEntity(Connection con, String productID) {
		GenRow check = new GenRow(); 
		check.setTableSpec("Products");
		check.setConnection(con);
		check.setParameter("-select1",PRODUCTID);
		check.setParameter("-select2","ProductType");
		check.setParameter("ProductID",productID);
		if(!check.isSet(PRODUCTID)) {
			throw new RuntimeException("Product ID was not specified");
		}
		check.doAction(GenerationKeys.SELECT);
		if(!check.isSuccessful()) { 
			throw new RuntimeException("Product record was not found");
		}
		
		PropertyType propertyType = PropertyType.getPropertyType(check.getData("ProductType"));
		switch(propertyType) {
			case Apartment: 
				return new Apartment(con, check.getData(PRODUCTID));
			case Building: 
				return new Building(con, check.getData(PRODUCTID));
			case BuildingStage: 
				return new BuildingStage(con, check.getData(PRODUCTID));
			case Home: 
				return new Home(con, check.getData(PRODUCTID));
			case HomeOption: 
				return new HomeOption(con, check.getData(PRODUCTID));
			case HomePlan: 
				return new HomePlan(con, check.getData(PRODUCTID));
			case HomePlanFeature: 
				return new HomePlanFeature(con, check.getData(PRODUCTID));
			case HomeRange: 
				return new HomeRange(con, check.getData(PRODUCTID));
			case HouseLandPackage: 
				return new HouseLandPackage(con, check.getData(PRODUCTID));
			case Estate: 
				return new Estate(con, check.getData(PRODUCTID));
			case Land: 
				return new Land(con, check.getData(PRODUCTID));
			case Stage: 
				return new Stage(con, check.getData(PRODUCTID));
			case ExistingProperty:
				return new ExistingProperty(con, check.getData(PRODUCTID));
		}
		return null;
	}
	
	/*
	 * 
	 * NB, this was taken from the top of one of the jsp pages. 
	 * I moved it here as it was being included a few times, but i have disabled those functions not in use.
	 * I left them there in case we need them to be re-enabled.
	 * 
	 * -Mike
	 */
	
	/*
	public static String getDisplayValue(String value, String type, String format) {
		if (value == null || value.length() == 0)
			return "";
		if (format == null || format.length() == 0)
			format = "dd/MM/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(sdf.parse(value));

			if ("Month".equalsIgnoreCase(type)) {
				String ret = "";
				SimpleDateFormat mdf = new SimpleDateFormat("EEEE, d 'of' MMMM");
				cal.set(Calendar.DAY_OF_MONTH, 1);
				ret += mdf.format(cal.getTime());
				cal.add(Calendar.MONTH, 1);
				cal.add(Calendar.HOUR, -24);
				ret += " to ";
				ret += mdf.format(cal.getTime());

				return ret;
			} else if ("Week".equalsIgnoreCase(type)) {
				String ret = "";
				SimpleDateFormat mdf = new SimpleDateFormat("d 'of' MMMM");
				int week = cal.get(Calendar.WEEK_OF_YEAR);
				ret += "Week " + (week - 1) + " - ";
				cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
				cal.set(Calendar.WEEK_OF_YEAR, week);
				ret += mdf.format(cal.getTime());
				cal.add(Calendar.DAY_OF_YEAR, 6);
				ret += " to ";
				ret += mdf.format(cal.getTime());

				return ret;
			}

			return value;

		} catch (Exception e) {
			if ("dd/MM/yyyy".equals(format)) {
				return getDisplayValue(value, type, "MMM d, yyyy h:mm:ss a");
			}
		}

		return "";
	}
	*/
	public static final String YEAR = "year =";
	public static final String MONTH = "month =";
	/*
	public String getDecodedParameter(String fieldValue) {

		if (fieldValue == null || fieldValue.equals("empty")) {
			fieldValue = "";
		}

		return fieldValue.trim();
	}
	*/
	public static boolean hasValue(String fieldValue) {
		return (fieldValue != null && fieldValue.length() != 0 && !fieldValue
				.equals("empty"));
	}
	/*
	public List<String> converToList(String str) {
		if (str == null)
			return null;
		StringTokenizer tokens = new StringTokenizer(str, "\\+");
		List<String> searchedproductids = new ArrayList<String>();
		while (tokens.hasMoreElements()) {
			searchedproductids.add(tokens.nextToken());
		}
		
		return searchedproductids;
	}
	*/
	public static GenRow getSearchGenRow2(HttpServletRequest request, String productIDs, boolean repView) {
		GenRow quickIndex = new GenRow();
		quickIndex.parseRequest(request);
		quickIndex.setViewSpec("properties/ProductQuickIndexView");
		
		Enumeration<String> param = request.getParameterNames();
		while (param.hasMoreElements()) {
			String s = (String) param.nextElement();
			System.out.println(s + " = " + request.getParameter(s));
		}

		boolean isClaimSearch = StringUtils.isNotBlank(request.getParameter("claimRep"));

		if (isClaimSearch) {

			quickIndex.setParameter("QuickIndexProductSecurityGroups.RepUserID", request.getParameter("claimRep"));

			quickIndex.setParameter("PackageStatus", "Approved");

		} else {
			boolean bulk = (request.getParameter("-bulk") != null && "true".equals(request.getParameter("-bulk")));

			String cost = "";
			if (hasValue(quickIndex.getString("MinimumCost")) && !"$0k".equals(quickIndex.getString("MinimumCost"))) {
				cost = ">=" + quickIndex.getString("MinimumCost").replaceAll("[^0-9]", "") + "000";
			} else {
				quickIndex.remove("MinimumCost");
			}

			if (hasValue(quickIndex.getString("MaximumCost")) && !"$1k".equals(quickIndex.getString("MaximumCost"))) {
				if (cost.length() > 0)
					cost += ";";
				cost += "<=" + quickIndex.getString("MaximumCost").replaceAll("[^0-9]", "") + "000";
			} else {
				quickIndex.remove("MaximumCost");
			}

			if (hasValue(quickIndex.getString("Storey"))) {
				quickIndex.setColumn("Storeys", quickIndex.getString("Storey"));
			}

			if (hasValue(quickIndex.getString("PlanID"))) {
				quickIndex.setColumn("CopiedFromID", quickIndex.getString("PlanID"));
				quickIndex.remove("PlanID");
			}

			if (cost.length() > 0) {
				quickIndex.addParameter("DripResult", cost + "+NULL");
			}
			String width = "";
			if (hasValue(quickIndex.getString("Minimum-minLotWidth"))) {
				width = ">=" + quickIndex.getString("Minimum-minLotWidth");
			}
			if (hasValue(quickIndex.getString("Maximum-minLotWidth"))) {
				if (width.length() > 0)
					width += ";";
				width += "<=" + quickIndex.getString("Maximum-minLotWidth");
			}
			if (width.length() > 0) {
				quickIndex.setParameter("LotWidth", width + "+NULL");
			}

			String packageIds = quickIndex.getString("selectedPackageIds");
			if (hasValue(packageIds)) {
				packageIds = packageIds.replaceAll(" ", "+");
				quickIndex.setParameter("ProductID", packageIds);
			}

			String homeWidth = "";
			String minHomeWidth = quickIndex.getString("MinimumHomeWidth");
			if (hasValue(minHomeWidth) && !("0".equals(minHomeWidth))) {
				homeWidth = ">=" + minHomeWidth;
			}
			String maxHomeWidth = quickIndex.getString("MaximumHomeWidth");
			if (hasValue(maxHomeWidth)) {
				if (homeWidth.length() > 0) {
					homeWidth += ";";
				}
				homeWidth += "<=" + maxHomeWidth;
			}
			if (homeWidth.length() > 0 && !">=0".equals(homeWidth)) {
				// quickIndex.setParameter("LotWidth", homeWidth + "+NULL");
				quickIndex.setParameter("HomeWidth", homeWidth + "+NULL");
			}

			String homeSize = "";
			String minHomeSize = quickIndex.getString("MinimumHomeSize");
			if (hasValue(minHomeSize) && !("0".equals(minHomeSize))) {
				homeSize = ">=" + minHomeSize;
			}
			String maxHomeSize = quickIndex.getString("MaximumHomeSize");
			if (hasValue(maxHomeSize)) {
				if (homeSize.length() > 0) {
					homeSize += ";";
				}
				homeSize += "<=" + maxHomeSize;
			}
			if (homeSize.length() > 0 && !">=0".equals(homeSize)) {
				quickIndex.setParameter("HomeSize", homeSize + "+NULL");
			}
			quickIndex.setParameter("-groupby0", "ProductID");

			if (!bulk && repView) {
				// must be active for reps
				quickIndex.setParameter("Active", "Y");
				// products.setParameter("DisplayFlag", "N+NULL+EMPTY");
				// reps can only see Available
				Properties sysprops = InitServlet.getSystemParams();
				String statusId = (sysprops.getProperty("ProductAvailableStatus") != null && sysprops.getProperty("ProductAvailableStatus").trim().length() > 0) ? sysprops.getProperty("ProductAvailableStatus") : request.getParameter("Status");
				if (!StringUtil.isBlankOrEmpty(statusId)) {
					quickIndex.setParameter("CurrentStatusID", statusId);
					quickIndex.setParameter("QuickIndexCurrentStatus.ProductStatusList.StatusID", statusId);
				}
				// reps can not see pre release packages or expired
				quickIndex.setParameter("PreAvailableDate", "<NOW+NULL");
				quickIndex.setParameter("ExpiryDate", ">NOW+NULL");
			} else {
				// only apply this if it is not a rep screen
				if (productIDs != null && productIDs.length() > 0) {
					quickIndex.setColumn("ProductID", productIDs);
				}
				String statusId = request.getParameter("Status");
				if (!StringUtil.isBlankOrEmpty(statusId)) {
					quickIndex.setParameter("CurrentStatusID", statusId);
					quickIndex.setParameter("QuickIndexCurrentStatus.ProductStatusList.StatusID", statusId);
				}
			}
			if (hasValue(quickIndex.getString("RepUserID"))) {
				quickIndex.setParameter("QuickIndexProductSecurityGroups.RepUserID", quickIndex.getString("RepUserID"));
			}

			if (hasValue(request.getParameter("-sorting"))) {
				String sortBy = request.getParameter("-sorting");
				String orderBy = request.getParameter("-ordering");

				if ("Available".equalsIgnoreCase(sortBy)) {
					quickIndex.sortBy("AvailableDate", 0);
					quickIndex.sortOrder(orderBy, 0);
				} else if ("Home/Plan".equalsIgnoreCase(sortBy)) {
					quickIndex.sortBy("HomePlanName", 0);
					quickIndex.sortOrder(orderBy, 0);
				} else if ("Estate".equalsIgnoreCase(sortBy)) {
					quickIndex.sortBy("EstateName", 0);
					quickIndex.sortOrder(orderBy, 0);
				} else if ("Stage".equalsIgnoreCase(sortBy)) {
					quickIndex.sortBy("EstateName", 0);
					quickIndex.sortOrder(orderBy, 0);
					quickIndex.sortBy("StageName", 1);
					quickIndex.sortOrder(orderBy, 1);
				} else if ("Lot".equalsIgnoreCase(sortBy)) {
					quickIndex.sortBy("EstateName", 0);
					quickIndex.sortOrder(orderBy, 0);
					quickIndex.sortBy("StageName", 1);
					quickIndex.sortOrder(orderBy, 1);
					quickIndex.sortBy("LotName", 2);
					quickIndex.sortOrder(orderBy, 2);
				} else if ("Depth".equalsIgnoreCase(sortBy)) {
					quickIndex.sortBy("LotDepth", 0);
					quickIndex.sortOrder(orderBy, 0);
				} else if ("Width".equalsIgnoreCase(sortBy)) {
					quickIndex.sortBy("LotWidth", 0);
					quickIndex.sortOrder(orderBy, 0);
				} else if ("Area".equalsIgnoreCase(sortBy)) {
					quickIndex.sortBy("LotArea", 0);
					quickIndex.sortOrder(orderBy, 0);
				} else if ("Price".equalsIgnoreCase(sortBy)) {
					quickIndex.sortBy("TotalCost", 0);
					quickIndex.sortOrder(orderBy, 0);
				} else if ("REA Start Date".equalsIgnoreCase(sortBy)) {
					quickIndex.setParameter("-join0", "ProductQuickIndexProductPublishing.ProductPublishingProductPublishingHistoryREA");
					quickIndex.setParameter("ProductQuickIndexProductPublishing.PublishMethod", "REA");
					quickIndex.setParameter("-groupby0", "ProductPublishingProductPublishingHistoryREA.ProductPublishingID");
					quickIndex.setParameter("-select0", "DATEDIFF(NOW(), Min(ProductPublishingProductPublishingHistoryREA.CreatedDate) ) as FirstPublishDate");

					quickIndex.sortBy("FirstPublishDate", 0);
					quickIndex.sortOrder(orderBy, 0);

				} else if ("Domain Start Date".equalsIgnoreCase(sortBy)) {
					quickIndex.setParameter("-join0", "ProductQuickIndexProductPublishing.ProductPublishingProductPublishingHistoryDomain");
					quickIndex.setParameter("ProductQuickIndexProductPublishing.PublishMethod", "Domain");
					quickIndex.setParameter("-groupby0", "ProductPublishingProductPublishingHistoryDomain.ProductPublishingID");
					quickIndex.setParameter("-select0", "DATEDIFF(NOW(), Min(ProductPublishingProductPublishingHistoryDomain.CreatedDate) ) as FirstPublishDate");

					quickIndex.sortBy("FirstPublishDate", 0);
					quickIndex.sortOrder(orderBy, 0);

				} else if ("MyPackage Start Date".equalsIgnoreCase(sortBy)) {
					quickIndex.setParameter("-join0", "ProductQuickIndexProductPublishing.ProductPublishingProductPublishingHistoryMyPackage");
					quickIndex.setParameter("ProductQuickIndexProductPublishing.PublishMethod", "MyPackage");
					quickIndex.setParameter("-groupby0", "ProductPublishingProductPublishingHistoryMyPackage.ProductPublishingID");
					quickIndex.setParameter("-select0", "DATEDIFF(NOW(), Min(ProductPublishingProductPublishingHistoryMyPackage.CreatedDate) ) as FirstPublishDate");

					quickIndex.sortBy("FirstPublishDate", 0);
					quickIndex.sortOrder(orderBy, 0);

				} else if ("Website Start Date".equalsIgnoreCase(sortBy)) {
					quickIndex.setParameter("-join0", "ProductQuickIndexProductPublishing.ProductPublishingProductPublishingHistoryWebsite");
					quickIndex.setParameter("ProductQuickIndexProductPublishing.PublishMethod", "Website");
					quickIndex.setParameter("-groupby0", "ProductPublishingProductPublishingHistoryWebsite.ProductPublishingID");
					quickIndex.setParameter("-select0", "DATEDIFF(NOW(), Min(ProductPublishingProductPublishingHistoryWebsite.CreatedDate) ) as FirstPublishDate");

					quickIndex.sortBy("FirstPublishDate", 0);
					quickIndex.sortOrder(orderBy, 0);

				} else if ("Print Start Date".equalsIgnoreCase(sortBy)) {
					quickIndex.setParameter("-join0", "ProductQuickIndexProductPublishing.ProductPublishingProductPublishingHistoryPrint");
					quickIndex.setParameter("ProductQuickIndexProductPublishing.PublishMethod", "Print");
					quickIndex.setParameter("-groupby0", "ProductPublishingProductPublishingHistoryPrint.ProductPublishingID");
					quickIndex.setParameter("-select0", "DATEDIFF(NOW(), Min(ProductPublishingProductPublishingHistoryPrint.CreatedDate) ) as FirstPublishDate");

					quickIndex.sortBy("FirstPublishDate", 0);
					quickIndex.sortOrder(orderBy, 0);

				} else {
					quickIndex.sortBy("Name", 0);
					quickIndex.sortOrder(orderBy, 0);
				}
				// products.sortBy(products.getString("-sorting"), 0);
			} else {
				quickIndex.sortBy("Name", 0);
			}

			// String createDate = request.getParameter("createDate");
			// if (!StringUtil.isBlankOrEmpty(createDate)) {
			// quickIndex.setParameter("CreatedDate", createDate);
			// }

			
		}
		quickIndex.setAction(GenerationKeys.SEARCH);
		
		return quickIndex;
	}
	
	
	
	public static GenRow getSearchGenRowForLandProperties(HttpServletRequest request, String productIDs, boolean repView) 
	{
			    // We count the number of non-empty optional parmaters because if 
			    // all of these parameters are empty, we do not want to go off 
			    // and do an expensive search for all companies.
			    final GenRow products =  new GenRow();
			    // Search for contact matches    
			    products.parseRequest(request);
			    products.setViewSpec("properties/ProductLotView");
			    boolean hasvalue = false;

				    if(hasValue(products.getString("Name"))) {
				       if("true".equals(request.getParameter("-exact"))){;
				          products.setParameter("Name", getDecodedParameter(products.getString("Name")));
				       }
				       else{
				          products.setParameter("Name", "%"+products.getString("Name")+"%");
				       }
				       hasvalue = true;
				    }
			      
			       
			       if(hasValue(products.getString("ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ProductID"))) {
			          hasvalue = true;
			       }
				    
			       if(hasValue(products.getString("ProductParentStageLot.ParentProductID"))) {
			          hasvalue = true;
			       }       
			       
			       if(hasValue(products.getString("GroupID"))) {
			          hasvalue = true;
			          products.setParameter("ProductProductSecurityGroups.GroupID|1", products.getString("GroupID"));
			          products.setParameter("GroupID|1", products.getString("GroupID"));
			          products.remove("GroupID");
			       }
			       
			       if(hasValue(products.getString("IsLotTitled"))) {
			           hasvalue = true;
			           products.setParameter("ProductHomeDetails.IsLotTitled", products.getString("IsLotTitled"));
			           if(hasValue(products.getString("LotTitleMonth"))) {
			               products.setParameter("ProductHomeDetails.LotTitleMonth", products.getString("LotTitleMonth"));
			           }
			           if(hasValue(products.getString("LotTitleYear"))) {
			               products.setParameter("ProductHomeDetails.LotTitleYear", products.getString("LotTitleYear"));
			           }
			        }

			       String cost = "";
			       if(hasValue(products.getString("MinimumCost")) && !"$0k".equals(products.getString("MinimumCost"))) {
			    	   cost = ">=" + products.getString("MinimumCost").replaceAll("[^0-9]","") + "000";
			        }       
			       
			       if(hasValue(products.getString("MaximumCost")) && !"$1k".equals(products.getString("MaximumCost"))) {
			    	   if (cost.length() > 0) cost += ";";
			    	   cost += "<=" + products.getString("MaximumCost").replaceAll("[^0-9]","") + "000";
			        }
			       if (cost.length() > 0) {
			    	   products.addParameter("TotalCost", cost + "+NULL");
			           hasvalue = true;
			       }
			       
					String area = "";
					if (hasValue(products.getString("Minimum-minLotArea"))) {
						area = ">=" + products.getString("Minimum-minLotArea");
					}
					if (hasValue(products.getString("Maximum-minLotArea"))) {
						if (area.length() > 0)
							area += ";";
						area += "<=" + products.getString("Maximum-minLotArea");
					}
					if (area.length() > 0) {
						products.setParameter("Area", area + "+NULL");
						products.setParameter("ProductHomeDetails.Area", area + "+NULL");
					}
					String width = "";
					if (hasValue(products.getString("Minimum-minLotWidth"))) {
						width = ">=" + products.getString("Minimum-minLotWidth");
					}
					if (hasValue(products.getString("Maximum-minLotWidth"))) {
						if (width.length() > 0)
							width += ";";
						width += "<=" + products.getString("Maximum-minLotWidth");
					}
					if (width.length() > 0) {
						products.setParameter("LotWidth", width + "+NULL");
						products.setParameter("ProductHomeDetails.LotWidth", width + "+NULL");
					}
					String depth = "";
					if (hasValue(products.getString("Minimum-minLotDepth"))) {
						depth = ">=" + products.getString("Minimum-minLotDepth");
					}
					if (hasValue(products.getString("Maximum-minLotDepth"))) {
						if (depth.length() > 0)
							depth += ";";
						depth += "<=" + products.getString("Maximum-minLotDepth");
					}
					if (depth.length() > 0) {
						products.setParameter("LotDepth", depth + "+NULL");
						products.setParameter("ProductHomeDetails.LotDepth", depth + "+NULL");
					}
					
					String packageIds = products.getString("selectedPackageIds");
					if (hasValue(packageIds)) {
						packageIds = packageIds.replaceAll(" ", "+");
						products.setParameter("ProductID", packageIds);
					}
					
					if (hasValue(products.getString("RegionID"))) {
						String locationID = products.getString("RegionID");
						String[] locationIDs = locationID.split(":");
						
						products.setParameter("ProductDisplayEntity.ParentDisplayEntityID", locationIDs[0]);
						
						if(locationIDs.length > 1)
							products.setParameter("LocationID", locationIDs[1]);			
							
						products.remove("RegionID");
					}
					Properties sysprops = InitServlet.getSystemParams();
					boolean bulk = (request.getParameter("-bulk") != null && "true".equals(request.getParameter("-bulk")));
					if (!bulk && repView) {
						// must be active for reps
						products.setParameter("Active", "Y");
						// reps can only see Available
						String statusId = (sysprops.getProperty("ProductAvailableStatus") != null && sysprops.getProperty("ProductAvailableStatus").trim().length() > 0)? sysprops.getProperty("ProductAvailableStatus") : request.getParameter("Status");
						if (!StringUtil.isBlankOrEmpty(statusId)) {
							products.setParameter("CurrentStatusID", statusId);
							products.setParameter(
									"ProductCurrentStatus.ProductStatusList.StatusID", statusId);
						}
						// reps can not see pre release packages
						products.setParameter("PreAvailableDate","<NOW+NULL");
					} else {
						
						// only apply this if it is not a rep screen
						if (productIDs != null && productIDs.length() > 0) {
							products.setColumn("ProductID", productIDs);
						}
						
						// only apply this if it is not a rep screen
						String statusId = request.getParameter("Status");
						if (!StringUtil.isBlankOrEmpty(statusId)) {
							products.setParameter("CurrentStatusID", statusId);
							products.setParameter(
									"ProductCurrentStatus.ProductStatusList.StatusID", statusId);
						}
					}
			       
					/*String sortBy = products.getString("SortBy");
			       String sortOrder = products.getString("SortOrder");
			       //sortBy = doParameterMapping(sortBy);
			       if (!StringUtil.isBlankOrEmpty(sortBy)) {
			    	   products.sortBy(sortBy, 0);
			    	   if (StringUtil.isBlankOrEmpty(sortOrder)) {
			    		   sortOrder = "Desc";
			    	   }
			    	   products.sortOrder(sortOrder, 0);
			       } else {
				       products.setParameter("-groupby0","ProductID");
			    	   products.sortBy("Name",0);
			       }
			       currentuser.setConstraint(products,"ProductProductSecurityGroups.ProductSecurityGroupGroup");
			       */
			       
			       products.setAction(GenerationKeys.SEARCH);
			       products.getResults(true);
			       
			       
			       /*boolean publishing = "true".equals(request.getParameter("-publishing"));
				
				final Logger logger = LoggerFactory.getLogger(this.getClass());
				
				if(logger.isTraceEnabled()) { //trace likely only enabled in development
					out.println("<!--");
					out.println(products.getStatement());
					out.println("-->");
				}
				
				logger.debug(products.getStatement());
				*/
			       
				return products;
	}
	
	public static String getDecodedParameter(String fieldValue)
	{
		  if(fieldValue == null || fieldValue.equals("empty")) {
		    fieldValue = "";
		  }
		  
		  return fieldValue.trim();
	}
	
	
	public static GenRow getSearchGenRowForExistingProperties(HttpServletRequest request, String productIDs, boolean repView) {
		GenRow products = new GenRow();
		products.parseRequest(request);
		products.setViewSpec("properties/ProductExistingPropertyView");
		products.setParameter("-join0", "ProductProductSecurityGroups.ProductSecurityGroupGroup");
		products.setParameter("-groupby0", "ProductID");
		
		boolean bulk = (request.getParameter("-bulk") != null && "true"
				.equals(request.getParameter("-bulk")));

		String cost = "";
		if (hasValue(products.getString("MinimumCost"))
				&& !"$0k".equals(products.getString("MinimumCost"))) {
			cost = ">="
					+ products.getString("MinimumCost")
							.replaceAll("[^0-9]", "") + "000";
		}

		if (hasValue(products.getString("MaximumCost"))
				&& !"$1k".equals(products.getString("MaximumCost"))) {
			if (cost.length() > 0)
				cost += ";";
			cost += "<="
					+ products.getString("MaximumCost")
							.replaceAll("[^0-9]", "") + "000";
		}
		if (cost.length() > 0) {
			products.addParameter("DripResult", cost + "+NULL");
		}
		
		if (hasValue(products.getString("ProductSubType"))) {
			products.setParameter("ProductSubType",
					products.getString("ProductSubType"));			
		}

		if (hasValue(products.getString("RepUserID"))) {
			products.setParameter(
					"ProductProductSecurityGroups.RepUserID",
					products.getString("RepUserID"));
			products.remove("RepUserID");
		}
		String packageIds = products.getString("selectedPackageIds");
		if (hasValue(packageIds)) {
			packageIds = packageIds.replaceAll(" ", "+");
			products.setParameter("ProductID", packageIds);
		}
	
		if (hasValue(products.getString("Bedrooms"))) {
			products.setParameter("ProductProductDetails#search.DetailsType^1^9", "Bedrooms [No]+Bedrooms");
			products.setParameter("ProductProductDetails#search.NumberOf^1^9", products.getString("Bedrooms"));
		}
		
		if (hasValue(products.getString("Bathrooms"))) {			
			products.setParameter("ProductProductDetails#search.DetailsType^2^9", "Bathroom [No]+Bathroom+Bathrooms");
			products.setParameter("ProductProductDetails#search.NumberOf^2^9", products.getString("Bathrooms"));
		}
		
		
		if (hasValue(products.getString("Study"))) {
			products.setParameter("ProductProductDetails#search.DetailsType^3^9", "Study");
			products.setParameter("ProductProductDetails#search.NumberOf^3^9", products.getString("Study"));
		}		
		
		if (hasValue(products.getString("Garage"))) {
			products.setParameter("ProductProductDetails#search.DetailsType^4^9", "Garage");
			products.setParameter("ProductProductDetails#search.NumberOf^4^9", products.getString("Garage"));
		}
		
		if (hasValue(products.getString("Storey"))) {
			products.setParameter("ProductProductDetails#search.DetailsType^5^9", "Storeys");
			products.setParameter("ProductProductDetails#search.NumberOf^5^9", products.getString("Storey"));
		}
		
		
		String size = "";
		if (hasValue(products.getString("MinimumSize"))) {
			size = ">=" + products.getString("MinimumSize");
		}
		
		if (hasValue(products.getString("MaximumSize"))) {
			if (size.length() > 0)
				size += ";";
			size += "<=" + products.getString("MaximumSize");
		}
		if (size.length() > 0) {
			products.setParameter("ProductProductDetails#search.DetailsType^6^9", "Home Area");
			products.setParameter("ProductProductDetails#search.SQmeters^6^9", size);
		}
		
		
		if (!bulk && repView) {
			// must be active for reps
			products.setParameter("Active", "Y");
			// reps can only see Available
			Properties sysprops = InitServlet.getSystemParams();
			String statusId = (sysprops.getProperty("ProductAvailableStatus") != null && sysprops.getProperty("ProductAvailableStatus").trim().length() > 0)? sysprops.getProperty("ProductAvailableStatus") : request.getParameter("Status");
			if (!StringUtil.isBlankOrEmpty(statusId)) {
				products.setParameter("CurrentStatusID", statusId);
				products.setParameter(
						"ProductCurrentStatus.ProductStatusList.StatusID", statusId);
			}
			// reps can not see pre release packages
			products.setParameter("PreAvailableDate","<NOW+NULL");
		} else {
			// only apply this if it is not a rep screen
			if (productIDs != null && productIDs.length() > 0) {
				products.setColumn("ProductID", productIDs);
			}
			String statusId = request.getParameter("Status");
			if (!StringUtil.isBlankOrEmpty(statusId)) {
				products.setParameter("CurrentStatusID", statusId);
				products.setParameter(
						"ProductCurrentStatus.ProductStatusList.StatusID", statusId);
			}
		}
		

		products.setParameter("-groupby0", "ProductID");
		
		if (hasValue(request.getParameter("-sorting"))) {
			String sortBy = request.getParameter("-sorting");
			String orderBy = request.getParameter("-ordering");
			
			if ("Available".equalsIgnoreCase(sortBy)) {
				products.sortBy("AvailableDate", 0);
				products.sortOrder(orderBy, 0);
			} else if ("Home/Plan".equalsIgnoreCase(sortBy)) {
				products.sortBy("HomePlanName", 0);
				products.sortOrder(orderBy, 0);
			} else if ("Estate".equalsIgnoreCase(sortBy)) {
				products.sortBy("EstateName", 0);
				products.sortOrder(orderBy, 0);
			} else if ("Stage".equalsIgnoreCase(sortBy)) {
				products.sortBy("EstateName", 0);
				products.sortOrder(orderBy, 0);
				products.sortBy("StageName", 1);
				products.sortOrder(orderBy, 1);
			} else if ("Lot".equalsIgnoreCase(sortBy)) {
				products.sortBy("EstateName", 0);
				products.sortOrder(orderBy, 0);
				products.sortBy("StageName", 1);
				products.sortOrder(orderBy, 1);
				products.sortBy("LotName", 2);
				products.sortOrder(orderBy, 2);
			} else if ("Depth".equalsIgnoreCase(sortBy)) {
				products.sortBy("LotDepth", 0);
				products.sortOrder(orderBy, 0);
			} else if ("Width".equalsIgnoreCase(sortBy)) {
				products.sortBy("LotWidth", 0);
				products.sortOrder(orderBy, 0);
			} else if ("Area".equalsIgnoreCase(sortBy)) {
				products.sortBy("LotArea", 0);
				products.sortOrder(orderBy, 0);
			} else if ("Price".equalsIgnoreCase(sortBy)) {
				products.sortBy("TotalCost", 0);
				products.sortOrder(orderBy, 0);
			} else {
				products.sortBy("Name", 0);
				products.sortOrder(orderBy, 0);
			}
		} else {
			products.sortBy("Name", 0);
		}
		return products;
	}
	
	public static GenRow getSearchGenRow(HttpServletRequest request, String productIDs, boolean repView) {
		
		if(logger.isTraceEnabled()) { 
			@SuppressWarnings("rawtypes") //request.getParameterNames does not return Enumeration<String>
			Enumeration param = request.getParameterNames();
			while (param.hasMoreElements()) {
				String s = String.valueOf(param.nextElement());
				logger.trace("{}={}",s, request.getParameter(s));
			}	
		}
		GenRow products = new GenRow();
		products.parseRequest(request);
		products.setViewSpec("properties/ProductHouseAndLandView");

		boolean bulk = (request.getParameter("-bulk") != null && "true"
				.equals(request.getParameter("-bulk")));

		StringBuilder cost = new StringBuilder();
		//String cost = "";
		if (hasValue(products.getString("MinimumCost"))
				&& !"$0k".equals(products.getString("MinimumCost"))) {
			cost.append(">=").append(products.getString("MinimumCost").replaceAll("[^0-9]", "")).append("000");
		}

		if (hasValue(products.getString("MaximumCost"))
				&& !"$1k".equals(products.getString("MaximumCost"))) {
			if (cost.length() > 0) cost.append(";");
			cost.append("<=").append(products.getString("MaximumCost").replaceAll("[^0-9]", "")).append("000");
		}
		if (cost.length() > 0) {
			cost.append("+NULL");
			products.addParameter("TotalCost", cost.toString());
			//hasvalue = true;
		}
		cost = null;
		if (hasValue(products.getString("DeveloperID"))) {
			products.setParameter("DeveloperProductID",
					products.getString("DeveloperID"));
			products.setParameter(
					"ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ProductParentDeveloperEstate.ProductVariationDeveloperEstate.ProductID",
					products.getString("DeveloperID"));
		}
		if (hasValue(products.getString("EstateID"))) {
			products.setParameter("EstateProductID",
					products.getString("EstateID"));
			products.setParameter(
					"ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ProductID",
					products.getString("EstateID"));
		}
		if (hasValue(products.getString("StageID"))) {
			products.setParameter("StageProductID",
					products.getString("StageID"));
			products.setParameter(
					"ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductParentStageLot.ProductVariationStageLot.ProductID",
					products.getString("StageID"));
		}

		if (hasValue(products.getString("LandID"))) {
			products.setParameter("LotID", products.getString("LandID"));
			products.setParameter(
					"ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductID",
					products.getString("LandID"));
		}

		if (hasValue(products.getString("PlanID"))) {
			products.setParameter("CopyPlanID", products.getString("PlanID"));
			products.setParameter(
					"ProductProductProducts#House.ProductProductsLinkedProductHouse.CopiedFromProductID",
					products.getString("PlanID"));
			products.remove("PlanID");
		}

		if (hasValue(products.getString("LotProductSubType"))) {
			products.setParameter("LotProductSubType",
					products.getString("LotProductSubType"));
			products.setParameter(
					"ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductSubType",
					products.getString("LotProductSubType"));
		}
		if (hasValue(products.getString("MarketingStatus"))) {
			products.setParameter("LotMarketingStatus",
					products.getString("MarketingStatus"));
			products.setParameter(
					"ProductProductProducts#Land.ProductProductsLinkedProductLand.MarketingStatus",
					products.getString("MarketingStatus"));
			products.remove("MarketingStatus");
		}
		String width = "";
		if (hasValue(products.getString("Minimum-minLotWidth"))) {
			width = ">=" + products.getString("Minimum-minLotWidth");
		}
		if (hasValue(products.getString("Maximum-minLotWidth"))) {
			if (width.length() > 0)
				width += ";";
			width += "<=" + products.getString("Maximum-minLotWidth");
		}
		if (width.length() > 0) {
			products.setParameter(
					"ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductLandDetails.LotWidth",
					width + "+NULL");
		}

		if (hasValue(products.getString("RepUserID"))) {
			products.setParameter(
					"ProductProductSecurityGroups.RepUserID",
					products.getString("RepUserID"));
			products.remove("RepUserID");
		}
		String packageIds = products.getString("selectedPackageIds");
		if (hasValue(packageIds)) {
			packageIds = packageIds.replaceAll(" ", "+");
			products.setParameter("ProductID", packageIds);
		}

		if (hasValue(products.getString("BuilderID"))) {
			products.setParameter(
					"ProductParentHomeDesign.ProductVariationHomeDesign.ProductParentHomeRange.ProductVariationHomeRange.ProductParentBrandRange.ProductVariationBrandRange.ProductID",
					products.getString("BuilderID"));
		}
		String rangeInput = request
				.getParameter("ProductParentHomeDesign.ProductVariationHomeDesign.ProductParentHomeRange.ProductVariationHomeRange.ProductID");
		if (!StringUtil.isBlankOrEmpty(rangeInput)) {
			products.setParameter(
					"ProductParentHomeDesign.ProductVariationHomeDesign.ProductParentHomeRange.ProductVariationHomeRange.ProductID",
					rangeInput);
		}
		String designInput = request
				.getParameter("ProductParentHomeDesign.ProductVariationHomeDesign.ProductID");
		if (!StringUtil.isBlankOrEmpty(designInput)) {
			products.setParameter(
					"ProductParentHomeDesign.ProductVariationHomeDesign.ProductID",
					designInput);
		}
		String homeWidth = "";
		String minHomeWidth = products.getString("MinimumHomeWidth");
		if (hasValue(minHomeWidth) && !("0".equals(minHomeWidth))) {
			homeWidth = ">=" + minHomeWidth;
		}
		String maxHomeWidth = products.getString("MaximumHomeWidth");
		if (hasValue(maxHomeWidth)) {
			if (homeWidth.length() > 0) {
				homeWidth += ";";
			}
			homeWidth += "<=" + maxHomeWidth;
		}
		if (homeWidth.length() > 0 && !">=0".equals(homeWidth)) {
			products.setParameter("LotWidth", homeWidth + "+NULL");
			products.setParameter("ProductHomeDetails.LotWidth", homeWidth
					+ "+NULL");
		}

		String homeSize = "";
		String minHomeSize = products.getString("MinimumHomeSize");
		if (hasValue(minHomeSize) && !("0".equals(minHomeSize))) {
			homeSize = ">=" + minHomeSize;
		}
		String maxHomeSize = products.getString("MaximumHomeSize");
		if (hasValue(maxHomeSize)) {
			if (homeSize.length() > 0) {
				homeSize += ";";
			}
			homeSize += "<=" + maxHomeSize;
		}
		if (homeSize.length() > 0 && !">=0".equals(homeSize)) {
			products.setParameter("HomeSize", homeSize + "+NULL");
			products.setParameter("ProductHomeDetails.HomeSize", homeSize
					+ "+NULL");
		}

		if (!bulk && repView) {
			// must be active for reps
			products.setParameter("Active", "Y");
			// reps can only see Available
			Properties sysprops = InitServlet.getSystemParams();
			String statusId = (sysprops.getProperty("ProductAvailableStatus") != null && sysprops.getProperty("ProductAvailableStatus").trim().length() > 0)? sysprops.getProperty("ProductAvailableStatus") : request.getParameter("Status");
			if (!StringUtil.isBlankOrEmpty(statusId)) {
				products.setParameter("CurrentStatusID", statusId);
				products.setParameter(
						"ProductCurrentStatus.ProductStatusList.StatusID", statusId);
			}
			// reps can not see pre release packages
			products.setParameter("PreAvailableDate","<NOW+NULL");
		} else {
			// only apply this if it is not a rep screen
			if (productIDs != null && productIDs.length() > 0) {
				products.setColumn("ProductID", productIDs);
			}
			String statusId = request.getParameter("Status");
			if (!StringUtil.isBlankOrEmpty(statusId)) {
				products.setParameter("CurrentStatusID", statusId);
				products.setParameter(
						"ProductCurrentStatus.ProductStatusList.StatusID", statusId);
			}
		}
		if (products.getString("Storey").length() > 0) {
			products.setColumn("ProductHomeDetails.Storey",
					products.getString("Storey"));
			//hasvalue = true;
		}
		if (products.getString("Bedrooms").length() > 0) {
			products.setColumn("ProductHomeDetails.Bedrooms",
					products.getString("Bedrooms"));
			//hasvalue = true;
		}
		if (products.getString("Bathrooms").length() > 0) {
			products.setColumn("ProductHomeDetails.Bathrooms",
					products.getString("Bathrooms"));
			//hasvalue = true;
		}
		if (products.getString("Study").length() > 0) {
			products.setColumn("ProductHomeDetails.Study",
					products.getString("Study"));
			//hasvalue = true;
		}

		products.setParameter("-groupby0", "ProductID");
		//products.setParameter("-groupby1","ProductParentHomeDesign.ProductVariationHomeDesign.ProductID");

		if (hasValue(request.getParameter("-sorting"))) {
			String sortBy = request.getParameter("-sorting");
			if ("Week".equalsIgnoreCase(sortBy)) {
				products.sortBy("CreatedDate", 0);
			} else if ("Month".equalsIgnoreCase(sortBy)) {
				products.sortBy("CreatedDate", 0);
			} else if ("StageName".equalsIgnoreCase(sortBy)) {
				products.sortBy("EstateName", 0);
				products.sortBy("StageName", 1);
			} else if ("LotName".equalsIgnoreCase(sortBy)) {
				products.sortBy("EstateName", 0);
				products.sortBy("StageName", 1);
				products.sortBy("LotName", 2);
			} else if ("BrandName".equalsIgnoreCase(sortBy)) {
				products.sortBy("BrandName", 0);
			} else if ("PlanName".equalsIgnoreCase(sortBy)) {
				products.sortBy("PlanName", 0);
			} else if ("RangeName".equalsIgnoreCase(sortBy)) {
				products.sortBy("RangeName", 0);
			} else if ("CanFitOnFrontage".equalsIgnoreCase(sortBy)) {
				products.sortBy("CanFitOnFrontage", 0);
			} else {
				products.sortBy("Name", 0);
			}
			//products.sortBy(products.getString("-sorting"), 0);
		} else {
			products.sortBy("Name", 0);
		}

		String createDate = request.getParameter("createDate");
		if (!StringUtil.isBlankOrEmpty(createDate)) {
			products.setParameter("CreatedDate", createDate);
		}
		
		products.setAction(GenerationKeys.SEARCH);
		//products.doAction();
		//products.getResults();

		if(logger.isDebugEnabled()) { 
			logger.debug(products.getStatement());
		}
		//System.out.println("<!--stupidSQL " + products.getStatement() + " -->");

		return products;
	}
	/*
	public String convertToString(ArrayList<String> productIds) {
		if (productIds == null || productIds.size() == 0) return "";
		StringBuilder buf = new StringBuilder();
		for (String s : productIds) {
			buf.append(s);
			buf.append("+");
		}

		buf = buf.deleteCharAt(buf.length() - 1);
		return buf.toString();
	}
	*/

	
}
