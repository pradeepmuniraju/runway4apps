package com.sok.runway.crm.cms.properties.pdf.templates.model;

public class RangeBrandInfo 
{
		public String rangeID = "";
		public String rangeName = "";
		public String rangeColour = "";
		
		public String getRangeID() {
			return rangeID;
		}
		public void setRangeID(String rangeID) {
			this.rangeID = rangeID;
		}
		public String getRangeName() {
			return rangeName;
		}
		public void setRangeName(String rangeName) {
			this.rangeName = rangeName;
		}
		public String getRangeColour() {
			return rangeColour;
		}
		public void setRangeColour(String rangeColour) {
			this.rangeColour = rangeColour;
		}
}