/* ========================================================================= **
 o FILE TYPE        : Java Source File - text/java
 o FILE NAME        : Document.java
 o DESIGNER(S)      :
 o CODER(S)         :
 o VERSION          : 1.0
 o LAST MODIFIED BY :
 o LAST MODIFIED ON :
** ========================================================================= */

/* ======================= ** PACKAGE DECLARATION ** ======================= */
package com.sok.runway.crm;
/* ========================================================================= */

/* ======================== ** IMPORT STATEMENTS ** ======================== */
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.*;
import com.sok.runway.security.*;
import javax.servlet.ServletRequest;

import java.io.File;
import java.sql.Connection;
import java.util.*;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
/* ========================================================================= */
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement(name="Document")
public class Document extends SecuredEntity {

	public Document() {
		super((Connection)null, "DocumentView");
	}
	
	public Document(Connection con) {

		super(con, "DocumentView");
	}

	public Document(ServletRequest request) {

		super(request, "DocumentView");
		populateFromRequest(request.getParameterMap());
	}

	public Document(Connection con, String documentID) {

		this(con);
	   load(documentID);
	}
	
	/**
	 * Convenience constructor to produce document element from Product Image / ImageName / Image Path
	 * @param documentID
	 * @param fileName
	 * @param filePath
	 */
	public Document(String documentID, String fileName, String filePath) {
		super((Connection)null, "DocumentView");
		setField("DocumentID", documentID);
		setField("FileName", fileName);
		setField("FilePath", filePath);
	}

	public Validator getValidator() {

		Validator val = Validator.getValidator("Document");

	   if (val == null) {
		   val = new Validator();
		   val.addMandatoryField("DocumentID","SYSTEM ERROR - DocumentID");
		   val.addMandatoryField("GroupID","Document must have a security group set");
		   val.addMandatoryField("FileName","Document must have a filename");
		   Validator.addValidator("Document", val);
	   }

	   return val;
	}
	
   /**
    * Deletes the physical file and any others in the directory after removing the database record. 
    */
   public boolean delete() {
	   
	   GenRow check = new GenRow();	
	   check.setConnection(getConnection());
	   check.setViewSpec("LinkedDocumentCheckView");
	   check.setColumn("DocumentID", getPrimaryKey());  	
	   check.doAction(GenerationKeys.SELECTFIRST);
	   
	   if(!check.isSuccessful()) { 
	      if (isLoaded()) {
	    	  if(!"Y".equals(getField("LocalFile")) ) { 
	    		  
	    		  String filePath = InitServlet.getRealPath() + "/" + bean.getColumn("FilePath");
	    		  
	    		  com.sok.Debugger d = com.sok.Debugger.getDebugger(); 
	    		  d.debug(filePath); 

	              if (File.separatorChar != '/') {
	  				   filePath = StringUtil.replace(filePath, '/', File.separatorChar);
	 	          }
	              File f = new File(filePath);
	              File parent = f.getParentFile();
	              f.delete();
	              
	              for(File rem: parent.listFiles()) { 
	            	  rem.delete();
	              }
	              
	              if(parent.delete()) { 
	            	  return super.delete();
	              }
	    	  } else { 
	    		  return super.delete(); 
	    	  }
	      }
	   } 
	   return false; 
   }
   
   public static void updateDocumentSize(HttpServletRequest request, String documentID, int width, int height) {
	   GenRow row = new GenRow();
	   row.setTableSpec("Documents");
	   row.setRequest(request);
	   row.setParameter("DocumentID", documentID);
	   row.setParameter("Width", "" + width);
	   row.setParameter("Height", "" + height);
	   row.doAction("update");
   }
   
   @XmlElement(name="DocumentID")
   public String getDocumentID() {
   		return getField("DocumentID");
   }
   public void setDocumentID(String DocumentID) {
	   setField("DocumentID", DocumentID);
   }
   @XmlElement(name="FileName")
   public String getFileName() {
   		return getField("FileName");
   }
   public void setFileName(String FileName) {
   		setField("FileName", FileName);
   }
   @XmlElement(name="FilePath")
   public String getFilePath() {
   		return getField("FilePath");
   }
   public void setFilePath(String FilePath) {
	   setField("FilePath", FilePath);
   }
}