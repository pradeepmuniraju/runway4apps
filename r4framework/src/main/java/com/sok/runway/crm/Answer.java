package com.sok.runway.crm;

import com.sok.runway.*;
import com.sok.runway.crm.profile.Question;
import com.sok.runway.security.*;
import com.sok.framework.*;

import java.sql.*;
import java.util.*;

public class Answer extends AuditEntity {
   
   public Answer(Connection con, String viewSpec) {
      super(con, viewSpec);
   }

	public void loadAnswer(String questionID, String id, String idColumn) {
	     
		if (questionID.length()>0 && id.length()>0) { 
         String[] fields = {"QuestionID",idColumn}; 
         String[] values = {questionID,id};
         
         loadFromField(fields,values); 
		}
	}
   
   
   public Validator getValidator() {
	   Validator val; 
	   if(getField("QuestionID").length()>0) { 
		   val = new Validator(); 
	   } else { 
		  val = Validator.getValidator("Answer"); //generic validator
	      if (val == null) { 
	          val = new Validator();
	          val.addMandatoryField("AnswerID","SYSTEM ERROR - AnswerID");
	          val.addMandatoryField("QuestionID","SYSTEM ERROR - QuestionID");
	          Validator.addValidator("Answer", val);
	      } 
	      return val; 
	   }
	
       val.addMandatoryField("AnswerID","SYSTEM ERROR - AnswerID");
       val.addMandatoryField("QuestionID","SYSTEM ERROR - QuestionID");
       
	   Question q = new Question(getConnection(), getField("QuestionID")); 
	   if("Number".equals(q.getField("InputType")) && 
			   (q.getField("MinValue").length()>0 || q.getField("MaxValue").length()>0)) { 
		   val.addRangeField("AnswerNum", q.getDouble("MinValue"), q.getDouble("MaxValue"), "VALIDATION ERROR - Number Range"); 
	   }
	   
      return val;
   }
   
}