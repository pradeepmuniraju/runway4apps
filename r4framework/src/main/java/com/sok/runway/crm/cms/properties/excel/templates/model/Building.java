package com.sok.runway.crm.cms.properties.excel.templates.model;

import java.util.ArrayList;
import java.util.List;

public class Building {
	private List<Apartment> hnlPackages = new ArrayList<Apartment>();
	private String buildingName;

	public List<Apartment> getApartments() {
		return hnlPackages;
	}

	public void setApartments(List<Apartment> hnlPackages) {
		this.hnlPackages = hnlPackages;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
}
