package com.sok.runway.crm.cms.properties;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

public class Stage extends PropertyEntity {

	final PropertyType propertyType = PropertyType.Stage;
	static final String VIEWSPEC = "properties/ProductStageView";
	final String PROPERTYTYPE = "Stage";
	private Estate estate = null;
	public Stage(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,PROPERTYTYPE);
	}
	public Stage(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		load(productID);
	}
	
	public Stage(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		load(productID);
	}
	public Stage load(String productID) { 
		return (Stage)super.load(productID);
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
	
	public Estate getEstate() {
		if(estate == null && isSuccessful() && isSet("EstateProductID")) {
			estate = new Estate(getConnection()).load(getData("EstateProductID"));
			if(!estate.isSuccessful()) { 
				estate = null;
			}
		}
		return estate;
	}
	
	public String getLocationID() {
		if(isSet("LocationID")) {
			return getString("LocationID");
		}
		if(getEstate() != null) {
			return estate.getLocationID();
		}
		return null;
	}
}
