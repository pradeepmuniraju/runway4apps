package com.sok.runway.crm.cms.properties.pdf.templates.model;

public class EstateLotHouseAndLandInfo 
{
		public String lotNumber;
		
		public String range;
		
		public String estateProductID;
		public String estateEstateName;
		public String estateStageName;

		/*public String isAccess;
		public String isAccessTerrace;
		public String isLifestyle;
		public String isLifestyleTerrace;
		public String isPrestigeSingleStorey;
		public String isPrestigeDoubleStorey;
		*/
		
		public String isRangeBrand1;
		public String isRangeBrand2;
		public String isRangeBrand3;
		public String isRangeBrand4;
		public String isRangeBrand5;
		public String isRangeBrand6;
		public String isRangeBrand7;
		public String isRangeBrand8;
		public String isRangeBrand9;
		public String isRangeBrand10;
		
		
		public String getIsRangeBrand1() {
			return isRangeBrand1;
		}
		public void setIsRangeBrand1(String isRangeBrand1) {
			this.isRangeBrand1 = isRangeBrand1;
		}
		public String getIsRangeBrand2() {
			return isRangeBrand2;
		}
		public void setIsRangeBrand2(String isRangeBrand2) {
			this.isRangeBrand2 = isRangeBrand2;
		}
		public String getIsRangeBrand3() {
			return isRangeBrand3;
		}
		public void setIsRangeBrand3(String isRangeBrand3) {
			this.isRangeBrand3 = isRangeBrand3;
		}
		public String getIsRangeBrand4() {
			return isRangeBrand4;
		}
		public void setIsRangeBrand4(String isRangeBrand4) {
			this.isRangeBrand4 = isRangeBrand4;
		}
		public String getIsRangeBrand5() {
			return isRangeBrand5;
		}
		public void setIsRangeBrand5(String isRangeBrand5) {
			this.isRangeBrand5 = isRangeBrand5;
		}
		public String getIsRangeBrand6() {
			return isRangeBrand6;
		}
		public void setIsRangeBrand6(String isRangeBrand6) {
			this.isRangeBrand6 = isRangeBrand6;
		}
		public String getIsRangeBrand7() {
			return isRangeBrand7;
		}
		public void setIsRangeBrand7(String isRangeBrand7) {
			this.isRangeBrand7 = isRangeBrand7;
		}
		public String getIsRangeBrand8() {
			return isRangeBrand8;
		}
		public void setIsRangeBrand8(String isRangeBrand8) {
			this.isRangeBrand8 = isRangeBrand8;
		}
		public String getIsRangeBrand9() {
			return isRangeBrand9;
		}
		public void setIsRangeBrand9(String isRangeBrand9) {
			this.isRangeBrand9 = isRangeBrand9;
		}
		public String getIsRangeBrand10() {
			return isRangeBrand10;
		}
		public void setIsRangeBrand10(String isRangeBrand10) {
			this.isRangeBrand10 = isRangeBrand10;
		}
		
		public String getLotNumber() {
			return lotNumber;
		}
		public void setLotNumber(String lotNumber) {
			this.lotNumber = lotNumber;
		}
		public String getEstateEstateName() {
			return estateEstateName;
		}
		public void setEstateEstateName(String estateEstateName) {
			this.estateEstateName = estateEstateName;
		}
		public String getEstateStageName() {
			return estateStageName;
		}
		public void setEstateStageName(String estateStageName) {
			this.estateStageName = estateStageName;
		}

		public String getEstateProductID() {
			return estateProductID;
		}
		public void setEstateProductID(String estateProductID) {
			this.estateProductID = estateProductID;
		}
		public String getRange() {
			return range;
		}
		public void setRange(String range) {
			this.range = range;
		}
}