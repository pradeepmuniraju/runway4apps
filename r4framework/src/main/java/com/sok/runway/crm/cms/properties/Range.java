package com.sok.runway.crm.cms.properties;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class Range extends PropertyEntity {

	final PropertyType propertyType = PropertyType.Range;

	static final String VIEWSPEC = "ProductView";

	public Range(Connection con) {
		setConnection(con);
		setViewSpec(VIEWSPEC);
		setParameter(PRODUCTTYPE,getProductType());
	}
	public Range(HttpServletRequest request, String productID) { 
		setViewSpec(VIEWSPEC);
		setRequest(request);
		setParameter(PRODUCTTYPE,getProductType());
		load(productID);
	}

	public Range(Connection con, String productID) {
		setViewSpec(VIEWSPEC);
		setConnection(con);
		setParameter(PRODUCTTYPE,getProductType());
		load(productID);
	}
	
	@Override
	public Range load(String productID) {
		return (Range)super.load(productID);
	}
	public List<BuildingStage> getStages() {
		return Collections.emptyList();
	}
	@Override
	public PropertyType getPropertyType() {
		return propertyType;
	}
}