package com.sok.runway;

import java.sql.Connection;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Properties;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringUtils; 

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.RelationSpec;
import com.sok.framework.InitServlet;
import com.sok.framework.RowArrayBean;
import com.sok.framework.RowBean;
import com.sok.framework.RowSetBean;
import com.sok.framework.RunwayUtil;
import com.sok.framework.SpecManager;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.exchange.ExchangeConnect;
import com.sok.runway.exchange.RunwayExchangeAppointmentAttendee;
import com.sok.runway.exchange.ExchangeConnect.RunwayExchangeEmail;
import com.sok.runway.exchange.ExchangeConnect.RunwayExchangeAppointment;
import com.moyosoft.exchange.folder.ExchangeFolder;
import com.moyosoft.exchange.folder.ExchangeFolderListener;
import com.sok.runway.offline.ThreadManager;
import com.sok.runway.security.SecurityFactory;
import com.sok.runway.security.User;
import com.sok.runway.security.interfaces.*;

import java.util.Date;

public class UserBean extends RowBean implements RunwayUser, ExchangeFolderListener {

	private static final Logger logger = LoggerFactory.getLogger(UserBean.class);

	public static final String repuserid = "RepUserID";
	public static final String sectioncode = "SectionCode";
	public static final String grouplevel = "GroupLevel";
	public static final String groupID = "GroupID";
	public static final String permissionID = "PermissionID";	
	public static final String Y = "Y";
	public static final String N = "N";

	public static final String NO_VIEWABLE_GROUPS= "NO VIEWABLE GROUPS";

	public static final String CanAdmin = "CanAdmin";
	public static final String CanManage = "CanManage";
	public static final String CanUseCMS = "CanUseCMS";
	public static final String CanReport = "CanReport";
	public static final String CanUseCalendar = "CanUseCalendar";
	public static final String CanUseInbox = "CanUseInbox";
	public static final String CanAdminRowProperties = "CanAdminRowProperties";

	public static final String CanSendEmails = "CanSendEmails";
	public static final String CanSendEmailsOffline = "CanSendEmailsOffline";
	public static final String CanDeleteSentEmails = "CanDeleteSentEmails";
	public static final String CanEditOffline = "CanEditOffline";
	public static final String CanExport = "CanExport";

	// Deprecated
	public static final String CanInsert = "CanInsert";
	public static final String CanUse = "CanUse";
	// End Deprecated

	public static final String[] logins = {"Login Failed","Login","Inactive Login","No Login"};

	public static final String CanAccess = "CanAccess";
	public static final String CanView = "CanView";
	public static final String CanViewAssigned = "CanViewAssigned";
	public static final String CanCreate = "CanCreate";
	public static final String CanEdit = "CanEdit";
	public static final String CanEditRep = "CanEditRep";
	public static final String CanEditAssigned = "CanEditAssigned";
	public static final String CanAdminStatus = "CanAdminStatus";
	public static final String CanDelete = "CanDelete";
	public static final String CanLink = "CanLink";
	public static final String CanMerge = "CanMerge";

	public static final String userid = "UserID";
	public static final String title = "Title";
	public static final String firstname = "FirstName";
	public static final String lastname = "LastName";
	public static final String company = "Company";
	public static final String position = "Position";
	public static final String lastlogin = "LastLogin";
	public static final String username = "Username";
	public static final String password = "Password";
	public static final String userlevel = "UserLevel";
	public static final String email = "Email";
	public static final String emailhost = "EmailHost";
	public static final String emailprotocol = "EmailProtocol";
	public static final String emailusername = "EmailUsername";
	public static final String emailpassword = "EmailPassword";
	public static final String timezone = "TimeZone";
	public static final String status = "Status";
	public static final String dateformat = "DateFormat";
	public static final String saveemails = "SaveEmails";
	public static final String territory = "Territory";
	public static final String ipaddress = "IPAddress";
	public static final String cipaddress = "-IPAddress";
	public static final String sessioncreated = "-SessionCreated";

	public static final String sessionkey = "SessionKey";

	public static final String authenticated = "1";
	public static final String loginfailed = "0";
	public static final String deactivated = "2";	
	public static final String maintance = "3";	

	public static final String active = "Active";	
	public static final String inactive = "Inactive";	

	public static final String sys = "system";
	public static final String admin = "admin";
	public static final String mgr = "mgr";
	public static final String rep = "rep";
	public static final String guest = "guest";
	public static final String rest = "restricted";

	public static final String usergroupview = "UserGroupView";
	public static final char wild = '%';
	public static final char plus = '+';
	public static final char dot = '.';

	public static final String LICENCE_TYPE = "LicenceType";
	public static final String ADMIN = "Admin";

	private ExchangeConnect		exchange = null; 
	transient private User user = null;

	private boolean			killMail = false;
	
	private boolean			isPortalLogin = false;

	TimeZone thistimezone = null;

	private String			lastLogin = "";

	//HashMap groupSecurity = null;

	HashMap rowProperties = null;

	boolean resolveip = true;

	private String currentIP = ActionBean._blank;

	public boolean isAdminUser() {
		boolean isAdminUser = false;
		String licenseType = getString(LICENCE_TYPE);
		if (!StringUtil.isBlankOrEmpty(licenseType)) {
			if (ADMIN.equalsIgnoreCase(licenseType)) {
				isAdminUser = true;
			}
		}

		return isAdminUser;
	}

	/* old style getters and setters for backwards compatibility */
	public String getCurrentUserID()
	{
		return(getString(userid));
	}

	public String getUserID()
	{
		return(getString(userid));
	}

	public String getUserLevel()
	{
		return(getString(userlevel));
	}

	public String getCurrentUserLevel()
	{
		return(getString(userlevel));
	}

	public void setCurrentUserLevel(String s)
	{
		put(userlevel, s);
		logSession(s);
	}

	public String getCurrentUserTerritory()
	{
		return(getString(territory));
	}

	public String getTerritory()
	{
		return(getString(territory));
	}

	public String getCurrentUserName()
	{
		return(getString(username));
	}

	public String getCurrentUserPassword()
	{
		return(getString(password));
	}

	public String getPosition()
	{
		return(getString(position));
	}

	public String getCompany()
	{
		return(getString(company));
	}

	public String getTitle()
	{
		return(getString(title));
	}

	public String getName() {
		return new StringBuilder().append(getFirstName()).append(" ").append(getLastName()).toString();
	}

	public String getFirstName()
	{
		return(getString(firstname));
	}

	public String getLastName()
	{
		return(getString(lastname));
	}

	public String getUserName()
	{
		return(getString(username));
	}

	public String getSaveEmails()
	{
		return(getString(saveemails));
	}

	public String getLastLogin()
	{
		return(getColumn(lastlogin));
	}

	public String getEmail()
	{
		return(getColumn(email));
	}
	public String getEmailProtocol()
	{
		return(getColumn(emailprotocol));
	}
	public String getEmailHost()
	{
		return(getColumn(emailhost));
	}

	public String getEmailUsername()
	{
		return(getColumn(emailusername));
	}

	public String getEmailPassword()
	{
		return(getColumn(emailpassword));
	}

	public void setUsername(String s)
	{
		put(username, s);	
	}

	public void setPassword(String s)
	{
		put(password, s);
	}

	public String getUserTimeZone()
	{
		return(getString(timezone));
	}

	public TimeZone getTimeZoneObject()
	{
		if(thistimezone==null)
		{
			logger.debug("Getting TimeZone from String [{}]", getString(timezone));
			thistimezone = TimeZone.getTimeZone(StringUtils.isNotBlank(getString(timezone)) ? getString(timezone) : RunwayUtil.getGlobalTimeZone());
		}
		return(thistimezone);
	}

	public void setResolveIP(boolean b)
	{
		resolveip = b;
	}	

	public void parseRequest(HttpServletRequest request)
	{
		try
		{
			currentIP = request.getRemoteAddr();
			if(resolveip) {
				setColumn(cipaddress,request.getRemoteHost());
				//setColumn(ipaddress,request.getRemoteHost());
			}
			else {
				setColumn(cipaddress,currentIP);
				//setColumn(ipaddress,currentIP);
			}
			if (user != null) {
				user.clear();
			}
		}
		catch(Exception e)
		{
			setColumn(ipaddress,request.getRemoteAddr());
			setError("Error encountered while getting remote host: ");
			setError(ActionBean.writeStackTraceToString(e));
		}
		super.parseRequest(request);
	}

	public String checkLogin() {
		String login = loginfailed;
		String enteredpassword = getString(password);
		try
		{
			Properties sysprops = InitServlet.getSystemParams();
			if("true".equals(sysprops.getProperty("PasswordEncyptionRequired"))){
				enteredpassword = SecurityFactory.hashPassword(enteredpassword);
			}
			super.setColumn("Password", enteredpassword);
			super.retrieveData();

			if(getString(userid).length()!=0 && enteredpassword.equals(getString(password)))
			{
				if(!getString(status).equals(active))
				{
					login = deactivated;
				}
				else
				{
					login = authenticated;
				}
			}
		}
		catch(Exception e)
		{
			setError("Error encountered while retrieving login: ");
			setError(ActionBean.writeStackTraceToString(e));
		}

		return(login);
	}

	public String getLogin() {
		return getLogin(true);
	}
	public String getLogin(boolean checkEmail)
	{
		String login = loginfailed;
		String enteredpassword = getString(password);
		String originalpassword = getString(password);
		
		try
		{
			Properties sysprops = InitServlet.getSystemParams();
			if("true".equals(sysprops.getProperty("PasswordEncyptionRequired"))){
				enteredpassword = SecurityFactory.hashPassword(enteredpassword);
			}
			super.setColumn("Password", enteredpassword);
			super.retrieveData();

			if(getString(userid).length() == 0) {
				// Possible non encrypted password used.
				enteredpassword = originalpassword;
				super.setColumn("Password", enteredpassword);
				super.retrieveData();
			}
			if(getString(userid).length()!=0 && enteredpassword.equals(getString(password)))
			{
				if(!getString(status).equals(active))
				{
					login = deactivated;
				}
				else
				{
					login = authenticated;
					updateLoginDate();
					loadGroups(getConnection());
					if (!getUser().getPermission("CanLogin")) {
						return maintance;
					}
					if (checkEmail) {
						ThreadManager.startThread(new Runnable()  {
							@Override
							public void run() {
								//setColumn("ExchangeMessage","Starting Exchange Sync");
								logSession("Exchange Started");
								syncAppointments(false, true);
								syncMail(false);
								//setColumn("ExchangeMessage","Ending Exchange Sync");
								logSession("Exchange Completed");
							}
						},"UserBean-ApptSync-");
					}
				}
			}
		}
		catch(Exception e)
		{
			setError("Error encountered while retrieving login: ");
			setError(ActionBean.writeStackTraceToString(e));
		}
		super.setParameter(ipaddress,getString(cipaddress));
		logSession(logins[Integer.parseInt(login)]);
		return(login);
	}
	
	public String getPortalLogin()
	{
		return getPortalLogin(true);
	}
	
	public String getPortalLogin(boolean log)
	{
		String login = loginfailed;
		
		try
		{	
			super.retrieveData();

			if(!getString(status).equals(active))
			{
				login = deactivated;
			}
			else
			{
				login = authenticated;
				isPortalLogin = true;
				updateLoginDate();
				loadGroups(getConnection());
				if (!getUser().getPermission("CanLogin")) {
					return maintance;
				}
			}
		}
		catch(Exception e)
		{
			setError("Error encountered while retrieving login: ");
			setError(ActionBean.writeStackTraceToString(e));
		}
		super.setParameter(ipaddress,getString(cipaddress));
		if (log) logSession(logins[Integer.parseInt(login)]);
		return(login);
	}
	
	public String getSessionCreated()
	{
		return(getColumn(lastlogin,"M"));
	}

	public void updateLoginDate()
	{
		put(lastlogin, new java.sql.Timestamp(new java.util.Date().getTime()));

		RowBean updatebean = new RowBean();
		updatebean.setJndiName((String)super.get(RowBean.jndiname));
		updatebean.setViewSpec(super.getViewSpec());
		updatebean.setLocale(super.getLocale());
		updatebean.setColumn(lastlogin,ActionBean.NOW);
		updatebean.setColumn(userid,getString(userid));
		updatebean.setColumn(ipaddress,getString(cipaddress));
		updatebean.setAction(ActionBean.UPDATE);
		updatebean.setToNewID(sessionkey);
		put(sessionkey, updatebean.getString(sessionkey));
		updatebean.doAction();

	}

	public void cleanUp()
	{
		logSessionExpire();
	}
	public void logSessionExpire() {
		logSession("Session Closed");
	}
	public void logSession(String actionType)
	{
		RowBean activitylog = new RowBean();
		activitylog.setJndiName((String)super.get(RowBean.jndiname)); 
		activitylog.setViewSpec(SpecManager.getViewSpec("ActivityView"));
		activitylog.setColumn("Location",getString("IPAddress"));
		if (activitylog.getColumn("Location").length() == 0) activitylog.setColumn("Location",getString(cipaddress));
		activitylog.setColumn("UserID",getString("UserID"));
		activitylog.setAction("insert"); 
		activitylog.setColumn("RecordType","Action");
		activitylog.setColumn("ActionType",actionType);
		activitylog.setColumn("CreatedDate","NOW");
		activitylog.createNewID();
		activitylog.doAction();
	}
	
	/**
	 * Convenience method to set defaults as request attributes (like search screens)
	 */
	public void setDefaults(HttpServletRequest req, String groupRelation, boolean search) {
		for(Map.Entry<String,String> v: getDefaults(groupRelation, search).entrySet()) {
			req.setAttribute(v.getKey(), v.getValue());
		}
	}
	
	/**
	 * Convenience method to set defaults as tabledata parameters (like action files)
	 */
	public void setDefaults(TableData data, String groupRelation, boolean search) {
		for(Map.Entry<String,String> v: getDefaults(groupRelation, search).entrySet()) {
			data.setParameter(v.getKey(), v.getValue());
		}
	}

	/**
	 * 
	 * This method should return a map of those parameters which should be set based on the user preferences
	 * and the search or list being invoked. 
	 * @return Map<String,String> of params or emptyMap if not applicable. 
	 */
	public Map<String,String> getDefaults(String groupRelation, boolean search) {
		
		final String type = getString(search ? "DefaultSearchGroup" : "DefaultList"); 
		groupRelation = StringUtils.trimToEmpty(groupRelation);
		if(groupRelation.length() != 0 && !groupRelation.endsWith(".")) {
			groupRelation += ".";
		}
		if("Mine".equals(type)) {
			Map<String, String> ret = new HashMap<String, String>(2);
			ret.put(groupRelation +"GroupID", getString("DefaultGroupID"));
			ret.put(groupRelation + "RepUserID", getString("UserID"));
			return ret; 
		} else if("My Group".equals(type)) {
			Map<String, String> ret = new HashMap<String, String>(1);
			ret.put(groupRelation +"GroupID", getString("DefaultGroupID"));
			return ret; 
		} else if("My Location".equals(type)) {
			List<String> userList = new LinkedList<String>(); 
			if(locationReps == null) { 
				GenRow list = new GenRow(); 
				list.setTableSpec("ContactLocations");
				list.setParameter("-select1","LocationID");
				list.setParameter("-select2","ContactLocationLocation.ProductID as LocationCompanyID");
				list.setParameter("UserID", getUserID());
				list.setParameter("ViewOther","Y");
				list.setParameter("LocationID","!EMPTY");
				try {
					if(logger.isTraceEnabled()) {
						list.doAction(GenerationKeys.SEARCH);
						logger.trace(list.getStatement());
					}
					list.getResults(); 
					while(list.getNext()) {
						userList.add(list.getData("LocationCompanyID"));
					}
				} finally {
					list.setCloseConnection(true);
					list.close();
				}
				
				list.clear();
				list.setTableSpec("CompanyContactLinks"); 
				list.setParameter("-select1","ContactID");
				list.setParameter("CompanyID", StringUtils.join(userList, "+"));
				userList.clear();
				try {
					if(logger.isTraceEnabled()) {
						list.doAction(GenerationKeys.SEARCH);
						logger.trace(list.getStatement());
					}
					list.getResults(); 
					while(list.getNext()) {
						userList.add(list.getData("ContactID"));
					}
				} finally {
					list.setCloseConnection(true);
					list.close();
				}
				
				list.clear(); 
				list.setTableSpec("UserContacts");
				list.setParameter("ContactID", StringUtils.join(userList, "+"));
				userList.clear();
				list.setParameter("-select1", "UserContactUsers.UserID as UserID"); 
				try {
					if(logger.isTraceEnabled()) {
						list.doAction(GenerationKeys.SEARCH);
						logger.trace(list.getStatement());
					}
					list.getResults(); 
					while(list.getNext()) {
						userList.add(list.getData("UserID"));
					}
				} finally {
					list.setCloseConnection(true);
					list.close();
				}
				locationReps = userList.isEmpty() ? "NO_LOCATION_REPS" : StringUtils.join(userList,"+");
			} 
			Map<String, String> ret = new HashMap<String, String>(1);
			ret.put(groupRelation +"RepUserID", locationReps);
			return ret; 
		}
		return Collections.emptyMap();
	} 

	private transient String locationReps = null;
	
	/* access restrictions */

	public void setConstraint(TableData data) {
		getUser().setSearchConstraint(data, null);
	}

	public void setConstraint(TableData data, String relation) {
		//setConstraint(data, relation, true, null, false, false);
		getUser().setSearchConstraint(data, relation);
	}

	/**
	 * @deprecated
	 */
	public void setConstraintWithRep(TableData data, String relation) {
		//setConstraint(data, relation, true, null, true, false);
		setConstraint(data, relation, null);
	}

	/**
	 * @deprecated
	 */
	public void setConstraintWithRep(TableData data, String relation, String restriction) {
		setConstraint(data, relation, restriction);
	}

	public void setConstraint(TableData data, String relation, String restriction) {
		setConstraint(data, relation, true, restriction, false, false);
	}

	public void setConstraint(TableData data, String relation, boolean overwrite) {
		setConstraint(data, relation, overwrite, null, false, false);
	}


	public void setConstraint(TableData data, String relation, boolean overwrite, String restriction, boolean repTable) {
		setConstraint(data, relation, overwrite, restriction, repTable, false);
	}

	/**
	 * @deprecated
	 */
	protected String getRelationPart(String relation) {
		String repRelation = null;
		if(relation!=null && relation.indexOf(dot) != -1) {
			repRelation = new StringBuffer().append(relation.substring(0, relation.indexOf(dot))).append(dot).toString();
		}
		else {
			repRelation = ActionBean._blank;
		}
		return(repRelation);
	}

	public void setConstraint(TableData data, String relation, boolean overwrite, String restriction, boolean repTable, boolean viewablebydefault) {

		if (viewablebydefault) {
			getUser().setStatusConstraint(data);
		}
		else if (CanExport.equals(restriction)) {
			getUser().setExportConstraint(data, relation);
		}
		else if (CanManage.equals(restriction)) {
			getUser().setManageConstraint(data, relation);
		}
		else if (CanEditOffline.equals(restriction)) {
			getUser().setBulkEditConstraint(data, relation);
		}
		else if (CanSendEmailsOffline.equals(restriction)) {
			getUser().setBulkSendConstraint(data, relation);
		}	   
		else if (overwrite) {
			getUser().setSearchConstraint(data, relation);
		}
		else {
			throw new RuntimeException("setConstraint with no overwrite no longer supported.");

			// USE OLD WAY
			/*data.setColumn(ActionBean.CURRENTUSERID, this.getUserID());
   		String key = null;
   		if(relation!=null) {
   			key = new StringBuffer().append(relation).append(dot).append(sectioncode).toString();
   		}
   		else {
   			key = sectioncode;
   		}


   		String existingvalue = data.getString(key);
   		if(overwrite || !getCodeAllowed(existingvalue))
   		{

   			// SectionCode for regular view groups
   			String viewCode = getConcatenatedCode(true, data, relation, restriction, false, viewablebydefault);

   			// If Table concerned has a RepUserID Column
   			if (repTable && data instanceof GenRow) {
   				// SectionCode for groups with view assigned setting
   				String assignedCode = getConcatenatedCode(true, data, relation, restriction, true);

         		String repRelation = getRelationPart(relation);

   				if (!assignedCode.equals(NO_VIEWABLE_GROUPS)) {
   					if (!viewCode.equals(NO_VIEWABLE_GROUPS)) {
   						// There needs to be a restriction for both View and View Assigned
   						data.setColumn(key+"|VC",viewCode);

   						String[] assignedCodes = assignedCode.split("/+"); 
   						StringBuffer nvAC = new StringBuffer();

   						for (String s:assignedCodes) { 
   							if(viewCode.indexOf(s) == -1) { //ie if it doesn't exist
   								if(nvAC.length()>0) { 
   									nvAC.append(plus).append(s); 
   								} else { 
   									nvAC.append(s); 
   								}
   							}
   						}
   						if(nvAC.length()>0) { 
   							data.setColumn(key+"^AC|VC",nvAC.toString());
   							data.setColumn(repRelation+repuserid+"^AC|VC", getUserID());
   						} 
   					}
   					else {
   						// There needs to be a restriction for View Assigned only
   						data.setColumn(key+"^AC",assignedCode);
   						data.setColumn(repRelation+repuserid+"^AC", getUserID());
   					}
   				}
   				else {
   					// There needs to be a restriction for View only
   					data.setColumn(key,viewCode);
   				}
   			}
   			else if(viewablebydefault){
   			   String repRelation = getRelationPart(relation);
               data.setColumn(key+"|GNE",viewCode);
               data.setColumn(repRelation+"GroupID|GNE","NULL+EMPTY");
   		   }
   			else {
   				// There needs to be a restriction for View only
   				data.setColumn(key,viewCode);
   			}
   		}
			 */
			// END OLD WAY
		}
	}

	/**
	 * @deprecated
	 */
	public boolean getCodeAllowed(String search)
	{
		if(search!=null && search.length()!=0)
		{
			String tempcode = null;
			for(int i=0; i<getGroupSize(); i++)
			{
				tempcode = getGroup(i).getString(sectioncode);
				if(tempcode.length()==0)
				{
					return(true);
				}
				else if(search.indexOf(tempcode)==0)
				{
					return(true);
				}
			}
		}
		return(false);
	}	

	/**
	 * @deprecated
	 */
	private boolean isViewableSectionCode(String groupID, String module, String relation, boolean assigned) {

		String tableName = null;
		String firstRelation = relation;
		String relationSuffix = "";

		if (module.equals("UserGroups")) {
			tableName = "Users";
		}
		else if (relation == null) {
			tableName = module;
		}
		else {
			int dotIndex = relation.indexOf(".");
			if (dotIndex > -1) {
				firstRelation = relation.substring(0,relation.indexOf("."));
				relationSuffix = relation.substring(dotIndex+1);
			}
			RelationSpec relSpec = SpecManager.getRelationSpec();

			if (relSpec.hasRelation(firstRelation)) {
				tableName = relSpec.getFromTableName(relSpec.getIndex(firstRelation));
			}
		}


		RowArrayBean security = getGroupSecurity(groupID);
		TableData access = security.get("Module", tableName);

		if(access != null) {
			if(access.getString(CanView).equals(Y) && !assigned) {
				return(true);
			}
			else if(assigned && access.getString(CanViewAssigned).equals(Y)) {
				return(true);
			}
		}
		else if (relationSuffix.length() > 0) {
			return isViewableSectionCode(groupID, module, relationSuffix, assigned);
		}
		else {
			throw new RuntimeException("Incorrect Security Implementation - " + tableName);
		}

		return false;
	}

	/**
	 * @deprecated
	 */
	public String getConcatenatedCode(boolean search, TableData data, String relation) {
		return getConcatenatedCode(search, data.getTableSpec().getTableName(), relation, null, false);
	}

	/**
	 * @deprecated
	 */
	public String getConcatenatedCode(boolean search, TableData data, String relation, String restriction, boolean assigned) {
		return getConcatenatedCode(search, data.getTableSpec().getTableName(), relation, restriction, assigned);
	}

	/**
	 * @deprecated
	 */
	public String getConcatenatedCode(boolean search, TableData data, String relation, String restriction, boolean assigned, boolean viewabledefault) {
		return getConcatenatedCode(search, data.getTableSpec().getTableName(), relation, restriction, assigned, viewabledefault);
	}

	/**
	 * @deprecated
	 */
	public String getConcatenatedCode(boolean search, String module, String relation) {
		return getConcatenatedCode(search, module, relation, null, false);
	}

	/**
	 * @deprecated
	 */
	public String getConcatenatedCode(boolean search, String module, String relation,String restriction, boolean assigned) {
		return getConcatenatedCode(search, module, relation, restriction, assigned, false);
	}	

	/**
	 * @deprecated
	 */
	public String getConcatenatedCode(boolean search, String module, String relation, String restriction, boolean assigned, boolean viewabledefault)
	{
		StringBuffer temp = new StringBuffer();
		String tempcode = null;
		boolean added = false;
		boolean found = false;
		ArrayList cache = new ArrayList();

		for(int i=0; i<getGroupSize(); i++)
		{
			tempcode = getGroup(i).getString(sectioncode);	//CURRENT GROUP'S SECTION CODE

			if (restriction == null || getGroup(i).getString(restriction).equals(Y)) {
				if (viewabledefault || isViewableSectionCode(getGroup(i).getString(groupID), module, relation, assigned)) {
					/*
					 * IF THE CURRENT USER CAN VIEW THE PARTICULAR GROUP 
					 */
					if(cache.size()==0)
					{
						/*
						 * IF IT HASN'T CHECKED ANY SECTION CODES YET
						 */
						temp.append(tempcode);
						if(search)
						{
							temp.append(wild);
						}
						cache.add(tempcode);
						added = true;
					}
					else
					{
						/*
						 * IF SECTION CODES HAVE BEEN CHECKED, CHECK AGAINST THE CACHE
						 * IF AA HAS BEEN ADDED, THEN AAAA DOES NOT NEED TO BE, ETC. 
						 */
						found = false; 
						for(int j=0; j<cache.size(); j++)
						{
							//CHECK CACHE FOR THE CURRENT GROUPS SECTION CODE
							if(tempcode.startsWith((String)cache.get(j)))
							{
								found = true; 
								break;
							}
						}
						if(!found) { 
							//IF CURRENT SECTION CODE NOT FOUND IN THE CACHE, ADD IT
							temp.append(plus);
							temp.append(tempcode);
							if(search)
							{
								temp.append(wild);
							}
							cache.add(tempcode);
							added = true;
						}
					}
				}
			}
		}
		if(!added)
		{
			if(search)
			{
				temp.append(NO_VIEWABLE_GROUPS);
			}
		}
		return(temp.toString());
	}

	public void appendConcatenatedLevel(StringBuffer temp, String fieldname)
	{
		HashSet levels = new HashSet(10);

		for(int i=0; i<getGroupSize(); i++)
		{
			levels.add(getGroup(i).getString(grouplevel));
		}

		Iterator it = levels.iterator();
		temp.append('(');
		temp.append(fieldname);
		temp.append(ActionBean._equals);
		temp.append("'ALL'");

		while(it.hasNext())
		{
			temp.append(ActionBean._or);
			temp.append(fieldname);
			temp.append(ActionBean._equals);
			temp.append('\'');
			temp.append((String)it.next());
			temp.append('\'');
		}
		temp.append(')');
	}

	public boolean getCanAdmin() {
		return getUser().canAdmin();
	}

	public boolean getCanManage() {
		return getUser().canManage();
	}

	public boolean getCanUseCMS() {
		return getUser().canUseCMS();
	}

	public boolean getCanAccess()
	{
		if(!getString(userlevel).equals(rest))
		{
			return(true);
		}
		return(false);
	}

	public boolean getCanCreate()
	{
		return(getUser().getPermission(CanInsert));
	}

	public boolean getCanCreate(String code)
	{
		return(getCanCreate());
	}

	public boolean getCanDelete(String cuserid, String ter)
	{
		if (getCanEdit(cuserid, ter)) 
		{
			if(getString(userlevel).equals(rep))
			{
				if(cuserid.equals(getString(userid)))
				{
					return(getUser().getPermission(cuserid, ter, CanDelete));
				}
			}
			else
			{
				return(getUser().getPermission(cuserid, ter, CanDelete));
			}
		}
		return(false);
	}

	public boolean getCanEdit(String cuserid, String ter)
	{
		if(getString(userlevel).equals(rep))
		{
			if(cuserid.equals(getString(userid)))
			{
				return(getUser().getPermission(cuserid, ter, CanEdit));
			}
		}
		else
		{
			return(getUser().getPermission(cuserid, ter, CanEdit));
		}
		return(getCanEditAssigned(cuserid, ter));
	}

	public boolean getCanEditAssigned(String cuserid, String ter)
	{
		if (cuserid.equals(getString(userid))) {
			return getUser().getPermission(ter,CanEditAssigned);
		}
		return(false);
	}

	public boolean getCanEditRep()
	{
		return(getUser().getPermission(CanEditRep));		
	}

	public boolean getCanRestrictedEdit(String cuserid, String ter)
	{
		if(getString(userlevel).equals(rest))
		{
			return(true);
		}
		return(false);
	}

	public boolean getCanView(String cuserid, String ter)
	{
		if(getString(userlevel).equals(rep))
		{
			if(cuserid.equals(getString(userid)))
			{
				return(getUser().getPermission(cuserid, ter, CanView));
			}
		}
		else
		{
			return(getUser().getPermission(cuserid, ter, CanView));
		}
		return(getCanViewAssigned(cuserid, ter));
	}

	public boolean getCanViewAssigned(String cuserid, String ter)
	{
		if (cuserid.equals(getString(userid))) {
			return getUser().getPermission(ter,CanViewAssigned);
		}
		return(false);
	}

	public boolean getCanUse(TableData bean)
	{
		return getUser().getPermission(bean.getString(sectioncode),CanUse);
	}



	public boolean getCanRestrictedEdit(TableData bean)
	{
		return(getCanRestrictedEdit(bean.getString(repuserid),bean.getString(sectioncode)));
	}

	public boolean getCanDelete(TableData bean)
	{
		return(getCanDelete(bean.getString(repuserid),bean.getString(sectioncode)));
	}

	public boolean getCanEdit(TableData bean)
	{
		return(getCanEdit(bean.getString(repuserid),bean.getString(sectioncode)));
	}

	public boolean getCanEditAssigned(TableData bean)
	{
		return(getCanEditAssigned(bean.getString(repuserid),bean.getString(sectioncode)));
	}

	public boolean getCanView(TableData bean)
	{
		return(getCanView(bean.getString(repuserid),bean.getString(sectioncode)));
	}

	public boolean getCanViewAssigned(TableData bean)
	{
		return(getCanViewAssigned(bean.getString(repuserid),bean.getString(sectioncode)));
	}

	public boolean getCanRestrictedEdit(Object rep, Object ter)
	{
		return(getCanRestrictedEdit((String)rep,(String)ter));
	}

	public boolean getCanDelete(Object rep, Object ter)
	{
		return(getCanDelete((String)rep,(String)ter));
	}

	public boolean getCanEdit(Object rep, Object ter)
	{
		return(getCanEdit((String)rep,(String)ter));
	}

	public boolean getCanEditAssigned(Object rep, Object ter)
	{
		return(getCanEditAssigned((String)rep,(String)ter));
	}

	public boolean getCanView(Object rep, Object ter)
	{
		return(getCanView((String)rep,(String)ter));
	}

	public boolean getCanViewAssigned(Object rep, Object ter)
	{
		return(getCanViewAssigned((String)rep,(String)ter));
	}


	public void loadGroups()
	{
		getUser().loadGroups();
	}

	public void loadGroups(Connection con)
	{
		getUser().loadGroups(con);
	}

	/*
	public void loadGroups(Connection con)
	{
		groups = new RowArrayBean();
		groups.setViewSpec(usergroupview);
		if (con != null) {
		   groups.setConnection(con);
		}
		else {
		   //groups.setJndiName(getJndiName());
		}
		groups.setLocale(getLocale());
		groups.setTimeZone(getTimeZone());	
		groups.setColumn(userid,getString(userid));
		groups.sortBy(sectioncode,0);
		groups.getResults();
		groups.close();
	}
	 */
	public String getFirstGroupID()
	{
		return(getGroup(0).getString(groupID));
	}

	public String getFirstPermissionID()
	{
		return(getGroup(0).getString(permissionID));
	}	

	public boolean hasSingleGroup()
	{
		return(getGroupSize()==1);
	}

	public void printGroupOptions(JspWriter out, String current)
	{
		TableData group = null;
		String cgroupid = null;
		try
		{
			for(int i=0; i<getGroupSize(); i++)
			{
				group = getGroup(i);
				cgroupid = group.getString(groupID);
				out.print("<option value=\"");
				out.print(cgroupid);
				out.print("\"");
				if(current!=null && current.equals(cgroupid))
				{
					out.print(" selected");
				}
				out.print(">");
				out.print(group.getString("Name"));
				out.print("");
			}
		}
		catch(Exception e){}
	}

	public boolean hasGroup(String groupid){
		TableData group = null;
		for(int i=0; i<getGroupSize(); i++)
		{
			group = getGroup(i);
			if(groupid.equals(group.getString(groupID))){
				return(true);
			}
		}
		return(false);
	}

	public int getGroupSize()
	{
		return(getUser().getGroups().size());
	}

	public TableData getGroup(int index)
	{
		return(getUser().getGroup(index).getRecord());
	}

	public void clear()
	{
		getUser().setGroups(null);
		user = null;
		super.clear();
	}

	public int getDefaultListSize() {
		try {
			return Integer.parseInt(getString("ListSize"));
		}
		catch (Exception e) {
			return 40;
		}
	}

	public RowArrayBean getGroupSecurity(String groupID) {
		return(getGroupSecurity("GroupID",groupID));
	}

	public RowArrayBean getGroupSecurity(String idname, String id) {
		return(getUser().getGroupSecurity(idname, id));
	}	

	public Map getRowProperties() {
		if (rowProperties == null) {
			buildRowProperties();
		}
		return rowProperties;
	}

	private synchronized void buildRowProperties() {
		if (rowProperties == null) {
			HashMap hashTable = new HashMap();

			RowSetBean propBean = new RowSetBean();
			propBean.setViewSpec("RowPropertyView");
			//propBean.setJndiName(getJndiName());
			propBean.generateSQLStatement();
			propBean.getResults();

			while (propBean.getNext()) {
				hashTable.put(propBean.getColumn("RowPropertyID"),new RowProperty(propBean));
			}

			propBean.close();

			rowProperties = hashTable;
		}
	}

	public boolean getRowPermission(String id, String perm) {
		if (id == null || id.length() == 0 || canAdminRowProperties()) {
			return true;
		}
		else {
			RowProperty prop = (RowProperty)getRowProperties().get(id);
			if (prop == null) {
				return true;
			}
			else {
				return prop.getPermission(perm);
			}
		}
	}

	// RIGHT LETS START AGAIN HERE
	public boolean canAdmin() {
		return getUser().canAdmin();
	}

	public boolean canManage() {
		return getUser().canManage();
	}
	public boolean canUseCMS() {
		return getUser().canUseCMS();
	}
	public boolean canReport() {
		return getUser().canReport();
	}
	public boolean canUseInbox() {
		return getUser().canUseInbox();
	}
	public boolean canUseCalendar() {
		return getUser().canUseCalendar();
	}
	public boolean canAdminRowProperties() {
		return getUser().canAdminRowProperties();
	}

	public boolean canSendEmails() {
		return getUser().canSendEmails();
	}
	public boolean canSendEmailsOffline() {
		return getUser().canSendEmailsOffline();
	}
	public boolean canDeleteSentEmails() {
		return getUser().canDeleteSentEmails();
	}
	public boolean canEditOffline() {
		return getUser().canEditOffline();
	}
	public boolean canExport() {
		return getUser().canExport();
	}


	public boolean canAccess(TableData bean) {
		return canAccess(bean.getTableSpec().getTableName());
	}

	public boolean canAccess(String tableName) {
		return getUser().canAccess(tableName);
	}

	public boolean canAccess(TableData bean, String groupsrelation) {
		return getUser().canAccess(bean, groupsrelation);
	}	

	public boolean canEditUser() {
		return getUser().canEdit("Users", null);
	}
	public boolean canDeleteUser() {
		return getUser().canDelete("Users", null);
	}

	public boolean canCreate(TableData bean) {
		return canCreate(bean.getTableSpec().getTableName());
	}

	public boolean canCreate(String tableName) {
		return getUser().canCreate(tableName);
	}

	public boolean canCreate(TableData bean, String groupsrelation) {
		return getUser().canCreate(bean, groupsrelation);
	}  	

	public boolean canViewAssigned(String tableName, TableData bean) {	   
		return getUser().canViewAssigned(tableName, bean.getString(sectioncode), bean.getString(repuserid));
	}

	public boolean canView(TableData bean) {
		return canView(bean.getTableSpec().getTableName(), bean) || canViewAssigned(bean);
	}

	public boolean canViewAssigned(TableData bean) {
		return canViewAssigned(bean.getTableSpec().getTableName(), bean);
	}

	public boolean canView(String tableName, TableData bean) {	   
		return getUser().canView(tableName, bean.getString(sectioncode));
	}

	public boolean canView(String tableName, String sc) {	   
		return getUser().canView(tableName, sc);
	}

	public boolean canView(String tableName) {	   
		return getUser().canView(tableName, this.getString(sectioncode));
	}

	public boolean canView(TableData bean, String groupsrelation) {
		return getUser().canView(bean, groupsrelation);
	}  	

	public boolean canDelete(TableData bean) {
		return canDelete(bean.getTableSpec().getTableName(), bean);
	}

	public boolean canDelete(String tableName, TableData bean) {
		// return true if can access, can edit & can delete the record. 
		// can edit added so as to restrict delete access in line with edit assigned. 
		return getUser().canDelete(tableName, bean.getString(sectioncode));
	}

	public boolean canDelete(TableData bean, String groupsrelation) {
		return getUser().canDelete(bean, groupsrelation);
	}  	

	/** 
	 * This method does not check contact groups / company groups
	 * TODO - deprecate
	 */
	public boolean canEdit(TableData bean) {
		logger.debug("canEdit({})", bean);
		return canEdit(bean.getTableSpec().getTableName(), bean) || canEditAssigned(bean);
	}

	/**
	 * This method does not check contact groups / company groups 
	 * TODO - deprecate
	 */
	public boolean canEdit(String tableName, TableData bean) {
		logger.debug("canEdit({}, {})",tableName, bean);
		if("Contacts".equals(tableName) || "Companies".equals(tableName)) {
			logger.warn("UserBean.canEdit(tableName, bean) does not check group security");
		}
		return getUser().canEdit(tableName, bean.getString(sectioncode));
	}	

	public boolean canEdit(TableData bean, String groupsrelation) {
		return getUser().canEdit(bean, groupsrelation);
	}  	

	public boolean canEditAssigned(TableData bean) {
		return canEditAssigned(bean.getTableSpec().getTableName(), bean, bean.getString(repuserid));
	}

	public boolean canEditAssigned(TableData bean, String repUserID) {
		return canEditAssigned(bean.getTableSpec().getTableName(), bean, repUserID);
	}

	public boolean canEditAssigned(String tableName, TableData bean, String repUserID) {
		return getUser().canEditAssigned(tableName, bean.getString(sectioncode), repUserID);
	}	

	/**
	 * Useful when looping through linked group records which contain a rep, there is a method 
	 * similar to this for canViewAssigned.
	 * @return
	 */
	public boolean canEditAssigned(String tableName, TableData bean) {
		return getUser().canEditAssigned(tableName, bean.getString(sectioncode), bean.getString(repuserid));
	}	

	public boolean canEditAssigned(TableData bean, String groupsrelation, String repUserID) {
		return getUser().canEditAssigned(bean, groupsrelation, repUserID);
	}  	

	public boolean canMerge(TableData bean) {
		return canMerge(bean.getTableSpec().getTableName(), bean);
	}

	public boolean canMergeRep(TableData bean, String repUserID) {
		return canMerge(bean.getTableSpec().getTableName(), bean, repUserID);
	}

	/**
	 * canMerge method returns true if canEdit && canMerge are true for the bean
	 * @param tableName String Name of the table
	 * @param bean TableData the bean in question
	 */
	public boolean canMerge(String tableName, TableData bean) {
		return getUser().canMerge(tableName, bean.getString(sectioncode));
	}

	public boolean canMerge(String tableName, TableData bean, String repUserID) {
		return getUser().canMerge(tableName, bean.getString(sectioncode), repUserID);
	}

	public boolean canMerge(TableData bean, String groupsrelation) {
		return getUser().canMerge(bean, groupsrelation);
	}	

	public boolean canMerge(TableData bean, String groupsrelation, String repUserID) {
		return getUser().canMerge(bean, groupsrelation, repUserID);
	}	

	public boolean canAdminStatus(TableData bean) {
		return canAdminStatus(bean.getTableSpec().getTableName(), bean);
	}

	public boolean canAdminStatus(String tableName, TableData bean) {
		return getUser().canAdminStatus(tableName, bean.getString(sectioncode));
	}	

	public boolean canAdminStatus(TableData bean, String groupsrelation) {
		return getUser().canAdminStatus(bean, groupsrelation);
	}  	

	public boolean canEditRep(TableData bean) {
		return canEditRep(bean.getTableSpec().getTableName(), bean);
	}

	public boolean canEditRep(String tableName, TableData bean) {
		return(getUser().canEditRep(tableName, bean.getString(sectioncode)));
	}

	public boolean canEditRep(TableData bean, String groupsrelation) {
		return getUser().canEditRep(bean, groupsrelation);
	} 	

	public boolean canEditRep(TableData bean, String groupsrelation, String repUserID) {
		return getUser().canEditRep(bean, groupsrelation, repUserID);
	} 	

	///
	
	public boolean canEditRepOnly(TableData bean) {
		return canEditRepOnly(bean.getTableSpec().getTableName(), bean);
	}

	public boolean canEditRepOnly(String tableName, TableData bean) {
		return(getUser().canEditRepOnly(tableName, bean.getString(sectioncode)));
	}

	public boolean canEditRepOnly(TableData bean, String groupsrelation) {
		return getUser().canEditRepOnly(bean, groupsrelation);
	} 	

	public boolean canEditRepOnly(TableData bean, String groupsrelation, String repUserID) {
		return getUser().canEditRepOnly(bean, groupsrelation, repUserID);
	} 	
	
	public boolean canLink(TableData bean) {
		return canLink(bean.getTableSpec().getTableName(), bean);
	}

	public boolean canLink(String tableName, TableData bean) {
		return getUser().canLink(tableName, bean.getString(sectioncode));
	}

	public boolean canLink(TableData bean, String groupsrelation) {
		return getUser().canLink(bean, groupsrelation);
	}  	
	
	public boolean canLinkAssigned(TableData bean, String groupsrelation, String repUserID) {
		return getUser().canLinkAssigned(bean, groupsrelation, repUserID);
	}  	
	
	//for modules with a module name that is the tablename
	public boolean hasModulePermission(String permission, TableData data) {
		return(getUser().hasModulePermission(permission, data));
	}    

	//for modules with a module name that is not the tablename
	public boolean hasModulePermission(String modulename, String permission, TableData data) {
		return(getUser().hasModulePermission(modulename, permission, data));
	}    

	//for modules access
	public boolean hasModulePermission(String modulename, String permission) {
		return(getUser().hasModulePermission(modulename, permission));
	}     

	//for modules that cannot have multiple groups assigned
	public boolean hasModulePermission(String modulename, String permission, String sectioncode) {
		return(getUser().hasModulePermission(modulename, permission, sectioncode));
	}   

	//for modules that can have multiple groups assigned, grouprelation is the relationship to assigned groups
	public boolean hasModulePermission(String modulename, String permission, String grouprelation, TableData data) {
		return(getUser().hasModulePermission(modulename, permission, grouprelation, data));
	}   

	public User getUser() {
		if (user == null || !user.isLoaded()) {
			loadUser();
		}
		user.setField(ipaddress, currentIP);
		return user;
	}

	protected synchronized void loadUser() {
		if (user == null) {
			user = new User((Connection)null);
		}
		if (!user.isLoaded()) {
			user.load(getUserID());
		}
	}

	private boolean emailsyncrunning = false;

	synchronized boolean setEmailSyncLock(boolean lockstate){
		if(!emailsyncrunning && lockstate){
			emailsyncrunning = lockstate;
			return(true);
		}else if(!lockstate && emailsyncrunning){
			emailsyncrunning = lockstate;
			return(true);
		}
		return(false);
	}

	public boolean isEmailSyncRunning(){
		return(emailsyncrunning);
	}	

	private boolean apptsyncrunning = false;

	synchronized boolean setApptSyncLock(boolean lockstate){
		if(!apptsyncrunning && lockstate){
			apptsyncrunning = lockstate;
			return(true);
		}else if(!lockstate && apptsyncrunning){
			apptsyncrunning = lockstate;
			return(true);
		}
		return(false);
	}	

	public boolean isApptSyncRunning(){
		return(apptsyncrunning);
	}

	/*
	 * 'EmailHost', 'varchar(50)', 'YES', '', '', ''
	 * 'EmailUsername', 'varchar(50)', 'YES', '', '', ''
	 * 'EmailPassword', 'varchar(50)', 'YES', '', '', ''
	 */
	public ExchangeConnect getExchangeConnect() {
		if (exchange == null && "exchange".equals(getColumn("EmailProtocol")) && "true".equals(InitServlet.getSystemParams().getProperty("useExchange"))) {
			String host = getColumn("EmailHost");
			if (InitServlet.getSystemParams().getProperty("useExchangeHost") != null && InitServlet.getSystemParams().getProperty("useExchangeHost").length() > 0) host = InitServlet.getSystemParams().getProperty("useExchangeHost");
			String username = getColumn("EmailUsername");
			String password = getColumn("EmailPassword");
			String domain = "";
			if (host.length() > 0 && username.length() > 0 && password.length() > 0) {
				username = RunwayUtil.decodeKey(getUserID(), "Exchange2015",username);
				if (username.indexOf("/") > 0) {
					domain = username.substring(0, username.indexOf("/"));
					username = username.substring(username.indexOf("/") + 1);
					
					exchange = new ExchangeConnect(host, username, RunwayUtil.decodeKey(getUserID(), "Exchange2015", password), domain);
				} else if (username.indexOf("\\") > 0) {
					domain = username.substring(0, username.indexOf("\\"));
					username = username.substring(username.indexOf("\\") + 1);
					exchange = new ExchangeConnect(host, username, RunwayUtil.decodeKey(getUserID(), "Exchange2015", password), domain);
				} else {
					exchange = new ExchangeConnect(host, RunwayUtil.decodeKey(getUserID(), "Exchange2015",username), RunwayUtil.decodeKey(getUserID(), "Exchange2015", password));
				}
				exchange.setServerTimeZone(getServerTimeZone());
				exchange.setUserTimeZone(getUserTimeZone());
			}
		}
		if (exchange != null && exchange.getError().length() > 0) {
			this.setParameter("ExchangeMessage", exchange.getError());

		}

		return (exchange != null && exchange.isConnected())? exchange : null;
	}

	private String getServerTimeZone() {
		Properties sysprops = InitServlet.getSystemParams();

		String zone = sysprops.getProperty("ExchangeServerTimeZone");
		if (zone == null || zone.length() == 0) zone = "Australia/Melbourne";

		return zone;
	}

	public ExchangeConnect.RunwayExchangeEmail readEmail(String exchangeUID) {
		ExchangeConnect.RunwayExchangeEmail ree = null;
		ExchangeConnect ec = getExchangeConnect();
		if (ec != null) {
			ree = ec.readEmail(exchangeUID);
		}

		return ree;
	}

	public void syncAppointments(boolean thread) {
		syncAppointments(thread, false);
	}

	public void syncAppointments(boolean thread, boolean islogin) {
		if (getExchangeConnect() != null) {
			if(setApptSyncLock(true))
			{
				Thread checkApp = new syncAppointmentsThread(getUserID(), islogin);
				if (thread)
					ThreadManager.startThread(checkApp);
				//checkApp.start();
				else
					checkApp.run();
			}
			else
			{
				logger.warn("Exchange Appointment Sync Locked for {} date {}", new String[]{getUserName(), (new Date()).toString()});
			}
		}
	}

	public class syncAppointmentsThread extends Thread{
		private String				userID = null;
		boolean 					onlogin = false;
		//private boolean				killMail = false; 

		public syncAppointmentsThread(String userID, boolean onlogin) {
			this.userID = userID;
			this.onlogin = onlogin;
		}

		protected String getEmailSyncDateStringFromDB(Connection con){
			GenRow row = new GenRow();
			row.setConnection(con);
			row.setViewSpec("UserView");
			row.setParameter("UserID", getUserID());
			row.doAction("select");
			return(row.getData("EmailSyncDate","dd/MM/yyyy"));
		}

		protected Date getEmailSyncDateFromDB(Connection con){
			GenRow row = new GenRow();
			row.setConnection(con);
			row.setViewSpec("UserView");
			row.setParameter("UserID", getUserID());
			row.doAction("select");
			return(row.getDate("EmailSyncDate"));
		}		

		protected Vector<RunwayExchangeAppointmentAttendee> getAttendees(String noteid, Connection con){
			Vector<RunwayExchangeAppointmentAttendee>   attendees = new Vector<RunwayExchangeAppointmentAttendee>();

			if(noteid!=null && noteid.length()!=0){
				GenRow attend = new GenRow();
				attend.setConnection(con);
				attend.setViewSpec("exchange/AttendeeView");
				attend.setParameter("-select1", "AttendeeID");
				attend.setParameter("NoteID", noteid);
				attend.doAction("search");
				attend.getResults();
				while(attend.getNext()){
					if(attend.getData("UserEmail").length()!=0){
						attendees.add(new RunwayExchangeAppointmentAttendee(attend.getData("UserEmail"), attend.getData("Status")));
					}
					else if(attend.getData("ContactEmail").length()!=0){
						attendees.add(new RunwayExchangeAppointmentAttendee(attend.getData("UserEmail"), attend.getData("Status")));
					}
				}
			}
			return attendees;
		}

		public void run() {
			logSession("Exchange Appt Sync Start");
			GenRow connectionrow = new GenRow();  
			try {


				//cannot use request db connection here since both request and connection objects are pooled
				//and this runs in its own thread, so get our own connection to reuse.

				ActionBean.connect(connectionrow);		   

				HashMap<String,String> appUIDs = new HashMap<String,String>();
				GenRow appointments = new GenRow();
				appointments.setConnection(connectionrow.getConnection());
				appointments.setViewSpec("exchange/NoteView");
				appointments.setColumn("RepUserID",userID);
				appointments.setColumn("Type", "Appointment");
				String lastsyncdate = getEmailSyncDateStringFromDB(connectionrow.getConnection());
				if(lastsyncdate!=null && lastsyncdate.length()!=0){
					appointments.setColumn("ModifiedDate|1", ">="+lastsyncdate);
				}
				appointments.setColumn("ExchangeUID^2|1","!NULL;!EMPTY");
				appointments.setColumn("NoteDate^2|1", "LAST14DAYS+TODAY+NEXT14DAYS");
				
				appointments.setColumn("ExchangeUID^3|1","NULL+EMPTY");
				appointments.setColumn("CreatedDate^3|1", "LAST14DAYS+TODAY");
				
				appointments.doAction(GenerationKeys.SEARCH);

				logger.error("Runway Sync Appointment Statement " + appointments.getStatement());

				appointments.getResults(true);
				//setColumn("ExchangeMessage", "Runway Sync Appointment " + appointments.getSize());
				logSession("Exchange Runway Appt " + appointments.getSize());
				logger.error("Runway Sync Appointment for " + getUserName() + " from " + lastsyncdate + " " + getColumn("EmailSyncDate") + " with " + String.valueOf(appointments.getSize()));
				
				try {
					GenRow note = new GenRow();
					note.setConnection(connectionrow.getConnection());
					String special = "";
					while (appointments.getNext()) {
						if (getExchangeConnect() != null) {
							if (appointments.getString("NoteID").equals(special)) {
								System.out.println("special");
							}
							Object obj = getExchangeConnect().syncAppointment(appointments, getAttendees(appointments.getString("NoteID"), connectionrow.getConnection()));
							if (obj == null) {
								note.clear();
								note.setTableSpec("Notes");
								note.setColumn("NoteID", appointments.getString("NoteID"));
								note.doAction(GenerationKeys.DELETE);
							} else if (obj != null && obj instanceof String) {
								if (!appointments.getString("NoteID").equals((String) obj)) {
									note.clear();
									note.setTableSpec("Notes");
									note.setColumn("NoteID", appointments.getString("NoteID"));
									getExchangeConnect().makeUID(note, (String) obj);
									note.doAction(GenerationKeys.UPDATE);
									appUIDs.put((String) obj, appointments.getString("NoteID"));
								}
							} else if (obj != null && obj instanceof GenRow) {
								GenRow row = (GenRow) obj;
								note.clear();
								note.setTableSpec("Notes");						
								note.setColumn("NoteID", row.getString("NoteID"));
								note.setParameter("ExchangeAppointmentUID", row.getString("ExchangeAppointmentUID"));
								note.setParameter("ExchangeUID", row.getString("ExchangeUID"));
								note.setParameter("ExchangeInstance", row.getString("ExchangeInstance"));
								note.doAction(GenerationKeys.UPDATE);
							} else if (obj != null && obj instanceof RunwayExchangeAppointment) {
								RunwayExchangeAppointment app = (RunwayExchangeAppointment) obj;
								createOrUpdateNote(appointments.getString("ContactID"), appointments.getString("NoteID"), app, connectionrow.getConnection());
								appUIDs.put(app.getUid(), appointments.getString("NoteID"));
							}
						}
						if (killMail) return;
					}
				} catch (Exception e){
					logger.error("Exchange.syncAppointmentsThread " + e.getClass().getCanonicalName() + " " + e.getMessage() + " " + e.getCause());
					e.printStackTrace();
				} finally {
					appointments.close();
				}


				try {

					if (getExchangeConnect() != null) {
						//syncAppointment wont sync any appts where uid is already in appUIDs, so clear it for incoming updates.
						Vector<ExchangeConnect.RunwayExchangeAppointment> apps = getExchangeConnect().syncAppointment(appUIDs, getEmailSyncDateFromDB(connectionrow.getConnection()), this);
						if (apps != null) {
							//setColumn("ExchangeMessage", "Exchnage Sync Appointment " + apps.size());
							logSession("Exchange Appt " + apps.size());
							logger.error("Exchange Sync Appointment for " + getUserName() + " with " +String.valueOf(apps.size()));
							for(ExchangeConnect.RunwayExchangeAppointment app : apps)
							{
								//System.out.println("Appointment Subject: " + app.getSubject());
								//System.out.println("Appointment Duration: " + app.getDuration());

								String noteid = "";
								noteid = createOrUpdateNote( "", noteid, app, connectionrow.getConnection());

								Vector<RunwayExchangeAppointmentAttendee> attendees = app.getAttendees();
								for(int i=0; i<attendees.size(); i++){
									//System.out.println("Appointment for " + attendees.get(i).toString());
									setAttendee(noteid, attendees.get(i).getEmail(), attendees.get(i).getResponse(), connectionrow.getConnection());
								}
							}
						}
						else{
							logger.error("Exchange Sync Appointment for " + getUserName() + " with null");
						}

						if (killMail) return;
					}
				} catch (Exception e){
					logger.error("Exchange.syncAppointmentsThread " + e.getClass().getCanonicalName() + " " + e.getMessage() + " " + e.getCause());
					e.printStackTrace();
				}


			}
			catch(Exception e){
				logger.error("Exchange.syncAppointmentsThread " + e.getClass().getCanonicalName() + " " + e.getMessage() + " " + e.getCause());
				e.printStackTrace();
			}
			finally{
				connectionrow.close();
				setApptSyncLock(false);
			}
			logSession("Exchange Appt Sync End");
		}

		public boolean processAppointment(ExchangeConnect.RunwayExchangeAppointment app) {
			GenRow connectionrow = new GenRow();  

			String noteid = "";
			noteid = createOrUpdateNote( "", noteid, app, connectionrow.getConnection());

			Vector<RunwayExchangeAppointmentAttendee> attendees = app.getAttendees();
			for(int i=0; i<attendees.size(); i++){
				//System.out.println("Appointment for " + attendees.get(i).toString());
				setAttendee(noteid, attendees.get(i).getEmail(), attendees.get(i).getResponse(), connectionrow.getConnection());
			}
			connectionrow.close();
			
			return noteid != null && noteid.length() > 0;
		}


		protected String findAttendee(String email, Connection con){
			if(email!=null && email.length()!=0){
				GenRow contact = new GenRow();
				contact.setConnection(con);
				contact.setTableSpec("Contacts");
				contact.setParameter("-select1", "ContactID");
				contact.setParameter("Email", "%"+email+"%");
				contact.doAction("selectfirst");
				if(contact.isSuccessful()){
					return(contact.getData("ContactID"));
				}
			}
			return(null);
		}		

		protected void setAttendee(String noteid, String email, String response, Connection con){
			String contactid = findAttendee(email, con);
			if(contactid!=null && contactid.length() > 0){
				GenRow attend = new GenRow();
				attend.setConnection(con);
				attend.setTableSpec("Attendees");
				attend.setParameter("-select1", "AttendeeID");
				attend.setParameter("NoteID", noteid);
				attend.setParameter("ContactID", contactid);
				attend.doAction("selectfirst");

				if(!attend.isSuccessful()){
					attend.setParameter("Status", response);
					attend.setConnection(con);
					attend.createNewID();
					attend.doAction("insert");
				}
				else{
					if(response!=null && response.length()!=0){
						attend.setParameter("Status", response);
					}
					attend.setConnection(con);
					attend.setParameter("AttendeeID", attend.getData("AttendeeID"));
					attend.doAction("update");
				}
			}
		}

		private String createOrUpdateNote(String contactID, String noteID, RunwayExchangeAppointment appt, Connection con) {
			GenRow note = new GenRow();
			note.setConnection(con);
			Date modified = new Date();

			// lets see if we can find a match in the system.
			if (noteID == null || noteID.length() == 0) {
				note.setViewSpec("exchange/NoteModifiedView");
				note.setParameter("ExchangeAppointmentUID", StringUtil.urlEncode(appt.getExchangeItemID()));
				note.setParameter("ExchangeUID", StringUtil.urlEncode(appt.getExchangeUserID()));
				note.setParameter("ExchangeInstance", StringUtil.urlEncode(appt.getExchangeInstanceID()));
				if(appt.getUid() != null && appt.getUid().length() != 0){
					note.doAction("selectfirst");
					if (note.isSuccessful() && note.isSet("NoteID")) {
						noteID = note.getString("NoteID");
						modified = note.getDate("ModifiedDate");
					} else {
						note.clear();
						note.setViewSpec("exchange/NoteModifiedView");
						note.setParameter("ExchangeUID", StringUtil.urlEncode(appt.getUid()));
						note.doAction("selectfirst");
						if (note.isSuccessful() && note.isSet("NoteID")) {
							noteID = note.getString("NoteID");
							modified = note.getDate("ModifiedDate");
						} else {
							note.clear();
							note.setViewSpec("exchange/NoteModifiedView");
							note.setParameter("ExchangeUID", appt.getUid());
							note.doAction("selectfirst");
							if (note.isSuccessful() && note.isSet("NoteID")) {
								noteID = note.getString("NoteID");
								modified = note.getDate("ModifiedDate");
							}
						}
					}
				}
			} else {
				note.setViewSpec("exchange/NoteModifiedView");
				note.setParameter("NoteID", noteID);
				// this if statement is redundant
				if(appt.getUid() != null && appt.getUid().length() != 0){
					note.doAction("selectfirst");
					if (note.isSuccessful() && note.isSet("NoteID")) {
						noteID = note.getString("NoteID");
						modified = note.getDate("ModifiedDate");
					}
				}
				
			}
			// if we still don't have one, then we need a new one
			//if (noteID == null | noteID.length() == 0) {
			//	noteID = KeyMaker.generate();
			//}

			note.clear();
			note.setViewSpec("NoteView");
			note.setParameter("ExchangeAppointmentUID", StringUtil.urlEncode(appt.getExchangeItemID()));
			note.setParameter("ExchangeUID", StringUtil.urlEncode(appt.getExchangeUserID()));
			note.setParameter("ExchangeInstance", StringUtil.urlEncode(appt.getExchangeInstanceID()));

			note.setParameter("NoteDate",appt.getDate());
			note.setParameter("NoteTime",appt.getDate());
			note.setParameter("EndDate",appt.getEndDate());

			//appt.getDuration() returns PT30M for half hour, PT1H for one hour, PT1H30M for one hour and thirty minutes, etc
			//we could parse this but might as well use appt.getEndDate()
			note.setParameter("Duration", ExchangeConnect.getDuration(appt.getDate(), appt.getEndDate()));
			note.setColumn("Venue", appt.getLocation());
			note.setColumn("Type","Appointment");

			String body = appt.getBody();
			if(body!=null) {
				body = StringUtil.stripHtml(body.replaceAll("'", "&quote;"));
			}
			note.setColumn("Body", body);
			note.setColumn("GroupID", getString("DefaultGroupID"));
			if (contactID != null && contactID.length() > 0) note.setColumn("ContactID", contactID);

			String subject = appt.getSubject();
			if (subject != null) {
				if (subject.length() >= 150) {
					subject = subject.substring(0, 140).replaceAll("'", "&quote;") + "...";
				} else {
					subject = subject.replaceAll("'", "&quote;");
				}
			}
			subject = subject.replaceAll("'", "&quote;");
			note.setColumn("Subject", subject);

			try {
				if(note.isSuccessful() || note.getData("NoteID").length() > 0 || (noteID != null && noteID.length() > 0)){
					if(modified!= null && modified.before(appt.getModifiedDate())){
						note.setParameter("NoteID", noteID);
						note.setParameter("ModifiedDate", appt.getModifiedDate());
						note.doAction(GenerationKeys.UPDATE);
						logger.error("Exchange Appointment updated id {} subject {} for {} date {}", new String[]{note.getString("NoteID"), appt.getSubject(), getUserName(), appt.getDate().toString()});
					} else {
						logger.error("Exchange Appointment not changed id {} subject {} for {} date {}", new String[]{noteID, appt.getSubject(), getUserName(), appt.getDate().toString()});
					}
				} else /* if(!"NoResponseReceived".equals(appt.getMyResponsetype()) && !"Decline".equals(appt.getMyResponsetype())) */ {
					//don't insert invites, wait till a response is set
					note.setColumn("NoteTypeDescription","Exchange");
					note.setColumn("RepUserID", getUserID());
					note.setColumn("NoteID",KeyMaker.generate());
					note.setParameter("ModifiedDate", appt.getModifiedDate());
					note.setColumn("ModifiedBy", getUserID());
					note.setColumn("CreatedBy", getUserID());
					note.setColumn("CreatedDate", "NOW");
	
					note.doAction(GenerationKeys.INSERT);
					logger.error("Exchange Appointment added id {} subject {} for {} date {}", new String[]{note.getString("NoteID"), appt.getSubject(), getUserName(), appt.getDate().toString()});
				//} else {
				//	return "";
				}
			} catch (Exception e) {
				logger.error("Exchange Appointment error uid {} message {} data {}", new String[]{appt.getUid(), e.getMessage(), note.toString()});			
			}

			if (note.isSuccessful()){
				noteID = note.getColumn("NoteID");
			}

			return noteID;
		}

		/*
		private String createOrUpdateNote(String contactID, String noteID, RunwayExchangeAppointment appt, Connection con) {

			//skip appointment invites
			//if(body==null || body.length()==0 || body.indexOf(".EmailQuote {")<0){
			if(appt.getAppointmentid()!=null && appt.getAppointmentid().length()!=0){
				String subject = appt.getSubject();
				if (subject.length() >= 150){
					subject = subject.substring(0, 145) + "...";
				}

				GenRow note = new GenRow();
				note.setConnection(con);
				note.setViewSpec("exchange/NoteModifiedView");
				note.setParameter("ExchangeAppointmentUID^1|1", appt.getExchangeItemID());
				note.setParameter("ExchangeUID^1|1", appt.getExchangeUserID());
				note.setParameter("ExchangeInstance^1|1", appt.getExchangeInstanceID());
				note.setParameter("ExchangeUID|1", appt.getUid());
				// this if statement is redundant
				if(appt.getUid() != null && appt.getUid().length() != 0){
					note.doAction("selectfirst");

					if(!note.isSuccessful() && appt.getUid()!=null && appt.getUid().length()!=0){
						note.setConnection(con);
						note.remove("ExchangeAppointmentUID");
						note.setParameter("ExchangeUID", appt.getUid());
						//note.setParameter("ExchangeAppointmentUID", appt.getAppointmentid());
						note.doAction("selectfirst");
					} 

					noteID = note.getString("NoteID");

					note.setConnection(con);
					note.setParameter("NoteDate",appt.getDate());
					note.setParameter("NoteTime",appt.getDate());
					note.setParameter("EndDate",appt.getEndDate());
					//note.setParameter("ExchangeAppointmentUID",appt.getAppointmentid());

					//appt.getDuration() returns PT30M for half hour, PT1H for one hour, PT1H30M for one hour and thirty minutes, etc
					//we could parse this but might as well use appt.getEndDate()
					note.setParameter("Duration", ExchangeConnect.getDuration(appt.getDate(), appt.getEndDate()));
					note.setColumn("Venue", appt.getLocation());
					note.setColumn("Type","Appointment");
					note.setColumn("Subject", subject);

					if(body!=null){
						body = StringUtil.stripHtml(body);
					}
					note.setColumn("Body", body);
					note.setColumn("GroupID", getString("DefaultGroupID"));
					note.setColumn("ContactID", contactID);

					if(note.isSuccessful() || note.getData("NoteID").length() > 0){
						Date modified = note.getDate("ModifiedDate");
						if(modified!= null && modified.before(appt.getModifiedDate())){
							note.setParameter("NoteID", note.getData("NoteID"));
							note.doAction(GenerationKeys.UPDATE);
						}
					}
					else if(!"NoResponseReceived".equals(appt.getMyResponsetype()) && !"Decline".equals(appt.getMyResponsetype())){
						//don't insert invites, wait till a response is set
						note.setColumn("NoteTypeDescription","Exchange");
						note.setColumn("RepUserID", getUserID());
						note.setColumn("NoteID",noteID);
						note.setColumn("ModifiedDate", "NOW");
						note.setColumn("ModifiedBy", getUserID());
						note.setColumn("CreatedBy", getUserID());
						note.setColumn("CreatedDate", "NOW");

						System.out.println(appt.getSubject()+": responserequired" + appt.isResponseRequired() + " ,myresponse: "+ appt.getMyResponsetype());
						note.doAction(GenerationKeys.INSERT);
					}

					if (note.isSuccessful()){
						noteID = note.getColumn("NoteID");
					}
				}
			}
			return noteID;
		}*/

	}

	public void syncMail(boolean thread) {
		if(setEmailSyncLock(true)){
			Thread checkApp = new syncMailThread(getColumn("EmailSyncDate"));
			if (thread)
				ThreadManager.startThread(checkApp); 
			//checkApp.start();
			else
				checkApp.run();
		}
		else
		{
			logger.error("Exchange Email Sync locked");
		}
	}

	public class syncMailThread extends Thread {
		private String					lastLogin = null;
		private HashMap<String,String> 	usersEmails = new HashMap<String,String>();
		private HashMap<String,String>	currentEmails = new HashMap<String,String>();

		//private boolean					killMail = false;


		public syncMailThread(String lastLogin) {
			this.lastLogin = lastLogin;

			GenRow email = new GenRow();
			ActionBean.connect(email); 
			email.setTableSpec("Exchange");
			email.setParameter("-select0", "ExchangeUID");
			email.setParameter("-select1", "NoteID");
			email.setParameter("UserID", getUserID());
			email.doAction(GenerationKeys.SEARCH);

			email.getResults();

			while (email.getNext()) {
				currentEmails.put(email.getString("ExchangeUID"), email.getString("NoteID"));
			}

			email.close();
		}

		public void run() {
			logSession("Exchange Mail Sync Start");
			GenRow connectionrow = new GenRow();  
			try {
				ActionBean.connect(connectionrow);     

				GenRow users = new GenRow();
				users.setConnection(connectionrow.getConnection());
				users.setTableSpec("Users");
				users.setParameter("-select", "Email");
				users.doAction(GenerationKeys.SEARCH);
				users.getResults();

				while (users.getNext()) {
					String em = users.getColumn("Email").toLowerCase();
					if (em.lastIndexOf("@") > 0 && em.lastIndexOf(" ",em.lastIndexOf("@")) > 0) {
						em = em.substring(0,em.lastIndexOf(" ",em.lastIndexOf("@")));
					}
					if (em.lastIndexOf(" ") > 0) {
						em = em.substring(em.lastIndexOf(" "));
					}
					em = em.replaceAll("[\\<\\[\\(\\>\\]\\)]", "");
					usersEmails.put(em.toLowerCase(), "Yes");
				}
				users.close();
				
				// get the system parameter and seperate them out into indevidual lines in usersEmails

				if (getExchangeConnect() != null) {
					Vector<ExchangeConnect.RunwayExchangeEmail> emails = getExchangeConnect().syncMailInbox(getColumn("EmailSyncDate"));
					//("ExchangeMessage", "Exchnage Sync InBox " + emails.size());
					logSession("Exchange InBox " + emails.size());
					logger.error("Exchange Sync Inbox for " + getUserName() + " with " + emails.size() + " emails");
					for(ExchangeConnect.RunwayExchangeEmail email : emails)
					{
						String emailID = checkEmail(email.getUid(), connectionrow.getConnection());

						if (emailID == null) {
							createEmail(email, connectionrow.getConnection());
						}
						if (killMail) return;
					}
					emails = getExchangeConnect().syncMailOutbox(getColumn("EmailSyncDate"));
					//setColumn("ExchangeMessage", "Exchnage Sync OutBox " + emails.size());
					logSession("Exchange OutBox " + emails.size());
					logger.error("Exchange Sync Outbox for " + getUserName() + " with " + emails.size() + " emails");
					for(ExchangeConnect.RunwayExchangeEmail email : emails)
					{
						String emailID = checkEmail(email.getUid(), connectionrow.getConnection());

						if (emailID == null) {
							createEmail(email, connectionrow.getConnection());
						}
						if (killMail) return;
					}

					GenRow user = new GenRow();
					user.setConnection(connectionrow.getConnection());
					user.setTableSpec("Users");
					user.setColumn("UserID", getUserID());
					user.setColumn("EmailSyncDate", "NOW");
					user.doAction(GenerationKeys.UPDATE);
				}

			} catch (Exception e){
				logger.error("Exchange.syncMailThread " + e.getClass().getCanonicalName() + " " + e.getMessage() + " " + e.getCause());
				e.printStackTrace();
			}
			finally{
				connectionrow.close();
				setEmailSyncLock(false);
			}
			logSession("Exchange Mail Sync End");
		}

		public String checkEmail(String uid, Connection con) {

			String emailID = null;
			GenRow email = new GenRow();
			//email.setConnection(con);
			email.setTableSpec("Exchange");
			email.setParameter("-select0", "EmailDate");
			email.setParameter("-select1", "NoteID");
			email.setParameter("-select2", "ExchangeUID");
			email.setParameter("ExchangeUID", StringUtil.urlEncode(uid));
			try {
				email.doAction(GenerationKeys.SEARCH);
				
				email.getResults();
				
				while (email.getNext()) {
					if (email.getData("ExchangeUID").equals(StringUtil.urlEncode(uid))) {
						if (email.getString("NoteID").length() > 0) emailID = email.getString("NoteID");
						break;
					}
				}
			} finally {
				email.close();
			}

			logger.trace("Exchange check email EmailID {} search is {}",new String[]{emailID, email.getStatement()});

			return emailID;
		}

		public String[] emailOnly(String email) {
			if (email == null || email.length() == 0) return null;
			String[] parts = email.split("[;,]");
			for (int p = 0; p < parts.length; ++p) {
				String em = parts[p].toLowerCase();
				if (em.lastIndexOf("@") > 0 && em.lastIndexOf(" ",em.lastIndexOf("@")) > 0) {
					em = em.substring(0,em.lastIndexOf(" ",em.lastIndexOf("@")));
				}
				if (em.lastIndexOf(" ") > 0) {
					em = em.substring(em.lastIndexOf(" "));
				}
				parts[p] = em.replaceAll("[\\<\\[\\(\\>\\]\\)]", "");

			}

			return parts;
		}

		public String createEmail(ExchangeConnect.RunwayExchangeEmail email, Connection con) {
			String noteID = "";

			// see if there is a contact that matches the email address
			boolean hasEmail = false;
			GenRow contact = new GenRow();
			contact.setConnection(con);
			contact.setTableSpec("Contacts");
			contact.setParameter("-select", "*");
			// this is fix to allow for none email address and multiple addresses
			String[] fromEmail = emailOnly(email.getFrom());
			StringBuilder emailStr = new StringBuilder("");
			if (fromEmail != null) {
				for (int p = 0; p < fromEmail.length; ++p) {
					logger.trace("From email is {}" ,fromEmail[p]);
					if(fromEmail[p] != null && fromEmail[p].toLowerCase().startsWith("mailto:")) {
						fromEmail[p] = fromEmail[p].substring(7);
						logger.trace("From email is updated to {}" ,fromEmail[p]);
					}

					// add checks for after the @ symbole as well as full addresses
					if (fromEmail[p] != null && fromEmail[p].indexOf("@") > 0 && !usersEmails.containsKey(fromEmail[p].toLowerCase())) {
						if (emailStr.length() > 0) emailStr.append("+");
						emailStr.append("%").append(fromEmail[p]).append("%");
					}
				}
			}
			// this is fix to allow for none email address and multiple addresses
			String[] toEmail = emailOnly(email.getTo());
			if (toEmail != null) {
				for (int p = 0; p < toEmail.length; ++p) {
					logger.trace("To email is {}" ,toEmail[p]);
					if(toEmail[p] != null && toEmail[p].toLowerCase().startsWith("mailto:")) {
						toEmail[p] = toEmail[p].substring(7);						
						logger.trace("To email is updated to {}" ,toEmail[p]);
					}
					
					if (toEmail[p] != null && toEmail[p].indexOf("@") > 0 && !usersEmails.containsKey(toEmail[p].toLowerCase())) {
						if (emailStr.length() > 0) emailStr.append("+");
						emailStr.append("%").append(toEmail[p]).append("%");
					}
				}
			}

			if (emailStr.length() > 0) {
				contact.addParameter("Email|1", emailStr.toString());
				contact.addParameter("Email2|1", emailStr.toString());
				contact.addParameter("Email3|1", emailStr.toString());
				contact.addParameter("Email4|1", emailStr.toString());
				hasEmail = true;
			}
			String contactID = null;
			if (hasEmail) {
				contact.doAction(GenerationKeys.SEARCH);
				contact.getResults(true);

				if (contact.getSize() > 1) {
					if (email.getName() != null && email.getName().length() > 0) {
						while (contact.getNext()) {
							if (email.getName().indexOf(" ") == -1) {
								if (email.getName().equalsIgnoreCase(contact.getString("FirstName")) || email.getName().equalsIgnoreCase(contact.getString("LastName"))) {
									contactID = contact.getString("ContactID");
									break;
								}
							} else {
								String[] parts = email.getName().split(" ");
								if (parts.length == 2) {
									if (parts[0].equalsIgnoreCase(contact.getString("FirstName")) || parts[1].equalsIgnoreCase(contact.getString("LastName"))) {
										contactID = contact.getString("ContactID");
										break;
									}
									if (parts[1].equalsIgnoreCase(contact.getString("FirstName")) || parts[0].equalsIgnoreCase(contact.getString("LastName"))) {
										contactID = contact.getString("ContactID");
										break;
									}
								} else {
									String firstname = email.getName().substring(0, email.getName().lastIndexOf(" ")).trim();
									String lastname = email.getName().substring(email.getName().indexOf(" ")).trim();
									if (parts[0].equalsIgnoreCase(contact.getString("FirstName")) || lastname.equalsIgnoreCase(contact.getString("LastName"))) {
										contactID = contact.getString("ContactID");
										break;
									} else if (firstname.equalsIgnoreCase(contact.getString("FirstName")) || parts[parts.length - 2].equalsIgnoreCase(contact.getString("LastName"))) {
										contactID = contact.getString("ContactID");
										break;
									}

								}
							}
						}
					}
				} else if (contact.getSize() == 1) {
					contact.getNext();
					contactID = contact.getString("ContactID");
				}
			}

			// added code to add this email to Exchange table in runway

			// we need to explicity check to see if the note is in there, using SQL Unique is not sufficent
			noteID = checkEmail(email.getUid(), con);
			logger.trace("Exchange ContactID is {} NoteID is {} for search {}", new String[]{contactID, noteID, contact.getStatement()});
			if (noteID == null || noteID.length() == 0) {
				if (contactID != null && contactID.length() > 0)  noteID = KeyMaker.generate();
				currentEmails.put(email.getUid(), noteID);

				GenRow row = new GenRow();
				row.setConnection(con);
				row.setTableSpec("Exchange");
				row.createNewID();
				row.setColumn("ExchangeUID", StringUtil.urlEncode(email.getUid()));
				row.setColumn("NoteID", noteID);
				row.setColumn("UserID",getUserID());
				row.setColumn("EmailSubject", email.getSubject());
				row.setColumn("EmailFrom", (email.getFrom() != null)? email.getFrom() : "- unknown -");
				row.setColumn("EmailTo", (email.getTo() != null)? email.getTo() : "- unknown -");
				row.setParameter("EmailDate", email.getDate());
				row.setColumn("Attachments", email.hasAttachment()? "Y" : "N");
				row.setColumn("EmailRead", email.isRead()? "Y" : "N");
				try {
					row.doAction(GenerationKeys.INSERT);
					if (row.isSuccessful() && contactID != null && contactID.length() > 0) {
						createNote(contactID, noteID,email,con);
					} else if (!row.isSuccessful()) {
						logger.error("Exchange Email could not add to Exchange Table " + email.getUid());
					}
				} catch (Exception e) {
					logger.error("Exchange Email could not add to Exchange Table " + email.getUid());
					e.printStackTrace();
				}
				row.close();
			}

			contact.close();

			return noteID;
		}

		private String createNote(String contactID, String noteID, RunwayExchangeEmail email, Connection con) {
			String subject = email.getSubject();
			if (subject.length() >= 150) subject = subject.substring(0, 145) + "...";

			GenRow note = new GenRow();
			note.setTableSpec("Notes");
			note.setColumn("NoteID",noteID);
			note.setColumn("ExchangeUID", StringUtil.urlEncode(email.getUid()));
			note.setColumn("RepUserID", getUserID());
			note.setParameter("NoteDate",email.getDate());
			note.setParameter("NoteTime",email.getDate());
			note.setColumn("Type","Exchange");
			note.setColumn("Subject", subject);
			note.setColumn("Body", email.getBody());
			note.setColumn("GroupID", getString("DefaultGroupID"));

			note.setColumn("CreatedDate", "NOW");
			note.setColumn("ModifiedDate", "NOW");
			note.setColumn("CreatedBy", getUserID());
			note.setColumn("ModifiedBy", getUserID());

			note.setColumn("ContactID", contactID);

			note.doAction(GenerationKeys.INSERT);

			if (note.isSuccessful()) noteID = note.getColumn("NoteID");

			return noteID;
		}
	}

	public void folderCopied(ExchangeFolder folder1, ExchangeFolder folder2)
	{
		// ExchangeFolderListener interface

	}

	public void folderCreated(ExchangeFolder folder)
	{
		// ExchangeFolderListener interface

	}

	public void folderDeleted(String folder)
	{
		// ExchangeFolderListener interface

	}

	public void folderModified(ExchangeFolder folder)
	{
		// ExchangeFolderListener interface
		logger.error("folderModified event received.");
		syncAppointments(true);
	}

	public void folderMoved(ExchangeFolder folder, String oldFolderId)
	{
		// ExchangeFolderListener interface

	}

	public void killExchange() {
		killMail = true;
		logSession("Exchange Stopped");
	}

	/** 
	 * This is a hack to replace an instance of new Date().toString() so don't be shy about changing it's 
	 * format if desired.
	 * @return
	 */
	public String getCurrentDateString() {
		DateFormat df = DateFormat.getInstance();
		df.setTimeZone(getTimeZoneObject());
		return df.format(new Date());
	}

	public boolean isPortalLogin() {
		return isPortalLogin;
	}
}