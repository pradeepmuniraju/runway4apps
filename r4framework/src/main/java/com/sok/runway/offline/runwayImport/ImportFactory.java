package com.sok.runway.offline.runwayImport;

import java.util.*;
import java.io.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.*;
import com.sok.framework.generation.*;
import com.sok.framework.*;
import com.sok.runway.*;

public class ImportFactory
{
   public static final String IMPORTSPEC = "-importspec";
   
   public static ImportConfiguration getImportConfiguration(HttpServletRequest request, ServletContext context)
   {
      GenRow requestrow = new GenRow();
      requestrow.parseRequest(request);
      ImportConfiguration config = getImportConfiguration(requestrow);
      setImportFile(config, request, context);
      return(config);
   }
   
   public static ImportConfiguration getImportConfiguration(GenRow requestrow)
   {
      String configname = (requestrow.getString(IMPORTSPEC));
      return(getImportConfiguration(configname));
   }
   
   public static ImportConfiguration getImportConfiguration(String configname)
   {
      ImportConfiguration config = SpecManager.getImportConfiguration(configname);
      return(config.clone());
   }   
   
   public static UserBean getCurrentUser(HttpServletRequest request)
   {
      return (UserBean)request.getSession().getAttribute(SessionKeys.CURRENT_USER);
   }
   
   public static void setImportFile(ImportConfiguration config, HttpServletRequest request, ServletContext context)
   {
      UploadWrapper multi = null;
      if (!(request instanceof UploadWrapper))
      {
         if(request.getAttribute("MultipartWrapper")!=null)
         {
            multi = (UploadWrapper)request.getAttribute("MultipartWrapper");
         }
      }
      else
      {
         multi = (UploadWrapper) request;
      }
      if(multi!=null)
      {   
         Enumeration files = multi.getFileNames();
         while (files.hasMoreElements())
         {
            String name = (String)files.nextElement();
            String filename = multi.getFilesystemName(name);
            
            File f = multi.getFile(name);
            String newFileName = null;
            if (f != null)
            {
               newFileName = StringUtil.removeNonFileSystemCharacters(f.getName());
               File folder = f.getParentFile().getParentFile();
               UserBean user = getCurrentUser(request);
               File userfolder = new File(folder,user.getCurrentUserID());
               if(!userfolder.exists())
               {
                  userfolder.mkdir();
               }

               File dest = new File(userfolder,newFileName);
               f.renameTo(dest);
               
               String filePath = dest.getAbsolutePath().substring(context.getRealPath("").length());
               if (File.separatorChar != '/')
               {
                  filePath = StringUtil.replace(filePath,File.separatorChar,'/');
               }
               if (filePath.startsWith("/"))
               {
                  filePath = filePath.substring(1);
               }  
               config.setImportFilePath(filePath);
               config.setImportFile(dest);
            }
         }
      }
   }
}
