package com.sok.runway.offline.exchangeSync;

import org.dom4j.Element;

import com.sok.runway.offline.*;

public class ExchangeSyncConfig extends AbstractDaemonWorkerConfig
{
	protected static String fullpath = null;
	protected static ExchangeSyncConfig exchangeconfig = null;
	private static String userID = "";
	protected static final String filepath = "/WEB-INF/exchange.xml";

	public static ExchangeSyncConfig getExchangeSyncConfig(String parentpath)
	{
		if(exchangeconfig == null || exchangeconfig.isModified())
		{
			loadConfig(parentpath);
		}
		return(exchangeconfig);
	}

	protected static synchronized void loadConfig(String parentpath)
	{
		if(exchangeconfig == null || exchangeconfig.isModified())
		{
			if(fullpath == null)
			{
				StringBuffer sb = new StringBuffer(parentpath);
				sb.append(filepath);
				fullpath = sb.toString();
			}
			exchangeconfig = new ExchangeSyncConfig(fullpath);

			Element root = exchangeconfig.document.getRootElement();
			userID  = root.attributeValue("UserID");
		}
	}

	public ExchangeSyncConfig(String path)
	{
		super(path);
	}

	public String getUserID() {
		return userID;
	}

}
