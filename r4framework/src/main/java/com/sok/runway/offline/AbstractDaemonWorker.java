package com.sok.runway.offline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import com.sok.framework.*;
import com.sok.framework.generation.*;

import javax.servlet.*;

public abstract class AbstractDaemonWorker implements TableSource, DaemonWorker
{
	private static final Logger logger = LoggerFactory.getLogger(AbstractDaemonWorker.class);
	protected DaemonWorkerConfig config = null;
	protected String configfilepath = null;
	protected int errorcount = 0;
	protected int currentitem = 0;
	protected int proccessedcount = 0;
	protected int processsize = 0;

	protected StringBuffer message = new StringBuffer();;
	protected StringBuffer productMessage = new StringBuffer();

	protected boolean debug = false;

	protected String systemuserid = "E3B3653517125741C947061350C510E4";   
	protected com.sok.Debugger debugger = null;  


	protected Connection con = null;
	protected String jndiname = null;

	protected WorkerTimer timer = null;

	protected String urlhome = null;

	protected String defaultFrom = "Runway<info@runway.net.au>";

	public AbstractDaemonWorker(String configfilepath)
	{     
		this.configfilepath = configfilepath;
		debugger = com.sok.Debugger.getDebugger();
	}

	protected void error(Exception e)
	{
		error(ActionBean.writeStackTraceToString(e));
	}   

	protected void error(String s)
	{
		message.append(s);
		message.append("\r\n");
		if(debugger.isDebugging()) debugger.debug(this.getClass().getName() + s);
		logger.error(s);
	}

	protected void log(String s)
	{
		try {
			if (debug) {
				message.append(s);
				message.append("\r\n");

				productMessage.append(s);
				productMessage.append("\r\n");
			}
			if (debugger.isDebugging())
				debugger.debug(this.getClass().getName() + s);
			logger.debug(s);
		} catch (Exception e) {
			logger.error("Exception while logging" , e);
		}
	}

	protected void log(String msg, String... args) {
		try {
			if (debug) {
				if (args != null && args.length > 0) {
					for (String s : args) {
						try {
							msg = msg.replaceFirst("\\{\\}*", s);
						} catch (Exception e) {
							// Ignore, its just logging
						}
					}
				}
				message.append(msg);
				message.append("\r\n");

				productMessage.append(msg);
				productMessage.append("\r\n");
			}
			if (debugger.isDebugging())
				debugger.debug(this.getClass().getName() + msg);
			logger.trace(msg, args);
		} catch (Exception e) {
			// Ignore, its just logging
		}
	}

	protected void debug(String s)
	{
		if(debug)
		{
			message.append(s);
			message.append("\r\n");
		}
		if(debugger.isDebugging()) debugger.debug(this.getClass().getName() + s);
		logger.debug(s);
	}

	public void connect()
	{
		ActionBean.connect(this);
	}

	public void setCloseConnection(boolean b)
	{
		if(!b){
			throw new IllegalConfigurationException("Offline proccess should always close connection");
		}
	}

	public boolean getCloseConnection()
	{
		return(true);
	}

	public void setConnection(Object con)
	{
		setConnection((Connection)con);
	}

	public void setConnection(Connection con)
	{
		this.con = con;
	}

	public void setConnection(ServletRequest request)
	{
		throw new IllegalConfigurationException("Cannot use request db connection in an offline proccess");
	}

	public Connection getConnection()
	{
		try {
			if (con == null || con.isClosed()) con = ActionBean.connect();
		} catch (SQLException e) {
		}
		return(con);
	}

	public void close()
	{
		if(con!=null)
		{
			try{
				con.close();
			}catch(Exception e){}

			con = null;
		}
		if(debug)
		{
			emailDebug();
		}
		message = new StringBuffer();
	}   

	public String getJndiName()
	{
		return(jndiname);
	}

	public void setError(String s)
	{
		log(s);
	}

	public abstract void checkConfig();

	protected void init()
	{
		if(currentitem == 0)
		{
			try {
				checkConfig();
			} catch (Exception e) {
				log("Error after check config {}");
				debug("Error after check config : "+  e.toString());
				e.printStackTrace();
			}
			log("Completed checking Config");
			debug("Completed checking Config");
			debug = config.getDebug();
			Properties sysprops = InitServlet.getSystemParams();
			if(sysprops!=null)
			{
				systemuserid = sysprops.getProperty("SystemUserID");
				urlhome = sysprops.getProperty("URLHome");
				defaultFrom = sysprops.getProperty("DefaultEmail");
			}      
			log("About to init timer");
			debug("About to init timer");
			initTimer();
			log("Completed init timer");
			debug("Completed init timer");
		}
		try
		{
			if(con == null || con.isClosed())
			{
				log("About to connect");
				debug("About to connect");
				connect();
				log("completed to connect");
				debug("completed to connect");
			}
		}
		catch(Exception e)
		{
			debug("on init(): "+e.getMessage());
		}
	}   

	public void debugInit(boolean debug) {
		this.debug = debug;
		Properties sysprops = InitServlet.getSystemParams();
		if(sysprops!=null)
		{
			systemuserid = sysprops.getProperty("SystemUserID");
			urlhome = sysprops.getProperty("URLHome");
			defaultFrom = sysprops.getProperty("DefaultEmail");
		}   
		try
		{
			if(con == null || con.isClosed())
			{
				connect();
			}
		}
		catch(Exception e)
		{
			debug("on init(): "+e.getMessage());
		}
	}

	protected void initTimer()
	{
		if(config.hasWorkerTimer())
		{
			timer = config.getWorkerTimer();
		}
		else
		{
			timer = null;
		}
	}

	protected boolean isTime()
	{
		if(timer != null)
		{
			//TODO
		}
		return(true);
	}         

	public abstract String getProcessName();

	protected void emailDebug()
	{
		if (debug) {
			String address = config.getDebugAddress();
			if(address!=null && address.length()!=0)
			{
				ServletConfig sc = InitServlet.getConfig();
				if(sc!=null)
				{
					MailerBean mailer = new MailerBean();
					mailer.setServletContext(sc.getServletContext());
					mailer.setFrom(defaultFrom);
					mailer.setTo(address);
					mailer.setSubject("Debug message from " + getProcessName());
					mailer.setTextbody(message.toString());
					mailer.getResponse();
				}
				else
				{
					System.out.println(message);
				}
			}
		}
	}

	/** 
	 * Provide a public interface to get at the workers messages for manual debugging. 
	 * @return
	 */
	public String getDebugMessage() {
		return message != null ? message.toString() : "null message object";
	}

	public boolean processNext()
	{
		try
		{
			logger.debug("processNext() init()");
			init();
			logger.debug("after processNext() init()");
			if(!isTime()){
				return(false);
			}
			if(currentitem < processsize)
			{
				try
				{           
					logger.debug("Processing index {}", currentitem);
					debug("Processing index " + String.valueOf(currentitem));
					processItem();
					proccessedcount++;
				}
				catch(Exception e)
				{
					resetRunning();
					logger.error("Error processing worker item ",e);
					error(e);
					errorcount++;
				}
				currentitem++;
			}
			if(currentitem >= processsize)
			{
				logger.info("Finishing daemon process {}", getProcessName());
				finish(); 
				close();
				return(false);
			}   
		}
		catch(Exception e)
		{
			resetRunning();
			logger.error("Exception in processItem", e);
			error(e);
			errorcount++;
			close();
			return(false);
		}

		return(true);      
	}

	protected abstract void finish();

	protected abstract void processItem();  

	public DaemonWorkerConfig getWorkerConfig()
	{
		return(config);
	}   

	public int getProcessSize() {
		return processsize;
	}

	public int getProcessedCount() {
		return proccessedcount;
	}   

	public int getErrorCount() {
		return errorcount;
	}

	// When any run has exception we must reset the flag so that subsequent runs do not think previous thread is still running
	public void resetRunning() {
		//do nothing here. can be overridden in child class to reset a running flag
	}     

}
