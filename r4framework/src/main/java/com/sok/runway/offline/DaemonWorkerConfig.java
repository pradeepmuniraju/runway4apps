package com.sok.runway.offline;

public interface DaemonWorkerConfig
{
   public int getInterval();
   
   public boolean getDebug();
   
   public boolean getPretend();
   
   public String getDebugAddress();
   
   public boolean isModified();

   public boolean hasWorkerTimer();
   
   public WorkerTimer getWorkerTimer();
}
