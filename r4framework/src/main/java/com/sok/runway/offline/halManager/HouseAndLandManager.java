package com.sok.runway.offline.halManager;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.MailerBean;
import com.sok.framework.RowBean;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;
import com.sok.runway.houseandland.CreateHouseAndLand;
import com.sok.runway.houseandland.CreateHouseAndLandDummy;
import com.sok.runway.houseandland.CreateHouseAndLandFailed;
import com.sok.runway.offline.AbstractDaemonWorker;
import com.sok.service.crm.cms.properties.DripService;

public class HouseAndLandManager extends AbstractDaemonWorker {
	private static final Logger logger = LoggerFactory.getLogger(HouseAndLandManager.class);
	private static final DripService dripService = DripService.getInstance();
	private static final String DEFAULT_VELOCITY_PATH = "/cms/emails/offlinealerts/";

	protected String alerttext = "generic/generic_text.vt";
	protected String alerthtml = "generic/generic.vt";

	private static HouseAndLandManager hlm;
	
	private HashMap<String,CreateHouseAndLand> chl = new HashMap<String,CreateHouseAndLand>();
	
	private int maxNumber = 2;	
	
	private int	sequenceNumber = 0;

	protected String emailalertsurlhome = null;


	public HouseAndLandManager(String configfilepath) {
		super(configfilepath);
		
		super.debug = false;
		
		super.debugInit(false);
		
		processsize = currentitem + maxNumber;
		
		readMaxPackage();
		readSequenceNumber();

		resetFailedTasks();
	}
	
	private void readMaxPackage() {
		String maxConcurrent = InitServlet.getSystemParam("MaxConcurrentPackaging");
		try {
			maxNumber = Integer.parseInt(maxConcurrent);
		} catch (Exception e) {}
	}

	private void readSequenceNumber() {
		String maxConcurrentSeqNum = InitServlet.getSystemParam("MaxConcurrentPackagingSeqNumber");
		try {
			sequenceNumber = Integer.parseInt(maxConcurrentSeqNum) + 1;
		} catch (Exception e) {}
	}

	private void writeSequenceNumber() {
		GenRow row = new GenRow();
		row.setTableSpec("SystemParameters");
		row.setParameter("ON-Name","MaxConcurrentPackagingSeqNumber");
		row.setParameter("Value", "" + sequenceNumber);
		row.doAction("updateall");
	}

	private void resetFailedTasks() {
		GenRow row = new GenRow();
		row.setTableSpec("ProductTasks");
		row.setParameter("-select0", "*");
		row.setParameter("StartedDate", "!NULL");
		row.setParameter("EndedDate", "NULL");
		row.sortBy("CreatedDate", 0);
		row.doAction("search");
		
		GenRow update = new GenRow();
		update.setTableSpec("ProductTasks");
		
		row.getResults();
		
		while (row.getNext()) {
			sendRestartEmail(row.getString("TaskID"), row.getString("UserID"), row.getString("Type"), row.getDate("CreatedDate"));
			update.clear();
			update.setParameter("TaskID", row.getString("TaskID"));
			update.setParameter("StartedDate", "NULL");
			update.setParameter("EndedDate", "NULL");
			update.doAction("update");
		}
		
		row.close();
	}

	public static HouseAndLandManager getInstance() {
		if (hlm == null)
			hlm = new HouseAndLandManager("");
		return hlm;
	}
	
	public static HouseAndLandManager getInstance(String configfilepath) {
		if (hlm == null)
			hlm = new HouseAndLandManager(configfilepath);
		return hlm;
	}
	
	public void init() {
		super.init();
	}

	public CreateHouseAndLand addHouseAndLand(HttpServletRequest request) {
		return addHouseAndLand(request, InitServlet.getSystemParam("SystemUserID"));
	}

	public CreateHouseAndLand addHouseAndLand(HttpServletRequest request, String userID) {
		readMaxPackage();
		CreateHouseAndLand halObject = null;
		if (chl.size() > 0) {
			if (cleanQueue()) ++processsize;
		}
		
		String id = saveHouseAndLand(request, userID);
		if (id == null || id.length() == 0) {
			sendFailedEmail(("" + sequenceNumber), userID, request.getParameter("-mode"), new Date());
			return new CreateHouseAndLandFailed(request, null); 
		}
		if (chl.size() < maxNumber) {
			if ("C".equals(request.getAttribute("-mode"))) {
				halObject = makeCreateTask(request, id, userID);
			} else if ("R".equals(request.getAttribute("-mode"))) {
				halObject = makeRecreateTask(request, id, userID);
			}
			
			if (halObject != null) {
				GenRow task = getTask(id);			
				sendStartEmail(id, task.getString("UserID"), task.getString("Type"), task.getDate("CreatedDate"));
				task.close();
			}			
		} 
		
		if (halObject == null) {
			halObject = new CreateHouseAndLandDummy(request, null);
		}
		
		return halObject ;
	}

	private CreateHouseAndLand makeCreateTask(HttpServletRequest request,
			String id, String userID) {
		
		ArrayList<String> houseproductids = new ArrayList<String>();
		ArrayList<String> landproductids = new ArrayList<String>();
		HashMap<String,String> selectedfacades = new HashMap<String,String>();

		if (request.getParameter("-selectedhouseproductids") != null) {
			String houseIDs = request.getParameter("-selectedhouseproductids");
			String[] hIDs = houseIDs.split("\\+");
			for (int s = 0; s < hIDs.length; ++s) {
				houseproductids.add(hIDs[s]);
			}
		}
		if (request.getParameter("-selectedlandproductids") != null) {
			String landIDs = request.getParameter("-selectedlandproductids");
			String[] lIDs = landIDs.split("\\+");
			for (int s = 0; s < lIDs.length; ++s) {
				landproductids.add(lIDs[s]);
			}
		}
		if (request.getParameter("-selectedhousefacadeproductids") != null) {
			String facadIDs = request.getParameter("-selectedhousefacadeproductids");
			String[] fIDs = facadIDs.split("\\+");
			String houseIDs = request.getParameter("-selectedhouseproductids");
			String[] hIDs = houseIDs.split("\\+");
			for (int s = 0; s < fIDs.length; ++s) {
				selectedfacades.put(hIDs[s],fIDs[s]);
			}
			//session.setAttribute("-selectedhousefacadeproductids",f);
		}
		
		if (houseproductids == null || houseproductids.size() == 0 || landproductids == null || landproductids.size() == 0) return new CreateHouseAndLandFailed(request, null);
		
		CreateHouseAndLand hal = new CreateHouseAndLand(request, null);
		hal.setToken("" + sequenceNumber);
		
		if ("true".equals(request.getParameter("-isPortal"))) hal.setPortal(true);

		hal.setCreateDuplicates(request.getParameter("-allowDups") != null && "Y".equals(request.getParameter("-allowDups")));
		
		String packageLibrary = (request.getParameter("newPackageCostLibrary") != null)? request.getParameter("newPackageCostLibrary") : "";
		
		if (packageLibrary.length() > 0) hal.setPackageCostLibrary(hal.getPackageIDs(packageLibrary));
	
		hal.setID(id);
		
		chl.put(id,hal);
		
		updateCreateStartTime(request, id);
		
		hal.createHouseAndLandPackages(houseproductids, landproductids, selectedfacades, userID);

		return hal;
	}

	private void updateCreateStartTime(HttpServletRequest request, String id) {
		GenRow task = new GenRow();
		task.setTableSpec("ProductTasks");
		if (request != null) task.setRequest(request);
		task.setParameter("TaskID", id);
		task.setParameter("StartedDate","NOW");
		task.setParameter("EndedDate","NULL");
		
		task.doAction("update");
	}

	private CreateHouseAndLand makeRecreateTask(HttpServletRequest request,
			String id, String userID) {

		javax.servlet.http.HttpSession session = request.getSession();
		String hlProductIDs = request.getParameter("-selectedhouselandproductids");
		
		if ("session".equals(hlProductIDs)) hlProductIDs = (String) session.getAttribute("productIDs");
		
		if (hlProductIDs == null || hlProductIDs.length() == 0) return new CreateHouseAndLandFailed(request, null);

		CreateHouseAndLand hal = null;
		
		if (hlProductIDs != null && hlProductIDs.length() > 0) {
			hal = new CreateHouseAndLand(request, null);
			hal.setToken("" + sequenceNumber);
			
			hlProductIDs = StringUtil.urlDecode(hlProductIDs).replaceAll(" ","+");
			String[] hlIDs = hlProductIDs.split("\\+");
			ArrayList<String> hl = new ArrayList<String>();
			for (int s = 0; s < hlIDs.length; ++s) {
				hl.add(hlIDs[s]);
			}
			hal.parseRequestOptions(request);

			hal.setID(id);
			
			chl.put(id,hal);
			
			updateCreateStartTime(request, id);
			
			hal.createHouseAndLandPackages(hl, userID);
		}
		return hal;
	}

	private String saveHouseAndLand(HttpServletRequest request, String userID) {
		GenRow row = new GenRow();
		row.parseRequest(request);
		
		if ("session".equals(row.getString("-selectedhouselandproductids"))) {
			javax.servlet.http.HttpSession session = request.getSession();
			String hlProductIDs = (String) session.getAttribute("productIDs");
	
			row.setParameter("-selectedhouselandproductids", hlProductIDs);
		}
		
		String taskID = row.getString("-taskID");
		if (taskID == null || taskID.length() == 0) taskID = KeyMaker.generate();
		
		String mode = "";

		boolean failed = true;
		if (!(row.getString("-selectedhouselandproductids").length() == 0 && !(row.getString("-selectedlandproductids").length() > 0 && row.getString("-selectedhouseproductids").length() > 0))) {
			failed = false;
			GenRow items = new GenRow();
			items.setTableSpec("ProductTaskItems");
			items.setRequest(request);
			items.setParameter("TaskID", taskID);
			items.setParameter("CreatedDate","NOW");
			
			if (row.getString("-selectedlandproductids").length() > 0 && row.getString("-selectedhouseproductids").length() > 0) {
				request.setAttribute("-mode","C");
				mode = "C";
				items.setParameter("LotID", row.getString("-selectedlandproductids"));
				items.setParameter("PlanID", row.getString("-selectedhouseproductids"));
				items.setParameter("TaskItemID", KeyMaker.generate());
				items.doAction("insert");
			} else if (row.getString("-selectedhouselandproductids").length() > 0) {
				request.setAttribute("-mode","R");
				mode = "R";
				try {
				javax.servlet.http.HttpSession session = request.getSession();
				String hlProductIDs = request.getParameter("-selectedhouselandproductids");
				
				if ("session".equals(hlProductIDs)) hlProductIDs = (String) session.getAttribute("productIDs");
	
				if (hlProductIDs != null && hlProductIDs.length() > 0) {
					hlProductIDs = StringUtil.urlDecode(hlProductIDs).replaceAll(" ","+");
					String[] hlIDs = hlProductIDs.split("\\+");
					ArrayList<String> hl = new ArrayList<String>();
					for (int s = 0; s < hlIDs.length; ++s) {
						hl.add(hlIDs[s]);
						items.setParameter("PackageID", hlIDs[s]);
						items.setParameter("TaskItemID", KeyMaker.generate());
						try {
							items.doAction("insert");
						} catch (Exception e) {}
					}
				}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				failed = true;
			}
		}
		
		GenRow task = new GenRow();
		task.setTableSpec("ProductTasks");
		task.setRequest(request);
		task.setParameter("TaskID", taskID);
		task.setParameter("UserID", userID);
		task.setParameter("SequenceNumber","" + (++sequenceNumber));
		task.setParameter("CreatedDate","NOW");
		task.setParameter("Parameters", row.toString());
		task.setParameter("Type", mode);
		if (failed) {
			task.setParameter("StartedDate","NOW");
			task.setParameter("EndedDate","NOW");
			
			task.setParameter("Errors", "Task could not be created correctly, check the parameters.");
			
		}
		
		task.doAction("insert");
		
		return failed? "" : task.getString("TaskID");
	}

	@Override
	public void checkConfig() {
		config = HouseAndLandConfig.getConfig(configfilepath);
	}

	@Override
	public String getProcessName() {
		return "House and Land Manager";
	}

	@Override
	protected void finish() {
		processsize = processsize + 1;
	}

	@Override
	protected void processItem() {
		readMaxPackage();
		if (chl.size() > 0) {
			if (cleanQueue()) ++processsize;
		}
		
		boolean failed = false;
		String taskID = "", userID = "";
		
		if (chl.size() < maxNumber) {
			GenRow task = getNextTask();
			if (task.isSuccessful() || task.isSet("TaskID")) {
				taskID = task.getString("TaskID");
				userID = task.getString("UserID");
				if ("C".equals(task.getString("Type"))) {
					GenRow row = new GenRow();
					row.parseRequest(task.getString("Parameters"));
					row.setParameter("UserID", task.getString("UserID"));
					
					if (row.getString("-selectedlandproductids").length() > 0 && row.getString("-selectedhouseproductids").length() > 0) {
						makeCreateTask(row, task.getString("TaskID"));
	
						if (!"true".equals(row.getString("-isPortal"))) sendStartEmail(task.getString("TaskID"), task.getString("UserID"), task.getString("Type"), task.getDate("CreatedDate"));
					} else {
						failed = true;
					}
				} else if ("R".equals(task.getString("Type"))) {
					GenRow row = new GenRow();
					row.parseRequest(task.getString("Parameters"));
					row.setParameter("UserID", task.getString("UserID"));
					if (row.getString("-selectedhouselandproductids").length() > 0) {
						makeRecreateTask(row, task.getString("TaskID"));
	
						if (!"true".equals(row.getString("-isPortal"))) sendStartEmail(task.getString("TaskID"), task.getString("UserID"), task.getString("Type"), task.getDate("CreatedDate"));
					} else {
						failed = true;
					}
				} else {
					failed = true;
				}
					
			}
		}
		
		if (failed && StringUtils.isNotBlank(taskID) && StringUtils.isNotBlank(userID)) {
			GenRow update = new GenRow();
			update.setTableSpec("ProductTasks");
			update.setParameter("TaskID", taskID);
			update.setParameter("StartedDate","NOW");
			update.setParameter("EndedDate","NOW");
			update.setParameter("Errors", "Task could not be created correctly, check the parameters.");
			update.doAction("update");
			
			sendFailedEmail(("" + sequenceNumber), userID, taskID, new Date());
		}
		writeSequenceNumber();
	}

	private Object makeRecreateTask(GenRow row, String id) {
		String userID = row.getString("UserID"); 
		if (!row.isSet("UserID")) userID = row.getString("CURRENTUSERID");

		CreateHouseAndLand hal = new CreateHouseAndLand(row);
		hal.setToken("" + sequenceNumber);
		chl.put(id,hal);
		hal.setID(id);
		hal.setUserID(userID);

		String hlProductIDs = row.getString("-selectedhouselandproductids");

		if (hlProductIDs != null && hlProductIDs.length() > 0) {
			hal = new CreateHouseAndLand(row);
			
			hlProductIDs = StringUtil.urlDecode(hlProductIDs).replaceAll(" ","+");
			String[] hlIDs = hlProductIDs.split("\\+");
			ArrayList<String> hl = new ArrayList<String>();
			for (int s = 0; s < hlIDs.length; ++s) {
				hl.add(hlIDs[s]);
			}
			hal.parseRequestOptions(row);

			hal.setID(id);
			
			chl.put(id,hal);
			
			updateCreateStartTime(null, id);
			
			hal.createHouseAndLandPackages(hl, userID);
		}
		return hal;
	}

	private void makeCreateTask(GenRow row, String id) {
		String userID = row.getString("UserID"); 
		if (!row.isSet("UserID")) userID = row.getString("CURRENTUSERID");

		CreateHouseAndLand hal = new CreateHouseAndLand(row);
		hal.setToken("" + sequenceNumber);
		chl.put(id,hal);
		hal.setID(id);
		hal.setUserID(userID); // TODO Fix this
		
		if ("true".equals(row.getString("-isPortal"))) hal.setPortal(true);
		
		ArrayList<String> houseproductids = new ArrayList<String>();
		ArrayList<String> landproductids = new ArrayList<String>();
		HashMap<String,String> selectedfacades = new HashMap<String,String>();

		if (row.getString("-selectedhouseproductids") != null) {
			String houseIDs = row.getString("-selectedhouseproductids");
			String[] hIDs = houseIDs.split("\\+");
			for (int s = 0; s < hIDs.length; ++s) {
				houseproductids.add(hIDs[s]);
			}
		}
		if (row.getString("-selectedlandproductids") != null) {
			String landIDs = row.getString("-selectedlandproductids");
			String[] lIDs = landIDs.split("\\+");
			for (int s = 0; s < lIDs.length; ++s) {
				landproductids.add(lIDs[s]);
			}
		}
		if (row.getString("-selectedhousefacadeproductids") != null) {
			String facadIDs = row.getString("-selectedhousefacadeproductids");
			String[] fIDs = facadIDs.split("\\+");
			String houseIDs = row.getString("-selectedhouseproductids");
			String[] hIDs = houseIDs.split("\\+");
			for (int s = 0; s < fIDs.length; ++s) {
				selectedfacades.put(hIDs[s],fIDs[s]);
			}
			//session.setAttribute("-selectedhousefacadeproductids",f);
		}
		
		hal.setCreateDuplicates(row.getString("-allowDups") != null && "Y".equals(row.getString("-allowDups")));
		
		String packageLibrary = (row.getString("newPackageCostLibrary") != null)? row.getString("newPackageCostLibrary") : "";
		
		if (packageLibrary.length() > 0) hal.setPackageCostLibrary(hal.getPackageIDs(packageLibrary));
		
		updateCreateStartTime(null, id);
		
		hal.createHouseAndLandPackages(houseproductids, landproductids, selectedfacades, userID);
	}

	private GenRow getNextTask() {
		GenRow row = new GenRow();
		row.setTableSpec("ProductTasks");
		row.setParameter("-select0", "*");
		row.setParameter("StartedDate", "NULL");
		row.sortBy("CreatedDate", 0);
		row.doAction("selectfirst");
		
		return row;
	}

	@Override
	public int getProcessSize() {
		return currentitem + 1;
	}

	@Override
	public int getProcessedCount() {
		return currentitem;
	}
	private synchronized boolean cleanQueue() {
		Set<String> set = chl.keySet();
		
		boolean cleaned = false;

		synchronized (set) {
			Iterator<String> i = set.iterator();	
				
			while (i.hasNext()) {
				String key = "";
				try {
					key = i.next();
					if (key != null && key.length() > 0) {
						CreateHouseAndLand hal = chl.get(key);
						if (hal != null) {
							if (!hal.isRunning()) {
								updateTask(key, hal.getErros(), hal.getMaxPackages(), hal.getDuplcatePackages(), hal.getFailedPackages(), !hal.isPortal());
								chl.remove(key);
								cleaned = true;
							}
						}
					}
				} catch (ConcurrentModificationException e) {
					set = chl.keySet();
					i = set.iterator();
				}
			}
		}
		
		return cleaned;
	}

	private void updateTask(String taskID, String errors, int packageCount, int duplicateCount, int failedCount, boolean sendEmail) {
		GenRow task = new GenRow();
		task.setTableSpec("ProductTasks");
		task.setParameter("-select0", "*");
		task.setParameter("TaskID", taskID);
		
		task.doAction("selectfirst");
		
		String userID = task.getString("UserID");
		String type = task.getString("Type");
		Date date = task.getDate("CreatedDate");
		
		task.clear();
		task.setParameter("TaskID", taskID);
		task.setParameter("EndedDate","NOW");
		task.setParameter("PackageCount", "" + packageCount);
		task.setParameter("DuplicateCount", "" + duplicateCount);
		task.setParameter("FailedCount", "" + failedCount);
		task.setParameter("Errors", errors);
		
		try {
			task.doAction("update");
		} catch (Exception e) {
			if (StringUtils.isNotBlank(errors) && errors.length() > 32000) {
				errors = errors.substring(errors.length() - 32000);
			}
			task.setParameter("Errors", errors);
			task.doAction("update");
		}

		if (sendEmail) sendEndEmail(taskID, userID, type, date, errors, packageCount, duplicateCount, failedCount);
		
	}
	
	private String getTaskSequence(String taskID) {
		GenRow task = new GenRow();
		task.setTableSpec("ProductTasks");
		task.setParameter("-select0", "SequenceNumber");
		task.setParameter("TaskID", taskID);
		
		task.doAction("selectfirst");
		
		return task.getString("SequenceNumber");
	}
	
	private GenRow getTask(String taskID) {
		GenRow task = new GenRow();
		task.setTableSpec("ProductTasks");
		task.setParameter("-select0", "*");
		task.setParameter("TaskID", taskID);
		
		task.doAction("selectfirst");
		
		return task;
	}
	
	public void addTaskItem(String taskID, String packageID, String lotID, String planID, String passed, String deleted, long timeTaken) {
		GenRow items = new GenRow();
		items.setTableSpec("ProductTaskItems");
		items.setParameter("TaskItemID", KeyMaker.generate());
		items.setParameter("TaskID", taskID);
		items.setParameter("PackageID", packageID);
		items.setParameter("LotID", lotID);
		items.setParameter("PlanID", planID);
		items.setParameter("CreatedDate","NOW");
		items.setParameter("Passed", passed);
		items.setParameter("Deleted", deleted);
		items.setParameter("TimeTaken", timeTaken);
		
		try {
			items.doAction("insert");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateTaskItem(String taskID, String packageID, String lotID, String planID, String passed, String deleted, long timeTaken) {
		GenRow items = new GenRow();
		items.setTableSpec("ProductTaskItems");
		items.setParameter("ON-TaskID", taskID);
		items.setParameter("ON-PackageID", packageID);
		items.setParameter("LotID", lotID);
		items.setParameter("PlanID", planID);
		items.setParameter("CreatedDate","NOW");
		items.setParameter("Passed", passed);
		items.setParameter("Deleted", deleted);
		items.setParameter("TimeTaken", timeTaken);
		
		try {
			items.doAction("updateall");
		} catch (Exception e) {
			addTaskItem(taskID, packageID, lotID, planID, passed, deleted, timeTaken);
			e.printStackTrace();
		}
	}
	
	SimpleDateFormat hhmm = new SimpleDateFormat("HH:mm");
	SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd MMM yyyy");
	
	private void sendStartEmail(String taskID, String userID, String type, Date date) {
		logger.debug("Sending start email to user - {} about task - {}", userID, taskID);
		if (userID == null || userID.length() == 0) return;
		
		StringBuilder msg = new StringBuilder();
		
		String subject = "";
		
		GenRow user = new GenRow();
		user.setTableSpec("Users");
		user.setParameter("UserID", userID);
		user.setParameter("-select0","Email");
		user.setParameter("-select1","FirstName");
		user.setParameter("-select2","LastName");
		user.doAction("selectfirst");
		
		if (!user.isSet("Email")) return;
		
		String seqNum = getTaskSequence(taskID);
		
		String url = InitServlet.getSystemParam("URLHome") + "/runway/cms.rpm.hal?-showprogress=true&TaskID=" + taskID;

		if ("C".equals(type)) {
			subject = "Your Create House and Land Task #" + seqNum + " has started";
			msg.append("Dear ")
			.append(user.getString("FirstName"))
			.append(",<br><br>The Create House and Land Task #" + seqNum + " that you submitted at ");
		} else {
			subject = "Your Recreate House and Land Task #" + seqNum + " has started";
			msg.append("Dear ")
			.append(user.getString("FirstName"))
			.append(",<br><br>The Recreate House and Land Task #" + seqNum + " that you submitted at ");
			url += "&-recreate=true";
		}
		
		msg.append(hhmm.format(date))
		.append(" on ")
		.append(ddmmyyyy.format(date))
		.append(" has started. You will receive a notification when the task has completed.");

		msg.append("<br><br>You can view the progress of this task at<br><a href=\"")
		.append(url)
		.append("\">")
		.append(url)
		.append("</a>")
		.append("<br><br>Regards Runway");
		
		
		sendEmail(user.getString("Email"), subject, msg.toString());
	}

	private void sendFailedEmail(String seqNum, String userID, String type, Date date) {
		logger.debug("Sending failed email to user - {} about task - {}", userID, seqNum);
		if (userID == null || userID.length() == 0) return;
		
		StringBuilder msg = new StringBuilder();
		
		String subject = "";
		
		GenRow user = new GenRow();
		user.setTableSpec("Users");
		user.setParameter("UserID", userID);
		user.setParameter("-select0","Email");
		user.setParameter("-select1","FirstName");
		user.setParameter("-select2","LastName");
		user.doAction("selectfirst");
		
		if (!user.isSet("Email")) return;

		if ("C".equals(type)) {
			subject = "Your Create House and Land Task #" + seqNum + " has failed";
			msg.append("Dear ")
			.append(user.getString("FirstName"))
			.append(",<br><br>The Create House and Land Task #" + seqNum + " that you submitted at ");
			msg.append(hhmm.format(date))
			.append(" on ")
			.append(ddmmyyyy.format(date))
			.append(" has failed. Please make sure you select at least one house and one lot to create a package.");
		} else {
			subject = "Your Recreate House and Land Task #" + seqNum + " has failed";
			msg.append("Dear ")
			.append(user.getString("FirstName"))
			.append(",<br><br>The Recreate House and Land Task #" + seqNum + " that you submitted at ");
			msg.append(hhmm.format(date))
			.append(" on ")
			.append(ddmmyyyy.format(date))
			.append(" has failed. Please make sure you have at least one package to recreate.");
		}
		

		msg.append("<br><br>No further emails will be sent regarding this task.");
		
		sendEmail(user.getString("Email"), subject, msg.toString());
	}

	private void sendRestartEmail(String taskID, String userID, String type, Date date) {
		logger.debug("Sending restart email to user - {} about task - {}", userID, taskID);
		if (userID == null || userID.length() == 0) return;
		
		StringBuilder msg = new StringBuilder();
		
		String subject = "";
		
		GenRow user = new GenRow();
		user.setTableSpec("Users");
		user.setParameter("UserID", userID);
		user.setParameter("-select0","Email");
		user.setParameter("-select1","FirstName");
		user.setParameter("-select2","LastName");
		user.doAction("selectfirst");
		
		if (!user.isSet("Email")) return;
		
		String seqNum = getTaskSequence(taskID);
		
		if ("C".equals(type)) {
			subject = "Your Create House and Land Task #" + seqNum + " has been reset";
			msg.append("Dear ")
			.append(user.getString("FirstName"))
			.append(",<br><br>Runway has restarted, the Create House and Land Task #" + seqNum + " that you submitted at ")
			.append(hhmm.format(date))
			.append(" on ")
			.append(ddmmyyyy.format(date))
			.append(" has been reset and placed back on the queue. You will receive a notification when the task has started processing again.")
			.append("<br><br>Any package aready created will be counted as a duplicate.")
			.append("<br><br>Regards Runway.");
		} else {
			subject = "Your Recreate House and Land Task, #" + seqNum + " has been reset";
			msg.append("Dear ")
			.append(user.getString("FirstName"))
			.append(",<br><br>The Recreate House and Land Task #" + seqNum + " that you submitted at ")
			.append(hhmm.format(date))
			.append(" on ")
			.append(ddmmyyyy.format(date))
			.append(" has been reset and placed back on the queue. You will receive a notification when the task has started processing again.")
			.append("<br><br>All packages in this task will be recreated again.")
			.append("<br><br>Regards Runway.");
			
		}
		
		sendEmail(user.getString("Email"), subject, msg.toString());
	}

	private void sendEndEmail(String taskID, String userID, String type, Date date, String errors, int packageCount, int duplicateCount, int failedCount) {
		logger.debug("Sending end email to user - {} about task - {}", userID, taskID);
		if (userID == null || userID.length() == 0) return;
		
		StringBuilder msg = new StringBuilder();
		
		String subject = "";
		
		GenRow user = new GenRow();
		user.setTableSpec("Users");
		user.setParameter("UserID", userID);
		user.setParameter("-select0","Email");
		user.setParameter("-select1","FirstName");
		user.setParameter("-select2","LastName");
		user.doAction("selectfirst");
		
		if (!user.isSet("Email")) return;
		
		String seqNum = getTaskSequence(taskID);

		if ("C".equals(type)) {
			subject = "Your Create House and Land Task #" + seqNum + " has finished";
			msg.append("Dear ")
			.append(user.getString("FirstName"))
			.append(",<br><br>The Create House and Land Task #" + seqNum + " that you submitted at ");
		} else {
			subject = "Your Recreate House and Land Task #" + seqNum + " has finished";
			msg.append("Dear ")
			.append(user.getString("FirstName"))
			.append(",<br><br>The Recreate House and Land Task #" + seqNum + " that you submitted at ");
		}
		
		msg.append(hhmm.format(date))
		.append(" on ")
		.append(ddmmyyyy.format(date))
		.append(" has finished.");
		
		if ("C".equals(type)) {
			msg.append("<br><br>Packages to processes: ").append(packageCount)
			   .append("<br>Packages Created: ").append((packageCount - (duplicateCount + failedCount)))
			   .append("<br>Duplicate Packages: ").append(duplicateCount)
			   .append("<br>Failed Rules: ").append(failedCount);
		} else {
			msg.append("<br><br>Packages to processes: ").append(packageCount)
			   .append("<br>Packages Recreated: ").append((packageCount - (duplicateCount + failedCount)))
			   .append("<br>Incorrect Status: ").append(duplicateCount)
			   .append("<br>Failed Rules: ").append(failedCount);
		}
		if (errors != null && errors.length() > 0) {
			msg.append("<br><br>The following errors occured;<br><br>")
			.append(errors);
		}
		String url = InitServlet.getSystemParam("URLHome") + "/runway/cms.rpm.hal?-showresults=true&TaskID=" + taskID;
		
		msg.append("<br><br>You can view the packages at<br><a href=\"")
		.append(url)
		.append("\">")
		.append(url)
		.append("</a>")
		.append("<br><br>Regards Runway");
		
		sendEmail(user.getString("Email"), subject, msg.toString());
	}

	protected void sendUserEmail(String alert) {
		debug("sending email to " + alert);
		try {

			RowBean context = new RowBean();
			context.put("URLHome", urlhome);
			context.put("EmailAlertsURLHome", emailalertsurlhome);
			context.put("alert", alert);

			String text = generateEmail(context, alerttext);
			String html = generateEmail(context, alerthtml);

			ServletConfig sc = InitServlet.getConfig();
			if (sc != null && alert.length() != 0) {
				MailerBean mailer = new MailerBean();
				mailer.setServletContext(sc.getServletContext());
				mailer.setFrom(defaultFrom);
				mailer.setTo(alert);
				mailer.setSubject("Status Alerts");
				mailer.setTextbody(text);
				mailer.setHtmlbody(html);
				if (config.getPretend()) {
					debug("email to " + alert + "\r\n " + html);
				} else {
					String response = mailer.getResponse();
					debug("email to " + alert + " " + response);
				}
			}

		} catch (Exception e) {
			error(e);
		}
	}

	protected void sendEmail(String to, String subject, String msg) {
		logger.debug("System param for notitication {} " , InitServlet.getSystemParams().getProperty("HAL-SendNotificationEmailOnHnLCreation"));
		if("false".equals(InitServlet.getSystemParams().getProperty("HAL-SendNotificationEmailOnHnLCreation"))) { 
				debug("Not sending email to " + to + "as systemparam is off");
				logger.debug("Not sending email to " + to + "as systemparam is off");
				return;
		}
		
		debug("sending email to " + to);
		try {

			if (config != null && config.getDebug()) {
				to += ", " + config.getDebugAddress();
			}
			
			Date d = new Date();
			RowBean context = new RowBean();
			context.put("URLHome", urlhome);
			context.put("EmailAlertsURLHome", emailalertsurlhome);
			context.put("alert", to);
			context.put("body", msg);
			context.put("subject", subject);
			context.put("date", ddmmyyyy.format(d) + ", " + hhmm.format(d));

			String text = generateEmail(context, alerttext);
			String html = generateEmail(context, alerthtml);
			//String text = generateEmail(context, StringUtil.convertHtmlToText(msg));
			//String html = generateEmail(context, msg);

			ServletConfig sc = InitServlet.getConfig();
			if (sc != null && to.length() != 0) {
				MailerBean mailer = new MailerBean();
				mailer.setServletContext(sc.getServletContext());
				mailer.setFrom(defaultFrom);
				mailer.setTo(to);
				mailer.setSubject(subject);
				mailer.setTextbody(text);
				mailer.setHtmlbody(html);
				if (config != null && config.getPretend()) {
					debug("email to " + to + "\r\n " + html);
				} else {
					String response = mailer.getResponse();
					debug("email to " + to + " " + response);
				}
			}

		} catch (Exception e) {
			error(e);
		}
	}

	protected String generateEmail(TableData context, String script) {
		StringWriter text = new StringWriter();
		try {
			StringBuffer roottemplate = new StringBuffer();
			roottemplate.append("#parse(\"");
			roottemplate.append(DEFAULT_VELOCITY_PATH);
			roottemplate.append(script);
			roottemplate.append("\")");

			VelocityEngine ve = VelocityManager.getEngine();
			ve.evaluate(context, text, "StatusAlerts", roottemplate.toString());

			text.flush();
			text.close();

		} catch (Exception e) {
			error(e);
		}
		return (text.toString());
	}

	public CreateHouseAndLand getPackageTask(String taskID) {
		CreateHouseAndLand hal = chl.get(taskID);
		if (hal != null) {
			return hal;
		}
		
		GenRow row = new GenRow();
		row.setTableSpec("ProductTasks");
		row.setParameter("-select0", "*");
		row.setParameter("TaskID", taskID);
		row.doAction("selectfirst");
		
		return new CreateHouseAndLandDummy(row);
	}
	
	public List<String> getTaskPackageIDs(String taskID) {
		List<String> list = new ArrayList<String>();
		
		if (taskID != null && taskID.length() > 0) {
			GenRow row = new GenRow();
			row.setTableSpec("ProductTaskItems");
			row.setParameter("-select0", "PackageID");
			row.setParameter("TaskID", taskID);
			
			row.doAction("search");
			
			row.getResults();
			
			while (row.getNext()) {
				if (row.isSet("PackageID"))	list.add(row.getString("PackageID"));
			}
			
			row.close();
		}
		
		return list;
		
	}
}