package com.sok.runway.offline;

import java.util.*;
import java.io.*;

import com.sok.Debugger;
import com.sok.framework.*;
import com.sok.runway.*;

public class OfflineNotificationSender {
   
   private String smtpHost = null;
   private String smtpPort = null;
   private String smtpUsername = null;
   private String smtpPassword = null;
   
   private String defaultFrom = null;
   
   private static final String DEFAULT_EMAIL = "Runway<info@switched-on.com.au>";
   
   public OfflineNotificationSender() {
		java.util.Properties sysprops = InitServlet.getSystemParams();
		   
		smtpHost = sysprops.getProperty("SMTPHost");
      smtpPort = sysprops.getProperty("SMTPPort");
		smtpUsername = sysprops.getProperty("SMTPUsername");
		smtpPassword = sysprops.getProperty("SMTPPassword");
		
      defaultFrom = sysprops.getProperty("DefaultEmail");
	   if (defaultFrom == null) {
	      defaultFrom = DEFAULT_EMAIL;
	   }
   }
   
   
   public void notify(OfflineNotifier notifier) {
		File attachment = null;
		try {
		   
			MailerBean mailer = new MailerBean();
			mailer.setFrom(defaultFrom);
			
			try {
				mailer.setTo(notifier.getStatusMailRecipient());
			}
			catch(Exception e) {
				mailer.setTo("user null <support@switched-on.com.au>");
			}
			if (!defaultFrom.equals(notifier.getStatusMailRecipient())) {
			   mailer.setCc(defaultFrom);
			}
			mailer.setHost(smtpHost);
			mailer.setPort(smtpPort);
			mailer.setUsername(smtpUsername);
			mailer.setPassword(smtpPassword);
			
			mailer.setHtmlbody(notifier.getStatusMailHtmlBody());
			mailer.setTextbody(notifier.getStatusMailBody());
			mailer.setSubject(notifier.getStatusMailSubject());
			
			attachment = notifier.getStatusAttachment();
			if(attachment != null) {
				mailer.setAttachment(attachment);
			}
			mailer.getResponse();
			
		}
		catch(Exception e) {
         StringBuffer sb = new StringBuffer();
         sb.append("Offline Error: ");
         sb.append(notifier.getStatusMailSubject());     
         sb.append(ActionBean.writeStackTraceToString(e));  
         sb.append(notifier.getStatusMailSubject());     
         
         Debugger.getDebugger().debug(sb.toString());			
		}
		finally {
			if(attachment != null) {
				attachment.delete();
			}
		}
	}
   	
}