package com.sok.runway.offline.campaignManager;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;
import com.sok.runway.offline.*;
import com.sok.runway.campaigns.Campaign;
import com.sok.runway.email.*;
import com.sok.service.crm.UserService;
import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.framework.servlet.ResponseOutputStream;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

public class CampaignManager extends AbstractDaemonWorker {

	private static final Logger logger = LoggerFactory.getLogger(CampaignManager.class);
	public static String URLHome = null;
	
	public static final String EVENTS = "Events"; 
	public static final String EVENTVIEW = "EventView";
	public static final String CONTACTEVENTS = "ContactEvents";
	public static final String CONTACTEVENTVIEW = "ContactEventView";
	public static final String EVENTBEANVIEW = "EventBeanView";
	public static final String EVENTID = "EventID";
	public static final String MASTEREVENTID = "MasterEventID";
	public static final String BEANID = "BeanID";
	public static final String REMOVALBEANID = "RemovalBeanID";
	
	private boolean isRunning = false;
	private boolean hasInit = false;
	
	static {
		Properties sysprops = InitServlet.getSystemParams();
		if (sysprops != null) {
			URLHome = sysprops.getProperty("URLHome");
		}
	}

	public CampaignManager(String configfilepath) {
		super(configfilepath);
		logger.info("CampaignManager({})", configfilepath);
	}

	protected void init() {
		logger.info("Campaign Manager Start");
		if (!isRunning) {
			isRunning = true;
			super.init();
			if(!hasInit) {
				try { 
					notifyFailedSends();
				} finally {
					hasInit = true;
				}
			}
			processEventBeans();
			processEventEmails();
			mapOutcomes(); 
			processOutcomes();
			isRunning = false;
		}
		logger.debug("Campaign Manager End");
	}
	
	private void notifyFailedSends() {
		logger.debug("processFailedSends()");
		
		GenRow search = new GenRow();
		search.setTableSpec("ContactEvents");
		search.setParameter("-join1","ContactEventCampaign.CampaignCampaign"); 
		// orig 
		search.setParameter("-select1","MasterEventID");
		search.setParameter("-select2","ContactEventCampaign.CampaignCampaign.Name as CampaignName");
		// end orig 
		search.setParameter("-select3","SendStatus");
		
		search.setParameter("Status","Active");
		search.setParameter("SendStatus","Sending%");
		search.setParameter("-groupby0", "MasterEventID");
		search.setParameter("-groupby1", "SendStatus");
		search.setParameter("ContactEventCampaign.Status","Active");
		search.setParameter("ContactEventCampaign.CampaignCampaign.Status","Active");
		
		GenRow update = new GenRow();
		update.setTableSpec("ContactEvents");
		
		if(logger.isDebugEnabled()) { 
			search.doAction(GenerationKeys.SEARCH);
			logger.info(search.getStatement());
		}
		try { 
			search.setConnection(getConnection());
			update.setConnection(getConnection());
			search.getResults(); 
			if(search.getNext()) { 	
				do { 
					// one loop for each MasterEventID / Sending-KEY pair
					update.clear();
					update.setParameter("SendStatus","NULL");
					update.setParameter("ON-SendStatus", search.getData("SendStatus"));
					update.setParameter("ON-MasterEventID", search.getData("MasterEventID"));
					update.doAction(GenerationKeys.UPDATEALL);
					
					GenRow bean = new GenRow();
					bean.setConnection(getConnection());
					bean.setParameter("EventID", search.getData("MasterEventID"));
					bean.setViewSpec("EventView");
					bean.doAction("select");
					
					if(update.isSuccessful() && bean.isSuccessful()) { 
						int updated = update.getUpdatedCount();
						if("Manual".equals(bean.getData("SendMethod"))) { 
							// auto sends should kick off on their own again 
							logger.info("Notifying user of failed send for Campaign {}", search.getData("CampaignName"));
							String userID = null;
							
							GenRow campaign = new GenRow();
							campaign.setTableSpec("Campaigns");
							campaign.setConnection(getConnection());
							campaign.setParameter("CampaignID", bean.getData("CampaignID"));
							campaign.setParameter("Status","Stopped");
							campaign.doAction(GenerationKeys.UPDATE);
							
							GenRow activity = new GenRow();
							activity.setConnection(getConnection()); 
							activity.setViewSpec("ActivityView");
							activity.setParameter("RecordType","Emails");
							activity.setParameter("ActionType","Bulk Send");
							activity.setParameter("RecordID", search.getData("SendStatus"));
							if(activity.isSet("RecordID")) {
								activity.doAction(GenerationKeys.SELECTFIRST);
								if(activity.isSuccessful() && activity.isSet("UserID")) {
									userID = activity.getData("UserID");
								}
							}
							if(userID == null && bean.isSet("CreatedBy")) {
								userID = bean.getData("CreatedBy");
							}
							if(userID == null) userID = InitServlet.getSystemParam("SystemUserID", "F180D2D08404F1241723606192C82747");
							UserBean user = UserService.getInstance().getSimpleUser(getConnection(), userID);
							
								
							HttpSession session = new com.sok.framework.servlet.HttpSession(InitServlet.getConfig().getServletContext());
							
							ResponseOutputStream responseOS = null;
							ByteArrayOutputStream baos = new ByteArrayOutputStream();

							try {
								String url = new StringBuilder()
								.append("/crm/autopilot/emails/campaignerror.jsp?-campaignName=")
								.append(StringUtil.urlEncode(bean.getData("CampaignName")))
								.append("&-recordCount=").append(updated)
								.append("&-viewLink=")
								.append(StringUtil.urlEncode(InitServlet.getSystemParam("URLHome")))
								.append(StringUtil.urlEncode("/runway/cms.event.view?-action=select&EventID="))
								.append(StringUtil.urlEncode(bean.getData("EventID"))
								).toString();
								
								responseOS = new ResponseOutputStream(session, baos, url.toString());
								responseOS.service(getConnection());
								
							} catch (Exception e) {
								baos = null;
								logger.error("Error pulling campaign jsp", e);
								//throw new RuntimeException(e);
							} finally {
								if (baos != null) {
									try {
										baos.flush();
										baos.close();
									} catch (Exception ex) {
										throw new RuntimeException(ex);
									}

								}
								if (responseOS != null) {
									responseOS.close();
								}

							}
							 
							StringBuilder email = new StringBuilder(); 
							email.append("Dear ")
								 .append(user.getFirstName())
								 .append("\n\nAn email event from ")
								 .append(bean.getData("CampaignName"))
								 .append(" has stopped sending, with ")
								 .append(updated)
								 .append(" emails remaining.\n\nTo view this campaign, please click here: ")
								 .append(InitServlet.getSystemParam("URLHome"))
								 .append("/runway/cms.event.view?-action=select&EventID=")
								 .append(bean.getData("EventID"))
								 .append("\n\n");
							/*
Dear ${FirstName},

An email event from ${CampaignName}} has stopped sending, with {{recordCount}} emails remaining. 

To view this campaign, please click here: {{link}}
							*/
							if(logger.isTraceEnabled()) { 
								logger.trace("Failed Campaign Send: to={}, body={}", user.getEmail(), email.toString());
								logger.trace("HtmlBody={}", baos.toString());
							}
							
							MailerBean mailerBean = new MailerBean();
							mailerBean.setFrom(InitServlet.getSystemParam("DefaultEmail", "info@switched-on.com.au"));
							mailerBean.setTo(user.getEmail());
							mailerBean.setCc("support@switched-on.com.au");
							mailerBean.setHost(InitServlet.getConfig().getServletContext().getInitParameter("SMTPHost"));
							mailerBean.setUsername(InitServlet.getConfig().getServletContext().getInitParameter("SMTPUsername"));
							mailerBean.setPassword(InitServlet.getConfig().getServletContext().getInitParameter("SMTPPassword"));
							mailerBean.setTextbody(email.toString());
							if(baos != null) mailerBean.setHtmlbody(baos.toString());
							mailerBean.setSubject("Action Required - Runway Email Campaign");
							
							logger.debug("Campaign Email Notification for Campaign={} with response={}", bean.getData("CampaignName"), mailerBean.getResponse());
						}
					} 	
					
					/* create new beans as they will be passed to another thread - this would process a send for the remaining on the campaign that had stopped.

					if(bean.isSuccessful()) {
						GenRow s = new GenRow();
						s.setViewSpec("ContactListView"); 
						s.setParameter("ContactCampaigns.ContactCampaignEvents.MasterEventID",bean.getData("EventID"));
						s.setParameter("ContactCampaigns.ContactCampaignEvents.SendStatus",search.getData("SendStatus"));
						s.setParameter("ContactCampaigns.ContactCampaignEvents.Status","Active");
						s.setParameter("ContactCampaigns.ContactCampaignEvents.EventCompleted|2","NULL");
						s.addParameter("ContactCampaigns.ContactCampaignEvents.EventCompleted|2","N");
						s.doAction(GenerationKeys.SEARCH);
						if(logger.isDebugEnabled()) {
							logger.debug(s.getStatement());
						}
				        if(bean.isSet("NewsletterCMSID")) 
				        {
				        	bean.setParameter("TemplateID",bean.getData("NewsletterTemplateID"));
				        	bean.setParameter("CMSID",bean.getData("NewsletterCMSID"));
				        }
				        bean.setParameter("NoteTypeDescription", "CampaignAutoRestart");
				        bean.setParameter("-checkcount", "-1");
				        bean.putInternalToken(SessionKeys.CURRENT_USER,UserService.getInstance().getSimpleUser(getConnection(), "E3B3653517125741C947061350C510E4"));
				        
				        new OfflineEmailer(bean, InitServlet.getConfig().getServletContext(), s);
						logger.info("Offline Process commenced");
					} else {
						logger.error("Could not find events, campaign not restarted");
					}
					*/
				} while(search.getNext()); 
			}
		} finally {
			search.close();
		}
		logger.info("processFailedSends complete");
	}

	private void mapOutcomes() { 
		logger.debug("mapOutcomes()");
		debug("Mapping outcomes..");
		try {
		GenRow search = new GenRow(); 
		search.setConnection(getConnection()); 
		search.setTableSpec("ContactEvents"); 
			for(Map.Entry<String, String[][]> o: Campaign.outcomeSearch.entrySet())
			{
				debug("Mapping " + o.getKey()); 
				search.clear();
				for(String[] k: o.getValue())
				{
					search.setParameter("ON-"+k[0], k[1]);
				}
			    //
				search.setParameter("SendOutcome", o.getKey());
				search.setParameter("ON-ModifiedDate", "LAST3MONTHS+TODAY");
				//search.setParameter("SendStatus", o.getKey());
				search.doAction(GenerationKeys.UPDATEALL);
				//debug(search.getSearchStatement());
			}
		} catch (Exception e) { 
			debug("error in mapping outcomes");
			debug(ActionBean.writeStackTraceToString(e));
			logger.error("error in mapping outcomes", e);
		}
	}
	
	private void processOutcomes() { 
		logger.debug("processOutcomes()");
		debug("Processing outcomes..");
		try { 
			/* outcomes needs to loop through linked statuses for active campaigns.. */
			GenRow ls = new GenRow(); 
			ls.setConnection(getConnection()); 
			ls.setViewSpec("LinkedStatusView");
			ls.setParameter("CampaignID","!EMPTY");
			ls.setParameter("LinkedStatusCampaign.Status", "Active");  
			ls.setParameter("LinkedStatusCampaign.Type", "Email");
			ls.setParameter("-join1","LinkedStatusBean"); 
			ls.setParameter("-select1","LinkedStatusBean.Bean");
			ls.setParameter("BeanID", "!EMPTY");
			ls.doAction(GenerationKeys.SEARCH);
			
			//debug(ls.getSearchStatement());
			ls.getResults();
			
			int updated = 0; 
			while(ls.getNext()) 
			{
				//debug("processing linked status " + ls.getData("LinkedStatusID"));
				//debug(ls.getData("Bean"));
				updated += Campaign.updateStatus(ls, ls.getData("Bean"), systemuserid);
			}
			debug("updated " + updated + " results");
		} catch (Exception e) { 
			debug("error in process outcomes");
			debug(ActionBean.writeStackTraceToString(e));
			logger.error("error in process outcomes", e);
		}
	}

	/**
	 * search for event beans whose parent event is active, and for which
	 * loading is automatic
	 */
	private void processEventBeans() {
		logger.debug("processEventBeans()");
		GenRow s = new GenRow();
		try {
			s.setViewSpec(EVENTBEANVIEW);
			s.setConnection(this.getConnection());
			s.setParameter("Status", "Active");
			s.setParameter("-select1","EventBeanEvent.CampaignID");
			s.setParameter("EventBeanEvent.Status", "Active");
			s.setParameter("EventBeanEvent.LoadMethod", "Automatic");
			//s.setParameter("EventBeanEvent.EventCampaign.Type", "Email");
			s.setParameter("EventBeanEvent.EventCampaign.Active", "Y");
			s.setParameter("-sort1", "Omit");
			s.setParameter("-order1", "ASC");
			s.doAction("search");
			
			s.getResults();
			int processed = 0; 
			while (s.getNext()) {
				/* perform the loading function */
				if (s.getString("Bean").length() > 0) {
					processResults(s);
					setLoaded(s);
					processed++;
				}
			}
			debug(String.valueOf(processed) + " event beans processed\r\n\r\n");
			//debug(s.getSearchStatement());
		} catch (Exception e) {
			error(e);
			debug("error in process loading");
			debug(ActionBean.writeStackTraceToString(e));
			logger.error("error in process loading", e);
		} finally {
			s.close();
		}
	}
	
	/**
	 * Process automatic sending of campaign emails.
	 * @param row
	 */
	private void processEventEmails() {
		GenRow e = new GenRow();
		GenRow s = new GenRow();
		Emailer emailer  = null;
		try {
			e.setConnection(this.getConnection());
			e.setViewSpec(EVENTVIEW);
			e.setParameter("Status","Active");
			e.setParameter("EventCampaign.Type", "Email");
			e.setParameter("EventCampaign.Active", "Y");
			e.setParameter("EventDate", "<NOW");
			e.setParameter("EventFinishDate", ">NOW");			
			e.setParameter("SendMethod", "Automatic");
			
			e.setParameter("NextSendDate|4","<NOW");
			e.setParameter("NextSendDate^3|4","NULL");
			e.setParameter("SendTime^3|4","<NOW+NULL");
			
			e.setParameter("TemplateID|1","!EMPTY");
			e.setParameter("NewsletterCMSID|1","!EMPTY");

			/* we set a sendId in the record so that the record won't get sent again by another process */
			String sendID = "Sending-" + KeyMaker.generate();
			
			/* we're updating all contact events which could conceivably recieve the communication 
			 * this is done so that manual sends cannot pick up records which are currently involved
			 * in an automated send, and vice-versa.
			 */
			GenRow u = new GenRow();
			u.setTableSpec("ContactEvents");
			/* contact event params */
			u.setParameter("ON-Status", "Active");
			u.setParameter("ON-EventCompleted","NULL+N"); 	
			u.setParameter("ON-SendStatus","NULL");
			
			/* campaign params */
			u.setParameter("ON-ContactEventEvent.EventCampaign.Type", "Email");
			u.setParameter("ON-ContactEventEvent.EventCampaign.Status", "Active");
			
			/* event params */
			u.setParameter("ON-ContactEventEvent.Status","Active");
			u.setParameter("ON-ContactEventEvent.EventDate", "<NOW");
			u.setParameter("ON-ContactEventEvent.EventFinishDate", ">NOW");			
			u.setParameter("ON-ContactEventEvent.SendMethod", "Automatic");
			
			u.setParameter("ON-ContactEventEvent.NextSendDate|4","<NOW");
			
			u.setParameter("ON-ContactEventEvent.NextSendDate^3|4","NULL");
			u.setParameter("ON-ContactEventEvent.SendTime^3|4","<NOW+NULL");
			
			u.setParameter("ON-ContactEventEvent.TemplateID|1","!EMPTY");
			u.setParameter("ON-ContactEventEvent.NewsletterCMSID|1","!EMPTY");
			
			/* event bean params */
			u.setParameter("ON-ContactEventEventBean.SendDate","<NOW+NULL");
			u.setParameter("ON-ContactEventEventBean.Status","Active");
			
			/* if the send restriction is send after, all is well, but if it's send only at then it mustn't 
			 * already be sent
			 */
			u.setParameter("ON-ContactEventEventBean.SendRestriction|2","Send After");
			u.setParameter("ON-ContactEventEventBean.SendRestriction^1|2","Send Only At");
			u.setParameter("ON-ContactEventEventBean.SendStatus^1|2","!Sent+NULL");

			// only update the last 3 months of data
			u.setParameter("ON-ModifiedDate", "LAST3MONTHS+TODAY");

			/* set the sendID */
			u.setParameter("SendStatus", sendID);			
			u.doAction(GenerationKeys.UPDATEALL);
			
			if(logger.isTraceEnabled()) {
				logger.trace(u.getStatement());
			}
			
			u.clear(); 
			u.setTableSpec("EventBeans"); 
			u.setParameter("ON-EventBeanEvent.EventContactEvent.SendStatus",sendID);
			u.setParameter("SendStatus","Sent"); 
			u.doAction(GenerationKeys.UPDATEALL);
			
			if(logger.isTraceEnabled()) {
				logger.trace(u.getStatement());
			}
			
			/* find all events which are active and are available for automatic sending */
			e.getResults();
			
			s.setViewSpec("ContactEventView");
			s.setConnection(this.getConnection());
			
			GenRow req = null;
			
			UserBean userBean = new UserBean();
			userBean.setViewSpec("UserView");
			userBean.setConnection(getConnection());
			userBean.setParameter("UserID", systemuserid);
			userBean.doAction(ActionBean.SELECT);
			
			Map<String, Integer> results = new HashMap<String, Integer>();
			
			while(e.getNext()) 
			{	
				debug("Processing event: "+e.getData(EVENTID) + " " + e.getString("Name"));
				
				/* find all contact events which have been queued for sending with this process. */
				s.clear();
				s.setParameter(MASTEREVENTID,e.getData(EVENTID));
				s.setParameter("Status","Active");
				s.setParameter("SendStatus",sendID);
				s.doAction(GenerationKeys.SEARCH);
				s.getResults(); 
				
				/* only instantiate the emailer if we actually have results */
				if(s.getNext())
				{
					results.clear(); 
					debug("Found events, creating emailer");
					/* create a 'requestrow' object to be used with emailer. this must be new each time we change the template. */
					req = new GenRow(); 
					req.setParameter(Emailer.OFFLINEMODE,Emailer.OFFLINEMODECONTACT);
					if(e.isSet("NewsletterCMSID"))
					{
						req.setParameter("CMSID",e.getData("NewsletterCMSID"));
					} else { 
						req.setParameter("TemplateID",e.getData("TemplateID"));
					}
					req.setParameter("CampaignID", e.getData("CampaignID"));
					req.setParameter(ActionBean.CURRENTUSERID, systemuserid);
					req.setParameter("actiontype", "send");
					req.setParameter("NoteTypeDescription","CampaignAutomatic");
					
					/* if we don't set the current user emailer will break. */
					req.putInternalToken(SessionKeys.CURRENT_USER,userBean);

					emailer = EmailerFactory.getEmailer(req, InitServlet.getConfig().getServletContext());
					emailer.getMailerBean().connect();
					do {
						String status = emailer.generateSendAndSave(s.getData("ContactID")); 
						setSent(s, status, emailer.getSpecificContextString("NoteID"));
						if(results.containsKey(status)) { 
							int i = results.get(status); 
							i++; 
							results.put(status, i); 
						} else { 
							results.put(status, 1);
						}
					} while (s.getNext());
					
					/* send notification email ? */
					debug("\r\n\r\nSent Results\r\n\r\n");
					for(Map.Entry<String, Integer> me: results.entrySet()) { 
						debug(me.getKey() + ": " + me.getValue() + "\r\n");
					}
				}
				/* update the next automatic send date/time for this event. */
				setNextSend(e);
			}
		} catch (Exception ex) {
			debug("Error in email sending "); 
			debug(ActionBean.writeStackTraceToString(ex)); 
			error(ex);
		} finally {
			s.close();
			try
            {
               if(emailer != null) 
            	   emailer.getMailerBean().close();
            }
            catch(Exception te)
            {
            }
		}
	}
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:MM:ss");
	private void setNextSend(final GenRow row)
	{
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR, row.getInt("SendInterval"));

		GenRow update = new GenRow();
		update.setTableSpec(EVENTS);
		update.setParameter(EVENTID,row.getData(EVENTID));
		update.setParameter("NextSendDate", sdf.format(c.getTime()));
		update.doAction(GenerationKeys.UPDATE);
	}
	
	
	/** 
	 * Update a contact event record with it's sent status. 
	 * Set the 'SendMethod' field to automatic so that it may be used in reporting. 
	 * @param row
	 * @param status
	 */
	private void setSent(final GenRow row, final String status, final String noteID) {
		GenRow update = new GenRow();
		update.setConnection(getConnection());
		update.setTableSpec(CONTACTEVENTS);
		update.setParameter(EVENTID,row.getData(EVENTID));
		update.setParameter("SendStatus", status);
		update.setParameter("SendMethod","Automatic");
		update.setParameter("SendEmailID",noteID);
		update.setParameter("SendDate","NOW");
		update.setParameter("EventCompleted","Y");
		update.doAction(GenerationKeys.UPDATE);
	}
	
	private void setLoaded(final GenRow row) {
		GenRow update = new GenRow();
		update.setConnection(getConnection());
		update.setTableSpec("EventBeans");
		update.setParameter("EventBeanID", row.getData("EventBeanID"));
		update.setParameter("Loaded", "Y");
		update.doAction(GenerationKeys.UPDATE);
	}
	
	private void getQuestionSearch(GenRow bean) {
        String refineKey = bean.getString("-refineKey").length() > 0? bean.getString("-refineKey") :  "#search";
        String questionKey = "ContactQuestionAnswer" + refineKey;
        if (bean.getString(questionKey + ".QuestionID").length() == 0) {
        	questionKey = "-ContactQuestionAnswer" + refineKey;
        }
        if (bean.getString(questionKey + ".QuestionID").length() > 0) {
        	// we need to do this as GenRow can't seem to handle this query
        	
        	//System.out.println("doing contact answer search");
        	//System.out.println(bean.toString());
        	//System.out.println("end bean");
        	GenRow temp = new GenRow();
        	temp.setTableSpec("answers/ContactAnswers");
        	temp.setConnection(getConnection());
        	temp.setParameter("-select0","ContactID");
        	temp.setParameter("QuestionID",bean.getString(questionKey + ".QuestionID"));
           	bean.remove(questionKey + ".QuestionID");
        	
        	String[] t = bean.getParameterValues(questionKey + ".Answer|ANSWERVL");
        	if (t == null && bean.getString(questionKey + ".Answer|ANSWERVL").length() > 0) {
        		t = new String[1];
        		t[0] = bean.getString(questionKey + ".Answer|ANSWERVL");
        	}
        	int ans = 0;
        	if(t != null) {
        		for(String ts: t) {
        			temp.setParameter("Answer|"+(ans++), ts);
        		}
        	}
           	bean.remove(questionKey + ".Answer|ANSWERVL");

        	String ts = StringUtils.trimToNull(bean.getParameter(questionKey + ".Answer"));
        	if(ts != null) {
        		temp.setParameter("Answer|"+(ans++), ts);
        	}
           	bean.remove(questionKey + ".Answer");
        	
        	if (bean.getString(questionKey + ".AnswerDate|ANSWERVL").length() > 0) {
        		System.out.println(questionKey + ".AnswerDate|ANSWERVL");
        		temp.setParameter("AnswerDate",bean.getString(questionKey + ".AnswerDate|ANSWERVL"));
            	if (!questionKey.startsWith("-")){
            		bean.setParameter("-" + questionKey + ".AnswerDate|ANSWERVL", bean.getParameter(questionKey + ".AnswerDate|ANSWERVL"));
                	bean.remove(questionKey + ".AnswerDate|ANSWERVL");
            	}
        	}

			if(!temp.isSet("Answer|0") && !temp.isSet("AnswerDate")) {
				temp.setParameter("Answer","!NULL+!EMPTY");
			}
        	temp.doAction("search");

        	temp.getResults();
        	if(temp.getNext()) {
        		StringBuilder sb = new StringBuilder();
        		boolean omit = "omit".equals(bean.getString("-" + questionKey)); 
        		int trel = 0; 
        		do {
        			if(omit) {
        				if (sb.length() > 0) sb.append(";");
        				sb.append("!").append(temp.getString("ContactID"));
        			} else {
        				if (sb.length() > 0) sb.append("+");
        				sb.append(temp.getString("ContactID"));
        			}
        		} while(temp.getNext());
        		
				bean.setParameter("ContactID", sb.toString());
        	} else {
        		bean.setParameter("ContactID","NOANSWERS");
        	}
        	temp.close();
        }
		
	}

	/**
	 * This is taken from /crm/cms/campaignmanager/actions/loadaction.jsp There
	 * is a sister jsp page - unloadaction.jsp - but as this would not be called
	 * automatically leaving it's functionality as-is would be ok.
	 * 
	 * @param bean
	 */
	private void processResults(final GenRow bean) {
		GenRow row = new GenRow();
		if (bean.getData("Bean") == null || bean.getData("Bean").length() == 0) return;
		try {
			row.setConnection(getConnection());
			row.parseRequest(bean.getData("Bean"));
			//getQuestionSearch(row);
			/* we only want to ever load records that are in one of the assigned campaign groups of the campaign */
			row.setParameter("ContactContactGroups.ContactGroupCampaignGroup.CampaignID",bean.getData("CampaignID"));
			row.doAction(GenerationKeys.SEARCH);
			row.getResults();

			/*
			 * //allocation code is not needed for email marketing campaigns.
			 * Map<String, Integer> allocationmap = getAllocations(request,
			 * bean.getString("EventBeanID")); Set<String> keyset =
			 * allocationmap.keySet(); String[] keys = null; if(keyset!=null){ keys
			 * = new String[keyset.size()]; keyset.toArray(keys); }
			 */
	
			String primarykey = row.getTableSpec().getPrimaryKeyName();
			String keyvalue = null;
			String contactcampaignid = null;
			String repuserid = null;
			Date eventdate = getEventDate(getConnection(), bean.getString(EVENTID));
	
			while (row.getNext()) {
				keyvalue = row.getData(primarykey);
				// allocation code is not needed for email marketing campaigns.
				// repuserid = getNextAllocation(allocationmap, keys);
				if (!"Y".equals(bean.getString("Omit"))) {
					contactcampaignid = insertContactCampaign(getConnection(),
							primarykey, keyvalue, bean.getString("CampaignID"),
							systemuserid, bean.getString(EVENTID));
					if (contactcampaignid.length() > 0)
						insertContactCampaignEvent(getConnection(),
								contactcampaignid, bean.getString(EVENTID),
								bean.getString("BeanID"), repuserid, systemuserid,
								bean.getString("Attempts"), eventdate, "Active", BEANID);
				} else {
					contactcampaignid = deleteContactCampaign(getConnection(),
							primarykey, keyvalue, bean.getString("CampaignID"),
							systemuserid);
					if (contactcampaignid.length() > 0)
						insertContactCampaignEvent(getConnection(),
								contactcampaignid, bean.getString(EVENTID),
								bean.getString(BEANID), repuserid, systemuserid,
								bean.getString("Attempts"), eventdate, "Inactive",REMOVALBEANID);
				}
			}
		} catch (Exception e) {
			debug("Error in email processResults " + bean.getString("BeanID") + " " + e.getMessage()); 
			logger.error("Error in email processResults {} {}", bean.getString("BeanID"), e.getMessage()); 
		} finally {
			row.close();
		}
	}

	public String getProcessName() {
		return "Campaign Manager";
	}

	protected void processItem() {

	}

	protected void finish() {
		// for (Map.Entry<String, UserAlert> useralert : alertusers.entrySet())
		// {
		// sendUserEmail(useralert.getValue());
		// }
	}

	/*
	 * protected void sendUserEmail(UserAlert alert) {
	 * debug("sending email to "+ alert.getEmail()); try{
	 * 
	 * RowBean context = new RowBean(); context.put("URLHome", urlhome);
	 * context.put("EmailAlertsURLHome", emailalertsurlhome);
	 * context.put("alert", alert);
	 * 
	 * String text = generateEmail(context, alerttext); String html =
	 * generateEmail(context, alerthtml);
	 * 
	 * ServletConfig sc = InitServlet.getConfig(); if(sc!=null &&
	 * alert.getEmail().length()!=0) { MailerBean mailer = new MailerBean();
	 * mailer.setServletContext(sc.getServletContext());
	 * mailer.setFrom(defaultFrom); mailer.setTo(alert.getEmail());
	 * mailer.setSubject("Status Alerts"); mailer.setTextbody(text);
	 * mailer.setHtmlbody(html); if(config.getPretend()) { debug("email to "+
	 * alert.getEmail() + "\r\n " + html); } else { String response =
	 * mailer.getResponse(); debug("email to "+ alert.getEmail() + " " +
	 * response); } }
	 * 
	 * }catch(Exception e){ error(e); } }
	 * 
	 * protected String generateEmail(TableData context, String script) {
	 * StringWriter text = new StringWriter(); try{ StringBuffer roottemplate =
	 * new StringBuffer(); //roottemplate.append("#parse(\"");
	 * //roottemplate.append(DEFAULT_VELOCITY_PATH);
	 * //roottemplate.append(script); //roottemplate.append("\")");
	 * 
	 * VelocityEngine ve = VelocityManager.getEngine(); ve.evaluate( context,
	 * text, "StatusAlerts", roottemplate.toString());
	 * 
	 * text.flush(); text.close();
	 * 
	 * }catch(Exception e){ error(e); } return(text.toString()); }
	 */
	public void close() {
		super.close();
	}

	public void checkConfig() {
		config = CampaignManagerConfig.getCampaignManagerConfig(configfilepath);
	}

	public static String insertContactCampaign(Connection con, String keyname,
			String keyvalue, String campaignid, String userid, String eventid) {
		GenRow row = new GenRow();
		row.setConnection(con);
		row.setTableSpec("ContactCampaigns");
		row.setParameter("-select", "*");
		row.setParameter("CampaignID", campaignid);
		row.setParameter(keyname, keyvalue);
		
		if(!row.isSet("CampaignID") || !row.isSet(keyname))
		{
			/* we don't have the keys to do a select here */
			return "";
		}
		
		row.doAction(GenerationKeys.SELECTFIRST);
		if (!row.isSuccessful()) {
			row.setConnection(con);
			row.setParameter("CreatedBy", userid);
			row.setParameter("ModifiedBy", userid);
			row.setParameter("CreatedDate", "NOW");
			row.setParameter("ModifiedDate", "NOW");
			row.setParameter("Status", "Active");
			row.createNewID();
			row.doAction(GenerationKeys.INSERT);
		} else if (!"Active".equals(row.getData("Status"))
				&& !isExitStatus(con, eventid,
						row.getString("ContactCampaignStatusID"))) {
			row.setConnection(con);
			row.setParameter("Status", "Active");
			row.setParameter("ContactCampaignID",
					row.getData("ContactCampaignID"));
			row.doAction(GenerationKeys.UPDATE);
		} else if (!"Active".equals(row.getData("Status"))) {
			return "";
		}

		return (row.getString("ContactCampaignID"));
	}

	public static String deleteContactCampaign(Connection con, String keyname,
			String keyvalue, String campaignid, String userid) {
		GenRow row = new GenRow();
		row.setConnection(con);
		row.setTableSpec("ContactCampaigns");
		row.setParameter("-select1", "ContactCampaignID");
		row.setParameter("-select2", "Status");
		row.setParameter("CampaignID", campaignid);
		row.setParameter(keyname, keyvalue);
		
		if(!row.isSet("CampaignID") || !row.isSet(keyname))
		{
			/* we don't have the keys to do a select here and could pull back anything. */
			return "";
		}
		
		row.doAction(GenerationKeys.SELECTFIRST);

		if ("Active".equals(row.getData("Status"))) {
			row.setConnection(con);
			row.setParameter("Status", "Inactive");
			row.setParameter("ContactCampaignID",
					row.getData("ContactCampaignID"));
			row.doAction(GenerationKeys.UPDATE);
			return (row.getString("ContactCampaignID"));
		}
		return "";
	}

	public static void insertContactCampaignEvent(Connection con,
			String contactcampaignid, String eventid, String beanid,
			String repuserid, String userid, String attempts, Date eventdate,
			String status, String beanField) {
		GenRow row = new GenRow();
		row.setConnection(con);
		row.setTableSpec("ContactEvents");
		row.setParameter("-select1", EVENTID);
		row.setParameter("ContactCampaignID", contactcampaignid);
		row.setParameter(MASTEREVENTID, eventid);
		row.doAction(GenerationKeys.SELECTFIRST);
		
		if (!row.isSuccessful()) {
			row.setParameter("EventCompleted","N");
			row.setParameter("CreatedBy", userid);
			row.setParameter("CreatedDate", "NOW");
			row.setAction(GenerationKeys.INSERT);
			row.createNewID();
			row.setParameter("EventDate", eventdate);
			row.setParameter("RepUserID", repuserid);
			row.setParameter("Attempts", attempts);
			row.setParameter("Status", status);
		} else { 
			row.setAction(GenerationKeys.UPDATE);
			/* we don't want to update inactive (ie, omitted) records to active */
			if(row.getString("Status").equals("Inactive"))
			{
				return;
			}
		}
		row.setParameter(beanField, beanid);
		row.setParameter("ModifiedBy", userid);
		row.setParameter("ModifiedDate", "NOW");
		row.doAction();
	}

	/*
	 * // For allocating reps to a contact event, not using it so not testing
	 * it... Map getAllocations(Connection con, String eventbeanid){ GenRow map
	 * = new GenRow();
	 * 
	 * GenRow row = new GenRow(); row.setViewSpec("EventBeanAllocationView");
	 * row.setConnection(con); row.setParameter("EventBeanID", eventbeanid);
	 * row.doAction("search"); row.getResults(true); boolean hascounts = false;
	 * int amount = 0; while(row.getNext()){ amount = row.getInt("Amount");
	 * if(amount>0){ hascounts = true; } map.put(row.getString("RepUserID"), new
	 * Integer(amount)); } row.close(); if(hascounts){ map.put("-hascounts", new
	 * Integer(1)); }else{ map.put("-hascounts", new Integer(0)); } return(map);
	 * }
	 * 
	 * String getNextAllocation(Map<String, Integer> map, String[] keys){
	 * if(keys!=null && map!=null){ boolean hascounts =
	 * (Integer)map.get("-hascounts").intValue() == 1; if(hascounts){ Integer
	 * amount = null; int amountint = 0; for(int i=0; i<keys.length; i++){
	 * amount = (Integer)map.get(keys[i]); if(amount!=null){ amountint =
	 * amount.intValue(); if(amountint!=0){ map.put(keys[i], new
	 * Integer(amountint - 1)); return(keys[i]); } } } } } return(null); }
	 */
	public static Date getEventDate(Connection con, String eventid) {
		GenRow row = new GenRow();
		row.setConnection(con);
		row.setViewSpec(EVENTVIEW);
		row.setParameter(EVENTID, eventid);
		row.doAction(GenerationKeys.SELECT);
		try { 
			return (row.getDate("EventDate"));
		} catch (IllegalConfigurationException ice) { 
			/* Genrow will petulantly throw an Exception if the node 
			 * is null, instead of returning (Date)null. What a douche. 
			 */
			return null;
		}
	}

	public static boolean isExitStatus(Connection con, String eventid,
			String contactCampaignStatusID) {
		if (contactCampaignStatusID == null
				|| contactCampaignStatusID.length() == 0)
			return false;
		GenRow row = new GenRow();
		row.setConnection(con);
		row.setViewSpec("ContactCampaignStatusView");
		row.setParameter("ContactCampaignStatusID", contactCampaignStatusID);
		row.doAction(GenerationKeys.SELECT);

		if (row.isSuccessful()) {
			GenRow linked = new GenRow();
			linked.setViewSpec("LinkedStatusView");
			linked.setConnection(con);
			linked.setColumn(EVENTID, eventid);
			linked.setColumn("StatusID", row.getString("StatusID"));
			linked.setColumn("ExitEvent|1", "Yes");
			linked.setColumn("ExitCampaign|1", "Yes");
			linked.doAction(GenerationKeys.SELECTFIRST);

			if (linked.isSuccessful()
					|| linked.getString("LinkedStatusID").length() > 0)
				return true;

		}
		return false;
	}

}
