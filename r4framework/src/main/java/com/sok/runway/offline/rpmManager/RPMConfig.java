package com.sok.runway.offline.rpmManager;

import java.io.File;
import javax.servlet.ServletContext;
import org.dom4j.*;

import com.sok.runway.*;
import com.sok.runway.offline.*;
import com.sok.framework.*;

public class RPMConfig extends AbstractDaemonWorkerConfig
{
   protected static String fullpath = null;
   protected static RPMConfig rpmconfig = null;
   protected static final String filepath = "/WEB-INF/rpm.xml";
   
   public static RPMConfig getRPMConfig(String parentpath)
   {
      if(rpmconfig == null || rpmconfig.isModified())
      {
         loadConfig(parentpath);
      }
      return(rpmconfig);
   }

   protected static synchronized void loadConfig(String parentpath)
   {
      if(rpmconfig == null || rpmconfig.isModified())
      {
         if(fullpath == null)
         {
            StringBuffer sb = new StringBuffer(parentpath);
            sb.append(filepath);
            fullpath = sb.toString();
         }
         rpmconfig = new RPMConfig(fullpath);
      }
   }

   public RPMConfig(String path)
   {
      super(path);
   }

}
