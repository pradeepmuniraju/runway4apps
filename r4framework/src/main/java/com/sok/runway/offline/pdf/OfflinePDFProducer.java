package com.sok.runway.offline.pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.RowBean;
import com.sok.framework.RunwayUtil;
import com.sok.framework.StringUtil;
import com.sok.framework.servlet.ResponseOutputStream;
import com.sok.framework.servlet.ServletRequest;
import com.sok.runway.OfflineProcess;
import com.sok.runway.OfflineProcessMonitor;
import com.sok.runway.OfflineProcessor;
import com.sok.runway.URLFilter;
import com.sok.runway.UserBean;
import com.sok.runway.autopilot.AutoPilot;
import com.sok.runway.crm.cms.properties.pdf.PropertyPDFServlet;
import com.sok.runway.crm.cms.properties.pdf.PropertyPDFServlet.PDFVariant;
import com.sok.runway.offline.OfflineNotifier;
import com.sok.runway.offline.ThreadManager;
import com.sok.service.crm.UserService;

public class OfflinePDFProducer extends OfflineProcess implements OfflineNotifier {
	
	private static final Logger logger = LoggerFactory.getLogger(OfflinePDFProducer.class);
	final UserBean user; 
	final ServletRequest req;
	final HttpSession session;
	
	private String pdfURL = null;
	private long pdfLen = -1;
	
	final PDFVariant variant;
	final String output;
	
	private String[] productID;
	
/*
	 public OfflinePDFProducer(HttpServletRequest request, ServletContext context, boolean autostart)
	 {
		  super(request, context);
		  user = UserService.getInstance().getCurrentUser(request);
		  logger.debug("new OfflinePDFProducer(request, context, autostart=[{}]);", autostart);
		  req = new ServletRequest(request, "/property.pdf");
		  session = request.getSession();
		  if(autostart)
		  {
				//initProcess(request, context);
				start();
		  }
	 }	 
	 */
	public OfflinePDFProducer(HttpServletRequest request, ServletContext context)
	{
		super(request, context);
		user = UserService.getInstance().getCurrentUser(request);
		logger.debug("new OfflinePDFProducer(request, context);");
		variant = PDFVariant.fromString(request.getParameter("-variant"));
		if (!RunwayUtil.isAdvancedModePDF(request) || "Runway".equals(request.getParameter("-category"))) {			
			 // 12-Apr-2013 NI: output = PDFVariant.SinglePackage == variant ? "/propertypdf.zip": "/property.pdf";
		    output = (PDFVariant.SinglePackage == variant || PDFVariant.DeveloperPdf == variant) ? "/propertypdf.zip": "/property.pdf";
		} else {
			output = "/aproperty.pdf";
	    }
	    req = new ServletRequest(request, output);
		session = request.getSession();
		
		productID = request.getParameterValues("ProductID");
		
		URLFilter.exceptionLogger(this, request, null);
		//initProcess(request, context);
		start();
	}
	private Connection con = null; 
	public Connection getConnection() {
		if(con != null) return con;
	    
		RowBean row = new RowBean();
	    ActionBean.connect(row);
	    
		return (con = row.getConnection());
	}
	/*
	public OfflinePDFProducer(String query, HttpServletRequest request, ServletContext context)
	{
		super(query, request, context);
		user = UserService.getInstance().getCurrentUser(request);
		logger.debug("new OfflinePDFProducer(query=[{}], request, context);", query);
		req = new ServletRequest(request, "/property.pdf");
		session = request.getSession();
		//initProcess(request, context);
		start();
	}

	public OfflinePDFProducer(RowBean bean, HttpServletRequest request, ServletContext context)
	{
		super(bean, request, context);
		user = UserService.getInstance().getCurrentUser(request);
		logger.debug("new OfflinePDFProducer(bean, request, context);");
		req = new ServletRequest(request, "/property.pdf");
		session = request.getSession();
		//initProcess(request, context);
		start();
	}	
	*/
	
	
	@Override
	public int getProcessSize() {
		return 1;
	}
	
	int processed = 0;
	int error = 0;
	@Override
	public int getProcessedCount() {
		return processed;
	}

	@Override
	public int getErrorCount() {
		return error;
	}
	
	private static final String file_Date_Format = "yyyy_MM_dd_HH_mm";


	@Override
	public void execute() {
	      StringBuffer fileName = new StringBuffer();
	      fileName.append("property_");
	      DateFormat dateFormat = new SimpleDateFormat(file_Date_Format);
	      fileName.append(dateFormat.format(new java.util.Date()))
	      	.append("_").append(KeyMaker.generate(5));
	      if(variant.equals(PDFVariant.ExcelPackageReport) || variant.equals(PDFVariant.ExcelApartmentReport)|| variant.equals(PDFVariant.ExcelRealEstateReport) || variant.equals(PDFVariant.ExcelLandReport)) {
					
	    	  fileName.append(".xls");//Adding excel file extension if it's excel file
	      }else if(output.endsWith("pdf")){
	    	  fileName.append(".pdf");
	      }else{
	    	  fileName.append(".zip");
	      }
	      
	      File temp = new File(new File(session.getServletContext().getRealPath(AutoPilot.AUTOPILOT_FILE_PATH)), fileName.toString());

	      ResponseOutputStream responseOS = null;
	      FileOutputStream fos = null;
	      try {
	    	  fos = new FileOutputStream(temp);
	    	  responseOS = new ResponseOutputStream(session, fos, output);
	    	  responseOS.service(getConnection(), req);
	    	  logger.debug("Should have completed file output");
	      } catch (Exception e) {
	    	  logger.error("Error doing file output from pdf producer", e);
	    	  error = 1;
	    	  throw new RuntimeException(e);
	      } finally {
	    	  if (fos != null) {
	    		  try {
	    			  logger.debug("flushing FileOutputStream");
	    			  fos.flush();
	    			  logger.debug("closing FileOutputStream");
	    			  fos.close();
	    		  } catch (Exception ex) {
	    			  logger.error("Error doing file output from pdf producer", ex);
	    		  }
	    	  } else {
	    		  logger.error("FileOutputStream was null");
	    	  }
	    	  if (responseOS != null) {
	    		  responseOS.close();
	    	  }
	      }
	      if(temp.length() == 0) {
	    	  logger.debug("file len was 0 for file " + AutoPilot.AUTOPILOT_FILE_PATH + fileName);
	    	  error = 1;
	    	  throw new RuntimeException("Failed to output file");
	      }
	      processed = 1;
		pdfLen = temp.length();
		pdfURL = InitServlet.getSystemParams().getProperty("URLHome")  + AutoPilot.AUTOPILOT_FILE_PATH + fileName;
		
		logger.info("PDF Result: {} {}", pdfLen, pdfURL);
	}

	@Override
	public String getStatusMailBody() {
		
		StringBuilder status = new StringBuilder()
			.append(" Runway offline process - PDF Generation Notification\r\nr\n")
			.append(new SimpleDateFormat("d MMM yyyy - h:mma").format(new java.util.Date()))
			.append("\r\n");
		
		if(pdfURL == null) {
			status.append("Sorry, we weren't able to make this pdf. Please advise the support team");
		} else {
			status.append("Your pdf has been generated and is available at the following url : \r\n\r\n").append(pdfURL);
		}
		return status.toString();
	}
	
	@Override
	public String getStatusMailHtmlBody() {
		StringBuilder url = new StringBuilder().append("/crm/autopilot/emails/offlinepdf.jsp?");

		if(pdfURL == null) {
			url.append("error=").append(StringUtil.urlEncode("Sorry, we weren't able to make this pdf. Please advise the support team"));
		} else {
			url.append("PDFURL=").append(StringUtil.urlEncode(pdfURL));
		}
		if (productID != null || productID.length > 0) {
			for (int s = 0; s < productID.length; ++s) {
				url.append("&ProductID=").append(productID[s]);
			}
		}
		url.append("&-variant=").append(variant.toString());
			
		ResponseOutputStream responseOS = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			responseOS = new ResponseOutputStream(session, baos, url.toString());
			responseOS.service(getConnection());
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (baos != null) {
				try {
					baos.flush();
					baos.close();
				} catch (Exception ex) {
					throw new RuntimeException(ex);
				}

			}
			if (responseOS != null) {
				responseOS.close();
			}

		}
		return baos.toString();
	}

	@Override
	public String getStatusMailSubject() {
		return "PDF Generation Notification";
	}

	@Override
	public String getStatusMailRecipient() {
		return user.getEmail();
	}
}
