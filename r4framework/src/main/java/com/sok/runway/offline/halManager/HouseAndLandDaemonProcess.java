package com.sok.runway.offline.halManager;

import java.io.File;

import com.sok.runway.*;
import com.sok.framework.*;
import com.sok.runway.offline.*;
/*
 * To run it put this in your web.xml, you may need to use a daemonClass number which is not in use.
 * You will need a statusalert.xml in your WEB-INF
<servlet>
    <servlet-name>Initiate</servlet-name>
    <servlet-class>com.sok.framework.InitServlet</servlet-class>
    <load-on-startup>0</load-on-startup>
     <init-param>
       <param-name>daemonClass0</param-name>
       <param-value>com.sok.runway.offline.statusAlert.TransmissionsDaemonProcess</param-value>
     </init-param>      
</servlet>
*/

public class HouseAndLandDaemonProcess extends AbstractDaemonWorkerController
{
   
   public static String NAME = "House and Land Manager";
   protected HouseAndLandManager hlm = null;
   
   public HouseAndLandDaemonProcess(DaemonFactory daemonFactory)
   {
      super(daemonFactory);
   }
   
   protected void initWorker()
   {
      hlm = HouseAndLandManager.getInstance(InitServlet.getRealPath());
      worker = hlm;
   }

   public String getName()
   {
      return (NAME);
   }
   
   public static void main(String[] args)
   {

   }
}
