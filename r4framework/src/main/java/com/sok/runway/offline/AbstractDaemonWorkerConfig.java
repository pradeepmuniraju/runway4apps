package com.sok.runway.offline;

import java.io.File;
import javax.servlet.ServletContext;
import org.dom4j.*;

import com.sok.runway.*;
import com.sok.framework.*;
import com.sok.framework.generation.*;

public abstract class AbstractDaemonWorkerConfig extends XMLSpec implements DaemonWorkerConfig
{
   protected int interval = 0;
   protected boolean debug = false;
   protected boolean pretend = false;
   protected String debugaddress = null;
   protected boolean hastimer = false;
   protected WorkerTimer timer = null; 
   protected static final String _interval = "interval";
   protected static final String _debug = "debug";
   protected static final String _pretend = "pretend";      
   protected static final String _debugAddress = "debugAddress"; 
   

   public AbstractDaemonWorkerConfig(String path)
   {
      super(path);
      if(super.document != null)
      {
         Element root = super.document.getRootElement();
         if(root != null)
         {
            interval = Integer.parseInt(root.attributeValue(_interval));
            debug = "true".equals(root.attributeValue(_debug));
            pretend = "true".equals(root.attributeValue(_pretend));
            debugaddress = root.attributeValue(_debugAddress);
         }
         else
         {
            throw new IllegalConfigurationException("Document has no root element - "
                  + path);
         }
      }
      else
      {
         throw new IllegalConfigurationException("Document cannot be loaded - " + path);
      }
   }

   public int getInterval()
   {
      return(interval);
   }
   
   public boolean getDebug()
   {
      return(debug);
   }
   
   public boolean getPretend()
   {
      return(pretend);
   }   
   
   public String getDebugAddress()
   {
      return(debugaddress);
   }   
   
   public boolean hasWorkerTimer()
   {
      return(hastimer);
   }
   
   public WorkerTimer getWorkerTimer()
   {
      return(timer);
   }
}
/* example config file
 * 
<processconfig interval="120" debug="false" debugAddress="shinytoaster@gmail.com">
<!-- interval - wait seconds -->
<!-- debug="true|false", debugAddress used for testing -->
<!-- this file automatically reloads -->
</processconfig>
*/