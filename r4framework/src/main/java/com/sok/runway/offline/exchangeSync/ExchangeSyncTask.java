package com.sok.runway.offline.exchangeSync;

import java.util.concurrent.ExecutorService;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.runway.UserBean;
import com.sok.runway.offline.AbstractDaemonWorker;
import com.sok.service.crm.UserService;

public class ExchangeSyncTask extends AbstractDaemonWorker {
	private static final Logger logger = LoggerFactory.getLogger(ExchangeSyncTask.class);
	private static final String DEFAULT_VELOCITY_PATH = "/cms/emails/offlinealerts/";

	protected String alerttext = "generic/generic_text.vt";
	protected String alerthtml = "generic/generic.vt";

	private static ExchangeSyncTask exchange;

	protected String emailalertsurlhome = null;

	// private Vector<QueueItem> queue = new Vector<QueueItem>();

	// private String realestateUser = "";
	// private String realestatePassword = "";

	protected String dbconn;
	protected ServletContext context;
	protected String contextpath;
	protected String contextname = null;
	
	//private ExecutorService executor = null;
	int 	threads = 5;
	int 	maxThreads = 5 * 5;
	
	private boolean isLoadingIndex = false;
	private String useExchange = "false";
	private String useHost = "";

	public ExchangeSyncTask(String configfilepath) {
		super(configfilepath);

		if (exchange == null)
			exchange = this;
	}

	public static ExchangeSyncTask getInstance() {
		if (exchange == null)
			new ExchangeSyncTask("");
		return exchange;
	}

	/**
	 * The init method is called when the worker is woken up from sleep. Any
	 * actions that need to be performed at that interval should be defined
	 * here.
	 */
	protected void init() {
		urlhome = InitServlet.getSystemParam("URLHome");
		useExchange = InitServlet.getSystemParam("useExchange");
		useHost =  InitServlet.getSystemParam("useExchangeHost");
		
		if (!"true".equals(useExchange) || StringUtils.isBlank(useHost)) return;

		/*
		if (executor == null) {
			try {
				threads = Integer.parseInt(InitServlet.getSystemParam("ExchangeSyncThreads", String.valueOf(threads)));
				maxThreads = threads * 5;
			} catch (NumberFormatException nfe) {
			}

			executor = com.sok.runway.offline.ThreadManager.createThreadExecutor(
					"ExchangeSyncExecutor", threads); // Executors.newFixedThreadPool(threads);

		}
		*/
		logger.info("Initializing Exchange Sync Task");
		if (!isLoadingIndex) {
			isLoadingIndex = true;
			String userID = InitServlet.getSystemParam("useExchangeSync");;
			
			final GenRow user = new GenRow();
			user.setTableSpec("Users");
			user.setParameter("-select0", "UserID");
			user.setParameter("-select1", "Username");
			user.setParameter("EmailProtocol", "Exchange");
			user.setParameter("EmailUsername", "!NULL;!EMPTY");
			user.setParameter("EmailPassword", "!NULL;!EMPTY");
			if (StringUtils.isNotBlank(userID)) user.setParameter("UserID", userID);
			user.doAction("search");
			
			user.getResults();
			
			while (user.getNext()) {
				try {
					logger.warn("Sync for {} {}", user.getData("UserID"), user.getData("Username"));
					UserBean userbean = UserService.getInstance().getSimpleUser(user.getConnection(), user.getData("UserID"));
					userbean.syncAppointments(false, true);
					userbean.syncMail(false);
				} catch (Exception e) {
					logger.error("Sync error for {} {}", user.getData("UserID"), e);
				}
			}
			user.close();
			isLoadingIndex = false;
		}
	}
	
	protected void finalize() {
		/*
		if (executor != null) {
			com.sok.runway.offline.ThreadManager.shutdownExecutor(executor);
		}
		*/
	}

	@Override
	public void checkConfig() {
		config = ExchangeSyncConfig.getExchangeSyncConfig(configfilepath);
	}

	@Override
	public String getProcessName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void finish() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void processItem() {
		// TODO Auto-generated method stub
		
	}


}