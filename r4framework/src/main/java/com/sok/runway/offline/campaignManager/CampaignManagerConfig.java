package com.sok.runway.offline.campaignManager;

import java.io.File;
import javax.servlet.ServletContext;
import org.dom4j.*;

import com.sok.runway.*;
import com.sok.runway.offline.*;
import com.sok.framework.*;

public class CampaignManagerConfig extends AbstractDaemonWorkerConfig
{
   protected static String fullpath = null;
   protected static CampaignManagerConfig campaignmanagerconfig = null;
   protected static final String filepath = "/WEB-INF/campaignmanager.xml";
   
   public static CampaignManagerConfig getCampaignManagerConfig(String parentpath)
   {
	  if(campaignmanagerconfig == null || campaignmanagerconfig.isModified())
      {
         loadConfig(parentpath);
      }
      return(campaignmanagerconfig);
   }

   protected static synchronized void loadConfig(String parentpath)
   {
	   if(campaignmanagerconfig == null || campaignmanagerconfig.isModified())
      {
         if(fullpath == null)
         {
            StringBuffer sb = new StringBuffer(parentpath);
            sb.append(filepath);
            fullpath = sb.toString();
         } 
         campaignmanagerconfig = new CampaignManagerConfig(fullpath);
      }
   }

   public CampaignManagerConfig(String path)
   {
	   super(path);
   }

}
/* example config file
 * 
<campaignmanager interval="3600" debug="false" debugAddress="michael.dekmetzian@gmail.com">
<!-- one hour interval = 3600. Probably what you want. -->
<!-- debug="true|false", debugAddress used for testing -->
<!-- this file automatically reloads -->
</campaignmanager>
*/