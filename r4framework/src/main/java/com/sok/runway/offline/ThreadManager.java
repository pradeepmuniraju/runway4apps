package com.sok.runway.offline;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.InitServlet;

/**
 * Thread manager, initially just used to generate id's for use in thread names. 
 * 
 * In future to be used to execute threads and ensure those threads are killed when the instance is stopped via a ThreadExecutor
 * 
 * @author Michael Dekmetzian
 * @date 21 March 2012
 * 
 * Added methods to start Threads, Runnable, Callables that are terminated when the application shuts down.
 * @date 26 March 2012
 * 
 */
public class ThreadManager {

	private static final Logger logger = LoggerFactory.getLogger(ThreadManager.class);
	
	private static AtomicInteger threadId = new AtomicInteger(0);
	
	
	private static int threads = 30;
	static {
		try {
			threads = Integer.parseInt(InitServlet.getSystemParam("ThreadManagerThreads", String.valueOf(threads))); /* if the system property is an empty string, this could throw an exception. */
		} catch (NumberFormatException nfe) { logger.error("Error parsing system property for thread manager threads", nfe); }
	}
	/* for now, we only want a maximum of 10 extra threads running. */
	private static ExecutorService executor =  Executors.newFixedThreadPool(threads);
	static {
		logger.info("Executor Started Up with {} threads", threads);
	}
	public static int getNextThreadId() { 
		return threadId.getAndIncrement();
	}
	
	public static void startThread(Thread t) { 
		logger.debug("Starting new thread with name {}", t.getName());
		executor.submit(t);
		if(logger.isTraceEnabled()) executorStatus();
	}
	
	private static void executorStatus() {
		if(executor instanceof ThreadPoolExecutor) {
			ThreadPoolExecutor tpe = (ThreadPoolExecutor)executor;
			//NB, this is a potentially non-trivial operation so it shouldn't be on unless you really want to know.
			logger.trace("ThreadPoolExecutor activeThreads={}, poolSize={}, queueSize={}", new String[]{ String.valueOf(tpe.getActiveCount()), String.valueOf(tpe.getPoolSize()), String.valueOf(tpe.getQueue().size())});
		} else {
			logger.trace("fyi, not threadpoolexecutor actually {}", executor.getClass().getName());
		}
	}
	
	public static void startThread(Runnable r) { 
		logger.debug("Starting new runnable {}", r.toString());
		executor.submit(r);
		if(logger.isTraceEnabled()) executorStatus();
	}
	
	public static void startThread(Runnable r, String namePrefix) {
		logger.debug("Starting new runnable {} with prefix", r.toString(), namePrefix);
		executor.submit(new Thread(r, namePrefix + getNextThreadId()));
		if(logger.isTraceEnabled()) executorStatus();
	}
	
	public static ExecutorService getThreadExecutor() {
		return executor;
	}
	
	/**
	 * Submit a collection of Callables, execute them all, and once finished return them all as a list in the same order that they were submitted.
	 * All callables must be of the same type, for ex ReportBean, GenRow, or whatever.
	 * @param solvers 
	 * @return
	 */
	public static <T>  List<T> solveCallables(Collection<Callable<T>> solvers, boolean interruptOnError) {
		logger.debug("solveCallables(collection, interruptOnError={}),", String.valueOf(interruptOnError));
		CompletionService<T> ecs = new ExecutorCompletionService<T>(executor);
		List<Future<T>> list = new ArrayList<Future<T>>(solvers.size());
		 for (Callable<T> s : solvers) {
			 logger.trace("adding to completion service ");
			 list.add(ecs.submit(s));
		 }
		logger.trace("waiting for completion");
		 try {
			 int remaining = solvers.size();
			while(remaining > 0) {
				//this will wait until one is complete
				ecs.take();
				remaining--;
				logger.trace("list is not complete, {} left ", remaining);
			 }
			logger.trace("list should be complete, creating out list");
			List<T> outList = new ArrayList<T>(solvers.size());
			for(Future<T> o: list) {
				outList.add(o.get());
			}
			return outList;
		} catch (InterruptedException e) {
			//this will occur if the executor was interrupted, for ex if it's shutting down
			logger.error("thread was interrupted",e);
			for(Future<T> o: list) {
				//if it's running, just kill it.
				o.cancel(interruptOnError);
			}
		} catch (ExecutionException e) {
			//this will occur if, lets say, one of the tasks throws an exception (called on o.get())
			//you could hypothetically catch the exception at that point and do something with it
			logger.error("error getting results",e);
		} 
		return null;
	}
	
	public static <T> Future<T> startCallable(Callable<T> c) {
		logger.debug("Starting new callable {}", c.toString());
		if(logger.isTraceEnabled()) {
			Future<T> ret = executor.submit(c);
			executorStatus();
			return ret;
		}
		return executor.submit(c);
	}
	
	public static void shutdown() {
		logger.info("Shutting down executor");
		executor.shutdown();
		synchronized(managedExecutors) { 
			for(ExecutorService es: managedExecutors.keySet()) {
				shutdownExecutor(es, false);
			}
			managedExecutors.clear();
		}
		try
		{
			executor.awaitTermination(10, TimeUnit.SECONDS);
		}
		catch (InterruptedException ie)
		{
			Thread.currentThread().interrupt();
		}
		logger.info("Finished any waiting for threads, Executor.shutdownNow()");
		executor.shutdownNow();
	}

	private static Map<ExecutorService, String> managedExecutors = Collections.synchronizedMap(new HashMap<ExecutorService, String>());
	
	public static void shutdownExecutor(ExecutorService executor2) {
		shutdownExecutor(executor2, true);
	}
	
	public static Map<ExecutorService, String> getManagedExecutors() {
		return managedExecutors;
	}
	
	private static void shutdownExecutor(ExecutorService executor2, boolean clearMap) {
		 logger.info("Shutting down executor - {}", managedExecutors.get(executor2));
		 executor2.shutdown();
		 try
		 {
			 executor2.awaitTermination(10, TimeUnit.SECONDS);
		 }
		 catch (InterruptedException ie)
		 {
		     Thread.currentThread().interrupt();
		 }
		 logger.info("Finished any waiting for threads, Executor.shutdownNow()");
		 executor2.shutdownNow();
		 if(clearMap) {
			 managedExecutors.remove(executor2);
		 }
	}

	public static ExecutorService createThreadExecutor(String namePrefix,int threads2) {
		logger.debug("createThreadExecutor({}, {})", namePrefix, threads2);
		ExecutorService esc = Executors.newFixedThreadPool(threads2);
		managedExecutors.put(esc, namePrefix + getNextThreadId());
		return esc;
	}
}
