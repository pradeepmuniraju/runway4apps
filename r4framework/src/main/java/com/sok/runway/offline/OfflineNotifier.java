package com.sok.runway.offline;

import java.io.*;

public interface OfflineNotifier {
   
   public String getStatusMailRecipient();
   
   public String getStatusMailBody();
   
   public String getStatusMailHtmlBody();
   
   public String getStatusMailSubject();
   
   public File getStatusAttachment();
   
   public int getProcessSize();
   
   public int getProcessedCount();
   
   public int getErrorCount();
}