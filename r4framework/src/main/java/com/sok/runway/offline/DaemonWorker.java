package com.sok.runway.offline;

public interface DaemonWorker
{
   public boolean processNext();
   
   public int getProcessSize();
   
   public int getProcessedCount();
   
   public int getErrorCount();
   
   public void close();
   
   public DaemonWorkerConfig getWorkerConfig();
   
   public void checkConfig();
  
}
