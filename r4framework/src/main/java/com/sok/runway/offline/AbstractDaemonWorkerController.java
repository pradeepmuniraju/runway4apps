package com.sok.runway.offline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.*;

public abstract class AbstractDaemonWorkerController extends DaemonProcess
{
	private static final Logger logger = LoggerFactory.getLogger(AbstractDaemonWorkerController.class);
   protected DaemonWorker worker = null;
   
   public AbstractDaemonWorkerController(DaemonFactory daemonFactory)
   {
      super(daemonFactory);
   }
   
   protected abstract void initWorker();
   
   public void run()
   {
      try
      {
         initWorker();
         if(worker!=null)
         {
            while (!isKilled())
            {
               worker.checkConfig();
               tryWorker();
               if(worker.getWorkerConfig()!=null)
               {
            	  if(logger.isTraceEnabled()) logger.trace("Worker for {} waiting for {} seconds", this.getName(), worker.getWorkerConfig().getInterval());
                  waitUnlessKilled(worker.getWorkerConfig().getInterval());
               }
               else
               {
                  addError("Worker has no config");
                  break;
               }
            }
         }
         else
         {
            addError("No worker set");
         }
      }
      catch(Exception e)
      {
    	  logger.error("Error in Daemon Worker", e);
         setRunning(false);
         addError(e);
         killProcess();
         if(worker!=null)
         {
            worker.close();
         }
      }      
   }

   protected void tryWorker()
   {
      setRunning(true);
      while (!isKilled() && isRunning())
      {
         try
         {
            while (!isKilled() && isRunning() && !isPaused() && worker.processNext())
            {
               Thread.yield();               
            }
         }
         catch(Exception e)
         {
            addError(e);
         }
         if(!isPaused())
         {
            setRunning(false);
         }
      } 
   }
   
   private void waitUnlessKilled(int seconds)
   {
      if(seconds>=5)
      {
         int count = seconds / 5;
   
         for(int i=0; i<count && !isKilled(); i++)
         {
            wait(5);
         }
      }
      else if(seconds>0 && !isKilled())
      {
         wait(seconds);
      }
   }
   
   private void wait(int seconds)
   {
      try 
      {
         Thread.sleep(seconds * 1000);
      }
      catch (Exception e) {}
   }
   
   public abstract String getName();

   
   public int getProcessSize() {
      return (worker!=null?worker.getProcessSize():0);
   }
   
   public int getProcessedCount() {
      return (worker!=null?worker.getProcessedCount():0);
   }    

   public int getErrorCount() {
      return (worker!=null?worker.getErrorCount():0);
   }   
   
}
