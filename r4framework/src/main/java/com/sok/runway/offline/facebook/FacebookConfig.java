package com.sok.runway.offline.facebook;

import java.io.File;
import javax.servlet.ServletContext;
import org.dom4j.*;

import com.sok.runway.*;
import com.sok.runway.offline.*;
import com.sok.framework.*;

public class FacebookConfig extends AbstractDaemonWorkerConfig
{
   protected static String fullpath = null;
   protected static FacebookConfig facebookconfig = null;
   protected static final String filepath = "/WEB-INF/facebook.xml";
   
   public static FacebookConfig getTransmissionConfig(String parentpath)
   {
      if(facebookconfig == null || facebookconfig.isModified())
      {
         loadConfig(parentpath);
      }
      return(facebookconfig);
   }

   protected static synchronized void loadConfig(String parentpath)
   {
      if(facebookconfig == null || facebookconfig.isModified())
      {
         if(fullpath == null)
         {
            StringBuffer sb = new StringBuffer(parentpath);
            sb.append(filepath);
            fullpath = sb.toString();
         }
         facebookconfig = new FacebookConfig(fullpath);
      }
   }

   public FacebookConfig(String path)
   {
      super(path);
   }

}
