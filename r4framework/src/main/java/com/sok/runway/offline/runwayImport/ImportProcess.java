package com.sok.runway.offline.runwayImport;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.sql.Connection;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.io.CsvMapReader;
import org.supercsv.prefs.CsvPreference;

import com.sok.framework.ActionBean;
import com.sok.framework.DisplaySpec;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.RelationSpec;
import com.sok.framework.RowBean;
import com.sok.framework.SpecManager;
import com.sok.framework.StringUtil;
import com.sok.framework.TableSpec;
import com.sok.framework.ViewSpec;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.generation.IllegalConfigurationException;
import com.sok.framework.generation.SQLKeys;
import com.sok.runway.OfflineProcess;
import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;
import com.sok.runway.offline.rpmManager.RPMtask;

public class ImportProcess extends OfflineProcess {
	private static final Logger logger = LoggerFactory
			.getLogger(ImportProcess.class);
	public static final String DEBUG = "-debug";
	public static final String TABLE = "-table";
	public static final String CONFIG = "-importconfig";

	StringBuffer log = new StringBuffer();
	StringBuffer status = new StringBuffer();

	String email = null;
	String userid = null;
	String importid = null;
	String filepath = null;
	Connection con = null;
	Context ctx = null;

	String importContactID = "";
	String importCompanyID = "";
	String importProductID = "";

	String lastProductType = "";

	int numLines = 0;
	int numProcessedLines = 0;
	int numDataErrors = 0;

	int numRecords = 0;
	int numProcessedRecords = 0;
	int numDuplicateRecords = 0;

	int updatedRecords = 0;
	int insertedRecords = 0;

	RelationSpec rspec = null;
	ImportConfiguration config = null;
	
	boolean isApartments = false;
	boolean isPlan = false;
	
	SimpleDateFormat ddmmyy = new SimpleDateFormat("dd-MM-yy");
	SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy");

	HashMap<String,String> groups	= new HashMap<String,String>(); 
	HashMap<String, HashMap<String,String>> estates =  new HashMap<String, HashMap<String,String>>();
	HashMap<String, HashMap<String,String>> ranges =  new HashMap<String, HashMap<String,String>>();
	HashMap<String,String> regions =  new HashMap<String, String>();

	boolean complete = false;

	public ImportProcess(ImportConfiguration config,
			HttpServletRequest request, ServletContext context) {
		super(request, context);
		init(config, request);
		start();
	}

	public ImportProcess(ImportConfiguration config,
			HttpServletRequest request, ServletContext context, boolean run) {
		super(request, context);
		init(config, request);
		if (run) start();
	}

	public ImportProcess(ImportConfiguration config, RowBean bean,
			HttpServletRequest request, ServletContext context, boolean run) {
		super(bean, request, context);
		init(config, request);
		if (run) start();
	}

	public ImportProcess(ImportConfiguration config, RowBean bean,
			HttpServletRequest request, ServletContext context) {
		super(bean, request, context);
		init(config, request);
		start();
	}

	protected void init(ImportConfiguration config, HttpServletRequest request) {
		this.config = config;
		rspec = SpecManager.getRelationSpec();
		HttpSession session = request.getSession();
		UserBean user = (UserBean) session
				.getAttribute(SessionKeys.CURRENT_USER);
		userid = user.getCurrentUserID();
		email = user.getEmail();
		debug = "true".equals(request.getParameter(DEBUG));
		// debug = true;
		importid = request.getParameter("ImportID");
		if (StringUtils.isBlank(importid)) importid = (String) request.getAttribute("ImportID"); 
		if (user == null || email == null) {
			throw new IllegalConfigurationException(
					"User not found in session, offline process halted.\r\n");
		}

		if (config == null) {
			throw new IllegalConfigurationException(
					"Import configuration not found, offline process halted.\r\n");
		}

		File importfile = config.getImportFile();
		if (importfile != null) {
			try {
				numLines = countLines(importfile);
			} catch (Exception e) {
				throw new IllegalConfigurationException(e);
			}
		}

		System.out.println(config.getImportName());
		
		readGroups(request);
		if (StringUtils.isNotBlank(config.getImportName()) && config.getImportName().indexOf("Apartment") >= 0) {
			getBuildingStages(request);
			isApartments = true;
			isPlan = false;
		} else if (StringUtils.isNotBlank(config.getImportName()) && config.getImportName().indexOf("Plan") >= 0) {
			getRangeHomes(request);
			isApartments = false;
			isPlan = true;
		} else {
			getEstateStages(request);
			isApartments = false;
			isPlan = false;
		}
		getRegions(request);
	}
	
	protected void getEstateStages(HttpServletRequest request) {
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductStageView");
		row.setRequest(request);
		row.setParameter("ProductType","Stage");
		row.doAction("search");
		
		row.getResults();
		
		while (row.getNext()) {
			if (StringUtils.isNotBlank(row.getString("EstateName"))) {
				HashMap<String,String> stages = estates.get(row.getString("EstateName").toLowerCase()); 
				if (stages == null) {
					stages = new HashMap<String,String>();
					stages.put(row.getString("EstateName").toLowerCase(), row.getString("EstateProductID"));
				}
				if (StringUtils.isNotBlank(row.getString("Name"))) stages.put(row.getString("Name").toLowerCase(), row.getString("ProductID"));
				estates.put(row.getString("EstateName").toLowerCase(), stages);
			}
		}
		
		row.close();
	}
	
	protected void getRangeHomes(HttpServletRequest request) {
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHomeView");
		row.setRequest(request);
		row.setParameter("ProductType","Home");
		row.doAction("search");
		
		row.getResults();
		
		while (row.getNext()) {
			if (StringUtils.isNotBlank(row.getString("RangeName"))) {
				HashMap<String,String> homes = ranges.get(row.getString("RangeName").toLowerCase()); 
				if (homes == null) {
					homes = new HashMap<String,String>();
					homes.put(row.getString("RangeName").toLowerCase(), row.getString("RangeProductID"));
				}
				if (StringUtils.isNotBlank(row.getString("Name"))) homes.put(row.getString("Name").toLowerCase(), row.getString("ProductID"));
				ranges.put(row.getString("RangeName").toLowerCase(), homes);
			}
		}
		
		row.close();
	}
	
	protected void getBuildingStages(HttpServletRequest request) {
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductBuildingStageView");
		row.setRequest(request);
		row.setParameter("ProductType","Building Stage");
		row.doAction("search");
		
		row.getResults();
		
		while (row.getNext()) {
			if (StringUtils.isNotBlank(row.getString("BuildingName"))) {
				HashMap<String,String> stages = estates.get(row.getString("BuildingName").toLowerCase()); 
				if (stages == null) {
					stages = new HashMap<String,String>();
					stages.put(row.getString("BuildingName").toLowerCase(), row.getString("BuildingProductID"));
				}
				if (StringUtils.isNotBlank(row.getString("Name"))) stages.put(row.getString("Name").toLowerCase(), row.getString("ProductID"));
				estates.put(row.getString("BuildingName").toLowerCase(), stages);
			}
		}
		
		row.close();
	}
	
	protected void getRegions(HttpServletRequest request) {
		GenRow row = new GenRow();
		row.setViewSpec("DisplayEntityView");
		row.setRequest(request);
		row.doAction("search");
		
		row.getResults();
		
		while (row.getNext()) {
			regions.put(row.getString("DisplayEntityID"), row.getString("DisplayEntityID"));
			if (row.isSet("Name")) regions.put(row.getString("Name"), row.getString("DisplayEntityID"));
 
		}
		
		row.close();
	}
	
	protected void readGroups(HttpServletRequest request) {
		GenRow row = new GenRow();
		row.setTableSpec("Groups");
		row.setRequest(request);
		row.setParameter("-select0", "GroupID");
		row.setParameter("-select1", "Name");
		row.doAction("search");
		row.getResults();
		
		while (row.getNext()) {
			if (row.getString("Name").length() > 0 && row.getString("GroupID").length() > 0) {
				groups.put(row.getString("Name"), row.getString("GroupID"));
				groups.put(row.getString("GroupID"), row.getString("GroupID"));
			}
		}
		row.close();
	}

	public boolean sendEmail() {
		return false;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	protected void log(String message) {
		log.append(message);
		log.append("\r\n");
		appendError(message);

		logger.error(message);
	}

	protected void log(Exception e, String message) {
		log(message);
		log("\r\n");
		log(e.toString());
		log("\r\n");

		logger.error(message, e);
	}

	protected void logDebug(String message) {
		if (debug) {
			log(message);
		}
		logger.debug(message);
	}

	protected void logDebug(String message, String... args) {
		if (debug) {
			if (args != null && args.length > 0) {
				for (String s : args) {
					message = message.replaceFirst("\\{\\}*", s);
				}
			}
			log(message);
		}
		logger.debug(message, args);
	}

	public String getLog() {
		return (log.toString());
	}

	void getConnection() throws Exception {
		if (con == null || con.isClosed()) {
			logDebug("getting connection with " + ActionBean.subcontextname
					+ " " + dbconn);
			ctx = new InitialContext();

			DataSource ds = null;
			try {
				dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
				Context envContext = (Context) ctx
						.lookup(ActionBean.subcontextname);
				ds = (DataSource) envContext.lookup(dbconn);
			} catch (Exception e) {
			}
			if (ds == null) {
				ds = (DataSource) ctx.lookup(dbconn);
			}
			con = ds.getConnection();
			logDebug("got connection");
		}
	}

	void closeConnection() {
		if (con != null) {
			try {
				con.close();
				con = null;
			} catch (Exception ex) {
			}
		}
		if (ctx != null) {
			try {
				ctx.close();
				ctx = null;
			} catch (Exception ex) {
			}
		}
	}

	public static String[] getHeaders(File file) throws IOException {
		if (file == null) return null;
		String[] header = null;
		CsvMapReader inFile = null;
		try {
			inFile = new CsvMapReader(new FileReader(file),	new CsvPreference('"', ',', "\r\n"));
			header = inFile.getCSVHeader(true);
		} finally {
			if (inFile != null) inFile.close();
		}
		return (header);
	}

	protected void loadDataBaseKeys(ImportConfiguration.Source source) {
		logDebug("loading database: " + source.viewspec);
		GenRow keys = new GenRow();
		keys.setConnection(con);
		ViewSpec vs = SpecManager.getViewSpec(source.viewspec);
		keys.setTableSpec(vs.getMainTableName());
		String keyname = keys.getTableSpec().getPrimaryKeyName();
		keys.setParameter(source.sourcekey, source.sourcevalue);
		if (!config.ignoredupsinfile) {
			keys.setParameter("Conflicts", "NULL+EMPTY");
		}
		logger.debug(keys.toString());
		keys.setParameter("-select1", keyname);
		keys.doAction("search");
		keys.getResults();
		while (keys.getNext()) {
			source.sourcekeyvalues.add(keys.getData(keyname));
		}
		keys.close();
		logDebug("found keys: " + String.valueOf(source.sourcekeyvalues)
				+ " in " + source.viewspec);
	}

	protected void loadDataBaseKeys() {
		for (int i = 0; i < config.getSourcesCount(); i++) {
			loadDataBaseKeys(config.getSource(i));
			numRecords = numRecords
					+ config.getSource(i).sourcekeyvalues.size();
		}
	}

	protected GenRow loadRow(ImportConfiguration.Source source, int index) {
		GenRow row = new GenRow();
		row.setConnection(con);
		row.setViewSpec(source.viewspec);
		row.setParameter(row.getTableSpec().getPrimaryKeyName(),
				source.sourcekeyvalues.get(index));
		row.doAction("select");
		return (row);
	}

	protected void loadDatabase() {
		logger.debug("loadDatabase()");
		loadDataBaseKeys();
		ImportConfiguration.Source source = null;
		GenRow row = null;
		for (int i = 0; i < config.getSourcesCount(); i++) {
			source = config.getSource(i);
			for (int j = 0; j < source.sourcekeyvalues.size(); j++) {
				try {
					row = loadRow(source, j);
					if (row.isSuccessful() && loadColumnsForTables(source, row)
							&& mapKeysForTables(source, row)
							&& saveTables(source)) {
						numProcessedRecords++;
					} else {
						numDataErrors++;
					}
				} catch (Exception e) {
					numDataErrors++;
					log(e, "Error while processing record: "
							+ source.sourcekeyvalues.get(j));
				}
			}
		}
	}

	protected void loadFile(File file) throws Exception {
		logDebug("loading file: " + file.getAbsolutePath());
		CsvMapReader inFile = new CsvRowReader(new FileReader(file),
				new CsvPreference('"', ',', "\r\n"));
		try {
			String[] header = inFile.getCSVHeader(true);
			logCVSHeader(header);

			Map<String, String> row = null;
			ImportConfiguration.Source source = null;

			while ((row = inFile.read(header)) != null) {
				numRecords++;
				try {
					for (int i = 0; i < config.getSourcesCount(); i++) {
						source = config.getSource(i);
						if (loadColumnsForTables(source, row)
								&& mapKeysForTables(source, row)
								&& saveTables(source)) {
							numProcessedLines = inFile.getLineNumber();
						} else {
							numDataErrors++;
						}
					}
					numProcessedRecords++;
				} catch (Exception e) {
					numDataErrors++;
					log(e, "Error while processing files.");
				}
			}
		} finally {
			inFile.close();
		}
	}

	protected boolean saveTables(ImportConfiguration.Source source) {
		try {
			GenRow products = null;
			boolean success = true;
			for (int i = 0; i < source.getTablesCount(); i++) {
				// if(success)
				// {
				ImportConfiguration.Table table = source.getTable(i);
				success = doAction(table);
				if (success) {
					GenRow row = table.data;
					if (row.getTableSpec() != null && "Products".equals(row.getTableSpec().getTableName())) {
						products = row;
					}
				}
				// }
			}
			if (products != null) {
				if("true".equals(InitServlet.getSystemParam("RegionPricingEnabled")) && "Home Plan".equals(products.getString("ProductType"))) {
					processPricing(importid, products.getString("ProductID"));
				}
				processDimensions(importid, products.getString("ProductID"));

			}
			return (true);
		} catch (Exception e) {
			log(e, "Error while processing import row.");
		}
		return (false);
	}

	protected void insertDupMarking(String table, String keyname,
			String keyvalue) {
		GenRow row = new GenRow();
		row.setTableSpec(table);
		row.setParameter("ImportID", importid);
		row.setParameter(keyname, keyvalue);
		row.createNewID();
		row.doAction("insert");
	}

	protected void markDup(ImportConfiguration.Table table) {
		if ("Contacts".equals(table.tableSpecname)) {
			insertDupMarking("ImportContactDups", "ContactID",
					table.data.getString("ContactID"));
			insertDupMarking("ImportContactDups", "ContactID",
					table.dedup.getString("ContactID"));
		} else if ("Company".equals(table.tableSpecname)) {
			insertDupMarking("ImportCompanyDups", "CompanyID",
					table.data.getString("CompanyID"));
			insertDupMarking("ImportCompanyDups", "CompanyID",
					table.dedup.getString("CompanyID"));
		} else if ("Product".equals(table.tableSpecname)) {
			insertDupMarking("ImportProductDups", "ProductID",
					table.data.getString("ProductID"));
			insertDupMarking("ImportProductDups", "ProductID",
					table.dedup.getString("ProductID"));
		}
	}

	protected void deDup(ImportConfiguration.Table table) {
		GenRow dedup = table.dedup;
		if (table.data.isSet("ProductType")) dedup.setParameter("ProductType", table.data.getString("ProductType"));
		if (hasDup(dedup)) {
			if (table.getTableSpec().indexOf("Imported") >= 0) {
				table.data.setParameter("Type", "Dup");
			}

			if (table.ignoreDupsInDatabase()) {
				logDebug("ignoring dups in database " + table.tableSpecname);
				table.data.createNewID();
				table.data.setAction("insert");
			} else {
				if (table.ignoredups) {
					table.data.setParameter("Conflicts", "Y");
					table.data.setAction("insert"); // import into holding area
													// and mark as dup.
				} else if (table.overwrite()) {
					logDebug("overwriting " + table.tableSpecname);
					setOverwrite(table);
				} else {
					setOverwrite(table); // this just updates foreign keys
				}
			}
			if (table.isMain()) {
				markDup(table);
				numDuplicateRecords++;
			}
		} else if (table.includenew) {
			// logDebug("setting new key for " + table.tableSpecname);
			logDebug("importing new {}", table.tableSpecname);
			if (!table.data
					.isSet(table.data.getTableSpec().getPrimaryKeyName())) {
				logDebug("Creating new id for {}", table.tableSpecname);
				table.data.createNewID();
			}
			table.data.setAction("insert");
		} else {
			logDebug("ignoring new " + table.tableSpecname);
		}
		// set the links
		String ts = table.data.getTableSpec().getTableName();
		if ("ImportContacts".equals(ts)) {
			importContactID = table.data.getString("ImportContactID");
		} else if ("ImportCompanies".equals(ts)) {
			importCompanyID = table.data.getString("ImportCompanyID");
		} else if ("ImportProducts".equals(ts)) {
			importProductID = table.data.getString("ImportProductID");
		}

	}

	/*
	public void insertValue(String productID, String productType,
			String detailsType, String key, String value) throws Exception {
		if (value.length() == 0)
			return;

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductDetails");
		getConnection();
		row.setConnection(con);
		row.setParameter("-select0", key);
		row.setParameter("-select1", "ProductDetailsID");
		row.setParameter("ProductID", productID);
		row.setParameter("ProductType", productType);
		row.setParameter("DetailsType", detailsType);
		row.doAction("selectfirst");

		if (!row.isSet(key) || "0.0".equals(row.getString(key))) {
			row.putAllDataAsParameters(row);
			try {
				row.setColumn(key, "" + Double.parseDouble(value));
				if (!row.isSet("ProductDetailsID")) {
					row.setColumn("ProductDetailsID", KeyMaker.generate());
					row.doAction("insert");
				} else {
					row.doAction("update");
				}
			} catch (Exception e) {
				System.out.println(productID + " " + detailsType + " " + key
						+ " " + value);
			}
		}
	}
	 */
	
	protected void processDimensions(String importID, String productID) {
		if (StringUtils.isBlank(importID) || StringUtils.isBlank(productID)) return;
		
		GenRow row = new GenRow();
		row.setViewSpec("ImportProductDetailsView");
		row.setConnection(con);
		row.setParameter("ImportID", importID);
		row.setParameter("ProductID", productID);
		row.setParameter("ImportProductDetailsImportedProduct.ProductID",
				productID);
		row.doAction("search");

		row.getResults();

		GenRow clone = new GenRow();
		clone.setTableSpec("properties/ProductDetails");
		clone.setConnection(con);
		
		GenRow check = new GenRow();
		check.setTableSpec("properties/ProductDetails");
		check.setConnection(con);
		
		int sort = 0;

		String ignorelist = "ProductDetailsID,ProductID,CopiedFromProductID,ProductType,DetailsType,SortOrder";
		
		TableSpec detailsSpec = SpecManager.getTableSpec("properties/ProductDetails");

		HashMap<String, String> dimMap = new HashMap<String,String>();

		String productType = lastProductType;
		if (productType != null && productType.length() > 0) productType = productType.replaceAll(" ", "");
		while (row.getNext()) {
			check.clear();
			check.setParameter("-select0","*");
			//check.setParameter("ProductType",productType + "Details");
			check.setParameter("ProductID",row.getString("ProductID"));
			check.setParameter("DetailsType",row.getString("DetailsType"));
			if (check.getString("ProductID").length() > 0) check.doAction("selectfirst");
			
			clone.clear();
			clone.putAllDataAsParameters(row, false);
			clone.setParameter("ProductDetailsID",row.getString("ImportProductDetailsID"));
			clone.setParameter("ProductType",productType + "Details");
			clone.setParameter("SortOrder","" + (++sort));
			if (check.isSuccessful() && check.getString("ProductDetailsID").length() > 0) {
				clone.setParameter("ProductDetailsID",check.getString("ProductDetailsID"));
				clone.doAction("update");
			} else {
				clone.setParameter("ProductDetailsID", KeyMaker.generate());
				clone.doAction("insert");
			}
			for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
				if (ignorelist.indexOf(detailsSpec.getFieldName(cs)) == -1) {
					dimMap.put((productType + "." + clone.getString("DetailsType") + "." + detailsSpec.getFieldName(cs)), clone.getString(detailsSpec.getFieldName(cs)));
				}
			}
		}

		row.close();
		if ("Land".equals(productType))
			processHomeDetails(dimMap, productID, "Land");
		else if ("Home Plan".equals(productType))
			processHomeDetails(dimMap, productID, "Plan");
		else if ("Apartment".equals(productType))
			processHomeDetails(dimMap, productID, "Apartment");
		
		RPMtask.getInstance().doIndexRefreash("update", productID, lastProductType);
	}
	
	protected void processPricing(String importID, String productID) {
		GenRow regions = new GenRow();
		regions.setTableSpec("DisplayEntities");
		regions.setConnection(con);
		regions.setParameter("-select0","*");
		regions.setParameter("Type","Region");
		regions.sortBy("Name", 0);
		regions.doAction("search");
		
		regions.getResults();

		GenRow clone = new GenRow();
		clone.setTableSpec("ProductRegionPrice");
		clone.setConnection(con);

		while (regions.getNext()) {
			GenRow row = new GenRow();
			row.setViewSpec("ImportProductRegionPriceView");
			row.setConnection(con);
			//row.setParameter("-select0", "*");
			row.setParameter("ImportID", importID);
			row.setParameter("ProductID", productID);
			row.setParameter("RegionID", regions.getString("DisplayEntityID"));
			row.setParameter("ImportProductRegionPriceImportedProduct.ProductID", productID);
			row.doAction("selectfirst");
	
			if (row.isSuccessful() && row.isSet("ImportProductRegionID")) {
				boolean data = false;
				clone.clear();
				clone.setParameter("ProductID",productID);
				clone.setParameter("RegionID", regions.getString("DisplayEntityID"));
				
				if (row.getDouble("Percentage") != 0) {
					clone.setParameter("Percentage", "" + (row.getDouble("Percentage") / 100));
					data = true;
				} else if (row.getDouble("Margin") != 0) {
					clone.setParameter("Margin", "" + row.getDouble("Margin"));
					data = true;
				} else if (row.getDouble("TotalCost") != 0) {
					clone.setParameter("TotalCost", "" + row.getDouble("TotalCost"));
					if (row.getDouble("Cost") == 0) {
						clone.setParameter("Cost", "" + (row.getDouble("TotalCost") / 1.1));
					} else {
						clone.setParameter("Cost", "" + row.getDouble("Cost"));
					}
					data = true;
				} else if (row.getDouble("Cost") != 0) {
					clone.setParameter("Cost", "" + row.getDouble("Cost"));
					if (row.getDouble("TotalCost") == 0) {
						clone.setParameter("TotalCost", "" + (row.getDouble("Cost") * 1.1));
					} else {
						clone.setParameter("TotalCost", "" + row.getDouble("TotalCost"));
					}
					data = true;
				}
				
				if (data) {
					try {
						clone.doAction("insert");
					} catch (Exception d) {
						clone.remove("ProductID");
						clone.remove("RegionID");
						clone.setParameter("ON-ProductID", productID);
						clone.setParameter("ON-RegionID", regions.getString("DisplayEntityID"));
						clone.doAction("updateall");
					}
				}
			}
		}
		
		regions.close();
		
	}
	
	protected void processHomeDetails(HashMap<String, String> dimensions, String productID, String type) {
		TableSpec detailsSpec = SpecManager.getTableSpec("properties/HomeDetails");

		GenRow home = new GenRow();
		home.setTableSpec("properties/HomeDetails");

		String spec = "Product" + type + "ViewDisplay";
		if ("Land".equals(type)) spec = "ProductLotViewDisplay";
		if ("Apartment".equals(type)) spec = "ProductApartmentViewDisplay";
		Map<String, String> map = getDetailsMap(spec);
		
		home.clear();
		
		boolean hasValues = false;
		for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
			if (map.containsKey(detailsSpec.getFieldName(cs))) {
				if (dimensions.containsKey(map.get(detailsSpec.getFieldName(cs)))) {
					home.setParameter(detailsSpec.getFieldName(cs), dimensions.get(map.get(detailsSpec.getFieldName(cs))));
					hasValues = true;
				}
			}
		}
		home.setParameter("ProductID", productID);
		if (hasValues) {
			try {
				home.doAction("insert");
			} catch (Exception e) {
				home.doAction("update");
			}
		}
	}

	public Map<String, String> getDetailsMap(String spec) {
		Map<String,String> map = new HashMap<String,String>();
		
	    DisplaySpec dspecLot = SpecManager.getDisplaySpec(spec);

	    String[] names = dspecLot.getItemNames();
		for (int d = 0; d < names.length; ++d) {
			map.put(names[d], dspecLot.getLocalItemFieldName(names[d]));
			
		}
		
		return map;
	}

	private GenRow categorySearch = null, importCategorySearch = null,
			propertyOptionGroupWorker = null;
	private Map<String, String[]> catMap = null;
	private String parent = null;
	private String parentId = null;
	private String cat = null;
	private String catId = null;
	private int nextCategory = 0;
	private NumberFormat catFormat = null;

	/**
	 * When loading from CSV, this method should insert data into
	 * ImportPropertyOptionCategories to enable creation of product groups.
	 * 
	 * @param name
	 * @param propertyOptionData
	 * @return
	 */
	private String getPropertyOptionCategoryID(GenRow propertyOptionData) {
		if (propertyOptionGroupWorker == null) {
			propertyOptionGroupWorker = new GenRow();
			propertyOptionGroupWorker.setTableSpec("ImportPropertyOptionCategories");
			propertyOptionGroupWorker.setConnection(con);
			propertyOptionGroupWorker.setParameter("ImportID", importid);

			categorySearch = new GenRow();
			categorySearch.setTableSpec("ProductGroups");
			categorySearch.setConnection(con);
			categorySearch.setParameter("-select1", "ProductGroupID");
			categorySearch.setParameter("-select2", "Description");
			categorySearch.setParameter("Description", "0%");
			categorySearch.setParameter("-sort0", "Description");
			categorySearch.setParameter("-order0", "DESC");
			categorySearch.doAction(GenerationKeys.SELECTFIRST);

			importCategorySearch = new GenRow();
			importCategorySearch.setTableSpec("ImportPropertyOptionCategories");
			importCategorySearch.setConnection(con);
			importCategorySearch.setParameter("-select1", "CategoryID");
			importCategorySearch.setParameter("-select2", "CategoryNum");
			importCategorySearch.setParameter("-sort0", "CategoryNum");
			importCategorySearch.setParameter("-order0", "DESC");
			importCategorySearch.setParameter("ImportID", importid);

			if (categorySearch.isSuccessful()) {
				String desc = StringUtils.trimToNull(categorySearch.getData("Description"));

				if (desc != null) {
					logger.debug("Have highest desc {}", desc);
					if (desc.length() > 5) {
						desc = desc.substring(0, 5);
					}
					logger.debug("Have highest desc first 5 {}", desc);
					while (desc.startsWith("0")) {
						desc = desc.substring(1);
					}
					if (desc.length() > 0) {
						try {
							nextCategory = Integer.parseInt(desc);
						} catch (NumberFormatException nfe) {
							// trace is intentional, if this doesn't parse set
							// the first to 000001;
							logger.trace("Error parsing Category Number", nfe);
						}
					}
				}
			}

			catFormat = NumberFormat.getIntegerInstance();
			catFormat.setMinimumIntegerDigits(5);
			catFormat.setGroupingUsed(false);
			nextCategory++;

			categorySearch = new GenRow();
			categorySearch.setConnection(propertyOptionData.getConnection());
			categorySearch.setTableSpec("ProductGroups");
			categorySearch.setParameter("-select1", "ProductGroupID");
			categorySearch.setParameter("-select2", "Description");
			categorySearch.setParameter("Description", "0%");

			catMap = new HashMap<String, String[]>();
		}
		String parentCategoryID = null, parentCategoryNum = null, categoryID = null, categoryNum = null, parentCategory = StringUtils
				.trimToNull(propertyOptionData.getString("ParentCategory")), category = StringUtils
				.trimToNull(propertyOptionData.getString("Category"));

		if ("Additional Sliding Doors".equals(category)) {
			System.out.println("Stop here");
		}
		if (parentCategory != null) {
			if (catMap.containsKey(parentCategory)) {
				parentCategoryID = catMap.get(parentCategory)[0];
				parentCategoryNum = catMap.get(parentCategory)[1];
			} else {
				categorySearch.clear();
				categorySearch.setTableSpec("ProductGroups");
				categorySearch.setParameter("-select1", "ProductGroupID");
				categorySearch.setParameter("-select2", "Description");
				categorySearch.setParameter("Description", "0%");
				categorySearch.setParameter("GroupName", parentCategory);
				categorySearch.doAction(GenerationKeys.SELECTFIRST);
				if (categorySearch.isSuccessful()) {
					parentCategoryID = categorySearch.getData("ProductGroupID");
					parentCategoryNum = categorySearch.getData("Description");
				} else {
					propertyOptionGroupWorker.setToNewID("CategoryID");
					propertyOptionGroupWorker.setToNewID("ImportCategoryID");
					propertyOptionGroupWorker.setParameter("CategoryName",
							parentCategory);
					propertyOptionGroupWorker.setParameter("CategoryNum",
							catFormat.format(nextCategory++));
					propertyOptionGroupWorker.setParameter("GroupID",
							propertyOptionData.getString("GroupID"));
					propertyOptionGroupWorker.doAction(GenerationKeys.INSERT);
					parentCategoryID = propertyOptionGroupWorker.getParameter("CategoryID");
					parentCategoryNum = propertyOptionGroupWorker.getParameter("CategoryNum");
				}
				catMap.put(parentCategory, new String[] { parentCategoryID,
						parentCategoryNum });
			}
		}

		if (StringUtils.isBlank(parentCategoryNum)) parentCategoryNum = "";
		
		if (category != null) {
			if (catMap.containsKey(category)) {
				categoryID = catMap.get(category)[0];
				categoryNum = catMap.get(category)[1];
			} else {
				categorySearch.clear();
				categorySearch.setTableSpec("ProductGroups");
				categorySearch.setParameter("-select1", "ProductGroupID");
				categorySearch.setParameter("-select2", "Description");
				categorySearch.setParameter("Description", parentCategoryNum
						+ "%");
				categorySearch.setParameter("GroupName", category);
				categorySearch.doAction(GenerationKeys.SELECTFIRST);
				if (categorySearch.isSuccessful()) {
					categoryID = categorySearch.getData("ProductGroupID");
					categoryNum = categorySearch.getData("Description");
				} else {
					if (StringUtils.isNotBlank(parentCategoryNum)) {
						importCategorySearch.setParameter("CategoryNum",parentCategoryNum + "0%");
						importCategorySearch.setParameter("-sort0", "CategoryNum");
						importCategorySearch.setParameter("-order0", "DESC");
						importCategorySearch.doAction(GenerationKeys.SELECTFIRST);
	
						if (importCategorySearch.isSuccessful()) {
							// base the category id on the newly created record
							String catNum = importCategorySearch.getData("CategoryNum").substring(parentCategoryNum.length());
	
							while (catNum.startsWith("0")) {
								catNum = catNum.substring(1);
							}
							if (catNum.length() > 0) {
								try {
									int cat = Integer.parseInt(catNum);
									cat++;
									propertyOptionGroupWorker.setParameter("CategoryNum", parentCategoryNum+ catFormat.format(cat++));
								} catch (NumberFormatException nfe) {
									// trace is intentional, if this doesn't parse
									// set the first to 000001;
									logger.trace("Error parsing Category Number",nfe);
									propertyOptionGroupWorker.setParameter("CategoryNum", parentCategoryNum+ catFormat.format(nextCategory++));
								}
							} else {
								propertyOptionGroupWorker.setParameter("CategoryNum", parentCategoryNum+ "00001");
							}
						} else {
							// check the existing
							if (StringUtils.isNotBlank(parentCategoryNum)) {
								categorySearch.clear();
								categorySearch.setTableSpec("ProductGroups");
								categorySearch.setParameter("-select1", "ProductGroupID");
								categorySearch.setParameter("-select2", "Description");
								categorySearch.setParameter("Description",parentCategoryNum + "0%");
								categorySearch.setParameter("-sort0", "Description");
								categorySearch.setParameter("-order0", "DESC");
								categorySearch.doAction(GenerationKeys.SELECTFIRST);
		
								if (categorySearch.isSuccessful()) {
									// base the categoryid on this one
									String catNum = categorySearch.getData("Description").substring(parentCategoryNum.length());
		
									while (catNum.startsWith("0")) {
										catNum = catNum.substring(1);
									}
									if (catNum.length() > 0) {
										try {
											int cat = Integer.parseInt(catNum);
											propertyOptionGroupWorker.setParameter("CategoryNum", parentCategoryNum+ catFormat.format(cat));
										} catch (NumberFormatException nfe) {
											// trace is intentional, if this doesn't
											// parse set the first to 000001;
											logger.trace("Error parsing Category Number",nfe);
											propertyOptionGroupWorker.setParameter("CategoryNum", parentCategoryNum+ "00001");
										}
									} else {
										propertyOptionGroupWorker.setParameter("CategoryNum", parentCategoryNum+ "00001");
									}
								} else {
									// create the first child category
									propertyOptionGroupWorker.setParameter("CategoryNum", parentCategoryNum + "00001");
								}
							} else {
								propertyOptionGroupWorker.setParameter("CategoryNum",catFormat.format(nextCategory++));
							}
						}
					} else {
						propertyOptionGroupWorker.setParameter("CategoryNum",catFormat.format(nextCategory++));
					}
					propertyOptionGroupWorker.setToNewID("CategoryID");
					propertyOptionGroupWorker.setToNewID("ImportCategoryID");
					propertyOptionGroupWorker.setParameter("CategoryName",category);
					propertyOptionGroupWorker.setParameter("GroupID",propertyOptionData.getString("GroupID"));
					propertyOptionGroupWorker.doAction(GenerationKeys.INSERT);
					
					if (propertyOptionGroupWorker.getString("CategoryNum").startsWith("36")) {
						System.out.println("36");
					}
					

					categoryID = propertyOptionGroupWorker.getParameter("CategoryID");
					categoryNum = propertyOptionGroupWorker.getParameter("CategoryNum");
				}
				catMap.put(category, new String[] { categoryID, categoryNum });
			}
		}
		return categoryID;
	}
	
	protected void insertProductVariation(Connection conn, String parentproductid, String childproductid, String type){
		if (parentproductid == null || parentproductid.length() == 0 || childproductid == null || childproductid.length() == 0) return;
		   GenRow productvariation = new GenRow();
		   productvariation.setConnection(conn);
		   productvariation.setTableSpec("ProductVariations");
		   productvariation.setParameter("ParentProductID", parentproductid);
		   productvariation.setParameter("ChildProductID", childproductid);
		   productvariation.setParameter("Type", type);
		   productvariation.createNewID();
		   productvariation.doAction("insert");
		}
	

	protected boolean doAction(ImportConfiguration.Table table)
			throws Exception {
		GenRow row = table.data;
		boolean success = true;
		if (row.getAction() != null && row.getAction().length() != 0) {
			logger.debug(row.toString());
			getConnection();
			row.setConnection(con);
			// if there is no primary key (unknown reason) then create one
			String pk = (row.getTableSpec() != null) ? row.getTableSpec()
					.getPrimaryKeyName() : "";
			if (pk == null || pk.length() == 0) {
				TableSpec ts = SpecManager.getTableSpec(row.getViewSpec()
						.getMainTableName());
				if (ts != null)
					pk = ts.getPrimaryKeyName();
			}
			if (pk != null && pk.length() > 0) {
				if (row.getColumn(pk).length() == 0) {
					logger.debug("pk length == 0");
					if (pk.indexOf("Import") >= 0)
						row.createNewID();
					logDebug("no primary key found for " + row.toString());
					// row.createNewID();
					// return false;
				}
			}
			logger.debug(row.toString());
			String action = row.getAction();
			// need to do some fancy stuff for Land Imports
			if (row.getTableSpec() != null
					&& "ImportProducts".equals(row.getTableSpec()
							.getTableName())) {
				if (row.getString("ProductType").indexOf("Land") >= 0 || "Apartment".equals(row.getString("ProductType"))) {
					row.setColumn("UpdateByProcess", "Y");
				} else {
					row.setColumn("UpdateByProcess", "N");
				}
				if (row.isSet("RegionID") && regions.containsKey(row.getString("RegionID")))
					row.setParameter("RegionID", regions.get(row.getString("RegionID")));
				if (row.isSet("LocationID") && regions.containsKey(row.getString("LocationID")))
					row.setParameter("LocationID", regions.get(row.getString("LocationID")));
			}
			if (row.getTableSpec() != null
					&& "ImportLocationStatistics".equals(row.getTableSpec()
							.getTableName())) {
				try {
				if (row.isSet("DayDate")) row.setParameter("DayDate", ddmmyyyy.format(ddmmyy.parse(row.getString("DayDate"))) + " 00:00:00");
				} catch (Exception e) {}
				if (row.getString("BlixBounce").indexOf("%") >= 0) row.setParameter("BlixBounce", row.getString("BlixBounce").replaceAll("%", ""));
			}
			// we need to stop the overwrite of Reps and Statuses with Blank
			// values
			if (row.getTableSpec() != null
					&& ("ProductSecurityGroups".equals(row.getTableSpec()
							.getTableName())
							|| "ContactGroups".equals(row.getTableSpec()
									.getTableName()) || "CompanyGroups"
								.equals(row.getTableSpec().getTableName()))) {
				if (row.getColumn("RepUserID").length() == 0)
					row.remove("RepUserID");
			}
			if (row.getTableSpec() != null
					&& ("ProductStatus".equals(row.getTableSpec()
							.getTableName()) || "GroupStatus".equals(row
							.getTableSpec().getTableName()))) {
				if (row.getColumn("StatusID").length() == 0)
					row.remove("StatusID");
			}
			if (row.getTableSpec() != null
					&& "Products".equals(row.getTableSpec().getTableName())) {
				if (row.getColumn("ProductStatusID").length() == 0)
					row.remove("ProductStatusID");
				if (row.getString("ProductType").length() > 0) lastProductType = row.getString("ProductType");
			}
			if (row.getTableSpec() != null
					&& "HomeDetails".equals(row.getTableSpec().getTableName())) {
				if (row.getColumn("LotTitleMonth").length() == 0 && row.getColumn("LotTitleYes").length() == 0 && row.getColumn("IsLotTitled").length() == 0) {
					row.setParameter("-select0", "ProductID");
					row.setAction("selectfirst");
				} else if (row.getColumn("LotTitleMonth").length() > 0 && row.getColumn("LotTitleYes").length() > 0){
				    SimpleDateFormat sdf = new SimpleDateFormat("MMMMM yyyy");

				    row.setParameter("LotTitleDate" , sdf.parse(row.getString("LotTitleMonth")  + " " + row.getString("LotTitleYear")));
				}
			}
			if (row.getTableSpec() != null
					&& "ProductDetails".equals(row.getTableSpec()
							.getTableName())) {
				// row.setParameter(row.getString("Field"),
				// row.getString("Value"));
				if (lastProductType != null && lastProductType.length() > 0) row.setParameter("ProductType", lastProductType.replaceAll(" ", "") + "Details");
				row.remove("Field");
				row.remove("Value");
			}
			if (row.getTableSpec() != null
					&& "Notes".equals(row.getTableSpec().getTableName())) {
				if (row.getColumn("Body").length() == 0 && row.getColumn("Subject").length() > 0) {
					row.setParameter("Body", row.getString("Subject"));
					row.setParameter("Subject","Imported Note");
				}
				if (row.getColumn("Body").length() == 0) {
					row.setParameter("-select1", "*");
					row.setAction("selectfirst");
				} else {
					if (row.getColumn("Subject").length() == 0) row.setColumn("Subject", "Imported Note");
					if (row.getColumn("NoteDate").length() == 0) row.setColumn("NoteDate", "NOW");
					if (row.getColumn("NoteTime").length() == 0) row.setColumn("NoteTime", "NOW");
				}
			}

			logger.debug(table.tableSpecname);
			logger.debug(row.toString());
			if ("ImportProducts".equals(table.tableSpecname) && table.dedup.isSet("ProductNum")) {
				if(row.isSet("ProductNum") && !row.isSet("ProductID")) {
					row.setParameter("ProductID", getProductIDForTypeNum(row.getString("ProductType"), row.getString("ProductNum")));
				}
			} else if ("ImportPropertyOptions".equals(table.tableSpecname)) {
				// need to create the product group if not found, or get the id
				// if found
				row.setParameter("ProductGroupID",
						getPropertyOptionCategoryID(row));
			} else if ("ImportPropertyDisplayOptions".equals(table.tableSpecname)) {
				logger.debug("Setting product ids");
				if(row.isSet("ProductNum")) {
					row.setParameter("ProductID", getPropertyProductIDForNum(row));
				}
				if(row.isSet("LinkedProductNum")) {
					row.setParameter("LinkedProductID", getOptionProductIDForNum(row));
				//} else {
					// ?
					//return false;
				}
			}
			// we need to check if there is existing Answers and then history
			// them
			if (row.getTableSpec() != null
					&& row.getTableSpec().getTableName().indexOf("Answer") >= 0
					&& row.getTableSpec().getTableName().indexOf("Import") == -1) {
				if ("update".equals(action)) {
					try {
						if (row.getColumn("Answer").length() > 0) {
							String qID = row.getColumn("QuestionID");
							GenRow bean = new GenRow();
							bean.setTableSpec(row.getTableSpec());
							bean.setColumn("AnswerID",
									row.getColumn("AnswerID"));
							bean.setParameter("-select1", "*");
							bean.doAction("selectfirst");
							if (qID.length() == 0)
								qID = bean.getString("QuestionID");
							if (bean.getString("ContactID").length() > 0)
								row.setColumn("ContactID",
										bean.getString("ContactID"));
							if (bean.getString("CompanyID").length() > 0)
								row.setColumn("CompanyID",
										bean.getString("CompanyID"));
							if (bean.getString("ProductID").length() > 0)
								row.setColumn("ProductID",
										bean.getString("ProductID"));
							bean.clear();
							bean.setColumn("AnswerID",
									row.getColumn("AnswerID"));
							bean.setColumn("QuestionID", qID + "history");
							bean.doAction("update");

							if (qID.length() > 0
									&& (row.getString("ContactID").length() > 0
											|| row.getString("CompanyID")
													.length() > 0 || row
											.getString("ProductID").length() > 0)) {
								row.setColumn("QuestionID", qID);
								row.setColumn("AnswerID", KeyMaker.generate());
								row.setAction("insert");
							}
						} else {
							row.setParameter("-select1", "*");
							row.setAction("selectfirst");
						}
					} catch (Exception e) {
					}
				} else if ("insert".equals(action)) {
					try {
						if (row.getColumn("Answer").length() > 0) {
							String qID = row.getColumn("QuestionID");
							GenRow bean = new GenRow();
							bean.setTableSpec(row.getTableSpec());
							bean.setParameter("-select", "AnswerID");
							bean.setColumn("QuestionID", qID);
							if (row.getString("ContactID").length() > 0)
								bean.setColumn("ContactID",
										row.getString("ContactID"));
							if (row.getString("CompanyID").length() > 0)
								bean.setColumn("CompanyID",
										row.getString("CompanyID"));
							if (row.getString("ProductID").length() > 0)
								bean.setColumn("ProductID",
										row.getString("ProductID"));
							if (row.getString("ContactID").length() > 0
									|| row.getString("CompanyID").length() > 0
									|| row.getString("ProductID").length() > 0) {
								bean.setParameter("-select1", "*");
								bean.doAction("selectfirst");
								if (bean.isSuccessful()) {
									String answerID = bean.getData("AnswerID");
									if (answerID.length() > 0) {
										bean.clear();
										bean.setColumn("AnswerID", answerID);
										bean.setColumn("QuestionID", qID
												+ "history");
										bean.doAction("update");
									}
								}
							} else {
								row.setParameter("-select1", "*");
								row.setAction("selectfirst");
							}
						} else {
							row.setParameter("-select1", "*");
							row.setAction("selectfirst");
						}
					} catch (Exception e) {
					}
				}
			}
			try {
				if ("update".equals(row.getAction())) {
					if (row.getString("ModifiedDate").length() == 0)
						row.setColumn("ModifiedDate", "NOW");
					else
						row.setParameter("ModifiedDate", StringUtil
								.urlDecode(row.getString("ModifiedDate")));
				} else if ("insert".equals(row.getAction())) {
					if (row.getString("ModifiedDate").length() == 0)
						row.setColumn("ModifiedDate", "NOW");
					else
						row.setParameter("ModifiedDate", StringUtil
								.urlDecode(row.getString("ModifiedDate")));
					if (row.getString("CreatedDate").length() == 0)
						row.setColumn("CreatedDate", "NOW");
					else
						row.setParameter("CreatedDate", StringUtil
								.urlDecode(row.getString("CreatedDate")));
				}
				if (row.getString("GroupID").length() > 0 && groups.containsKey(row.getString("GroupID"))) {
					row.setParameter("GroupID", groups.get(row.getString("GroupID")));
				}
				if (row.getString("ImportID").length() == 0)
					row.setColumn("ImportID", importid);
				try {
					row.doAction();
				} catch (Exception e23) {
					if ("insert".equals(row.getAction())) {
						row.doAction("update");
					} else {
						throw e23;
					}
				}
				success = row.isSuccessful();
				if (success	&& row.getTableSpec() != null && ("Products".equals(row.getTableSpec().getTableName()) || "properties/HomeDetails".equals(row.getTableSpec().getTableName()))) {
					if ("Land".equals(row.getString("ProductType"))) 
						insertProductVariation(row.getConnection(), row.getString("StageProductID"), row.getString("ProductID"), "Stage");
					if ("Home Plan".equals(row.getString("ProductType"))) 
						insertProductVariation(row.getConnection(), row.getString("HomeProductID"), row.getString("ProductID"), "Home");					
					if ("Apartment".equals(row.getString("ProductType"))) 
						insertProductVariation(row.getConnection(), row.getString("StageProductID"), row.getString("ProductID"), "Building Stage");					
					//EP Updates
					if ("Existing Property".equals(row.getString("ProductType")))
						updateEPData(row);
						
				}

				logDebug("{} action ({}) with : {}", (success ? "Done"
						: "Failed"), row.getAction(), row.toString(true));
			} catch (Exception e) {
				logDebug("failed doing action ({}) with : {} \n row {}",
						row.getAction(), e.getMessage(), row.toString(true));
				logger.error("failed doing action ({}) with : {} \n row {} \n Statement {}",
						new String[] {row.getAction(), e.getMessage(), row.toString(true), row.getStatement()});
				logger.error("Error performing doAction()", e);
				success = false;
			}
			if (success && table.isMain()) {
				if ("insert".equals(action)) {
					this.insertedRecords++;
				} else {
					this.updatedRecords++;
				}
			}
		} else if (debug) {
			if (row.getTableSpec() != null
					&& row.getTableSpec().getTableName().indexOf("Answers") == -1) {
				logDebug("no action found for {}", row.toString());
			}
		}
		return (success);
	}

	private void updateEPData(GenRow row) {
		try {
			Double cost = new Double(row.getString("LotCost"));
			if (StringUtils.isNotBlank(row.getString("LotCost")) && cost != 0) {
				insertProductDetails(row.getConnection(), row.getString("ProductID"), row.getString("ProductType"), "Lot Cost", "NumberOf", row.getString("LotCost"));
			}
		} catch (Exception e) {
			logger.error("Error while updating lot cost" + e.getMessage());
		}

		try {
			Double cost = new Double(row.getString("HomeCost"));
			if (StringUtils.isNotBlank(row.getString("HomeCost")) && cost != 0) {
				insertProductDetails(row.getConnection(), row.getString("ProductID"), row.getString("ProductType"), "Home Cost", "NumberOf", row.getString("HomeCost"));
			}
		} catch (Exception e) {
			logger.error("Error while updating home cost" + e.getMessage());
		}
	}

	private void insertProductDetails(Connection conn, String productID, String productType, String dimName, String dimType, String dimValue) {
		logger.debug("insertProductDetails for productID {}", productID);
		logger.debug("Dimension dimName {}, dimValue {}", dimName, dimValue);
		if (StringUtils.isNotBlank(productID)) {
			GenRow productDetails = new GenRow();
			productDetails.setConnection(conn);
			productDetails.setTableSpec("properties/ProductDetails");
			productDetails.setParameter("-select0", "*");
			productDetails.setParameter("ProductID", productID);
			productDetails.setParameter("DetailsType", dimName);
			productDetails.doAction("selectfirst");

			productDetails.setParameter(dimType, dimValue);
			if (productDetails.isSuccessful()) {
				productDetails.setParameter(dimType, dimValue);
				productDetails.doAction("update");

				logger.debug("Updating for ProductDetailsID {}, {}", productDetails.getString("ProductDetailsID"), productDetails.getStatement());
			} else {
				productDetails.createNewID();
				productDetails.setParameter("ProductType", productType);
				productDetails.doAction("insert");
				logger.debug("Inserting for ProductDetailsID {}", productDetails.getStatement());
			}

		}
	}

	private String getOptionProductIDForNum(GenRow row) {
		String productNum = StringUtils.trimToNull(row.getString("LinkedProductNum"));
		if(productNum != null) {
			return getProductIDForTypeNum("Property Option", productNum);
		}
		return null;
	}

	private String getPropertyProductIDForNum(GenRow row) {
		String productNum = StringUtils.trimToNull(row.getString("ProductNum"));
		if(productNum != null) {
			return getProductIDForTypeNum("Home Plan", productNum);
		}
		return null;
	}
	
	private GenRow productIDWorker = null;
	private String getProductIDForTypeNum(String type, String num) {
		if (type == null || num == null || type.length() == 0 || num.length() == 0) return null;
		if(productIDWorker == null) {
			productIDWorker = new GenRow();
			productIDWorker.setTableSpec("Products");
			productIDWorker.setParameter("Name", "!NULL;!EMPTY");
			productIDWorker.setParameter("Category", "NULL+EMPTY");	//excludes PlanSale etc.
			productIDWorker.setParameter("-select1","ProductID");
			productIDWorker.setConnection(con);
		}
		productIDWorker.setParameter("ProductType", type.trim());
		productIDWorker.setParameter("ProductNum", num.trim());
		productIDWorker.doAction(GenerationKeys.SELECTFIRST);
		
		if(productIDWorker.isSuccessful()) {
			return productIDWorker.getData("ProductID");
		}
		return null;
	}

	/*
	protected void setDemensions(String productType, GenRow row)
			throws Exception {
		if ("Land".equals(productType)) {
			insertValue(row.getString("ProductID"), "LandDetails", "Lot",
					"Perimeter", row.getString("Perimeter"));
			insertValue(row.getString("ProductID"), "LandDetails", "Lot",
					"SQmeters", row.getString("Area"));
			insertValue(row.getString("ProductID"), "LandDetails", "Lot",
					"Width", row.getString("LotWidth"));
			insertValue(row.getString("ProductID"), "LandDetails", "Lot",
					"Depth", row.getString("LotDepth"));

			insertValue(row.getString("ProductID"), "LandDetails", "SetBack",
					"DimLeft", row.getString("LeftSetback"));
			insertValue(row.getString("ProductID"), "LandDetails", "SetBack",
					"DimRight", row.getString("RightSetback"));
			insertValue(row.getString("ProductID"), "LandDetails", "SetBack",
					"DimFront", row.getString("FrontSetback"));
			insertValue(row.getString("ProductID"), "LandDetails", "SetBack",
					"DimRear", row.getString("RearSetback"));
		}
		if ("Home Plan".equals(productType)) {
			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Bedrooms", "NumberOf", row.getString("Bedrooms"));
			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Bathrooms", "NumberOf", row.getString("Bathrooms"));

			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Home Area", "SQmeters", row.getString("HomeSize"));
			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Home Area", "Squares", row.getString("HomeSizeSq"));
			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Home Area", "Width", row.getString("LotWidth"));
			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Home Area", "Depth", row.getString("LotDepth"));

			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Can Fit On", "Width", row.getString("CanFitOnFrontage"));
			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Can Fit On", "Depth", row.getString("CanFitOnDepth"));

			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Garage", "SQmeters", row.getString("Garage"));
			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Garage", "NumberOf", row.getString("CarParks"));

			insertValue(row.getString("ProductID"), "HomePlanDetails", "Study",
					"NumberOf",
					("Y".equals(row.getString("Study")) ? "1" : "0"));
			insertValue(row.getString("ProductID"), "HomePlanDetails", "Study",
					"YesNo",
					("Y".equals(row.getString("Study")) ? "Yes" : "No"));
			insertValue(row.getString("ProductID"), "HomePlanDetails",
					"Ground Floor Area", "SQmeters",
					row.getString("GroundFloorArea"));

			if ("Double".equals(row.getString("Storeys"))
					|| "2".equals(row.getString("Storeys"))) {
				insertValue(row.getString("ProductID"), "HomePlanDetails",
						"Storeys", "NumberOf", "2");
				insertValue(row.getString("ProductID"), "HomePlanDetails",
						"Storeys", "AlphaValue", "Double");
			} else {
				insertValue(row.getString("ProductID"), "HomePlanDetails",
						"Storeys", "NumberOf", "1");
				insertValue(row.getString("ProductID"), "HomePlanDetails",
						"Storeys", "AlphaValue", "Double");
			}
		}
	}
	*/
	protected void setOverwrite(ImportConfiguration.Table table) {
		GenRow row = table.data;
		GenRow dedup = table.dedup;
		String keyname = row.getTableSpec().getPrimaryKeyName();

		GenRow overwrite = new GenRow();
		overwrite.setTableSpec(row.getTableSpec());
		overwrite.setParameter(keyname, dedup.getData(keyname));

		ImportConfiguration.Mapping mapping = null;

		for (int i = 0; i < table.getMappingCount(); i++) {
			mapping = table.getMapping(i);

			if (mapping.isOverwrite()) {
				String value = row.getString(mapping.destination);
				if (value != null && value.length() != 0) {
					overwrite.setParameter(mapping.destination, value);
					logDebug("overwriting column " + mapping.destination
							+ " with " + value);
				}
			}
		}
		overwrite.setAction("update");
		table.data = overwrite;
		logDebug("found dup " + overwrite);
	}

	protected boolean hasDup(GenRow dedup) {
		if (dedup.getAction() != null && dedup.getAction().length() != 0) {
			if (dedup.getTableSpec().getTableName().indexOf("Import") >= 0
					&& dedup.getColumn("ImportID").length() == 0)
				dedup.setColumn("ImportID", importid);
			if (dedup.getTableSpec().getTableName().indexOf("Products") >= 0) {
				if (dedup.getTableSpec().getTableName().indexOf("Import") == -1) {
					dedup.setParameter("Category","NULL+EMPTY");
					if (isApartments || "Apartment".equals(dedup.getString("ProductType"))) {
						if (dedup.getString("StageProductID").length() > 0) dedup.setParameter("ProductParentBuildingStageApartment.ProductVariationBuildingStageApartment.ProductID",dedup.getString("StageProductID"));
						else if (dedup.getString("EstateProductID").length() > 0) dedup.setParameter("ProductParentBuildingStageApartment.ProductVariationBuildingStageApartment.ProductParentBuildingBuildingStage.ProductVariationBuildingBuildingStage.ProductID",dedup.getString("EstateProductID"));
					} else 	if (isPlan || "Home Plan".equals(dedup.getString("ProductType"))) {
						if (dedup.getString("HomeProductID").length() > 0) dedup.setParameter("ProductParentHomeDesign.ProductVariationHomeDesign.ProductID",dedup.getString("HomeProductID"));
						else if (dedup.getString("RangeProductID").length() > 0) dedup.setParameter("ProductParentHomeDesign.ProductVariationHomeDesign.ProductParentHomeRange.ProductVariationHomeRange.ProductID",dedup.getString("RangeProductID"));
					} else {
						if (dedup.getString("StageProductID").length() > 0) dedup.setParameter("ProductParentStageLot.ProductVariationStageLot.ProductID",dedup.getString("StageProductID"));
						else if (dedup.getString("EstateProductID").length() > 0) dedup.setParameter("ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ProductID",dedup.getString("EstateProductID"));
					}
					
					dedup.remove("StageProductID");
					dedup.remove("EstateProductID");
					dedup.remove("HomeProductID");
					dedup.remove("RangeProductID");
				}
			}
			dedup.setConnection(con);
			dedup.setParameter("-select", "*");
			dedup.doAction();
			
			System.out.println(dedup.getStatement());
			
			logDebug("looking for dup with " + dedup + " found:"
					+ String.valueOf(dedup.isSuccessful()));
			return (dedup.isSuccessful());
		}
		return (false);
	}

	protected boolean mapKeysForTables(ImportConfiguration.Source source,
			Map row) {
		for (int i = 0; i < source.getTablesCount(); i++) {
			try {
				setRelations(source, source.getTable(i).relation,
						source.getTable(i));
			} catch (Exception e) {
				log(e, "Error while maping relationships.");
			}
		}
		return (true);
	}

	protected void setRelations(ImportConfiguration.Source source,
			String relation, ImportConfiguration.Table totableconfig) {
		if(logger.isDebugEnabled()) logger.debug("setRelations({},{},{})", new String[] { source.viewspec,
				relation, totableconfig.relation });
		if (relation != null && relation.length() != 0) {
			if (relation.indexOf(',') > 0) {
				String[] relations = relation.split("\\,");
				for (int i = 0; i < relations.length; i++) {
					setRelation(source, relations[i], totableconfig);
				}
			} else {
				setRelation(source, relation, totableconfig);
			}
		}
	}

	protected void setRelation(ImportConfiguration.Source source,
			String relation, ImportConfiguration.Table totableconfig) {
		int rindex = rspec.getIndex(relation);
		if (rindex >= 0) {
			String fromtable = rspec.getFromTableSpecLocation(rindex);
			// String totable = rspec.getToTableSpecLocation(rindex);
			// logDebug("found relation " + String.valueOf(rindex) + " " +
			// relation + " from " + fromtable + " to " +totable);
			// ImportConfiguration.Table totableconfig =
			// source.getTable(source.getIndexForTableSpecName(totable));
			int tableindex = source.getIndexForTableSpecName(fromtable);
			if (tableindex >= 0) {
				ImportConfiguration.Table fromtableconfig = source
						.getTable(tableindex);

				setForeignKey(fromtableconfig, totableconfig,
						rspec.getFromKey(rindex), rspec.getToKey(rindex));
				int onlength = rspec.getOnLength(rindex);
				if (onlength > 0) {
					for (int j = 0; j < onlength; j++) {
						setForeignKey(fromtableconfig, totableconfig,
								rspec.getOnFromKey(rindex, j),
								rspec.getOnToKey(rindex, j));
					}
				}
			} else {
				logDebug("table not found: " + fromtable);
			}
		} else if (relation != null && relation.length() != 0) {
			log("Relation not found: " + relation);
		}
	}

	protected void setForeignKey(ImportConfiguration.Table fromtableconfig,
			ImportConfiguration.Table totableconfig, String fromkey,
			String tokey) {
		Object value = fromtableconfig.data.get(fromkey);
		logDebug("setting foreignKey " + tokey + " with value " + value
				+ " in " + totableconfig.getTableSpec());
		totableconfig.data.setParameter(tokey, value);
		if (totableconfig.dedup.containsKey(tokey)) {
			totableconfig.dedup.setParameter(tokey, value);
		}
	}

	protected boolean loadColumnsForTables(ImportConfiguration.Source source,
			Map row) {
		boolean success = false;
		// clear the linking keys
		importContactID = "";
		importCompanyID = "";
		importProductID = "";
		for (int i = 0; i < source.getTablesCount(); i++) {
			if (loadColumns(source, row, source.getTable(i))) {
				success = true;
			}
		}

		return (success);
	}

	protected boolean loadColumns(ImportConfiguration.Source source, Map row,
			ImportConfiguration.Table table) {
		logger.debug("loading columns for {}", table.tableSpecname);

		table.data.clear();
		table.data.setTableSpec(table.tableSpecname);
		table.dedup.clear();
		table.dedup.setTableSpec(table.tableSpecname);

		int columns = table.mappings.size();
		boolean mappinghasvalues = false;

		for (int i = 0; i < columns; i++) {
			if (mapColumn(row, table, table.mappings.get(i))) {
				mappinghasvalues = true;
			}
		}
		if ("ImportProducts".equals(table.tableSpecname)) {
			if (table.data.getString("EstateLookup").length() > 0 && table.data.getString("StageLookup").length() > 0) {
				logger.debug("Look Up Land/Apartment " + table.data.getString("EstateLookup") + " - " + table.data.getString("StageLookup"));
				HashMap<String,String> stages = estates.get(table.data.getString("EstateLookup").toLowerCase());
				if (stages != null) {
					String estateID = stages.get(table.data.getString("EstateLookup").toLowerCase());
					String stageProductID = stages.get(table.data.getString("StageLookup").toLowerCase());
					
					if (isApartments) {
						if (stageProductID == null || stageProductID.length() == 0 && table.data.getString("StageLookup").toLowerCase().indexOf("level") == -1) stageProductID = stages.get(("Level " + table.data.getString("StageLookup")).toLowerCase());
					} else {
						if (stageProductID == null || stageProductID.length() == 0 && table.data.getString("StageLookup").toLowerCase().indexOf("stage") == -1) stageProductID = stages.get(("Stage " + table.data.getString("StageLookup")).toLowerCase());
					}

					if (stageProductID != null && stageProductID.length() > 0) {
						table.data.setColumn("StageProductID", stageProductID);
						if (estateID != null && estateID.length() > 0) table.data.setColumn("EstateProductID", estateID);
					} else {
						if (estateID != null && estateID.length() > 0) {
							GenRow stage = new GenRow();
							stage.setTableSpec("Products");
							stage.setConnection(table.data.getConnection());
							stage.setColumn("Name", table.data.getString("StageLookup"));
							stage.setColumn("Description", "Stage Created by Import");
							stage.setColumn("Active", "N");
							stage.setColumn("ModifiedDate", "NOW");
							stage.setColumn("CreatedDate", "NOW");
							stage.setColumn("ModifiedBy", userid);
							stage.setColumn("CreatedBy", userid);
							stage.setColumn("ProductID", KeyMaker.generate());
							
							if (isApartments) {
								stage.setColumn("ProductType","Building Stage");
								stage.doAction("insert");
								
								insertProductVariation(table.data.getConnection(),estateID, stage.getString("ProductID"),"Building");
							} else {
								stage.setColumn("ProductType","Stage");
								stage.doAction("insert");
								
								insertProductVariation(table.data.getConnection(),estateID, stage.getString("ProductID"),"Estate");
							}
							
							stages.put(table.data.getString("StageLookup").toLowerCase(), stage.getString("ProductID"));
							table.data.setColumn("StageProductID", stage.getString("ProductID"));
							if (estateID != null && estateID.length() > 0) table.data.setColumn("EstateProductID", estateID);
	
						}
					}
				}
				if (table.data.getString("StageProductID").length() == 0) {
					table.data.setColumn("EstateProductID", table.data.getString("EstateLookup"));
					table.data.setColumn("StageProductID", table.data.getString("StageLookup"));
				}
			}
			if (table.data.getString("RangeLookup").length() > 0 && table.data.getString("DesignLookup").length() > 0) {
				logger.debug("Look Up Home " + table.data.getString("RangeLookup") + " - " + table.data.getString("DesignLookup"));
				HashMap<String,String> homes = ranges.get(table.data.getString("RangeLookup").toLowerCase());
				if (homes != null) {
					String estateID = homes.get(table.data.getString("RangeLookup").toLowerCase());
					String stageProductID = homes.get(table.data.getString("DesignLookup").toLowerCase());
					
					if (stageProductID != null && stageProductID.length() > 0) {
						table.data.setColumn("HomeProductID", stageProductID);
						if (estateID != null && estateID.length() > 0) table.data.setColumn("RangeProductID", estateID);
					} else {
						if (estateID != null && estateID.length() > 0) {
							GenRow stage = new GenRow();
							stage.setTableSpec("Products");
							stage.setConnection(table.data.getConnection());
							stage.setColumn("Name", table.data.getString("DesignLookup"));
							stage.setColumn("Description", "Home Created by Import");
							stage.setColumn("Active", "N");
							stage.setColumn("ModifiedDate", "NOW");
							stage.setColumn("CreatedDate", "NOW");
							stage.setColumn("ModifiedBy", userid);
							stage.setColumn("CreatedBy", userid);
							stage.setColumn("ProductID", KeyMaker.generate());
							
							stage.setColumn("ProductType","Home");
							stage.doAction("insert");
							
							insertProductVariation(table.data.getConnection(),estateID, stage.getString("ProductID"),"Home Range");
							
							homes.put(table.data.getString("DesignLookup").toLowerCase(), stage.getString("ProductID"));
							table.data.setColumn("HomeProductID", stage.getString("ProductID"));
							if (estateID != null && estateID.length() > 0) table.data.setColumn("RangeProductID", estateID);
	
						}
					}
				}
				if (table.data.getString("HomeProductID").length() == 0) {
					table.data.setColumn("RangeProductID", table.data.getString("RangeLookup"));
					table.data.setColumn("HomeProductID", table.data.getString("DesignLookup"));
				}
			}
		}

		boolean ret = false;
		if (mappinghasvalues) {

			setRelations(source, table.relation, table);
			// set the links for table specs, make sure we don't over-write what
			// is there.
			if ("ImportAnswers".equals(table.tableSpecname)) {
				if (table.data.getColumn("ImportContactID").length() == 0) {
					table.data.setColumn("ImportContactID", importContactID);
					table.dedup.setColumn("ImportContactID", importContactID);
				}
				if (table.data.getColumn("ImportCompanyID").length() == 0) {
					table.data.setColumn("ImportCompanyID", importCompanyID);
					table.dedup.setColumn("ImportCompanyID", importCompanyID);
				}
				if (table.data.getColumn("ImportProductID").length() == 0) {
					table.data.setColumn("ImportProductID", importProductID);
					table.dedup.setColumn("ImportProductID", importProductID);
				}
			}
			table.dedup.setColumn("StageProductID", table.data.getString("StageProductID"));
			table.dedup.setColumn("EstateProductID", table.data.getString("EstateProductID"));
			table.dedup.setColumn("HomeProductID", table.data.getString("HomeProductID"));
			table.dedup.setColumn("RangeProductID", table.data.getString("RangeProductID"));
			deDup(table);
			return (true);
		}

		return (false);
	}

	protected String findRep(String firstname, String lastname) {
		if (firstname != null && firstname.length() > 0 && lastname != null
				&& lastname.length() > 0) {
			GenRow row = new GenRow();
			row.setConnection(con);
			row.setTableSpec("Users");
			row.setParameter("-select1", "UserID");
			row.setParameter("FirstName", firstname);
			row.setParameter("LastName", lastname);
			row.doAction("selectfirst");
			if (row.isSuccessful()) {
				return (row.getString("UserID"));
			}
		}
		return (null);
	}

	protected String findRep(String repUserID) {
		if (repUserID != null && repUserID.length() > 0) {
			GenRow row = new GenRow();
			row.setConnection(con);
			row.setTableSpec("Users");
			
			
			String sql ="SELECT UserID FROM Users WHERE UserID = '" + repUserID + "'";
			if (repUserID.lastIndexOf(" ") >= 0) {
				String f = repUserID.substring(0, repUserID.lastIndexOf(" ")).trim();
				String l = repUserID.substring(repUserID.lastIndexOf(" ")).trim();
				
				sql += " OR (FirstName = '" + f + "' AND LastName = '" + l + "')";
			}
				
			sql += " OR CONCAT(FirstName, ' ', LastName) = '" + repUserID + "'";
			
			row.setStatement(sql);
			//row.setParameter("-select1", "UserID");
			//row.setParameter("UserID|1", repUserID);
			//row.setParameter("FirstName|1", repUserID);
			//row.setParameter("LastName|1", repUserID);
			//row.setParameter("CONCAT(FirstName, ' ', LastName)|1", repUserID);
			row.doAction("execute");
			
			if (row.isSuccessful()) {
				return (row.getData("UserID"));
			}
		}
		return (null);
	}

	protected String findContact(String firstname, String lastname) {
		if (firstname != null && firstname.length() > 0 && lastname != null
				&& lastname.length() > 0) {
			GenRow row = new GenRow();
			row.setConnection(con);
			row.setTableSpec("Contacts");
			row.setParameter("-select1", "ContactID");
			row.setParameter("FirstName", firstname);
			row.setParameter("LastName", lastname);
			row.doAction("selectfirst");
			if (row.isSuccessful()) {
				return (row.getString("ContactID"));
			}
		}
		return (null);
	}

	protected String findContact(String repUserID) {
		if (repUserID != null && repUserID.length() > 0) {
			GenRow row = new GenRow();
			row.setConnection(con);
			row.setTableSpec("Contacts");
			row.setParameter("-select1", "ContactID");
			row.setParameter("ContactID", repUserID);
			row.doAction("selectfirst");
			if (row.isSuccessful()) {
				return (row.getString("ContactID"));
			}
		}
		return (null);
	}
	
	protected String lookupRep(ImportConfiguration.Table table,
			ImportConfiguration.Mapping mapping, String repname) {
		String returnvalue = repname;
		if (returnvalue != null && returnvalue.length() > 0
				&& returnvalue.indexOf(' ') > 0) {
			String[] name = returnvalue.split("\\ ");
			if (name.length == 2) {
				String repid = findRep(name[0], name[1]);
				if (repid != null) {
					returnvalue = repid;
				}
			}
			if (name == null || name.length == 1 || name.length == 1) {
				String repid = findRep(returnvalue);
				if (repid != null) {
					returnvalue = repid;
				}
			}
		}
		return (returnvalue);
	}

	protected String lookupStatus(ImportConfiguration.Table table,
			ImportConfiguration.Mapping mapping, String status) {
		String returnvalue = status;

		if (returnvalue != null && returnvalue.length() > 0) {
			GenRow row = new GenRow();
			row.setConnection(con);
			// System.out.println("Look Up Status for " + table.getTableSpec());
			if (table.getTableSpec().indexOf("Products") >= 0) {
				row.setTableSpec("StatusList");
				row.setParameter("-select1", "StatusID");
				row.setParameter("Status", returnvalue);
				row.setParameter("ListName", "Product");
			} else {
				row.setTableSpec("LinkedStatuses");
				row.setParameter("-select1", "StatusID");
				if (table.getTableSpec().indexOf("Contacts") >= 0) {
					row.setParameter("LinkedStatusStatus.ForContacts", "Y");
				}
				if (table.getTableSpec().indexOf("Companies") >= 0) {
					row.setParameter("LinkedStatusStatus.ForCompanies", "Y");
				}
				row.setParameter("LinkedStatusStatus.Status|1", returnvalue);
				row.setParameter("LinkedStatusStatus.StatusID|1", returnvalue);
			}
			row.doAction("selectfirst");
			if (row.isSuccessful()) {
				returnvalue = row.getString("StatusID");
			}
		}

		return (returnvalue);
	}
	
	protected String lookupCompany(ImportConfiguration.Table table, ImportConfiguration.Mapping mapping, String status) {
		String returnvalue = status;

		if (returnvalue != null && returnvalue.length() > 0) {
			GenRow row = new GenRow();
			row.setConnection(con);
			// System.out.println("Look Up Status for " + table.getTableSpec());
			if (table.getTableSpec().indexOf("Products") >= 0) {
				row.setTableSpec("Companies");
				row.setParameter("-select1", "CompanyID");
				row.setParameter("Company|1", returnvalue);
				row.setParameter("CompanyID|1", returnvalue);
			}
			row.doAction("selectfirst");
			if (row.isSuccessful()) {
				returnvalue = row.getData("CompanyID");
			}
		}

		return (returnvalue);
	}
	
	protected String lookupContact(ImportConfiguration.Table table, ImportConfiguration.Mapping mapping, String repname) {
		String returnvalue = repname;
		if (returnvalue != null && returnvalue.length() > 0
				&& returnvalue.indexOf(' ') > 0) {
			String[] name = returnvalue.split("\\ ");
			if (name.length == 2) {
				String repid = findContact(name[0], name[1]);
				if (repid != null) {
					returnvalue = repid;
				}
			}
			if (name == null || name.length == 1 || name.length == 1) {
				String repid = findContact(returnvalue);
				if (repid != null) {
					returnvalue = repid;
				}
			}
		}
		return (returnvalue);
	}


	protected boolean isNumberField(TableSpec tspec, String fieldname) {
		int index = tspec.getFieldsIndex(fieldname);
		if (index >= 0) {
			int fieldtype = tspec.getFieldType(index);
			if (fieldtype == SQLKeys.jdbctype_int
					|| fieldtype == SQLKeys.jdbctype_float) {
				return (true);
			}
		}
		return (false);
	}

	protected boolean mapColumn(Map row, ImportConfiguration.Table table,
			ImportConfiguration.Mapping mapping) {
		if (logger.isDebugEnabled())
			logger.debug("mapColumn(Row, {}, {})", table.tableSpecname, mapping != null ? mapping.toString() : "null");
		boolean hasvalue = false;

		if (mapping.source != null) {
			if (mapping.getType().equals(
					ImportConfiguration.MAPPING_TYPE_IMPORT)) {
				String value = (String) row.get(mapping.source);
				if (value != null) {
					value = value.trim();
					if (mapping.destination.equals("RepUserID")) {
						value = lookupRep(table, mapping, value);
					} else if (mapping.destination.equals("StatusID")) {
						value = lookupStatus(table, mapping, value);
					} else if (mapping.destination.equals("Developer") || mapping.destination.equals("SellingAgent")) {
						value = lookupCompany(table, mapping, value);
					} else if (mapping.destination.equals("Vendor") || mapping.destination.equals("SellingAgentContactID")) {
						value = lookupContact(table, mapping, value);
					}
					if (value.length() != 0) {
						if (isNumberField(table.data.getTableSpec(),
								mapping.destination)) {
							if (value != null && value.length() > 0) value = value.replaceAll("[^0-9\\-\\.]", "");
							value = StringUtil.toDecimal(value);
						}
						table.data.setParameter(mapping.destination, value);
						hasvalue = true;
					}
				}
				logDebug("setting column {} to {} with {}",
						mapping.destination, mapping.source, value);
				if (mapping.isDedup()) {
					logDebug("setting dedup column {} to {}",
							mapping.destination, value);
					if (value != null && value.length() != 0) {
						table.dedup.setParameter(mapping.destination, value);
					} else {
						table.dedup.setParameter(mapping.destination,
								"EMPTY+NULL");
					}
					table.dedup.setAction("selectfirst");
				}

			} else if (mapping.getType().equals(
					ImportConfiguration.MAPPING_TYPE_STATIC)) {

				logDebug("setting column {} to {}", mapping.destination,
						mapping.source);
				table.data.setParameter(mapping.destination, mapping.source);
				if (mapping.isDedup()) {
					logDebug("setting dedup column {} to {}",
							mapping.destination, mapping.source);
					table.dedup.setParameter(mapping.destination,
							mapping.source);
					table.dedup.setAction("selectfirst");
				}
			}
		}
		return (hasvalue);
	}

	public void execute() {
		try {
			getConnection();
			if (config.getImportFile() != null) {
				loadFile(config.getImportFile());
			} else {
				loadDatabase();
			}
		} catch (Exception e) {
			log(e, "Error on execute");
			if (debug)
				e.printStackTrace();
		} finally {
			complete = true;
			status.append(log);
			closeConnection();
		}
	}

	public boolean isFinished() {
		return (complete);
	}

	protected void logCVSHeader(String[] columns) {
		if (debug) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < columns.length; i++) {
				sb.append(columns[i]);
				sb.append(", ");
			}
			logDebug(sb.toString());
		}
	}

	public int countLines(File file) throws IOException {
		LineNumberReader reader = new LineNumberReader(new FileReader(file));
		int cnt = 0;
		String lineRead = "";
		while ((lineRead = reader.readLine()) != null) {
		}
		cnt = reader.getLineNumber();
		if (cnt > 1) {
			cnt--;
		}
		reader.close();

		return cnt;
	}

	public int getProcessSize() {
		return numRecords;
	}

	public int getProcessedCount() {
		return numProcessedRecords;
	}

	public int getImportLines() {
		return numLines;
	}

	public int getProcessedImportLines() {
		return numProcessedLines;
	}

	public int getErrorCount() {
		return numDataErrors;
	}

	public int getDuplicateRecords() {
		return numDuplicateRecords;
	}

	public int getUpdatedRecords() {
		return this.updatedRecords;
	}

	public int getInsertedRecords() {
		return this.insertedRecords;
	}

	public String getImportID() {
		return this.importid;
	}

	// Implemented Abstract Methods
	public String getStatusMailBody() {
		return status.toString();
	}

	public String getStatusMailSubject() {
		return (contextname + " Runway offline process - Import");
	}

	public String getStatusMailRecipient() {
		return (email);
	}

	public static void main(String[] args) {
		// ImportConfiguration config =
		// SpecManager.getImportConfiguration("imports/ImportContacts");
		// config = config.clone();
		// System.out.println(config);
	}

	public class CsvRowReader extends CsvMapReader {
		public CsvRowReader(final Reader reader, final CsvPreference preferences) {
			super(reader, preferences);
		}

		public Map<String, String> read(final String... nameMapping)
				throws IOException {
			final Map<String, String> destination = new HashMap<String, String>();
			super.line.clear();
			// read the line, if result, convert it to a map
			if (tokenizer.readStringList(super.line)) {
				mapStringList(destination, nameMapping, super.line);
				return destination;
			}

			return null;
		}

		public <T> void mapStringList(final Map<String, T> destination,
				final String[] nameMapper, final List<T> values) {

			destination.clear();

			// map each element of the array
			if (nameMapper.length != values.size())
				logDebug("Headings to Values Missmatch " + nameMapper.length
						+ " ~ " + values.size());
			for (int i = 0; i < nameMapper.length; i++) {
				final String key = nameMapper[i];

				// null's in the name mapping means skip column
				if (key == null) {
					continue;
				}

				// ignore dups
				if (!destination.containsKey(key)) {
					if (values.size() > i) {
						destination.put(key, values.get(i));
						if (nameMapper.length != values.size())
							logDebug("Headings to Values Missmatch value "
									+ values.get(i));
					}
				}

			}
		}
	}
}
