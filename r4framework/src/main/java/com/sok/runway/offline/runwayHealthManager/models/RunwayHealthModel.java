package com.sok.runway.offline.runwayHealthManager.models;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;

public class RunwayHealthModel {
	private String 	serial = KeyMaker.generate();
	
	private Date	healthTime = new Date();
	
	private long	serverUpTime = 0;
	private long	appUpTime = 0;
	
	private int		threadCount = 0;
	private int		connectionCount = 0;
	
	private double	maxConnectionTime = 0;
	private double	avgConnectionTime = 0;
	private double	avgSystemLoad = 0;
	private double	maxSystemLoad = 0;
	private double	avgAppLoad = 0;
	private double	maxAppLoad = 0;
	private double	avgAppTime = 0;
	private double	maxAppTime = 0;
	private double	totalAppTime = 0;
	private double	avgUsedMem = 0;
	private double	maxUsedMem = 0;
	private double	avgFreeMem = 0;
	private double	maxFreeMem = 0;
	private double	avgTotalMem = 0;
	private double	maxTotalMem = 0;
	private double	avgLimitMem = 0;
	private double	maxLimitMem = 0;
	private double	avgImages = 0;
	private double	avgImageTime = 0;
	private double	maxImages = 0;
	private double	maxImageTime = 0;
	private double	avgCSS = 0;
	private double	avgCSSTime = 0;
	private double	maxCSS = 0;
	private double	maxCSSTime = 0;
	private double	avgJS = 0;
	private double	avgJSTime = 0;
	private double	maxJS = 0;
	private double	maxJSTime = 0;
	private double	avgJSP = 0;
	private double	avgJSPTime = 0;
	private double	maxJSP = 0;
	private double	maxJSPTime = 0;
	private double	avgAPI = 0;
	private double	avgAPITime = 0;
	private double	maxAPI = 0;
	private double	maxAPITime = 0;
	private double	avgOther = 0;
	private double	avgOtherTime = 0;
	private double	maxOther = 0;
	private double	maxOtherTime = 0;

	public RunwayHealthModel() {
		// TODO Auto-generated constructor stub
	}
	
	public RunwayHealthModel(List<RunwayHealthModel> items) {
		processItem(items, 5);
	}
	
	public RunwayHealthModel(List<RunwayHealthModel> items, int minutes) {
		processItem(items, minutes);
	}
	
	private void processItem(List<RunwayHealthModel> items, int minutes) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.SECOND, 0);
		cal.add(Calendar.MINUTE, -(cal.get(Calendar.MINUTE) % minutes));
		
		int c = 0;
		
		for (int x = 0; x < items.size(); ++x) {
			RunwayHealthModel model = items.get(x);
			if (cal.after(model.getHealthTime())) break;
			++c;
			healthTime = cal.getTime();
			
			serverUpTime = model.serverUpTime;
			appUpTime = model.appUpTime;
			
			if (model.threadCount > threadCount) threadCount = model.threadCount;
			if (model.connectionCount > connectionCount) connectionCount = model.connectionCount;
			
			avgConnectionTime += model.avgConnectionTime;
			if (model.maxConnectionTime > maxConnectionTime) maxConnectionTime = model.maxConnectionTime;
			
			avgSystemLoad += model.avgSystemLoad;
			if (model.maxSystemLoad > maxSystemLoad) maxSystemLoad = model.maxSystemLoad;
			
			avgAppLoad += model.avgAppLoad;
			if (model.maxAppLoad > maxAppLoad) maxAppLoad = model.maxAppLoad;
			
			avgAppTime += model.avgAppTime;
			if (model.maxAppTime > maxAppTime) maxAppTime = model.maxAppTime;
			
			totalAppTime = model.totalAppTime;
			
			avgUsedMem += model.avgUsedMem;
			if (model.maxUsedMem > maxUsedMem) maxUsedMem = model.maxUsedMem;
			
			avgFreeMem += model.avgFreeMem;
			if (model.maxFreeMem > maxFreeMem) maxFreeMem = model.maxFreeMem;
			
			avgTotalMem += model.avgTotalMem;
			if (model.maxTotalMem > maxTotalMem) maxTotalMem = model.maxTotalMem;
			
			avgLimitMem += model.avgLimitMem;
			if (model.maxLimitMem > maxLimitMem) maxLimitMem = model.maxLimitMem;
			
			avgImages += model.avgImages;
			if (model.maxImages > maxImages) maxImages = model.maxImages;
			
			avgImageTime += model.avgImageTime;
			if (model.maxImageTime > maxImageTime) maxImageTime = model.maxImageTime;
			
			avgCSS += model.avgCSS;
			if (model.maxCSS > maxCSS) maxCSS = model.maxCSS;
			
			avgCSSTime += model.avgCSSTime;
			if (model.maxCSSTime > maxCSSTime) maxCSSTime = model.maxCSSTime;
			
			avgJS += model.avgJS;
			if (model.maxJS > maxJS) maxJS = model.maxJS;
			
			avgJSTime += model.avgJSTime;
			if (model.maxJSTime > maxJSTime) maxJSTime = model.maxJSTime;
			
			avgJSP += model.avgJSP;
			if (model.maxJSP > maxJSP) maxJSP = model.maxJSP;
			
			avgJSPTime += model.avgJSPTime;
			if (model.maxJSPTime > maxJSPTime) maxJSPTime = model.maxJSPTime;
			
			avgAPI += model.avgAPI;
			if (model.maxAPI > maxAPI) maxAPI = model.maxAPI;
			
			avgAPITime += model.avgAPITime;
			if (model.maxAPITime > maxAPITime) maxAPITime = model.maxAPITime;
			
			avgOther += model.avgOther;
			if (model.maxImages > maxOther) maxImages = model.maxOther;
			
			avgOtherTime += model.avgOtherTime;
			if (model.maxOtherTime > maxOtherTime) maxOtherTime = model.maxOtherTime;
		}
		
		if (c > 0) {
			avgConnectionTime /= c;
			
			avgSystemLoad /= c;
			avgAppLoad /= c;
			avgAppTime /= c;
			
			avgUsedMem /= c;
			avgFreeMem /= c;
			avgTotalMem /= c;
			avgLimitMem /= c;
			
			avgImages /= c;
			avgImageTime /= c;
			
			avgCSS /= c;
			avgCSSTime /= c;
			
			avgJS /= c;
			avgJSTime /= c;
			
			avgJSP /= c;
			avgJSPTime /= c;
			
			avgAPI /= c;
			avgAPITime /= c;
			
			avgOther /= c;
			avgOtherTime /= c;
		}
		
	}

	/**
	 * @return the serial
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * @param serial the serial to set
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
	 * @return the healthTime
	 */
	public Date getHealthTime() {
		return healthTime;
	}

	/**
	 * @param healthTime the healthTime to set
	 */
	public void setHealthTime(Date healthTime) {
		this.healthTime = healthTime;
	}

	/**
	 * @return the serverUpTime
	 */
	public long getServerUpTime() {
		return serverUpTime;
	}

	/**
	 * @param serverUpTime the serverUpTime to set
	 */
	public void setServerUpTime(long serverUpTime) {
		this.serverUpTime = serverUpTime;
	}

	/**
	 * @return the appUpTime
	 */
	public long getAppUpTime() {
		return appUpTime;
	}

	/**
	 * @param appUpTime the appUpTime to set
	 */
	public void setAppUpTime(long appUpTime) {
		this.appUpTime = appUpTime;
	}

	/**
	 * @return the threadCount
	 */
	public int getThreadCount() {
		return threadCount;
	}

	/**
	 * @param threadCount the threadCount to set
	 */
	public void setThreadCount(int threadCount) {
		this.threadCount = threadCount;
	}

	/**
	 * @return the connectionCount
	 */
	public int getConnectionCount() {
		return connectionCount;
	}

	/**
	 * @param connectionCount the connectionCount to set
	 */
	public void setConnectionCount(int connectionCount) {
		this.connectionCount = connectionCount;
	}

	/**
	 * @return the maxConnectionTime
	 */
	public double getMaxConnectionTime() {
		return maxConnectionTime;
	}

	/**
	 * @param maxConnectionTime the maxConnectionTime to set
	 */
	public void setMaxConnectionTime(double maxConnectionTime) {
		this.maxConnectionTime = maxConnectionTime;
	}

	/**
	 * @return the avgConnectionTime
	 */
	public double getAvgConnectionTime() {
		return avgConnectionTime;
	}

	/**
	 * @param avgConnectionTime the avgConnectionTime to set
	 */
	public void setAvgConnectionTime(double avgConnectionTime) {
		this.avgConnectionTime = avgConnectionTime;
	}

	/**
	 * @param systemLoad the avgSystemLoad and maxSystemLoad to set
	 */
	public void setSystemLoad(double systemLoad) {
		this.avgSystemLoad = systemLoad;
		this.maxSystemLoad = systemLoad;
	}

	/**
	 * @return the avgSystemLoad
	 */
	public double getAvgSystemLoad() {
		return avgSystemLoad;
	}

	/**
	 * @param avgSystemLoad the avgSystemLoad to set
	 */
	public void setAvgSystemLoad(double avgSystemLoad) {
		this.avgSystemLoad = avgSystemLoad;
	}

	/**
	 * @return the maxSystemLoad
	 */
	public double getMaxSystemLoad() {
		return maxSystemLoad;
	}

	/**
	 * @param maxSystemLoad the maxSystemLoad to set
	 */
	public void setMaxSystemLoad(double maxSystemLoad) {
		this.maxSystemLoad = maxSystemLoad;
	}

	/**
	 * @return the avgAppLoad
	 */
	public double getAvgAppLoad() {
		return avgAppLoad;
	}

	/**
	 * @param avgAppLoad the avgAppLoad to set
	 */
	public void setAvgAppLoad(double avgAppLoad) {
		this.avgAppLoad = avgAppLoad;
	}

	/**
	 * @return the maxAppLoad
	 */
	public double getMaxAppLoad() {
		return maxAppLoad;
	}

	/**
	 * @param maxAppLoad the maxAppLoad to set
	 */
	public void setMaxAppLoad(double maxAppLoad) {
		this.maxAppLoad = maxAppLoad;
	}

	/**
	 * @return the avgAppTime
	 */
	public double getAvgAppTime() {
		return avgAppTime;
	}

	/**
	 * @param avgAppTime the avgAppTime to set
	 */
	public void setAvgAppTime(double avgAppTime) {
		this.avgAppTime = avgAppTime;
	}

	/**
	 * @return the maxAppTime
	 */
	public double getMaxAppTime() {
		return maxAppTime;
	}

	/**
	 * @param maxAppTime the maxAppTime to set
	 */
	public void setMaxAppTime(double maxAppTime) {
		this.maxAppTime = maxAppTime;
	}

	/**
	 * @return the totalAppTime
	 */
	public double getTotalAppTime() {
		return totalAppTime;
	}

	/**
	 * @param totalAppTime the totalAppTime to set
	 */
	public void setTotalAppTime(double totalAppTime) {
		totalAppTime = totalAppTime;
	}

	/**
	 * @return the avgUsedMem
	 */
	public double getAvgUsedMem() {
		return avgUsedMem;
	}

	/**
	 * @param avgUsedMem the avgUsedMem to set
	 */
	public void setAvgUsedMem(double avgUsedMem) {
		this.avgUsedMem = avgUsedMem;
	}

	/**
	 * @return the maxUsedMem
	 */
	public double getMaxUsedMem() {
		return maxUsedMem;
	}

	/**
	 * @param maxUsedMem the maxUsedMem to set
	 */
	public void setMaxUsedMem(double maxUsedMem) {
		this.maxUsedMem = maxUsedMem;
	}

	/**
	 * @return the avgFreeMem
	 */
	public double getAvgFreeMem() {
		return avgFreeMem;
	}

	/**
	 * @param avgFreeMem the avgFreeMem to set
	 */
	public void setAvgFreeMem(double avgFreeMem) {
		this.avgFreeMem = avgFreeMem;
	}

	/**
	 * @return the maxFreeMem
	 */
	public double getMaxFreeMem() {
		return maxFreeMem;
	}

	/**
	 * @param maxFreeMem the maxFreeMem to set
	 */
	public void setMaxFreeMem(double maxFreeMem) {
		this.maxFreeMem = maxFreeMem;
	}

	/**
	 * @return the avgTotalMem
	 */
	public double getAvgTotalMem() {
		return avgTotalMem;
	}

	/**
	 * @param avgTotalMem the avgTotalMem to set
	 */
	public void setAvgTotalMem(double avgTotalMem) {
		this.avgTotalMem = avgTotalMem;
	}

	/**
	 * @return the maxTotalMem
	 */
	public double getMaxTotalMem() {
		return maxTotalMem;
	}

	/**
	 * @param maxTotalMem the maxTotalMem to set
	 */
	public void setMaxTotalMem(double maxTotalMem) {
		this.maxTotalMem = maxTotalMem;
	}

	/**
	 * @return the avgLimitMem
	 */
	public double getAvgLimitMem() {
		return avgLimitMem;
	}

	/**
	 * @param avgLimitMem the avgLimitMem to set
	 */
	public void setAvgLimitMem(double avgLimitMem) {
		this.avgLimitMem = avgLimitMem;
	}

	/**
	 * @return the maxLimitMem
	 */
	public double getMaxLimitMem() {
		return maxLimitMem;
	}

	/**
	 * @param maxLimitMem the maxLimitMem to set
	 */
	public void setMaxLimitMem(double maxLimitMem) {
		this.maxLimitMem = maxLimitMem;
	}

	/**
	 * @return the avgImages
	 */
	public double getAvgImages() {
		return avgImages;
	}

	/**
	 * @param avgImages the avgImages to set
	 */
	public void setAvgImages(double avgImages) {
		this.avgImages = avgImages;
	}

	/**
	 * @return the avgImageTime
	 */
	public double getAvgImageTime() {
		return avgImageTime;
	}

	/**
	 * @param avgImageTime the avgImageTime to set
	 */
	public void setAvgImageTime(double avgImageTime) {
		this.avgImageTime = avgImageTime;
	}

	/**
	 * @return the maxImages
	 */
	public double getMaxImages() {
		return maxImages;
	}

	/**
	 * @param maxImages the maxImages to set
	 */
	public void setMaxImages(double maxImages) {
		this.maxImages = maxImages;
	}

	/**
	 * @return the maxImageTime
	 */
	public double getMaxImageTime() {
		return maxImageTime;
	}

	/**
	 * @param maxImageTime the maxImageTime to set
	 */
	public void setMaxImageTime(double maxImageTime) {
		this.maxImageTime = maxImageTime;
	}

	/**
	 * @return the avgCSS
	 */
	public double getAvgCSS() {
		return avgCSS;
	}

	/**
	 * @param avgCSS the avgCSS to set
	 */
	public void setAvgCSS(double avgCSS) {
		this.avgCSS = avgCSS;
	}

	/**
	 * @return the avgCSSTime
	 */
	public double getAvgCSSTime() {
		return avgCSSTime;
	}

	/**
	 * @param avgCSSTime the avgCSSTime to set
	 */
	public void setAvgCSSTime(double avgCSSTime) {
		this.avgCSSTime = avgCSSTime;
	}

	/**
	 * @return the maxCSS
	 */
	public double getMaxCSS() {
		return maxCSS;
	}

	/**
	 * @param maxCSS the maxCSS to set
	 */
	public void setMaxCSS(double maxCSS) {
		this.maxCSS = maxCSS;
	}

	/**
	 * @return the maxCSSTime
	 */
	public double getMaxCSSTime() {
		return maxCSSTime;
	}

	/**
	 * @param maxCSSTime the maxCSSTime to set
	 */
	public void setMaxCSSTime(double maxCSSTime) {
		this.maxCSSTime = maxCSSTime;
	}

	/**
	 * @return the avgJS
	 */
	public double getAvgJS() {
		return avgJS;
	}

	/**
	 * @param avgJS the avgJS to set
	 */
	public void setAvgJS(double avgJS) {
		this.avgJS = avgJS;
	}

	/**
	 * @return the avgJSTime
	 */
	public double getAvgJSTime() {
		return avgJSTime;
	}

	/**
	 * @param avgJSTime the avgJSTime to set
	 */
	public void setAvgJSTime(double avgJSTime) {
		this.avgJSTime = avgJSTime;
	}

	/**
	 * @return the maxJS
	 */
	public double getMaxJS() {
		return maxJS;
	}

	/**
	 * @param maxJS the maxJS to set
	 */
	public void setMaxJS(double maxJS) {
		this.maxJS = maxJS;
	}

	/**
	 * @return the maxJSTime
	 */
	public double getMaxJSTime() {
		return maxJSTime;
	}

	/**
	 * @param maxJSTime the maxJSTime to set
	 */
	public void setMaxJSTime(double maxJSTime) {
		this.maxJSTime = maxJSTime;
	}

	/**
	 * @return the avgJSP
	 */
	public double getAvgJSP() {
		return avgJSP;
	}

	/**
	 * @param avgJSP the avgJSP to set
	 */
	public void setAvgJSP(double avgJSP) {
		this.avgJSP = avgJSP;
	}

	/**
	 * @return the avgJSPTime
	 */
	public double getAvgJSPTime() {
		return avgJSPTime;
	}

	/**
	 * @param avgJSPTime the avgJSPTime to set
	 */
	public void setAvgJSPTime(double avgJSPTime) {
		this.avgJSPTime = avgJSPTime;
	}

	/**
	 * @return the maxJSP
	 */
	public double getMaxJSP() {
		return maxJSP;
	}

	/**
	 * @param maxJSP the maxJSP to set
	 */
	public void setMaxJSP(double maxJSP) {
		this.maxJSP = maxJSP;
	}

	/**
	 * @return the maxJSPTime
	 */
	public double getMaxJSPTime() {
		return maxJSPTime;
	}

	/**
	 * @param maxJSPTime the maxJSPTime to set
	 */
	public void setMaxJSPTime(double maxJSPTime) {
		this.maxJSPTime = maxJSPTime;
	}

	/**
	 * @return the avgAPI
	 */
	public double getAvgAPI() {
		return avgAPI;
	}

	/**
	 * @param avgAPI the avgAPI to set
	 */
	public void setAvgAPI(double avgAPI) {
		this.avgAPI = avgAPI;
	}

	/**
	 * @return the avgAPITime
	 */
	public double getAvgAPITime() {
		return avgAPITime;
	}

	/**
	 * @param avgAPITime the avgAPITime to set
	 */
	public void setAvgAPITime(double avgAPITime) {
		this.avgAPITime = avgAPITime;
	}

	/**
	 * @return the maxAPI
	 */
	public double getMaxAPI() {
		return maxAPI;
	}

	/**
	 * @param maxAPI the maxAPI to set
	 */
	public void setMaxAPI(double maxAPI) {
		this.maxAPI = maxAPI;
	}

	/**
	 * @return the maxAPITime
	 */
	public double getMaxAPITime() {
		return maxAPITime;
	}

	/**
	 * @param maxAPITime the maxAPITime to set
	 */
	public void setMaxAPITime(double maxAPITime) {
		this.maxAPITime = maxAPITime;
	}

	/**
	 * @return the avgOther
	 */
	public double getAvgOther() {
		return avgOther;
	}

	/**
	 * @param avgOther the avgOther to set
	 */
	public void setAvgOther(double avgOther) {
		this.avgOther = avgOther;
	}

	/**
	 * @return the avgOtherTime
	 */
	public double getAvgOtherTime() {
		return avgOtherTime;
	}

	/**
	 * @param avgOtherTime the avgOtherTime to set
	 */
	public void setAvgOtherTime(double avgOtherTime) {
		this.avgOtherTime = avgOtherTime;
	}

	/**
	 * @return the maxOther
	 */
	public double getMaxOther() {
		return maxOther;
	}

	/**
	 * @param maxOther the maxOther to set
	 */
	public void setMaxOther(double maxOther) {
		this.maxOther = maxOther;
	}

	/**
	 * @return the maxOtherTime
	 */
	public double getMaxOtherTime() {
		return maxOtherTime;
	}

	/**
	 * @param maxOtherTime the maxOtherTime to set
	 */
	public void setMaxOtherTime(double maxOtherTime) {
		this.maxOtherTime = maxOtherTime;
	}
	
	/**
	 * Save the data to database
	 */
	public void save() {
		GenRow row = new GenRow();
		row.setTableSpec("RunwayHealth");

		row.setParameter("HealthTime",healthTime);
		row.setParameter("ServerUpTime",serverUpTime);
		row.setParameter("AppUpTime",appUpTime);
		row.setParameter("Threads",threadCount);
		row.setParameter("Connections",connectionCount);
		row.setParameter("MaxConnectionTime",maxConnectionTime);
		row.setParameter("AvgConnectionTime",avgConnectionTime);
		row.setParameter("AvgSystemLoad",avgSystemLoad);
		row.setParameter("MaxSystemLoad",maxSystemLoad);
		row.setParameter("AvgAppLoad",avgAppLoad);
		row.setParameter("MaxAppLoad",maxAppLoad);
		row.setParameter("AvgAppTime",avgAppTime);
		row.setParameter("MaxAppTime",maxAppTime);
		row.setParameter("TotalAppTime",totalAppTime);
		row.setParameter("AvgUsedMem",avgUsedMem);
		row.setParameter("MaxUsedMem",maxUsedMem);
		row.setParameter("AvgFreeMem",avgFreeMem);
		row.setParameter("MaxFreeMem",maxFreeMem);
		row.setParameter("AvgTotalMem",avgTotalMem);
		row.setParameter("MaxTotalMem",maxTotalMem);
		row.setParameter("AvgLimitMem",avgLimitMem);
		row.setParameter("MaxLimitMem",maxLimitMem);
		row.setParameter("AvgImages",avgImages);
		row.setParameter("AvgImageTime",avgImageTime);
		row.setParameter("MaxImages",maxImages);
		row.setParameter("MaxImageTime",maxImageTime);
		row.setParameter("AvgCSS",avgCSS);
		row.setParameter("AvgCSSTime",avgCSSTime);
		row.setParameter("MaxCSS",maxCSS);
		row.setParameter("MaxCSSTime",maxCSSTime);
		row.setParameter("AvgJS",avgJS);
		row.setParameter("AvgJSTime",avgJSTime);
		row.setParameter("MaxJS",maxJS);
		row.setParameter("MaxJSTime",maxJSTime);
		row.setParameter("AvgJSP",avgJSP);
		row.setParameter("AvgJSPTime",avgJSPTime);
		row.setParameter("MaxJSP",maxJSP);
		row.setParameter("MaxJSPTime",maxJSPTime);
		row.setParameter("AvgAPI",avgAPI);
		row.setParameter("AvgAPITime",avgAPITime);
		row.setParameter("MaxAPI",maxAPI);
		row.setParameter("MaxAPITime",maxAPITime);
		row.setParameter("AvgOther",avgOther);
		row.setParameter("AvgOtherTime",avgOtherTime);
		row.setParameter("MaxOther",maxOther);
		row.setParameter("MaxOtherTime",maxOtherTime);
		
		try {
			row.doAction("insert");
		} catch (Exception e) {
			row.setParameter("ON-HealthTime", healthTime);
			row.doAction("updateall");
		}
	}

}

/*
CREATE TABLE `RunwayHealth` (
`HealthID` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
`HealthTime` DATETIME NOT NULL,
`ServerUpTime` INTEGER UNSIGNED,
`AppUpTime` INTEGER UNSIGNED,
`Threads` INTEGER UNSIGNED,
`Connections` INTEGER UNSIGNED,
`MaxConnectionTime` DOUBLE,
`AvgConnectionTime` DOUBLE,
`AvgSystemLoad` DOUBLE,
`MaxSystemLoad` DOUBLE,
`AvgAppLoad` DOUBLE,
`MaxAppLoad` DOUBLE,
`AvgAppTime` DOUBLE,
`MaxAppTime` DOUBLE,
`TotalAppTime` DOUBLE,
`AvgUsedMem` DOUBLE,
`MaxUsedMem` DOUBLE,
`AvgFreeMem` DOUBLE,
`MaxFreeMem` DOUBLE,
`AvgTotalMem` DOUBLE,
`MaxTotalMem` DOUBLE,
`AvgLimitMem` DOUBLE,
`MaxLimitMem` DOUBLE,
`AvgImages` DOUBLE,
`AvgImageTime` DOUBLE,
`MaxImages` DOUBLE,
`MaxImageTime` DOUBLE,
`AvgCSS` DOUBLE,
`AvgCSSTime` DOUBLE,
`MaxCSS` DOUBLE,
`MaxCSSTime` DOUBLE,
`AvgJS` DOUBLE,
`AvgJSTime` DOUBLE,
`MaxJS` DOUBLE,
`MaxJSTime` DOUBLE,
`AvgJSP` DOUBLE,
`AvgJSPTime` DOUBLE,
`MaxJSP` DOUBLE,
`MaxJSPTime` DOUBLE,
`AvgAPI` DOUBLE,
`AvgAPITime` DOUBLE,
`MaxAPI` DOUBLE,
`MaxAPITime` DOUBLE,
`AvgOther` DOUBLE,
`AvgOtherTime` DOUBLE,
`MaxOther` DOUBLE,
`MaxOtherTime` DOUBLE,
PRIMARY KEY (`HealthID`),
UNIQUE INDEX `IX_HealthTime`(`HealthTime`),
INDEX `IX_HealthIndex`(`HealthTime`)
)
ENGINE = InnoDB;
*/