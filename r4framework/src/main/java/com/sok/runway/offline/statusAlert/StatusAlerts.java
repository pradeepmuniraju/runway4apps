package com.sok.runway.offline.statusAlert;

import java.io.StringWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.*;

import com.sok.runway.*;
import com.sok.runway.offline.*;
import com.sok.runway.email.*;
import com.sok.framework.*;
import com.sok.framework.generation.*;

import javax.servlet.*;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

public class StatusAlerts extends AbstractDaemonWorker
{   
   HashMap<String, UserAlert> alertusers = null;
   
   private static final String DEFAULT_VELOCITY_PATH = "/cms/emails/statusalerts/";
   
   protected String alerttext = "generic/generic_text.vt";
   protected String alerthtml = "generic/generic.vt";
   
   protected static final String contactsearchurl = "/crm/actions/nextcontact.jsp";
   protected static final String companysearchurl = "/crm/actions/nextcompanyfromsearch.jsp";
   
   protected String emailalertsurlhome = null;
   
   protected boolean isDaily = false;
   
	SimpleDateFormat					yyyymmdd = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	SimpleDateFormat					ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

	private String		dayOfMonth = "";
	private String		dayOfWeek = "";
	private	String		startTime = "00:00";
	private String		endTime = "07:00";
	
   public StatusAlerts(String configfilepath)
   {     
      super(configfilepath);
   }  
   
   protected void init()
   {
      super.init();
      
		if (!isDaily) {
			isDaily = true;
			// daily tasks, these need to be done once a day
			boolean isValidTime = isValidTime(new Date());
			if (isValidTime) {
				String lastDate = getCurrentDates("StatusAlerts-DailyTask");
				if (lastDate != null && lastDate.length() > 0) {
					try {
						isValidTime = !isValidTime(ddmmyyyy.parse(lastDate));
					} catch (Exception e) {}
				}
				System.out.println("RPM-DailyTask " + lastDate + " " + isValidTime);
			}
			if (isValidTime) {
				// TODO daily tasks go here
				Properties sysprops = InitServlet.getSystemParams();
			      if(sysprops!=null)
			      {
			         emailalertsurlhome = sysprops.getProperty("EmailAlertsURLHome");
			         if(emailalertsurlhome==null || emailalertsurlhome.length()==0)
			         {
			            emailalertsurlhome = urlhome;
			         }
			      }       
			      
			      alertusers = new HashMap<String, UserAlert>();
			      getAlerts();
				
				insertCurrentDates("StatusAlerts-DailyTask");
			}
			isDaily = false;
		}

   }
   
	private void insertCurrentDates(String param) {
		InitServlet.getSystemParams().put(param, ddmmyyyy.format(new Date()));

		GenRow row = new GenRow();
		row.setTableSpec("SystemParameters");
		row.setParameter("-select0", "SystemParameterID");
		row.setParameter("Name", param);
		row.doAction("selectfirst");

		if (row.isSuccessful()) {
			String sysParamID = row.getString("SystemParameterID");
			row.clear();
			row.setParameter("SystemParameterID", sysParamID);
			row.setParameter("Value", ddmmyyyy.format(new Date()));
			row.setParameter("Type", "Text");
			row.doAction("update");
		} else {
			row.setParameter("SystemParameterID", KeyMaker.generate());
			row.setParameter("Value", ddmmyyyy.format(new Date()));
			row.setParameter("Type", "Text");
			row.doAction("insert");
		}
	}

	private String getCurrentDates(String param) {
		GenRow row = new GenRow();
		row.setTableSpec("SystemParameters");
		row.setParameter("-select0", "Value");
		row.setParameter("Name", param);
		row.doAction("selectfirst");
		
		return row.getString("Value");
	}

	public boolean isValidTime(Date now) {
		
		if (now == null) return false;
		
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		
		start.setTime(new Date());
		end.setTime(new Date());
		
		try {
			String startHr = "";
			String startMin = "00";
			
			if (startTime.indexOf(":") >= 0) {
				startHr = startTime.substring(0,startTime.indexOf(":"));
				startMin = startTime.substring(startTime.indexOf(":") + 1);
			} else {
				startHr = dayOfMonth;
			}

			if (dayOfMonth != null && dayOfMonth.length() > 0) {
				start.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayOfMonth));
			} else if (dayOfWeek != null && dayOfWeek.length() > 0){
				start.set(Calendar.DAY_OF_WEEK, Integer.parseInt(dayOfWeek));
			}
			
			start.set(Calendar.HOUR_OF_DAY, Integer.parseInt(startHr));
			start.set(Calendar.MINUTE, Integer.parseInt(startMin));
			start.set(Calendar.SECOND,0);
		} catch (Exception e) {
			return false;
		}
		
		try {
			String endHr = "";
			String endMin = "00";
			
			if (endTime.indexOf(":") >= 0) {
				endHr = endTime.substring(0,endTime.indexOf(":"));
				endMin = endTime.substring(endTime.indexOf(":") + 1);
			} else {
				endMin = endTime;
			}

			if (dayOfMonth != null && dayOfMonth.length() > 0) {
				end.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayOfMonth));
			} else if (dayOfWeek != null && dayOfWeek.length() > 0){
				end.set(Calendar.DAY_OF_WEEK, Integer.parseInt(dayOfWeek));
			}
			
			end.set(Calendar.HOUR_OF_DAY, Integer.parseInt(endHr));
			end.set(Calendar.MINUTE, Integer.parseInt(endMin));
			end.set(Calendar.SECOND,0);
		} catch (Exception e) {
			return false;
		}
		
		// this so you can use 23:00 to 03:00
		if (end.before(start)) end.add(Calendar.DAY_OF_YEAR, 1);
		
		System.out.println("Time Check " + start.getTime().toString() + " < " + now.toString() + " < " + end.getTime().toString() + " = " + (!now.before(start.getTime()) && !now.after(end.getTime())));
		
		if (!now.before(start.getTime()) && !now.after(end.getTime())) return true;

		return false;
	}

	public String getProcessName()
   {
      return "Status Alerts";
   }

   protected void processItem()
   {

   }
   
   protected void finish()
   {
	   if (alertusers != null && alertusers.entrySet() != null) {
	      for (Map.Entry<String, UserAlert> useralert : alertusers.entrySet()) {
	         sendUserEmail(useralert.getValue());
	      }
	   }
   }
   
   protected void sendUserEmail(UserAlert alert)
   {
      debug("sending email to "+ alert.getEmail());
      try{
         
         RowBean context = new RowBean();
         context.put("URLHome", urlhome);
         context.put("EmailAlertsURLHome", emailalertsurlhome);
         context.put("alert", alert);
         
         String text = generateEmail(context, alerttext);
         String html = generateEmail(context, alerthtml);

         ServletConfig sc = InitServlet.getConfig();
         if(sc!=null && alert.getEmail().length()!=0)
         {
            MailerBean mailer = new MailerBean();
            mailer.setServletContext(sc.getServletContext());
            mailer.setFrom(defaultFrom);
            mailer.setTo(alert.getEmail());
            mailer.setSubject("Status Alerts");
            mailer.setTextbody(text);
            mailer.setHtmlbody(html);
            if(config.getPretend())
            {
               debug("email to "+ alert.getEmail() + "\r\n " + html);
            }
            else
            {
               String response = mailer.getResponse();
               debug("email to "+ alert.getEmail() + " " + response);
            }
         }
         
      }catch(Exception e){
         error(e);
      }
   }
   
   protected String generateEmail(TableData context, String script)
   {
      StringWriter text = new StringWriter();
      try{
         StringBuffer roottemplate = new StringBuffer();
         roottemplate.append("#parse(\"");
         roottemplate.append(DEFAULT_VELOCITY_PATH);
         roottemplate.append(script);
         roottemplate.append("\")");
   
         VelocityEngine ve = VelocityManager.getEngine();
         ve.evaluate( context, text, "StatusAlerts", roottemplate.toString());
   
         text.flush();
         text.close();

      }catch(Exception e){
         error(e);
      }
      return(text.toString());
   }
   
   public void close()
   {
      super.close();
      alertusers = null;
   }
   
   public void checkConfig()
   {
      debug("Checking config");
      config = StatusAlertsConfig.getStatusAlertsConfig(configfilepath);
      debug("Wait interval set to "+String.valueOf(config.getInterval()));
   }
   
   protected void getAlerts()
   {
      debug("Searching for alerts");
      getContactAlerts();
      getCompanyAlerts();
      getProcessAlerts();
      getContactDeadlines();
      getCompanyDeadlines();
      getProcessDeadlines();
   }
   
   protected void getContactAlerts()
   {
      GenRow row = new GenRow();
      try
      {
         row.setViewSpec("email/ContactGroupStatusAlertView");
         row.setConnection(con);
         row.setParameter("AlertDate", "<NOW");
         row.setParameter("DeadlineDate", ">NOW");
         row.setParameter("-sort1", "AlertDate");
         row.doAction("search");
         
         debug("searching with: \r\n"+ row.getStatement());
         row.getResults();
         
         while(row.getNext())
         {
            addAlert(
               row,
               getContactUrl(row.getData("ContactID")),
               "Alert"
               );
         }
      }
      catch(Exception e)
      {
         error(e);
      }
      finally
      {
         row.close();
      }
   }
   
   protected void getContactDeadlines()
   {
      GenRow row = new GenRow();
      try
      {
         row.setViewSpec("email/ContactGroupStatusAlertView");
         row.setConnection(con);
         //row.setParameter("DeadlineDate", "<NOW");
         row.setParameter("DeadlineDate", "TODAY");
         row.setParameter("-sort1", "DeadlineDate");
         row.doAction("search");
         
         debug("searching with: \r\n"+ row.getStatement());
         row.getResults();
         
         while(row.getNext())
         {
            addAlert(
               row,
               getContactUrl(row.getData("ContactID")),
               "Deadline"
               );
         }
      }
      catch(Exception e)
      {
         error(e);
      }
      finally
      {
         row.close();
      }
   }   
   
   protected void getCompanyAlerts()
   {
      GenRow row = new GenRow();
      try
      {
         row.setViewSpec("email/CompanyGroupStatusAlertView");
         row.setConnection(con);
         row.setParameter("AlertDate", "<NOW");
         row.setParameter("DeadlineDate", ">NOW");   
         row.setParameter("-sort1", "AlertDate");
         row.doAction("search");

         debug("searching with: \r\n"+ row.getStatement());
         row.getResults();
         
         while(row.getNext())
         {
            addAlert(
               row,
               getCompanyUrl(row.getData("CompanyID")),
               "Alert"
               );
         }
      }
      catch(Exception e)
      {
         error(e);
      }
      finally
      {
         row.close();
      }
   }   
   
   protected void getCompanyDeadlines()
   {
      GenRow row = new GenRow();
      try
      {
         row.setViewSpec("email/CompanyGroupStatusAlertView");
         row.setConnection(con);
         //row.setParameter("DeadlineDate", "<NOW");
         row.setParameter("DeadlineDate", "TODAY");
         row.setParameter("-sort1", "DeadlineDate");
         row.doAction("search");
         
         debug("searching with: \r\n"+ row.getStatement());
         row.getResults();
         
         while(row.getNext())
         {
            addAlert(
               row,
               getCompanyUrl(row.getData("CompanyID")),
               "Deadline"
               );
         }
      }
      catch(Exception e)
      {
         error(e);
      }
      finally
      {
         row.close();
      }
   }    
   
   protected void getProcessAlerts()
   {
      GenRow row = new GenRow();
      try
      {
         row.setViewSpec("email/OpportunityStatusAlertView");
         row.setConnection(con);
         row.setParameter("AlertDate", "<NOW");
         row.setParameter("DeadlineDate", ">NOW");
         row.setParameter("-sort1", "AlertDate");
         row.doAction("search");
         
         debug("searching with: \r\n"+ row.getStatement());
         row.getResults();
         
         while(row.getNext())
         {
            addAlert(
               row,
               getProcessUrl(row.getData("OpportunityID")),
               "Alert"
               );
         }
      }
      catch(Exception e)
      {
         error(e);
      }
      finally
      {
         row.close();
      }
   }
   
   protected void getProcessDeadlines()
   {
      GenRow row = new GenRow();
      try
      {
         row.setViewSpec("email/OpportunityStatusAlertView");
         row.setConnection(con);
         //row.setParameter("DeadlineDate", "<NOW");
         row.setParameter("DeadlineDate", "TODAY");
         row.setParameter("-sort1", "DeadlineDate");
         row.doAction("search");
         
         debug("searching with: \r\n"+ row.getStatement());
         row.getResults();
         
         while(row.getNext())
         {
            addAlert(
               row,
               getProcessUrl(row.getData("OpportunityID")),
               "Deadline"
               );
         }
      }
      catch(Exception e)
      {
         error(e);
      }
      finally
      {
         row.close();
      }
   }   
   
   protected void addAlert(GenRow row, String url, String type)
   {
      String userid, username, useremail = null;
      GenRow alert = getAlert(row.getString("GroupID"), row.getString("StatusID"));
      if(type.equals("Alert"))
      {
         if("Y".equals(alert.getString("EmailRepAlert"))){
            userid = row.getData("RepUserID");
            username = row.getData("RepFirstName")+ " " + row.getData("RepLastName");
            useremail = row.getData("RepEmail");
            addAlert(row, userid, username, useremail, url, type, false);
         }
         if("Y".equals(alert.getString("EmailMgrAlert"))){
            userid = row.getData("MgrUserID");
            username = row.getData("MgrFirstName")+ " " + row.getData("MgrLastName");
            useremail = row.getData("MgrEmail");
            addAlert(row, userid, username, useremail, url, type, true);
         }
      }
      else
      {
         if("Y".equals(alert.getString("EmailRepDeadline"))){
            userid = row.getData("RepUserID");
            username = row.getData("RepFirstName")+ " " + row.getData("RepLastName");
            useremail = row.getData("RepEmail");
            addAlert(row, userid, username, useremail, url, type, false);
         }
         if("Y".equals(alert.getString("EmailMgrDeadline"))){
            userid = row.getData("MgrUserID");
            username = row.getData("MgrFirstName")+ " " + row.getData("MgrLastName");
            useremail = row.getData("MgrEmail");
            addAlert(row, userid, username, useremail, url, type, true);
         }
      }
   }
  
   public GenRow getAlert(String groupid, String statusid)
   {
     GenRow alertdays = new GenRow();
     alertdays.setConnection(con);
     alertdays.setViewSpec("StatusAlertView");
     alertdays.setParameter("StatusAlertLinkedStatus.LinkedStatusesGroup.GroupID", groupid);
     alertdays.setParameter("StatusAlertLinkedStatus.StatusID", statusid);
     if(groupid.length()!=0 && statusid.length()!=0){
        alertdays.doAction("selectfirst");
     }
     debug("Alert: "+ alertdays.toString(true));

     return(alertdays);
   }   
   
   protected void setCounts(UserAlert useralert)
   {
      setOverdueContactsUrl(useralert);
      setOverdueContactsCount(useralert);
      setOverdueCompaniesUrl(useralert);
      setOverdueCompaniesCount(useralert);
      
      setPendingContactsUrl(useralert);
      setPendingContactsCount(useralert);
      setPendingCompaniesUrl(useralert);
      setPendingCompaniesCount(useralert);
      
      setImminentContactsUrl(useralert);
      setImminentContactsCount(useralert);
      setImminentCompaniesUrl(useralert);
      setImminentCompaniesCount(useralert);      
   }
   
   protected void addAlert(GenRow row, String userid, String username, String useremail, String url, String type, boolean isMgr)
   {
      UserAlert useralert = alertusers.get(userid);
      if(useralert == null)
      {
         useralert = new UserAlert(username, useremail, userid, isMgr);
         setCounts(useralert);
         alertusers.put(userid, useralert);
      }
      EmailerMap emailermap = getEmailerMap(row);
      emailermap.put("RecordUrl", url);
      
      debug("GenRow: "+ row.toString(true));
      debug("Map: "+ emailermap.toString());
      
      if(type.equals("Alert"))
      {
         useralert.getAlerts().add(emailermap);
      }
      else
      {
         useralert.getDeadlines().add(emailermap);
      }
   }
   
   protected void setOverdueContactsUrl(UserAlert alert)
   {
      StringBuffer url = new StringBuffer(emailalertsurlhome);
      url.append(contactsearchurl);
      url.append("?-mode=list&-action=search&-sort1=Contacts.FirstName&-sort2=Contacts.LastName");
      url.append("&ContactContactGroups%23R.ContactGroupCurrentStatus%23R.DeadlineDate=<NOW");
      if(alert.isMgr)
      {
         url.append("&ContactContactGroups%23R.ContactGroupRep%23R.MgrUserID=");
         url.append(alert.userid);
      }
      else
      {
         url.append("&ContactContactGroups%23R.RepUserID=");
         url.append(alert.userid);
      }
      alert.overdueContactsUrl = url.toString();
   }
   
   protected void setOverdueContactsCount(UserAlert alert)
   {
      GenRow count = new GenRow();
      count.setConnection(con);
      count.setTableSpec("Contacts"); 
      count.setParameter("-select1", "count(*) as Count");
      count.setParameter("ContactContactGroups.ContactGroupCurrentStatus.DeadlineDate","<NOW");
      if(alert.isMgr)
      {
         count.setParameter("ContactContactGroups.ContactGroupRep.MgrUserID", alert.userid);
      }
      else
      {
         count.setParameter("ContactContactGroups.RepUserID", alert.userid);
      }
      count.doAction("selectfirst");
      alert.overdueContactsCount = String.valueOf(count.getInt("Count"));
   }
   
   protected void setOverdueCompaniesUrl(UserAlert alert)
   {
      StringBuffer url = new StringBuffer(emailalertsurlhome);
      url.append(companysearchurl);
      url.append("?-mode=list&-action=search&-sort1=Name");
      url.append("&CompanyCompanyGroups%23R.CompanyGroupCurrentStatus%23R.DeadlineDate=<NOW");
      if(alert.isMgr)
      {
         url.append("&CompanyCompanyGroups%23R.CompanyGroupRep%23R.MgrUserID=");
         url.append(alert.userid);
      }
      else
      {
         url.append("&CompanyCompanyGroups%23R.RepUserID=");
         url.append(alert.userid);
      }
      alert.overdueCompaniesUrl = url.toString();
   }
   
   protected void setOverdueCompaniesCount(UserAlert alert)
   {
      GenRow count = new GenRow();
      count.setConnection(con);
      count.setTableSpec("Companies"); 
      count.setParameter("-select1", "count(*) as Count");
      count.setParameter("CompanyCompanyGroups.CompanyGroupCurrentStatus.DeadlineDate","<NOW");
      if(alert.isMgr)
      {
         count.setParameter("CompanyCompanyGroups.CompanyGroupRep.MgrUserID", alert.userid);
      }
      else
      {
         count.setParameter("CompanyCompanyGroups.RepUserID", alert.userid);
      }
      count.doAction("selectfirst");
      alert.overdueCompaniesCount = String.valueOf(count.getInt("Count"));
   }     
   
   protected void setPendingContactsUrl(UserAlert alert)
   {
      StringBuffer url = new StringBuffer(emailalertsurlhome);
      url.append(contactsearchurl);
      url.append("?-mode=list&-action=search&-sort1=Contacts.FirstName&-sort2=Contacts.LastName");
      url.append("&ContactContactGroups%23R.ContactGroupCurrentStatus%23R.AlertDate=>NOW");
      url.append("&ContactContactGroups%23R.ContactGroupCurrentStatus%23R.DeadlineDate=>NOW");
      if(alert.isMgr)
      {
         url.append("&ContactContactGroups%23R.ContactGroupRep%23R.MgrUserID=");
         url.append(alert.userid);
      }
      else
      {
         url.append("&ContactContactGroups%23R.RepUserID=");
         url.append(alert.userid);
      } 
      alert.pendingContactsUrl = url.toString();
   }
   
   protected void setPendingContactsCount(UserAlert alert)
   {
      GenRow count = new GenRow();
      count.setConnection(con);
      count.setTableSpec("Contacts"); 
      count.setParameter("-select1", "count(*) as Count");
      count.setParameter("ContactContactGroups.ContactGroupCurrentStatus.AlertDate",">NOW");
      count.setParameter("ContactContactGroups.ContactGroupCurrentStatus.DeadlineDate",">NOW");
      if(alert.isMgr)
      {
         count.setParameter("ContactContactGroups.ContactGroupRep.MgrUserID", alert.userid);
      }
      else
      {
         count.setParameter("ContactContactGroups.RepUserID", alert.userid);
      }
      count.doAction("selectfirst");
      alert.pendingContactsCount = String.valueOf(count.getInt("Count"));
   }   
   
   protected void setPendingCompaniesUrl(UserAlert alert)
   {
      StringBuffer url = new StringBuffer(emailalertsurlhome);
      url.append(companysearchurl);
      url.append("?-mode=list&-action=search&-sort1=Name");
      url.append("&CompanyCompanyGroups%23R.CompanyGroupCurrentStatus%23R.AlertDate=>NOW");
      url.append("&CompanyCompanyGroups%23R.CompanyGroupCurrentStatus%23R.DeadlineDate=>NOW");
      if(alert.isMgr)
      {
         url.append("&CompanyCompanyGroups%23R.CompanyGroupRep%23R.MgrUserID=");
         url.append(alert.userid);
      }
      else
      {
         url.append("&CompanyCompanyGroups%23R.RepUserID=");
         url.append(alert.userid);
      } 
      alert.pendingCompaniesUrl = url.toString();
   }
   
   protected void setPendingCompaniesCount(UserAlert alert)
   {
      GenRow count = new GenRow();
      count.setConnection(con);
      count.setTableSpec("Companies"); 
      count.setParameter("-select1", "count(*) as Count");
      count.setParameter("CompanyCompanyGroups.CompanyGroupCurrentStatus.AlertDate",">NOW");
      count.setParameter("CompanyCompanyGroups.CompanyGroupCurrentStatus.DeadlineDate",">NOW");
      if(alert.isMgr)
      {
         count.setParameter("CompanyCompanyGroups.CompanyGroupRep.MgrUserID", alert.userid);
      }
      else
      {
         count.setParameter("CompanyCompanyGroups.RepUserID", alert.userid);
      }
      count.doAction("selectfirst");
      alert.pendingCompaniesCount = String.valueOf(count.getInt("Count"));
   }    
   
   protected void setImminentCompaniesUrl(UserAlert alert)
   {
      StringBuffer url = new StringBuffer(emailalertsurlhome);
      url.append(companysearchurl);
      url.append("?-mode=list&-action=search&-sort1=Name");
      url.append("&CompanyCompanyGroups%23R.CompanyGroupCurrentStatus%23R.AlertDate=<NOW");
      url.append("&CompanyCompanyGroups%23R.CompanyGroupCurrentStatus%23R.DeadlineDate=>NOW");
      if(alert.isMgr)
      {
         url.append("&CompanyCompanyGroups%23R.CompanyGroupRep%23R.MgrUserID=");
         url.append(alert.userid);
      }
      else
      {
         url.append("&CompanyCompanyGroups%23R.RepUserID=");
         url.append(alert.userid);
      } 
      alert.imminentCompaniesUrl = url.toString();
   }
   
   protected void setImminentCompaniesCount(UserAlert alert)
   {
      GenRow count = new GenRow();
      count.setConnection(con);
      count.setTableSpec("Companies"); 
      count.setParameter("-select1", "count(*) as Count");
      count.setParameter("CompanyCompanyGroups.CompanyGroupCurrentStatus.AlertDate","<NOW");
      count.setParameter("CompanyCompanyGroups.CompanyGroupCurrentStatus.DeadlineDate",">NOW");
      if(alert.isMgr)
      {
         count.setParameter("CompanyCompanyGroups.CompanyGroupRep.MgrUserID", alert.userid);
      }
      else
      {
         count.setParameter("CompanyCompanyGroups.RepUserID", alert.userid);
      }
      count.doAction("selectfirst");
      alert.imminentCompaniesCount = String.valueOf(count.getInt("Count"));
   }     
   
   protected void setImminentContactsUrl(UserAlert alert)
   {
      StringBuffer url = new StringBuffer(emailalertsurlhome);
      url.append(contactsearchurl);
      url.append("?-mode=list&-action=search&-sort1=Contacts.FirstName&-sort2=Contacts.LastName");
      url.append("&ContactContactGroups%23R.ContactGroupCurrentStatus%23R.AlertDate=<NOW");
      url.append("&ContactContactGroups%23R.ContactGroupCurrentStatus%23R.DeadlineDate=>NOW");
      if(alert.isMgr)
      {
         url.append("&ContactContactGroups%23R.ContactGroupRep%23R.MgrUserID=");
         url.append(alert.userid);
      }
      else
      {
         url.append("&ContactContactGroups%23R.RepUserID=");
         url.append(alert.userid);
      } 
      alert.imminentContactsUrl = url.toString();
   }
   
   protected void setImminentContactsCount(UserAlert alert)
   {
      GenRow count = new GenRow();
      count.setConnection(con);
      count.setTableSpec("Contacts"); 
      count.setParameter("-select1", "count(*) as Count");
      count.setParameter("ContactContactGroups.ContactGroupCurrentStatus.AlertDate","<NOW");
      count.setParameter("ContactContactGroups.ContactGroupCurrentStatus.DeadlineDate",">NOW");
      if(alert.isMgr)
      {
         count.setParameter("ContactContactGroups.ContactGroupRep.MgrUserID", alert.userid);
      }
      else
      {
         count.setParameter("ContactContactGroups.RepUserID", alert.userid);
      }
      count.doAction("selectfirst");
      alert.imminentContactsCount = String.valueOf(count.getInt("Count"));
   } 
   
   protected EmailerMap getEmailerMap(GenRow row)
   {
      EmailerMap map = new EmailerMap();
      map.putAll(row);
      return(map);
   }
   
   protected String getContactUrl(String contactid)
   {
      StringBuffer url = new StringBuffer(emailalertsurlhome);
      url.append("/runway/contact.view?ContactID=");
      url.append(contactid);
      return(url.toString());
   }
   
   protected String getCompanyUrl(String companyid)
   {
      StringBuffer url = new StringBuffer(emailalertsurlhome);
      url.append("/runway/company.view?CompanyID=");
      url.append(companyid);
      return(url.toString());
   }   

   protected String getProcessUrl(String contactid)
   {
      StringBuffer url = new StringBuffer(emailalertsurlhome);
      url.append("/runway/process.view?OpportunityID=");
      url.append(contactid);
      return(url.toString());
   }
   
   
   public class UserAlert
   {
      protected String name;
      protected String email;
      protected String userid;
      protected boolean isMgr;
      protected ArrayList<Context> alerts = new ArrayList<Context>();
      protected ArrayList<Context> deadlines = new ArrayList<Context>();
      
      protected String overdueContactsCount;
      protected String overdueContactsUrl;
      
      protected String overdueCompaniesCount;
      protected String overdueCompaniesUrl;
      
      protected String imminentContactsCount;
      protected String imminentContactsUrl;
      
      protected String imminentCompaniesCount;
      protected String imminentCompaniesUrl;
      
      protected String pendingContactsCount;
      protected String pendingContactsUrl;
      
      protected String pendingCompaniesCount;
      protected String pendingCompaniesUrl; 
      
      public UserAlert(String name, String email, String userid, boolean isMgr)
      {
         this.name = name;
         this.email = email;
         this.userid = userid;
         this.isMgr = isMgr;
      }
      
      public String getName()
      {
         return name;
      }
      
      public String getEmail()
      {
         return email;
      }
      
      public String getUserID()
      {
         return userid;
      }
      
      public String getIsMgr()
      {
         return String.valueOf(isMgr);
      }
      
      public String getOverdueContactsCount()
      {
         return(overdueContactsCount);
      }
      
      public String getOverdueContactsUrl()
      {
         return(overdueContactsUrl);
      }
      
      public String getOverdueCompaniesCount()
      {
         return(overdueCompaniesCount);
      }
      
      public String getOverdueCompaniesUrl()
      {
         return(overdueCompaniesUrl);
      }
      
      public String getImminentContactsCount()
      {
         return(imminentContactsCount);
      }
      
      public String getImminentContactsUrl()
      {
         return(imminentContactsUrl);
      }
      
      public String getImminentCompaniesCount()
      {
         return(imminentCompaniesCount);
      }
      
      public String getImminentCompaniesUrl()
      {
         return(imminentCompaniesUrl);
      }
      
      public String getPendingContactsCount()
      {
         return(pendingContactsCount);
      }
      
      public String getPendingContactsUrl()
      {
         return(pendingContactsUrl);
      }
      
      public String getPendingCompaniesCount()
      {
         return(pendingCompaniesCount);
      }
      
      public String getPendingCompaniesUrl()
      {
         return(pendingCompaniesUrl);
      }
      
      public ArrayList<Context> getAlerts()
      { 
         return alerts;
      }
      
      public ArrayList<Context> getDeadlines()
      { 
         return deadlines;
      }      
   }
   
   public static void main(String[] args)
   {


   }



}
