package com.sok.runway.offline.runwayImport;
import org.dom4j.*;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultDocument;

import java.util.*;
import java.io.*;

import com.sok.framework.*;
import com.sok.framework.generation.*;

public class ImportConfiguration extends XMLSpec
{   
   private Element root = null;   
   public static String _name = "name";
   public static String _import = "import";
   public static String _nextconfig = "nextconfig";
   public static String _table = "table";
   public static String _relation = "relation";
   public static String _tablespecname = "tablespec";
   public static String _viewspecname = "viewspec";
   public static String _sourcedisplayspecname = "sourcedisplayspec";
   public static String _destinationdisplayspecname = "destinationdisplayspec";
   public static String _column = "column";
   public static String _isimport = "isimport";
   public static String _isdedup = "isdedup";
   public static String _isstatic = "isstatic"; 
   public static String _hidden = "hidden";
   public static String _source = "source"; 
   public static String _destination = "destination";  
   public static String _sourcelabel = "sourcelabel"; 
   public static String _destinationlabel = "destinationlabel";    
   public static String _type = "type"; 
   public static String _importfile = "importfile"; 
   public static String _overwrite = "overwrite"; 
   public static String _valuelist = "valuelist";
   public static String _sourcekey = "sourcekey";
   public static String _sourcevalue = "sourcevalue";
   public static String _main = "main";
   public static String _ignoredups = "ignoredups";
   public static String _hasoverwritecolumnselected = "hasoverwritecolumnselected";
   
   public static String _ignoredupsinfile = "ignoredupsinfile";
   public static String _ignoredupsindatabase = "ignoredupsindatabase";
   public static String _includenew = "includenew";
   
   public static String MAPPING_TYPE_STATIC = "static";
   public static String MAPPING_TYPE_IMPORT = "import";
   
   protected ArrayList<Source> sources = new ArrayList<Source>();

   protected String importname = null;
   protected String importfilepath = null;
   protected String nextconfig = null;
   protected File importfile = null; 
   
   boolean ignoredupsinfile = false;   
   
   boolean hasoverwritecolumnselected = false;
   
   public ImportConfiguration(String path) 
   {  
      this(path, false);
   }
   
   public ImportConfiguration(String path, boolean ignoreerror) 
   {  
      super(path, ignoreerror);
      if(super.document!=null)
      {        
         this.root = super.document.getRootElement();
         if(this.root != null)
         {
            loadSources();
         }
      }     
   }       
   
   public ImportConfiguration(Document document)
   {
      super(document);
      this.root = super.document.getRootElement();
      loadSources();
   }
   
   public ImportConfiguration(ByteArrayInputStream stream)
   {
	   try {
	       SAXReader reader = new SAXReader();
	       super.document = (Document) reader.read(stream);
		   
	       this.root = super.document.getRootElement();
	       loadSources();
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
   }
   
   public ImportConfiguration()
   {
      super();
   }
   
   public String getImportName()
   {
      return(importname);
   }
   
   public void setImportName(String name)
   {
      importname = name;
   }
   
   public String getImportFilePath()
   {
      return(importfilepath);
   }
   
   public void setImportFilePath(String filepath)
   {
      importfilepath = filepath;
   }  
   
   public void setHasOverwriteColumnSelected(boolean hasoverwritecolumnselected)
   {
      setAttribute(this.root, _hasoverwritecolumnselected, String.valueOf(hasoverwritecolumnselected));
      this.hasoverwritecolumnselected = hasoverwritecolumnselected;
   }
   
   public boolean hasOverwriteColumnSelected()
   {
      return(hasoverwritecolumnselected);
   }    
   
   public void setIgnoreDupsInFile(boolean ignoredupsinfile)
   {
      setAttribute(this.root, _ignoredupsinfile, String.valueOf(ignoredupsinfile));
      this.ignoredupsinfile = ignoredupsinfile;
   }
   
   public boolean ignoreDupsInFile()
   {
      return(ignoredupsinfile);
   }   
   
   public String getNextConfig()
   {
      return(nextconfig);
   }
   
   public void setNextConfig(String nextconfig)
   {
      this.nextconfig = nextconfig;
   }   
   
   public void setImportFile(File file)
   {
      importfile = file;
   }
   
   public File getImportFile(String contextpath)
   {
      if(importfile == null && importfilepath!=null)
      {
         importfile = new File(contextpath + importfilepath);
      }
      return(importfile);
   }   
   
   public File getImportFile()
   {
      return(importfile);
   }
   
   public String[] getHeaders() throws IOException
   {
      return ImportProcess.getHeaders(importfile);
   }
   
   protected void loadSources(){

      if(root==null)
      {
         throw new IllegalConfigurationException("Import configuration is empty: "+this.name); 
      }
      importname = root.attributeValue(_name);
      importfilepath = root.attributeValue(_importfile);
      nextconfig = root.attributeValue(_nextconfig);
      
      Element[] sourceeles = super.getElementArray(root, _source);
      Element[] tableeles = null;
      Element[] columneles = null;
      
      Source source = null;
      Table table = null;
      Mapping mapping = null;
      
      for(int n=0; n<sourceeles.length; n++){
         source = new Source(sourceeles[n]);
         source.type = getAttribute(sourceeles[n], _type);
         source.viewspec = getAttribute(sourceeles[n], _viewspecname);
         source.sourcekey = getAttribute(sourceeles[n], _sourcekey);
         source.sourcevalue = getAttribute(sourceeles[n], _sourcevalue);
         source.main = "true".equals(getAttribute(sourceeles[n], _main));
         tableeles = getElementArray(sourceeles[n], _table);
         
         for(int i=0; i<tableeles.length; i++){
            table = new Table(tableeles[i]);
            table.tableSpecname = getAttribute(tableeles[i], _tablespecname);
            table.sourceDisplaySpecname = getAttribute(tableeles[i], _sourcedisplayspecname);
            table.destinationDisplaySpecname = getAttribute(tableeles[i], _destinationdisplayspecname);
            table.relation = getAttribute(tableeles[i], _relation);
            table.main = "true".equals(getAttribute(tableeles[i], _main));
            table.ignoredups = "true".equals(getAttribute(tableeles[i], _ignoredups));
            table.ignoredupsindatabase = "true".equals(getAttribute(tableeles[i], _ignoredupsindatabase));
            table.includenew = !"false".equals(getAttribute(tableeles[i], _includenew));
            table.overwrite = !"false".equals(getAttribute(tableeles[i], _overwrite));
            columneles = getElementArray(tableeles[i], _column);

            for(int j=0; j<columneles.length; j++){
               mapping = new Mapping(columneles[j]);
               mapping.type = getAttribute(columneles[j], _type);
               mapping.isDedup = "true".equals(getAttribute(columneles[j], _isdedup));
               mapping.overwrite = "true".equals(getAttribute(columneles[j], _overwrite));
               mapping.hidden = "true".equals(getAttribute(columneles[j], _hidden));
               mapping.source = getAttribute(columneles[j], _source);
               mapping.destination = getAttribute(columneles[j], _destination);
               mapping.sourceLabel = getAttribute(columneles[j], _sourcelabel);
               mapping.destinationLabel = getAttribute(columneles[j], _destinationlabel); 
               mapping.valuelist = getAttribute(columneles[j], _valuelist);  
               table.mappings.add(mapping);
            }
            table.setLabels();
            source.tables.add(table);
         }
         sources.add(source);
      }
   }
   
   protected void saveSources(){

      Element[] sourceeles = super.getElementArray(root, _source); 
      Element[] tableeles = null;
      Element[] columneles = null;
      
      super.setAttribute(root, _name, importname);
      super.setAttribute(root, _importfile, importfilepath);
      super.setAttribute(root, _nextconfig, nextconfig);
      Source source = null;
      Table table = null;
      Mapping mapping = null;
      
      for(int n=0; n<sourceeles.length; n++){
         
         source = sources.get(n);
         setAttribute(sourceeles, n, _type,  source.type);
         setAttribute(sourceeles, n, _viewspecname,  source.viewspec);
         setAttribute(sourceeles, n, _sourcekey,  source.sourcekey);
         setAttribute(sourceeles, n, _sourcevalue,  source.sourcevalue);
         setAttribute(sourceeles, n, _main,  String.valueOf(source.main));
         tableeles = super.getElementArray(sourceeles[n], _table);
         
         for(int i=0; i<tableeles.length; i++){
            table = source.tables.get(i);
            setAttribute(tableeles, i, _tablespecname,  table.tableSpecname);
            setAttribute(tableeles, i, _destinationdisplayspecname,  table.destinationDisplaySpecname);
            setAttribute(tableeles, i, _sourcedisplayspecname,  table.sourceDisplaySpecname);
            setAttribute(tableeles, i, _relation,  table.relation);
            setAttribute(tableeles, i, _main, String.valueOf(table.main));
            setAttribute(tableeles, i, _ignoredups, String.valueOf(table.ignoredups));
            setAttribute(tableeles, i, _ignoredupsindatabase, String.valueOf(table.ignoredupsindatabase));
            setAttribute(tableeles, i, _includenew, String.valueOf(table.includenew));
            setAttribute(tableeles, i, _overwrite, String.valueOf(table.overwrite));
            columneles = super.getElementArray(tableeles[i], _column);
   
            for(int j=0; j<columneles.length; j++){
               mapping = table.mappings.get(j);
               setAttribute(columneles, j, _type, mapping.type);
               setAttribute(columneles, j, _isdedup, String.valueOf(mapping.isDedup));
               setAttribute(columneles, j, _overwrite, String.valueOf(mapping.overwrite));
               setAttribute(columneles, j, _hidden, String.valueOf(mapping.hidden));
               setAttribute(columneles, j, _source, mapping.source);
               setAttribute(columneles, j, _destination, mapping.destination);
               setAttribute(columneles, j, _sourcelabel, mapping.sourceLabel);
               setAttribute(columneles, j, _destinationlabel, mapping.destinationLabel);
               setAttribute(columneles, j, _valuelist, mapping.valuelist);
            }
         }
      }
   }   
  
   public String toString()
   {
      saveSources();
      return(super.toString());
   }
   
   public int getSourcesCount(){
      return(sources.size());
   }  
   
   public Source getSource(int i)
   {
      if(sources!=null && i < sources.size())
      {
         return(sources.get(i));
      }
      return(null);
   }   
   
   public Source getMainSource()
   {
      for(Source source: sources) {
    	  if(source.isMain()) {
    		  return source;
    	  }
      }
      return(null);
   }  
   
   public class Source
   {
      String type;
      String viewspec;
      String sourcekey;
      String sourcevalue;
      boolean main = false;
      Element node;
      protected ArrayList<Table> tables = null;
      protected ArrayList<String> sourcekeyvalues = null;
      
      public Source(Element node)
      {
         this.node = node;
         tables = new ArrayList<Table>();
         sourcekeyvalues = new ArrayList<String>();
      }
      
      public String getType()
      {
         return(type);
      } 
      
      public void setType(String type)
      {
         setAttribute(node, _type, type);
         this.type = type;
      } 
      
      public String getViewSpec()
      {
         return(viewspec);
      } 
      
      public void setViewSpec(String viewspec)
      {
         setAttribute(node, _viewspecname, viewspec);
         this.viewspec = viewspec;
      } 
      
      public String getSourceKey()
      {
         return(sourcekey);
      } 
      
      public void setSourceKey(String sourcekey)
      {
         setAttribute(node, _sourcekey, sourcekey);
         this.sourcekey = sourcekey;
      }       
      
      public String getSourceValue()
      {
         return(sourcevalue);
      } 
      
      public void setSourceValue(String sourcevalue)
      {
         setAttribute(node, _sourcevalue, sourcevalue);
         this.sourcevalue = sourcevalue;
      }        

      public boolean isMain()
      {
         return(main);
      }        
      
      public Table addTable(String tableSpecname, String relation, String sourceDisplaySpecname, String destinationDisplaySpecname)
      {
         node.addText(_t);
         Element childnode = node.addElement(_table);
         childnode.addText(_n);
         node.addText(_n);
         
         Table table = new Table(childnode);
         table.setTableSpec(tableSpecname);
         table.setRelation(relation);
         table.setSourceDisplaySpec(sourceDisplaySpecname);
         table.setDestinationDisplaySpec(destinationDisplaySpecname);
         tables.add(table);

         return(table);
      }
      
      public int getTablesCount(){
         return(tables.size());
      }  
      
      public Table getTable(int i)
      {
         if(tables!=null && i < tables.size())
         {
            return(tables.get(i));
         }
         return(null);
      }      
      
      public Table getMainTable()
      {
         Table table = null;
         for(int i=0; i<getTablesCount(); i++)
         {
            table = getTable(i);
            if(table.isMain())
            {
               return(table);
            }
         }
         return(null);
      } 
      
      public int getIndexForTableSpecName(String tablespecname)
      {
         for(int i=0; i<getTablesCount(); i++)
         {
            if(getTable(i) != null && getTable(i).tableSpecname.equals(tablespecname))
            {
               return(i);
            }
         }
         return(-1);
      }   
      
      public int getIndexForTableRelation(String relation)
      {
         for(int i=0; i<getTablesCount(); i++)
         {
            if(getTable(i) != null && getTable(i).relation.equals(relation))
            {
               return(i);
            }
         }
         return(-1);
      }          
   }
   
   public class Table
   {
      ArrayList<Mapping> mappings;
      String tableSpecname;
      String destinationDisplaySpecname;
      String sourceDisplaySpecname;      
      String relation = "";
      GenRow data = new GenRow();
      GenRow dedup = new GenRow();
      Element node;
      DisplaySpec ds;
      boolean main = false;
      boolean ignoredups = false;
      

      boolean ignoredupsindatabase = false;
      boolean overwrite = true;
      boolean includenew = true;   
      
      public Table(Element node)
      {
         this.node = node;
         mappings = new ArrayList<Mapping>();
      }
      
      public void addMapping(String type, boolean isDedup, boolean overwrite,
            String source, String destination, String sourceLabel, String destinationLabel, String valuelist, boolean hidden)
      {
         node.addText(_t);
         Element childnode = node.addElement(_column);
         node.addText(_n);
         
         Mapping map = new Mapping(childnode);
         map.setType(type);
         map.setIsDedup(isDedup);
         map.setHidden(hidden);
         map.setOverwrite(overwrite);
         map.setSource(source);
         map.setDestination(destination);
         map.setSourceLabel(sourceLabel);
         map.setDestinationLabel(destinationLabel);
         map.setValueList(valuelist);
         
         mappings.add(map);
      }      
      
      public Mapping getMapping(int i)
      {
         return(mappings.get(i));
      }
      
      public int getMappingCount()
      {
         if(mappings == null)
         {
            return(0);
         }
         return(mappings.size());
      }
      
      public String getTableSpec()
      {
         return(tableSpecname);
      }      
      
      public String getDestinationDisplaySpec()
      {
         return(destinationDisplaySpecname);
      } 
      
      public String getSourceDisplaySpec()
      {
         return(sourceDisplaySpecname);
      } 
      
      public String getRelation()
      {
         return(relation);
      } 

      public void setTableSpec(String tableSpecname)
      {
         setAttribute(node, _tablespecname, tableSpecname);
         this.tableSpecname = tableSpecname;
      } 
      
      public void setDestinationDisplaySpec(String displaySpecname)
      {
         setAttribute(node, _destinationdisplayspecname, destinationDisplaySpecname);
         this.destinationDisplaySpecname = displaySpecname;
      } 
      
      public void setSourceDisplaySpec(String displaySpecname)
      {
         setAttribute(node, _sourcedisplayspecname, sourceDisplaySpecname);
         this.sourceDisplaySpecname = displaySpecname;
      }       
      
      public void setRelation(String relation)
      {
         setAttribute(node, _relation, relation);
         this.relation = relation;
      }      
      
      public boolean isMain()
      {
         return(main);
      }  
      
      public void setIgnoreDupsInDatabase(boolean ignoredupsindatabase)
      {
         setAttribute(node, _ignoredupsindatabase, String.valueOf(ignoredupsindatabase));
         this.ignoredupsindatabase = ignoredupsindatabase;
      } 
      
      public boolean ignoreDupsInDatabase()
      {
         return(ignoredupsindatabase);
      }   
      
      public void setOverwrite(boolean overwrite)
      {
         setAttribute(node, _overwrite, String.valueOf(overwrite));
         this.overwrite = overwrite;
      }    
      
      public boolean overwrite()
      {
         return(overwrite);
      }    
      
      public void setIncludeNew(boolean includenew)
      {
         setAttribute(node, _includenew, String.valueOf(includenew));
         this.includenew = includenew;
      }  
      
      public boolean includeNew()
      {
         return(includenew);
      }        
      
      protected void setLabels()
      {
         Mapping mapping = null;
         
         DisplaySpec destdspec = null, sourcedspec = null;
         if(destinationDisplaySpecname!=null && destinationDisplaySpecname.length()!=0)
         {
            destdspec = SpecManager.getDisplaySpec(destinationDisplaySpecname);
         }
         if(sourceDisplaySpecname!=null && sourceDisplaySpecname.length()!=0)
         {
            sourcedspec = SpecManager.getDisplaySpec(sourceDisplaySpecname);
         }
         String label = null;
         for(int i=0; i<mappings.size(); i++)
         {
            mapping = mappings.get(i);
            label = null;
            if(mapping.getDestinationLabel()==null || mapping.getDestinationLabel().length()==0)
            {
               if(destdspec != null)
               {
                  label = destdspec.getItemLabel(mapping.getDestination());
               }
               if(label == null || label.length()==0)
               {
                  label = mapping.getDestination();
               }
               mapping.setDestinationLabel(label);               
            }
            
            if(mapping.getSourceLabel()==null || mapping.getSourceLabel().length()==0)
            {
               if(sourcedspec != null)
               {
                  label = sourcedspec.getItemLabel(mapping.getSource());
               }
               if(label == null || label.length()==0)
               {
                  label = mapping.getSource();
               }                  
               mapping.setSourceLabel(label);               
            }            
         }
      }
      
   }
   
   private String replaceDebug(String replace, String... args) {
	   if (replace == null || replace.length() == 0) return replace;
	   try { 
		   if(args != null && args.length > 0) {
			   for(String s: args) {
				   if (s == null) s = "null";
				   replace = replace.replaceFirst("\\{\\}*", s);
			   }
		   }
	   } catch (Exception e) { e.printStackTrace(); } 
	   return replace;
   }
   
   public class Mapping
   {  
      String source;
      String destination;
      String sourceLabel;
      String destinationLabel;      
      String type;  
      String valuelist;   
      Element node;
      boolean isDedup = false; 
      boolean overwrite = false; 
      boolean hidden = false; 
      
      @Override
      public String toString() {
    	  return replaceDebug("Mapping(source={},destination={},sourceLabel={},destinationLabel={},type={},valuelist={},isDedup={},overwrite={},hidden={}", 
    			  				source, destination, String.valueOf(sourceLabel), String.valueOf(destinationLabel), String.valueOf(type), String.valueOf(valuelist), String.valueOf(isDedup), String.valueOf(overwrite), String.valueOf(hidden));
      }
      
      public Mapping(Element node)
      {
         this.node = node;
      }
      
      public String getType()
      {
         return(type);
      }
      
      public boolean isDedup()
      {
         return(isDedup);
      }
      
      public boolean isOverwrite()
      {
         return(overwrite);
      }
      
      public boolean isHidden()
      {
         return(hidden);
      }
      
      public String getSource()
      {
         return(source);
      }    
      
      public String getDestination()
      {
         return(destination);
      } 
      
      public String getSourceLabel()
      {
         return(sourceLabel);
      }    
      
      public String getDestinationLabel()
      {
         return(destinationLabel);
      }  
      
      public String getValueList()
      {
         return(valuelist);
      }     
      
      public void setType(String type)
      {
         setAttribute(node, _type, type);
         this.type = type;
      }
      
      public void setIsDedup(boolean isDedup)
      {
         setAttribute(node, _isdedup, String.valueOf(isDedup));
         this.isDedup = isDedup;
      }
      
      public void setOverwrite(boolean overwrite)
      {
         setAttribute(node, _overwrite, String.valueOf(overwrite));
         this.overwrite = overwrite;
      }
      
      public void setHidden(boolean hidden)
      {
         setAttribute(node, _hidden, String.valueOf(hidden));
         this.hidden = hidden;
      }
      
      public void setSource(String source)
      {
         setAttribute(node, _source, source);         
        this.source = source;
      }    
      
      public void setDestination(String destination)
      {
         setAttribute(node, _destination, destination);
         this.destination = destination;
      }   
      
      public void setSourceLabel(String sourcelabel)
      {
         setAttribute(node, _sourcelabel, sourceLabel);
        this.sourceLabel = sourcelabel;
      }    
      
      public void setDestinationLabel(String destinationlabel)
      {
         setAttribute(node, _destinationlabel, destinationLabel);
         this.destinationLabel = destinationlabel;
      }
      
      public void setValueList(String valuelist)
      {
         setAttribute(node, _valuelist, valuelist);
         this.valuelist = valuelist;
      }
   }
   
   public ImportConfiguration clone()
   {
      Document doc = (Document)document.clone();
      return(new ImportConfiguration(doc));
   }
   
   public void fromString(String documentstring)
   {
      super.fromString(documentstring);
      this.root = super.document.getRootElement();
      loadSources();
   }
   
}
/*
 * <import name="">
 *    <table relation="" tablespecname="" sourcedisplayspec="" destinationdisplayspec="">
 *       <column type="" isdedup="" source="" sourceLabel="" destination="" destinationLabel="" />
 *    </table>
 * </import> 
 * 
 */
