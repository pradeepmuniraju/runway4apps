package com.sok.runway.offline.facebook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.*;
import com.restfb.types.*;
import com.sok.framework.ActionBean;
import com.sok.framework.DisplaySpec;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.MailerBean;
import com.sok.framework.RowBean;
import com.sok.framework.SpecManager;
import com.sok.framework.TableData;
import com.sok.framework.TableSpec;
import com.sok.framework.VelocityManager;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.email.EmailerMap;
import com.sok.runway.offline.AbstractDaemonWorker;
import com.sok.runway.offline.ThreadManager;
import com.sok.runway.offline.halManager.HouseAndLandManager;
import com.sok.runway.view.crm.ProductView;
import com.sok.runway5.properties.dao.PropertyBuilderList;
import com.sok.runway5.properties.dao.PropertyBuildingDeveloperList;
import com.sok.runway5.properties.dao.PropertyBuildingList;
import com.sok.runway5.properties.dao.PropertyBuildingStageList;
import com.sok.runway5.properties.dao.PropertyDesignList;
import com.sok.runway5.properties.dao.PropertyDeveloperList;
import com.sok.runway5.properties.dao.PropertyEstateList;
import com.sok.runway5.properties.dao.PropertyRangeList;
import com.sok.runway5.properties.dao.PropertyStageList;
import com.sok.runway5.properties.entity.PropertyBuilding;
import com.sok.runway5.properties.entity.PropertyEntity;
import com.sok.runway5.properties.entity.PropertyEstate;
import com.sok.service.crm.cms.properties.DripService;
import com.sok.service.crm.cms.properties.DripService.DripProduct;

public class FacebookTask extends AbstractDaemonWorker {
	private static final Logger logger = LoggerFactory.getLogger(FacebookTask.class);
	private static final DripService dripService = DripService.getInstance();
	private static final String DEFAULT_VELOCITY_PATH = "/cms/emails/offlinealerts/";

	protected String alerttext = "generic/generic_text.vt";
	protected String alerthtml = "generic/generic.vt";

	private static FacebookTask facebook;

	SimpleDateFormat yyyymmdd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	SimpleDateFormat runyyyymmdd = new SimpleDateFormat("yyyy-MM-dd");

	private boolean hasInit = false;

	private Connection con = null;

	protected String dbconn;
	protected ServletContext context;
	protected String contextpath;
	protected String contextname = null;
	protected Locale thislocale;
	
	private boolean running = false;

	public FacebookTask(String configfilepath) {
		super(configfilepath);

		if (facebook == null)
			facebook = this;
	}

	public static FacebookTask getInstance() {
		if (facebook == null)
			facebook = new FacebookTask("");
		return facebook;
	}

	/**
	 * The init method is called when the worker is woken up from sleep. Any
	 * actions that need to be performed at that interval should be defined
	 * here.
	 */
	protected void init() {
		urlhome = InitServlet.getSystemParam("URLHome");

		logger.info("Initializing Facebook Task");
		if (!hasInit) {
		}
		if (!running ) {
			running = true;
			
			
			running = false;
		}
	}
	
	protected void finalize() {
	}

	@Override
	public void checkConfig() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getProcessName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void finish() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void processItem() {
		// TODO Auto-generated method stub
		
	}
	
	protected void checkForLikes() {
		String pageCode = InitServlet.getSystemParam("FaceBook-PageCode");
		String pageID = InitServlet.getSystemParam("FaceBook-PageID");
		
		if (StringUtils.isBlank(pageCode) || StringUtils.isBlank(pageID)) return;
		
		String secret = InitServlet.getSystemParam("FaceBook-ApplicationSecret");

		FacebookClient facebookClient = null;

		if (StringUtils.isNotBlank(secret))
			facebookClient = new DefaultFacebookClient(pageCode, secret, Version.VERSION_2_4);
		else
			facebookClient = new DefaultFacebookClient(pageCode, Version.VERSION_2_4);

		GenRow face = new GenRow();
		face.setTableSpec("Facebook");
		face.setParameter("-select0", "*");
		face.setParameter("CreatedDate", "LAST30DAYS+TODAY");
		face.doAction("search");
		
		face.getResults();
	
		while (face.getNext()) {
			if (StringUtils.isNotBlank(face.getString("FacebookPostID"))) {
			    FacebookType publishPhotoResponse = facebookClient.publish(face.getString("FacebookPostID") + "/likes?fields=name,link,profile_type,username,picture", FacebookType.class);

			}
			
		}
	}

	
}