package com.sok.runway.offline.r2r;

import com.sok.runway.offline.AbstractDaemonWorkerConfig;

public class R2RConfig extends AbstractDaemonWorkerConfig
{
   protected static String fullpath = null;
   protected static R2RConfig rpmconfig = null;
   protected static final String filepath = "/WEB-INF/rpm.xml";
   
   public static R2RConfig getR2RConfig(String parentpath)
   {
      if(rpmconfig == null || rpmconfig.isModified())
      {
         loadConfig(parentpath);
      }
      return(rpmconfig);
   }

   protected static synchronized void loadConfig(String parentpath)
   {
      if(rpmconfig == null || rpmconfig.isModified())
      {
         if(fullpath == null)
         {
            StringBuffer sb = new StringBuffer(parentpath);
            sb.append(filepath);
            fullpath = sb.toString();
         }
         rpmconfig = new R2RConfig(fullpath);
      }
   }

   public R2RConfig(String path)
   {
      super(path);
   }

}
