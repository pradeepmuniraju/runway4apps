package com.sok.runway.offline.statusAlert;

import java.io.File;
import javax.servlet.ServletContext;
import org.dom4j.*;

import com.sok.runway.*;
import com.sok.runway.offline.*;
import com.sok.framework.*;

public class StatusAlertsConfig extends AbstractDaemonWorkerConfig
{
   protected static String fullpath = null;
   protected static StatusAlertsConfig statusalertconfig = null;
   protected static final String filepath = "/WEB-INF/statusalert.xml";
   
   public static StatusAlertsConfig getStatusAlertsConfig(String parentpath)
   {
      if(statusalertconfig == null || statusalertconfig.isModified())
      {
         loadConfig(parentpath);
      }
      return(statusalertconfig);
   }

   protected static synchronized void loadConfig(String parentpath)
   {
      if(statusalertconfig == null || statusalertconfig.isModified())
      {
         if(fullpath == null)
         {
            StringBuffer sb = new StringBuffer(parentpath);
            sb.append(filepath);
            fullpath = sb.toString();
         }
         statusalertconfig = new StatusAlertsConfig(fullpath);
      }
   }

   public StatusAlertsConfig(String path)
   {
      super(path);
   }

}
/* example config file
 * 
<statusalerts interval="120" debug="false" debugAddress="shinytoaster@gmail.com">
<!-- interval - wait seconds -->
<!-- debug="true|false", debugAddress used for testing -->
<!-- this file automatically reloads -->
</statusalerts>
*/