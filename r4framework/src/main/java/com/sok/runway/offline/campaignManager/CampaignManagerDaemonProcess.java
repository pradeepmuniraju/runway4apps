package com.sok.runway.offline.campaignManager;

import com.sok.framework.DaemonFactory;
import com.sok.framework.InitServlet;
import com.sok.runway.offline.AbstractDaemonWorkerController;
/*
 * To run it put this in your web.xml, you may need to use a daemonClass number which is not in use.
 * You will need a statusalert.xml in your WEB-INF
<servlet>
    <servlet-name>Initiate</servlet-name>
    <servlet-class>com.sok.framework.InitServlet</servlet-class>
    <load-on-startup>0</load-on-startup>
     <init-param>
       <param-name>daemonClass0</param-name>
       <param-value>com.sok.runway.offline.campaignManager.CampaignManagerDaemonProcess</param-value>
     </init-param>
</servlet>
*/

public class CampaignManagerDaemonProcess extends AbstractDaemonWorkerController
{
   
   public static String NAME = "Campaign Manager";
   protected CampaignManager manager = null;
   
   public CampaignManagerDaemonProcess(DaemonFactory daemonFactory)
   {
      super(daemonFactory);
   }
   
   protected void initWorker()
   {
	   manager = new CampaignManager(InitServlet.getRealPath());
       worker = manager;
   }

   public String getName()
   {
      return (NAME);
   }
   
   public static void main(String[] args)
   {

   }
}
