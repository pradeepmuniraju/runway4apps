package com.sok.runway.offline.runwayHealthManager;

import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.email.EmailerMap;
import com.sok.runway.offline.AbstractDaemonWorker;
import com.sok.runway.offline.ThreadManager;
import com.sok.runway.offline.halManager.HouseAndLandManager;
import com.sok.runway.view.crm.ProductView;
import com.sok.runway5.properties.dao.*;
import com.sok.runway5.properties.entity.PropertyBuilding;
import com.sok.runway5.properties.entity.PropertyEntity;
import com.sok.runway5.properties.entity.PropertyEstate;
import com.sok.service.crm.cms.properties.DripService;
import com.sok.service.crm.cms.properties.DripService.DripProduct;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

public class RunwayHealthManager extends AbstractDaemonWorker {
	private static final Logger logger = LoggerFactory.getLogger(RunwayHealthManager.class);
	private static final DripService dripService = DripService.getInstance();
	private static final String DEFAULT_VELOCITY_PATH = "/cms/emails/offlinealerts/";

	protected String alerttext = "generic/generic_text.vt";
	protected String alerthtml = "generic/generic.vt";

	private static RunwayHealthManager rpm;

	public static enum type {
		all, house, land, apartments, hal
	};

	protected String emailalertsurlhome = null;

	// private Vector<QueueItem> queue = new Vector<QueueItem>();

	// private String realestateUser = "";
	// private String realestatePassword = "";

	SimpleDateFormat yyyymmdd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	SimpleDateFormat runyyyymmdd = new SimpleDateFormat("yyyy-MM-dd");

	private PropertyBuilderList builderList = null;
	private PropertyDesignList designList = null;
	private PropertyDeveloperList developerList = null;
	private PropertyEstateList estateList = null;
	private PropertyEstateList estateCustomList = null;
	// private PropertyLandList landList = null;
	// private PropertyPlanList planList = null;
	private PropertyRangeList rangeList = null;
	// private PropertyStageList stageList = null;
	private PropertyBuildingDeveloperList buildingDeveloperList = null;
	private PropertyBuildingList buildingList = null;
	// private PropertyApartmentList apartmentList = null;
	// private PropertyBuildingStageList buildingStageList = null;

	private String maxHomeSize = "";
	private String minHomeSize = "";
	private String maxHomeWidth = "";
	private String maxLotWidth = "";
	private String maxLotDepth = "";
	private String maxLotArea = "";
	private String minLotArea = "";
	private String maxApartmentArea = "";
	private String minApartmentArea = "";

	private String maxTotal = "";
	private String maxEPTotal = "";

	private boolean isGettingData = true;
	private boolean hasInit = false;
	private boolean isLoadingIndex = false;
	private boolean isDaily = false;
	private boolean isUpdatingPublishing = false;
	private boolean firstRun = true;
	private boolean isCleanup = false;

	private String dayOfMonth = "";
	private String dayOfWeek = "";
	private String startTime = "00:00";
	private String endTime = "06:00";

	private Connection con = null;

	protected String dbconn;
	protected ServletContext context;
	protected String contextpath;
	protected String contextname = null;
	protected Locale thislocale;

	private ArrayList<String> extraSearch = new ArrayList<String>();

	private HashSet<String> deleted = new HashSet<String>();

	private ExecutorService executor = null;
	private int 	threads = 50;
	private int 	maxThreads = 50 * 5;

	private boolean pretend = false;

	public RunwayHealthManager(String configfilepath) {
		super(configfilepath);

		checkConfig();

		if (rpm == null)
			rpm = this;
	}

	public static RunwayHealthManager getInstance() {
		if (rpm == null)
			new RunwayHealthManager("");
		return rpm;
	}

	/**
	 * The init method is called when the worker is woken up from sleep. Any
	 * actions that need to be performed at that interval should be defined
	 * here.
	 */
	protected void init() {
		urlhome = InitServlet.getSystemParam("URLHome");

		if (executor == null) {
			try {
				threads = Integer.parseInt(InitServlet.getSystemParam("RPMtaskThreads", String.valueOf(threads)));
				maxThreads = threads * 5;
			} catch (NumberFormatException nfe) {
			}

			executor = com.sok.runway.offline.ThreadManager.createThreadExecutor(
					"RPMtaskExecutor", threads); // Executors.newFixedThreadPool(threads);

		}
		logger.info("Initializing RPM Task");
		if (!hasInit) {
			refreshData();
			hasInit = true;
		}
		if (!isLoadingIndex) {
			// make sure we do this first as the products need to be up to date
			if ("true".equals(InitServlet.getSystemParam("PropertyPackageFollowLotStatus")))
				if (!pretend) hourlyLotProcessingFollowMe(null);
				else
					if (!pretend) hourlyLotProcessing(null);

			if (!pretend) hourlyPackageProcessing(null);
			if (!pretend) hourlyCalculatePackageDrips();
			if(!pretend && "true".equals(InitServlet.getSystemParams().getProperty("PipelinePushEnabled")) ) {
				logger.info("RPM-hourly Task, processing pipeline push");
				hourlyPipelinePushProcessing();
				logger.info("RPM-hourly Task, processing pipeline push completed");
			}
			if (firstRun && !debug)
				if (!pretend) refreshIndex("create", "", "");
				else
					if (!pretend) refreshIndex("update", "", "");
			firstRun = false;
		}
		if (!pretend && !isUpdatingPublishing) {
			isUpdatingPublishing = true;
			checkPublishingDates();
			checkPublishingInDates();
			isUpdatingPublishing = false;
		}
		if (!pretend && !isDaily) {
			isDaily = true;
			// daily tasks, these need to be done once a day
			String lastDate = getCurrentDates("RPM-DailyTask");
			if (!"RUN NOW".equals(lastDate)) {
				boolean isValidTime = isValidTime(new Date());
				if (isValidTime) {
					if (lastDate != null && lastDate.length() > 0) {
						try {
							isValidTime = !isValidTime(ddmmyyyy.parse(lastDate));
						} catch (Exception e) {
						}
					}
					logger.info("RPM-DailyTask {} {}", lastDate, isValidTime);
				}
				if (isValidTime) {
					// TODO daily tasks go here
					try {
						nightlyProductSoldProcessing(null);
					} catch (Exception e) {
						System.out.println("nightlyProductSoldProcessing(null);");
						e.printStackTrace();
					}
					try {
						nightlyProductSoldOtherProcessing(null);
					} catch (Exception e) {
						System.out.println("nightlyProductSoldOtherProcessing(null);");
						e.printStackTrace();
					}
					try {
						nightlyEmailProcessing();			
					} catch (Exception e) {
						System.out.println("nightlyEmailProcessing();");
						e.printStackTrace();
					}
					try {
						nightlyDeleteProcessing();
					} catch (Exception e) {
						System.out.println("nightlyDeleteProcessing();");
						e.printStackTrace();
					}
					try {
						expireProducts();
					} catch (Exception e) {
						System.out.println("expireProducts();");
						e.printStackTrace();
					}
					try {
						calculatePackageDrips();
					} catch (Exception e) {
						System.out.println("calculatePackageDrips();");
						e.printStackTrace();
					}
					try {
						calculatePlanDrips();
					} catch (Exception e) {
						System.out.println("calculatePlanDrips();");
						e.printStackTrace();
					}
					try {
						calculateApartmentDrips();
					} catch (Exception e) {
						System.out.println("calculateApartmentDrips();");
						e.printStackTrace();
					}
					try {
						calculateEPDrips();
					} catch (Exception e) {
						System.out.println("calculateEPDrips();");
						e.printStackTrace();
					}
					try {
						nightlyNastyCleanUp();
					} catch (Exception e) {
						System.out.println("nightlyNastyCleanUp();");
						e.printStackTrace();
					}

					insertCurrentDates("RPM-DailyTask");
				}
			} else {
				logger.error("RPM-DailyTask Run Now");

				try {
					nightlyProductSoldProcessing(null);
				} catch (Exception e) {
					System.out.println("nightlyProductSoldProcessing(null);");
					e.printStackTrace();
				}
				try {
					nightlyProductSoldOtherProcessing(null);
				} catch (Exception e) {
					System.out.println("nightlyProductSoldOtherProcessing(null);");
					e.printStackTrace();
				}
				try {
					nightlyEmailProcessing();			
				} catch (Exception e) {
					System.out.println("nightlyEmailProcessing();");
					e.printStackTrace();
				}
				try {
					nightlyDeleteProcessing();
				} catch (Exception e) {
					System.out.println("nightlyDeleteProcessing();");
					e.printStackTrace();
				}
				try {
					expireProducts();
				} catch (Exception e) {
					System.out.println("expireProducts();");
					e.printStackTrace();
				}
				try {
					calculatePackageDrips();
				} catch (Exception e) {
					System.out.println("calculatePackageDrips();");
					e.printStackTrace();
				}
				try {
					calculatePlanDrips();
				} catch (Exception e) {
					System.out.println("calculatePlanDrips();");
					e.printStackTrace();
				}
				try {
					calculateApartmentDrips();
				} catch (Exception e) {
					System.out.println("calculateApartmentDrips();");
					e.printStackTrace();
				}
				try {
					calculateEPDrips();
				} catch (Exception e) {
					System.out.println("calculateEPDrips();");
					e.printStackTrace();
				}

				insertCurrentDates("RPM-DailyTask");
			}
			isDaily = false;
		}

	}

	/** Updates the ProductPipeline with all products
	 * 
	 */
	public void hourlyPipelinePushProcessing() {
		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		row.setParameter("ProductType", "Estate+Stage+Land+Brand+Range+Home+Home Plan+Building+BuildingStage+Apartment");
		row.doAction("search");
		row.getResults(true);
		logger.trace("Start hourlyPipelinePushProcessing {} {}", row.getStatement(), row.getSize());

		while (row.getNext()) {
			upsertProductPipeline(row.getString("ProductID"));
		}
		row.close();
	}

	/**
	 * @param productID
	 */
	private void processProductPipeline(String productID) {
		logger.trace("Processing PipelinePush for {}", productID);
		GenRow quick = getProductQuick(productID);
		String productType = quick.getString("ProductType");

		upsertProductPipeline(productID);

		if ("Home Plan".equalsIgnoreCase(productType)) {			
			upsertProductPipeline(quick.getString("RangeID"));
			upsertProductPipeline(quick.getString("DesignID"));

		} else if ("Land".equalsIgnoreCase(productType) || "Apartment".equalsIgnoreCase(productType)) {
			upsertProductPipeline(quick.getString("EstateID"));
			upsertProductPipeline(quick.getString("StageID"));

		} else if ("House and Land".equalsIgnoreCase(productType)) {
			upsertProductPipeline(quick.getString("RangeID"));
			upsertProductPipeline(quick.getString("DesignID"));
			upsertProductPipeline(quick.getString("EstateID"));
			upsertProductPipeline(quick.getString("StageID"));

		} else if ("Existing Property".equalsIgnoreCase(productType)) {
			upsertProductPipeline(quick.getString("RangeID"));
			upsertProductPipeline(quick.getString("DesignID"));
			upsertProductPipeline(quick.getString("EstateID"));
			upsertProductPipeline(quick.getString("StageID"));
		}
	}

	private void upsertProductPipeline(String productID) {
		logger.trace("UpsertProductPipeline for {}", productID);
		if (StringUtils.isNotBlank(productID)) {
			GenRow row = new GenRow();
			row.setViewSpec("ProductPipelineView");
			row.setConnection(con != null ? con : getConnection());
			row.setParameter("ProductID", productID);
			row.doAction("search");
			row.getResults(true);

			GenRow update = new GenRow();
			update.setViewSpec("ProductPipelineView");
			update.setConnection(con != null ? con : getConnection());
			update.setParameter("ON-ProductID", productID);
			update.setParameter("Publish", canPublishToPipeline(productID));
			update.setParameter("ModifiedDate", "NOW");
			update.setParameter("ModifiedBy", InitServlet.getSystemParams().getProperty("SystemUserID"));

			if (row.getSize() > 0) {
				logger.trace("Update ProductPipeline {}", productID);
				update.doAction(GenerationKeys.UPDATEALL);

			} else {
				logger.trace("Record not found for ProductPipeline, inserting " + productID);
				update.setParameter("ProductID", productID);

				update.createNewID();
				update.setParameter("CreatedDate", "NOW");
				update.setParameter("CreatedBy", InitServlet.getSystemParams().getProperty("SystemUserID"));
				update.doAction(GenerationKeys.INSERT);
			}

			row.close();
		}
	}


	/**1 - Publish 
	 * 0 - Don't Publish
	 * @param productID
	 * @return
	 */
	private int canPublishToPipeline(String productID) {
		GenRow row = new GenRow();
		if (StringUtils.isNotBlank(productID)) {
			row.setViewSpec("ProductView");
			row.setConnection(con != null ? con : getConnection());
			row.setColumn("ProductID", productID);
			row.doAction("selectfirst");

		}
		return "Y".equals(row.getString("Active")) ? 1 : 0;
	}

	private GenRow getProductQuick(String productID) {
		GenRow row = new GenRow();
		if (StringUtils.isNotBlank(productID)) {
			row.setViewSpec("properties/ProductQuickIndexView");
			row.setConnection(con != null ? con : getConnection());
			row.setColumn("ProductID", productID);
			row.doAction("selectfirst");
		}
		return row;
	}

	protected void finalize() {
		if (executor != null) {
			com.sok.runway.offline.ThreadManager.shutdownExecutor(executor);
		}
	}

	protected void expireProducts() {
		String available = InitServlet.getSystemParams().getProperty(
				"ProductAvailableStatus");
		String expired = InitServlet.getSystemParams().getProperty(
				"ProductExpiredStatus");

		if (available == null || available.length() == 0 || expired == null
				|| expired.length() == 0)
			return;

		logger.info("Start RPM package product Expire");

		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		row.setParameter("ExpiryDate", "<NOW;!NULL");
		row.setParameter("Active", "Y");
		row.setParameter("ProductType", "Home Plan+Land+House and Land");
		row.setParameter("CurrentStatusID", available);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID",
				available);
		int c = 0;
		try {
			row.setConnection(getConnection());
			row.doAction("search");
			row.getResults();

			GenRow update = new GenRow();
			update.setTableSpec("Products");
			update.setConnection(getConnection());

			while (row.getNext()) {
				update.clear();
				update.setParameter("ProductID", row.getString("ProductID"));
				update.setParameter("Active", "N");
				update.setParameter(
						"ProductStatusID",
						insertProductStatus(row.getString("ProductID"), expired));
				update.doAction("update");

				expireProcesses(row.getString("ProductID"),
						"The product has expired, this Process is now Locked",
						false);
				expireOrder(row.getString("ProductID"), false);

				doIndexRefreshIndex(row.getString("ProductID"),
						row.getData("ProductType"));

				if ("Land".equals(row.getData("ProductType"))) {
					expireLandProducts(row.getString("ProductID"));
				}

				logger.trace("Expired Package {} {}", row.getString("ProductID"), row.getString("Name"));


				++c;
			}
		} finally {
			row.close();
		}
		logger.info("End RPM product Expire with " + c + " Products Update");
	}

	protected void expireLandProducts(String productID) {
		String available = InitServlet.getSystemParams().getProperty(
				"ProductAvailableStatus");
		String expired = InitServlet.getSystemParams().getProperty(
				"ProductExpiredStatus");

		if (available == null || available.length() == 0 || expired == null
				|| expired.length() == 0)
			return;

		logger.info("Start RPM land product Expire");

		GenRow row = new GenRow();
		row.setViewSpec("ProductProductView");
		row.setParameter("LinkedProductID", productID);
		row.setParameter("Active", "Y");
		row.setParameter("ProductType", "House and Land");
		row.setParameter("CurrentStatusID", available);
		row.setParameter("ProductProductsProduct.Active", "Y");
		row.setParameter("ProductProductsProduct.ProductType", "House and Land");
		row.setParameter(
				"ProductProductsProduct.ProductCurrentStatus.ProductStatusList.StatusID",
				available);
		int c = 0;
		try {
			row.setConnection(getConnection());
			row.doAction("search");
			row.getResults();

			GenRow update = new GenRow();
			update.setTableSpec("Products");
			update.setConnection(getConnection());

			while (row.getNext()) {
				update.clear();
				update.setParameter("ProductID", row.getString("ProductID"));
				update.setParameter("Active", "N");
				update.setParameter(
						"ProductStatusID",
						insertProductStatus(row.getString("ProductID"), expired));
				update.doAction("update");

				expireProcesses(row.getString("ProductID"),
						"Product Expired, Opportunity Locked", false);
				expireOrder(row.getString("ProductID"), false);

				doIndexRefreshIndex(row.getString("ProductID"),
						"House and Land");
				++c;

				logger.trace("Expired Land {} {}", row.getString("ProductID"), row.getString("Name"));
			}
		} finally {
			row.close();
		}
		logger.info("End RPM land product Expire with " + c
				+ " Products Update");
	}

	protected void expireProcesses(String productID, String msg, boolean active) {
		GenRow opp = new GenRow();
		opp.setViewSpec("OpportunityView");
		opp.setParameter("ReferenceProductID|1", productID);
		// opp.setParameter("OpportunityLinkedProducts.ProductID|1", productID);
		opp.setParameter("Inactive", "!Y+NULL");
		try {
			opp.setConnection(getConnection());
			opp.doAction("search");
			opp.getResults();

			GenRow update = new GenRow();
			update.setTableSpec("Opportunities");
			update.setConnection(getConnection());

			GenRow oppLog = new GenRow();
			oppLog.setViewSpec("OpportunityLogView");
			oppLog.setConnection(getConnection());

			while (opp.getNext()) {
				update.clear();
				update.setParameter("OpportunityID",
						opp.getString("OpportunityID"));
				update.setParameter("Inactive", active ? "N" : "L");
				update.doAction("update");

				oppLog.createNewID();
				oppLog.setParameter("OpportunityID",
						opp.getString("OpportunityID"));
				oppLog.setParameter("Type", "Note");
				oppLog.setParameter("Details", msg);
				oppLog.setColumn("CreatedDate", "NOW");
				oppLog.setColumn("CreatedBy", InitServlet.getSystemParams()
						.getProperty("SystemUserID"));

				oppLog.doAction(ActionBean.INSERT);
			}
		} finally {
			opp.close();
		}
	}

	protected void expireOrder(String productID, boolean active) {
		GenRow order = new GenRow();
		order.setViewSpec("OrderView");
		order.setParameter("ProductID|1", productID);
		order.setParameter("OrderOrderItem.ProductID|1", productID);
		// order.setParameter("Active", !active? "Y" : "N");
		try {
			order.setConnection(getConnection());
			order.doAction("search");
			order.getResults();

			GenRow update = new GenRow();
			update.setTableSpec("Orders");
			update.setConnection(getConnection());

			while (order.getNext()) {
				update.clear();
				update.setParameter("OrderID", order.getString("OrderID"));
				update.setParameter("Active", active ? "Y" : "N");
				update.doAction("update");
			}
		} finally {
			order.close();
		}
	}

	@SuppressWarnings("resource")
	protected void calculatePackageDrips() {
		String available = InitServlet.getSystemParams().getProperty(
				"ProductAvailableStatus");

		if (available == null || available.length() == 0)
			return;

		Date d = new Date();

		logger.error("Start RPM Package DRIP calculations");

		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		// Active param disabled on request 14 Aug 2012
		// row.setParameter("Active","Y");
		row.setParameter("ProductType", "House and Land");
		row.setParameter("CurrentStatusID", available);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID",
				available);
		List<String[]> products = Collections.emptyList();
		try {
			row.setConnection(getConnection());
			row.sortBy("ProductID", 0);
			row.doAction("search");
			row.getResults();

			products = new ArrayList<String[]>();
			while (row.getNext()) {
				products.add(new String[] { row.getString("ProductID"),
						row.getString("TotalCost"), row.getString("DripResult") });
			}
		} finally {
			row.close();
		}
		GenRow update = new GenRow();
		update.setTableSpec("Products");
		update.setConnection(getConnection());

		int c = 0;
		long l = 0;
		for (String[] product : products) {
			try {
				Date s = new Date();
				if (calculatePackageDrips(update, product))	c++;
				l = ((new Date()).getTime() - s.getTime());
				logger.debug("Package {} took {}ms", new String[] { product[0],	"" + l });
			} catch (Exception e) {
				logger.error("Error RPM Package DRIP calculations " + e.getMessage());
			}
		}

		logger.error("End RPM Package DRIP calculations " + c
				+ " Products Update in "
				+ ((double) ((new Date()).getTime() - d.getTime()) / 1000));
	}

	@SuppressWarnings("resource")
	protected void hourlyCalculatePackageDrips() {
		String available = InitServlet.getSystemParams().getProperty(
				"ProductAvailableStatus");

		if (available == null || available.length() == 0)
			return;

		Date d = new Date();

		Date last1 = new Date();
		last1.setTime(last1.getTime() - (2 * 60 * 60 * 1000));

		logger.error("Start RPM Package DRIP calculations");

		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		// Active param disabled on request 14 Aug 2012
		// row.setParameter("Active","Y");
		row.setParameter("ProductType", "House and Land");
		row.setParameter("CurrentStatusID", available);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID",
				available);
		row.setParameter("ModifiedDate", ">" + ddmmyyyy.format(last1));

		List<String[]> products = Collections.emptyList();
		try {
			row.setConnection(getConnection());
			row.sortBy("ProductID", 0);
			row.doAction("search");
			row.getResults();

			products = new ArrayList<String[]>();
			while (row.getNext()) {
				products.add(new String[] { row.getString("ProductID"),
						row.getString("TotalCost"), row.getString("DripResult") });
			}
		} finally {
			row.close();
		}
		GenRow update = new GenRow();
		update.setTableSpec("Products");
		update.setConnection(getConnection());

		int c = 0;
		long l = 0;
		for (String[] product : products) {
			try {
				Date s = new Date();
				if (calculatePackageDrips(update, product))	c++;
				l = ((new Date()).getTime() - s.getTime());
				logger.debug("Package {} took {}ms", new String[] { product[0],	"" + l });
			} catch (Exception e) {
				logger.error("Error RPM Package DRIP calculations " + e.getMessage());
			}
		}

		logger.error("End RPM Package DRIP calculations " + c
				+ " Products Update in "
				+ ((double) ((new Date()).getTime() - d.getTime()) / 1000));
	}

	public void calculateSinglePackageDrips(Connection con, String productID) {

		GenRow update = new GenRow();
		update.setTableSpec("Products");
		update.setConnection(con != null ? con : getConnection());

		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		row.setConnection(con != null ? con : getConnection());
		row.setParameter("ProductID", productID);

		if (row.isSet("ProductID"))
			row.doAction("select");
		if (row.isSuccessful())
			calculatePackageDrips(
					update,
					new String[] { row.getString("ProductID"),
							row.getString("TotalCost"),
							row.getString("DripResult") });
		else
			logger.warn("Product not found for package calculation "
					+ productID);
	}

	public boolean calculatePackageDrips(GenRow update, String[] product) {
		try {
			if (update == null) {
				update = new GenRow();
				update.setTableSpec("Products");
			}
			DripService.DripCollection collection = dripService.getAllDrips(
					getConnection(), null, new String[] { product[0],
						product[0], product[0] }, new Date());
			double totalCost = 0;
			try {
				totalCost = Double.parseDouble(product[1]);
			} catch (Exception ne) {
				logger.error(
						"Package {} Drip Price cound not be updated with Error {}, \nNumber {}",
						new String[] { product[0], ne.getMessage(),
								product[1] });
				return false;
			}
			String prevDripResult = product[2];
			update.clear();
			update.setConnection(getConnection());
			update.setParameter("ProductID", product[0]);
			update.setParameter("DripCost",	String.valueOf(collection.includedDripAmount));

			// This shouldn't need a try/catch, but i don't want the chance of
			// any errors.
			try {
				double cdc = 0;
				for (DripProduct dp : collection.allDrips) {
					if ("Included".equals(dp.Type)
							&& "Campaign".equals(dp.Summary)
							&& "Y".equals(dp.Active)) {
						cdc += dp.TotalCost;
					}
				}
				update.setParameter("CampaignDripCost", String.valueOf(cdc));
			} catch (Exception e) {
				logger.error("Error calculating campaign drip cost", e);
			}

			update.setParameter("DripResult",
					String.valueOf(totalCost + collection.includedDripAmount));
			if (update.getString("DripCost").length() == 0) {
				update.setParameter("DripCost", "0");
				update.setParameter("DripResult", totalCost);
			} else if (update.getString("DripResult").length() == 0) {
				update.setParameter("DripResult", totalCost);
			}
			if (update.getString("DripResult").endsWith(".0")) update.setParameter("DripResult", update.getString("DripResult").replaceFirst("\\.0", ""));
			if (!prevDripResult.equals(update.getString("DripResult"))) {
				// if the value of the drips have changed then update the
				// modified date and then reindex
				update.setParameter("ModifiedDate", "NOW");
				update.setParameter("DripModifiedDate", "NOW");
				update.doAction("update");
				doIndexRefreshIndex(product[0], "House and Land");
			} else {
				update.setParameter("DripModifiedDate", "NOW");
				update.doAction("update");
			}
			return true;
		} catch (Exception e) {
			logger.error(
					"Package {} Drip Price cound not be updated with Error {}, Params {}, Stmt {}",
					new String[] { product[0], e.getMessage(),
							update.toString(), update.getStatement() });
			return false;
		}
	}

	protected void calculatePlanDrips() {
		String available = InitServlet.getSystemParams().getProperty(
				"ProductAvailableStatus");

		if (available == null || available.length() == 0)
			return;

		Date d = new Date();

		logger.info("Start RPM Plan DRIP calculations");

		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductPlanView");

		// Active param disabled on request 14 Aug 2012
		row.setParameter("Category", "!PlanSale+NULL");
		row.setParameter("ProductType", "Home Plan");
		row.setParameter("CurrentStatusID", available);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID",
				available);

		List<String[]> products = Collections.emptyList();
		try {
			row.setConnection(getConnection());
			row.sortBy("ProductID", 0);
			row.doAction("search");
			row.getResults();
			products = new ArrayList<String[]>();
			while (row.getNext()) {
				products.add(new String[] { row.getString("ProductID"),
						row.getString("TotalCost"),
						row.getString("DripResult"),
						row.getString("HomeProductID"),
						row.getString("RangeProductID") });
			}
		} finally {
			row.close();
		}

		GenRow update = new GenRow();
		update.setTableSpec("Products");
		update.setConnection(getConnection());

		DripService ds = DripService.getInstance();

		int c = 0;
		for (String[] product : products) {

			try {
				DripService.DripCollection collection = ds.getAllDrips(
						getConnection(), null, new String[] { product[4],
							product[3], product[0] }, new Date());

				double totalCost = 0;
				try {
					totalCost = Double.parseDouble(product[1]);
				} catch (Exception ne) {
					logger.error(
							"Plan {} Drip Price cound not be updated with Error {}, \nNumber {}",
							new String[] { product[0], ne.getMessage(),
									product[1] });
					continue;
				}

				String prevDripResult = product[2];

				update.clear();
				update.setParameter("ProductID", product[0]);
				update.setParameter("DripCost",
						String.valueOf(collection.includedDripAmount));
				update.setParameter(
						"DripResult",
						String.valueOf(totalCost
								+ collection.includedDripAmount));
				if (update.getString("DripCost").length() == 0) {
					update.setParameter("DripCost", "0");
					update.setParameter("DripResult",
							row.getDouble("TotalCost"));
				} else if (update.getString("DripResult").length() == 0) {
					update.setParameter("DripResult",
							row.getDouble("TotalCost"));
				}
				if (!prevDripResult.equals(update.getString("DripResult"))) {
					// if the value of the drips have changed then update the
					// modified date and then reindex
					update.setParameter("ModifiedDate", "NOW");
					update.setParameter("DripModifiedDate", "NOW");
					update.doAction("update");
					doIndexRefreshIndex(product[0], "Home Plan");
				} else {
					update.setParameter("DripModifiedDate", "NOW");
					update.doAction("update");
				}

				++c;
			} catch (Exception e) {
				logger.error(
						"Package {} Drip Price cound not be updated with Error {}",
						product[0], e.getMessage());
			}
		}

		logger.error("End RPM Plan DRIP calculations " + c
				+ " Products Update in "
				+ ((double) ((new Date()).getTime() - d.getTime()) / 1000));
	}

	protected void calculateApartmentDrips() {
		String available = InitServlet.getSystemParams().getProperty(
				"ProductAvailableStatus");

		if (available == null || available.length() == 0)
			return;

		Date d = new Date();

		logger.info("Start RPM Apartment DRIP calculations");

		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductApartmentView");

		// Active param disabled on request 14 Aug 2012
		row.setParameter("ProductType", "Apartment");
		row.setParameter("CurrentStatusID", available);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID",	available);

		List<String[]> products = Collections.emptyList();
		try {
			row.setConnection(getConnection());
			row.sortBy("ProductID", 0);
			row.doAction("search");
			row.getResults();
			products = new ArrayList<String[]>();
			while (row.getNext()) {
				products.add(new String[] { row.getString("ProductID"),
						row.getString("TotalCost"),
						row.getString("DripResult"),
						row.getString("BuildingStageProductID"),
						row.getString("BuildingProductID") });
			}
		} finally {
			row.close();
		}

		GenRow update = new GenRow();
		update.setTableSpec("Products");
		update.setConnection(getConnection());

		DripService ds = DripService.getInstance();

		int c = 0;
		for (String[] product : products) {

			try {
				DripService.DripCollection collection = ds.getAllDrips(
						getConnection(), null, new String[] { product[4],
							product[3], product[0] }, new Date());

				double totalCost = 0;
				try {
					totalCost = Double.parseDouble(product[1]);
				} catch (Exception ne) {
					continue;
				}

				String prevDripResult = product[2];

				update.clear();
				update.setParameter("ProductID", product[0]);
				update.setParameter("DripCost",
						String.valueOf(collection.includedDripAmount));
				update.setParameter(
						"DripResult",
						String.valueOf(totalCost
								+ collection.includedDripAmount));
				if (update.getString("DripCost").length() == 0) {
					update.setParameter("DripCost", "0");
					update.setParameter("DripResult",
							row.getDouble("TotalCost"));
				} else if (update.getString("DripResult").length() == 0) {
					update.setParameter("DripResult",
							row.getDouble("TotalCost"));
				}
				if (!prevDripResult.equals(update.getString("DripResult"))) {
					// if the value of the drips have changed then update the
					// modified date and then reindex
					update.setParameter("ModifiedDate", "NOW");
					update.setParameter("DripModifiedDate", "NOW");
					update.doAction("update");
					doIndexRefreshIndex(product[0], "Apartment");
				} else {
					update.setParameter("DripModifiedDate", "NOW");
					update.doAction("update");
				}

				++c;
			} catch (Exception e) {
				logger.error(
						"Package {} Drip Price cound not be updated with Error {}",
						product[0], e.getMessage());
			}
		}

		logger.error("End RPM Apartment DRIP calculations " + c
				+ " Products Update in "
				+ ((double) ((new Date()).getTime() - d.getTime()) / 1000));
	}

	public void calculateSinglePlanDrips(Connection con, String productID) {

		GenRow update = new GenRow();
		update.setTableSpec("Products");
		update.setConnection(con != null ? con : getConnection());

		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		row.setConnection(con != null ? con : getConnection());
		row.setParameter("ProductID", productID);

		if (row.isSet("ProductID"))
			row.doAction("select");
		if (row.isSuccessful())
			calculateSinglePlanDrips(
					update,
					new String[] { row.getString("ProductID"),
							row.getString("TotalCost"),
							row.getString("DripResult"),
							row.getString("HomeProductID"),
							row.getString("RangeProductID") });
		else
			logger.warn("Product not found for plan calculation "
					+ productID);

	}

	public boolean calculateSinglePlanDrips(GenRow update, String[] product) {
		try {
			if (update == null) {
				update = new GenRow();
				update.setTableSpec("Products");
			}
			DripService ds = DripService.getInstance();
			DripService.DripCollection collection = ds.getAllDrips(
					getConnection(), null, new String[] { product[4],
						product[3], product[0] }, new Date());

			double totalCost = 0;
			try {
				totalCost = Double.parseDouble(product[1]);
			} catch (Exception ne) {
				return false;
			}

			String prevDripResult = product[2];

			update.clear();
			update.setParameter("ProductID", product[0]);
			update.setParameter("DripCost",
					String.valueOf(collection.includedDripAmount));
			update.setParameter("DripResult",String.valueOf(totalCost + collection.includedDripAmount));
			if (update.getString("DripCost").length() == 0) {
				update.setParameter("DripCost", "0");
				update.setParameter("DripResult",product[1]);
			} else if (update.getString("DripResult").length() == 0) {
				update.setParameter("DripResult", product[1]);
			}
			if (!prevDripResult.equals(update.getString("DripResult"))) {
				// if the value of the drips have changed then update the
				// modified date and then reindex
				update.setParameter("ModifiedDate", "NOW");
				update.setParameter("DripModifiedDate", "NOW");
				update.doAction("update");
				doIndexRefreshIndex(product[0], "Home Plan");
			} else {
				update.setParameter("DripModifiedDate", "NOW");
				update.doAction("update");
			}

			return true;
		} catch (Exception e) {
			logger.error(
					"Plan {} Drip Price cound not be updated with Error {}, Params {}, Stmt {}",
					new String[] { product[0], e.getMessage(),
							update.toString(), update.getStatement() });
			return false;
		}
	}

	@SuppressWarnings("resource")
	protected void calculateEPDrips() {
		String available = InitServlet.getSystemParams().getProperty("ProductAvailableStatus");

		if (available == null || available.length() == 0)
			return;

		Date d = new Date();

		logger.error("Start RPM Package DRIP calculations");

		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		// Active param disabled on request 14 Aug 2012
		// row.setParameter("Active","Y");
		row.setParameter("ProductType", "Existing Property");
		row.setParameter("CurrentStatusID", available);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID", available);
		List<String[]> products = Collections.emptyList();
		try {
			row.setConnection(getConnection());
			row.sortBy("ProductID", 0);
			row.doAction("search");
			row.getResults();

			products = new ArrayList<String[]>();
			while (row.getNext()) {
				products.add(new String[] { row.getString("ProductID"), row.getString("TotalCost"), row.getString("DripResult") });
			}
		} finally {
			row.close();
		}
		GenRow update = new GenRow();
		update.setTableSpec("Products");
		update.setConnection(getConnection());

		int c = 0;
		long l = 0;
		for (String[] product : products) {
			try {
				Date s = new Date();
				if (calculateEPDrips(update, product))
					c++;
				l = ((new Date()).getTime() - s.getTime());
				logger.debug("EP {} took {}ms", new String[] { product[0], "" + l });
			} catch (Exception e) {
				logger.error("Error RPM EP DRIP calculations " + e.getMessage());
			}
		}

		logger.error("End RPM EP DRIP calculations " + c + " Products Update in " + ((double) ((new Date()).getTime() - d.getTime()) / 1000));
	}

	public boolean calculateEPDrips(GenRow update, String[] product) {
		try {
			if (update == null) {
				update = new GenRow();
				update.setTableSpec("Products");
			}
			DripService.DripCollection collection = dripService.getAllDrips(getConnection(), null, new String[] { product[0], product[0], product[0] }, new Date());
			double totalCost = 0;
			try {
				totalCost = Double.parseDouble(product[1]);
			} catch (Exception ne) {
				return false;
			}
			String prevDripResult = product[2];
			update.clear();
			update.setConnection(getConnection());
			update.setParameter("ProductID", product[0]);
			update.setParameter("DripCost", String.valueOf(collection.includedDripAmount));

			// This shouldn't need a try/catch, but i don't want the chance of
			// any errors.
			try {
				double cdc = 0;
				for (DripProduct dp : collection.allDrips) {
					if ("Included".equals(dp.Type) && "Campaign".equals(dp.Summary) && "Y".equals(dp.Active)) {
						cdc += dp.TotalCost;
					}
				}
				update.setParameter("CampaignDripCost", String.valueOf(cdc));
			} catch (Exception e) {
				logger.error("Error calculating campaign drip cost", e);
			}

			update.setParameter("DripResult", String.valueOf(totalCost + collection.includedDripAmount));
			if (update.getString("DripCost").length() == 0) {
				update.setParameter("DripCost", "0");
				update.setParameter("DripResult", totalCost);
			} else if (update.getString("DripResult").length() == 0) {
				update.setParameter("DripResult", totalCost);
			}
			if (!prevDripResult.equals(update.getString("DripResult"))) {
				// if the value of the drips have changed then update the
				// modified date and then reindex
				update.setParameter("ModifiedDate", "NOW");
				update.setParameter("DripModifiedDate", "NOW");
				update.doAction("update");
				doIndexRefreshIndex(product[0], "Existing Property");
			} else {
				update.setParameter("DripModifiedDate", "NOW");
				update.doAction("update");
			}
			return true;
		} catch (Exception e) {
			logger.error("EP {} Drip Price cound not be updated with Error {}, Params {}, Stmt {}", new String[] { product[0], e.getMessage(), update.toString(), update.getStatement() });
			return false;
		}
	}

	/**
	 * This method will update all packages on a lot to a given status. 
	 * 
	 * It will processes all Lots that have been modified in the last 24hrs or the give lot
	 *  
	 * @param productID of a lot that will be process or null for all lots
	 */

	public void hourlyLotProcessing(String productID) {
		Date last24 = new Date();
		last24.setTime(last24.getTime() - (2 * 60 * 60 * 1000));

		String lotsSoldByOther_PackageStatusID = InitServlet.getSystemParam("LotsSoldByOther.PackageStatusID");
		String lotsSoldByOther_PackageActive = InitServlet.getSystemParam("LotsSoldByOther.PackageActive");
		String lotsSoldByOther_LotActive = InitServlet.getSystemParam("LotsSoldByOther.LotActive");
		String lotsSoldByOther_LotStatusID = InitServlet.getSystemParam("LotsSoldByOther.LotStatusID");

		// nothing to do so return
		if (lotsSoldByOther_LotStatusID == null || lotsSoldByOther_LotStatusID.length() == 0) return;

		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		row.setParameter("CurrentStatusID", lotsSoldByOther_LotStatusID);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID", lotsSoldByOther_LotStatusID);
		row.setParameter("ProductType", "Land");
		if (productID != null && productID.length() > 0) {
			row.setParameter("ProductID", productID);
		} else {
			row.setParameter("ModifiedDate|1", ">" + ddmmyyyy.format(last24));
			row.setParameter("ProductCurrentStatus.CreatedDate|1",  ">" + ddmmyyyy.format(last24));
		}
		row.doAction("search");

		StringBuilder lotIDs = new StringBuilder();

		GenRow update = new GenRow();
		update.setTableSpec("Products");

		row.getResults();

		while (row.getNext()) {
			if (lotsSoldByOther_LotActive != null && lotsSoldByOther_LotActive.length() == 1 && !row.getString("Active").equals(lotsSoldByOther_LotActive)) {
				update.clear();
				update.setParameter("ProductID", row.getData("ProductID"));
				update.setParameter("Active", lotsSoldByOther_LotActive);
				update.doAction("update");
				doIndexRefreash("update",  row.getData("ProductID"),  row.getData("ProductType"));

				logger.trace("hourlyLotProcessing Updated Land {} {}", row.getString("ProductID"), row.getString("Name"));
			}
			if (lotIDs.length() > 0) lotIDs.append("+");
			lotIDs.append(row.getData("ProductID"));
		}

		if (lotIDs.length() > 0 && ((lotsSoldByOther_PackageStatusID != null && lotsSoldByOther_PackageStatusID.length() > 0) || (lotsSoldByOther_PackageActive != null && lotsSoldByOther_PackageActive.length() > 0))) {
			row.clear();
			row.setViewSpec("properties/ProductHouseAndLandSimpleView");
			row.setParameter("CurrentStatusID", "!" + lotsSoldByOther_PackageStatusID);
			row.setParameter("QuickIndexCurrentStatus.ProductStatusList.StatusID", "!" + lotsSoldByOther_PackageStatusID);
			row.setParameter("ProductType", "House and Land");
			row.setParameter("LotID", lotIDs.toString());
			row.setParameter("ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductID", lotIDs.toString());
			row.doAction("search");

			row.getResults();

			while (row.getNext()) {
				update.clear();
				update.setParameter("ProductID", row.getData("ProductID"));
				if (lotsSoldByOther_PackageActive != null && lotsSoldByOther_PackageActive.length() > 0) update.setParameter("Active", lotsSoldByOther_PackageActive);
				if (lotsSoldByOther_PackageStatusID != null && lotsSoldByOther_PackageStatusID.length() > 0) update.setParameter("ProductStatusID",insertProductStatus(row.getData("ProductID"), lotsSoldByOther_PackageStatusID));
				if (update.isSet("Active") || update.isSet("ProductStatusID")) {
					update.setParameter("ModifiedDate","NOW");
					update.doAction("update");
					doIndexRefreash("update",  row.getData("ProductID"),  row.getData("ProductType"));

					logger.trace("hourlyLotProcessing Updated Package {} {}", row.getString("ProductID"), row.getString("Name"));
				}
			}

		}

		row.close();
		update.close();
	}

	public void hourlyLotProcessingFollowMe(String productID) {
		Date last24 = new Date();
		last24.setTime(last24.getTime() - (2 * 60 * 60 * 1000));

		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		row.setParameter("ProductType", "Land");
		if (productID != null && productID.length() > 0) {
			row.setParameter("ProductID", productID);
		} else {
			row.setParameter("ModifiedDate|1", ">" + ddmmyyyy.format(last24));
			row.setParameter("ProductCurrentStatus.CreatedDate|1",  ">" + ddmmyyyy.format(last24));
		}
		row.setParameter("-groupby0", "ProductID");
		row.doAction("search");

		row.getResults();

		GenRow update = new GenRow();
		update.setTableSpec("Products");
		update.setConnection(row.getConnection());

		GenRow packages = new GenRow();
		packages.setViewSpec("properties/ProductQuickIndexView");
		update.setConnection(row.getConnection());

		while (row.getNext()) {
			packages.clear();
			packages.setParameter("LotID", row.getString("ProductID"));
			packages.setParameter("QuickIndexCurrentStatus.ProductStatusList.StatusID|1", "!" + row.getString("CurrentStatusID"));
			packages.setParameter("Active|1", "!" + row.getString("Active"));
			packages.setParameter("ProductType", "House and Land");
			packages.setParameter("-groupby0", "ProductID");
			packages.doAction("search");

			packages.getResults();

			while (packages.getNext()) {
				update.clear();
				update.setParameter("ProductID", packages.getData("ProductID"));
				update.setParameter("Active", row.getString("Active"));
				if (!row.getString("CurrentStatusID").equals(packages.getData("CurrentStatsusID"))) update.setParameter("ProductStatusID",insertProductStatus(packages.getData("ProductID"), row.getString("CurrentStatusID")));
				if (update.isSet("Active") || update.isSet("ProductStatusID")) {
					update.setParameter("ModifiedDate","NOW");
					update.doAction("update");
					doIndexRefreash("update",  packages.getData("ProductID"),  packages.getData("ProductType"));

					logger.trace("hourlyLotProcessingFollowMe Updated Package {} {}", row.getString("ProductID"), row.getString("Name"));
				}
			}

		}

		packages.close();
		update.close();
		row.close();
	}

	/**
	 * This method will update all packages on a lot to a given status. 
	 * 
	 * It will processes all Lots that have been modified in the last 2hrs or the give lot
	 *  
	 * @param productID of a lot that will be process or null for all lots
	 */

	public void hourlyPackageProcessing(String productID) {
		Date last24 = new Date();
		last24.setTime(last24.getTime() - (2 * 60 * 60 * 1000));

		String lotsSoldByClient_OtherPackageStatusID = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageStatusID");
		String lotsSoldByClient_SoldPackageLotStatusID = InitServlet.getSystemParam("LotsSoldByClient.SoldPackageLotStatusID");
		String lotsSoldByClient_SoldPackageActive = InitServlet.getSystemParam("LotsSoldByClient.SoldPackageActive");
		String lotsSoldByClient_OtherPackageActive = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageActive");
		String lotsSoldByClient_SoldPackageStatusID = InitServlet.getSystemParam("LotsSoldByClient.SoldPackageStatusID");

		// nothing to do so return
		if (lotsSoldByClient_SoldPackageStatusID == null || lotsSoldByClient_SoldPackageStatusID.length() == 0) return;

		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHouseAndLandSimpleView");
		row.setParameter("CurrentStatusID", lotsSoldByClient_SoldPackageStatusID);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID", lotsSoldByClient_SoldPackageStatusID);
		row.setParameter("ProductType", "House and Land");
		if (productID != null && productID.length() > 0) {
			row.setParameter("ProductID", productID);
		} else {
			row.setParameter("ModifiedDate", ">" + ddmmyyyy.format(last24));
		}
		row.doAction("search");

		StringBuilder lotIDs = new StringBuilder();
		HashMap<String,String> productIDs = new HashMap<String,String>();

		GenRow update = new GenRow();
		update.setTableSpec("Products");

		row.getResults();

		while (row.getNext()) {
			if ((lotsSoldByClient_SoldPackageLotStatusID != null && lotsSoldByClient_SoldPackageLotStatusID.length() > 0) || (lotsSoldByClient_SoldPackageActive != null && lotsSoldByClient_SoldPackageActive.length() > 0)) {
				update.clear();
				update.setParameter("ProductID", row.getData("LotID"));
				if (lotsSoldByClient_SoldPackageLotStatusID != null && lotsSoldByClient_SoldPackageLotStatusID.length() > 0) update.setParameter("ProductStatusID",insertProductStatus(row.getData("LotID"), lotsSoldByClient_SoldPackageLotStatusID));
				if (lotsSoldByClient_SoldPackageActive != null && lotsSoldByClient_SoldPackageActive.length() > 0) update.setParameter("Active", lotsSoldByClient_SoldPackageActive);
				update.doAction("update");
				doIndexRefreash("update",  row.getData("ProductID"), "Land");
				logger.trace("hourlyPackageProcessing Updated Package 1 {} {}", row.getString("ProductID"), row.getString("Name"));
			}
			if (lotIDs.length() > 0) lotIDs.append("+");
			lotIDs.append(row.getData("LotID"));
			productIDs.put(row.getData("LotID"), row.getData("ProductID"));
		}

		if (lotIDs.length() > 0 && ((lotsSoldByClient_OtherPackageStatusID != null && lotsSoldByClient_OtherPackageStatusID.length() > 0) || (lotsSoldByClient_OtherPackageActive != null && lotsSoldByClient_OtherPackageActive.length() > 0))) {
			row.clear();
			row.setViewSpec("properties/ProductHouseAndLandSimpleView");
			row.setParameter("CurrentStatusID", "!" + lotsSoldByClient_OtherPackageStatusID + ";!" + lotsSoldByClient_SoldPackageStatusID);
			row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID", "!" + lotsSoldByClient_OtherPackageStatusID + ";!" + lotsSoldByClient_SoldPackageStatusID);
			row.setParameter("ProductType", "House and Land");
			row.setParameter("LotID", lotIDs.toString());
			row.setParameter("ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductID", lotIDs.toString());
			row.doAction("search");

			row.getResults();

			while (row.getNext()) {
				if (row.getData("LotID") != null && !row.getString("ProductID").equals(productIDs.get(row.getData("LotID")))) {
					update.clear();
					update.setParameter("ProductID", row.getData("ProductID"));
					if (lotsSoldByClient_OtherPackageActive != null && lotsSoldByClient_OtherPackageActive.length() > 0) update.setParameter("Active", lotsSoldByClient_OtherPackageActive);
					if (lotsSoldByClient_OtherPackageStatusID != null && lotsSoldByClient_OtherPackageStatusID.length() > 0) update.setParameter("ProductStatusID",insertProductStatus(row.getData("ProductID"), lotsSoldByClient_OtherPackageStatusID));
					if (update.isSet("Active") || update.isSet("ProductStatusID")) {
						update.setParameter("ModifiedDate","NOW");
						update.doAction("update");
						doIndexRefreash("update",  row.getData("ProductID"),  row.getData("ProductType"));
						expireProcesses(row.getString("ProductID"),"Product Sold in different Process, this Process is Locked",	"Y".equals(lotsSoldByClient_OtherPackageActive));
						expireOrder(row.getString("ProductID"), 	"Y".equals(lotsSoldByClient_OtherPackageActive));
						logger.trace("hourlyPackageProcessing Updated Package 2 {} {}", row.getString("ProductID"), row.getString("Name"));
					}

				}
			}

		}

		row.close();
		update.close();
	}

	public void nightlyProductSoldProcessing(String productID) {
		String lotsSoldByClient_OtherPackageStatusID = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageStatusID");
		String lotsSoldByClient_OtherPackageFinalStatusID = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageFinalStatusID");
		String lotsSoldByClient_OtherPackageDays = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageDays");

		// nothing to do so return
		if (lotsSoldByClient_OtherPackageStatusID == null || lotsSoldByClient_OtherPackageStatusID.length() == 0) return;
		if (lotsSoldByClient_OtherPackageFinalStatusID == null || lotsSoldByClient_OtherPackageFinalStatusID.length() == 0) return;
		if (lotsSoldByClient_OtherPackageDays == null || lotsSoldByClient_OtherPackageDays.length() == 0) return;

		Date last48 = new Date();

		try {
			long offset = (Long) Long.parseLong(lotsSoldByClient_OtherPackageDays) * 24 * 60 * 60 * 1000;
			last48.setTime(last48.getTime() - offset);	
		} catch (Exception e) {
			return;
		}

		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHouseAndLandSimpleView");
		row.setParameter("CurrentStatusID", lotsSoldByClient_OtherPackageStatusID);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID", lotsSoldByClient_OtherPackageStatusID);
		row.setParameter("ProductType", "House and Land");
		if (productID != null && productID.length() > 0) {
			row.setParameter("ProductID", productID);
		} else {
			row.setParameter("ModifiedDate", "<" + ddmmyyyy.format(last48));
		}
		row.doAction("search");

		GenRow update = new GenRow();
		update.setTableSpec("Products");

		row.getResults();

		while (row.getNext()) {
			update.clear();
			update.setParameter("ProductID", row.getData("ProductID"));
			update.setParameter("ProductStatusID",insertProductStatus(row.getData("ProductID"), lotsSoldByClient_OtherPackageFinalStatusID));
			update.doAction("update");
			doIndexRefreash("update",  row.getData("ProductID"), "House and Land");
			logger.trace("nightlyProductSoldProcessing Updated Package {} {}", row.getString("ProductID"), row.getString("Name"));
		}

		row.close();
		update.close();
	}	

	public void nightlyProductSoldOtherProcessing(String productID) {
		String lotsSoldByOther_PackageStatusID = InitServlet.getSystemParam("LotsSoldByOther.PackageStatusID");
		String lotsSoldByOther_PackageFinalStatusID = InitServlet.getSystemParam("LotsSoldByOther.PackageFinalStatusID");
		String lotsSoldByOther_PackageDays = InitServlet.getSystemParam("LotsSoldByOther.PackageDays");

		// nothing to do so return
		if (lotsSoldByOther_PackageStatusID == null || lotsSoldByOther_PackageStatusID.length() == 0) return;
		if (lotsSoldByOther_PackageFinalStatusID == null || lotsSoldByOther_PackageFinalStatusID.length() == 0) return;
		if (lotsSoldByOther_PackageDays == null || lotsSoldByOther_PackageDays.length() == 0) return;

		Date last48 = new Date();

		try {
			long offset = (Long) Long.parseLong(lotsSoldByOther_PackageDays) * 24 * 60 * 60 * 1000;
			last48.setTime(last48.getTime() - offset);	
		} catch (Exception e) {
			return;
		}

		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHouseAndLandSimpleView");
		row.setParameter("CurrentStatusID", lotsSoldByOther_PackageStatusID);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID", lotsSoldByOther_PackageStatusID);
		row.setParameter("ProductType", "House and Land");
		if (productID != null && productID.length() > 0) {
			row.setParameter("ProductID", productID);
		} else {
			row.setParameter("ModifiedDate", "<" + ddmmyyyy.format(last48));
		}
		row.doAction("search");

		GenRow update = new GenRow();
		update.setTableSpec("Products");

		row.getResults();

		while (row.getNext()) {
			update.clear();
			update.setParameter("ProductID", row.getData("ProductID"));
			update.setParameter("ProductStatusID",insertProductStatus(row.getData("ProductID"), lotsSoldByOther_PackageFinalStatusID));
			update.doAction("update");
			doIndexRefreash("update",  row.getData("ProductID"), "House and Land");
			logger.trace("nightlyProductSoldOtherProcessing Updated Package {} {}", row.getString("ProductID"), row.getString("Name"));
		}

		row.close();
		update.close();
	}	

	public void nightlyEmailProcessing() {
		String lotsSoldByOther_PackageFinalStatusID = InitServlet.getSystemParam("LotsSoldByOther.PackageFinalStatusID");
		String lotsSoldByClient_OtherPackageFinalStatusID = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageFinalStatusID");

		if (lotsSoldByOther_PackageFinalStatusID != null && lotsSoldByClient_OtherPackageFinalStatusID != null && lotsSoldByOther_PackageFinalStatusID.equals(lotsSoldByClient_OtherPackageFinalStatusID)) {
			nightlyEmailSoldOtherProcessing(true);
		} else {
			nightlyEmailSoldOtherProcessing(false);
			nightlyEmailSoldProcessing();
		}
	}

	public void nightlyEmailSoldOtherProcessing(boolean mode) {
		logger.trace("nightlyEmailSoldOtherProcessing Stated");

		String lotsSoldByOther_PackageStatusID = InitServlet.getSystemParam("LotsSoldByOther.PackageStatusID");
		String lotsSoldByOther_PackageFinalStatusID = InitServlet.getSystemParam("LotsSoldByOther.PackageFinalStatusID");
		String lotsSoldByOther_EmailDays = InitServlet.getSystemParam("LotsSoldByOther.EmailDays");
		String lotsSoldByOther_Email = InitServlet.getSystemParam("LotsSoldByOther.Email");
		String lotsSoldByOther_PackageDeleteDay = InitServlet.getSystemParam("LotsSoldByOther.PackageDeleteDay");

		// nothing to do so return
		if (lotsSoldByOther_PackageFinalStatusID == null || lotsSoldByOther_PackageFinalStatusID.length() == 0) return;
		if (lotsSoldByOther_EmailDays == null || lotsSoldByOther_EmailDays.length() == 0) return;
		if (lotsSoldByOther_Email == null || lotsSoldByOther_Email.length() == 0) return;
		if (lotsSoldByOther_PackageDeleteDay == null || lotsSoldByOther_PackageDeleteDay.length() == 0) return;

		Calendar cal = Calendar.getInstance();
		try {
			int day = 0;
			day = Integer.parseInt(lotsSoldByOther_PackageDeleteDay);
			if (cal.getMaximum(Calendar.DAY_OF_MONTH) < day) day = cal.getMaximum(Calendar.DAY_OF_MONTH);
			if (cal.get(Calendar.DAY_OF_MONTH) > day) {
				cal.add(Calendar.MONTH,1);
				day = Integer.parseInt(lotsSoldByOther_PackageDeleteDay);
				if (cal.getMaximum(Calendar.DAY_OF_MONTH) < day) day = cal.getMaximum(Calendar.DAY_OF_MONTH);
			}
			cal.set(Calendar.DAY_OF_MONTH, day);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		Date runDay = new Date(cal.getTimeInMillis());

		Date last48 = new Date();

		try {
			long offset = (Long) Long.parseLong(lotsSoldByOther_EmailDays) * 24 * 60 * 60 * 1000;
			runDay.setTime(runDay.getTime() - offset);	
			last48 = runyyyymmdd.parse(runyyyymmdd.format(runDay));	
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (!runyyyymmdd.format(new Date()).equals(runyyyymmdd.format(runDay))) return;

		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();

		if (!mode) {
			sb.append("<br>\n<br>\nThe following is a list of packages that were at ")
			.append(getStatus(lotsSoldByOther_PackageStatusID))
			.append(" and will be deleted in ")
			.append(lotsSoldByOther_EmailDays)
			.append(" days on ")
			.append(cal.getTime())
			.append(", you may click on any package to edit it before then if you wish to retain the package.<br>\n<br>\n<br>\n");
		} else {
			sb.append("<br>\n<br>\nThe following is a list of packages that will be deleted in ")
			.append(lotsSoldByOther_EmailDays)
			.append(" days on ")
			.append(cal.getTime())
			.append(", you may click on any package to edit it before then if you wish to retain the package.<br>\n<br>\n<br>\n");
		}

		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHouseAndLandSimpleView");
		row.setParameter("CurrentStatusID", lotsSoldByOther_PackageFinalStatusID);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID", lotsSoldByOther_PackageFinalStatusID);
		row.setParameter("ProductType", "House and Land");
		row.setParameter("ModifiedDate", "<" + ddmmyyyy.format(last48));
		row.setParameter("ProductCurrentStatus.CreatedDate", "<" + ddmmyyyy.format(last48));
		row.doAction("search");

		row.getResults();

		logger.trace("nightlyEmailSoldOtherProcessing Statement {}", row.getStatement());

		while (row.getNext()) {
			logger.trace("nightlyEmailSoldOtherProcessing Added Package to email {} {}", row.getString("ProductID"), row.getString("Name"));
			sb2.append("<a href=\"")
			.append(urlhome)
			.append("/crm/cms/properties/houseandlandview.jsp?fromSearch=true&ProductID=")
			.append(row.getString("ProductID"))
			.append("\">")
			.append(row.getString("Name"))
			.append("</a><br>\n");
		}

		row.close();

		if (sb2.length() > 0) {
			sb.append(sb2);

			sendEmail(lotsSoldByOther_Email, sb.toString());
		}

	}

	public void nightlyEmailSoldProcessing() {
		logger.trace("nightlyEmailSoldProcessing Stated");
		String lotsSoldByClient_OtherPackageStatusID = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageStatusID");
		String lotsSoldByClient_OtherPackageFinalStatusID = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageFinalStatusID");
		String lotsSoldByClient_OtherPackageEmailDays = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageEmailDays");
		String lotsSoldByClient_OtherPackageEmail = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageEmail");
		String lotsSoldByClient_OtherPackageDeleteDay = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageDeleteDay");

		// nothing to do so return
		if (lotsSoldByClient_OtherPackageFinalStatusID == null || lotsSoldByClient_OtherPackageFinalStatusID.length() == 0) return;
		if (lotsSoldByClient_OtherPackageEmailDays == null || lotsSoldByClient_OtherPackageEmailDays.length() == 0) return;
		if (lotsSoldByClient_OtherPackageEmail == null || lotsSoldByClient_OtherPackageEmail.length() == 0) return;
		if (lotsSoldByClient_OtherPackageDeleteDay == null || lotsSoldByClient_OtherPackageDeleteDay.length() == 0) return;

		Calendar cal = Calendar.getInstance();
		try {
			int day = 0;
			day = Integer.parseInt(lotsSoldByClient_OtherPackageDeleteDay);
			if (cal.getMaximum(Calendar.DAY_OF_MONTH) < day) day = cal.getMaximum(Calendar.DAY_OF_MONTH);
			if (cal.get(Calendar.DAY_OF_MONTH) > day) {
				cal.add(Calendar.MONTH,1);
				day = Integer.parseInt(lotsSoldByClient_OtherPackageDeleteDay);
				if (cal.getMaximum(Calendar.DAY_OF_MONTH) < day) day = cal.getMaximum(Calendar.DAY_OF_MONTH);
			}
			cal.set(Calendar.DAY_OF_MONTH, day);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		Date runDay = new Date(cal.getTimeInMillis());

		Date last48 = new Date();

		try {
			long offset = (Long) Long.parseLong(lotsSoldByClient_OtherPackageEmailDays) * 24 * 60 * 60 * 1000;
			runDay.setTime(runDay.getTime() - offset);	
			last48 = runyyyymmdd.parse(runyyyymmdd.format(runDay));	
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		if (!runyyyymmdd.format(new Date()).equals(runyyyymmdd.format(runDay))) return;

		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();

		sb.append("The following is a list of packages that were at ")
		.append(getStatus(lotsSoldByClient_OtherPackageStatusID))
		.append(" and will be deleted in ")
		.append(lotsSoldByClient_OtherPackageEmailDays)
		.append(" days on ")
		.append(cal.getTime())
		.append(", you may click on any package to edit it before then if you wish to retain the package.<br>\n<br>\n<br>\n");

		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHouseAndLandSimpleView");
		row.setParameter("CurrentStatusID", lotsSoldByClient_OtherPackageFinalStatusID);
		row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID", lotsSoldByClient_OtherPackageFinalStatusID);
		row.setParameter("ProductType", "House and Land");
		row.setParameter("ModifiedDate", "<" + ddmmyyyy.format(last48));
		row.setParameter("ProductCurrentStatus.CreatedDate", "<" + ddmmyyyy.format(last48));
		row.doAction("search");

		row.getResults();

		logger.trace("nightlyEmailSoldProcessing Statement {}", row.getStatement());

		while (row.getNext()) {
			logger.trace("nightlyEmailSoldProcessing Added Package to email {} {}", row.getString("ProductID"), row.getString("Name"));
			sb2.append("<a href=\"")
			.append(urlhome)
			.append("/crm/cms/properties/houseandlandview.jsp?fromSearch=true&ProductID=")
			.append(row.getString("ProductID"))
			.append("\">")
			.append(row.getString("Name"))
			.append("</a><br>\n");
		}

		row.close();

		if (sb2.length() > 0) {
			sb.append(sb2);

			sendEmail(lotsSoldByClient_OtherPackageEmail, sb.toString());
		}

	}

	public void nightlyDeleteProcessing() {
		String lotsSoldByOther_PackageFinalStatusID = InitServlet.getSystemParam("LotsSoldByOther.PackageFinalStatusID");
		String lotsSoldByClient_OtherPackageFinalStatusID = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageFinalStatusID");

		if (lotsSoldByOther_PackageFinalStatusID != null && lotsSoldByClient_OtherPackageFinalStatusID != null && lotsSoldByOther_PackageFinalStatusID.equals(lotsSoldByClient_OtherPackageFinalStatusID)) {
			nightlyDeleteSoldOtherProcessing();
		} else {
			nightlyDeleteSoldOtherProcessing();
			nightlyDeleteSoldProcessing();
		}
	}

	public void nightlyDeleteSoldOtherProcessing() {
		final String lotsSoldByOther_PackageFinalStatusID = InitServlet.getSystemParam("LotsSoldByOther.PackageFinalStatusID");
		final String lotsSoldByOther_EmailDays = InitServlet.getSystemParam("LotsSoldByOther.EmailDays");
		final String lotsSoldByOther_PackageDeleteDay = InitServlet.getSystemParam("LotsSoldByOther.PackageDeleteDay");

		// nothing to do so return
		if (lotsSoldByOther_PackageFinalStatusID == null || lotsSoldByOther_PackageFinalStatusID.length() == 0) return;
		if (lotsSoldByOther_EmailDays == null || lotsSoldByOther_EmailDays.length() == 0) return;
		if (lotsSoldByOther_PackageDeleteDay == null || lotsSoldByOther_PackageDeleteDay.length() == 0) return;

		Calendar cal = Calendar.getInstance();
		try {
			int day = 0;
			day = Integer.parseInt(lotsSoldByOther_PackageDeleteDay);
			if (cal.getMaximum(Calendar.DAY_OF_MONTH) < day) day = cal.getMaximum(Calendar.DAY_OF_MONTH);
			if (cal.get(Calendar.DAY_OF_MONTH) > day) {
				cal.add(Calendar.MONTH,1);
				day = Integer.parseInt(lotsSoldByOther_PackageDeleteDay);
				if (cal.getMaximum(Calendar.DAY_OF_MONTH) < day) day = cal.getMaximum(Calendar.DAY_OF_MONTH);
			}
			cal.set(Calendar.DAY_OF_MONTH, day);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
		} catch (Exception e) {
			return;
		}

		final Date runDay = new Date(cal.getTimeInMillis());

		final Date last48 = new Date();

		if (last48.before(runDay)) return;

		try {
			long offset = (Long) Long.parseLong(lotsSoldByOther_EmailDays) * 24 * 60 * 60 * 1000;
			runDay.setTime(runDay.getTime() - offset);	
			last48.setTime(runyyyymmdd.parse(runyyyymmdd.format(runDay)).getTime());	
		} catch (Exception e) {
			return;
		}

		Thread thread = new Thread("RPMTask-nightlyDeleteSoldOtherProcessing-" + ThreadManager.getNextThreadId()) {
			public void run() {
				GenRow row = new GenRow();
				row.setViewSpec("properties/ProductHouseAndLandSimpleView");
				row.setParameter("CurrentStatusID", lotsSoldByOther_PackageFinalStatusID);
				row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID", lotsSoldByOther_PackageFinalStatusID);
				row.setParameter("ProductType", "House and Land");
				row.setParameter("ModifiedDate", "<" + ddmmyyyy.format(last48));
				row.setParameter("ProductCurrentStatus.CreatedDate", "<" + ddmmyyyy.format(last48));
				row.doAction("search");

				row.getResults();

				while (row.getNext()) {
					logger.trace("nightlyDeleteSoldOtherProcessing Deleted Package {} {}", row.getString("ProductID"), row.getString("Name"));
					try {
						ProductView p = new ProductView(row.getConnection(), row.getString("ProductID"));
						p.delete();
						doIndexRefreash("delete",  row.getString("ProductID"), "House and Land");
					} catch (Exception e) {}
				}

				row.close();
			}
		};
		executor.submit(thread);
	}

	public void nightlyDeleteSoldProcessing() {
		final String lotsSoldByClient_OtherPackageFinalStatusID = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageFinalStatusID");
		final String lotsSoldByClient_OtherPackageEmailDays = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageEmailDays");
		final String lotsSoldByClient_OtherPackageDeleteDay = InitServlet.getSystemParam("LotsSoldByClient.OtherPackageDeleteDay");

		// nothing to do so return
		if (lotsSoldByClient_OtherPackageFinalStatusID == null || lotsSoldByClient_OtherPackageFinalStatusID.length() == 0) return;
		if (lotsSoldByClient_OtherPackageEmailDays == null || lotsSoldByClient_OtherPackageEmailDays.length() == 0) return;
		if (lotsSoldByClient_OtherPackageDeleteDay == null || lotsSoldByClient_OtherPackageDeleteDay.length() == 0) return;

		Calendar cal = Calendar.getInstance();
		try {
			int day = 0;
			day = Integer.parseInt(lotsSoldByClient_OtherPackageDeleteDay);
			if (cal.getMaximum(Calendar.DAY_OF_MONTH) < day) day = cal.getMaximum(Calendar.DAY_OF_MONTH);
			if (cal.get(Calendar.DAY_OF_MONTH) > day) {
				cal.add(Calendar.MONTH,1);
				day = Integer.parseInt(lotsSoldByClient_OtherPackageDeleteDay);
				if (cal.getMaximum(Calendar.DAY_OF_MONTH) < day) day = cal.getMaximum(Calendar.DAY_OF_MONTH);
			}
			cal.set(Calendar.DAY_OF_MONTH, day);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
		} catch (Exception e) {
			return;
		}

		final Date runDay = new Date(cal.getTimeInMillis());

		final Date last48 = new Date();

		if (last48.before(runDay)) return;

		try {
			long offset = (Long) Long.parseLong(lotsSoldByClient_OtherPackageEmailDays) * 24 * 60 * 60 * 1000;
			runDay.setTime(runDay.getTime() - offset);	
			last48.setTime(runyyyymmdd.parse(runyyyymmdd.format(runDay)).getTime());;	
		} catch (Exception e) {
			return;
		}

		Thread thread = new Thread("RPMTask-nightlyDeleteSoldOtherProcessing-" + ThreadManager.getNextThreadId()) {
			public void run() {
				GenRow row = new GenRow();
				row.setViewSpec("properties/ProductHouseAndLandSimpleView");
				row.setParameter("CurrentStatusID", lotsSoldByClient_OtherPackageFinalStatusID);
				row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID", lotsSoldByClient_OtherPackageFinalStatusID);
				row.setParameter("ProductType", "House and Land");
				row.setParameter("ModifiedDate", "<" + ddmmyyyy.format(last48));
				row.setParameter("ProductCurrentStatus.CreatedDate", "<" + ddmmyyyy.format(last48));
				row.doAction("search");

				row.getResults();

				while (row.getNext()) {
					logger.trace("nightlyDeleteSoldProcessing Deleted Package {} {}", row.getString("ProductID"), row.getString("Name"));
					try {
						ProductView p = new ProductView(row.getConnection(), row.getString("ProductID"));
						p.delete();
						doIndexRefreash("delete",  row.getString("ProductID"), "House and Land");
					} catch (Exception e) {}
				}

				row.close();
			}
		};
		executor.submit(thread);
	}

	/**
	 * This is needed as Runway is not relational and tends to leave stuff in the database that it should not.
	 * 
	 * If this data is left in the database it will slow down and also tends to clog the HDD
	 */
	public void nightlyNastyCleanUp() {
		Thread thread = new Thread("RPMTask-nightlyDeleteSoldOtherProcessing-" + ThreadManager.getNextThreadId()) {
			public void run() {
				if (isCleanup) return;
				isCleanup = true;
				GenRow row = new GenRow();
				row.setTableSpec("Products");
				row.setStatement("delete From Products WHERE ProductType = 'Home Plan' AND Category = 'PlanSale' AND ProductID NOT IN (Select LinkedProductID from ProductProducts)");
				row.doAction(ActionBean.EXECUTE);
				row.setStatement("delete From Products WHERE ProductType = 'Land' AND ProductID NOT IN (Select ChildProductID from ProductVariations)");
				row.doAction(ActionBean.EXECUTE);
				row.setStatement("delete From Products WHERE ProductType = 'Home Plan' AND ProductID NOT IN (Select ChildProductID from ProductVariations)");
				row.doAction(ActionBean.EXECUTE);
				row.setTableSpec("properties/ProductDetails");
				row.setStatement("delete from Products WHERE (ProductType = 'Drip' OR ProductType = 'Package Cost' OR ProductType = 'Package Rules') AND ProductID NOT IN (Select LinkedProductID from ProductProducts)");
				row.doAction(ActionBean.EXECUTE);
				row.setTableSpec("properties/ProductDetails");
				row.setStatement("delete from ProductDetails WHERE ProductID NOT IN (SELECT ProductID FROM Products)");
				row.doAction(ActionBean.EXECUTE);
				row.setTableSpec("properties/HomeDetails");
				row.setStatement("delete from HomeDetails WHERE ProductID NOT IN (SELECT ProductID FROM Products)");
				row.doAction(ActionBean.EXECUTE);
				row.setTableSpec("properties/PackageCostings");
				row.setStatement("delete from PackageCostings WHERE ParentProductID NOT IN (SELECT ProductID FROM Products)");
				row.doAction(ActionBean.EXECUTE);
				row.setTableSpec("properties/PackageRules");
				row.setStatement("delete from PackageRules WHERE ProductID NOT IN (SELECT ProductID FROM Products)");
				row.doAction(ActionBean.EXECUTE);
				row.setTableSpec("properties/ProductPublishing");
				row.setStatement("delete from ProductPublishing WHERE ProductID NOT IN (SELECT ProductID FROM Products)");
				row.doAction(ActionBean.EXECUTE);
				row.setTableSpec("ProductTransmission");
				row.setStatement("delete from ProductTransmission WHERE ProductID NOT IN (SELECT ProductID FROM Products)");
				row.doAction(ActionBean.EXECUTE);
				row.setTableSpec("properties/ProductPublishingHistories");
				row.setStatement("delete from ProductPublishingHistory WHERE ProductPublishingID NOT IN (SELECT ProductPublishingID FROM ProductPublishing)");
				row.doAction(ActionBean.EXECUTE);
				row.setTableSpec("ProductTransmissionLog");
				row.setStatement("delete from ProductTransmissionLog WHERE TransmissionID NOT IN (SELECT TransmissionID FROM ProductTransmission)");
				row.doAction(ActionBean.EXECUTE);
				row.setTableSpec("ProductPipeline");
				row.setStatement("delete from ProductPipeline WHERE ProductID NOT IN (SELECT ProductID FROM Products)");
				row.doAction(ActionBean.EXECUTE);
				row.close();
				isCleanup = false;
			}
		};
		executor.submit(thread);
	}

	private Object getStatus(String statusID) {
		if (statusID == null || statusID.length() == 0) return "-missing-";

		GenRow row = new GenRow();
		row.setTableSpec("StatusList");
		row.setParameter("-select0", "Status");
		row.setParameter("StatusID", statusID);
		row.doAction("selectfirst");

		return row.getString("Status");
	}

	public String insertProductStatus(String productID, String statusID) {
		GenRow status = new GenRow();
		status.setTableSpec("ProductStatus");
		status.setConnection(getConnection());
		status.createNewID();
		status.setParameter("ProductID", productID);
		status.setParameter("StatusID", statusID);
		status.setParameter("CreatedDate", "NOW");
		status.setParameter("CreatedBy", InitServlet.getSystemParams()
				.getProperty("SystemUserID"));
		status.doAction("insert");

		return status.getString("ProductStatusID");
	}

	protected void reinit(final type mode) {
		logger.info("Loading RPM Data");
		try {
			logger.debug("starting min/max");
			isGettingData = true;
			/*
			 * These will be populated by the
			 * loadHouseBounds/loadLotBounds/loadApartmentBounds methods
			 * minHomeSize = MinHomeSize(); maxHomeSize = MaxHomeSize();
			 * maxHomeWidth = MaxHouseWidth();
			 * 
			 * maxLotWidth = MaxLotWidth(); maxLotDepth = MaxLotDepth();
			 * minLotArea = MinLotArea(); maxLotArea = MaxLotArea();
			 * 
			 * minApartmentArea = MinApartmentArea(); maxApartmentArea =
			 * MaxApartmentArea();
			 */

			if (mode == type.all || mode == type.house)
				loadHouseBounds();
			if (mode == type.all || mode == type.land)
				loadLotBounds();
			if (mode == type.all || mode == type.apartments)
				loadApartmentBounds();

			if (mode == type.all || mode == type.hal) {
				maxTotal = MaxTotal();
				maxEPTotal = MaxEPTotal();
			}

			logger.debug("ending min/max");
			isGettingData = false;

			// the rest can be loaded when needed, this will slow down the page
			// load, but it will still work
			if (mode == type.all || mode == type.land) {
				PropertyDeveloperList developerList = new PropertyDeveloperList();
				developerList.setActive(true);
				developerList.loadData(null, true);
				this.developerList = developerList;

				logger.debug("developerList should now be populated");

				PropertyEstateList estateList = new PropertyEstateList();
				estateList.setActive(true);
				estateList.loadData(null, true);
				this.estateList = estateList;
				logger.debug("estateList should now be populated");

				// Used to only show stages marked for custom packaging during custom package creation
				PropertyEstateList estateCustomList = new PropertyEstateList();
				estateCustomList.setActive(true);
				estateCustomList.setCustomOnly(true);
				estateCustomList.loadData(null, true);
				this.estateCustomList = estateCustomList;
				logger.debug("estateCustomList should now be populated");
			}

			if (mode == type.all || mode == type.house) {
				PropertyBuilderList builderList = new PropertyBuilderList();
				builderList.setActive(true);
				builderList.loadData(null, true);
				this.builderList = builderList;

				logger.debug("builderList should now be populated");

				PropertyRangeList rangeList = new PropertyRangeList();
				rangeList.setActive(true);
				rangeList.loadData(null, true);
				this.rangeList = rangeList;

				logger.debug("range should now be populated");

				PropertyDesignList designList = new PropertyDesignList();
				designList.setActive(true);
				designList.loadData(null, true);
				this.designList = designList;

				logger.debug("designList should now be populated");
			}

			if (mode == type.all || mode == type.apartments) {
				PropertyBuildingDeveloperList buildingDeveloperList = new PropertyBuildingDeveloperList();
				buildingDeveloperList.setActive(true);
				buildingDeveloperList.loadData(null, true);
				this.buildingDeveloperList = buildingDeveloperList;

				logger.debug("buildingdeveloperList should now be populated");

				PropertyBuildingList buildingList = new PropertyBuildingList();
				buildingList.setActive(true);
				buildingList.loadData(null, true);
				this.buildingList = buildingList;

				logger.debug("buildingList should now be populated");
			}
		} catch (Exception e) {
			logger.error("Error initializing rpm data", e);
			isGettingData = false;
		}

		logger.info("Loading RPM Data compleated");
	}

	public String getProcessName() {
		return "RPM data cache worker";
	}

	protected void processItem() {

	}

	protected void finish() {
		System.err.println("Finnish");
	}

	protected void sendUserEmail(String alert) {
		debug("sending email to " + alert);
		try {

			RowBean context = new RowBean();
			context.put("URLHome", urlhome);
			context.put("EmailAlertsURLHome", emailalertsurlhome);
			context.put("alert", alert);

			String text = generateEmail(context, alerttext);
			String html = generateEmail(context, alerthtml);

			ServletConfig sc = InitServlet.getConfig();
			if (sc != null && alert.length() != 0) {
				MailerBean mailer = new MailerBean();
				mailer.setServletContext(sc.getServletContext());
				mailer.setFrom(defaultFrom);
				mailer.setTo(alert);
				mailer.setSubject("Status Alerts");
				mailer.setTextbody(text);
				mailer.setHtmlbody(html);
				if (config.getPretend()) {
					debug("email to " + alert + "\r\n " + html);
				} else {
					String response = mailer.getResponse();
					debug("email to " + alert + " " + response);
				}
			}

		} catch (Exception e) {
			error(e);
		}
	}

	protected void sendEmail(String to, String msg) {
		debug("sending email to " + to);
		try {

			RowBean context = new RowBean();
			context.put("URLHome", urlhome);
			context.put("EmailAlertsURLHome", emailalertsurlhome);
			context.put("alert", to);
			context.put("body", msg);

			String text = generateEmail(context, alerttext);
			String html = generateEmail(context, alerthtml);
			//String text = generateEmail(context, StringUtil.convertHtmlToText(msg));
			//String html = generateEmail(context, msg);

			ServletConfig sc = InitServlet.getConfig();
			if (sc != null && to.length() != 0) {
				MailerBean mailer = new MailerBean();
				mailer.setServletContext(sc.getServletContext());
				mailer.setFrom(defaultFrom);
				mailer.setTo(to);
				mailer.setSubject("Product Delete Alert");
				mailer.setTextbody(text);
				mailer.setHtmlbody(html);
				if (config.getPretend()) {
					debug("email to " + to + "\r\n " + html);
				} else {
					String response = mailer.getResponse();
					debug("email to " + to + " " + response);
				}
			}

		} catch (Exception e) {
			error(e);
		}
	}

	protected String generateEmail(TableData context, String script) {
		StringWriter text = new StringWriter();
		try {
			StringBuffer roottemplate = new StringBuffer();
			roottemplate.append("#parse(\"");
			roottemplate.append(DEFAULT_VELOCITY_PATH);
			roottemplate.append(script);
			roottemplate.append("\")");

			VelocityEngine ve = VelocityManager.getEngine();
			ve.evaluate(context, text, "StatusAlerts", roottemplate.toString());

			text.flush();
			text.close();

		} catch (Exception e) {
			error(e);
		}
		return (text.toString());
	}

	public void close() {
		super.close();
	}

	public void checkConfig() {
		// debug("Checking config");
		config = RunwayHealthConfig.getRPMConfig(configfilepath);
		pretend = config.getPretend();

		// debug("Wait interval set to "+String.valueOf(config.getInterval()));
	}

	protected EmailerMap getEmailerMap(GenRow row) {
		EmailerMap map = new EmailerMap();
		map.putAll(row);
		return (map);
	}

	protected String getProductUrl(String productid) {
		StringBuffer url = new StringBuffer(emailalertsurlhome);
		url.append("/runway/product.view?ProductID=");
		url.append(productid);
		return (url.toString());
	}

	public static void main(String[] args) {

	}

	public void refreshData() {
		refreshData(type.all);
	}

	public void refreshData(final type mode) {
		// if the queue is to long then we need to do this in real time
		if (executor == null || ((ThreadPoolExecutor) executor).getQueue() == null || ((ThreadPoolExecutor) executor).getQueue().size() > maxThreads) {
			reinit(mode);
		} else {
			Thread thread = new Thread("RPMTask-RefreshDataThread-" + ThreadManager.getNextThreadId()) {
				public void run() {
					reinit(mode);
				}
			};
			executor.submit(thread);
		}
		// thread.start();
	}

	public void refreshIndex(final String mode, final String productID,	final String productType) {
		// if the queue is to long then we need to do this in real time
		if (executor == null || ((ThreadPoolExecutor) executor).getQueue() == null || ((ThreadPoolExecutor) executor).getQueue().size() > maxThreads) {
			if (productID == null || productID.length() == 0) {
				isLoadingIndex = true;
				logger.error("ReIndexing has started at " + new Date());
			}
			doIndexRefreash(mode, productID, productType);
			if (productID == null || productID.length() == 0) {
				cleanQuickIndex();
				logger.error("ReIndexing has ended at " + new Date());
				isLoadingIndex = false;
			}
		} else {
			Thread thread = new Thread("RPMTask-RefreshIndexThread-"
					+ ThreadManager.getNextThreadId()) {
				public void run() {
					// these need to go here as this task can go over the alloted time.
					if (productID == null || productID.length() == 0) {
						isLoadingIndex = true;
						logger.error("ReIndexing has started at " + new Date());
					}
					doIndexRefreash(mode, productID, productType);
					if (productID == null || productID.length() == 0) {
						cleanQuickIndex();
						logger.error("ReIndexing has ended at " + new Date());
						isLoadingIndex = false;
					}
				}
			};
			executor.submit(thread);
		}
		// thread.start();
	}

	public void doIndexRefreash(String mode, String productID) {

		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setParameter("ProductID", productID);
		row.setParameter("-select1", "ProductType");
		row.doAction(GenerationKeys.SELECTFIRST);

		if (row.isSuccessful()) {
			doIndexRefreash(mode, productID, row.getData("ProductType"));
		}
	}

	public void doIndexRefreash(String mode, String productID,
			String productType) {
		if (productID == null)
			productID = "";
		if (productType == null)
			productType = "";
		if (mode == null || mode.length() == 0 || "create".equals(mode)) {
			GenRow row = new GenRow();
			row.setTableSpec("properties/ProductQuickIndex");
			row.setColumn("ON-QuickIndexID", "!NULL");
			row.doAction("deleteall");
			row.setTableSpec("properties/ProductDescriptions");
			row.setColumn("ON-ProductDescriptionID", "!NULL");
			row.doAction("deleteall");
			row.setTableSpec("properties/ProductPublishingIndex");
			row.setColumn("ON-ProductPublishingIndexID", "!NULL");
			row.doAction("deleteall");
			row.close();
			productID = "create";
		}
		if ("delete".equals(mode)) {
			if (productID.length() > 0) {
				deleted.add(productID);
				GenRow row = new GenRow();
				row.setTableSpec("properties/ProductQuickIndex");
				row.setColumn("ON-ProductID", productID);
				row.doAction("deleteall");
				row.clear();

				row.setTableSpec("properties/ProductDescriptions");
				row.setColumn("ON-ProductID", productID);
				row.doAction("deleteall");
				row.clear();

				row.setTableSpec("properties/ProductPublishingIndex");
				row.setColumn("ON-ProductID", productID);
				row.doAction("deleteall");
				row.clear();

				row.setTableSpec("Activities");
				row.setParameter("RecordID", productID);
				row.setParameter("RecordType","Product");
				row.setParameter("ActionType", "Deleted");
				row.setParameter("CreatedDate", "NOW");
				row.setParameter("UserID", InitServlet.getSystemParam("DefaultRepUserID"));
				row.setParameter("ActivityID", KeyMaker.generate());
				row.doAction("insert");

				logger.warn("Attempted to delete product {}", productID);
			} else {
				logger.error("Attempted to delete all products");
			}
		} else {
			if (productType.length() > 0) {
				doIndexRefreshIndex(productID, productType);
				logger.warn("Attempted to add product {} of {}", productID,
						productType);
			} else {
				try {
					doIndexRefreshIndex(productID, "Land");
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					doIndexRefreshIndex(productID, "Apartment");
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					doIndexRefreshIndex(productID, "Home Plan");
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					doIndexRefreshIndex(productID, "House and Land");
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					doIndexRefreshIndex(productID, "Existing Property");
				} catch (Exception e) {
					logger.error("Error while indexing Existing Property");
					e.printStackTrace();
				}
			}
		}
	}

	public void checkPublishingDates() {
		logger.debug("Publishing Dates checking...");
		try {
			GenRow g = new GenRow();
			ActionBean.connect(g);
			g.setViewSpec("properties/ProductPublishingView");
			g.setParameter("PublishStatus", "!Expired");
			g.setParameter("StartDate|1", ">NOW");
			g.setParameter("EndDate|1", "<NOW");
			g.doAction(GenerationKeys.SEARCH);

			g.getResults();

			if (logger.isTraceEnabled()) {
				logger.trace(g.getStatement());
			}
			int updates = 0;
			if (g.getNext()) {
				GenRow update = new GenRow();
				update.setConnection(g.getConnection());
				update.setViewSpec("properties/ProductPublishingView");
				update.setParameter("PublishStatus", "Expired");
				update.setAction(GenerationKeys.UPDATE);
				do {
					// PublishMethod pm =
					// PublishMethod.valueOf(g.getData("PublishMethod"));
					// if(pm.hasFeed()) {
					// The transmission should be disabled in this instance by
					// the Transmissions daemon which will be checking the same
					// dates.
					// }
					update.setParameter("ProductPublishingID",
							g.getData("ProductPublishingID"));
					update.doAction();
					updates++;

					if ("Website".equals(g.getString("PublishMethod"))) {
						setPublished(g.getConnection(), g.getString("ProductID"), "Expired", "N");
					}
				} while (g.getNext());
			}
			logger.info("Publishing Dates checked, {} updated", updates);
			g.close();
		} catch (Exception e) {
			logger.error("Error checking publishing dates", e);
		}
	}

	public void checkPublishingInDates() {
		logger.debug("Publishing IN Dates checking...");

		Date d = new Date();

		try {
			GenRow g = new GenRow();
			ActionBean.connect(g);
			g.setViewSpec("properties/ProductPublishingView");
			g.setParameter("PublishStatus", "Pre-release+Published");
			g.setParameter("PublishMethod", "Website");
			g.setParameter("StartDate", "<=NOW+NULL");
			g.setParameter("EndDate", ">=NOW+NULL");
			g.doAction(GenerationKeys.SEARCH);

			g.getResults();

			System.out.println(g.getStatement());

			if (logger.isTraceEnabled()) {
				logger.trace(g.getStatement());
			}
			int updates = 0;
			if (g.getNext()) {
				GenRow update = new GenRow();
				update.setConnection(g.getConnection());
				update.setViewSpec("properties/ProductPublishingView");
				update.setParameter("PublishStatus", "Published");
				update.setAction(GenerationKeys.UPDATE);
				do {
					// PublishMethod pm =
					// PublishMethod.valueOf(g.getData("PublishMethod"));
					// if(pm.hasFeed()) {
					// The transmission should be disabled in this instance by
					// the Transmissions daemon which will be checking the same
					// dates.
					// }
					update.setParameter("ProductPublishingID",
							g.getData("ProductPublishingID"));
					update.doAction();
					updates++;

					if ("Website".equals(g.getString("PublishMethod"))) {
						//String state = ((!g.isSet("StartDate") || d.after(g.getDate("StartDate"))) && (!g.isSet("EndDate") || d.after(g.getDate("EndDate"))))? "Y" : "N"; 
						setPublished(g.getConnection(), g.getString("ProductID"), "Published", "Y");
					}
				} while (g.getNext());
			}
			logger.info("Publishing IN Dates checked, {} updated", updates);
			g.close();
		} catch (Exception e) {
			logger.error("Error checking publishing dates", e);
		}
	}

	private void setPublished(Connection conn, String productID, String status, String published) {
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductPublishingIndex");
		row.setConnection(conn);
		row.setParameter("ON-ProductID", productID);
		row.setParameter("WebsitePublishStatus", status);
		row.setParameter("WebsitePublishedFlag", published);
		row.doAction("updateall");
	}

	protected void doIndexRefreshIndex(String productID, String productType) {
		if (productID == null)
			productID = "";
		if (productType == null)
			productType = "";

		boolean create = false;
		if ("create".equals(productID)) {
			create = true;
			productID = "";
		}

		logger.error("Re-indexing for " + productType + " and " + productID);

		long startTime = System.currentTimeMillis();

		Date d = new Date();

		GenRow products = new GenRow();
		if (productID.length() > 0 && !create) {
			products.setParameter("ProductID", productID);
		} else if (productType.length() > 0) {
			products.setParameter("ProductType", productType);
			if (!create) {
				int interval = super.config.getInterval();
				long l = (long) Math.floor(d.getTime() - ((interval * 1000) * 1.1));
				d.setTime(l);
			}
		} 

		String vs = "";
		if ("Home Plan".equalsIgnoreCase(productType)) {
			vs = "properties/ProductPlanView";
			// products.setParameter("Category","NULL");
			products.setParameter("HomeProductID", "!NULL;!EMPTY");
			products.setParameter(
					"ProductParentHomeDesign.ProductVariationHomeDesign.ProductID",
					"!NULL;!EMPTY");
			if (!create && StringUtils.isBlank(productID)) {
				products.setParameter("ModifiedDate|m", ">" + d);
				products.setParameter("ProductParentHomeDesign.ProductVariationHomeDesign.ModifiedDate|m", ">" + d);
				products.setParameter("ProductParentHomeDesign.ProductVariationHomeDesign.ProductParentHomeRange.ProductVariationHomeRange.ModifiedDate|m", ">" + d);
			}

		} else if ("Land".equalsIgnoreCase(productType)) {
			vs = "properties/ProductLotView";
			products.setParameter("StageProductID", "!NULL;!EMPTY");
			products.setParameter(
					"ProductParentStageLot.ProductVariationStageLot.ProductID",
					"!NULL;!EMPTY");
			if (!create && StringUtils.isBlank(productID)) {
				products.setParameter("ModifiedDate|m", ">" + d);
				products.setParameter("ProductParentStageLot.ProductVariationStageLot.ModifiedDate|m", ">" + d);
				products.setParameter("ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ModifiedDate|m", ">" + d);
			}

		} else if ("Apartment".equalsIgnoreCase(productType)) {
			vs = "properties/ProductApartmentView";
			products.setParameter("BuildingStageProductID", "!NULL;!EMPTY");
			products.setParameter(
					"ProductParentBuildingStageApartment.ProductVariationBuildingStageApartment.ProductID",
					"!NULL;!EMPTY");
			if (!create && StringUtils.isBlank(productID)) {
				products.setParameter("ModifiedDate|m", ">" + d);
				products.setParameter("ProductParentBuildingStageApartment.ProductVariationBuildingStageApartment.ModifiedDate|m", ">" + d);
				products.setParameter("ProductParentBuildingStageApartment.ProductVariationBuildingStageApartment.ProductParentBuildingBuildingStage.ProductVariationBuildingBuildingStage.ModifiedDate|m", ">" + d);
			}

		} else if ("House and Land".equalsIgnoreCase(productType)) {
			vs = "properties/ProductHouseAndLandView";
			products.setParameter("HomeProductID", "!NULL;!EMPTY");
			products.setParameter(
					"ProductParentHomeDesign.ProductVariationHomeDesign.ProductID",
					"!NULL;!EMPTY");
			products.setParameter("StageProductID", "!NULL;!EMPTY");
			products.setParameter(
					"ProductParentStageLot.ProductVariationStageLot.ProductID",
					"!NULL;!EMPTY");
			if (!create && StringUtils.isBlank(productID)) {
				products.setParameter("ModifiedDate", ">" + d);
			}
		} else if ("Existing Property".equalsIgnoreCase(productType)) {
			vs = "properties/ProductExistingPropertySimpleView";
			if (!create && StringUtils.isBlank(productID)) {
				products.setParameter("ModifiedDate", ">" + d);
			}
		} else if (StringUtils.isNotBlank(productID)) {
			if (!create && StringUtils.isBlank(productID)) {
				products.setParameter("ModifiedDate", ">" + d);
			}
			PropertyEntity pe = null;
			if ("Estate".equals(productType)) {
				pe = getEstateList().getItemByID(productID);
			} else if ("Stage".equals(productType)) {
				GenRow st = new GenRow();
				st.setViewSpec("products/ProductStageView");
				st.setParameter("ProductID", productID);
				st.doAction("select");
				PropertyEstate estate = (PropertyEstate) getEstateList()
						.getItemByID(st.getData("EstateProductID"));
				if (estate != null) {
					PropertyStageList psl = estate.getStageList();
					if (psl != null) {
						pe = psl.getItemByID(productID);
					}
				}
			} else if ("Building".equals(productType)) {
				pe = getBuildingList().getItemByID(productID);
			} else if ("Building Stage".equals(productType)) {
				GenRow st = new GenRow();
				st.setViewSpec("products/ProductBuildingStageView");
				st.setParameter("ProductID", productID);
				st.doAction("select");
				PropertyBuilding building = (PropertyBuilding) getBuildingList()
						.getItemByID(st.getData("BuildingProductID"));
				if (building != null) {
					PropertyBuildingStageList psl = building
							.getBuildingStageList();
					if (psl != null) {
						pe = psl.getItemByID(productID);
					}
				}
			} else if ("Home Range".equals(productType)) {
				pe = getRangeList().getItemByID(productID);
			} else if ("Home".equals(productType)) {
				pe = getDesignList().getItemByID(productID);
			} else if ("Developer".equals(productType)) {
				pe = getDeveloperList().getItemByID(productID);
			} else if ("Brand".equals(productType)) {
				pe = getBuilderList().getItemByID(productID);
			}
			if (pe != null)
				pe.loadSecurityGroups(null);

			return;
		}

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		row.setConnection(getConnection());

		GenRow description = new GenRow();
		description.setTableSpec("properties/ProductDescriptions");
		description.setConnection(getConnection());

		GenRow publish = new GenRow();
		publish.setTableSpec("properties/ProductPublishingIndex");
		publish.setConnection(getConnection());

		products.setViewSpec(vs);
		// products.setParameter("-groupby0","ProductID");
		try {
			// let it get it's own connection for this one
			// products.setConnection(getConnection());
			products.doAction("search");
			products.getResults();
			// if (productID.length() > 0)
			//System.out.println(products.getStatement());
		} catch (Exception e) {
			e.printStackTrace();
			products.close();
			return;
		}

		logger.trace("Re-indexing statement " + products.getStatement());

		HashSet<String> keys = new HashSet<String>();

		while (products.getNext()) {
			if (keys.contains(products.getData("ProductID"))) continue;
			if (deleted.contains(products.getData("ProductID"))) {
				logger.error("Re-indexing detected deleted product " + products.getData("ProductID"));
				continue;
			}
			keys.add(products.getData("ProductID"));
			if (logger.isTraceEnabled()) {
				logger.trace("RPM Index " + products.getData("ProductID") + " - " + products.getData("ProductType"));
			}
			try {
				if (!convertProduct2QuickIndex(products, row, description, publish))
					continue;
			} catch (Exception e) {
				logger.error("Error with conversion ", e);
			}

			try {
				row.doAction("insert");
				description.doAction("insert");				
			} catch (Exception e) {
				try {
					row.doAction("update");
					description.doAction("update");				
				} catch (Exception e1) {
					try {
						row.setColumn("ON-ProductID", products.getData("ProductID"));
						row.doAction("updateall");
						description.setColumn("ON-ProductID", products.getData("ProductID"));
						description.doAction("updateall");
					} catch (Exception e2) {
						if (e2 instanceof SQLIntegrityConstraintViolationException) {
							logger.error("Duplicate {} {} Error {}",
									new String[] { row.getString("ProductID"),
									productType, e2.getMessage() });
							// System.out.println("Duplicate " +
							// row.getString("ProductID") + " " + productType +
							// " Error " + e2.getMessage());
						} else {
							// unexpected error type, we want the stack trace
							logger.error("Error with " + row.toString() + " "
									+ productType + " msg " + row.getError(), e2);
						}
					}
				}
			}

			try {
				if (publish.getString("ProductPublishingIndexID").length() > 0) publish.doAction("insert");
			} catch (Exception e) {
				try {
					if (publish.getString("ProductPublishingIndexID").length() > 0) publish.doAction("update");
				} catch (Exception e1) {
					try {
						publish.setColumn("ON-ProductID", products.getData("ProductID"));
						if (publish.getString("ProductPublishingIndexID").length() > 0) publish.doAction("updateall");
					} catch (Exception e2) {
						if (e2 instanceof SQLIntegrityConstraintViolationException) {
							logger.error("Duplicate {} {} Error {}",
									new String[] { row.getString("ProductID"),
									productType, e2.getMessage() });
							// System.out.println("Duplicate " +
							// row.getString("ProductID") + " " + productType +
							// " Error " + e2.getMessage());
						} else {
							// unexpected error type, we want the stack trace
							logger.error("Error with " + row.toString() + " "
									+ productType + " msg " + row.getError(), e2);
						}
					}
				}
			}

			if (logger.isTraceEnabled()) {
				logger.trace("RPM Index [" + row.getString("QuickIndexID") + "] " + row.getString("ProductID") + " - " + row.getString("ProductType"));
			}
			if (logger.isTraceEnabled()) {
				logger.trace("RPM Index Insert " +  row.toString());
			}
			// this will be handled by a different process now, but we need to backwards it for runways that don't have this
			if ("Land".equals(products.getData("ProductType")) && (InitServlet.getSystemParam("LotsSoldByOther.LotStatusID") == null || InitServlet.getSystemParam("LotsSoldByOther.LotStatusID").length() == 0)) {
				processLandStatuses(products);
			}
			if("true".equals(InitServlet.getSystemParams().getProperty("PipelinePushEnabled")) ) {
				processProductPipeline(products.getData("ProductID"));
			}
		}
		products.close();

		//deleted.clear();



		logger.error("Re-indexing for " + productType + " and " + productID + " compleated. time "
				+ ((double) (System.currentTimeMillis() - startTime) / 1000));
	}

	protected void doIndexRefreshIndexForExistingProperty(String productID, String productType) {
		if (productID == null)
			productID = "";
		if (productType == null)
			productType = "";

		logger.error("Re-indexing for " + productType + " and " + productID);

		long startTime = System.currentTimeMillis();

		GenRow products = new GenRow();
		String vs = "";
		if ("Home Plan".equalsIgnoreCase(productType)) {
			vs = "properties/ProductPlanView";
			// products.setParameter("Category","NULL");
			products.setParameter("HomeProductID", "!NULL;!EMPTY");
			products.setParameter(
					"ProductParentHomeDesign.ProductVariationHomeDesign.ProductID",
					"!NULL;!EMPTY");

		} else if ("Land".equalsIgnoreCase(productType)) {
			vs = "properties/ProductLotView";
			products.setParameter("StageProductID", "!NULL;!EMPTY");
			products.setParameter(
					"ProductParentStageLot.ProductVariationStageLot.ProductID",
					"!NULL;!EMPTY");

		} else if ("Apartment".equalsIgnoreCase(productType)) {
			vs = "properties/ProductApartmentView";
			products.setParameter("BuildingStageProductID", "!NULL;!EMPTY");
			products.setParameter(
					"ProductParentBuildingStageApartment.ProductVariationBuildingStageApartment.ProductID",
					"!NULL;!EMPTY");

		} else if ("House and Land".equalsIgnoreCase(productType)) {
			vs = "properties/ProductHouseAndLandView";
			products.setParameter("HomeProductID", "!NULL;!EMPTY");
			products.setParameter(
					"ProductParentHomeDesign.ProductVariationHomeDesign.ProductID",
					"!NULL;!EMPTY");
			products.setParameter("StageProductID", "!NULL;!EMPTY");
			products.setParameter(
					"ProductParentStageLot.ProductVariationStageLot.ProductID",
					"!NULL;!EMPTY");
		} else if (StringUtils.isNotBlank(productID)) {
			PropertyEntity pe = null;
			if ("Estate".equals(productType)) {
				pe = getEstateList().getItemByID(productID);
			} else if ("Stage".equals(productType)) {
				GenRow st = new GenRow();
				st.setViewSpec("products/ProductStageView");
				st.setParameter("ProductID", productID);
				st.doAction("select");
				PropertyEstate estate = (PropertyEstate) getEstateList()
						.getItemByID(st.getData("EstateProductID"));
				if (estate != null) {
					PropertyStageList psl = estate.getStageList();
					if (psl != null) {
						pe = psl.getItemByID(productID);
					}
				}
			} else if ("Building".equals(productType)) {
				pe = getBuildingList().getItemByID(productID);
			} else if ("Building Stage".equals(productType)) {
				GenRow st = new GenRow();
				st.setViewSpec("products/ProductBuildingStageView");
				st.setParameter("ProductID", productID);
				st.doAction("select");
				PropertyBuilding building = (PropertyBuilding) getBuildingList()
						.getItemByID(st.getData("BuildingProductID"));
				if (building != null) {
					PropertyBuildingStageList psl = building
							.getBuildingStageList();
					if (psl != null) {
						pe = psl.getItemByID(productID);
					}
				}
			} else if ("Home Range".equals(productType)) {
				pe = getRangeList().getItemByID(productID);
			} else if ("Home".equals(productType)) {
				pe = getDesignList().getItemByID(productID);
			} else if ("Developer".equals(productType)) {
				pe = getDeveloperList().getItemByID(productID);
			} else if ("Brand".equals(productType)) {
				pe = getBuilderList().getItemByID(productID);
			}
			if (pe != null)
				pe.loadSecurityGroups(null);

			return;
		}

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		row.setConnection(getConnection());

		GenRow description = new GenRow();
		description.setTableSpec("properties/ProductDescriptions");
		description.setConnection(getConnection());

		GenRow publish = new GenRow();
		publish.setTableSpec("properties/ProductPublishingIndex");
		publish.setConnection(getConnection());

		products.setViewSpec(vs);
		if (productID.length() > 0)
			products.setParameter("ProductID", productID);
		if (productType.length() > 0)
			products.setParameter("ProductType", productType);
		// products.setParameter("-groupby0","ProductID");
		try {
			// let it get it's own connection for this one
			// products.setConnection(getConnection());
			products.doAction("search");
			products.getResults();
			// if (productID.length() > 0)
			// System.out.println(products.getStatement());
		} catch (Exception e) {
			e.printStackTrace();
			products.close();
			return;
		}

		logger.trace("Re-indexing statement " + products.getStatement());

		HashSet<String> keys = new HashSet<String>();

		while (products.getNext()) {
			if (keys.contains(products.getData("ProductID"))) continue;
			if (deleted.contains(products.getData("ProductID"))) {
				logger.error("Re-indexing detected deleted product " + products.getData("ProductID"));
				continue;
			}
			keys.add(products.getData("ProductID"));
			try {
				if (!convertProduct2QuickIndex(products, row, description, publish))
					continue;
			} catch (Exception e) {
				logger.error("Error with conversion ", e);
			}

			try {
				row.doAction("insert");
				description.doAction("insert");
				if (publish.getString("ProductPublishingIndexID").length() > 0) publish.doAction("insert");
			} catch (Exception e) {
				try {
					row.setColumn("ON-ProductID", products.getData("ProductID"));
					row.doAction("updateall");
					description.setColumn("ON-ProductID", products.getData("ProductID"));
					description.doAction("updateall");
					publish.setColumn("ON-ProductID", products.getData("ProductID"));
					if (publish.getString("ProductPublishingIndexID").length() > 0) publish.doAction("updateall");
				} catch (Exception e2) {
					if (e2 instanceof SQLIntegrityConstraintViolationException) {
						logger.error("Duplicate {} {} Error {}",
								new String[] { row.getString("ProductID"),
								productType, e2.getMessage() });
						// System.out.println("Duplicate " +
						// row.getString("ProductID") + " " + productType +
						// " Error " + e2.getMessage());
					} else {
						// unexpected error type, we want the stack trace
						logger.error("Error with " + row.toString() + " "
								+ productType + " msg " + e + " - " + e2);
					}
				}
			}
			// this will be handled by a different process now, but we need to backwards it for runways that don't have this
			if ("Land".equals(products.getData("ProductType")) && (InitServlet.getSystemParam("LotsSoldByOther.LotStatusID") == null || InitServlet.getSystemParam("LotsSoldByOther.LotStatusID").length() == 0)) {
				processLandStatuses(products);
			}
		}
		products.close();

		//deleted.clear();

		logger.error("Re-indexing for " + productType + " and " + productID + " compleated. time "
				+ ((double) (System.currentTimeMillis() - startTime) / 1000));
	}

	public boolean convertProduct2QuickIndex(GenRow product, GenRow quickIndex) {
		return convertProduct2QuickIndex(product, quickIndex, null, null);
	}

	public boolean convertProduct2QuickIndex(GenRow product, GenRow quickIndex, GenRow description, GenRow publish) {
		if ("Home Plan".equalsIgnoreCase(product.getString("ProductType"))
				&& product.getString("Category").length() > 0) {
			return false;
		}

		if ("Existing Property".equalsIgnoreCase(product.getData("ProductType"))) {
			return convertProduct2QuickIndexForEP(product, quickIndex, description, publish);
		}

		quickIndex.clear();
		quickIndex.setColumn("QuickIndexID", product.getData("ProductID"));
		quickIndex.setColumn("ProductID", product.getData("ProductID"));
		quickIndex.setColumn("GroupID", product.getData("GroupID"));
		quickIndex.setColumn("Name", product.getData("Name"));
		quickIndex.setColumn("ProductNum", product.getData("ProductNum"));
		quickIndex.setColumn("BrandName", product.getData("BrandName"));
		quickIndex.setColumn("BuilderID", product.getData("BrandProductID"));
		quickIndex.setColumn("BuilderProductID", product.getData("BuilderProductID"));
		quickIndex.setColumn("BuilderName", product.getData("BuilderName"));
		quickIndex.setColumn("RangeID", product.getData("RangeProductID"));
		quickIndex.setColumn("DesignID", product.getData("HomeProductID"));
		if (product.getData("PlanName").startsWith(product.getData("HomeName")))
			quickIndex.setColumn("HomePlanName", product.getData("PlanName"));
		else
			quickIndex.setColumn("HomePlanName", product.getData("HomeName") + " " + product.getData("PlanName"));
		quickIndex.setColumn("RangeName", product.getData("RangeName"));
		quickIndex.setColumn("PlanID", product.getData("PlanID"));
		quickIndex.setColumn("CopiedFromID", product.getData("CopyPlanID"));
		quickIndex.setColumn("producttatusID", product.getData("producttatusID"));
		quickIndex.setColumn("ProductType", product.getData("ProductType"));
		quickIndex.setColumn("LotType", product.getData("ProductSubType"));
		quickIndex.setColumn("ThumbDocumentID", product.getData("ThumbnailImage"));
		quickIndex.setColumn("ImageDocumentID", product.getData("Image"));
		quickIndex.setColumn("RangeColorHex", product.getData("RangeColorHex"));
		quickIndex.setColumn("DisplayPrice", product.getData("DisplayPrice"));
		quickIndex.setColumn("DisplayPriceType", product.getData("DisplayPriceType"));
		quickIndex.setColumn("LotExclusivity", product.getData("MarketingStatus"));
		quickIndex.setColumn("LocationID", product.getData("LocationID"));
		quickIndex.setColumn("RegionID", product.getData("RegionID"));
		quickIndex.setColumn("ExtraSearch", product.getData("ExtraSearch"));
		quickIndex.setColumn("Media", product.getData("Media"));
		quickIndex.setColumn("Orientation", product.getData("Orientation"));

		if ("Land".equals(product.getData("ProductType"))) {
			quickIndex.setColumn("LotName", product.getData("Name"));
			quickIndex.setColumn("LotWidth",
					cleanNumbers(product.getData("LotWidth")));
			quickIndex.setColumn("LotDepth",
					cleanNumbers(product.getData("LotDepth")));
			//quickIndex.setColumn("LotType", product.getData("productubType"));
			quickIndex.setColumn("BuildWidth",
					cleanNumbers(product.getData("BuildWidth")));
			quickIndex.setColumn("BuildDepth",
					cleanNumbers(product.getData("BuildDepth")));
			quickIndex.setColumn("FitsBuildWidth",
					cleanNumbers(product.getData("FitsBuildWidth")));
			quickIndex.setColumn("FitsBuildDepth",
					cleanNumbers(product.getData("FitsBuildDepth")));
			quickIndex.setParameter("LotArea", product.getDouble("Area"));
			quickIndex.setParameter("LotPrice", product.getDouble("TotalCost"));
			if (quickIndex.getString("ImageDocumentID").length() == 0) quickIndex.setColumn("ImageDocumentID", product.getData("EstateImage"));
			if (quickIndex.getString("ThumbDocumentID").length() == 0) quickIndex.setColumn("ThumbDocumentID", product.getData("EstateThumbnailImage"));
			quickIndex.setColumn("DeveloperID",	product.getData("DeveloperProductID"));
			quickIndex.setColumn("DeveloperName", product.getData("DeveloperName"));
			quickIndex.setColumn("EstateID", product.getData("EstateProductID"));
			quickIndex.setColumn("EstateName", product.getData("EstateName"));
			quickIndex.setColumn("StageID", product.getData("StageProductID"));
			quickIndex.setColumn("StageName", product.getData("StageName"));
			quickIndex.setColumn("LotID", product.getData("LotID"));
			quickIndex.setColumn("LotName", product.getData("LotName"));
		}
		if ("Apartment".equals(product.getData("ProductType"))) {
			quickIndex.setColumn("DeveloperID",	product.getData("BuildingDeveloperID"));
			quickIndex.setColumn("DeveloperName", product.getData("BuildingDeveloperName"));
			quickIndex.setColumn("EstateID", product.getData("BuildingProductID"));
			quickIndex.setColumn("EstateName", product.getData("BuildingName"));
			quickIndex.setColumn("StageID", product.getData("BuildingStageProductID"));
			quickIndex.setColumn("StageName", product.getData("BuildingStageName"));
			quickIndex.setColumn("LotName", product.getData("Name"));
			quickIndex.setParameter("InternalArea", product.getDouble("InternalArea"));
			quickIndex.setParameter("ExternalArea", product.getDouble("ExternalArea"));
			quickIndex.setColumn("LotWidth", cleanNumbers(product.getData("LotWidth")));
			quickIndex.setColumn("LotDepth", cleanNumbers(product.getData("LotDepth")));
			//quickIndex.setColumn("LotType", product.getData("ProductSubType"));
			quickIndex.setParameter("LotArea", product.getDouble("Area"));
			if (!quickIndex.isSet("LotArea") || product.getDouble("Area") == 0) quickIndex.setParameter("LotArea", product.getDouble("HomeSize"));

			quickIndex.setParameter("LotPrice", product.getDouble("TotalCost"));
			quickIndex.setColumn("HomeWidth",
					cleanNumbers(product.getData("LotWidth")));
			quickIndex.setColumn("HomeDepth",
					cleanNumbers(product.getData("LotDepth")));
			quickIndex.setParameter("HomeArea", product.getDouble("LotArea"));
			quickIndex.setParameter("HomePrice", product.getDouble("LotPrice"));
			if (quickIndex.getString("ImageDocumentID").length() == 0) quickIndex.setColumn("ImageDocumentID", product.getData("BuildingImage"));
			if (quickIndex.getString("ThumbDocumentID").length() == 0) quickIndex.setColumn("ThumbDocumentID", product.getData("BuildingThumbnailImage"));
		}
		if ("Home Plan".equals(product.getData("ProductType"))) {
			quickIndex.setColumn("DesignName", product.getData("HomeName"));
			quickIndex.setColumn("HomeWidth", product.getData("LotWidth"));
			quickIndex.setColumn("HomeDepth", product.getData("LotDepth"));
			quickIndex.setParameter("HomeArea", product.getString("HomeSize"));
			quickIndex.setParameter("HomePrice", product.getDouble("TotalCost"));
			quickIndex.setParameter("DripCost", product.getDouble("DripCost"));
			quickIndex.setParameter("DripResult",product.getDouble("DripResult"));
			quickIndex.setColumn("CanFitOnDepth",product.getData("CanFitOnDepth"));
			quickIndex.setColumn("CanFitOnFrontage",product.getData("CanFitOnFrontage"));
			quickIndex.setColumn("DesignThumbDocumentID", product.getData("HomeThumbnailImage"));
			quickIndex.setColumn("DesignImageDocumentID", product.getData("HomeImage"));

		}
		if ("House and Land".equals(product.getData("ProductType"))) {
			quickIndex.setColumn("DeveloperID",	product.getData("DeveloperProductID"));
			quickIndex.setColumn("DeveloperName", product.getData("DeveloperName"));
			quickIndex.setColumn("EstateID", product.getData("EstateProductID"));
			quickIndex.setColumn("EstateName", product.getData("EstateName"));
			quickIndex.setColumn("StageID", product.getData("StageProductID"));
			quickIndex.setColumn("StageName", product.getData("StageName"));
			quickIndex.setColumn("LotID", product.getData("LotID"));
			quickIndex.setColumn("LotName", product.getData("LotName"));
			quickIndex.setColumn("DisplayFlag", product.getData("DisplayFlag"));

			GenRow land = getProductQuickIndexLand(quickIndex.getString("LotID"));
			GenRow home = getProductQuickIndexHome(quickIndex.getString("CopiedFromID"));
			GenRow facade = getFacade(product.getData("ProductID"));

			quickIndex.setColumn("DesignName", home.getData("DesignName"));
			quickIndex.setColumn("HomePlanName", home.getData("HomePlanName"));
			quickIndex.setColumn("FacadeProductID", facade.getString("LinkedProductID"));
			quickIndex.setColumn("FacadeName", facade.getString("Name"));
			quickIndex.setColumn("HomeWidth", cleanNumbers(home.getData("HomeWidth")));
			quickIndex.setColumn("HomeDepth", cleanNumbers(home.getData("HomeDepth")));
			quickIndex.setParameter("HomeArea", home.getDouble("HomeArea"));
			quickIndex.setParameter("HomePrice", product.getDouble("PlanTotalCost"));
			quickIndex.setColumn("LotWidth", cleanNumbers(land.getData("LotWidth")));
			quickIndex.setColumn("LotDepth", cleanNumbers(land.getData("LotDepth")));
			quickIndex.setColumn("DisplayPriceType",product.getData("DisplayPriceType"));
			if (!quickIndex.isSet("DisplayPriceType")) quickIndex.setColumn("DisplayPriceType",land.getData("DisplayPriceType")); 
			quickIndex.setParameter("LotArea", land.getDouble("LotArea"));
			quickIndex.setParameter("LotPrice", product.getDouble("LotTotalCost"));
			quickIndex.setParameter("DripCost", product.getDouble("DripCost"));
			quickIndex.setParameter("DripResult", product.getDouble("DripResult"));
			quickIndex.setColumn("PortalFlag", product.getData("PortalFlag"));
			quickIndex.setColumn("ShowOnPortalFlag", product.getData("ShowOnPortalFlag"));
			quickIndex.setParameter("PortalTotalCost", product.getDouble("PortalTotalCost"));
			quickIndex.setColumn("LotExclusivity", StringUtils.isBlank(product.getData("MarketingStatus")) ? product.getData("LotMarketingStatus")  : product.getData("MarketingStatus"));
			quickIndex.setColumn("DesignThumbDocumentID", home.getData("DesignThumbDocumentID"));
			quickIndex.setColumn("DesignImageDocumentID", home.getData("DesignImageDocumentID"));
			if (quickIndex.getString("DesignImageDocumentID").length() == 0) quickIndex.setColumn("ImageDocumentID", home.getData("HomeImage"));
			if (quickIndex.getString("DesihnThumbDocumentID").length() == 0) quickIndex.setColumn("ThumbDocumentID", home.getData("HomeThumbnailImage"));
		}
		String stories = product.getData("Stories");
		if (stories.length() == 0)
			stories = product.getData("Storey");
		if (stories.length() == 0)
			stories = product.getData("Storeys");
		if ("Single".equals(stories))
			stories = "1";
		if ("Double".equals(stories))
			stories = "2";
		quickIndex.setColumn("Storeys", stories);
		quickIndex.setColumn("Bedrooms", product.getData("Bedrooms"));
		quickIndex.setColumn("Bathrooms", product.getData("Bathrooms"));
		quickIndex.setColumn("Garage", product.getData("Garage"));
		quickIndex.setColumn("CarParks", product.getData("CarParks"));
		quickIndex.setColumn("Study", product.getData("Study"));
		quickIndex.setColumn("LivingRooms", product.getData("LivingRooms"));
		quickIndex.setColumn("Active", product.getData("Active"));
		quickIndex.setColumn("ProductStatusID",	product.getData("ProductStatusID"));
		quickIndex.setColumn("PublishingStatus", product.getData("PublishingStatus"));
		quickIndex.setColumn("PackageStatus", product.getData("PackageStatus"));
		quickIndex.setColumn("CreatedBy", product.getData("CreatedBy"));
		quickIndex.setColumn("CreatedDate", product.getData("CreatedDate"));
		quickIndex.setColumn("AvailableDate", product.getData("AvailableDate"));
		quickIndex.setColumn("ExpiryDate", product.getData("ExpiryDate"));
		quickIndex.setColumn("PreAvailableDate", product.getData("PreAvailableDate"));
		quickIndex.setParameter("TotalCost", product.getDouble("TotalCost"));
		quickIndex.setColumn("CampaignID", product.getData("CampaignID"));
		quickIndex.setColumn("ProductSubType",  product.getData("ProductSubType"));

		if (description != null) {
			description.clear();
			description.setColumn("ProductDescriptionID", quickIndex.getString("QuickIndexID"));
			description.setColumn("ProductID", product.getData("ProductID"));
			description.setColumn("PublishedFor", "Website");
			if (product.isSet("PublishDescription"))  description.setParameter("Description", evaluateHeadline(product, product.getString("PublishDescription")));
			else if ("true".equals(InitServlet.getSystemParam("RPM-UseDescription"))) description.setParameter("Description", evaluateHeadline(product, product.getString("Description")));
			description.setParameter("HeadLine", evaluateHeadline(product, product.getString("Summary")));
		}

		if (publish != null) {
			publish.clear();
			convertPublishing(quickIndex, publish);
		}

		return true;
	}

	private void convertPublishing(GenRow quickIndex, GenRow publish) {
		GenRow row = new GenRow();
		row.setConnection(quickIndex.getConnection());
		row.setTableSpec("properties/ProductPublishing");
		row.setParameter("-select0", "*");
		row.setParameter("ProductID", quickIndex.getString("ProductID"));
		row.doAction("search");

		row.getResults();

		if (row.getNext()) {
			publish.setParameter("ProductPublishingIndexID", quickIndex.getString("QuickIndexID"));
			publish.setParameter("ProductID", quickIndex.getString("ProductID"));

			do {
				String method = row.getString("PublishMethod");
				publish.setParameter(method + "PublishStatus", row.getString("PublishStatus"));
				publish.setParameter(method + "StartDate", row.getString("StartDate"));
				publish.setParameter(method + "EndDate", row.getString("EndDate"));
				publish.setParameter(method + "PublishedFlag", row.getString("PublishedFlag"));
				publish.setParameter(method + "ProductPublishingID", row.getString("ProductPublishingID"));
				publish.setParameter(method + "ListingID", row.getString("ListingID"));
				publish.setParameter(method + "FirstPublishDate", row.getString("FirstPublishDate"));

				//System.out.println("ProductID " + publish.getString("ProductID") + " " + method + " " + publish.getString(method + "PublishStatus"));
			} while (row.getNext());
		}

		row.close();
	}

	private static String evaluateHeadline(GenRow product, String headline) {
		if (headline != null && headline.length() > 0) {
			if (headline.indexOf("${") >= 0) {
				while (headline.indexOf("${") >= 0) {
					int start = headline.indexOf("${");
					int end = headline.indexOf("}");
					if (start < end && start != -1 && end != -1) {
						String tag = headline.substring(start + 2, end);
						String value = product.getString(tag);

						headline = headline.replaceAll((new StringBuilder("\\$\\{")).append(tag).append("\\}").toString(), value);
					} else {
						break;
					}
				}
			}
		}
		return headline;
	}


	/** Populate quick index table with Existing property related info
	 * @param product
	 * @param quickIndex
	 * @param description 
	 * @param publish 
	 * @return
	 */
	public boolean convertProduct2QuickIndexForEP(GenRow product, GenRow quickIndex, GenRow description, GenRow publish) {
		quickIndex.clear();

		quickIndex.setColumn("QuickIndexID", product.getData("ProductID"));

		quickIndex.setColumn("Name", product.getData("Name"));
		quickIndex.setColumn("ProductNum", product.getData("ProductNum"));
		quickIndex.setColumn("ProductID", product.getData("ProductID"));
		quickIndex.setColumn("GroupID", product.getData("GroupID"));
		quickIndex.setColumn("ProductType", product.getData("ProductType"));

		// Since Lot and Plan are optional
		String lotProductID = null;
		String planProductID = null;

		GenRow pp = new GenRow();
		try {
			pp.setConnection(getConnection());
			pp.setViewSpec("ProductProductLinkedView");
			pp.setParameter("ProductID", product.getData("ProductID"));
			pp.getResults();

			while (pp.getNext()) {
				if ("Land".equals(pp.getData("ProductType"))) {
					lotProductID = pp.getData("LinkedProductID");
				} else if ("Home Plan".equals(pp.getData("ProductType"))) {
					planProductID = pp.getData("LinkedProductID");
				}
			}
		} finally {
			pp.close();
		}

		// do this first, if there is land then we need to over write it.
		quickIndex.setColumn("LotType", product.getData("ProductSubType"));

		// Land
		if (StringUtils.isNotBlank(lotProductID)) {
			GenRow landQuick = getProductQuickIndexLand(lotProductID);
			quickIndex.setColumn("LotID", landQuick.getData("ProductID"));
			quickIndex.setColumn("LotName", landQuick.getData("Name"));
			quickIndex.setColumn("StageID", landQuick.getData("StageID"));
			quickIndex.setColumn("EstateID", landQuick.getData("EstateID"));
			quickIndex.setColumn("StageName", landQuick.getData("StageName"));
			quickIndex.setColumn("EstateName", landQuick.getData("EstateName"));
			quickIndex.setColumn("DeveloperID", landQuick.getData("DeveloperID"));
			quickIndex.setColumn("LotExclusivity", StringUtils.isBlank(landQuick.getData("LotExclusivity")) ? product.getData("MarketingStatus") : landQuick.getData("LotExclusivity"));

			// Most products have sub type at land level
			if(StringUtils.isNotBlank(landQuick.getString("ProductSubType"))) {
				quickIndex.setParameter("ProductSubType", landQuick.getString("ProductSubType"));
			} else {
				quickIndex.setParameter("ProductSubType", product.getData("ProductSubType"));
			}

			quickIndex.setParameter("DisplayPriceType", landQuick.getString("DisplayPriceType"));
			quickIndex.setColumn("Orientation", landQuick.getData("Orientation"));
			landQuick.close();
		} else {
			quickIndex.setColumn("ProductSubType", product.getData("ProductSubType"));
			quickIndex.setColumn("LotExclusivity", product.getData("MarketingStatus"));
		}

		// Plan
		if (StringUtils.isNotBlank(planProductID)) {
			GenRow planQuick = getProductQuickIndexHome(planProductID);
			quickIndex.setColumn("PlanID", planQuick.getData("ProductID"));
			quickIndex.setColumn("RangeID", planQuick.getData("RangeID"));
			quickIndex.setColumn("DesignID", planQuick.getData("DesignID"));
			quickIndex.setColumn("BuilderID", planQuick.getData("BuilderID"));
			quickIndex.setColumn("BuilderProductID", product.getData("BuilderProductID"));	 // 13-Aug-2013 NI: Added as required in the Pipeline.
			quickIndex.setColumn("BrandName", planQuick.getData("BrandName"));
			quickIndex.setColumn("RangeName", planQuick.getData("RangeName"));
			quickIndex.setColumn("DesignName", planQuick.getData("DesignName"));
			if (planQuick.getData("HomePlanName").startsWith(planQuick.getData("DesignName")))
				quickIndex.setColumn("HomePlanName", planQuick.getData("HomePlanName"));
			else
				quickIndex.setColumn("HomePlanName", planQuick.getData("HomeName") + " " + planQuick.getData("PlanName"));
			quickIndex.setColumn("Media", planQuick.getData("Media"));
			quickIndex.setColumn("Orientation", planQuick.getData("Orientation"));
			planQuick.close();
		}

		// Facade
		/*
		 * GenRow facade = getFacade(product.getData("ProductID"));
		 * quickIndex.setColumn("FacadeProductID", facade.getString("LinkedProductID"));
		 * quickIndex.setColumn("FacadeName", facade.getString("Name"));
		 */

		// Dimensions
		populateEPDimensions(product.getData("ProductID"), quickIndex);

		// Generic
		quickIndex.setColumn("Active", product.getData("Active"));
		quickIndex.setParameter("DripCost", product.getDouble("DripCost"));
		quickIndex.setParameter("DripResult", product.getDouble("DripResult"));
		quickIndex.setColumn("RegionID", product.getData("RegionID"));
		quickIndex.setColumn("LocationID", product.getData("LocationID"));
		quickIndex.setColumn("PortalFlag", product.getData("PortalFlag"));
		quickIndex.setColumn("ProductStatusID", product.getData("ProductStatusID"));
		quickIndex.setColumn("PublishingStatus", product.getData("PublishingStatus"));
		quickIndex.setColumn("PackageStatus", product.getData("PackageStatus"));
		quickIndex.setColumn("CreatedBy", product.getData("CreatedBy"));
		quickIndex.setColumn("CreatedDate", product.getData("CreatedDate"));
		quickIndex.setColumn("AvailableDate", product.getData("AvailableDate"));
		quickIndex.setColumn("ExpiryDate", product.getData("ExpiryDate"));
		quickIndex.setColumn("PreAvailableDate", product.getData("PreAvailableDate"));
		quickIndex.setColumn("CampaignID", product.getData("CampaignID"));
		quickIndex.setColumn("CopiedFromID", product.getData("CopiedFromID"));		

		quickIndex.setParameter("TotalCost", product.getDouble("TotalCost"));

		if (description != null) {
			description.clear();
			description.setColumn("ProductDescriptionID", quickIndex.getString("QuickIndexID"));
			description.setColumn("ProductID", product.getData("ProductID"));
			description.setColumn("PublishedFor", "Website");
			if (product.isSet("PublishDescription"))  description.setParameter("Description", evaluateHeadline(product, product.getString("PublishDescription")));
			else if ("true".equals(InitServlet.getSystemParam("RPM-UseDescription"))) description.setParameter("Description", evaluateHeadline(product, product.getString("Description")));
			description.setParameter("HeadLine", evaluateHeadline(product, product.getString("Summary")));
		}

		if (publish != null) {
			publish.clear();
			convertPublishing(quickIndex, publish);
		}

		return true;
	}

	public void populateEPDimensions(String productID, GenRow quick) {
		boolean hasValues = false;
		GenRow home = new GenRow();
		home.setConnection(getConnection());
		home.setParameter("ProductID", productID);
		home.setTableSpec("properties/HomeDetails");

		GenRow check = new GenRow();
		check.setConnection(getConnection());
		check.setParameter("ProductID", productID);
		check.setTableSpec("properties/HomeDetails");
		check.setParameter("-select0","*");
		check.doAction("selectfirst");

		home.putAllDataAsParameters(check);

		TableSpec detailsSpec = SpecManager.getTableSpec("properties/HomeDetails");

		String spec = "ProductPlanViewDisplay";
		Map<String, String> map = getDetailsMap(spec);
		HashMap<String, String> dimensions = getDimensions(con, productID, "HomePlanDetails", "HomePlan");

		for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
			if (map.containsKey(detailsSpec.getFieldName(cs))) {
				if (dimensions.containsKey(map.get(detailsSpec.getFieldName(cs)))) {
					home.setParameter(detailsSpec.getFieldName(cs), dimensions.get(map.get(detailsSpec.getFieldName(cs))));
					hasValues = true;
				}
			}
		}

		spec = "ProductLotViewDisplay";
		map = getDetailsMap(spec);
		dimensions = getDimensions(con, productID, "LandDetails", "Land");

		for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
			if (map.containsKey(detailsSpec.getFieldName(cs))) {
				if (dimensions.containsKey(map.get(detailsSpec.getFieldName(cs)))) {
					home.setParameter(detailsSpec.getFieldName(cs), dimensions.get(map.get(detailsSpec.getFieldName(cs))));
					hasValues = true;
				}
			}
		}

		spec = "ProductExistingPropertyViewDisplay";
		map = getDetailsMap(spec);
		dimensions = getDimensions(con, productID, "%", "ExistingProperty");

		for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
			if (map.containsKey(detailsSpec.getFieldName(cs))) {
				if (dimensions.containsKey(map.get(detailsSpec.getFieldName(cs)))) {
					home.setParameter(detailsSpec.getFieldName(cs), dimensions.get(map.get(detailsSpec.getFieldName(cs))));
					hasValues = true;
				}
			}
		}

		home.setParameter("ProductID", productID);
		if (hasValues) {
			try {
				home.doAction("insert");
			} catch (Exception e) {
				home.doAction("update");
			}

			if (home.getString("LotWidth").length() > 0)
				quick.setParameter("HomeWidth", home.getString("LotWidth"));
			if (home.getString("LotDepth").length() > 0)
				quick.setParameter("HomeDepth", home.getString("LotDepth"));
			if (home.getString("HomeSize").length() > 0)
				quick.setParameter("HomeArea", home.getString("HomeSize"));
			if (home.getString("Bedrooms").length() > 0)
				quick.setParameter("Bedrooms", home.getString("Bedrooms"));
			if (home.getString("Bathrooms").length() > 0)
				quick.setParameter("Bathrooms", home.getString("Bathrooms"));
			if (home.getString("Study").length() > 0)
				quick.setParameter("Study", home.getString("Study"));
			if (home.getString("CarParks").length() > 0)
				quick.setParameter("CarParks", home.getString("CarParks"));
			if (home.getString("LotWidth").length() > 0)
				quick.setParameter("LotWidth", home.getString("LotWidth"));
			if (home.getString("LotDepth").length() > 0)
				quick.setParameter("LotDepth", home.getString("LotDepth"));
			if (home.getString("Area").length() > 0)
				quick.setParameter("LotArea", home.getString("Area"));
			if (home.getString("CanFitOnDepth").length() > 0)
				quick.setParameter("CanFitOnDepth", home.getString("CanFitOnDepth"));
			if (home.getString("CanFitOnFrontage").length() > 0)
				quick.setParameter("CanFitOnFrontage", home.getString("CanFitOnFrontage"));
			if (home.getString("InternalArea").length() > 0)
				quick.setParameter("InternalArea", home.getString("InternalArea"));
			if (home.getString("ExternalArea").length() > 0)
				quick.setParameter("ExternalArea", home.getString("ExternalArea"));
		}

		dimensions = getDimensions(con, productID, "Existing Property", "ExistingProperty");

		quick.setParameter("LotPrice", dimensions.get("ExistingProperty.Lot Cost.NumberOf"));
		quick.setParameter("HomePrice", dimensions.get("ExistingProperty.Home Cost.NumberOf"));

	}

	public Map<String, String> getDetailsMap(String spec) {
		Map<String, String> map = new HashMap<String, String>();

		try {
			DisplaySpec dspecLot = SpecManager.getDisplaySpec(spec);

			String[] names = dspecLot.getItemNames();
			for (int d = 0; d < names.length; ++d) {
				map.put(names[d], dspecLot.getLocalItemFieldName(names[d]));

			}
		} catch (Exception e) {}
		return map;
	}

	private HashMap<String, String> getDimensions(Connection con, String productid, String productType, String specKey) {
		HashMap<String, String> dimMap = new HashMap<String, String>();
		if (productid == null || productid.length() == 0 || productType == null || productType.length() == 0)
			return dimMap;

		String ignorelist = "ProductDetailsID,ProductID,CopiedFromProductID,ProductType,DetailsType,SortOrder";

		TableSpec detailsSpec = SpecManager.getTableSpec("properties/ProductDetails");

		GenRow details = new GenRow();
		details.setTableSpec("properties/ProductDetails");
		details.setConnection(con);
		details.setParameter("-select", "*");
		details.setColumn("ProductID", productid);
		details.setColumn("ProductType", productType);
		details.sortBy("SortOrder", 0);
		details.sortBy("DetailsType", 1);
		if (details.getString("ProductID").length() > 0) {
			details.doAction("search");
			details.getResults(true);
		}

		while (details.getNext()) {
			for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
				if (ignorelist.indexOf(detailsSpec.getFieldName(cs)) == -1) {
					dimMap.put((specKey + "." + details.getString("DetailsType") + "." + detailsSpec.getFieldName(cs)), details.getString(detailsSpec.getFieldName(cs)));
				}
			}
		}
		details.close();
		return dimMap;

	}

	private GenRow getFacade(String productid) {
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("ProductLinkedProductView");
		row.setColumn("ProductID", productid);
		row.setColumn("ProductProductsLinkedProduct.ProductType", "Home Facade");
		if (productid != null && productid.length() != 0) {
			row.doAction("selectfirst");
		}
		return row;
	}

	private void cleanQuickIndex() {
		Object[] keys = deleted.toArray();

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		row.setConnection(getConnection());
		row.setParameter("-select0", "QuickIndexID");

		GenRow blah = new GenRow();
		blah.setTableSpec("properties/ProductQuickIndex");
		blah.setConnection(getConnection());

		GenRow desc = new GenRow();
		desc.setTableSpec("properties/ProductDescriptions");
		desc.setConnection(getConnection());

		GenRow pub = new GenRow();
		pub.setTableSpec("properties/ProductPublishingIndex");
		pub.setConnection(getConnection());

		GenRow pp = new GenRow();
		pp.setTableSpec("ProductProducts");
		pp.setConnection(getConnection());

		logger.warn("Cleaning {} Items for QuickIndex",keys.length);

		int t = 0;

		for (int s = 0; s < keys.length; ++s) {
			row.setParameter("ProductID", keys[s].toString());
			row.doAction("search");

			row.getResults();

			int i = 0;

			while (row.getNext()) {
				blah.setParameter("QuickIndexID", row.getString("QuickIndexID"));
				blah.doAction("delete");
				desc.setParameter("ProductDescriptionID", row.getString("QuickIndexID"));
				desc.doAction("delete");
				pub.setParameter("ProductPublishingIndexID", row.getString("QuickIndexID"));
				pub.doAction("delete");
				++i;
				++t;
			}

			logger.trace("Cleaning {} Entries for {}",i,keys[s].toString());

			pp.setParameter("ON-ProductID|1", keys[s].toString());
			pp.setParameter("ON-ChildProductID|1", keys[s].toString());
			pp.doAction("deleteall");
		}

		logger.warn("Cleaning {} Total Entries for QuickIndex",t);

		row.close();
	}

	private String cleanNumbers(String data) {
		if (data == null || data.length() == 0)
			return "";

		return data.replaceAll("[^0-9\\.\\-]", "");
	}

	/**
	 * This method will update all packages on a lot
	 * 
	 * See {@link HouseAndLandManager}
	 * 
	 * @deprecated  
	 * @param products
	 */
	@Deprecated
	private void processLandStatuses(GenRow products) {
		String available = InitServlet.getSystemParams().getProperty(
				"ProductAvailableStatus");
		String expired = InitServlet.getSystemParams().getProperty(
				"ProductExpiredStatus");
		String external = InitServlet.getSystemParams().getProperty(
				"ProductExternalSoldStatus");
		String internal = InitServlet.getSystemParams().getProperty(
				"ProductInternalSoldStatus");
		String packageExternal = InitServlet.getSystemParams().getProperty(
				"ProductExternalPackageStatus");

		if (available == null || available.length() == 0)
			return;

		boolean active = false;
		String setStatus = internal;
		String setAvailable = available;
		// if the current lot status is Available then do nothing
		if (available.equals(products.getString("CurrentStatusID"))) {
			if (internal == null || internal.length() == 0)
				return;

			setStatus = available;
			setAvailable = internal;

			active = true;
		} else {
			// if it's expired, then do nothing, this is handled by the nightly
			// expire
			if (expired != null
					&& expired.equals(products.getString("CurrentStatusID")))
				return;

			if (external != null
					&& external.equals(products.getString("CurrentStatusID")))
				setStatus = packageExternal;
		}
		if (setStatus != null && setStatus.length() > 0) {
			GenRow row = new GenRow();
			row.setViewSpec("ProductProductView");
			row.setConnection(getConnection());
			row.setParameter("LinkedProductID", products.getString("ProductID"));
			row.setParameter("Active", !active ? "Y" : "N");
			row.setParameter("ProductType", "House and Land+Existing Property");
			row.setParameter("CurrentStatusID", setAvailable);
			row.setParameter("ProductProductsProduct.Active", !active ? "Y"
					: "N");
			row.setParameter("ProductProductsProduct.ProductType",
					"House and Land");
			row.setParameter(
					"ProductProductsProduct.ProductCurrentStatus.ProductStatusList.StatusID",
					setAvailable);
			row.doAction("search");
			try {
				row.getResults();

				GenRow update = new GenRow();
				update.setTableSpec("Products");
				update.setConnection(getConnection());

				while (row.getNext()) {
					update.clear();
					update.setParameter("ProductID", row.getString("ProductID"));
					update.setParameter("Active", active ? "Y" : "N");
					update.setParameter(
							"ProductStatusID",
							insertProductStatus(row.getString("ProductID"),
									setStatus));
					update.setParameter("ModifiedDate", "NOW");
					update.doAction("update");

					expireProcesses(
							row.getString("ProductID"),
							active ? "This process has been reactivated"
									: "Product Sold in different Process, this Process is Locked",
									active);
					expireOrder(row.getString("ProductID"), active);

					doIndexRefreshIndex(row.getString("ProductID"),row.getData("ProductType"));
				}
			} finally {
				row.close();
			}

		}
	}

	protected GenRow getProductQuickIndexLand(String productID) {
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		row.setConnection(getConnection());
		row.setParameter("-select0", "*");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");

		if (!row.isSuccessful() || !row.isSet("Name")) {
			row.clear();
			row.setViewSpec("properties/ProductLotView");
			row.setConnection(getConnection());
			row.setParameter("ProductID", productID);
			row.doAction("selectfirst");
		}

		return row;
	}

	protected GenRow getProductQuickIndexHome(String productID) {
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		row.setConnection(getConnection());
		row.setParameter("-select0", "*");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");

		if (!row.isSuccessful() || !row.isSet("Name")) {
			row.clear();
			row.setViewSpec("properties/ProductPlanView");
			row.setConnection(getConnection());
			row.setParameter("ProductID", productID);
			row.doAction("selectfirst");
		}

		return row;
	}

	public PropertyBuilderList getBuilderList() {
		waitOnData();
		return builderList;
	}

	public PropertyDesignList getDesignList() {
		waitOnData();
		return designList;
	}

	public PropertyDeveloperList getDeveloperList() {
		waitOnData();
		return developerList;
	}

	public PropertyEstateList getEstateList() {
		waitOnData();
		return estateList;
	}

	public PropertyEstateList getCustomEstateList() {
		waitOnData();
		return estateCustomList;
	}

	/*
	 * public PropertyLandList getLandList() { waitOnData(); return landList; }
	 * 
	 * public PropertyPlanList getPlanList() { waitOnData(); return planList; }
	 */
	public PropertyRangeList getRangeList() {
		waitOnData();
		return rangeList;
	}

	/*
	 * public PropertyStageList getStageList() { waitOnData(); return stageList;
	 * }
	 */
	public String MaxTotal() {
		/*
		 * GenRow row = new GenRow(); row.setViewSpec("ProductView");
		 * row.setParameter("ProductType", "House and Land");
		 * row.sortBy("TotalCost", 0); row.sortOrder("DESC", 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getString("TotalCost").length() == 0 ||
		 * row.getDouble("TotalCost") == 0) return "1000";
		 * 
		 * return "" + (row.getDouble("TotalCost") / 1000);
		 */

		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(getConnection());
		row.setParameter("-select1", "max(TotalCost) as maxTotalCost");
		row.setParameter("-select2", "max(DripResult) as maxDripResult");
		// we just want the total for the House and Land
		row.setParameter("ProductType", "House and Land");

		if (logger.isTraceEnabled()) {
			row.doAction(GenerationKeys.SEARCH);
			logger.trace(row.getStatement());
		}
		row.doAction(GenerationKeys.SELECTFIRST);

		if (logger.isDebugEnabled())
			logger.debug("MaxTotal::DB maxtotal={}", row.getString("maxTotalCost"));

		double t = 0, d = 0;

		if (row.getString("maxTotalCost").length() != 0) t = (row.getDouble("maxTotalCost") / 1000) * 1.1;
		if (row.getString("maxDripResult").length() != 0) d = (row.getDouble("maxDripResult") / 1000) * 1.1;

		maxTotal = "" + Math.max(t,d);

		if (t == 0 && d == 0) maxTotal = "999";

		if (logger.isDebugEnabled())
			logger.debug("MaxTotal::Ret maxtotal={}", maxTotal);

		return maxTotal;
	}

	public String MaxEPTotal() {
		/*
		 * GenRow row = new GenRow(); row.setViewSpec("ProductView");
		 * row.setParameter("ProductType", "House and Land");
		 * row.sortBy("TotalCost", 0); row.sortOrder("DESC", 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getString("TotalCost").length() == 0 ||
		 * row.getDouble("TotalCost") == 0) return "1000";
		 * 
		 * return "" + (row.getDouble("TotalCost") / 1000);
		 */

		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(getConnection());
		row.setParameter("-select1", "max(TotalCost) as maxTotalCost");
		row.setParameter("-select2", "max(DripResult) as maxDripResult");
		// we just want the total for the House and Land
		row.setParameter("ProductType", "Existing Property");

		if (logger.isTraceEnabled()) {
			row.doAction(GenerationKeys.SEARCH);
			logger.trace(row.getStatement());
		}
		row.doAction(GenerationKeys.SELECTFIRST);

		if (logger.isDebugEnabled())
			logger.debug("MaxTotal::DB maxtotal={}", row.getString("maxTotalCost"));

		double t = 0, d = 0;

		if (row.getString("maxTotalCost").length() != 0) t = (row.getDouble("maxTotalCost") / 1000) * 1.1;
		if (row.getString("maxDripResult").length() != 0) d = (row.getDouble("maxDripResult") / 1000) * 1.1;

		maxEPTotal = "" + Math.max(t,d);

		if (t == 0 && d == 0) maxEPTotal = "999";

		if (logger.isDebugEnabled())
			logger.debug("MaxEPTotal::Ret maxEPtotal={}", maxEPTotal);

		return maxEPTotal;
	}

	/**
	 * @deprecated - use loadLotBounds
	 */
	public String MaxLotWidth() {
		/*
		 * GenRow row = new GenRow();
		 * row.setViewSpec("properties/ProductLotView");
		 * row.setParameter("ProductType", "Land"); row.sortBy("LotWidth", 0);
		 * row.sortOrder("DESC", 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getString("LotWidth").length() == 0 ||
		 * row.getDouble("LotWidth") == 0) return "30";
		 * 
		 * return row.getString("LotWidth");
		 */
		loadLotBounds();
		return maxLotWidth;
	}

	public void loadLotBounds() {
		logger.debug("loadLotBounds()");
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setTableSpec("Products");
		row.setParameter("ProductType", "Land");
		row.setParameter("-join1", "ProductHomeDetails");
		row.setParameter("-select1",
				"max(ProductHomeDetails.LotWidth) as MaxLotWidth");
		row.setParameter("-select2",
				"max(ProductHomeDetails.LotDepth) as MaxLotDepth");
		row.setParameter("-select3",
				"min(ProductHomeDetails.Area) as MinLotArea");
		row.setParameter("-select4",
				"max(ProductHomeDetails.Area) as MaxLotArea");

		if (logger.isTraceEnabled()) {
			row.doAction(GenerationKeys.SEARCH);
			logger.trace(row.getStatement());
		}
		row.doAction(GenerationKeys.SELECTFIRST);

		if (logger.isDebugEnabled())
			logger.debug(
					"loadLotBounds::DB maxLotWidth={}, maxLotDepth={}, minLotArea={}, maxLotArea={}",
					new String[] { row.getString("MaxLotWidth"),
							row.getString("MaxLotDepth"),
							row.getString("MinLotArea"),
							row.getString("MaxLotArea") });

		maxLotWidth = (row.getString("MaxLotWidth").length() == 0 || row
				.getDouble("MaxLotWidth") == 0) ? "30" : row
						.getString("MaxLotWidth");
		maxLotDepth = (row.getString("MaxLotDepth").length() == 0 || row
				.getDouble("MaxLotDepth") == 0) ? "60" : row
						.getString("MaxLotDepth");
		minLotArea = (row.getString("MinLotArea").length() == 0 || row
				.getDouble("MinLotArea") == 0) ? "0" : row
						.getString("MinLotArea");
		maxLotArea = (row.getString("MaxLotArea").length() == 0 || row
				.getDouble("MaxLotArea") == 0) ? "1000" : row
						.getString("MaxLotArea");

		row.clear();
		row.setParameter("-select0", "ExtraSearch");
		row.setParameter("ProductType", "Land");
		row.setParameter("-groupby0", "ExtraSearch");
		row.sortBy("ExtraSearch", 0);
		row.doAction("search");

		row.getResults();

		while (row.getNext()) {
			if (row.isSet("ExtraSearch")) extraSearch.add(row.getString("ExtraSearch"));
		}

		row.close();

		if (logger.isDebugEnabled())
			logger.debug(
					"loadLotBounds::Ret maxLotWidth={}, maxLotDepth={}, minLotArea={}, maxLotArea={}",
					new String[] { maxLotWidth, maxLotDepth, minLotArea,
							maxLotArea });
	}

	public void loadApartmentBounds() {
		logger.debug("loadApartmentBounds()");
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setTableSpec("Products");
		row.setParameter("ProductType", "Apartment");
		row.setParameter("-join1", "ProductHomeDetails");
		row.setParameter("-select1",
				"min(ProductHomeDetails.Area) as MinAptArea");
		row.setParameter("-select2",
				"max(ProductHomeDetails.Area) as MaxAptArea");

		if (logger.isTraceEnabled()) {
			row.doAction(GenerationKeys.SEARCH);
			logger.trace(row.getStatement());
		}
		row.doAction(GenerationKeys.SELECTFIRST);

		if (logger.isDebugEnabled())
			logger.debug(
					"loadApartmentBounds::DB minAptArea={}, maxAptArea={}",
					row.getString("MinAptArea"), row.getString("MaxAptArea"));

		minApartmentArea = (row.getString("MinAptArea").length() == 0 || row
				.getDouble("MinAptArea") == 0) ? "0" : row
						.getString("MinAptArea");
		maxApartmentArea = (row.getString("MaxAptArea").length() == 0 || row
				.getDouble("MaxAptArea") == 0) ? "1000" : row
						.getString("MaxAptArea");

		if (logger.isDebugEnabled())
			logger.debug(
					"loadApartmentBounds::DB minAptArea={}, maxAptArea={}",
					minApartmentArea, maxApartmentArea);
	}

	public void loadHouseBounds() {
		logger.debug("loadHouseBounds()");
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setTableSpec("Products");
		row.setParameter("ProductType", "Home Plan");
		row.setParameter("-join1", "ProductHomeDetails");
		row.setParameter("-select1",
				"max(ProductHomeDetails.LotWidth) as MaxHomeWidth");
		row.setParameter("-select2",
				"max(ProductHomeDetails.HomeSize) as MaxHomeSize");
		row.setParameter("-select3",
				"min(ProductHomeDetails.HomeSize) as MinHomeSize");

		if (logger.isTraceEnabled()) {
			row.doAction(GenerationKeys.SEARCH);
			logger.trace(row.getStatement());
		}
		row.doAction(GenerationKeys.SELECTFIRST);

		if (logger.isDebugEnabled())
			logger.debug(
					"loadHouseBounds::DB maxHomeWidth={}, maxHomeSize={}, minHomeSize={}",
					new String[] { row.getString("MaxHomeWidth"),
							row.getString("MaxHomeSize"),
							row.getString("MinHomeSize") });

		maxHomeWidth = (row.getString("MaxHomeWidth").length() == 0 || row
				.getDouble("MaxHomeWidth") == 0) ? "30" : row
						.getString("MaxHomeWidth");
		maxHomeSize = (row.getString("MaxHomeSize").length() == 0 || row
				.getDouble("MaxHomeSize") == 0) ? "500" : row
						.getString("MaxHomeSize");
		minHomeSize = (row.getString("MinHomeSize").length() == 0 || row
				.getDouble("MinHomeSize") == 0) ? "0" : row
						.getString("MinHomeSize");

		if (logger.isDebugEnabled())
			logger.debug(
					"loadHouseBounds::DB maxHomeWidth={}, maxHomeSize={}, minHomeSize={}",
					new String[] { maxHomeWidth, maxHomeSize, minHomeSize });
	}

	/**
	 * @deprecated - use loadHouseBounds
	 */
	public String MaxHouseWidth() {
		/*
		 * GenRow row = new GenRow();
		 * row.setViewSpec("properties/ProductPlanView");
		 * row.setParameter("ProductType", "Home Plan"); row.sortBy("LotWidth",
		 * 0); row.sortOrder("DESC", 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getString("LotWidth").length() == 0 ||
		 * row.getDouble("LotWidth") == 0) return "30";
		 * 
		 * return row.getString("LotWidth");
		 */
		loadHouseBounds();
		return maxHomeSize;
	}

	/**
	 * @deprecated - use loadHouseBounds
	 */
	public String MaxHomeSize() {
		/*
		 * GenRow row = new GenRow();
		 * row.setViewSpec("properties/ProductPlanView");
		 * row.setParameter("ProductType", "Home Plan"); row.sortBy("HomeSize",
		 * 0); row.sortOrder("DESC", 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getString("HomeSize").length() == 0 ||
		 * row.getDouble("HomeSize") == 0) return "500";
		 * 
		 * return row.getString("HomeSize");
		 */
		loadHouseBounds();
		return maxHomeSize;
	}

	/**
	 * @deprecated - use loadHouseBounds
	 */
	public String MinHomeSize() {
		/*
		 * GenRow row = new GenRow();
		 * row.setViewSpec("properties/ProductPlanView");
		 * row.setParameter("ProductType", "Home Plan"); row.sortBy("HomeSize",
		 * 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getData("HomeSize").length() == 0 ||
		 * row.getDouble("HomeSize") == 0) return "0";
		 * 
		 * return row.getData("HomeSize");
		 */
		loadHouseBounds();
		return minHomeSize;
	}

	/**
	 * @deprecated - use loadLotBounds
	 */
	public String MaxLotDepth() {
		/*
		 * GenRow row = new GenRow();
		 * row.setViewSpec("properties/ProductLotView");
		 * row.setParameter("ProductType", "Land"); row.sortBy("LotDepth", 0);
		 * row.sortOrder("DESC", 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getString("LotDepth").length() == 0 ||
		 * row.getDouble("LotDepth") == 0) return "30";
		 * 
		 * return row.getString("LotDepth");
		 */
		loadLotBounds();
		return maxLotDepth;
	}

	/**
	 * @deprecated - use loadLotBounds
	 */
	public String MinLotArea() {
		/*
		 * GenRow row = new GenRow();
		 * row.setViewSpec("properties/ProductLotView");
		 * row.setParameter("ProductType", "Land"); row.sortBy("Area", 0);
		 * row.sortOrder("ASC", 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getString("Area").length() == 0 || row.getDouble("Area") ==
		 * 0) return "0";
		 * 
		 * return row.getString("Area");
		 */
		loadLotBounds();
		return minLotArea;
	}

	/**
	 * @deprecated - use loadLotBounds
	 */
	public String MaxLotArea() {
		/*
		 * GenRow row = new GenRow();
		 * row.setViewSpec("properties/ProductLotView");
		 * row.setParameter("ProductType", "Land"); row.sortBy("Area", 0);
		 * row.sortOrder("DESC", 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getString("Area").length() == 0 || row.getDouble("Area") ==
		 * 0) return "1000";
		 * 
		 * return row.getString("Area");
		 */
		loadLotBounds();
		return maxLotArea;
	}

	/**
	 * @deprecated - use loadApartmentBounds
	 */
	public String MinApartmentArea() {
		/*
		 * GenRow row = new GenRow();
		 * row.setViewSpec("properties/ProductApartmentView");
		 * row.setParameter("ProductType", "Apartment"); row.sortBy("Area", 0);
		 * row.sortOrder("ASC", 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getString("Area").length() == 0 || row.getDouble("Area") ==
		 * 0) return "0";
		 * 
		 * return row.getString("Area");
		 */
		loadApartmentBounds();
		return minApartmentArea;
	}

	/**
	 * @deprecated - use loadApartmentBounds
	 */
	public String MaxApartmentArea() {
		/*
		 * GenRow row = new GenRow();
		 * row.setViewSpec("properties/ProductApartmentView");
		 * row.setParameter("ProductType", "Apartment"); row.sortBy("Area", 0);
		 * row.sortOrder("DESC", 0);
		 * 
		 * row.doAction("selectfirst");
		 * 
		 * if (row.getString("Area").length() == 0 || row.getDouble("Area") ==
		 * 0) return "1000";
		 * 
		 * return row.getString("Area");
		 */
		loadApartmentBounds();
		return maxApartmentArea;
	}

	// we don't want to get incompleat data, so if there is a load, then wait
	protected void waitOnData() {
		int c = 0;
		while (++c < 40 && isGettingData) {
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				break;
			}
		}
		if (c == 40) {
			logger.warn("RPMTask failed waitOnData, null pointer exceptions are possible");
		}
	}

	public String getMaxHomeSize() {
		waitOnData();
		return maxHomeSize;
	}

	public String getMinHomeSize() {
		waitOnData();
		return minHomeSize;
	}

	public String getMaxHomeWidth() {
		waitOnData();
		return maxHomeWidth;
	}

	public String getMaxLotWidth() {
		waitOnData();
		return maxLotWidth;
	}

	public String getMaxTotal() {
		waitOnData();
		return maxTotal;
	}

	public String getMaxEPTotal() {
		waitOnData();
		return maxEPTotal;
	}

	public String getMaxLotDepth() {
		waitOnData();
		return maxLotDepth;
	}

	public String getMaxLotArea() {
		waitOnData();
		return maxLotArea;
	}

	public String getMinLotArea() {
		waitOnData();
		return minLotArea;
	}

	public String getMaxApartmentArea() {
		waitOnData();
		return maxApartmentArea;
	}

	public String getMinApartmentArea() {
		waitOnData();
		return minApartmentArea;
	}

	public PropertyBuildingDeveloperList getBuildingDeveloperList() {
		return buildingDeveloperList;
	}

	public PropertyBuildingList getBuildingList() {
		return buildingList;
	}

	/*
	 * public PropertyApartmentList getApartmentList() { return apartmentList; }
	 * 
	 * 
	 * public PropertyBuildingStageList getBuildingStageList() { return
	 * buildingStageList; }
	 */

	private void insertCurrentDates(String param) {
		InitServlet.getSystemParams().put(param, ddmmyyyy.format(new Date()));

		GenRow row = new GenRow();
		row.setTableSpec("SystemParameters");
		row.setParameter("-select0", "SystemParameterID");
		row.setParameter("Name", param);
		row.doAction("selectfirst");

		if (row.isSuccessful()) {
			String sysParamID = row.getString("SystemParameterID");
			row.clear();
			row.setParameter("SystemParameterID", sysParamID);
			row.setParameter("Value", ddmmyyyy.format(new Date()));
			row.setParameter("Type", "Text");
			row.doAction("update");
		} else {
			row.setParameter("SystemParameterID", KeyMaker.generate());
			row.setParameter("Value", ddmmyyyy.format(new Date()));
			row.setParameter("Type", "Text");
			row.doAction("insert");
		}
	}

	private String getCurrentDates(String param) {
		GenRow row = new GenRow();
		row.setTableSpec("SystemParameters");
		row.setParameter("-select0", "Value");
		row.setParameter("Name", param);
		row.doAction("selectfirst");

		return row.getString("Value");
	}

	public boolean isValidTime(Date now) {

		if (now == null)
			return false;

		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();

		start.setTime(new Date());
		end.setTime(new Date());

		try {
			String startHr = "";
			String startMin = "00";

			if (startTime.indexOf(":") >= 0) {
				startHr = startTime.substring(0, startTime.indexOf(":"));
				startMin = startTime.substring(startTime.indexOf(":") + 1);
			} else {
				startHr = dayOfMonth;
			}

			if (dayOfMonth != null && dayOfMonth.length() > 0) {
				start.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayOfMonth));
			} else if (dayOfWeek != null && dayOfWeek.length() > 0) {
				start.set(Calendar.DAY_OF_WEEK, Integer.parseInt(dayOfWeek));
			}

			start.set(Calendar.HOUR_OF_DAY, Integer.parseInt(startHr));
			start.set(Calendar.MINUTE, Integer.parseInt(startMin));
			start.set(Calendar.SECOND, 0);
		} catch (Exception e) {
			return false;
		}

		try {
			String endHr = "";
			String endMin = "00";

			if (endTime.indexOf(":") >= 0) {
				endHr = endTime.substring(0, endTime.indexOf(":"));
				endMin = endTime.substring(endTime.indexOf(":") + 1);
			} else {
				endMin = endTime;
			}

			if (dayOfMonth != null && dayOfMonth.length() > 0) {
				end.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayOfMonth));
			} else if (dayOfWeek != null && dayOfWeek.length() > 0) {
				end.set(Calendar.DAY_OF_WEEK, Integer.parseInt(dayOfWeek));
			}

			end.set(Calendar.HOUR_OF_DAY, Integer.parseInt(endHr));
			end.set(Calendar.MINUTE, Integer.parseInt(endMin));
			end.set(Calendar.SECOND, 0);
		} catch (Exception e) {
			return false;
		}

		// this so you can use 23:00 to 03:00
		if (end.before(start))
			end.add(Calendar.DAY_OF_YEAR, 1);

		System.out.println("RPM Time Check " + start.getTime().toString()
				+ " < " + now.toString() + " < " + end.getTime().toString()
				+ " = "
				+ (!now.before(start.getTime()) && !now.after(end.getTime())));

		if (!now.before(start.getTime()) && !now.after(end.getTime()))
			return true;

		return false;
	}

	@Override
	public Connection getConnection() {
		/*
		 * We will allow all GernRows to get their own connections try { if (con
		 * == null || con.isClosed()) { ctx = new InitialContext();
		 * 
		 * DataSource ds = null; try { dbconn = (String)
		 * InitServlet.getConfig().
		 * getServletContext().getInitParameter("sqlJndiName"); Context
		 * envContext = (Context) ctx.lookup(ActionBean.subcontextname); ds =
		 * (DataSource) envContext.lookup(dbconn); } catch (Exception e) { } if
		 * (ds == null) { ds = (DataSource) ctx.lookup(dbconn); } con =
		 * ds.getConnection(); } } catch (Exception e) { return null; }
		 * 
		 * return con;
		 */
		return null;
	}

	public ArrayList<String> getExtraSearch() {
		return extraSearch;
	}

}