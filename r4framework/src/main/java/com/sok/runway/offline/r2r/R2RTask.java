package com.sok.runway.offline.r2r;

import java.io.File;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.servlet.ServletConfig;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.MailerBean;
import com.sok.framework.RowBean;
import com.sok.framework.RunwayUtil;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;
import com.sok.runway.email.EmailerMap;
import com.sok.runway.offline.AbstractDaemonWorker;

public class R2RTask extends AbstractDaemonWorker {
	private static final Logger logger = LoggerFactory.getLogger(R2RTask.class);

	private String alerthtml = "generic/generic.vt";
	private String alerttext = "generic/generic_text.vt";
	private static final String DEFAULT_VELOCITY_PATH = "/cms/emails/statusalerts/";

	SimpleDateFormat yyyymmdd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	protected String to = "";
	private boolean isRunning = false;
	protected String emailalertsurlhome;

	private Vector<QueueItem> queue = new Vector<QueueItem>();

	public R2RTask(String configfilepath) {
		super(configfilepath);
	}

	public void checkConfig() {
		config = R2RConfig.getR2RConfig(configfilepath);
	}

	protected void init() {
		if (!isRunning) {
			isRunning = true;
			message = new StringBuffer();
			queue = new Vector<QueueItem>();
			super.init();

			loadTranmissions();
			loadQueue();
			if (queue.size() > 0) {
				packageProducts();
				sendProducts();
			} else if (debug) {
				log("There was no packages to send");
			}

			if (super.message.length() > 0 && "true".equals(InitServlet.getSystemParam("Publish-DebugEnabled"))) {
				sendUserEmail(to);
			}

			isRunning = false;
		} else {
			log("Another instance seems to be running");
		}
	}

	public void debugInit(boolean debug) {
		super.debugInit(debug);
	}

	protected void loadQueue() {
		GenRow row = new GenRow();
		row.setTableSpec("ProductTransmissionQueue");
		row.setParameter("-select0", "*");
		row.doAction("search");

		row.getResults();

		while (row.getNext()) {
			queue.add(new QueueItem(row));
		}

		row.close();
	}

	protected void loadTranmissions() {
		loadTranmissions(null);
	}

	protected void loadOutOfTranmissions() {
		loadOutOfTranmissions(null);
	}

	protected void loadTranmissions(String transmissionID) {
		log("Starting loadTranmissions()");
		GenRow search = new GenRow();
		search.setConnection(getConnection());
		search.setViewSpec("R2RTransmissionView");
		search.setParameter("Active", "Y");
		search.setParameter("ProductID", "!NULL;!EMPTY");
		//search.setParameter("Name", "!NULL;!EMPTY");
		search.setParameter("R2RTransmissionProduct.ModifiedDate", "> ProductTransmission.TransmissionDate");
		
		if (StringUtils.isNotBlank(transmissionID)) {
			search.setParameter("TransmissionID", transmissionID);
		}

		search.doAction("search");

		log("loadTranmissions() SQL Search");
		log(search.getStatement());

		search.getResults();

		int added = 0;
		while (search.getNext()) {
			if (StringUtils.isNotBlank(search.getString("ProductID"))) {
				this.productMessage = new StringBuffer();
				//if (checkTransmissionEntries(search.getString("ProductID"))) {
					QueueItem q = new QueueItem(search.getString("ProductID"));
					if (q.save()) {
						++added;
						log("added for transmission : {} to queue. RF = {}; TS = {}", search.getString("ProductID"));
						// log("added for transmission : " + search.getString("ProductID") + " to queue. RF = " + search.getString("RefreshFeed") + "; TS = " + search.getString("TransmissionSuccess").length());
						insertLog(search.getData("TransmissionID"), productMessage, "Pick", true);
					} else {
						log("failed addin transmission : {} to queue. P = {}", search.getString("TransmissionID"), search.getString("ProductID"));
					}
				//}
				log("Found a record with blank ProductID : t = {}", search.getData("TransmissionID"));
			}
		}

		log("added {}", String.valueOf(added));

		search.close();

	}

	protected void loadOutOfTranmissions(String transmissionID) {
		String now = RunwayUtil.getCurDateInServerTimeZone(ddmmyyyy);

		GenRow search = new GenRow();
		search.setConnection(getConnection());
		search.setViewSpec("ProductTransmissionView");
		// search.addParameter("EndDate|1", "<NOW");
		search.addParameter("EndDate|1", "<" + now);
		search.addParameter("StartDate|2", "NULL");
		// search.addParameter("StartDate|2", "<NOW");
		search.addParameter("StartDate|2", "<" + now);
		search.setParameter("Active", "!N");
		search.setParameter("Name", "!NULL;!EMPTY");

		if (StringUtils.isNotBlank(transmissionID)) {
			search.setParameter("TransmissionID", transmissionID);
		}

		search.doAction("search");

		log("loadOutOfTranmissions() SQL Search");
		log(search.getStatement());

		search.getResults();
		int added = 0;
		while (search.getNext()) {
			if (StringUtils.isNotBlank(search.getString("ProductID"))) {
				QueueItem q = new QueueItem(search.getString("ProductID"));

				q.save();

				++added;

				log("added for out of transmission : t = {} p = {}", search.getData("TransmissionID"), search.getString("ProductID"));
			} else {
				log("Found a record with blank ProductID : t = {}", search.getData("TransmissionID"));
			}
		}
		log("added " + added);

		search.close();

	}

	private boolean productModified(String productID, String feedName, String date) {
		PublishMethod pm;

		Date now = RunwayUtil.getCurDateInServerTimeZone();
		log("Current server time is = " + now);
		now.setTime(now.getTime() - (10 * 60 * 1000));

		try {
			pm = PublishMethod.getFromFeedName(feedName);
		} catch (IllegalArgumentException e) {
			log(productID + " has no Provider for " + feedName);
			return false;
		}

		// has the product been linked since the last change transmission?
		if (date != null && date.indexOf(".") > 0)
			date = date.substring(0, date.indexOf("."));
		GenRow products = new GenRow();
		products.setConnection(getConnection());
		products.setViewSpec("ProductView");
		products.setColumn("ProductID", productID);

		products.doAction(GenerationKeys.SELECTFIRST);

		if (!products.isSuccessful() || products.getString("ProductType").length() == 0) {
			log("product missing. Last transmission date = " + date + "; ProductID = " + productID);
			return true;
		}

		try {
			products.setParameter("ModifiedDate^1", ">" + ddmmyyyy.format(yyyymmdd.parse(date)));
		} catch (ParseException e) {
			products.setParameter("ModifiedDate^1", ">" + date);
		}
		products.addParameter("ModifiedDate^1", "<" + ddmmyyyy.format(now));
		products.doAction(GenerationKeys.SELECTFIRST);

		// System.out.println("Product " + products.getStatement());

		if (products.isSuccessful()) {
			log("product modified. Last transmission date = " + date + "; ProductID = " + productID + "; Product modified date = " + products.getData("ModifiedDate"));
			return true;
		}

		// we should not check sub products if the product is a House and Land as we need a recreate to get the modified date.
		if (!"House and Land".equals(products.getString("ProductType"))) {
			// has any sub product been linked since the last change transmission?
			products.clear();
			products.setViewSpec("ProductLinkedProductView");
			products.setColumn("ProductID", productID);
			try {
				products.setParameter("ProductProductsLinkedProduct.ModifiedDate^1", ">" + ddmmyyyy.format(yyyymmdd.parse(date)));
			} catch (ParseException e) {
				products.setParameter("ProductProductsLinkedProduct.ModifiedDate^1", ">" + date);
			}
			products.addParameter("ProductProductsLinkedProduct.ModifiedDate^1", "<" + ddmmyyyy.format(now));
			products.doAction(GenerationKeys.SELECTFIRST);

			if (products.isSuccessful()) {
				log("product modified. Last transmission date = " + date + "; ProductID = " + productID + "; Product modified date = " + products.getData("ModifiedDate"));
				return true;
			}
		}

		GenRow productPublishing = new GenRow();
		productPublishing.setConnection(getConnection());
		productPublishing.setViewSpec("properties/ProductPublishingView");
		productPublishing.setColumn("ProductID", productID);
		productPublishing.setColumn("PublishMethod", pm.getName());
		try {
			productPublishing.setParameter("ModifiedDate^1", ">" + ddmmyyyy.format(yyyymmdd.parse(date)));
		} catch (ParseException e) {
			productPublishing.setParameter("ModifiedDate^1", ">" + date);
		}
		productPublishing.addParameter("ModifiedDate^1", "<" + ddmmyyyy.format(now));
		productPublishing.doAction(GenerationKeys.SELECTFIRST);

		// System.out.println("Linked Product " + products.getStatement());
		log(" ");
		log("ProductPublishing modified query: [   " + productPublishing.getStatement() + "  ]");

		if (StringUtils.isNotBlank(productPublishing.getData("ModifiedDate"))) {
			log("productPublishing modified on " + productPublishing.getData("ModifiedDate"));
		}

		boolean isProductModified = ((products.isSuccessful() && products.getString("LinkedProductID").length() > 0) || (productPublishing.isSuccessful() && productPublishing.getString("ProductPublishingID").length() > 0));
		products.close();
		productPublishing.close();

		log("isProductModified = " + isProductModified);
		return isProductModified;
	}

	/**
	 * Provide a public method to forcibly do a transmission so that they can be tested manually. Force: if true will make sure that the product is also available for Transmision
	 * 
	 * @param transmissionID
	 */
	public void debugTransmission(String transmissionID, String force) {
		debugTransmission(transmissionID, force, "false");
	}

	public void debugTransmission(String transmissionID, String force, String removeOnly) {
		this.queue.clear();

		if ("true".equals(force)) {
			loadTranmissions(transmissionID);
		} else {
			GenRow search = new GenRow();
			search.setConnection(getConnection());
			search.setViewSpec("ProductTransmissionView");
			search.setParameter("TransmissionID", transmissionID);
			if (logger.isTraceEnabled()) {
				search.doAction("search");
				logger.trace(search.getStatement());
			}
			if (search.isSet("TransmissionID"))
				search.doAction(GenerationKeys.SELECTFIRST);
			if (search.isSuccessful()) {
				this.queue.clear();

				QueueItem debugItem = new QueueItem(search.getString("ProductID"));
				debugItem.save();
			} else {
				logger.debug("unable to find transmission with id {} ", transmissionID);
			}
			search.close();
		}

		loadQueue();

		if (queue.size() > 0) {
			packageProducts();
			sendProducts();
		} else if (debug) {
			log("There was no packages to send");
		}
	}

	private void packageProducts() {
		for (QueueItem qi : queue) { // uses iterator
			processQueueItem(qi);
			qi.delete();
		}
	}

	/**
	 * Split out process Item to enable simpler debugging
	 * 
	 * @param queueItem
	 */
	private void processQueueItem(QueueItem queueItem) {

	}

	public void updatePublishing(String publishingID, String status) {
		// update the transmission data
		GenRow entry = new GenRow();
		entry.setTableSpec("properties/ProductPublishing");
		entry.setConnection(getConnection());
		entry.setParameter("ProductPublishingID", publishingID);
		entry.setParameter("PublishStatus", status);
		// entry.setParameter("ModifiedDate","NOW");
		// entry.setParameter("ModifiedBy",systemuserid);
		entry.doAction(GenerationKeys.UPDATE);
		entry.close();
		log("Updating ProductPublishing to (PublishStatus={}, ProductPublishingID={})", new String[] { status, publishingID });
	}

	public void updatePublishedFlag(String publishingID, boolean publishedFlag, String listingID) {
		// If the package has never been published then update first publish date and publishedflag
		GenRow entry = new GenRow();
		entry.setTableSpec("properties/ProductPublishing");
		entry.setConnection(getConnection());
		entry.setParameter("ProductPublishingID", publishingID);

		if (!publishedFlag && StringUtils.isNotBlank(publishingID)) {
			entry.setParameter("PublishedFlag", "Y");
			entry.setParameter("FirstPublishDate", "NOW()");
			log("Maiden Publish! Updating publish record for ProductPublishingID = " + publishingID);
		}

		if (StringUtils.isNotBlank(publishingID)) {
			entry.setParameter("ListingID", listingID);
			entry.doAction(GenerationKeys.UPDATE);
			log("ListingID updated to {} ", listingID);
		}

		entry.close();
	}

	public void logPublishingHistory(String publishingID, String startDate, String endDate, String status, String repUserID) {
		GenRow settingHistory = new GenRow();
		settingHistory.setConnection(getConnection());
		settingHistory.setViewSpec("properties/ProductPublishingHistoryView");
		settingHistory.setParameter("ProductPublishingHistoryID", KeyMaker.generate());
		settingHistory.setParameter("ProductPublishingID", publishingID);

		settingHistory.setParameter("Status", status);
		settingHistory.setParameter("RepUserID", repUserID);
		settingHistory.setParameter("CreatedDate", "NOW");
		settingHistory.setParameter("CreatedBy", "system");
		settingHistory.setParameter("StartDate", startDate);
		settingHistory.setParameter("EndDate", endDate);
		settingHistory.setParameter("ModifiedDate", "NOW");
		settingHistory.setParameter("ModifiedBy", "system");
		settingHistory.doAction(GenerationKeys.INSERT);
		settingHistory.close();
	}

	public void updateProduct(String productID, String status, String errors) {
		// update the transmission data
		GenRow entry = new GenRow();
		entry.setTableSpec("Products");
		entry.setConnection(getConnection());
		entry.setParameter("ProductID", productID);
		entry.setParameter("PublishingStatus", status);
		// if (errors != null && errors.length() > 0) entry.setParameter("PublishingValidation",errors);
		// entry.setParameter("ModifiedDate","NOW");
		// entry.setParameter("ModifiedBy",systemuserid);
		entry.doAction(GenerationKeys.UPDATE);

		entry.setTableSpec("properties/ProductQuickIndex");
		entry.setParameter("ON-ProductID", productID);
		entry.doAction(GenerationKeys.UPDATEALL);

		log("Updating Products/ProductQuickIndex to (PublishStatus={})", new String[] { status, productID });

		entry.close();
	}

	public void updatePackage(String transmissionID, String productID, String publishingID, boolean success, String errors, String filename) {
		updatePackage(transmissionID, productID, publishingID, success, errors, filename, true, false);
	}

	public void updatePackage(String transmissionID, String productID, String publishingID, boolean success, String errors, String filename, boolean active, boolean skip) {
		// update the transmission data
		GenRow entry = new GenRow();
		entry.setTableSpec("ProductTransmission");
		// entry.setRequest(feed.);
		entry.setConnection(getConnection());
		entry.setParameter("TransmissionID", transmissionID);
		entry.setParameter("Active", (active ? "Y" : "N"));
		entry.setParameter("ValidatedSuccess", (success ? "Y" : "N"));
		entry.setParameter("ValidatedDate", "NOW");
		entry.setParameter("PackagedSuccess", (success ? "Y" : "N"));
		entry.setParameter("PackagedDate", "NOW");

		if (StringUtils.isNotBlank(filename)) {
			entry.setParameter("File", filename);
		}

		entry.setParameter("ModifiedDate", "NOW");
		entry.setParameter("ModifiedBy", systemuserid);
		entry.doAction(GenerationKeys.UPDATE);

		if (publishingID != null && publishingID.length() > 0) {
			String status = "Published";
			if (!success)
				status = "Invalid";
			else if (!active)
				status = "Expired";
			updatePublishing(publishingID, status);
			updateProduct(productID, status, errors);
		} else {
			log("Not updating status");
		}

		GenRow log = new GenRow();
		log.setTableSpec("ProductTransmissionLog");
		log.setConnection(getConnection());
		log.setParameter("TransmissionID", transmissionID);
		log.setParameter("CreatedDate", "NOW");
		log.setParameter("CreatedBy", systemuserid);
		log.setParameter("Success", (success ? "Y" : "N"));
		log.setParameter("Type", "Pack");
		log.setParameter("Error", errors + "\r\nDetail Logs:\r\n" + productMessage.toString());
		log.createNewID();
		log.doAction(GenerationKeys.INSERT);

		if (logger.isTraceEnabled())
			logger.trace("ProductTransmissionLog(TransmissionID={}, Success={}, Error={})", new String[] { transmissionID, String.valueOf(success), errors });

		log.setParameter("Type", "Valid");
		log.remove("TransmissionLogID");
		log.createNewID();
		log.doAction(GenerationKeys.INSERT);

		entry.close();
	}

	public void updateTransmission(String transmissionID, String productID, String publishingID, String startDate, String endDate, boolean success, String errors) {
		updateTransmission(transmissionID, productID, publishingID, startDate, endDate, success, errors, true, false, false);
	}

	public void updateTransmission(String transmissionID, String productID, String publishingID, String startDate, String endDate, boolean success, String errors, boolean active, boolean skip, boolean valid) {
		// update the transmission data
		GenRow entry = new GenRow();
		entry.setTableSpec("ProductTransmission");
		// entry.setRequest(feed.);
		entry.setConnection(getConnection());
		entry.setParameter("TransmissionID", transmissionID);
		entry.setParameter("TransmissionSuccess", (success ? "Y" : "N"));
		entry.setParameter("Active", (active ? "Y" : "N"));
		entry.setParameter("MarkForDelete", (active ? "N" : "Y"));
		entry.setParameter("RefreshFeed", "N");
		entry.setParameter("TransmissionDate", "NOW");
		entry.setParameter("ModifiedDate", "NOW");
		entry.setParameter("ModifiedBy", systemuserid);
		entry.doAction(GenerationKeys.UPDATE);

		if (publishingID != null && publishingID.length() > 0) {
			String status = "Published";

			if (!valid)
				status = "Invalid";
			else if (!active)
				status = "Expired";

			updatePublishing(publishingID, status);
			updateProduct(productID, status, "");
		} else {
			log("Not updating status");
		}
		entry.close();

		GenRow log = new GenRow();
		log.setTableSpec("ProductTransmissionLog");
		log.setConnection(getConnection());
		log.setParameter("TransmissionID", transmissionID);
		log.setParameter("CreatedDate", "NOW");
		log.setParameter("CreatedBy", systemuserid);
		log.setParameter("StartDate", startDate);
		log.setParameter("EndDate", endDate);
		log.setParameter("Success", (success ? "Y" : "N"));
		log.setParameter("Type", "Sent");
		log.setParameter("Error", errors + "\r\nDetail Logs:\r\n" + productMessage.toString());
		log.createNewID();
		log.doAction(GenerationKeys.INSERT);
		log.close();

	}

	private GenRow getProduct(String productID) {
		GenRow product = new GenRow();
		product.setConnection(getConnection());
		product.setColumn("ProductID", productID);
		product.setViewSpec("ProductView");
		product.doAction("selectfirst");

		if ("House and Land".equals(product.getString("ProductType"))) {
			product.clear();
			product.setViewSpec("properties/ProductHouseAndLandView");
			product.setColumn("ProductID", productID);
			product.doAction("selectfirst");
		} else if ("Land".equals(product.getString("ProductType"))) {
			product.clear();
			product.setViewSpec("properties/ProductLotView");
			product.setColumn("ProductID", productID);
			product.doAction("selectfirst");
		} else if ("Apartment".equals(product.getString("ProductType"))) {
			product.clear();
			product.setViewSpec("properties/ProductApartmentView");
			product.setColumn("ProductID", productID);
			product.doAction("selectfirst");
		} else if ("Home Plan".equals(product.getString("ProductType"))) {
			product.clear();
			product.setViewSpec("properties/ProductPlanView");
			product.setColumn("ProductID", productID);
			product.doAction("selectfirst");
		}

		return product;
	}

	private void removeFile(String transmissionID) {
		GenRow entry = new GenRow();
		entry.setTableSpec("ProductTransmission");
		entry.setParameter("-select", "File");
		entry.setConnection(getConnection());
		entry.setParameter("TransmissionID", transmissionID);
		entry.doAction(GenerationKeys.SELECTFIRST);

		File file = new File(entry.getString("File"));
		if (file.exists())
			file.delete();
		entry.close();
	}

	private void removeFileName(String filename) {
		if (filename != null) {
			File file = new File(filename);
			if (file.exists())
				file.delete();
		}
	}

	private void updateValidationLog(String productID, String publishMethod, String errors) {
		if (errors == null || errors.length() == 0)
			errors = "";
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductValidation");
		row.setConnection(getConnection());
		row.setParameter("-select1", "*");
		row.setParameter("ProductID", productID);
		row.setParameter("PublishMethod", publishMethod);
		row.doAction(GenerationKeys.SELECTFIRST);

		if (!row.isSuccessful()) {
			row.setColumn("ProductValidationID", KeyMaker.generate());
			row.setAction("insert");
		} else {
			row.setAction("update");
		}
		row.setParameter("ProductID", productID);
		row.setParameter("PublishMethod", publishMethod);
		row.setColumn("Message", errors);
		if (errors.indexOf("Required:") >= 0) {
			row.setColumn("Status", "Content Required");
		} else if (errors.indexOf("Optional:") >= 0) {
			row.setColumn("Status", "Validated - Optional");
		} else if (errors.indexOf("Deferred:") >= 0) {
			row.setColumn("Status", "Validated - Deferred");
		} else {
			row.setColumn("Status", "Validated - Passed");
		}
		row.setColumn("ValidationDate", "NOW");
		row.setColumn("ValidationBy", row.getString("CURRENTUSERID"));
		row.doAction();
		row.close();
	}

	public String getProcessName() {
		return "Product Transmissions";
	}

	protected void processItem() {

	}

	protected void finish() {
	}

	protected void sendUserEmail(String alert) {
		debug("sending email to " + alert);
		log("\r\nCOMPLETED SUCCESSFULLY\r\n");
		try {

			RowBean context = new RowBean();
			context.put("URLHome", urlhome);
			context.put("EmailAlertsURLHome", emailalertsurlhome);
			context.put("alert", alert);
			// context.put("body", message);

			// String text = generateEmail(context, alerttext);
			// String html = generateEmail(context, alerthtml);

			ServletConfig sc = InitServlet.getConfig();
			if (sc != null && alert.length() != 0) {
				MailerBean mailer = new MailerBean();
				mailer.setServletContext(sc.getServletContext());
				mailer.setFrom(defaultFrom);
				mailer.setTo(alert);
				mailer.setSubject("Transmission Logs");
				mailer.setTextbody(message.toString());
				mailer.setHtmlbody(message.toString().replaceAll("\\n", "<br />"));
				if (config.getPretend()) {
					debug("email to " + alert + "\r\n " + message);
				} else {
					String response = mailer.getResponse();
					debug("email to " + alert + " " + response);
				}
			}

		} catch (Exception e) {
			error(e);
		}
	}

	protected String generateEmail(TableData context, String script) {
		StringWriter text = new StringWriter();
		try {
			StringBuffer roottemplate = new StringBuffer();
			roottemplate.append("#parse(\"");
			roottemplate.append(DEFAULT_VELOCITY_PATH);
			roottemplate.append(script);
			roottemplate.append("\")");

			VelocityEngine ve = VelocityManager.getEngine();
			ve.evaluate(context, text, "StatusAlerts", roottemplate.toString());

			text.flush();
			text.close();

		} catch (Exception e) {
			error(e);
		}
		return (text.toString());
	}

	public void close() {
		super.close();
	}

	protected EmailerMap getEmailerMap(GenRow row) {
		EmailerMap map = new EmailerMap();
		map.putAll(row);
		return (map);
	}

	public static void main(String[] args) {

	}

	private class QueueItem {
		public String productID;
		public String productR2RQueueID;

		private String ts = "ProductR2RQueue";

		public QueueItem(String productID) {
			this.productID = productID;
		}

		public QueueItem(GenRow row) {
			this.productID = row.getString("ProductID");
			this.productR2RQueueID = row.getData("ProductR2RQueueID");
		}

		public boolean delete() {
			GenRow row = new GenRow();
			row.setTableSpec(ts);
			row.setParameter("ProductR2RQueueID", productR2RQueueID);
			row.doAction("delete");
			return row.isSuccessful();
		}

		public boolean save() {
			GenRow row = new GenRow();
			row.setTableSpec(ts);
			row.setParameter("ProductR2RQueueID", productR2RQueueID);
			row.setParameter("ProductID", productID);
			try {
				row.doAction("insert");
			} catch (Exception e) {
				row.clearStatement();
				row.doAction("update");
			}

			return row.isSuccessful();
		}
	}

	private void insertLog(String transmissionID, StringBuffer productMessage, String type, boolean success) {
		GenRow log = new GenRow();
		log.setTableSpec("ProductTransmissionLog");
		log.setConnection(getConnection());
		log.setParameter("TransmissionID", transmissionID);
		log.setParameter("CreatedDate", "NOW");
		log.setParameter("CreatedBy", systemuserid);
		log.setParameter("Success", (success ? "Y" : "N"));
		log.setParameter("Type", type);
		log.setParameter("Error", productMessage.toString());
		log.createNewID();
		log.doAction(GenerationKeys.INSERT);
		log.close();
	}

	public void resetRunning() {
		isRunning = false;
	}

	private void sendProducts() {

	}

}