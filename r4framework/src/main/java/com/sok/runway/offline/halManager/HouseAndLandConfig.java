package com.sok.runway.offline.halManager;

import java.io.File;
import javax.servlet.ServletContext;

import com.sok.runway.*;
import com.sok.runway.offline.*;
import com.sok.framework.*;

public class HouseAndLandConfig extends AbstractDaemonWorkerConfig
{
   protected static String fullpath = null;
   protected static HouseAndLandConfig hlmconfig = null;
   protected static final String filepath = "/WEB-INF/hlm.xml";
   
   public static HouseAndLandConfig getConfig(String parentpath)
   {
      if(hlmconfig == null || hlmconfig.isModified())
      {
         loadConfig(parentpath);
      }
      return(hlmconfig);
   }

   protected static synchronized void loadConfig(String parentpath)
   {
      if(hlmconfig == null || hlmconfig.isModified())
      {
         if(fullpath == null)
         {
            StringBuffer sb = new StringBuffer(parentpath);
            sb.append(filepath);
            fullpath = sb.toString();
         }
         hlmconfig = new HouseAndLandConfig(fullpath);
      }
   }

   public HouseAndLandConfig(String path)
   {
      super(path);
   }

}
