package com.sok.runway;
import com.sok.framework.*;
import com.sok.runway.crm.factories.StatusUpdater;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnswerBean extends RowBean {
	
	private static final Logger logger = LoggerFactory.getLogger(AnswerBean.class);
	public static final String INPUT_VALUE_OTHER_ANSWER = "Other"; 

	public static final String ANSWER_QT_TOKEN = "::";
	public static final String ENTITY_ID_PREFIX = "Q";

	private static final String FIELDNAME_ANSWER = "Answer";
	private static final String FIELDNAME_OTHER_ANSWER = "OtherAnswer";

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	String profileTypes[] = {"CompanyID","ContactID","CompanyProductGroupID","ContactProductGroupID",
			"OpportunityID","OpportunityStageID","ProductID","OrderID","VisitID","NoteID","OrderItemID"}; 

	RowSetBean checkBean = null;
	UserBean currentuser = null;

	HashMap<String,String> viewSpecs = null;

	private String answerFieldName;

	private String processStageID = null;

	protected GenRow requestrow = null;

	public AnswerBean() {
		super();      
		checkBean = new RowSetBean();

		viewSpecs = new HashMap<String,String>();
		viewSpecs.put("CompanyID","answers/CompanyAnswerView");
		viewSpecs.put("CompanyProductGroupID","answers/CompanyProductGroupAnswerView");
		viewSpecs.put("ContactID","answers/ContactAnswerView");
		viewSpecs.put("ContactProductGroupID","answers/ContactProductGroupAnswerView");
		viewSpecs.put("NoteID","answers/NoteAnswerView");
		viewSpecs.put("OpportunityID","answers/OpportunityAnswerView");
		viewSpecs.put("OpportunityStageID","answers/OpportunityStageAnswerView");
		viewSpecs.put("OrderID","answers/OrderAnswerView");
		viewSpecs.put("ProductID","answers/ProductAnswerView");
		viewSpecs.put("VisitID","answers/VisitAnswerView");
		viewSpecs.put("OrderItemID","answers/OrderItemAnswerView");
		viewSpecs.put("ProductGroupID","answers/ProductGroupAnswerView");

		this.answerFieldName = FIELDNAME_ANSWER;
	}

	public void setRequest(HttpServletRequest request) {
		super.setRequest(request);
		requestrow = new GenRow();
		requestrow.parseRequest(request);
		HttpSession session = request.getSession();
		if (session != null) {
			setCurrentUser((UserBean)session.getAttribute("currentuser"));
		}
	}

	public void parseRequest(HttpServletRequest request) {
		super.parseRequest(request);
		HttpSession session = request.getSession();
		if (session != null) {
			setCurrentUser((UserBean)session.getAttribute("currentuser"));
		}
	}

	public void setConnection(Connection connection) {
		super.setConnection(connection);
		checkBean.setConnection(connection);
		if(requestrow == null) {
			requestrow = new GenRow();
		}
	}

	public void setCurrentUser(UserBean currentuser) {
		this.currentuser = currentuser;
	}

	public String getAnswerViewSpec(String idColumn) {
		return viewSpecs.get(idColumn);
	}


	public void saveAnswer(ServletRequest request, String questionID, String answerID, 
			String[] answer, int quantifier) {
		saveAnswer(null, request, questionID, answerID, answer, quantifier);
	}

	public void saveAnswer(String formPrefix, ServletRequest request, String questionID, String answerID, 
			String[] answer, int quantifier) {


		String entityPrefix = null;
		if (formPrefix != null && formPrefix.length() > 0) {
			entityPrefix = formPrefix + ActionBean._at + ENTITY_ID_PREFIX;
		}
		else {
			entityPrefix = ENTITY_ID_PREFIX;
		}
		String pgID = request.getParameter(entityPrefix + "ProductGroupID");
		String id = null;
		// If there is a ProductGroupID for backwards compatability
		if (pgID != null && pgID.length() > 0) {
			id = request.getParameter(entityPrefix + "CompanyID");
			if (id != null && id.length() > 0 ) {
				id = getCompanyProductGroupID(id, pgID);
				saveAnswer(id, questionID, "CompanyProductGroupID", answerID, answer, quantifier);
			}
			else {
				id = request.getParameter(entityPrefix + "ContactID");
				if (id != null && id.length() > 0 ) {
					id = getContactProductGroupID(id, pgID);
					saveAnswer(id, questionID, "ContactProductGroupID", answerID, answer, quantifier);
				} else { 
					saveAnswer(pgID, questionID, "ProductGroupID", answerID, answer, quantifier);
				}
			}
		}
		else {
			for (int i = 0; i < profileTypes.length; i++) {
				id = request.getParameter(entityPrefix + profileTypes[i]);
				logger.debug("Updating profile for  {}" , id);
				if (id != null && id.length() > 0) {
					if (id.indexOf("+") > -1) {
						String[] idArray = id.split("\\+");
						for (String currentID : idArray) {
							logger.debug("Updating profile for currentID {}" , currentID);
							logger.debug("Updating profile for answerID {} answer {}" , answerID, answer);
							saveAnswer(currentID, questionID, profileTypes[i], answerID, answer, quantifier);
						}
					} else {
						saveAnswer(id, questionID, profileTypes[i], answerID, answer, quantifier);
					}
					break;
				}
			}
		}

	}

	public void saveOtherAnswer(String formPrefix, ServletRequest request, String questionID, String answerID, 
			String[] answer, int quantifier) {
		/*
		 * Temporarily change the answer fieldname so that the answer is stored in the 
		 * "OtherAnswer" field instead of the "Answer" field on the answers table. 
		 * This method has been implemented this way because reusing the existing saveAnswer 
		 * methods would require major refactoring, and all of the participating saveAnswer methods 
		 * are public, so cannot change their signatures without destroying backward compatibility.
		 */
		this.answerFieldName = FIELDNAME_OTHER_ANSWER;
		this.saveAnswer(formPrefix, request, questionID, answerID, answer, quantifier);
		this.answerFieldName = FIELDNAME_ANSWER;
	}

	/**
	 * Made Deprecated by Mikey for Backwards Compatibility for runway.jar 13/03/2007
	 * @deprecated
	 */
	public String saveMatrixAnswer(String formPrefix, ServletRequest request, String questionID, String matrixQuestionID, 
			String[] answer, int quantifier) {
		String entityPrefix = null;
		if (formPrefix != null && formPrefix.length() > 0) {
			entityPrefix = formPrefix + ActionBean._at + ENTITY_ID_PREFIX;
		}
		else {
			entityPrefix = ENTITY_ID_PREFIX;
		}
		String id = null;
		for (int i=0; i < profileTypes.length; i++) {
			id = request.getParameter(entityPrefix + profileTypes[i]);
			if (id != null && id.length() > 0) {
				return saveMatrixAnswer(id, questionID, matrixQuestionID, profileTypes[i], answer, quantifier);
			}
		}
		return null;
	}

	/**
	 * Made Deprecated by Mikey for Backwards Compatibility for runway.jar 13/03/2007
	 * @deprecated
	 */
	public void updateMatrixDateAndTotals(String formPrefix, HttpServletRequest request, String matrixID, String completedDate) {
		/*   String entityPrefix = null;
      if (formPrefix != null && formPrefix.length() > 0) {
         entityPrefix = formPrefix + ActionBean._at + ENTITY_ID_PREFIX;
      }
      else {
         entityPrefix = ENTITY_ID_PREFIX;
      }
      String id = null;
      for (int i=0; i < profileTypes.length; i++) {
         id = request.getParameter(entityPrefix + profileTypes[i]);
         if (id != null && id.length() > 0) {

            StringBuffer totalCalc = new StringBuffer();
            totalCalc.append("UPDATE Matrices SET Total = ");
            totalCalc.append("(SELECT COUNT(*) FROM Answers WHERE Answers.MatrixID = Matrices.MatrixID");
            totalCalc.append(" AND Answer IS NOT NULL AND Answer <> '' AND Answer <> 'N' and Answer <> 'No'");
            totalCalc.append(" AND QuestionID IN (SELECT QuestionID FROM SurveyQuestions, Surveys WHERE ");
            totalCalc.append(" SurveyQuestions.SurveyID =  Surveys.SurveyID AND Surveys.RelatedSurveyID LIKE '_%'");
            totalCalc.append("))");
            totalCalc.append(" WHERE ");
            totalCalc.append(profileTypes[i]);
            totalCalc.append(" = '");
            totalCalc.append(id);
            totalCalc.append("';");

            RowBean totaller = new RowBean();
            totaller.setConnection(this.getConnection());
            totaller.setSearchStatement(totalCalc.toString());
      	   totaller.setAction("execute");
      	   totaller.doAction();
	         this.setError(totaller.getError());
	         totaller.clear();
            totaller.setViewSpec("MatrixView");
            totaller.setColumn("MatrixID", matrixID);
            totaller.setColumn("CompletedDate", completedDate);
            totaller.setAction("update");
            totaller.doAction();
	         this.setError(totaller.getError());
         }
      }

		 */
	}

	public void saveCompanyAnswer(String companyID, String questionID, String answerID, 
			String[] answer, int quantifier) {
		saveAnswer(companyID, questionID, "CompanyID", answerID, answer, quantifier);
	}

	public void saveCompanyAnswer(String companyID, String questionID, String answer) {
		String[] answers = new String[1];
		answers[0] = answer;
		saveCompanyAnswer(companyID,questionID,null, answers,-1);
	}

	public void saveContactAnswer(String contactID, String questionID, String answer) {
		String[] answers = new String[1];
		answers[0] = answer;

		saveContactAnswer(contactID,questionID,null, answers,-1);
	}

	public void saveContactAnswer(String contactID, String questionID, String answerID, 
			String[] answer, int quantifier) {
		saveContactAnswer(contactID, questionID, answerID, answer, quantifier, true);
	}

	public void saveContactAnswer(String contactID, String questionID, String answerID, 
			String[] answer, int quantifier, boolean clearBlankAnswers) {
		saveAnswer(contactID, questionID, "ContactID", answerID, answer, quantifier);
	}

	public void saveOpportunityAnswer(String opportunityID, String questionID, String answerID, 
			String[] answer, int quantifier) {
		saveAnswer(opportunityID, questionID, "OpportunityID", answerID, answer, quantifier);
	}

	public void saveOpportunityStageAnswer(String opportunityStageID, String questionID, String answerID, 
			String[] answer, int quantifier) {
		saveAnswer(opportunityStageID, questionID, "OpportunityStageID", answerID, answer, quantifier);
	}

	public void saveProductAnswer(String productID, String questionID, String answerID, 
			String[] answer, int quantifier) {
		saveAnswer(productID, questionID, "ProductID", answerID, answer, quantifier);
	}

	public void saveOrderAnswer(String orderID, String questionID, String answerID, 
			String[] answer, int quantifier) {
		saveAnswer(orderID, questionID, "OrderID", answerID, answer, quantifier);
	}

	public void saveVisitAnswer(String visitID, String questionID, String answerID, 
			String answer, int quantifier) {
		String[] strArray = {answer};
		saveAnswer(visitID, questionID, "VisitID", answerID, strArray, quantifier);
	}

	public void saveVisitAnswer(String visitID, String questionID, String answerID, 
			String[] answer, int quantifier) {
		saveAnswer(visitID, questionID, "VisitID", answerID, answer, quantifier);
	}

	public void saveCompanyProductGroupAnswer(String companyProductGroupID, String questionID, String answerID, 
			String[] answer, int quantifier) {
		saveAnswer(companyProductGroupID, questionID, "CompanyProductGroupID", answerID, answer, quantifier);
	}

	public void saveContactProductGroupAnswer(String contactProductGroupID, String questionID, String answerID, 
			String[] answer, int quantifier) {
		saveAnswer(contactProductGroupID, questionID, "ContactProductGroupID", answerID, answer, quantifier);
	}

	public void saveAnswer(String id, String questionID, String idColumn, String[] answer) {
		saveAnswer(id, questionID, idColumn, null, answer, 0);
	}

	/**
	 * Made Deprecated by Mikey for Backwards Compatibility for runway.jar 13/03/2007
	 * @deprecated
	 */
	public synchronized String saveMatrixAnswer(String id, String questionID, String matrixQuestionID, String idColumn, String[] answer, int quantifier) {

		saveAnswer(id,questionID, idColumn, answer, quantifier);
		return null; 
		/*   
      RowSetBean matrixBean = new RowSetBean();
      matrixBean.setConnection(this.getConnection());
      matrixBean.setViewSpec("MatrixView");
      //matrixBean.setColumn("QuestionXID",questionID);
      matrixBean.setColumn("MatrixQuestionID",matrixQuestionID);
      matrixBean.setColumn(idColumn,id);
      matrixBean.generateSQLStatement();
      matrixBean.getResults();
      if (!matrixBean.getNext()) {
         matrixBean.clear();  
         matrixBean.setColumn("MatrixID",KeyMaker.generate());
         matrixBean.setColumn(idColumn,id); 
         //matrixBean.setColumn("QuestionXID",questionID);
         matrixBean.setColumn("MatrixQuestionID",matrixQuestionID);
         matrixBean.setAction("insert");
         matrixBean.doAction();
      }
      saveAnswer(matrixBean.getString("MatrixID"),questionID,"MatrixID",null,answer,quantifier);

      String theId = matrixBean.getString("MatrixID");
      matrixBean.close();
      return theId;

	  return null;
		 */
	}

	public synchronized void saveAnswer(String id, String questionID, String idColumn, String answerID, String[] answer, int quantifier) {
		saveAnswer(id, questionID, idColumn, answer, quantifier);
	}

	public synchronized void saveAnswer(String id, String questionID, String idColumn, String[] answer, int quantifier) {
		saveAnswer(id, questionID, idColumn, answer, quantifier, true);
	}

	public synchronized void saveAnswer(String id, String questionID, String idColumn, String[] answer, int quantifier, boolean clearBlankAnswers) {
		logger.info("saveAnswer({})", id);
		this.clear();
		this.setViewSpec(getAnswerViewSpec(idColumn));
		String prevAnswer = null;
		String prevAnswerID = null;

		try {
			// need to do this first
			QuestionBean questionBean = new QuestionBean();
			questionBean.setConnection(this.getConnection());
			questionBean.setViewSpec("QuestionView");
			questionBean.setAction("select");
			questionBean.setColumn("QuestionID",questionID);
			questionBean.doAction();
	
			// if the Answer already exists and it's a Contact or Company answer then we history it and create a new one
			boolean exists = answerExists(id, questionID, idColumn);
			logger.trace("Answer Existed? [{}]", exists);
			if (exists && ("CompanyID".equals(idColumn) || "ContactID".equals(idColumn))) {
				prevAnswer = checkBean.getString(this.answerFieldName);
				prevAnswerID = checkBean.getString("AnswerID");
				this.setColumn("AnswerID",checkBean.getString("AnswerID"));
				this.setColumn("QuestionID", questionID + "history");
				this.doAction("update");
	
				this.setColumn("AnswerID",KeyMaker.generate());
				this.setAction("insert");
				this.setColumn("CreatedDate", "NOW");
				this.setColumn("CreatedBy", currentuser.getCurrentUserID());
				this.setAction("insert");
			} else if (exists) {
				prevAnswer = checkBean.getString(this.answerFieldName);
				if (processStageID != null && processStageID.length() > 0) this.setColumn("ProcessStageID", processStageID);
				this.setColumn("AnswerID",checkBean.getString("AnswerID"));
				this.setAction("update");
			} else {
				if (processStageID != null && processStageID.length() > 0) this.setColumn("ProcessStageID", processStageID);
				this.setColumn("AnswerID",KeyMaker.generate());
				this.setAction("insert");
				this.setColumn("CreatedDate", "NOW");
				this.setColumn("CreatedBy", currentuser.getCurrentUserID());
			}
			this.setColumn(idColumn, id);
			this.setColumn("QuestionID", questionID);
	
			setAnswerAndQuantifier(answer, quantifier, questionBean.getString("InputType"));
	
			if (idColumn.equals("VisitID")) {
				setCalculatedAnswer(id, "VisitID", questionBean);
			}
			else if (idColumn.equals("NoteID")) {
				setCalculatedAnswer(id, "NoteID", questionBean);
			}
	
			// Only update if answer has changed
			if (!this.getColumn(this.answerFieldName).equals(prevAnswer)) {
				// If answer already exists or if an answer was speciified
				// If answer is empty we only clear if clearBlankAnswers = true
	
				if (this.getColumn(this.answerFieldName).length() > 0 || (exists && clearBlankAnswers)) {
					this.setColumn("ModifiedDate", "NOW");
					this.setColumn("ModifiedBy", currentuser.getCurrentUserID());
					this.doAction();
					
					logger.trace(this.toString());
					
					processMultiAnswerConverters(questionBean, idColumn, this.getColumn(this.answerFieldName));
				}
			} else if (prevAnswer != null && prevAnswerID != null) {
				// revert back if there is no new answers
				this.clear();
				this.setViewSpec(getAnswerViewSpec(idColumn));
				this.setColumn("AnswerID",prevAnswerID);
				this.setColumn("QuestionID", questionID);
				this.doAction("update");
			}
			if(logger.isDebugEnabled() && StringUtils.isNotBlank(this.getError())){ 
				logger.debug(this.getError());	
				logger.trace(this.getStatement());
			} 
		} catch (Exception e) {
			logger.error("Exception in answer bean", e);
		}
	}

	protected void processMultiAnswerConverters(QuestionBean questionBean, String idColumn, String answer)
	{
		if(answer!=null && answer.length()!=0){
			if("ContactID".equals(idColumn)){    
				String inputtype = questionBean.getColumn("InputType");
				if("Drop Down".equals(inputtype) || "Radio Button".equals(inputtype) || "Checkbox".equals(inputtype)){  
					if(questionBean.getColumn("ValueList").length()!=0){         
						if(answer.indexOf("+")>=0){
							MultiField multifield = new MultiField(answer);
							for(int i=0; i<multifield.length(); i++){
								processAnswerConverters(questionBean, idColumn, multifield.get(i));
							}
						}else{
							processAnswerConverters(questionBean, idColumn, answer);
						}
					}
				}
			}
		}
	}

	protected String[] getQuestionSurveyGroups(String questionid)
	{
		String[] groupids = null;
		if(questionid!=null && questionid.length()!=0)
		{
			GenRow row = new GenRow();
			row.setConnection(this.getConnection());
			row.setTableSpec("Groups");
			row.setParameter("-select1", "GroupID");
			row.setParameter("Applicable", "Y");
			row.setParameter("GroupLinkedSurvey.LinkedSurveySurvey.SurveySurveyQuestions.QuestionID", questionid);
			row.doAction("search");
			row.getResults(true);
			groupids = new String[row.getSize()];
			int i = 0;
			while(row.getNext())
			{
				groupids[i] = row.getData("GroupID");
				i++;
			}
			row.close();
		}
		return(groupids);
	}

	protected void processAnswerConverters(QuestionBean questionBean, String idColumn, String answer)
	{
		if(answer.length()!=0)
		{

			GenRow listitem = new GenRow();
			listitem.setConnection(this.getConnection());
			listitem.setViewSpec("ListItemView");
			listitem.setParameter("ListID", questionBean.getColumn("ValueList"));
			listitem.setParameter("ItemValue", answer);
			listitem.doAction("selectfirst");

			if(listitem.isSuccessful()){
				String statusid = listitem.getData("SetStatusID");
				String locationid = listitem.getData("SetLocationID");
				String regionid = listitem.getData("SetRegionID");
				String contactid = getColumn(idColumn);
				String repuserid = requestrow.getParameter("RepUserID");;
				Properties params = InitServlet.getSystemParams();
				if(repuserid == null){
					repuserid = params.getProperty("DefaultRepUserID");
				}
				if(questionBean.getColumn("SetsLocation").length()!=0){
					insertContactLocation(contactid, locationid, regionid);
				}
				if(questionBean.getColumn("SetsStatus").length()!=0 && statusid.length()!=0){
					String[] groupids = getQuestionSurveyGroups(questionBean.getColumn("QuestionID"));
					insertContactGroupStatus(contactid, groupids, statusid, repuserid);
				}  
			}
		}
	}

	public void insertContactLocation(String contactid, String locationid, String regionid)
	{   
		if((locationid!=null && locationid.length()!=0) || (regionid!=null && regionid.length()!=0)){
			GenRow contactlocation = new GenRow();
			contactlocation.setConnection(this.getConnection());
			contactlocation.setParameter("-select1","ContactLocationID");
			contactlocation.setTableSpec("ContactLocations");
			contactlocation.setParameter("ContactID", contactid); 
			contactlocation.setParameter("LocationID", locationid);
			contactlocation.setParameter("RegionID", regionid);
			contactlocation.doAction("selectfirst");

			if(!contactlocation.isSuccessful() || requestrow.getParameter("LocationStreet")!=null) // insert location if a build address was entered.
			{
				contactlocation.setConnection(this.getConnection());
				contactlocation.setParameter("CreatedBy", currentuser.getCurrentUserID());
				contactlocation.setParameter("CreatedDate", "NOW");
				contactlocation.createNewID();

				if(requestrow.getParameter("LocationStreet")!=null){
					contactlocation.setParameter("BuildAddressID", insertContactLocationAddress());
				}

				contactlocation.doAction("insert");
			}
		}
	}

	public String insertContactLocationAddress()
	{
		GenRow address = new GenRow();
		address.setTableSpec("Addresses");
		address.setConnection(this.getConnection());
		address.createNewID();
		address.setParameter("Street", requestrow.getParameter("LocationStreet")); 
		address.setParameter("Street2", requestrow.getParameter("LocationStreet2")); 
		address.setParameter("City", requestrow.getParameter("LocationCity")); 
		address.setParameter("State", requestrow.getParameter("LocationState")); 
		address.setParameter("Postcode", requestrow.getParameter("LocationPostcode"));
		address.setParameter("Country", requestrow.getParameter("LocationCountry"));
		address.setParameter("Latitude", requestrow.getParameter("LocationLatitude")); 
		address.setParameter("Longitude", requestrow.getParameter("LocationLongitude")); 
		address.doAction("insert");
		return(address.getString("AddressID"));
	}



	public void insertContactGroupStatus(String contactid, String groupid, String statusid, String repuserid)
	{
		GenRow contactgroup = new GenRow();
		contactgroup.setConnection(this.getConnection());
		contactgroup.setParameter("-select1","ContactGroupID");
		contactgroup.setTableSpec("ContactGroups");
		contactgroup.setParameter("ContactID", contactid); 
		contactgroup.setParameter("GroupID", groupid);
		contactgroup.setParameter("RepUserID", repuserid);
		contactgroup.doAction("selectfirst");

		if (!contactgroup.isSuccessful()) {
			contactgroup.setConnection(this.getConnection());
			contactgroup.remove("RepUserID");
			contactgroup.doAction("selectfirst");
		}

		if(!contactgroup.isSuccessful())
		{
			contactgroup.setConnection(this.getConnection());
			contactgroup.setParameter("RepUserID", repuserid);
			contactgroup.setParameter("GroupID", groupid);
			contactgroup.setParameter("CreatedBy", currentuser.getCurrentUserID());
			contactgroup.setParameter("CreatedDate", "NOW");
			contactgroup.createNewID();
			contactgroup.doAction("insert");
		}

		if(contactgroup.isSuccessful()) {
			contactgroup.setConnection(this.getConnection());
			contactgroup.setParameter("ContactGroupID", contactgroup.getString("ContactGroupID")); 
			contactgroup.setParameter("ContactGroupStatusID", StatusUpdater.insertGroupStatus(this.getConnection(), "ContactGroupID", contactgroup.getString("ContactGroupID"), groupid, statusid, currentuser.getCurrentUserID())); 
			contactgroup.doAction(ActionBean.UPDATE);
		}
	}   

	public void insertContactGroupStatus(String contactid, String[] groupids, String statusid, String repuserid)
	{
		if(groupids!=null)
		{
			for(int i=0; i<groupids.length; i++)
			{
				insertContactGroupStatus(contactid, groupids[i], statusid, repuserid);
			}
		}
	}    

	public void setAnswerAndQuantifier(String[] values, int quantifier, String inputType) {
		int total = 0;
		int tokenIndex;
		String answerPart = "";
		StringBuffer answerBuffer = new StringBuffer();

		if (values != null && values.length > 0) {

			for (int j=0; j < values.length; j++) {
				tokenIndex = values[j].indexOf(ANSWER_QT_TOKEN);
				if (tokenIndex >= 0) {
					answerPart = values[j].substring(0,tokenIndex);
					if (answerPart.length() > 0) {
						try {
							total = total + Integer.parseInt(values[j].substring(tokenIndex+2));
						}
						catch (Exception e) {}

						if (answerBuffer.length() > 0) {
							answerBuffer.append("+");
						}
						answerBuffer.append(answerPart.trim());
					}
				} 
				else if (values[j].length() > 0) {
					if (answerBuffer.length() > 0) {
						answerBuffer.append("+");
					}
					answerBuffer.append(values[j].trim());
				}
			}
		}

		////////MAYBE NEEDS CHANGING////////
		if (total > quantifier && quantifier > 0) {
			total = quantifier;
		}
		////////////////////////////////

		boolean isDate = inputType.equals(Profile._date);
		boolean isNumber = inputType.equals(Profile._number )
				|| inputType.equals(Profile._percentage )
				|| inputType.equals(Profile._calculation )
				|| inputType.equals(Profile._calculation_sum_order )
				|| inputType.equals(Profile._calculation_profile_total )
				|| inputType.equals(Profile._currency );

		// Truncate as Answer column can only take 200 characters
		if(answerBuffer.toString().length() > 200 ){
			this.setColumn(this.answerFieldName, answerBuffer.toString().substring(0, 200));
		} else {
			this.setColumn(this.answerFieldName, answerBuffer.toString());
		}
		
		String answerText = (this.answerFieldName.equals(FIELDNAME_OTHER_ANSWER)) 
				? INPUT_VALUE_OTHER_ANSWER 
						: answerBuffer.toString();
		this.setColumn("AnswerText", answerText);
		this.setColumn("Quantifier", String.valueOf(total));

		if (isDate) {
			AnswerBean.setDateAnswer(answerBuffer.toString(), this);
			this.setColumn("AnswerNumber", "NULL");
		}
		else if (isNumber) {
			AnswerBean.setNumberAnswer(answerBuffer.toString(), this);
			this.setColumn("AnswerDate", "NULL");
		}
		else {
			this.setColumn("AnswerNumber", "NULL");
			this.setColumn("AnswerDate", "NULL");
		}

	}

	public synchronized static void setDateAnswer(String answer, TableData bean) {
		try {
			if ("TODAY".equals(answer)) {
				bean.setColumn("AnswerDate",answer);
				bean.setColumn(FIELDNAME_ANSWER,dateFormat.format(new java.util.Date()));
			}
			else {
				java.util.Date test = dateFormat.parse(answer);
				bean.setColumn("AnswerDate",answer);
			}
		}
		catch (Exception e) {
			bean.setColumn("AnswerDate","NULL");
		}

	}

	public static void setNumberAnswer(String answer, TableData bean) {
		try {
			double test = Double.parseDouble(answer);
			bean.setColumn("AnswerNumber",answer);
		}
		catch (Exception e) {
			bean.setColumn("AnswerNumber","NULL");
		}
	}

	private boolean answerExists(String id, String questionID, String idColumn) {
		checkBean.clear();
		checkBean.setViewSpec(getAnswerViewSpec(idColumn));
		checkBean.setConnection(this.getConnection());
		checkBean.setColumn(idColumn,id);
		checkBean.setColumn("QuestionID",questionID);
		if (processStageID != null && processStageID.length() > 0) checkBean.setColumn("ProcessStageID", processStageID);
		checkBean.generateSQLStatement();
		
		logger.debug(checkBean.getStatement());
		checkBean.getResults();
		this.setError(checkBean.getError());

		boolean check = checkBean.getNext();
		checkBean.close();
		return check;
	}

	public String getCompanyProductGroupID(String id, String pgID) {
		RowSetBean bean = new RowSetBean();
		bean.setConnection(this.getConnection());
		bean.setViewSpec("CompanyProductGroupView");
		bean.setColumn("CompanyID",id);
		bean.setColumn("ProductGroupID",pgID);
		bean.generateSQLStatement();
		bean.getResults();
		String theId = null;
		if (bean.getNext()) {
			theId = bean.getString("CompanyProductGroupID");
			bean.close();
		}
		else {
			RowBean pgBean = new RowBean();
			pgBean.setConnection(this.getConnection());
			pgBean.setViewSpec("CompanyProductGroupView");
			pgBean.setColumn("CompanyProductGroupID",KeyMaker.generate());
			pgBean.setColumn("CompanyID",id);
			pgBean.setColumn("ProductGroupID",pgID);
			pgBean.setColumn("CreatedDate","NOW");
			pgBean.setColumn("ModifiedDate","NOW");
			pgBean.setColumn("CreatedBy",currentuser.getCurrentUserID());
			pgBean.setColumn("ModifiedBy",currentuser.getCurrentUserID());
			pgBean.setAction("insert");
			pgBean.doAction();
			id = pgBean.getString("CompanyProductGroupID");
		}
		return theId;
	}

	public String getContactProductGroupID(String id, String pgID) {
		RowSetBean bean = new RowSetBean();
		bean.setConnection(this.getConnection());
		bean.setViewSpec("ContactProductGroupView");
		bean.setColumn("ContactID",id);
		bean.setColumn("ProductGroupID",pgID);
		bean.generateSQLStatement();
		bean.getResults();
		bean.getResults();
		String theId = null;
		if (bean.getNext()) {
			theId = bean.getString("ContactProductGroupID");
			bean.close();
			return theId;
		}
		else {
			RowBean pgBean = new RowBean();
			pgBean.setConnection(this.getConnection());
			pgBean.setViewSpec("ContactProductGroupView");
			pgBean.setColumn("ContactProductGroupID",KeyMaker.generate());
			pgBean.setColumn("ContactID",id);
			pgBean.setColumn("ProductGroupID",pgID);
			pgBean.setColumn("CreatedDate","NOW");
			pgBean.setColumn("ModifiedDate","NOW");
			pgBean.setColumn("CreatedBy",currentuser.getCurrentUserID());
			pgBean.setColumn("ModifiedBy",currentuser.getCurrentUserID());
			pgBean.setAction("insert");
			pgBean.doAction();
			return pgBean.getString("ContactProductGroupID");
		}
	}

	public void setCalculatedAnswer(String id, String columnName, QuestionBean questionBean) {

		if (questionBean.getString("InputType").equals(Profile._calculation)) {
			String calcPeriod = questionBean.getString("CalculationPeriod");
			String calcType = questionBean.getString("CalculationType");

			RowBean visitBean = new RowBean();
			visitBean.setConnection(getConnection());
			visitBean.setViewSpec("VisitView");
			visitBean.setColumn(columnName, id);
			visitBean.setAction("select");
			visitBean.doAction();

			Calendar cal = new GregorianCalendar();
			try {
				cal.setTime((java.util.Date)visitBean.getObjectFromString("NoteDate"));
			}
			catch (Exception e) {}

			int relatedAnswer = 0;
			RowSetBean compareBean = new RowSetBean();
			compareBean.setConnection(getConnection());
			compareBean.setViewSpec(getAnswerViewSpec(columnName));
			compareBean.setColumn(columnName, id);
			compareBean.setColumn("QuestionID",questionBean.getString("RelatedQuestionID"));
			compareBean.generateSQLStatement();
			compareBean.getResults();
			if (compareBean.getNext()) {
				try { relatedAnswer = Integer.parseInt(compareBean.getString(this.answerFieldName)); }
				catch (Exception e) {}
			}
			compareBean.close();

			compareBean = new RowSetBean();
			compareBean.clear();
			compareBean.setConnection(getConnection());
			compareBean.setViewSpec("AnswerVisitView");
			compareBean.setColumn("AnswerVisit.CompetitorID", visitBean.getString("CompetitorID"));
			compareBean.setColumn("QuestionID",questionBean.getString("CalculationQuestionID"));

			StringBuffer theDate = new StringBuffer();
			theDate.append("<");
			if (calcPeriod.equals(ActionBean.LASTYEAR)) {
				cal.set(Calendar.DATE,1);
				cal.set(Calendar.MONTH,1);
			}
			else if (calcPeriod.equals(ActionBean.LASTMONTH)) {
				cal.set(Calendar.DATE,1);
				//compareBean.setColumn("AnswerVisit.VisitNote.NoteDate","<" + "01/" + Calendar.MONTH + "/" + cal.get(Calendar.MONTH));
			}
			else if (calcPeriod.equals(ActionBean.LASTWEEK)) {
				while(cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
					cal.roll(Calendar.DATE,-1);
				}
			}
			// else anything before the notedate
			if (cal.get(Calendar.DATE) < 10) {
				theDate.append("0");
			}
			theDate.append(cal.get(Calendar.DATE));
			theDate.append("/");
			if (cal.get(Calendar.MONTH)+1 < 10) {
				theDate.append("0");
			}
			theDate.append(cal.get(Calendar.MONTH)+1);
			theDate.append("/");
			theDate.append(cal.get(Calendar.YEAR));

			compareBean.setColumn("AnswerVisit.VisitNote.NoteDate",theDate.toString());

			compareBean.setTop(1);
			compareBean.sortBy("NoteDate",0);
			compareBean.sortOrder("DESC",0);
			compareBean.generateSQLStatement();
			compareBean.getResults();

			int calcAnswer = 0;
			if (compareBean.getNext()) {
				try { calcAnswer = Integer.parseInt(compareBean.getString(this.answerFieldName)); }
				catch (Exception e) {}
			}

			compareBean.close();

			if (calcType.equals(Profile._calculation_sum)) {
				calcAnswer = relatedAnswer + calcAnswer;
			}
			else if (calcType.equals(Profile._calculation_diff)) {
				calcAnswer = relatedAnswer - calcAnswer;
			}
			else if (calcType.equals(Profile._calculation_prod)) {
				calcAnswer = relatedAnswer * calcAnswer;
			}
			else if (calcType.equals(Profile._calculation_div)) {
				calcAnswer = relatedAnswer / calcAnswer;
			}
			else if (calcType.equals(Profile._calculation_percent)) {
				calcAnswer = (relatedAnswer / calcAnswer) * 100;
			}

			this.setColumn(this.answerFieldName, String.valueOf(calcAnswer));
			this.setColumn("AnswerNumber", String.valueOf(calcAnswer));
		}
		else if (questionBean.getString("InputType").equals(Profile._calculation_profile_total)) {
			/* THIS WILL NOT WORK, MULTIPLE ANSWER TABLES NOW
         RowSetBean calc = new RowSetBean();

         calc.setViewSpec(getAnswerViewSpec(columnName));
         calc.setConnection(getConnection());
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT Count(*) AS AnswerCalc FROM Answers");
         sql.append(" INNER JOIN SurveyQuestions ON Answers.QuestionID = SurveyQuestions.QuestionID");
         sql.append(" INNER JOIN SurveyQuestions SQ ON SurveyQuestions.SurveyID = SQ.SurveyID");
         sql.append(" WHERE Answer IS NOT NULL AND Answer <> '' AND Answer <> 'N' and Answer <> 'No'");
         sql.append(" AND Answers.QuestionID <> '");
         sql.append(questionBean.getString("QuestionID"));
         sql.append("' AND SQ.QuestionID = '");
         sql.append(questionBean.getString("QuestionID"));
         sql.append("' AND Answers.");
         sql.append(columnName);
         sql.append(" = '");
         sql.append(id);
         sql.append("';");

         calc.setSearchStatement(sql.toString());
         calc.getResults();
         calc.getNext();
         this.setColumn(this.answerFieldName, calc.getString("AnswerCalc"));
         this.setColumn("AnswerNumber", calc.getString("AnswerCalc"));
         calc.close();
			 */
		}
	}

	public String getProcessStageID() {
		return processStageID;
	}

	public void setProcessStageID(String processStageID) {
		this.processStageID = processStageID;
	}
}


/*SELECT Count(*) FROM Answers 
INNER JOIN SurveyQuestions ON Answers.QuestionID = SurveyQuestions.QuestionID
INNER JOIN SurveyQuestions SQ ON SurveyQuestions.SurveyID = SQ.SurveyID
WHERE Answer IS NOT NULL AND Answer <> '' AND Answer <> 'N' and Answer <> 'No'
AND SQ.QuestionID = '0V100K999W2A79185T597P8F1Q5J'

SELECT QuestionID FROM SurveyQuestions 
INNER OUTER JOIN Questions ON Questions.QuestionID = SurveyQuestions.
 */
