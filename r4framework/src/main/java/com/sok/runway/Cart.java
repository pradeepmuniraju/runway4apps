package com.sok.runway;
import java.util.*;
import com.sok.framework.*;
public class Cart extends TableDataHash
{
	TableData order = null;
	TableData delivery = null;
	boolean completed = false;

	public void setCompleted(boolean b)
	{
		completed = true;
	}

	public boolean getCompleted()
	{
		return(completed);
	}

	public void setOrder(TableData hd)
	{
		order = hd;
	}

	public TableData getOrder()
	{
		return(order);
	}

	public TableData getDelivery()
	{
		return(delivery);
	}

	public void setDelivery(TableData delivery)
	{
		this.delivery = delivery;
	}

	public ProductBean nextProduct()
	{
		return((ProductBean)get(super.getIterator().next()));
	}

	public ProductBean getProduct(String key)
	{
		return((ProductBean)get(key));
	}

	public void putProduct(String key, ProductBean bean)
	{
		put(key,bean);
		super.clearIterator();
	}

	public void addTotals()
	{
		double totalcost = 0.0;
		double cost = 0.0;
		double gst = 0.0;
		int qty = 0;
		Iterator products = keySet().iterator();
		ProductBean product = null;
		while(products.hasNext())
		{
			product = (ProductBean)get(products.next());
			cost = cost + doubleValue(product.get("Cost"));
			gst = gst  + doubleValue(product.get("GST"));
			totalcost = totalcost + doubleValue(product.get("TotalCost"));
			qty = qty + Integer.parseInt((String)product.get("Quantity"));
		}
		((HashMap)order).put("Cost",new Double(cost));
		((HashMap)order).put("GST",new Double(gst));
		((HashMap)order).put("TotalCost",new Double(totalcost));
		((HashMap)order).put("Quantity",String.valueOf(qty));
	}

	static double doubleValue(Object obj)
	{
		if(obj instanceof Double)
		{
			return(((Double)obj).doubleValue());
		}
		else
		{
			return(Double.parseDouble((String)obj));
		}
	}

}
