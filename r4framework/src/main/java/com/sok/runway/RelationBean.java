package com.sok.runway;

import com.sok.framework.*;
public class RelationBean extends RowBean {
   
   public UserBean currentUser = null;
   
   public void setCurrentUser(UserBean currentUser) {
      this.currentUser = currentUser;
   }
   
   // Convenience functions, route to generic functions below
   
   // Company
   public boolean isNewCompanyStatus(String entityID, String statusID) {
      return isNewStatus("CompanyView", "CompanyID", entityID, statusID);
   }
   
   public void updateCompanyStatusHistory(String entityStatusID, String entityID, String statusID) {
      updateStatusHistory("CompanyStatus", entityStatusID, entityID, "CompanyID", statusID);
   }

   // CompanyProductGroup
   public boolean isNewCompanyProductGroupStatus(String entityID, String statusID) {
      return isNewStatus("CompanyProductGroupView", "CompanyProductGroupID", entityID, statusID);
   }
   
   public void updateCompanyProductGroupStatusHistory(String entityStatusID, String entityID, String statusID) {
      updateStatusHistory("CPGStatus", entityStatusID, entityID, "CompanyProductGroupID", statusID);
   }

   // Contact
   public boolean isNewContactStatus(String entityID, String statusID) {
      return isNewStatus("ContactView", "ContactID", entityID, statusID);
   }
   
   public void updateContactStatusHistory(String entityStatusID, String entityID, String statusID) {
      updateStatusHistory("ContactStatus", entityStatusID, entityID, "ContactID", statusID);
   }

   // Contact Campaign
   public boolean isNewContactCampaignStatus(String entityID, String statusID) {
      return isNewStatus("ContactCampaignView", "ContactCampaignID", entityID, statusID);
   }
   
   public void updateContactCampaignStatusHistory(String entityStatusID, String entityID, String statusID) {
      updateStatusHistory("ContactCampaignStatus", entityStatusID, entityID, "ContactCampaignID", statusID);
   }
   
   // ContactProductGroup
   public boolean isNewContactProductGroupStatus(String entityID, String statusID) {
      return isNewStatus("ContactProductGroupView", "ContactProductGroupID", entityID, statusID);
   }
   
   public void updateContactProductGroupStatusHistory(String entityStatusID, String entityID, String statusID) {
      updateStatusHistory("ContactPGStatus", entityStatusID, entityID, "ContactProductGroupID", statusID);
   }
   
   // Opportunity
   public boolean isNewOpportunityStatus(String entityID, String statusID) {
      return isNewStatus("OpportunityView", "OpportunityID", entityID, statusID);
   }
   
   public void updateOpportunityStatusHistory(String entityStatusID, String entityID, String statusID) {
      updateStatusHistory("OpportunityStatus", entityStatusID, entityID, "OpportunityID", statusID);
   }
   
   // SimpleOpportunity
   public boolean isNewSimpleOpportunityStatus(String entityID, String statusID) {
      return isNewStatus("SimpleOpportunityView", "OpportunityID", entityID, statusID);
   }
   
   public void updateSimpleOpportunityStatusHistory(String entityStatusID, String entityID, String statusID) {
      updateStatusHistory("OpportunityStatus", entityStatusID, entityID, "OpportunityID", statusID);
   }   
   
   // Order
   public boolean isNewOrderStatus(String entityID, String statusID) {
      return isNewStatus("OrderView", "OrderID", entityID, statusID);
   }
   
   // OrderItems/ Tasks
   public boolean isNewOrderItemStatus(String entityID, String statusID) {
      return isNewStatus("OrderItemsView", "OrderItemID", entityID, statusID);
   }
   
   public void updateOrderStatusHistory(String entityStatusID, String entityID, String statusID) {
      updateStatusHistory("OrderStatus", entityStatusID, entityID, "OrderID", statusID);
   }
   
   // OrderItems/ Tasks
   public void updateOrderItemsStatusHistory(String entityStatusID, String entityID, String statusID) {
      updateStatusHistory("TaskStatus", entityStatusID, entityID, "OrderItemID", statusID);
   }
   
   // Generic function called by above
   public boolean isNewStatus(String viewSpec, String pkField, String entityID, String statusID) {
      if (statusID != null && statusID.length() > 0) {
         clear();
         setViewSpec(viewSpec);
         setColumn(pkField, entityID);
         setAction("select");
         doAction();
         return !getString("CurrentStatusID").equals(statusID) || getString("CurrentStatusID").length() == 0;
      }
      return false;
   }
   
   public void updateStatusHistory(String specName, String id, String foreignID, String foreignIDName, String statusID) {
      clear();
      TableSpec tSpec = SpecManager.getTableSpec(specName);
      setTableSpec(tSpec);
   	setColumn(tSpec.getPrimaryKeyName(), id);
   	setColumn(foreignIDName, foreignID);
   	setColumn("StatusID", statusID);
   	setColumn("CreatedDate", "NOW");
   	setColumn("CreatedBy", currentUser.getCurrentUserID());
   	setAction(ActionBean.INSERT);
   	doAction();
   }
}
   
