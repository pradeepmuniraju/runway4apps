package com.sok.runway;

import com.sok.framework.*;
import com.sok.runway.offline.*;
import java.io.*;

public class OfflineProcessor implements Runnable
{
	OfflineProcess process;

	public OfflineProcessor(OfflineProcess process)
	{
		this.process = process;
	}

	public void run()
	{
		try
		{
			process.runProcess();
		}
		catch(Exception e)
		{
			process.appendError(ActionBean.writeStackTraceToString(e));
		}
		finally
		{
			sendStatus();
		}
	}

	public void sendStatus()
	{
	   if (process.sendEmail()) {
	      
		   OfflineNotificationSender notificationSender = new OfflineNotificationSender();
	      notificationSender.notify(process);
	      
   	}
	}
}
