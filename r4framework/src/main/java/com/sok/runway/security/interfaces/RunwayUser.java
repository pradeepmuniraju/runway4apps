package com.sok.runway.security.interfaces;


public interface RunwayUser {
	
	public String getUserID();
	
	public String getFirstName();
	
	public String getLastName();
	
	public String getUserName();
}