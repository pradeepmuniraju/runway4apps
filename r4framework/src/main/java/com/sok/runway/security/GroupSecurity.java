package com.sok.runway.security;

import com.sok.runway.*;
import com.sok.framework.*;

import java.sql.*;
import java.util.*;
import javax.servlet.ServletRequest;

public class GroupSecurity extends RunwayEntity implements SecurityKeys {
      
   private ArrayList moduleList = null;
       
   public GroupSecurity(Connection con) {
      super(con, "GroupSecurityView");
   }
   
   public GroupSecurity(Connection con, String groupID) {
      this(con);
      load(groupID);
   }
   
   public String getGroupSecurityID() {
      return getField("GroupSecurityID");
   }
   
   public String getGroupID() {
      return getField("GroupID");
   }
   
   public String getPermissionID() {
      return getField("PermissionID");
   }   
   
   public String getModule() {
      return getField("Module");
   }
   
   public boolean hasPermission(String permission) {
      return YES.equals(getField(permission));
   }
   
	public boolean canAccess() {
	   return hasPermission(CanAccess);
	}
	
	public boolean canCreate() {
	   return hasPermission(CanCreate);
	}
	
	public boolean canEdit() {
	   return hasPermission(CanEdit);
	}
	
	public boolean canDelete() {
	   return hasPermission(CanDelete);
	}
	
	public boolean canEditRep() {
	   return hasPermission(CanEditRep);
	}
	
	public boolean canEditAssigned() {
	   return hasPermission(CanEditAssigned);
	}
	
	public boolean canLink() {
	   return hasPermission(CanLink);
	}
	
	public boolean canView() {
	   return hasPermission(CanView);
	}
	
	public boolean canViewAssigned() {
	   return hasPermission(CanViewAssigned);
	}
	
	public boolean canAdminStatus() {
	   return hasPermission(CanAdminStatus);
	}
	
	public boolean canMerge() {
	   return hasPermission(CanMerge);
	}
   
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("GroupSecurity");
      if (val == null) {
         val = new Validator();
         //val.addMandatoryField("GroupID","SYSTEM ERROR");
         //val.addMandatoryField("PermissionID","SYSTEM ERROR");
         val.addMandatoryField("GroupSecurityID","SYSTEM ERROR");
         val.addMandatoryField("Module","Group Security must have a name");
         
         Validator.addValidator("GroupSecurity", val);
      }
      return val;
   }

}