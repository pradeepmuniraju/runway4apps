package com.sok.runway.security;

import com.sok.runway.*;
import com.sok.framework.*;

import java.sql.*;
import java.util.*;
import javax.servlet.ServletRequest;

public class Permission extends AuditEntity {
      
   private HashMap moduleList = null;
   
   public Permission(Connection con) {
      super(con, "PermissionView");
   }
   
   public Permission(ServletRequest request) {
       super(request, "PermissionView");
       populateFromRequest(request.getParameterMap());
   }
   
   public Permission(Connection con, String permissionID) {
      this(con);
      load(permissionID);
   }
   
   public Permission(Connection con, String viewspec, String id) {
      super();
      this.setConnection(con);
      this.setViewSpec(viewspec);
      load(id);
   }   
   
   
   public String getPermissionID() {
      return getField("PermissionID");
   }   
   
   public String getName() {
      return getField("Name");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("Group");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("Permission","SYSTEM ERROR");
         val.addMandatoryField("Name","Permission must have a name");
         
         Validator.addValidator("Permission", val);
      }
      return val;
   }
   
   public GroupSecurity getGroupSecurity(String module) {
      if (moduleList == null) {
         loadModules(getConnection());
      }
      return (GroupSecurity)moduleList.get(module);
   }
   
   public synchronized void loadModules(Connection con) { 
      moduleList = new HashMap(); 
      loadModulesWith(con, "PermissionID", getPermissionID());
   }
   
   protected void loadModulesWith(Connection con, String idname, String idvalue){
      GenRow groupSecurityData = new GenRow();
      groupSecurityData.setTableSpec("GroupSecurity");
      if (con != null) {
         groupSecurityData.setConnection(con);
      }
      else {
         con = ActionBean.connect(groupSecurityData);
      }
      groupSecurityData.setParameter("-select#1","GroupSecurityID");
      //groupSecurityData.setParameter("GroupID",getGroupID());
      //groupSecurityData.setParameter("PermissionID",getPermissionID());
      groupSecurityData.setParameter(idname,idvalue);
      if(idname.length()!=0 && idvalue.length()!=0){
         groupSecurityData.getResults();
         while (groupSecurityData.getNext()) {
            GroupSecurity securityModule = new GroupSecurity(con, groupSecurityData.getData("GroupSecurityID"));
            moduleList.put(securityModule.getModule(), securityModule);
            securityModule.setConnection((Connection)null);
         }
         
         groupSecurityData.close();
      }
      if (groupSecurityData.getError().length() >0 ) {
         throw new RuntimeException(groupSecurityData.getError());
      }     
   }
}