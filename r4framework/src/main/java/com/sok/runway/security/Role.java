package com.sok.runway.security;

import com.sok.runway.*;
import com.sok.framework.*;

import java.sql.*;

public class Role extends SecuredEntity {
      
   public Role(Connection con) {
      super(con, "RoleView");
   }
   
   public Role(Connection con, String roleID) {
      this(con);
      load(roleID);
   }
   
   public String getRoleID() {
      return getField("RoleID");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("Role");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("RoleID","SYSTEM ERROR");
         val.addMandatoryField("Role","Role must have a name");
         val.addMandatoryField("GroupID","Role must have a security group.");
         
         Validator.addValidator("Role", val);
      }
      return val;
   }
   
   
   

}