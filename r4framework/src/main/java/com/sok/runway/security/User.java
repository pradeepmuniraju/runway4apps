package com.sok.runway.security;

import com.sok.runway.*;
import com.sok.runway.crm.Setup;
import com.sok.framework.*;
import com.sok.framework.generation.util.*;
import com.sok.runway.security.interfaces.*;

import java.sql.*;
import java.util.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class User extends AuditEntity implements RunwayUser{
   
	private static final Logger logger = LoggerFactory.getLogger(User.class);
	
   protected ArrayList<Group> groupList = null;
   protected HashMap roleList = null;
   protected HashMap groupSecurity = null;
   
   transient protected History recordsecuritycache = new History(50, true, false, false);
   
   //RowArrayBean groups = null;   
   
   /*
    * EmailClient types 
    */
   public static final int WEB_STANDARDS = 0; 
   public static final int OUTLOOK_2003 = 1; 
   public static final int OUTLOOK_2007 = 2; 
   
   public static final String[] clientTypes = {"Web Standards","Outlook 2003","Outlook 2007"}; 
   
   public User(Connection con) {
      super(con, "UserView");
   }
	
	public User(ServletRequest request) {
	    super(request, "UserView");
       populateFromRequest(request.getParameterMap());
	}
   
   public User(Connection con, String userID) {
      this(con);
      load(userID);
   }
   
   public String getUserID() {
      return getField("UserID");
   }
   
   public String getFirstName() {
      return getField("FirstName");
   }
	
   public String getLastName() {
      return getField("LastName");
   }
   
   public String getEmail() {
      return getField("Email");
   }
   
   public String getIPAddress() {
      return getField("IPAddress");
   }
   
   public String getUserName() {
	  return getField("Username");
   }
   
   public int getListSize() {
      int listSize = getInt("ListSize");
      if (listSize == 0) {
         listSize = 40;
      }
      return listSize;
   }
   
   public String getSetupID() {
      if (getField("SetupID").length() == 0) {
         return Setup.DEFAULT_SETUP_ID;
      }
      else {
         return getField("SetupID");
      }
   }
   
   public Setup getSetup() {
      Setup setup = null;
      if (getSetupID().length() != 0) {
         setup = new Setup(getConnection());
         setup.load(getSetupID());
         setup.setCurrentUser(getCurrentUser());
         if (!setup.isLoaded()) {
            setup = null;
         }
      }
      return setup;
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("User");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("UserID","SYSTEM ERROR");
         val.addMandatoryField("FirstName","User must have a first name");
         val.addMandatoryField("LastName","User must have a last name.");
         val.addMandatoryField("Username","User must have a username.");
         val.addMandatoryField("Password","User must have a password.");
         
         Validator.addValidator("User", val);
      }
      return val;
   }
   
   /**
    *  Group Methods
    */
	public void setGroups(ArrayList<Group> groupList) {
      this.groupList = groupList;
	}
	
	public Collection<Group> getGroups() {
	   if (groupList == null) {
	      loadGroups();
	   }
      return groupList;
	}
	
	public Group getGroup(int i)
	{
	   return(groupList.get(i));
	}
	
	public void loadGroups() {
	   loadGroups(getConnection());
	}
	
	public synchronized void loadGroups(Connection con)
	{  
      ArrayList<Group> groupList = new ArrayList<Group>();
	   if (getUserID().length() > 0) {
                  
   		GenRow groupData = new GenRow();
   		groupData.setViewSpec("UserGroupIDView"); //there is a backwards version for runway3
   		if (con != null) {
   		   groupData.setConnection(con);
   		}
   		else {
   		   //groupData.setJndiName(ActionBean.getJndiName());
   		   con = ActionBean.connect(groupData);
   		}
   		//groupData.setParameter("-select#1","GroupID");
   		//groupData.setParameter("-select#2","UserGroup.SectionCode");
   		groupData.setParameter("UserID",getUserID());
   		groupData.setParameter("-sort1","UserGroup.SectionCode");
   		groupData.getResults();

   		while (groupData.getNext()) {
   		   Group group = new Group(con, "UserGroupView", groupData.getData("UserGroupID"));
   		   group.loadModules(con);
   		   groupList.add(group);
   		   group.setConnection((Connection)null);
   		}
   		
   		groupData.close();
   		if (groupData.getError().length() >0 ) {
   		   throw new RuntimeException(groupData.getError());
   		}
   	}
      this.groupList = groupList;
	}
	
   public RecordSecurity[] getRecordSecurity(String relationname, TableData data)
   {
      if(relationname!=null)
      {
         TableSpec tspec = data.getTableSpec();
         return(getRecordSecurity(relationname, tspec.getPrimaryKeyName(), data.getString(tspec.getPrimaryKeyName())));
      }
      return(null);
   }
	
   public RecordSecurity[] getRecordSecurity(String relationname, String primaryname, String primarykey)
   {
      RecordSecurity[] codes = (RecordSecurity[])recordsecuritycache.getValue(primaryname+primarykey);
      if(codes == null)
      {
         codes = loadRecordSecurity(relationname, primaryname, primarykey);
      }
      return(codes);
   } 
   
   protected synchronized RecordSecurity[] loadRecordSecurity(String relationname, String primaryname, String primarykey)
   {
      String key = primaryname+primarykey;
      RecordSecurity[] codes = (RecordSecurity[])recordsecuritycache.getValue(primaryname+primarykey);
      if(codes == null)
      {
         GenRow row = new GenRow();
         row.setConnection(getConnection());
         row.setTableSpec("Groups");
         row.setParameter("-select1","SectionCode");
         row.setParameter("-select2",relationname+".RepUserID");
         row.setParameter("-join1",relationname);
         row.setParameter(relationname+"."+primaryname, primarykey);
         row.doAction("search");
         row.getResults(true);
         codes = new RecordSecurity[row.getSize()];
         for(int i=0; i<codes.length; i++)
         {
            row.getNext();
            codes[i] = new RecordSecurity(row.getString("SectionCode"), row.getString("RepUserID")) ;
         }  
         row.close();
         recordsecuritycache.append(key, codes);
      }
      return(codes);
   }
   
   public void removeRecordSecurity(String primaryname, String primarykey) {
	   recordsecuritycache.remove(primaryname+primarykey);
   }
   
   public History getSectionCodeCache()
   {
      return(recordsecuritycache);
   }
	
   /**
    *  Role Methods
    */   
	public void setRoles(HashMap roleList) {
      this.roleList = roleList;
	}
	
	public HashMap getRoles() {
	   if (roleList == null) {
	      loadRoles();
	   }
      return roleList;
	}
	
	public void loadRoles() {
	   loadRoles(getConnection());
	}
	public synchronized void loadRoles(Connection con)
	{  
      HashMap roleList = new HashMap();
	   if (getUserID().length() > 0) {
                  
   		GenRow roleData = new GenRow();
   		roleData.setTableSpec("UserRoles");
   		if (con != null) {
   		   roleData.setConnection(con);
   		}
   		else {
   		   //roleData.setJndiName(ActionBean.getJndiName());
   		   con = ActionBean.connect(roleData);
   		}
   		roleData.setParameter("-select#1","RoleID");
   		roleData.setParameter("UserID",getUserID());
   		roleData.getResults();
   		while (roleData.getNext()) {
   		   Role role = new Role(con, roleData.getData("RoleID"));
   		   roleList.put(roleData.getData("RoleID"), role);
   		   role.setConnection((Connection)null);
   		}
   		
   		roleData.close();
   		if (roleData.getError().length() >0 ) {
   		   throw new RuntimeException(roleData.getError());
   		}
   	}
      this.roleList = roleList;
	}
   
   /**
    *  Group Security Methods
    */
  /* public synchronized RowArrayBean getGroupSecurity(String groupID) {
	   if (groupSecurity == null) {
	      groupSecurity = new Hashtable();
	   }
	   RowArrayBean security = (RowArrayBean)groupSecurity.get(groupID);
	   if (security == null) {
	      security = new RowArrayBean();
   	   security.setViewSpec("GroupSecurityView");
   	   security.setJndiName(getJndiName());
   	   security.setColumn("GroupID", groupID);
   	   security.generateSQLStatement();
   	   security.getResults();
   	   groupSecurity.put(groupID, security);
   	   security.close();
	   }
	   return security;
	} */
    
    
   public boolean getPermission(String code, String task)
   {
      return(SecurityFactory.getSecurityFactory().getPermission(this, code, task));
   }

   public String getPermissionString(String task)
   {
      return(SecurityFactory.getSecurityFactory().getPermissionString(this, task));
   }	
	
   public boolean getPermission(String task)
   {
      return(SecurityFactory.getSecurityFactory().getPermission(this, task));
   }	
	
   public boolean getRestriction(String task)
   {
      return(SecurityFactory.getSecurityFactory().getRestriction(this, task));
   }     
   
   public boolean getPermission(String cuserid, String code, String task)
   {
      return getPermission(code, task);
   }   
    
   public boolean getModulePermission(String tableName, String sectionCode, String permission) {
      return(SecurityFactory.getSecurityFactory().getModulePermission(this, tableName, sectionCode, permission));
   }   
   
   public boolean getModulePermission(TableData data, String relationname, String permission) {
      User.RecordSecurity[] recordsecurity = getRecordSecurity(relationname, data);
      return(SecurityFactory.getSecurityFactory().hasModulePermission(this, data, recordsecurity, permission));
   }
   
   //for modules with a module name that is the tablename
   public boolean hasModulePermission(String permission, TableData data) {
      return(SecurityFactory.getSecurityFactory().getModulePermission(this, data, null, permission, null));
   }    
   
   //for modules with a module name that is not the tablename
   public boolean hasModulePermission(String modulename, String permission, TableData data) {
      return(SecurityFactory.getSecurityFactory().getModulePermission(this, data, modulename, permission, null));
   }    

   //for modules access
   public boolean hasModulePermission(String modulename, String permission) {
      return(SecurityFactory.getSecurityFactory().getModulePermission(this, modulename, null, permission));
   }     
   
   //for modules that cannot have multiple groups
   public boolean hasModulePermission(String modulename, String permission, String sectioncode) {
      return(SecurityFactory.getSecurityFactory().getModulePermission(this, modulename, sectioncode, permission));
   }   
   
   //for modules that can have multiple groups, grouprelation is the relationship to assigned groups
   public boolean hasModulePermission(String modulename, String permission, String grouprelation, TableData data) {
      return(SecurityFactory.getSecurityFactory().getModulePermission(this, data, modulename, permission, grouprelation));
   }
   
   //id is groupid in runway3 permissionid in runway4
   protected synchronized void loadGroupSecurity(String idname, String id) {
      
      RowArrayBean security = (RowArrayBean)groupSecurity.get(id);
      if(security == null)
      {
          security = new RowArrayBean();
            security.setViewSpec("GroupSecurityView");
            security.setColumn(idname, id);
            security.generateSQLStatement();
            security.getResults();
            groupSecurity.put(idname, security);
            security.close();
      }
   }    
   

   /**
    * @deprecated
    */   
   public RowArrayBean getGroupSecurity(String id) { 
      return(getGroupSecurity("GroupID", id));
   }   
   
   //id is groupid in runway3 permissionid in runway4
   public RowArrayBean getGroupSecurity(String idname, String id) { 
      if (groupSecurity == null) {
         groupSecurity = new HashMap();
      }
      RowArrayBean security = (RowArrayBean)groupSecurity.get(id);
      if (security == null) {
         loadGroupSecurity(idname, id);
         security = (RowArrayBean)groupSecurity.get(id);
      }
      return security;
   }    

	public boolean canAdmin() {
	   return SecurityFactory.getSecurityFactory().canAdmin(this);
	}
	public boolean canManage() {
	   return SecurityFactory.getSecurityFactory().canManage(this);
	}
	public boolean canManage(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canManage(this, entity);
	}
	public boolean canUseCMS() {
	   return SecurityFactory.getSecurityFactory().canUseCMS(this);
	}
	public boolean canReport() {
	   return SecurityFactory.getSecurityFactory().canReport(this);
	}
	public boolean canUseInbox() {
	   return SecurityFactory.getSecurityFactory().canUseInbox(this);
	}
	public boolean canUseCalendar() {
	   return SecurityFactory.getSecurityFactory().canUseCalendar(this);
	}
   public boolean canAdminRowProperties() {
      return SecurityFactory.getSecurityFactory().canAdminRowProperties(this);
   }
	public boolean canSendEmails() {
	   return SecurityFactory.getSecurityFactory().canSendEmails(this);
	}
	public boolean canSendEmails(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canSendEmails(this, entity);
	}
	public boolean canSendEmailsOffline() {
	   return SecurityFactory.getSecurityFactory().canSendEmailsOffline(this);
	}
	public boolean canSendEmailsOffline(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canSendEmailsOffline(this, entity);
	}
	public boolean canDeleteSentEmails() {
	   return SecurityFactory.getSecurityFactory().canDeleteSentEmails(this);
	}
	public boolean canDeleteSentEmails(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canDeleteSentEmails(this, entity);
	}
	public boolean canEditOffline() {
	   return SecurityFactory.getSecurityFactory().canEditOffline(this);
	}
	public boolean canEditOffline(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canEditOffline(this, entity);
	}
	public boolean canExport() {
	   return SecurityFactory.getSecurityFactory().canExport(this);
	}
	public boolean canExport(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canExport(this, entity);
	}

   /**
    *  Group Module Security Methods
    */

	public boolean canAccess(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canAccess(this, entity);
	}
	
   public boolean canAccess(TableData data, String groupsrelation) {
      return SecurityFactory.getSecurityFactory().canAccess(this, data, groupsrelation);
   }	
	
	public boolean canAccess(String tableName) {
	   return SecurityFactory.getSecurityFactory().canAccess(this, tableName);
	}

	/*public boolean canEditUser() {
	   return getModulePermission("Users", null, CanEdit);
	}
	public boolean canDeleteUser() {
	   return getModulePermission("Users", null, CanDelete);
	}*/

	public boolean canCreate(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canCreate(this, entity);
	}
	
   public boolean canCreate(TableData data, String groupsrelation) {
      return SecurityFactory.getSecurityFactory().canCreate(this, data, groupsrelation);
   }	
	
	public boolean canCreate(String tableName) {
	   return SecurityFactory.getSecurityFactory().canCreate(this, tableName);
	}
	
	public boolean canView(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canView(this, entity);
	}
	
   public boolean canView(TableData data, String groupsrelation) {
      return SecurityFactory.getSecurityFactory().canView(this, data, groupsrelation);
   }	
	
   public boolean canViewAssigned(TableData data, String groupsrelation, String repUserID ) {
	      return SecurityFactory.getSecurityFactory().canViewAssigned(this, data, groupsrelation, repUserID);
	   }	
		
	public boolean canView(String tableName, String sectionCode) {
	   return SecurityFactory.getSecurityFactory().canView(this, tableName, sectionCode);
	}
	
	public boolean canViewAssigned(String tableName, String sectionCode, String repUserID) {
		   return SecurityFactory.getSecurityFactory().canViewAssigned(this, tableName, sectionCode, repUserID);
		}
		
	public boolean canDelete(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canDelete(this, entity);
	}
	
   public boolean canDelete(TableData data, String groupsrelation) {
      return SecurityFactory.getSecurityFactory().canDelete(this, data, groupsrelation);
   }	
	
	public boolean canDelete(String tableName, String sectionCode) {
	   return SecurityFactory.getSecurityFactory().canDelete(this, tableName, sectionCode);
	}
	
	public boolean canEdit(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canEdit(this, entity);
	}
	
   public boolean canEdit(TableData data, String groupsrelation) {
      return SecurityFactory.getSecurityFactory().canEdit(this, data, groupsrelation);
   }	
	
	public boolean canEdit(String tableName, String sectionCode) {
		logger.debug("canEdit({}, {})",tableName, sectionCode);
	   return SecurityFactory.getSecurityFactory().canEdit(this, tableName, sectionCode);
	}
	
	public boolean canEditAssigned(SecuredEntity entity) {
		   return SecurityFactory.getSecurityFactory().canEdit(this, entity);
		}
		
	   public boolean canEditAssigned(TableData data, String groupsrelation, String repUserID) {
	      return SecurityFactory.getSecurityFactory().canEdit(this, data, groupsrelation, repUserID);
	   }	
		
		public boolean canEditAssigned(String tableName, String sectionCode, String repUserID) {
		   return SecurityFactory.getSecurityFactory().canEdit(this, tableName, sectionCode, repUserID);
		}
		
	public boolean canEditRep(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canEditRep(this, entity);
	}
	
   public boolean canEditRep(TableData data, String groupsrelation) {
      return SecurityFactory.getSecurityFactory().canEditRep(this, data, groupsrelation);
   }	
	
   public boolean canEditRep(TableData data, String groupsrelation, String repUserID) {
	      return SecurityFactory.getSecurityFactory().canEditRep(this, data, groupsrelation, repUserID);
	   }	
		
	public boolean canEditRep(String tableName, String sectionCode) {
	   return SecurityFactory.getSecurityFactory().canEditRep(this, tableName, sectionCode);
	}
	
	public boolean canEditRepOnly(SecuredEntity entity) {
		   return SecurityFactory.getSecurityFactory().canEditRepOnly(this, entity);
		}
		
	   public boolean canEditRepOnly(TableData data, String groupsrelation) {
	      return SecurityFactory.getSecurityFactory().canEditRepOnly(this, data, groupsrelation);
	   }	
		
	   public boolean canEditRepOnly(TableData data, String groupsrelation, String repUserID) {
		      return SecurityFactory.getSecurityFactory().canEditRepOnly(this, data, groupsrelation, repUserID);
		   }	
			
		public boolean canEditRepOnly(String tableName, String sectionCode) {
		   return SecurityFactory.getSecurityFactory().canEditRepOnly(this, tableName, sectionCode);
		}
		
	public boolean canLink(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canLink(this, entity);
	}
	
   public boolean canLink(TableData data, String groupsrelation) {
      return SecurityFactory.getSecurityFactory().canLink(this, data, groupsrelation);
   }	
	
	public boolean canLink(String tableName, String sectionCode) {
	   return SecurityFactory.getSecurityFactory().canLink(this, tableName, sectionCode);
	}
	
	public boolean canLinkAssigned(TableData data, String groupsrelation, String repUserID) {
	      return SecurityFactory.getSecurityFactory().canLink(this, data, groupsrelation, repUserID);
	   }
	
	public boolean canAdminStatus(TableData data, String groupsrelation) {
	   return SecurityFactory.getSecurityFactory().canAdminStatus(this, data, groupsrelation);
	}
	
   public boolean canAdminStatus(SecuredEntity entity) {
      return SecurityFactory.getSecurityFactory().canAdminStatus(this, entity);
   }	
	
	public boolean canAdminStatus(String tableName, String sectionCode) {
	   return SecurityFactory.getSecurityFactory().canAdminStatus(this, tableName, sectionCode);
	}
	
	public boolean canMerge(SecuredEntity entity) {
	   return SecurityFactory.getSecurityFactory().canMerge(this, entity);
	}
	
   public boolean canMerge(TableData data, String groupsrelation) {
      return SecurityFactory.getSecurityFactory().canMerge(this, data, groupsrelation);
   }	
	
   public boolean canMerge(TableData data, String groupsrelation, String repUserID) {
	      return SecurityFactory.getSecurityFactory().canMerge(this, data, groupsrelation, repUserID);
	   }	
		
	public boolean canMerge(String tableName, String sectionCode) {
	   return SecurityFactory.getSecurityFactory().canMerge(this, tableName, sectionCode);
	}
	
	public boolean canMerge(String tableName, String sectionCode, String repUserID) {
		   return SecurityFactory.getSecurityFactory().canMerge(this, tableName, sectionCode, repUserID);
		}
		
	public void setSearchConstraint(TableData search, String securityRelation) {
	   SecurityFactory.getSecurityFactory().setSearchConstraint(this, search, securityRelation);
	}
	
   public void setSearchConstraint(TableData search, String securityRelation, String restriction) {
      SecurityFactory.getSecurityFactory().setSearchConstraint(this, search, securityRelation, restriction);
   }
   
	
	public void setStatusConstraint(TableData search) {
	   SecurityFactory.getSecurityFactory().setStatusConstraint(this, search);
	}
	
	public void setExportConstraint(TableData search, String securityRelation) {
	   SecurityFactory.getSecurityFactory().setExportConstraint(this, search, securityRelation);
	}
	
	public void setBulkEditConstraint(TableData search, String securityRelation) {
	   SecurityFactory.getSecurityFactory().setBulkEditConstraint(this, search, securityRelation);
	}
	
	public void setBulkSendConstraint(TableData search, String securityRelation) {
	   SecurityFactory.getSecurityFactory().setBulkSendConstraint(this, search, securityRelation);
	}
	
	public void setManageConstraint(TableData search, String securityRelation) {
	   SecurityFactory.getSecurityFactory().setManageConstraint(this, search, securityRelation);
	}

	
	public static String getClientType(String value) { 
		return clientTypes[getClientInt(value)];
	}
	
	public static String getClientOptions(String value) { 
	   return getClientOptions(getClientInt(value)); 
   }
	
   private static int getClientInt(String value) { 
	   int val = 0; 
	   if(value != null && value.length()>0) { 
		   try { 
			   val = Integer.parseInt(value); 
		   } catch (Exception e) {} 
	   } 
	   return val; 
   }
	
   public String getClientOptions() { 
	   return getClientOptions(getInt("EmailClient"));
   }
	
   public static String getClientOptions(int value) { 
	   StringBuilder s = new StringBuilder(); 
	   
	   s.append("<option value=\"").append(WEB_STANDARDS).append("\""); 
	   if(value == WEB_STANDARDS) { s.append(" selected=\"selected\""); } 
	   s.append(">Web Standards</option>"); 
	   
	   s.append("<option value=\"").append(OUTLOOK_2003).append("\""); 
	   if(value == OUTLOOK_2003) { s.append(" selected=\"selected\""); } 
	   s.append(">Outlook 2003</option>"); 
	   
	   s.append("<option value=\"").append(OUTLOOK_2007).append("\""); 
	   if(value == OUTLOOK_2007) { s.append(" selected=\"selected\""); } 
	   s.append(">Outlook 2007</option>"); 
	   
	   return s.toString(); 
   }
   
	public static User getCurrentUser(HttpServletRequest request) { 
		User u = new User(request); 
		return u.getCurrentUser(); 
	}
	
	public class RecordSecurity
	{
	   public String sectioncode = null;
	   public String repuserid = null;
	   
	   public RecordSecurity(String sectioncode, String repuserid)
	   {
	      this.sectioncode = sectioncode;
	      this.repuserid = repuserid;
	   }
	}
}