package com.sok.runway.security;


public interface SecurityKeys {
   
   // Group Level Permnissions
	public static final String CanAdmin = "CanAdmin";
	public static final String CanManage = "CanManage";
	public static final String CanUseCMS = "CanUseCMS";
	public static final String CanReport = "CanReport";
	public static final String CanUseCalendar = "CanUseCalendar";
	public static final String CanUseInbox = "CanUseInbox";
	public static final String CanAdminRowProperties = "CanAdminRowProperties";
	public static final String CanSendEmails = "CanSendEmails";
	public static final String CanSendEmailsOffline = "CanSendEmailsOffline";
	public static final String CanDeleteSentEmails = "CanDeleteSentEmails";
	public static final String CanEditOffline = "CanEditOffline";
	public static final String CanExport = "CanExport";
	
   // GroupSecurity Level Permnissions
	public static final String CanAccess = "CanAccess";
	public static final String CanView = "CanView";
	public static final String CanViewAssigned = "CanViewAssigned";
	public static final String CanCreate = "CanCreate";
	public static final String CanEdit = "CanEdit";
	public static final String CanEditRep = "CanEditRep";
	public static final String CanEditAssigned = "CanEditAssigned";
	public static final String CanDelete = "CanDelete";
	public static final String CanLink = "CanLink";
	public static final String CanAdminStatus = "CanAdminStatus";
	public static final String CanMerge = "CanMerge";
	
	public static final String YES = "Y";
   
	static final String SECTION_CODE = "SectionCode";
	static final String REP_USER_ID = "RepUserID";
	
	public static final String SHA_1 = "SHA-1";
	public static final String UTF_8 = "UTF-8";
   
}