package com.sok.runway.security;

import com.sok.runway.*;
import com.sok.framework.*;

import java.sql.*;
import java.util.*;
import javax.servlet.ServletRequest;

public class Group extends AuditEntity {
      
   private HashMap moduleList = null;
   
   public Group(Connection con) {
      super(con, "GroupView");
   }
	
   public Group() {
      super();
   }
   
	public Group(ServletRequest request) {
	    super(request, "GroupView");
       populateFromRequest(request.getParameterMap());
	}
   
   public Group(Connection con, String groupID) {
      this(con);
      load(groupID);
   }
   
   public Group(Connection con, String viewspec, String id) {
      super();
      this.setConnection(con);
      this.setViewSpec(viewspec);
      load(id);
   }   
   
   public String getGroupID() {
      return getField("GroupID");
   }
   
   public String getPermissionID() {
      return getField("PermissionID");
   }   
   
   public String getName() {
      return getField("Name");
   }
   
   public String getSectionCode() {
      return getField("SectionCode");
   }
   
   public boolean canAccessChildGroups() {
      return !"N".equals(getField("CanAccessChildGroups"));
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("Group");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("GroupID","SYSTEM ERROR");
         val.addMandatoryField("Name","Group must have a name");
         
         Validator.addValidator("Group", val);
      }
      return val;
   }
   
   public GroupSecurity getGroupSecurity(String module) {
      if (moduleList == null) {
         loadModules(getConnection());
      }
      return (GroupSecurity)moduleList.get(module);
   }
	
	public synchronized void loadModules(Connection con) { 
	   moduleList = new HashMap(); 
      if(getPermissionID().length() >0) {
         loadModulesWith(con, "PermissionID",getPermissionID());
      }
      else if (getGroupID().length() > 0) {
         loadModulesWith(con, "GroupID",getGroupID());
   	}
	}
	
	protected void loadModulesWith(Connection con, String idname, String idvalue){
      GenRow groupSecurityData = new GenRow();
      groupSecurityData.setTableSpec("GroupSecurity");
      if (con != null) {
         groupSecurityData.setConnection(con);
      }
      else {
         groupSecurityData.setJndiName(ActionBean.getJndiName());
         con = ActionBean.connect(groupSecurityData);
      }
      groupSecurityData.setParameter("-select#1","GroupSecurityID");
      //groupSecurityData.setParameter("GroupID",getGroupID());
      //groupSecurityData.setParameter("PermissionID",getPermissionID());
      groupSecurityData.setParameter(idname,idvalue);
      groupSecurityData.getResults();
      while (groupSecurityData.getNext()) {
         GroupSecurity securityModule = new GroupSecurity(con, groupSecurityData.getData("GroupSecurityID"));
         moduleList.put(securityModule.getModule(), securityModule);
         securityModule.setConnection((Connection)null);
      }
      
      groupSecurityData.close();
      
      if (groupSecurityData.getError().length() >0 ) {
         throw new RuntimeException(groupSecurityData.getError());
      }	   
	}
}