package com.sok.runway.security;

import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.annotate.JsonProperty;

import com.sok.framework.GenRow;
import com.sok.framework.generation.IllegalConfigurationException;
import com.sok.runway.LockManager;
import com.sok.runway.RunwayEntity;
import com.sok.runway.UserBean;
import com.sok.runway.crm.activity.ActivityLogger;
import com.sok.runway.crm.activity.ActivityLogger.ActionType;
import com.sok.runway.crm.activity.Logable;
import com.sok.runway.externalInterface.beans.shared.PersonName;
import com.sok.service.crm.UserService;


public abstract class AuditEntity extends RunwayEntity  {
   
   private User user = null;
      
   public AuditEntity(){
      super();
   }
   
   public AuditEntity(Connection con, String viewSpec) {
      super(con, viewSpec);
   }
   
   public AuditEntity(ServletRequest request, String viewSpec) {
      super(request, viewSpec);
      
      if (request instanceof HttpServletRequest) {
    	  UserBean currentuser = UserService.getInstance().getCurrentUser((HttpServletRequest)request);
    	  if(currentuser != null) {
    		  setCurrentUser(currentuser.getUser());
    	  }
    	  /*
         HttpSession session = ((HttpServletRequest)request).getSession();
         if (session != null) {
            UserBean currentuser = (UserBean)session.getAttribute(SessionKeys.CURRENT_USER);
            if (currentuser != null) {
               //currentuser.getUser().setParameter("IPAddress",currentuser.getString("IPAddress));
               setCurrentUser(currentuser.getUser());
            }
         }
         */
      }      
   }
   
   public AuditEntity(Object con, String viewSpec) {
      super((Connection)con, viewSpec);
   }
   
   public void setCurrentUser(User user) {
      this.user = user;
   }
   
   public void setCurrentUser(String userid) { 
	   
	   this.user = new User(this.getConnection(),userid); 
   }
   
   public User getCurrentUser() {
      if (user == null) {
         throw new RuntimeException("No User set");
      }
      return user;
   }
   
   public String getCreatedBy() {
      return getField("CreatedBy");
   }
   
   public String getModifiedBy() {
      return getField("ModifiedBy");
   }
   
   //nb, read only json property
   @JsonProperty("CreatedBy")
   public PersonName getCreatedByName() {
	   return new PersonName(getField("OwnFirstName"), getField("OwnLastName"));
   }
   
   //nb, read only json property
   @JsonProperty("ModifiedBy")
   public PersonName getModifiedByName() {
	   return new PersonName(getField("ModFirstName"), getField("ModLastName"));
   }
   
   //nb, read only json property
   @JsonProperty("CreatedDate")
   public Long getCreatedDateLong() {	//for serialization only, not internal use.
	   try { 
		   Date d = getDate("CreatedDate");
		   if(d != null) return new Long(d.getTime());
	   } catch (IllegalConfigurationException iec) { }
	   return null;
   }
   
   //nb, read only json property
   @JsonProperty("ModifiedDate")
   public Long getModifiedDateLong() {	//for serialization only, not internal use.
	   try { 
		   Date d = getDate("ModifiedDate");
		   if(d != null) return new Long(d.getTime());
	   } catch (IllegalConfigurationException iec) { }
	   return null;
   }
   
   public Date getCreatedDate() {
      return getDate("CreatedDate");
   }
   
   public Date getModifiedDate() {
      return getDate("ModifiedDate");
   }
   
   public void beforeSave() {
	   super.beforeSave();
      setField("ModifiedDate", "NOW");
      setField("ModifiedBy", getCurrentUser().getUserID());
   }
   
   public void beforeInsert() {
	   super.beforeInsert();
      setField("CreatedDate", "NOW");
      setField("CreatedBy", getCurrentUser().getUserID());
   }
   
   public void postSave() {
	   if(this instanceof Logable) 
		   ActivityLogger.getActivityLogger().logActivity((Logable)this, bean.getAction());
	   super.postSave();
   }
   
   public void postDelete() {
	   if(this instanceof Logable) 
		   ActivityLogger.getActivityLogger().logActivity((Logable)this, ActionType.Deleted);
	   super.postDelete();
   }
   
   /*
    *  Created By : Puja Shah
    *  Created Date : 31st July 2008
    *  Purpose : To get the lock on the table when there are two simultanous update on table.
   */
   public User getLock() {
	   LockManager.unlockAll(getCurrentUser(), this.getTableName());
	   User edituser = LockManager.lock(((GenRow)this.getRecord()).getLockKey(),getCurrentUser());
	   return edituser;
   }
}