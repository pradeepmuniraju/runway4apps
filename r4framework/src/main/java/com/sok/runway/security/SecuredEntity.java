package com.sok.runway.security;

import java.sql.Connection;
import javax.servlet.ServletRequest;

public abstract class SecuredEntity extends AuditEntity implements SecurityKeys {
   
   //private GenRow activityLog = null;   
   String grouprelation = null;
   
   public SecuredEntity(Connection con, String viewSpec) {
      super(con, viewSpec);
   }
   
   public SecuredEntity(ServletRequest request, String viewSpec) {
      super(request, viewSpec);
   }
   
   public SecuredEntity(Object con, String viewSpec) {
      super((Connection)con, viewSpec);
   }
   
   public void setGroupsRelation(String relation){
      grouprelation = relation;
   }
   
   public String getGroupsRelation(){
      return(grouprelation);
   }
      
   
   //public abstract String getEntityName();
   
   public String getSectionCode() {
      return getField("SectionCode");
   }
   
   public String getGroupID() {
      return getField("GroupID");
   }
   
   public void setGroupID(String groupID) {
      setField("GroupID", groupID);
   }
   
   public String getRepUserID() {
      return getField("RepUserID");
   }
   
   public User getRepUser() {
      User repUser = null;
      if (getRepUserID().length() != 0) {
         repUser = new User(getConnection());
         repUser.load(getRepUserID());
         repUser.setCurrentUser(getCurrentUser());
         if (!repUser.isLoaded()) {
            repUser = null;
         }
      }
      return repUser;
   }
   
   public void beforeSave() {
      super.beforeSave();
      
      String groupID = getGroupID();
      
      if (groupID.length() == 0) {
         
         User user = getCurrentUser();
         groupID = getCurrentUser().getField("DefaultGroupID");
         
         // DefaultGroupID is mandatory in User, should not get to here, this is merely a backup.
         if (groupID.length() == 0) {
            
            for (Group group : user.getGroups()) {
               
               if (group.getField("Applicable").startsWith("Y")) {
                  groupID = group.getPrimaryKey();
               }
            }
         }
      }
      setGroupID(groupID);
   }
   
   /**
    *  Group Module Security Methods
    */

	public boolean canAccess() {
	   return getCurrentUser().canAccess(this);
	}
	
	public boolean canCreate() {
	   return getCurrentUser().canCreate(this);
	}
	
	public boolean canView() {
	   return getCurrentUser().canView(this);
	}
	
	public boolean canDelete() {
	   return getCurrentUser().canDelete(this);
	}
	
	public boolean canEdit() {
	   return getCurrentUser().canEdit(this);
	}
	
	public boolean canEditRep() {
	   return getCurrentUser().canEditRep(this);
	}

	public boolean canLink() {
	   return getCurrentUser().canLink(this);
	}
	
	public boolean canAdminStatus() {
	   return getCurrentUser().canAdminStatus(this);
	}
	
	public boolean canMerge() {
	   return getCurrentUser().canMerge(this);
	}
	

}