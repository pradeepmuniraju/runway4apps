package com.sok.runway.security;

import com.sok.runway.*;
import com.sok.framework.*;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecurityFactory implements SecurityKeys {

	private static final Logger logger = LoggerFactory.getLogger(SecurityFactory.class);

	static final String NO_VIEWABLE_GROUPS= "NO VIEWABLE GROUPS";

	private static SecurityFactory securityFactory = null;

	public static SecurityFactory getSecurityFactory() {
		if (securityFactory == null) {
			securityFactory = new SecurityFactory();
		}
		return securityFactory;
	}

	public static void setSecurityFactory(SecurityFactory security) {
		securityFactory = security;
	}

	public SecurityFactory() {
		SecurityFactory.securityFactory = this;
	}

	/**
	 *  Group Security Methods
	 */
	public boolean canAdmin(User user) {
		return hasPermission(user, CanAdmin);
	}
	public boolean canManage(User user) {
		return hasPermission(user, CanManage);
	}
	public boolean canManage(User user, SecuredEntity entity) {
		return hasPermission(user, CanManage, entity);
	}
	public boolean canUseCMS(User user) {
		return hasPermission(user, CanUseCMS);
	}
	public boolean canReport(User user) {
		return hasPermission(user, CanReport);
	}
	public boolean canUseInbox(User user) {
		return hasPermission(user, CanUseInbox);
	}
	public boolean canUseCalendar(User user) {
		return hasPermission(user, CanUseCalendar);
	}
	public boolean canAdminRowProperties(User user) {
		return hasPermission(user, CanAdminRowProperties);
	}
	public boolean canSendEmails(User user) {
		return hasPermission(user, CanSendEmails);
	}
	public boolean canSendEmails(User user, SecuredEntity entity) {
		return hasPermission(user, CanSendEmails, entity);
	}
	public boolean canSendEmailsOffline(User user) {
		return hasPermission(user, CanSendEmailsOffline);
	}
	public boolean canSendEmailsOffline(User user, SecuredEntity entity) {
		return hasPermission(user, CanSendEmails, entity);
	}
	public boolean canDeleteSentEmails(User user) {
		return hasPermission(user, CanDeleteSentEmails);
	}
	public boolean canDeleteSentEmails(User user, SecuredEntity entity) {
		return hasPermission(user, CanDeleteSentEmails, entity);
	}
	public boolean canEditOffline(User user) {
		return hasPermission(user, CanEditOffline);
	}
	public boolean canEditOffline(User user, SecuredEntity entity) {
		return hasPermission(user, CanEditOffline, entity);
	}
	public boolean canExport(User user) {
		return hasPermission(user, CanExport);
	}
	public boolean canExport(User user, SecuredEntity entity) {
		return hasPermission(user, CanExport, entity);
	}

	public boolean canAccess(User user, TableData data, String grouprelation) {
		return hasModulePermission(user, data, user.getRecordSecurity(grouprelation, data), CanAccess);
	}	

	public boolean canAccess(User user, SecuredEntity entity) {
		return canAccess(user, entity.getRecord(), entity.getGroupsRelation());
	}

	public boolean canAccess(User user, String tableName) {
		return hasModuleSinglePermission(user, tableName, null, CanAccess);
	}

	/*public boolean canEditUser() {
	   return getModulePermission("Users", null, CanEdit);
	}
	public boolean canDeleteUser() {
	   return getModulePermission("Users", null, CanDelete);
	}*/

	public boolean canCreate(User user, SecuredEntity entity) {
		return canCreate(user, entity.getTableName());
	}

	public boolean canCreate(User user, String tableName) {
		if (canAccess(user, tableName)) {
			return hasModuleSinglePermission(user, tableName, null, CanCreate);
		}
		return false;
	}

	public boolean canCreate(User user, TableData data, String grouprelation) {
		if (canAccess(user, data, grouprelation)) {
			return hasModulePermission(user, data, user.getRecordSecurity(grouprelation, data), CanCreate);
		}
		return false;
	}	

	public boolean canView(User user, SecuredEntity entity) {
		return canView(user, entity.getRecord(), entity.getGroupsRelation(), entity.getRepUserID());
	}

	public boolean canView(User user, String tableName, String sectioncode) {
		return canView(user, tableName, sectioncode, null);
	}

	public boolean canView(User user, TableData data, String grouprelation) {
		return(canView(user, data, grouprelation, data.getString(REP_USER_ID)));
	}	

	public boolean canViewAssigned(User user, String tableName, String sectioncode, String repUserID) {
		return canView(user, tableName, sectioncode, repUserID);
	}

	public boolean canViewAssigned(User user, TableData data, String grouprelation) {
		return(canView(user, data, grouprelation, data.getString(REP_USER_ID)));
	}	

	public boolean canViewAssigned(User user, TableData data, String grouprelation, String repUserID) {
		return(canView(user, data, grouprelation, repUserID));
	}	

	public boolean canView(User user, String tableName, String sectioncode, String repUserID) {
		if(logger.isTraceEnabled()) logger.debug("canView(user={},table={},code={},rep={}", new String[]{user != null ? user.getUserName(): "null user", tableName, sectioncode, repUserID});
		if (canAccess(user, tableName)) {
			if (hasModuleSinglePermission(user, tableName, sectioncode, CanView)) {
				return true;
			}
			else if (user.getUserID().equals(repUserID)) {
				return hasModuleSinglePermission(user, tableName, sectioncode, CanViewAssigned);
			}
		} else if(logger.isTraceEnabled()) logger.trace("no access");
		return false;
	}

	public boolean canView(User user, TableData data, String grouprelation, String repUserID) {
		String tableName = data.getTableSpec().getTableName();
		User.RecordSecurity[] recordsecurity = user.getRecordSecurity(grouprelation, data);
		if (canAccess(user, tableName)) {
			if (hasModulePermission(user, data, recordsecurity, CanView)) {
				return true;
			}
			else if (isRep(user, repUserID, recordsecurity)) {
				return hasModulePermission(user, data, recordsecurity, CanViewAssigned);
			}
		}
		return false;
	}	

	public boolean canDelete(User user, SecuredEntity entity) {
		return canDelete(user, entity.getRecord(), entity.getGroupsRelation());
	}

	public boolean canDelete(User user, String tableName, String sectioncode) {
		if (canView(user, tableName, sectioncode)) {
			return hasModuleSinglePermission(user, tableName, sectioncode, CanDelete);
		}
		return false;
	}

	public boolean canDelete(User user, TableData data, String grouprelation) {
		logger.debug("canDelete");
		if (canView(user, data, grouprelation, null)) {
			logger.debug("canView");
			return hasModulePermission(user, data, user.getRecordSecurity(grouprelation, data), CanDelete);
		}
		logger.debug("cannotView");
		return false;
	}	

	public boolean canEdit(User user, SecuredEntity entity) {
		return canEdit(user, entity.getRecord(), entity.getGroupsRelation(), entity.getRepUserID());
	}


	public boolean canEdit(User user, String tableName, String sectionCode) {
		if(logger.isDebugEnabled()) logger.debug("canEdit({}, {}, {})", new String[]{user != null ? user.getUserName() : "null", tableName, sectionCode});
		return canEdit(user, tableName, sectionCode, null);
	}

	public boolean canEdit(User user, String tableName, String sectionCode, String repUserID) {
		if(logger.isDebugEnabled()) logger.debug("canEdit({}, {}, {}, {})", new String[]{user != null ? user.getUserName() : "null", tableName, sectionCode, repUserID});
		if (canAccess(user, tableName)) {
			if (hasModuleSinglePermission(user, tableName, sectionCode, CanEdit)) {
				return true;
			}
			else if (user.getUserID().equals(repUserID)) {
				return hasModuleSinglePermission(user, tableName, sectionCode, CanEditAssigned);
			}
		}
		return false;
	}

	public boolean canEdit(User user, TableData data, String grouprelation) {
		return(canEdit(user, data, grouprelation, data.getString(REP_USER_ID)));
	}

	public boolean canEdit(User user, TableData data, String grouprelation, String repUserID) {
		User.RecordSecurity[] recordsecurity = user.getRecordSecurity(grouprelation, data);
		if (canAccess(user, data, grouprelation)) {
			if (hasModulePermission(user, data, recordsecurity, CanEdit)) {
				return true;
			}
			else if (isRep(user, repUserID, recordsecurity)) {
				return hasModulePermission(user, data, recordsecurity, CanEditAssigned);
			}
		}
		return false;
	}	

	public boolean canEditRep(User user, SecuredEntity entity) {
		return canEditRep(user, entity.getTableName(), entity.getSectionCode(), entity.getRepUserID());
	}

	public boolean canEditRep(User user, String tableName, String sectioncode) {
		return canEditRep(user, tableName, sectioncode, null);
	}

	public boolean canEditRep(User user, String tableName, String sectioncode, String repUserID) {
		if (canEdit(user, tableName, sectioncode)) {
			if (hasModuleSinglePermission(user, tableName, sectioncode, CanEditRep)) {
				return true;
			}
		}
		return false;
	}

	public boolean canEditRep(User user, TableData data, String grouprelation) {
		return(canEditRep(user, data, grouprelation, data.getString(REP_USER_ID)));
	}  	

	public boolean canEditRep(User user, TableData data, String grouprelation, String repUserID) {
		User.RecordSecurity[] recordsecurity = user.getRecordSecurity(grouprelation, data);
		if (canEdit(user, data, grouprelation, repUserID)) {
			if (hasModulePermission(user, data, recordsecurity, CanEditRep)) {
				return true;
			}
		}
		return false;
	}	

	public boolean canEditRepOnly(User user, SecuredEntity entity) {
		return canEditRepOnly(user, entity.getTableName(), entity.getSectionCode(), entity.getRepUserID());
	}

	public boolean canEditRepOnly(User user, String tableName, String sectioncode) {
		return canEditRepOnly(user, tableName, sectioncode, null);
	}

	public boolean canEditRepOnly(User user, String tableName, String sectioncode, String repUserID) {
		//if (canEdit(user, tableName, sectioncode)) {
		if (hasModuleSinglePermission(user, tableName, sectioncode, CanEditRep)) {
			return true;
		}
		//}
		return false;
	}

	public boolean canEditRepOnly(User user, TableData data, String grouprelation) {
		return(canEditRepOnly(user, data, grouprelation, data.getString(REP_USER_ID)));
	}  	

	public boolean canEditRepOnly(User user, TableData data, String grouprelation, String repUserID) {
		User.RecordSecurity[] recordsecurity = user.getRecordSecurity(grouprelation, data);
		//if (canEdit(user, data, grouprelation, repUserID)) {
		if (hasModulePermission(user, data, recordsecurity, CanEditRep)) {
			return true;
		}
		//}
		return false;
	}	
	protected boolean isRepOrNotSet(User user, String repUserID, User.RecordSecurity[] recordsecurity)
	{
		return(repUserID != null && (repUserID.equals(user.getUserID()) 
				|| repUserID.length() == 0));    
	}

	protected boolean isRep(User user, String repUserID, User.RecordSecurity[] recordsecurity)
	{
		if(user.getUserID().equals(repUserID)){
			return(true);
		}
		for(int i=0; recordsecurity != null && i < recordsecurity.length; i++){
			if(user.getUserID().equals(recordsecurity[i].repuserid)){
				return(true);
			}
		}
		return(false);
	}

	public boolean canLink(User user, SecuredEntity entity) {
		return canLink(user, entity.getTableName(), entity.getSectionCode());
	}

	public boolean canLink(User user, String tableName, String sectioncode) {
		if (canView(user, tableName, sectioncode)) {
			return hasModuleSinglePermission(user, tableName, sectioncode, CanLink);
		}
		return false;
	}

	public boolean canLink(User user, TableData data, String grouprelation) {
		if (canView(user, data, grouprelation, null)) {
			return hasModulePermission(user, data, user.getRecordSecurity(grouprelation, data), CanLink);
		}
		return false;
	}	

	public boolean canLink(User user, TableData data, String grouprelation, String repUserID) {
		User.RecordSecurity[] recordsecurity = user.getRecordSecurity(grouprelation, data);
		if (canAccess(user, data, grouprelation)) {
			if (hasModulePermission(user, data, recordsecurity, CanLink)) {
				return true;
			}
		}
		return false;
	}	

	public boolean canAdminStatus(User user, SecuredEntity entity) {
		return canAdminStatus(user, entity.getRecord(), entity.getGroupsRelation());
	}

	public boolean canAdminStatus(User user, String tableName, String sectioncode) {
		if (canEdit(user, tableName, sectioncode)) {
			return hasModuleSinglePermission(user, tableName, sectioncode, CanAdminStatus);
		}
		return false;
	}

	public boolean canAdminStatus(User user, TableData data, String grouprelation) {
		if (canEdit(user, data, grouprelation, null)) {
			return hasModulePermission(user, data, user.getRecordSecurity(grouprelation, data), CanAdminStatus);
		}
		return false;
	}	

	public boolean canMerge(User user, SecuredEntity entity) {
		return canMerge(user, entity.getRecord(), entity.getGroupsRelation());
	}

	public boolean canMerge(User user, String tableName, String sectioncode) {
		if (canEdit(user, tableName, sectioncode)) {
			return hasModuleSinglePermission(user, tableName, sectioncode, CanMerge);
		}
		return false;
	}

	public boolean canMerge(User user, String tableName, String sectioncode, String repUserID) {
		if (canEdit(user, tableName, sectioncode, repUserID)) {
			return hasModuleSinglePermission(user, tableName, sectioncode, CanMerge);
		}
		return false;
	}

	public boolean canMerge(User user, TableData data, String grouprelation) {
		if (canEdit(user, data, grouprelation, null)) {
			return hasModulePermission(user, data, user.getRecordSecurity(grouprelation, data), CanMerge);
		}
		return false;
	}  	

	public boolean canMerge(User user, TableData data, String grouprelation, String repUserID) {
		if (canEdit(user, data, grouprelation, repUserID)) {
			return hasModulePermission(user, data, user.getRecordSecurity(grouprelation, data), CanMerge);
		}
		return false;
	}  	

	public void setSearchConstraint(User user, TableData data, String relation) {
		setSearchConstraint(user, data, relation, null, false);
	}

	public void setSearchConstraint(User user, TableData data, String relation, String restriction) {
		setSearchConstraint(user, data, relation, restriction, false);
	}	

	public void setStatusConstraint(User user, TableData data) {
		setSearchConstraint(user, data, "StatusGroup" , null, true);
	}
	public void setExportConstraint(User user, TableData data, String relation) {
		setSearchConstraint(user, data, relation, CanExport, false);
	}
	public void setManageConstraint(User user, TableData data, String relation) {
		setSearchConstraint(user, data, relation, CanManage, false);
	}
	public void setBulkEditConstraint(User user, TableData data, String relation) {
		setSearchConstraint(user, data, relation, CanEditOffline, false);
	}
	public void setBulkSendConstraint(User user, TableData data, String relation) {
		setSearchConstraint(user, data, relation, CanSendEmailsOffline, false);
	}

	/**
	 * Returns a hash Password for the given password
	 * @author Puja Shah 25/07/2008.
	 * @return String
	 * 
	 * TODO this should also salt passwords 
	 */
	public static String hashPassword(String text){
		try{
			return(hashString(text, "SHA-1","UTF-8"));
		}catch(Exception e){
			e.printStackTrace();
			return(null);
		}
	}

	/**
	 * Returns a random alphanumeric password
	 * @author Puja Shah 25/07/2008.
	 * @return String
	 */
	public static String generatePassword(){
		char[] symbols = new char[36];
		for(int i=0; i<26; ++i)
			symbols[i] = (char)('a' + i);
		for(int i=0; i<10; ++i)
			symbols[26+i] = (char)('0' + i);
		Random rnd = new Random();

		StringBuffer b = new StringBuffer();
		for(int i=0; i<7; ++i)
			b.append(symbols[rnd.nextInt(36)]);
		String password = b.toString();
		return password;
	}  

	public static String hashString(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException { 
		return SecurityFactory.hashString(text, SecurityKeys.SHA_1, SecurityKeys.UTF_8);
	}
	/**
	 * Hashes a given string using specified algorithm and encoding. 
	 * Adapted from http://www.anyexample.com/programming/java/java_simple_class_to_compute_sha_1_hash.xml
	 * @author Michael Dekmetzian 13/12/2007.
	 * @param text to be hashed
	 * @param algorithm to be used
	 * @param encoding to be used
	 * @return hashed string
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	public static String hashString(String text, String algorithm, String encoding) 
			throws NoSuchAlgorithmException, UnsupportedEncodingException  {
		MessageDigest md;
		md = MessageDigest.getInstance(algorithm);
		byte[] data = new byte[40];
		md.update(text.getBytes(encoding));
		data = md.digest();

		StringBuffer sha1hash = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					sha1hash.append((char) ('0' + halfbyte));
				else
					sha1hash.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while(two_halfs++ < 1);
		}
		return sha1hash.toString();
	}  


	private void setSearchConstraint(User user, TableData data, String relation, String restriction, boolean viewablebydefault) {
		if(logger.isDebugEnabled()) logger.debug("setSearchConstraint(user={},data,relation={},restriction={},viewablebydefault={})", new String[]{ user != null ? user.getUserName() : "null user", relation, restriction, String.valueOf(viewablebydefault) });
		data.setColumn(ActionBean.CURRENTUSERID, user.getUserID());
		String key = null;
		if(relation!=null) {
			if (relation.indexOf("|") >= 0) {
				int e = relation.indexOf("|");
				String or = relation.substring(e);
				relation = relation.substring(0,e);
				key = new StringBuffer().append(relation).append(ActionBean._dot).append(SECTION_CODE).append(or).toString();
			} else {
				key = new StringBuffer().append(relation).append(ActionBean._dot).append(SECTION_CODE).toString();
			}
		}
		else {
			// For Groups Table
			key = SECTION_CODE;
		}


		//String existingvalue = data.getString(key);
		// overwrite is true by default
		// if(overwrite || !getCodeAllowed(user, existingvalue))
		//{

		// SectionCode for regular view groups
		String viewCode = getConcatenatedCode(user, data.getTableSpec().getSecurityTableName(), relation, restriction, false, viewablebydefault);

		String repRelation = getRepRelationPart(relation);
		//boolean repTable = isRepTable(repRelation);

		if(viewablebydefault){
			// this is used by statuslist
			data.setColumn(key+"|GNE",viewCode);
			data.setColumn(repRelation+"GroupID|GNE","NULL+EMPTY");
		}          
		else if (data instanceof GenRow && isRepTable(data, repRelation)) {
			// If Table concerned has a RepUserID Column
			// SectionCode for groups with view assigned setting
			Group[] assignedGroups = getSearchGroups(user, data.getTableSpec().getSecurityTableName(), relation, restriction, true, false);

			if (assignedGroups.length!=0 && key.indexOf("|") == -1) {
				if (!viewCode.equals(NO_VIEWABLE_GROUPS)) {
					// There needs to be a restriction for both View and View Assigned
					data.setColumn(key+"|VC",viewCode);

					StringBuffer nvAC = new StringBuffer();

					for (Group g:assignedGroups) { 
						if(viewCode.indexOf(g.getSectionCode()) == -1) { //ie if it doesn't exist
							if(nvAC.length()>0) { 
								nvAC.append(ActionBean._plus).append(g.getSectionCode()); 
							} else { 
								nvAC.append(g.getSectionCode()); 
							}
						}
					}
					if(nvAC.length()>0) { 
						data.setColumn(key+"^AC|VC",nvAC.toString());
						data.setColumn(getRepKey(repRelation,"^AC|VC"), user.getUserID());
					} 
				}
				else {
					// There needs to be a restriction for View Assigned only
					data.setColumn(key+"^AC",this.getConcatenatedCode(assignedGroups));
					data.setColumn(getRepKey(repRelation,"^AC"), user.getUserID());
				}
			}
			else {
				// There needs to be a restriction for View only
				data.setColumn(key,viewCode);
			}
		}
		else {
			// There needs to be a restriction for View only
			data.setColumn(key,viewCode);
		}
		//}
	}

	private String getRepKey(String repRelation, String suffix)
	{
		StringBuffer temp = new StringBuffer();
		if(repRelation!=null && repRelation.length()!=0)
		{
			temp.append(repRelation);
			temp.append(ActionBean._dot);
		}
		temp.append(REP_USER_ID);
		temp.append(suffix);
		return(temp.toString());
	}

	private String getRepRelationPart(String relation) {
		String repRelation = null;
		if(relation!=null && relation.indexOf(ActionBean._dot) != -1) {
			repRelation = new StringBuffer().append(relation.substring(0, relation.indexOf(ActionBean._dot))).toString();
		}
		else {
			repRelation = ActionBean._blank;
		}
		return(repRelation);
	}

	private boolean isRepTable(TableData data, String repRelation) {
		if (repRelation == null || repRelation.length() == 0) {
			TableSpec tSpec = data.getTableSpec();
			if (tSpec != null) {
				return tSpec.hasField(REP_USER_ID);
			}
		}
		else {
			if (repRelation != null && repRelation.length() != 0) {

				//ie ContactContactGroups
				RelationSpec rSpec = SpecManager.getRelationSpec();
				int index = rSpec.getIndex(repRelation);

				if (index >= 0) {
					String tableName = rSpec.getToTableName(index);
					if (tableName != null) {
						TableSpec tSpec = SpecManager.getTableSpec(tableName);
						if (tSpec != null) {
							return tSpec.hasField(REP_USER_ID);
						}
					}
				}
				return false;
			}
		}
		return false;
	}

	/*private boolean getCodeAllowed(User user, String search) {
		if(search!=null && search.length()!=0) {
			String tempcode = null;

			Collection<Group> groups = user.getGroups();

			for (Group group : groups) {
			   tempcode = group.getSectionCode();
			   if(tempcode.length()==0) {
					return(true);
				}
				else if(search.indexOf(tempcode)==0) {
					return(true);
				}
			}
		}
		return(false);
	}*/


	private String getConcatenatedCode(User user, TableData data, String relation, String restriction, boolean assigned, boolean viewabledefault) {
		return getConcatenatedCode(user, data.getTableSpec().getTableName(), relation, restriction, assigned, viewabledefault);
	}

	private String getConcatenatedCode(User user, String module, String relation, String restriction, boolean assigned, boolean viewabledefault)
	{
		if(logger.isDebugEnabled()) logger.debug("getConcatenatedCode(user={},module={},relation={},restriction={},assigned={},viewabledefault={})", new String[]{ user != null ? user.getUserName() : "null user", module, relation, restriction, String.valueOf(assigned), String.valueOf(viewabledefault) });
		Group[] groups = this.getSearchGroups(user, module, relation, restriction, assigned, viewabledefault);
		return(getConcatenatedCode(groups));
	}

	private String getConcatenatedCode(Group[] codes)
	{
		StringBuffer temp = new StringBuffer();

		if(codes.length == 0) {
			//if(search) {
			temp.append(NO_VIEWABLE_GROUPS);
			//}
		}else{
			for(int i=0; i<codes.length; i++){
				if(i!=0){
					temp.append(ActionBean._plus);
				}
				temp.append(codes[i].getSectionCode());
				if(codes[i].canAccessChildGroups()){
					temp.append(ActionBean._percent);
				}
			}
		}
		if(logger.isDebugEnabled()); logger.debug(temp.toString());
		return(temp.toString());
	}

	private Group[] getSearchGroups(User user, String module, String relation, String restriction, boolean assigned, boolean viewabledefault)
	{
		if(logger.isDebugEnabled()) logger.debug("getSearchGroups(user={},module={},relation={},restriction={},assigned={},viewabledefault={})", new String[]{ user != null ? user.getUserName() : "null user",module, relation, restriction, String.valueOf(assigned), String.valueOf(viewabledefault) });
		String tempcode = null;

		ArrayList<Group> cache = new ArrayList<Group>();

		Collection<Group> groups = user.getGroups();

		for (Group group : groups) {
			tempcode = group.getSectionCode(); //CURRENT GROUP'S SECTION CODE

			if (restriction == null || group.getField(restriction).equals(YES)) {
				if (viewabledefault || isViewableSectionCode(group, module, relation, assigned)) {
					// IF THE CURRENT USER CAN VIEW THE PARTICULAR GROUP
					if(cache.size()==0) {
						cache.add(group);

					}
					else { 

						if(needsCode(cache, group)) { 
							//IF CURRENT SECTION CODE NOT FOUND IN THE CACHE, ADD IT
							cache.add(group);
						}
					}
				}
			}
		}
		if(logger.isDebugEnabled()) logger.debug("Groups[{}]", StringUtils.join(cache, ","));
		Group[] codes = new Group[cache.size()];
		cache.toArray(codes);
		return(codes);
	}

	private boolean needsCode(ArrayList<Group> cache, Group tempgroup)
	{
		// IF SECTION CODES HAVE BEEN CHECKED, CHECK AGAINST THE CACHE
		// IF AA HAS BEEN ADDED, THEN AAAA DOES NOT NEED TO BE, ETC.	   
		for(int j=0; j<cache.size(); j++) {
			//CHECK CACHE FOR THE CURRENT GROUPS SECTION CODE
			if(cache.get(j).canAccessChildGroups() ==  tempgroup.canAccessChildGroups() && 
					tempgroup.getSectionCode().equals(cache.get(j).getSectionCode())){
				return(false);
			} else if(cache.get(j).canAccessChildGroups() && tempgroup.getSectionCode().startsWith(cache.get(j).getSectionCode())) {
				return(false);
			}
		}
		return(true);
	}

	private boolean isViewableSectionCode(Group group, String module, String relation, boolean assigned) {
		if(logger.isDebugEnabled()) logger.debug("isViewableSectionCode(group=[{}],module={},relation={},assigned={}", new String[]{group.getField("Name"), module, relation, String.valueOf(assigned)});
		String tableName = null;
		String firstRelation = relation;
		String relationSuffix = "";

		if (module.equals("UserGroups")) {
			tableName = "Users";
		}
		else if (relation == null) {
			tableName = module;
		}
		else {
			int dotIndex = relation.indexOf(ActionBean._dot);
			if (dotIndex > -1) {
				firstRelation = relation.substring(0,relation.indexOf(ActionBean._dot));
				relationSuffix = relation.substring(dotIndex+1);
			}
			RelationSpec relSpec = SpecManager.getRelationSpec();
			if (relSpec.hasRelation(firstRelation)) {	
				try { 
					tableName = relSpec.getFromTableSpecLocation(relSpec.getIndex(firstRelation));
					TableSpec ts = SpecManager.getTableSpec(tableName);
					tableName = ts.getSecurityTableName();
				} catch (NullPointerException npe) {
					logger.error("TableSpec denoted in relation did not actually exist: " + tableName, npe);
					tableName = relSpec.getFromTableName(relSpec.getIndex(firstRelation));
				}	
			}
		}
		GroupSecurity groupSecurity = group.getGroupSecurity(tableName);

		if(groupSecurity != null) {
			if((groupSecurity.canView() || groupSecurity.canViewAssigned()) && !assigned) {
				return(true);
			}
			else if(assigned && groupSecurity.canViewAssigned()) {
				return(true);
			}
		}
		else if (relationSuffix.length() > 0) {
			return isViewableSectionCode(group, module, relationSuffix, assigned);
		}
		else {
			throw new RuntimeException("Incorrect Security Implementation - " + tableName);
		}

		return false;
	}

	/**
	 *  Group Module Security Methods
	 */
	public boolean hasModulePermission(User user, SecuredEntity entity, String permission) {
		if(entity.getGroupsRelation() == null){
			String tableName = entity.getTableName();   
			String sectionCode = entity.getSectionCode();
			return hasModuleSinglePermission(user, tableName, sectionCode, permission);         
		}else{
			return hasModuleMultiplePermission(user, entity, permission);
		}
	}

	public boolean hasModulePermission(User user, TableData data, User.RecordSecurity[] recordsecurity, String permission) {
		if(recordsecurity == null){
			return hasModuleSinglePermission(user, data, permission);         
		}else{
			return hasModuleMultiplePermission(user, data, recordsecurity, permission);
		}
	}    

	public boolean hasModulePermission(User user, User.RecordSecurity[] recordsecurity, String modulename, String sectioncode, String permission) {
		if(recordsecurity == null){
			return hasModuleSinglePermission(user, modulename, sectioncode, permission);         
		}else{
			return hasModuleMultiplePermission(user, modulename, sectioncode, recordsecurity, permission);
		}
	}

	public boolean hasModuleSinglePermission(User user, TableData data, String permission) {
		return hasModuleSinglePermission(user, data.getTableSpec().getTableName(), data.getString(SECTION_CODE), permission);  
	}

	//permission stored at group security
	private boolean hasModuleSinglePermission(User user, String tableName, String sectionCode, String permission) {
		return(this.hasModuleMultiplePermission(user, tableName, sectionCode, null, permission));
	} 

	public boolean hasModuleMultiplePermission(User user, SecuredEntity entity, String permission) {
		TableData data = entity.getRecord();
		User.RecordSecurity[] recordsecurity = user.getRecordSecurity(entity.getGroupsRelation(), entity.getRecord());
		return(hasModuleMultiplePermission(user, data, recordsecurity, permission));
	}

	public boolean hasModuleMultiplePermission(User user, TableData data, User.RecordSecurity[] recordsecurity, String permission) {
		TableSpec tspec = data.getTableSpec();
		return(hasModuleMultiplePermission(user, tspec.getTableName(),  null, recordsecurity, permission));
	}  

	private boolean hasModuleMultiplePermission(User user, String tableName, String sectioncode, User.RecordSecurity[] recordsecurity, String permission) {
		Collection groups = user.getGroups();
		if(groups != null && permission != null) {
			Group group = null;
			Iterator iter = groups.iterator();
			while(iter.hasNext()) {
				group = (Group)iter.next();

				if(sectioncode == null || hasMatchingSection(sectioncode, recordsecurity, group.getSectionCode(), group.canAccessChildGroups())) {
					GroupSecurity securityModule = group.getGroupSecurity(tableName);
					if (securityModule != null) {
						if (securityModule.hasPermission(permission)) {
							return(true);
						}
					}
					else {
						throw new RuntimeException("Incorrect Security Implementation - " + tableName);
					}
				}
			}
		}
		return false;
	}    

	public boolean hasMatchingSection(String sectioncode, User.RecordSecurity[] recordsecurity, String groupcode)
	{
		return(hasMatchingSection(sectioncode, recordsecurity, groupcode, true));
	}

	//User.RecordSecurity[] recordsecurity from module, String sectioncode from user group
	public boolean hasMatchingSection(String sectioncode, User.RecordSecurity[] recordsecurity, String groupcode, boolean canAccessChildGroups)
	{
		if(sectioncode!=null)
		{
			// bloody stupid jong
			//return(sectioncode.startsWith(groupcode) && canAccessChildGroups);
			if (canAccessChildGroups)
				return sectioncode.startsWith(groupcode);
			else
				return sectioncode.equals(groupcode);
		}
		if(recordsecurity != null)
		{
			for(int i=0; i<recordsecurity.length; i++)
			{
				if(canAccessChildGroups && recordsecurity[i].sectioncode.startsWith(groupcode))
				{
					return(true);
				}
				else if(!canAccessChildGroups && recordsecurity[i].sectioncode.equals(groupcode))
				{
					return(true);
				}
			}
		}
		return(false);
	}	

	public boolean getModulePermission(User user, String tableName, String sectionCode, String permission) {
		return(this.hasModuleSinglePermission(user, tableName, sectionCode, permission));
	}  	

	public boolean getModulePermission(User user, TableData data, String modulename, String permission, String grouprelation) {
		String sectioncode = null;
		if(data!=null)
		{
			sectioncode = data.getData(UserBean.sectioncode);
			if(modulename == null)
			{
				modulename = data.getTableSpec().getTableName();
			}
		}

		return(getModulePermission(user, data, modulename, permission, sectioncode, grouprelation));
	}   

	public boolean getModulePermission(User user, TableData data, String modulename, String permission, String sectioncode, String grouprelation) {
		User.RecordSecurity[] recordsecurity = null;
		if(grouprelation!=null && data!=null)
		{
			recordsecurity = user.getRecordSecurity(grouprelation, data);
		}
		return(this.hasModulePermission(user, recordsecurity, modulename, sectioncode, permission));
	}    

	private boolean hasPermission(User user, String permission){
		return hasPermission(user, permission, null);
	}

	//permission stored at group
	private boolean hasPermission(User user, String permission, SecuredEntity entity){
		Collection groups = user.getGroups();
		if(groups != null && permission != null) {

			Group group = null;
			Iterator iter = groups.iterator();
			while(iter.hasNext()) {
				group = (Group)iter.next();

				if (group.getField(permission).equals("Y")) {
					if (entity == null) {
						return(true);
					}
					else if (group.canAccessChildGroups() && entity.getSectionCode().startsWith(group.getSectionCode())) {
						return(true);
					}
					else if (!group.canAccessChildGroups() && entity.getSectionCode().equals(group.getSectionCode())) {
						return(true);
					}               
				}
			}
		}
		return(false);
	}

	public boolean getPermission(User user, String code, String task)
	{
		Collection<Group> groupList = user.getGroups();
		if(groupList!=null)
		{
			String groupcode = null;
			Group group = null;
			for(int i=0; i<groupList.size(); i++)
			{
				group = user.getGroup(i);
				groupcode = group.getRecord().getString(UserBean.sectioncode);
				if(groupcode.length()==0 || (code!=null && code.indexOf(groupcode)==0))
				{
					if(task!=null)
					{
						if(group.getRecord().getString(task).equals(UserBean.Y))
						{
							return(true);
						}
					}
					else
					{
						return(true);
					}
				}
				//if(cuserid!=null && getString(userid).equals(cuserid))
				//{
				//   return(true);
				//}
			}     
		}
		return(false);
	}

	public boolean getPermission(User user, String task)
	{
		if(task!=null && task.length() > 0) // changed this to here, before it would go through the loop and check nothing if null 
		{
			Collection<Group> groupList = user.getGroups();
			if(groupList!=null)
			{
				for(int i=0; i<groupList.size(); i++)
				{
					if(logger.isDebugEnabled()) logger.debug("Checking group {} for task {} with result {}", new String[]{user.getGroup(i).getField("Name"), task, user.getGroup(i).getField(task)});
					if(user.getGroup(i).getRecord().getString(task).equals(UserBean.Y))
					{
						return(true);
					}
				}
			}     
		}
		return(false);
	}     

	public String getPermissionString(User user, String task)
	{
		if(task!=null && task.length() > 0) // changed this to here, before it would go through the loop and check nothing if null 
		{
			Collection<Group> groupList = user.getGroups();
			if(groupList!=null)
			{
				for(int i=0; i<groupList.size(); i++)
				{
					if(user.getGroup(i).getRecord().getString(task).length() > 0)
					{
						return user.getGroup(i).getRecord().getString(task);
					}
				}
			}     
		}
		return "";
	}     

	//Restrictions only take effect if all user permissions have this restriction
	public boolean getRestriction(User user, String task)
	{
		if(task!=null && task.length() > 0) // changed this to here, before it would go through the loop and check nothing if null 
		{
			Collection<Group> groupList = user.getGroups();
			if(groupList!=null)
			{
				for(int i=0; i<groupList.size(); i++)
				{
					if(!user.getGroup(i).getRecord().getString(task).equals(UserBean.Y))
					{
						return(false);
					}
				}
				return(true);
			}     
		}
		return(false);
	}

}