package com.sok.runway;

import javax.servlet.*;     
import javax.servlet.http.*;
import java.util.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import java.text.*;

import com.sok.framework.*;
import com.sok.framework.sql.*;

public class OfflineImport extends OfflineProcess {	

   StringBuffer status = new StringBuffer();

	String email = null;
	String userid = null;
	Connection connection = null;

   int numRecords = 0;
   int numSuccess = 0;
   int numDataErrors = 0;
   
   String importID = null;            	
   String productMatch = null;
   String companyMatch = null;
   String contactMatch = null;
   boolean updateMode = false;
	
	public OfflineImport(HttpServletRequest request, ServletContext context) {
	   this(request, context, true, null);
	}
	
	public OfflineImport(HttpServletRequest request, ServletContext context, boolean autoStart, String importID) {
		super(request, context);
		   HttpSession session = request.getSession();
			UserBean user = (UserBean)session.getAttribute(SessionKeys.CURRENT_USER);
			this.userid = user.getCurrentUserID();
			this.email = user.getEmail();
			this.importID = importID;
	   if (autoStart) {
		   start();
		}
	}
	
	public void execute() {
	   
		try {
			if (importID == null) {
			   importID = requestbean.getString("ImportID");
			}
   	   productMatch = requestbean.getString("ProductMatch");
         companyMatch = requestbean.getString("CompanyMatch");
         contactMatch = requestbean.getString("ContactMatch");
         updateMode = "update".equals(requestbean.getString("-action"));
         
   		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, getLocale());
   		this.status.append(context.getServletContextName());
   		this.status.append(" Runway offline process - ");
   		this.status.append(" Finalise Import\r\n");
   		this.status.append("Initiated ");
   		this.status.append(dt.format(new java.util.Date()));
   		this.status.append("\r\n");
   		
		
			Context ctx = new InitialContext();

			DataSource ds = null;
			try{
				dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
				Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
				ds = (DataSource)envContext.lookup(dbconn);
			}catch(Exception e){}
			if(ds == null)
			{
				ds = (DataSource)ctx.lookup(dbconn);
			}
			connection = ds.getConnection();
			
         if (importID != null && importID.length() > 0) {
            doImport(importID, updateMode);
         }
   		
   		status.append("Data Import processed ");
   		status.append(numRecords);
   		status.append(" records.\r\n");
         status.append("Successfully imported ");

   		status.append(numSuccess);
   		status.append(" records");
   		if (numDataErrors > 0) {
   		   status.append(" (with errors - see below for details)");
   		   //status.append(errors.toString());
   		}
   		   status.append(errors.toString());
		   status.append(".\r\n");
		   if (isKilled()) {
		      status.append("Offline process terminated by - ");
		      status.append(getKiller());
		      status.append("\r\n\r\n");
		   }
		   else {	   
		      status.append("Offline process completed.\r\n\r\n");
		   }
   		connection.close();
   	} 
   	catch (Exception e) {
   	   this.status.append(ActionBean.writeStackTraceToString(e));
   	}
   	finally {
   	   if (connection != null) {
   	      try { connection.close(); } catch (Exception e) {}
   	   }
      }
   }
	
   // Implemented Abstract Methods
	public String getStatusMailBody() {
		return status.toString();
   }

	public String getStatusMailSubject() {
		return(contextname + " Runway offline process - Finalise Import");
   }

	public String getStatusMailRecipient() {
		return(email);
   }
   
   /////////////////////////////////
   
   public void doImport(String importID, boolean update) {
      RowBean importBean = new RowBean();
      importBean.setConnection(connection);
      importBean.setViewSpec("ImportView");
      importBean.setColumn("ImportID", importID);
      importBean.setAction("select");
      importBean.doAction();
      
      
      if (update) {
         RowBean group = new RowBean();
         group.setViewSpec("GroupView");
         group.setConnection(connection);
         group.setColumn("GroupID", importBean.getString("GroupID"));
         group.setAction("select");
         group.doAction();
         
         String sectionCode = group.getColumn("SectionCode");
         if (sectionCode.length() > 0 ) {
            sectionCode = sectionCode + "%";
         }
         else {
            sectionCode = "XXXXXX";
         }
         updateBeans(importBean, productMatch, "Product", sectionCode);
         updateBeans(importBean, companyMatch, "Company", sectionCode);
         updateBeans(importBean, contactMatch, "Contact", sectionCode);
      }
      else {
         importBeans(importBean, "Company");
         importBeans(importBean, "Contact");
         importBeans(importBean, "Product");
      }

      importBean.clear();
      importBean.setColumn("ImportID", importID);
      importBean.setColumn("CommittedDate", "NOW");
      importBean.setAction("update");
      importBean.doAction();
      if (importBean.getError().length() > 0) {
         errors.append(importBean.getError());
      }
   }
   
   private RowBean getTargetBean(RowBean importBean, String beanName) {
      RowBean bean = new RowBean();
      bean.setConnection(connection);
      bean.setViewSpec(beanName + "View");
      bean.setColumn("Source", importBean.getColumn("Source"));
      bean.setColumn("GroupID", importBean.getColumn("GroupID"));
      bean.setColumn("ProductGroupID", importBean.getColumn("ProductGroupID"));
      if (importBean.getColumn("RepUserID").length() > 0) {
         bean.setColumn("RepUserID", importBean.getColumn("RepUserID"));
      }
      if (importBean.getColumn("MgrUserID").length() > 0) {
         bean.setColumn("MgrUserID", importBean.getColumn("MgrUserID"));
      }
      bean.setColumn("CreatedBy", userid);
      bean.setColumn("CreatedDate", "NOW");
      bean.setColumn("ModifiedBy", userid);
      bean.setColumn("ModifiedDate", "NOW");
      bean.setAction("insert");
      
      return bean;
   }
   
   private RowBean getTargetStatusBean(RowBean importBean, String beanName) {
      
      if (beanName.equals("Contact") || beanName.equals("Company")) {
         RowBean statusBean = new RowBean();
         statusBean.setConnection(connection);
         statusBean.setViewSpec(beanName +"StatusView");
         statusBean.setColumn("CreatedBy", userid);
         statusBean.setColumn("CreatedDate", "NOW");
         statusBean.setColumn("StatusID", importBean.getColumn(beanName + "StatusID"));
         statusBean.setAction("insert");
         
         return statusBean;
      }
      else {
         return null;
      }
   }
   
   private RowBean getTargetGroupBean(RowBean importBean, String beanName) {
      
      if (beanName.equals("Contact") || beanName.equals("Company") || beanName.equals("Product")) {
         RowBean groupBean = new RowBean();
         groupBean.setConnection(connection);
         if(beanName.equals("Product")){
            groupBean.setViewSpec(beanName +"SecurityGroupView");
         } else {
            groupBean.setViewSpec(beanName +"GroupView");
         }

         groupBean.setColumn("CreatedBy", userid);
         groupBean.setColumn("CreatedDate", "NOW");
         groupBean.setColumn("GroupID", importBean.getColumn("GroupID"));
         groupBean.setColumn("RepUserID", importBean.getColumn("RepUserID"));
         groupBean.setAction("insert");
         
         return groupBean;
      }
      else {
         return null;
      }
   }   
   
   public void importBeans(RowBean importBean, String beanName) {
      RowSetBean importList = new RowSetBean();
      importList.setConnection(connection);
      importList.setViewSpec("Import" + beanName + "View");
      importList.setColumn("ImportID", importBean.getString("ImportID"));
      importList.setColumn("ImportFlg", "Y");
      importList.generateSQLStatement();
      importList.getResults();
      
      RowBean bean = getTargetBean(importBean, beanName);
      RowBean statusBean = getTargetStatusBean(importBean, beanName);
      RowBean groupBean = getTargetGroupBean(importBean, beanName);
      
      while (importList.getNext() && !isKilled()) {
         numRecords++;
         importSingleBean(bean, importList, beanName, statusBean, groupBean);
      }
      
      importAnswers(importBean, beanName);
   }
   
   
   private void importSingleBean(RowBean targetBean, RowBean sourceBean, String beanName, RowBean statusBean, RowBean groupbean) {
      
      //String beanID = sourceBean.getColumn("Import" + beanName + "ID");
      String beanID = sourceBean.getColumn(beanName + "ID");
      
      targetBean.putAll(sourceBean);
      
      if (statusBean != null) {
         String statusID = KeyMaker.generate();
         targetBean.setColumn(beanName + "StatusID", statusID);
         statusBean.setColumn(beanName + "StatusID", statusID);
         statusBean.setColumn(beanName + "ID", beanID);
         if(groupbean!=null) {
            groupbean.setColumn(beanName + "StatusID", statusID);
         }
      }
      if(groupbean!=null) {
         groupbean.setColumn(beanName + "ID", beanID);
      }      
      
      targetBean.setColumn(beanName + "ID", beanID);
      //if (beanName.equals("Contact")) {
      //   targetBean.setColumn("CompanyID", sourceBean.getColumn("ImportCompanyID"));
      //}
      
      targetBean.doAction();
      if (targetBean.getError().length() > 0) {
         numDataErrors++;
         setLastError(targetBean.getError());
         errors.append(targetBean.getError());
      }
      else {
         numSuccess++;
         if (statusBean != null) {
            statusBean.doAction();
            errors.append(statusBean.getError());
         }
         if(groupbean!=null) {
            groupbean.doAction();
            errors.append(groupbean.getError());
         }
      }
      
   }
   
   public void importAnswers(RowBean importBean, String beanName) {
      RowSetBean importList = new RowSetBean();
      importList.setConnection(connection);
      importList.setViewSpec("Import" + beanName + "AnswerView");
      importList.setColumn("ImportID", importBean.getString("ImportID"));
      importList.setColumn("ImportAnswer" + beanName + ".ImportFlg", "Y");
      importList.generateSQLStatement();
      importList.getResults();
      
      errors.append(importList.getError());
      
      String fkName = beanName +"ID";
      RowBean bean = new RowBean();
      bean.setConnection(connection);
      bean.setViewSpec("answers/" + beanName + "AnswerView");
      bean.setAction("insert");
      
      while (importList.getNext()) {
         bean.setColumn("AnswerID", importList.getColumn("ImportAnswerID"));
         bean.setColumn(fkName, importList.getColumn(fkName));
         bean.setColumn("QuestionID", importList.getColumn("QuestionID"));
         bean.setColumn("Answer", importList.getColumn("Answer"));
         bean.setColumn("AnswerText", importList.getColumn("Answer"));
         
      	AnswerBean.setDateAnswer(importList.getColumn("Answer"), bean);
      	AnswerBean.setNumberAnswer(importList.getColumn("Answer"), bean);
         
      	bean.setColumn("CreatedDate","NOW");
      	bean.setColumn("CreatedBy",userid);
      	bean.setColumn("ModifiedDate","NOW");
      	bean.setColumn("ModifiedBy",userid);
         bean.doAction();
      }
      errors.append(bean.getError());
   }
   
   public void updateBeans(RowBean importBean, String matchField, String beanName, String sectionCode) {
      String viewName = beanName + "View";
      String beanPk = beanName + "ID";
      String importPK = "Import" + beanPk;

      
      if (matchField != null && matchField.length() > 0) {
         RowSetBean importList = new RowSetBean();
         importList.setConnection(connection);
         importList.setViewSpec("Import"+viewName);
         importList.setColumn("ImportID", importBean.getString("ImportID"));
         importList.setColumn("ImportFlg", "Y");
         importList.generateSQLStatement();
         importList.getResults();
         errors.append(importList.getError());
            
         RowSetBean beanList = new RowSetBean();
         beanList.setConnection(connection);
         beanList.setViewSpec(viewName);
         
         RowBean bean = new RowBean();
         bean.setConnection(connection);
         bean.setViewSpec(viewName);
         bean.setAction("execute");
         
         TableSpec tSpec = SpecManager.getTableSpec(importList.getTableSpec().getTableName().substring(6));
         
         String fieldName = null;
         GenRow update = new GenRow();

         while (importList.getNext() && !isKilled()) {
            if (importList.getColumn(matchField).length() > 0) {
               numRecords++;
               
               beanList.clear();
               if (matchField.equals(importPK)) {
                  beanList.setColumn(beanPk, importList.getColumn(matchField));
               }
               else {
                  beanList.setColumn(matchField, importList.getColumn(matchField));
               }
               beanList.setColumn("SectionCode", sectionCode);
               beanList.generateSQLStatement();
               beanList.getResults();
               errors.append(beanList.getError());
               
               if (beanList.getNext()) {
                  update.clear();
                  update.setConnection(connection);
                  update.setTableSpec(tSpec.getTableName());
                  update.setParameter("ModifiedDate", "NOW");
                  update.setParameter("ModifiedBy", userid);


                  for (int i=0; i < tSpec.getFieldsLength(); i++) {
                     if (tSpec.getImport(i).equals("true")) { 
                        fieldName = tSpec.getFieldName(i);
                        if (!tSpec.getPrimaryKeyName().equals(fieldName)) {
                           if (importList.getColumn(fieldName).length() > 0){
                              update.setParameter(fieldName, importList.getColumn(fieldName));
                           }
                        }
                     }
                  }
                  update.setParameter(beanPk, beanList.getString(beanPk));
                  try
                  {
                     update.doAction("update");
                     if (!update.isSuccessful()) {
                        numDataErrors++;
                        errors.append(bean.getError());
                     }
                     else {
                        numSuccess++;
                        updateAnswers(importList.getString(importPK), beanList.getString(beanPk), beanName);
                     }                     
                  }
                  catch(Exception e)
                  {
                     numDataErrors++;
                     errors.append(e);
                  }
               }
               else {
                  RowBean insertBean = getTargetBean(importBean, beanName);
                  RowBean insertStatusBean = getTargetStatusBean(importBean, beanName);
                  RowBean insertGroupBean = getTargetGroupBean(importBean, beanName);
                  importSingleBean(insertBean, importList, beanName, insertStatusBean, insertGroupBean);
                  updateAnswers(importList.getString(importPK), importList.getColumn(importPK), beanName);
               }
               if (beanList.getNext()) {
                  status.append("WARNING: Found more than 1 ");
                  status.append(beanName);
                  status.append(" that matched field '");
                  status.append(matchField);
                  status.append("' with value '");
                  status.append(importList.getColumn(matchField));
                  status.append("'. 1 Record updated.\r\n");
               }
            }
         }
      }
   }
   
   public void updateAnswers(String importBeanID, String beanID, String beanName) {
      
      String fkName = beanName +"ID";
      
      RowSetBean importList = new RowSetBean();
      importList.setConnection(connection);
      importList.setViewSpec("Import" + beanName + "AnswerView");
      importList.setColumn("Import"+fkName, importBeanID);
      importList.generateSQLStatement();
      importList.getResults();
      
      //importList.setColumn("ImportID", importBean.getString("ImportID"));
      //importList.setColumn("Import" + beanName + "Answer.ImportFlg", "Y");
      
      
      RowBean bean = new RowBean();
      bean.setConnection(connection);
      bean.setViewSpec("answers/" + beanName + "AnswerView");
      bean.setAction("updateall");
      
      while (importList.getNext()) {
         String value = importList.getColumn("Answer");
         bean.clear();
         bean.setAction("updateall");
         bean.setColumn("ON-" + fkName, beanID);
         bean.setColumn("ON-QuestionID", importList.getColumn("QuestionID"));
         bean.setColumn("Answer", value);
         bean.setColumn("AnswerText", value);
			AnswerBean.setDateAnswer(value, bean);
			AnswerBean.setNumberAnswer(value, bean);
			bean.setColumn("ModifiedDate", "NOW");
			bean.setColumn("ModifiedBy", this.userid);
         bean.doAction();
         
         if (bean.getError().length() > 0) {
            errors.append(bean.getError());
         }
         else if(bean.getUpdatedCount()==0 ) {
            bean.clear();
            bean.setAction("insert");
            bean.setColumn("AnswerID", importList.getColumn("ImportAnswerID"));
            bean.setColumn(fkName, beanID);
            bean.setColumn("QuestionID", importList.getColumn("QuestionID"));
            bean.setColumn("Answer", value);
            bean.setColumn("AnswerText", value);
				AnswerBean.setDateAnswer(value, bean);
				AnswerBean.setNumberAnswer(value, bean);
				bean.setColumn("CreatedDate", "NOW");
				bean.setColumn("CreatedBy", this.userid);
				bean.setColumn("ModifiedDate", "NOW");
				bean.setColumn("ModifiedBy", this.userid);
            bean.doAction();
         }
      }
   }
   
   public int getProcessSize() {
      return -1;
   }
   
   public int getProcessedCount() {
      return numRecords;
   }
   
   public int getErrorCount() {
      return numDataErrors;
   }
}