package com.sok.runway.validationRule;

import com.sok.framework.TableData;

/**
 * NumericValidationRule makes sure the field value is numeric
 * 
 * @author raymond.dinata
 */
public class NumericValidationRule extends ValidationRule
{
	/**
	 * Constructor - initialises the field name and error message 
	 * @param fieldName the field name of the field to be validated
	 * @param errorMsg the error message to be displayed when validation fails
	 */
	public NumericValidationRule(String fieldName, String errorMsg)
	{
		this.fieldName = fieldName;
		this.errorMsg = errorMsg;
	}

	/**
	 * Validates the field to make sure it has numeric value 
	 */
	@Override
	public boolean validate(TableData parameters) 
	{
		boolean result = false;

		String fieldValueStr = parameters.getColumn(fieldName); 
		if(fieldValueStr != null && fieldValueStr.length()>0) 
			try 
		{ 
				Double.parseDouble(fieldValueStr);
				result = true;
		} 
		catch (NumberFormatException nfe) 
		{
			// Ignore and return false
		}
		catch (Exception e)
		{
			// Ignore and return false
		}
		return result;
	}
}