package com.sok.runway.validationRule;

import com.sok.framework.TableData;

/**
 * MandatoryValidationRule validates a field to make sure  
 * the field value is not empty. 

 * @author raymond.dinata 
 */
public class MandatoryValidationRule extends ValidationRule
{
	/**
	 * Constructor for initialising the field name and error message
	 * @param fieldName the field name of the field to be validated
	 * @param errorMsg the error message to be displayed when validation fails
	 */
	public MandatoryValidationRule(String fieldName, String errorMsg)
	{
		this.fieldName = fieldName;
		this.errorMsg = errorMsg;
	}

	/**
	 * Validates the field to make sure the field is not empty
	 * @param parameters data which holds a list of fields and their values.
	 * @return true when the field value is not empty, otherwise false
	 */
	public boolean validate(TableData parameters)
	{
		String value = parameters.getColumn(fieldName);

		if ( (value == null) || ((value != null) && (value.trim().length() == 0)) )
			return false;
		else
			return true;
	}
}
