package com.sok.runway.validationRule;

import java.util.Iterator;
import java.util.Set;

import com.sok.framework.TableData;

/**
 * RestrictedValidationRule validates a field to make sure 
 * the field value is one of the allowed values.
 * 
 * @author raymond.dinata
 */
public class RestrictedValidationRule extends ValidationRule
{
	/** The list of allowed values for the field. */
	public Set<String> allowedValues = null;

	/**
	 * Constructor for initialising the field name, and allowed values,
	 * It also constructs custom error message.
	 * @param fieldName the field name of the field to be validated
	 * @param allowedValues list of allowed values for the field
	 */
	public RestrictedValidationRule(String fieldName, Set<String> allowedValues)
	{
		this.fieldName = fieldName;
		this.allowedValues = allowedValues;

		// Convert the Set of allowed values in to a comma separated String
		StringBuffer allowedValuesStr = new StringBuffer();
		Iterator<String> iter = allowedValues.iterator();
		if (iter.hasNext()) 
			allowedValuesStr.append(iter.next());
		while (iter.hasNext()) 
		{
			allowedValuesStr.append(", ");
			allowedValuesStr.append(iter.next());
		}
		// Set a custom error message
		this.errorMsg = fieldName + " must be one of the following [" + allowedValuesStr + "]";
	}

	/**
	 * Validates the field to make sure the field value is one of the allowed values.
	 * @param parameters data which holds a list of fields and their values.
	 * @return true when the field value is one of the allowed values, otherwise false
	 */
	public boolean validate(TableData parameters)
	{
		boolean result = false;

		String value = parameters.getColumn(fieldName).trim();
		if ( (value.length() > 0) && (allowedValues.contains(value)) ) 
			result = true;

		return result;
	}
}