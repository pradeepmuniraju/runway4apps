package com.sok.runway.validationRule;

import com.sok.framework.TableData;

/**
 * URLValidationRule makes sure the field conforms to the URL regular expression or it has a prefix of "file://"
 * 
 * @author raymond.dinata
 */
public class URLValidationRule extends ValidationRule
{
	/**
	 * Constructor - initialises the field name and the error message
	 * @param fieldName the field name of the field to be validated
	 * @param errorMsg the error message to be displayed when validation fails
	 */
	public URLValidationRule(String fieldName, String errorMsg)
	{
		this.fieldName = fieldName;
		this.errorMsg = errorMsg;
	}

	/**
	 * Validates the field to make sure the value matches with the URL regular expression or it has a prefix "file://"
	 */
	@Override
	public boolean validate(TableData parameters) 
	{
		String input = parameters.getColumn(fieldName);
		return ( (input!=null) && input.startsWith("file://") ) || RegExpValidationRule.validate(input, RegExpValidationRule.URL_REG_EX_PATTERN);
	}
}
