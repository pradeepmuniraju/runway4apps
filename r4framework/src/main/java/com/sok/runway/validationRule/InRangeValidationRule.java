package com.sok.runway.validationRule;

import com.sok.framework.TableData;

/**
 * InRangeValidationRule validates a numeric double field to make sure  
 * the field value is within the range of the minimum value and maximum value.
 * 
 * @author raymond.dinata 
 */
public class InRangeValidationRule<T extends Number> extends ValidationRule
{
	/** The minimum value which represents the minimum limit for the field value */
	public T minValue; 

	/** The maximum value which represents the maximum limit for the field value */
	public T maxValue; 

	/**
	 * Constructor for initialising the field name, error message, the minimum and maximum values
	 * @param fieldName the field name of the field to be validated
	 * @param minValue the minimum limit of the field value
	 * @param maxValue the maximum limit of the field value
	 * @param errorMsg the error message to be displayed when validation fails
	 */
	public InRangeValidationRule(String fieldName, T minValue, T maxValue, String errorMsg)
	{
		this.fieldName = fieldName;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.errorMsg = errorMsg;
	}

	/**
	 * Validates the field to make sure the field value is within the range of
	 * the minimum value and maximum value.
	 * @param parameters data which holds a list of fields and their values.
	 * @return true when the field value is within range, otherwise false
	 */
	public boolean validate(TableData parameters) 
	{
		boolean result = false;

		String fieldValueStr = parameters.getColumn(fieldName); 
		if(fieldValueStr != null && fieldValueStr.length()>0) 
			try 
		{ 
				// Validates Double
				if ( (minValue instanceof Double) && (maxValue instanceof Double) )
				{
					Double fieldValue = Double.parseDouble(fieldValueStr);
					Double tMinValue = minValue.doubleValue();
					Double tMaxValue = maxValue.doubleValue();
					if ( (fieldValue >= tMinValue) && (fieldValue <= tMaxValue) )  
						result = true;
				}
				// Validates Integer
				else if ( (minValue instanceof Integer) && (maxValue instanceof Integer) )
				{
					Integer fieldValue = Integer.parseInt(fieldValueStr);
					Integer tMinValue = minValue.intValue();
					Integer tMaxValue = maxValue.intValue();
					if ( (fieldValue >= tMinValue) && (fieldValue <= tMaxValue) )  
						result = true;
				}
		} 
		catch (NumberFormatException nfe) 
		{
			// Ignore and return false
		}
		catch (Exception e)
		{
			// Ignore and return false
		}
		return result;
	}
}