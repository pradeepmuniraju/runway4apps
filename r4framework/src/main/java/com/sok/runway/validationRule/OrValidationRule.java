package com.sok.runway.validationRule;

import com.sok.framework.TableData;

public class OrValidationRule extends ValidationRule {

	private ValidationRule[] rules; 
	public OrValidationRule(String errorMsg, ValidationRule... rules) { 
		this.errorMsg = errorMsg;
		this.rules = rules;
	}
	
	@Override
	public boolean validate(TableData parameters) {
		if(rules != null) { 
			for(ValidationRule r: rules) { 
				if(r.validate(parameters)) { 
					return true;
				}
			}
		}
		return false;
	}

}
