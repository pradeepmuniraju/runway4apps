package com.sok.runway.validationRule;

import java.util.Date;
import java.util.HashSet;

import com.sok.framework.TableData;
import com.sok.runway.payment.CreditCardType;
import com.sok.runway.payment.CreditCardTypes;

/**
 * CreditCardValidationRule validates credit card number, expiry date, or type,
 * depending which validation rule type is required.
 * 
 * @author raymond.dinata
 */
public class CreditCardValidationRule extends ValidationRule
{
	/** Credit Card Number */
	public static int CREDIT_CARD_NUMBER = 0;

	/** Credit Card Expiry Date */
	public static int CREDIT_CARD_EXPIRY_DATE = 1;

	/** Credit Card Type */
	public static int CREDIT_CARD_TYPE = 2;

	
	/** Holds the credit card type, e.g: Visa, MasterCard, etc */
	public String cardTypeName = null;
	
	/** Specifies which credit card number, or expiry date, or card type to use for validation. By default it uses the credit card number validation. */
	public int ruleType = CREDIT_CARD_NUMBER;

	/**
	 * Constructor - initialises the field name and error message 
	 * @param fieldName the field name of the field to be validated
	 * @param errorMsg the error message to be displayed when validation fails
	 * @param ruleType the validation type to use (either credit card number, expiry date, or card type)
	 */
	public CreditCardValidationRule(String fieldName, String errorMsg, int ruleType)
	{
		this.fieldName = fieldName;
		this.errorMsg = errorMsg;
		this.ruleType = ruleType;
	}

	/**
	 * Constructor - initialises the field name, error message, and credit card type 
	 * @param fieldName the field name of the field to be validated
	 * @param errorMsg the error message to be displayed when validation fails
	 * @param ruleType the validation type to use (either credit card number, expiry date, or card type)
	 * @param cardTypeName the credit card type, used for validating credit card number or type.
	 */
	public CreditCardValidationRule(String fieldName, String errorMsg, int ruleType, String cardTypeName)
	{
		this.fieldName = fieldName;
		this.errorMsg = errorMsg;
		this.ruleType = ruleType;
		this.cardTypeName = cardTypeName;
	}

	/**
	 * Checks whether the credit card number prefix is valid
	 * @param numStr a string representation of the credit card number
	 * @return true when the prefix number is valid, otherwise false
	 */
	private boolean isValidPrefix(String numStr)
	{
		CreditCardTypes ccTypes = CreditCardTypes.getCreditCardTypes();
		CreditCardType ccType = ccTypes.getCreditCardType(cardTypeName);
		if (ccType != null)
		{
			HashSet<String> currentCCPrefixes = ccType.getPrefixes();
			for (String prefix: currentCCPrefixes)
				if (numStr.indexOf(prefix) == 0)
					return true;
		}
		return false;
	}
	
	/**
	 * Checks whether the credit card number length is valid
	 * @param numStr a string representation of the credit card number
	 * @return true when the number length is valid, otherwise false
	 */
	private boolean isValidLength(String numStr)
	{
		CreditCardTypes ccTypes = CreditCardTypes.getCreditCardTypes();
		CreditCardType ccType = ccTypes.getCreditCardType(cardTypeName);
		if (ccType != null)
		{
			HashSet<String> currentCCNumLengths = ccType.getNumLengths();
			for (String numLengthStr: currentCCNumLengths)
			{
				try
				{
					Integer numLength = Integer.parseInt(numLengthStr);
					if (numStr.length() == numLength)
						return true;
				}
				catch (NumberFormatException nfe)
				{
					// Ignore 
				}
				catch (Exception e)
				{
					// Ignore 
				}
			}
		}
		return false;
	}
	
	/**
	 * Checks whether the credit card type is valid. 
	 * @param cardTypeName the credit card type name @see com.sok.runway.payment.CreditCardTypes
	 */
	private boolean isTypeValid(String cardTypeName)
	{
		if ( (cardTypeName == null) || (cardTypeName.trim().length() == 0) )
			return false;

		CreditCardTypes ccTypes = CreditCardTypes.getCreditCardTypes();
		CreditCardType ccType = ccTypes.getCreditCardType(cardTypeName);
		if (ccType != null)
			return true;

		return false;
	}

	/**
	 * Checks the credit card number using the Luhn algorithm 
	 * See: http://en.wikipedia.org/wiki/Luhn_algorithm
	 * 
	 * The formula verifies a number against its included check digit, 
	 * which is usually appended to a partial account number to generate the full account number. 
	 * This account number must pass the following test:
	 * 1. Counting from the check digit, which is the rightmost, and moving left, double the value of every second digit.
	 *    For any digits that thus become 10 or more, add their digits together.
	 * 2. Sum the digits of the products together with the undoubled digits from the original number.
	 * 3. If the total ends in 0 (put another way, if the total modulus 10 is congruent to 0), 
	 *    then the number is valid according to the Luhn formula; else it is not valid.
	 *    
	 * As an illustration, if the account number is 49927398716, it will be validated as follows:
	 * 1. Double every second digit, from the rightmost: (1*2) = 2, (8*2) = 16, (3*2) = 6, (2*2) = 4, (9*2) = 18
	 * 2. Sum all digits (digits in parentheses are the products from Step 1): 6 + (2) + 7 + (1 + 6) + 9 + (6) + 7 + (4) + 9 + (1 + 8) + 4 = 70
	 * 3. Take the sum modulo 10: 70 mod 10 = 0; the account number is valid.   
	 * 
	 * @param numStr the credit card number to validate
	 * @return true if valid, otherwise false
	 */
	private boolean luhnValidate(String numStr)
	{
		int sum = 0;

		boolean alternate = false;
		for (int i = numStr.length() - 1; i >= 0; i--) {
			int n = Integer.parseInt(numStr.substring(i, i + 1));
			if (alternate) {
				n *= 2;
				if (n > 9) {
					n = (n % 10) + 1;
				}
			}
			sum += n;
			alternate = !alternate;
		}

		return (sum % 10 == 0);
	}

	/**
	 * Checks whether the credit card number is valid
	 * @param numStr a string representation of the credit card number
	 * @return true when the number is valid, otherwise false
	 */
	private boolean isValidNumber(String numStr)
	{
		if (!isTypeValid(cardTypeName))
			return false;
	
		return ( isValidPrefix(numStr) && isValidLength(numStr) && luhnValidate(numStr));  
	}
	
	/**
	 * Validates the field to make sure the credit card type, or number, or expiry date is valid 
	 * @return true if valid, otherwise false
	 */
	@Override
	public boolean validate(TableData parameters) 
	{
		boolean result = false;

		String fieldValueStr = parameters.getColumn(fieldName);
		if (ruleType == CREDIT_CARD_EXPIRY_DATE)
		{
			int valueLength = fieldValueStr.length();
			Date dateValue = null;
			switch (valueLength)
			{
				case 4: // MMYY format
					dateValue = DateTimeFormatValidationRule.str2Date(fieldValueStr, "MMyy");
					break;
				case 5: // MM/YY format
					dateValue = DateTimeFormatValidationRule.str2Date(fieldValueStr, "MM/yy");
					break;
				default:
					// Ignore and return false 
					break;
			}
			
			if (dateValue != null)
			{
				Date now = new Date();
				result = dateValue.after(now);
			}
		}
		else if (ruleType == CREDIT_CARD_NUMBER)
		{
			result = isValidNumber(fieldValueStr);
		}
		else if (ruleType == CREDIT_CARD_TYPE)
		{
			result = isTypeValid(fieldValueStr);
		}

		return result;
	}
}
