package com.sok.runway.validationRule;

import com.sok.framework.TableData;

/**
 * The ValidationRule class represents the rule used for validating a field.
 * 
 * @author raymond.dinata 
 */
public abstract class ValidationRule 
{
	/** The name of the field used in the HTML form */
	public String fieldName;

	/** The error message to be displayed on the screen */
	public String errorMsg;

	/**
	 * Validates the field
	 * @param parameters data which holds a list of fields and their values.
	 * @return true when the field has been validated correctly, otherwise false
	 */
	public abstract boolean validate(TableData parameters);   
}

