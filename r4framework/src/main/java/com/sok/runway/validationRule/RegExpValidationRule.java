package com.sok.runway.validationRule;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sok.framework.TableData;

/**
 * RegExpValidationRule makes sure the field value conforms to the regular expression
 * 
 * @author raymond.dinata
 */
public class RegExpValidationRule extends ValidationRule
{
	/** The regular expression for validating an email address */
	public static final String EMAIL_REG_EX = "^[^\\f\\n\\r\\t]*<{0,1}[^@ \\f\\n\\r\\t]+@[^@ \\f\\n\\r\\t]+\\.[^@\\. \\f\\n\\r\\t]+[a-zA-Z]>{0,1}$";
	
	/** The regular expression for validating a phone number */
	public static final String PHONE_NUMBER_REG_EX = "^\\({0,1}\\d{0,3}\\){0,1}\\s{0,1}\\d{2,4}\\s{0,1}\\d{3,4}\\s{0,1}\\d{3,4}$";
	
	/** The regular expression for validating an alpha field */
	public static final String ALPHA_REG_EX = "^[A-Za-z]+$";
	
	/** The regular expression for validating an alpha numeric field */
	public static final String ALPHA_NUMERIC_REG_EX = "^[0-9A-Za-z]+$";
   
	/** The regular expression for validating an alpha with a space field */
	public static final String ALPHA_WITH_SPACE_REG_EX = "^[A-Za-z\\s]+$";
	
	/** The regular expression for validating an alpha numeric with a space field */
	public static final String ALPHA_NUMERIC_WITH_SPACE_REG_EX = "^[0-9A-Za-z\\s]+$";
	
	/** The regular expression for validating URL */
	public static final String URL_REG_EX = "^(((ht|f)tp(s?))://)?(www.|[a-zA-Z].)[a-zA-Z0-9-.]+.(com|edu|gov|mil|net|org|biz|info|name|museum|us|ca|uk|au)(/($|[a-zA-Z0-9.,;?'\\+&%$#=~_-]+))*$";
	
	/** The regular expression pattern for validating an email address */
	public static final Pattern EMAIL_REG_EX_PATTERN = Pattern.compile(EMAIL_REG_EX);
	
	/** The regular expression pattern for validating a phone number */
	public static final Pattern PHONE_NUMBER_REG_EX_PATTERN = Pattern.compile(PHONE_NUMBER_REG_EX);
	
	/** The regular expression pattern for validating an alpha field */
	public static final Pattern ALPHA_REG_EX_PATTERN =  Pattern.compile(ALPHA_REG_EX);
	
	/** The regular expression pattern for validating an alpha numeric field */
	public static final Pattern ALPHA_NUMERIC_REG_EX_PATTERN = Pattern.compile(ALPHA_NUMERIC_REG_EX);
   
	/** The regular expression pattern for validating an alpha with a space field */
	public static final Pattern ALPHA_WITH_SPACE_REG_EX_PATTERN = Pattern.compile(ALPHA_WITH_SPACE_REG_EX);
	
	/** The regular expression pattern for validating an alpha numeric with a space field */
	public static final Pattern ALPHA_NUMERIC_WITH_SPACE_PATTERN = Pattern.compile(ALPHA_NUMERIC_WITH_SPACE_REG_EX);
	
	/** The regular expression pattern for validating URL */
	public static final Pattern URL_REG_EX_PATTERN = Pattern.compile(URL_REG_EX);
	
	/** The regular expression pattern used for validation, by default it is the email pattern */
	public Pattern regExPattern = EMAIL_REG_EX_PATTERN;

	/**
	 * Constructor - initialises the field name and the error message
	 * @param fieldName the field name of the field to be validated
	 * @param errorMsg the error message to be displayed when validation fails
	 * @param regEx regular expression used for validation, the default is the email address regular expression
	 */
	public RegExpValidationRule(String fieldName, String errorMsg, String regEx)
	{
		this.fieldName = fieldName;
		this.errorMsg = errorMsg;
		if (regEx != null)
			this.regExPattern = Pattern.compile(regEx);
	}

	/**
	 * Constructor - initialises the field name and the error message
	 * @param fieldName the field name of the field to be validated
	 * @param errorMsg the error message to be displayed when validation fails
	 * @param regExPattern regular expression pattern used for validation, the default is the email address regular expression
	 */
	public RegExpValidationRule(String fieldName, String errorMsg, Pattern regExPattern)
	{
		this.fieldName = fieldName;
		this.errorMsg = errorMsg;
		if (regExPattern != null)
			this.regExPattern = regExPattern;
	}

	/**
	 * Validates the input string to make sure it matches with the regular expression
	 * @param inputStr the inputString to validate 
	 * @param regEx the regular expression used for validation
	 * @return true when the input string value matches the regular expression, otherwise false
	 */
	public static boolean validate(String inputStr, String regEx)
	{
		boolean result = false;

		Pattern pattern = Pattern.compile(regEx);
		if (inputStr!=null)
		{
			if (inputStr.trim().length() == 0)
				return true;
			
			Matcher matcher = pattern.matcher(inputStr);
			if (matcher.matches())
				result = true;
		}
		
		return result;
	}
	
	/**
	 * Validates the input string to make sure it matches with the regular expression
	 * @param inputStr the inputString to validate 
	 * @param regExPattern the pattern object of the regular expression used for validation
	 * @return true when the input string value matches the regular expression, otherwise false
	 */
	public static boolean validate(String inputStr, Pattern regExPattern)
	{
		boolean result = false;

		if ((inputStr!=null) && (regExPattern!=null))
		{
			if (inputStr.trim().length() == 0)
				return true;
			
			Matcher matcher = regExPattern.matcher(inputStr);
			if (matcher.matches())
				result = true;
		}
		
		return result;
	}

	/**
	 * Validates the field to make sure the value matches with the regular expression
	 * @param parameters data which holds a list of fields and their values.
	 * @return true when the field value matches the regular expression, otherwise false
	 */
	@Override
	public boolean validate(TableData parameters) 
	{
		String input = parameters.getColumn(fieldName);
		return validate(input, regExPattern);
	}
}