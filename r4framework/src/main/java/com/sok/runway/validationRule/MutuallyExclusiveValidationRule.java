package com.sok.runway.validationRule;

import java.util.Iterator;
import java.util.Set;

import com.sok.framework.TableData;

/**
 * MutuallyExclusiveValidationRule validates a list of field names to make sure 
 * that they are mutually exclusive. Only one field contains a value.
 * 
 * @author raymond.dinata
 */
public class MutuallyExclusiveValidationRule extends ValidationRule
{
	/** List of field names which are mutually exclusive */
	public Set<String> fieldNames = null;

	/**
	 * Constructor - initialising the list of field names to be validated
	 * @param fieldNames list of field names to be validated
	 */
	public MutuallyExclusiveValidationRule(Set<String> fieldNames)
	{
		this.fieldNames = fieldNames;

		// Convert the Set of field names in to a comma separated String
		StringBuffer fieldNameListStr = new StringBuffer();
		Iterator<String> iter = fieldNames.iterator();
		if (iter.hasNext()) 
			fieldNameListStr.append(iter.next());
		while (iter.hasNext()) 
		{
			fieldNameListStr.append(", ");
			fieldNameListStr.append(iter.next());
		}		   
		// Set a custom error message
		this.errorMsg = "The following fields are mutually exclusive [" + fieldNameListStr + "]";
	}

	/**
	 * Validates the field to make sure only one field contains a value.
	 * @param parameters data which holds a list of fields and their values.
	 * @return true when only one of the fields contains a value, otherwise false
	 */
	@Override
	public boolean validate(TableData parameters) 
	{
		boolean result = false;

		int numFound = 0;
		Iterator<String> iter = fieldNames.iterator();
		while (iter.hasNext())
		{
			String fieldName = iter.next();
			if (parameters.getColumn(fieldName).trim().length() > 0) 
				numFound++;
		}
		if (numFound == 1) 
			result = true;
		return result;
	}
}

