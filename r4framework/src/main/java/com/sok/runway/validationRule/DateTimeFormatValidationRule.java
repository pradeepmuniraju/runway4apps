package com.sok.runway.validationRule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sok.framework.TableData;

/**
 * DateTimeFormatValidationRule validates a date or time field to make sure it follows certain format pattern.
 * 
 * @author raymond.dinata
 */
public class DateTimeFormatValidationRule extends ValidationRule
{
	/** The US date format pattern */
	public static final String US_DATE_FORMAT_PATTERN = "MM/dd/yyyy";

	/** The AU date format pattern */
	public static final String AU_DATE_FORMAT_PATTERN = "dd/MM/yyyy";

	/** The hour and minute time format pattern */
	public static final String TIME_FORMAT_PATTERN = "kk:mm";

	/** The hour and minute time format pattern with am or pm */
	public static final String MERIDIEM_TIME_FORMAT_PATTERN = "KK:mm a";
	
	/** The date format pattern used for validation, the default value is AU date format pattern. */
	String formatPattern = AU_DATE_FORMAT_PATTERN;

	/**
	 * Constructor - initialises the field name, error msg, and the format pattern
	 * @param fieldName the field name of the field to be validated
	 * @param errorMsg the error message to be displayed when validation fails
	 * @param formatPattern the format pattern, e.g: dd/mm/yyyy, null means using the default value, which is AU date format pattern
	 */
	public DateTimeFormatValidationRule(String fieldName, String errorMsg, String formatPattern)
	{
		this.fieldName = fieldName;
		this.errorMsg = errorMsg;
		if (formatPattern != null)
			this.formatPattern = formatPattern;
	}

	/**
	 * Converts a String into a Date object using a date or time format pattern
	 * @param inputStr the input string to convert
	 * @param formatPattern the date or time format pattern e.g: dd/mm/yyyy
	 * @return the date object, null if it is not a valid date string
	 */
	public static Date str2Date(String inputStr, String formatPattern)
	{
		Date result = null;
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
		try 
		{
			if ( (inputStr != null) && (inputStr.trim().length()>0) )
			{
				simpleDateFormat.applyPattern(formatPattern);
				simpleDateFormat.setLenient(false);
				Date validDate = simpleDateFormat.parse(inputStr);
				result = validDate;
			}
		}
		catch (ParseException e) 
		{
			// Ignore and return null
		}
		catch (Exception e)
		{
			// Ignore and return null
		}
		return result;
	}
	
	/**
	 * Validates the input string using a date or time format pattern
	 * @param inputStr the input string to validate
	 * @param formatPattern the date or time format pattern e.g: dd/mm/yyyy
	 * @return true when valid, otherwise false
	 */
	public static boolean validate(String inputStr, String formatPattern)
	{
		return str2Date(inputStr, formatPattern) != null;
	}
	
	/**
	 * Validates the field to make sure it follows a given date  or time format pattern
	 * @param parameters data which holds a list of fields and their values.
	 * @return true when the field value follows the given date or time format pattern, otherwise false
	 */
	@Override
	public boolean validate(TableData parameters) 
	{
		String fieldValue = parameters.getColumn(fieldName);
		return validate(fieldValue, formatPattern);
	}
}
