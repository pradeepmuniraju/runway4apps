package com.sok.runway;
import com.sok.framework.*;
public class ProductBean extends RowBean
{

	public void addQuantity(String quantity)
	{
		if(quantity==null || quantity.length()==0)
		{
			put("Quantity","1");
		}
		else
		{
			int qty = Integer.parseInt(quantity);
			int oldqty = Integer.parseInt(getString("Quantity"));
			qty = qty + oldqty;
			updateCosts(qty, oldqty, this);
		}		
	}

	public void setQuantity(String quantity)
	{
		if(quantity==null || quantity.length()==0)
		{
			put("Quantity","1");
		}
		else
		{
			int qty = Integer.parseInt(quantity);
			int oldqty = Integer.parseInt(getString("Quantity"));
			updateCosts(qty, oldqty, this);
		}	
	}

	static void updateCosts(int qty, int oldqty, ProductBean bean)
	{
		bean.put("Quantity",String.valueOf(qty));
		bean.put("Cost",calcQuantityCost(qty, oldqty, bean.get("Cost")));
		bean.put("GST",calcQuantityCost(qty, oldqty, bean.get("GST")));
		bean.put("TotalCost",calcQuantityCost(qty, oldqty, bean.get("TotalCost")));
	}

	static Double calcQuantityCost(int qty, int oldqty, Object value)
	{
		double cost = 0.0;
		if(value instanceof Double)
		{
			cost = ((Double)value).doubleValue();
		}
		else
		{
			cost = Double.parseDouble((String)value);
		}
		return(new Double((cost/oldqty)*qty));
	}

	public void finaliseValues()
	{
		finaliseValues(this, "Cost");
		finaliseValues(this, "GST");
		finaliseValues(this, "TotalCost");
	}

	static void finaliseValues(ProductBean bean, String key)
	{
		Object obj = bean.get(key);
		if(obj instanceof Double)
		{
			bean.put(key,((Double)obj).toString());
		}
	}

}
