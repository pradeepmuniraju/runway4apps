package com.sok.runway.exchange;

import com.moyosoft.exchange.calendar.ResponseType;

public class RunwayExchangeAppointmentAttendee{
   protected String email;
   protected String response;
   
   /*
    * Exchange statuses:
    * Accept
    * Decline 
    * NoResponseReceived 
    * Organizer 
    * Tentative
    * Unknown
    * 
    * Runway statuses:
    * Attended
    * Rejected
    * Tentative
    * Rescheduled
    */
   
   public RunwayExchangeAppointmentAttendee(String email, String response){
      this.email = email;
      this.response = response;
   }
   
   ResponseType getResponseType(){
      if("Attended".equals(response) || "Accept".equals(response)){
         return(ResponseType.Accept);
      }else if("Rejected".equals(response) || "Decline".equals(response)){
         return(ResponseType.Decline);
      }else if("Tentative".equals(response)){
         return(ResponseType.Tentative);
      }else if("NoResponseReceived".equals(response)){
         return(ResponseType.NoResponseReceived);
      }else if("Organizer".equals(response)){
         return(ResponseType.Organizer);
      }
      
      return(ResponseType.Unknown);
   }
   
   public String getResponse(){
      return response;
   }
   
   public String getEmail(){
      return email;
   }   
}
