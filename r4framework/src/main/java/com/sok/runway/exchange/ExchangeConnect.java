/**
 * 
 */
package com.sok.runway.exchange;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import com.moyosoft.exchange.Exchange;
import com.moyosoft.exchange.ExchangeServiceException;
import com.moyosoft.exchange.calendar.ExchangeAttendee;
import com.moyosoft.exchange.calendar.ExchangeCalendarItem;
import com.moyosoft.exchange.calendar.ExchangeMeetingMessage;
import com.moyosoft.exchange.calendar.ExchangeMeetingRequest;
import com.moyosoft.exchange.contact.ExchangeContact;
import com.moyosoft.exchange.folder.ExchangeFolder;
import com.moyosoft.exchange.folder.ExchangeFolderListener;
import com.moyosoft.exchange.folder.FolderContentType;
import com.moyosoft.exchange.folder.FolderType;
import com.moyosoft.exchange.item.ExchangeItem;
import com.moyosoft.exchange.item.ExchangeItemField;
import com.moyosoft.exchange.item.ItemsCollection;
import com.moyosoft.exchange.item.SensitivityChoices;
import com.moyosoft.exchange.mail.ExchangeMail;
import com.moyosoft.exchange.mail.ExchangeMailbox;
import com.moyosoft.exchange.mail.ExchangeResponseMail;
import com.moyosoft.exchange.search.If;
import com.moyosoft.exchange.search.Restriction;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.DatabaseException;
import com.sok.runway.UserBean;
import com.sok.runway.UserCalendar;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;
import org.supercsv.io.ITokenizer;

/**
 * @author dion
 *
 * The ExchangeConnect was written so we didn't need to expose a 3rd party library to the JSP code. 
 * If in the future we need to change libraries then all that will need to change is this one class
 * and maybe the UserBean which is mostly what uses this class
 *  
 */
public class ExchangeConnect {

	private static final Logger logger = LoggerFactory.getLogger(ExchangeConnect.class);

	private String				host = null;
	private String				username = null;
	private String				password = null;
	private String				domain = null;

	private Exchange			exchange = null; 

	private String				version = null;

	private boolean				isConnected = false;

	private String				error = "";

	private SimpleDateFormat	sdd = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat	sdf = new SimpleDateFormat("dd/MM/yyyy hh:mma");
	private SimpleDateFormat	ldf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private SimpleDateFormat	hdf = new SimpleDateFormat("HH:mm:ss");

	private String serverZone = "Australia/Melbourne";
	private String userZone = "Australia/Melbourne";

	private HashMap<String,String> contacts = new HashMap<String,String>();
	
	/**
	 * @param host - the host IP number (best)
	 * @param username - username for Exchange
	 * @param password - Exchange password
	 */
	public ExchangeConnect(String host, String username, String password) {
		this.host = host;
		this.username = username;
		this.password = password;

		// we really don't care about the certificates
		Exchange.Settings.setTrustInvalidCertificate(true);

		init();
	}

	/**
	 * @param host - the host IP number (best)
	 * @param username - username for Exchange
	 * @param password - Exchange password
	 * @param domain the Windows Domain for the Exchange
	 */
	public ExchangeConnect(String host, String username, String password, String domain) {
		this.host = host;
		this.username = username;
		this.password = password;
		this.domain = domain;

		// we really don't care about the certificates
		Exchange.Settings.setTrustInvalidCertificate(true);
		Exchange.Settings.AutoSetTimeZones = false;

		init();
	}

	/**
	 * methode to start the Exchange connection
	 */
	private void init() {
		boolean ssl = false;
		String url = "";

		if (host.toLowerCase().startsWith("https://")) {
			ssl = true;
			host = host.substring("https://".length());
		} else if (host.toLowerCase().startsWith("http://")) {
			host = host.substring("http://".length());
		}

		if (host.indexOf("/") > 0) {
			url = (ssl? "https://" : "http://") + host;
			host = host.substring(0,host.indexOf("/"));
			if (host.indexOf(":") > 0) host = host.substring(0,host.indexOf(":"));
		}

		try {
			try {
				if (url.length() > 0) {
					exchange = new Exchange(host,username,password,domain,url);
				} else {
					exchange = new Exchange(host,username,password,domain,ssl);
				}
				getContacts();
			} catch (IllegalStateException ise) {}
			isConnected = true;
		} catch (ExchangeServiceException e) {
			String p = "";
			for (int x = 0; x < password.length(); ++x) {
				if (x < password.length() / 2) 
					p += password.charAt(x);
				else
					p += "*";
			}

			logger.error("Exchange host for {} with {} {} {} responded with exception {}", new String[]{host, username, domain, p, e.getMessage()});
			if (e.getMessage() != null && e.getMessage().toLowerCase().indexOf("401 - unauthorized") != -1) {
				error += "Your Exchange Password and/or Username are incorrect. please click the settings icon and enter the correct values.";
				return;
			} else if (e.getMessage() != null && e.getMessage().toLowerCase().indexOf("302 - found") != -1) {
				if (exchange != null &&  exchange.getExchangeFullVersion() != null) {
					error = "";
				} else {
					error += "Link Failed " + e.getMessage();
					return;
				}
			} else {
				error += e.getMessage();
				return;
			}
		}
		version = exchange.getExchangeFullVersion();
		logger.error("Exchange host for {} with {} {} reports version {}", new String[]{host, username, domain, version});
	}
	private void getContacts() {
		if (exchange == null) return;
		
		try {
			Iterator<ExchangeFolder> p = exchange.getTopFolders().iterator();
			while (p.hasNext()) {
				ExchangeFolder contactFolder = p.next();
				
				if (contactFolder != null && contactFolder.getContentType() == FolderContentType.Contacts) {
					Iterator<ExchangeItem> i = contactFolder.getItems().iterator();
					
					while (i.hasNext()) {
						ExchangeItem ei = i.next();
						if (ei instanceof ExchangeContact) {
							ExchangeContact ec = (ExchangeContact) ei;
							String name = ec.getDisplayName();
							String email = ec.getEmailAddress1();
							
							logger.error("ExchangeConnect User: " + username + " found " + name + " - " + email);
							
							if (name != null && email != null && name.length() > 0 && email.length() > 0) contacts.put(name.toLowerCase() + "1", email);
							
							email = ec.getEmailAddress2();
							if (name != null && email != null && name.length() > 0 && email.length() > 0) contacts.put(name.toLowerCase() + "2", email);
							
							email = ec.getEmailAddress2();
							if (name != null && email != null && name.length() > 0 && email.length() > 0) contacts.put(name.toLowerCase() + "3", email);
						} else {
							logger.debug(ei.getClass().getCanonicalName());
						}
					}
				}
			}
			
		} catch (ExchangeServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * 
	 * @return true if there is a connection
	 */
	public boolean isConnected() {
		return isConnected;
	}

	public void setServerTimeZone(String zone) {
		serverZone = zone;
	}

	public void setUserTimeZone(String zone) {
		userZone = zone;
	}

	public Date convertToDate(Date date) {
		if (date == null || serverZone.equals(userZone)) return date;

		TimeZone tzServer = TimeZone.getTimeZone(serverZone);
		tzServer.inDaylightTime(date);
		Calendar server = Calendar.getInstance(tzServer);
		server.setTime(date);

		TimeZone tzUser = TimeZone.getTimeZone(userZone);
		tzUser.inDaylightTime(server.getTime());
		Calendar user = Calendar.getInstance(tzUser);
		user.setTimeInMillis(server.getTimeInMillis());

		int dst = tzServer.getOffset(server.getTime().getTime()) - tzUser.getOffset(user.getTime().getTime());

		user.add(Calendar.MILLISECOND, dst);

		logger.trace("convertToDate {} for {} -> {}", new String[]{String.valueOf(dst), server.getTime().toString(), user.getTime().toString()});

		return user.getTime();
	}

	public Date convertFromDate(Date date) {
		if (date == null || serverZone.equals(userZone)) return date;

		TimeZone tzServer = TimeZone.getTimeZone(serverZone);
		tzServer.inDaylightTime(date);
		Calendar server = Calendar.getInstance(tzServer);
		server.setTime(date);

		TimeZone tzUser = TimeZone.getTimeZone(userZone);
		tzUser.inDaylightTime(server.getTime());
		Calendar user = Calendar.getInstance(tzUser);
		user.setTimeInMillis(server.getTimeInMillis());


		int dst = tzUser.getOffset(user.getTime().getTime()) - tzServer.getOffset(server.getTime().getTime());

		user.add(Calendar.MILLISECOND, dst);
		logger.trace("convertFromDate {} for {} -> {}", new String[]{String.valueOf(dst), server.getTime().toString(), user.getTime().toString()});

		return user.getTime();
	}

	public void makeUID(GenRow row, String uid) {
		String uid1 = "", uid2 = "", uid3 = ""; 
		int s = uid.indexOf("AAg");
		if (s > 0) {
			uid1 = uid.substring(0,s);
			uid2 = uid.substring(s);
			s = uid2.indexOf("AAAA");
			if (s > 0) {
				uid3 = uid2.substring(s);
				uid2 = uid2.substring(0, s);
			} else {
				uid3 = "";
			}
		} else {
			uid2 = "";
			s = uid.indexOf("AAAA");
			if (s > 0) {
				uid3 = uid.substring(s);
				uid1 = uid.substring(0, s);
			}
		}
		row.setParameter("ExchangeUID", StringUtil.urlEncode(uid1));
		row.setParameter("ExchangeInstance", StringUtil.urlEncode(uid2));
		row.setParameter("ExchangeAppointmentUID", StringUtil.urlEncode(uid3));
	}

	public String addAppointment(GenRow appointmentRow, Vector<RunwayExchangeAppointmentAttendee> attendees) {
		ExchangeCalendarItem appointment = null;
		try {
			appointment = exchange.createCalendarItem();
		} catch (ExchangeServiceException e) {
			return null;
		}

		try {
			setAppointment(appointment, appointmentRow);
			setAppointmentAttendees(appointment, attendees);

			appointment.saveOnly();

			String uid = appointment.getItemId();
			return uid;
		} catch (ExchangeServiceException e) {
			return null;
		}

	}

	public String updateAppointment(GenRow appointmentRow, Vector<RunwayExchangeAppointmentAttendee> attendees) {
		if (appointmentRow.getColumn("ExchangeUID").length() == 0){
			return addAppointment(appointmentRow, attendees);
		} else {
			StringBuilder uID = new StringBuilder().append(appointmentRow.getString("ExchangeUID")).append(appointmentRow.getString("ExchangeInstance")).append(appointmentRow.getString("ExchangeAppointmentUID"));
			ExchangeCalendarItem appointment = getAppointment(uID.toString());
			if (appointment == null){
				return addAppointment(appointmentRow, attendees);
			}
			else{
				try {
					setAppointment(appointment, appointmentRow);
					setAppointmentAttendees(appointment, attendees);

					// for sum stuffed up reason the start date gets fucked up when updating...
					// appointment.setStart(appointmentRow.getDate("NoteDate"));

					appointment.saveOnly();

					String uid = appointment.getItemId();
					return uid;
				} catch (ExchangeServiceException e) {

				}
			}
		}
		return null;
	}

	/* not used
	public boolean appointmentAttendee(TableData appointmentRow, String email) {
		if (appointmentRow.getColumn("ExchangeUID").length() == 0) return false;

		return appointmentAttendee(appointmentRow.getColumn("ExchangeUID"), email);
	}

	public boolean appointmentAttendee(String exchangeUID, String email) {
		return appointmentAttendee(exchangeUID, email, false);
	}

	public boolean appointmentAttendee(final String exchangeUID, final String email, final boolean required) {
		if (exchangeUID.length() == 0) return false;

		ExchangeCalendarItem appointment = getAppointment(exchangeUID);
		if (appointment == null) return false;

		try {
			if (required) {
					appointment.addRequiredAttendee(email);
			} else { 
					appointment.addOptionalAttendee(email);
			}

			appointment.saveOnly();
		} catch (ExchangeServiceException e) {
			return false;
		}
	    return true;
	}

	public boolean appointmentAttendee(String exchangeUID, List attendees) {
		return appointmentAttendee(exchangeUID, attendees, false);
	}

	public boolean appointmentAttendee(final String exchangeUID, final List attendees, final boolean required) {
		if (exchangeUID.length() == 0) return false;

		ExchangeCalendarItem appointment = getAppointment(exchangeUID);
		if (appointment == null) return false;

		try {
			if (required) {
					appointment.setRequiredAttendees(attendees);
			} else { 
					appointment.setOptionalAttendees(attendees);
			}

			appointment.saveOnly();

		} catch (ExchangeServiceException e) {
			return false;
		}
	    return true;
	}
	 */
	public boolean deleteAppointment(final String exchangeUID) {
		if (exchangeUID.length() == 0) return false;

		ExchangeCalendarItem appointment = getAppointment(StringUtil.urlDecode(exchangeUID));
		if (appointment == null) return false;

		try {
			appointment.delete(false);
		} catch (ExchangeServiceException e) {
			return false;
		}
		return true;
	}

	public Vector syncAppointment(HashMap<String,String> appointments, Date lastLogin, UserBean.syncAppointmentsThread listener) {
		ExchangeFolder calfolder = null;
		try{
			calfolder = exchange.getCalendarFolder();
			if(listener!=null){
				/*
		      try {
		           //this throws com.moyosoft.exchange.ExchangeServiceException: ErrorAccessDenied: Unable to subscribe. Check permissions.
		         //calfolder.addFolderListener(listener);
		      } catch (ExchangeServiceException e) {
		         logger.error("Could not set listener", e);
		      }
				 */
			}
			return syncAllAppointments(appointments, calfolder, lastLogin, listener);

		} catch (ExchangeServiceException e) {
			e.printStackTrace();
		}

		return null;
	}

	public Vector syncAllAppointments(HashMap<String,String> appointments, ExchangeFolder calfolder, Date lastLogin, UserBean.syncAppointmentsThread listener) {
		Vector<RunwayExchangeAppointment> appts = new Vector<RunwayExchangeAppointment>();
		try {

			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DAY_OF_YEAR, -30);
			if (lastLogin == null || lastLogin.before(cal.getTime())){
				lastLogin = cal.getTime(); 
			}

			Restriction restriction = If.CalendarItem.LastModifiedTime.isAfterOrEqualTo(lastLogin);

			ItemsCollection items = calfolder.getItems().restrict(restriction);
			int i = -1;
			for(ExchangeItem item : items)
			{
				i++;
				if(item == null) {
					logger.error("Exchange item {} was null", i);
					continue;
				}
				if(i % 100 == 0) {
					logger.debug("Processing item {}", i);
				}
				
				if (item instanceof ExchangeCalendarItem 
						&& !(item instanceof ExchangeMeetingMessage) 
						&& !(item instanceof ExchangeMeetingRequest)
						&& !(item instanceof ExchangeResponseMail)) {
					try {
						ExchangeCalendarItem app = (ExchangeCalendarItem) item;

						if (!appointments.containsKey(app.getItemId()) && app.getSensitivity() != SensitivityChoices.Private && app.getSensitivity() != SensitivityChoices.Confidential) {

							logger.error("Exchange Appointment {} for {} at {} to {} is of type {}", new String[]{app.getSubject(), username, app.getStart().toString(), app.getEnd().toString(), app.getCalendarItemType().name()});
							if ("RecurringMaster".equals(app.getCalendarItemType().name())) {
								//String uid = app.getUid();
								//String iid = app.getItemId();
								int c = 0;
								int a = 0;
								ExchangeCalendarItem rec = null;
								try {
									while (a < 25 && (rec = app.getRecurrenceOccurrence(++c)) != null) {
										if (rec.getStart().after(lastLogin)) {
											RunwayExchangeAppointment ap = makeAppointment(rec); 
											//if (uid != null && rec != null && uid.equals(rec.getUid())) {
											//	ap.setRepeatID(sdd.format(rec.getRecurrenceId().getTime()));
											//}
											if (!listener.processAppointment(ap)) appts.add(ap);
											logger.error("Exchange Appointment repeat {} of {} for {} date {} to {} is of type {}", new String[]{String.valueOf(a), rec.getSubject(), username, rec.getStart().toString(), rec.getEnd().toString(), rec.getCalendarItemType().name()});
											//logger.error("Exchange Appointment repeat uid {} appid {}", new String[]{rec.getItemId(), rec.getUid()});
											++a;
										}
									}
								} catch (ExchangeServiceException ese) {
								}
								if (a == 0) {
									RunwayExchangeAppointment ap = makeAppointment(app);
									if (!listener.processAppointment(ap)) appts.add(ap);
								}
								logger.error("Exchange Appointment {} for {} is of type {} had {} repeats", new String[]{app.getSubject(), username, app.getCalendarItemType().name(), String.valueOf(a)});
							} else {
								RunwayExchangeAppointment ap = makeAppointment(app);
								if (!listener.processAppointment(ap)) appts.add(ap);
								logger.error("Exchange Appointment {} for {} is of type {}", new String[]{app.getSubject(), username, app.getCalendarItemType().name()});
							}
						}

					} catch (ClassCastException cce) {
						// if it is not of type CalendarItem then forget it and go to the next
					}
				}
				else{
					logger.debug("Exchange Ignoring {} for {}", item.getSubject(),username);
				}
			}

		} catch (ExchangeServiceException e) {
			e.printStackTrace();
		}
		return appts;
	}

	public RunwayExchangeAppointment makeAppointment(ExchangeCalendarItem app) throws ExchangeServiceException {
		RunwayExchangeAppointment ree = new RunwayExchangeAppointment(app.getItemId(), app.getUid(), app.getSubject(), app.getBody(), "",
				getContactEmail(app.getDisplayTo()), app.getStart(), app.getEnd(), app.getDuration(), app.getLastModifiedTime(),
				app.getLocation(), app.isResponseRequested(), String.valueOf(app.getMyResponseType()));
		ree.setAttachment(app.hasVisibleAttachments());
		ree.addAttendees(app.getRequiredAttendees());

		//runway has no way to tell requires from optional
		//ree.addAttendees(app.getOptionalAttendees());
		//ree.setRead(app.isRead());
		ree.setCc(getContactEmail(app.getDisplayCc()));
		//ree.setName(app.getFromName());

		//if (app.getRecurrenceId() != null) {
		//	ree.setRepeatID(String.valueOf(app.getRecurrenceId().getTime()));
		//}

		return ree;

	}
	public static Date getEndDate(Date notedate, String blocks)
	{
		try
		{
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(notedate);
			cal.add(Calendar.MINUTE, UserCalendar.getDurationMinutes(blocks));
			return(cal.getTime());
		}
		catch(Exception e){}

		return(null);
	}	

	public static String getDuration(Date start, Date end)
	{
		long mili = end.getTime() - start.getTime();
		long secs = mili / 1000;
		long mins = secs / 60;
		long hrs = mins / 60;
		long reminder = mins - (hrs * 60);
		StringBuffer duration = new StringBuffer();
		if(hrs == 0){
			duration.append("0");
		}else{
			duration.append(String.valueOf(hrs));
		}
		duration.append(":");
		if(reminder<10){
			duration.append("0");
		}
		duration.append(String.valueOf(reminder));
		return(duration.toString());
	}   

	public Object syncAppointment(GenRow appointmentRow, Vector<RunwayExchangeAppointmentAttendee> attendees) {
		StringBuilder uID = new StringBuilder().append(appointmentRow.getString("ExchangeUID")).append(appointmentRow.getString("ExchangeInstance")).append(appointmentRow.getString("ExchangeAppointmentUID"));
		if (uID.length() == 0) {

			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DAY_OF_YEAR, -30);
			if (appointmentRow.getDate("NoteDate").after(cal.getTime())) {
				return addAppointment(appointmentRow, attendees);
			}

		} else {
			ExchangeCalendarItem appointment = getAppointment(uID.toString());
			if (appointment == null || appointmentRow == null) return null;
			try {
				//System.out.println("Dates " + appointmentRow.getDate("ModifiedDate") + " -> " + appointment.getLastModifiedTime());
				Date modifiedDate = appointmentRow.getDate("ModifiedDate");
				if (modifiedDate == null) {
					modifiedDate = appointmentRow.getDate("CreatedDate");
				}
				if (modifiedDate == null) {
					return null;
				} else if (!modifiedDate.before(appointment.getLastModifiedTime()) && !modifiedDate.after(appointment.getLastModifiedTime())) {
					return appointmentRow.getString("NoteID");
				} else if (modifiedDate.after(appointment.getLastModifiedTime())) {
					String subject = appointmentRow.getString("Subject");
					if (subject.length() == 0){
						subject = StringUtil.toShort(appointmentRow.getString("Body"), 8);
					}
					appointment.setSubject(subject);
					appointment.setBody(appointmentRow.getString("Body"));
					Date noteDate = appointmentRow.getDate("NoteDate");

					appointment.setStart(convertToDate(noteDate));

					Date endDate = getEndDate(noteDate, appointmentRow.getString("Duration"));
					appointment.setEnd(convertToDate(endDate));

					/*
					if (userZone != null && userZone.length() > 0) 
						appointment.setStartTimeZone(TimeZone.getTimeZone(userZone));
					else if (serverZone != null && serverZone.length() > 0) 
						appointment.setStartTimeZone(TimeZone.getTimeZone(serverZone));
					else 
						appointment.setStartTimeZone(TimeZone.getTimeZone("Australia/Melbourne"));
					 */
					appointment.setLocation(appointmentRow.getString("Venue"));

					if ("8:00".equals(appointmentRow.getString("Duration"))){
						appointment.setIsAllDayEvent(true);
					}
					if (appointmentRow.getString("ExchangeURL").length() > 0){ 
						appointment.setMeetingWorkspaceUrl(appointmentRow.getString("ExchangeURL"));
					}
					try {
						appointment.saveOnly();
					} catch (Exception e) {
						logger.error("Exchange Appointment {} for {} failed update {}", new String[]{appointment.getSubject(), username, e.getMessage()});
					}

					String uid = appointment.getItemId();

					GenRow row = new GenRow();
					row.setTableSpec("Notes");
					row.setColumn("NoteID", appointmentRow.getString("NoteID"));
					makeUID(row, uid);

					return row;
				} else {

					//String uid = appointment.getItemId();					
					//GenRow row = new GenRow();
					//row.setTableSpec("Notes");
					//row.setColumn("NoteID", appointmentRow.getString("NoteID"));
					//makeUID(row, uid);

					return makeAppointment(appointment);
				}
			} catch (DatabaseException e) {
				e.printStackTrace();
				return null;
			} catch (ExchangeServiceException e) {
				e.printStackTrace();
				return null;
			}
		}

		return null;
	}

	public RunwayExchangeEmail readEmail(String exchangeUID) {
		if (exchangeUID.length() == 0) return null;

		ExchangeMail mail = getMail(exchangeUID);
		if (mail == null) return null;

		try {
			mail.setIsRead(true);
			mail.save();
		} catch (ExchangeServiceException e) {
		}

		try {
			Date date = mail.getDateTimeReceived();
			if (date == null) date = mail.getDateTimeSent();
			if (date == null) date = mail.getDateTimeCreated();
			if (date == null) date = mail.getLastModifiedTime();

			RunwayExchangeEmail ree = new RunwayExchangeEmail(mail.getItemId(), mail.getSubject(), mail.getBody(), mail.getFromEmailAddress(), getContactEmail(findMailBoxAddress(mail.getToRecipientsMailboxes())), date);
			ree.setAttachment(mail.hasVisibleAttachments());
			ree.setRead(mail.isRead());
			ree.setName(mail.getFromName());

			return ree;
		} catch (ExchangeServiceException e) {
		}


		return null;
	}

	private String findMailBoxAddress(List<ExchangeMailbox> toRecipientsMailboxes) {
		String toEmail = "";
		
		if (toRecipientsMailboxes == null) return toEmail;
		
		for (ExchangeMailbox mb : toRecipientsMailboxes) {
			try {
				if (StringUtils.isNotBlank(mb.getEmailAddress())) {
					if (toEmail.length() > 0) toEmail += "; ";
					toEmail += mb.getEmailAddress();
				}
			} catch (ExchangeServiceException e) {
			}
		}
		
		return toEmail;
	}

	public int countMailInbox() {
		ExchangeFolder mailbox = null;
		try {
			mailbox = exchange.getInboxFolder();
			return mailbox.getItemsCount();
		} catch (ExchangeServiceException e) {
		}

		return -1;
	}

	public int countMailOutbox() {
		ExchangeFolder mailbox = null;
		try {
			mailbox = exchange.getSentItemsFolder();
			return mailbox.getItemsCount();
		} catch (ExchangeServiceException e) {
		}

		return -1;
	}

	public Vector syncMailInbox(String lastLogin) {
		ExchangeFolder mailbox = null;
		try {
			mailbox = exchange.getInboxFolder();
			return syncMail(mailbox, sdf.parse(lastLogin));
		} catch (ParseException e) {
			try {
				return syncMail(mailbox, ldf.parse(lastLogin));
			} catch (ParseException e1) {
				return syncMail(mailbox, (Date) null);
			}
		} catch (ExchangeServiceException e) {
			e.printStackTrace();
		}

		return null;
	}

	public Vector syncMailOutbox(String lastLogin) {
		ExchangeFolder mailbox = null;
		try {
			mailbox = exchange.getSentItemsFolder();
			return syncMail(mailbox, sdf.parse(lastLogin));
		} catch (ParseException e) {
			try {
				return syncMail(mailbox, ldf.parse(lastLogin));
			} catch (ParseException e1) {
				return syncMail(mailbox, (Date) null);
			}
		} catch (ExchangeServiceException e) {
			e.printStackTrace();
		}

		return null;
	}

	public Vector syncMail(ExchangeFolder mailbox, Date lastLogin) {
		Vector<RunwayExchangeEmail> mails = new Vector<RunwayExchangeEmail>();
		try {


			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DAY_OF_YEAR, -30);
			if (lastLogin == null || lastLogin.before(cal.getTime())) lastLogin = cal.getTime(); 

			Restriction restriction = If.Message.DateTimeReceived.isAfter(lastLogin);

			ItemsCollection items = mailbox.getItems().restrict(restriction).sortBy(ExchangeItemField.item_DateTimeReceived);

			for(ExchangeItem item : items)
			{
				logger.trace("Processing Email {} " , item.getItemId());
				logger.trace("Got an email to {} with subject {}" , item.getDisplayTo(),  item.getSubject());
				
				if (item instanceof ExchangeMail) {
					try {
						ExchangeMail mail = (ExchangeMail) item;

						Date date = mail.getDateTimeReceived();
						if (date == null) date = mail.getDateTimeSent();
						if (date == null) date = mail.getDateTimeCreated();
						if (date == null) date = mail.getLastModifiedTime();

						RunwayExchangeEmail ree = new RunwayExchangeEmail(mail.getItemId(), mail.getSubject(), mail.getBody(), mail.getFromEmailAddress(), getContactEmail(findMailBoxAddress(mail.getToRecipientsMailboxes())), date);
						ree.setAttachment(mail.hasVisibleAttachments());
						ree.setRead(mail.isRead());
						ree.setCc(getContactEmail(mail.getDisplayCc()));
						ree.setName(mail.getFromName());

						mails.add(ree);
						logger.trace("GetContactEmail for to  {} ",  getContactEmail(findMailBoxAddress(mail.getToRecipientsMailboxes())));
						logger.trace("Processed ExchangeMail from {}, cc {} ", mail.getFromEmailAddress(), mail.getDisplayCc());
						logger.trace("body {} ",  mail.getBody());
					} catch (ClassCastException cce) {
						// if it is not of type ExchnageMail then forget it and go to the next
						logger.trace("not of type ExchangeMail ");
					}
				} else {
					logger.trace("Ignoring email as it is not an instance of ExchangeMail. Subject {} , ItemID {}" , item.getSubject(), item.getItemId());
					logger.trace("Email to {} " , item.getDisplayTo());
				}
			}

		} catch (ExchangeServiceException e) {
		}
		return mails;
	}

	protected String getContactEmail(String contact) {
		if (contact == null) return contact;

		if (contact.indexOf(";") == -1) {
			return getContactEmailSingle(contact.trim());
		} else {
			String[] parts = contact.split(";");
			if (parts != null) {
				String out = "";
				for (int p = 0; p < parts.length; ++p) {
					if (parts[p] != null && parts[p].length() > 0) {
						if (out.length() > 0) out += "; ";
						out += getContactEmailSingle(parts[p].trim());
					}
				}
				return out;
			}
		}

		return contact;
	}

	// this needs to be fixed so it doesn't use a for loop
	protected String getContactEmailSingle(String contact) {
		if (contact == null) return contact;

		if (contact.startsWith("'")) contact = contact.substring(1);
		if (contact.endsWith("'")) contact = contact.substring(0,contact.length() - 1);

		if (contact.indexOf("@") > 0 && contact.indexOf(" @ ") == -1) return contact;
		
		String cEmail = contacts.get(contact.trim().toLowerCase() + "1");
		if (cEmail != null && cEmail.indexOf("@") > 0 && cEmail.indexOf(" @ ") == -1) {
			logger.error("Found email 1 " + contact + " - " + cEmail);
			return cEmail;
		}

		cEmail = contacts.get(contact.trim().toLowerCase() + "2");
		if (cEmail != null && cEmail.indexOf("@") > 0 && cEmail.indexOf(" @ ") == -1) {
			logger.error("Found email 2 " + contact + " - " + cEmail);
			return cEmail;
		}

		cEmail = contacts.get(contact.trim().toLowerCase() + "3");
		if (cEmail != null && cEmail.indexOf("@") > 0 && cEmail.indexOf(" @ ") == -1) {
			logger.error("Found email 3 " + contact + " - " + cEmail);
			return cEmail;
		}

		try {
			for(ExchangeMailbox item : exchange.resolveNames(contact.trim(),true))
			{
				ExchangeMailbox mailbox = (ExchangeMailbox) item;
				if (mailbox.getEmailAddress() != null && mailbox.getEmailAddress().length() > 0) {
					return mailbox.getEmailAddress();
				}
			}

		} catch (ExchangeServiceException e) {
		}

		return contact;
	}

	protected ExchangeCalendarItem getAppointment(String exchangeUID) {
		if (exchangeUID.length() == 0 || !isConnected) return null;

		ExchangeCalendarItem appointment = null;
		try {
			appointment = (ExchangeCalendarItem) exchange.getItem(StringUtil.urlDecode(exchangeUID));
			if (appointment == null) return null;
		} catch (ExchangeServiceException e) {
			try {
				if (appointment == null) appointment = (ExchangeCalendarItem) exchange.getItem(exchangeUID);
				if (appointment == null) return null;
			} catch (ExchangeServiceException e2) {
				return null;
			} catch (IllegalStateException i2) {
				isConnected = false;
				init();
				appointment = getAppointment(exchangeUID);
			}
		} catch (IllegalStateException i) {
			isConnected = false;
			init();
			appointment = getAppointment(exchangeUID);
		}

		return appointment;
	}

	protected ExchangeMail getMail(String exchangeUID) {
		if (exchangeUID.length() == 0 || !isConnected) return null;

		ExchangeMail mail = null;
		try {
			mail = (ExchangeMail) exchange.getItem(StringUtil.urlDecode(exchangeUID));

			if (mail == null) return null;
		} catch (ExchangeServiceException e) {
			return null;
		} catch (IllegalStateException i) {
			isConnected = false;
			init();
			mail = getMail(exchangeUID);
		}

		return mail;
	}

	protected void setAppointment(ExchangeCalendarItem appointment, GenRow appointmentRow) throws ExchangeServiceException {

		String subject = appointmentRow.getColumn("Subject");
		if (subject.length() == 0) subject = StringUtil.toShort(appointmentRow.getColumn("Body"), 8);

		/*
		if (userZone != null && userZone.length() > 0) 
			appointment.setStartTimeZone(TimeZone.getTimeZone(userZone));
		else if (serverZone != null && serverZone.length() > 0) 
			appointment.setStartTimeZone(TimeZone.getTimeZone(serverZone));
		else 
			appointment.setStartTimeZone(TimeZone.getTimeZone("Australia/Melbourne"));
		 */
		appointment.setSubject(subject);
		appointment.setBody(appointmentRow.getColumn("Body"));
		appointment.setStart(convertToDate(appointmentRow.getDate("NoteDate")));
		appointment.setEnd(convertToDate(appointmentRow.getDate("EndDate")));

		appointment.setLocation(appointmentRow.getColumn("Venue"));

		if ("8:00".equals(appointmentRow.getColumn("Duration"))) appointment.setIsAllDayEvent(true);

		if (appointmentRow.getColumn("ExchangeURL").length() > 0) 
			appointment.setMeetingWorkspaceUrl(appointmentRow.getColumn("ExchangeURL"));
	}

	protected boolean hasAttendee(java.util.List<ExchangeAttendee> list, String email){
		Iterator<ExchangeAttendee> itr = list.iterator();
		ExchangeAttendee attendee = null;
		while (itr.hasNext()) {
			attendee = itr.next();

			try{
				if(email.equals(attendee.getMailbox().getEmailAddress())){
					return(true);
				}
			}
			catch(ExchangeServiceException e){
				e.printStackTrace();
			}

		}
		return false;
	}

	protected void setAppointmentAttendees(ExchangeCalendarItem appointment, Vector<RunwayExchangeAppointmentAttendee> attendees) {
		try{
			List<ExchangeAttendee> list = appointment.getRequiredAttendees();
			String email = null;
			if(list!=null){
				for(int i=0; i<attendees.size(); i++){
					email = attendees.get(i).getEmail();
					if(email!=null && email.length()!=0){
						if(!hasAttendee(list, email)){
							try{
								appointment.addRequiredAttendee(email);
							}
							catch(ExchangeServiceException e){
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
		catch(ExchangeServiceException e){
			e.printStackTrace();
		}
	}

	public class RunwayExchangeEmail {
		private String 			uid = null;
		private String			subject = null;
		private String			body = null;
		private Date			date = null;
		private boolean			attachment = false;
		private boolean			read = true;
		private boolean			deleted = true;

		private	String			from = null;
		private String			name = null;
		private String			to = null;

		private String			cc = null;
		private String			bcc = null;
		
		private String 			uid1 = null;
		private String			uid2 = null;
		private String			uid3 = null;

		public RunwayExchangeEmail(String uid, String subject, String body, String from, String to, Date date) {
			this.uid = uid;
			this.subject = subject;
			this.body = body;
			this.from = from;
			this.to = to;
			this.date = convertFromDate(date);
			if (this.subject == null || this.subject.length() == 0) {
				if (this.body != null && !this.body.toLowerCase().trim().startsWith("<html")) {
					this.body = this.body.trim();
					this.subject = StringUtil.toShort(this.body, 10);
				}
			} else {
				this.subject = this.subject.trim();
			}
			if (this.subject == null || this.subject.trim().length() == 0) this.subject = "- no subject -";
			if (this.body == null) body = "";
			
			makeUID();
		}
		
		private void makeUID() {
			int s = uid.indexOf("AAgI");
			if (s > 0) {
				uid1 = uid.substring(0,s);
				uid2 = uid.substring(s);
				s = uid2.indexOf("AAAA");
				if (s > 0) {
					uid3 = uid2.substring(s);
					uid2 = uid2.substring(0, s);
				} else {
					uid3 = "";
				}
			} else {
				uid2 = "";
				s = uid.indexOf("AAAA");
				if (s > 0) {
					uid3 = uid.substring(s);
					uid1 = uid.substring(0, s);
				}
			}
		}
		
		
		public String getUid() {
			return uid;
		}

		public void setUid(String uid) {
			this.uid = uid;
			makeUID();
		}
		
		public String getExchangeUserID() {
			return uid1;
		}

		public String getExchangeInstanceID() {
			return uid2;
		}

		public String getExchangeItemID() {
			return uid3;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getBody() {
			return body;
		}

		public void setBody(String body) {
			this.body = body;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public boolean hasAttachment() {
			return attachment;
		}

		public void setAttachment(boolean attachment) {
			this.attachment = attachment;
		}

		public String getFrom() {
			return from;
		}

		public void setFrom(String from) {
			this.from = from;
		}

		public String getTo() {
			return to;
		}

		public void setTo(String to) {
			this.to = to;
		}

		public String getCc() {
			return cc;
		}

		public void setCc(String cc) {
			this.cc = cc;
		}

		public String getBcc() {
			return bcc;
		}

		public void setBcc(String bcc) {
			this.bcc = bcc;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean isRead() {
			return read;
		}

		public void setRead(boolean read) {
			this.read = read;
		}

		public boolean isAttachment() {
			return attachment;
		}

		public boolean isDeleted() {
			return deleted;
		}

		public void setDeleted(boolean deleted) {
			this.deleted = deleted;
		}
	}

	public class RunwayExchangeAppointment extends RunwayExchangeEmail {

		public Vector<RunwayExchangeAppointmentAttendee>	attendees = new Vector<RunwayExchangeAppointmentAttendee>();

		private String duration = null;
		private Date enddate = null;
		private Date modifieddate = null;
		private String location = null;
		private String appointmentid = null;
		private boolean responserequired = false;
		private String myresponsetype = null;
		private String repeatID = null;

		public RunwayExchangeAppointment(String uid, String appointmentid, String subject,
				String body, String from, String to, Date date, Date enddate, String duration, Date modifieddate,
				String location, boolean responserequired, String myresponsetype) {
			super(uid, subject, body, from, to, date);
			this.duration = duration;
			this.enddate = convertFromDate(enddate);
			this.modifieddate = modifieddate;
			this.location = location;
			this.appointmentid = appointmentid;
			this.responserequired = responserequired;
			this.myresponsetype = myresponsetype;
		}

		public void addAttendees(List<ExchangeAttendee> attendees) {
			if (attendees != null ) {
				for (ExchangeAttendee attendee : attendees) {
					try {
						this.attendees.add(new RunwayExchangeAppointmentAttendee(attendee.getMailbox().getEmailAddress(), attendee.getResponseType().toString()));
						if(logger.isDebugEnabled()) logger.debug("Attendee Response: {} ; {}", attendee.getResponseType().name(), attendee.getResponseType().toString());
					} catch (ExchangeServiceException e) {
						logger.error("Error adding exchange attendee", e);
					}
				}
			}
		}



		public Vector<RunwayExchangeAppointmentAttendee> getAttendees() {
			return attendees;
		}

		public String getDuration() {
			return duration;
		}

		public boolean isResponseRequired() {
			return responserequired;
		}

		public String getMyResponsetype() {
			return myresponsetype;
		}      

		public String getLocation() {
			return location;
		}    

		public String getAppointmentid() {
			if (repeatID != null && repeatID.length() > 0) {
				return repeatID + ":" + appointmentid;
			}
			return appointmentid;
		}

		public void setDuration(String duration) {
			this.duration = duration;
		}		

		public Date getEndDate() {
			return enddate;
		}

		public void setEndDate(Date date) {
			this.enddate = date;
		}

		public Date getModifiedDate() {
			return modifieddate;
		}

		public String getRepeatID() {
			return repeatID;
		}

		public void setRepeatID(String repeatID) {
			this.repeatID = repeatID;
		}

//		@Override
//		public String getUid() {
//			if (repeatID != null && repeatID.length() > 0) {
//				return repeatID + ":" + super.getUid();
//			}
//
//			return super.getUid();
//		}
	}

	public String getError() {
		return error;
	}
}
