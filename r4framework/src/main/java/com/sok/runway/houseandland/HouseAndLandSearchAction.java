package com.sok.runway.houseandland;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.GenRow;

public class HouseAndLandSearchAction {

    public GenRow getSalesRep(HttpServletRequest request) {
	GenRow linkedUserList = new GenRow();
	linkedUserList.setViewSpec("UserSelectView");
	linkedUserList.setRequest(request);
	linkedUserList.setParameter("Status", "Active");
	linkedUserList.setParameter("-sort1", "FirstName");
	linkedUserList.setParameter("-sort2", "LastName");
	if (request.getParameter("GroupID") != null) linkedUserList.setParameter("UserGroups.GroupID", request.getParameter("GroupID"));
	linkedUserList.doAction("search");
	linkedUserList.getResults();
	return linkedUserList;
    }
    
}
