/**
 * 
 */
package com.sok.runway.houseandland;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.RunwayUtil;
import com.sok.framework.SpecManager;
import com.sok.framework.StringUtil;
import com.sok.framework.TableSpec;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.runway.crm.cms.properties.PropertyType;
import com.sok.runway.crm.cms.properties.publishing.PublishMethod;
import com.sok.runway.offline.ThreadManager;
import com.sok.runway.offline.halManager.HouseAndLandManager;
import com.sok.runway.offline.rpmManager.RPMtask;

/**
 * @author dion
 * 
 */
public class CreateHouseAndLand extends Thread {

	private static final Logger logger = LoggerFactory
			.getLogger(CreateHouseAndLand.class);

	private String id = "";

	private Map<String, GenRow> productMap = Collections
			.synchronizedMap(new HashMap<String, GenRow>());
	private Map<String, ArrayList<Map<String, String>>> costingsMap = Collections
			.synchronizedMap(new HashMap<String, ArrayList<Map<String, String>>>());
	private Map<String, ArrayList<Map<String, String>>> linkMap = Collections
			.synchronizedMap(new HashMap<String, ArrayList<Map<String, String>>>());
	private Map<String, Map<String, String>> documentMap = Collections
			.synchronizedMap(new HashMap<String, Map<String, String>>());
	private Map<String, String> costingsLinks = Collections
			.synchronizedMap(new HashMap<String, String>());

	private NumberFormat currency = NumberFormat.getCurrencyInstance();
	private NumberFormat number = NumberFormat.getNumberInstance();

	protected ArrayList<String> createdpackages = new ArrayList<String>();
	protected ArrayList<String> houselandids = null;
	private ArrayList<String> houseproductids = null;
	private ArrayList<String> landproductids = null;
	private HashMap<String, String> selectedfacades = null;
	private String userid = null;

	private HttpServletRequest request = null;

	private String packageCostLibraryID = "";

	private boolean createDuplicates = false;

	protected int maxPackages = 0;
	private int maxSteps = 0;
	private AtomicInteger currentPackages = new AtomicInteger(0);
	private AtomicInteger currentStep = new AtomicInteger(0);
	private AtomicInteger threadCount = new AtomicInteger(0);
	private AtomicInteger processingPackages = new AtomicInteger(0);

	protected AtomicInteger passedRules = new AtomicInteger(0);
	protected AtomicInteger failedRules = new AtomicInteger(0);
	protected AtomicInteger failedDuplicates = new AtomicInteger(0);

	protected StringBuffer failedMessages = new StringBuffer();
	private ThreadLocal<String> failedCalc = new ThreadLocal<String>();
	private StringBuffer log = new StringBuffer();

	private Date startTime = null;

	private double timePackage = 0;
	private double timePerPackage = 0;
	private double estimatedTime = 0;
	private double estimatedRemaining = 0;

	private boolean isRunning = false;
	private boolean hasStarted = false;

	private Connection con = null;
	private Context ctx = null;

	String sequenceNum = "";

	protected String dbconn;
	// protected ServletContext context;
	protected String contextpath;
	// protected String contextname = null;
	protected Locale thislocale;

	protected HashMap<String, ArrayList<PackageRule>> rules = new HashMap<String, ArrayList<PackageRule>>();
	protected HashSet<String> duplicates = new HashSet<String>();

	private SimpleDateFormat dformat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	private SimpleDateFormat sformat = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat fformat = new SimpleDateFormat(
			"yyyy-MM-dd_HH_mm_ss");

	private boolean keepRep = true;
	private boolean keepDates = true;
	private boolean keepPublish = true;
	private boolean keepStatus = true;
	private boolean keepBad = false;

	private boolean updateCosts = false;
	private boolean updateOptions = false;
	private boolean updateFeatures = false;
	private boolean updateDrips = false;
	private boolean updateDims = false;
	private boolean updatePlan = false;
	private boolean updatePrice = false;
	private boolean updateDetails = false;
	private boolean updateAnswers = false;
	private boolean keepFacade = false;
	private boolean updatePublishHeadline = false;
	private boolean updatePublishDescription = false;
	private boolean updateImages = false;
	private boolean fullUpdate = false;
	private boolean isPortal = false;

	private boolean resetContents = false;
	private boolean recreateTask = false;

	private long startTimeLong = 0;

	private String lastLotID = "";

	private String preAvailableDate = null;
	private String availableDate = null;
	private String expiryDate = null;

	private String groupID = null;
	private String repUserID = null;
	private String statusID = null;
	private String packageStatus = "Pending";
	private String active = "N";
	private String showOnPortal = "N";
	private String marketingStatus = null;
	private String productSubType = null;
	private String displayPriceType = null;
	private boolean isSendEmailNotification = false;

	private boolean includeBuilderGroups;
	private boolean includeBrandGroups;
	private boolean includeRangeGroups;
	private boolean includeDesignGroups;
	private boolean includePlanGroups;
	private boolean includeDeveloperGroups;
	private boolean includeEstateGroups;
	private boolean includeStageGroups;
	private boolean includeLandGroups;
	

	/**
	 * 
	 */
	public CreateHouseAndLand(HttpServletRequest request, ServletContext context) {
		// this.context = context;
		// contextname = context.getServletContextName();
		contextpath = request.getContextPath();
		dbconn = InitServlet.getSystemParam("sqlJndiName");
		thislocale = request.getLocale();

		preAvailableDate = request.getParameter("PreAvailableDate");
		availableDate = request.getParameter("AvailableDate");
		expiryDate = request.getParameter("ExpiryDate");

		groupID = request.getParameter("GroupID");
		repUserID = request.getParameter("RepUserID");
		statusID = request.getParameter("StatusID");
		packageStatus = request.getParameter("PackageStatus");
		active = request.getParameter("Active");
		showOnPortal = request.getParameter("ShowOnPortal");
		marketingStatus = request.getParameter("MarketingStatus");
		productSubType = request.getParameter("ProductSubType");
		displayPriceType = request.getParameter("DisplayPriceType");
		isSendEmailNotification = "true".equals(request
				.getParameter("SendEmailNotification"));
		
		includeBuilderGroups = "Y".equals(request.getParameter("IncludeBuilderGroups"));
		includeBrandGroups = "Y".equals(request.getParameter("IncludeBrandGroups"));
		includeRangeGroups = "Y".equals(request.getParameter("IncludeRangeGroups")); 	
		includeDesignGroups = "Y".equals(request.getParameter("IncludeDesignGroups"));
		includePlanGroups = "Y".equals(request.getParameter("IncludePlanGroups"));
		includeDeveloperGroups = "Y".equals(request.getParameter("IncludeDeveloperGroups"));
		includeEstateGroups = "Y".equals(request.getParameter("IncludeEstateGroups"));
		includeStageGroups = "Y".equals(request.getParameter("IncludeStageGroups"));
		includeLandGroups = "Y".equals(request.getParameter("IncludeLandGroups"));
	}

	public CreateHouseAndLand(String contextpath, Locale locale,
			ServletContext context) {
		// this.context = context;
		// contextname = context.getServletContextName();
		this.contextpath = contextpath;
		dbconn = InitServlet.getSystemParam("sqlJndiName");
		thislocale = locale;
	}

	public CreateHouseAndLand(GenRow row) {
		// contextpath = request.getContextPath();
		dbconn = InitServlet.getSystemParam("sqlJndiName");
		// thislocale = request.getLocale();

		preAvailableDate = row.getString("PreAvailableDate");
		availableDate = row.getString("AvailableDate");
		expiryDate = row.getString("ExpiryDate");

		groupID = row.getString("GroupID");
		repUserID = row.getString("RepUserID");
		statusID = row.getString("StatusID");
		packageStatus = row.getString("PackageStatus");
		active = row.getString("Active");
		marketingStatus = row.getString("MarketingStatus");
		productSubType = row.getString("ProductSubType");
		displayPriceType = row.getString("DisplayPriceType");
		isSendEmailNotification = "true".equals(row.getString("SendEmailNotification"));
	}

	public void setPackageCostLibrary(String libID) {
		packageCostLibraryID = libID;
	}

	public GenRow findCostings(String productID, String parentID) {
		GenRow costings = new GenRow();
		costings.setTableSpec("PackageCostings");
		costings.setConnection(this.getConnection());
		costings.setParameter("-select0", "*");
		costings.setColumn("ParentProductID", productID);
		costings.setColumn("ParentID", parentID);
		costings.sortBy("ApplyToID", 0);
		costings.sortOrder("DESC", 0);
		costings.doAction("search");

		costings.getResults(true);

		if (costings.isSuccessful() && costings.getSize() > 0) {
			return costings;
		}
		costings.close();

		GenRow product = getCachedTabledProduct("ProductsDrips", "ProductID",
				productID);
		// GenRow product = new GenRow();
		// product.setTableSpec("Products");
		// product.setConnection(this.getConnection());
		// product.setParameter("-select0", "PackageCostProductID");
		// product.setColumn("ProductID", productID);
		// product.doAction("selectfirst");

		if (product.isSuccessful()
				&& product.getString("PackageCostProductID").length() > 0) {
			return findCostings(product.getString("PackageCostProductID"),
					parentID);
		}

		return null;
	}

	public GenRow findRules(String productID, String parentID) {
		GenRow costings = new GenRow();
		costings.setTableSpec("PackageRules");
		costings.setConnection(this.getConnection());
		costings.setParameter("-select0","PackageRuleProduct.Description AS Description");
		costings.setParameter("-select1", "*");
		costings.setColumn("ProductID", productID);
		costings.setColumn("ParentID", parentID);
		costings.sortBy("ApplyToID", 0);
		costings.sortOrder("DESC", 0);
		costings.doAction("search");

		costings.getResults(true);

		if (costings.isSuccessful() && costings.getSize() > 0) {
			return costings;
		}
		costings.close();

		GenRow product = getCachedTabledProduct("Products", "ProductID",
				productID);
		// GenRow product = new GenRow();
		// product.setTableSpec("Products");
		// product.setConnection(this.getConnection());
		// product.setParameter("-select", "CopiedFromProductID");
		// product.setColumn("ProductID", productID);
		// product.doAction("selectfirst");

		if (product.isSuccessful()
				&& product.getString("CopiedFromProductID").length() > 0) {
			return findRules(product.getString("PackageCostProductID"), parentID);
		}

		return null;
	}

	public String copy(String tablespecname, String productidfieldname,
			String fromproductid, String toproductid, String userid) {

		if (fromproductid == null || fromproductid.length() == 0)
			return null;
		if (toproductid == null || toproductid.length() == 0)
			return null;

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);

		TableSpec ts = row.getTableSpec();

		row.setConnection(this.getConnection());
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.setConnection(this.getConnection());
			clone.putAllDataAsParameters(row);
			if ("Products".equals(row.getTableSpec().getTableName())) {
				clone.setParameter("CopiedFromProductID", fromproductid);
				clone.setParameter("Category", "");
				clone.setParameter("ProductType", "House and Land");
				clone.setParameter("UpdateByProcess", "Y");
				clone.remove("PreAvailableDate");
				clone.remove("AvailableDate");
				clone.remove("DripCost");
				clone.remove("DripResult");
			}
			if ("ProductDetails".equals(row.getTableSpec().getTableName())) {
				if (row.getString("ProductType").indexOf("Facade") >= 0)
					clone.setParameter("ProductType", "HomeFacadeDetails");
				else if (row.getString("ProductType").startsWith("Home"))
					clone.setParameter("ProductType", "HomePlanDetails");
				else if (row.getString("ProductType").startsWith("Land"))
					clone.setParameter("ProductType", "LandDetails");
				clone.setParameter("CopiedFromProductID", fromproductid);
			}
			clone.setParameter("CreatedDate", "NOW");
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("CreatedBy", userid);
			clone.setParameter("ModifiedBy", userid);
			clone.remove(row.getTableSpec().getPrimaryKeyName());
			clone.createNewID();
			clone.setParameter(productidfieldname, toproductid);
			try {
				clone.doAction("insert");
			} catch (Exception e) {
				logger.error("error in {} of {}", "copy", e.getMessage());
				clone.setParameter(productidfieldname, toproductid);
				try {
					clone.doAction("updated");
				} catch (Exception e1) {
				}
			}
		}
		row.close();
		return (clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	public String copyCommissions(String tablespecname,
			String productidfieldname, String fromproductid,
			String toproductid, String userid) {
		if (fromproductid == null || fromproductid.length() == 0)
			return null;
		if (toproductid == null || toproductid.length() == 0)
			return null;

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);
		row.setConnection(this.getConnection());
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		Set<String> existingComAndPymntTemplates = getExistingComAndPymntTemplates(toproductid);

		GenRow clone = new GenRow();
		while (row.getNext()) {
			if (!existingComAndPymntTemplates.contains(row
					.getString("CommissionTemplateID"))) {
				clone.clear();
				clone.setTableSpec(tablespecname);
				clone.setConnection(this.getConnection());
				clone.putAllDataAsParameters(row);
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				clone.remove(row.getTableSpec().getPrimaryKeyName());
				clone.createNewID();
				clone.setParameter(productidfieldname, toproductid);
				try {
					clone.doAction("insert");
				} catch (Exception e) {
					logger.error("error in {} of {}", "copy", e.getMessage());
				}
			}
		}
		row.close();
		return (clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	public String copyDripCostingsCached(String fromproductid,
			String toproductid, String userid) {
		ArrayList<Map<String, String>> costing = costingsMap.get(fromproductid);
		if (costing != null) {
			HashMap<String, String> keys = new HashMap<String, String>();
			for (Map<String, String> keyValue : costing) {
				if (keyValue != null && keyValue.size() > 0) {
					keys.put(keyValue.get("CostingID"), KeyMaker.generate());
				}
			}

			GenRow clone = new GenRow();
			clone.setTableSpec("PackageCostings");
			clone.setConnection(this.getConnection());
			for (Map<String, String> keyValue : costing) {
				if (keyValue != null && keyValue.size() > 0) {
					clone.putAll(keyValue);
					clone.setParameter("CreatedDate", "NOW");
					clone.setParameter("ModifiedDate", "NOW");
					clone.setParameter("CreatedBy", userid);
					clone.setParameter("ModifiedBy", userid);
					clone.setParameter("ParentProductID", toproductid);
					clone.setParameter("CostingID",
							keys.get(keyValue.get("CostingID")));
					if (keyValue.get("ParentID") != null
							&& keyValue.get("ParentID").length() > 0)
						clone.setParameter("ParentID",
								keys.get(keyValue.get("ParentID")));
					try {
						clone.doAction("insert");
					} catch (Exception e) {
						clone.clearStatement();
						clone.setParameter("ParentProductID", toproductid);
						clone.doAction("update");
						logger.error("error in {} of {}", "copyDripCostings",
								e.getMessage());
					}
				}
			}
		} else {
			HashMap<String, String> keys = new HashMap<String, String>();
			costing = new ArrayList<Map<String, String>>();
			GenRow row = new GenRow();
			row.setTableSpec("PackageCostings");

			TableSpec ts = row.getTableSpec();

			row.setConnection(this.getConnection());
			row.setParameter("-select1", "*");
			row.setParameter("ParentProductID", fromproductid);
			row.doAction(ActionBean.SEARCH);
			row.getResults();

			while (row.getNext()) {
				keys.put(row.getString("CostingID"), KeyMaker.generate());
				costing.add(row.getDataMap());
			}
			costingsMap.put(fromproductid, costing);

			row.close();

			GenRow clone = new GenRow();
			clone.setTableSpec("PackageCostings");
			clone.setConnection(this.getConnection());
			for (Map<String, String> keyValue : costing) {
				if (keyValue != null && keyValue.size() > 0) {
					clone.putAll(keyValue);
					clone.setParameter("CreatedDate", "NOW");
					clone.setParameter("ModifiedDate", "NOW");
					clone.setParameter("CreatedBy", userid);
					clone.setParameter("ModifiedBy", userid);
					clone.setParameter("ParentProductID", toproductid);
					clone.setParameter("CostingID",
							keys.get(keyValue.get("CostingID")));
					if (keyValue.get("ParentID") != null
							&& keyValue.get("ParentID").length() > 0)
						clone.setParameter("ParentID",
								keys.get(keyValue.get("ParentID")));
					try {
						clone.doAction("insert");
					} catch (Exception e) {
						clone.clearStatement();
						clone.setParameter("ParentProductID", toproductid);
						clone.doAction("update");
						logger.error("error in {} of {}", "copyDripCostings",
								e.getMessage());
					}
				}
			}
		}

		return "";
	}

	public String copyDripCostings(String tablespecname,
			String productidfieldname, String fromproductid,
			String toproductid, String userid) {

		HashMap<String, String> keys = new HashMap<String, String>();

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);

		TableSpec ts = row.getTableSpec();

		row.setConnection(this.getConnection());
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		while (row.getNext()) {
			keys.put(row.getString(row.getTableSpec().getPrimaryKeyName()),
					KeyMaker.generate());
		}

		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.setConnection(this.getConnection());
			clone.putAllDataAsParameters(row);
			clone.setParameter("CreatedDate", "NOW");
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("CreatedBy", userid);
			clone.setParameter("ModifiedBy", userid);
			// clone.remove(row.getTableSpec().getPrimaryKeyName());
			// clone.createNewID();
			clone.setParameter(productidfieldname, toproductid);
			clone.setParameter(row.getTableSpec().getPrimaryKeyName(), keys
					.get(row.getString(row.getTableSpec().getPrimaryKeyName())));
			if (row.getString("ParentID").length() > 0)
				clone.setParameter("ParentID",
						keys.get(row.getString("ParentID")));
			try {
				clone.doAction("insert");
			} catch (Exception e) {
				clone.clearStatement();
				clone.setParameter(productidfieldname, toproductid);
				clone.doAction("update");
				logger.error("error in {} of {}", "copyDripCostings",
						e.getMessage());
			}
		}
		row.close();
		return (clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	public String copyCostings(String fromproductid, String toproductid,
			String userid) {

		Set<String> existingCosts = getExistingCosts(toproductid);

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", fromproductid);
		// row.setParameter("Category",
		// "!Discount;!Rebate;!Inclusion;!Promotion");
		row.setParameter("ProductType", "Package Cost+Package Cost Library");
		row.setParameter("ProductProductsLinkedProduct.ProductType", "Package Cost+Package Cost Library");
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			String rootID = findRootCostings(getConnection(),
					row.getString("LinkedProductID"));
			if (rootID != null && rootID.length() > 0
					&& !existingCosts.contains(rootID)) {
				clone.clear();
				clone.setTableSpec("ProductProducts");
				clone.setConnection(this.getConnection());
				clone.putAllDataAsParameters(row);
				// if it is a Package Cost then clone that and use the clone
				if ("Package Cost".equals(row.getData("ProductType"))) {
					String newproductid = KeyMaker.generate();
					copyplan("Products", "ProductID",
							row.getString("LinkedProductID"), newproductid,
							userid);
					clone.setParameter("LinkedProductID", newproductid);
				}
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				clone.setParameter("ProductID", toproductid);
				clone.createNewID();
				clone.doAction("insert");

				existingCosts.add(row.getString("PackageCostProductID"));
				existingCosts.add(row.getString("LinkedProductID"));
			}
		}
		row.close();
		return "";
	}

	private String findRootCostings(Connection conn, String productID) {
		if (productID == null || productID.length() == 0)
			return null;

		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");

		if (row.getString("PackageCostProductID").length() > 0) {
			String tmpID = findRootCostings(conn,row.getString("PackageCostProductID"));
			if (StringUtils.isNotBlank(tmpID)) {
				return tmpID;
			}
		}

		return row.getData("ProductID");
	}

	private String findRootCostingsLinks(Connection conn, String productID) {
		if (productID == null || productID.length() == 0)
			return null;

		String costingID = costingsLinks.get(productID);
		if (costingID != null && costingID.length() > 0)
			return findRootCostingsLinks(conn, costingID);

		if (costingID != null && costingID.length() == 0)
			return productID;

		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");

		costingsLinks.put(productID, row.getString("PackageCostProductID"));

		if (row.getString("PackageCostProductID").length() > 0) {
			String tmpID = findRootCostingsLinks(conn,row.getString("PackageCostProductID"));
			if (StringUtils.isNotBlank(tmpID)) {
				return tmpID;
			}
		}
		return row.getData("ProductID");
	}

	public String copyDripsFromRootDrip(String fromproductid,
			String toproductid, String fromProductType, String userid) {

		if (StringUtils.isBlank(fromproductid)) {
			logger.error("fromproductid was blank");
			return "";
		}

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", fromproductid);
		row.setParameter("Category", "Discount+Rebate+Inclusion+Promotion");
		row.setParameter("Product Type", "Drip+Drip Library");
		row.setParameter("ProductProductsLinkedProduct.ProductType", "Drip+Drip Library");
		row.doAction(GenerationKeys.SELECTFIRST);

		GenRow check = new GenRow();
		check.setViewSpec("ProductLinkedProductView");

		if (!row.isSuccessful()) {
			logger.error("drip could not be found {}", fromproductid);
			return "";
		}

		try {
			check.setConnection(this.getConnection());
			check.setParameter("ProductID", toproductid);
			check.setParameter("Category",
					"Discount+Rebate+Inclusion+Promotion");
			check.setParameter("ProductProductsLinkedProduct.Category",
					"Discount+Rebate+Inclusion+Promotion");
			check.setParameter("ProductType", "Drip+Drip Library");
			check.setParameter("ProductProductsLinkedProduct.ProductType",
					"Drip+Drip Library");
			check.getResults();

			while (check.getNext()) {
				String foundProductID = findRootCostingsLinks(
						this.getConnection(),
						check.getString("LinkedProductID"));
				if (StringUtils.equals(foundProductID, fromproductid)) {
					logger.debug("skipping copy drip as already on product");
					return "";
				}
			}
		} finally {
			check.close();
		}
		GenRow clone = new GenRow();
		clone.setTableSpec("ProductProducts");
		clone.setConnection(this.getConnection());
		clone.putAllDataAsParameters(row);
		// if it is a Package Cost then clone that and use the clone
		if (row.getData("ProductType").indexOf("Drip") >= 0) {
			String newproductid = KeyMaker.generate();
			copyplan("Products", "ProductID", row.getString("LinkedProductID"),
					newproductid, fromProductType, userid);
			clone.setParameter("LinkedProductID", newproductid);
		}
		clone.setParameter("CreatedDate", "NOW");
		clone.setParameter("ModifiedDate", "NOW");
		clone.setParameter("CreatedBy", userid);
		clone.setParameter("ModifiedBy", userid);
		clone.setParameter("ProductID", toproductid);
		clone.createNewID();
		clone.doAction("insert");
		String costingID = findRootCostingsLinks(getConnection(),
				row.getString("LinkedProductID"));
		if (costingID != null && costingID.length() > 0) {
			copyDripRules(costingID, clone.getString("LinkedProductID"));
			copyDripContent(costingID, clone.getString("LinkedProductID"));
		}
		return "";
	}

	public String copyLinkedProducts(String fromproductid, String toproductid,
			String productType, String userid) {

		if (fromproductid == null || fromproductid.length() == 0)
			return null;

		logger.debug("copyLinkedProducts from {} to {} of {}", new String[] {
				fromproductid, toproductid, productType });

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", fromproductid);
		row.setParameter("ProductType", productType);
		row.setParameter("ProductProductsLinkedProduct.ProductType", productType);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow check = new GenRow();
		check.setViewSpec("ProductLinkedProductView");

		Set<String> rootCostingIDs = Collections.emptySet();
		try {
			check.setConnection(this.getConnection());
			check.setParameter("ProductID", toproductid);
			check.setParameter("ProductType", productType);
			check.setParameter("ProductProductsLinkedProduct.ProductType", productType);
			check.getResults(true);

			rootCostingIDs = new HashSet<String>(check.getSize());
			while (check.getNext()) {
				String foundProductID = findRootCostingsLinks(this.getConnection(),	check.getString("LinkedProductID"));
				rootCostingIDs.add(foundProductID);
			}
		} finally {
			check.close();
		}

		GenRow clone = new GenRow();
		while (row.getNext()) {
			// boolean checked = false;
			String testProductID = findRootCostingsLinks(this.getConnection(), row.getString("LinkedProductID"));
			// if we don't have a root productID then skip it
			if (StringUtils.isBlank(testProductID))	continue;
			
			boolean checked = rootCostingIDs.contains(testProductID);
			// make sure it is not already there
			if (!checked) {
				clone.clear();
				clone.setTableSpec("ProductProducts");
				clone.setConnection(this.getConnection());
				clone.putAllDataAsParameters(row);
				// if it is a Package Cost then clone that and use the clone
				String newproductid = KeyMaker.generate();
				copyplan("Products", "ProductID", row.getString("LinkedProductID"), newproductid, null, userid);
				clone.setParameter("LinkedProductID", newproductid);
				clone.setParameter("ProductType", productType);
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				clone.setParameter("ProductID", toproductid);
				clone.createNewID();
				clone.doAction("insert");
			}
		}
		row.close();
		return "";
	}

	public String copyLinksOnly(String fromproductid, String toproductid,
			String productType, String userid) {

		if (fromproductid == null || fromproductid.length() == 0)
			return null;

		logger.debug("copyLinksOnly from {} to {} of {}", new String[] {
				fromproductid, toproductid, productType });

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", fromproductid);
		row.setParameter("ProductType", productType);
		row.setParameter("ProductProductsLinkedProduct.ProductType", productType);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow check = new GenRow();
		check.setViewSpec("ProductLinkedProductView");

		Set<String> rootCostingIDs = Collections.emptySet();
		try {
			check.setConnection(this.getConnection());
			check.setParameter("ProductID", toproductid);
			check.setParameter("ProductType", productType);
			check.setParameter("ProductProductsLinkedProduct.ProductType", productType);
			check.getResults(true);

			rootCostingIDs = new HashSet<String>(check.getSize());
			while (check.getNext()) {
				String foundProductID = findRootCostingsLinks(this.getConnection(),	check.getString("LinkedProductID"));
				rootCostingIDs.add(foundProductID);
			}
		} finally {
			check.close();
		}

		GenRow clone = new GenRow();
		while (row.getNext()) {
			// boolean checked = false;
			String testProductID = findRootCostingsLinks(this.getConnection(), row.getString("LinkedProductID"));
			// if we don't have a root productID then skip it
			if (StringUtils.isBlank(testProductID))	continue;
			
			boolean checked = rootCostingIDs.contains(testProductID);
			// make sure it is not already there
			if (!checked) {
				clone.clear();
				clone.setTableSpec("ProductProducts");
				clone.setConnection(this.getConnection());
				clone.putAllDataAsParameters(row);
				clone.setParameter("ProductType", productType);
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				clone.setParameter("ProductID", toproductid);
				clone.createNewID();
				clone.doAction("insert");
			}
		}
		row.close();
		return "";
	}

	public String copyDrips(String fromproductid, String toproductid,
			String fromProductType, String userid) {

		if (fromproductid == null || fromproductid.length() == 0)
			return null;

		logger.debug("CopyDrips from {} to {} of {}", new String[] {
				fromproductid, toproductid, fromProductType });

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", fromproductid);
		row.setParameter("Category", "Discount+Rebate+Inclusion+Promotion");
		row.setParameter("ProductProductsLinkedProduct.Category",
				"Discount+Rebate+Inclusion+Promotion");
		row.setParameter("ProductType", "Drip+Drip Library");
		row.setParameter("ProductProductsLinkedProduct.ProductType", "Drip+Drip Library");
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow check = new GenRow();
		check.setViewSpec("ProductLinkedProductView");

		Set<String> rootCostingIDs = Collections.emptySet();
		try {
			check.setConnection(this.getConnection());
			check.setParameter("ProductID", toproductid);
			check.setParameter("Category",
					"Discount+Rebate+Inclusion+Promotion");
			check.setParameter("ProductProductsLinkedProduct.Category",
					"Discount+Rebate+Inclusion+Promotion");
			check.setParameter("ProductType", "Drip+Drip Library");
			check.setParameter("ProductProductsLinkedProduct.ProductType",
					"Drip+Drip Library");
			check.getResults(true);

			rootCostingIDs = new HashSet<String>(check.getSize());
			while (check.getNext()) {
				String foundProductID = findRootCostingsLinks(
						this.getConnection(),
						check.getString("LinkedProductID"));
				rootCostingIDs.add(foundProductID);
			}
		} finally {
			check.close();
		}

		GenRow clone = new GenRow();
		while (row.getNext()) {
			// boolean checked = false;
			String testProductID = findRootCostingsLinks(this.getConnection(),
					row.getString("LinkedProductID"));
			// if we don't have a root productID then skip it
			if (StringUtils.isBlank(testProductID))
				continue;
			boolean checked = rootCostingIDs.contains(testProductID);
			/*
			 * check.clear(); check.setConnection(this.getConnection());
			 * check.setParameter("ProductID", toproductid);
			 * check.setParameter("Category",
			 * "Discount+Rebate+Inclusion+Promotion");
			 * check.setParameter("Product Type", "%Drip%");
			 * check.setParameter("ProductProductsLinkedProduct.ProductType",
			 * "%Drip%"); check.doAction(ActionBean.SEARCH); check.getResults();
			 * 
			 * while (check.getNext()) { String foundProductID =
			 * findRootCostings(this.getConnection(),
			 * check.getString("PackageCostProductID")); if
			 * (testProductID.equals(foundProductID)) { checked = true; break; }
			 * } check.close();
			 */
			// make sure it is not already there
			if (!checked) {
				clone.clear();
				clone.setTableSpec("ProductProducts");
				clone.setConnection(this.getConnection());
				clone.putAllDataAsParameters(row);
				// if it is a Package Cost then clone that and use the clone
				if (row.getData("ProductType").indexOf("Drip") >= 0) {
					String newproductid = KeyMaker.generate();
					copyplan("Products", "ProductID",
							row.getString("LinkedProductID"), newproductid,
							fromProductType, userid);
					clone.setParameter("LinkedProductID", newproductid);
					clone.setParameter("ProductType", "Drip");
				}
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				clone.setParameter("ProductID", toproductid);
				clone.createNewID();
				clone.doAction("insert");

				String costingID = findRootCostingsLinks(getConnection(),
						row.getString("LinkedProductID"));

				if (costingID != null && costingID.length() > 0) {
					try {
						copyDripRules(costingID,
								clone.getString("LinkedProductID"));
					} catch (Exception e) {
						logger.error(
								"CopyDrips Rule error for {} to {} error {}",
								new String[] { fromproductid, toproductid,
										e.getMessage() });
					}
					try {
						copyDripContent(costingID,
								clone.getString("LinkedProductID"));
					} catch (Exception e) {
						logger.error(
								"CopyDrips Content error for {} to {} error {}",
								new String[] { fromproductid, toproductid,
										e.getMessage() });
					}
				}
			}
		}
		row.close();
		return "";
	}

	public String copyGlobalDrips(String toproductid, String fromProductType,
			String userid) {

		if (toproductid == null || toproductid.length() == 0)
			return null;

		logger.debug("CopyGlobalDrips to {} of {}", new String[] { toproductid,
				fromProductType });

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(this.getConnection());
		row.setParameter("Category", "Global");
		row.setParameter("ProductProductsLinkedProduct.Category", "Global");
		row.setParameter("Active", "Y");
		row.setParameter("ProductProductsLinkedProduct.Active", "Y");
		row.setParameter("ProductType", "Drip Library");
		row.setParameter("ProductProductsLinkedProduct.ProductType",
				"Drip Library");
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow check = new GenRow();
		check.setViewSpec("ProductLinkedProductView");

		Set<String> rootCostingIDs = Collections.emptySet();
		try {
			check.setConnection(this.getConnection());
			check.setParameter("ProductID", toproductid);
			check.setParameter("Category", "Global");
			check.setParameter("ProductProductsLinkedProduct.Category",
					"Global");
			check.setParameter("ProductType", "Drip+Drip Library");
			check.setParameter("ProductProductsLinkedProduct.ProductType",
					"Drip+Drip Library");
			check.getResults(true);

			rootCostingIDs = new HashSet<String>(check.getSize());
			while (check.getNext()) {
				String foundProductID = findRootCostingsLinks(
						this.getConnection(),
						check.getString("LinkedProductID"));
				rootCostingIDs.add(foundProductID);
			}
		} finally {
			check.close();
		}

		GenRow clone = new GenRow();
		while (row.getNext()) {
			// boolean checked = false;
			String testProductID = findRootCostingsLinks(this.getConnection(),
					row.getString("LinkedProductID"));
			// if we don't have a root productID then skip it
			if (StringUtils.isBlank(testProductID))
				continue;
			boolean checked = rootCostingIDs.contains(testProductID);
			/*
			 * check.clear(); check.setConnection(this.getConnection());
			 * check.setParameter("ProductID", toproductid);
			 * check.setParameter("Category",
			 * "Discount+Rebate+Inclusion+Promotion");
			 * check.setParameter("Product Type", "%Drip%");
			 * check.setParameter("ProductProductsLinkedProduct.ProductType",
			 * "%Drip%"); check.doAction(ActionBean.SEARCH); check.getResults();
			 * 
			 * while (check.getNext()) { String foundProductID =
			 * findRootCostings(this.getConnection(),
			 * check.getString("PackageCostProductID")); if
			 * (testProductID.equals(foundProductID)) { checked = true; break; }
			 * } check.close();
			 */
			// make sure it is not already there
			if (!checked) {
				clone.clear();
				clone.setTableSpec("ProductProducts");
				clone.setConnection(this.getConnection());
				clone.putAllDataAsParameters(row);
				// if it is a Package Cost then clone that and use the clone
				if (row.getData("ProductType").indexOf("Drip") >= 0) {
					String newproductid = KeyMaker.generate();
					copyplan("Products", "ProductID",
							row.getString("LinkedProductID"), newproductid,
							fromProductType, userid);
					clone.setParameter("LinkedProductID", newproductid);
					clone.setParameter("ProductType", "Drip");
				}
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				clone.setParameter("ProductID", toproductid);
				clone.createNewID();
				clone.doAction("insert");

				String costingID = findRootCostingsLinks(getConnection(),
						row.getString("LinkedProductID"));

				if (costingID != null && costingID.length() > 0) {
					try {
						copyDripRules(costingID,
								clone.getString("LinkedProductID"));
					} catch (Exception e) {
						logger.error(
								"CopyGlobalDrips Rule error to {} error {}",
								new String[] { toproductid, e.getMessage() });
					}
					try {
						copyDripContent(costingID,
								clone.getString("LinkedProductID"));
					} catch (Exception e) {
						logger.error(
								"CopyGlobalDrips Content error to {} error {}",
								new String[] { toproductid, e.getMessage() });
					}
				}
			}
		}
		row.close();
		return "";
	}

	public String copyRawDrips(String dripproductid, String toproductid,
			String userid) {

		if (dripproductid == null || dripproductid.length() == 0)
			return null;

		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", dripproductid);
		row.setParameter("Category", "Discount+Rebate+Inclusion+Promotion");
		row.setParameter("ProductType", "Drip+Drip Library");
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow check = new GenRow();
		check.setViewSpec("ProductLinkedProductView");

		Set<String> rootCostingIDs = Collections.emptySet();
		try {
			check.setConnection(this.getConnection());
			check.setParameter("ProductID", toproductid);
			check.setParameter("Category",
					"Discount+Rebate+Inclusion+Promotion");
			check.setParameter("ProductProductsLinkedProduct.Category",
					"Discount+Rebate+Inclusion+Promotion");
			check.setParameter("ProductType", "Drip+Drip Library");
			check.setParameter("ProductProductsLinkedProduct.ProductType",
					"Drip+Drip Library");
			check.getResults(true);

			rootCostingIDs = new HashSet<String>(check.getSize());
			while (check.getNext()) {
				String foundProductID = findRootCostingsLinks(
						this.getConnection(),
						check.getString("LinkedProductID"));
				rootCostingIDs.add(foundProductID);
			}
		} finally {
			check.close();
		}

		GenRow clone = new GenRow();
		while (row.getNext()) {
			// boolean checked = false;
			String testProductID = findRootCostingsLinks(this.getConnection(),
					row.getString("ProductID"));
			// if we don't have a root productID then skip it
			if (StringUtils.isBlank(testProductID))
				continue;
			boolean checked = rootCostingIDs.contains(testProductID);
			/*
			 * check.clear(); check.setConnection(this.getConnection());
			 * check.setParameter("ProductID", toproductid);
			 * check.setParameter("Category",
			 * "Discount+Rebate+Inclusion+Promotion");
			 * check.setParameter("Product Type", "%Drip%");
			 * check.setParameter("ProductProductsLinkedProduct.ProductType",
			 * "%Drip%"); check.doAction(ActionBean.SEARCH); check.getResults();
			 * 
			 * while (check.getNext()) { String foundProductID =
			 * findRootCostings(this.getConnection(),
			 * check.getString("PackageCostProductID")); if
			 * (testProductID.equals(foundProductID)) { checked = true; break; }
			 * } check.close();
			 */
			// make sure it is not already there
			if (!checked) {
				clone.clear();
				clone.setTableSpec("ProductProducts");
				clone.setConnection(this.getConnection());
				// clone.putAllDataAsParameters(row);
				// if it is a Package Cost then clone that and use the clone
				if (row.getData("ProductType").indexOf("Drip") >= 0) {
					String newproductid = KeyMaker.generate();
					copyplan("Products", "ProductID",
							row.getString("ProductID"), newproductid,
							"Campaign", userid);
					clone.setParameter("LinkedProductID", newproductid);
				}
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				clone.setParameter("ProductID", toproductid);
				clone.createNewID();
				clone.doAction("insert");

				String costingID = findRootCostingsLinks(getConnection(),
						row.getString("ProductID"));

				if (costingID != null && costingID.length() > 0) {
					copyDripRules(costingID, clone.getString("LinkedProductID"));
					copyDripContent(costingID,
							clone.getString("LinkedProductID"));
				}
			}
		}
		row.close();
		return "";
	}

	public void copyDripRules(String fromProductID, String toProductID) {

		if (fromProductID == null || fromProductID.length() == 0)
			return;
		if (toProductID == null || toProductID.length() == 0)
			return;

		GenRow pcc = new GenRow();
		pcc.setTableSpec("PackageCostings");
		pcc.setConnection(getConnection());
		pcc.setParameter("ON-ParentProductID", toProductID);
		pcc.doAction("deleteall");

		copyDripCostingsCached(fromProductID, toProductID, userid);

	}

	public void copyDripContent(String fromProductID, String toProductID) {

		if (fromProductID == null || fromProductID.length() == 0)
			return;
		if (toProductID == null || toProductID.length() == 0)
			return;

		GenRow pcc = new GenRow();
		pcc.setTableSpec("LinkedDocuments");
		pcc.setConnection(getConnection());
		pcc.setParameter("ON-ProductID", toProductID);
		pcc.doAction("deleteall");

		copyDripContentCached(fromProductID, toProductID);

	}

	public void copyDripContentCached(String fromProductID, String toProductID) {
		ArrayList<Map<String, String>> links = linkMap.get(fromProductID);
		if (links != null) {
			GenRow clone = new GenRow();
			clone.setTableSpec("LinkedDocuments");
			clone.setConnection(this.getConnection());

			GenRow bean = new GenRow();
			bean.setTableSpec("Documents");
			bean.setConnection(this.getConnection());

			for (Map<String, String> keyValue : links) {
				if (keyValue != null && keyValue.size() > 0) {
					Map<String, String> docs = documentMap.get(keyValue
							.get("DocumentID"));

					if (docs == null || docs.size() == 0) {
						bean.clear();
						bean.setTableSpec("Documents");
						bean.setConnection(this.getConnection());
						bean.setParameter("DocumentID",
								keyValue.get("DocumentID"));
						bean.setParameter("-select0", "*");
						bean.setParameter("-top", "1");
						bean.doAction("search");

						bean.getResults();

						bean.getNext();

						docs = bean.getDataMap();

						documentMap.put(keyValue.get("DocumentID"), docs);

					}
					if (docs != null && docs.size() > 0) {
						String newDocumentID = KeyMaker.generate();

						bean.clear();
						bean.setConnection(getConnection());
						bean.putAll(docs);
						bean.setColumn("DocumentID", newDocumentID);
						// for some stupid reason we are getting , in intagers
						bean.setParameter("FileSize", bean
								.getString("FileSize").replaceAll("[^0-9]", ""));
						bean.setAction("insert");
						bean.doAction();

						clone.putAll(keyValue);
						clone.setParameter("CreatedDate", "NOW");
						clone.setParameter("ModifiedDate", "NOW");
						clone.setParameter("CreatedBy", userid);
						clone.setParameter("ModifiedBy", userid);
						clone.setColumn("LinkedDocumentID", KeyMaker.generate());
						clone.setColumn("CreatedDate", "NOW");
						clone.setColumn("CreatedBy", userid);
						clone.setColumn("DocumentID", newDocumentID);
						clone.setColumn("ContentID", KeyMaker.generate());
						clone.setColumn("ProductID", toProductID);
						try {
							clone.doAction("insert");
						} catch (Exception e) {
							clone.clearStatement();
							clone.setParameter("ParentProductID", toProductID);
							clone.doAction("update");
							logger.error("error in {} of {}",
									"copyDripCostings", e.getMessage());
						}
					}
				}
			}
		} else {
			links = new ArrayList<Map<String, String>>();
			GenRow row = new GenRow();
			row.setTableSpec("LinkedDocuments");

			row.setConnection(this.getConnection());
			row.setParameter("-select1", "*");
			row.setParameter("ProductID", fromProductID);
			row.doAction(ActionBean.SEARCH);
			row.getResults();

			while (row.getNext()) {
				links.add(row.getDataMap());
			}
			linkMap.put(fromProductID, links);

			row.close();

			GenRow clone = new GenRow();
			clone.setTableSpec("LinkedDocuments");
			clone.setConnection(this.getConnection());

			GenRow bean = new GenRow();
			bean.setTableSpec("Documents");
			bean.setConnection(this.getConnection());

			for (Map<String, String> keyValue : links) {
				if (keyValue != null && keyValue.size() > 0) {
					Map<String, String> docs = documentMap.get(keyValue
							.get("DocumentID"));
					if (docs == null || docs.size() == 0) {
						bean.clear();
						bean.setTableSpec("Documents");
						bean.setConnection(this.getConnection());
						bean.setParameter("DocumentID",
								keyValue.get("DocumentID"));
						bean.setParameter("-select0", "*");
						bean.setParameter("-top", "1");
						bean.doAction("search");

						bean.getResults();

						bean.getNext();

						docs = bean.getDataMap();

						documentMap.put(keyValue.get("DocumentID"), docs);

					}
					if (docs != null && docs.size() > 0) {
						String newDocumentID = KeyMaker.generate();

						bean.clear();
						bean.setConnection(getConnection());
						bean.putAll(docs);
						bean.setColumn("DocumentID", newDocumentID);
						// for some stupid reason we are getting , in intagers
						bean.setParameter("FileSize", bean
								.getString("FileSize").replaceAll("[^0-9]", ""));
						bean.setAction("insert");
						bean.doAction();

						clone.putAll(keyValue);
						clone.setParameter("CreatedDate", "NOW");
						clone.setParameter("ModifiedDate", "NOW");
						clone.setParameter("CreatedBy", userid);
						clone.setParameter("ModifiedBy", userid);
						clone.setColumn("LinkedDocumentID", KeyMaker.generate());
						clone.setColumn("CreatedDate", "NOW");
						clone.setColumn("CreatedBy", userid);
						clone.setColumn("DocumentID", newDocumentID);
						clone.setColumn("ContentID", KeyMaker.generate());
						clone.setColumn("ProductID", toProductID);
						try {
							clone.doAction("insert");
						} catch (Exception e) {
							clone.clearStatement();
							clone.setParameter("ParentProductID", toProductID);
							clone.doAction("update");
							logger.error("error in {} of {}",
									"copyDripCostings", e.getMessage());
						}
					}
				}
			}
		}
	}

	/*
	 * public void copyDripContent(String fromProductID, String toProductID) {
	 * 
	 * if (fromProductID == null || fromProductID.length() == 0) return;
	 * 
	 * GenRow link = new GenRow(); link.setTableSpec("LinkedDocuments");
	 * link.setConnection(getConnection()); link.setParameter("-select0", "*");
	 * link.setParameter("ProductID", fromProductID); link.doAction("search");
	 * 
	 * link.getResults();
	 * 
	 * GenRow row = new GenRow(); GenRow bean = new GenRow();
	 * 
	 * while (link.getNext()) { row.clear(); row.setViewSpec("DocumentView");
	 * row.setConnection(getConnection());
	 * row.setParameter("DocumentID",link.getString("DocumentID"));
	 * row.doAction("selectfirst");
	 * 
	 * if (row.isSuccessful()) { String newDocumentID = KeyMaker.generate();
	 * 
	 * bean.clear(); bean.setViewSpec("DocumentView");
	 * bean.setConnection(getConnection()); bean.putAllDataAsParameters(row);
	 * bean.setColumn("DocumentID",newDocumentID); bean.setAction("insert");
	 * bean.doAction();
	 * 
	 * bean.clear(); bean.setTableSpec("LinkedDocuments"); String
	 * linkedDocumentID = KeyMaker.generate();
	 * bean.setColumn("LinkedDocumentID",linkedDocumentID);
	 * bean.setColumn("CreatedDate","NOW"); bean.setColumn("CreatedBy",userid);
	 * bean.setColumn("DocumentID",newDocumentID);
	 * bean.setColumn("ContentID",KeyMaker.generate());
	 * bean.setColumn("ProductID",toProductID); bean.setAction("insert");
	 * bean.doAction();
	 * 
	 * //row.clear(); //row.setViewSpec("LinkedDocumentView");
	 * //row.setColumn("LinkedDocumentID",linkedDocumentID);
	 * //row.doAction("selectfirst"); } }
	 * 
	 * link.close(); }
	 */

	public String update(String tablespecname, String productidfieldname,
			String fromproductid, String toproductid, String userid) {
		return update(tablespecname, productidfieldname, fromproductid,
				toproductid, userid, false, true);
	}

	public String update(String tablespecname, String productidfieldname,
			String fromproductid, String toproductid, String userid,
			boolean updateTimeStamp) {
		return update(tablespecname, productidfieldname, fromproductid,
				toproductid, userid, false, updateTimeStamp);
	}

	public String update(String tablespecname, String productidfieldname,
			String fromproductid, String toproductid, String userid,
			boolean overwrite, boolean updateTimeStamp) {

		if (fromproductid == null || fromproductid.length() == 0)
			return "";

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);

		TableSpec ts = row.getTableSpec();

		row.setConnection(this.getConnection());
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow check = new GenRow();
		check.setTableSpec(tablespecname);

		GenRow clone = new GenRow();
		while (row.getNext()) {
			check.clear();
			check.setConnection(this.getConnection());
			check.setParameter("-select1", "*");
			check.setParameter(productidfieldname, toproductid);
			if ("ProductDetails".equals(row.getTableSpec().getTableName())) {
				check.setParameter("ProductType", row.getString("ProductType"));
				check.setParameter("DetailsType", row.getString("DetailsType"));
				if (check.getString("ProductType").startsWith("Home"))
					check.setParameter("ProductType", "HomePlanDetails");
				if (check.getString("ProductType").startsWith("Land"))
					check.setParameter("ProductType", "LandDetails");
			}
			check.doAction("selectfirst");

			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.setConnection(this.getConnection());
			clone.putAllDataAsParameters(row, overwrite);
			if ("Products".equals(tablespecname)
					&& "Home Plan".equals(row.getString("ProductType"))) {
				if("true".equals(InitServlet.getSystemParam("RPM-RetainPackageDescription"))) {
					clone.remove("Description");
				}
				if (!updatePlan) {
					clone.remove("TotalCost");
					clone.remove("Cost");
					clone.remove("GST");
				}
				if (!updatePrice) {
					clone.remove("DripCost");
					clone.remove("DripResult");
				} else {
					clone.setParameter("DripResult", "0");
					clone.remove("DripCost");
				}
				clone.remove("Active");
				clone.remove("AvailableDate");
				clone.remove("ProductStatusID");
				clone.remove("MarketingStatus");
				clone.remove("ProductSubType");
				clone.remove("DisplayPriceType");
				clone.remove("ExpiryDate");
				clone.remove("SellingAgent");
				clone.remove("Vendor");
				clone.remove("ReleaseStatus");
				clone.remove("PreAvailableDate");
				clone.remove("PublishingStatus");
				clone.remove("PackageStatus");
				clone.remove("QuoteViewID");
				clone.remove("EstSettlementDate");
				// these should be in the package, do not remove them.
				clone.remove("PublishDescription");
				clone.remove("Summary");
				clone.remove("CampaignID");
				clone.remove("GroupID");

				// Donot update the publish fields if the product has been
				// claimed by any rep.
				/*
				 * if (isRecreateTask() && isPackageClaimed(toproductid)) {
				 * logger.debug("Not updating publish fields"); }
				 */
			}

			if ("ProductDetails".equals(row.getTableSpec().getTableName())) {
				if (row.getString("ProductType").startsWith("Home"))
					clone.setParameter("ProductType", "HomePlanDetails");
				if (row.getString("ProductType").startsWith("Land"))
					clone.setParameter("ProductType", "LandDetails");
				clone.setParameter("CopiedFromProductID", fromproductid);
			}
			// make sure it is not already there
			if (!check.isSuccessful()
					|| check.getString(row.getTableSpec().getPrimaryKeyName())
							.length() == 0) {
				// if it is a Package Cost then clone that and use the clone
				clone.setParameter(productidfieldname, toproductid);
				if (!row.getTableSpec().getPrimaryKeyName()
						.equals(productidfieldname))
					clone.createNewID();
				if (updateTimeStamp) {
					clone.setParameter("ModifiedDate", "NOW");
					clone.setParameter("ModifiedBy", userid);
				} else {
					clone.remove("ModifiedDate");
					clone.remove("ModifiedBy");
				}
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("CreatedBy", userid);

				try {
					clone.doAction("insert");
				} catch (Exception e) {
					clone.doAction("update");
					logger.trace("error in {} of {}", "update-insert",
							e.getMessage());
				}
			} else {
				clone.setParameter(productidfieldname, toproductid);
				if (!row.getTableSpec().getPrimaryKeyName()
						.equals(productidfieldname))
					clone.setParameter(row.getTableSpec().getPrimaryKeyName(),
							check.getString(row.getTableSpec()
									.getPrimaryKeyName()));
				// In case of product table copy we dont want the timestamp to
				// be updated until the end of the process
				if (updateTimeStamp) {
					clone.setParameter("ModifiedDate", "NOW");
					clone.setParameter("ModifiedBy", userid);
				} else {
					clone.remove("ModifiedDate");
					clone.remove("ModifiedBy");
				}
				try {
					clone.doAction("update");
				} catch (Exception e) {
					logger.trace("error in {} of {}", "update-update",
							e.getMessage());
				}
			}
		}
		row.close();
		return "";
	}

	public String copyCostingsLibrary(String fromproductid, String toproductid,
			String userid) {

		GenRow row = new GenRow();
		row.setViewSpec("ProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", fromproductid);
		row.setParameter("ProductType", "Package Cost+Package Cost Library");
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec("ProductProducts");
			clone.setConnection(this.getConnection());
			clone.putAllDataAsParameters(row);
			// if it is a Package Cost then clone that and use the clone
			if ("Package Cost Library".equals(row.getData("ProductType"))) {				
				String newproductid = KeyMaker.generate();
				copyplan("Products", "ProductID", row.getData("ProductID"),
						newproductid, userid);
				
				GenRow pc = new GenRow();
				pc.setTableSpec("Products");
				pc.setParameter("ProductID", newproductid);
				pc.setParameter("ProductType", "Package Cost");
				pc.doAction("update");
				
				clone.setParameter("LinkedProductID", newproductid);
			}
			clone.setParameter("CreatedDate", "NOW");
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("CreatedBy", userid);
			clone.setParameter("ModifiedBy", userid);
			clone.setParameter("ProductID", toproductid);
			clone.createNewID();
			clone.doAction("insert");
		}
		row.close();
		return "";
	}

	public void remove(String tablespecname, String productidfieldname,
			String fromproductid, String toproductid, String userid) {
		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);
		row.setConnection(getConnection());
		row.setParameter("ON-" + productidfieldname, fromproductid);
		row.doAction("deleteall");
	}

	public String copyplan(String tablespecname, String productidfieldname,
			String fromproductid, String toproductid, String userid) {
		return copyplan(tablespecname, productidfieldname, fromproductid,
				toproductid, null, userid);
	}

	public String copyplan(String tablespecname, String productidfieldname,
			String fromproductid, String toproductid, String fromProductType,
			String userid) {

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);

		TableSpec ts = row.getTableSpec();

		row.setConnection(this.getConnection());
		row.setParameter("-select1", "*");
		if ("LinkedDocuments".equals(tablespecname)) row.setParameter("OrderID", "NULL+EMPTY");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();
		// System.out.println("row " + row.getStatement());

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.setConnection(this.getConnection());
			clone.putAllDataAsParameters(row);

			if ("Products".endsWith(tablespecname)
					&& "Home Plan".equals(row.getString("ProductType"))) {
				if (!updatePlan) {
					clone.remove("TotalCost");
					clone.remove("Cost");
					clone.remove("GST");
				}
				clone.setParameter("CopiedFromProductID", fromproductid);
				clone.setParameter("Category", "PlanSale");
				clone.setParameter("UpdateByProcess", "Y");
			}

			if (fromProductType != null) {
				clone.setParameter("Summary", fromProductType);
				clone.setParameter("ProductType", "Drip");
			}

			clone.setParameter("PackageCostProductID", fromproductid);
			clone.setParameter("CopiedFromProductID", fromproductid);
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("ModifiedBy", userid);
			clone.setParameter(productidfieldname, toproductid);
			clone.setAction("update");
			if (!productidfieldname.equals(row.getTableSpec()
					.getPrimaryKeyName())) {
				clone.remove(row.getTableSpec().getPrimaryKeyName());
				clone.createNewID();
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("CreatedDate", "NOW");
				clone.setAction("insert");
			} else if (clone.getString(row.getTableSpec().getPrimaryKeyName())
					.equals(toproductid)) {
				clone.setAction("insert");
			}
			try {
				clone.doAction();
			} catch (Exception e) {
				// clone.doAction("insert");
				logger.error("error in {} of {}", "copyplan", e.getMessage());
			}
			// System.out.println("clone " + clone.getStatement());
		}
		row.close();
		return (clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	public String updateplan(String tablespecname, String productidfieldname,
			String fromproductid, String toproductid, String fromProductType,
			String userid) {

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);

		// TableSpec ts = row.getTableSpec();

		row.setConnection(this.getConnection());
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();
		// System.out.println("row " + row.getStatement());

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.setConnection(this.getConnection());
			clone.putAllDataAsParameters(row);

			if ("Products".endsWith(tablespecname)
					&& "Home Plan".equals(row.getString("ProductType"))) {
				if (!updatePlan) {
					clone.remove("TotalCost");
					clone.remove("Cost");
					clone.remove("GST");
				}
				clone.setParameter("CopiedFromProductID", fromproductid);
				clone.setParameter("Category", "PlanSale");
				clone.setParameter("UpdateByProcess", "Y");
			}

			if (fromProductType != null) {
				clone.setParameter("Summary", fromProductType);
			}

			// these need to be removed from the update
			clone.remove("Active");
			clone.remove("AvailableDate");
			clone.remove("ProductStatusID");
			clone.remove("MarketingStatus");
			clone.remove("ProductSubType");
			clone.remove("DisplayPriceType");
			clone.remove("ExpiryDate");
			clone.remove("SellingAgent");
			clone.remove("Vendor");
			clone.remove("ReleaseStatus");
			clone.remove("PreAvailableDate");
			clone.remove("PublishingStatus");
			clone.remove("PackageStatus");
			clone.remove("QuoteViewID");
			clone.remove("EstSettlementDate");
			clone.remove("CreatedDate");
			clone.remove("CreatedBy");
			clone.remove("ModifiedDate");
			clone.remove("ModifiedBy");
			clone.remove("GroupID");

			clone.setParameter("PackageCostProductID", fromproductid);
			clone.setParameter("CopiedFromProductID", fromproductid);
			// clone.setParameter("ModifiedDate", "NOW");
			// clone.setParameter("ModifiedBy", userid);
			clone.setParameter(productidfieldname, toproductid);
			clone.setAction("update");
			if (!productidfieldname.equals(row.getTableSpec()
					.getPrimaryKeyName())) {
				clone.remove(row.getTableSpec().getPrimaryKeyName());
				clone.createNewID();
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("CreatedDate", "NOW");
				clone.setAction("insert");
			} else if (clone.getString(row.getTableSpec().getPrimaryKeyName())
					.equals(toproductid)) {
				clone.setAction("insert");
			}
			try {
				clone.doAction();
			} catch (Exception e) {
				try {
					clone.doAction("update");
				} catch (Exception e2) {
					logger.error("error in {} of {}", "updateplan1",
							e.getMessage());
					logger.error("error in {} of {}", "updateplan2",
							e2.getMessage());
				}
			}
			// System.out.println("clone " + clone.getStatement());
		}
		row.close();
		return (clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	public String setProductImageAndName(String productid, String planproductid,
			String facadeproductid, String landproductid, String image) {
		GenRow house = getCachedViewedProduct("properties/ProductPlanView",
				"ProductID", planproductid);

		GenRow facade = null;
		if (facadeproductid != null && facadeproductid.length() != 0) {
			facade = getCachedViewedProduct("ProductListView", "ProductID",
					facadeproductid);
		} else {
			facade = new GenRow();
		}

		String housandlandimage = house.getData("HomeImage");
		String housandlandthumbnailimage = house.getData("HomeThumbnailImage");

		if (image != null && image.length() > 0) {
			housandlandthumbnailimage = housandlandimage;
			housandlandimage = image;
		}

		if (facade != null && facade.getString("Image") != null
				&& !"".equals(facade.getString("Image"))) {
			housandlandimage = facade.getData("Image");
		}

		GenRow land = getCachedViewedProduct("properties/ProductLotView",
				"ProductID", landproductid);

		StringBuffer hlname = new StringBuffer();
		if(house.getString("Name").startsWith(house.getString("HomeName"))) {
			hlname.append(house.getString("Name"));
		} else {			
			hlname.append(house.getString("HomeName"));
			hlname.append(" ");
			hlname.append(house.getString("Name"));
		}
		hlname.append(" - Lot ");
		hlname.append(land.getString("Name"));
		hlname.append(" ");
		hlname.append(land.getString("Street"));

		GenRow houseandland = new GenRow();
		houseandland.setConnection(this.getConnection());
		houseandland.setTableSpec("Products");
		houseandland.setParameter("ProductID", productid);
		houseandland.setParameter("Name", hlname.toString());
		houseandland.setParameter("Image", housandlandimage);
		houseandland.setParameter("ProductType", "House and Land");
		houseandland.setParameter("Category", "NULL");
		houseandland.setParameter("ThumbnailImage", housandlandthumbnailimage);
		houseandland.setParameter("CopiedFromProductID", "NULL");
		
		String dPriceType = "";
		
		if (!isRecreateTask()) {
			// If its recreate skip this
			houseandland.setParameter(
					"MarketingStatus",
					("As Per Lot".equalsIgnoreCase(marketingStatus)) ? land
							.getString("MarketingStatus") : marketingStatus);
			houseandland.setParameter(
					"ProductSubType",
					("As Per Lot".equalsIgnoreCase(productSubType)) ? land
							.getString("ProductSubType") : productSubType);
			houseandland.setParameter(
					"DisplayPriceType",
					("As Per Lot".equalsIgnoreCase(displayPriceType)) ? land
							.getString("DisplayPriceType") : displayPriceType);
			dPriceType = houseandland.getString("DisplayPriceType");
		}
		houseandland.doAction("update");
		
		return dPriceType;

	}

	public void setProductName(String productid, String planproductid,
			String landproductid) {
		GenRow house = getCachedViewedProduct("properties/ProductPlanView",
				"ProductID", planproductid);

		GenRow land = getCachedViewedProduct("properties/ProductLotView",
				"ProductID", landproductid);

		StringBuffer hlname = new StringBuffer();
		if(house.getString("Name").startsWith(house.getString("HomeName"))) {
			hlname.append(house.getString("Name"));
		} else {			
			hlname.append(house.getString("HomeName"));
			hlname.append(" ");
			hlname.append(house.getString("Name"));
		}
		hlname.append(" - Lot ");
		hlname.append(land.getString("Name"));
		hlname.append(" ");
		hlname.append(land.getString("Street"));

		GenRow houseandland = new GenRow();
		houseandland.setConnection(this.getConnection());
		houseandland.setTableSpec("Products");
		houseandland.setParameter("ProductID", productid);
		houseandland.setParameter("ProductType", "House and Land");
		houseandland.setParameter("Category", "NULL");
		houseandland.setParameter("Name", hlname.toString());
		houseandland.setParameter("CopiedFromProductID", "NULL");
		houseandland.doAction("update");

	}

	public void updateProductCost(String productid) {
		updateProductCost(productid, "");
	}
	
	public void updateProductCost(String productid, String displayPriceType) {
		if (productid == null || productid.length() == 0)
			return;

		GenRow sum = new GenRow();
		sum.setConnection(this.getConnection());
		// do not change from a view spec that uses a JOIN as using a table spec
		// is way to slow
		sum.setViewSpec("ProductLinkedProductPriceView");
		// sum.setParameter("-select1", "sum(Cost) as SumCost");
		// sum.setParameter("-select2", "sum(GST) as SumGST");
		sum.setParameter("-select3", "sum(TotalCost) as SumTotalCost");
		sum.setParameter("-select4", "sum(BaseCost) as SumBaseCost");
		sum.setParameter("ProductType", "!Drip;!Drip Library");
		sum.setParameter("ProductProductsLinkedProduct.ProductType", "!Drip;!Drip Library");
		sum.setParameter("ProductID", productid);
		sum.doAction("selectfirst");

		double total = 0, cost = 0, gst = 0;
		try {
			total = Double.parseDouble(sum.getString("SumTotalCost"));
			cost = total / 1.1;
			gst = total - cost;
		} catch (Exception e) {
			logger.trace("error in {} of {}", "updateProductCost",
					e.getMessage());
		}

		GenRow product = new GenRow();
		product.setConnection(this.getConnection());
		product.setTableSpec("Products");
		product.setParameter("ProductID", productid);
		product.setParameter("Cost", "" + cost);
		product.setParameter("GST", "" + gst);
		// Commenting out the totalcost calculation as this was leading to
		// amount becoming 249,999.99 instead of 250,000 (Eg: ID real estate
		// runway)
		// product.setParameter("TotalCost", "" + (Math.floor(total / 1.1) *
		// 1.1));
		product.setParameter("TotalCost", "" + total);
		
		if (StringUtils.isNotBlank(displayPriceType)) product.setParameter("DisplayPrice", RunwayUtil.displayPrice(displayPriceType, total));

		if (!recreateTask) {
			// Do this only for the create package. The nightly run will update
			// later with the actual drip result.
			product.setParameter("DripCost", "0");
			product.setParameter("DripResult", product.getString("TotalCost"));
		}

		product.setParameter("BaseCost", sum.getString("SumBaseCost"));
		product.setParameter("PackagedCost", sum.getString("SumTotalCost"));
		product.setParameter("ModifiedDate", "NOW");
		product.setParameter("ModifiedBy", userid);
		product.doAction("update");
	}

	public void updateProductNumber(String productid) {
		GenRow product = new GenRow();
		product.setConnection(this.getConnection());
		product.setTableSpec("Products");
		product.setParameter("ProductID", productid);
		product.setParameter("ProductNum", KeyMaker.getRandomNumberString(6));
		product.doAction("update");
	}

	public void updateProductStatus(String productid, String productstatusid) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(this.getConnection());
		row.setParameter("ProductStatusID", productstatusid);
		if (packageStatus != null && packageStatus.length() > 0) {
			row.setParameter("PackageStatus", packageStatus);
			row.setParameter("PublishingStatus", "To be published");
		}
		row.setParameter("ProductID", productid);
		row.setParameter("Active",
				(active != null && active.length() > 0) ? active : "N");
		
		row.setParameter("ShowOnPortalFlag",
				(showOnPortal != null && showOnPortal.length() > 0) ? showOnPortal : "N");
		
		if (this.groupID != null && this.groupID.length() > 0)
			row.setParameter("GroupID", this.groupID);
		row.doAction(ActionBean.UPDATE);
	}

	public void updateSecurityGroup(String productID) {
		if (this.groupID == null || this.groupID.length() == 0)
			return;
		GenRow row = new GenRow();
		row.setTableSpec("ProductSecurityGroups");
		row.setParameter("ProductID", productID);
		row.setParameter("RepUserID", this.repUserID);
		row.setParameter("GroupID", this.groupID);
		row.setParameter("CreatedDate", "NOW");
		row.setParameter("CreatedBy", userid);
		row.setToNewID("ProductSecurityGroupID");
		row.doAction(ActionBean.INSERT);
	}

	public void updateAvailableDate(String productid) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(this.getConnection());
		row.setParameter(
				"PreAvailableDate",
				(preAvailableDate != null && preAvailableDate.length() > 0) ? preAvailableDate
						: "NOW");
		row.setParameter(
				"AvailableDate",
				(availableDate != null && availableDate.length() > 0) ? availableDate
						: "NOW");
		row.setParameter("ExpiryDate", (expiryDate != null && expiryDate
				.length() > 0) ? expiryDate : "NULL");
		row.setParameter("ProductID", productid);
		row.doAction(ActionBean.UPDATE);
	}

	public void updateLocation(String productid, String locationid,
			String regionID) {

		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(this.getConnection());
		row.setParameter("LocationID", locationid);
		row.setParameter("RegionID", regionID);
		row.setParameter("ProductID", productid);
		row.doAction(ActionBean.UPDATE);
		row.close();
	}

	public void insertProductProduct(String productid, String linkedproductid,
			String userid) {
		if (linkedproductid != null && linkedproductid.length() != 0) {
			GenRow row = new GenRow();
			row.setConnection(this.getConnection());
			row.setTableSpec("ProductProducts");
			row.setParameter("ProductID", productid);
			row.setParameter("LinkedProductID", linkedproductid);
			row.setParameter("CreatedDate", "NOW");
			row.setParameter("ModifiedDate", "NOW");
			row.setParameter("CreatedBy", userid);
			row.setParameter("ModifiedBy", userid);
			row.createNewID();
			row.doAction("insert");
		}
	}

	public void deleteProductProduct(String productid, String linkedproductid) {
		if (linkedproductid != null && linkedproductid.length() != 0) {
			GenRow row = new GenRow();
			row.setConnection(this.getConnection());
			row.setTableSpec("ProductProducts");
			row.setParameter("ON-ProductID", productid);
			row.setParameter("ON-LinkedProductID", linkedproductid);
			row.doAction("deleteall");
		}
	}

	protected void insertProductVariation(String parentproductid,
			String childproductid, String type) {
		GenRow productvariation = new GenRow();
		productvariation.setConnection(this.getConnection());
		productvariation.setTableSpec("ProductVariations");
		productvariation.setParameter("ParentProductID", parentproductid);
		productvariation.setParameter("ChildProductID", childproductid);
		productvariation.setParameter("Type", type);
		productvariation.createNewID();
		try {
			productvariation.doAction("insert");
		} catch (Exception e) {
			productvariation.doAction("update");
			logger.error("error in {} of {}", "insertProductVariation",
					e.getMessage());
		}
	}

	public String duplicateplan(String oldProductID, String userID) {
		return duplicateplan(oldProductID, KeyMaker.generate(), userID, true);
	}

	public String duplicateplan(String oldProductID, String newproductid,
			String userID, boolean groups) {
		if (oldProductID == null || oldProductID.length() == 0)
			return null;
		if (newproductid == null || newproductid.length() == 0)
			return null;

		GenRow existingProduct = getCachedViewedProduct(
				"properties/ProductPlanView", "ProductID", oldProductID);

		String homeProductID = existingProduct.getString("HomeProductID");

		if (groups) {
			copyplan("Products", "ProductID",
					existingProduct.getString("ProductID"), newproductid,
					userID);
			// copyplan("ProductSecurityGroups", "ProductID",
			// existingProduct.getString("ProductID"), newproductid,
			// userID);
			insertProductVariation(homeProductID, newproductid, "Home");
		} else {
			updateplan("Products", "ProductID",
					existingProduct.getString("ProductID"), newproductid, null,
					userID);
		}

		if (!recreateTask) {
			deleteAllExistingLinkedDocuments(newproductid);
			copyplan("LinkedDocuments", "ProductID",
					existingProduct.getString("ProductID"), newproductid,
					userID);
			copyplan("properties/HomeDetails", "ProductID",
					existingProduct.getString("ProductID"), newproductid,
					userID);
			copyplan("properties/ProductDetails", "ProductID",
					existingProduct.getString("ProductID"), newproductid,
					userID);
			copyCommissions("ProductCommissions", "ProductID",
					existingProduct.getString("ProductID"), newproductid,
					userID);
		} else if (updateDims || updateImages) {
			if (updateDims) {
				GenRow delete = new GenRow();
				delete.setTableSpec("properties/HomeDetails");
				delete.setConnection(getConnection());
				delete.setParameter("ProductID", newproductid);
				delete.doAction("delete");

				copy("properties/HomeDetails", "ProductID",
						existingProduct.getString("ProductID"), newproductid,
						userID);

				delete.clear();
				delete.setTableSpec("properties/ProductDetails");
				delete.setConnection(getConnection());
				delete.setParameter("ON-ProductID", newproductid);
				delete.doAction("deleteall");

				copy("properties/ProductDetails", "ProductID",
						existingProduct.getString("ProductID"), newproductid,
						userID);
				copyCommissions("ProductCommissions", "ProductID",
						existingProduct.getString("ProductID"), newproductid,
						userID);
			}
			if (updateImages) {
				deleteAllExistingLinkedDocuments(newproductid);
				copyplan("LinkedDocuments", "ProductID",
						existingProduct.getString("ProductID"), newproductid,
						userID);
			}
		}
		copyCostings(existingProduct.getString("ProductID"), newproductid,
				userID);

		GenRow duplicateProduct = new GenRow();
		duplicateProduct.setConnection(this.getConnection());
		duplicateProduct.setTableSpec("Products");
		duplicateProduct.setParameter("ProductID", newproductid);
		duplicateProduct.setParameter("CopiedFromProductID",
				existingProduct.getString("ProductID"));
		duplicateProduct.setParameter("Category", "PlanSale");
		duplicateProduct.setParameter("UpdateByProcess", "Y");
		duplicateProduct.doAction("update");

		return newproductid;
	}

	protected boolean checkCondition(GenRow costings, String houselandid) {
		if (costings.getString("IfDimension").length() == 0
				|| costings.getString("IfDimValue").length() == 0
				|| "FreeForm".equals(costings.getString("IfDimension")))
			return true;
		
		System.out.println(costings.toString());

		String ifDim = costings.getString("IfDimension");

		String productType = "";
		if (ifDim.toLowerCase().replaceAll(" ", "").indexOf("plan") >= 0)
			productType = "HomePlan+HomePlanDetails+Home Plan";
		else if (ifDim.toLowerCase().replaceAll(" ", "").indexOf("facade") >= 0)
			productType = "Home Facade+HomeFacadeDetails";
		else if (ifDim.toLowerCase().replaceAll(" ", "").indexOf("land") >= 0)
			productType = "Land+LandDetails";

		if (ifDim.indexOf("-") >= 0)
			ifDim = ifDim.substring(ifDim.indexOf("-") + 1);

		String ifDimValue = costings.getString("IfDimValue");
		if (ifDimValue.indexOf("-") >= 0)
			ifDimValue = ifDimValue.substring(ifDim.indexOf("-") + 1);

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductDetails");
		row.setConnection(this.getConnection());
		row.setParameter("-select", ifDimValue);
		row.setParameter("ProductType", productType);
		row.setParameter("DetailsType", ifDim);
		row.setParameter("ProductID", houselandid);
		try {
			if (row.getString("ProductID").length() > 0)
				row.doAction("selectfirst");
			if (!row.isSuccessful()) {
				row.remove("ProductType");
				if (row.getString("ProductID").length() > 0)
					row.doAction("selectfirst");
			}
			System.out.println(row.toString());
			System.out.println(row.getStatement());
		} catch (Exception e) {
			logger.trace("error in {} of {}", "checkCondition", e.getMessage());
			return false;
		}

		try {
			double value = Double.parseDouble(calculate(costings
					.getString("IfValue")));

			if (row.isSuccessful() && row.getString(ifDimValue).length() > 0) {
				if (costings.getString("IfCalc").indexOf("-") > 0) {
					try {
						String range = costings.getString("IfCalc");
						String[] parts = range.split("-");
						double from = Double.parseDouble(parts[0].trim());
						double to = Double.parseDouble(parts[1].trim());
						return row.getDouble(ifDimValue) >= from
								&& row.getDouble(ifDimValue) <= to;
					} catch (Exception e) {
						logger.trace("error in {} of {}", "checkCondition",
								e.getMessage());
					}
				} else if ("<".equals(costings.getString("IfCalc"))) {
					return row.getDouble(ifDimValue) < value;
				} else if (">".equals(costings.getString("IfCalc"))) {
					return row.getDouble(ifDimValue) > value;
				} else {
					return row.getDouble(ifDimValue) == value;
				}
			}
		} catch (Exception e) {
			logger.trace("error in {} of {}", "checkCondition", e.getMessage());
			String value = costings.getString("IfValue");

			if (row.isSuccessful() && row.getString(ifDimValue).length() > 0) {
				if (costings.getString("IfCalc").indexOf("-") > 0) {
					try {
						String range = costings.getString("IfCalc");
						String[] parts = range.split("-");
						double from = Double.parseDouble(parts[0].trim());
						double to = Double.parseDouble(parts[1].trim());
						return row.getDouble(ifDimValue) >= from
								&& row.getDouble(ifDimValue) <= to;
					} catch (Exception e1) {
						logger.trace("error in {} of {}", "checkCondition",
								e.getMessage());
					}
				} else if ("<".equals(costings.getString("IfCalc"))) {
					return row.getString(ifDimValue).compareToIgnoreCase(value) < 0;
				} else if (">".equals(costings.getString("IfCalc"))) {
					return row.getString(ifDimValue).compareToIgnoreCase(value) > 0;
				} else {
					return row.getString(ifDimValue).compareToIgnoreCase(value) == 0;
				}
			}
			if ("!".equals(value) || "=!".equals(value)) {
				return true;
			}
		}

		return false;
	}

	private String dashIfZero(String s) {
		if ("-".equals(s) || StringUtils.isBlank(s)) {
			return "0";
		}
		return s;
	}

	protected String calculate(String string) {
		if (string == null || string.length() == 0)
			return "";

		if ("-".equals(string))
			return string;

		int count = 0;
		string = string.trim();
		if (!string.startsWith("-") && string.length() != string.replaceAll("[\\+\\-\\*\\/]", "").length()) string = string.replaceAll("[\\+\\-\\*\\/]", " $0 ").trim();
		String result = string.replaceAll("  ", " ").replaceAll("  ", " ");
		// do this until there is no more maths or 20 attempts (so we don't get
		// into an endless loop due to bad maths
		while (result.length() != result.replaceAll("[\\+\\-\\*\\/]", "")
				.length() && ++count < 20) {
			// tokenize it, make sure all double spaces become single space
			StringTokenizer st = new StringTokenizer(result, " ");
			while (st.hasMoreTokens()) {
				String token1 = st.nextToken();
				if (token1.indexOf("=") >= 0 || token1.indexOf("<") >= 0
						|| token1.indexOf(">") >= 0)
					continue;
				if (token1.indexOf("+") >= 0 || token1.indexOf("-") >= 0
						|| token1.indexOf("*") >= 0 || token1.indexOf("/") >= 0)
					break;
				if (st.hasMoreTokens()) {
					String maths = st.nextToken();
					if (maths.indexOf("=") >= 0 || maths.indexOf("<") >= 0
							|| maths.indexOf(">") >= 0)
						continue;
					if (maths.indexOf("-") == -1 && maths.indexOf("+") == -1
							&& maths.indexOf("*") == -1
							&& maths.indexOf("/") == -1)
						continue;
					if (st.hasMoreTokens()) {
						String token2 = st.nextToken();
						if (token2.indexOf("=") >= 0
								|| token2.indexOf("<") >= 0
								|| token2.indexOf(">") >= 0)
							continue;
						double a = 0;
						try {
							double d1 = Double.parseDouble(token1.replaceAll("[^0-9\\.\\-]", ""));
							double d2 = Double.parseDouble(token2.replaceAll("[^0-9\\.\\-]", ""));
							if ("+".equals(maths))
								a = d1 + d2;
							else if ("-".equals(maths))
								a = d1 - d2;
							else if ("*".equals(maths))
								a = d1 * d2;
							else if ("/".equals(maths) && d2 != 0)
								a = d1 / d2;
							else if ("/".equals(maths) && d2 == 0)
								a = 0;
							a = (double) Math.floor((double) a * 1000) / 1000;
						} catch (Exception e) {
							logger.trace("error in {} of {}", "calculate",
									e.getMessage());
						}
						StringBuilder sb = (new StringBuilder(token1))
								.append(" \\").append(maths).append(" ")
								.append(token2);
						if (!result.equals(result.replaceFirst(sb.toString(),
								String.valueOf(a)))) {
							result = result.replaceFirst(sb.toString(),
									String.valueOf(a));
						} else {
							sb = (new StringBuilder(token1)).append(" \\")
									.append(maths).append(" ").append(token2);
							if (!result.equals(result.replaceFirst(
									sb.toString(), String.valueOf(a)))) {
								result = result.replaceFirst(sb.toString(),
										String.valueOf(a));
							} else {
								sb = (new StringBuilder(token1)).append(maths)
										.append(token2);
								if (!result.equals(result.replaceFirst(
										sb.toString(), String.valueOf(a)))) {
									result = result.replaceFirst(sb.toString(),
											String.valueOf(a));
								} else {
									sb = (new StringBuilder(token1))
											.append("\\").append(maths)
											.append(token2);
									if (!result.equals(result.replaceFirst(
											sb.toString(), String.valueOf(a)))) {
										result = result.replaceFirst(
												sb.toString(),
												String.valueOf(a));
									}

								}

							}
						}
					}
				}
			}
		}
		
		if (result.replaceAll("[a-zA-Z\\<\\>\\=\\~]", "").length() != result.length()) return result;

		return result.replaceAll("[^0-9\\.\\-]", "");
	}

	protected String getCondition(GenRow costings, String houselandid) {
		if (costings.getString("IfDimension").length() == 0
				|| costings.getString("IfDimValue").length() == 0)
			return "";

		String ifDim = costings.getString("IfDimension");

		String productType = "";
		if (ifDim.toLowerCase().replaceAll(" ", "").indexOf("plan") >= 0)
			productType = "HomePlan+HomePlanDetails+Home Plan";
		else if (ifDim.toLowerCase().replaceAll(" ", "").indexOf("facade") >= 0)
			productType = "HomeFacadeDetails+Home Facade";
		else if (ifDim.toLowerCase().replaceAll(" ", "").indexOf("land") >= 0)
			productType = "Land+LandDetails";

		if (ifDim.indexOf("-") >= 0)
			ifDim = ifDim.substring(ifDim.indexOf("-") + 1);

		String ifDimValue = costings.getString("IfDimValue");
		if (ifDimValue.indexOf("-") >= 0)
			ifDimValue = ifDimValue.substring(ifDim.indexOf("-") + 1);

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductDetails");
		row.setConnection(this.getConnection());
		row.setParameter("-select", ifDimValue);
		row.setParameter("ProductType", productType);
		row.setParameter("DetailsType", ifDim);
		row.setParameter("ProductID", houselandid);
		try {
			if (row.getString("ProductID").length() > 0)
				row.doAction("selectfirst");
			if (!row.isSuccessful()) {
				row.remove("ProductID");
				if (row.getString("ProductID").length() > 0)
					row.doAction("selectfirst");
			}
		} catch (Exception e) {
			logger.trace("error in {} of {}", "checkCondition", e.getMessage());
			return "";
		}

		String value = costings.getString("IfValue");

		if (row.isSuccessful() && row.getString(ifDimValue).length() > 0) {
			if (costings.getString("IfCalc").indexOf("-") > 0) {
				try {
					return ifDim + ":" + ifDimValue + " "
							+ costings.getString("IfCalc");
				} catch (Exception e) {
					logger.trace("error in {} of {}", "getCondition",
							e.getMessage());
				}
			} else if ("<".equals(costings.getString("IfCalc"))) {
				return ifDim + ":" + ifDimValue + " < " + value;
			} else if (">".equals(costings.getString("IfCalc"))) {
				return ifDim + ":" + ifDimValue + " > " + value;
			} else {
				return ifDim + ":" + ifDimValue + " = " + value;
			}
		}

		return "";
	}

	protected void updatePackageCost(String houseproductid,
			String lotproductid, String houselandid) {

		TableSpec detailsSpec = SpecManager.getTableSpec("properties/ProductDetails");
		// TableSpec homeSpec =
		// SpecManager.getTableSpec("properties/HomeDetails");

		GenRow productProducts = new GenRow();
		productProducts.setViewSpec("ProductLinkedProductView");
		productProducts.setParameter("ProductID", houselandid);
		productProducts.setParameter("ProductType", "Package Cost");
		productProducts.setParameter("ProductProductsLinkedProduct.ProductType", "Package Cost");
		productProducts.doAction("search");
		productProducts.getResults();

		// System.out.println(productProducts.getStatement());
		logger.trace("updatePackageCost Statement {}",productProducts.getStatement());

		 String findings = "Concrete Piers Additional";
		while (productProducts.getNext()) {
			if (findings.equalsIgnoreCase(productProducts.getString("Name")))
				System.out.println("Found");
			String conditionsStr = "";
			String parentStr = "";
			String costCalcField = productProducts.getString("PackageCostCalcField");
			String costCalcProduct = productProducts.getString("PackageCostProductDetails");
			String cost = calculate(productProducts.getString("Cost"));
			GenRow updateProduct = new GenRow();
			updateProduct.setTableSpec("Products");
			updateProduct.setConnection(this.getConnection());

			updateProduct.putAllDataAsParameters(productProducts);
			updateProduct.setColumn("ProductID", productProducts.getString("LinkedProductID"));

			logger.trace("costCalcField {}", costCalcField);

			if ("MultiCosting".equals(costCalcField) || "FreeCosting".equals(costCalcField)) {
				GenRow costings = findCostings(productProducts.getString("LinkedProductID"),"NULL+EMPTY");
				if (costings != null && costings.getSize() > 0) {
					GenRow houseland = getCachedViewedProduct(
							"properties/ProductHouseAndLandView", "ProductID",
							houselandid);

					if (houseland.getString("ProductType").length() > 0) {
						cost = calculate(updateProduct.getColumn("Cost"));
						while (costings.getNext()) {
							costCalcField = productProducts.getString("PackageCostCalcField");
							String condition = "";
							String applyTo = costings.getString("ApplyTo");
							String applyToID = costings.getString("ApplyToID");
							boolean theCondition = true;
							if ("FreeForm".equals(costings.getString("IfDimension"))) {
								theCondition = checkComplicatedFree(costings, houselandid);
								if (theCondition) parentStr += costings.getString("IfDimValue") + "<br>";
							} else {
								theCondition = checkCondition(costings, houselandid);
								if (theCondition) parentStr += costings.getString("IfDimension") + "." + costings.getString("IfDimValue") + costings.getString("IfCalc") + costings.getString("IfValue") + "<br>"; 
							}

							logger.debug(costCalcField + " cost {} ",
									costings.getString("Cost"));

							String dbCost = costings.getString("Cost");
							String packageCostCalcField = costings
									.getString("PackageCostCalcField");
							String PackageCostProductDetails = costings
									.getString("PackageCostProductDetails");
							if (theCondition
									&& costings.getString("Cost")
											.replaceAll("[0\\.\\-]", "").length() == 0) {
								GenRow child = findCostings(
										productProducts
												.getString("LinkedProductID"),
										costings.getString("CostingID"));
								if (child != null) {
									String calcStr = updateProduct
											.getColumn("PackageCostCalcString");
									while (child.getNext()) {
										if ("FreeForm".equals(child
												.getString("IfDimension"))) {
											theCondition = checkComplicatedFree(child, houselandid);
											if (theCondition) conditionsStr += parentStr + "&nsp;&nbsp;" + child.getString("IfDimValue") + "<br>";
										} else {
											theCondition = checkCondition(child, houselandid);
											if (theCondition) conditionsStr += parentStr + "&nsp;&nbsp;" + costings.getString("IfDimension") + "." + costings.getString("IfDimValue") + costings.getString("IfCalc") + costings.getString("IfValue") + "<br>"; 
										}
										if (theCondition) {
											dbCost = child.getString("Cost");
											packageCostCalcField = child
													.getString("PackageCostCalcField");
											PackageCostProductDetails = child
													.getString("PackageCostProductDetails");
											if (calcStr.length() > 0)
												calcStr += " +<br />";
											calcStr += getCondition(child,
													houselandid);

											updateProduct.setColumn(
													"PackageCostCalcField",
													packageCostCalcField);
											updateProduct
													.setColumn(
															"PackageCostProductDetails",
															applyTo.replaceAll(
																	" ", "")
																	+ "Details-"
																	+ PackageCostProductDetails);
											updateProduct.setColumn("Cost",
													calculate(dbCost));
											updateProduct.setColumn("Active",
													"Y");
											break;
										}
									}
									updateProduct.setColumn(
											"PackageCostCalcString",
											calcStr);
									child.close();
								} else {
									if (theCondition) conditionsStr += parentStr;
								}
							} else {
								if (theCondition) conditionsStr += parentStr;
							}
							if (dbCost.length() == 0)
								dbCost = "0";
							if ("".equals(applyTo) && theCondition) {
								updateProduct.setColumn("PackageCostCalcField",
										packageCostCalcField);
								updateProduct.setColumn(
										"PackageCostProductDetails",
										applyTo.replaceAll(" ", "")
												+ "Details-"
												+ PackageCostProductDetails);
								updateProduct.setColumn("Cost",
										calculate(dbCost));
								cost = calculate(dbCost);
								if (condition.length() > 0)
									condition += ", ";
								if ("FreeForm".equals(costings.getString("IfDimension")))
									condition += costings.getString("ifDim");
								else
									condition += getCondition(costings,
											houselandid);
								updateProduct.setColumn("Active", "Y");
							} else if ("Brand".equals(applyTo)
									&& costings
											.getString("ApplyToID")
											.equals(houseland
													.getString("BrandProductID"))
									&& theCondition) {
								updateProduct.setColumn("PackageCostCalcField",
										packageCostCalcField);
								updateProduct.setColumn(
										"PackageCostProductDetails",
										applyTo.replaceAll(" ", "")
												+ "Details-"
												+ PackageCostProductDetails);
								updateProduct.setColumn("Cost",
										calculate(dbCost));
								cost = calculate(dbCost);
								if (condition.length() > 0)
									condition += ", ";
								if ("FreeForm".equals(costings.getString("IfDimension")))
									condition += costings.getString("ifDim");
								else
									condition += getCondition(costings,
											houselandid);
								updateProduct.setColumn("Active", "Y");
							} else if ("Home Range".equals(applyTo)
									&& costings
											.getString("ApplyToID")
											.equals(houseland
													.getString("RangeProductID"))
									&& theCondition) {
								updateProduct.setColumn("PackageCostCalcField",
										packageCostCalcField);
								updateProduct.setColumn(
										"PackageCostProductDetails",
										applyTo.replaceAll(" ", "")
												+ "Details-"
												+ PackageCostProductDetails);
								updateProduct.setColumn("Cost",
										calculate(dbCost));
								cost = calculate(dbCost);
								if (condition.length() > 0)
									condition += ", ";
								if ("FreeForm".equals(costings.getString("IfDimension")))
									condition += costings.getString("ifDim");
								else
									condition += getCondition(costings,
											houselandid);
								updateProduct.setColumn("Active", "Y");
							} else if ("Home".equals(applyTo)
									&& costings
											.getString("ApplyToID")
											.equals(houseland
													.getString("HomeProductID"))
									&& theCondition) {
								updateProduct.setColumn("PackageCostCalcField",
										packageCostCalcField);
								updateProduct.setColumn(
										"PackageCostProductDetails",
										applyTo.replaceAll(" ", "")
												+ "Details-"
												+ PackageCostProductDetails);
								updateProduct.setColumn("Cost",
										calculate(dbCost));
								cost = calculate(dbCost);
								if (condition.length() > 0)
									condition += ", ";
								if ("FreeForm".equals(costings.getString("IfDimension")))
									condition += costings.getString("ifDim");
								else
									condition += getCondition(costings,
											houselandid);
								updateProduct.setColumn("Active", "Y");
							} else if ("Home Plan".equals(applyTo)
									&& costings.getString("ApplyToID").equals(
											houseland.getString("CopyPlanID"))
									&& theCondition) {
								updateProduct.setColumn("PackageCostCalcField",
										packageCostCalcField);
								updateProduct.setColumn(
										"PackageCostProductDetails",
										applyTo.replaceAll(" ", "")
												+ "Details-"
												+ PackageCostProductDetails);
								updateProduct.setColumn("Cost",
										calculate(dbCost));
								cost = calculate(dbCost);
								if (condition.length() > 0)
									condition += ", ";
								if ("FreeForm".equals(costings.getString("IfDimension")))
									condition += costings.getString("ifDim");
								else
									condition += getCondition(costings,
											houselandid);
								updateProduct.setColumn("Active", "Y");
							} else if ("Location".equals(applyTo)
									&& (costings.getString("ApplyToID").equals(
											houseland.getString("LocationID")) || costings
											.getString("ApplyToID")
											.equals(houseland
													.getString("RegionID")))
									&& theCondition) {
								updateProduct.setColumn("PackageCostCalcField",
										packageCostCalcField);
								updateProduct.setColumn(
										"PackageCostProductDetails",
										applyTo.replaceAll(" ", "")
												+ "Details-"
												+ PackageCostProductDetails);
								updateProduct.setColumn("Cost",
										calculate(dbCost));
								cost = calculate(dbCost);
								if (condition.length() > 0)
									condition += ", ";
								if ("FreeForm".equals(costings.getString("IfDimension")))
									condition += costings.getString("ifDim");
								else
									condition += getCondition(costings,
											houselandid);
								updateProduct.setColumn("Active", "Y");
							} else if ("Estate".equals(applyTo)
									&& (costings
											.getString("ApplyToID")
											.equals(houseland
													.getString("EstateProductID")) || "UseAllOthers"
											.equals(costings
													.getString("ApplyToID")))
									&& theCondition) {
								updateProduct.setColumn("PackageCostCalcField",
										packageCostCalcField);
								updateProduct.setColumn(
										"PackageCostProductDetails",
										applyTo.replaceAll(" ", "")
												+ "Details-"
												+ PackageCostProductDetails);
								updateProduct.setColumn("Cost",
										calculate(dbCost));
								cost = calculate(dbCost);
								if (condition.length() > 0)
									condition += ", ";
								if ("FreeForm".equals(costings.getString("IfDimension")))
									condition += costings.getString("ifDim");
								else
									condition += getCondition(costings,
											houselandid);
								updateProduct.setColumn("Active", "Y");
							} else if ("SubType".equals(applyTo)
									&& (costings
											.getString("ApplyToID")
											.equals(houseland
													.getString("ProductSubType")) || "UseAllOthers"
											.equals(costings
													.getString("ApplyToID")))
									&& theCondition) {
								updateProduct.setColumn("PackageCostCalcField",
										packageCostCalcField);
								updateProduct.setColumn(
										"PackageCostProductDetails",
										applyTo.replaceAll(" ", "")
												+ "Details-"
												+ PackageCostProductDetails);
								updateProduct.setColumn("Cost",
										calculate(dbCost));
								cost = calculate(dbCost);
								if (condition.length() > 0)
									condition += ", ";
								if ("FreeForm".equals(costings.getString("IfDimension")))
									condition += costings.getString("ifDim");
								else
									condition += getCondition(costings,
											houselandid);
								updateProduct.setColumn("Active", "Y");
							} else if ("Marketing".equals(applyTo)
									&& (costings
											.getString("ApplyToID")
											.equals(houseland
													.getString("MarketingStatus")) || "UseAllOthers"
											.equals(costings
													.getString("ApplyToID")))
									&& theCondition) {
								updateProduct.setColumn("PackageCostCalcField",
										packageCostCalcField);
								updateProduct.setColumn(
										"PackageCostProductDetails",
										applyTo.replaceAll(" ", "")
												+ "Details-"
												+ PackageCostProductDetails);
								updateProduct.setColumn("Cost",
										calculate(dbCost));
								cost = calculate(dbCost);
								if (condition.length() > 0)
									condition += ", ";
								if ("FreeForm".equals(costings.getString("IfDimension")))
									condition += costings.getString("ifDim");
								else
									condition += getCondition(costings,
											houselandid);
								updateProduct.setColumn("Active", "Y");
							} else if (applyTo.length() == 0
									&& applyToID.length() == 0 && theCondition) {
								updateProduct.setColumn("PackageCostCalcField",
										packageCostCalcField);
								updateProduct.setColumn(
										"PackageCostProductDetails",
										applyTo.replaceAll(" ", "")
												+ "Details-"
												+ PackageCostProductDetails);
								updateProduct.setColumn("Cost",
										calculate(dbCost));
								cost = calculate(dbCost);
								if (condition.length() > 0)
									condition += ", ";
								if ("FreeForm".equals(costings.getString("IfDimension")))
									condition += costings.getString("ifDim");
								else
									condition += getCondition(costings,
											houselandid);
								updateProduct.setColumn("Active", "Y");
							} else {
								cost = "0";
								if (updateProduct.getString(
										"PackageCostCalcString").length() == 0)
									updateProduct.setColumn("Active", "N");
							}
							costCalcField = updateProduct
									.getColumn("PackageCostCalcField");
							costCalcProduct = updateProduct
									.getColumn("PackageCostProductDetails");

							if (costCalcField.indexOf("-") >= 0)
								costCalcField = costCalcField
										.substring(costCalcField.indexOf("-") + 1);
							
							if(StringUtils.isNotBlank(costCalcProduct) && costCalcProduct.contains("Details-Home Plan")) {
								costCalcProduct = costings.getString("PackageCostProductDetails");
								double tCost = 0;
								 GenRow homePlanRow = getCachedTabledProduct("Products", "ProductID", houseproductid);
								 if ("Home Plan".equals(costCalcProduct)) {
									 tCost = Double.parseDouble(cost.replaceAll("[^0-9\\-\\.]","")) * homePlanRow.getDouble("Cost");
								} else if ("Home Plan Total".equals(costCalcProduct)) {
									tCost = Double.parseDouble(cost.replaceAll("[^0-9\\-\\.]","")) * homePlanRow.getDouble("TotalCost");
								}
								 
								 updateProduct.setColumn("TotalCost", "" + tCost);
								 updateProduct.setColumn("PackageCostCalcString", "(" + costCalcProduct + " X " + currency.format(Double.parseDouble(cost)) + ") "  + currency.format(tCost));
								 updateProduct.setColumn("PackageCostProductDetails", costCalcProduct);
								 
							} else if (costCalcField.length() == 0) {
								// System.out.println("Direct " +
								// updateProduct.getColumn("TotalCost") + " + "
								// + cost);
								double tCost = 0;
								try {
									tCost = Double.parseDouble(updateProduct
											.getColumn("TotalCost").replaceAll(
													"[^0-9\\-\\.]", ""))
											+ Double.parseDouble(cost
													.replaceAll("[^0-9\\-\\.]",
															""));
								} catch (Exception e) {
									logger.trace("error in {} of {}",
											"updatePackageCost", e.getMessage());
								}
								updateProduct
										.setColumn("TotalCost", "" + tCost);
								if (!"-".equals(cost)
										&& cost.replaceAll("[^0-9\\-\\.]", "")
												.trim().length() > 0
										&& Double.parseDouble(cost.replaceAll(
												"[^0-9\\-\\.]", "")) != 0) {
									String calcStr = updateProduct
											.getColumn("PackageCostCalcString");
									if (calcStr.length() > 0)
										calcStr += " +<br />";
									if (condition.length() > 0)
										condition += " ";
									calcStr += condition
											+ currency
													.format(Double
															.parseDouble(calculate(cost)));
									updateProduct.setColumn(
											"PackageCostCalcString", calcStr);
								} else if ("-".equals(cost)) {
									String calcStr = updateProduct
											.getColumn("PackageCostCalcString");
									if (calcStr.length() > 0)
										calcStr += " +<br />";
									if (condition.length() > 0)
										condition += " ";
									calcStr += condition + "$0.00";
									updateProduct.setColumn(
											"PackageCostCalcString", calcStr);
								}
								// } else if
								// ("MultiCosting".equals(costCalcField)) {
								// updateProduct.setColumn("PackageCostCalcString","Not Applied");
							} else if (detailsSpec.hasField(costCalcField)) {
								if (costCalcProduct.indexOf("-") >= 0) {
									costCalcProduct = costCalcProduct
											.substring(costCalcProduct
													.indexOf("-") + 1);
									// yes we have to remove it twice
									String productType = "";
									if (costCalcProduct.toLowerCase().replaceAll(" ", "")
											.indexOf("plan") >= 0)
										productType = "HomePlan+HomePlanDetails+Home Plan";
									else if (costCalcProduct.toLowerCase().replaceAll(" ", "")
											.indexOf("facade") >= 0)
										productType = "HomeFacadeDetails+Home Facade";
									else if (costCalcProduct.toLowerCase().replaceAll(" ", "").indexOf("land") >= 0)
										productType = "Land+LandDetails";

									if (costCalcProduct.indexOf("-") >= 0)
										costCalcProduct = costCalcProduct
												.substring(costCalcProduct
														.indexOf("-") + 1);
									GenRow details = productMap.get(houselandid
											+ productType + costCalcProduct);
									if (details == null && houselandid != null
											&& houselandid.length() > 0) {
										details = new GenRow();
										details.setTableSpec("properties/ProductDetails");
										details.setConnection(this
												.getConnection());
										details.setParameter("-select", "*");
										details.setColumn("ProductID",
												houselandid);
										details.setColumn("ProductType",
												productType);
										details.setColumn("DetailsType",
												costCalcProduct);
										if (details.getString("ProductID")
												.length() > 0)
											details.doAction("selectfirst");
										if (!details.isSuccessful()) {
											details.remove("ProductType");
											if (details.getString("ProductID")
													.length() > 0)
												details.doAction("selectfirst");
										}

										//System.out.println("Details Search " + details.getStatement());

										productMap.put(
												houselandid + productType
														+ costCalcProduct,
												details);
									}
									try {
										double total = Double
												.parseDouble(cost.replaceAll(
														"[^0-9\\-\\.]", ""))
												* details
														.getDouble(costCalcField);
										// System.out.println("Range " +
										// updateProduct.getColumn("TotalCost")
										// + " + " + total);
										if (total != 0) {
											String calcStr = updateProduct
													.getColumn("PackageCostCalcString");
											if (calcStr.length() > 0)
												calcStr += " +<br />";
											if (condition.length() > 0)
												condition += " ";
											calcStr += condition + " ("
													+ costCalcProduct + ":"
													+ costCalcField + " ";
											String calc = details
													.getString(costCalcField);
											try {
												calc = number
														.format(Double
																.parseDouble(calculate(calc)));
											} catch (Exception e) {
												logger.trace(
														"error in {} of {}",
														"updatePackageCost",
														e.getMessage());
											}
											calcStr += calc
													+ " X "
													+ currency
															.format(Double
																	.parseDouble(calculate(cost)))
													+ ") "
													+ currency.format(total);
											updateProduct.setColumn(
													"PackageCostCalcString",
													calcStr);
											total += Double
													.parseDouble(updateProduct
															.getColumn("TotalCost"));
											updateProduct.setColumn(
													"TotalCost", "" + total);
										}
									} catch (Exception e) {
										logger.trace("error in {} of {}",
												"updatePackageCost",
												e.getMessage());
									}
								}
							} else {
								if (costCalcField.startsWith("Lot")
										|| "Perimeter".equals(costCalcField)) {
									GenRow details = getCachedTabledProduct(
											"properties/HomeDetails",
											"ProductID", lotproductid);
									if (details != null) {
										if ("LotArea".equals(costCalcField))
											costCalcField = "Area";
										double total = productProducts
												.getDouble("Cost")
												* details
														.getDouble(costCalcField);
										// System.out.println("Old Lot " +
										// updateProduct.getColumn("TotalCost")
										// + " + " + total);
										if (total != 0) {
											String calcStr = updateProduct
													.getColumn("PackageCostCalcString");
											if (calcStr.length() > 0)
												calcStr += " +<br />";
											String calc = details
													.getString(costCalcField);
											try {
												calc = number
														.format(Double
																.parseDouble(calculate(calc)));
											} catch (Exception e) {
												logger.trace(
														"error in {} of {}",
														"updatePackageCost",
														e.getMessage());
											}
											calcStr += calc
													+ " X "
													+ currency
															.format(productProducts
																	.getDouble("Cost"));
											updateProduct.setColumn(
													"PackageCostCalcString",
													calcStr);
											total += Double
													.parseDouble(updateProduct
															.getColumn("TotalCost"));
											updateProduct.setColumn(
													"TotalCost", "" + total);
										}
									}
								} else {
									costCalcField = costCalcField.replaceAll(
											"Home", "Lot");
									GenRow details = getCachedTabledProduct(
											"properties/HomeDetails",
											"ProductID", houseproductid);
									if (details != null) {
										if ("LotArea".equals(costCalcField)) {
											if (details.getString("Storey") != null
													&& "Double"
															.equals(details
																	.getString("Storey"))) {
												costCalcField = "GroundFloorArea";
											} else {
												costCalcField = "HomeSize";
											}
											updateProduct.setColumn(
													"PackageCostCalcField",
													costCalcField);
										}
										double total = productProducts
												.getDouble("Cost")
												* details
														.getDouble(costCalcField);
										// System.out.println("Old House " +
										// updateProduct.getColumn("TotalCost")
										// + " + " + total);
										if (total != 0) {
											String calcStr = updateProduct
													.getColumn("PackageCostCalcString");
											if (calcStr.length() > 0)
												calcStr += " +<br />";
											String calc = details
													.getString(costCalcField);
											try {
												calc = number
														.format(Double
																.parseDouble(calculate(calc)));
											} catch (Exception e) {
												logger.trace(
														"error in {} of {}",
														"updatePackageCost",
														e.getMessage());
											}
											calcStr += calc
													+ " X "
													+ currency
															.format(productProducts
																	.getDouble("Cost"));
											updateProduct.setColumn(
													"PackageCostCalcString",
													calcStr);
											total += Double
													.parseDouble(updateProduct
															.getColumn("TotalCost"));
											updateProduct.setColumn(
													"TotalCost", "" + total);
										}
									}
								}
							}
						}
					}
				}
				if (costings != null)
					costings.close();
				if (updateProduct.getColumn("PackageCostCalcString").length() == 0) {
					if ((!"-".equals(cost) && !"0".equals(cost))
							|| "N".equals(updateProduct.getString("Active"))) {
						updateProduct.setColumn("PackageCostCalcString",
								"Not Applied");
						updateProduct.setColumn("Active", "N");
					} else {
						updateProduct.setColumn("PackageCostCalcString",
								"Included");
					}
				} else if ("$0.00".equals(updateProduct
						.getString("PackageCostCalcString"))) {
					if ("Y".equals(updateProduct.getString("Active")))
						updateProduct.setColumn("PackageCostCalcString",
								"Included");
					else
						updateProduct.setColumn("PackageCostCalcString",
								"Not Applied");
				}
			} else {
				costCalcField = updateProduct.getColumn("PackageCostCalcField");
				costCalcProduct = updateProduct
						.getColumn("PackageCostProductDetails");

				String pType = "";
				if (costCalcField.indexOf("-") >= 0)
					costCalcField = costCalcField.substring(costCalcField
							.indexOf("-") + 1);
				if (costCalcProduct.indexOf("-") >= 0) {
					pType = costCalcProduct.substring(0,
							costCalcProduct.indexOf("-"));
					costCalcProduct = costCalcProduct.substring(costCalcProduct
							.indexOf("-") + 1);
				}

				cost = calculate(updateProduct.getColumn("Cost"));
				if (StringUtils.isNotBlank(costCalcProduct) && costCalcProduct.contains("Home Plan")) {
					double tCost = 0;
					try {
						tCost = Double.parseDouble(cost.replaceAll(
								"[^0-9\\-\\.]", ""));
					} catch (Exception e) {
						logger.trace("error in {} of {}", "updatePackageCost",
								e.getMessage());
					}
					
					GenRow homePlanRow = getCachedTabledProduct("Products", "ProductID", houseproductid);
					if ("Home Plan".equals(costCalcProduct)) {
						tCost = tCost * homePlanRow.getDouble("Cost");
					} else if ("Home Plan Total".equals(costCalcProduct)) {
						tCost = tCost * homePlanRow.getDouble("TotalCost");
					}

					updateProduct.setColumn("TotalCost", "" + tCost);
				} else if (costCalcField.length() == 0) {

					double tCost = 0;
					try {
						tCost = Double.parseDouble(cost.replaceAll(
								"[^0-9\\-\\.]", ""));
					} catch (Exception e) {
						logger.trace("error in {} of {}", "updatePackageCost",
								e.getMessage());
					}


				} else if (detailsSpec.hasField(costCalcField)) {
					if (costCalcProduct.indexOf("-") >= 0) {
						costCalcProduct = costCalcProduct
								.substring(costCalcProduct.indexOf("-") + 1);
					}
					GenRow details = productMap.get(houselandid
							+ costCalcProduct);
					if (details == null && houselandid != null
							&& houselandid.length() > 0) {
						details = new GenRow();
						details.setTableSpec("properties/ProductDetails");
						details.setConnection(this.getConnection());
						details.setParameter("-select", "*");
						details.setColumn("ProductID", houselandid);
						details.setColumn("DetailsType", costCalcProduct);
						details.setColumn("ProductType", pType);
						if (details.getString("ProductID").length() > 0)
							details.doAction("selectfirst");

						if (!details.isSuccessful()) {
							details.remove("ProductType");
							details.doAction("selectfirst");
						}

						productMap.put(houselandid + costCalcProduct, details);
					}
					try {
						double total = Double.parseDouble(cost.replaceAll(
								"[^0-9\\-\\.]", ""))
								* details.getDouble(costCalcField);
						updateProduct.setColumn("TotalCost", "" + total);
					} catch (Exception e) {
						logger.trace("error in {} of {}", "updatePackageCost",
								e.getMessage());
					}
				} else {
					if (costCalcField.startsWith("Lot")
							|| "Perimeter".equals(costCalcField)) {
						GenRow details = getCachedTabledProduct(
								"properties/HomeDetails", "ProductID",
								lotproductid);
						if (details != null) {
							if ("LotArea".equals(costCalcField))
								costCalcField = "Area";

							double total = Double
									.parseDouble(calculate(productProducts
											.getColumn("Cost")))
									* details.getDouble(costCalcField);
							updateProduct.setColumn("TotalCost", "" + total);
						}
					} else {
						costCalcField = costCalcField.replaceAll("Home", "Lot");
						GenRow details = getCachedTabledProduct(
								"properties/HomeDetails", "ProductID",
								houseproductid);
						if (details != null) {
							if ("LotArea".equals(costCalcField)) {
								if (details.getString("Storey") != null
										&& "Double".equals(details
												.getString("Storey"))) {
									costCalcField = "GroundFloorArea";
								} else {
									costCalcField = "HomeSize";
								}
								updateProduct.setColumn("PackageCostCalcField",
										costCalcField);
							}

							double total = Double
									.parseDouble(calculate(productProducts
											.getColumn("Cost")))
									* details.getDouble(costCalcField);
							updateProduct.setColumn("TotalCost", "" + total);
							/*
							 * double total = Double
							 * .parseDouble(calculate(productProducts
							 * .getColumn("Cost").replaceAll("[^0-9\\.\\-]",
							 * ""))) details.getDouble(costCalcField);
							 * updateProduct.setColumn("TotalCost", "" + total);
							 */
						}
					}
				}
				if (updateProduct.getString("PackageCostCalcString").length() == 0) {
					if ("-".equals(calculate(updateProduct
							.getColumn("TotalCost")))
							|| "0".equals(calculate(updateProduct
									.getColumn("TotalCost")))) {
						if ("Y".equals(updateProduct.getString("Active")))
							updateProduct.setColumn("PackageCostCalcString",
									"Included");
						else
							updateProduct.setColumn("PackageCostCalcString",
									"Not Applied");
					} else {
						updateProduct
								.setColumn(
										"PackageCostCalcString",
										"Flat "
												+ currency.format(Double
														.parseDouble(dashIfZero(calculate(updateProduct
																.getColumn("TotalCost"))))));
					}
				} else if ("$0.00".equals(updateProduct
						.getString("PackageCostCalcString"))) {
					if ("Y".equals(updateProduct.getString("Active")))
						updateProduct.setColumn("PackageCostCalcString",
								"Included");
					else
						updateProduct.setColumn("PackageCostCalcString",
								"Not Applied");
				}
			}
			updateProduct
					.setColumn("TotalCost", dashIfZero(calculate(updateProduct
							.getColumn("TotalCost"))));
			updateProduct.setColumn("Cost",
					dashIfZero(calculate(updateProduct.getColumn("Cost"))));
			if ("Not Applied".equals(updateProduct
					.getString("PackageCostCalcString"))) {
				updateProduct.setColumn("Active", "N");
			}
			if ("Flat -$0.00".equals(updateProduct
					.getString("PackageCostCalcString"))
					|| "Flat $0.00".equals(updateProduct
							.getString("PackageCostCalcString"))) {
				updateProduct.setParameter("PackageCostCalcString", "Included");
				updateProduct.setColumn("TotalCost", "0.00");
				updateProduct.setColumn("Cost", "0.00");
			}
			logger.trace("updateProduct {}", updateProduct.toString());

			if (StringUtils.isNotBlank(conditionsStr) && !"<br>".equals(conditionsStr)) {
				if (StringUtils.isBlank(productProducts.getString("Description"))) updateProduct.setParameter("Description", conditionsStr);
				else updateProduct.setParameter("Description", productProducts.getString("Description").replaceFirst("\\$\\{Conditions\\}", "<br>" + conditionsStr));
			}
			updateProduct.doAction("update");
		}
		productProducts.close();
	}

	/*
	 * This is now done in Drip Services public GenRow updateDRIPvalue(String
	 * dripID, String productID) { return updateDRIPvalue(dripID, productID, new
	 * Date()); }
	 * 
	 * public GenRow updateDRIPvalue(String dripID, String productID, Date date)
	 * {
	 * 
	 * PropertyEntity pe = PropertyFactory.getPropertyEntity(getConnection(),
	 * productID);
	 * 
	 * GenRow drip = new GenRow(); drip.setViewSpec("ProductView");
	 * drip.setConnection(getConnection()); drip.setParameter("ProductID",
	 * dripID); drip.doAction("selectfirst");
	 * 
	 * processDRIPvalue(pe, drip, date);
	 * 
	 * return drip; }
	 * 
	 * public Date dateString(String date) { if (date == null || date.length()
	 * == 0) return null;
	 * 
	 * if (date.indexOf(".") > 0) date = date.substring(0, date.indexOf("."));
	 * try { return dformat.parse(date); } catch (Exception e) { if
	 * (date.indexOf(" ") > 0) { try { return sformat.parse(date.substring(0,
	 * date.indexOf(" "))); } catch (Exception e2) { } } }
	 * 
	 * return null; }
	 * 
	 * this code is now done in DripServices public GenRow
	 * updateDRIPvalue(GenRow drip, String productID) { return
	 * updateDRIPvalue(drip, productID, new Date()); }
	 * 
	 * 
	 * public GenRow updateDRIPvalue(GenRow drip, String productID, Date date) {
	 * PropertyEntity pe = PropertyFactory.getPropertyEntity(getConnection(),
	 * productID);
	 * 
	 * processDRIPvalue(pe, drip, date);
	 * 
	 * return drip; }
	 * 
	 * private void processDRIPvalue(GenRow product, GenRow drip, Date now) {
	 * Date start = null; Date end = null;
	 * 
	 * if (drip.getString("AvailableDate").length() > 0) start =
	 * dateString(drip.getString("AvailableDate")); if
	 * (drip.getString("ExpiryDate").length() > 0) end =
	 * dateString(drip.getString("ExpiryDate"));
	 * 
	 * if ("Y".equals(drip.getString("Active"))) { if ((start == null ||
	 * start.before(now) || start.equals(now)) && (end == null ||
	 * end.after(now)) || end.equals(now)) { if
	 * ("House And Land".equalsIgnoreCase(product .getString("ProductType"))) {
	 * String productID = product.getString("ProductID"); product.clear();
	 * product.setViewSpec("properties/ProductHouseAndLandView");
	 * product.setParameter("ProductID", productID);
	 * product.doAction("selectfirst"); } drip.setParameter("-active",
	 * "Active");
	 * 
	 * String costCalcField = drip.getString("PackageCostCalcField"); String
	 * costCalcProduct = drip .getString("PackageCostProductDetails"); String
	 * cost = drip.getString("Cost");
	 * 
	 * if ("MultiCosting".equals(costCalcField) ||
	 * "FreeCosting".equals(costCalcField)) { GenRow costings = findCostings(
	 * drip.getString("LinkedProductID"), "NULL"); } else { if
	 * (costCalcProduct.length() == 0 &&
	 * (drip.getString("Margin").replaceAll("[0\\.]", "").length() == 0)) {
	 * drip.setParameter("-value",
	 * currency.format(drip.getDouble("TotalCost"))); } else if
	 * ("Package Cost".equals(costCalcProduct)) { GenRow packageCost =
	 * getCachedTabledProduct("Products", "PackageCostProductID",
	 * costCalcField); //if
	 * (packageCost.getString("TotalCost").replaceAll("[0\\.]", "").length() ==
	 * 0) { // System.out.println(packageCost.getString("TotalCost")); //} }
	 * else if ("Land".equals(product.getString("ProductType"))) { if
	 * ("Land".equals(drip .getString("PackageCostProductDetails"))) { try { if
	 * (drip.getString("Margin").replaceAll("[0\\.]", "").length() == 0) {
	 * drip.setParameter("-value", currency
	 * .format(drip.getDouble("TotalCost"))); } else {
	 * drip.setParameter("-value", currency .format(drip.getDouble("Margin")
	 * product.getDouble("Cost"))); } } catch (Exception e) { } } else if
	 * ("Land Total".equals(drip .getString("PackageCostProductDetails"))) { try
	 * { if (drip.getString("Margin").replaceAll("[0\\.]", "").length() == 0) {
	 * drip.setParameter("-value", drip.getString("TotalCost")); } else {
	 * drip.setParameter( "-value", currency.format(drip .getDouble("Margin")
	 * product .getDouble("TotalCost"))); } } catch (Exception e) { } } } else
	 * if ("Home Plan".equals(product.getString("ProductType"))) { if
	 * ("Home Plan".equals(drip .getString("PackageCostProductDetails"))) { try
	 * { if (drip.getString("Margin").replaceAll("[0\\.]", "").length() == 0) {
	 * drip.setParameter("-value", currency
	 * .format(drip.getDouble("TotalCost"))); } else {
	 * drip.setParameter("-value", currency .format(drip.getDouble("Margin")
	 * product.getDouble("Cost"))); } } catch (Exception e) { } } else if
	 * ("Home Plan Total".equals(drip .getString("PackageCostProductDetails")))
	 * { try { if (drip.getString("Margin").replaceAll("[0\\.]", "").length() ==
	 * 0) { drip.setParameter("-value", currency
	 * .format(drip.getDouble("TotalCost"))); } else { drip.setParameter(
	 * "-value", currency.format(drip .getDouble("Margin") product
	 * .getDouble("TotalCost"))); } } catch (Exception e) { } } } else if
	 * ("House and Land".equalsIgnoreCase(product .getString("ProductType"))) {
	 * if ("Land".equals(drip .getString("PackageCostProductDetails"))) { try {
	 * if (drip.getString("Margin").replaceAll("[0\\.]", "").length() == 0) {
	 * drip.setParameter("-value", currency
	 * .format(drip.getDouble("TotalCost"))); } else {
	 * drip.setParameter("-value", currency .format(drip.getDouble("Margin")
	 * product.getDouble("LotCost"))); } } catch (Exception e) { } } else if
	 * ("Land Total".equals(drip .getString("PackageCostProductDetails"))) { try
	 * { if (drip.getString("Margin").replaceAll("[0\\.]", "").length() == 0) {
	 * drip.setParameter("-value", currency
	 * .format(drip.getDouble("TotalCost"))); } else { drip.setParameter(
	 * "-value", currency.format(drip .getDouble("Margin") product
	 * .getDouble("LotTotalCost"))); } } catch (Exception e) { } } if
	 * ("Home Plan".equals(drip .getString("PackageCostProductDetails"))) { try
	 * { if (drip.getString("Margin").replaceAll("[0\\.]", "").length() == 0) {
	 * drip.setParameter("-value", currency
	 * .format(drip.getDouble("TotalCost"))); } else { drip.setParameter(
	 * "-value", currency.format(drip .getDouble("Margin")
	 * product.getDouble("PlanCost"))); } } catch (Exception e) { } } else if
	 * ("Home Plan Total".equals(drip .getString("PackageCostProductDetails")))
	 * { try { if (drip.getString("Margin").replaceAll("[0\\.]", "").length() ==
	 * 0) { drip.setParameter("-value", currency
	 * .format(drip.getDouble("TotalCost"))); } else { drip.setParameter(
	 * "-value", currency.format(drip .getDouble("Margin") product
	 * .getDouble("PlanTotalCost"))); } } catch (Exception e) { } } if
	 * ("House And Land".equalsIgnoreCase(drip
	 * .getString("PackageCostProductDetails"))) { try { if
	 * (drip.getString("Margin").replaceAll("[0\\.]", "").length() == 0) {
	 * drip.setParameter("-value", currency
	 * .format(drip.getDouble("TotalCost"))); } else {
	 * drip.setParameter("-value", currency .format(drip.getDouble("Margin")
	 * product.getDouble("Cost"))); } } catch (Exception e) { } } else if
	 * ("House And Land Total".equalsIgnoreCase(drip
	 * .getString("PackageCostProductDetails"))) { try { if
	 * (drip.getString("Margin").replaceAll("[0\\.]", "").length() == 0) {
	 * drip.setParameter("-value", currency
	 * .format(drip.getDouble("TotalCost"))); } else { drip.setParameter(
	 * "-value", currency.format(drip .getDouble("Margin") product
	 * .getDouble("TotalCost"))); } } catch (Exception e) { } } } else {
	 * drip.setParameter("-value", ""); } } } else {
	 * drip.setParameter("-active", "Inactive"); drip.setParameter("-value",
	 * ""); } } else { drip.setParameter("-active", "Inactive");
	 * drip.setParameter("-value", ""); } }
	 */

	// we need to cache everything we can to speed this up a lot
	public GenRow getCachedTabledProduct(String tableSpec, String key,
			String value) {
		GenRow row = productMap.get(value + tableSpec);

		if (row != null)
			return row;

		row = new GenRow();
		row.setTableSpec(tableSpec);
		row.setConnection(this.getConnection());
		row.setParameter("-select", "*");
		row.setColumn(key, value);

		row.doAction("selectfirst");

		productMap.put(value + tableSpec, row);

		return row;
	}

	public void removeCachedProduct(String viewSpec, String key, String value) {
		productMap
				.remove(value + viewSpec.replaceAll("properties/Product", ""));
	}

	public GenRow getCachedViewedProduct(String viewSpec, String key,
			String value) {
		GenRow row = productMap.get(value
				+ viewSpec.replaceAll("properties/Product", ""));

		if (row != null)
			return row;

		row = new GenRow();
		row.setViewSpec(viewSpec);
		row.setConnection(this.getConnection());
		row.setColumn(key, value);

		row.doAction("selectfirst");

		productMap.put(value + viewSpec.replaceAll("properties/Product", ""),
				row);

		return row;
	}

	protected ArrayList<String> getGalleryIDs(String productID) {
		ArrayList<String> galleryIDs = new ArrayList<String>();
		if (productID != null && productID.length() > 0) {
			/*
			 * GenRow row = new GenRow(); row.setViewSpec("LinkedDocumentView");
			 * row.setConnection(this.getConnection());
			 * //row.setParameter("ProductID", productID);
			 * row.setParameter("GalleryID", "!NULL+!EMPTY");
			 * row.setParameter("-groupby0", "GalleryID");
			 * row.doAction("search");
			 */

			GenRow row = new GenRow();
			row.setViewSpec("GalleryView");
			row.setConnection(this.getConnection());
			row.setParameter("ProductID", productID);
			row.setParameter("-groupby0", "GalleryID");
			row.doAction("search");
			row.getResults();

			while (row.getNext()) {
				galleryIDs.add(row.getData("GalleryID"));
			}

			row.close();
		}

		return galleryIDs;
	}

	// TODO: this needs to check that there is no album there
	// TODO: change this so all home galleries are copied
	public String createAlbum(String productid, String planproductid,
			String facadeproductid, String landproductid, String userid,
			boolean refresh) {
		GenRow plan = getCachedViewedProduct("properties/ProductPlanView",
				"ProductID", planproductid);
		GenRow house = getCachedViewedProduct("properties/ProductHomeView",
				"ProductID", plan.getString("HomeProductID"));

		GenRow facade = null;
		if (facadeproductid != null && facadeproductid.length() != 0) {
			facade = getCachedViewedProduct("ProductListView", "ProductID",
					facadeproductid);
		} else {
			facadeproductid = getDefaultFacade(planproductid);
			if (facadeproductid != null && facadeproductid.length() != 0) {
				facade = getCachedViewedProduct("ProductListView", "ProductID",
						facadeproductid);
			} else {
				facade = new GenRow();
			}
		}

		GenRow land = getCachedViewedProduct("properties/ProductLotView",
				"ProductID", landproductid);

		String galleryID = KeyMaker.generate();
		String publisher = "House and Land Packages";

		int sortNumber = 0;
		ArrayList<String> documentIDs = new ArrayList<String>();

		String mainDocumentID = "";

		// put all the default images in
		if (facade.getString("Image").length() > 0
				&& facade.getString("ImageName").length() > 0
				&& !documentIDs.contains(facade.getString("Image"))) {
			addDocumentToGallery(productid, galleryID,
					facade.getString("Image"), userid, "" + sortNumber);
			++sortNumber;
			documentIDs.add(facade.getString("Image"));
			logger.debug("Adding Facade Image " + facade.getString("ImageName"));

		}
		if (plan.getString("Image").length() > 0
				&& plan.getString("ImageName").length() > 0
				&& !documentIDs.contains(plan.getString("Image"))) {
			addDocumentToGallery(productid, galleryID, plan.getString("Image"),
					userid, "" + sortNumber);
			++sortNumber;
			documentIDs.add(plan.getString("Image"));
			logger.debug("Adding Plan Image " + plan.getString("ImageName"));
		}
		if (house.getString("Image").length() > 0
				&& house.getString("ImageName").length() > 0
				&& !documentIDs.contains(house.getString("Image"))) {
			addDocumentToGallery(productid, galleryID,
					house.getString("Image"), userid, "" + sortNumber);
			++sortNumber;
			documentIDs.add(house.getString("Image"));
			logger.debug("Adding Design Image " + house.getString("ImageName"));
		}
		if (land.getString("Image").length() > 0
				&& land.getString("ImageName").length() > 0
				&& !documentIDs.contains(land.getString("Image"))) {
			addDocumentToGallery(productid, galleryID, land.getString("Image"),
					userid, "" + sortNumber);
			++sortNumber;
			documentIDs.add(land.getString("Image"));
			logger.debug("Adding Land Image " + land.getString("ImageName"));
		}
		if (mainDocumentID.length() == 0
				&& facade.getString("ImageName").length() > 0) {
			mainDocumentID = facade.getString("Image");
			logger.debug("Set Facade Image " + mainDocumentID);
		}
		if (mainDocumentID.length() == 0
				&& house.getString("ImageName").length() > 0) {
			mainDocumentID = house.getString("Image");
			logger.debug("Set Design Image " + mainDocumentID);
		}
		if (mainDocumentID.length() == 0
				&& plan.getString("ImageName").length() > 0) {
			mainDocumentID = plan.getString("Image");
			logger.debug("Set Plan Image " + mainDocumentID);
		}
		if (mainDocumentID.length() == 0
				&& land.getString("ImageName").length() > 0) {
			mainDocumentID = land.getString("Image");
			logger.debug("Set Land Image " + mainDocumentID);
		}

		boolean createAlbum = true;

		// remove all the old images and copy the new images
		// if (isRecreateTask() && !isPackageClaimed(productid)) {
		if (isRecreateTask()) {
			logger.debug("Deleting and recreating albums related to "
					+ productid);
			deleteAllExistingAlbums(productid);

		}/*
		 * else if (isRecreateTask() && isPackageClaimed(productid)) {
		 * logger.debug("Retaining albums related to " + productid); createAlbum
		 * = false; }
		 */

		if (createAlbum) {
			ArrayList<String> galleryIDs = getGalleryIDs(house
					.getString("ProductID"));

			if (galleryIDs.size() > 0) {
				for (int g = 0; g < galleryIDs.size(); ++g) {
					String thisGalleryID = null;

					GenRow gal = getCachedTabledProduct("Gallery", "GalleryID",
							galleryIDs.get(g));

					boolean HNLAlbum = true;
					if (gal.getString("Publisher").indexOf(
							"House and Land Packages") >= 0) {

						thisGalleryID = galleryID;
						publisher = mergePublishers(publisher,
								gal.getString("Publisher"));

					} else {
						// Copy all the non 'House and Land Packages' gallery
						HNLAlbum = false;

						thisGalleryID = KeyMaker.generate();
						gal.putAllDataAsParameters(gal);

						gal.setParameter("GalleryID", thisGalleryID);
						gal.setParameter("ProductID", productid);
						gal.setParameter("CreatedBy", userid);
						gal.setParameter("CreatedDate", "NOW");
						gal.setParameter("ModifiedBy", userid);
						gal.setParameter("ModifiedDate", "NOW");

						if (gal.getString("ProductID").length() > 0)
							gal.doAction(GenerationKeys.INSERT);
					}

					GenRow docs = new GenRow();
					docs.setViewSpec("LinkedDocumentView");
					docs.setConnection(this.getConnection());
					// docs.setParameter("ProductID",
					// house.getString("ProductID"));
					docs.setParameter("GalleryID", galleryIDs.get(g));
					docs.sortBy("ShowOnGalleryOrder", 0);
					docs.sortBy("SortOrder", 1);
					docs.doAction("search");

					docs.getResults();

					if (!HNLAlbum) {
						// For a non HNL album, reserve 0 & 1 for Hero & Plan
						// respectively
						sortNumber = 2;
					}
					while (docs.getNext()) {
						if (HNLAlbum) {
							if (docs.getString("DocumentID").length() > 0
									&& !documentIDs.contains(docs
											.getString("DocumentID"))) {
								addDocumentToGallery(productid, thisGalleryID,
										docs.getString("DocumentID"), userid,
										"" + sortNumber);
								++sortNumber;
								documentIDs.add(docs.getString("DocumentID"));
							}
						} else {
							addDocumentToGallery(null, thisGalleryID,
									docs.getString("DocumentID"), userid, ""
											+ sortNumber);
							++sortNumber;
						}
					}

					docs.close();
				}
			}

			galleryIDs = getGalleryIDs(land.getString("ProductID"));

			if (galleryIDs.size() > 0) {
				for (int g = 0; g < galleryIDs.size(); ++g) {
					GenRow gal = getCachedTabledProduct("Gallery", "GalleryID",
							galleryIDs.get(g));
					if (gal.getString("Publisher").indexOf(
							"House and Land Packages") >= 0) {
						publisher = mergePublishers(publisher,
								gal.getString("Publisher"));
						GenRow docs = new GenRow();
						docs.setViewSpec("LinkedDocumentView");
						docs.setConnection(this.getConnection());
						// docs.setParameter("ProductID",
						// house.getString("ProductID"));
						docs.setParameter("GalleryID", galleryIDs.get(g));
						docs.sortBy("ShowOnGalleryOrder", 0);
						docs.sortBy("SortOrder", 1);
						docs.doAction("search");

						docs.getResults();

						while (docs.getNext()) {
							if (docs.getString("DocumentID").length() > 0
									&& !documentIDs.contains(docs
											.getString("DocumentID"))) {
								addDocumentToGallery(productid, galleryID,
										docs.getString("DocumentID"), userid,
										"" + sortNumber);
								++sortNumber;
								documentIDs.add(docs.getString("DocumentID"));
							}
						}

						docs.close();
					}
				}
			}

			GenRow row = new GenRow();
			row.setTableSpec("Gallery");
			row.setConnection(this.getConnection());
			row.setParameter("GalleryID", galleryID);
			row.setParameter("ProductID", productid);
			row.setParameter("GalleryName", "Package Gallery");
			row.setParameter("Description",
					"This gallery was created for the package.");
			row.setParameter("Active", "Y");
			row.setParameter("Publisher", publisher);
			row.setParameter("CreatedBy", userid);
			row.setParameter("CreatedDate", "NOW");
			row.setParameter("ModifiedBy", userid);
			row.setParameter("ModifiedDate", "NOW");

			if (row.getString("ProductID").length() > 0)
				row.doAction(GenerationKeys.INSERT);
		}

		return mainDocumentID;
	}

	public String recreateAlbum(String productid, String planproductid,
			String facadeproductid, String landproductid, String userid,
			boolean refresh) {
		GenRow plan = getCachedViewedProduct("properties/ProductPlanView",
				"ProductID", planproductid);
		GenRow house = getCachedViewedProduct("properties/ProductHomeView",
				"ProductID", plan.getString("HomeProductID"));

		GenRow facade = null;
		if (facadeproductid != null && facadeproductid.length() != 0) {
			facade = getCachedViewedProduct("ProductListView", "ProductID",
					facadeproductid);
		} else {
			facade = new GenRow();
		}

		GenRow land = getCachedViewedProduct("properties/ProductLotView",
				"ProductID", landproductid);

		String mainDocumentID = "";

		if (mainDocumentID.length() == 0
				&& facade.getString("ImageName").length() > 0)
			mainDocumentID = facade.getString("Image");
		if (mainDocumentID.length() == 0
				&& house.getString("ImageName").length() > 0)
			mainDocumentID = house.getString("Image");
		if (mainDocumentID.length() == 0
				&& plan.getString("ImageName").length() > 0)
			mainDocumentID = plan.getString("Image");
		if (mainDocumentID.length() == 0
				&& land.getString("ImageName").length() > 0)
			mainDocumentID = land.getString("Image");

		return mainDocumentID;
	}

	private void deleteAllExistingAlbums(String productid) {
		ArrayList<String> galleryIDs = getGalleryIDs(productid);

		GenRow row = new GenRow();
		row.setTableSpec("LinkedDocuments");
		row.setConnection(this.getConnection());

		for (String galleryID : galleryIDs) {
			row.setParameter("ON-GalleryID", galleryID);
			row.doAction("deleteall");

			// delete the gallery itself
			GenRow gallery = new GenRow();
			gallery.setViewSpec("GalleryView");
			gallery.setParameter("GalleryID", galleryID);
			gallery.doAction("delete");
		}
	}

	protected String mergePublishers(String a, String b) {
		String[] parts = b.split("\\+");

		for (int p = 0; p < parts.length; ++p) {
			if (parts[p].length() > 0) {
				if (a.indexOf(parts[p]) == -1) {
					if (a.length() > 0)
						a += "+";
					a += parts[p];
				}
			}
		}

		return a;
	}

	protected void addDocumentToGallery(String productID, String galleryID,
			String documentID, String userID, String sortNumber) {
		GenRow row = new GenRow();
		row.setTableSpec("LinkedDocuments");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", productID);
		row.setParameter("GalleryID", galleryID);
		row.setParameter("DocumentID", documentID);
		row.setParameter("CreatedBy", userID);
		row.setParameter("CreatedDate", "NOW");
		row.setParameter("ShowOnGalleryOnly", "Y");
		row.setParameter("ShowOnGalleryOrder", sortNumber);
		row.setParameter("ShowOnREorder", sortNumber);
		row.setParameter("SortOrder", sortNumber);
		row.setParameter("LinkedDocumentID", KeyMaker.generate());

		row.doAction("insert");
	}

	public String creatProductStatus(String productID, String userID) {
		java.util.Properties sysprops = InitServlet.getSystemParams();
		String statusID = sysprops.getProperty("ProductUnapprovedStatus");
		if (this.statusID != null && this.statusID.length() > 0)
			statusID = this.statusID;
		if (statusID == null || statusID.length() == 0)
			return "";

		String key = KeyMaker.generate();

		GenRow row = new GenRow();
		row.setTableSpec("ProductStatus");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", productID);
		row.setParameter("StatusID", statusID);
		row.setParameter("CreatedBy", userID);
		row.setParameter("CreatedDate", "NOW");
		row.setParameter("ProductStatusID", key);
		row.doAction("insert");

		return key;
	}

	/**
	 * This is the standard call for creating a house and land product, every
	 * thing is taken from the components
	 * 
	 * @param houseproductid
	 * @param landproductid
	 * @param facadeproductid
	 * @param userid
	 * @return
	 */
	public String createHouseAndLandPackage(String houseproductid,
			String landproductid, String facadeproductid, String userid) {
		// we load the land up so we can check any package rules
		if (!checkRules(landproductid, houseproductid, facadeproductid))
			return "";

		return createHouseAndLandPackage(houseproductid, landproductid,
				facadeproductid, null, null, null, userid);
	}

	/**
	 * This call is used to create a package without rules being applied, you
	 * can also override the pricing of parts of the package
	 * 
	 * @param houseproductid
	 * @param landproductid
	 * @param facadeproductid
	 * @param housePrice
	 * @param landPrice
	 * @param halPrice
	 * @param userid
	 * @return
	 */
	public String createHouseAndLandPackage(String houseproductid,
			String landproductid, String facadeproductid, String housePrice,
			String landPrice, String halPrice, String userid) {
		GenRow land = getCachedViewedProduct("properties/ProductLotView",
				"ProductID", landproductid);
		
		String displayPriceType = "";

		String copiedfromid = houseproductid;
		// check the rules
		GenRow bean = new GenRow();
		bean.setTableSpec("ProductProducts");
		bean.setConnection(this.getConnection());

		String housenandlandproductid = KeyMaker.generate();

		GenRow landLocation = getCachedTabledProduct("Products", "ProductID",
				landproductid);

		GenRow searchProducts = getCachedTabledProduct("Products", "ProductID",
				houseproductid);
		String searchProductProductType = searchProducts
				.getString("ProductType");
		if (searchProductProductType != null
				&& !"".equals(searchProductProductType)
				&& "Home Plan".equals(searchProductProductType)) {
			// String originalHomeID = houseproductid;
			houseproductid = duplicateplan(houseproductid, userid);
			
			// clear the price from the Package so if it dies the price will be 0
			//updatePrice(houseproductid, "0");
			
			// copyDrips(originalHomeID, houseproductid, null, userid);
			if ("true".equals(InitServlet
					.getSystemParam("RegionPricingEnabled"))) {
				String regionID = land.getString("RegionID");
				// TODO add code for LocationID
				if (regionID.length() > 0) {
					updateRegionPricing(copiedfromid, houseproductid, regionID,
							searchProducts.getDouble("TotalCost"));
				}
			}
		}

		copy("Products", "ProductID", houseproductid, housenandlandproductid,
				userid);
		updatePrice(housenandlandproductid, "0");
		String mainImage = createAlbum(housenandlandproductid, copiedfromid,
				facadeproductid, landproductid, userid, false);
		String dPriceType = setProductImageAndName(housenandlandproductid, copiedfromid,
				facadeproductid, landproductid, mainImage);
		insertProductProduct(housenandlandproductid, facadeproductid, userid);

		copy("answers/ProductAnswers", "ProductID", landproductid,
				housenandlandproductid, userid);
		copy("LinkedDocuments", "ProductID", copiedfromid,
				housenandlandproductid, userid);
		// the following only linked
		// copy("ProductProducts", "ProductID", houseproductid,
		// housenandlandproductid, userid);

		copy("answers/ProductAnswers", "ProductID", copiedfromid,
				housenandlandproductid, userid);
		copy("LinkedDocuments", "ProductID", landproductid,
				housenandlandproductid, userid);
		// the following only linked
		// copy("ProductProducts", "ProductID", landproductid,
		// housenandlandproductid, userid);

		// costings will depend on the campaign, so lets do that first
		updateCampaign(landproductid, housenandlandproductid);

		// need to copy any costing attached to the Plan and Lot to the House
		// and Land, the rest are just linked
		copyCostings(copiedfromid, housenandlandproductid, userid);
		// if the land has an override then use that
		if (packageCostLibraryID != null && packageCostLibraryID.length() > 0) {
			copyCostingsLibrary(packageCostLibraryID, housenandlandproductid,
					userid);
			// we need to copy DRIPs
			copyDrips(landproductid, housenandlandproductid, "Land", userid);
		} else {
			if ("true".equals(InitServlet.getSystemParam("HAL-CopyCosts"))) {
				copyCostings(land.getString("EstateProductID"),
						housenandlandproductid, userid);
				copyCostings(land.getString("StageProductID"),
						housenandlandproductid, userid);
			}
			copyCostings(landproductid, housenandlandproductid, userid);
		}

		// new we need to drip from Stage, Estate, Range, Home
		copyDrips(land.getString("EstateProductID"), housenandlandproductid,
				"Land", userid);
		copyDrips(land.getString("StageProductID"), housenandlandproductid,
				"Land", userid);
		copyDrips(landproductid, housenandlandproductid, "Land", userid);
		GenRow home = getCachedViewedProduct("properties/ProductPlanView",
				"ProductID", houseproductid);
		copyDrips(home.getString("RangeProductID"), housenandlandproductid,
				"House", userid);
		copyDrips(home.getString("HomeProductID"), housenandlandproductid,
				"House", userid);
		copyDrips(copiedfromid, housenandlandproductid, "House", userid);

		copyGlobalDrips(housenandlandproductid, "Global", userid);

		copy("Addresses", "ProductID", landproductid, housenandlandproductid,
				userid);
		
		copyLinkedProducts(copiedfromid, houseproductid, "Home Option", userid);
		copyLinksOnly(houseproductid, housenandlandproductid, "Home Option", userid);
		copyLinkedProducts(home.getString("HomeProductID"), houseproductid, "Home Option", userid);
		copyLinkedProducts(home.getString("RangeProductID"), houseproductid, "Home Option", userid);
		copyLinkedProducts(copiedfromid, houseproductid, "Plan Feature", userid);
		copyLinksOnly(houseproductid, housenandlandproductid, "Plan Feature", userid);
		
		if (includeBuilderGroups && StringUtils.isNotBlank(home.getString("BuilderProductID"))) {
			copy("ProductSecurityGroups", "ProductID", home.getString("BuilderProductID"), housenandlandproductid, userid);
		}
		if (includeBrandGroups && StringUtils.isNotBlank(home.getString("BrandProductID"))) {
			copy("ProductSecurityGroups", "ProductID", home.getString("BrandProductID"), housenandlandproductid, userid);
		}
		if (includeRangeGroups && StringUtils.isNotBlank(home.getString("RangeProductID"))) {
			copy("ProductSecurityGroups", "ProductID", home.getString("RangeProductID"),  housenandlandproductid, userid);
		}
		if (includeDesignGroups && StringUtils.isNotBlank(home.getString("HomeProductID"))) {
			copy("ProductSecurityGroups", "ProductID", home.getString("HomeProductID"),  housenandlandproductid, userid);
		}
		if (includePlanGroups && StringUtils.isNotBlank(copiedfromid)) {
			copy("ProductSecurityGroups", "ProductID", copiedfromid,  housenandlandproductid, userid);
		}
		if (includeDeveloperGroups && StringUtils.isNotBlank(land.getString("DeveloperProductID"))) {
			copy("ProductSecurityGroups", "ProductID", land.getString("DeveloperProductID"), housenandlandproductid, userid);
		}
		if (includeEstateGroups && StringUtils.isNotBlank(land.getString("EstateProductID"))) {
			copy("ProductSecurityGroups", "ProductID", land.getString("EstateProductID"), housenandlandproductid, userid);
		}
		if (includeStageGroups && StringUtils.isNotBlank(land.getString("StageProductID"))) {
			copy("ProductSecurityGroups", "ProductID", land.getString("StageProductID"), housenandlandproductid, userid);
		}
		if (includeLandGroups && StringUtils.isNotBlank(landproductid)) {
			copy("ProductSecurityGroups", "ProductID", landproductid, housenandlandproductid, userid);
		}
		
		copy("properties/HomeDetails", "ProductID", houseproductid,
				housenandlandproductid, userid);
		update("properties/HomeDetails", "ProductID", landproductid,
				housenandlandproductid, userid);

		// copy the new details to the house and land
		copy("properties/ProductDetails", "ProductID", landproductid,
				housenandlandproductid, userid);
		copy("properties/ProductDetails", "ProductID", copiedfromid,
				housenandlandproductid, userid);
		copy("properties/ProductDetails", "ProductID", facadeproductid,
				housenandlandproductid, userid);

		// copy("ProductAnswers","ProductID",copiedfromid,
		// housenandlandproductid,userid);

		// copy product commissions and payment templates from plan and land
		copyCommissions("ProductCommissions", "ProductID", landproductid,
				housenandlandproductid, userid);
		copyCommissions("ProductCommissions", "ProductID", houseproductid,
				housenandlandproductid, userid);

		String productstatusid = creatProductStatus(housenandlandproductid,
				userid);
		if (productstatusid == null || productstatusid.length() == 0) {
			productstatusid = copy("ProductStatus", "ProductID", landproductid,
					housenandlandproductid, userid);

		}
		updateProductStatus(housenandlandproductid, productstatusid);
		updateAvailableDate(housenandlandproductid);
		updateSecurityGroup(housenandlandproductid);

		bean.setParameter("ProductID", housenandlandproductid);
		bean.setParameter("LinkedProductID", houseproductid);
		bean.setParameter("CreatedDate", "NOW");
		bean.setParameter("ModifiedDate", "NOW");
		bean.setParameter("CreatedBy", userid);
		bean.setParameter("ModifiedBy", userid);

		bean.setParameter("ProductProductID", KeyMaker.generate());
		bean.doAction("insert");

		bean.setConnection(this.getConnection());
		bean.setParameter("LinkedProductID", landproductid);
		bean.setParameter("ProductProductID", KeyMaker.generate());
		bean.doAction("insert");
		
		updateLocation(housenandlandproductid,
				landLocation.getString("LocationID"),
				landLocation.getString("RegionID"));
		
		removeCachedProduct("ProductHouseAndLandView", "ProductID", housenandlandproductid);

		if (housePrice != null && housePrice.length() > 0) {
			updatePrice(houseproductid, housePrice);
		}
		
		//String displayPrice = getCachedTabledProduct("Products", "ProductID", housenandlandproductid).getString("DisplayPriceType");

		updatePackageCost(houseproductid, landproductid, housenandlandproductid);

		updateProductCost(housenandlandproductid, dPriceType);
		if (home.getString("ProductNum").replaceAll("[0-9]", "").length() == 0 &&
				!"true".equals(InitServlet.getSystemParam("RPM-KeepProductNum")))
			updateProductNumber(housenandlandproductid);

		if (halPrice != null && halPrice.length() > 0) {
			updatePrice(housenandlandproductid, halPrice, dPriceType);
		}

		// updatePublishFields(home.getString("HomeProductID"),
		// housenandlandproductid);

		return (housenandlandproductid);
	}
	
	private void updatePrice(String productID, String price) {
		updatePrice(productID, price, "");
	}
	private void updatePrice(String productID, String price, String displayPriceType) {
		try {
			double t = Double.parseDouble(price);
			double c = t / 1.1;
			double g = t - c;

			GenRow row = new GenRow();
			row.setTableSpec("Products");
			row.setConnection(getConnection());
			row.setParameter("ProductID", productID);
			row.setParameter("GST", g);
			row.setParameter("Cost", c);
			row.setParameter("TotalCost", t);
			
			if (StringUtils.isNotBlank(displayPriceType)) {
				row.setParameter("DisplayPrice", RunwayUtil.displayPrice(displayPriceType, t));
			}
			
			row.doAction("update");
		} catch (Exception e) {
		}
	}

	/**
	 * Updates Publish fields from Home plan into H&L package and Publish table
	 * 
	 * @param hnlProductid
	 * @param planProductid
	 * @param userid
	 */
	public void updatePublishFields(String hnlProductid, String planProductid,
			String heroImageID, String userid) {
		GenRow hnl = getCachedViewedProduct(
				"properties/ProductHouseAndLandView", "ProductID", hnlProductid);
		GenRow plan = getCachedViewedProduct("properties/ProductPlanView",
				"ProductID", planProductid);

		// if (hnl.isSuccessful() &&
		// "Approved".equals(hnl.getString("PackageStatus")) &&
		// !isPackageClaimed(productid)) {
		if (plan.isSuccessful() && hnl.isSuccessful()
				&& "Approved".equals(hnl.getString("PackageStatus"))) {
			GenRow pp = new GenRow();
			pp.setViewSpec("properties/ProductPublishingView");
			pp.setConnection(getConnection());

			boolean update = false;
			
			String headline = "true".equals(InitServlet.getSystemParam("RPM-RetainPackageDescription"))? hnl.getString("Summary") : plan.getString("Summary");
			if (updatePublishHeadline && StringUtils.isNotBlank(plan.getString("Summary"))) {
				update = true;
				pp.setParameter("Summary", headline);
				hnl.setParameter("Summary", headline);
			}
			
			String pubDesc = "true".equals(InitServlet.getSystemParam("RPM-RetainPackageDescription"))? hnl.getString("PublishDescription") : plan.getString("PublishDescription");
			if (updatePublishDescription && StringUtils.isNotBlank(plan .getString("PublishDescription"))) {
				update = true;
				pp.setParameter("Description", pubDesc);
				hnl.setParameter("PublishDescription", pubDesc);
			}

			if (updateImages && StringUtils.isNotBlank(heroImageID)) {
				update = true;
				pp.setParameter("HeroImageID", heroImageID);
				hnl.setParameter("Image", heroImageID);
			}

			if (update) {
				pp.setParameter("ModifiedBy", userid);
				pp.setParameter("ModifiedDate", "NOW");
				pp.setParameter("ON-ProductID", hnlProductid);
				pp.doAction("updateall");

				hnl.setParameter("ModifiedBy", userid);
				hnl.setParameter("ModifiedDate", "NOW");

				hnl.doAction("update");
			}

			pp.close();
		}
	}

	public String getFacade(String productid) {
		GenRow row = new GenRow();
		row.setConnection(this.getConnection());
		row.setViewSpec("ProductLinkedProductView");
		row.setColumn("ProductID", productid);
		row.setColumn("ProductProductsLinkedProduct.ProductType", "Home Facade");
		row.sortBy("ProductProductsLinkedProduct.Name", 0);
		if (productid != null && productid.length() != 0) {
			row.doAction("selectfirst");
		}
		return (row.getString("LinkedProductID"));
	}

	public String getDefaultFacade(String planProductID) {
		String homeProductID = getHomeProductID(planProductID);

		if (homeProductID.length() == 0)
			return "";

		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("ProductLinkedProductView");
		row.setColumn("ProductID", planProductID);
		row.setColumn("ProductProductsLinkedProduct.ProductType", "Home Facade");
		row.setColumn("ProductType", "Home Facade");
		row.setColumn("Active", "Y");
		row.setColumn("ProductProductsLinkedProduct.Active", "Y");
		row.sortBy("IsExclusive", 0);
		row.sortOrder("DESC", 0);
		row.sortBy("SortOrder", 1);
		row.sortBy("ProductProductsLinkedProduct.TotalCost", 2);
		row.sortBy("ProductProductsLinkedProduct.Cost", 3);
		row.doAction("selectfirst");
		
		System.out.println("Query 1 " + row.getStatement());

		if ("Y".equals(row.getString("IsExclusive"))
				&& row.getString("LinkedProductID").length() > 0)
			return row.getString("LinkedProductID");

		String planFacadeID = row.getString("LinkedProductID");

		row.setColumn("ProductID", homeProductID);
		row.doAction("selectfirst");

		System.out.println("Query 2 " + row.getStatement());

		if ("Y".equals(row.getString("IsExclusive"))
				&& row.getString("LinkedProductID").length() > 0)
			return row.getString("LinkedProductID");

		System.out.println("Using " + planFacadeID);
		if (planFacadeID.length() > 0) return planFacadeID;

		System.out.println("Using " + row.getString("LinkedProductID"));
		return row.getString("LinkedProductID");
	}

	public String getHomeProductID(String planProductID) {
		GenRow row = getCachedViewedProduct("properties/ProductPlanView",
				"ProductID", planProductID);

		return row.getString("HomeProductID");
	}

	public String resetContents(String houseproductid, String copiedfromid,
			String landproductid, String homedesignid,
			String housenandlandproductid, String userid) {
		return resetContents(houseproductid, copiedfromid, landproductid,
				homedesignid, housenandlandproductid, userid,
				new StringBuilder(), System.currentTimeMillis());

	}

	public String resetContents(String houseproductid, String copiedfromid,
			String landproductid, String homedesignid,
			String housenandlandproductid, String userid,
			StringBuilder logStep, long mil) {
		// Make sure records exist for all publish methods before we proceed
		GenRow pp = new GenRow();
		pp.setViewSpec("properties/ProductPublishingView");
		pp.setConnection(getConnection());
		for (PublishMethod pm : PublishMethod.values()) {
			pp.clear();
			pp.setColumn("ProductID", housenandlandproductid);
			pp.setColumn("PublishMethod", pm.toString());
			pp.doAction("selectfirst");

			if (!pp.isSuccessful()) {
				insertProductPublishing(getConnection(),
						housenandlandproductid, pm.getName(), userid);
			}
		}
		pp.close();

		String facadeproductid = getFacade(housenandlandproductid);
		String mainImageID = "";

		currentStep.addAndGet(1);
		
		String dPriceType = "";
		logStep.append(",").append(System.currentTimeMillis() - mil); // 15
		if (updateImages) {
			deleteAllExistingLinkedDocuments(houseproductid);
			currentStep.addAndGet(1);
			logStep.append(",").append(System.currentTimeMillis() - mil); // 16
			deleteAllExistingLinkedDocuments(housenandlandproductid);
			currentStep.addAndGet(1);
			logStep.append(",").append(System.currentTimeMillis() - mil); // 17

			mainImageID = createAlbum(housenandlandproductid, houseproductid,
					facadeproductid, landproductid, userid, true);
			dPriceType = setProductImageAndName(housenandlandproductid, houseproductid,
					facadeproductid, landproductid, mainImageID);
			currentStep.addAndGet(1);
			logStep.append(",").append(System.currentTimeMillis() - mil); // 18

			copy("LinkedDocuments", "ProductID", copiedfromid, houseproductid,
					userid);
			currentStep.addAndGet(1);
			logStep.append(",").append(System.currentTimeMillis() - mil); // 19

			copy("LinkedDocuments", "ProductID", houseproductid,
					housenandlandproductid, userid);
			currentStep.addAndGet(1);
			logStep.append(",").append(System.currentTimeMillis() - mil); // 20

			copy("LinkedDocuments", "ProductID", landproductid,
					housenandlandproductid, userid);
			currentStep.addAndGet(1);
			logStep.append(",").append(System.currentTimeMillis() - mil); // 21

		} else {
			currentStep.addAndGet(9);
			logStep.append(",").append(System.currentTimeMillis() - mil); // 22
		}

		update("Addresses", "ProductID", landproductid, housenandlandproductid,
				userid);
		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 23

		copy("answers/ProductAnswers", "ProductID", landproductid,
				housenandlandproductid, userid);
		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 24

		copy("answers/ProductAnswers", "ProductID", houseproductid,
				housenandlandproductid, userid);

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 25

		updatePublishFields(housenandlandproductid, copiedfromid, mainImageID,
				userid);

		for (PublishMethod pm : PublishMethod.values())
			checkPublishAlbums(this.getConnection(), housenandlandproductid,
					mainImageID, pm, userid);

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 26

		return housenandlandproductid;
	}

	/**
	 * Takes the campaignID applied to the land product and updates the house
	 * and land product with it. Campaign from land level will override the hnl
	 * level campaign, if applied.
	 */
	private void updateCampaign(String landproductid,
			String housenandlandproductid) {
		GenRow g = new GenRow();
		g.setConnection(getConnection());
		g.setViewSpec("CampaignProductView");
		g.setParameter("ProductID", landproductid);
		if (g.isSet("ProductID")) {
			g.doAction(GenerationKeys.SELECTFIRST);
		}
		final String campaignID = g.isSuccessful() ? g.getData("CampaignID")
				: null;
		g.clear();
		g.setParameter("ProductID", housenandlandproductid);
		if (g.isSet("ProductID")) {
			g.doAction(GenerationKeys.SELECTFIRST);
		}
		if (g.isSuccessful()) {
			if (campaignID != null) {
				g.setParameter("CampaignID", campaignID);
				g.doAction(GenerationKeys.UPDATE);
			} else if (g.getString("CampaignID").length() > 0) {
				copyCampaignDrips(g.getString("CampaignID"),
						housenandlandproductid);
			}
			/*
			 * do nothing if the land product has no campaign
			 * g.setAction(campaignID != null ? GenerationKeys.UPDATE :
			 * GenerationKeys.DELETE); g.doAction();
			 */
		} else if (campaignID != null) {
			g.setToNewID("CampaignProductID");
			g.setParameter("CampaignID", campaignID);
			g.doAction(GenerationKeys.INSERT);
		}
		if (campaignID != null && campaignID.length() > 0) {
			copyCampaignDrips(campaignID, housenandlandproductid);
		}
	}

	private void copyCampaignDrips(String campaignID,
			String housenandlandproductid) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(getConnection());
		row.setParameter("-select0", "ProductID");
		row.setParameter("ProductType", "Drip Library");
		row.setParameter("ProductPackageCostings.ApplyToID", campaignID);
		row.setParameter("-groupby0", "ProductID");
		row.doAction("search");

		row.getResults();

		while (row.getNext()) {

			if (row.getString("ProductID").length() > 0)
				copyRawDrips(row.getString("ProductID"),
						housenandlandproductid, userid);
		}

		row.close();
	}

	public String refreshHouseAndLandPackage(String houseproductid,
			String copiedfromid, String landproductid, String homedesignid,
			String housenandlandproductid, String userid) {
		
		String displayPriceType = "";

		long mil = System.currentTimeMillis();
		StringBuilder logStep = new StringBuilder(housenandlandproductid)
				.append(",").append(copiedfromid).append(",")
				.append(landproductid);

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 3

		String facadeproductid = getFacade(housenandlandproductid);
		if (!keepFacade || facadeproductid.length() == 0)
			facadeproductid = getDefaultFacade(copiedfromid);

		if (!checkRules(landproductid, copiedfromid, facadeproductid)) {
			return "";
		}

		displayPriceType = getCachedTabledProduct("Products", "ProductID", housenandlandproductid).getString("DisplayPriceType");
		
		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 1

		// we load the land up so we can check any package rules
		GenRow land = getCachedViewedProduct("properties/ProductLotView",
				"ProductID", landproductid);
		
		if (StringUtils.isBlank(displayPriceType)) displayPriceType = land.getString("DisplayPriceType");

		// check the rules
		// if (!checkRules(land, landproductid, houseproductid))
		// return "";
		/*
		 * GenRow bean = new GenRow(); bean.setTableSpec("ProductProducts");
		 * bean.setConnection(this.getConnection());
		 */
		// GenRow landLocation = getCachedTabledProduct("Products", "ProductID",
		// landproductid);

		/* get the plan that is duplicated in the hnl product */
		GenRow copiedPlanProduct = getCachedTabledProduct("Products",
				"ProductID", copiedfromid); /* get home plan */
		if ("Home Plan".equals(copiedPlanProduct.getString("ProductType"))) {
			/*
			 * if we find the duplicated plan, then run the duplicate plan
			 * action
			 */
			houseproductid = duplicateplan(copiedfromid, houseproductid,
					userid, false);
			if ("true".equals(InitServlet
					.getSystemParam("RegionPricingEnabled"))) {
				String regionID = land.getString("RegionID");
				// TODO add code for LocationID
				if (regionID.length() > 0) {
					updateRegionPricing(copiedfromid, houseproductid, regionID,
							copiedPlanProduct.getDouble("TotalCost"));
				}
			}
		}

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 2

		update("Products", "ProductID", houseproductid, housenandlandproductid,
				userid, false);
		
		if (updatePrice) {
			// clear the price from the Package so if it dies the price will be 0
			updatePrice(housenandlandproductid, "0");
		}

		// deleteAllExistingLinkedDocuments(housenandlandproductid);

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 4
		String mainImage = recreateAlbum(housenandlandproductid,
				houseproductid, facadeproductid, landproductid, userid, true);

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 5
		setProductImageAndName(housenandlandproductid, houseproductid,
				facadeproductid, landproductid, mainImage);

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 6
		deleteProductProduct(housenandlandproductid,
				getFacade(housenandlandproductid));
		insertProductProduct(housenandlandproductid, facadeproductid, userid);
		/*
		 * String mainImage = createAlbum(housenandlandproductid,
		 * houseproductid, facadeproductid, landproductid, userid);
		 * setProductImageAndName(housenandlandproductid, houseproductid,
		 * facadeproductid, landproductid, mainImage);
		 * insertProductProduct(housenandlandproductid, facadeproductid,
		 * userid);
		 */
		// copy("answers/ProductAnswers", "ProductID", houseproductid,
		// housenandlandproductid, userid);
		// copy("LinkedDocuments", "ProductID", houseproductid,
		// housenandlandproductid, userid);
		// the following only linked
		// copy("ProductProducts", "ProductID", houseproductid,
		// housenandlandproductid, userid);

		// copy("answers/ProductAnswers", "ProductID", landproductid,
		// housenandlandproductid, userid);
		// copy("LinkedDocuments", "ProductID", landproductid,
		// housenandlandproductid, userid);
		// the following only linked
		// copy("ProductProducts", "ProductID", landproductid,
		// housenandlandproductid, userid);

		// need to copy any costing attached to the Plan and Lot to the House
		// and Land, the rest are just linked
		// make sure we remove the old ones first
		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 7
		if (updateDims) {
			update("properties/HomeDetails", "ProductID", copiedfromid,
					housenandlandproductid, userid, true);
			update("properties/HomeDetails", "ProductID", landproductid,
					housenandlandproductid, userid, false);

			GenRow delete = new GenRow();
			delete.setTableSpec("properties/ProductDetails");
			delete.setConnection(getConnection());
			delete.setParameter("ON-ProductID", housenandlandproductid);
			delete.doAction("deleteall");

			// copy the new details to the house and land
			copy("properties/ProductDetails", "ProductID", landproductid,
					housenandlandproductid, userid);
			copy("properties/ProductDetails", "ProductID", copiedfromid,
					housenandlandproductid, userid);
			copy("properties/ProductDetails", "ProductID", facadeproductid,
					housenandlandproductid, userid);
		}

		// copy product commissions and payment templates from plan and land
		copyCommissions("ProductCommissions", "ProductID", landproductid,
				housenandlandproductid, userid);
		copyCommissions("ProductCommissions", "ProductID", houseproductid,
				housenandlandproductid, userid);

		if (updateAnswers) {
			GenRow delete = new GenRow();
			delete.setTableSpec("ProductAnswers");
			delete.setConnection(getConnection());
			delete.setParameter("ON-ProductID", housenandlandproductid);
			delete.doAction("deleteall");

			// copy("ProductAnswers","ProductID",landproductid,
			// housenandlandproductid,userid);
			copy("ProductAnswers", "ProductID", copiedfromid,
					housenandlandproductid, userid);
		}

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 8
		if (updateCosts) {
			removeProductProduct(housenandlandproductid, "Package Cost+Package Cost Library");
			copyCostings(copiedfromid, housenandlandproductid, userid);
			if ("true".equals(InitServlet.getSystemParam("HAL-CopyCosts"))) {
				copyCostings(land.getString("EstateProductID"),
						housenandlandproductid, userid);
				copyCostings(land.getString("StageProductID"),
						housenandlandproductid, userid);
			}
			copyCostings(landproductid, housenandlandproductid, userid);
		}

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 9
		if (updateDrips) {
			removeProductProduct(housenandlandproductid, "Drip+Drip Library");
			// new we need to drip from Stage, Estate, Range, Home
			copyDrips(land.getString("EstateProductID"),
					housenandlandproductid, "Land", userid);
			copyDrips(land.getString("StageProductID"), housenandlandproductid,
					"Land", userid);
			copyDrips(landproductid, housenandlandproductid, "Land", userid);

			GenRow home = getCachedViewedProduct("properties/ProductPlanView",
					"ProductID", houseproductid);
			copyDrips(home.getString("RangeProductID"), housenandlandproductid,
					"House", userid);
			copyDrips(home.getString("HomeProductID"), housenandlandproductid,
					"House", userid);
			copyDrips(copiedfromid, housenandlandproductid, "House", userid);
			copyGlobalDrips(housenandlandproductid, "Global", userid);
		}

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 10
		if (!keepRep) {
			removeProductGroups(housenandlandproductid);
		}

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 11
		updateCampaign(landproductid, housenandlandproductid);

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 12
		if (updatePrice) {
			if (updateCosts) {
				updatePackageCost(houseproductid, landproductid,
						housenandlandproductid);
			}

			updateProductCost(housenandlandproductid, displayPriceType);
			
			// Clear it so we get freshly modified values
			removeCachedProduct("Products", "ProductID",housenandlandproductid);
			GenRow hal = getCachedTabledProduct("Products", "ProductID",
					housenandlandproductid);
			try {
				if (hal.getDouble("TotalCost") > 0) {
					double totalCost = hal.getDouble("TotalCost");
					double dripCost = hal.getDouble("DripCost");
					GenRow update = new GenRow();
					update.setTableSpec("Products");
					update.setConnection(getConnection());
					update.setParameter("ProductID", hal.getString("ProductID"));
					update.setParameter("DripResult",
							String.valueOf(totalCost + dripCost));

					if (StringUtils.isNotBlank(displayPriceType)) {
						update.setParameter("DisplayPrice", RunwayUtil.displayPrice(displayPriceType, totalCost + dripCost));
						update.setParameter("DisplayPriceType", displayPriceType);
					}
					
					update.doAction("update");
				}
			} catch (Exception e) {
				logger.trace("error in {} of {}", "RefreshHouseAndLandPackage",
						e.getMessage());
			}
		}
		updateLocation(housenandlandproductid, land.getString("LocationID"),
				land.getString("RegionID"));

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 13
		setProductName(housenandlandproductid, houseproductid, landproductid);

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 14
		resetContents(houseproductid, copiedfromid, landproductid,
				homedesignid, housenandlandproductid, userid, logStep, mil);

		currentStep.addAndGet(1);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 15
		// this plan sale is only required once;
		removeCachedProduct("properties/ProductPlanView", "ProductID",
				houseproductid);
		logStep.append(",").append(System.currentTimeMillis() - mil); // 16

		log.append(logStep).append("\n");
		return (housenandlandproductid);
	}

	private void updateRegionPricing(String copiedfromid,
			String houseproductid, String regionID, double currentPrice) {
		GenRow prices = new GenRow();
		prices.setTableSpec("ProductRegionPrice");
		prices.setParameter("-select0", "*");
		prices.setParameter("ProductID", copiedfromid);
		prices.setParameter("RegionID", regionID);
		prices.doAction("selectfirst");

		double totalCost = 0;
		if (prices.getDouble("Percentage") != 0) {
			totalCost = currentPrice
					+ (currentPrice * prices.getDouble("Percentage"));
		} else if (prices.getDouble("Margin") != 0) {
			totalCost = currentPrice + prices.getDouble("Margin");
		} else if (prices.getDouble("TotalCost") != 0) {
			totalCost = prices.getDouble("TotalCost");
		} else {
			totalCost = currentPrice;
		}

		prices.clear();
		prices.setTableSpec("Products");
		prices.setParameter("ProductID", houseproductid);
		prices.setParameter("TotalCost", "" + totalCost);
		prices.setParameter("Cost", "" + (totalCost - (totalCost * 0.1)));
		prices.setParameter("GST", "" + (totalCost * 0.1));
		prices.doAction("update");
	}

	private void deleteAllExistingLinkedDocuments(String productid) {
		GenRow row = new GenRow();
		row.setTableSpec("LinkedDocuments");
		row.setConnection(this.getConnection());
		row.setParameter("ON-ProductID", productid);
		row.doAction("deleteall");
	}

	private void removeProductProduct(String productID, String productType) {
		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", productID);
		row.setParameter("ProductType", productType);
		row.setParameter("ProductProductsLinkedProduct.ProductType",
				productType);
		// we don't want to delete the global drips as they may have content.
		if (productType != null
				&& productType.toLowerCase().indexOf("drip") >= 0)
			row.setParameter("ProductProductsLinkedProduct.Category",
					"!Global+NULL");
		row.setParameter("CopiedFromProductID", "!NULL;!EMPTY");
		row.setParameter("ProductProductsLinkedProduct.CopiedFromProductID",
				"!NULL;!EMPTY");
		row.doAction("search");
		row.getResults();

		// System.out.println(row.getStatement());

		GenRow delete = new GenRow();
		delete.setTableSpec("Products");
		delete.setConnection(this.getConnection());

		GenRow delProduct = new GenRow();
		delProduct.setTableSpec("ProductProducts");
		delProduct.setConnection(this.getConnection());

		GenRow delCostings = new GenRow();
		delCostings.setTableSpec("PackageCostings");
		delCostings.setConnection(this.getConnection());

		while (row.getNext()) {
			delete.clear();
			delete.setParameter("ProductID", row.getString("LinkedProductID"));
			delete.doAction("delete");

			delProduct.clear();
			delProduct.setParameter("ProductProductID",
					row.getString("ProductProductID"));
			delProduct.doAction("delete");

			delCostings.clear();
			delCostings.setParameter("ON-ParentProductID",
					row.getString("LinkedProductID"));
			delCostings.doAction("deleteall");
		}

		// delete.clear();
		// delete.setParameter("ProductID", productID);
		// delete.doAction("delete");

		row.close();
	}

	private void removeProductGroups(String productID) {
		GenRow row = new GenRow();
		row.setTableSpec("ProductSecurityGroups");
		row.setConnection(this.getConnection());
		row.setParameter("ON-ProductID", productID);
		row.doAction("deleteall");
	}

	private String findRulesProductID(String productID) {
		if (productID == null || productID.length() == 0)
			return "";

		GenRow row = new GenRow();
		row.setConnection(this.getConnection());
		row.setViewSpec("ProductLinkedProductView");
		row.setParameter("ProductID", productID);
		row.setParameter("ProductType", "Rules Library");
		row.setParameter("ProductProductsLinkedProduct.ProductType",
				"Rules Library");
		row.doAction("search");

		row.getResults();

		String ids = "";

		while (row.getNext()) {
			if (ids.length() > 0)
				ids += "+";
			ids += row.getString("LinkedProductID");
		}

		row.close();

		return ids;

	}

	private boolean checkRulesFlag = false;

	public boolean checkRulesOnly(String landproductid, String houseproductid) {
		checkRulesFlag = true;

		boolean b = checkRules(landproductid, houseproductid, "");

		checkRulesFlag = false;

		return b;
	}

	public boolean checkRules(String landproductid, String houseproductid,
			String facadeproductid) {
		// System.err.println("---------------------------------------------------------------------------------------");
		// System.err.println("Start Rules for " + landproductid + " " +
		// houseproductid);
		if (StringUtils.isBlank(facadeproductid)) facadeproductid = getDefaultFacade(houseproductid);
		
		boolean regionPricing = "true".equals(InitServlet
				.getSystemParam("RegionPricingEnabled"));
		
		GenRow land = getCachedViewedProduct("properties/ProductLotView",
				"ProductID", landproductid);
		
		if (StringUtils.isBlank(land.getData("ProductID"))) {
			failedMessages.append("A package has been skipped because the Lot is no longer in Runway (" + landproductid + ")<br />");
			failedCalc.remove();
			return false;
		}

		GenRow house = getCachedViewedProduct("properties/ProductPlanView",
				"ProductID", houseproductid);

		if (StringUtils.isBlank(house.getData("ProductID"))) {
			failedMessages.append("A package has been skipped because the Home Plan is no longer in Runway (" + houseproductid + ")<br />");
			failedCalc.remove();
			return false;
		}

		if (regionPricing) {
			if (StringUtils.isNotBlank(land.getString("RegionID"))) {
				double houseRegionPrice = RunwayUtil.getRegionalPrice(
						houseproductid, land.getString("RegionID"));

				logger.trace("Land Region is {}", land.getString("RegionID"));
				logger.debug("Region Price is {}",
						Double.toString((houseRegionPrice)));

				if (houseRegionPrice <= 0.0) {
					failedMessages.append(house.getString("HomeName") + " "
							+ house.getString("Name") + " on Lot "
							+ land.getString("Name") + " "
							+ land.getString("EstateName") + " failed as "
							+ house.getString("HomeName")
							+ " cannot be built in Region "
							+ land.getString("RegionName") + "<br />");
					failedCalc.remove();
					return false;
				}
			}
		}

		GenRow rules = new GenRow();
		rules.setViewSpec("ProductLinkedProductView");
		rules.setConnection(getConnection());
		rules.setParameter("ProductID", landproductid);
		rules.setParameter("ProductType", "Package Rules");
		rules.setParameter("ProductProductsLinkedProduct.ProductType",
				"Package Rules");
		rules.doAction("search");

		rules.getResults();

		while (rules.getNext()) {
			GenRow packRules = findRules(rules.getString("LinkedProductID"),
					"NULL+EMPTY"); // Package Rules ProductID
			try {
				if (packRules != null && packRules.size() > 0) {
					if (!parseRules(packRules, landproductid, houseproductid,
							facadeproductid)) {
						return false;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (packRules != null)
					packRules.close();
			}
		}

		rules.close();

		return true;
	}

	private boolean parseRules(GenRow rules, String landproductid,
			String houseproductid, String facadeproductid) {
		GenRow land = getCachedViewedProduct("properties/ProductLotView",
				"ProductID", landproductid);
		GenRow house = getCachedViewedProduct("properties/ProductPlanView",
				"ProductID", houseproductid);

		return parseRules(rules, land, house, facadeproductid);
	}

	private boolean parseRules(GenRow rules, GenRow land, GenRow house,
			String facadeproductid) {
		boolean brand = false, homeRange = false, home = false, homePlan = false, location = false, estate = false;
		// we need to know the IDs
		HashSet<String> keys = new HashSet<>();
		
		GenRow rls = new GenRow();
		rls.setTableSpec("PackageRules");
		rls.setConnection(rules.getConnection());
		rls.setParameter("-select0", "ApplyTo");
		rls.setParameter("-select1", "ApplyToID");
		rls.setParameter("ProductID", rules.getString("ProductID"));
		rls.setParameter("ApplyTo", "!NULL;!EMPTY");
		rls.setParameter("ApplyToID", "!NULL;!EMPTY;!UseAllOthers");
		rls.doAction("search");
		rls.getResults();
		
		while (rls.getNext()) keys.add(rls.getData("ApplyToID"));
		
		rls.close();
		
		while (rules.getNext()) {
			// System.err.println("Rule checking " + rules.getString("Name") +
			// " " + rules.getString("FromCalc"));
			// check to see if this rule applies to these products
			if (rules.getString("ApplyTo").length() > 0) {
				String applyTo = rules.getString("ApplyTo");
				String applyToID = rules.getString("ApplyToID");
				// System.err.println("Rule Check " + rules.getString("Name"));
				if ("Brand".equals(applyTo)
						&& !applyToID.equals(house.getString("BrandProductID"))
						&& !("UseAllOthers".equals(applyToID) && !keys.contains(house.getString("BrandProductID"))))  {
					continue;
				} else if ("Home Range".equals(applyTo)
						&& !applyToID.equals(house.getString("RangeProductID"))
						&& !("UseAllOthers".equals(applyToID) && !keys.contains(house.getString("RangeProductID")))) {
					continue;
				} else if ("Home".equals(applyTo)
						&& !applyToID.equals(house.getString("HomeProductID"))
						&& !("UseAllOthers".equals(applyToID) && !keys.contains(house.getString("HomeProductID")))) {
					continue;
				} else if ("Home Plan".equals(applyTo)
						&& !applyToID.equals(house
								.getString("CopiedFromProductID"))
						&& !applyToID.equals(house.getString("ProductID"))
						&& !("UseAllOthers".equals(applyToID) && !keys.contains(house.getString("ProductID")))) {
					continue;
				} else if ("Location".equals(applyTo)
						&& !applyToID.equals(land.getString("LocationID"))
						&& !applyToID.equals(land.getString("RegionID"))
						&& !("UseAllOthers".equals(applyToID)
						&& !keys.contains(land.getString("LocationID")) && !keys.contains(land.getString("LocationID")))) {
					continue;
				} else if ("SubType".equals(applyTo)
						&& !applyToID.equals(land.getString("ProductSubType"))
						&& !("UseAllOthers".equals(applyToID) && !keys.contains(land.getString("ProductSubType")))) {
					continue;
				} else if ("Marketing".equals(applyTo)
						&& !applyToID.equals(land.getString("MarketingStatus"))
						&& !("UseAllOthers".equals(applyToID) && !keys.contains(land.getString("MarketingStatus")))) {
					continue;
				} else if ("Estate".equals(applyTo)
						&& !applyToID.equals(land.getString("EstateProductID"))
						&& !("UseAllOthers".equals(applyToID) && !keys.contains(land.getString("EstateProductID")))) {
					continue;
				} else if ("Brand".equals(applyTo)
						&& "UseAllOthers".equals(applyToID) && brand) {
					continue;
				} else if ("Home Range".equals(applyTo)
						&& "UseAllOthers".equals(applyToID) && homeRange) {
					continue;
				} else if ("Home".equals(applyTo)
						&& "UseAllOthers".equals(applyToID) && home) {
					continue;
				} else if ("Home Plan".equals(applyTo)
						&& !applyToID.equals(house
								.getString("CopiedFromProductID"))
						&& "UseAllOthers".equals(applyToID) && homePlan) {
					continue;
				} else if ("Location".equals(applyTo)
						&& "UseAllOthers".equals(applyToID) && location) {
					continue;
				} else if ("Estate".equals(applyTo)
						&& "UseAllOthers".equals(applyToID) && estate) {
					continue;
				}

				// System.err.println("Rule Check Pass " +
				// rules.getString("Name"));
				if ("Brand".equals(applyTo)) {
					brand = true;
				} else if ("Home Range".equals(applyTo)) {
					homeRange = true;
				} else if ("Home".equals(applyTo)) {
					home = true;
				} else if ("Home Plan".equals(applyTo)) {
					homePlan = true;
				} else if ("Location".equals(applyTo)) {
					location = true;
				} else if ("Estate".equals(applyTo)) {
					estate = true;
				}
			}

			GenRow moreRules = findRules(rules.getString("ProductID"),
					rules.getString("PackageRuleID"));
			try {
				if (!parseComplicatedRule(rules.getString("FromCalc"), land,
						house, facadeproductid)) {
					// System.err.println("Rule Parent Failed " +
					// rules.getString("Name") + " " +
					// rules.getString("FromCalc") + " : " + failedCalc.trim());
					if (moreRules == null || moreRules.size() == 0) {
						// System.err.println("Rule Child Failed " +
						// rules.getString("Name") + " " +
						// rules.getString("FromCalc") + " : " +
						// failedCalc.trim());
						String fail = failedCalc.get();
						if (isPortal) 
							failedMessages.append("Failed Rule: " + rules.getString("Name") + "<br />" + rules.getString("Description") + "<br />");
						else
							failedMessages.append(house.getString("HomeName") + " "
									+ house.getString("Name") + " on Lot "
									+ land.getString("Name") + " "
									+ land.getString("EstateName") + " failed "
									+ rules.getString("Name") + "<br />"
									+ "&nbsp;&nbsp;" + rules.getString("FromCalc")
									+ " (" + (fail != null ? fail.trim() : "")
									+ ")<br>");
						failedCalc.remove();
						if (!checkRulesFlag) {
							if (moreRules != null)
								moreRules.close();
							return false;
						}
					} else if (checkRulesFlag) {
						String fail = failedCalc.get();
						if (isPortal) 
							failedMessages.append("Failed Rule: " + rules.getString("Name") + "<br />" + rules.getString("Description") + "<br />");
						else
							failedMessages.append(house.getString("HomeName") + " "
									+ house.getString("Name") + " on Lot "
									+ land.getString("Name") + " "
									+ land.getString("EstateName") + " failed "
									+ rules.getString("Name") + "<br />"
									+ "&nbsp;&nbsp;" + rules.getString("FromCalc")
									+ " (" + (fail != null ? fail.trim() : "")
									+ ")<br>");
						failedCalc.remove();
					}
				} else if (moreRules != null && moreRules.size() > 0) {
					// System.err.println("Rule parent Passed " +
					// rules.getString("Name") + " " +
					// rules.getString("FromCalc") + " : " + failedCalc.trim());
					if (checkRulesFlag) {
						String fail = failedCalc.get();
						if (isPortal) 
							failedMessages.append("Failed Rule: " + rules.getString("Name") + "<br />" + rules.getString("Description") + "<br />");
						else
							failedMessages.append(house.getString("HomeName") + " "
									+ house.getString("Name") + " on Lot "
									+ land.getString("Name") + " "
									+ land.getString("EstateName") + " passed "
									+ rules.getString("Name") + "<br />"
									+ "&nbsp;&nbsp;" + rules.getString("FromCalc")
									+ " (" + (fail != null ? fail.trim() : "")
									+ ")<br>");
						failedCalc.remove();
					}
					if (!parseRules(moreRules, land, house, facadeproductid)) {
						// System.err.println("Rule child Failed " +
						// rules.getString("Name") + " " +
						// rules.getString("FromCalc") + " : " +
						// failedCalc.trim());
						String fail = failedCalc.get();
						if (isPortal) 
							failedMessages.append("Failed Rule: " + rules.getString("Name") + "<br />" + rules.getString("Description") + "<br />");
						else
							failedMessages.append(house.getString("HomeName") + " "
									+ house.getString("Name") + " on Lot "
									+ land.getString("Name") + " "
									+ land.getString("EstateName") + " failed "
									+ rules.getString("Name") + "<br />"
									+ "&nbsp;&nbsp;" + rules.getString("FromCalc")
									+ " (" + (fail != null ? fail.trim() : "")
									+ ")<br>");
						failedCalc.remove();
						if (!checkRulesFlag) {
							if (moreRules != null)
								moreRules.close();
							return false;
						}
					}
				} else if (checkRulesFlag) {
					String fail = failedCalc.get();
					if (isPortal) 
						failedMessages.append("Failed Rule: " + rules.getString("Name") + "<br />" + rules.getString("Description") + "<br />");
					else
						failedMessages.append(house.getString("HomeName") + " "
								+ house.getString("Name") + " on Lot "
								+ land.getString("Name") + " "
								+ land.getString("EstateName") + " passed "
								+ rules.getString("Name") + "<br />"
								+ "&nbsp;&nbsp;" + rules.getString("FromCalc")
								+ " (" + (fail != null ? fail.trim() : "")
								+ ")<br>");
					failedCalc.remove();
				}
			} catch (Exception e) {
				if (isPortal) 
					failedMessages.append("Rules failed due to internal error<br />");
				else
					failedMessages.append("Rules failed due to exception "
						+ e.getMessage() + "<br>");
			} finally {
				if (moreRules != null)
					moreRules.close();
			}
		}

		// System.err.println("Rule Passed");

		return true;
	}

	private boolean parseComplicatedRule(String rule, GenRow land,
			GenRow house, String facadeproductid) {
		String ifDim = rule;
		String[] parts = ifDim.split("[\\\\&\\\\|]{2}");

		// TODO: added code for || or method

		for (int p = 0; p < parts.length; ++p) {
			String tag = parts[p];
			boolean b = parseFreeRules(tag, land, house, facadeproductid);
			if (!b && rule.indexOf("||") == -1)
				return false;
			ifDim = ifDim.replaceAll(
					tag.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]")
							.replaceAll("\\(", "\\\\(")
							.replaceAll("\\)", "\\\\)")
							.replaceAll("\\+", "\\\\+")
							.replaceAll("\\-", "\\\\-")
							.replaceAll("\\/", "\\\\/")
							.replaceAll("\\*", "\\\\*"),
					" " + String.valueOf(b) + " ");
		}

		return ifDim.indexOf("false") == -1;
	}

	private boolean checkComplicatedFree(GenRow costings, String houselandid) {
		if (costings.getString("IfDimension").length() == 0
				|| costings.getString("IfDimValue").length() == 0
				|| !"FreeForm".equals(costings.getString("IfDimension")))
			return true;
		String ifDim = costings.getString("IfDimValue");
		String[] parts = ifDim.split("[\\\\&\\\\|]{2}");

		// TODO: added code for || or method

		for (int p = 0; p < parts.length; ++p) {
			String tag = parts[p];
			boolean b = checkConditionFree(tag, houselandid);
			if (!b && costings.getString("IfDimValue").indexOf("||") == -1)
				return false;
			ifDim = ifDim.replaceAll(
					tag.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]")
							.replaceAll("\\(", "\\\\(")
							.replaceAll("\\)", "\\\\)")
							.replaceAll("\\+", "\\\\+")
							.replaceAll("\\-", "\\\\-")
							.replaceAll("\\/", "\\\\/")
							.replaceAll("\\*", "\\\\*"),
					" " + String.valueOf(b) + " ");
		}

		return ifDim.indexOf("false") == -1;
	}

	protected boolean checkConditionFree(String costings, String houselandid) {

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductDetails");

		String ifDim = costings;
		ifDim = ifDim.replaceAll("\\\\r", "").replaceAll("\\\\n", "");
		boolean missing = false;
		String[] partss = ifDim.split("[\\+\\-\\*\\/\\<\\=\\!\\>~]");
		for (int p = 0; p < partss.length; ++p) {
			// System.out.println(partss[p]);
			String tag = partss[p].trim();
			if (tag.length() != tag.replaceAll("[a-zA-Z]", "").length()) {
				String[] fields = tag.split("\\.");

				if (fields.length == 3) {
					String productType = "";
					if (ifDim.toLowerCase().replaceAll(" ", "").indexOf("plan") >= 0)
						productType = "HomePlanDetails+Home Plan+HomePlan";
					else if (ifDim.toLowerCase().replaceAll(" ", "").indexOf("facade") >= 0)
						productType = "HomeFacadeDetails+Home Facade";
					else if (ifDim.toLowerCase().replaceAll(" ", "").indexOf("land") >= 0)
						productType = "Land+LandDetails";

					row.clear();
					row.setConnection(this.getConnection());
					row.setParameter("-select", fields[2]);
					row.setParameter("ProductType", productType);
					row.setParameter("DetailsType", fields[1]);
					row.setParameter("ProductID", houselandid);
					if (row.getString("ProductID").length() > 0)
						row.doAction("selectfirst");

					if (!row.isSuccessful()) {
						row.remove("ProductType");
						if (row.getString("ProductID").length() > 0)
							row.doAction("selectfirst");
						row.doAction("selectfirst");
					}

					if (row.isSuccessful()) {
						ifDim = ifDim.replaceAll(
								tag.replaceAll("\\[", "\\\\[")
										.replaceAll("\\]", "\\\\]")
										.replaceAll("\\(", "\\\\(")
										.replaceAll("\\)", "\\\\)"),
								" " + row.getString(fields[2]) + " ");
						failedCalc.set(ifDim);
					} else {
						ifDim = ifDim.replaceAll(
								tag.replaceAll("\\[", "\\\\[")
										.replaceAll("\\]", "\\\\]")
										.replaceAll("\\(", "\\\\(")
										.replaceAll("\\)", "\\\\)"),
								" -missing- ");
						failedCalc.set(ifDim);
						missing = true;
					}
				}
			}
		}

		if (ifDim.length() != ifDim.replaceAll("[\\+\\-\\*\\/\\<\\=\\!\\>~]", "").length()) {
			ifDim = calculate(ifDim);
			boolean ret = true;
			StringTokenizer st = new StringTokenizer(ifDim, " ");
			if (st.hasMoreTokens()) {
				String token1 = st.nextToken();
				while (st.hasMoreTokens()) {
					if (token1.indexOf("=") >= 0 || token1.indexOf("<") >= 0
							|| token1.indexOf(">") >= 0)
						return false;
					if (st.hasMoreTokens()) {
						String maths = st.nextToken();
						if (maths.indexOf("=") == -1
								&& maths.indexOf("~") == -1
								&& maths.indexOf("<") == -1
								&& maths.indexOf(">") == -1)
							return false;
						if (st.hasMoreTokens()) {
							String token2 = st.nextToken();
							if (token2.indexOf("=") >= 0
									|| token2.indexOf("<") >= 0
									|| token2.indexOf(">") >= 0)
								return false;
							try {
								if (missing && "~".equals(maths.trim())) {
									if ("-missing-".equals(token1.trim()))
										token1 = "0";
									if ("-missing-".equals(token2.trim()))
										token2 = "0";
									maths = "=";
								}
								double d1 = Double.parseDouble(token1.replaceAll("[^0-9\\.\\-]", ""));
								double d2 = Double.parseDouble(token2.replaceAll("[^0-9\\.\\-]", ""));
								if ("=".equals(maths) || "==".equals(maths))
									ret = d1 == d2;
								else if ("<".equals(maths))
									ret = d1 < d2;
								else if ("<=".equals(maths)
										|| "=<".equals(maths))
									ret = d1 <= d2;
								else if (">".equals(maths))
									ret = d1 > d2;
								else if (">=".equals(maths)
										|| "=>".equals(maths))
									ret = d1 >= d2;
								else if ("!=".equals(maths))
									ret = d1 != d2;
							} catch (Exception e) {
								logger.trace("error in {} of {}",
										"checkConditionFree", e.getMessage());
								try {
									if ("=".equals(maths) || "==".equals(maths))
										ret = token1.trim().equalsIgnoreCase(
												token2.trim());
									else if ("<".equals(maths))
										ret = token1.trim()
												.compareToIgnoreCase(
														token2.trim()) < 0;
									else if ("<=".equals(maths)
											|| "=<".equals(maths))
										ret = token1.trim()
												.compareToIgnoreCase(
														token2.trim()) <= 0;
									else if (">".equals(maths))
										ret = token1.trim()
												.compareToIgnoreCase(
														token2.trim()) > 0;
									else if (">=".equals(maths)
											|| "=>".equals(maths))
										ret = token1.trim()
												.compareToIgnoreCase(
														token2.trim()) >= 0;
									else if ("!=".equals(maths))
										ret = !token1.trim().equalsIgnoreCase(
												token2.trim());
								} catch (Exception e2) {
									logger.trace("error in {} of {}",
											"checkConditionFree",
											e.getMessage());
									return false;
								}
							}
							token1 = token2;
						}
					}
					if (!ret)
						break;
				}
			}
			return ret;
		}

		return false;
	}

	private boolean parseFreeRules(String rule, GenRow land, GenRow house,
			String facadeproductid) {
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductDetails");

		String ifDim = rule;

		failedCalc.set(ifDim);

		boolean missing = false;

		String[] partss = ifDim.split("[\\+\\-\\*\\/\\<\\=\\!\\>~]");
		for (int p = 0; p < partss.length; ++p) {
			// System.out.println(partss[p]);
			String tag = partss[p].trim();
			if (tag.length() != tag.replaceAll("[a-zA-Z]", "").length()) {
				String[] fields = tag.split("\\.");

				if (fields.length == 3) {
					String id = land.getString("ProductID");
					if (house.isSet("ProductID")) {
						if (id.length() > 0)
							id += "+";
						id += house.getString("ProductID");
					}
					if (StringUtils.isNotBlank(facadeproductid)) {
						if (id.length() > 0)
							id += "+";
						id += facadeproductid;
					}

					String pType = fields[0];
					if (StringUtils.isBlank(pType)) pType = "";
					if (pType.toLowerCase().replaceAll(" ", "").indexOf("plan") >= 0)
						pType = "HomePlanDetails+Home Plan+HomePlan";
					else if (pType.toLowerCase().replaceAll(" ", "").indexOf("facade") >= 0)
						pType = "HomeFacadeDetails+Home Facade";
					else if (pType.toLowerCase().replaceAll(" ", "").indexOf("land") >= 0)
						pType = "Land+LandDetails";
					
					row.clear();
					row.setConnection(this.getConnection());
					row.setParameter("-select0", fields[2]);
					row.setParameter("-select1", "ProductDetailsID");
					row.setParameter("DetailsType", fields[1]);
					row.setParameter("ProductType", pType);
					row.setParameter("ProductID", id);
					if (row.getString("ProductID").length() > 0)
						row.doAction("selectfirst");

					if (!row.isSuccessful()) {
						row.remove("ProductType");
						row.doAction("selectfirst");
					}

					// System.out.println(row.getStatement());

					if (row.isSuccessful()
							&& row.getString("ProductDetailsID").length() > 0) {
						ifDim = ifDim.replaceAll(
								tag.replaceAll("\\[", "\\\\[")
										.replaceAll("\\]", "\\\\]")
										.replaceAll("\\(", "\\\\(")
										.replaceAll("\\)", "\\\\)"),
								" " + row.getString(fields[2]) + " ");
						failedCalc.set(ifDim);

					} else {
						ifDim = ifDim.replaceAll(
								tag.replaceAll("\\[", "\\\\[")
										.replaceAll("\\]", "\\\\]")
										.replaceAll("\\(", "\\\\(")
										.replaceAll("\\)", "\\\\)"),
								" -missing- ");
						failedCalc.set(ifDim);
						missing = true;
					}
				}
			}
		}

		failedCalc.set(ifDim);

		if (ifDim.length() != ifDim.replaceAll("[\\+\\-\\*\\/\\<\\=\\!\\>~]",
				"").length()) {

			ifDim = calculate(ifDim);

			boolean ret = true;
			StringTokenizer st = new StringTokenizer(ifDim, " ");
			if (st.hasMoreTokens()) {
				String token1 = st.nextToken().trim();
				while (st.hasMoreTokens()) {
					if (token1.indexOf("=") >= 0 || token1.indexOf("<") >= 0
							|| token1.indexOf(">") >= 0)
						return false;
					if (st.hasMoreTokens()) {
						String maths = st.nextToken().trim();
						if (maths.indexOf("=") == -1
								&& maths.indexOf("~") == -1
								&& maths.indexOf("<") == -1
								&& maths.indexOf(">") == -1)
							return false;
						if (st.hasMoreTokens()) {
							String token2 = st.nextToken().trim();
							if (token2.indexOf("=") >= 0
									|| token2.indexOf("<") >= 0
									|| token2.indexOf(">") >= 0)
								return false;
							try {
								if (missing && "~".equals(maths.trim())) {
									if ("-missing-".equals(token1.trim()))
										token1 = "0";
									if ("-missing-".equals(token2.trim()))
										token2 = "0";
									maths = "=";
								}
								double d1 = Double.parseDouble(token1.trim());
								double d2 = Double.parseDouble(token2.trim());
								if ("=".equals(maths) || "==".equals(maths))
									ret = (d1 == d2);
								else if ("<".equals(maths))
									ret = (d1 < d2);
								else if ("<=".equals(maths)
										|| "=<".equals(maths))
									ret = (d1 <= d2);
								else if (">".equals(maths))
									ret = (d1 > d2);
								else if (">=".equals(maths)
										|| "=>".equals(maths))
									ret = (d1 >= d2);
								else if ("!=".equals(maths))
									ret = (d1 != d2);
							} catch (Exception e) {
								logger.trace("error in {} of {}",
										"parseFreeRules", e.getMessage());
								try {
									if ("=".equals(maths) || "==".equals(maths))
										ret = token1.trim().equalsIgnoreCase(
												token2.trim());
									else if ("<".equals(maths))
										ret = token1.trim()
												.compareToIgnoreCase(
														token2.trim()) < 0;
									else if ("<=".equals(maths)
											|| "=<".equals(maths))
										ret = token1.trim()
												.compareToIgnoreCase(
														token2.trim()) <= 0;
									else if (">".equals(maths))
										ret = token1.trim()
												.compareToIgnoreCase(
														token2.trim()) > 0;
									else if (">=".equals(maths)
											|| "=>".equals(maths))
										ret = token1.trim()
												.compareToIgnoreCase(
														token2.trim()) >= 0;
									else if ("!=".equals(maths))
										ret = !token1.trim().equalsIgnoreCase(
												token2.trim());
								} catch (Exception e2) {
									logger.trace("error in {} of {}",
											"parseFreeRules", e.getMessage());
									return false;
								}
							}
							token1 = token2;
						}
					}
					if (!ret)
						break;
				}
			}
			return ret;
		}
		return false;
	}

	/*
	 * REMOVED AS UNUSED 12-10-2012 private String calcProductValue(String
	 * value, String calc) { String maths = calc.substring(0, 1); String val =
	 * calc.substring(1); if (maths.replaceAll("[^\\+\\-\\*\\/]", "").length()
	 * == 0) { maths = "+"; val = calc; }
	 * 
	 * try { double cal = Double.parseDouble(val); double data =
	 * Double.parseDouble(value); if ("+".equals(maths)) { return "" + (data +
	 * cal); } else if ("-".equals(maths)) { return "" + (data - cal); } else if
	 * ("*".equals(maths)) { return "" + (data * cal); } else if
	 * ("/".equals(maths)) { return "" + (data / cal); } } catch (Exception e) {
	 * logger.trace("error in {} of {}","calcProductValue",e.getMessage()); if
	 * ("+".equals(maths)) { return value + val; } else if ("-".equals(maths)) {
	 * return value.replaceAll(val, ""); } }
	 * 
	 * return value; }
	 * 
	 * private String getPrductValue(String fromType, String fromDim, String
	 * fromValue, String productid) { GenRow details = new GenRow();
	 * details.setTableSpec("properties/ProductDetails");
	 * details.setConnection(this.getConnection());
	 * details.setParameter("-select0", fromValue);
	 * details.setParameter("ProductID", productid);
	 * details.setParameter("ProductType", fromType.replaceAll(" ", "") + "%");
	 * details.setParameter("DetailsType", fromDim);
	 * details.doAction(GenerationKeys.SELECTFIRST);
	 * 
	 * return details.getString(fromValue); }
	 * 
	 * private boolean checkRuleCondition(PackageRule pr, String productid) {
	 * GenRow details = new GenRow();
	 * details.setTableSpec("properties/ProductDetails");
	 * details.setConnection(this.getConnection());
	 * details.setParameter("-select0", pr.conditionValue);
	 * details.setParameter("ProductID", productid);
	 * details.setParameter("ProductType", pr.conditionType.replaceAll(" ", "")
	 * + "%"); details.setParameter("DetailsType", pr.conditionDim);
	 * details.doAction(GenerationKeys.SELECTFIRST);
	 * 
	 * if (details.isSuccessful()) { if ((pr.conditionCalc.length() == 0 ||
	 * "=".equals(pr.conditionCalc)) &&
	 * details.getString(pr.conditionValue).length() == 0) { return true; } else
	 * { String cond = pr.conditionCalc.replaceAll("[^<>=]", ""); if
	 * (cond.length() == 0) cond = "="; String value =
	 * pr.conditionCalc.replaceAll("[<>=]", ""); try { // first we need to do a
	 * number compare as this is different // from a string one double val =
	 * Double.parseDouble(value); double data = Double.parseDouble(details
	 * .getString(pr.conditionValue));
	 * 
	 * if ("=".equals(cond) && data == val) { return true; } else if
	 * ("<".equals(cond) && data < val) { return true; } else if
	 * ("<=".equals(cond) && data <= val) { return true; } else if
	 * (">".equals(cond) && data > val) { return true; } else if
	 * (">=".equals(cond) && data >= val) { return true; } } catch (Exception e)
	 * { logger.trace("error in {} of {}","checkRuleCondition",e.getMessage());
	 * // failed a number compare try string one String d =
	 * details.getString(pr.conditionValue); if ("=".equals(cond) &&
	 * value.equalsIgnoreCase(d)) { return true; } else if ("<".equals(cond) &&
	 * value.compareToIgnoreCase(d) < 0) { return true; } else if
	 * ("<=".equals(cond) && value.compareToIgnoreCase(d) <= 0) { return true; }
	 * else if (">".equals(cond) && value.compareToIgnoreCase(d) > 0) { return
	 * true; } else if (">=".equals(cond) && value.compareToIgnoreCase(d) >= 0)
	 * { return true; } } }
	 * 
	 * return false; }
	 * 
	 * return false; }
	 */
	public String getPackageIDs(String packageCostLibraryID) {
		if (packageCostLibraryID != null && packageCostLibraryID.length() != 0) {
			GenRow row = new GenRow();
			row.setTableSpec("ProductLibraries");
			row.setConnection(this.getConnection());
			row.setParameter("-select0", "ProductIDs");
			row.setParameter("ProductLibraryID", packageCostLibraryID);
			row.doAction(GenerationKeys.SELECTFIRST);

			if (row.getString("ProductIDs").length() > 0)
				return row.getString("ProductIDs");
		}
		return "NOTHINGTODO";
	}

	public void deleteHouseAndLandPackage(String productid) {
		if (productid != null && productid.length() != 0) {
			GenRow row = new GenRow();
			row.setTableSpec("Products");
			row.setConnection(this.getConnection());
			row.setParameter("ProductID", productid);
			row.doAction(ActionBean.DELETE);

			GenRow ProductDetails = new GenRow();
			ProductDetails.setTableSpec("properties/ProductDetails");
			ProductDetails.setConnection(getConnection());
			ProductDetails.setColumn("ON-ProductID", productid);
			ProductDetails.doAction("deleteall");

			GenRow childVariation = new GenRow();
			childVariation.setTableSpec("ProductVariations");
			childVariation.setConnection(getConnection());
			childVariation.setParameter("-select", "*");
			childVariation.setColumn("ON-ParentProductID|1", productid);
			childVariation.setColumn("ON-ChildProductID|1", productid);
			childVariation.doAction("deleteall");

			GenRow childProduct = new GenRow();
			childProduct.setTableSpec("ProductProducts");
			childProduct.setConnection(getConnection());
			childProduct.setColumn("ON-ProductID|1", productid);
			childProduct.setColumn("ON-LinkedProductID|1", productid);
			childProduct.doAction("deleteall");

			if (RPMtask.getInstance() != null)
				RPMtask.getInstance().doIndexRefreash("delete", productid,
						"House and Land");
		}
	}

	public void createHouseAndLandPackages(ArrayList<String> houseproductids,
			ArrayList<String> landproductids,
			HashMap<String, String> selectedfacades, String userid) {
		this.request = request;
		this.houseproductids = houseproductids;
		this.landproductids = landproductids;
		this.selectedfacades = selectedfacades;

		this.userid = userid;

		if (houseproductids != null && landproductids != null) {
			startTime = new Date();
			this.setName("Create House and Land");
			this.start();
		}
	}

	public void createHouseAndLandPackages(
			ArrayList<String> houselandproductids, String userid) {
		this.request = request;
		this.houselandids = houselandproductids;

		this.userid = userid;

		if (houselandids != null) {
			startTime = new Date();
			this.setName("Recreate House and Land");
			this.start();
		}
	}

	public void setConnection(Connection c) {
		try {
			if (con != null && !con.isClosed()) {
				con.close();
			}

		} catch (SQLException e) {
			logger.warn("Error closing connection", e);
		} finally {
			con = c;
		}
	}

	protected Connection getConnection() {
		try {
			if (con == null || con.isClosed()) {
				ctx = new InitialContext();

				DataSource ds = null;
				try {
					dbconn = (String) InitServlet.getConfig()
							.getServletContext()
							.getInitParameter("sqlJndiName");
					Context envContext = (Context) ctx
							.lookup(ActionBean.subcontextname);
					ds = (DataSource) envContext.lookup(dbconn);
				} catch (Exception e) {
				}
				if (ds == null) {
					ds = (DataSource) ctx.lookup(dbconn);
				}
				con = ds.getConnection();
			}
		} catch (Exception e) {
			return null;
		} finally {
			try {
				ctx.close();
				ctx = null;
			} catch (Exception e) {
			}
		}

		return con;
	}

	public void run() {
		isRunning = true;
		hasStarted = true;
		passedRules = new AtomicInteger(0);
		failedRules = new AtomicInteger(0);
		failedDuplicates = new AtomicInteger(0);
		currentPackages = new AtomicInteger(0);
		currentStep = new AtomicInteger(0);

		productMap = Collections.synchronizedMap(new HashMap<String, GenRow>());
		costingsMap = Collections
				.synchronizedMap(new HashMap<String, ArrayList<Map<String, String>>>());
		linkMap = Collections
				.synchronizedMap(new HashMap<String, ArrayList<Map<String, String>>>());
		documentMap = Collections
				.synchronizedMap(new HashMap<String, Map<String, String>>());
		costingsLinks = Collections
				.synchronizedMap(new HashMap<String, String>());

		createdpackages.clear();

		try {
			if (houselandids != null && houselandids.size() > 0) {
				runRefreash();
			} else {
				runCreate();
			}
			if (con != null && !con.isClosed())
				con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			isRunning = false;
		}
	}

	private void saveLogs() {
		try {
			FileOutputStream fis = new FileOutputStream(
					"/tmp/createhouseandland_" + fformat.format(new Date())
							+ ".log");
			fis.write(log.toString().getBytes());
			fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void runCreate() {
		try {
			recreateTask = false;

			updateDims = fullUpdate = updateDetails = updateAnswers = updateCosts = updateDrips = updatePlan = updatePrice = true;
			maxPackages = houseproductids.size() * landproductids.size();
			maxSteps = maxPackages;

			logger.debug("Starting Create on");

			// load up the duplicates if we need them
			if (!createDuplicates)
				loadDuplcates();

			startTime = new Date();
			startTimeLong = startTime.getTime();
			// Date now = new Date();
			for (int i = 0; i < houseproductids.size(); i++) {
				for (int j = 0; j < landproductids.size(); j++) {
					if (!isRunning)
						return;

					processingPackages.incrementAndGet();

					logger.debug("Started Create on {} {}",
							landproductids.get(j), houseproductids.get(i));

					if (!duplicates.contains((String) landproductids.get(j)
							+ (String) houseproductids.get(i))) {
						failedCalc.set("");
						Date d = new Date();
						String id = createHouseAndLandPackage(
								(String) houseproductids.get(i),
								(String) landproductids.get(j),
								(String) selectedfacades.get(houseproductids
										.get(i)), userid);
						if (id != null && id.length() > 0) {
							createdpackages.add(id);
							RPMtask.getInstance().doIndexRefreash("update", id,
									"House and Land");
							// ++passedRules;
							passedRules.incrementAndGet();
							if (HouseAndLandManager.getInstance() != null)
								HouseAndLandManager.getInstance().addTaskItem(
										this.id, id, landproductids.get(j),
										houseproductids.get(i), "Y", "N",
										((new Date()).getTime() - d.getTime()));
						} else {
							// ++failedRules;
							failedRules.incrementAndGet();
						}
					} else {
						// ++failedDuplicates;
						failedDuplicates.incrementAndGet();
					}
					currentPackages.incrementAndGet();
					currentStep.incrementAndGet();
					// ++currentPackages;
					// now = new Date();
					// timePackage = now.getTime() - startTime.getTime();
					// timePerPackage = (double) timePackage /
					// currentPackages.intValue();
					// estimatedTime = (double) timePerPackage * maxPackages;
					// estimatedRemaining = estimatedTime - timePackage;
					// if (estimatedRemaining < 0)
					// estimatedRemaining = 0;

					logger.debug("Ended Create on {} {}",
							landproductids.get(j), houseproductids.get(i));
				}
			}

			saveLogs();

			String activityID = logActivity();

			logger.debug("isSendEmailNotification {}", isSendEmailNotification);

			if (isSendEmailNotification) {
				String emailIDs = getUserEmailIDs();
				String message = getUserMessage(activityID);

				logger.debug("isSendEmailNotification to {}", emailIDs);

				if (StringUtils.isNotBlank(emailIDs)) {
					UserAlertEmailer.sendUserEmail(emailIDs, message);
				}
			}

			if (RPMtask.getInstance() != null)
				RPMtask.getInstance().refreshData(RPMtask.type.hal);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String getUserMessage(String activityID) {
		return "New H&L packages have been created. Please follow the link to view them.\n"
				+ InitServlet.getSystemParams().getProperty("URLHome")
				+ "/crm/cms/properties/shownotifiedhouseandland.jsp?ActivityID="
				+ activityID;
	}

	private String logActivity() {
		String activityID = KeyMaker.generate();

		GenRow activitylog = new GenRow();
		activitylog.setConnection(this.getConnection());
		activitylog.setViewSpec("ActivityView");
		activitylog.setColumn("Location", "");
		activitylog.setColumn("UserID", userid);
		activitylog.setColumn("ActivityID", activityID);
		activitylog.setColumn("RecordType", "House and Land");
		activitylog.setColumn("ActionType", "Created");
		activitylog.setColumn("CreatedDate", "NOW");
		activitylog.setColumn("RecordID",
				StringUtils.join(createdpackages, "+"));
		activitylog.setAction("insert");
		activitylog.doAction();
		activitylog.close();

		logger.debug("Activity logged. ActivityID {}", activityID);

		return activityID;
	}

	/**
	 * @param request
	 */
	private String getUserEmailIDs() {
		String userIDs = userid
				+ "+"
				+ InitServlet.getSystemParams().getProperty(
						"HNLNotficationUserIDs");

		logger.debug("NotficationUserIDs {} ", userIDs);

		Set<String> emailIDs = new HashSet<String>();

		GenRow row = new GenRow();
		if (StringUtils.isNotBlank(userIDs)) {
			row.setViewSpec("UserView");
			row.setConnection(this.getConnection());
			row.setColumn("UserID", userIDs);
			row.doAction("search");
			row.getResults();
			logger.debug("NotficationUserIDs Query {} ", row.getStatement());
		}

		while (row.getNext()) {
			if (StringUtils.isNotBlank(row.getString("Email"))) {
				emailIDs.add(row.getString("Email"));
			}
		}

		row.close();

		return StringUtils.join(emailIDs, "+");
	}

	public void runRefreash() {
		recreateTask = true;
		maxPackages = houselandids.size();

		maxSteps = maxPackages * (resetContents ? 11 : 26);

		logger.debug("Starting Recreate on");

		StringBuilder hlIDs = new StringBuilder();
		for (String hlID : houselandids) {
			if (hlIDs.length() > 0)
				hlIDs.append("+");
			hlIDs.append(hlID);
		}

		String availableStatusID = InitServlet.getSystemParams().getProperty(
				"ProductAvailableStatus");

		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHouseAndLandSimpleView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", hlIDs.toString());
		row.sortBy("LotID", 0);
		row.sortBy("PlanID", 1);
		// if (availableStatusID != null && availableStatusID.length() > 0) {
		// row.setParameter("CurrentStatusID", availableStatusID);
		// row.setParameter("ProductCurrentStatus.ProductStatusList.StatusID",
		// availableStatusID);
		// }
		row.doAction("search");

		row.getResults();

		startTime = new Date();
		// Date now = new Date();

		final String userID = userid;
		final boolean reset = resetContents;
		startTimeLong = startTime.getTime();

		ExecutorService executor = null;

		final String taskID = this.id;

		try {
			int threads = 5;
			try {
				threads = Integer.parseInt(InitServlet.getSystemParam(
						"HNLCreateThreads", String.valueOf(threads)));
			} catch (NumberFormatException nfe) {
			}

			executor = ThreadManager.createThreadExecutor(
					"CreateHouseAndLand:runRefreash(" + maxPackages + ")",
					threads); // Executors.newFixedThreadPool(threads);
			final CompletionService<Boolean> ecs = new ExecutorCompletionService<Boolean>(
					executor);
			int submitted = 0;
			while (row.getNext()) {
				final String planID = row.getString("PlanID");
				final String copyPlanID = row.getString("CopyPlanID");
				final String lotID = row.getString("LotID");
				final String homeDesignID = row.getString("HomeDesignID");
				final String productID = row.getData("ProductID");

				if (row.getString("CurrentStatusID").equals(availableStatusID)) {
					ecs.submit(new Runnable() {
						public void run() {
							if (!isRunning)
								return;

							logger.debug("Started Recreate on {}", productID);

							processingPackages.addAndGet(resetContents ? 11
									: 26);

							Date d = new Date();
							final String id = reset ? resetContents(planID,
									copyPlanID, lotID, homeDesignID, productID,
									userID) : refreshHouseAndLandPackage(
									planID, copyPlanID, lotID, homeDesignID,
									productID, userID);

							if (StringUtils.isNotBlank(id)) {
								createdpackages.add(id);
								if (RPMtask.getInstance() != null)
									RPMtask.getInstance().refreshIndex(
											"update", id, "House and Land");
								passedRules.incrementAndGet();
								if (HouseAndLandManager.getInstance() != null)
									HouseAndLandManager.getInstance()
											.updateTaskItem(
													taskID,
													id,
													lotID,
													planID,
													"Y",
													"N",
													((new Date()).getTime() - d
															.getTime()));
							} else {
								if (!keepBad) {
									deleteHouseAndLandPackage(productID);
									if (RPMtask.getInstance() != null)
										RPMtask.getInstance().refreshIndex(
												"delete", productID,
												"House and Land");
									if (HouseAndLandManager.getInstance() != null)
										HouseAndLandManager
												.getInstance()
												.updateTaskItem(
														taskID,
														id,
														lotID,
														planID,
														"N",
														"Y",
														((new Date()).getTime() - d
																.getTime()));
								} else {
									if (HouseAndLandManager.getInstance() != null)
										HouseAndLandManager
												.getInstance()
												.updateTaskItem(
														taskID,
														id,
														lotID,
														planID,
														"N",
														"N",
														((new Date()).getTime() - d
																.getTime()));
								}
								failedRules.incrementAndGet();
							}

							currentPackages.incrementAndGet();
							// timePackage = System.currentTimeMillis() -
							// startTimeLong;
							// timePerPackage = (double) timePackage / c;
							// estimatedTime = (double) timePerPackage *
							// maxPackages;
							// estimatedRemaining = estimatedTime - timePackage;
							// if (estimatedRemaining < 0)
							// estimatedRemaining = 0;

							logger.debug("Ended Recreate on {}", productID);
						}
					}, Boolean.FALSE /*
									 * it needs an object to return when the
									 * execution completes
									 */);
					submitted++;
					/*
					 * String id = null; if(resetContents) { id =
					 * resetContents(row.getString("PlanID"),
					 * row.getString("CopyPlanID"),
					 * row.getString("LotID"),row.getString("HomeDesignID"),
					 * row.getData("ProductID"), userid); } else { id =
					 * refreshHouseAndLandPackage(row.getString("PlanID"),
					 * row.getString("CopyPlanID"),
					 * row.getString("LotID"),row.getString("HomeDesignID"),
					 * row.getData("ProductID"), userid); } if (id != null &&
					 * id.length() > 0) { createdpackages.add(id); if
					 * (RPMtask.getInstance() != null)
					 * RPMtask.getInstance().doIndexRefreash("update",
					 * id,"House and Land"); ++passedRules; } else { // we need
					 * to go and delete this
					 * removeProductProduct(row.getData("ProductID"),
					 * "!Land;!Home Facade"); if (RPMtask.getInstance() != null)
					 * RPMtask.getInstance().refreshIndex("delete",
					 * row.getData("ProductID"),"House and Land");
					 * ++failedRules; }
					 */
				} else {
					failedDuplicates.incrementAndGet();
					currentPackages.incrementAndGet();
					if (HouseAndLandManager.getInstance() != null)
						HouseAndLandManager
								.getInstance()
								.updateTaskItem(
										taskID,
										id,
										lotID,
										planID,
										"S",
										"S",
										0);
				}
				// ++currentPackages; now moved above into duplicate method.
				if (!isRunning)
					break;
			}
			row.close();
			try {
				while (submitted > 0 && isRunning) {
					try {
						// this action blocks until one is complete, no need for
						// sleep etc.
						ecs.take().get();
					} catch (ExecutionException ee) {
						logger.error("error completing recreate", ee);
						failedRules.incrementAndGet();
					}
					submitted--;
					logger.trace("Recreate is not complete, {} left ",
							submitted);
				}
			} catch (InterruptedException e) {
				// this will occur if the executor was interrupted, for ex if
				// it's shutting down
				logger.error("thread was interrupted", e);
				isRunning = false;
			}
		} catch (Exception e) {
			failedMessages.append("Something Really Bad Happend, ").append(e);
			e.printStackTrace();
		} finally {
			if (executor != null) {
				ThreadManager.shutdownExecutor(executor);
			}
		}
		saveLogs();
		if (isRunning)
			if (RPMtask.getInstance() != null)
				RPMtask.getInstance().refreshData(RPMtask.type.hal);
	}

	public void parseRequestOptions(HttpServletRequest request) {
		if ("true".equals(request.getParameter("KeepBad")))
			keepBad = true;
		if ("true".equals(request.getParameter("ResetContents")))
			resetContents = true;
		if ("true".equals(request.getParameter("KeepFacade")))
			keepFacade = true;
		if ("true".equals(request.getParameter("UpdatePrice"))) {
			updatePrice = true;
			if ("true".equals(request.getParameter("UpdateCosts")))
				updateCosts = true;
			if ("true".equals(request.getParameter("UpdateDrips")))
				updateDrips = true;
			if ("true".equals(request.getParameter("UpdatePlan")))
				updatePlan = true;
			if ("true".equals(request.getParameter("UpdateDim")))
				updateDims = true;
			if ("true".equals(request.getParameter("UpdateAnswers")))
				updateAnswers = true;
		}

		if ("true".equals(request.getParameter("UpdatePublishHeadline")))
			updatePublishHeadline = true;
		if ("true".equals(request.getParameter("UpdatePublishDescription")))
			updatePublishDescription = true;
		if ("true".equals(request.getParameter("UpdateImages")))
			updateImages = true;
		if ("true".equals(request.getParameter("isPortal")))
			isPortal = true;
	}

	public boolean isProcessAssociated(String productid) {
		boolean isProcessAssociated = false;

		GenRow linkedProducts = new GenRow();
		linkedProducts.setTableSpec("LinkedProducts");
		linkedProducts.setParameter("-select0", "LinkedProductID");
		linkedProducts.setParameter("ProductID", productid);
		linkedProducts.doAction("search");
		linkedProducts.getResults(true);

		if (linkedProducts.getSize() > 0) {
			isProcessAssociated = true;
		}
		linkedProducts.close();
		return isProcessAssociated;
	}

	public List<String> getCreatedpackages() {
		if (!isRunning)
			return createdpackages;
		else
			return null;
	}

	public void stopRunning() {
		isRunning = false;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public boolean hasStarted() {
		return hasStarted;
	}

	public void setCreateDuplicates(boolean b) {
		createDuplicates = b;
	}

	@SuppressWarnings("unchecked")
	public String getProgressJSON() {
		double step = 0;

		if (startTimeLong == 0)
			startTimeLong = (new Date()).getTime();
		timePackage = System.currentTimeMillis() - startTimeLong;
		// if (currentStep.get() > 0) step = (double) currentStep.get() / 15;
		// / threadCount.get();
		if (currentPackages.get() > 0) {
			timePerPackage = (double) timePackage
					/ (currentPackages.get() + step);
		} else {
			timePerPackage = timePackage;
		}
		estimatedTime = (double) timePerPackage * maxPackages;
		estimatedRemaining = estimatedTime - timePackage;
		if (estimatedRemaining < 0)
			estimatedRemaining = 0;

		double remaining = ((double) Math.round(estimatedRemaining / 1000));
		if (remaining < 1)
			remaining = 0;
		String remStr = "" + remaining;
		if (remaining < 10) {
			remStr = "0:0" + remaining;
		} else if (remaining < 60) {
			remStr = "0:" + remaining;
		} else {
			remStr = "" + Math.round(remaining / 60);
			remaining = remaining % 60;
			if (remaining == 0) {
				remStr += ":00";
			} else if (remaining < 10) {
				remStr += ":0" + remaining;
			} else if (remaining < 60) {
				remStr += ":" + remaining;
			}
		}
		if (remStr.indexOf(".") >= 0) {
			remStr = remStr.substring(0, remStr.indexOf("."));
		}

		double taken = ((double) Math.round(timePackage / 1000));
		if (taken < 1)
			taken = 0;
		String takeStr = "" + taken;
		if (taken < 10) {
			takeStr = "0:0" + taken;
		} else if (taken < 60) {
			takeStr = "0:" + taken;
		} else {
			takeStr = "" + Math.round(taken / 60);
			taken = taken % 60;
			if (taken == 0) {
				takeStr += ":00";
			} else if (taken < 10) {
				takeStr += ":0" + taken;
			} else if (taken < 60) {
				takeStr += ":" + taken;
			}
		}
		if (takeStr.indexOf(".") >= 0) {
			takeStr = takeStr.substring(0, takeStr.indexOf("."));
		}

		JSONObject obj = new JSONObject();
		obj.put("currentPackage", (double) currentPackages.get());
		obj.put("currentStep", (double) currentStep.get());
		obj.put("processingPackage", processingPackages.toString());
		obj.put("maxPackage", maxPackages);
		obj.put("maxSteps", maxSteps);
		obj.put("packageTime", Math.round(timePerPackage));
		obj.put("packageTotal", ((double) Math.round(estimatedTime / 100) / 10));
		obj.put("remainingTotal", remStr);
		obj.put("takenTotal", takeStr);
		obj.put("duplicatesSkiped", failedDuplicates.toString());
		obj.put("passedRules", passedRules.toString());
		obj.put("failedRules", failedRules.toString());
		obj.put("recreateTask", String.valueOf(recreateTask));
		obj.put("completed", (hasStarted && !isRunning ? "true" : "false"));
		if (hasStarted && !isRunning) {
			obj.put("message",
					StringUtil.urlEncode(failedMessages.length() > 0 ? failedMessages
							.toString()
							: (passedRules
									+ " Package Created without any errors at "
									+ ((double) Math.round(timePerPackage) / 1000) + " sec per package")));

			if (getCreatedpackages() != null
					&& getCreatedpackages().size() == 1)
				obj.put("productID", getCreatedpackages().get(0));
		}
		logger.debug(obj.toJSONString());
		return obj.toJSONString();
	}

	public String getFailedMessage() {
		return failedMessages.toString();
	}

	public Set<String> getExistingCosts(String packageID) {
		HashSet<String> existingPackageCostList = new HashSet<String>();

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", packageID);
		// row.setParameter("Category",
		// "!Discount;!Rebate;!Inclusion;!Promotion");
		row.setParameter("ProductType", "Package Cost+Package Cost Library");
		row.setParameter("ProductProductsLinkedProduct.ProductType", "Package Cost+Package Cost Library");
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		while (row.getNext()) {
			String rootID = findRootCostings(getConnection(),
					row.getString("LinkedProductID"));
			if (rootID != null && rootID.length() > 0)
				existingPackageCostList.add(rootID);

		}

		row.close();

		return existingPackageCostList;
	}

	public Set<String> getExistingComAndPymntTemplates(String productID) {
		HashSet<String> existingComAndPymntTemplateList = new HashSet<String>();

		GenRow row = new GenRow();
		row.setViewSpec("ProductCommissionView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", productID);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		while (row.getNext()) {
			existingComAndPymntTemplateList.add(row
					.getString("CommissionTemplateID"));
		}

		row.close();

		return existingComAndPymntTemplateList;
	}

	private boolean isPackageClaimed(String productid) {
		GenRow productSecurity = new GenRow();
		productSecurity.setViewSpec("ProductSecurityGroupView");
		productSecurity.setConnection(this.getConnection());
		productSecurity.setParameter("ProductID", productid);
		productSecurity.doAction("selectfirst");
		logger.debug("Product has been claimed by "
				+ productSecurity.getString("RepUserID"));

		return (productSecurity.isSuccessful() && StringUtils
				.isNotBlank(productSecurity.getString("RepUserID")));
	}

	public void loadDuplcates() {
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHouseAndLandDuplicate");
		row.setConnection(this.getConnection());
		row.setParameter("LotID", arrayToString(landproductids));
		row.setParameter("CopyPlanID", arrayToString(houseproductids));
		row.setParameter(
				"-select0",
				"CONCAT(ProductProductsLinkedProductLand.ProductID,ProductProductsLinkedProductHouse.CopiedFromProductID) AS UniqueID");
		if (row.getParameter("LotID").length() > 0)
			row.setParameter(
					"ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductID",
					row.getParameter("LotID"));
		if (row.getParameter("CopyPlanID").length() > 0)
			row.setParameter(
					"ProductProductProducts#House.ProductProductsLinkedProductHouse.CopiedFromProductID",
					row.getParameter("CopyPlanID"));
		row.setParameter("-groupby0", "UniqueID");
		row.doAction(GenerationKeys.SEARCH);

		row.getResults();

		while (row.getNext()) {
			duplicates.add(row.getString("UniqueID"));
		}

		row.close();

	}

	protected String arrayToString(ArrayList<String> array) {
		String text = "";

		Iterator<String> i = array.iterator();

		while (i.hasNext()) {
			if (text.length() > 0)
				text += "+";
			text += i.next();
		}

		return text;
	}

	public ArrayList<PackageRule> getPackageRules(String productID) {
		if (rules.containsKey(productID)) {
			return rules.get(productID);
		}

		ArrayList<PackageRule> rule = loadPackageRules(productID);
		rules.put(productID, rule);

		return rule;
	}

	private ArrayList<PackageRule> loadPackageRules(String productID) {
		ArrayList<PackageRule> rule = new ArrayList<PackageRule>();

		GenRow lib = new GenRow();
		lib.setViewSpec("ProductPackageRulesView");
		lib.setConnection(this.getConnection());
		lib.setParameter("ProductID", productID);
		lib.sortBy("SortOrder", 0);
		lib.doAction(GenerationKeys.SEARCH);

		lib.getResults();

		while (lib.getNext()) {
			rule.add(new PackageRule(lib));
		}

		lib.close();

		return rule;
	}

	private ArrayList<PackageRule> loadChildRules(String parentID) {
		ArrayList<PackageRule> rule = new ArrayList<PackageRule>();

		GenRow lib = new GenRow();
		lib.setViewSpec("ProductPackageRulesView");
		lib.setConnection(this.getConnection());
		lib.setParameter("ProductPackageRulePackageRules.ParentID", parentID);
		lib.setParameter("ParentID", parentID);
		lib.sortBy("SortOrder", 0);
		lib.doAction(GenerationKeys.SEARCH);

		lib.getResults();

		while (lib.getNext()) {
			rule.add(new PackageRule(lib));
		}

		lib.close();

		return rule;
	}

	public static void checkPublishAlbums(HttpServletRequest request,
			String productID, String heroImageID, PublishMethod pm,
			String userID) {
		checkPublishAlbums(ActionBean.getConnection(request), productID,
				heroImageID, pm, userID);
	}

	public static void checkPublishAlbums(Connection con, String productID,
			String heroImageID, PublishMethod pm, String userID) {
		GenRow gallery = new GenRow();
		gallery.setConnection(con);
		gallery.setViewSpec("GalleryView");
		gallery.setParameter("ProductID", productID);
		gallery.setParameter("Publisher", "%" + pm.getPublisher() + "%");
		gallery.doAction("selectfirst");

		String galleryID = "";

		if (gallery.isSuccessful()) {
			galleryID = gallery.getData("GalleryID");
		} else {
			// Insert an Album if not present
			galleryID = KeyMaker.generate();
			gallery.setParameter("GalleryID", galleryID);
			gallery.setParameter("ProductID", productID);
			gallery.setParameter("GalleryName", pm.getName());
			gallery.setParameter("Description", "");
			gallery.setParameter("Active", "Y");
			gallery.setParameter("Publisher", "" + pm.getPublisher());
			gallery.setParameter("CreatedBy", userID);
			gallery.setParameter("CreatedDate", "NOW");
			gallery.setParameter("ModifiedBy", userID);
			gallery.setParameter("ModifiedDate", "NOW");
			gallery.doAction(GenerationKeys.INSERT);
			gallery.close();
		}

		// check if the planimage is present in the gallery or not, if not add
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductHouseAndLandView");
		row.setConnection(con);
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");

		GenRow linkedDocuments = new GenRow();
		linkedDocuments.setViewSpec("LinkedDocumentView");
		linkedDocuments.setConnection(con);
		linkedDocuments.setParameter("GalleryID", galleryID);
		linkedDocuments.setParameter("DocumentID", row.getString("PlanImage"));
		linkedDocuments.doAction("selectfirst");

		// Add the plan image to the gallery
		if (!linkedDocuments.isSuccessful()
				&& "House and Land".equals(row.getString("ProductType"))) {
			linkedDocuments.setParameter("LinkedDocumentID",
					KeyMaker.generate());
			linkedDocuments.setParameter("ShowOnGalleryOnly", "Y");
			linkedDocuments.setParameter("ShowOnPortal", "Y");
			linkedDocuments.setParameter("ShowOnREorder", "1");
			linkedDocuments.setParameter("CreatedBy", userID);
			linkedDocuments.setParameter("CreatedDate", "NOW");
			linkedDocuments.doAction(GenerationKeys.INSERT);
		}

		if (StringUtils.isNotBlank(heroImageID)) {

			linkedDocuments.clear();
			linkedDocuments.setConnection(con);
			linkedDocuments.setParameter("GalleryID", galleryID);
			linkedDocuments.setParameter("DocumentID", heroImageID);
			linkedDocuments.doAction("selectfirst");

			// Add the hero image to the gallery
			if (!linkedDocuments.isSuccessful()) {
				linkedDocuments.setParameter("LinkedDocumentID",
						KeyMaker.generate());
				linkedDocuments.setParameter("ShowOnGalleryOnly", "Y");
				linkedDocuments.setParameter("ShowOnPortal", "Y");
				linkedDocuments.setParameter("ShowOnREorder", "0");
				linkedDocuments.doAction(GenerationKeys.INSERT);
			}
		}
	}

	public class PackageRule {
		/*
		 * `PackageRuleID` VARCHAR(50) NOT NULL, `Name` VARCHAR(150),
		 * `ConditionType` VARCHAR(50), `ConditionDim` VARCHAR(50),
		 * `ConditionValue` VARCHAR(50), `ConditionCalc` VARCHAR(150),
		 * `FromType` VARCHAR(50), `FromDim` VARCHAR(50), `FromValue`
		 * VARCHAR(50), `FromCalc` VARCHAR(150), `Compare` VARCHAR(5), `ToType`
		 * VARCHAR(50), `ToDim` VARCHAR(50), `ToValue` VARCHAR(50), `ToCalc`
		 * VARCHAR(150), PRIMARY KEY (`PackageRuleID`)
		 */

		public String packageRuleID = "";

		public String name = "";

		public String conditionType = "";
		public String conditionDim = "";
		public String conditionValue = "";
		public String conditionCalc = "";

		public String fromType = "";
		public String fromDim = "";
		public String fromValue = "";
		public String fromCalc = "";

		public String toType = "";
		public String toDim = "";
		public String toValue = "";
		public String toCalc = "";

		public String compare = "";

		public PackageRule(GenRow row) {
			name = row.getString("Name");

			packageRuleID = row.getString("PackageRuleID");

			conditionType = row.getString("ConditionType");
			conditionDim = row.getString("ConditionDim");
			conditionValue = row.getString("ConditionValue");
			conditionCalc = row.getString("ConditionCalc");

			fromType = row.getString("FromType");
			fromDim = row.getString("FromDim");
			fromValue = row.getString("FromValue");
			fromCalc = row.getString("FromCalc");

			toType = row.getString("ToType");
			toDim = row.getString("ToDim");
			toValue = row.getString("ToValue");
			toCalc = row.getString("ToCalc");

			compare = row.getString("Compare");
		}

	}

	public boolean isKeepRep() {
		return keepRep;
	}

	public void setKeepRep(boolean keepRep) {
		this.keepRep = keepRep;
	}

	public boolean isKeepDates() {
		return keepDates;
	}

	public void setKeepDates(boolean keepDates) {
		this.keepDates = keepDates;
	}

	public boolean isKeepPublish() {
		return keepPublish;
	}

	public void setKeepPublish(boolean keepPublish) {
		this.keepPublish = keepPublish;
	}

	public boolean isKeepStatus() {
		return keepStatus;
	}

	public void setKeepStatus(boolean keepStatus) {
		this.keepStatus = keepStatus;
	}

	public boolean isUpdateCosts() {
		return updateCosts;
	}

	public void setUpdateCosts(boolean keepCosts) {
		this.updateCosts = keepCosts;
	}

	public boolean isUpdatePrice() {
		return updatePrice;
	}

	public void setUpdatePrice(boolean keepPrice) {
		this.updatePrice = keepPrice;
	}

	public boolean isUpdatePlan() {
		return updatePlan;
	}

	public void setUpdatePlan(boolean keepPrice) {
		this.updatePlan = keepPrice;
	}

	public boolean isRecreateTask() {
		return recreateTask;
	}

	public String getToken() {
		return (sequenceNum != null) ? sequenceNum : "";
	}

	public void setToken(String token) {
		this.sequenceNum = token;
	}

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}

	public ArrayList<String> getHouselandids() {
		return houselandids;
	}

	public void setHouselandids(ArrayList<String> houselandids) {
		this.houselandids = houselandids;
	}

	public ArrayList<String> getHouseproductids() {
		return houseproductids;
	}

	public void setHouseproductids(ArrayList<String> houseproductids) {
		this.houseproductids = houseproductids;
	}

	public ArrayList<String> getLandproductids() {
		return landproductids;
	}

	public void setLandproductids(ArrayList<String> landproductids) {
		this.landproductids = landproductids;
	}

	public HashMap<String, String> getSelectedfacades() {
		return selectedfacades;
	}

	public void setSelectedfacades(HashMap<String, String> selectedfacades) {
		this.selectedfacades = selectedfacades;
	}

	public void setUserID(String userID) {
		this.userid = userID;
	}

	public String getErros() {
		return failedMessages.toString();
	}

	public int getMaxPackages() {
		return maxPackages;
	}

	public void parseRequestOptions(GenRow row) {
		if ("true".equals(row.getString("KeepBad")))
			keepBad = true;
		if ("true".equals(row.getString("ResetContents")))
			resetContents = true;
		if ("true".equals(row.getString("KeepFacade")))
			keepFacade = true;
		if ("true".equals(row.getString("UpdatePrice"))) {
			updatePrice = true;
			if ("true".equals(row.getString("UpdateCosts")))
				updateCosts = true;
			if ("true".equals(row.getString("UpdateOptions")))
				updateOptions = true;
			if ("true".equals(row.getString("UpdateFeatures")))
				updateFeatures = true;
			if ("true".equals(row.getString("UpdateDrips")))
				updateDrips = true;
			if ("true".equals(row.getString("UpdatePlan")))
				updatePlan = true;
			if ("true".equals(row.getString("UpdateDim")))
				updateDims = true;
		}

		if ("true".equals(row.getString("UpdatePublishHeadline")))
			updatePublishHeadline = true;
		if ("true".equals(row.getString("UpdatePublishDescription")))
			updatePublishDescription = true;
		if ("true".equals(row.getString("UpdateImages")))
			updateImages = true;
		if ("true".equals(row.getString("isPortal")))
			isPortal = true;
	}

	// Used in packageportal.jsp as well
	public static GenRow insertProductPublishing(Connection conn,
			String productID, String pm, String userID) {
		// GenRow product = getProduct(request, "ProductView", productID);
		PropertyEntity product = PropertyFactory.getPropertyEntity(conn,
				productID);

		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductPublishing");
		row.setConnection(conn);
		row.createNewID();
		row.setColumn("ProductID", productID);
		row.setColumn("PublishMethod", pm);
		row.setColumn("PublishStatus", "Approved");
		row.setColumn("Summary", product.getString("Summary"));
		row.setColumn("Description", product.getString("PublishDescription"));
		row.setColumn("HeroImageID", product.getString("Image"));
		row.setColumn("CreatedDate", "now()");
		row.setColumn("ModifiedDate", "now()");
		row.setColumn("ModifiedBy", userID);
		row.setColumn("CreatedBy", userID);

		PropertyType pt = PropertyType.getPropertyType(product
				.getString("ProductType"));
		switch (pt) {
		case HouseLandPackage:
			row.setColumn("HomeProductID", product.getString("HomeProductID"));
			row.setColumn("EstateProductID",
					product.getString("EstateProductID"));
			row.setColumn("LotProductID", product.getString("LotProductID"));
			break;
		case Land:
			row.setColumn("EstateProductID",
					product.getString("EstateProductID"));
			break;
		case Apartment:
			row.setColumn("BuildingProductID",
					product.getString("BuildingProductID"));
			break;
		}

		row.doAction("insert");

		return row;
	}

	public int getFailedPackages() {
		// TODO Auto-generated method stub
		return failedRules.get();
	}

	public int getDuplcatePackages() {
		// TODO Auto-generated method stub
		return failedDuplicates.get();
	}

	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public void setPortal(boolean isPortal) {
		this.isPortal = isPortal;
		
		includeBuilderGroups = "true".equals(InitServlet.getSystemParam("RPM-HNL-IncludeBuilderGroups"));
		includeBrandGroups = "true".equals(InitServlet.getSystemParam("RPM-HNL-IncludeBrandGroups"));
		includeRangeGroups = "true".equals(InitServlet.getSystemParam("RPM-HNL-IncludeRangeGroups")); 	
		includeDesignGroups = "true".equals(InitServlet.getSystemParam("RPM-HNL-IncludeDesignGroups"));
		includePlanGroups = "true".equals(InitServlet.getSystemParam("RPM-HNL-IncludePlanGroups"));
		includeDeveloperGroups = "true".equals(InitServlet.getSystemParam("RPM-HNL-IncludeDeveloperGroups"));
		includeEstateGroups = "true".equals(InitServlet.getSystemParam("RPM-HNL-IncludeEstateGroups"));
		includeStageGroups = "true".equals(InitServlet.getSystemParam("RPM-HNL-IncludeStageGroups"));
		includeLandGroups = "true".equals(InitServlet.getSystemParam("RPM-HNL-IncludeLandGroups"));
		
	}
	
	public boolean isPortal() {
		return isPortal;
	}

}
