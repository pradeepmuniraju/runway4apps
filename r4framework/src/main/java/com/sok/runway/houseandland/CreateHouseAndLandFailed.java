/**
 * 
 */
package com.sok.runway.houseandland;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;

/**
 * @author Puggs
 *
 */
public class CreateHouseAndLandFailed extends CreateHouseAndLand {

	private static final Logger logger = LoggerFactory
			.getLogger(CreateHouseAndLand.class);
	
	/**
	 * @param request
	 * @param context
	 */
	public CreateHouseAndLandFailed(HttpServletRequest request,
			ServletContext context) {
		super(request, context);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param contextpath
	 * @param locale
	 * @param context
	 */
	public CreateHouseAndLandFailed(String contextpath, Locale locale,
			ServletContext context) {
		super(contextpath, locale, context);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param row
	 */
	public CreateHouseAndLandFailed(GenRow row) {
		super(row);
		
		try {
			super.maxPackages = row.getInt("PackageCount");
		} catch (Exception e) {}
		super.failedMessages = new StringBuffer().append(row.getString("Errors"));
		
		GenRow items = new GenRow();
		items.setTableSpec("ProductTaskItems");
		items.setParameter("-select0", "PackageID");
		items.setParameter("TaskID",row.getString("TaskID"));
		items.doAction("search");
		
		items.getResults();
		
		houselandids = new ArrayList<String>();
		
		while (items.getNext()) {
			houselandids.add(items.getString("PackageID"));
			if ("Y".equals(items.getString("Passed"))) passedRules.addAndGet(1);
			else if ("N".equals(items.getString("Passed"))) failedRules.addAndGet(1);
			else failedDuplicates.addAndGet(1);
		}
		
		items.close();

	}
	
	@SuppressWarnings("unchecked")
	public String getProgressJSON() {
		JSONObject obj = new JSONObject();
		obj.put("currentPackage", maxPackages);
		obj.put("currentStep", 0);
		obj.put("processingPackage", 0);
		obj.put("maxPackage", maxPackages);
		obj.put("maxSteps", 0);
		obj.put("packageTime", 0);
		obj.put("packageTotal", 0);
		obj.put("remainingTotal", 0);
		obj.put("takenTotal", 0);
		obj.put("duplicatesSkiped", failedDuplicates.get());
		obj.put("passedRules", passedRules.get());
		obj.put("failedRules", failedRules.get());
		obj.put("recreateTask", 0);
		obj.put("completed", "true");
		obj.put("message","The Task, " + getToken() + " has failed as no packages can be created from the data provided.");
		
		logger.debug(obj.toJSONString());
		
		return obj.toJSONString();
	}
	
	public List<String> getCreatedpackages() {
		return null;
	}

}
