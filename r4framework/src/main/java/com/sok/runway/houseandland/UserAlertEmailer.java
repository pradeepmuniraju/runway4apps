package com.sok.runway.houseandland;

import java.util.Properties;

import javax.servlet.ServletConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.InitServlet;
import com.sok.framework.MailerBean;
import com.sok.framework.RowBean;

public class UserAlertEmailer {
	private static final Logger logger = LoggerFactory.getLogger(UserAlertEmailer.class);

	public static void sendUserEmail(String toEmailIDs, String message) {
		Properties sysprops = InitServlet.getSystemParams();

		String systemuserid = sysprops.getProperty("SystemUserID");
		String urlhome = sysprops.getProperty("URLHome");
		String defaultFrom = sysprops.getProperty("DefaultEmail");
		String emailalertsurlhome = sysprops.getProperty("EmailAlertsURLHome", urlhome);

		try {

			RowBean context = new RowBean();
			context.put("URLHome", urlhome);
			context.put("EmailAlertsURLHome", emailalertsurlhome);
			context.put("alert", toEmailIDs);

			ServletConfig sc = InitServlet.getConfig();
			if (sc != null && toEmailIDs.length() != 0) {
				MailerBean mailer = new MailerBean();
				mailer.setServletContext(sc.getServletContext());
				mailer.setFrom(defaultFrom);
				mailer.setTo(toEmailIDs);
				mailer.setSubject("H&L Package Notification");
				mailer.setTextbody(message);
				mailer.setHtmlbody(message.toString().replaceAll("\\n", "<br />"));
				String response = mailer.getResponse();
				logger.debug("email to " + toEmailIDs + " Response: " + response);
			}
		} catch (Exception e) {
			logger.error("Exception " + e);
		}
	}
}