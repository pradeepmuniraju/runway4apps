package com.sok.runway.houseandland;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.DisplaySpec;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.SpecManager;
import com.sok.framework.StringUtil;
import com.sok.framework.TableSpec;
import com.sok.runway.ItemLists;
import com.sok.runway.SingleItemList;
import com.sok.runway.UserBean;
import com.sok.runway.crm.Product;
import com.sok.runway.offline.rpmManager.RPMtask;

public class NewLotAction {

	private static final Logger log = LoggerFactory.getLogger(NewLotAction.class);

	protected void insertProductVariation(HttpServletRequest request, String parentproductid, String childproductid, String type) {
		if (parentproductid == null || parentproductid.length() < 5 || childproductid == null || childproductid.length() < 5)
			return;
		GenRow productvariation = new GenRow();
		productvariation.setRequest(request);
		productvariation.setTableSpec("ProductVariations");
		productvariation.setParameter("ParentProductID", parentproductid);
		productvariation.setParameter("ChildProductID", childproductid);
		productvariation.setParameter("Type", type);
		productvariation.createNewID();
		productvariation.doAction("insert");
	}

	protected void updateProductVariation(HttpServletRequest request, String parentproductid, String childproductid, String type) {
		if (parentproductid == null || parentproductid.length() < 5 || childproductid == null || childproductid.length() < 5)
			return;
		GenRow productvariation = new GenRow();
		productvariation.setRequest(request);
		productvariation.setTableSpec("ProductVariations");
		productvariation.setParameter("-select1", "ProductVariationID");
		productvariation.setParameter("ChildProductID", childproductid);
		productvariation.setParameter("Type", type);
		productvariation.doAction("selectfirst");
		if (productvariation.isSuccessful()) {
			productvariation.setParameter("ProductVariationID", productvariation.getData("ProductVariationID"));
			productvariation.setParameter("ParentProductID", parentproductid);
			productvariation.doAction("update");
		} else {
			insertProductVariation(request, parentproductid, childproductid, type);
		}
	}

	public String copy(HttpServletRequest request, String tablespecname, String productidfieldname, String fromproductid, String toproductid, String userid) {
		return copy(request, tablespecname, productidfieldname, fromproductid, toproductid, userid, "Land", "");
	}
	
	public String copy(HttpServletRequest request, String tablespecname, String productidfieldname, String fromproductid, String toproductid, String userid, String productType, String category) {

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);
		row.setRequest(request);
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.setRequest(request);
			clone.putAllDataAsParameters(row);
			if ("Products".equals(row.getTableSpec().getTableName())) {
				if (row.getString("Name").indexOf("copy") == -1)
					clone.setParameter("Name", row.getString("Name") + " copy");
				clone.setParameter("ProductType", productType);
				clone.setParameter("CopiedFromProductID", fromproductid);
				clone.setParameter("Category", category);
				clone.setParameter("UpdateByProcess", "Y");
			}
			clone.setParameter("CreatedDate", "NOW");
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("CreatedBy", userid);
			clone.setParameter("ModifiedBy", userid);
			clone.remove(row.getTableSpec().getPrimaryKeyName());
			clone.createNewID();
			if(StringUtils.isNotBlank(toproductid)) {
				clone.setParameter(productidfieldname, toproductid);
			}
			
			clone.doAction("insert");
		}
		row.close();
		return (clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	public String copyCostings(HttpServletRequest request, String fromproductid, String toproductid, String userid) {

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setRequest(request);
		row.setParameter("ProductID", fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec("ProductProducts");
			clone.setRequest(request);
			clone.putAllDataAsParameters(row);
			// if it is a Package Cost then clone that and use the clone
			if ("Package Cost".equals(row.getString("ProductType"))) {
				String newproductid = KeyMaker.generate();
				copyplan(request, "Products", "ProductID", row.getString("LinkedProductID"), newproductid, userid);
				clone.setParameter("LinkedProductID", newproductid);
			}
			clone.setParameter("CreatedDate", "NOW");
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("CreatedBy", userid);
			clone.setParameter("ModifiedBy", userid);
			clone.setParameter("ProductID", toproductid);
			clone.createNewID();
			clone.doAction("insert");
		}
		row.close();
		return "";
	}

	public String copyplan(HttpServletRequest request, String tablespecname, String productidfieldname, String fromproductid, String toproductid, String userid) {

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);
		row.setRequest(request);
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();
		// System.out.println("row " + row.getStatement());

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.setRequest(request);
			clone.putAllDataAsParameters(row);
			clone.setParameter("CopiedFromProductID", fromproductid);
			clone.setParameter("CreatedDate", "NOW");
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("CreatedBy", userid);
			clone.setParameter("ModifiedBy", userid);
			clone.remove(row.getTableSpec().getPrimaryKeyName());
			clone.createNewID();
			clone.setParameter(productidfieldname, toproductid);
			clone.doAction("insert");
			// System.out.println("clone " + clone.getStatement());
		}
		row.close();
		return (clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	void updateProductStatus(HttpServletRequest request, String productid, String productstatusid) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setRequest(request);
		row.setParameter("ProductStatusID", productstatusid);
		row.setParameter("ProductID", productid);
		row.doAction(ActionBean.UPDATE);
	}

	void updateAvailableDate(HttpServletRequest request, String productid) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setRequest(request);
		row.setParameter("AvailableDate", "NOW");
		row.setParameter("ProductID", productid);
		row.doAction(ActionBean.UPDATE);
	}

	public void calcHomeDetails(HttpServletRequest request, String productID) {
		GenRow home = new GenRow();
		home.setTableSpec("properties/HomeDetails");
		home.setRequest(request);
		home.setParameter("-select", "*");
		home.setParameter("ProductID", productID);
		home.doAction("selectfirst");

		boolean doUpdate = false;
		try {
			double buildWidth = Double.parseDouble(home.getColumn("LotWidth")) - (Double.parseDouble((home.getColumn("LeftSetback").length() > 0) ? home.getColumn("LeftSetback") : "0") + Double.parseDouble((home.getColumn("RightSetback").length() > 0) ? home.getColumn("RightSetback") : "0"));
			home.setColumn("BuildWidth", "" + buildWidth);
			doUpdate = true;
		} catch (Exception e) {
		}
		try {
			double buildDepth = Double.parseDouble(home.getColumn("LotDepth")) - (Double.parseDouble((home.getColumn("FrontSetback").length() > 0) ? home.getColumn("FrontSetback") : "0") + Double.parseDouble((home.getColumn("RearSetback").length() > 0) ? home.getColumn("RearSetback") : "0"));
			home.setColumn("BuildDepth", "" + buildDepth);
			doUpdate = true;
		} catch (Exception e) {
		}

		if (doUpdate) home.doAction("update");
	}

	public void processProductDetails(HttpServletRequest request, ItemLists itemlists, String productID) {
		TableSpec detailsSpec = SpecManager.getTableSpec("properties/ProductDetails");
		SingleItemList detailTypes = itemlists.getList("LandDetails");
		String ignorelist = "ProductID,CopiedFromProductID,SortOrder";
		
		for (int d = 0; d < detailTypes.size(); ++d) {
			if (request.getParameter("action_" + d) == null || request.getParameter("action_" + d).length() == 0)
				continue;
			GenRow details = new GenRow();
			details.setTableSpec("properties/ProductDetails");
			details.setRequest(request);
			details.setColumn("ProductID", productID);
			details.setColumn("SortOrder", "" + d);
			for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
				if (ignorelist.indexOf(detailsSpec.getFieldName(cs)) == -1) {
					String columnValue = request.getParameter(detailsSpec.getFieldName(cs) + "_" + d);
					if (StringUtils.isNotBlank(columnValue)) {						
						details.setColumn(detailsSpec.getFieldName(cs), columnValue);
					}
				}
			}
			String action = request.getParameter("action_" + d);
			if (details.getColumn("ProductDetailsID").length() == 0) {
				details.setColumn("ProductDetailsID", KeyMaker.generate());
				action = "insert";
			} else {
				action = "update";
			}
			try {
				details.doAction(action);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		TableSpec homeDetailsSpec = SpecManager.getTableSpec("properties/HomeDetails");
		Map<String, String> map = getDetailsMap();
		HashMap<String, String> dimensions = getDimensions(request, productID, "Land");
		GenRow home = new GenRow();
		home.setRequest(request);
		home.setTableSpec("properties/HomeDetails");
		home.setParameter("ProductID", productID);

		for (int cs = 0; cs < homeDetailsSpec.getFieldsLength(); ++cs) {
			if (map.containsKey(homeDetailsSpec.getFieldName(cs))) {
				if (dimensions.containsKey(map.get(homeDetailsSpec.getFieldName(cs)))) {
					home.setParameter(homeDetailsSpec.getFieldName(cs), dimensions.get(map.get(homeDetailsSpec.getFieldName(cs))));
				}
			}
		}

		try {
			home.doAction("insert");
		} catch (Exception e) {
			home.doAction("update");
		}

	}

	public boolean service(HttpServletRequest request) throws java.io.IOException, ServletException {

		log.debug("service entered");
		HttpSession session = null;

		session = request.getSession();
		ItemLists itemlists = null;
		synchronized (session) {

			itemlists = (ItemLists) session.getAttribute("itemlists");

			if (itemlists == null) {
				itemlists = new ItemLists();
				session.setAttribute("itemlists", itemlists);
			}
		}
		UserBean currentuser = null;
		synchronized (session) {
			currentuser = (UserBean) session.getAttribute("currentuser");
			if (currentuser == null) {
				currentuser = new UserBean();
				session.setAttribute("currentuser", currentuser);
			}
		}

		String action = request.getParameter("-action");

		Properties sysprops = InitServlet.getSystemParams();

		String maxareadoflandrequired = sysprops.getProperty("MaxAreaOfLandRequired");
		int inxmaxareoflandrequired = 0;
		if (maxareadoflandrequired != null && !"".equals(maxareadoflandrequired)) {
			inxmaxareoflandrequired = Integer.parseInt(maxareadoflandrequired);
		}

		Product product = new Product(request);
		request.setAttribute("product", product);

		if (ActionBean.INSERT.equals(action)) {

			product.insert();
			
			insertAddress(request, product.getPrimaryKey());
			processProductDetails(request, itemlists, product.getPrimaryKey());

			GenRow lotProduct = new GenRow();
			lotProduct.setTableSpec("Products");
			lotProduct.setParameter("ProductID", product.getPrimaryKey());
			lotProduct.setParameter("ProductNum", KeyMaker.getRandomNumberString(6));
			lotProduct.doAction(ActionBean.UPDATE);

			String productid = product.getPrimaryKey();
			
			String estateProductID = request.getParameter("EstateProductID");
			String stageID = request.getParameter("StageProductID");
			if (stageID == null || stageID.length() == 0) stageID = request.getParameter("ProductParentStageLot.ParentProductID");
			if (stageID != null && stageID.length() != 0) {
				if("AddCustomStage".equals(stageID)) {					
					
					log.debug("Inserting Custom Stage");
					stageID = insertCustomStage(request, currentuser);
					log.debug("Inserting Custom Stage ProductVariation");
					insertProductVariation(request, estateProductID, stageID, "Estate");
					log.debug("Copying Custom Stage ProductSecurityGroups");
					copy(request, "ProductSecurityGroups", "ProductID", estateProductID, stageID, currentuser.getString("UserID"));
				}
				
				log.debug("Inserting Custom Stage");
				insertProductVariation(request, stageID, productid, "Stage");
				
				log.debug("Copying PacakgeCosts");
				copyPacakgeCostsOrRules(request, productid, currentuser, "Package Cost");
				log.debug("Copying Rules");
				copyPacakgeCostsOrRules(request, productid, currentuser, "Package Rules");
				
				copy(request, "ProductSecurityGroups", "ProductID", stageID, productid, currentuser.getString("UserID"));
			}

			//calcHomeDetails(request, product.getPrimaryKey());
			/*
			GenRow plan = new GenRow();
			plan.setTableSpec("properties/HomeDetails");
			plan.setRequest(request);
			plan.setParameter("ProductID", productid);

			String lotArea = product.getField("Area");

			if (inxmaxareoflandrequired != 0 && lotArea != null && !"".equals(lotArea)) {
				double intLotArea = Double.parseDouble(lotArea);
				double calcWillFitHouseArea = (intLotArea * inxmaxareoflandrequired) / 100;
				calcWillFitHouseArea = intLotArea - calcWillFitHouseArea;
				plan.setParameter("WillFitHouseSize", calcWillFitHouseArea);
				try {
					plan.doAction(ActionBean.INSERT);
				} catch (Exception e) {
					plan.doAction(ActionBean.UPDATE);
				}
			}
			*/
			// Add to quick index
			RPMtask rpmTask = RPMtask.getInstance();
			if(rpmTask != null) {
				RPMtask.getInstance().refreshData(RPMtask.type.land);
				RPMtask.getInstance().refreshIndex("update",product.getPrimaryKey(),"Land");
				RPMtask.getInstance().hourlyLotProcessing(product.getPrimaryKey());
			}
			
			request.setAttribute("ProductID", product.getPrimaryKey());
			log.debug("product id " + product.getPrimaryKey());
			return true;
		} else {
			log.debug("return false");
			return false;
		}
	}

	private void insertAddress(HttpServletRequest request, String productID) {
		GenRow address = new GenRow();
		address.setTableSpec("Addresses");
		address.parseRequest(request);
		
		if (StringUtil.isBlankOrEmpty(address.getString("Latitude"))) {
			address.remove("Latitude");
		}
		if (StringUtil.isBlankOrEmpty(address.getString("Longitude"))) {
			address.remove("Longitude");
		}
		
		address.createNewID();
		address.setParameter("ProductID", productID);
		address.doAction("insert");
		address.close();
	}

	private String insertCustomStage(HttpServletRequest request, UserBean currentuser) {
		String stageID = KeyMaker.generate();
		GenRow stageProduct = new GenRow();
		stageProduct.setRequest(request);
		stageProduct.setTableSpec("Products");
		stageProduct.setParameter("ProductID", stageID);
		stageProduct.setParameter("Active", "Y");
		stageProduct.setParameter("ProductType", "Stage");
		stageProduct.setParameter("Name", "Custom Stage");
		stageProduct.setParameter("Description", "This is a custom stage");
		stageProduct.setParameter("CreatedDate", "NOW");
		stageProduct.setParameter("ModifiedDate", "NOW");
		stageProduct.setParameter("CreatedBy", currentuser.getString("UserID"));
		stageProduct.setParameter("ModifiedBy", currentuser.getString("UserID"));
		stageProduct.doAction(ActionBean.INSERT);
		stageProduct.close();
		return stageID;
	}
	
	/** Copies packages costs from parent estate and stage
	 * @param currentuser
	 */
	private void copyPacakgeCostsOrRules(HttpServletRequest request, String lotid, UserBean currentuser, String type) {
		String userID = currentuser.getString("UserID");

		GenRow lotbean = new GenRow();
		lotbean.setRequest(request);
		lotbean.setViewSpec("properties/ProductLotView");
		lotbean.setParameter("ProductID", lotid);
		lotbean.doAction("selectfirst");

		GenRow stagelinkedproducts = new GenRow();
		stagelinkedproducts.setViewSpec("ProductLinkedProductView");

		GenRow estatelinkedproducts = new GenRow();
		estatelinkedproducts.setViewSpec("ProductLinkedProductView");

		GenRow lotlinkedproducts = new GenRow();
		lotlinkedproducts.setViewSpec("ProductLinkedProductView");

		GenRow row = new GenRow();
		row.setRequest(request);
		row.setTableSpec("ProductProducts");

		String stageID = lotbean.getString("StageProductID");
		String estateID = lotbean.getString("EstateProductID");
		
		log.debug("LotID {} ", lotid);
		log.debug("estateID {} stageID {} ", estateID, stageID);
		
		
		stagelinkedproducts.clear();
		stagelinkedproducts.setRequest(request);
		stagelinkedproducts.setColumn("ProductID", stageID);
		stagelinkedproducts.setColumn("ProductProductsLinkedProduct.Active", "Y");
		stagelinkedproducts.setColumn("ProductProductsLinkedProduct.ProductType", type);
		stagelinkedproducts.sortBy("ProductProductsLinkedProduct.Name", 0);
		stagelinkedproducts.doAction("search");
		stagelinkedproducts.getResults(true);

		estatelinkedproducts.clear();
		estatelinkedproducts.setRequest(request);
		estatelinkedproducts.setColumn("ProductID", estateID);
		estatelinkedproducts.setColumn("ProductProductsLinkedProduct.Active", "Y");
		estatelinkedproducts.setColumn("ProductProductsLinkedProduct.ProductType", type);
		estatelinkedproducts.sortBy("ProductProductsLinkedProduct.Name", 0);
		estatelinkedproducts.doAction("search");
		estatelinkedproducts.getResults(true);

		GenRow productProductBean = new GenRow();
		productProductBean.setTableSpec("ProductProducts");
		productProductBean.setParameter("-select0", "LinkedProductID");
		productProductBean.setParameter("ProductID", lotid);
		productProductBean.doAction("search");
		productProductBean.getResults();

		ArrayList<String> existingPackageCostList = new ArrayList<String>();

		GenRow copiedFromBean = new GenRow();
		copiedFromBean.setTableSpec("Products");
		while (productProductBean.getNext()) {
			copiedFromBean.clear();
			copiedFromBean.setParameter("-select0", "PackageCostProductID");
			copiedFromBean.setParameter("ProductID", productProductBean.getString("LinkedProductID"));
			copiedFromBean.doAction("search");
			copiedFromBean.getResults();
			while (copiedFromBean.getNext()) {
				String copiedFromPID = copiedFromBean.getString("PackageCostProductID");
				if (copiedFromPID != null && !"".equals(copiedFromPID)) {
					existingPackageCostList.add(copiedFromPID);
				}
			}
		}
		
		log.trace("stagelinkedproducts query: " + stagelinkedproducts.getStatement());
		
		while (stagelinkedproducts.getNext()) {
			GenRow pcproductBean = new GenRow();
			pcproductBean.setTableSpec("Products");
			pcproductBean.setParameter("-select0", "PackageCostProductID");
			pcproductBean.setParameter("-select1", "Category");
			pcproductBean.setParameter("ProductID", stagelinkedproducts.getColumn("LinkedProductID"));
			pcproductBean.doAction("selectfirst");
			// out.println("hi:"+existingPackageCostList.contains(pcproductBean.getString("PackageCostProductID")));
			if ((existingPackageCostList != null && existingPackageCostList.isEmpty()) || (existingPackageCostList != null && !existingPackageCostList.isEmpty() && !existingPackageCostList.contains(pcproductBean.getString("PackageCostProductID")))) {
				String newProductID = copy(request, "Products", "ProductID", stagelinkedproducts.getColumn("LinkedProductID"), "", currentuser.getString("UserID"), type, pcproductBean.getString("Category"));
				row.clear();
				row.setParameter("LinkedProductID", newProductID);
				row.setParameter("ProductID", lotid);
				row.setParameter("CreatedDate", "NOW");
				row.setParameter("ModifiedDate", "NOW");
				row.setParameter("CreatedBy", userID);
				row.setParameter("ModifiedBy", userID);
				row.createNewID();
				row.doAction("insert");
				log.trace("calling updateTotalCost newProductID {} , lotid {} " , newProductID ,lotid);
				updateTotalCost(request, newProductID, lotid);
				existingPackageCostList.add(pcproductBean.getString("PackageCostProductID"));
			}
			pcproductBean.close();
		}

		while (estatelinkedproducts.getNext()) {
			GenRow pcproductBean = new GenRow();
			pcproductBean.setTableSpec("Products");
			pcproductBean.setParameter("-select0", "PackageCostProductID");
			pcproductBean.setParameter("-select1", "Category");
			pcproductBean.setParameter("ProductID", estatelinkedproducts.getColumn("LinkedProductID"));
			pcproductBean.doAction("selectfirst");

			if ((existingPackageCostList != null && existingPackageCostList.isEmpty()) || (existingPackageCostList != null && !existingPackageCostList.isEmpty() && !existingPackageCostList.contains(pcproductBean.getString("PackageCostProductID")))) {
				String newProductID = copy(request, "Products", "ProductID", estatelinkedproducts.getColumn("LinkedProductID"), "", currentuser.getString("UserID"), type, pcproductBean.getString("Category"));
				row.clear();
				row.setParameter("LinkedProductID", newProductID);
				row.setParameter("ProductID", lotid);
				row.setParameter("CreatedDate", "NOW");
				row.setParameter("ModifiedDate", "NOW");
				row.setParameter("CreatedBy", userID);
				row.setParameter("ModifiedBy", userID);
				row.createNewID();
				row.doAction("insert");
				updateTotalCost(request, newProductID, lotid);
				existingPackageCostList.add(pcproductBean.getString("PackageCostProductID"));
			}
			pcproductBean.close();
		}

		productProductBean.close();
		copiedFromBean.close();

		stagelinkedproducts.close();
		estatelinkedproducts.close();
		row.close();
	}
	
	private void updateTotalCost(HttpServletRequest request, String productID, String lotProductID) {
		GenRow products = new GenRow();
		products.setTableSpec("Products");
		products.setParameter("-select0", "PackageCostCalcField");
		products.setParameter("-select1", "Cost");
		products.setParameter("ProductID", productID);
		products.doAction("selectfirst");

		String pckgCostCalcField = products.getString("PackageCostCalcField");

		// multi costings for lots will be done at the creation of house and land packages.
		if (pckgCostCalcField != null && (pckgCostCalcField.startsWith("Lot") || (pckgCostCalcField).startsWith("Perimeter"))) {
			GenRow homeDetails = new GenRow();
			homeDetails.setRequest(request);
			homeDetails.setTableSpec("properties/HomeDetails");
			if ("LotArea".equals(pckgCostCalcField)) {
				homeDetails.setParameter("-select0", "Area As packcostfield");
			} else {
				homeDetails.setParameter("-select0", pckgCostCalcField + " As packcostfield");
			}
			homeDetails.setParameter("ProductID", lotProductID);
			homeDetails.doAction("selectfirst");
			
			log.debug("homeDetails query : {}, {} " , homeDetails.getStatement() , homeDetails.getString("packcostfield"));
			
			if (homeDetails.getString("packcostfield") != null && !"0".equals(homeDetails.getString("packcostfield")) && !"null".equals(homeDetails.getString("packcostfield")) && products.getString("Cost") != null) {
				try {
					double productCost = Double.parseDouble(products.getString("Cost"));
					double packcostfield = Double.parseDouble(homeDetails.getString("packcostfield"));
					double totalCost = (productCost * packcostfield);
					products.clear();
					products.setParameter("ProductID", productID);
					products.setParameter("TotalCost", totalCost);
					products.doAction("update");
				} catch (Exception e) {					
					log.error("Error updating the total cost" + e);
					e.printStackTrace();
				}
			}
			homeDetails.close();
		}
		products.close();
	}
	
	public Map<String, String> getDetailsMap() {
		Map<String, String> map = new HashMap<String, String>();

		DisplaySpec dspecLot = SpecManager.getDisplaySpec("ProductLotViewDisplay");

		String[] names = dspecLot.getItemNames();
		for (int d = 0; d < names.length; ++d) {
			map.put(names[d], dspecLot.getLocalItemFieldName(names[d]));

		}

		return map;
	}
	

	HashMap<String, String> getDimensions(HttpServletRequest request, String productid, String type) {
		HashMap<String, String> dimMap = new HashMap<String,String>();
		if (productid == null || productid.length() == 0 || type == null || type.length() == 0) return dimMap;
		
		String ignorelist = "ProductDetailsID,ProductID,CopiedFromProductID,ProductType,DetailsType,SortOrder";
		
		TableSpec detailsSpec = SpecManager.getTableSpec("properties/ProductDetails");
		
		GenRow details = new GenRow();
		details.setTableSpec("properties/ProductDetails");
		details.setRequest(request);
		details.setParameter("-select","*");
		details.setColumn("ProductID",productid);
		details.sortBy("SortOrder",0);
		details.sortBy("DetailsType",1);
		if (details.getString("ProductID").length() > 0) {
			details.doAction("search");
			details.getResults(true);
		}
		
		int d = 0;
		while (details.getNext()) {
			for (int cs = 0; cs < detailsSpec.getFieldsLength(); ++cs) {
				if (ignorelist.indexOf(detailsSpec.getFieldName(cs)) == -1) {
					dimMap.put((type.replaceAll(" ","") + "." + details.getString("DetailsType") + "." + detailsSpec.getFieldName(cs)), details.getString(detailsSpec.getFieldName(cs)));
				}
			}
			++d;
		}
		details.close();
		return dimMap;
		
	}
} 