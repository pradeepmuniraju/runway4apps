package com.sok.runway;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class XMLUtil { 
   private static final XMLUtil XML_UTIL_INSTANCE;
   private static final TagSelector TAG_SELECTOR;
   private static final AttributeSelector ATTRIBUTE_SELECTOR;
   private static final TextSelector TEXT_SELECTOR;
   
   private static final String WILD_CARD = "*";
   
   static {
      XML_UTIL_INSTANCE = new XMLUtil();
      TAG_SELECTOR = XML_UTIL_INSTANCE.new TagSelector();
      ATTRIBUTE_SELECTOR = XML_UTIL_INSTANCE.new AttributeSelector();
      TEXT_SELECTOR = XML_UTIL_INSTANCE.new TextSelector();
   }
   
   /* XML document creation methods */
   
   public static String getXmlFromDocument(Document document) { 
      return getXmlFromDocument(document, false);
   }
    
   public static String getXmlFromDocument(Document document, boolean prettyPrint) { 
      String xml = null; 
      try { 
          OutputFormat format = (prettyPrint) ? OutputFormat.createPrettyPrint() : OutputFormat.createCompactFormat();
          StringWriter sw = new StringWriter(); 
          XMLWriter writer = new XMLWriter( sw, format );
          writer.write( document );
          sw.flush(); 
          xml = sw.toString(); 
          sw.close(); 
       	
      } catch (IOException e) {
         
      }
      
      return xml; 
   }    
    
   public static Document createDocument() {
      Document document = DocumentHelper.createDocument();
      
      return document;
   }
    
   public static void addAttribute(Element element, String attrName, String attrValue) {
      if(element != null && attrName != null && attrName.length() > 0 && attrValue != null) {
      	element.addAttribute(attrName, attrValue);
      }
   }
    
   public static void addText(Element element, String text) {
      if(element != null && text != null) {
      	element.addText(text);
      }
   }
   
   /* End XML document creation methods */

   /* XML parsing methods */
   
   public static Document createDocumentFromXml(String xml) {
      Document document = null;
      
      if(xml != null && xml.length() > 0) {
         SAXReader reader = new SAXReader();
         StringReader stringReader = new StringReader(xml);
         try {
            document = reader.read(stringReader);
         } catch (DocumentException e) {
            //e.printStackTrace();
         }
      }
      
      return document;
   }
   
   public static Element getFirstElement(Element parent, String tagname) {
      Element tagEl = null;
      
      if(parent != null && tagname != null) {
         tagEl = parent.element(tagname); 
      }
      
      return tagEl;
   }
   
   public static String getFirstElementAttribute(Element parent, String tagname, String attname) {
      if(parent != null && tagname != null && attname != null) {
         Element first = parent.element(tagname);
         
         if(first != null) {
            return(first.attributeValue(attname));
         }
      }
      
      return(null);
   }  
   
   public static String getFirstElementText(Element parent, String tagname) {
      if(parent != null && tagname != null) {
         Element first = parent.element(tagname);
         if(first != null)
         {
            return(first.getText());
         }
      }
      return(null);      
   }
   
   public static boolean firstElementHasText(Element parent, String tagname) {
      if(parent != null && tagname != null) {
         String test = getFirstElementText(parent, tagname);
         if(test != null && test.length() != 0)
         {
            return(true);
         }
      }
      return(false);
   }
   
   public static String getAttribute(Element[] list, int i, String attname) {
      if(list != null && attname != null) {
         if(i >= 0 && i < list.length) {
            return(list[i].attributeValue(attname));
         }
      }
      return(null);
   } 
   
   public static String getText(Element[] list, int i) {
      if(list != null) {
         if(i >= 0 && i < list.length)
         {
            return(list[i].getText());
         }
      }
      return(null);
   }
   
   public static int getIndexForName(Element[] list, String attname, String search1, String search2, boolean substring) {
      if(list != null)
      {
         String namevalue = null;
         for (int i = 0; i < list.length; i++)
         {
            namevalue = getAttribute(list, i, attname);
            if(namevalue != null)
            {
               if(substring)
               {
                  if(search1 != null && namevalue.indexOf(search1) >= 0)
                  {
                     return(i);
                  }
                  else if(search2 != null && namevalue.indexOf(search2) >= 0)
                  {
                     return(i);
                  }
                  else if(substring && namevalue.equals(WILD_CARD))
                  {
                     return(i);
                  }
               }
               else
               {
                  if(search1 != null && search1.equals(namevalue))
                  {
                     return(i);
                  }
                  else if(search2 != null && search2.equals(namevalue))
                  {
                     return(i);
                  }
               }
            }
         }
      }
      return(-1);
   }   
   
   public static String getTag(Element element) {
      String tag = "";
      
      if(element != null) {
         tag = element.getName();
      }
      
      return tag;
   }
   
   public static String getText(Element element) {
      String text = "";
      
      if(element != null) {
         text = element.getText();
      }
      
      return text;
   }
   
   public static String getAttribute(Element element, String name) {
      String value = null;

      if(element != null && name != null) {
         value = element.attributeValue(name);
      }
      
      return value;
   }
   
   public static Element[] getElementArray(Element parent, String name) {
      Element[] children = null;
      
      if(parent != null && name != null) {
         children = getElementArray(parent.elements(name));
      }
      
      return children;
   }   
   
   public static Element[] getElementArray(List list) {
      if(list != null)
      {
         ArrayList alist = new ArrayList(list);
         if(alist.size() != 0)
         {
            Element[] elist = new Element[alist.size()];
            alist.toArray(elist);
            return(elist);
         }
      }
      return(null);
   }
   
   public static int getCount(Element[] list) {
      if(list != null)
      {
         return(list.length);
      }
      return(0);
   }   
   
   /* End XML parsing methods */
   
   /* Utility methods */
   
   public static String[] getStringArrayByTag(Element[] elements) {
      return XMLUtil.getStringArray(elements, TAG_SELECTOR);
   }
   
   public static String[] getStringArrayByAttribute(Element[] elements, String attributeName) {
      ATTRIBUTE_SELECTOR.setName(attributeName);
      
      return XMLUtil.getStringArray(elements, ATTRIBUTE_SELECTOR);
   }

   public static String[] getStringArrayByText(Element[] elements) {
      return XMLUtil.getStringArray(elements, TEXT_SELECTOR);
   }
   
   /* End Utility methods */
   
   /* Helper methods */
   
   private static String[] getStringArray(Element[] elements, NodeSelector selector) {
      String[] stringArray = null;
      
      if(elements != null && selector != null) {
         stringArray = new String[elements.length];
         
         for(int index = 0; index < elements.length; index++) {
            stringArray[index] = selector.getString(elements[index]);
         }
      }
      
      return stringArray;
   }
   
   private interface NodeSelector {
      public String getString(Element element);
   };
      
   private class TagSelector implements NodeSelector {
      public String getString(Element element) {
         return XMLUtil.getTag(element);
      }
   };
   
   private class AttributeSelector implements NodeSelector {
      private String name;
      
      public String getString(Element element) {
         return XMLUtil.getAttribute(element, name);
      }

      public String getName() {
         return name;
      }

      public void setName(String attributeName) {
         this.name = attributeName;
      }
   };
   
   private class TextSelector implements NodeSelector {
      public String getString(Element element) {
         return XMLUtil.getText(element);
      }
   }
   
   /* End Helper methods */
}
