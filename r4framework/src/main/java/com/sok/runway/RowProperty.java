package com.sok.runway;

import com.sok.framework.*;
public class RowProperty {
   
	public static final String CanEdit = "CanEdit";
	public static final String CanDelete = "CanDelete";
	public static final String CanView = "CanView";
	public static final String CanPreview = "CanPreview";
	public static final String CanDuplicate = "CanDuplicate";
	public static final String CanCreateChildren = "CanCreateChildren";
	public static final String CanLink = "CanLink";
   
   boolean canEdit = false;
   boolean canDelete = false;
   boolean canView = false;
   boolean canPreview = false;
   boolean canDuplicate = false;
   boolean canCreateChildren = false;
   boolean canLink = false;
   
   public RowProperty(TableData row) {
      canEdit = "true".equals(row.getColumn(CanEdit));
      canDelete = "true".equals(row.getColumn(CanDelete));
      canView = "true".equals(row.getColumn(CanView));
      canPreview = "true".equals(row.getColumn(CanPreview));
      canDuplicate = "true".equals(row.getColumn(CanDuplicate));
      canCreateChildren = "true".equals(row.getColumn(CanCreateChildren));
      canLink = "true".equals(row.getColumn(CanLink));
   }
   
   public boolean getPermission(String perm) {
      if (perm.equals(RowProperty.CanEdit)) {
	      return canEdit();
	   }
	   else if (perm.equals(RowProperty.CanDelete)) {
	      return canDelete();
	   }
	   else if (perm.equals(RowProperty.CanView)) {
	      return canView();
	   }
	   else if (perm.equals(RowProperty.CanPreview)) {
	      return canPreview();
	   }
	   else if (perm.equals(RowProperty.CanDuplicate)) {
	      return canDuplicate();
	   }
	   else if (perm.equals(RowProperty.CanCreateChildren)) {
	      return canCreateChildren();
	   }
	   else if (perm.equals(RowProperty.CanLink)) {
	      return canLink();
	   }
	   else {
	      return false;
	   }
   }
   
   public boolean canEdit() {
      return canEdit;
   }
   public boolean canDelete() {
      return canDelete;
   }
   public boolean canView() {
      return canView;
   }
   public boolean canPreview() {
      return canPreview;
   }
   public boolean canDuplicate() {
      return canDuplicate;
   }
   public boolean canCreateChildren() {
      return canCreateChildren;
   }
   public boolean canLink() {
      return canLink;
   }
   
}
