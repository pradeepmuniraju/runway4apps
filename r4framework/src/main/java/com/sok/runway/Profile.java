package com.sok.runway;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import javax.servlet.jsp.JspWriter;
import org.apache.commons.lang.StringUtils;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.crm.profile.ProfileTypes;
import com.sok.runway.view.FormFields;

public class Profile
{
	private static final String PROFILE_GENERATOR_NO_DASHBOARD = "PROFILE_GENERATOR_NO_DASHBOARD";
	
	public static String _tiny =        FormFields.FIELD_TEXT_TINY;
	public static String _short =       FormFields.FIELD_TEXT_SHORT;
	public static String _medium =      FormFields.FIELD_TEXT_MEDIUM;
	public static String _long =        FormFields.FIELD_TEXT_LONG;
	public static String _multiline =   FormFields.FIELD_TEXTAREA;

	public static String _select =      FormFields.FIELD_SELECT;
	
	// _select_set no longer supported
	public static String _select_set =  FormFields.FIELD_LIST;
	public static String _list =        FormFields.FIELD_LIST;
	public static String _radio =       FormFields.FIELD_RADIO;
	public static String _checkbox =    FormFields.FIELD_CHECKBOX;
	
	public static String _date =        FormFields.FIELD_DATE;
	public static String _number =      FormFields.FIELD_NUMBER;
	public static String _large_number =FormFields.FIELD_LARGE_NUMBER;
	public static String _percentage =  FormFields.FIELD_PERCENTAGE;
	public static String _currency =    FormFields.FIELD_CURRENCY;
	public static String _yesno =       FormFields.FIELD_YESNO;
	public static String _hidden =       FormFields.FIELD_HIDDEN;
	
	public static String _calculation = "Calculation";
	public static String _calculation_sum_order = "Sum Act. Orders";
	public static String _calculation_profile_total = "Profile Total";
	
	public static String _calculation_sum = "Sum";
	public static String _calculation_diff = "Difference";
	public static String _calculation_prod = "Product";
	public static String _calculation_div = "Division";
	public static String _calculation_percent = "Percentage";
	
	public static String[] inputtypes = {
		_tiny,
		_short,
 		_medium,
 		_long,
 		_multiline,
 		_select, 
 		_list, 
		_radio,
		_checkbox, 
		_date, 
		_number, 
		_large_number, 
		_percentage,
		_currency,
		_yesno,
		_calculation,
		_calculation_sum_order,
		_calculation_profile_total,
		_hidden
	};
	
	public static String[] maxlength = {
		"10", //_tiny
		"50", //_short
		"100", //_medium
		"200", //_long
		null, //_multiline
		"100", //_select
		"100", //_list
		"200", //_radio
		"200", //_checkbox
		null, //_date
		null, //_number
		null, //_large_number
		null, //_percentage
		null, //_currency
		"5", //_yesno
		null, //_calculation
		null, //_calculation_sum_order
		null //_calculation_profile_total
	};
	
	public static String[] calculationTypes = {
		_calculation_sum,
		_calculation_diff,
 		_calculation_prod,
		_calculation_percent,
		_calculation_div
	};
	
	private static final HashMap<String, String> PROFILE_TYPE_ANSWER_RELATIONS = new HashMap<String, String>();
	
	static {
      PROFILE_TYPE_ANSWER_RELATIONS.put("CompanyProfile", "ContactCompany.CompanyAnswer"); 
      PROFILE_TYPE_ANSWER_RELATIONS.put("ContactProfile", "ContactQuestionAnswer");
      PROFILE_TYPE_ANSWER_RELATIONS.put("ProductProfile", "ContactContactProductGroup.ContactProductGroupQuestionAnswers");
      PROFILE_TYPE_ANSWER_RELATIONS.put("NoteProfile", "ContactNotes.NoteAnswer");
      PROFILE_TYPE_ANSWER_RELATIONS.put("OrderProfile", "ContactOrders.OrderAnswer");
      PROFILE_TYPE_ANSWER_RELATIONS.put("OpportunityProfile", "ContactOpportunity.OpportunityAnswer");
   }	
	
   public static void printInputTypeOptions(JspWriter out) throws IOException {
      printInputTypeOptions(out, null);
   }
   
   public static void printInputTypeOptions(JspWriter out, String selected) throws IOException {
      
      for (int i =0; i < inputtypes.length; i++) {
         out.print("<option value=\"");
         out.print(inputtypes[i]);
         out.print("\"");
         if (inputtypes[i].equals(selected)) {
            out.print(" selected=\"selected\"");
         }
         out.print(">");
         out.print(inputtypes[i]);
         out.print("</option>");
      }
   }

   public static void printCalculationTypeOptions(JspWriter out) throws IOException {
      printCalculationTypeOptions(out, null);
   }
   
   public static void printCalculationTypeOptions(JspWriter out, String selected) throws IOException {
      
      for (int i =0; i < calculationTypes.length; i++) {
         out.print("<option value=\"");
         out.print(calculationTypes[i]);
         out.print("\"");
         if (calculationTypes[i].equals(selected)) {
        	 out.print(" selected=\"selected\"");
         }
         out.print(">");
         out.print(calculationTypes[i]);
         out.print("</option>");
      }
   }

	public static String getMaxLengthForInputType(String type)
	{
		for(int i=0; i<inputtypes.length; i++)
		{
			if(type.equals(inputtypes[i]))
			{
				return(maxlength[i]);
			}
		}
		return(null);
	}
	
	public static GenRow getProfileViewBean(String profileID) {
	   GenRow profileViewBean = null;
	   
	   try {
         if(profileID != null && profileID.length() > 0) {
            profileViewBean = new GenRow();
            profileViewBean.setViewSpec("SurveyView");
            profileViewBean.setParameter("SurveyID", profileID);
            profileViewBean.setAction(GenerationKeys.SELECTFIRST);
            profileViewBean.doAction();
         }
	   } catch(Exception e) {
	      if(profileViewBean != null) { 
	         profileViewBean.close();
	      }
         profileViewBean = null;
	   }
	
	   return profileViewBean;
	}
	
   public static String getProfileType(GenRow profile) {
      String profileType = "";
      
      if(profile != null && profile.getData("ProfileType") != null) {
         profileType = profile.getData("ProfileType");
      }
      
      return profileType;
   }
   
   public static String getProfileType(String profileID) {
      String profileType = "";
      GenRow profileViewBean = getProfileViewBean(profileID);
      profileType = getProfileType(profileViewBean);
      
      return profileType;
   }
   
   public static boolean isMatchingProfileTypes(String sourceProfileType, String targetProfileType) {
      boolean isMatchingProfileTypes = false;
      
      if(sourceProfileType != null && sourceProfileType.length() > 0
            && targetProfileType != null && targetProfileType.length() > 0) {
        isMatchingProfileTypes = sourceProfileType.equals(targetProfileType);
      }
      
      return isMatchingProfileTypes;
   }
   
   public static class ValidationException extends Exception {
      public ValidationException(String message) {
         super(message);
      }
   }

   public static String duplicateQuestion(String questionID, com.sok.runway.UserBean user) {
      GenRow question = new GenRow();
      GenRow newQuestion = new GenRow();

      try {
         question.setTableSpec("Questions");
         question.setParameter("QuestionID", questionID);
         question.setParameter("-select", "*");
         question.doAction(GenerationKeys.SELECTFIRST);

         newQuestion.setTableSpec("Questions");
         newQuestion.putAllDataAsParameters(question);
         newQuestion.createNewID();
         newQuestion.setParameter("Label", "Copy of " + question.getString("Label"));
         newQuestion.setParameter("CreatedDate", "NOW");
         newQuestion.setParameter("CreatedBy", user.getString("UserID"));
         newQuestion.setParameter("ModifiedDate", "NOW");
         newQuestion.setParameter("ModifiedBy", user.getCurrentUserID());
         newQuestion.setParameter("GroupID", user.getString("DefaultGroupID"));
         newQuestion.doAction(ActionBean.INSERT);
      }
      catch (Exception e) {
         // Data failure
         e.printStackTrace(System.out);
      }
      finally {
         question.close();
         newQuestion.close();
      }

      return newQuestion.get("QuestionID").toString();
   }

   public static void addQuestionToProfile(String questionID, String profileID) throws ValidationException {
      
      GenRow association = null;
      GenRow survey = null;
      GenRow question = null;

      try {
         survey = new GenRow();
         survey.setTableSpec("Surveys");
         survey.setParameter("-select0", "ProfileType");
         survey.setParameter("SurveyID", profileID);
         survey.doAction(GenerationKeys.SELECTFIRST);

         question = new GenRow();
         question.setTableSpec("Questions");
         question.setParameter("-select0", "ProfileType");
         question.setParameter("QuestionID", questionID);
         question.doAction(GenerationKeys.SELECTFIRST);

         // Validation: question.profileType must be equal to the survey.profiletype (including null)
         if (survey.getString("ProfileType") != null && !StringUtils.equals(survey.getString("ProfileType"), "")) {
	         if (!StringUtils.equals(survey.getString("ProfileType"), question.getString("ProfileType"))) {
	            throw new ValidationException("ProfileType is different");
	         }
         }
         
         // Validation: don't allow duplicates
         if (profileHasQuestion(questionID, profileID)) {
            throw new ValidationException("Profile already has this question.");
         }

         association = new GenRow();
         association.setTableSpec("SurveyQuestions");
         association.setToNewID("SurveyQuestionID");
         association.setParameter("SurveyID", profileID);
         association.setParameter("QuestionID", questionID);
         association.doAction(GenerationKeys.INSERT);

      }
      catch (ValidationException e) {
         throw e;
      }
      catch (Exception e) {
         // TODO: handle exception
         e.printStackTrace(System.out);
      }
      finally {
         association.close();
         survey.close();
         question.close();
      }
   }

   public static boolean profileHasQuestion(String questionID, String profileID) {
      GenRow question = new GenRow();

      try {
         question.setTableSpec("SurveyQuestions");
         question.setParameter("SurveyID", profileID);
         question.setParameter("QuestionID", questionID);

         question.doAction(GenerationKeys.SEARCH);

         return question.getSize() != 0;
      }
      finally {
         question.close();
      }
   }
      
   public static String calculateQuestionAnswerCount(String questionID, String profileType) {
      String questionAnswerCount = "";
      GenRow questionAnswerCountBean = null;
      
      try {
         questionAnswerCountBean = getContactAnswersBeanForQuestion(questionID, profileType);
         if(questionAnswerCountBean != null) {
            questionAnswerCount = String.valueOf(questionAnswerCountBean.getSize());
         }
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         if(questionAnswerCountBean != null) {
            questionAnswerCountBean.close();
         }
      }
      
      return questionAnswerCount;
   }
   
   public static String[] calculateAnswersCount(String questionID, String[] answers, String profileType) {
      String[] answerCounts = null;
      
      if(profileType != null && profileType.length() > 0) {
         String relation = getRelation(profileType);
         if(answers != null && answers.length > 0 && relation != null && relation.length() > 0) {
           answerCounts = new String[answers.length];
           GenRow answerCountBean = null;
           try {
              answerCountBean = new GenRow();
              for(int i=0; i< answers.length; i++) {
                 String answer = answers[i];
                 answerCounts[i] = calculateAnswerCount(answerCountBean, questionID, answer, relation);
              }
           } catch (Exception e) {
              e.printStackTrace();
           } finally {
              if(answerCountBean != null) {
                 answerCountBean.close();
              }
           }
         }
      }
      
      return answerCounts;
   }
   
   public static String calculateAnswerCount(GenRow answerCountBean, String questionID, String answer, String relation) {
      String answerCount = "";
      if(answerCountBean != null && questionID != null && questionID.length() > 0 
            && answer != null && answer.length() > 0) {
        answerCountBean.clear();
        answerCountBean.setViewSpec("ContactListView");    
        answerCountBean.setParameter("-join1", relation);
        answerCountBean.setParameter(relation + "." + "QuestionID", questionID);
        answerCountBean.setParameter(relation + "." + "Answer", answer);
        answerCountBean.setAction(GenerationKeys.SEARCH);
        answerCountBean.getResults(true);
        answerCount = String.valueOf(answerCountBean.getSize());        
      }
      
      return answerCount;
   }
   
   public static GenRow getContactAnswersBeanForQuestion(String questionID, String profileType) {
      GenRow contactAnswersBean = null;
      String relation = getRelation(profileType);
      
      if(questionID != null && questionID.length() > 0 && relation != null && relation.length() > 0) {
         contactAnswersBean = new GenRow();
         contactAnswersBean.setViewSpec("ContactListView");    
         contactAnswersBean.setParameter("-join1", relation);
         contactAnswersBean.setParameter(relation + "." + "QuestionID", questionID);
         contactAnswersBean.setAction(GenerationKeys.SEARCH);            
         contactAnswersBean.getResults(true);
      }     
      
      return contactAnswersBean;
   }
    
   public static GenRow getContactAnswersBeanForAnswer(String questionID, String answer, String profileType) {
      GenRow contactAnswersBean = null;
       
      String relation = getRelation(profileType);
      
      if(questionID != null && questionID.length() > 0 
            && answer != null && answer.length() > 0 
            && relation != null && relation.length() > 0) {
         contactAnswersBean = new GenRow();
         contactAnswersBean.setViewSpec("ContactListView");    
         contactAnswersBean.setParameter("-join1", relation);
         contactAnswersBean.setParameter(relation + "." + "QuestionID", questionID);
         contactAnswersBean.setParameter(relation + "." + "Answer", answer);
         contactAnswersBean.setAction(GenerationKeys.SEARCH);
      }         
       
      return contactAnswersBean;      
   }
   
   private static String getRelation(String profileType) {
      String relation = null;
      
      if(profileType != null && PROFILE_TYPE_ANSWER_RELATIONS.containsKey(profileType)) {
         relation = PROFILE_TYPE_ANSWER_RELATIONS.get(profileType);
      }
      
      return relation;
   }   
}
