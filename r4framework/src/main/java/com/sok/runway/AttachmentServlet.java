package com.sok.runway;

import java.io.*;
import java.net.URLEncoder;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.sok.framework.*;

public class AttachmentServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		ServletOutputStream out = response.getOutputStream();

		EmailBean mailbox = (EmailBean) session.getAttribute("mailbox");
		UserBean currentUser = (UserBean) session
				.getAttribute(SessionKeys.CURRENT_USER);

		InputStream input = null;
		BufferedInputStream bufferedInput = null;
		
		try {

			int partNum = Integer.parseInt(request.getParameter("part"));
			String messageid = StringUtil.hexDecode(request
					.getParameter("MessageID"));

			if (partNum < 0) {
				out.print(mailbox.getBody(messageid));
			} else {
				javax.mail.Part part = mailbox.getAttachment(messageid, partNum);
				if (part != null) {
					String sct = part.getContentType();

					ContentType ct = new ContentType(sct);

					response.setContentType(ct.getBaseType());
					String attachmentHeader = "attachment; filename=" + URLEncoder.encode(part.getFileName(), "UTF-8");
					System.out.println(attachmentHeader);
					response.setHeader("Content-Disposition", attachmentHeader);
					
					input = part.getInputStream();
					bufferedInput = new BufferedInputStream(input);
					final byte[] buffer = new byte[1024];
					int readBytes;
					while ((readBytes = bufferedInput.read(buffer)) != -1) {
						out.write(buffer, 0, readBytes);
					}
				} else {
					out.println("invalid part");
					return;
				}
			}
		} catch (MessagingException ex) {
			throw new ServletException(ex.getMessage());
		} finally {			
			out.flush();
			out.close();
			
			if (bufferedInput != null) {
				bufferedInput.close();
			}
			
			if (input != null) {
				input.close();
			}
		}
	}

}
