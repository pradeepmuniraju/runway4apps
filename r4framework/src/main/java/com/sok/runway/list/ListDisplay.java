package com.sok.runway.list;

import com.sok.framework.DisplaySpec;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.PagingRowSetBean;
import com.sok.framework.SpecManager;
import com.sok.framework.TableData;
import com.sok.framework.ActionBean;
import com.sok.framework.VelocityManager;
import com.sok.framework.ViewSpec;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.generation.Row;
import com.sok.framework.generation.nodes.LeafNode;
import com.sok.framework.generation.nodes.Node;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.service.crm.UserService;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;

/**
 * 
 * @author Simon Sher
 * @created April 2009 for Runway version 4
 * 
 * This class is used to encapsulate the functionality of the list pages.
 * The three important objects are:
 * searchbean, pagingRowSetBean, resultSet 
 * 
 * VERSION HISTORY (please guys ! )
 * April 2009 - Created
 * 
 * @version June 2012 - Updated to allow saving list headings for sharing between users.
 * @author Michael Dekmetzian 
 * 
 * @version August 2012 - Updated to use display specs for storage of headers / sizes 
 * 
 */
public abstract class ListDisplay {

	private static final Logger logger = LoggerFactory.getLogger(ListDisplay.class);
	
	protected GenRow searchbean = new GenRow();
	protected PagingRowSetBean pagingRowSetBean = null;
	private Properties sysprops = InitServlet.getSystemParams();

	private DisplaySpec dspec;
	private String pageNum;
	private String userID;
	private String action;
	
	String requestPath;
	String contextPath;
	private static final VelocityEngine ve;
	static {
		try { 
			ve = VelocityManager.getEngine();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private int userDefaultListSize;
	protected TableData resultSet;

	protected List<String> listHeadings;
	protected List<String> listSpec;
	protected List<String> columnWidths;
	protected List<String> sortFields;

	private boolean sortByOneListHeadingOnly = false;

	/**
	 * @deprecated	- use with displayspec and request
	 */
	public ListDisplay(int numOfColumns) {
		numberOfColumns = numOfColumns;
		listHeadings = new ArrayList<String>(numOfColumns);
		listSpec = new ArrayList<String>(numOfColumns);
		columnWidths = new ArrayList<String>(numOfColumns);
		sortFields = new ArrayList<String>(numOfColumns);
	}
	
	/**
	 * @deprecated
	 */
	public ListDisplay() {
		
	}
	
	protected UserBean user = null;
	
	/**
	 * Sets the current user and the displayspec. 
	 * The columns to be used are pulled from the DisplaySpec by default so this should be done first. 
	 */
	public ListDisplay(HttpServletRequest request, String displaySpecName) {
		user = UserService.getInstance().getCurrentUser(request);
		setUserID(user.getUserID());
		setUserDefaultListSize(user.getDefaultListSize());
		setDisplaySpec(displaySpecName);
	}

	public String evaluateField(String velocity, TableData context) {
		StringWriter sw = new StringWriter();
		try {
			ve.evaluate(context, sw, this.getClass().getName(), velocity);
			sw.flush();
			sw.close();
		} catch (Exception e) {
			logger.error("Error evaluating field", e);
			return velocity;
		}
		return (sw.toString());
	}
	
	public void close() {
		pagingRowSetBean.close();
	}

	public void setViewSpec(String viewSpec) {
		if (searchbean != null) searchbean.setViewSpec(viewSpec);
	}

	public ViewSpec getViewSpec() {
		return (searchbean != null)? searchbean.getViewSpec() : null;
	}

	
	public String getData(String field)
	{
		if(resultSet == null) 
		{
			return (searchbean != null)? searchbean.getData(field) : null;
		}
		else
		{	
			return resultSet.getData(field);
		}	
	}
	
	public String getParameter(String field)
	{
		if(resultSet == null) 
		{
			return (searchbean != null)? searchbean.getParameter(field) : null;
		}
		else
		{	
			return resultSet.getParameter(field);
		}		
	}
	
	public String getColumn(String field)
	{
		if(resultSet == null) 
		{
			return (searchbean != null)? searchbean.getColumn(field) : null;
		}
		else
		{	
			return resultSet.getColumn(field);
		}
			
	}
	
	public void setColumn(String field, String value)
	{
		if(resultSet == null) 
		{
			if (searchbean != null) searchbean.setColumn(field,value);
		}
		else
		{	
			resultSet.setColumn(field,value);
		}
			
	}	

	public void setParameter(String field, String value)
	{
		if(resultSet == null) 
		{
			if (searchbean != null) searchbean.setParameter(field,value);
		}
		else
		{	
			resultSet.setParameter(field,value);
		}
			
	}	
	
	public DisplaySpec getDisplaySpec()
	{
		return dspec;
	}

	public void setDisplaySpec(String parameter)
	{
		this.dspec = SpecManager.getDisplaySpec(parameter);
	}

	public String getSysProperty(String property)
	{
		return sysprops.getProperty(property);
	}

	public String getPageNum() {
		return pageNum;
	}   

	public void setPageNum(String field) {
		if(field == null)
			this.pageNum = "1";
		else	
			this.pageNum = field;
	}   

	public String getAction() {
		return action;
	}   

	public void setAction(String field) {
		if(field == null)
			this.action = "list";
		else	
			this.action = field;
	}

	public void sortColumnBy(String field, int num)
	{   
		if (searchbean != null) searchbean.sortBy(field,num); 
	}

	public void sortOrder(String field, int num)
	{   
		if (searchbean != null) searchbean.sortOrder(field,num); 
	}

	public void setTop(int num)
	{
		if (searchbean != null) searchbean.setTop(num); 	
	}


	/**
	 * Helper method to perform the multi sort on the searchbean.  If the user
	 * has chosen to sort on one column only by clicking on the ascending or descending
	 * arrow next to the list heading, we don't need to perform the multi sort.
	 * 
	 * @param request The request object
	 */
	public void performSort(HttpServletRequest request)
	{
		if( ! sortByOneListHeadingOnly)
		{
			// Retrieve the users sort preferences from the database
			GenRow listMultiSort = new GenRow();
			listMultiSort.setRequest(request);			
			listMultiSort.setTableSpec("UserLayoutMultiSort");
			listMultiSort.setParameter("-select0","SortField");
			listMultiSort.setParameter("-select1","SortOrder");
			listMultiSort.setParameter("-select2","SortPrecedence");
			listMultiSort.setParameter("UserID", userID);
			listMultiSort.setParameter("RequestPath", getRequestPath());
			listMultiSort.sortBy("SortPrecedence",0);
			listMultiSort.sortOrder("ASC",0);
			listMultiSort.doAction(GenerationKeys.SEARCH);			
			listMultiSort.getResults();

			//apply the sort preferences to the search bean
			int sortNum = 0;			
			while(listMultiSort.getNext())
			{
				sortColumnBy(listMultiSort.getData("SortField"),sortNum);
				sortOrder(listMultiSort.getData("SortOrder"),sortNum);
				sortNum++;	
			}
			listMultiSort.close();					

		}
		if (searchbean != null) searchbean.doAction(GenerationKeys.SEARCH);		
	}

	private int numberOfColumns = 0;
	public int getNumberOfColumns() {
		return numberOfColumns;
	}
	
	private boolean usingLayout = Boolean.FALSE;
	private boolean editLayout = Boolean.FALSE;
	protected String layoutID = null;
		
	public boolean isUsingSavedLayout() {
		return usingLayout;
	}
	public boolean canEditLayout() {
		return usingLayout && editLayout;
	}
	public String getLayoutID() {
		return layoutID;
	}
	
	public static String getDefaultLayoutID(HttpServletRequest request, String userID, String module) {
		GenRow d = new GenRow();
		d.setRequest(request);
		d.setViewSpec("UserLayoutHeadingsGroupUserView");
		d.setParameter("UserLayoutHeadingGroupUserUserLayoutHeadingGroups.Module", module);
		d.setParameter("UserID", userID);
		d.setParameter("DefaultLayout", "Y");
		d.doAction(GenerationKeys.SELECTFIRST);
		
		if(d.isSuccessful()) {
			return d.getData("LayoutID");
		}
		return null;
	}
	
	public boolean isHeadingUsed(String heading) {
		return listSpec.contains(heading);
	}
	
	/**
	 * @deprecated - headings and default widths now to be pulled from display spec.
	 */
	protected void listHeadings(double availableWidth, int numOfColumns, String defaultListHeadings[], String defaultListSpec[], String defaultColumnWidths[], HttpServletRequest request)
	{
		logger.warn("listHeadings(width, {}, [{}], spec, widths, request) is deprecated, parameters ignored", String.valueOf(numOfColumns), StringUtils.join(defaultListHeadings));
		listHeadings(request);
	}
	protected void listHeadings(HttpServletRequest request) 
	{
		double widthsTotal = 0.0;
		java.text.DecimalFormat df = new java.text.DecimalFormat("###.#");
		
		/* modified as we want to set the layout to the user's default in another method */
		String tLayoutID = StringUtils.trimToNull((String)request.getAttribute("-LayoutID"));
		if(tLayoutID == null) {
			tLayoutID = StringUtils.trimToNull(request.getParameter("-LayoutID"));
		}
		if(tLayoutID != null) {
			layoutID = tLayoutID;
		}
		
		if(layoutID != null) {
			GenRow l = new GenRow();
    		l.setRequest(request);
    		l.setTableSpec("UserLayoutHeadingsGroup");
    		l.setParameter("-select1","LayoutID");
    		l.setParameter("-select2","CreatedBy");
    		l.setParameter("LayoutID", layoutID);
    		l.doAction(GenerationKeys.SELECT);
			if(l.isSuccessful()) {
				usingLayout = Boolean.TRUE;
				UserBean ub = UserService.getInstance().getCurrentUser(request);
				if(ub != null) 
					editLayout = (l.getString("CreatedBy").equals(ub.getUserID()));
			} else {
				layoutID = null;
			}
		} else {
			usingLayout = Boolean.FALSE;
			editLayout = Boolean.FALSE;
		}

		logger.debug("using layout {} editing layout {}", usingLayout, editLayout);
		
		GenRow listHeadingsDB = new GenRow();
		listHeadingsDB.setRequest(request);
		listHeadingsDB.setTableSpec("UserLayoutListHeadings");
		listHeadingsDB.setParameter("-select0","DisplayName");
		listHeadingsDB.setParameter("-select1","DatabaseName");
		listHeadingsDB.setParameter("-select2","ColumnPosition");
		listHeadingsDB.setParameter("-select3","Width");
		listHeadingsDB.setParameter("-select4","SortField");
		listHeadingsDB.sortBy("ColumnPosition",0);
		listHeadingsDB.sortOrder("ASC",0);
		
		if(layoutID != null) {
			listHeadingsDB.setParameter("LayoutID", layoutID);
		} else {
			listHeadingsDB.setParameter("UserID", userID);
			listHeadingsDB.setParameter("RequestPath", getRequestPath());
		}
		try {
			
			if(logger.isTraceEnabled()) { 
				listHeadingsDB.doAction(GenerationKeys.SEARCH);
				logger.trace(listHeadingsDB.getStatement()); 
			}
			listHeadingsDB.getResults(true);
	
			int recordCount = listHeadingsDB.getSize();
	
		 	// iterates through every userlayout record found to calculate record counts and
			// total width of customisable columns to be displayed on page.
			while(listHeadingsDB.getNext())
			{
				if(listHeadingsDB.getData("Width").length() > 0 && StringUtils.containsOnly( listHeadingsDB.getData("Width"),"0123456789.") )
				{
					widthsTotal += Double.valueOf(listHeadingsDB.getData("Width"));
				}
			}	
			Set<String> names = dspec.getDefaultItemNames();
			int numOfColumns = names.size();
			
			if(logger.isDebugEnabled()) logger.debug("recordCount {}, numOfColumns {}, layoutID null {}", new String[]{String.valueOf(recordCount), String.valueOf(numOfColumns), String.valueOf(layoutID == null)});
			
			/* Handle the case where the number of records in the resultset is not equal to the expected number of columns.
			 * This will occur on the users first invocation of the page */
			if(recordCount != numOfColumns && layoutID == null /* layouts will have varying numbers of columns so this doesn't apply */)
			{
	
				// FAILSAFE: presume db write error from a previous session and remove relevent userlayout records.
				if (recordCount > 0)
				{								
					GenRow bean = new GenRow();
					bean.setRequest(request);
					bean.setTableSpec("UserLayoutListHeadings");
					bean.setParameter("ON-UserID", userID);
					bean.setParameter("ON-RequestPath", getRequestPath());
					bean.doAction(ActionBean.DELETEALL);				
				}
	
				GenRow userLayout = new GenRow();
				userLayout.setTableSpec("UserLayoutListHeadings");
	
				userLayout.setColumn("UserID", userID);
				userLayout.setColumn("RequestPath", getRequestPath());
				
				listHeadings = new ArrayList<String>(numOfColumns);
				listSpec = new ArrayList<String>(numOfColumns);
				columnWidths = new ArrayList<String>(numOfColumns);
				sortFields = new ArrayList<String>(numOfColumns);

				int i=0;
				
				for(String s: names) {
					String width = dspec.getItemSize(s);
					if(StringUtils.containsOnly( width,"0123456789.") && width.length() > 0) {
						widthsTotal += Double.parseDouble(width);
					} else {
						logger.error("Width not set correctly for {} as {}", s, width);
					}
				}
				
				
				for(String s: names) {
					String label = dspec.getItemLabel(s), 
							spec = s,
							width = dspec.getItemSize(s);
					
					if(!StringUtils.containsOnly( width,"0123456789.") || width.length()==0) {
						width = "0.0";
					}
					
					userLayout.setRequest(request);
					userLayout.createNewID();
					userLayout.setColumn("DisplayName", label);
					userLayout.setColumn("DatabaseName", spec);
					userLayout.setColumn("ColumnPosition", String.valueOf(i++));
					userLayout.setColumn("Width", width);		
					userLayout.doAction(ActionBean.INSERT);
					if(logger.isDebugEnabled()) {
						logger.debug("insert success {}, error {}", userLayout.isSuccessful(), userLayout.getError());
						logger.debug("adding heading {}",label);
					}
					
					listHeadings.add(label);
					listSpec.add(s);
					columnWidths.add(df.format(Double.parseDouble(width) / widthsTotal * 100));
					sortFields.add(ActionBean._blank);
				}
				this.numberOfColumns = numOfColumns;
			}
			else
			{
				logger.debug("getting headings for list ");
				if(logger.isTraceEnabled()) { 
					listHeadingsDB.doAction(GenerationKeys.SEARCH);
					logger.trace(listHeadingsDB.getStatement()); 
				}
				
				// using column headings from DB
				listHeadingsDB.getResults(true);
				
				listHeadings = new ArrayList<String>(listHeadingsDB.getSize());
				listSpec = new ArrayList<String>(listHeadingsDB.getSize());
				columnWidths = new ArrayList<String>(listHeadingsDB.getSize());
				sortFields = new ArrayList<String>(listHeadingsDB.getSize());
				
				int i=0;
				
				while(listHeadingsDB.getNext()) {
					
					//can return a displayspec field or a runway join relationship 
					String spec = listHeadingsDB.getData("DatabaseName"), label = null, width = null;
					if(spec.indexOf(" as ") == -1) { 
						label = dspec.getItemLabel(spec); 
						width = dspec.getItemSize(spec);
					}
							
					if(StringUtils.isBlank(label)) {
						label = listHeadingsDB.getData("DisplayName");
					}
					if(StringUtils.isBlank(width)) {
						width = listHeadingsDB.getData("Width");
					}
					
					
					listHeadings.add(i, label);
					listSpec.add(i, spec);
	
					if(logger.isDebugEnabled()) 
						logger.debug("Doing List DisplayName[{}], DatabaseName[{}], Width[{}]", new String[]{label, 
									spec, width});	
					
					double widthTotal = 0.0;
					double columnWidth = 0.0;
	
					if(StringUtils.containsOnly( width ,"0123456789.") && StringUtils.isNotBlank(width))
					{
						columnWidth = Double.valueOf(width);
						widthTotal =  columnWidth / widthsTotal * 100;
					}
					columnWidths.add(i, String.valueOf(df.format(widthTotal)));
					sortFields.add(i, listHeadingsDB.getData("SortField"));
					i++;
				}
				this.numberOfColumns = i;
			}
		} finally {
			listHeadingsDB.close();
		}
	}	   
	
	public String getFieldText(String databaseName, TableData context) {
		if(StringUtils.isBlank(databaseName))
			return ActionBean._blank;
		if(databaseName.indexOf(" as ") > -1 || StringUtils.isBlank(dspec.getItemLabel(databaseName))) { 
			if(databaseName.indexOf(" as ") > -1)
				databaseName = StringUtils.trim(databaseName.substring(databaseName.indexOf(" as ")+4));
			return context.getData(databaseName);
		} 
		String text = dspec.getItemValue(databaseName);
		if(StringUtils.isBlank(text)) {
			text = context.getData(databaseName);
		} else {
			text = evaluateField(text, context);
		}
		return text;
	}
	
	public void writeField(String databaseName, TableData context, Writer out) throws IOException {
		if(StringUtils.isBlank(databaseName))
			return;
		if(databaseName.indexOf(" as ") > -1 || StringUtils.isBlank(dspec.getItemLabel(databaseName))) {
			if(databaseName.indexOf(" as ") > -1)
				databaseName = StringUtils.trim(databaseName.substring(databaseName.indexOf(" as ")+4));
			if(logger.isTraceEnabled()) logger.trace("output from context {} {} {}", new String[]{ databaseName, context.getString(databaseName), context.getData(databaseName)});	
			out.write(context.getString(databaseName));
				return;
		} 
		String text = dspec.getItemValue(databaseName);
		if(StringUtils.isBlank(text)) {
			text = context.getString(databaseName);
		} else {
			text = evaluateField(text, context);
		}
		String link = dspec.getItemLink(databaseName);	
		boolean hasLink = StringUtils.isNotBlank(link);
		
		link = hasLink ? evaluateField(link, context) : null;
		hasLink = StringUtils.isNotBlank(link);
		
		if(hasLink) {
			out.write("<a href=\"");
			out.write(evaluateField(link, context));
			out.write("\" title=\"");
			out.write(text);
			out.write("\">");
		}
		out.write(text);
		if(hasLink) {
			out.write("</a>");
		}
		return;
	}

	public PagingRowSetBean getPagingRowSetBean()
	{
		return pagingRowSetBean;
	}

	public void initializePagingRowSetBean(final HttpServletRequest request)
	{
		pagingRowSetBean = new ListPagingRowSetBean();
		pagingRowSetBean.setPageSize(getUserDefaultListSize());
		pagingRowSetBean.setRequest(request);
		pagingRowSetBean.setSearchBean(searchbean);
		pagingRowSetBean.getResults();
		pagingRowSetBean.setPageUrl(getRequestPath());	

		resultSet = pagingRowSetBean.getResultBean();
	}
	
	public class ListPagingRowSetBean extends PagingRowSetBean {
		@Override
		public boolean getNext() {
			boolean ret = super.getNext();
			if(ret) resultSet.put("contextPath",getContextPath());
			return ret;
		}
	}

	public int getSizeOfPagingRowSetBean()
	{
		return pagingRowSetBean.getSize();
	}
	
	public void createSearchBean()
	{
		searchbean = new GenRow();
	}

	protected void generateSQLStatement()
	{
		if (searchbean != null) searchbean.doAction(GenerationKeys.SEARCH); 	
	}

	public String getRequestPath() {
		return requestPath;
	}

	public void setRequestPath(String requestPath) {
		this.requestPath = requestPath;
	}

/*	public String getUserID() {
		return userID;
	}
*/
	public void setUserID(String userID) {
		this.userID = userID;
	}

	public int getUserDefaultListSize() {
		return userDefaultListSize;
	}

	public void setUserDefaultListSize(int userDefaultListSize) {
		this.userDefaultListSize = userDefaultListSize;
	}

	/**
	 * If the user sorts the list by clicking on either the ascending or descending
	 * arrow next to the list heading, this will return true.  If this is true, we
	 * do not perform a multisort but sort on the one column only.
	 * @param sortByOneListHeadingOnly if the list is to be sorted by one column only
	 */
	public void setSortByOneListHeadingOnly(boolean sortByOneListHeadingOnly) {
		this.sortByOneListHeadingOnly = sortByOneListHeadingOnly;
	}

	public void setSearchbean(TableData searchbean) {
		
		if (searchbean != null) {
			if(searchbean instanceof GenRow) 
				this.searchbean = (GenRow) searchbean;
			else {
				this.searchbean = new GenRow();
				this.searchbean.setConnection(searchbean.getConnection());
				this.searchbean.setCloseConnection(true);
				this.searchbean.parseRequest(searchbean.toString());
			}
		}
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public List<String> getListHeadings() {
		return listHeadings;
	}

	public List<String> getListSpec() {
		return listSpec;
	}
	
	public List<String> getSortFields() {
		return sortFields;
	}

	public List<String> getColumnWidths() {
		return columnWidths;
	}

	public TableData getResultSet() {
		return resultSet;
	}
	
	public GenRow getSearchbean() {
		return searchbean;		
	}
	
	public boolean getNext() {
		return pagingRowSetBean.getNext();
	}
	
	public boolean isItemUsed(String field) {
		return dspec.isItemUsed(field);
	}
	
	
	public static class ListField {
		public final String heading, field, width, link, text;
		
		public ListField(String heading, String field, String width) {
			this.heading = heading;
			this.field = field;
			this.width = width;
			this.link = this.text = null;
		}
		public ListField(String heading, String field, String width, String link, String text) {
			this.heading = heading;
			this.field = field;
			this.width = width;
			this.link = link; 
			this.text = text;
		}
	}
	
	private GenRow check = null;
	private GenRow data = null;
	private GenRow data2 = null;
	protected void getProductBean(String productID) {
		if(pagingRowSetBean == null) {
			logger.error("Attempted to get product bean without a row set bean");
			return;
		}
		if(check == null) { 
			check = new GenRow(); 
			check.setTableSpec("Products");
			check.setConnection(pagingRowSetBean.getConnection());
			check.setParameter("-select1",PropertyFactory.PRODUCTID);
			check.setParameter("-select2","ProductType");
		}
		check.setParameter("ProductID",productID);
		if(!check.isSet(PropertyFactory.PRODUCTID)) {
			logger.error("Product ID was not specified");
			return;
		}
		check.doAction(GenerationKeys.SELECT);
		if(!check.isSuccessful()) {
			logger.error("Could not find product for id {}", check.getString("ProductID"));
		}
		if(data == null) {
			data = new GenRow();
			data.setConnection(pagingRowSetBean.getConnection());
		} else {
			data.clear();
		}
		if("Land".equals(check.getData("ProductType"))) { 
			loadLandProduct(productID);
		} else if("House and Land".equals(check.getData("ProductType"))) {
			loadHouseLandProduct(productID);
		} else if ("Home Plan".equals(check.getData("ProductType"))) {
			loadPlanProduct(productID);
		} else if("Existing Property".equals(check.getData("ProductType"))) {
			loadExistingPropertyProduct(productID);
		} else {
			logger.warn("Unknown product type for list view productID={} productType={}", check.getData("ProductID"), check.getData("ProductType"));
		}
	}
	protected void loadProduct(String productID, String viewSpec) {
		data.setViewSpec(viewSpec);
		data.setParameter("ProductID", productID);
		data.doAction("select");
		if(data.isSuccessful()) {
			putAllDataAsParameters(data);
		} else {
			logger.warn("Did not find product to load with ID {}", productID);
		}
	}
	protected void loadLandProduct(String productID) {
		logger.trace("loadLandProduct({})", productID);
		loadProduct(productID, "properties/ProductLotView");
	}
	
	protected void loadPlanProduct(String productID) {
		logger.trace("loadPlanProduct({})", productID);
		loadProduct(productID, "properties/ProductPlanView");
	}
	
	protected void loadHouseLandProduct(String productID) {
		logger.trace("loadHouseLandProduct({})", productID);
		loadProduct(productID, "properties/ProductHouseAndLandView");
	}
	
	protected void loadExistingPropertyProduct(String productID) {
		logger.trace("loadExistingPropertyProduct({})", productID);
		loadProduct(productID, "properties/ProductExistingPropertyView");
		try 
		{
			if(data2 == null) {
				data2 = new GenRow();
				data2.setConnection(pagingRowSetBean.getConnection());
			} else {
				data2.clear();
			}
			data2.setViewSpec("ProductProductLinkedView");
			data2.setParameter("ProductID", productID);
			data2.getResults(); 
			while(data2.getNext()) {
				if("Land".equals(data2.getData("ProductType"))) {
					resultSet.setParameter("LotProductID", data2.getData("LinkedProductID"));
					resultSet.setParameter("LotTotalCost", data2.getData("TotalCost"));
					resultSet.setParameter("LotNumber", data2.getData("Name"));
					
					data.clear();
					data.setViewSpec("properties/ProductLotView");
					data.setParameter("ProductID", data2.getData("LinkedProductID"));
					data.doAction(GenerationKeys.SELECT);
					
					if(data.isSuccessful()) {
						resultSet.setParameter("StageName", data.getData("StageName"));
						resultSet.setParameter("StageProductID", data.getData("StageProductID"));
						resultSet.setParameter("EstateName", data.getData("EstateName"));
						resultSet.setParameter("EstateProductID", data.getData("EstateProductID"));
					}
				} else if ("Home Plan".equals(data2.getData("ProductType"))) {
					resultSet.setParameter("PlanProductID", data2.getData("LinkedProductID"));
					resultSet.setParameter("PlanTotalCost", data2.getData("TotalCost"));
					resultSet.setParameter("PlanName", data2.getData("Name"));
					
					data.clear();
					data.setViewSpec("properties/ProductPlanView");
					data.setParameter("ProductID",  data2.getData("LinkedProductID"));
					data.doAction(GenerationKeys.SELECT);
					if(data.isSuccessful()) { 
						resultSet.setParameter("PlanName", data.getData("PlanName"));
						resultSet.setParameter("RangeName", data.getData("RangeName"));
						resultSet.setParameter("HomeName", data.getData("HomeName"));
						resultSet.setParameter("HomeProductID", data.getData("HomeProductID"));
					}
				}
			}
		} finally { 
			if(data2 != null) data2.close();
		}
	}
	
	
	protected void putAllDataAsParameters(Row row)
    {
		boolean blank = false;
        Iterator entries = row.entrySet(Node.type_DataNode).iterator();
        LeafNode entry = null;
        while(entries.hasNext())
        {
            entry = (LeafNode)entries.next();
            //Don't override values that are in the order list view, such as Cost TotalCost etc
            if(resultSet.isSet(entry.getKey().toString())) {
        		continue;
        	}
            Object value = entry.getValue();
            if (blank) {
            	resultSet.setParameter(entry.getKey().toString(), entry.getValue());
            } else if (value instanceof String) {
            	if (((String) value).length() > 0) {
            		resultSet.setParameter(entry.getKey().toString(), entry.getValue());
            	}
            } else if (value instanceof Double) {
            	if (((Double)value).doubleValue() != 0) {
            		resultSet.setParameter(entry.getKey().toString(), entry.getValue());
            	}
            } else if (value instanceof Float) {
            	if (((Float)value).floatValue() != 0) {
            		resultSet.setParameter(entry.getKey().toString(), entry.getValue());
            	}
            } else if (value instanceof Integer) {
            	if (((Integer) value).intValue() != 0) {
            		resultSet.setParameter(entry.getKey().toString(), entry.getValue());
            	}
            } else if (entry.getValue() != null) {
            	resultSet.setParameter(entry.getKey().toString(), entry.getValue());
            }
        }
    }
}
