package com.sok.runway.list;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.*;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.PagingRowSetBean;
import com.sok.framework.TableData;
import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;

import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PipelineListDisplay extends ListDisplay {

	private static final Logger logger = LoggerFactory.getLogger(PipelineListDisplay.class);

	private static final String DISPLAY_SPEC = "OpportunityListDisplay";
	private static final String DISPLAY_SPEC_PROPERTIES = "OpportunityListPropertiesDisplay";
	private static final String VIEW_SPEC = "OpportunityListView";
	private static final String VIEW_SPEC_SIMPLE = "SimpleOpportunityListView";

	private boolean processes;

	public PipelineListDisplay(HttpServletRequest request, HttpServletResponse response) {
		this(request, response, "true".equals(InitServlet.getSystemParam("PropertyMode")));
	}

	public PipelineListDisplay(HttpServletRequest request, HttpServletResponse response, boolean propertymode) {
		super(request, propertymode ? DISPLAY_SPEC_PROPERTIES : DISPLAY_SPEC);
		initializeObject(request);
	}

	private void initializeObject(final HttpServletRequest request) {
		setDisplaySpec(DISPLAY_SPEC);
		parseRequestObject(request);
		this.processes = "true".equals(getSysProperty("OpportunityProcesses"));
		this.layoutID = getDefaultLayoutID(request, user.getUserID(), "Opportunities");

		if (getAction().equals("list")) {
			createSearchBean();
			setViewSpec(processes ? VIEW_SPEC : VIEW_SPEC_SIMPLE);
			setParameter("ProductID", "NULL+EMPTY");
			user.setConstraint(getSearchbean(), "OpportunityGroup");
			user.setDefaults(getSearchbean(), null, false);
			if (!user.canManage()) {
				setParameter("RepUserID", user.getUserID());
			}
			
			
			// 18-Sep-2013 NI:
			String reqSortBy = request.getParameter("-sortBy");
	    	String reqOrderBy = request.getParameter("-sortOrder");
	    	if (reqSortBy == null || "".equalsIgnoreCase(reqSortBy) || "null".equalsIgnoreCase(reqSortBy)) {
	    		reqSortBy = "";
	    	} /*else {
	    	}*/
	    	
	    	if (reqOrderBy == null || "".equalsIgnoreCase(reqOrderBy) || "null".equalsIgnoreCase(reqOrderBy)) {
	    		reqOrderBy = "";
	    	}
    		logger.info("reqSortBy: " + reqSortBy + "  ,  reqOrderBy: " + reqOrderBy  + "  , processes: " + processes);			
			
			
    		if (!"".equalsIgnoreCase(reqSortBy) && !"".equalsIgnoreCase(reqOrderBy)) {
    			
		    	if ("RepFirstName".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("RepFirstName", 0);
		    		sortColumnBy("RepLastName", 1);
	    		} 
		    	else if ("Contact".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("FirstName",0);
		    		sortColumnBy("LastName",1);
	    		} 
		    	else if ("QuoteDate".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("Opportunities.DateOfSale",0);
	    		} 
		    	else if ("OppName".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("Opportunities.Name",0);
	    		}
		    	else if ("Status".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("CurrentStatus",0);
	    		}
		    	else if ("Product".equalsIgnoreCase(reqSortBy) || "ReferenceProduct".equalsIgnoreCase(reqSortBy)) {
	    			sortColumnBy(("ReferenceProductName"),0);
	    		}
		    	else if ("Company".equalsIgnoreCase(reqSortBy)) {
	    			sortColumnBy(("Company"),0);
	    		}
		    	else {
		    		sortColumnBy(("Opportunities."+reqSortBy),0);
	    		}
	    		sortOrder(reqOrderBy,0);
	    		
	    	} else {	        
	    		sortColumnBy("$database.getDatePartYearMonthStmt(\"Opportunities.CreatedDate\")", 1);
	    		sortColumnBy("Opportunities.Probability", 2);
	    	}
    		
			getSearchbean().doAction(ActionBean.SEARCH);
			request.getSession().setAttribute(SessionKeys.OPPORTUNITY_SEARCH, getSearchbean());

		} else {
			setSearchbean((TableData) request.getSession().getAttribute(SessionKeys.OPPORTUNITY_SEARCH));
			if (StringUtils.isNotBlank(request.getParameter("-sortBy"))) {
				String reqSortBy = request.getParameter("-sortBy");
		    	if ("RepFirstName".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("RepFirstName", 0);
		    		sortColumnBy("RepLastName", 1);
	    		} 
		    	else if ("Contact".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("FirstName",0);
		    		sortColumnBy("LastName",1);
	    		} 
		    	else if ("QuoteDate".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("Opportunities.DateOfSale",0);
	    		} 
		    	else if ("OppName".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("Opportunities.Name",0);
	    		}
		    	else if ("Status".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("CurrentStatus",0);
	    		}
		    	else if ("Product".equalsIgnoreCase(reqSortBy) || "ReferenceProduct".equalsIgnoreCase(reqSortBy)) {
	    			sortColumnBy(("ReferenceProductName"),0);
	    		}
		    	else if ("Company".equalsIgnoreCase(reqSortBy)) {
	    			sortColumnBy(("Company"),0);
	    		}
		    	else {
					sortColumnBy(reqSortBy, 0);
					setSortByOneListHeadingOnly(true);
	    		}
			}

			if (StringUtils.isNotBlank(request.getParameter("-sortOrder"))) {
				sortOrder(request.getParameter("-sortOrder"), 0);
			}
		}

		listHeadings(request);

		// TODO very similar to the Quote List Display version, except for sort
		// fields. We should merge these.

		GenRow tempBean = null;
		// we do this as we do not want to save data into the session based
		// search-bean.
		/*
		 * need to get out a list of custom fields, and if there are any perform
		 * a join on these fields
		 */
		final int lastColumn = getNumberOfColumns() - 1;
		int joinCount = 0;
		int selectCount = 0;
		int sortCount = 0;

		if (lastColumn >= 0) {
			Set<String> joins = new HashSet<String>();

			tempBean = new GenRow();
			tempBean.setRequest(request);
			tempBean.setViewSpec(processes ? VIEW_SPEC : VIEW_SPEC_SIMPLE);

			/* add the joins, then the search, then the sorting. */
			for (int i = 0;; i++) {
				final String field = listSpec.get(i);
				if (field.indexOf(".") > -1) {
					final String join = field.substring(0, field.lastIndexOf("."));
					if (!joins.contains(join)) {
						tempBean.setParameter("-join" + joinCount++, join);
						joins.add(join);
					}
					tempBean.setParameter("-select" + selectCount++, field);
				}

				if (i == lastColumn)
					break;
			}
			tempBean.parseRequest(getSearchbean().toString());
			logger.debug(getSearchbean().toString());
			logger.debug(tempBean.toString());
			setSearchbean(tempBean);
			for (int i = 0; i < 10; i++) {
				if (!searchbean.isSet("-groupby" + i)) {
					searchbean.setParameter("-groupby" + i, "OpportunityID");
					break;
				}
			}
			for (int i = 0;; i++) {
				final String field = listSpec.get(i);
				if (StringUtils.isNotBlank(sortFields.get(i))) {
					int si = field.indexOf(" ");
					boolean doSort = true;
					// out.println("Sort:"+sortFields.get(i)+":"+field+"<br/>");
					if ("D".equals(sortFields.get(i))) {
						tempBean.setParameter("-order" + sortCount, "DESC");
					} else if ("A".equals(sortFields.get(i))) {
						tempBean.setParameter("-order" + sortCount, "ASC");
					} else {
						doSort = false;
					}
					if (doSort) {
						if (si > -1) {
							tempBean.setParameter("-sort" + sortCount++, field.substring(0, si));
						} else if ("SalesRep".equals(field)) {
							tempBean.setParameter("-sort" + sortCount++, "RepFirstName");
							tempBean.setParameter("-sort" + sortCount++, "RepLastName");
						} else if ("OppName".equals(field)) {
							tempBean.setParameter("-sort" + sortCount++, "Opportunities.Name");
						} else if ("Contact".equals(field)) {
							tempBean.setParameter("-sort" + sortCount++, "FirstName");
							tempBean.setParameter("-sort" + sortCount++, "LastName");
						} else {
							tempBean.setParameter("-sort" + sortCount++, field);
						}
					}
				}

				if (i == lastColumn)
					break;
			}
		}
		initializePagingRowSetBean(request);
	}

	private void parseRequestObject(HttpServletRequest request) {
		setContextPath(request.getContextPath());
		setAction(request.getParameter("-action"));
		// setRequestPath(request.getRequestURI());
		setRequestPath(request.getContextPath() + "/runway/process.list");
		setPageNum(request.getParameter("page"));
	}

	@Override
	public void initializePagingRowSetBean(final HttpServletRequest request) {
		pagingRowSetBean = new OpportunityPagingRowSetBean();
		pagingRowSetBean.setPageSize(getUserDefaultListSize());
		pagingRowSetBean.setRequest(request);
		pagingRowSetBean.setSearchBean(searchbean);
		pagingRowSetBean.getResults();
		pagingRowSetBean.setPageUrl(getRequestPath());

		resultSet = pagingRowSetBean.getResultBean();
	}

	public class OpportunityPagingRowSetBean extends PagingRowSetBean {
		@Override
		public boolean getNext() {
			boolean ret = super.getNext();
			resultSet.put("contextPath", getContextPath());
			if (resultSet.isSet("ProductID")) {
				getProductBean(resultSet.getString("ProductID"));
			} else if (resultSet.isSet("ReferenceProductID")) {
				getProductBean(resultSet.getString("ReferenceProductID"));
			}
			return ret;
		}
	}
}
