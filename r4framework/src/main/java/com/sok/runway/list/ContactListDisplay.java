package com.sok.runway.list;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.*;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.TableData;
import com.sok.runway.SessionKeys;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContactListDisplay extends ListDisplay {
 
	private static final Logger logger = LoggerFactory.getLogger(ContactListDisplay.class);
	
	private static final String DISPLAY_SPEC = "ContactListDisplay";
	private static final String VIEW_SPEC = "ContactListView";
	private static final String CONTACT_GROUP = "ContactContactGroups.ContactGroupGroup";
	private static final String CONTACT_REP = "ContactContactGroups";

	
	private static final double AVAILABLE_WIDTH = 85.0;


	private boolean campaignRemoval = false;
	private String type;
	private String showCompanyProfilesForContact;
	private String newSavedSearch = ActionBean._blank;
	private String originalCount;
	private String showlist;
	private String applyEvent;
	private String preFilterCount;
	private String bulkFilter = "";
	private String filterType = "";
	private String applyCampaign = "";
	private boolean assigning = false;
	private boolean reminders;
	private boolean canSendSMS;
	private boolean isB2B;
	private boolean goToViewPage=false;
	
	public ContactListDisplay(HttpServletRequest request, HttpServletResponse response) {
		super(request, DISPLAY_SPEC);	
		logger.debug("ContactListDisplay(request, response) [{}]");
		initializeObject(request);
	}
	
	public ContactListDisplay(HttpServletRequest request, String requestPath) {
		super(request, DISPLAY_SPEC);	
		setRequestPath(requestPath);
		logger.debug("ContactListDisplay(request, response) [{}]");
		initializeObject(request);
	}
	
	private void initializeObject(final HttpServletRequest request) {
		this.layoutID = getDefaultLayoutID(request, user.getUserID(), "Contacts");
		/*
		 * 
		 * TODO - allow user to select group for showing group statuses. 
		List<String[]> visibleGroupList = (List<String[]>)request.getSession().getAttribute("VisibleGroups");
		if(visibleGroupList == null) {
			GenRow visibleGroups = new GenRow();
	        try { 
		        visibleGroups.setViewSpec("GroupView");
		        visibleGroups.setConnection(con);
		        currentuser.setConstraint(visibleGroups);
		        visibleGroups.sortBy("Groups.Name",0);
		        visibleGroups.sortBy("Groups.SectionCode",1);
		        visibleGroups.doAction("search");
	        	visibleGroups.getResults(true);
	        	if(visibleGroups.getNext()) {
	        		visibleGroupList = new ArrayList<String[]>(visibleGroups.getSize());
	        		do {
	        			groupList.add(new VisibleGroup(visibleGroups, getGroupStatusList(con, currentuser, visibleGroups.getData("GroupID"), GroupStatusType.Contacts)));
	        		} while(visibleGroups.getNext());
	        		return groupList;
	        	}
	        	return Collections.emptyList();
	        } finally {
	        	visibleGroups.close();
	        }
		}
		*/
		setUserDefaultListSize(user.getDefaultListSize());            
		parseRequestObject(request);
		this.reminders = "true".equals(getSysProperty("Reminders"));
		this.canSendSMS = "true".equals(getSysProperty("SendSMS"));

		if(getAction().equals("list")) 
		{
			logger.debug("list action create search bean");
			createSearchBean();
			setViewSpec(VIEW_SPEC);

	      	user.setConstraint(getSearchbean(), CONTACT_GROUP);
	      	user.setDefaults(getSearchbean(), CONTACT_REP, false);

			sortColumnBy("Contacts.ModifiedDate",0); 
			sortOrder("DESC",0); 
			setTop(getUserDefaultListSize()); 

			generateSQLStatement(); 
		}
		else 
		{
			setSearchbean( (TableData)request.getSession().getAttribute(SessionKeys.CONTACT_SEARCH) );
			setViewSpec(VIEW_SPEC);
			
			this.filterType = getData("-filterType");
			this.showCompanyProfilesForContact = getData("-showCompanyProfilesForContact");

			setColumn("-filterType", "");
			
			if( StringUtils.isNotBlank( request.getParameter("-sortBy") )){
				sortColumnBy(request.getParameter("-sortBy"),0);
				setSortByOneListHeadingOnly(true);
			}

			if( StringUtils.isNotBlank( request.getParameter("-sortOrder") )){
				sortOrder(request.getParameter("-sortOrder"),0);
			}
			
			performSort(request);
			
			if (applyCampaign != null)
			{
				this.applyCampaign = getColumn("applycampaign");			
			}
			
			this.applyEvent = getColumn("applyevent");
			
			this.campaignRemoval = "true".equals(getParameter("-campaignRemoval"));
						
			this.newSavedSearch = getColumn("-newSavedSearch");
			
			if (applyCampaign.length() != 0 || applyEvent.length() != 0) 
			{
				this.assigning = true;
			}
			
		}
		
		preFilterCount = getData("-preFilterCount");
		
		/* custom headers */
		listHeadings(request);
		List<String> listSpec = getListSpec();
		List<String> sortFields = getSortFields();
		
		GenRow tempBean = null;
		final int lastColumn = getNumberOfColumns() - 1;
	   int joinCount = 0;
	   int selectCount = 0;
	   int sortCount = 0;
	   
	   logger.debug("before custom fields");
	   
	   if(lastColumn >= 0) {
		   Set<String> joins = new HashSet<String>();
		   
		   logger.debug("testing custom fields");
		   
		   tempBean = new GenRow();
		   tempBean.setRequest(request);
		   tempBean.setViewSpec(VIEW_SPEC);
		   
		   /* add the joins, then the search, then the sorting. */ 
		   for(int i=0; ; i++) {
			   final String field = listSpec.get(i);
			   logger.debug("testing fields {}", field);
			   if(field.indexOf(".")>-1) {
				   	final String join = field.substring(0, field.lastIndexOf("."));
				   	if(!joins.contains(join)) {
				   		logger.debug("adding join {}", join);
				   		tempBean.setParameter("-join" + joinCount++, join);
				   		joins.add(join);
				   	}
				   	tempBean.setParameter("-select" + selectCount++, field);
			   }
			   
			   if(i == lastColumn)
				   break;
		   }
		   
		   tempBean.parseRequest(getSearchbean().toString());
		   logger.debug(getSearchbean().toString());
		   logger.debug(tempBean.toString());
		   setSearchbean(tempBean);
		   
		   for(int i=0; i<10; i++) {
			   if(!tempBean.isSet("-groupby"+i)) {
				   tempBean.setParameter("-groupby"+i,"ContactID");
				   break;
			   }
		   }
		   
		   for(int i=0; ; i++) {
			   final String field = listSpec.get(i);
		   
			   if(StringUtils.isNotBlank(sortFields.get(i))) {
			   		int si = field.indexOf(" ");
			   		boolean doSort = true;
			   		
			   		//out.println("Sort:"+sortFields.get(i)+":"+field+"<br/>");
			   		
			   		if("D".equals(sortFields.get(i))) { 
			   			tempBean.setParameter("-order" + sortCount, "DESC");
			   		} else if("A".equals(sortFields.get(i))) { 
			   			tempBean.setParameter("-order" + sortCount, "ASC");
			   		} else {
			   			doSort = false;
			   		}
			   		if(doSort) { 
				   		if(si > -1) {				   			

				   			boolean t = true;
				   			String sortField = field.substring(0, si);
				   			logger.debug("Addding sort for {}" , sortField);
				   			if(sortField.contains("#ContactAnswer") && isNumericProfileAnswer(request, sortField)) {
				   				tempBean.setParameter("-sort" + sortCount++, "ABS(" + sortField + ")");
				   			} else {
				   				tempBean.setParameter("-sort" + sortCount++, sortField);
				   			}
				   		//} else if ("SalesRep".equals(field)) {
				   		//	tempBean.setParameter("-sort" + sortCount++, "RepFirstName");
				   		//	tempBean.setParameter("-sort" + sortCount++, "RepLastName");
				   		} else if ("Contact".equals(field)) {
				   			tempBean.setParameter("-sort" + sortCount++, "FirstName");
				   			tempBean.setParameter("-sort" + sortCount++, "LastName");
				   		} else { 
				   			tempBean.setParameter("-sort" + sortCount++, field);
				   		}
			   		} 
			   	}
		   
			   if(i == lastColumn)
					   break;
		   }
	   } 
		   
		/* custom headers */
		initializePagingRowSetBean(request);
		setOriginalCount();

		this.isB2B = "B2B".equals(getSysProperty("ClientType"));
		
		boolean includeHeaders = !"false".equals(request.getParameter("-includeHeaders"));

		if(getSizeOfPagingRowSetBean()==1 && !assigning&& !getType().equals("inbound") && !"true".equals(showlist))
		{
			this.goToViewPage = true;
			getNext();			
			request.setAttribute("-redirect",getContextPath() + "/runway/contact.view?search=true&-action=select&ContactID="+getData("ContactID"));
		}	
	}

	private boolean isNumericProfileAnswer(HttpServletRequest request, String sortField) {
		// for profile answers, use the numeric sorting, QuestionContactAnswer#ContactAnswerQuestionID=0D1H4L0X2R5B324X1D328V2O706B.Answer
		//Extract QuestionID
		int startIndex = sortField.indexOf("=");
		if(startIndex > 0 && sortField.indexOf(".Answer") > 0) {
			logger.debug("Extracting QuestionID for {} - {}" , startIndex + 1, sortField.indexOf(".Answer"));
			String questionID = sortField.substring(startIndex + 1, sortField.indexOf(".Answer"));
			logger.debug("QuestionID is {}" , questionID);
			
			GenRow	questionRow = new GenRow();	
		 	questionRow.setRequest(request);
		 	questionRow.setViewSpec("QuestionView");
		 	
		 	if(StringUtils.isNotBlank(questionID)) {
		 		questionRow.setParameter("QuestionID", questionID);
		 		questionRow.setParameter("InputType", "Number");
		 		questionRow.doAction("selectfirst");
		 		logger.debug("QuestionID statement :  {}" , questionRow.getStatement());
		 		return questionRow.isSuccessful();
		 	}
		}
		return false;
	}

	private void parseRequestObject(HttpServletRequest request)
	{

		if (request.getParameter("type")!=null)
			this.type = request.getParameter("type");
		else
			this.type = "";	

		setContextPath(request.getContextPath());
		if(getRequestPath() == null) 
			setRequestPath(request.getRequestURI());
		setAction(request.getParameter("-action"));	   	   		
		setPageNum(request.getParameter("-page"));
		
		this.showlist = request.getParameter("showlist");

	}

	private void setOriginalCount()
	{
		if (preFilterCount.length() == 0) 
		{
			this.originalCount = String.valueOf(getSizeOfPagingRowSetBean());
		}
		else 
			this.originalCount = preFilterCount;
	}
	
	public String getOriginalCount()
	{
		return originalCount;
	}

	public String getShowCompanyProfilesForContact() {
		return showCompanyProfilesForContact;
	}

	public String getNewSavedSearch() {
		return newSavedSearch;
	}

	public String getType() {
		return type;
	}

	public String getApplyEvent() {
		return applyEvent;
	}

	public boolean getCampaignRemoval()
	{
		return campaignRemoval;
	}
	
	public String getBulkFilter()
	{
		return bulkFilter;
	}
	
	public String getFilterType() {
		return filterType;
	}

	public String getApplyCampaign() {
		return applyCampaign;
	}

	public boolean getAssigning()
	{
		return assigning;   	   	
	}

	public boolean isReminders() {
		return reminders;
	}

	public boolean canSendSMS() {
		return canSendSMS;
	}

	public boolean isB2B() {
		return isB2B;
	}
	
	public boolean goToViewPage() {
		return goToViewPage;
	}
}    

