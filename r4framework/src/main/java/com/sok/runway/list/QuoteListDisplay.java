package com.sok.runway.list;

import javax.servlet.http.*;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.PagingRowSetBean;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.generation.Row;
import com.sok.framework.generation.nodes.LeafNode;
import com.sok.framework.generation.nodes.Node;
import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;
import com.sok.runway.crm.cms.properties.PropertyEntity;
import com.sok.runway.crm.cms.properties.PropertyFactory;
import com.sok.service.crm.UserService;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringWriter;
import java.io.Writer;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class QuoteListDisplay extends ListDisplay {

	private static final Logger logger = LoggerFactory.getLogger(QuoteListDisplay.class);
	
	private static final String DISPLAY_SPEC = "QuoteListDisplay";
	private static final String VIEW_SPEC = "OrderView";

	private static final String[] DEFAULT_LIST_HEADINGS = {"Quote No.", "Name","Sales Rep","Contact","Company","Quote Date","Outcome Date","Probability", "Cost","Total Cost"};
	private static final String[] DEFAULT_LIST_SPEC = {"OrderNum", "Name","SalesRep","Contact","Company","DateOfSale","OutcomeDate","Probability", "Cost","TotalCost"};
	private static final String[] DEFAULT_COLUMN_WIDTHS = {"6","16","12","13","14","10","7","7","6","6"};
	
	
	private static final String[] DEFAULT_LIST_LINKS = {null, 
														"${contextPath}#if(${QuoteViewID})#**#/crm/orders/views/index.jsp?#**##else#**#/runway/order.view?-action=select&#**##end#**#OrderID=${OrderID}", null, 
														"#if(${ContactID})${contextPath}/runway/contact.view?ContactID=${ContactID}&-action=select#end",
														"#if(${CompanyID})${contextPath}/runway/company.view?CompanyID=${CompanyID}&-action=select#end",
														null,
														null,
														null,
														null,
														null};
														
	private static final String[] DEFAULT_LIST_DISPLAY = {null,
		"#if(${Name} && $Name.length()!=0)${Name}#else#**#&lt;No name specified&gt;#end",
		"$!{RepFirstName} $!{RepLastName}","$!{FirstName} $!{LastName}",null,null,null,null,null,null};
	
	private static final Map<String, ListField> FIELDS = new LinkedHashMap<String, ListField>(DEFAULT_LIST_HEADINGS.length);
	static {		
		final int max = DEFAULT_LIST_HEADINGS.length-1;
		for(int i=0; ; i++) {
			FIELDS.put(DEFAULT_LIST_SPEC[i], new ListField(DEFAULT_LIST_HEADINGS[i], DEFAULT_LIST_SPEC[i], DEFAULT_COLUMN_WIDTHS[i],
					DEFAULT_LIST_LINKS[i], DEFAULT_LIST_DISPLAY[i]));
			if(i==max) 
				break;
		}
	}

	public static final int NUM_OF_COLUMNS = DEFAULT_LIST_HEADINGS.length;
	private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	private final Date CURRENT_DATE = new Date();
	
	public QuoteListDisplay(HttpServletRequest request, HttpServletResponse response) {
		super(request, DISPLAY_SPEC);
		initializeObject(request,response);
	}
	
	public void writeField(String databaseName, TableData context, Writer out) throws IOException {
		if(StringUtils.isBlank(databaseName))
			return;
		//custom field probably, profile answer or similar.
		if(!FIELDS.containsKey(databaseName)) {
			 if(databaseName.indexOf(" as ") > -1)
				 databaseName = StringUtils.trim(databaseName.substring(databaseName.indexOf(" as ")+4));
			out.write(context.getData(databaseName));
			return;
		}
		ListField lf = FIELDS.get(databaseName);
		String text = lf.text != null ? evaluateField(lf.text, context) : context.getData(databaseName);
		
		String link = lf.link;
		boolean hasLink = StringUtils.isNotBlank(link);
		if("Name".equals(databaseName)) {
			try {
				if(StringUtils.isNotBlank(context.getString("OutcomeDate")) && CURRENT_DATE.after(df.parse(context.getString("OutcomeDate")))) {
					link = "javascript:alert('This quote is expired, and can no longer be viewed.')";
				} else {
					logger.debug("Date ok for {}", context.getString("OutcomeDate"));
				}
			} catch (ParseException pe) {
				logger.error("Error parsing date", pe);
			}
		} else if("Contact".equals(databaseName) && !user.canAccess("Contacts")) {
			hasLink = false;
		} else if("Company".equals(databaseName) && !user.canAccess("Companies")) {
			hasLink = false;
		}
		
		if(hasLink) {
			out.write("<a href=\"");
			out.write(evaluateField(link, context));
			out.write("\" title=\"");
			out.write(text);
			out.write("\">");
		}
		out.write(text);
		if(hasLink) {
			out.write("</a>");
		}
		return;
	}
	/*
	public String getFieldText(String databaseName, TableData context) {
		if(StringUtils.isBlank(databaseName))
			return "";
		//custom field probably, profile answer or similar.
		if(!FIELDS.containsKey(databaseName)) {
			 if(databaseName.indexOf("as") > -1)
				 databaseName = StringUtils.trim(databaseName.substring(databaseName.indexOf("as")+2));
			return context.getData(databaseName);
		}
		ListField lf = FIELDS.get(databaseName);
		return lf.text != null ? evaluateField(lf.text, context) : context.getData(databaseName);
	}
	*/
	private void initializeObject(final HttpServletRequest request, final HttpServletResponse response) {
		setDisplaySpec(DISPLAY_SPEC);
		parseRequestObject(request);
		this.layoutID = getDefaultLayoutID(request, user.getUserID(), "Orders");

		if(getAction().equals("list")) 
		{
			createSearchBean();
			setViewSpec(VIEW_SPEC);
	      	user.setConstraint(getSearchbean(),"OrderGroup");
	      	user.setDefaults(getSearchbean(), null, false);
	        if (!user.canManage()) {
	            setParameter("RepUserID", user.getUserID());
	        }
	        
	    	String reqSortBy = request.getParameter("-sortBy");
	    	String reqOrderBy = request.getParameter("-sortOrder");
	    	if (reqSortBy == null || "".equalsIgnoreCase(reqSortBy) || "null".equalsIgnoreCase(reqSortBy)) {
	    		reqSortBy = "";
	    	} else {
	    		if ("Company".equalsIgnoreCase(reqSortBy)) {
	    			reqSortBy = "CompanyName";
	    		}
	    	}
	    	
	    	if (reqOrderBy == null || "".equalsIgnoreCase(reqOrderBy) || "null".equalsIgnoreCase(reqOrderBy)) {
	    		reqOrderBy = "";
	    	}
    		logger.info("reqSortBy: " + reqSortBy + "  ,  reqOrderBy: " + reqOrderBy);

	    	
	    	if (!"".equalsIgnoreCase(reqSortBy) && !"".equalsIgnoreCase(reqOrderBy)) {
	    		
		    	if ("RepFirstName".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("RepFirstName", 0);
		    		sortColumnBy("RepLastName", 1);
	    		} 
		    	else if ("Contact".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("FirstName",0);
		    		sortColumnBy("LastName",1);
	    		} 
		    	else if ("QuoteDate".equalsIgnoreCase(reqSortBy)) {
		    		sortColumnBy("Orders.DateOfSale",0);
	    		} 
		    	else {
		    		sortColumnBy(("Orders."+reqSortBy),0);
	    		}
	    		sortOrder(reqOrderBy,0);
	    		
	    	} else {	        
				sortColumnBy("Orders.DateOfSale",0);
				sortOrder("DESC",0);
				sortColumnBy("Orders.TimeOfSale",1);
				sortOrder("DESC",1);
				sortColumnBy("Orders.OutcomeDate",2);
				sortOrder("DESC",2);
	    	}
	    	
			// TODO - not required ?
			getSearchbean().setTop(getUserDefaultListSize());
			getSearchbean().doAction(ActionBean.SEARCH);
			request.getSession().setAttribute(SessionKeys.ORDER_SEARCH, getSearchbean());
		}
		else 
		{
			setSearchbean( (TableData)request.getSession().getAttribute(SessionKeys.ORDER_SEARCH) );
			if( StringUtils.isNotBlank( request.getParameter("-sortBy") )){
				sortColumnBy(request.getParameter("-sortBy"),0);
				setSortByOneListHeadingOnly(true);
			}

			if( StringUtils.isNotBlank( request.getParameter("-sortOrder") )){
				sortOrder(request.getParameter("-sortOrder"),0);
			}
		}			

		listHeadings(request);
		
		List<String> listSpec = getListSpec();
		List<String> sortFields = getSortFields();

		GenRow tempBean = null;
		final int lastColumn = getNumberOfColumns() - 1;
		int joinCount = 0;
		int selectCount = 0;
		int sortCount = 0;

		if(lastColumn >= 0) {
			Set<String> joins = new HashSet<String>();

			tempBean = new GenRow();
			tempBean.setRequest(request);
			tempBean.setViewSpec(VIEW_SPEC);

			/* add the joins, then the search, then the sorting. */ 
			for(int i=0; ; i++) {
				final String field = listSpec.get(i);
				if(field.indexOf(".")>-1) {
					final String join = field.substring(0, field.lastIndexOf("."));
					if(!joins.contains(join)) {
						tempBean.setParameter("-join" + joinCount++, join);
						joins.add(join);
					}
					tempBean.setParameter("-select" + selectCount++, field);
				}

				if(i == lastColumn)
					break;
			}
			tempBean.parseRequest(getSearchbean().toString());
			logger.debug(getSearchbean().toString());
			logger.debug(tempBean.toString());
			setSearchbean(tempBean);
			for(int i=0; i<10; i++) {
				if(!tempBean.isSet("-groupby"+i)) {
					tempBean.setParameter("-groupby"+i,"OrderID");
					break;
				}
			}
			for(int i=0; ; i++) {
				final String field = listSpec.get(i);
				if(StringUtils.isNotBlank(sortFields.get(i))) {
					int si = field.indexOf(" ");
					boolean doSort = true;
					if("D".equals(sortFields.get(i))) { 
						tempBean.setParameter("-order" + sortCount, "DESC");
					} else if("A".equals(sortFields.get(i))) { 
						tempBean.setParameter("-order" + sortCount, "ASC");
					} else {
						doSort = false;
					}
					if(doSort) { 
						if(si > -1) {
							tempBean.setParameter("-sort" + sortCount++, field.substring(0, si));	
						} else if ("SalesRep".equals(field)) {
							tempBean.setParameter("-sort" + sortCount++, "RepFirstName");
							tempBean.setParameter("-sort" + sortCount++, "RepLastName");
						} else if ("Contact".equals(field)) {
							tempBean.setParameter("-sort" + sortCount++, "FirstName");
							tempBean.setParameter("-sort" + sortCount++, "LastName");
						} else { 
							tempBean.setParameter("-sort" + sortCount++, field);
						}
					} 
				}
				if(i == lastColumn)
					break;
			}
		} 
		initializePagingRowSetBean(request);
	}

	private void parseRequestObject(HttpServletRequest request)
	{
		setContextPath(request.getContextPath());
		setAction(request.getParameter("-action"));				
		setRequestPath(request.getRequestURI());
	}
	
	@Override
	public void initializePagingRowSetBean(final HttpServletRequest request)
	{
		pagingRowSetBean = new OrderPagingRowSetBean();
		pagingRowSetBean.setPageSize(getUserDefaultListSize());
		pagingRowSetBean.setRequest(request);
		pagingRowSetBean.setSearchBean(searchbean);
		pagingRowSetBean.getResults();
		pagingRowSetBean.setPageUrl(getRequestPath());	

		resultSet = pagingRowSetBean.getResultBean();
	}
	
	public class OrderPagingRowSetBean extends PagingRowSetBean {
		@Override
		public boolean getNext() {
			boolean ret = super.getNext();
			resultSet.put("contextPath",getContextPath());
			if(resultSet.isSet("ProductID")) {
				getProductBean(resultSet.getString("ProductID"));
			}
			return ret;
		}
	}
}    

