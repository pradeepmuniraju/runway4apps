package com.sok.runway.list;

import javax.servlet.http.*;

import com.sok.framework.ActionBean;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;

import org.apache.commons.lang.StringUtils;


public class CompanyListDisplay extends ListDisplay {

	private static final String DISPLAY_SPEC = "CompanyListDisplay";
	private static final String VIEW_SPEC = "CompanyListView";

	private static final String COMPANY_GROUP = "CompanyCompanyGroups.CompanyGroupGroup";
	private static final String COMPANY_REP = "CompanyCompanyGroups"; 
	/*
	private static final String[] DEFAULT_LIST_HEADINGS = {"Company", "Street","Street 2","City","State","Postcode"};
	private static final String[] DEFAULT_LIST_SPEC = {"Company", "Street","Street2","City","State","Postcode"};
	private static final String[] DEFAULT_COLUMN_WIDTHS = {"25","15","15","15","5","5"};

	private static final double AVAILABLE_WIDTH = 85.0;
	public static final int NUM_OF_COLUMNS = DEFAULT_LIST_HEADINGS.length;
	*/

	private String originalCount;
	private String showlist;
	private String assignCompanyLink = "";
	private String companyName;
	private boolean isSearch = false;
	private String workflowSearch;
	private boolean hasForecast;
	private String targetName;
	private String preFilterCount;
	private String bulkFilter = "";
	private String filterType = "";
	private String applyCampaign = "";
	private boolean assigning = false;
	private boolean campaignRemoval = false;
	private boolean goToViewPage = false;

	public CompanyListDisplay(HttpServletRequest request, HttpServletResponse response) {
		super(request, DISPLAY_SPEC);
		initializeObject(request,response);
	}
	
	private void initializeObject(final HttpServletRequest request, final HttpServletResponse response) {
		setDisplaySpec(DISPLAY_SPEC);
		this.layoutID = getDefaultLayoutID(request, user.getUserID(), "Companies");
		setUserDefaultListSize(user.getDefaultListSize());       
		parseRequestObject(request);
		
		if(assignCompanyLink.length() > 0 ) {
			this.assigning = true;
		}

		if(getAction().equals("list")) 
		{
			createSearchBean();
			setViewSpec(VIEW_SPEC);

			user.setConstraint(getSearchbean(),COMPANY_GROUP);
			user.setDefaults(getSearchbean(), COMPANY_REP, true);

			sortColumnBy("Companies.ModifiedDate",0); 
			sortOrder("DESC",0); 
			setTop(getUserDefaultListSize()); 
			getSearchbean().doAction(ActionBean.SEARCH);	
			generateSQLStatement(); 
		}
		else 
		{
			setSearchbean( (TableData)request.getSession().getAttribute(SessionKeys.COMPANY_SEARCH) );
			setViewSpec(VIEW_SPEC);
			
			this.bulkFilter = getData("-bulkFilter");
			this.filterType = getData("-filterType");

			setColumn("-filterType", "");
			
			this.isSearch = true;


			if( StringUtils.isNotBlank( request.getParameter("-sortBy") )){
				sortColumnBy(request.getParameter("-sortBy"),0);
				setSortByOneListHeadingOnly(true);
			}

			if( StringUtils.isNotBlank( request.getParameter("-sortOrder") )){
				sortOrder(request.getParameter("-sortOrder"),0);
			}
			
			performSort(request);
			
			this.applyCampaign = getColumn("applycampaign");
			this.campaignRemoval = "true".equals(getParameter("-campaignRemoval"));
			
			this.workflowSearch = getData("-workflowSearch");
			
			if(workflowSearch.length() > 0) {
				this.assigning = true;
			}
								
		}

		preFilterCount = getData("-preFilterCount");
		initializePagingRowSetBean(request);
		setOriginalCount();

		if(getSizeOfPagingRowSetBean()==1 && !assigning&& !"true".equals(showlist))
		{
			getNext();
			this.goToViewPage = true;
			request.setAttribute("-redirect",getContextPath() + "/runway/company.view?search=true&-action=select&CompanyID="+getData("CompanyID"));	
		}
		else
		{		
			this.hasForecast = isItemUsed("CompanyDisplay.ForecastList");
			
		   	StringBuffer thispage = new StringBuffer();
		   	thispage.append(request.getRequestURI());
		   	thispage.append("?");
		   	thispage.append(request.getQueryString());
		   	this.targetName = StringUtil.urlEncode(thispage.toString());			
						
			listHeadings(request);			
		}

	}

	private void parseRequestObject(HttpServletRequest request)
	{
		setContextPath(request.getContextPath());
		setRequestPath(request.getRequestURI());
		setAction(request.getParameter("-action"));	   	   		
		setPageNum(request.getParameter("-page"));
		
		if( StringUtils.isNotBlank( request.getParameter("assigncompanylink")) ){
			this.assignCompanyLink = request.getParameter("assigncompanylink");
		}
		
		this.companyName = request.getParameter("CompanyName");
		
		this.showlist = request.getParameter("showlist");
	}

	private void setOriginalCount()
	{
		if (preFilterCount.length() == 0) 
		{
			this.originalCount = String.valueOf(getSizeOfPagingRowSetBean());
		}
		else 
			this.originalCount = preFilterCount;

	}
	
	public String getOriginalCount()
	{
		return originalCount;
	}

	public String getApplyCampaign() {
		return applyCampaign;
	}

	public String getCompanyName() {
		return companyName;
	}

	public boolean isSearch() {
		return isSearch;
	}

	public boolean hasForecast() {
		return hasForecast;
	}

	public String getTargetName() {
		return targetName;
	}

	public String getBulkFilter() {
		return bulkFilter;
	}

	public String getFilterType() {
		return filterType;
	}

	public boolean isCampaignRemoval() {
		return campaignRemoval;
	}	

	public boolean isAssigning() {
		return assigning;
	}
	
	public String getAssignCompanyLink() {
		return assignCompanyLink; 
	}
	
	public String getWorkFlowSearch() {
		return workflowSearch; 
	}
	
	public boolean goToViewPage() {
		return goToViewPage;
	}
}    

