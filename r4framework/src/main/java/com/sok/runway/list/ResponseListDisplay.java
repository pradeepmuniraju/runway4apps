package com.sok.runway.list;

import javax.servlet.http.*;

import com.sok.framework.ActionBean;
import com.sok.framework.PagingRowSetBean;
import com.sok.framework.TableData;
import com.sok.runway.UserBean;
import com.sok.runway.list.QuoteListDisplay.OrderPagingRowSetBean;

import org.apache.commons.lang.StringUtils;

public class ResponseListDisplay extends ListDisplay {

	private static final String DISPLAY_SPEC = "ResponseListDisplay";
	private static final String VIEW_SPEC = "ResponseListView";
	private static final String SEARCH_NAME = "currentresponsesearch";

	private boolean isSearch = false;
	private String originalCount;
	private String bulkFilter = "";
	private String filterType = "";
	private String preFilterCount;

	public ResponseListDisplay(HttpServletRequest request, HttpServletResponse response) {
		super(request, DISPLAY_SPEC); 
		//super(NUM_OF_COLUMNS);
		initializeObject(request,response);
	}
	
	private void initializeObject(final HttpServletRequest request, final HttpServletResponse response) {
		setDisplaySpec(DISPLAY_SPEC);            
		parseRequestObject(request);

		if(getAction().equals("list")) 
		{
			createSearchBean();
			setViewSpec(VIEW_SPEC);
			
			setColumn("-join1","ResponseContact");
	      	user.setConstraint(getSearchbean(),"ResponseContact.ContactContactGroups.ContactGroupGroup");
	      	user.setDefaults(getSearchbean(), "ResponseContact.ContactContactGroups", false);

			sortColumnBy("Responses.ModifiedDate",0); 
			sortOrder("DESC",0);
					
			setTop(getUserDefaultListSize()); 
			getSearchbean().doAction(ActionBean.SEARCH);	
		}
		else 
		{
			setSearchbean( (TableData)request.getSession().getAttribute(SEARCH_NAME) );
			this.bulkFilter = getData("-bulkFilter");
			this.filterType = getData("-filterType");
			
			setColumn("-filterType", "");
			isSearch = true;
		}	
			
		if( StringUtils.isNotBlank( request.getParameter("-sortBy") )){
			sortColumnBy(request.getParameter("-sortBy"),0);
			setSortByOneListHeadingOnly(true);
		}

		if( StringUtils.isNotBlank( request.getParameter("-sortOrder") )){
			sortOrder(request.getParameter("-sortOrder"),0);
		}
		
		performSort(request);
		
		if ("true".equals(getData("-fromAssistant"))) {
			request.getSession().removeAttribute("numReminders");
		}
		
		preFilterCount = getData("-preFilterCount");
		
		initializePagingRowSetBean(request);
		
		setOriginalCount();

		listHeadings(request); 		


	}

	private void parseRequestObject(HttpServletRequest request)
	{
		setContextPath(request.getContextPath());
		setPageNum(request.getParameter("page"));			
		setAction(request.getParameter("-action"));				
		setRequestPath(request.getRequestURI());			   	   		
					
	}
	
	private void setOriginalCount()
	{
		if (preFilterCount.length() == 0) 
		{
			this.originalCount = String.valueOf(getSizeOfPagingRowSetBean());
		}
		else 
			this.originalCount = preFilterCount;
	}
	
	public String getOriginalCount()
	{
		return originalCount;
	}
	
	public String getBulkFilter()
	{
		return bulkFilter;
	}
	
	public String getFilterType() {
		return filterType;
	}

	public boolean isSearch() {
		return isSearch;
	}	
}    

