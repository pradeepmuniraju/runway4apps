package com.sok.runway.list;

import javax.servlet.http.*;

import com.sok.framework.ActionBean;
import com.sok.framework.TableData;
import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;

import org.apache.commons.lang.StringUtils;

public class NoteListDisplay extends ListDisplay {

	private static final String DISPLAY_SPEC = "NoteListDisplay";
	private static final String VIEW_SPEC = "NoteListView";

	private static final double AVAILABLE_WIDTH = 90.0;

	@SuppressWarnings("deprecation")
	public NoteListDisplay(HttpServletRequest request, HttpServletResponse response) {

		super(request, DISPLAY_SPEC);
		initializeObject(request,response);
	}
	
	private void initializeObject(final HttpServletRequest request, final HttpServletResponse response) {
		setDisplaySpec(DISPLAY_SPEC);            
		parseRequestObject(request);

		//TODO: FIXME - getAction().equals(null) is always false
		if(getAction().equals("list") || getAction().equals(null)) 
		{
			createSearchBean();
			setViewSpec(VIEW_SPEC);

	      	user.setConstraint(getSearchbean(),"NoteGroup");
	      	user.setDefaults(getSearchbean(), null, false);

			sortColumnBy("Notes.ModifiedDate",0); 
			sortOrder("DESC",0);
			sortColumnBy("Notes.NoteDate",1); 
			sortOrder("DESC",1);
			sortColumnBy("Notes.NoteTime",2); 
			sortOrder("DESC",2);			
			
			setTop(getUserDefaultListSize()); 
			getSearchbean().doAction(ActionBean.SEARCH);	
			generateSQLStatement(); 
		}
		else 
		{
			setSearchbean( (TableData)request.getSession().getAttribute(SessionKeys.NOTE_SEARCH) );
		}	

		if( StringUtils.isNotBlank( request.getParameter("-sortBy") )){
			sortColumnBy(request.getParameter("-sortBy"),0);
			setSortByOneListHeadingOnly(true);
		}

		if( StringUtils.isNotBlank( request.getParameter("-sortOrder") )){
			sortOrder(request.getParameter("-sortOrder"),0);
		}
		
		performSort(request);

		if ("true".equals(getData("-fromAssistant"))) {
			request.getSession().removeAttribute("numReminders");
		}

		initializePagingRowSetBean(request);
		listHeadings(request);			
	}

	private void parseRequestObject(HttpServletRequest request)
	{
		setContextPath(request.getContextPath());
		setAction(request.getParameter("-action"));				
		setRequestPath(request.getRequestURI());			   	   		
		setPageNum(request.getParameter("page"));				
	}

}    

