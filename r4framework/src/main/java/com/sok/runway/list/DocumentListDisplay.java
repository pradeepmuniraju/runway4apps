package com.sok.runway.list;

import javax.servlet.http.*;

import com.sok.framework.ActionBean;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.runway.SessionKeys;
import com.sok.runway.UserBean;

import org.apache.commons.lang.StringUtils;

public class DocumentListDisplay extends ListDisplay {

	private static final String DISPLAY_SPEC = "DocumentListDisplay";
	private static final String VIEW_SPEC = "DocumentView";

	//private static final String[] DEFAULT_LIST_HEADINGS = {"Name", "Description","Document Type","Category","File Size","Created By", "Created Date"};
	//private static final String[] DEFAULT_LIST_SPEC = {"FileName", "Description","DocumentType","DocumentCategory","FileSize","DocumentCreated", "CreatedDate"};
	//private static final String[] DEFAULT_COLUMN_WIDTHS = {"25","30","15","10","8","12","13"};

	private static final double AVAILABLE_WIDTH = 100.0;
	//public static final int NUM_OF_COLUMNS = DEFAULT_LIST_HEADINGS.length;

	private String category = "Attached+MailAttachment";
	private String linkID = "";
	private String linkDocument = "";
	private String target;

	public DocumentListDisplay(HttpServletRequest request, HttpServletResponse response) {
		super(request, DISPLAY_SPEC);
		initializeObject(request,response);
	}
	
	private void initializeObject(final HttpServletRequest request, final HttpServletResponse response) {
		parseRequestObject(request);

		//Cannot use this while we use Table Bean!!!!!!!!! */
		/*if(getAction().equals("list")) 
		{
			createSearchBean();
			setViewSpec(VIEW_SPEC);
			setColumn("DocumentCategory","category");
											
	      	currentuser.setConstraint(getSearchbean(),"DocumentGroup");
	      	
			if(currentuser.getString("DefaultSearchGroup").equals("My Group")) 
			{ 
				setColumn("GroupID", currentuser.getString("DefaultGroupID"));
			}
			

			sortColumnBy("FileName",0); 
			generateSQLStatement();
			
			request.getSession().setAttribute("currentdocumentlist", getSearchbean());

		}
		else 
		{
			setSearchbean( (TableData)request.getSession().getAttribute("currentattacheddocsearch"));
		}	
		
				
		initializePagingRowSetBean(request);
		*/
		   StringBuffer thispage = new StringBuffer();
			thispage.append(request.getRequestURI());
			if (request.getQueryString() != null) {
		   	thispage.append("?");
		   	thispage.append(request.getQueryString());
		   }
			this.target = StringUtil.urlEncode(thispage.toString());
			
			listHeadings(request); 
		    //listHeadings(AVAILABLE_WIDTH, NUM_OF_COLUMNS, DEFAULT_LIST_HEADINGS, DEFAULT_LIST_SPEC, DEFAULT_COLUMN_WIDTHS, request);

			/*
			 int cols = 3; //default cols 
			
				if (dspec.isItemUsed("DocumentType")) { 
					cols++;
				} 
				if (dspec.isItemUsed("Description")) { 
					cols++;
				} 
				if (dspec.isItemUsed("DocumentCategory")) { 
					cols++;
				} 
				if (dspec.isItemUsed("DocumentStatus")) { 
					cols++;
				} 
				if (dspec.isItemUsed("ProductGroupName")) { 
					cols++;
				} 
				if (dspec.isItemUsed("DocumentCreated")) { 
					cols++;
				} 
				if (dspec.isItemUsed("FileSize")) {
					cols++; 
				} 
				if (dspec.isItemUsed("CheckedOutBy")) { 
					cols++;
					cols++; 
				} 		
			 */
		   
	}

	private void parseRequestObject(HttpServletRequest request)
	{
		setContextPath(request.getContextPath());
		setAction(request.getParameter("-action"));				
		setRequestPath(request.getRequestURI());
		
		if( request.getParameter("LinkDocument") != null)
			this.linkDocument = request.getParameter("LinkDocument");
		
		if( request.getParameter("LinkID") != null)
			this.linkID = request.getParameter("LinkID");
	}

}    

