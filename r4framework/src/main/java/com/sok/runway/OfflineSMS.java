package com.sok.runway;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.sql.Connection;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.velocity.app.VelocityEngine;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.RowBean;
import com.sok.framework.RowSetWrapperBean;
import com.sok.framework.RunwayUtil;
import com.sok.framework.SpecManager;
import com.sok.framework.StringUtil;
import com.sok.framework.TableBean;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.sms.SMSProvider;

public class OfflineSMS extends OfflineProcess
{
	StringBuffer status = null;

	UserBean user = null;
	TableData contactsearch = null;	
	GenRow contactselect = null;
	GenRow questionList = null;
	RowBean notesave = null;
	RowBean formbean = null;
	RowBean newletterdata = null;

	boolean debug = false;
	List<String> contacts = null;

	Connection con = null;
	Context ctx = null;
	SMSBean transport = null;
	VelocityEngine ve = null;
	
	boolean useRepMobile = false; 
	String groupID = "";
	
	int total = 0;
	int sent = 0;
	int failed = 0;

	int checkcount = -1;
	boolean newsletter = false;
	Properties sysprops = InitServlet.getSystemParams();
	String defaultSmsfrom = sysprops.getProperty("SMSFrom");
	
	private static final Logger logger = LoggerFactory.getLogger(OfflineSMS.class);
	
	String[][] keys = {	{"RepTitle", "Title"},
						{"RepFirstName", "FirstName"},
						{"RepLastName", "LastName"},
						{"RepCompany", "Company"},
						{"RepPosition", "Position"},
						{"RepEmail", "Email"},
						{"RepStreet", "Street"},
						{"RepStreet2", "Street2"},
						{"RepCity", "City"},
						{"RepState", "State"},
						{"RepPostcode", "Postcode"},
						{"RepCountry", "Country"},
						{"RepPhone", "Phone"},
						{"RepFax", "Fax"},
						{"RepMobile", "Mobile"}};
	
	public OfflineSMS(HttpServletRequest request, ServletContext context)
	{
		super(request, context);
		HttpSession session = request.getSession();
		user = (UserBean) session.getAttribute(SessionKeys.CURRENT_USER);
		contactsearch = (TableData) session.getAttribute(SessionKeys.CONTACT_SEARCH);
		
		if (contactsearch != null){
			contactsearch.setViewSpec("OfflineContactView");
			// we need to remove all group by
			for (int x = 0; x < 10; ++x) {
				contactsearch.remove("-groupby" + x);
				contactsearch.remove("-sort" + x);
				contactsearch.remove("-order" + x);
			}
			contactsearch.setParameter("-groupby0", "ContactID");

			boolean hasSMSLimit = StringUtils.isNotBlank(user.getString("BulkSMSLimit"));
		     if(hasSMSLimit)
		     {
		    	 int limitCount = RunwayUtil.getLeftSMSBulkLimit(request, user);
		    	 contactsearch.setTop(limitCount);
		    	 contactsearch.doAction("search");
		    	 logger.debug("Contact seach query : {}" , contactsearch.getStatement());
		     }
		}
	     
	     
		if("RepMobile".equals(request.getParameter("SMSFROM")))
				useRepMobile = true;
		
		groupID = request.getParameter("GroupID");
		start();
	}
	
	public OfflineSMS() {
		super();
	}
	
	
	void retrieveContacts()throws Exception
	{

		if(debug)
		{		  
			errors.append(contactsearch.getSearchStatement());
		}			
		try
		{
		   getConnection();
		   contactsearch.setConnection(getConnection());
		   RowSetWrapperBean list = new RowSetWrapperBean();		
		   list.setSearchBean(contactsearch);
			try { 
				   list.setConnection(getConnection());
			   list.getResults();
			   
			   TableData resultset = list.getResultBean();
			   while (list.getNext()) {
					contacts.add(resultset.getString("ContactID"));
				}		
			   logger.debug("Contact search statement : {}" , list.getSearchBean().getSearchStatement());
			    logger.debug("Contact search size : {}" , contacts.size());
				total = contacts.size();		      			
			} finally {
				list.close();
			}
		}		
		finally
		{    		
			try{
            	con.close();
        	}catch (Exception ex){}    		  		    
		}
		
		/*StringBuffer searchstmt = new StringBuffer();
		searchstmt.append("select Contacts.ContactID from Contacts where ");
		searchstmt.append(contactsearch.getSearchCriteria());

		if(debug)
		{
			errors.append(searchstmt.toString());
		}

		ResultSet current = null;
		Statement stmt = null;
		try
		{
			getConnection();
			stmt = con.createStatement();
			current = stmt.executeQuery(searchstmt.toString());
			while(current.next())
			{
				contacts.add(current.getString(1));
			}
			total = contacts.size();
		}
		finally
		{
			if (stmt != null) 
			{
				try{
				stmt.close();
				}catch (Exception ex){}
			}
			if (current != null) 
			{
				try{
				current.close();
				}catch (Exception ex){}
			}
		}*/

	}

	void sendMessages() throws Exception
	{
		try
		{
			transport = new SMSBean();
			transport.connect();
			
			ve = VelocityManager.getEngine();

			Iterator<String> ci = contacts.iterator();
			while(ci.hasNext() && !isKilled()) {
				if(sendSMS(ci.next()) == 0)
				{
					sent++;
				}
				else
				{
					failed++;
				}
			}
		}
		finally
		{
			if(transport!=null)
			{
				try{
					transport.close();
					transport = null;
				}catch(Exception te){}
			}
		}
	}

	TableData getContact(String contactid)
	{
		contactselect.clear();
		contactselect.setColumn("ContactID",contactid);

		try
		{
			contactselect.setConnection(getConnection());
			if(debug)
			{
				contactselect.doAction(GenerationKeys.SEARCH);
				errors.append(contactselect.getStatement());
				errors.append("\r\n");
			}

			contactselect.doAction(GenerationKeys.SELECT);
			// GroupID was getting killed here - ME
			String groupID = contactselect.getString("GroupID");
			contactselect.putAll(formbean);
			contactselect.put("GroupID",groupID);
			
			contactselect.put("AuthorFirstName",user.getString("FirstName"));
			contactselect.put("AuthorLastName",user.getString("LastName"));
			contactselect.put("AuthorPosition",user.getString("Position"));

			if(questionList == null) {
				questionList = new GenRow();
				questionList.setViewSpec("answers/ContactAnswerView");
			}
			try {
				questionList.clear();
				questionList.setColumn("ContactID",contactid);
				questionList.setConnection(contactselect.getConnection());	
				if (contactselect.isSet("Type")) {
					questionList.setColumn("ContactAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.ContactType",contactselect.getString("Type")+"+All");
				}
				else {
					questionList.setColumn("ContactAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.ContactType","All");
				}
				questionList.setColumn("ContactAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.DisplayInContactView","Y");
				questionList.getResults();
				while(questionList.getNext()) {
					contactselect.put("Profile-"+StringUtil.ToAlphaNumeric(questionList.getData("QuestionLabel")),questionList.getData("Answer"));
					contactselect.put("Profile-"+questionList.getData("QuestionID"),questionList.getData("Answer"));
				}
			} catch (Exception e) {
				status.append(e.toString());
			} finally {
				questionList.close();
			}
			
			GenRow repUser = getContactGroupRep(groupID, contactselect.getString("ContactID"), user.getString("UserID"));
			if (repUser.isSuccessful()) {
				for (int s = 0; s < keys.length; ++s) {
					contactselect.setParameter(keys[s][0], repUser.getString(keys[s][1]));
				}
			}

			if(debug)
			{
				if(contactselect.getError().length()!=0)
				{
					appendError(contactselect.getError(), contactid);
				}
			}

		}
		catch(Exception e)
		{
			appendError(e.toString(), contactid);
		}

		return(contactselect);
	}

	int sendSMS(String contactid)
	{
		String newnoteid = KeyMaker.generate();
		String emailstatus = "Sent";

		TableData contact = getContact(contactid);
		if(contact==null)
		{
			appendError("Contact not found", contactid);
			return(1);
		}
		
	   String mobileName = "Mobile";
	   if (contact.getString("MainPhoneField").startsWith("Mobile")) mobileName = contact.getString("MainPhoneField");

		String mobile = contact.getString(mobileName).replaceAll("[^0-9]", "");
		if (mobile == null || mobile.length() == 0) mobile = contact.getString("Mobile").replaceAll("[^0-9]", "");
		if (mobile == null || mobile.length() == 0) mobile = contact.getString("Mobile2").replaceAll("[^0-9]", "");
		if (mobile == null || mobile.length() == 0) mobile = contact.getString("Mobile3").replaceAll("[^0-9]", "");
		if (mobile == null || mobile.length() == 0) mobile = contact.getString("Mobile4").replaceAll("[^0-9]", "");
		
		String contactstatus = contact.getString("CurrentStatus");
		
		if(contactstatus != null && contactstatus.equals("OptOut"))
		{
			appendError("Opt-out contact", contactid);
			return(1);
		}
		if(contactstatus != null && contactstatus.equals("Left Company"))
		{
			appendError("Contact Left Company", contactid);
			return(1);
		}
		String contactstatusid = (String)contact.get("CurrentStatusID");

		if(contactstatusid != null && contactstatusid.equals(context.getInitParameter("ContactOptOutStatusID")))
		{
			appendError("Opt-out contact", contactid);
			return(1);
		}
		if(contactstatusid != null && contactstatusid.equals(context.getInitParameter("ContactLeftCompanyStatusID")))
		{
			appendError("Contact Left Company", contactid);
			return(1);
		}

		if(mobile == null || mobile.length()==0)
		{
			appendError("No mobile number", contactid);
			return(1);
		}


		String from = null;
		String emailfrom = formbean.getString("from");
		if(emailfrom.length()!=0)
		{
			if(emailfrom.equals("Support"))
			{
				from = contact.getString("RepSupportEmail");
			}
			else if(emailfrom.equals("User"))
			{
				from = user.getString("Email");
			}
			else if(emailfrom.indexOf("@")>0)
			{
				from = emailfrom;
			}
			else
			{
				from = contact.getString("RepEmail");
			}
		}
		else
		{
			from = contact.getString("RepEmail");
		}
		// since the from is not really used we will just use a standard one for now
		if(from == null || from.length()==0) from = "info@switched-on.com.au";
        formbean.put("FROM", from);
		if(from == null || from.length()==0)
		{
			appendError("no from email address", contactid);
			return(1);
		}

		/*
		InternetAddress fromAddress = null;
		try
		{
			fromAddress = new InternetAddress(from);
		}
		catch(Exception ex)
		{
			errors.append("invalid from email address");
			errors.append(" - ");
			errors.append(formbean.getString("URLHome"));
			errors.append("/crm/contactview.jsp?-action=select&ContactID=");
			errors.append(contactid);
			errors.append("\r\n");
			return(1);
		}
		*/
		contact.put("NoteID",newnoteid);
     
		StringWriter textw = new StringWriter();
		StringWriter sw = new StringWriter();


		try
		{
			String smsFrom = defaultSmsfrom;
			
			if(useRepMobile) {	
				String repNumber = getRepMobileNumber(contactid, contact.getString("RepUserID"));
				
				if(StringUtils.isNotBlank(groupID)) {
					// See if we can find a rep in the specified group
					String groupRepMobile = getGroupRepMobileNumber(contactid, groupID);
					if(StringUtils.isNotBlank(groupRepMobile)) {						
						repNumber = groupRepMobile;
						logger.debug("Overriding mobile number {} ", repNumber);
					}
				}
				
				if(StringUtils.isNotBlank(repNumber)) {
					smsFrom = repNumber.replaceAll("[^0-9]", "");
				}
			}
			
			ve.evaluate( contact, sw, contactid, formbean.getString("Subject"));
			sw.flush();
			sw.close();
			String notesubject = sw.toString();


			//ve.evaluate( contact, textw, contactid, formbean.getString("TextHeader") );
			ve.evaluate( contact, textw, contactid, formbean.getString("Body"));
			//ve.evaluate( contact, textw, contactid, formbean.getString("TextFooter") );
			textw.flush();
			textw.close();
			String textbody = textw.toString();

			transport.clear();
			transport.setNumber(mobile);
			transport.setMessage(textbody);
			transport.setSubject(notesubject);
			transport.setFrom(from);
			transport.setSender(smsFrom);

			if(!debug)
			{
				emailstatus  = transport.send();
			}
			else
			{
				emailstatus = "Testing";
				errors.append(textbody);
				errors.append("\r\n");
			}

			saveNote(contact, newnoteid, textbody, notesubject, emailstatus);
			
			if(SMSProvider.sent.equals(emailstatus))
			{
				return(0);
			}
			else
			{
				appendError(emailstatus, contactid);
				return(1);
			}
		}
		catch(Exception e)
		{
			appendError(e.toString(), contactid);
			return(1);
		}
	}

	public void appendError(String msg, String contactid)
	{
		setLastError(msg);
		errors.append(msg);
		errors.append(" - ");
		errors.append(formbean.getString("URLHome"));
		errors.append("/crm/contactview.jsp?-action=select&ContactID=");
		errors.append(contactid);
		errors.append("\r\n");

	}
	
	void saveNote(TableData contact, String newnoteid, String notebody, String notesubject, String status)
	{
		try
		{
			String userid = user.getUserID();

			notesave.clear();
			notesave.setConnection(getConnection());
			notesave.setColumn("NoteID",newnoteid);
			notesave.setColumn("ContactID",contact.getString("ContactID"));
			notesave.setColumn("CompanyID",contact.getString("CompanyID"));
			notesave.setColumn("Type","SMS");
			notesave.setColumn("RepUserID",contact.getString("RepUserID"));
			notesave.setColumn("Territory",user.getTerritory());
			notesave.setColumn("ModifiedBy",userid);
			notesave.setColumn("TemplateUsed",formbean.getString("TemplateID"));
			notesave.setColumn("Campaign",formbean.getString("Campaign"));
			notesave.setColumn("CallStatus",status);
			notesave.setColumn("Completed","Yes");
			notesave.setColumn("Subject",notesubject);
			notesave.setColumn("Body",notebody);
			notesave.setColumn("CreatedBy",userid);
			notesave.setColumn("NoteTypeDescription","BulkSMS");
			if(status.equals(SMSProvider.sent))
			{
				notesave.setColumn("SentDate","NOW");
			}
            notesave.setColumn("EmailFrom", formbean.getString("FROM"));
            notesave.setColumn("EmailTo", contact.getString("Mobile"));
			notesave.setColumn("NoteDate","NOW");
			notesave.setColumn("CreatedDate","NOW");
			notesave.setColumn("ModifiedDate","NOW");
			notesave.setColumn("GroupID",contact.getString("GroupID"));
			notesave.doAction();
			if(notesave.getUpdatedCount()==0)
			{
				errors.append("Could not save note, ");
				errors.append(notesave.getError());
				errors.append(" - ");
				errors.append(contact.getString("ContactID"));
				errors.append(".\r\n");
			}
			if(debug)
			{
				if(notesave.getError().length()!=0)
				{
					appendError(notesave.getError(), contact.getString("ContactID"));
				}
			}
		}
		catch (Exception ex)
		{
			appendError("Could not save note, "+ex.toString(), contact.getString("ContactID"));
		}

	}


	Connection getConnection() throws Exception
	{
		/* we will let GenRow handle the connections
		if(con==null || con.isClosed())
		{
			try
			{
				if(ctx == null)
				{
					ctx = new InitialContext();
				}

				DataSource ds = null;
				try{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource)envContext.lookup(dbconn);
				}catch(Exception e){}
				if(ds == null)
				{
					ds = (DataSource)ctx.lookup(dbconn);
				}
				con = ds.getConnection();
			}
			finally
			{
				try{
					ctx.close();
					ctx = null;
				}catch(Exception e){}
			}
		}
		
		return con;
		*/
		return null;
	}

	public void execute()
	{
		con = null;
		ctx = null;
		try
		{

			status = new StringBuffer();

			if(user == null || contactsearch == null)
			{
				status.append("Resources not found, offline process halted.\r\n");
				return;
			}	

		setEmailHeader();

			if(requestbean.getString("-debug").length()!=0)
			{
				debug = true;
				status.append("Debug mode on.\r\n");
			}

			//RowBean requestbean = new RowBean();
			requestbean.setViewSpec(SpecManager.getViewSpec("TemplateView"));
			//requestbean.parseRequest(request);
			requestbean.put("URLHome",context.getInitParameter("URLHome"));

			if(requestbean.getString("TemplateID").length()!=0)
			{
				formbean = new RowBean();
				formbean.setViewSpec(SpecManager.getViewSpec("TemplateView"));

				formbean.put("contextPath",contextpath);
				formbean.setConnection(getConnection());
				formbean.put("TemplateID",requestbean.getString("TemplateID"));
				formbean.setAction("select");
				formbean.doAction();
				formbean.put("URLHome",context.getInitParameter("URLHome"));
				formbean.put("contextPath",contextpath);
				formbean.putAll(requestbean);
				if(formbean.getString("TemplateName").length()==0)
				{
					status.append("Template not found, offline process halted.\r\n");
					status.append(formbean.getError());
					return;
				}
			}
			else
			{
				formbean = requestbean;
			}

			getConnection();

			if(formbean.getString("-checkcount").length()!=0)
			{
				checkcount = Integer.parseInt(formbean.getString("-checkcount"));
			}

			contactselect = new GenRow();
			contactselect.setConnection(getConnection());
			contactselect.setLocale(thislocale);
			contactselect.setViewSpec(SpecManager.getViewSpec("ContactEmailView"));
			contactselect.setAction(ActionBean.SELECT);
		
			notesave = new TableBean();
			notesave.setConnection(getConnection());
			notesave.setTableSpec("Notes");
			notesave.setAction(ActionBean.INSERT);

			contacts = new ArrayList<String>();
			retrieveContacts();

			if(checkcount!=-1)
			{
				if(total!=checkcount)
				{
					status.append("Search results count does not match, offline process halted.\r\n");
					status.append("Current - ");
					status.append(String.valueOf(total));
					status.append(", Original - ");
					status.append(String.valueOf(checkcount));
					status.append("\r\n");
					return;	
				}
			}

			sendMessages();
			status.append(String.valueOf(total));
			status.append(" contacts found.\r\n");
			status.append(String.valueOf(sent));
			status.append(" sms sent.\r\n");
			status.append(String.valueOf(failed));
			status.append(" sms failed.\r\n");

			if (isKilled()) {
				status.append("Offline process terminated by - ");
				status.append(getKiller());
				status.append("\r\n\r\n");
			}
			else {	   
				status.append("Offline process completed.\r\n\r\n");
			}

		}
		catch(Exception e)
		{
			status.append("Offline process halted on ");
			status.append(String.valueOf(sent+failed));
			status.append(" of ");
			status.append(String.valueOf(total));
			status.append(".\r\n\r\n");
			status.append(ActionBean.writeStackTraceToString(e));

		}
		finally
		{
			if (con != null) 
			{
				try{
				con.close();
					con = null;
				}catch (Exception ex){}
			}
			if (ctx != null) 
			{
				try{       
				ctx.close();
					ctx = null;
				}catch (Exception ex){}
			}

			if(errors!=null && errors.length()!=0)
			{
				status.append("\r\n");
				status.append(errors.toString());
				System.out.println("SMS Errors " + status.toString());
			}
		}
	}



	void setEmailHeader()
	{
		status.append(context.getServletContextName());
		status.append(" Runway offline process - Send SMS\r\n");
		status.append("Initiated ");
		try
		{
			DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
			status.append(dt.format(new java.util.Date()));
		}
		catch(Exception e)
		{
			status.append(new java.util.Date().toString());
		}
		status.append("\r\n");
	}

	public String getStatusMailBody()
	{
		return(status.toString());
	}

	public String getStatusMailSubject()
	{
		return(context.getServletContextName() + " Runway offline process - Send SMS");
	}

	public String getStatusMailRecipient()
	{
		return(user.getEmail());
	}
	
	public int getProcessSize() {
      return total;
   }
   
   public int getProcessedCount() {
      return sent+failed;
   }
   
   public int getErrorCount() {
      return failed;
   }
   
	private String getRepMobileNumber(String contactID, String repUserID) throws Exception {
		String mobile = "";
		GenRow groups = new GenRow();
		groups.setConnection(getConnection());
		groups.setTableSpec("ContactGroups");
		groups.setParameter("-select", "*");
		groups.setParameter("ContactID", contactID);
		groups.setParameter("RepUserID", repUserID);
		groups.sortBy("SortOrder", 0);
		groups.sortOrder("ASC", 0);
		groups.doAction("selectfirst");

		if (!groups.isSuccessful()) {
			groups.remove("RepUserID");
			groups.doAction("selectfirst");
		}

		if (groups.isSuccessful()) {
			if(StringUtils.isNotBlank(groups.getString("RepUserID"))){
				GenRow user = getUser(groups.getString("RepUserID"));
				mobile = user.getString("Mobile");
			}
		}		
		  
		return mobile;
	}
	
	private String getGroupRepMobileNumber(String contactID, String groupID) throws Exception {
		String mobile = "";
		GenRow groups = new GenRow();
		groups.setConnection(getConnection());
		groups.setViewSpec("ContactGroupView");		
		groups.setParameter("GroupID", groupID);
		groups.setParameter("ContactID", contactID);
		groups.sortBy("SortOrder", 0);
		groups.sortOrder("ASC", 0);
		groups.doAction("selectfirst");

		logger.debug("groups stattement" , groups.getStatement());
		
		// lets see if we can find a matching group and contactID
		if (groups.isSuccessful()) {
			mobile = groups.getString("RepMobile");
			logger.debug("mobile: " , mobile);
		}
		return mobile;
	}
	
	private GenRow getContactGroupRep(String groupID, String contactID, String repUserID) throws Exception {
		GenRow groups = new GenRow();
		groups.setConnection(getConnection());
		groups.setTableSpec("ContactGroups");
		groups.setParameter("-select", "*");
		groups.setParameter("GroupID", groupID);
		groups.setParameter("ContactID", contactID);
		groups.setParameter("RepUserID", repUserID);
		groups.sortBy("SortOrder", 0);
		groups.sortOrder("ASC", 0);
		groups.doAction("selectfirst");

		// lets see if we can find a matching group and contactID
		if (!groups.isSuccessful()) {
			groups.remove("RepUserID");
			groups.doAction("selectfirst");
		}
		// ok now try for a contact and rep, but any group
		if (!groups.isSuccessful()) {
			groups.remove("GroupID");
			groups.setParameter("RepUserID", repUserID);
			groups.doAction("selectfirst");
		}
		// ok then just a group for the contact
		if (!groups.isSuccessful()) {
			groups.remove("RepUserID");
			groups.doAction("selectfirst");
		}
		if (groups.isSuccessful()) {
			return getUser(groups.getData("RepUserID"));
		}

		return new GenRow();
	}
	private GenRow getUser(String userID) throws Exception {
		GenRow row = new GenRow();
		row.setTableSpec("Users");
		row.setConnection(getConnection());
		row.setParameter("UserID", userID);
		row.setParameter("-select0","*");
		row.doAction(GenerationKeys.SELECT);
		return row;
	}
}
