package com.sok.runway;

import java.sql.Connection;
import java.util.Hashtable;
import java.util.Iterator;

import com.sok.framework.ActionBean;
import com.sok.runway.security.User;
import com.sok.runway.security.interfaces.RunwayUser;


public class LockManager
{
	private static Hashtable<String, RunwayUser> locks = new Hashtable<String, RunwayUser>();

	public static synchronized UserBean lock(String id, UserBean user)
	{
		RunwayUser runwayEnity = locks.get(id);
		if(runwayEnity==null)
		{
			locks.put(id,user);
		}
		else if(!runwayEnity.getUserID().equals(user.getUserID()))
		{
			if(runwayEnity instanceof UserBean){
				return((UserBean)runwayEnity);
			}else if(runwayEnity instanceof User){
				return(getUserBeanObject((User)runwayEnity));
			}
		}
		return(null);
	}
	
	/*
	 *  Created By : Puja Shah
	 *  Created Date : 31st July 2008
	 *  Purpose : Overloaded method of lock which takes the parameter User instead of UserBean
	*/
	public static synchronized User lock(String id, User user)
	{
		RunwayUser runwayUser = locks.get(id);
		if(runwayUser==null)
		{
			locks.put(id,user);
		}
		else if(!runwayUser.getUserID().equals(user.getUserID()))
		{
			if(runwayUser instanceof User){
				return((User)runwayUser);
			}else if(runwayUser instanceof UserBean){
				return(getUserObject((UserBean)runwayUser));
			}
		}
		return(null);
	}

	public static synchronized UserBean isLocked(String id, UserBean user)
	{
		RunwayUser runwayUser = locks.get(id);
		if(runwayUser!=null && !runwayUser.getUserID().equals(user.getUserID()))
		{
			if(runwayUser instanceof UserBean){
				return((UserBean)runwayUser);
			}else if(runwayUser instanceof User){
				return(getUserBeanObject((User)runwayUser));
			}
		}
		return(null);
	}

	public static synchronized void unlockAll(RunwayUser user) {
	   unlockAll(user, null);
	}
	
	public static synchronized void unlockAll(RunwayUser user, String tableName)
	{
		Iterator it = locks.keySet().iterator();
		String tempkey = null;
		RunwayUser ub = null;
		while(it.hasNext())
		{
			tempkey = (String)it.next();
			ub = locks.get(tempkey);
			if(ub.getUserID().equals(user.getUserID()))
			{
			   if (tableName == null || tempkey.startsWith(tableName + ActionBean._dot)) {
   				locks.remove(tempkey);
   				it = locks.keySet().iterator();
			   }
			}
		}
	}

	public static synchronized void getAllLocks(javax.servlet.jsp.JspWriter out) throws java.io.IOException
	{
		Iterator it = locks.keySet().iterator();
		String tempkey = null;
		RunwayUser ub = null;
		StringBuffer output; 
		while(it.hasNext())
		{
			tempkey = (String)it.next();
			
			ub = locks.get(tempkey);
			
			output = new StringBuffer(); 
			output.append("Record "); 
			output.append(tempkey); 
			output.append(" is locked by "); 
			//output.append(ub.getString("Username")); 
			output.append(ub.getUserName());
			output.append("<br>"); 
			
			out.print(output.toString()); 
		}
	}
	
	public static synchronized void getAllLocks() throws java.io.IOException
	{
		Iterator it = locks.keySet().iterator();
		String tempkey = null;
		RunwayUser ub = null;
		StringBuffer output; 
		while(it.hasNext())
		{
			tempkey = (String)it.next();
			
			ub = locks.get(tempkey);
			
			output = new StringBuffer(); 
			output.append("Record "); 
			output.append(tempkey); 
			output.append(" is locked by "); 
			//output.append(ub.getString("Username")); 
			output.append(ub.getUserName());
			output.append("\n"); 
			
			System.out.print(output.toString()); 
		}
	}
	public static synchronized void unlock(String id, RunwayUser user)
	{
		RunwayUser ub = locks.get(id);
		if(ub!=null)
		{
			if(ub.getUserID().equals(user.getUserID()))
			{
				locks.remove(id);
			}
		}
	}
	
	public static synchronized void clearAll()
	{
		locks.clear();
	}
	
	private static User getUserObject(UserBean ub){
		User user = new User((Connection)null);
		user.setField("UserID",ub.getColumn("UserID"));
		user.setField("FirstName",ub.getColumn("FirstName"));
		user.setField("LastName",ub.getColumn("LastName"));
		return user;
	}
	
	private static UserBean getUserBeanObject(User user){
		UserBean ub = new UserBean();
		ub.setColumn("UserID",user.getField("UserID"));
		ub.setColumn("FirstName",user.getField("FirstName"));
		ub.setColumn("LastName",user.getField("LastName"));
		return ub;
	}

}
	
