package com.sok.runway.sms;

import com.sok.framework.StringUtil;
import com.sok.runway.SMSBean;
import java.io.*;
import java.net.*;

public class MessageMediaSMS implements SMSProvider
{

    SMSBean bean;
    OutputStreamWriter wr;
    BufferedReader rd;
    URLConnection conn;
    int status;
    String response;
    int port;
    String address;
    String account;
    String password;

    public MessageMediaSMS(SMSBean smsbean)
    {
        bean = null;
        wr = null;
        rd = null;
        conn = null;
        status = 0;
        response = null;
        port = 80;
        address = "http://smsmaster.m4u.com.au:8080/api/send";
        account = "Australianpro009";
        password = "apa009";
        bean = smsbean;
    }

    public String connect()
	{
		if(bean.getIP()!=null)
		{
			address = bean.getIP();
		}	
		
		try
		{
			URL url = new URL(address);
			conn = url.openConnection();
		}
		catch(Exception ex)
		{
         if(wr!=null)
         {
            try{
               wr.close();
               wr = null;
            }catch(Exception e){}
         }
         if(rd!=null)
         {
            try{
               rd.close();
               rd = null;
            }catch(Exception e){}
         }     
			return(ex.toString());
		}
		return(null);
	}

    public void close()
    {
        if(wr != null)
        {
            try
            {
                wr.close();
                wr = null;
            }
            catch(Exception e) { }
        }
        if(rd != null)
        {
            try
            {
                rd.close();
                rd = null;
            }
            catch(Exception e) { }
        }
        conn = null;
    }

    String encodeMessage()
    {
        if(bean.getAccount() != null)
        {
            account = bean.getAccount();
        }
        if(bean.getPassword() != null)
        {
            password = bean.getPassword();
        }
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append("username=");
        stringbuffer.append(account);
        stringbuffer.append("&password=");
        stringbuffer.append(password);
        stringbuffer.append("&phone=");
        stringbuffer.append(StringUtil.urlEncode(bean.getNumber()));
        stringbuffer.append("&message=");
        stringbuffer.append(StringUtil.urlEncode(bean.getMessage()));
        return stringbuffer.toString();
    }

    public String send()
    {
        try
        {
           if (conn == null) connect();
           
            HttpURLConnection httpurlconnection = null;
            if(conn instanceof HttpURLConnection)
            {
                httpurlconnection = (HttpURLConnection)conn;
            }
            conn.setDoOutput(true);
            wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(encodeMessage());
            wr.flush();

            if(httpurlconnection != null)
            {
                status = httpurlconnection.getResponseCode();
                response = httpurlconnection.getResponseMessage();
            }
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer stringbuffer = new StringBuffer();
            String s1;
            while((s1 = rd.readLine()) != null) 
            {
                stringbuffer.append(s1);
                stringbuffer.append("\r\n");
            }
            response = stringbuffer.toString();
            
            close();
        }
        catch(Exception exception)
        {
           close();
            return exception.getMessage();
        }
        String s = null;
        if(response.indexOf("OK") >= 0)
        {
            s = "Sent";
        } else
        {
            s = response;
        }
        return s;
    }
}
