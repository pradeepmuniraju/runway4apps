package com.sok.runway.sms;

public interface SMSProvider
{
	static final String sent = "Sent";
	static final String error = "Unspecified Error";		
	
	public String connect();
	
	public void close();
	
	public String send();
}