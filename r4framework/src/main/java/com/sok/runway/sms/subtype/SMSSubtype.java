package com.sok.runway.sms.subtype;

import com.sok.framework.TableData;
import com.sok.runway.sms.SMSSubtypeHandler;

public abstract interface SMSSubtype
{
   public void setNext(SMSSubtype subtype);
   public SMSSubtype getNext();
   public boolean isSMSSubtype(int type);
   
   public void init(SMSSubtypeHandler sms); 
   public void loadSharedTokens(SMSSubtypeHandler sms);
   /**
    * @deprecated
    */
   public void loadSpecificTokens(SMSSubtypeHandler sms);
   public void loadSpecificTokens(SMSSubtypeHandler sms, String id);
   
   public boolean overridesSMSGeneration();
   public void generateSMS(SMSSubtypeHandler sms);
   
   public void setNoteParameters(TableData notebean);
}