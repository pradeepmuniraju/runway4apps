package com.sok.runway.sms.subtype;

import com.sok.runway.email.subtype.*;
import javax.servlet.ServletContext;
import com.sok.framework.TableData;
import com.sok.runway.SMSBean;
import com.sok.runway.email.*;
import com.sok.runway.sms.SMSSubtypeHandler;

public abstract class AbstractSMSSubtype implements SMSSubtype
{
   protected SMSSubtype next;
   protected SMSBean smsBean = null;
   protected TableData sharedcontext = null;
   protected TableData requestrow = null;
   protected ServletContext context = null;
   
   public void setNext(SMSSubtype subtype)
   {
      next = subtype;
   }
   
   public SMSSubtype getNext()
   {
      return(next);
   }
   
   public abstract boolean isSMSSubtype(int type);   
   
   public void init(SMSSubtypeHandler sms)
   {
      requestrow = sms.getRequestRow();
      smsBean = sms.getSMSBean();
      sharedcontext = sms.getSharedContext();
      context = sms.getServletContext();
   }
   
   public abstract void loadSharedTokens(SMSSubtypeHandler sms);
   /**
    * @deprecated
    */
   public abstract void loadSpecificTokens(SMSSubtypeHandler sms);  
   
   public abstract void loadSpecificTokens(SMSSubtypeHandler sms, String id);  
   
   public void generateSMS(SMSSubtypeHandler sms)
   {
      com.sok.Debugger.getDebugger().debug("AbstractSMSSubType Generate SMS");
   }   
   
   public boolean overridesSMSGeneration()
   {
      return(false);
   }
   
   public abstract void setNoteParameters(TableData notebean);
   
}