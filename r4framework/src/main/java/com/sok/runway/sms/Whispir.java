package com.sok.runway.sms;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.Yytoken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.GenRow;
import com.sok.runway.SMSBean;

public class Whispir implements SMSProvider {

	private static final Logger logger = LoggerFactory.getLogger(Whispir.class);

	SMSBean bean = null;
	PostMethod postMethod = null;
	HttpClient client = null;

	String response = null;

	String address = "https://api.whispir.com/messages?apikey=";
	String account = "";
	String password = "";

	SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public Whispir(SMSBean bean) {
		this.bean = bean;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sok.runway.sms.SMSProvider#connect()
	 * 
	 * SET UP: SMSID: APIKey SMSProvider: Whispir SMSPassword: Authorization Header
	 * Places example: SMSID: 8ret8t5cw4efzench32xexcb, SMSPassword: cGxhY2VzdmljYXBpOnBsYWNlc3ZpYzMwMDg=
	 */
	public String connect() {

		if (bean.getAccount() != null) {
			account = bean.getAccount();
		}
		if (bean.getPassword() != null) {
			password = bean.getPassword();
		}

		String url = address + account;

		try {
			postMethod = new PostMethod(url);
			postMethod.addRequestHeader("authorization", "Basic " + password);
			postMethod.addRequestHeader("content-type", "application/vnd.whispir.message-v1+json");
			postMethod.addRequestHeader("accept", "application/vnd.whispir.message-v1+json");

			client = new HttpClient();

			logger.debug("Sending sms via :" + url);
			logger.debug("sms authorization :" + password);

		} catch (Exception e) {

			return (e.toString());
		} finally {
		}
		return (null);
	}

	public void close() {

		try {
			if (postMethod != null) {
				postMethod.releaseConnection();
			}
			postMethod = null;
		} catch (Exception e) {
		}

		postMethod = null;
	}

	@SuppressWarnings("unchecked")
	public String send() {
		try {
			while (!checkCanSend()) Thread.sleep(100);
			if (postMethod == null || client == null)
				connect();

			JSONObject requestBody = new JSONObject();
			requestBody.put("to", bean.getNumber());
			requestBody.put("subject", StringUtils.isBlank(bean.getSubject()) ? " " : bean.getSubject());
			requestBody.put("body", bean.getMessage());
			postMethod.setRequestBody(requestBody.toString());

			int status = client.executeMethod(postMethod);
			response = postMethod.getResponseBodyAsString();

			logger.debug("RequestBody:" + requestBody.toString());
			logger.debug("Status:" + status);
			logger.debug("Response:" + response);

			close();
		} catch (Exception e) {
			close();
			e.printStackTrace();
			return (e.getMessage());
		}

		return (response);
	}
	
	private boolean checkCanSend() {
		Date d = new Date();
		d.setTime(d.getTime() - 1000);
		
		GenRow row = new GenRow();
		row.setTableSpec("Notes");
		row.setParameter("-select0", "COUNT(*) AS Count");
		row.setParameter("Type","SMS");
		row.setParameter("CreatedDate", ">" + ddmmyyyy.format(d));
		row.doAction("selectfirst");
		
		// 7 per second
		if (!(row.getInt("Count") < 7)) return false;
		
		d.setTime(d.getTime() - (24 * 60 * 60 * 1000));
		
		row.setParameter("CreatedDate", ">" + ddmmyyyy.format(d));
		row.doAction("selectfirst");

		// 5000 per day
		return row.getInt("Count") < 5000;
	}
}