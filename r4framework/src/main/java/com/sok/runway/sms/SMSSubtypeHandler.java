package com.sok.runway.sms;

import javax.servlet.*;
import javax.servlet.http.*;
import com.sok.framework.*;
import com.sok.runway.*;
import java.sql.Connection;
import java.util.*;

public interface SMSSubtypeHandler 
{  
   public Connection getConnection();
   
   public TableData getSharedContext();
   
   public TableData getSpecificContext();
   
   public ServletContext getServletContext();
   
   public Properties getSystemProperties();
   
   public String generateText(String template);
   
   public String getRequestString(String name);
   
   public void generateSMS(String subjecttemplate, String texttemplate); 
   
   public SMSBean getSMSBean();
   
   public TableData getRequestRow();

   public TableData getBean(String viewspec, String idname, String idvalue);
   
   public TableData getSetupBean(String userID, String setupID);
      
   public void loadProfile(GenRow genrow, String viewspec, String keyname, String keyvalue, TableData context);
   
   public void loadProfile(String viewspec, String keyname, String keyvalue, TableData context);

   public void setTemplate(GenRow template);
}