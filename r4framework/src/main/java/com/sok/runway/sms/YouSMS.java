package com.sok.runway.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import com.sok.runway.*;
import com.sok.framework.*;

public class YouSMS implements SMSProvider
{
	SMSBean bean = null;	
	OutputStreamWriter wr = null;
	BufferedReader rd = null;
	URLConnection conn = null;

	int status = 0;
	String response = null;
	
	int port = 80;
	String address = "http://services.yousms.com.au/MessageCreator.asp";
	String account = "switchedon";
	String password = "%20somtysa";	
	
	public YouSMS(SMSBean bean)
	{
		this.bean = bean;
	}	
	
	public String connect()
	{
		if(bean.getIP()!=null)
		{
			address = bean.getIP();
		}	
		
		try
		{
			URL url = new URL(address);
			conn = url.openConnection();
		}
		catch(Exception e)
		{
			return(e.toString());
		}
		finally
		{
			if(wr!=null)
			{
				try{
					wr.close();
					wr = null;
				}catch(Exception e){}
			}
			if(rd!=null)
			{
				try{
					rd.close();
					rd = null;
				}catch(Exception e){}
			}			
		}
		return(null);
	}

	
	public void close()
	{
		if(wr!=null)
		{
			try{
				wr.close();
				wr = null;
			}catch(Exception e){}
		}
		if(rd!=null)
		{
			try{
				rd.close();
				rd = null;
			}catch(Exception e){}
		}	
	}
	
	String encodeMessage()
	{
		if(bean.getAccount()!=null)
		{
			account = bean.getAccount();
		}
		if(bean.getPassword()!=null)
		{
			password = bean.getPassword();
		}		
		
		StringBuffer post = new StringBuffer();
		post.append("UserName=");
		post.append(account);
		post.append("&Password=");
		post.append(password);
		post.append("&MessageType=0");
		post.append("&MSISDNList=");
		post.append(StringUtil.urlEncode(bean.getNumber()));
		post.append("&ValidityTime=2");
		post.append("&ValiditySpan=h");
		post.append("&MsgText=");
		post.append(StringUtil.urlEncode(bean.getMessage()));
		post.append("&Version=1");
		post.append("&Subject=");
		post.append("SMS");
		post.append("&Class=2");
		post.append("&MobileNotification=no");
		post.append("&AckType=Order");
		post.append("&Send=yes");
		//System.out.println(post);
		
		return(post.toString());
	}
	
	public String send()
	{
		try
		{		
			HttpURLConnection httpconn = null;
			if(conn instanceof HttpURLConnection)
			{
				httpconn = (HttpURLConnection)conn;
			}
			

			conn.setDoOutput(true);
			wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(encodeMessage());
			wr.flush();				
			
			conn.connect();
			
			if(httpconn!=null)
			{
				status = httpconn.getResponseCode();
				response = httpconn.getResponseMessage();
			}
			
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			StringBuffer responseBuffer = new StringBuffer();
			while((line = rd.readLine()) != null)
			{
				responseBuffer.append(line);
				responseBuffer.append("\r\n");
			}
			response = responseBuffer.toString();
			//System.out.println(responseBuffer);
		}
		catch(Exception e)
		{
			return(e.getMessage());
		}
		
		String returnstr = null;
		if(response.indexOf("SUCCESS")>0)
		{
			returnstr = sent;
		}
		else
		{
			returnstr = response;
		}
		return(returnstr);		
	}

}