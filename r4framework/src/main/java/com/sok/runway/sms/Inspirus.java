package com.sok.runway.sms;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;

import com.sok.framework.StringUtil;
import com.sok.runway.*;

public class Inspirus implements SMSProvider
{
   SMSBean bean = null; 
   OutputStreamWriter wr = null;
   BufferedReader rd = null;
   URLConnection conn = null;

   int status = 0;
   String response = null;
   
   int port = 443;
   String address = "http://203.63.5.170:9098/inspirusWS/smspost.aspx";
   String account = "";
   String password = "";  
   
   public Inspirus(SMSBean bean)
   {
      this.bean = bean;
   }  
   
   public String connect()
   {
      if(bean.getIP()!=null)
      {
         address = bean.getIP();
      }  
      
      try
      {
         URL url = new URL(address + "?" + encodeMessage());
         conn = url.openConnection();
      }
      catch(Exception e)
      {
          if(wr!=null)
          {
             try{
                wr.close();
                wr = null;
             }catch(Exception e1){}
          }
          if(rd!=null)
          {
             try{
                rd.close();
                rd = null;
             }catch(Exception e1){}
          }        
         return(e.toString());
      }
      finally
      {
      }
      return(null);
   }

   
   public void close()
   {
      if(wr!=null)
      {
         try{
            wr.close();
            wr = null;
         }catch(Exception e){}
      }
      if(rd!=null)
      {
         try{
            rd.close();
            rd = null;
         }catch(Exception e){}
      }
      
      conn = null;
   }
   
   String encodeMessage()
   {
      if(bean.getAccount()!=null)
      {
         account = bean.getAccount();
      }
      if(bean.getPassword()!=null)
      {
         password = bean.getPassword();
      }     
      
      StringBuffer post = new StringBuffer();
      post.append("user=");
      post.append(account);
      post.append("&pass=");
      post.append(password);
      post.append("&from=");
      if(bean.getSender()!=null)
      {
        String sender = bean.getSender();
        if (sender.length() > 11) sender = sender.substring(0,11);
            post.append(StringUtil.urlEncode(sender));
      }
      post.append("&to=");
      post.append(StringUtil.urlEncode(bean.getNumber()));      
      post.append("&text=");
      post.append(StringUtil.urlEncode(bean.getMessage()));

      //System.out.println(post);
      
      return(post.toString());
   }
   
   public String send()
   {
      try
      {
        if (conn == null) connect();
        
         HttpURLConnection httpconn = null;
         if(conn instanceof HttpURLConnection)
         {
            httpconn = (HttpURLConnection)conn;
         }      
         
         if(httpconn!=null)
         {
            status = httpconn.getResponseCode();
            response = httpconn.getResponseMessage();
         }
         
         if (rd == null) {
          rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
         }
         String line;
         StringBuffer responseBuffer = new StringBuffer();
         while((line = rd.readLine()) != null)
         {
            responseBuffer.append(line);
            responseBuffer.append("\r\n");
         }
         response = responseBuffer.toString();
         
         close();
      }
      catch(Exception e)
      {
        close();
       e.printStackTrace();
         return(e.getMessage());
      }
      
      String returnstr = null;
      if(response.indexOf("OK")>=0)
      {
         returnstr = sent;
      }
      else
      {
         returnstr = response;
      }
      return(returnstr);      
   }
}

