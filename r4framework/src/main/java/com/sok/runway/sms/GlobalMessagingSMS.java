package com.sok.runway.sms;

import com.sok.framework.StringUtil;
import com.sok.runway.SMSBean;
import java.io.*;
import java.net.*;

public class GlobalMessagingSMS implements SMSProvider
{

    SMSBean bean;
    OutputStreamWriter wr;
    BufferedReader rd;
    URLConnection conn;
    int status;
    String response;
    int port;
    String address;
    String account;
    String password;

    public GlobalMessagingSMS(SMSBean smsbean)
    {
        bean = null;
        wr = null;
        rd = null;
        conn = null;
        status = 0;
        response = null;
        port = 80;
        address = "http://www.globalmessaging.co.uk/";
        account = "vcthale";
        password = "password";
        bean = smsbean;
    }

    public String connect()
	{
		if(bean.getIP()!=null)
		{
			address = bean.getIP();
		}	
		
		try
		{
			URL url = new URL(address);
			conn = url.openConnection();
		}
		catch(Exception e)
		{
			return(e.toString());
		}
		finally
		{
			if(wr!=null)
			{
				try{
					wr.close();
					wr = null;
				}catch(Exception e){}
			}
			if(rd!=null)
			{
				try{
					rd.close();
					rd = null;
				}catch(Exception e){}
			}			
		}
		return(null);
	}

    public void close()
    {
        if(wr != null)
        {
            try
            {
                wr.close();
                wr = null;
            }
            catch(Exception e) { }
        }
        if(rd != null)
        {
            try
            {
                rd.close();
                rd = null;
            }
            catch(Exception e) { }
        }
    }

    String encodeMessage()
    {
        if(bean.getAccount() != null)
        {
            account = bean.getAccount();
        }
        if(bean.getPassword() != null)
        {
            password = bean.getPassword();
        }
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append("a=");
        stringbuffer.append("api_multi_submit");
        stringbuffer.append("&login=");
        stringbuffer.append(account);
        stringbuffer.append("&password=");
        stringbuffer.append(password);
        stringbuffer.append("&dest=");
        stringbuffer.append(StringUtil.urlEncode(bean.getNumber()));
        stringbuffer.append("&msg=");
        stringbuffer.append(StringUtil.urlEncode(bean.getMessage()));
        stringbuffer.append("&expiry=259200");
        return stringbuffer.toString();
    }

    public String send()
    {
        try
        {
            HttpURLConnection httpurlconnection = null;
            if(conn instanceof HttpURLConnection)
            {
                httpurlconnection = (HttpURLConnection)conn;
            }
            conn.setDoOutput(true);
            wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(encodeMessage());
            wr.flush();
            conn.connect();
            if(httpurlconnection != null)
            {
                status = httpurlconnection.getResponseCode();
                response = httpurlconnection.getResponseMessage();
            }
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer stringbuffer = new StringBuffer();
            String s1;
            while((s1 = rd.readLine()) != null) 
            {
                stringbuffer.append(s1);
                stringbuffer.append("\r\n");
            }
            response = stringbuffer.toString();
        }
        catch(Exception exception)
        {
            return exception.getMessage();
        }
        String s = null;
        if(response.indexOf("Ok") > 0)
        {
            s = "Sent";
        } else
        {
            s = response;
        }
        return s;
    }
}
