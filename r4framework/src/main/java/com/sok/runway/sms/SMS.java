/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sok.runway.sms;

import com.sok.runway.UserBean;
import javax.servlet.ServletContext;
import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.framework.generation.nodes.Parameter;
import com.sok.runway.SMSBean;
import com.sok.runway.SessionKeys;
import com.sok.runway.email.Emailer;
import com.sok.runway.email.EmailerException;
import com.sok.runway.email.subtype.*;
import com.sok.runway.sms.subtype.AppointmentSMS;
import com.sok.runway.sms.subtype.SMSSubtype;
import java.io.File;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;

public class SMS implements SMSSubtypeHandler {

	public static final String CONTACTID = "ContactID";
	public static final String USERID = "UserID";
	public static final String APPOINTMENTID = "AppointmentID";
	public static final int AppointmentSMS = 1;
	public static final String SENT = "Sent";
	public static final String OPTOUT = "Opt-out or left company";
	public static final String TOEMPTY = "Empty sms address";
	public static final String FROMEMPTY = "Empty from sms address";
	protected TableData template = null;
	protected TableData requestrow = null;
	protected TableData sharedcontext = null;
	protected TableData specificcontext = null;
	protected ServletContext context = null;
	protected SMSBean smsBean = null;
	protected UserBean currentUser = null;
	protected SMSSubtype subtypehead = null;
	protected boolean offlinemode = false;
	protected boolean previewmode = false;
	Properties sysprops = null;
	VelocityEngine ve = null;
	protected Connection con = null;
	protected boolean closeconnection = true;

	private boolean toContact = true;

	public SMS(GenRow requestrow, ServletContext context) {

		sharedcontext = new GenRow();
		smsBean = new SMSBean();

		if (requestrow.getString(APPOINTMENTID).length() > 0) {
			addSubtype(new AppointmentSMS());
		}

		init(requestrow, context);



	}

	public void toUser() {
		toContact = false;
	}

	public void toContact() {
		toContact = true;
	}

	public void init(GenRow requestrow, ServletContext context) {
		setRequest(requestrow);
		this.requestrow = requestrow;

		this.context = context;

		init();
		SMSSubtype subtype = subtypehead;
		while (subtype != null) {
			subtype.init(this);
			subtype = subtype.getNext();
		}

		if (requestrow.getString("SMSTemplateID").length() != 0) {
			// requires a URLHome which is set after here loadSharedTokens()
			sharedcontext.put("URLHome", context.getInitParameter("URLHome"));
			loadTemplate();
		}
		if (hasTemplate()) {
			loadSharedTokens();
		}
	}

	protected void init() {
		setNoteDefaults();

		sharedcontext.putAll(requestrow);

		this.sysprops = InitServlet.getSystemParams();

		setConnection(requestrow.getConnection());
		closeconnection = requestrow.getCloseConnection();

		try {
			ve = VelocityManager.getEngine();
		} catch (Exception e) {
			throw new EmailerException(e);
		}
	}

	protected void initPreview() {
		previewmode = true;
		if (offlinemode) {
			specificcontext = new GenRow();
			mergeContexts();
			generateSMS();
		} else {
			generate();
		}
	}

	public String getTextPreview() {
		initPreview();
		return (smsBean.getMessage());
	}

	public void addSubtype(SMSSubtype subtype) {


		if (subtypehead == null) {
			subtypehead = subtype;
		} else {         
			subtype.setNext(subtypehead);
			subtypehead = subtype;
		}
	}

	public SMSSubtype getSubtype(int type) {
		SMSSubtype subtype = subtypehead;
		while (subtype != null) {
			if (subtype.isSMSSubtype(type)) {
				return (subtype);
			}
			subtype = subtype.getNext();
		}
		return (null);
	}

	public boolean hasSMSSubType(int type) {
		SMSSubtype subtype = subtypehead;
		while (subtype != null) {
			if (subtype.isSMSSubtype(type)) {
				return (true);
			}
			subtype = subtype.getNext();
		}
		return (false);
	}

	protected SMSSubtype getOverrideSubtype() {
		SMSSubtype subtype = subtypehead;

		while (subtype != null) {
			if (subtype.overridesSMSGeneration()) {
				return (subtype);
			}
			subtype = subtype.getNext();
		}
		return (null);
	}

	public boolean hasTemplate() {
		return (template != null);
	}

	protected void loadTemplate() {
		if (template == null && requestrow.getString("SMSTemplateID").length() != 0) {
			template = getBean("TemplateView", "TemplateID", requestrow.getString("SMSTemplateID"));

			String mastertemplateid = requestrow.getString("MasterTemplateID");
			if (mastertemplateid.length() == 0) {
				mastertemplateid = template.getString("MasterTemplateID");
				requestrow.put("MasterTemplateID", mastertemplateid);
			}

			if (mastertemplateid.length() > 0) {
				template.putAll(getBean("MasterTemplateView", "MasterTemplateID", mastertemplateid));
			}

			setTemplateDefaults();
		} else {
			throw new EmailerException("No email template specified");
		}
	}

	protected void setTemplateDefaults() {

		if (requestrow.getString("TO").length() == 0) {
			requestrow.setParameter("TO", template.getString("EmailTo"));
		}
		if (requestrow.getString("FROM").length() == 0) {
			requestrow.setParameter("FROM", template.getString("EmailFrom"));
		}
		if (requestrow.getString("CC").length() == 0) {
			requestrow.setParameter("CC", template.getString("EmailCC"));
		}
		if (requestrow.getString("REPLY_TO").length() == 0) {
			requestrow.setParameter("REPLY_TO", template.getString("EmailReplyTo"));
		}
	}

	protected boolean hasOptOut() {
		GenRow temp = getOptOutChecker();
		temp.setColumn(CONTACTID, specificcontext.getString(CONTACTID));
		temp.doAction(GenerationKeys.SELECTFIRST);

		return (temp.isSuccessful());
	}

	public boolean isOptedOut() {
		if (specificcontext != null) {
			if (isSuperOptedOut()) {
				return (true);
			}
			if (specificcontext.getString("CurrentStatusID").length() != 0) {
				return (specificcontext.getString("CurrentStatus").equals("Left Company") || specificcontext.getString("CurrentStatusID").equals(sysprops.getProperty("ContactLeftCompanyStatusID")));
			}
		}
		return (false);
	}

	public boolean isSuperOptedOut() {
		if (specificcontext != null) {
			if (hasOptOut()) {
				return (true);
			} else if (specificcontext.getString("CurrentStatusID").length() != 0) {
				return (specificcontext.getString("CurrentStatus").equals("OptOut") || specificcontext.getString("CurrentStatusID").equals(sysprops.getProperty("ContactOptOutStatusID")));
			}
		}
		return (false);
	}

	protected boolean hasGlobalOptOut()
	{
		if(specificcontext.getString("CurrentStatusID").length()!=0 && 
				sysprops.getProperty("ContactLeftCompanyStatusID") != null && 
				sysprops.getProperty("ContactLeftCompanyStatusID").length() > 0 &&
				sysprops.getProperty("ContactOptOutStatusID") != null &&
				sysprops.getProperty("ContactOptOutStatusID").length() > 0) 
		{
			if (specificcontext.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("leftcompany")
					|| specificcontext.getString("CurrentStatusID").equals(sysprops.getProperty("ContactLeftCompanyStatusID"))) {

				return true;
			} else if (specificcontext.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
					|| specificcontext.getString("CurrentStatusID").equals(sysprops.getProperty("ContactOptOutStatusID"))) {
				//System.out.println("AbstractEmailer:isOptedOut(" + specificcontext.getString("ContactID") + ")");
				return true;
			}
		}
		return(false);
	}

	private boolean hasSingleGroupOptOut(String groupID)
	{
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("email/ContactGroupGroupStatusView");
		row.setParameter("GroupID", groupID);
		row.setParameter(CONTACTID, specificcontext.getString(CONTACTID));
		row.doAction("selectfirst");

		specificcontext.setColumn("GroupID", row.getData("GroupID"));
		specificcontext.setColumn("GroupName", row.getString("Name"));

		if (row.getString("CurrentStatus").length() > 0) {
			return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
					|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
					|| "Y".equals(row.getString("EmailOptOut")));         
		}
		return false;
	}

	protected boolean hasGroupOptOut()
	{
		// if multiple groups has been set, they must all be checked.
		if (!sharedcontext.isSet("CampaignID")) {
			if(requestrow.isSet("GroupIDs")) { 
				String[] groupIDs = requestrow.getParameterValues("GroupIDs");
				for(String gID: groupIDs) 
				{
					if(hasSingleGroupOptOut(gID)) {

						return true;
					}  
				}
				return false;
			}
			// check to see if there is a Group for the GroupID and if it is OptOut
			if(requestrow.isSet("GroupID")) 
			{
				if(hasSingleGroupOptOut(requestrow.getString("GroupID"))) { 
					return true;
				}
				return false;
			}
		}

		// check for a single group and if it is OptOut (bulk emailer will provide the wrong groupID
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("email/ContactGroupGroupStatusView");
		//row.setParameter("GroupID", requestrow.getString("GroupID"));
		row.setParameter(CONTACTID, specificcontext.getString(CONTACTID));
		row.sortBy("SortOrder", 0);
		row.doAction("search");
		row.getResults(true);

		if (row.getSize() == 1) {
			row.getNext();

			if (sharedcontext.isSet("CampaignID")) {
				GenRow groups = new GenRow();
				groups.setConnection(getConnection());
				groups.setTableSpec("LinkedCampaigns");
				groups.setParameter("-select","*");
				groups.setParameter("CampaignID",sharedcontext.getString("CampaignID"));
				groups.setParameter("LinkedGroupID",row.getString("GroupID"));
				groups.doAction("selectfirst");
				if (groups.isSuccessful()) {
					specificcontext.setColumn("GroupID", row.getString("GroupID"));
					specificcontext.setColumn("GroupName", row.getString("Name"));

					row.close();

					return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
							|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
							|| "Y".equals(row.getString("EmailOptOut")));
				}
			} else if (requestrow.isSet("GroupIDs")) {
				String[] groupIDs = requestrow.getParameterValues("GroupIDs");
				for(String gID: groupIDs) 
				{
					if (gID != null && gID.equals(row.getString("GroupID"))) { 
						specificcontext.setColumn("GroupID", row.getData("GroupID"));
						specificcontext.setColumn("GroupName", row.getString("Name"));

						row.close();

						return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
								|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
								|| "Y".equals(row.getString("EmailOptOut")));
					}
				}
				row.close();
				return false;
			} else if (requestrow.isSet("GroupID") && requestrow.getString("GroupID").equals(row.getString("GroupID"))) {
				specificcontext.setColumn("GroupID", row.getData("GroupID"));
				specificcontext.setColumn("GroupName", row.getString("Name"));

				row.close();

				return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
						|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
						|| "Y".equals(row.getString("EmailOptOut")));
			}

			specificcontext.setColumn("GroupID", row.getData("GroupID"));
			specificcontext.setColumn("GroupName", row.getString("Name"));

			row.close();

			return(row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
					|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID")));         
		} else if (row.getSize() > 1) {
			while (row.getNext()) {

				//System.out.println("ContactEmailer: multigroup");
				if (sharedcontext.isSet("CampaignID")) {
					GenRow groups = new GenRow();
					groups.setConnection(getConnection());
					groups.setTableSpec("LinkedCampaigns");
					groups.setParameter("-select","*");
					groups.setParameter("CampaignID",sharedcontext.getString("CampaignID"));
					groups.setParameter("LinkedGroupID",row.getString("GroupID"));
					groups.doAction("selectfirst");
					if (groups.isSuccessful()) {
						specificcontext.setColumn("GroupID", row.getData("GroupID"));
						specificcontext.setColumn("GroupName", row.getString("Name"));

						TableData rep = getRepBean();
						//System.out.println("ContactEmailer rep: "+rep.toString() + ""+ );
						if (rep.isSet("UserID") && rep.isSet("Status") && "Active".equals(rep.getString("Status"))) {
							boolean r = (row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
									|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
									|| "Y".equals(row.getString("EmailOptOut")));

							if (!r) {
								row.close();
								return false;
							}
						}
					}
				} else if (requestrow.isSet("GroupIDs") && !requestrow.isSet("GroupID")) {
					String[] groupIDs = requestrow.getParameterValues("GroupIDs");
					for(String gID: groupIDs) 
					{
						if (gID != null && gID.equals(row.getString("GroupID"))) { 
							specificcontext.setColumn("GroupID", row.getData("GroupID"));
							specificcontext.setColumn("GroupName", row.getString("Name"));

							TableData rep = getRepBean();

							if (rep.isSet("UserID") && rep.isSet("Status") && "Active".equals(rep.getString("Status"))) {
								boolean r = (row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
										|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
										|| "Y".equals(row.getString("EmailOptOut")));

								if (!r) {
									row.close();
									return false;
								}
							}
						}
					}
				} else if (requestrow.isSet("GroupID") && requestrow.getString("GroupID").equals(row.getString("GroupID"))) {
					specificcontext.setColumn("GroupID", row.getData("GroupID"));
					specificcontext.setColumn("GroupName", row.getString("Name"));

					TableData rep = getRepBean();

					if (rep.isSet("UserID") && rep.isSet("Status") && "Active".equals(rep.getString("Status"))) {
						boolean r = (row.getString("CurrentStatus").replaceAll("[^A-Za-z]","").equalsIgnoreCase("optout")
								|| row.getString("CurrentStatusID").equals(sysprops.getProperty("ContactGroupOptOutStatusID"))
								|| "Y".equals(row.getString("EmailOptOut")));

						if (!r) {
							row.close();
							return false;
						}
					}
				}
			}
			row.close();
			specificcontext.setColumn("GroupName", "No Active Sales Rep Found for " + getGroupName(specificcontext.getString("GroupID")));
			return true;
		}

		return(false);
	}

	protected String getGroupName(String groupID) {
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("GroupView");
		row.setParameter("GroupID", groupID);
		row.doAction("selectfirst");

		return row.getString("Name");
	}

	protected boolean isRep()
	{
		if(specificcontext!=null){
			GenRow row = new GenRow();
			row.setConnection(getConnection());
			row.setTableSpec("ContactGroups");
			row.setParameter("-select1", "ContactGroupID");
			row.setParameter(CONTACTID, specificcontext.getString(CONTACTID));
			row.setParameter("RepUserID", currentUser.getUserID());
			row.doAction("selectfirst");

			return(row.isSuccessful());    
		}
		return(true);
	}   

	/**
	 * Checks for appropriate rep/group to use for this specific context 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected TableData getRepBean() {
		if ("Appointment".equals(sharedcontext.getString("Type")) && specificcontext.isSet("GroupID") && sharedcontext.isSet("CurrentUserID")) {
			GenRow groups = new GenRow();
			groups.setConnection(getConnection());
			groups.setTableSpec("ContactGroups");
			groups.setParameter("-select","*");
			groups.setParameter("ContactID",specificcontext.getString("ContactID"));
			groups.setParameter("GroupID",specificcontext.getString("GroupID"));
			groups.setParameter("RepUserID", sharedcontext.getString("CurrentUserID"));
			groups.sortBy("SortOrder", 0);
			groups.doAction("selectfirst");

			if (!groups.isSuccessful() || groups.getString("ContactGroupID").length() == 0) {
				groups.setParameter("RepUserID", sharedcontext.getString("RepUserID"));
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
			}

			String repUserID = groups.getData("RepUserID");
			if (StringUtils.isBlank(repUserID)) repUserID = sharedcontext.getString("RepUserID");
			if (StringUtils.isBlank(repUserID)) repUserID = sharedcontext.getString("CurrentUserID");

			if (StringUtils.isBlank(repUserID)) {
				groups.remove("RepUserID");
				groups.setParameter("RepUserID", "!NULL;!EMPTY");
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");

				repUserID = groups.getData("RepUserID");
			}

			return getUserBean(repUserID);
			// we need to handle campaign sends differently
		} else if (sharedcontext.isSet("CampaignID") && sharedcontext.isSet("EventID")) {
			GenRow row = new GenRow();
			row.setConnection(getConnection());
			row.setViewSpec("EventView");
			row.setParameter("EventID", sharedcontext.getString("EventID"));
			row.doAction("selectfirst");

			String repOpt = row.getString("RepSpecific");
			String repUserID = "";
			if ("A".equals(repOpt) || "I".equals(repOpt)) {
				GenRow groups = new GenRow();
				groups.setConnection(getConnection());
				groups.setTableSpec("LinkedCampaigns");
				groups.setParameter("-select","*");
				groups.setParameter("CampaignID",sharedcontext.getString("CampaignID"));
				groups.setParameter("LinkedGroupID",specificcontext.getString("GroupID"));
				groups.sortBy("SortOrder", 0);
				groups.doAction("selectfirst");
				repUserID = groups.getString("RepUserID");
				if ("A".equals(repOpt) && groups.isSet("RepUserID")) return getUserBean(repUserID);   
			}

			GenRow groups = new GenRow();
			groups.setConnection(getConnection());
			groups.setTableSpec("ContactGroups");
			groups.setParameter("-select","*");
			groups.setParameter("ContactID",specificcontext.getString("ContactID"));
			groups.setParameter("GroupID",specificcontext.getString("GroupID"));
			if (requestrow != null && requestrow.getString("CURRENTUSERID").length() > 0) groups.setParameter("RepUserID", requestrow.getString("CURRENTUSERID"));
			groups.sortBy("SortOrder", 0);
			groups.doAction("selectfirst");

			if (!groups.isSuccessful() || groups.getString("ContactGroupID").length() == 0) {
				groups.remove("RepUserID");
				groups.setParameter("RepUserID", "!NULL;!EMPTY");
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
			}

			if ("I".equals(repOpt)) {
				// if no rep in the group use the one from campaign
				if (!groups.isSet("RepUserID")) return getUserBean(repUserID);
				TableData rep = getUserBean(groups.getData("RepUserID"));
				// if the user is invalid the use the one from the campaign 
				if (!rep.isSet("Email")) return getUserBean(repUserID);
				// if user is inactive use the one from campaigns
				if (rep.isSet("Status") && !"Active".equals(rep.getString("Status"))) return getUserBean(repUserID);
				return rep;
			}

			return getUserBean(groups.getData("RepUserID"));
		} else if (specificcontext.isSet("GroupID") && sharedcontext.isSet("CurrentUserID")) {
			// by now we know the group we are sending this too, so lets just get the rep
			GenRow groups = new GenRow();
			groups.setConnection(getConnection());
			groups.setTableSpec("ContactGroups");
			groups.setParameter("-select","*");
			groups.setParameter("ContactID",specificcontext.getString("ContactID"));
			groups.setParameter("GroupID",specificcontext.getString("GroupID"));
			groups.setParameter("RepUserID", sharedcontext.getString("CurrentUserID"));
			groups.sortBy("SortOrder", 0);
			groups.doAction("selectfirst");

			if (!groups.isSuccessful() || groups.getString("ContactGroupID").length() == 0) {
				groups.setParameter("RepUserID", sharedcontext.getString("RepUserID"));
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
			}

			if (!groups.isSuccessful() || groups.getString("ContactGroupID").length() == 0) {
				groups.remove("RepUserID");
				groups.setParameter("RepUserID", "!NULL;!EMPTY");
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
			}

			return getUserBean(groups.getData("RepUserID"));
		} else if (specificcontext.isSet("GroupID") && !specificcontext.isSet("RepUserID")) {
			// by now we know the group we are sending this too, so lets just get the rep
			GenRow groups = new GenRow();
			groups.setConnection(getConnection());
			groups.setTableSpec("ContactGroups");
			groups.setParameter("-select","*");
			groups.setParameter("ContactID",specificcontext.getString("ContactID"));
			groups.setParameter("GroupID",specificcontext.getString("GroupID"));
			if (requestrow != null && requestrow.getString("CURRENTUSERID").length() > 0) groups.setParameter("RepUserID", requestrow.getString("CURRENTUSERID"));
			groups.sortBy("SortOrder", 0);
			groups.doAction("selectfirst");

			if (!groups.isSuccessful() || groups.getString("ContactGroupID").length() == 0) {
				groups.remove("RepUserID");
				groups.setParameter("RepUserID", "!NULL;!EMPTY");
				groups.setConnection(getConnection());
				groups.doAction("selectfirst");
			}

			return getUserBean(groups.getData("RepUserID"));
		} else {
			String repUserID = null; 
			String[] groupIDs = null;

			Object o = sharedcontext.getObject("GroupIDs");
			Map<String, String> groupMap = null; 

			if(o != null && o instanceof Map<?, ?>) { 
				groupMap =  (Map<String, String>)o;
			}
			/* campaigns will have a set of groups assigned */
			if(groupMap != null) { 
				if(specificcontext.isSet("DefaultGroupID")) { 
					if(groupMap.containsKey(specificcontext.getParameter("DefaultGroupID")))
					{
						specificcontext.setParameter("GroupID",specificcontext.getParameter("DefaultGroupID"));
						repUserID = groupMap.get(specificcontext.getParameter("DefaultGroupID"));
						if(repUserID != null && repUserID.length()==0) { 
							repUserID = null; // - noticed this was changed in 1.19 getGroupRep(specificcontext.getParameter("DefaultGroupID")); 
						}
					}
				}
				if(repUserID == null) {
					/* returns the rep from the contact group that matches any of the context groups, in order */
					for(String gID: groupMap.keySet()) 
					{
						if((repUserID = getGroupRep(gID)) != null) { 
							/* found a contact group rep */
							String tempRep = groupMap.get(gID);
							/* does the group map have an override rep */
							if(tempRep != null && tempRep.length()>0) {
								repUserID = tempRep;
							}
							specificcontext.setParameter("GroupID", gID);
							break;
						}
					}
				}
			}
			/* check for groupIDs in the shared context (for ex set by campaigns) and then the request row */
			if((groupIDs = requestrow.getParameterValues("GroupIDs")) != null) { 

				/* returns the rep from the *DEFAULT* contact group 
				 * 	that matches the request group */
				if(specificcontext.isSet("DefaultGroupID")) { 
					for(String gID: groupIDs) 
					{				  
						/* this groupid needs to be the 'first' group in the list, not the group at the contact level 
						 * this is getColumn so that it will check parameters first and THEN the data */
						if(gID.equals(specificcontext.getParameter("DefaultGroupID"))) { 
							specificcontext.setParameter("GroupID",gID);
							repUserID = getGroupRep(gID);
						}
					}
				}
				if(repUserID == null) {
					/* returns the rep from the contact group that matches any of the context groups, in order */
					for(String gID: groupIDs) 
					{
						if((repUserID = getGroupRep(gID)) != null) { 
							specificcontext.setParameter("GroupID", gID);
							break;
						}
					}
				}
			}

			/* we've checked (or have no) list of groups, if the contact has the group that was selected in the request lets use the rep from that */
			if(repUserID == null && requestrow.isSet("GroupID")) 
			{
				/* returns the rep from the contact group that matches the request group */
				repUserID = getGroupRep(requestrow.getParameter("GroupID"));
				if(repUserID != null) 
					specificcontext.setParameter("GroupID", requestrow.getParameter("GroupID"));
			}

			/* so we have no list of groups and the contact is not in the group that came with the request but the contact has a default group - lets use that if it has a rep */
			if(repUserID == null && specificcontext.isSet("DefaultGroupID"))
			{
				repUserID = getGroupRep(specificcontext.getParameter("DefaultGroupID"));
				/* ok, so this is unlikely.. but lets check it anyway */
				if(repUserID != null) 
					specificcontext.setParameter("GroupID", specificcontext.getParameter("DefaultGroupID"));
			}

			/* if all else fails just use whatever was pulled out of the contact to begin with, which was the way emailer worked previously. */
			if(repUserID == null) 
			{
				repUserID = specificcontext.getString("RepUserID");
			} else {
				specificcontext.setParameter("RepUserID", repUserID);
			}

			return getUserBean(repUserID);   
		}
	}

	public TableData getUserBean(String userID) {
		// if there is no userID return an empty table data
		if (userID == null || userID.length() == 0) return new GenRow();
		return getBean("email/UserView", "UserID", userID);
	}

	protected String getGroupRep(String groupID) { 
		GenRow row = new GenRow();
		row.setConnection(getConnection());
		row.setViewSpec("ContactGroupView");
		row.setParameter("GroupID", groupID);
		row.setParameter(CONTACTID, specificcontext.getString(CONTACTID));
		row.setParameter("-sort1","SortOrder");

		if (requestrow != null && requestrow.getString("CURRENTUSERID").length() > 0) row.setParameter("RepUserID", requestrow.getString("CURRENTUSERID"));
		row.doAction("selectfirst");
		if (!row.isSuccessful() || row.getString("ContactGroupID").length() == 0) {
			row.remove("RepUserID");
			row.doAction("selectfirst");
		}

		if(row.isSuccessful()) 
			return row.getData("RepUserID");
		return null;
	}

	protected GenRow getOptOutChecker() {
		GenRow temp = new GenRow();
		temp.setConnection(con);
		temp.setViewSpec("OptOutView");

		temp.setColumn("Type|1", "Email");
		temp.setColumn("Type|1", "All");

		//temp.setColumn("CampaignID",requestrow.getString("NULL"));

		return (temp);
	}

	protected void loadSharedTokens() {
		sharedcontext.put("Date", new DateToken());
		sharedcontext.put("URLHome", context.getInitParameter("URLHome"));
		loadCurrentUser();
		loadAuthor();
		loadSetupTokens();

		SMSSubtype subtype = subtypehead;
		while (subtype != null) {
			subtype.loadSharedTokens(this);
			subtype = subtype.getNext();
		}
	}

	protected void loadCurrentUser() {
		if (currentUser == null) {
			throw new EmailerException("Current user not found.");
		}
		loadBean(sharedcontext, "sms/UserCurrentView", "UserID", currentUser.getUserID());
	}

	protected void loadBean(TableData context, String viewspec, String idname, String idvalue) {
		context.putAll(getBean(viewspec, idname, idvalue));
	}

	protected void loadAuthor() {
		String repUserID = requestrow.getString("RepUserID");
		if (repUserID.length() == 0 && currentUser != null) {
			repUserID = currentUser.getUserID();
			requestrow.put("RepUserID", repUserID);
		}

		if (repUserID.length() > 0) {
			TableData user = getBean("sms/UserView", "UserID", repUserID);//getUserBean(repUserID);

			sharedcontext.put("author", user);
			loadBean(sharedcontext, "sms/UserAuthorView", "UserID", repUserID);
		} else {
			throw new EmailerException("Author could not be determined.");
		}
	}

	protected void loadSetupTokens() {
		String userID = requestrow.getString("RepUserID");
		String setupID = requestrow.getString("SetupID");

		TableData setupBean = getSetupBean(userID, setupID);

		sharedcontext.put("setup", setupBean);
	}

	public TableData getSetupBean(String userID, String setupID) {

		TableData setupBean = null;
		boolean found = false;
		if (userID != null && userID.length() > 0) {
			setupBean = getBean("UserSetupView", "UserID", userID);
			if (setupBean.getString("SetupID").length() > 0) {
				found = true;
			}
		}
		if (!found) {
			if (setupID == null || setupID.length() == 0) {
				setupBean = getBean("SetupView", "SetupID", "Default");
			} else {
				setupBean = getBean("SetupView", "SetupID", setupID);
			}
		}
		return setupBean;
	}

	protected void loadSpecificTokens() {

		String id = null;
		if(toContact) {
			id = requestrow.getString(CONTACTID);
			specificcontext = getBean("sms/ContactSMSView", CONTACTID, id);

			loadProfileTokens(id);
			loadNoteTokens(id); 

			specificcontext.put("rep", getBean("sms/UserView", "UserID", specificcontext.getString("RepUserID"))/*getUserBean(specificcontext.getString("RepUserID"))*/);      
		} else {
			id =  requestrow.getString(USERID);
			specificcontext = getBean("sms/UserView", USERID,id);
		}

		SMSSubtype subtype = subtypehead;
		while(subtype!=null)
		{
			subtype.loadSpecificTokens(this, id);
			subtype = subtype.getNext();
		}


	}

	public void loadProfileTokens(String id)
	{
		if (id.length() > 0) {
			RowSetBean questionList = new RowSetBean();
			questionList.setConnection(con);
			questionList.setViewSpec("answers/ContactAnswerView");
			questionList.setColumn("ContactID",id);
			if (specificcontext.getString("Type").length() > 0) {
				questionList.setColumn("ContactAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.ContactType",specificcontext.getString("Type")+"+All");
			}
			else {
				questionList.setColumn("ContactAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.ContactType","All");
			}
			questionList.generateSQLStatment();
			questionList.getResults();

			while(questionList.getNext()) {
				specificcontext.put("Profile-"+StringUtil.ToAlphaNumeric(questionList.getString("QuestionLabel")),questionList.getString("Answer"));
				specificcontext.put("Profile-"+questionList.getString("QuestionID"),questionList.getString("Answer"));
			}
			questionList.close();
		}
	} 

	public void loadNoteTokens(String id) { 
		if(id.length()>0) { 
			GenRow noteBean = new GenRow(); 
			noteBean.setConnection(con);
			noteBean.setViewSpec("NoteView"); 
			noteBean.setColumn("ContactID",id); 
			noteBean.setColumn("Type","Export"); //This could be set by system parameter, if required.
			noteBean.sortBy("CreatedDate",0); 
			noteBean.sortOrder("DESC",0); 
			noteBean.doAction(GenerationKeys.SELECTFIRST);  

			specificcontext.put("LastNoteDate",noteBean.getColumn("NoteDate"));
			specificcontext.put("LastNoteSubject",noteBean.getColumn("Subject")); 
			specificcontext.put("LastNoteBody",noteBean.getColumn("Body")); 
		}
	}

	protected void setNoteParameters(GenRow notebean) {

		SMSSubtype subtype = subtypehead;
		while (subtype != null) {
			subtype.setNoteParameters(notebean);
			subtype = subtype.getNext();
		}

		notebean.setParameter(CONTACTID, specificcontext.getString(CONTACTID));
	}

	public String getPrimaryKeyName() {
		return (CONTACTID);
	}

	public void setConnection(Connection con) {
		try {
			if (con != null && !con.isClosed()) {
				this.con = con;
				closeconnection = false;
			} else {
				this.con = null;
			}
		} catch (Exception e) {

		}
	}

	public String generateSendAndSave() {
		generate();
		return (sendAndSave());
	}

	protected String sendAndSave() {
		String status = send();
		save(status);
		return (status);
		//      return "simulate send";
	}

	public String send() {
		setHeaders();


		if (smsBean.getNumber() == null || smsBean.getNumber().length() == 0) {
			return (SMS.TOEMPTY);
		}

		if (smsBean.getFrom() == null || smsBean.getFrom().length() == 0) {
			return (SMS.FROMEMPTY);
		}

		String status = sendSMS();

		if ("Message Sent".equals(status)) {
			return (SMS.SENT);
		}
		return (status);
	}

	protected String sendSMS() {

		if(toContact && isOptedOut())
		{
			String msg = Emailer.OPTOUT + " for Communications";  
			return(msg);
		}
		if(toContact && hasGlobalOptOut()) {
			String msg = Emailer.OPTOUT + " for Old Style Contact";  
			return(msg);
		}
		if (toContact && hasGroupOptOut()) {
			String groupName = specificcontext.getString("GroupName");
			String groupID = specificcontext.getString("GroupID");

			String msg = Emailer.OPTOUT + " for Group " + groupName + "(" + groupID + ")";  
			return(msg);
		}

		//mailerBean.setAttachments(getAttachments(specificcontext.getString("NoteID")));
		return (smsBean.getResponse());
	}

	protected void setHeaders() {

		Properties sysprops = InitServlet.getSystemParams();

		String smshost = sysprops.getProperty("SMSHost");
		String smsid = sysprops.getProperty("SMSID");
		String smspassword = sysprops.getProperty("SMSPassword");
		String smsprovider = sysprops.getProperty("SMSProvider");

		if (smshost != null) {
			smsBean.setIP(smshost);

		}
		if (smsprovider != null) {
			smsBean.setProvider(smsprovider);
			smsBean.setPassword(smspassword);
			smsBean.setAccount(smsid);
		}

		smsBean.setProvider(smsprovider);
		smsBean.setAccount(smsid);
		smsBean.setPassword(smspassword);

		//      smsBean.setFrom(from);
		//
		//      smsBean.setNumber(mobile);
		//      smsBean.setSubject(actionType.getField("Subject"));
		//      smsBean.setMessage(appointmentGenerator.getOutput());

		//      String cc = requestrow.getString("CC");
		//      String bcc = requestrow.getString("BCC");
		String to = requestrow.getString("TO");
		String from = requestrow.getString("FROM");
		String replyto = requestrow.getString("REPLY_TO");

		if (from.length() > 0) {
			smsBean.setFrom(getFromAddress(from, specificcontext, currentUser, null));
		} else if (sharedcontext.getString("EmailFrom").length() > 0) {
			smsBean.setFrom(getFromAddress(sharedcontext.getString("EmailFrom"), specificcontext, currentUser, null));
		} else if (specificcontext.getString("RepEmail").length() > 0) {
			smsBean.setFrom(specificcontext.getString("RepEmail"));
		} else {
			smsBean.setFrom(InitServlet.getSystemParams().getProperty("DefaultEmail"));
		}

		// Setting the mobile number
		smsBean.setNumber(specificcontext.getString("Mobile")); //mailerBean.setTo(specificcontext.getString("Email"));
	}

	public static String getFromAddress(String emailfrom, TableData contact, TableData user,
			TableData note) {
		String from = null;
		if (emailfrom.length() != 0) {
			emailfrom = ActionBean.getSubstituted(emailfrom, contact, "fromemailaddress");

			if (emailfrom.equals("Support") || emailfrom.equals("RepSupportEmail")) {
				from = contact.getString("RepSupportEmail");
			} else if (emailfrom.equals("User") || emailfrom.equals("CurrentUser")) {
				from = user.getString("Email");
			} else if (note != null && emailfrom.equals("AuthorEmail")) {
				from = note.getString("AuthorEmail");
			} else if (emailfrom.equals("RepEmail")) {
				from = contact.getString("RepEmail");
			} else if (emailfrom.indexOf("@") > 0) {
				from = emailfrom;
			} else {
				from = contact.getString("RepEmail");
			}
		} else {
			from = contact.getString("RepEmail");
		}
		return (from);
	}

	public void save(String status) {
		if (SMS.SENT.equals(status) || !offlinemode) {
			saveNote(status);
			saveDocuments();
		}
	}

	protected void saveNote(String status) {

		GenRow row = new GenRow();
		row.setTableSpec("Notes");
		row.setConnection(con);
		row.putAll(specificcontext);
		setNoteParameters(row);

		row.setParameter("EmailFrom", smsBean.getFrom());
		row.setParameter("Type", "SMS");
		row.setParameter("Subject", smsBean.getSubject());
		row.setParameter("Body", smsBean.getMessage());


		row.setParameter("NoteID", specificcontext.getString("NoteID"));
		row.setParameter("TemplateUsed", requestrow.getString("SMSTemplateID"));
		row.setParameter("CMSUsed", requestrow.getString("CMSID"));

		if (requestrow.getString("-action").equals("update")) {
			row.setAction(requestrow.getString("-action"));
		} else {
			row.setParameter("NoteDate", "NOW");
			row.setParameter("NoteTime", "NOW");
			row.setParameter("CreatedDate", "NOW");
			row.setParameter("CreatedBy", currentUser.getCurrentUserID());
			row.setAction("insert");
		}

		if (status != null && status.equals(SENT)) {
			row.setParameter("SentDate", "NOW");
		}

		row.setParameter("GroupID", requestrow.getString("GroupID").length() != 0 ? requestrow.getString("GroupID") : specificcontext.getString("GroupID"));
		row.setParameter("ModifiedDate", "NOW");
		row.setParameter("ModifiedBy", currentUser.getCurrentUserID());

		row.setParameter("Status", status);
		row.doAction();
	}

	private void saveDocuments() {
		String noteID = specificcontext.getString("NoteID");

		if (EmailAttachmentBuilder.hasEmailAttachmentBuilder(noteID)) {
			String contactID = requestrow.getString("ContactID");
			String companyID = requestrow.getString("CompanyID");
			String opportunityID = requestrow.getString("OpportunityID");

			EmailAttachmentBuilder builder = EmailAttachmentBuilder.getEmailAttachmentBuilder(noteID);

			Iterator iter = builder.getUploadedFiles().iterator();
			File file = null;
			File tempDir = null;
			File parentDir = null;
			while (iter.hasNext()) {
				file = (File) iter.next();
				if (file != null) {
					if (parentDir == null) {
						tempDir = file.getParentFile();
						parentDir = tempDir.getParentFile().getParentFile();
						parentDir = new File(parentDir, KeyMaker.generate());
						parentDir.mkdir();
					}
					saveDocument(builder, file, parentDir);
				}
			}
			if (tempDir != null) {
				tempDir.delete();
			}

			iter = builder.getDocumentIDs().iterator();
			while (iter.hasNext()) {
				String documentID = (String) iter.next();

				GenRow doc = new GenRow();
				doc.setConnection(con);
				doc.setViewSpec("LinkedDocumentView");
				doc.setColumn("LinkedDocumentID", KeyMaker.generate());
				doc.setColumn("DocumentID", documentID);
				doc.setColumn("NoteID", noteID);
				doc.setColumn("ContactID", contactID);
				doc.setColumn("OpportunityID", opportunityID);
				doc.setColumn("CompanyID", companyID);
				doc.setColumn("CreatedDate", "NOW");
				doc.setColumn("CreatedBy", currentUser.getCurrentUserID());
				doc.setAction("insert");
				doc.doAction();
			}

			builder.removeAllFiles();
		}
	}

	protected void saveDocument(EmailAttachmentBuilder builder, File file, File parentDir) {
		File movedFile = new File(parentDir, file.getName());
		file.renameTo(movedFile);
		String filePath = getDocumentFilePath(movedFile);
		String documentID = KeyMaker.generate();

		GenRow doc = new GenRow();
		doc.setConnection(con);
		doc.setViewSpec("DocumentView");
		doc.putAll(requestrow);
		doc.setColumn("DocumentID", documentID);
		doc.setColumn("DocumentType", "-Unspecified-");
		doc.setColumn("FileName", movedFile.getName());
		doc.setColumn("Description", "Email Attachment");
		doc.setColumn("FilePath", filePath);
		doc.setColumn("DocumentCategory", "MailAttachment");
		doc.setColumn("ModifiedDate", "NOW");
		doc.setColumn("CreatedDate", "NOW");
		doc.setColumn("ModifiedBy", currentUser.getCurrentUserID());
		doc.setColumn("CreatedBy", currentUser.getCurrentUserID());
		doc.setAction("insert");
		doc.doAction();

		builder.moveUploadedFileToLinkedDocument(movedFile, documentID);
	}

	protected String getDocumentFilePath(File file) {
		String filePath = file.getAbsolutePath().substring(context.getRealPath("").length());
		if (File.separatorChar != '/') {
			filePath = StringUtil.replace(filePath, File.separatorChar, '/');
		}
		return (filePath);
	}

	public void generate() {      
		loadSpecificTokens();      
		generateSMS();
	}

	public String generateText(String template) {
		try {
			StringWriter text = new StringWriter();
			ve.evaluate(specificcontext, text, "Email", template);
			text.flush();
			text.close();
			return (text.toString());
		} catch (Exception e) {
			throw new EmailerException(e);
		}
	}

	protected void generateSMS() {
		mergeContexts();
		SMSSubtype subtype = getOverrideSubtype();
		if (subtype == null) {         
			generateDefaultSMS();         
		} else {
			subtype.generateSMS(this);
		}
	}

	public void generateSMS(String subjecttemplate, String texttemplate) { // tidligere generateEmail
		if (hasTemplate()) {

			smsBean.setSubject(generateText(subjecttemplate));
			smsBean.setMessage(generateText(texttemplate));

		} else {

			smsBean.setSubject(subjecttemplate);
			smsBean.setMessage(texttemplate);   
		}
	}

	protected void generateDefaultSMS() {

		if (hasTemplate()) {
			StringBuffer textbuffer = new StringBuffer();         
			String body = requestrow.get("Body") != null ? requestrow.getString("Body") : template.getString("Body");
			String subject = requestrow.get("Subject") != null ? requestrow.getString("Subject") : template.getString("Subject");

			//         textbuffer.append(template.getString("TextHeader"));
			//         com.sok.Debugger.getDebugger().debug("TextHeader="+template.getString("TextHeader")+" <br>\n");
			//         textbuffer.append(" \r\n\r\n ");
			//         textbuffer.append(body);
			//         textbuffer.append(" \r\n\r\n ");
			textbuffer.append(template.getString("TextFooter"));
			//         generateSMS(subject, textbuffer.toString());
			generateSMS(subject, body);
		} else {
			generateSMS(requestrow.getString("Subject"), requestrow.getString("Body"));

		}
	}

	protected String getTemplateBody() {
		String body = template.getString("Body");
		body = generateText(body);

		return (body);
	}

	protected void mergeContexts() {
		specificcontext.putAll(sharedcontext);
		setNoteID();
	}

	protected void setNoteID() {
		if ((offlinemode && (!previewmode)) || (!offlinemode && requestrow.getString("NoteID").length() == 0)) {
			specificcontext.setToNewID("NoteID");
		} else {
			specificcontext.put("NoteID", requestrow.getString("NoteID"));
		}
	}

	//***************************************************************************
	//
	// Getters
	//
	//***************************************************************************
	public Connection getConnection() {
		return (con);
	}

	public TableData getSharedContext() {
		return (sharedcontext);
	}

	public TableData getSpecificContext() {
		return (specificcontext);
	}

	public ServletContext getServletContext() {
		return (context);
	}

	public Properties getSystemProperties() {
		return (this.sysprops);
	}

	public TableData getRequestRow() {
		return (requestrow);
	}

	public SMSBean getSMSBean()
	{
		return(smsBean);
	}

	//***************************************************************************
	//
	// Utility methods
	//
	//***************************************************************************
	public TableData getBean(String viewspec, String idname, String idvalue) {
		RowBean temp = new RowBean();
		temp.setConnection(con);
		temp.setViewSpec(viewspec);
		temp.setColumn(idname, idvalue);
		temp.doAction(ActionBean.SELECT);
		return (temp);
	}

	protected void setRequest(GenRow requestrow) {
		UserBean u = (UserBean) requestrow.get(SessionKeys.CURRENT_USER, Parameter.type_InternalTokenParameter);
		if (u != null) {
			currentUser = u;
		}
	}

	protected void setNoteDefaults() {
		if (requestrow.getString("Type").length() == 0) {
			requestrow.setParameter("Type", "Appointment");
		}
		if (requestrow.getString("Completed").length() == 0) {
			requestrow.setParameter("Completed", "Yes");
		}
		if (requestrow.getString("NoteDate").length() == 0) {
			requestrow.setParameter("NoteDate", "NOW");
		}
	}

	public void loadProfile(GenRow genrow, String viewspec, String keyname, String keyvalue, TableData context) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void loadProfile(String viewspec, String keyname, String keyvalue, TableData context) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void setTemplate(GenRow template) {
		this.template = template;
	}

	public String getRequestString(String name) {
		return(requestrow.getString(name));
	}
}
