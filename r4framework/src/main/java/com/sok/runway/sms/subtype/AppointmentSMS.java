package com.sok.runway.sms.subtype;

import com.sok.runway.sms.SMSSubtypeHandler;
import com.sok.runway.email.*;
import com.sok.framework.*;
import com.sok.runway.sms.SMS;

public class AppointmentSMS extends AbstractSMSSubtype
{
   public static final String APPOINTMENTID = "AppointmentID";  
   
   public boolean isSMSSubtype(int type)
   {
      return(type == SMS.AppointmentSMS);
   }
   
   public void loadSharedTokens(SMSSubtypeHandler sms)
   {

   }

   
//   public boolean overridesSMSGeneration() {
//      return true;
//   }
   
   /**
    * @deprecated
    */
   public void loadSpecificTokens(SMSSubtypeHandler sms)
   {
      loadAppointmentTokens(sms, requestrow.getString(APPOINTMENTID), sms.getSpecificContext());
   } 
   
   public void loadSpecificTokens(SMSSubtypeHandler sms, String id)
   {
      loadAppointmentTokens(sms, requestrow.getString(APPOINTMENTID), sms.getSpecificContext());
   } 
   
   protected void loadAppointmentTokens(SMSSubtypeHandler sms, String appointmentID, TableData emailcontext)
   {
      if(appointmentID != null && appointmentID.length() > 0)
      {
                 
         com.sok.Debugger.getDebugger().debug("loadAppointmentTokens: First NoteID"+appointmentID);
         
         TableData appointmentBean = sms.getBean("sms/AppointmentSMSView", "NoteID"/*APPOINTMENTID*/, appointmentID);
         
         
         com.sok.Debugger.getDebugger().debug("Venue="+appointmentBean.getData("Venue")+", ");
         com.sok.Debugger.getDebugger().debug("Subject="+appointmentBean.getData("Subject")+", ");
         com.sok.Debugger.getDebugger().debug("NoteID="+appointmentBean.getData("NoteID")+", ");
         
         emailcontext.put("appointment", appointmentBean);

         appointmentBean.put("setup", sms.getSetupBean(appointmentBean.getString("RepUserID"), null));
         appointmentBean.put("rep", sms.getBean("sms/UserView", "UserID", appointmentBean.getString("RepUserID")));
         
      }      
   }   
   
   public void setNoteParameters(TableData notebean)
   {
      notebean.setParameter(APPOINTMENTID, requestrow.getString(APPOINTMENTID));
   }

   public void generateSMS(SMSSubtypeHandler sms)
   {
      
      com.sok.Debugger.getDebugger().debug("Generate SMS");
      sms.generateSMS(requestrow.getString("Subject"),requestrow.getString("Body"));

   }
}