package com.sok.runway.sms;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import com.sok.runway.*;
/* 
# AccountName: (Mandatory) Supplied by Redrock
# Sender: (Mandatory)  Description of Sender Company - Anything you want
# SenderAddr: (Optional) Description of Sender Address - Anything you want
# Dest User: (Optional) Description of Recipient - Anything you want
# PhoneNumber: (Mandatory) Destination GSM phone number
# NetworkCode: (Mandatory) Destination GSM Network - (OGSM,TGSM,VGSM) Default to GSM
# Subject: (Optional) If you have a password on the account put it in here
# Message: (Mandatory) Your message text

print $remote "\r\rAccountName\r1\r1\rSender Co.\rSenderAddr\rDest User\rPhoneNumber\rGSM\r0\r0\r0\rSubject\rA Message from Perl";
 
# Send result
# Accepted Result code = 0x06 Description = Lodge OK
# Rejected Result code = 0x15 Description = Lodge Error

$remote->read ( $strbuff,10,0);

print STDOUT $strbuff;
*/ 
public class RedrockSMS implements SMSProvider
{
	Socket socket = null;
	PrintWriter out = null;
	BufferedReader in = null;		
	SMSBean bean = null;

	static final char ok = '';
	static final char r = '\r';
	static final char one = '1';
	static final char two = '2';
	static final char zero = '0';
	static final char space = '0';
	static final String blank = "";

	
	public static final String gsm = "GSM";
	public static final String email = "EMAIL";
	
	int port = 7001;
	String address = "203.44.144.135";
	String account = "knowledge1";
	String password = "on1";
	
	public RedrockSMS(SMSBean bean)
	{
		this.bean = bean;
	}
	
	public String connect()
	{
		if(bean.getIP()!=null)
		{
			address = bean.getIP();
		}
		
		if(bean.getPort()!= -1)
		{
			port = bean.getPort();
		}
		
		try
		{
			socket = new Socket(address, port);
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			return(null);
		}
		catch(Exception e)
		{	
			if(out!=null){
				out.close();
				out = null;
			}
			if(in!=null){
				try{
					in.close();
					in = null;
				}catch(Exception ex){}
			}
			if(socket!=null){
				try{
					socket.close();
					socket = null;
				}catch(Exception ex){}
			}
			return(e.getMessage());
		}
	}

	public String send()
	{
		if(bean.getAccount()!=null)
		{
			account = bean.getAccount();
		}
		if(bean.getPassword()!=null)
		{
			password = bean.getPassword();
		}	
		
		String response = null;
		out.print(bean.getFrom());out.print(r);		//ReplyAddr
		out.print(email);out.print(r);	//ReplyType
		out.print(account);out.print(r); // username
		out.print(one);out.print(r);//1
		out.print(two);out.print(r);//2
		out.print(bean.getSender());out.print(r);//Sender
		out.print(r);//SenderAddr			
		out.print(r);//Dest User
		out.print(bean.getNumber());out.print(r);//rPhoneNumber
		out.print(gsm);out.print(r); //Destination GSM Network - (OGSM,TGSM,VGSM)
		out.print(zero);out.print(r);
		out.print(zero);out.print(r);
		out.print(zero);out.print(r);
		out.print("SMS");out.print(space);out.print(password);out.print(r);//Subject Password
		out.print(bean.getMessage());out.print(r); //Message
		out.flush();
				
		try{
			Thread.sleep(500);
		}catch(Exception e){}
		
		try{		
			response = in.readLine();
		}catch(Exception e){
			response = e.getMessage();
		}
			
		if(response.charAt(0) == ok || response.indexOf("OK")>0)
		{
			response = sent;
		}
		else if(response.length() < 2)
		{
			//response = error;
		}
		return(response);
	}	
	
	public void close()
	{
		if(out!=null){
			out.close();
			out = null;
		}
		if(in!=null){
			try{
				in.close();
				in = null;
			}catch(Exception ex){}
		}
		if(socket!=null){
			try{
				socket.close();
				socket = null;
			}catch(Exception ex){}
		}
	}	
}