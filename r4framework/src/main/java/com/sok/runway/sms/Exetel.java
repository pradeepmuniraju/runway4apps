package com.sok.runway.sms;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.StringUtil;
import com.sok.runway.*;

public class Exetel implements SMSProvider
{
   SMSBean bean = null; 
   OutputStreamWriter wr = null;
   BufferedReader rd = null;
   URLConnection conn = null;

   int status = 0;
   String response = null;
   
   int port = 443;
   String address = "https://smsgw.exetel.com.au/sendsms/api_sms.php";
   String account = "";
   String password = "";  
   
   private static final Logger logger = LoggerFactory.getLogger(Exetel.class);
   
   public Exetel(SMSBean bean)
   {
      this.bean = bean;
   }  
   
   public String connect()
   {
      if(bean.getIP()!=null)
      {
         address = bean.getIP();
      }  
      
      try
      {
         URL url = new URL(address);
         conn = url.openConnection();
      }
      catch(Exception e)
      {
          if(wr!=null)
          {
             try{
                wr.close();
                wr = null;
             }catch(Exception e1){}
          }
          if(rd!=null)
          {
             try{
                rd.close();
                rd = null;
             }catch(Exception e1){}
          }        
         return(e.toString());
      }
      finally
      {
      }
      return(null);
   }

   public String connect(String params)
   {
      if(bean.getIP()!=null)
      {
         address = bean.getIP();
      }  
      
      try
      {
         URL url = new URL(address + "?" + params);
         conn = url.openConnection();
      }
      catch(Exception e)
      {
          if(wr!=null)
          {
             try{
                wr.close();
                wr = null;
             }catch(Exception e1){}
          }
          if(rd!=null)
          {
             try{
                rd.close();
                rd = null;
             }catch(Exception e1){}
          }        
         return(e.toString());
      }
      finally
      {
      }
      return(null);
   }

   
   public void close()
   {
      if(wr!=null)
      {
         try{
            wr.close();
            wr = null;
         }catch(Exception e){}
      }
      if(rd!=null)
      {
         try{
            rd.close();
            rd = null;
         }catch(Exception e){}
      }
      
      conn = null;
   }
   
   String encodeMessage()
   {
      if(bean.getAccount()!=null)
      {
         account = bean.getAccount();
      }
      if(bean.getPassword()!=null)
      {
         password = bean.getPassword();
      }     
      
      StringBuffer post = new StringBuffer();
      post.append("username=");
      post.append(account);
      post.append("&password=");
      post.append(password);
      post.append("&mobilenumber=");
      post.append(StringUtil.urlEncode(bean.getNumber()));
      post.append("&sender=");
      if(bean.getSender()!=null)
      {
    	  String sender = bean.getSender();
    	  if (sender.length() > 11) sender = sender.substring(0,11);
            post.append(StringUtil.urlEncode(sender));
      } else {
    	  post.append("runway");
      }
      post.append("&message=");
      post.append(StringUtil.urlEncode(bean.getMessage()));
      post.append("&messagetype=Text");
//      post.append("&referencenumber="); //optional

      //System.out.println(post);
      
      return(post.toString());
   }
   
   public String send()
   {
	  if (StringUtils.isBlank(bean.getSender())) return "Invalid Sender";
	  
	  Date s = new Date(); 
      try
      {
    	  connect(encodeMessage());
    	  
         HttpURLConnection httpconn = null;
         if(conn instanceof HttpURLConnection)
         {
            httpconn = (HttpURLConnection)conn;
         }
         

         /*
         if (wr == null) {
	         conn.setDoOutput(true);
	         wr = new OutputStreamWriter(conn.getOutputStream());
         }
         wr.write(encodeMessage());
         wr.flush();          
         conn.connect();
         */

         
         if(httpconn!=null)
         {
            status = httpconn.getResponseCode();
            response = httpconn.getResponseMessage();
         }
         
         if (rd == null) {
        	 rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
         }
         String line;
         StringBuffer responseBuffer = new StringBuffer();
         while((line = rd.readLine()) != null)
         {
            responseBuffer.append(line);
            responseBuffer.append("\r\n");
         }
         response = responseBuffer.toString();
         
         close();
      }
      catch(Exception e)
      {
    	  close();
    	 e.printStackTrace();
         return(e.getMessage());
      }
      
      String returnstr = null;
      if(response.indexOf("1|")==0) // 0: rejected, 1 sent, 2 failed.
      {
         returnstr = sent;
      }
      else
      {
         returnstr = response;
      }
      Date e = new Date();
      
      logger.trace("Sent SMS in " + (e.getTime() - s.getTime()) + " to " + bean.getNumber() + " response " + response);
      
      return(returnstr);      
   }
}

