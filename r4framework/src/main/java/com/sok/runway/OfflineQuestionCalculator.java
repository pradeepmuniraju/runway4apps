package com.sok.runway;

import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import java.text.*;
import com.sok.framework.*;

public class OfflineQuestionCalculator extends OfflineProcess {
   
	StringBuffer status = null;
	StringBuffer errors = null;

	UserBean user = null;
	//HttpSession session = null;

	Connection con = null;
	Context ctx = null;

   int numCalculated = 0;
   int numFailedCalculation = 0;
   
   AnswerBean answerBean = null;
   RowSetBean sumBean = null;
   GregorianCalendar calendar = null;
   
	public OfflineQuestionCalculator(HttpServletRequest request, ServletContext context) {
		super(request, context);
		status = new StringBuffer();
		errors = new StringBuffer();
		HttpSession session = request.getSession();
		user = (UserBean) session.getAttribute(SessionKeys.CURRENT_USER);
		
		answerBean = new AnswerBean();
		answerBean.setCurrentUser(user);
      
      sumBean = new RowSetBean();
      sumBean.setViewSpec("OrderView");
		
      calendar = new GregorianCalendar();
      start();
	}
	
	private Connection getConnection() {
	   try {
		   if(con == null || con.isClosed()) {
				if(ctx == null) {
					ctx = new InitialContext();
				}

				DataSource ds = null;
				try {
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource) envContext.lookup(dbconn);
				}
				catch(Exception e) {
				}
				if(ds == null) {
					ds = (DataSource) ctx.lookup(dbconn);
				}
				con = ds.getConnection();
			}
		}
		catch (Exception e) {
		   errors.append(ActionBean.writeStackTraceToString(e));
		}
		return con;
	}

	public void execute() {		
		try {
			getConnection();
			
			RowSetBean calculatableQuestions = new RowSetBean();
			calculatableQuestions.setViewSpec("NoteView");
			calculatableQuestions.setConnection(getConnection());
			
			StringBuffer qry = new StringBuffer();
			qry.append("SELECT Visits.VisitID, Answers.QuestionID, Answers.Answer, RelatedQuestions.InputType, RelatedQuestions.ProductGroupID,");
			qry.append("RelatedQuestions.CalculationPeriod,RelatedQuestions.QuestionID AS CalcQuestionID, Notes.NoteDate,Notes.CompanyID,");
			qry.append("(SELECT X.Answer FROM Visits Y LEFT OUTER JOIN Competitors ON Y.CompetitorID = Competitors.CompetitorID");
			qry.append("         LEFT OUTER JOIN Answers X ON X.VisitID = Y.VisitID");
			qry.append("         WHERE (Y.NoteID = Visits.NoteID) AND (Competitors.PrimaryFlg = 'Y') AND (X.QuestionID = Answers.QuestionID)) AS BaseValue ");
			qry.append("FROM Visits LEFT OUTER JOIN Answers ON Visits.VisitID = Answers.VisitID ");
			qry.append("LEFT OUTER JOIN Notes ON Notes.NoteID = Visits.NoteID ");
			qry.append("LEFT OUTER JOIN Questions RelatedQuestions ON RelatedQuestions.RelatedQuestionID = Answers.QuestionID ");
			qry.append("WHERE RelatedQuestions.InputType = '");
			qry.append(Profile._calculation_sum_order);
			qry.append("' AND Notes.NoteDate < ");
			qry.append(ActionBean.getDatabase().getCurrentDateMethod());
			
			calculatableQuestions.setSearchStatement(qry.toString());
			calculatableQuestions.getResults();
			
			errors.append(calculatableQuestions.getError());
			//status.append(qry);
						
			while (calculatableQuestions.getNext() && !isKilled()) {
			   calculateQuestion(calculatableQuestions.getString("CompanyID"),calculatableQuestions.getString("VisitID"), 
			                     calculatableQuestions.getString("CalcQuestionID"), 
			                     calculatableQuestions.getString("CalculationPeriod"), calculatableQuestions.getString("Answer"), 
			                     calculatableQuestions.getString("BaseValue"), calculatableQuestions.getString("ProductGroupID"),
			                     (java.util.Date)calculatableQuestions.getObjectFromString("NoteDate"));
			}
			setEmailHeader();
		}
		catch(Exception e) {
			status.append("Offline process halted on because of ");
			status.append(".\r\n\r\n");
			status.append(ActionBean.writeStackTraceToString(e));

		}
		finally {
		   closeConnection();
		}
	}

   private void closeConnection() {
			if(con != null) {
				try {
					con.close();
					con = null;
				}
				catch(Exception ex) {
				}
			}
			if(ctx != null) {
				try {
					ctx.close();
					ctx = null;
				}
				catch(Exception ex) {
				}
			}

			if(errors != null && errors.length() != 0) {
				status.append("\r\n");
				status.append(errors.toString());
			}
   }
   
   private void calculateQuestion(String companyID, String visitID, String questionID, String calcPeriod, 
                                 String numeratorStr, String denominatorStr, 
                                 String productGroupID, java.util.Date noteDate) {
      answerBean.clear();
      numCalculated++;
      boolean hasProductGroup = productGroupID != null && productGroupID.length() > 0;
      
      calendar.setTime(noteDate);
      
      StringBuffer startDate = new StringBuffer();
      StringBuffer endDate = new StringBuffer();
      if (calcPeriod.equals(ActionBean.CURRENTYEAR)) {
         startDate.append("01/01/");
         endDate.append("31/12/");
         
         startDate.append(calendar.get(Calendar.YEAR));
         endDate.append(calendar.get(Calendar.YEAR));            
      }
      else if (calcPeriod.equals(ActionBean.CURRENTWEEK)) {
         while(calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.roll(Calendar.DATE,-1);
         }
         
         if (calendar.get(Calendar.DATE) < 10) {
            startDate.append("0");
         }
         startDate.append(calendar.get(Calendar.DATE));
         startDate.append("/");
         if (calendar.get(Calendar.MONTH)+1 < 10) {
            startDate.append("0");
         }
         startDate.append(calendar.get(Calendar.MONTH)+1);
         startDate.append("/");
         startDate.append(calendar.get(Calendar.YEAR));
         
         calendar.roll(Calendar.DATE,6);
         if (calendar.get(Calendar.DATE) < 10) {
            endDate.append("0");
         }
         endDate.append(calendar.get(Calendar.DATE));
         endDate.append("/");
         if (calendar.get(Calendar.MONTH)+1 < 10) {
            endDate.append("0");
         }
         endDate.append(calendar.get(Calendar.MONTH)+1);
         endDate.append("/");
         endDate.append(calendar.get(Calendar.YEAR));
         
      }
      // We Default to Month if nothing specified
      else { // if (calcPeriod.equals(ActionBean.CURRENTMONTH)) {
         startDate.append("01/");
         endDate.append(calendar.getActualMaximum(Calendar.DATE));
         endDate.append("/");
         
         if (calendar.get(Calendar.MONTH)+1 < 10) {
            startDate.append("0");
            endDate.append("0");
         }
         startDate.append(calendar.get(Calendar.MONTH)+1);
         startDate.append("/");
         endDate.append(calendar.get(Calendar.MONTH)+1);
         endDate.append("/");
         
         startDate.append(calendar.get(Calendar.YEAR));
         endDate.append(calendar.get(Calendar.YEAR));  
      }
         
      
      StringBuffer sumQry = new StringBuffer();
      sumQry.append("SELECT SUM(OrderItems.TotalCost) AS Amount From Orders  ");
      sumQry.append(" INNER JOIN OrderItems ON Orders.OrderID = OrderItems.OrderID ");
      if (hasProductGroup) {
         sumQry.append(" INNER JOIN Products ON Products.ProductID = OrderItems.ProductID WHERE ");
         sumQry.append(" Products.ProductGroupID = '");
         sumQry.append(productGroupID);
         sumQry.append("'");
         sumQry.append(" AND ");
      }
      else {
         sumQry.append(" WHERE ");
      }
      sumQry.append("Orders.CompanyID = '");
      sumQry.append(companyID);
      sumQry.append("' AND ");
      ActionBean.getDatabase().appendDateRangeStmt(sumQry,"Orders.DateOfSale",startDate.toString(),endDate.toString());
      
      sumBean.clear();
      sumBean.setConnection(getConnection());
      sumBean.setSearchStatement(sumQry.toString());
      sumBean.getResults();
      sumBean.getNext();
		if (sumBean.getError().length() > 0) {
		   errors.append(sumBean.getError());
		   setLastError(sumBean.getError());
		   numFailedCalculation++;
		}
		//status.append(sumQry);
      
      double numerator = 0d;
      double denominator = 0d;
      double factor = 0d;
      double amount = 0d;
      try { numerator = Double.parseDouble(numeratorStr); }
      catch (Exception e) {}
      try { denominator = Double.parseDouble(denominatorStr); }
      catch (Exception e) {}
      try { factor = numerator / denominator; }
      catch (Exception e) {}
      try { amount = Double.parseDouble(sumBean.getString("Amount")) * factor; }
      catch (Exception e) {}
      
      /*status.append("\r\n\r\n");
      status.append(visitID);
      status.append("\r\n\r\n");
      status.append(questionID);
      status.append("\r\n\r\n");
      status.append(String.valueOf(amount));*/
      
		answerBean.setConnection(getConnection());
      answerBean.saveVisitAnswer(visitID, questionID, null, String.valueOf(amount), 0);
      
      errors.append(answerBean.getError());
   }
   
   
	void setEmailHeader()
	{
		status.append(contextname);
		status.append(" Runway offline process - Offline Calculation\r\n");
		status.append("Initiated ");
		try
		{
			DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM,
					super.getLocale());
			status.append(dt.format(new java.util.Date()));
		}
		catch(Exception e)
		{
			status.append(new java.util.Date().toString());
		}
		status.append("\r\n");
		
		status.append(String.valueOf(numCalculated));
		status.append(" values required calculation.\r\n");
		status.append(String.valueOf(numCalculated - numFailedCalculation));
		status.append(" values calculated successfully.\r\n");
		status.append(String.valueOf(numFailedCalculation));
		status.append(" values failed calculation.\r\n");
		if (isKilled()) {
	      status.append("Offline process terminated by - ");
	      status.append(getKiller());
	      status.append("\r\n\r\n");
	   }
	   else {	   
	      status.append("Offline process completed.\r\n\r\n");
	   }
			
	}
   
	public String getStatusMailBody()
	{
		return (status.toString());
	}

	public String getStatusMailSubject()
	{
		return (contextname + " Runway offline process - QuestionCalculator");
	}

	public String getStatusMailRecipient()
	{
		return (user.getEmail());
	}
	
	public int getProcessSize() {
      return -1;
   }
   
   public int getProcessedCount() {
      return numCalculated;
   }
   
   public int getErrorCount() {
      return numFailedCalculation;
   }
}