package com.sok.runway;

import com.sok.framework.InitServlet;
import com.sok.runway.sms.*;

public class SMSBean
{	
	String number = null;
	String sender = "switchon";
	String message = null;
	String from = null;
	String subject = "";
	
	SMSProvider smssender;

	int port = -1;
	String address = null;
	String account = null;
	String password = null;	
	
	public static final String redrock = "Redrock";
	public static final String yousms = "youSMS";
	public static final String globalmessagingsms = "globalmessagingSMS";
	public static final String fivecentsms = "FiveCentSMS";
	public static final String exetel = "Exetel";   
	public static final String messagemediasms = "MessageMediaSMS";
   public static final String inspirus = "Inspirus";
   public static final String whispir = "Whispir";
   
   public static boolean debug = false;
   
	public SMSBean()
	{
	   String smshost = null, smsid = null, smspassword = null, smsprovider = null, smsfrom = null;;
		java.util.Properties sysprops = InitServlet.getSystemParams();

	     smshost = sysprops.getProperty("SMSHost");
	     smsid = sysprops.getProperty("SMSID");
	     smspassword = sysprops.getProperty("SMSPassword");
	     smsprovider = sysprops.getProperty("SMSProvider");
         smsfrom = sysprops.getProperty("SMSFrom");

		if(smshost!=null)
		{
			setIP(smshost);
		}
		if(smsprovider!=null)
		{
			setProvider(smsprovider);
			setAccount(smsid);
			setPassword(smspassword);
		}
		if(smsfrom!=null)
		{
         setSender(smsfrom);
		}
	}
	
   public SMSBean(boolean debug)
   {
      this.debug = debug;
   }	

	public void setProvider(String name)
	{
		if(name!=null)
		{
         if(name.equals(redrock))
         {
         	smssender = new RedrockSMS(this);
         }
         else if(name.equals(yousms))
         {
         	smssender = new YouSMS(this);
         }
         else if(name.equals("globalmessagingSMS"))
         {
         	smssender = new GlobalMessagingSMS(this);
         }	
         else if(name.equals("messagemediasms"))
         {
         	smssender = new MessageMediaSMS(this);
         }	
         else if(name.equals(fivecentsms))
         {
            smssender = new FiveCentSMS(this);
         }  
         else if(name.equals(exetel))
         {
            smssender = new Exetel(this);
         }
         else if(name.equals(inspirus))
         {
            smssender = new Inspirus(this);
         } else if(name.equals(whispir))
         {
            smssender = new Whispir(this);
         }
		}
	}
	
	public void setProvider(SMSProvider sms)
	{
		smssender = sms;
	}
	
	public void clear()
	{
		number = null;
		from = null;
		subject = null;
		message = null;
	}
	
	public void setNumber(String num)
	{
		number = num;
	}
	
	public String getNumber()
	{
		return(number);
	}
	
	public void setPort(int num)
	{
		port = num;
	}
	
	public int getPort()
	{
		return(port);
	}
	
	public void setIP(String ip)
	{
		address = ip;
	}

	public String getIP()
	{
		return(address);
	}
	
	public void setFrom(String email)
	{
		from = email;
	}

	public String getFrom()
	{	
		return(from);
	}
	
	public void setAccount(String name)
	{
		account = name;
	}

	public String getAccount()
	{
		return(account);
	}
	
	public void setPassword(String word)
	{
		password = word;
	}
	
	public String getPassword()
	{
		return(password);
	}
	
	public void setSender(String name)
	{
		sender = name;
	}

	public String getSender()
	{
		return(sender);
	}
	
	public void setSubject(String text)
	{
		subject = text;
	}
	
	public String getSubject()
	{
		return(subject);
	}
	
	public void setMessage(String msg)
	{
		message = msg;
	}

	public String getMessage()
	{
		return(message);
	}
	
	public String connect()
	{
		//if(smssender == null)
		//{
		//	smssender = new RedrockSMS(this);			
		//}
		return(smssender.connect());
	}
	
	public void close()
	{
		smssender.close();
	}

	public String send()
	{
		return(smssender.send());
	}
	
	public String getResponse()
	{
		String response = connect();
		try
		{
			if(response == null)
			{
				response = send();
			}
		}
		catch(Exception e)
		{
			response = e.getMessage();
		}
		finally
		{
			close();
		}
		return(response);
	}	
	
	public static void main(String[] args)
	{
		SMSBean sms = new SMSBean(true);
		sms.setProvider(SMSBean.inspirus);
		//sms.setIP("http://gm-sms.com/api/");
		//sms.setNumber("0418888888");
      sms.setAccount("reactor");
      sms.setPassword("reactor1");
		//sms.setNumber("0408600708");
		//sms.setAccount("switchedon");
		//sms.setPassword("switchmeon");
		sms.setMessage("Testing");
		sms.setSender("9999999999");
		//sms.setSender("SOK");
		System.out.println(sms.getResponse());
	}

}
