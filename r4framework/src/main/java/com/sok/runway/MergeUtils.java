package com.sok.runway;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletContext;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;

import com.sok.framework.*;
import java.util.StringTokenizer;

public class MergeUtils
{
	public static final int CONTACT_RECORD = 1;
	public static final int COMPANY_RECORD = 2;
		
	public static int calculateScore(TableData contact)
	{
		return calculateScore(contact, CONTACT_RECORD);
	}

	public static int calculateScore(TableData data, int recordType)
	{
		switch (recordType)
		{
			case COMPANY_RECORD:
				return calculateCompanyScore(data);
			default: // deliberate fall-through
			case CONTACT_RECORD:
				return calculateContactScore(data);
		}
	}

	public static int calculateContactScore(TableData contact)
	{
		int score = 0;
		if (contact.getColumn("FirstName").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("LastName").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Email").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Title").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Company").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Position").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Street").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Street2").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("City").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("State").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Postcode").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Phone").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("HomeStreet").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("HomeStreet2").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("HomeCity").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("HomeState").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("HomePostcode").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("HomePhone").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Mobile").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Fax").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Interest").trim().length() != 0) {
			score++;
		}
		return score;
	}

	public static int calculateCompanyScore(TableData contact)
	{
		int score = 0;
		if (contact.getColumn("Company").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Street").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Street2").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("City").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("State").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Postcode").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("PostalStreet").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("PostalStreet2").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("PostalCity").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("PostalState").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("PostalPostcode").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Web").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Email").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Phone").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Phone2").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Fax").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Type").trim().length() != 0) {
			score++;
		}
		if (contact.getColumn("Industry").trim().length() != 0) {
			score++;
		}
		return score;
	}

	public static TableBean[] getMergeRecords(ServletContext servletContext, HttpServletRequest request, int recordType) throws Exception
	{
		SpecManager.setServletContext(servletContext);
		String recordIDName = null;
		String requestIDsParam = null;
		String recordViewName = null;
		switch (recordType)
		{
			case COMPANY_RECORD:
				recordIDName = "CompanyID";
				requestIDsParam = "companyIDs";
				recordViewName = "CompanyView";
				break;
			default: // Deliberate fall-through
			case CONTACT_RECORD:
				recordIDName = "ContactID";
				requestIDsParam = "contactIDs";
				recordViewName = "ContactDetailedListView";
				break;
		}
		String recordidlist = request.getParameter(requestIDsParam);
		while (recordidlist.endsWith(",0")) {
			recordidlist = recordidlist.substring(0,recordidlist.length()-2);
		}
		StringTokenizer toker = new StringTokenizer(recordidlist, ",");
		int mergeCount = toker.countTokens();
		TableBean [] beans = new TableBean[mergeCount];


		for (int ix = 0; toker.hasMoreTokens(); ix++) {
			beans[ix] = new TableBean();
			beans[ix].setJndiName(servletContext.getInitParameter("sqlJndiName"));
			beans[ix].setViewSpec(SpecManager.getViewSpec(recordViewName));
			ActionBean.setColumn(recordIDName, toker.nextToken(), beans[ix]);
			beans[ix].processRequest(request);
		}

		// Now sort the beans (according to their score):
		int score0 = calculateScore(beans[0], recordType);
		int score1 = calculateScore(beans[1], recordType);
		int score2 = -1;
		if (mergeCount == 3) {
			score2 = calculateScore(beans[2], recordType);
		}
		TableBean tmp;
		int tmpScore;
		if (score1 < score2) {
			tmp = beans[1];
			tmpScore = score1;
			beans[1] = beans[2];
			beans[2] = tmp;
			score1 = score2;
			score2 = tmpScore;
		}
		if (score0 < score1) {
			tmp = beans[0];
			tmpScore = score0;
			beans[0] = beans[1];
			beans[1] = tmp;
			score0 = score1;
			score1 = tmpScore;
			if (score1 < score2) {
				tmp = beans[1];
				tmpScore = score1;
				beans[1] = beans[2];
				beans[2] = tmp;
				score1 = score2;
				score2 = tmpScore;
			}
		}

		return beans;
	}

	/**
	 * Determines how many distinct non-empty values for a specified field (column) exist
	 * amongst an array of TableBeans. The result is either 0, 1, or 2, representing the
	 * following possible results:<BR>
	 * <ul>
	 * <li>0 - there are no non-empty values for any of the beans.
	 * <li>1 - there is one distinct, non-empty value shared between one or more of the beans.
	 * <li>2 - there are multiple distinct, non-empty values shared between the beans.
	 * </ul>
	 * Note that, for the third case, the function only determines that there are multiple
	 * values, not necessarily how many of them they are (which would require a more
	 * complicated algorithm, and simply ins't needed for the function's current intended use).
	 *
	 * @param beans an array of TableBeans whose values will be compared.
	 * @param field - the field within each TableBean to be checked.
	 * @return number of distinct, non-empty values.
	 */
	public static int getDistinctFieldCount(TableBean [] beans, String field, String secondField, String thirdField)
	{
		int count = 0;
		int len = beans.length;
		String thisVal = null;
		String lastVal = null;
		for (int ix = 0; ix < len; ix++) {
			if (secondField != null) {
				// Special case: compare join of first and second (and third, if present) fields:
				if (thirdField != null) {
					thisVal = (String) beans[ix].getColumn(field) + ' ' + (String) beans[ix].getColumn(secondField)  + ' ' + (String) beans[ix].getColumn(thirdField);
				} else {
					thisVal = (String) beans[ix].getColumn(field) + ' ' + (String) beans[ix].getColumn(secondField);
				}
			} else {
				thisVal = (String) beans[ix].getColumn(field);
			}
			if ((thisVal != null) && (thisVal.trim().length() != 0)) {
				if (!thisVal.equals(lastVal)) {
					count++;
				}
				lastVal = thisVal;
			}
		}
		// We only look for distinctness between the current value and the
		// previous non-empty value (if any). This means that counts beyond
		// 2 may not be recognised. Safest not to allow others to rely on
		// more than the fact that there are at least 2 values. This is all
		// that is needed at present.
		return count > 2 ? 2 : count;
	}

	public static TableBean mergeRecords(TableBean [] mergelist, HttpServletRequest request,
			ServletContext servletContext, UserBean currentuser, int recordType)
	{
		int master = Integer.parseInt(request.getParameter("master"));

		// Safest to always ensure closure of any previous connections,
		// and install a new connection at this point:
		mergelist[master].setCloseConnection(true);
		mergelist[master].close();
		Connection conn = (Connection) request.getAttribute("conn");
		boolean closeConn = false;
		if (conn == null) {
			mergelist[master].setJndiName(servletContext.getInitParameter("sqlJndiName"));
			mergelist[master].connect();
			conn = mergelist[master].getConnection();
			closeConn = true;
		}
		for (int ix = 0; ix < mergelist.length; ix++) {
			mergelist[ix].setJndiName(servletContext.getInitParameter("sqlJndiName"));
			mergelist[ix].setConnection(conn);
			mergelist[ix].setCloseConnection(false);
		}

		mergelist[master].setColumn("ModifiedDate","NOW");
		mergelist[master].setColumn("ModifiedBy",currentuser.getCurrentUserID());
		
		// This section is to remove formatting problems with dates
		/*TableSpec tSpec = mergelist[master].getTableSpec();
		for(int j=0; j<tSpec.getFieldsLength(); j++) {
			String fieldname = tSpec.getFieldName(j);
			int fieldtype = tSpec.getFieldType(j);
			String format = tSpec.getFormat(j);
			if (fieldtype == Types.TIMESTAMP && format == null) {
				mergelist[master].remove(fieldname);
			}
			//mergelist[master].remove("CreatedDate"); // Don't want to update this field, unnessesary and causes formatting problems.
		}*/
		// end section
		
		mergelist[master].processRequest(request);
		mergelist[master].setColumn("mySearchStatement",mergelist[master].getSearchStatement());
		
		// Need to add/associate all related records to beans[0].
		mergeLists(mergelist, master, request, servletContext, conn, recordType);

		// Need to merge profile questions where support available.
		String[] questionids = request.getParameterValues("QuestionID"); 
	
		//TODO: FIX ME - questionids.length>-1 is always true!
		if (questionids != null && questionids.length>-1) { 
			String answer = ActionBean._blank; 
		
			AnswerBean answerBean = new AnswerBean(); 
			answerBean.setConnection(conn); 
			answerBean.setCurrentUser(currentuser); 
			switch (recordType)
			{
				case COMPANY_RECORD:
					for (int i=0; i<questionids.length; i++) { 
						answerBean.setViewSpec("answers/CompanyAnswerView"); 
						MultiField mf = new MultiField(request.getParameterValues("Answer"+questionids[i]));
						answer = mf.toString();
						answerBean.saveCompanyAnswer(mergelist[master].getString("CompanyID"),questionids[i],answer);
					}	
					break; 
				case CONTACT_RECORD: 
					for (int i=0; i<questionids.length; i++) { 
						answerBean.setViewSpec("answers/ContactAnswerView");
						MultiField mf = new MultiField(request.getParameterValues("Answer"+questionids[i]));
						answer = mf.toString();
						answerBean.saveContactAnswer(mergelist[master].getString("ContactID"),questionids[i],answer); 
					}				
					break; 				
			} 
			
		} 
		
		// Need to delete the non-master beans & orphan Records
		String[] orphans = {}; 
		RowBean orphanAction = new RowBean(); 
		orphanAction.setConnection(conn); 
		orphanAction.setAction(ActionBean.DELETEALL);  

		for (int ix = 0; ix < mergelist.length; ix++) {
			if (ix != master) {
				
				switch (recordType)
				{
					case COMPANY_RECORD:
						orphans = new String[] {"CompanyStatus","answers/CompanyAnswers"}; 
						orphanAction.setColumn("CompanyID",mergelist[ix].getString("CompanyID")); 
						break; 
					case CONTACT_RECORD: 
						orphans = new String[] {"ContactStatus","answers/ContactAnswers"};
						orphanAction.setColumn("ContactID",mergelist[ix].getString("ContactID")); 
						break; 				
				} 
				for (int ixo =0; ixo<orphans.length; ixo++) { 
					orphanAction.setTableSpec(orphans[ixo]);
					if(mergelist[ix].getString(mergelist[ix].getTableSpec().getPrimaryKeyName()).length()>0) {
						orphanAction.doAction(); 
					}
				}
				
				mergelist[ix].setAction(ActionBean.DELETE);
				mergelist[ix].doAction();
			}
		}

		if (closeConn && (conn != null)) {
			try {
				conn.close();
			} catch (SQLException sqlex) {}
		}

		return mergelist[master];
	}

	private static void mergeLists(TableBean [] mergelist, int master, HttpServletRequest request,
			ServletContext servletContext, Connection conn, int recordType)
	{
		boolean skipDocMerge = "true".equals(InitServlet.getSystemParam("DisableDocumentMerge"));
		
		String recordIDName = null;
		String [] attachmentspecs = null;
		String [] recordIDfields = null;
		String [] recordSecondaryFields = null;
		switch (recordType)
		{
			case COMPANY_RECORD:
				recordIDName = "CompanyID";
				if(skipDocMerge) {
					attachmentspecs = new String[] {"ContactListView", "NoteView", "OpportunityView", "ContactCampaignView", "LinkeeCompanyListView", "LinkeeCompanyListView", "CompanyContactLinkView", "OpportunityCompanyListView", "OrderView", "OrderCompanyListView", "CompanyGroupView"};
					recordIDfields = new String[] {"CompanyID", "CompanyID", "CompanyID", "CompanyID", "LinkerID", "LinkeeID", "CompanyID", "CompanyID", "CompanyID", "CompanyID", "CompanyID"};
					recordSecondaryFields = new String[] {"Company", null, null, null, null, null, null,  null, null, null, null};
				} else {
					attachmentspecs = new String[] {"ContactListView", "NoteView", "OpportunityView", "LinkedDocumentView","ContactCampaignView", "LinkeeCompanyListView", "LinkeeCompanyListView", "CompanyContactLinkView", "OpportunityCompanyListView", "OrderView", "OrderCompanyListView", "CompanyGroupView"};
					recordIDfields = new String[] {"CompanyID", "CompanyID", "CompanyID", "CompanyID", "CompanyID", "LinkerID", "LinkeeID", "CompanyID", "CompanyID", "CompanyID", "CompanyID", "CompanyID"};
					recordSecondaryFields = new String[] {"Company", null, null, null, null, null, null, null, null, null, null, null};
				}
				
				
				break;
			default: // Deliberate fall-through
			case CONTACT_RECORD:
				recordIDName = "ContactID";
				
				if(skipDocMerge) {
					attachmentspecs = new String[] {"NoteView", "ResponseView", "ContactCampaignView", "LinkeeContactListView", "LinkerContactListView", "OrderView", "OrderContactListView", "OpportunityView", "OpportunityContactsListView", "AttendeeView", "CompanyContactLinkView", "UserView", "ContactProductView", "ContactGroupShortView"};
					recordIDfields = new String[] {"ContactID", "ContactID",    "ContactID",            "LinkerID",             "LinkeeID",              "ContactID", "ContactID",            "ContactID",       "ContactID",                   "ContactID",     "ContactID",              "ContactID", "ContactID", "ContactID"};
				} else {
					attachmentspecs = new String[] {"NoteView", "ResponseView", "ContactCampaignView", "LinkeeContactListView", "LinkerContactListView", "OrderView", "OrderContactListView", "OpportunityView", "LinkedDocumentView", "OpportunityContactsListView", "AttendeeView", "CompanyContactLinkView", "UserView", "ContactProductView", "ContactGroupShortView"};
					recordIDfields = new String[] {"ContactID", "ContactID",    "ContactID",            "LinkerID",             "LinkeeID",              "ContactID", "ContactID",            "ContactID",       "ContactID",          "ContactID",                   "ContactID",     "ContactID",              "ContactID", "ContactID", "ContactID"};
				}
				break;
		}
		SpecManager.setServletContext(servletContext);
		GenRow updaterBean = new GenRow();
		updaterBean.setRequest(request);
		
		RowSetBean recordFinder = new RowSetBean(); 
		recordFinder.setRequest(request); 
		
		String nonMasterIDs = ""; 
		
		/*
		 * 	WILL FIND THE RECORD ID'S OF THOSE WHO ARE NON-MASTER; IE WILL BE REMOVED
		 */
		for (int im = 0; im<mergelist.length; im++) {
			if(master!=im) { 
				if(nonMasterIDs.length()>0) { 
					nonMasterIDs += "+"; 
				}
				nonMasterIDs += mergelist[im].getColumn(recordIDName); 
			}
		}
		
		StringBuffer errors = new StringBuffer();
		for(int ix = 0; ix < attachmentspecs.length; ix++) {
			recordFinder.setViewSpec(attachmentspecs[ix]); 
			recordFinder.setColumn(recordIDfields[ix],nonMasterIDs); 
			recordFinder.generateSQLStatement(); 
			recordFinder.getResults(); 
			
			while (recordFinder.getNext()) { 
				updaterBean.clear(); 
				updaterBean.setTableSpec(recordFinder.getTableSpec());
				updaterBean.setParameter(recordFinder.getTableSpec().getPrimaryKeyName(),recordFinder.getData(recordFinder.getTableSpec().getPrimaryKeyName()));
				updaterBean.setParameter(recordIDfields[ix],mergelist[master].getColumn(recordIDName)); 
				if ((recordSecondaryFields != null) && (recordSecondaryFields[ix] != null)) {
					updaterBean.setParameter(recordSecondaryFields[ix], mergelist[master].getColumn(recordSecondaryFields[ix]));
				}
				updaterBean.doAction("update");
				
				if (updaterBean.getError().length() > 0) {
						errors.append(updaterBean.getError());
						errors.append("\r\n\r\n");
				}
			}
			recordFinder.clear();
		} 
		recordFinder.close(); 
		updaterBean.clear(); 
		updaterBean.close(); 
		
		if (errors.length() > 0) {
			java.util.Properties sysprops = InitServlet.getSystemParams();
			String defaultFrom = sysprops.getProperty("DefaultEmail");
			if (defaultFrom == null) {
				defaultFrom = "Runway<info@switched-on.com.au>";
			}
			MailerBean mailer = new MailerBean();
				mailer.setFrom(defaultFrom);
				mailer.setTo("support@switched-on.com.au");
				mailer.setCc("jong@switched-on.com.au");
				mailer.setHost(servletContext.getInitParameter("SMTPHost"));
				mailer.setUsername(servletContext.getInitParameter("SMTPUsername"));
				mailer.setPassword(servletContext.getInitParameter("SMTPPassword"));
				mailer.setTextbody(errors.toString());
				mailer.setSubject("Merge error in " + servletContext.getServletContextName() + " Runway");
				mailer.getResponse();
		}
	}

	private static void clearDateFields(TableData data)
	{
		int len = data.getTableSpec().getFieldsLength();
		for (int ix = 0; ix < len; ix++) {
			if (data.getTableSpec().getFieldName(ix).endsWith("Date")) {
				data.setColumn(data.getTableSpec().getFieldName(ix), null);
			}
		}
	}
}
