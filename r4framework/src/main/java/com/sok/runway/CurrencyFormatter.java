package com.sok.runway;

import java.text.NumberFormat;

public class CurrencyFormatter
{

    public CurrencyFormatter()
    {
    }
    
    private static NumberFormat nf = NumberFormat.getCurrencyInstance();

    public static String format(String s)
    {
    	try {
    		return nf.format(Double.parseDouble(s));
    	} catch (Exception e) {
    		return s;
    	}
    	/* this code causes recursiveness.
        String s1 = s;
        double f = Double.parseDouble(s1);
        int i = s1.indexOf(".");
        if(i < 0)
            return s1 + ".00";
        if(i == 0)
            return format("0" + s1);
        if(i == s1.length() - 1)
            return s1 + "00";
        if(i == s1.length() - 2)
            return s1 + "0";
        if(i == s1.length() - 3)
        {
            return s1;
        } else
        {
            double f1 = f * 100F;
            long j = Math.round(f1);
            f1 = (double)j / 100F;
            String s2 = String.valueOf(f1);
            return format(s2);
        }
        */
    }
}