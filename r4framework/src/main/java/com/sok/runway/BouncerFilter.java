package com.sok.runway;
import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.lang.StringUtils;

import java.io.*;
import com.sok.framework.*;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.offline.ThreadManager;

import java.util.Calendar;

public class BouncerFilter implements Filter
{
	static String jsp = "extendededit.jsp";
	static String logout = "logout";
	String loginpage = "/runway.login?-action=expired";
	//String loginpage = "/login.jsp?-action=expired";
	String homepage = "/runway/home.view";
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws ServletException, IOException
	{
		HttpServletRequest httprequest = (HttpServletRequest)request;
		URLFilter.exceptionLogger(this, httprequest, null);
		if(!response.isCommitted())
		{
			HttpSession session = httprequest.getSession(false);
			if(session!=null)
			{ 
				//((HttpServletResponse)response).setHeader("Set-Cookie", "JSESSIONID=" +  session.getId() + "; Path=/; HttpOnly; Secure;");
				UserBean currentuser = (UserBean)session.getAttribute(SessionKeys.CURRENT_USER);
				if(currentuser!=null && currentuser.getCurrentUserLevel()!=null && !currentuser.getCurrentUserLevel().equals("logout"))				{
					String requesturi = httprequest.getRequestURI();
					//if(requesturi.indexOf(jsp)>0 || requesturi.indexOf(logout)>0)
					if (currentuser.isPortalLogin()) {
						bounce(request ,response);
					}
					if(requesturi.indexOf(logout)>0)
					{
						LockManager.unlockAll(currentuser);
					}
					if (requesturi.indexOf("/crm/admin/") != -1 && !currentuser.getCanAdmin()) {
					   bounceHome(request ,response, currentuser);
					}
					else if (requesturi.indexOf("/runway/admin.") != -1 && !currentuser.getCanAdmin()) {
					   bounceHome(request ,response, currentuser);
					}
					else if (requesturi.indexOf("/crm/cms/") != -1 && !currentuser.getCanUseCMS()) {
					   bounceHome(request ,response, currentuser);
					}
					else if (requesturi.indexOf("/runway/cms.") != -1 && !currentuser.getCanUseCMS()) {
					   bounceHome(request ,response, currentuser);
					} else if (("API User".equals(currentuser.getString("LicenceType")) || "LaL User".equals(currentuser.getString("LicenceType"))) && 
							requesturi.indexOf("/api-docs") == -1 && requesturi.indexOf("/api/2/") == -1 && requesturi.indexOf("/oauth") == -1) {
						if (requesturi.indexOf("crm/home.jsp") != -1 && httprequest.getParameter("-target") != null && httprequest.getParameter("-target").indexOf("%2Foauth") != -1) {
							   doRequest(httprequest, response, chain, currentuser);
						} else {
							   bounceHome(request ,response, currentuser);
						}
					}
					else { 
					   doRequest(httprequest, response, chain, currentuser);
					} 
				}
				else if (checkAccessKey(httprequest)) { 
					doRequest(httprequest, response, chain, null);
				}
				else
				{
					bounce(request ,response);
				}
			}
			else if (checkAccessKey(httprequest)) { 
				doRequest(httprequest, response, chain, null);
			}
			else
			{
				bounce(request, response);
			}
		}
		else
		{
			chain.doFilter(request,response);
		}
	}
	
	/**
	 * Check if an access key exists for the given path. If it does, forward the request and optionally create an expiry date for the key.
	 * @param HttpServletRequest for a path that has already been denied access to Runway.
	 * @return boolean - true if access exists
	 */
	private boolean checkAccessKey(HttpServletRequest request) {
		if(request.getParameter("-accessKey")==null || request.getParameter("-accessKey").length()==0) {
			return false;
		}
		final GenRow access = new GenRow(); 
		access.setViewSpec("AccessKeyView"); 
		access.setConnection(ActionBean.getConnection(request));
		access.setParameter("AccessKeyID", request.getParameter("-accessKey"));
		
		access.setParameter("AccessPath", request.getRequestURI().substring(request.getContextPath().length()));
		access.setParameter("ExpiryDate", "<NOW+NULL");
		access.doAction(GenerationKeys.SELECT);
		
		if(access.isSuccessful()) {
			request.setAttribute("AccessData", access.getData("AccessData"));
			if(access.getDate("ExpiryDate")==null) {
				access.setConnection((java.sql.Connection)null);
				ThreadManager.startThread(new Thread(new StringBuilder(20).append("BouncerFilter-AccessKeyUpdateThread-").append(ThreadManager.getNextThreadId()).toString()) { 
					public void run() { 
							Calendar c = Calendar.getInstance();
							c.add(Calendar.HOUR, access.getInt("ExpiryHours"));
							
							ActionBean.connect(access);
							access.setParameter("ExpiryDate", c.getTime());
							access.doAction(GenerationKeys.UPDATE);
							c = null;
						}
				}); 
			}
			return true;
		}
		
		return false;
	}

	protected void doRequest(HttpServletRequest request, ServletResponse response, FilterChain chain, UserBean currentuser)throws ServletException, IOException
	{
		try
		{
			if(currentuser!=null)
			{
				currentuser.setRequest(request);
			}
			chain.doFilter(request,response);
		}
		finally
		{
			if(currentuser!=null)
			{
				currentuser.close();
			}
		}
	}

	public void bounce(ServletRequest request, ServletResponse response)throws IOException
	{
		StringBuffer bounceurl = new StringBuffer();
		bounceurl.append(((HttpServletRequest)request).getContextPath());
		bounceurl.append(loginpage);

		HttpServletRequest httprequest = (HttpServletRequest)request;
		String requesturl = httprequest.getRequestURL().toString();
		String query = httprequest.getQueryString();
		if(requesturl.indexOf("/crm/") > -1 || requesturl.indexOf("/oauth/") > -1 || requesturl.indexOf("/api-docs/") > -1 || requesturl.indexOf("/runway/") > -1 || (query != null && query.indexOf("-loginaction=true") > -1 )) {
			bounceurl.append("&-target=");
			bounceurl.append(StringUtil.urlEncode(requesturl));
			if(query!=null){
				bounceurl.append(StringUtil.urlEncode("?"));
				bounceurl.append(StringUtil.urlEncode(query));
			}
		}
		((HttpServletResponse)response).sendRedirect(bounceurl.toString());
	}
	
	public void bounceHome(ServletRequest request, ServletResponse response, UserBean currentuser)throws IOException
	{
		StringBuffer bounceurl = new StringBuffer();
		bounceurl.append(((HttpServletRequest)request).getContextPath());
		if (currentuser.getString("HomePage").length() > 0) {
		   bounceurl.append(currentuser.getString("HomePage"));
		}
		else  {
		   bounceurl.append(homepage);
		}
		
		//HttpServletRequest httprequest = (HttpServletRequest)request;
      
		((HttpServletResponse)response).sendRedirect(bounceurl.toString());
	}
	
	public void init(FilterConfig config)throws ServletException
	{
		String page = config.getInitParameter("loginPage");
		if(page!=null && page.length()!=0)
		{
			loginpage = page;
		}
	}
	
	public void destroy()
	{
		
	}
}
