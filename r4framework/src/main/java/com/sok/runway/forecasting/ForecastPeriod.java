/**
 * 
 */
package com.sok.runway.forecasting;

import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.RowBean;

/**
 * @author dion
 *
 */
public class ForecastPeriod {
	
	private final int						ONE_DAY = 24 * 60 * 60 * 1000;

	private HttpServletRequest				request = null;
	private boolean							hasChanged = false;
	private String							action = ActionBean.UPDATE;
	
	// data access members
	private String							ID = "";
	private String							Forecast = "";
	private double							Percentage = 0.0;
	private double							Value = 0.0;
	private boolean							ValueSet = false;
	private double							Actual = 0.0;
	private int								PeriodDays = 0;
	private Date							StartDate = null;
	private Date							EndDate = null;

	private Vector<ForecastUserPeriod>			periods 	  	= new Vector<ForecastUserPeriod>();
	private HashMap<String,ForecastUserPeriod>	quickPeriods 	= new HashMap<String,ForecastUserPeriod>();
	private HashMap<String,ForecastUserPeriod>	quickUsers 		= new HashMap<String,ForecastUserPeriod>();
	
	private final SimpleDateFormat			sdf = new SimpleDateFormat("dd/MM/yyyy");

	/**
	 * 
	 */
	public ForecastPeriod(HttpServletRequest request, GenRow row) {
		this.request = request;
		
		ID = row.getString("ForecastPeriodID");
		Forecast = row.getString("ForecastID");
		Percentage = row.getDouble("Percentage");
		Value = row.getDouble("Value");
		ValueSet = "Y".equals(row.getString("ValueSet"));
		Actual = row.getDouble("Actual");
		PeriodDays = row.getInt("PeriodDays");
		
		// these need to be in try catch as we use a buggy MySQL library
		try {
			StartDate = row.getDate("StartDate");
		} catch (Exception e) {
			StartDate = new Date();
		}
		
		try {
			EndDate = row.getDate("EndDate");
		} catch (Exception e) {
			EndDate = new Date();
		}
		
		if (EndDate.after((new Date()))) {
			ValueSet = true;
		}
	}

	public ForecastPeriod(HttpServletRequest request, String forecastID, Date startDate, Date endDate) {
		this.request = request;
		
		ID = KeyMaker.generate();
		Forecast = forecastID;
		StartDate = startDate;
		EndDate = endDate;
		
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		
		start.setTime(startDate);
		end.setTime(endDate);
		
		int years = end.get(Calendar.YEAR) - start.get(Calendar.YEAR);
		PeriodDays = end.get(Calendar.DAY_OF_YEAR) - start.get(Calendar.DAY_OF_YEAR) + (years * 365) + 1;
		
		hasChanged = true;
		action = ActionBean.INSERT;
		
		//if (EndDate.after((new Date()))) {
		//	ValueSet = true;
		//}
		
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		hasChanged = true;
		ID = iD;
	}

	public String getForecastID() {
		return Forecast;
	}

	public void setForecastID(String forecastID) {
		hasChanged = true;
		Forecast = forecastID;
	}

	public double getPercentage() {
		return Percentage;
	}

	public void setPercentage(double percentage) {
		hasChanged = true;
		Percentage = percentage;
		if (Percentage < 0) Percentage = 0;
	}

	public void setPercentage(String value) {
		hasChanged = true;
		try {
			Percentage = Double.parseDouble(value);
			if (Percentage < 0) Percentage = 0;
		} catch (Exception e) {
			Percentage = 0;
			setValueSet(false);
		}
	}

	public String getPercentageString() {
		Format format = NumberFormat.getPercentInstance();
		
		return format.format(Percentage);
	}

	public double getValue() {
		return Value;
	}

	public void setValue(double value) {
		hasChanged = true;
		Value = value;
		if (Value < 0) Value = 0;
	}

	public void setValue(String value) {
		hasChanged = true;
		try {
			Value = Double.parseDouble(value);
			if (Value < 0) Value = 0;
		} catch (Exception e) {
			Value = 0;
			setValueSet(false);
		}
	}

	public double getActual() {
		return Actual;
	}

	public void setActual(double actual) {
		hasChanged = true;
		Actual = actual;
	}

	public double getActualPercentage() {
		if (Value == 0) return 0;
		return Actual / Value;
	}

	public String getActualPercentageString() {
		Format format = NumberFormat.getPercentInstance();
		
		return format.format(getActualPercentage());
	}

	public int getPeriodDays() {
		return PeriodDays;
	}

	public void setPeriodDays(int periodDays) {
		hasChanged = true;
		PeriodDays = periodDays;
	}

	public Date getStartDate() {
		return StartDate;
	}

	public String getStartDateString() {
		return sdf.format(StartDate);
	}
	
	public void setStartDate(Date startDate) {
		hasChanged = true;
		StartDate = startDate;
	}

	public Date getEndDate() {
		return EndDate;
	}

	public String getEndDateString() {
		return sdf.format(EndDate);
	}
	
	public void setEndDate(Date endDate) {
		hasChanged = true;
		EndDate = endDate;
	}

	public boolean hasChanged() {
		return hasChanged;
	}

	public void addForecastUserPeriod(ForecastUserPeriod forecastUserPeriod) {
		periods.add(forecastUserPeriod);
		quickPeriods.put(forecastUserPeriod.getID(), forecastUserPeriod);
		quickUsers.put(forecastUserPeriod.getForecastUserID(), forecastUserPeriod);
	}
	
	public ForecastUserPeriod getForecastUserPeriod(String forecastUserPeriodID) {
		return quickPeriods.get(forecastUserPeriodID);
	}

	public ForecastUserPeriod getForecastUserPeriodUser(String forecastUserID) {
		return quickUsers.get(forecastUserID);
	}
	
	public Vector<ForecastUserPeriod> getForecastUserPeriods() {
		return periods;
	}
	
	public boolean isValueSet() {
		return ValueSet;
	}

	public boolean isCompleated() {
		return EndDate.before((new Date()));
	}
	
	public void setValueSet(boolean valueSet) {
		ValueSet = valueSet;
	}

	public void save(HttpServletRequest request) {
		if (hasChanged || ActionBean.INSERT.equals(action)) {
			GenRow row = new GenRow();
			row.setTableSpec("forecasting/GroupForecastPeriod");
			row.setRequest(request);
			row.setColumn("ForecastPeriodID",ID);
			row.setColumn("ForecastID",Forecast);
			row.setParameter("Value",(Value > 0? Value : 0));
			row.setParameter("ValueSet",(ValueSet? "Y" : "N"));
			row.setParameter("Percentage",(Percentage > 0? Percentage : 0));
			row.setParameter("Actual",(Actual > 0? Actual : 0));
			row.setParameter("PeriodDays",PeriodDays);
			row.setParameter("StartDate",StartDate);
			row.setParameter("EndDate",EndDate);
			row.doAction(action);
		}
		
		for (int f = 0; f < periods.size(); ++f) {
			ForecastUserPeriod fup = periods.get(f);
			fup.save(request);
		}
		
		hasChanged = false;
		action = ActionBean.UPDATE;
	}
}
