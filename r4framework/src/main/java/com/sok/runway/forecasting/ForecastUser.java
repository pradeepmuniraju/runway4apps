/**
 * 
 */
package com.sok.runway.forecasting;

import java.text.Format;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.RowBean;
import com.sok.runway.UserBean;

/**
 * @author dion
 *
 */
public class ForecastUser {

	private HttpServletRequest				request = null;
	private boolean							hasChanged = false;
	private String							action = ActionBean.UPDATE;
	
	// data access members
	private String							ID = "";
	private String							Forecast = "";
	private String							User = "";
	private String							Name = "";
	private double							Percentage = 0.0;
	private double							Value = 0.0;
	private boolean							ValueSet = false;
	private double							Actual = 0.0;
	
	private Vector<ForecastUserPeriod>			periods 	  	= new Vector<ForecastUserPeriod>();
	private HashMap<String,ForecastUserPeriod>	quickPeriods 	= new HashMap<String,ForecastUserPeriod>();
	private HashMap<String,ForecastUserPeriod>	quickUsers 		= new HashMap<String,ForecastUserPeriod>();
	/**
	 * 
	 */
	public ForecastUser(HttpServletRequest request, GenRow row) {
		this.request = request;
		
		ID = row.getString("UserForecastID");
		Forecast = row.getString("ForecastID");
		User = row.getString("UserID");
		Name =	(row.getString("Title") + " " + row.getString("FirstName") + " " + row.getString("LastName")).trim();
		
		Percentage = row.getDouble("Percentage");
		Value = row.getDouble("Value");
		ValueSet = "Y".equals(row.getString("ValueSet"));
		Actual = row.getDouble("Actual");
	}

	public ForecastUser(HttpServletRequest request, String forecastID, String userID, String name) {
		this.request = request;
		
		ID = KeyMaker.generate();
		Forecast = forecastID;
		User = userID;
		Name = name;
		
		hasChanged = true;
		action = ActionBean.INSERT;
	}

	public boolean hasChanged() {
		return hasChanged;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		hasChanged = true;
		ID = iD;
	}

	public String getForecastID() {
		return Forecast;
	}

	public void setForecastID(String forecast) {
		hasChanged = true;
		Forecast = forecast;
	}

	public String getUserID() {
		return User;
	}

	public void setUserID(String user) {
		hasChanged = true;
		User = user;
	}

	public double getPercentage() {
		return Percentage;
	}

	public String getPercentageString() {
		Format format = NumberFormat.getPercentInstance();
		
		return format.format(Percentage);
	}

	public void setPercentage(double percentage) {
		hasChanged = true;
		Percentage = percentage;
		if (Percentage < 0) Percentage = 0;
	}

	public void setPercentage(String value) {
		hasChanged = true;
		try {
			Percentage = Double.parseDouble(value);
			if (Percentage < 0) Percentage = 0;
		} catch (Exception e) {
			Percentage = 0;
			setValueSet(false);
		}
	}

	public double getValue() {
		return Value;
	}

	public void setValue(double value) {
		hasChanged = true;
		Value = value;
	}

	public void setValue(String value) {
		hasChanged = true;
		try {
			Value = Double.parseDouble(value);
		} catch (Exception e) {
			Value = 0;
			setValueSet(false);
		}
	}

	public void addForecastUserPeriod(ForecastUserPeriod forecastUserPeriod) {
		periods.add(forecastUserPeriod);
		quickUsers.put(forecastUserPeriod.getID(), forecastUserPeriod);
		quickPeriods.put(forecastUserPeriod.getForecastPeriodID(), forecastUserPeriod);
	}

	public ForecastUserPeriod getForecastUserPeriod(String forecastUserPeriodID) {
		return quickUsers.get(forecastUserPeriodID);
	}

	public ForecastUserPeriod getForecastUserPeriodPeriod(String forecastPeriodID) {
		return quickPeriods.get(forecastPeriodID);
	}
	
	public Vector<ForecastUserPeriod> getForecastUserPeriods() {
		return periods;
	}
	
	public boolean isValueSet() {
		return ValueSet;
	}

	public void setValueSet(boolean valueSet) {
		ValueSet = valueSet;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public double getActualPercentage() {
		if (Value == 0) return 0;
		return Actual / Value;
	}

	public String getActualPercentageString() {
		Format format = NumberFormat.getPercentInstance();
		
		return format.format(getActualPercentage());
	}

	public void save(HttpServletRequest request) {
		if (hasChanged || ActionBean.INSERT.equals(action)) {
			GenRow row = new GenRow();
			row.setTableSpec("forecasting/UserForecast");
			row.setRequest(request);
			row.setColumn("UserForecastID",ID);
			row.setColumn("ForecastID",Forecast);
			row.setColumn("UserID",User);
			row.setParameter("Value",(Value > 0? Value : 0));
			row.setParameter("Actual",(Actual > 0? Actual : 0));
			row.setParameter("Percentage",(Percentage > 0? Percentage : 0));
			row.setParameter("ValueSet",(ValueSet? "Y" : "N"));
			row.doAction(action);
		}
		hasChanged = false;
		action = ActionBean.UPDATE;
	}

	public double getActual() {
		return Actual;
	}

	public void calculateActual() {
		double actual = 0;
		for (int f = 0; f < periods.size(); ++f) {
			ForecastUserPeriod fup = periods.get(f);
			actual += fup.getActual();
		}
		Actual = actual;
		hasChanged = true;
	}
}
