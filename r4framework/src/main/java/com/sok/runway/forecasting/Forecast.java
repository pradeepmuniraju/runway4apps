/**
 * 
 */
package com.sok.runway.forecasting;

import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;

/**
 * @author dion
 *
 */
public class Forecast {

	public static final String				FORECAST_TYPE_CONTACT_NEW    = "Contact New";
	public static final String				FORECAST_TYPE_CONTACT_STATUS = "Contact Status";
	public static final String				FORECAST_TYPE_CONTACT_CHANGE = "Contact Change";
	
	public static final String				FORECAST_TYPE_COMPANY_NEW    = "Company New";
	public static final String				FORECAST_TYPE_COMPANY_STATUS = "Company Status";
	public static final String				FORECAST_TYPE_COMPANY_CHANGE = "Company Change";
	
	public static final String				FORECAST_TYPE_QUOTE_NEW    = "Quote New";
	public static final String				FORECAST_TYPE_QUOTE_STATUS = "Quote Status";
	public static final String				FORECAST_TYPE_QUOTE_CHANGE = "Quote Change";

	public static final String				FORECAST_PERIOD_WEEKLY      = "Weekly";
	public static final String				FORECAST_PERIOD_FORTNIGHTLY = "Fortnightly";
	public static final String				FORECAST_PERIOD_MONTHLY     = "Monthly";
	
	public static final String[]			forecastTypes  = {FORECAST_TYPE_CONTACT_NEW, FORECAST_TYPE_CONTACT_STATUS, FORECAST_TYPE_COMPANY_NEW, FORECAST_TYPE_COMPANY_STATUS, FORECAST_TYPE_QUOTE_NEW, FORECAST_TYPE_QUOTE_STATUS};

	public static final String[]			forecastPeriod = {FORECAST_PERIOD_WEEKLY, FORECAST_PERIOD_FORTNIGHTLY, FORECAST_PERIOD_MONTHLY};

	private HttpServletRequest				request = null;
	private boolean							hasChanged = false;
	private String							action = ActionBean.UPDATE;
	
	// data access members
	private String							ID = "";
	private String							GroupID = "";
	private String							Name = "";
	private String							Description = "";
	private String							Type = "";
	private String							Condition1 = "";
	private String							Condition2 = "";
	private double							Value = 0.0;
	private Date							StartDate = null;
	private Date							EndDate = null;
	private String							Period = "Monthly";
	private int								PeriodStart = 0;
	
	private String							cond1String = null;
	private String							cond2String = null;
	
	private int								periodsCompleated = 0;
	private int								usersOnTarget = 0;
	
	private int								totalDays = 0;
	
	private Vector<ForecastPeriod>			periods 	  	= new Vector<ForecastPeriod>();
	private HashMap<String,ForecastPeriod>	quickPeriods 	= new HashMap<String,ForecastPeriod>();
	
	private Vector<ForecastUser>			users 	  		= new Vector<ForecastUser>();
	private HashMap<String,ForecastUser>	quickUser 		= new HashMap<String,ForecastUser>();
	private HashMap<String,ForecastUser>	quickForecast 	= new HashMap<String,ForecastUser>();
	
	private final SimpleDateFormat			sdf = new SimpleDateFormat("dd/MM/yyyy");
	private final SimpleDateFormat			obcd = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * 
	 */
	public Forecast(HttpServletRequest request, GenRow row) {
		this.request = request;
		
		ID = row.getString("ForecastID");
		GroupID = row.getString("GroupID");
		Name = row.getString("Name");
		Description = row.getString("Description");
		Type = row.getString("Type");
		Condition1 = row.getString("Condition1");
		Condition2 = row.getString("Condition2");
		Value = row.getDouble("Value");
		Period = row.getString("Period");
		PeriodStart = row.getInt("PeriodStart");
		
		// these need to be in try catch as we use a buggy MySQL library
		try {
			StartDate = row.getDate("StartDate");
		} catch (Exception e) {
			StartDate = new Date();
		}
		
		try {
			EndDate = row.getDate("EndDate");
		} catch (Exception e) {
			EndDate = new Date();
		}
		
		loadUsers(ID);
		loadPeriods(ID);
		
		loadUsersPeriods();
	}
	
	/**
	 * 
	 */
	public Forecast(HttpServletRequest request, String groupID) {
		this.request = request;
		
		ID = KeyMaker.generate();
		GroupID = groupID;
		Name = "";
		Description = "";
		Type = FORECAST_TYPE_CONTACT_NEW;
		Condition1 = "";
		Condition2 = "";
		Value = 0;
		Period = FORECAST_PERIOD_MONTHLY;
		PeriodStart = 0;
		
		// these need to be in try catch as we use a buggy MySQL library
		StartDate = new Date();
		EndDate = new Date();
		
		hasChanged = true;
		action = ActionBean.INSERT;
	}
	
	protected void loadUsers(String forecastID) {
		GenRow row = new GenRow();
		row.setViewSpec("forecasting/UserForecastView");
		row.setRequest(request);
		//row.setParameter("-select", "*");
		row.setParameter("ForecastID", forecastID);
		row.sortBy("FirstName", 0);
		row.sortBy("LastName", 1);
		row.doAction(ActionBean.SEARCH);
		row.getResults();
		while (row.getNext()) {
			ForecastUser fu = new ForecastUser(request, row);
			users.add(fu);
			quickUser.put(fu.getUserID(), fu);
			quickForecast.put(fu.getID(), fu);
		}
		row.close();
	}

	protected void loadPeriods(String forecastID) {
		GenRow row = new GenRow();
		row.setTableSpec("forecasting/GroupForecastPeriod");
		row.setRequest(request);
		row.setParameter("-select", "*");
		row.setParameter("ForecastID", forecastID);
		row.sortBy("StartDate", 0);
		row.sortBy("EndDate", 1);
		row.doAction(ActionBean.SEARCH);
		row.getResults();
		
		periodsCompleated = 0;
		
		while (row.getNext()) {
			ForecastPeriod fp = new ForecastPeriod(request, row);
			periods.add(fp);
			quickPeriods.put(fp.getID(), fp);
			
			if (fp.isCompleated()) ++periodsCompleated;
			
			totalDays += fp.getPeriodDays();
		}
		row.close();
	}
	
	/**
	 * This will match the data to the User and Period, must be used last
	 * 
	 */
	protected void loadUsersPeriods() {
		for (int index = 0; index < periods.size(); ++index) {
			ForecastPeriod fp = periods.get(index);
			// generate the ForecastUserPeriod so we know they all have them
			// this is important as we need it for the graph and to stop accumalation
			for (int u = 0; u < users.size(); ++u) {
				ForecastUser fu = users.get(u);
				if (fp.getForecastUserPeriodUser(fu.getID()) == null) {
					ForecastUserPeriod fup = new ForecastUserPeriod(request, fp, fu);
				}
			}

			GenRow row = new GenRow();
			row.setTableSpec("forecasting/UserForecastPeriod");
			row.setRequest(request);
			row.setParameter("-select", "*");
			row.setParameter("ForecastPeriodID", fp.getID());
			row.doAction(ActionBean.SEARCH);
			row.getResults();
			while (row.getNext()) {
				ForecastUser fu = quickForecast.get(row.getString("UserForecastID"));
				if (fu != null) {
					ForecastUserPeriod fup = fp.getForecastUserPeriodUser(fu.getID());
					if (fup != null) {
						fup.setActual(row.getDouble("Actual"));
					} else {
						fup = new ForecastUserPeriod(request, fp, fu);
					}
				}
			}
			row.close();
		}
	}
	
	public void save(HttpServletRequest request) {
		if (hasChanged || ActionBean.INSERT.equals(action)) {
			UserBean currentuser = (UserBean) request.getAttribute("currentuser");
			GenRow row = new GenRow();
			row.setTableSpec("forecasting/GroupForecast");
			row.setRequest(request);
			row.setColumn("ForecastID",ID);
			row.setColumn("GroupID",GroupID);
			row.setColumn("Name",Name);
			row.setColumn("Description",Description);
			row.setColumn("Type",Type);
			row.setColumn("Condition1",Condition1);
			row.setColumn("Condition2",Condition2);
			row.setParameter("Value",Value);
			row.setParameter("Actual",getActual());
			row.setParameter("Period",Period);
			row.setParameter("PeriodStart",PeriodStart);
			row.setParameter("StartDate",StartDate);
			row.setParameter("EndDate",EndDate);
			if (currentuser != null) {
				if (ActionBean.INSERT.equals(action)) {
					row.setColumn("CreatedDate", "NOW");
					row.setColumn("CreatedBy", currentuser.getUserID());
				}
				row.setColumn("ModifiedDate", "NOW");
				row.setColumn("ModifiedBy", currentuser.getUserID());
			}
			row.doAction(action);
		}
		
		for (int u = 0; u < users.size(); ++u) {
			ForecastUser fu = users.get(u);
			fu.save(request);
		}
		
		for (int p = 0; p < periods.size(); ++p) {
			ForecastPeriod fp = periods.get(p);
			fp.save(request);
		}
		
		hasChanged = false;
		action = ActionBean.UPDATE;
	}
	
	protected String getConditionStatus(String statusID) {
		GenRow row = new GenRow();
		row.setTableSpec("StatusList");
		row.setRequest(request);
		row.setParameter("-select", "Status");
		row.setParameter("StatusID", statusID);
		row.doAction(GenerationKeys.SELECTFIRST);
		
		return row.getString("Status");
	}
	
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		hasChanged = true;
		ID = iD;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		hasChanged = true;
		Name = name;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		hasChanged = true;
		Type = type;
	}

	public String getCondition1() {
		return Condition1;
	}
	
	public String getCondition1String() {
		// if there is no value return a blank string
		if (Condition1 == null || Condition1.length() == 0) return "";
		
		if (hasChanged || cond1String == null) {
			cond1String = getConditionStatus(Condition1);
		}
		
		// if there is no status return a blank string
		return (cond1String == null)? "" : cond1String;
	}

	public void setCondition1(String condition1) {
		hasChanged = true;
		if (!Condition1.equals(condition1)) cond1String = getConditionStatus(condition1);
		Condition1 = condition1;
	}

	public String getCondition2() {
		return Condition2;
	}

	public String getCondition2String() {
		// if there is no value return a blank string
		if (Condition2 == null || Condition2.length() == 0) return "";
		
		if (hasChanged || cond2String == null) {
			cond2String = getConditionStatus(Condition2);
		}
		
		// if there is no status return a blank string
		return (cond2String == null)? "" : cond2String;
	}

	public void setCondition2(String condition2) {
		hasChanged = true;
		if (!Condition2.equals(condition2)) cond2String = getConditionStatus(condition2);
		Condition2 = condition2;
	}

	public double getValue() {
		return Value;
	}
	
	public String getValueString() {
		Format format = null;
		if (FORECAST_TYPE_QUOTE_NEW.equals(Type) || FORECAST_TYPE_QUOTE_STATUS.equals(Type) || FORECAST_TYPE_QUOTE_CHANGE.equals(Type)) {
			format = NumberFormat.getCurrencyInstance();
		} else {
			format = NumberFormat.getIntegerInstance();
		}
		
		return format.format(Value);
	}

	public void setValue(double value) {
		hasChanged = true;
		Value = value;
	}

	public void setValue(String value) {
		hasChanged = true;
		try {
			Value = Double.parseDouble(value);
		} catch (Exception e) {
			Value = 0;
		}
	}

	public double getActual() {
		double actual = 0;
		for (int p = 0; p < periods.size(); ++p) {
			ForecastPeriod fp = periods.get(p);
			actual += fp.getActual();
		}
		return actual;
	}

	public String getActualString() {
		Format format = null;
		if (FORECAST_TYPE_QUOTE_NEW.equals(Type) || FORECAST_TYPE_QUOTE_STATUS.equals(Type) || FORECAST_TYPE_QUOTE_CHANGE.equals(Type)) {
			format = NumberFormat.getCurrencyInstance();
		} else {
			format = NumberFormat.getIntegerInstance();
		}
		
		return format.format(getActual());
	}

	public double getActualPercentage() {
		if (Value == 0) return 0;
		return getActual() / Value;
	}

	public String getActualPercentageString() {
		Format format = NumberFormat.getPercentInstance();
		
		return format.format(getActualPercentage());
	}

	public Date getStartDate() {
		return StartDate;
	}

	public String getStartDateString() {
		return sdf.format(StartDate);
	}
	
	public void setStartDate(Date startDate) {
		hasChanged = true;
		StartDate = startDate;
	}

	public void setStartDate(String startDate) {
		hasChanged = true;
		try {
			Calendar calStart = Calendar.getInstance();
			calStart.setTime(sdf.parse(startDate));
			calStart.set(Calendar.HOUR_OF_DAY, 0);
			calStart.set(Calendar.MINUTE, 0);
			calStart.set(Calendar.SECOND, 0);
			StartDate = calStart.getTime();
		} catch (Exception e) {
			StartDate = new Date();
		}
	}
	
	public Date getEndDate() {
		return EndDate;
	}

	public String getEndDateString() {
		return sdf.format(EndDate);
	}
	
	public void setEndDate(Date endDate) {
		hasChanged = true;
		this.EndDate = endDate;
	}

	public void setEndDate(String endDate) {
		hasChanged = true;
		try {
			Calendar calEnd = Calendar.getInstance();
			calEnd.setTime(sdf.parse(endDate));
			calEnd.set(Calendar.HOUR_OF_DAY, 23);
			calEnd.set(Calendar.MINUTE, 59);
			calEnd.set(Calendar.SECOND, 59);
			EndDate = calEnd.getTime();
		} catch (Exception e) {
			EndDate = new Date();
		}
	}

	public String getPeriod() {
		return Period;
	}

	public void setPeriod(String period) {
		hasChanged = true;
		Period = period;
	}

	public int getPeriodStart() {
		return PeriodStart;
	}

	public void setPeriodStart(int periodStart) {
		hasChanged = true;
		PeriodStart = periodStart;
	}

	public void setPeriodStart(String periodStart) {
		hasChanged = true;
		try {
			PeriodStart = Integer.parseInt(periodStart);
		} catch (Exception e) {
			PeriodStart = 0;
		}
	}

	public boolean hasChanged() {
		return hasChanged;
	}

	// if it is an insert then it must be a new forecast
	public boolean isNew() {
		return ActionBean.INSERT.equals(action);
	}
	
	public String getGroupID() {
		return GroupID;
	}

	public void setGroupID(String groupID) {
		hasChanged = true;
		GroupID = groupID;
	}
	
	public int getPeriodsCount() {
		return periods.size();
	}

	public int getPeriodsCompleatedCount() {
		return periodsCompleated;
	}
	
	public int getUsersCount() {
		return users.size();
	}

	public int getUsersOnTargetCount() {
		return usersOnTarget;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		hasChanged = true;
		Description = description;
	}
	
	protected Calendar getNextDate(Calendar cal) {
		if (FORECAST_PERIOD_WEEKLY.equals(Period)) {
			cal.add(Calendar.DAY_OF_MONTH, 7);
		} else if (FORECAST_PERIOD_FORTNIGHTLY.equals(Period)) {
			cal.add(Calendar.DAY_OF_MONTH, 14);
		} else {
			cal.add(Calendar.MONTH, 1);
		}
		
		return cal;
	}
	
	protected Calendar getPrevDate(Calendar cal) {
		if (FORECAST_PERIOD_WEEKLY.equals(Period)) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
		} else if (FORECAST_PERIOD_FORTNIGHTLY.equals(Period)) {
			cal.add(Calendar.DAY_OF_MONTH, -14);
		} else {
			cal.add(Calendar.MONTH, -1);
		}
		
		return cal;
	}
	
	public void generatePeriods() {
		// if any of the periods have been completed, then we can't do new ones
		if (!isNew() && periodsCompleated > 0) return;
		
		periods.removeAllElements();
		quickPeriods.clear();
		
		// start date
		Calendar calStart = Calendar.getInstance();
		calStart.setTime(StartDate);
		calStart.set(Calendar.HOUR_OF_DAY, 0);
		calStart.set(Calendar.MINUTE, 0);
		calStart.set(Calendar.SECOND, 0);

		// end date
		Calendar calEnd = Calendar.getInstance();
		calEnd.setTime(EndDate);
		calEnd.set(Calendar.HOUR_OF_DAY, 23);
		calEnd.set(Calendar.MINUTE, 59);
		calEnd.set(Calendar.SECOND, 59);
		
		// our calculator
		Calendar cal = Calendar.getInstance();
		cal.setTime(calStart.getTime());

		// if the start period is not the same day as the start date
		if (PeriodStart != 0 && cal.get(Calendar.DAY_OF_MONTH) != PeriodStart) {
			cal.set(Calendar.DAY_OF_MONTH, PeriodStart);
			while (cal.after(calStart)) {
				cal = getPrevDate(cal);
			}
		}
		
		// set up the end date of each period
		Calendar old = Calendar.getInstance();
		
		totalDays = 0;
		periodsCompleated = 0;

		while (cal.before(calEnd)) {
			old.setTime(cal.getTime());
			old = getNextDate(old);
			old.add(Calendar.SECOND, -1);

			ForecastPeriod fp = null;
			if (cal.before(calStart)) {
				fp = new ForecastPeriod(request, ID, calStart.getTime(), old.getTime());
			} else if (old.after(calEnd)) {
				fp = new ForecastPeriod(request, ID, cal.getTime(), calEnd.getTime());
			} else {
				fp = new ForecastPeriod(request, ID, cal.getTime(), old.getTime());
			}
			
			periods.add(fp);
			quickPeriods.put(fp.getID(), fp);
			for (int u = 0; u < users.size(); ++u) {
				ForecastUser fu = users.get(u);
				if (fp.getForecastUserPeriodUser(fu.getID()) == null) {
					ForecastUserPeriod fup = new ForecastUserPeriod(request, fp, fu);
				}
			}

			if (fp.getEndDate().before((new Date()))) ++periodsCompleated;
			
			totalDays += fp.getPeriodDays();
			
			cal = getNextDate(cal);
		}
		
		
		// now lets do the calculations
		calculatePeriodPercentages();
	}
	
	public void calculatePeriodPercentages() {
		double totalV = Value;
		int    totalD = totalDays;
		double remainingPer = 1;
		
		// go and find any that we have set the value explicitly on
		for (int v = 0; v < periods.size(); ++v) {
			ForecastPeriod fp = periods.get(v);
			if (Value == 0) {
				fp.setValueSet(false);			// if we have a value of 0 then reset all periods
			} else if (fp.isValueSet()) {
				totalV -= fp.getValue();
				totalD -= fp.getPeriodDays();
				remainingPer -= fp.getPercentage();
			}
		}
		
		// now lets set all the rest
		for (int v = 0; v < periods.size(); ++v) {
			ForecastPeriod fp = periods.get(v);
			if (!fp.isValueSet()) {
				double per = (double) ((double) fp.getPeriodDays() / totalD) * remainingPer;
				double val = (double) totalV * per;
				fp.setPercentage(per);
				fp.setValue(val);
				fp.setValueSet(false); // should already be false
			}
		}
		
		hasChanged = true;
	}

	public void calculateUserPercentages() {
		double totalV = Value;
		int totalU = users.size();
		
		// go and find any that we have set the value explicitly on
		for (int v = 0; v < users.size(); ++v) {
			ForecastUser fu = users.get(v);
			if (Value == 0) {
				fu.setValueSet(false);			// if we have a value of 0 then reset all periods
			} else if (fu.isValueSet()) {
				totalV -= fu.getValue();
				--totalU;
			}
		}
		
		if (totalU > 0) {
			double per = (double) 1 / totalU;
			// now lets set all the rest
			for (int v = 0; v < users.size(); ++v) {
				ForecastUser fu = users.get(v);
				if (!fu.isValueSet()) {
					double val = (double) totalV * per;
					fu.setPercentage(val / Value);
					fu.setValue(val);
					fu.setValueSet(false); // should already be false
				}
			}
		}
		hasChanged = true;
	}

	public void calculateActualValues() {
		double totalActual = 0;
		for (int p = 0; p < periods.size(); ++p) {
			ForecastPeriod fp = periods.get(p);
			// we need to clean the results before we calculate as they may not get over written
			for (int u = 0; u < fp.getForecastUserPeriods().size(); ++u) {
				ForecastUserPeriod fup = fp.getForecastUserPeriods().get(u);
				fup.setActual(0);
			}
			if (FORECAST_TYPE_CONTACT_NEW.equals(Type)) {
				calculateContactNewValues(fp);
				totalActual += fp.getActual();
			} else if (FORECAST_TYPE_CONTACT_STATUS.equals(Type)) {
				calculateContactStatusValues(fp);
				totalActual += fp.getActual();
			} else if (FORECAST_TYPE_COMPANY_NEW.equals(Type)) {
				calculateCompanyNewValues(fp);
				totalActual += fp.getActual();
			} else if (FORECAST_TYPE_COMPANY_STATUS.equals(Type)) {
				calculateCompanyStatusValues(fp);
				totalActual += fp.getActual();
			} else if (FORECAST_TYPE_QUOTE_NEW.equals(Type)) {
				calculateQuoteNewValues(fp);
				totalActual += fp.getActual();
			} else if (FORECAST_TYPE_QUOTE_STATUS.equals(Type)) {
				calculateQuoteStatusValues(fp);
				totalActual += fp.getActual();
			}
		}
		for (int u = 0; u < users.size(); ++u) {
			ForecastUser fu = users.get(u);
			fu.calculateActual();
		}
		
		hasChanged = true;
		
	}
	
	private void calculateContactNewValues(ForecastPeriod fp) {
		GenRow contacts = new GenRow();
		contacts.setRequest(request);
		contacts.setTableSpec("Contacts");
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT COUNT(c.ContactID) AS ContactCount, cg.RepUserID  FROM `Contacts` c JOIN `ContactGroups` cg ON c.ContactID = cg.ContactID ");
		sql.append("WHERE c.CreatedDate >= '");
		sql.append(obcd.format(fp.getStartDate()));
		sql.append("' AND c.CreatedDate <= '");
		sql.append(obcd.format(fp.getEndDate()));
		sql.append("' AND cg.GroupID = '");
		sql.append(getGroupID());
		sql.append("' GROUP BY RepUserID ORDER BY RepUserID");
		
		contacts.setSearchStatement(sql.toString());
		//contacts.doAction(ActionBean.SEARCH);
		contacts.getResults();
		
		double actual = 0;
		while (contacts.getNext()) {
			ForecastUser fu = quickUser.get(contacts.getString("RepUserID"));
			if (fu != null) {
				double val = 0;
				try {
					val = contacts.getInt("ContactCount");
				} catch (Exception e) {
					val = 0;
				}
				ForecastUserPeriod fup = fp.getForecastUserPeriodUser(fu.getID());
				if (fup != null) {
					fup.setActual(val);
				} else {
					fup = new ForecastUserPeriod(request, fp, fu);
				}
				
				actual += val;
			}
		}
		
		fp.setActual(actual);
	}

	private void calculateCompanyNewValues(ForecastPeriod fp) {
		GenRow Companys = new GenRow();
		Companys.setRequest(request);
		Companys.setTableSpec("Companies");
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT COUNT(c.CompanyID) AS CompanyCount, cg.RepUserID  FROM `Companies` c JOIN `CompanyGroups` cg ON c.CompanyID = cg.CompanyID ");
		sql.append("WHERE c.CreatedDate >= '");
		sql.append(obcd.format(fp.getStartDate()));
		sql.append("' AND c.CreatedDate <= '");
		sql.append(obcd.format(fp.getEndDate()));
		sql.append("' AND cg.GroupID = '");
		sql.append(getGroupID());
		sql.append("' GROUP BY RepUserID ORDER BY RepUserID");
		
		Companys.setSearchStatement(sql.toString());
		//Companys.doAction(ActionBean.SEARCH);
		Companys.getResults();
		
		double actual = 0;
		while (Companys.getNext()) {
			ForecastUser fu = quickUser.get(Companys.getString("RepUserID"));
			if (fu != null) {
				double val = 0;
				try {
					val = Companys.getInt("CompanyCount");
				} catch (Exception e) {
					val = 0;
				}
				ForecastUserPeriod fup = fp.getForecastUserPeriodUser(fu.getID());
				if (fup != null) {
					fup.setActual(val);
				} else {
					fup = new ForecastUserPeriod(request, fp, fu);
				}
				
				actual += val;
			}
		}
		
		fp.setActual(actual);
	}

	private void calculateQuoteNewValues(ForecastPeriod fp) {
		GenRow contacts = new GenRow();
		contacts.setRequest(request);
		contacts.setTableSpec("Orders");
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT SUM(o.TotalCost) AS OrderCount, o.RepUserID  FROM `Orders` o ");
		sql.append("WHERE o.CreatedDate >= '");
		sql.append(obcd.format(fp.getStartDate()));
		sql.append("' AND o.CreatedDate <= '");
		sql.append(obcd.format(fp.getEndDate()));
		sql.append("' AND o.GroupID = '");
		sql.append(getGroupID());
		sql.append("' GROUP BY o.RepUserID ORDER BY o.RepUserID");
		
		contacts.setSearchStatement(sql.toString());
		//contacts.doAction(ActionBean.SEARCH);
		contacts.getResults();
		
		double actual = 0;
		while (contacts.getNext()) {
			ForecastUser fu = quickUser.get(contacts.getString("RepUserID"));
			if (fu != null) {
				double val = 0;
				try {
					val = contacts.getInt("OrderCount");
				} catch (Exception e) {
					val = 0;
				}
				ForecastUserPeriod fup = fp.getForecastUserPeriodUser(fu.getID());
				if (fup != null) {
					fup.setActual(val);
				} else {
					fup = new ForecastUserPeriod(request, fp, fu);
				}
				
				actual += val;
			}
		}
		
		fp.setActual(actual);
	}

	private void calculateContactStatusValues(ForecastPeriod fp) {
		GenRow contacts = new GenRow();
		contacts.setRequest(request);
		contacts.setTableSpec("Contacts");
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT COUNT(c.ContactID) AS ContactCount, cg.RepUserID  FROM `Contacts` c JOIN `ContactGroups` cg ON c.ContactID = cg.ContactID ");
		sql.append("JOIN `GroupStatus` gs ON cg.ContactGroupStatusID = gs.GroupStatusID ");
		sql.append("WHERE c.CreatedDate >= '");
		sql.append(obcd.format(fp.getStartDate()));
		sql.append("' AND c.CreatedDate <= '");
		sql.append(obcd.format(fp.getEndDate()));
		sql.append("' AND gs.StatusID = '");
		sql.append(getCondition1());
		sql.append("' AND cg.GroupID = '");
		sql.append(getGroupID());
		sql.append("' GROUP BY RepUserID ORDER BY RepUserID");
		
		contacts.setSearchStatement(sql.toString());
		//contacts.doAction(ActionBean.SEARCH);
		contacts.getResults();
		
		double actual = 0;
		while (contacts.getNext()) {
			ForecastUser fu = quickUser.get(contacts.getString("RepUserID"));
			if (fu != null) {
				double val = 0;
				try {
					val = contacts.getInt("ContactCount");
				} catch (Exception e) {
					val = 0;
				}
				ForecastUserPeriod fup = fp.getForecastUserPeriodUser(fu.getID());
				if (fup != null) {
					fup.setActual(val);
				} else {
					fup = new ForecastUserPeriod(request, fp, fu);
				}
				
				actual += val;
			}
		}
		fp.setActual(actual);
	}

	private void calculateCompanyStatusValues(ForecastPeriod fp) {
		GenRow Companys = new GenRow();
		Companys.setRequest(request);
		Companys.setTableSpec("Companies");
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT COUNT(c.CompanyID) AS CompanyCount, cg.RepUserID  FROM `Companies` c JOIN `CompanyGroups` cg ON c.CompanyID = cg.CompanyID ");
		sql.append("JOIN `GroupStatus` gs ON cg.CompanyGroupStatusID = gs.GroupStatusID ");
		sql.append("WHERE c.CreatedDate >= '");
		sql.append(obcd.format(fp.getStartDate()));
		sql.append("' AND c.CreatedDate <= '");
		sql.append(obcd.format(fp.getEndDate()));
		sql.append("' AND gs.StatusID = '");
		sql.append(getCondition1());
		sql.append("' AND cg.GroupID = '");
		sql.append(getGroupID());
		sql.append("' GROUP BY RepUserID ORDER BY RepUserID");
		
		Companys.setSearchStatement(sql.toString());
		//Companys.doAction(ActionBean.SEARCH);
		Companys.getResults();
		
		double actual = 0;
		while (Companys.getNext()) {
			ForecastUser fu = quickUser.get(Companys.getString("RepUserID"));
			if (fu != null) {
				double val = 0;
				try {
					val = Companys.getInt("CompanyCount");
				} catch (Exception e) {
					val = 0;
				}
				ForecastUserPeriod fup = fp.getForecastUserPeriodUser(fu.getID());
				if (fup != null) {
					fup.setActual(val);
				} else {
					fup = new ForecastUserPeriod(request, fp, fu);
				}
				
				actual += val;
			}
		}
		fp.setActual(actual);
	}

	private void calculateQuoteStatusValues(ForecastPeriod fp) {
		GenRow contacts = new GenRow();
		contacts.setRequest(request);
		contacts.setTableSpec("Orders");
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT SUM(o.TotalCost) AS OrderCount, o.RepUserID  FROM `Orders` o JOIN `OrderStatus` os ON o.OrderStatusID = os.OrderStatusID ");
		sql.append("WHERE o.DateOfSale >= '");
		sql.append(obcd.format(fp.getStartDate()));
		sql.append("' AND o.DateOfSale <= '");
		sql.append(obcd.format(fp.getEndDate()));
		sql.append("' AND os.StatusID = '");
		sql.append(getCondition1());
		sql.append("' AND o.GroupID = '");
		sql.append(getGroupID());
		sql.append("' GROUP BY o.RepUserID ORDER BY o.RepUserID");
		
		contacts.setSearchStatement(sql.toString());
		//contacts.doAction(ActionBean.SEARCH);
		contacts.getResults();
		
		double actual = 0;
		while (contacts.getNext()) {
			ForecastUser fu = quickUser.get(contacts.getString("RepUserID"));
			if (fu != null) {
				double val = 0;
				try {
					val = contacts.getInt("OrderCount");
				} catch (Exception e) {
					val = 0;
				}
				ForecastUserPeriod fup = fp.getForecastUserPeriodUser(fu.getID());
				if (fup != null) {
					fup.setActual(val);
				} else {
					fup = new ForecastUserPeriod(request, fp, fu);
				}
				
				actual += val;
			}
		}
		
		fp.setActual(actual);
	}

	public Vector<ForecastPeriod> getPeriods() {
		return periods;
	}
	
	public ForecastPeriod getPeriod(String forecastID) {
		return quickPeriods.get(forecastID);
	}
	
	public Vector<ForecastUser> getUsers() {
		return users;
	}
	
	public ForecastUser getUser(String userID) {
		return quickUser.get(userID);
	}
	
	public ForecastUser getUserForecast(String userForecastID) {
		return quickForecast.get(userForecastID);
	}
	
	public void addUser(ForecastUser user) {
		if (user != null) {
			users.add(user);
			quickUser.put(user.getUserID(),user);
			quickForecast.put(user.getID(),user);
			for (int p = 0; p < periods.size(); ++p) {
				ForecastPeriod fp = periods.get(p);
				if (fp.getForecastUserPeriodUser(user.getID()) == null) {
					ForecastUserPeriod fup = new ForecastUserPeriod(request, fp, user);
				}
			}
		}
	}
	
	/**
	 * This is a convenience method to format values in a readable form 
	 * 
	 * @param value
	 * @return
	 */
	public String formatValue(double value) {
		Format format = null;
		if (FORECAST_TYPE_QUOTE_NEW.equals(Type) || FORECAST_TYPE_QUOTE_STATUS.equals(Type) || FORECAST_TYPE_QUOTE_CHANGE.equals(Type)) {
			format = NumberFormat.getCurrencyInstance();
		} else {
			format = NumberFormat.getIntegerInstance();
		}
		
		return format.format(value);
	}
}
