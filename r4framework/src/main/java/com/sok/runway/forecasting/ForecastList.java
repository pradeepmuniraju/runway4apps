/**
 * 
 */
package com.sok.runway.forecasting;

import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.RowBean;

/**
 * @author dion
 *
 */
public class ForecastList {

	private HttpServletRequest			request = null;
	
	private String						groupID = null;
	
	private Vector<Forecast>			forecastes = new Vector<Forecast>();
	private HashMap<String,Forecast>	quickSearch = new HashMap<String,Forecast>();
	private HashMap<String,Vector>		groupLists = new HashMap<String,Vector>();
	
	/**
	 * This is a container for the Forecast objects
	 * 
	 * @param request
	 */
	public ForecastList(HttpServletRequest request) {
		this.request = request;
		
		load(null);
	}
	
	/**
	 * This is a container for the Forecast objects
	 * 
	 * @param request
	 * @param groupID
	 */
	public ForecastList(HttpServletRequest request, String groupID) {
		this.request = request;
		
		this.groupID = groupID;
		
		load(groupID);
	}
	
	/**
	 * Loads the Forecast from the dabase, these then load up the relative information
	 */
	public void load(String groupID) {
		GenRow row = new GenRow();
		row.setTableSpec("forecasting/GroupForecast");
		row.setRequest(request);
		row.setParameter("-select", "*");
		if (groupID != null) row.setParameter("GroupID", groupID);
		row.sortBy("StartDate",0);
		row.sortBy("EndDate",1);
		row.doAction(ActionBean.SEARCH);
		row.getResults();
		while (row.getNext()) {
			Forecast f = new Forecast(request, row);
			forecastes.add(f);
			quickSearch.put(f.getID(), f);
			Vector<Forecast> v = groupLists.get(f.getGroupID());
			if (v == null) {
				v = new Vector<Forecast>();
				v.add(f);
				groupLists.put(f.getGroupID(),v);
			} else {
				v.add(f);
			}
		}
		row.close();
		
		// if there was no records just make an empty vector so we know it has tried to read the data  
		if (groupID != null && !groupLists.containsKey(groupID)) groupLists.put(groupID, new Vector<Forecast>());
 	}
	
	public Forecast getForecast(String forecastID) {
		return quickSearch.get(forecastID);
	}

	public Vector<Forecast> getForecasts() {
		return forecastes;
	}
	
	public Vector<Forecast> getForecasts(String groupID) {
		return groupLists.get(groupID);
	}

	public Vector<Forecast> getForecastsByUser(String repUserID) {
		Vector<Forecast> v = new Vector<Forecast>();
		for (int i = 0; i < forecastes.size(); ++i) {
			Forecast f = forecastes.get(i);
			if (f.getUser(repUserID) != null) {
				v.add(f);
			}
		}
		return v;
	}

	public Forecast getNewForecast(String groupID) {
		if (groupID == null || groupID.length() == 0) return null;
		
		Forecast f = new Forecast(request, groupID);
		forecastes.add(f);
		quickSearch.put(f.getID(), f);
		Vector<Forecast> v = groupLists.get(f.getGroupID());
		if (v == null) {
			v = new Vector<Forecast>();
			v.add(f);
			groupLists.put(f.getGroupID(),v);
		} else {
			v.add(f);
		}
		return f;
	}
	
	public void removeForecast(String forecastID) {
		removeForecast(quickSearch.get(forecastID));
	}

	public void removeForecast(Forecast forecast) {
		forecastes.remove(forecast);
		quickSearch.remove(forecast.getID());
		Vector<Forecast> v = groupLists.get(forecast.getGroupID());
		if (v != null) v.remove(forecast);
	}
}
