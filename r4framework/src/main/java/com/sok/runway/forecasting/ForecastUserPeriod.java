/**
 * 
 */
package com.sok.runway.forecasting;

import java.text.Format;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.RowBean;

/**
 * @author dion
 *
 */
public class ForecastUserPeriod {

	private HttpServletRequest				request = null;
	private boolean							hasChanged = false;
	
	// data access members
	private String							ID = "";
	private String							ForecastPeriod = "";
	private String							ForecastUser = "";
	private double							Actual = 0.0;
	
	private String							action = ActionBean.UPDATE;
	
	private ForecastUser					user = null;
	private ForecastPeriod					period = null;
	
	/**
	 * @param fu 
	 * @param fp 
	 * @param row 
	 * @param request 
	 * 
	 */
	public ForecastUserPeriod(HttpServletRequest request, GenRow row, ForecastPeriod fp, ForecastUser fu) {
		this.request = request;
		
		ID = row.getString("UserForecastPeriodID");
		ForecastPeriod = row.getString("ForecastPeriodID");
		ForecastUser = row.getString("UserForecastID");
		Actual = row.getDouble("Actual");
		
		user = fu;
		period = fp;
		
		fu.addForecastUserPeriod(this);
		fp.addForecastUserPeriod(this);
	}

	public ForecastUserPeriod(HttpServletRequest request, ForecastPeriod fp, ForecastUser fu) {
		this.request = request;
		
		ID = KeyMaker.generate();
		
		ForecastPeriod = fp.getID();
		ForecastUser = fu.getID();

		user = fu;
		period = fp;
		
		fu.addForecastUserPeriod(this);
		fp.addForecastUserPeriod(this);
		
		action = ActionBean.INSERT;
		hasChanged = true;
	}

	public boolean hasChanged() {
		return hasChanged;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		hasChanged = true;
		ID = iD;
	}

	public String getForecastPeriodID() {
		return ForecastPeriod;
	}

	public void setForecastPeriodID(String forecastPeriod) {
		hasChanged = true;
		ForecastPeriod = forecastPeriod;
	}

	public String getForecastUserID() {
		return ForecastUser;
	}

	public void setForecastUserID(String forecastUser) {
		hasChanged = true;
		ForecastUser = forecastUser;
	}

	public double getValue() {
		return period.getValue() * user.getPercentage(); 
	}
	
	public double getActual() {
		return Actual;
	}

	public void setActual(double actual) {
		hasChanged = true;
		Actual = actual;
	}

	public double getUserPercentage() {
		if (user.getValue() == 0) return 0;
		return Actual / user.getValue();
	}

	public String getUserPercentageString() {
		Format format = NumberFormat.getPercentInstance();
		
		return format.format(getUserPercentage());
	}

	public double getPeriodPercentage() {
		if (period.getValue() == 0) return 0;
		return Actual / period.getValue();
	}

	public String getPeriodPercentageString() {
		Format format = NumberFormat.getPercentInstance();
		
		return format.format(getPeriodPercentage());
	}

	public ForecastUser getForecastUser() {
		return user;
	}

	public ForecastPeriod getForecastPeriod() {
		return period;
	}

	public void save(HttpServletRequest request) {
		if (hasChanged || ActionBean.INSERT.equals(action)) {
			GenRow row = new GenRow();
			row.setTableSpec("forecasting/UserForecastPeriod");
			row.setRequest(request);
			row.setColumn("UserForecastPeriodID",ID);
			row.setColumn("ForecastPeriodID",ForecastPeriod);
			row.setColumn("UserForecastID",ForecastUser);
			row.setParameter("Actual",(Actual > 0? Actual : 0));
			row.setParameter("Value", (getValue() > 0? getValue() : 0));
			row.doAction(action);
		}
		hasChanged = false;
		action = ActionBean.UPDATE;
	}

}
