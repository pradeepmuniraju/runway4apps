package com.sok.runway;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.*;
import com.sok.runway.crm.*;
import com.sok.framework.*;

public class ContactLoginFilter implements Filter
{
   public static final String loginstatus = "-sessionstatus";
   public static final String failed = "failed";   
   public static final String authenticated = "authenticated"; 
   public static final String deactivated = "deactivated";
   public static final String expired = "expired";
   
   String loginpage;
    String deactivatedpage;
   String realm;
   String loginbean;
   String escape;

   public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws ServletException, IOException
   {
      HttpServletRequest httprequest = (HttpServletRequest)request;
      HttpSession session = httprequest.getSession(false);
      String requesturi = httprequest.getRequestURI();
      if(realm==null || requesturi.indexOf(realm)>=0)
      {
    	  if(escape!=null && requesturi.indexOf(escape)>=0)
          {
             chain.doFilter(request,response);
          } 
    	  else if(session!=null)
         { 
            Object obj = session.getAttribute(loginbean);
            TableData data = null;
            if(obj instanceof Contact)
            {
               Contact contact = (Contact)obj;
               data = contact.getRecord();
            }
            else if(obj instanceof TableData)
            {
               data = (TableData)obj;
            }

            if(data!=null && data.getString(loginstatus).equals(authenticated))
            {
               chain.doFilter(request,response);
            }
            else
            {
               bounce(request ,response, data);
            }
         }
         else
         {
            bounce(request, response, null);
         }
      }
      else
      {
         chain.doFilter(request,response);
      }
   }
   
    
    
   public void bounce(ServletRequest request, ServletResponse response, TableData bean)throws IOException
   {
      StringBuffer bounceurl = new StringBuffer();
      bounceurl.append(((HttpServletRequest)request).getContextPath());
        if(bean == null || !bean.getString(loginstatus).equals(deactivated))
        {
            bounceurl.append(loginpage);
        }
        else
        {
            bounceurl.append(deactivatedpage);
        }
      ((HttpServletResponse)response).sendRedirect(bounceurl.toString());
   }
   
   public void init(FilterConfig config)throws ServletException
   {
      loginpage = config.getInitParameter("loginPage");
        deactivatedpage = config.getInitParameter("deactivatedPage");
        if(deactivatedpage == null)
        {
            deactivatedpage = loginpage;
        }
      realm = config.getInitParameter("realm");
      loginbean = config.getServletContext().getInitParameter("CMSLoginBean");
      if(loginbean==null || loginbean.length()==0)
      {
         loginbean = config.getInitParameter("loginBean");
      }
      escape = config.getInitParameter("escape");
   }
   
   public void destroy()
   {
      
   }
}
