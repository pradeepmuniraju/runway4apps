package com.sok.runway;

import javax.servlet.*;
import javax.servlet.http.*;

import java.util.*;
import java.util.zip.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import java.text.*;
import java.io.*;
import java.net.*;

import javax.mail.*;

import com.sok.framework.*;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.*;
import org.apache.velocity.app.*;

public class OfflinePDF extends OfflineProcess
{
	static final String SENT = "Sent";
	public static final String contactlist = "-contactList";
	StringBuffer status = null;

	RowBean contactselect = null;
	RowBean notesave = null;
	RowBean formbean = null;
	RowBean newletterdata = null;
	TemplateURLs urllinks = null;
	TemplateURLs cmsurllinks = null;

	ArrayList contacts = null;

	Connection con = null;
	Context ctx = null;
	Transport transport = null;
	javax.mail.Session msession = null;
	VelocityEngine ve = null;

	int total = 0;
	int sent = 0;
	int failed = 0;

	int checkcount = -1;
	boolean newsletter = false;

	UserBean user = null;
	TableData contactsearch = null;
	RowBean companyproperties = null;

	File pdffile;
	Font font;

	public OfflinePDF(HttpServletRequest request, ServletContext context)
	{
		super(request, context);

		status = new StringBuffer();

		contacts = new ArrayList(500);

		HttpSession session = request.getSession();
		user = (UserBean) session.getAttribute(SessionKeys.CURRENT_USER);
		contactsearch = (TableData) session.getAttribute(SessionKeys.CONTACT_SEARCH);
		if (contactsearch != null){
			contactsearch.setViewSpec("OfflineContactView");
			// we need to remove all group by
			for (int x = 0; x < 10; ++x) {
				contactsearch.remove("-groupby" + x);
				contactsearch.remove("-sort" + x);
				contactsearch.remove("-order" + x);
			}
			contactsearch.setParameter("-groupby0", "ContactID");
		}
		start();
	}

	void retrieveContactsFromList()
	{
		StringTokenizer st = new StringTokenizer(requestbean.getString(contactlist), "+");
		while(st.hasMoreTokens())
		{
			contacts.add(st.nextToken());
		}
		total = contacts.size();
	}

	void retrieveContactsFromSearch() throws Exception
	{
		if(debug)
		{		  
			errors.append(contactsearch.getSearchStatement());
		}			
		try
		{
		   getConnection();
		   contactsearch.setConnection(con);
		   RowSetWrapperBean list = new RowSetWrapperBean();		
		   list.setConnection(con);
		   list.setSearchBean(contactsearch);
		   list.getResults();
		   
		   TableData resultset = list.getResultBean();
		   while (list.getNext()) {
				contacts.add(resultset.getString("ContactID"));
			}		   			  
			total = contacts.size();		      			
		}		
		finally
		{    		
			try{
            	con.close();
        	}catch (Exception ex){}    		  		    
		}
		
		/*StringBuffer searchstmt = new StringBuffer();
		searchstmt.append("select Contacts.ContactID from Contacts where ");
		searchstmt.append(contactsearch.getSearchCriteria());

		if(debug)
		{
			errors.append(searchstmt.toString());
		}

		ResultSet current = null;
		Statement stmt = null;
		try
		{
			getConnection();
			stmt = con.createStatement();
			current = stmt.executeQuery(searchstmt.toString());
			while(current.next())
			{
				contacts.add(current.getString(1));
			}
			total = contacts.size();
		}
		finally
		{
			if(stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch(Exception ex)
				{
				}
			}
			if(current != null)
			{
				try
				{
					current.close();
				}
				catch(Exception ex)
				{
				}
			}
		}*/
	}

	void retrieveContacts() throws Exception
	{
		if(requestbean.getString(contactlist).length() == 0)
		{
			retrieveContactsFromSearch();
		}
		else
		{
			retrieveContactsFromList();
		}
	}

	public static void setMargins(Document document, TableData requestdata)
	{
		String topmargin = requestdata.getString("-PDFMarginTop");
		String bottommargin = requestdata.getString("-PDFMarginBottom");
		if(topmargin.length() != 0 && bottommargin.length() != 0)
		{
			try
			{
				document.setMargins(document.leftMargin(), document.rightMargin(), Float
						.parseFloat(topmargin), Float.parseFloat(bottommargin));
			}
			catch(Exception e)
			{
			}
		}
	}

	public static Font getFont(TableData requestdata)
	{
		String font = requestdata.getString("-PDFFont");
		String fontsize = requestdata.getString("-PDFFontSize");
		if(font.length() != 0 && fontsize.length() != 0)
		{
			try
			{
				return (FontFactory.getFont(font, Integer.parseInt(fontsize)));
			}
			catch(Exception e)
			{
			}
		}
		return (FontFactory.getFont(FontFactory.HELVETICA, 8));
	}

	public Image getLogo(Document document, String home) 
	{
		Image image = null;
		try
		{
			StringBuffer urlstring = new StringBuffer();
			urlstring.append(home);
			urlstring.append("/specific/images/logolarge.jpg");
			String logopath = urlstring.toString();

			URL imageurl = new URL(logopath);
			image = Image.getInstance(imageurl);
			image.scaleToFit(180, 80); // w ,h
			image.setAlignment(Image.LEFT);

		}
		catch(Exception e)
		{
		}
		return (image);
	}

	public static void addFooter(com.lowagie.text.Document document, TableData company)
	{
		Font smallfont = FontFactory.getFont(FontFactory.HELVETICA, 6);
		HeaderFooter footer = new HeaderFooter(new Paragraph(ActionBean.getSubstituted(company
				.getString("Footer"), company, "footer"), smallfont), false);
		footer.setBorder(Rectangle.NO_BORDER);
		document.setFooter(footer);
	}

	void makepdfs() throws Exception
	{
		Document document = new Document();
		try
		{
			font = getFont(formbean);
            setMargins(document, formbean);
			StringBuffer path = new StringBuffer(context.getRealPath(""));
			path.append("/files/");
			pdffile = new File(path.toString(), KeyMaker.generate() + ".zip");
			// appendError(pdffile.getAbsolutePath());
			ZipOutputStream zip = new ZipOutputStream(new FileOutputStream(pdffile));
			zip.putNextEntry(new ZipEntry("mailout.pdf"));
			PdfWriter.getInstance(document, zip);
			// have to add footer before open
			if(formbean.getString("-PDFFooter").length() != 0)
			{
				addFooter(document, companyproperties);
			}
			document.open();
			Image image = null;
			if(formbean.getString("-PDFLogo").length() != 0)
			{
				image = getLogo(document, context.getInitParameter("URLHome"));
			}
			ve = VelocityManager.getEngine();
			int success = 0;
			int currentsent = 0;
			int currentfailed = 0;
			for(int i = 0; i < total && !isKilled(); i++)
			{
				if(i != 0)
				{
					document.newPage();
				}

				if(image != null)
				{
					document.add(image);
				}
				success = makepdfs(document, newsletter, (String) contacts.get(i));

				if(success == 0)
				{
					sent++;
					currentsent++;
				}
				else
				{
					failed++;
					currentfailed++;
				}
			}
		}
		finally
		{
			document.close();
		}
	}

	protected int makepdfs(Document document, boolean newsletter, String contactid)
	{
		int success = 0;

		TableData contact = OfflineMailer.getContact(contactselect, contactid, this);
		if(contact == null)
		{
			appendError("Contact not found", contactid);
			return (1);
		}
		OfflineMailer.setUserEmailTokens(contactselect, user, formbean);

		String contactstatus = (String) contact.get("CurrentStatus");

		if(contactstatus != null && contactstatus.equals("OptOut"))
		{
			appendError("Opt-out contact", contactid);
			return (1);
		}
		if(contactstatus != null && contactstatus.equals("Left Company"))
		{
			appendError("Contact Left Company", contactid);
			return (1);
		}
		String contactstatusid = (String) contact.get("CurrentStatusID");

		if(contactstatusid != null
				&& contactstatusid.equals(InitServlet.getSystemParams().getProperty(
						"ContactOptOutStatusID")))
		{
			appendError("Opt-out contact", contactid);
			return (1);
		}
		if(contactstatusid != null
				&& contactstatusid.equals(InitServlet.getSystemParams().getProperty(
						"ContactLeftCompanyStatusID")))
		{
			appendError("Contact Left Company", contactid);
			return (1);
		}

		if(newsletter)
		{
			success = pdfNewsLetter(document, contact);
		}
		else
		{
			success = pdfEmail(document, contact);
		}
		return (success);
	}

	int pdfNewsLetter(Document document, TableData contact)
	{
		String newnoteid = KeyMaker.generate();

		String contactid = contact.getString("ContactID");

		contact.put("NoteID", newnoteid);
		contact.put("cms", newletterdata);

		contact.put("Date", new DateToken());

		StringWriter tnlw = new StringWriter();

		StringWriter textw = new StringWriter();
		StringWriter sw = new StringWriter();

		try
		{
			ve.evaluate(contact, sw, contactid, formbean.getString("Subject"));
			sw.flush();
			sw.close();
			String notesubject = sw.toString();

			urllinks.setTextLinks(contact);

			ve.evaluate(contact, textw, contactid, formbean.getString("Body"));
			textw.flush();
			textw.close();
			String textbody = textw.toString();

			StringBuffer roottemplate = new StringBuffer();
			roottemplate.append("#parse(\"");
			roottemplate.append(newletterdata.getString("EmailTemplate"));
			roottemplate.append(".vt\")");

			StringBuffer textroottemplate = new StringBuffer();
			textroottemplate.append("#parse(\"");
			textroottemplate.append(newletterdata.getString("EmailTemplate"));
			textroottemplate.append("_text.vt\")");

			StringWriter pretnlw = new StringWriter();

			contact.put("Body", textbody);
			ve.evaluate(contact, pretnlw, "NewsLetter", textroottemplate.toString());
			pretnlw.flush();
			pretnlw.close();

			cmsurllinks.setTextLinks(contact);
			ve.evaluate(contact, tnlw, "NewsLetter", pretnlw.toString());
			tnlw.flush();
			tnlw.close();
			
			String noteletter = tnlw.toString();

			Paragraph p = new Paragraph();
			p.add(new Chunk(StringUtil.htmlToText(noteletter), font));
			document.add(p);

			saveNote(con, contact, newnoteid, noteletter, notesubject);
			return (0);
		}
		catch(Exception e)
		{
			appendError(e.toString(), contactid);
			return (1);
		}
	}

	int pdfEmail(Document document, TableData contact)
	{
		String newnoteid = KeyMaker.generate();
		String contactid = contact.getString("ContactID");
		contact.put("NoteID", newnoteid);
		contact.put("Date", new DateToken());

		try
		{
			RowSetBean questionList = new RowSetBean();
			questionList.setJndiName(dbconn);
			questionList.setViewSpec("answers/ContactAnswerView");
			questionList.setColumn("ContactID", contact.getString("ContactID"));
			if(contact.getString("Type").length() > 0) {
				questionList.setColumn(
						"ContactAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.ContactType", contact.getString("Type") + "+All");
			}
			else {
				questionList.setColumn(
						"ContactAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.ContactType", "All");
			}
			questionList.setColumn(
					"ContactAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.DisplayInContactView", "Y");
			questionList.generateSQLStatment();
			questionList.getResults();
			while(questionList.getNext())
			{
				contact.put("Profile-"
						+ StringUtil.ToAlphaNumeric(questionList.getString("QuestionLabel")),
						questionList.getString("Answer"));
			}
		}
		catch(Exception e)
		{
			status.append(e.toString());
		}

		StringWriter textw = new StringWriter();
		StringWriter sw = new StringWriter();

		try
		{
			ve.evaluate(contact, sw, contactid, formbean.getString("Subject"));
			sw.flush();
			sw.close();
			String notesubject = sw.toString();

			urllinks.setTextLinks(contact);
			// ve.evaluate(contact, textw, contactid,
			// formbean.getString("TextHeader"));
			ve.evaluate(contact, textw, contactid, formbean.getString("Body"));
			// ve.evaluate(contact, textw, contactid,
			// formbean.getString("TextFooter"));
			textw.flush();
			textw.close();
			String textbody = textw.toString();
			
			Paragraph p = new Paragraph();
			p.add(new Chunk(StringUtil.htmlToText(textbody), font));
			document.add(p);

			getConnection();
			saveNote(con, contact, newnoteid, textbody, notesubject);
			return (0);
		}
		catch(Exception e)
		{
			appendError(e.toString(), contactid);
			return (1);
		}
	}

	public void appendError(String msg, String contactid)
	{
		setLastError(msg);
		errors.append(msg);
		errors.append(" - ");
		errors.append(formbean.getString("URLHome"));
		errors.append("/crm/contactview.jsp?-action=select&ContactID=");
		errors.append(contactid);
		errors.append("\r\n");
	}

	void saveNote(Connection conn, TableData contact, String newnoteid, String notebody, String notesubject)
	{
		try
		{
			String userid = user.getUserID();

			notesave.clear();
            notesave.setConnection(con);
			notesave.setColumn("NoteID", newnoteid);
			notesave.setColumn("ContactID", contact.getString("ContactID"));
			notesave.setColumn("CompanyID", contact.getString("CompanyID"));
			notesave.setColumn("Type", "PDF");
			notesave.setColumn("RepUserID", contact.getString("RepUserID"));
			notesave.setColumn("ModifiedBy", userid);
			notesave.setColumn("TemplateUsed", formbean.getString("TemplateID"));
			notesave.setColumn("CMSUsed", formbean.getString("CMSID"));
			notesave.setColumn("Campaign", formbean.getString("Campaign"));
			// This will be useful when we get rid of the Damn Campaign String
			notesave.setColumn("CampaignID", formbean.getString("CampaignID"));
			notesave.setColumn("OpportunityID", requestbean.getString("OpportunityID"));
			notesave.setColumn("ProcessStageID", requestbean.getString("ProcessStageID"));
			notesave.setColumn("Completed", "Yes");
			notesave.setColumn("Subject", notesubject);
			notesave.setColumn("Body", notebody);
			notesave.setColumn("CreatedBy", userid);
			notesave.setColumn("EmailTo", contact.getString("Email"));
			notesave.setColumn("EmailFrom", contact.getString("EmailFrom"));
			notesave.setColumn("MasterTemplateID", requestbean.getString("MasterTemplateID"));

			if(status.toString().equals(SENT))
			{
				notesave.setColumn("SentDate", "NOW");
			}
			notesave.setColumn("NoteDate", "NOW");
			notesave.setColumn("CreatedDate", "NOW");
			notesave.setColumn("ModifiedDate", "NOW");
			notesave.setColumn("GroupID", contact.getString("GroupID"));
			notesave.doAction();
			if(notesave.getUpdatedCount() == 0)
			{
				errors.append("Could not save note, ");
				errors.append(notesave.getError());
				errors.append(" - ");
				errors.append(contact.getString("ContactID"));
				errors.append(".\r\n");
			}
			if(debug)
			{
				if(notesave.getError().length() != 0)
				{
					appendError(notesave.getError(), contact.getString("ContactID"));
				}
			}
		}
		catch(Exception ex)
		{
			appendError("Could not save note, " + ex.toString(), contact.getString("ContactID"));
		}

	}

	void getConnection() throws Exception
	{
		if(con == null || con.isClosed())
		{
			try
			{
				if(ctx == null)
				{
					ctx = new InitialContext();
				}

				DataSource ds = null;
				try
				{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource) envContext.lookup(dbconn);
				}
				catch(Exception e)
				{
				}
				if(ds == null)
				{
					ds = (DataSource) ctx.lookup(dbconn);
				}
				con = ds.getConnection();
			}
			finally
			{
				try
				{
					ctx.close();
					ctx = null;
				}
				catch(Exception e)
				{
				}
			}
		}
	}

	public void execute()
	{
		con = null;
		ctx = null;
		try
		{
			if(user == null
					|| (contactsearch == null && requestbean.getString(contactlist).length() == 0))
			{
				status.append("Resources not found, offline process halted.\r\n");
				return;
			}

			setEmailHeader();

			if(requestbean.getString("-debug").length() != 0)
			{
				debug = true;
				status.append("Debug mode on.\r\n");
			}

			requestbean.setViewSpec("TemplateView");

			if(requestbean.getString("CMSID").length() != 0)
			{
				newsletter = true;
				newletterdata = new RowBean();
				newletterdata.setJndiName(dbconn);
				newletterdata.setViewSpec("PageView");
				newletterdata.setColumn("CMSID", requestbean.getString("CMSID"));
				newletterdata.setColumn("ParentID", "NULL+EMPTY");
				newletterdata.setLocale(thislocale);
				newletterdata.retrieveData();

				RowArrayBean newslist = new RowArrayBean();
				newslist.setJndiName(dbconn);
				newslist.setViewSpec("PageView");
				newslist.setLocale(thislocale);
				newslist.setColumn("ParentID", newletterdata.getColumn("PageID"));
				newslist.setColumn("Active", "Active");
				newslist.sortBy("PageLinks.SortOrder", 0);
				newslist.getResults();

				RowArrayBean productlist = null;
				productlist = new RowArrayBean();
				productlist.setJndiName(dbconn);
				productlist.setViewSpec("PageProductView");
				productlist.setLocale(thislocale);
				productlist.setColumn("PageID", newletterdata.getString("PageID"));
				productlist.generateSQLStatment();
				productlist.getResults();
				newletterdata.put("products", productlist.getList());

				RowArrayBean newsitemlist = null;
				for(int i = 0; i < newslist.getSize(); i++)
				{
					newsitemlist = new RowArrayBean();
					newsitemlist.setJndiName(dbconn);
					newsitemlist.setViewSpec("PageView");
					newsitemlist.setLocale(thislocale);
					newsitemlist.setColumn("ParentID", newslist.get(i).getColumn("PageID"));
					newsitemlist.setColumn("Active", "Active");
					newsitemlist.sortBy("PageLinks.SortOrder", 0);
					newsitemlist.getResults();
					newslist.get(i).put("newsitems", newsitemlist.getList());

					productlist = new RowArrayBean();
					productlist.setJndiName(dbconn);
					productlist.setViewSpec("PageProductView");
					productlist.setLocale(thislocale);
					productlist.setColumn("PageID", newslist.get(i).getColumn("PageID"));
					productlist.generateSQLStatment();
					productlist.getResults();
					newslist.get(i).put("products", productlist.getList());
				}

				newletterdata.put("news", newslist.getList());

				cmsurllinks = new TemplateURLs();
				cmsurllinks.getCMSLinks(requestbean.getString("CMSID"), context);

			}

			if(requestbean.getString("TemplateID").length() != 0)
			{
				formbean = new RowBean();
				formbean.setViewSpec("TemplateView");
				formbean.put("URLHome", context.getInitParameter("URLHome"));
				formbean.put("contextPath", contextpath);
				formbean.setJndiName(super.dbconn);
				formbean.put("TemplateID", requestbean.getString("TemplateID"));
				formbean.setAction("select");
				formbean.doAction();
				formbean.put("URLHome", context.getInitParameter("URLHome"));
				formbean.put("contextPath", contextpath);
				formbean.putAll(requestbean);
			}
			else
			{
				formbean = requestbean;
			}

			urllinks = new TemplateURLs();
			if(formbean.getColumn("TemplateID").length() != 0)
			{
				urllinks.getLinks(formbean.getColumn("TemplateID"), formbean
						.getColumn("MasterTemplateID"), context);
			}

			getConnection();

			if(formbean.getString("-checkcount").length() != 0)
			{
				checkcount = Integer.parseInt(formbean.getString("-checkcount"));
			}

			if(newsletter)
			{
				if(newletterdata.getString("PageID").length() == 0)
				{
					status.append("Template not found, offline process halted.\r\n");
					status.append(newletterdata.getError());
					return;
				}
			}
			else
			{
				if(formbean.getString("TemplateName").length() == 0)
				{
					status.append("Template not found, offline process halted.\r\n");
					status.append(formbean.getError());
					return;
				}
			}

			contactselect = new TableBean();
			contactselect.setJndiName(dbconn);
			contactselect.setLocale(thislocale);
			contactselect.setViewSpec("ContactEmailView");
			contactselect.setAction(ActionBean.SELECT);

			notesave = new TableBean();
			notesave.setJndiName(dbconn);
			notesave.setTableSpec("Notes");
			notesave.setAction(ActionBean.INSERT);

			companyproperties = new RowBean();
			companyproperties.setJndiName(dbconn);
			companyproperties.setLocale(thislocale);
			companyproperties.setViewSpec("SetupView");
			companyproperties.retrieveData();

			retrieveContacts();

			if(checkcount != -1)
			{
				if(total != checkcount)
				{
					status
							.append("Search results count does not match, offline process halted.\r\n");
					status.append("Current - ");
					status.append(String.valueOf(total));
					status.append(", Original - ");
					status.append(String.valueOf(checkcount));
					status.append("\r\n");
					return;
				}
			}

			makepdfs();
			status.append(String.valueOf(total));
			status.append(" contacts found.\r\n");
			status.append(String.valueOf(sent));
			status.append(" PDFs made.\r\n");
			status.append(String.valueOf(failed));
			status.append(" PDFs failed.\r\n");
			if(isKilled())
			{
				status.append("Offline process terminated by - ");
				status.append(getKiller());
				status.append("\r\n\r\n");
			}
			else
			{
				status.append("Offline process completed.\r\n\r\n");
			}

		}
		catch(Exception e)
		{
			status.append("Offline process halted on ");
			status.append(String.valueOf(sent + failed));
			status.append(" of ");
			status.append(String.valueOf(total));
			status.append(".\r\n\r\n");
			status.append(ActionBean.writeStackTraceToString(e));

		}
		finally
		{
			if(con != null)
			{
				try
				{
					con.close();
					con = null;
				}
				catch(Exception ex)
				{
				}
			}
			if(ctx != null)
			{
				try
				{
					ctx.close();
					ctx = null;
				}
				catch(Exception ex)
				{
				}
			}
		}
	}

	public File getStatusAttachment()
	{
		return (pdffile);
	}

	void setEmailHeader()
	{
		status.append(contextname);
		status.append(" Runway offline process - Generate PDF\r\n");
		status.append("Initiated ");
		try
		{
			DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM,
					super.getLocale());
			status.append(dt.format(new java.util.Date()));
		}
		catch(Exception e)
		{
			status.append(new java.util.Date().toString());
		}
		status.append("\r\n");
	}

	String statusstring = null;

	public String getStatusMailBody()
	{
		if(statusstring == null)
		{
			if(errors != null && errors.length() != 0)
			{
				status.append("\r\n");
				status.append(errors.toString());
			}
			statusstring = status.toString();
		}

		return (statusstring);
	}

	public String getStatusMailSubject()
	{
		return (contextname + " Runway offline process - Generate PDF");
	}

	public String getStatusMailRecipient()
	{
		return (user.getEmail());
	}

	public int getProcessSize()
	{
		return total;
	}

	public int getProcessedCount()
	{
		return sent + failed;
	}

	public int getErrorCount()
	{
		return failed;
	}
}