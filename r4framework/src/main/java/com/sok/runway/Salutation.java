package com.sok.runway;
import javax.servlet.jsp.PageContext;
import com.sok.framework.*;
public class Salutation
{

	public static String getSalutation(PageContext pageContext)
	{
		String pfirst = (String)pageContext.getAttribute("PartnerFirstName");
		String plast = (String)pageContext.getAttribute("PartnerLastName");
		if((pfirst !=null && pfirst.length()!=0) || (plast !=null && plast.length()!=0))
		{
			return(getSalutation((String) pageContext.getAttribute("Title"), (String) pageContext.getAttribute("FirstName"), (String) pageContext.getAttribute("LastName"),
				(String) pageContext.getAttribute("PartnerTitle"), pfirst, plast));
		}
		else
		{
			return(getSalutation(pageContext.getAttribute("Title"), pageContext.getAttribute("FirstName"), pageContext.getAttribute("LastName"),
				pageContext.getAttribute("SpouseTitle"), pageContext.getAttribute("Spouse"), pageContext.getAttribute("SpouseLastName")));
		}
	}

	public static String getSalutation(RowBean row)
	{
		if(row.getColumn("PartnerFirstName").length()!=0 || row.getColumn("PartnerLastName").length()!=0)
		{
			return(getSalutation(row.getColumn("Title"), row.getColumn("FirstName"), row.getColumn("LastName"),
				row.getColumn("PartnerTitle"), row.getColumn("PartnerFirstName"), row.getColumn("PartnerLastName")));
		}
		else
		{
			return(getSalutation(row.getColumn("Title"), row.getColumn("FirstName"), row.getColumn("LastName"),
				row.getColumn("SpouseTitle"), row.getColumn("Spouse"), row.getColumn("SpouseLastName")));
		}
	}

	public static String getSalutation(Object atitle, Object afirst, Object alast, Object btitle, Object bfirst, Object blast)
	{
		return(getSalutation((String) atitle, (String) afirst, (String) alast, (String) btitle, (String) bfirst, (String) blast));
	}
/* */
	public static String getSalutation(String atitle, String afirst, String alast, String btitle, String bfirst, String blast)
	{
		StringBuffer temp = new StringBuffer();
	
		if(bfirst.length() == 0)
		{
			if(btitle.length()==0 && atitle.length()==0)
			{
				getname(afirst, alast, temp);
			}
			else if(btitle.length()!=0 && atitle.length()!=0)
			{
				temp.append(atitle);
				temp.append(" & ");
				temp.append(btitle);
				temp.append(" ");
				getname(afirst, alast, temp);
			}
			else if(atitle.length()!=0)
			{
				temp.append(atitle);
		 		temp.append(" ");
		 		getname(afirst, alast, temp);
			}
		}
		else
		{
			if(btitle.length()!=0 && atitle.length()!=0 && alast.length()!=0)
			{
				temp.append(atitle);
		 		temp.append(" ");
		 		getname(afirst, alast, temp);
				temp.append(" & ");
				temp.append(btitle);
				temp.append(" ");
				if(blast.length() !=0)
				{
					getname(bfirst, blast, temp);
				}
				else
				{
					temp.append(bfirst);
					temp.append(" ");
					temp.append(alast);
				}
			}
			else if(blast.length()==0 && alast.length()!=0)
			{
				temp.append(afirst);
				temp.append(" & ");
				temp.append(bfirst);
				temp.append(" ");
				temp.append(alast);			
			}
			else if(bfirst.length()!=0 && afirst.length()!=0 && alast.length()!=0 && alast.equals(blast))
			{
				temp.append(afirst);
				temp.append(" & ");
				getname(bfirst, blast, temp);		
			}
			else
			{
				getname(afirst, alast, temp);
				temp.append(" & ");
				getname(bfirst, blast, temp);			
			}
		}
		return(temp.toString());
	}

	static void getname(String nfirst, String nlast, StringBuffer temp)
	{
		if(nfirst.length()!=0 && nlast.length()!=0)
		{
			temp.append(nfirst);
			temp.append(" ");
			temp.append(nlast);
		}
		else if(nfirst.length()!=0)
		{
			temp.append(nfirst);
		}
		else if(nlast.length()!=0)
		{
			temp.append(nlast);
		}
	}
}
