/*
 * User: Behrang
 * Date: Jun 9, 2010
 * Time: 4:58:05 PM 
 */
package com.sok.runway.model;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.Serializable;

public abstract class AbstractModel implements Serializable {

    public String escapeHtml(String text) {
        return StringEscapeUtils.escapeHtml(text);
    }

    public String enquote(String text) {
        return "\"" + escapeHtml(text) + "\"";
    }

}
