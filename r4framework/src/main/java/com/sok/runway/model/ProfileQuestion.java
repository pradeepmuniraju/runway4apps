package com.sok.runway.model;

public class ProfileQuestion extends AbstractModel {
    private String id;
    private String label;
    private String exportLabel;
    private String inputType;
    private String valueList;
    private String parentQuestionID;
    private String createdDate;
    private String createdBy;
    private String modifiedDate;
    private String modifedBy;
    private String description;
    private String calculationType;
    private String relatedQuestionID;
    private String calculationPeriod;
    private String productGroupID;
    private String groupID;
    private String minValue;
    private String maxValue;
    private String holderID;
    private String profileType;
    private String defaultGraph; 
    
	/**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }
    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }
    /**
     * @return the exportLabel
     */
    public String getExportLabel() {
        return exportLabel;
    }
    /**
     * @param exportLabel the exportLabel to set
     */
    public void setExportLabel(String exportLabel) {
        this.exportLabel = exportLabel;
    }
    /**
     * @return the inputType
     */
    public String getInputType() {
        return inputType;
    }
    /**
     * @param inputType the inputType to set
     */
    public void setInputType(String inputType) {
        this.inputType = inputType;
    }
    /**
     * @return the valueList
     */
    public String getValueList() {
        return valueList;
    }
    /**
     * @param valueList the valueList to set
     */
    public void setValueList(String valueList) {
        this.valueList = valueList;
    }
    /**
     * @return the parentQuestionID
     */
    public String getParentQuestionID() {
        return parentQuestionID;
    }
    /**
     * @param parentQuestionID the parentQuestionID to set
     */
    public void setParentQuestionID(String parentQuestionID) {
        this.parentQuestionID = parentQuestionID;
    }
    /**
     * @return the createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }
    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    /**
     * @return the modifiedDate
     */
    public String getModifiedDate() {
        return modifiedDate;
    }
    /**
     * @param modifiedDate the modifiedDate to set
     */
    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
    /**
     * @return the modifedBy
     */
    public String getModifedBy() {
        return modifedBy;
    }
    /**
     * @param modifedBy the modifedBy to set
     */
    public void setModifedBy(String modifedBy) {
        this.modifedBy = modifedBy;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the calculationType
     */
    public String getCalculationType() {
        return calculationType;
    }
    /**
     * @param calculationType the calculationType to set
     */
    public void setCalculationType(String calculationType) {
        this.calculationType = calculationType;
    }
    /**
     * @return the relatedQuestionID
     */
    public String getRelatedQuestionID() {
        return relatedQuestionID;
    }
    /**
     * @param relatedQuestionID the relatedQuestionID to set
     */
    public void setRelatedQuestionID(String relatedQuestionID) {
        this.relatedQuestionID = relatedQuestionID;
    }
    /**
     * @return the calculationPeriod
     */
    public String getCalculationPeriod() {
        return calculationPeriod;
    }
    /**
     * @param calculationPeriod the calculationPeriod to set
     */
    public void setCalculationPeriod(String calculationPeriod) {
        this.calculationPeriod = calculationPeriod;
    }
    /**
     * @return the productGroupID
     */
    public String getProductGroupID() {
        return productGroupID;
    }
    /**
     * @param productGroupID the productGroupID to set
     */
    public void setProductGroupID(String productGroupID) {
        this.productGroupID = productGroupID;
    }
    /**
     * @return the groupID
     */
    public String getGroupID() {
        return groupID;
    }
    /**
     * @param groupID the groupID to set
     */
    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }
    /**
     * @return the minValue
     */
    public String getMinValue() {
        return minValue;
    }
    /**
     * @param minValue the minValue to set
     */
    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }
    /**
     * @return the maxValue
     */
    public String getMaxValue() {
        return maxValue;
    }
    /**
     * @param maxValue the maxValue to set
     */
    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }
    /**
     * @return the holderID
     */
    public String getHolderID() {
        return holderID;
    }
    /**
     * @param holderID the holderID to set
     */
    public void setHolderID(String holderID) {
        this.holderID = holderID;
    }
    /**
     * @return the profileType
     */
    public String getProfileType() {
        return profileType;
    }
    /**
     * @param profileType the profileType to set
     */
    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }
    
    public String getDefaultGraph() {
		return defaultGraph;
	}
	public void setDefaultGraph(String defaultGraph) {
		this.defaultGraph = defaultGraph;
	}
    
    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{ id: ").append(enquote(id)).append(", ");
        json.append("label: ").append(enquote(label)).append("}");
        json.append("exportLabel: ").append(enquote(exportLabel)).append("}");
        json.append("inputType: ").append(enquote(inputType)).append("}");
        json.append("valueList: ").append(enquote(valueList)).append("}");
        json.append("parentQuestionID: ").append(enquote(parentQuestionID)).append("}");
        json.append("createdDate: ").append(enquote(createdDate)).append("}");
        json.append("createdBy: ").append(enquote(createdBy)).append("}");
        json.append("modifiedDate: ").append(enquote(modifiedDate)).append("}");
        json.append("modifedBy: ").append(enquote(modifedBy)).append("}");
        json.append("description: ").append(enquote(description)).append("}");
        json.append("calculationType: ").append(enquote(calculationType)).append("}");
        json.append("relatedQuestionID: ").append(enquote(relatedQuestionID)).append("}");
        json.append("calculationPeriod: ").append(enquote(calculationPeriod)).append("}");
        json.append("productGroupID: ").append(enquote(productGroupID)).append("}");
        json.append("groupID: ").append(enquote(groupID)).append("}");
        json.append("minValue: ").append(enquote(minValue)).append("}");
        json.append("maxValue: ").append(enquote(maxValue)).append("}");
        json.append("holderID: ").append(enquote(holderID)).append("}");
        json.append("profileType: ").append(enquote(profileType)).append("}");
        json.append("defaultGraph: ").append(enquote(defaultGraph)).append("}");
        return json.toString();
     }
    
     @Override
     public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
    
         ProfileQuestion profileQuestion = (ProfileQuestion) o;
    
         if (id != null ? !id.equals(profileQuestion.id) : profileQuestion.id != null) return false;
    
         return true;
     }
    
     @Override
     public int hashCode() {
         return id != null ? id.hashCode() : 0;
     }
    
     @Override
     public String toString() {
         return "Profile{" +
                 "id='" + id + '\'' +
                 ", label='" + label + '\'' +
                 ", exportLabel='" + exportLabel + '\'' +
                 ", inputType='" + inputType + '\'' +
                 ", valueList='" + valueList + '\'' +
                 ", parentQuestionID='" + parentQuestionID + '\'' +
                 ", createdDate='" + createdDate + '\'' +
                 ", createdBy='" + createdBy + '\'' +
                 ", modifiedDate='" + modifiedDate + '\'' +
                 ", modifedBy='" + modifedBy + '\'' +
                 ", description='" + description + '\'' +
                 ", calculationType='" + calculationType + '\'' +
                 ", relatedQuestionID='" + relatedQuestionID + '\'' +
                 ", calculationPeriod='" + calculationPeriod + '\'' +
                 ", productGroupID='" + productGroupID + '\'' +
                 ", groupID='" + groupID + '\'' +
                 ", minValue='" + minValue + '\'' +
                 ", maxValue='" + maxValue + '\'' +
                 ", holderID='" + holderID + '\'' +
                 ", profileType='" + profileType + '\'' +
                 ", defaultGraph='" + defaultGraph + '\'' +
                 '}';
     }    
}
