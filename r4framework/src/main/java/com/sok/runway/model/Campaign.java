/*
 * User: Behrang
 * Date: Jun 21, 2010
 * Time: 12:25:19 PM 
 */
package com.sok.runway.model;

import java.util.Date;

public class Campaign extends AbstractModel {

    private String id;
    private String name;
    private String description;
    private String status;
    private Date createdDate;
    private Date modifiedDate;
    private String modifiedBy;
    private String createdBy;
    private String groupId;
    private String primarySort;
    private String defaultStatusId;
    private String exitStatusId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getPrimarySort() {
        return primarySort;
    }

    public void setPrimarySort(String primarySort) {
        this.primarySort = primarySort;
    }

    public String getDefaultStatusId() {
        return defaultStatusId;
    }

    public void setDefaultStatusId(String defaultStatusId) {
        this.defaultStatusId = defaultStatusId;
    }

    public String getExitStatusId() {
        return exitStatusId;
    }

    public void setExitStatusId(String exitStatusId) {
        this.exitStatusId = exitStatusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Campaign campaign = (Campaign) o;

        if (id != null ? !id.equals(campaign.id) : campaign.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
