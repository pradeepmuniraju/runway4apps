/*
 * User: Behrang
 * Date: Jun 8, 2010
 * Time: 3:03:03 PM 
 */
package com.sok.runway.model;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Territory extends AbstractModel {

    private String id;
    private String name;
    private String description;
    private String state;
    private String country;          
    private String repUserId;
    private String groupId;

    private List<CityGeocode> cities;

    public Territory() {
        cities = new ArrayList<CityGeocode>();              
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRepUserId() {
        return repUserId;
    }

    public void setRepUserId(String repUserId) {
        this.repUserId = repUserId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public List<CityGeocode> getCities() {
        return cities;
    }

    public void setCities(List<CityGeocode> cities) {
        this.cities = cities;
    }

    public void addCity(CityGeocode cg) {
        cities.add(cg);
    }

    public boolean removeCity(CityGeocode cg) {
        return cities.remove(cg);
    }

    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{\n");
        json.append("id: ").append(enquote(id)).append(",\n");
        json.append("name: ").append(enquote(name)).append(",\n");
        json.append("country: ").append(enquote(country)).append(",\n");
        json.append("state: ").append(enquote(state)).append(",\n");
        json.append("groupId: ").append(enquote(groupId)).append(",\n");
        json.append("repUserId: ").append(enquote(repUserId)).append(",\n");
        json.append("cities: [\n");

        for (CityGeocode cg : cities) {
            json.append(cg.toJson()).append(",\n");
        }

        if (cities.size() > 0) {
            json.setLength(json.length() - 2);
        }

        json.append("\n]}");

        return json.toString();        
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Territory");
        sb.append("{id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", state='").append(state).append('\'');
        sb.append(", country='").append(country).append('\'');
        sb.append(", repUserId='").append(repUserId).append('\'');
        sb.append(", groupId='").append(groupId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Territory territory = (Territory) o;

        if (id != null ? !id.equals(territory.id) : territory.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
