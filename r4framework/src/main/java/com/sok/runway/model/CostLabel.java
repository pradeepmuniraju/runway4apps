/*
 * User: Behrang
 * Date: Jun 16, 2010
 * Time: 1:07:43 PM 
 */
package com.sok.runway.model;

public class CostLabel extends AbstractModel {

    private String id;
    private String categoryId;
    private String label;
    private String description;
    private String defaultCost;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultCost() {
        return defaultCost;
    }

    public void setDefaultCost(String defaultCost) {
        this.defaultCost = defaultCost;
    }

    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{\n");
        json.append("id: \"").append(escapeHtml(id)).append("\",\n");
        json.append("categoryId: \"").append(escapeHtml(categoryId)).append("\",\n");
        json.append("label: \"").append(escapeHtml(label)).append("\",\n");
        json.append("description: \"").append(escapeHtml(description)).append("\",\n");
        json.append("defaultCost: \"").append(escapeHtml(defaultCost)).append("\"\n}");
        return json.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CostLabel costLabel = (CostLabel) o;

        if (id != null ? !id.equals(costLabel.id) : costLabel.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
