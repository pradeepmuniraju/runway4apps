/*
 * User: Behrang
 * Date: Jun 9, 2010
 * Time: 4:55:05 PM 
 */
package com.sok.runway.model;

import java.io.Serializable;

public class Group extends AbstractModel {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{ id: ").append(enquote(id)).append(", ");
        json.append("name: ").append(enquote(name)).append("}");
        return json.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (id != null ? !id.equals(group.id) : group.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
