/*
 * User: Behrang
 * Date: Jun 9, 2010
 * Time: 8:09:00 PM
 */
package com.sok.runway.model;

public interface Geocodable {

    String getCountry();

    String getState();

    String getCity();

}
