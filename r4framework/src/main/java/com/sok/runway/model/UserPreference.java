/*
 * User: Behrang
 * Date: Jul 13, 2010
 * Time: 1:32:06 PM 
 */
package com.sok.runway.model;

public class UserPreference extends AbstractModel {

    private String id;
    private String userId;
    private String preferenceKey;
    private String preferenceValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPreferenceKey() {
        return preferenceKey;
    }

    public void setPreferenceKey(String preferenceKey) {
        this.preferenceKey = preferenceKey;
    }

    public String getPreferenceValue() {
        return preferenceValue;
    }

    public void setPreferenceValue(String preferenceValue) {
        this.preferenceValue = preferenceValue;
    }
}
