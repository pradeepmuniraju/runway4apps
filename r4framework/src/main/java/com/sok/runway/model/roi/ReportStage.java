/*
 * User: Behrang
 * Date: Jun 30, 2010
 * Time: 6:01:50 PM 
 */
package com.sok.runway.model.roi;

import com.sok.runway.model.Event;

import java.util.ArrayList;
import java.util.List;

public class ReportStage {

    public static final String STAGE_COUNT_MODE_COMPANIES = "COMPANY";
    public static final String STAGE_COUNT_MODE_CONTACTS = "CONTACT";

    private String id;
    private String roiReportId;
    private String eventId;
    private List<StageCost> costs;

    /**
     * <code>stageCountMode</code> can be one of
     * <ul>
     *  <li><code>STAGE_COUNT_MODE_COMPANIES</code></li>
     *  <li><code>STAGE_COUNT_MODE_CONTACTS</code></li>
     * </ul> 
     */
    private String stageCountMode;
    private Integer displayIndex;

    public ReportStage() {
        costs = new ArrayList<StageCost>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoiReportId() {
        return roiReportId;
    }

    public void setRoiReportId(String roiReportId) {
        this.roiReportId = roiReportId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getStageCountMode() {
        return stageCountMode;
    }

    public void setStageCountMode(String stageCountMode) {
        this.stageCountMode = stageCountMode;
    }

    public List<StageCost> getCosts() {
        return costs;
    }

    public void setCosts(List<StageCost> costs) {
        this.costs = costs;
    }

    public void addCost(StageCost cost) {
        costs.add(cost);
    }

    public Integer getDisplayIndex() {
        return displayIndex;
    }

    public void setDisplayIndex(Integer displayIndex) {
        this.displayIndex = displayIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReportStage that = (ReportStage) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
