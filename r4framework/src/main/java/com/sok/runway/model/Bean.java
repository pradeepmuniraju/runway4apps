/*
 * User: Behrang
 * Date: Jun 30, 2010
 * Time: 1:56:03 PM 
 */
package com.sok.runway.model;

import org.json.simple.JSONObject;

import java.sql.Timestamp;

public class Bean extends AbstractModel {

    private String id;
    private String userId;
    private String groupId;
    private String campaignId;
    private String holderId;
    private String type;
    private String name;
    private String description;
    private String bean;
    private String shared;
    private Timestamp createdDate;

    private String ownerFirstName;
    private String ownerLastName;
    private String holderFirstName;
    private String holderLastName;
    private String campaignName;
    private Integer userCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getHolderId() {
        return holderId;
    }

    public void setHolderId(String holderId) {
        this.holderId = holderId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBean() {
        return bean;
    }

    public void setBean(String bean) {
        this.bean = bean;
    }

    public String getShared() {
        return shared;
    }

    public void setShared(String shared) {
        this.shared = shared;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }

    public String getOwnerLastName() {
        return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }

    public String getHolderFirstName() {
        return holderFirstName;
    }

    public void setHolderFirstName(String holderFirstName) {
        this.holderFirstName = holderFirstName;
    }

    public String getHolderLastName() {
        return holderLastName;
    }

    public void setHolderLastName(String holderLastName) {
        this.holderLastName = holderLastName;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bean bean = (Bean) o;

        if (id != null ? !id.equals(bean.id) : bean.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @SuppressWarnings("unchecked")
    public String toJson() {
        JSONObject json = new JSONObject();
        // json.put("bean", bean);
//        json.put("campaignId", campaignId);
//        json.put("createdDate", createdDate);
//        json.put("description", description);
//        json.put("groupId", groupId);
//        json.put("holderId", holderId);
        json.put("id", id);
        json.put("name", name);
//        json.put("shared", shared);
        json.put("type", type);
//        json.put("userId", userId);
//        json.put("ownerFirstName", ownerFirstName);
//        json.put("ownerLastName", ownerLastName);
//        json.put("holderFirstName", holderFirstName);
//        json.put("holderLastName", holderLastName);
//        json.put("campaignName", campaignName);
//        json.put("userCount", userCount);

        return json.toString();
    }
}
