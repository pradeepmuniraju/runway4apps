/*
 * User: Behrang
 * Date: Jun 8, 2010
 * Time: 3:00:16 PM
 */
package com.sok.runway.model;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.Serializable;

public class CityGeocode extends AbstractModel {

    private String id;
    private String country;
    private String state;
    private String city;
    private String latitude;
    private String longitude;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{\n");
        json.append("id: ").append(enquote(id)).append(",\n");
        json.append("country: ").append(enquote(country)).append(",\n");
        json.append("state: ").append(enquote(state)).append(",\n");
        json.append("city: ").append(enquote(city)).append(",\n");
        json.append("lat: ").append(enquote(latitude)).append(",\n");
        json.append("lng: ").append(enquote(longitude)).append("\n");
        json.append("}");

        return json.toString();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CityGeocode");
        sb.append("{id='").append(id).append('\'');
        sb.append(", country='").append(country).append('\'');
        sb.append(", state='").append(state).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", latitude='").append(latitude).append('\'');
        sb.append(", longitude='").append(longitude).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CityGeocode that = (CityGeocode) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
