/*
 * User: Behrang
 * Date: Jul 29, 2010
 * Time: 2:31:20 PM 
 */
package com.sok.runway.model;

import java.util.ArrayList;
import java.util.List;

public class MasterTemplate extends AbstractModel {

    private String id;
    private String name;
    private List<Template> templates;

    public MasterTemplate() {
        templates = new ArrayList<Template>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(List<Template> templates) {
        this.templates = templates;
        
    }

    public void addTemplate(Template temp) {
        templates.add(temp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MasterTemplate that = (MasterTemplate) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
