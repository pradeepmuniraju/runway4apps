package com.sok.runway.model;

public class Profile extends AbstractModel {
    private String id;
    private String name;
    private String description;
    private String groupID;
    private String profileType;
   
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the groupID
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * @param groupID the groupID to set
     */
    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    /**
     * @return the profileType
     */
    public String getProfileType() {
        return profileType;
    }

    /**
     * @param profileType the profileType to set
     */
    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String toJson() {
       StringBuilder json = new StringBuilder();
       json.append("{ id: ").append(enquote(id)).append(", ");
       json.append("name: ").append(enquote(name)).append("}");
       json.append("description: ").append(enquote(description)).append("}");
       json.append("groupID: ").append(enquote(groupID)).append("}");
       json.append("profileType: ").append(enquote(profileType)).append("}");
       return json.toString();
    }
   
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
   
        Profile profile = (Profile) o;
   
        if (id != null ? !id.equals(profile.id) : profile.id != null) return false;
   
        return true;
    }
   
    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
   
    @Override
    public String toString() {
        return "Profile{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", groupID='" + groupID + '\'' +
                ", profileType='" + profileType + '\'' +
                '}';
    }
}
