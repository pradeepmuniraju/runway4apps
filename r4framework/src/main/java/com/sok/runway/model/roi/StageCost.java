/*
 * User: Behrang
 * Date: Jun 30, 2010
 * Time: 6:15:33 PM 
 */
package com.sok.runway.model.roi;

import com.sok.runway.model.CostLabel;

import java.math.BigDecimal;

public class StageCost {

    public static final String ACTUAL_UNITS_COUNT_TYPE_MANUAL = "MANUAL";
    public static final String ACTUAL_UNITS_COUNT_TYPE_STAGE_COUNT = "STAGE_COUNT";

    private String id;
    private String reportStageId;
    private String costLabelId;
    private Double unitCost;
    private Integer forecastedUnitsCount;
    private String actualUnitsCountType;
    private Integer actualUnitsCount;
    private Integer displayIndex;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportStageId() {
        return reportStageId;
    }

    public void setReportStageId(String reportStageId) {
        this.reportStageId = reportStageId;
    }

    public String getCostLabelId() {
        return costLabelId;
    }

    public void setCostLabelId(String costLabelId) {
        this.costLabelId = costLabelId;
    }

    public Double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(Double unitCost) {
        this.unitCost = unitCost;
    }

    public Integer getForecastedUnitsCount() {
        return forecastedUnitsCount;
    }

    public void setForecastedUnitsCount(Integer forecastedUnitsCount) {
        this.forecastedUnitsCount = forecastedUnitsCount;
    }

    public Integer getActualUnitsCount() {
        return actualUnitsCount;
    }

    public void setActualUnitsCount(Integer actualUnitsCount) {
        this.actualUnitsCount = actualUnitsCount;
    }

    public String getActualUnitsCountType() {
        return actualUnitsCountType;
    }

    public void setActualUnitsCountType(String actualUnitsCountType) {
        this.actualUnitsCountType = actualUnitsCountType;
    }

    public Integer getDisplayIndex() {
        return displayIndex;
    }

    public void setDisplayIndex(Integer displayIndex) {
        this.displayIndex = displayIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StageCost stageCost = (StageCost) o;

        if (id != null ? !id.equals(stageCost.id) : stageCost.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
