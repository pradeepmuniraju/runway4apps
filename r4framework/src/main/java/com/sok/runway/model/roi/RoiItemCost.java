/*
 * User: Behrang
 * Date: Jul 2, 2010
 * Time: 7:48:00 PM 
 */
package com.sok.runway.model.roi;

public class RoiItemCost {

    private String id;
    private String reportItemId;
    private String stageCostId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportItemId() {
        return reportItemId;
    }

    public void setReportItemId(String reportItemId) {
        this.reportItemId = reportItemId;
    }

    public String getStageCostId() {
        return stageCostId;
    }

    public void setStageCostId(String stageCostId) {
        this.stageCostId = stageCostId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoiItemCost that = (RoiItemCost) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
