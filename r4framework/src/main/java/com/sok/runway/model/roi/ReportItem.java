/*
 * User: Behrang
 * Date: Jun 30, 2010
 * Time: 6:04:30 PM 
 */
package com.sok.runway.model.roi;

import java.util.ArrayList;
import java.util.List;

public class ReportItem {

    public static final String COUNT_TYPE_BEAN = "BEAN";
    public static final String COUNT_TYPE_MANUAL = "MANUAL";

    private String id;
    private String roiReportId;
    private String countBeanId;
    private String name;
    private Integer count;
    private Integer displayIndex;
    private List<String> reportItemCostCoords;
    private List<RoiItemCost> reportItemCosts;

    /**
     * <code>countType</code> can be one of
     * <ul>
     *  <li><code>COUNT_TYPE_BEAN</code></li>
     *  <li><code>COUNT_TYPE_MANUAL</code></li>
     * </ul>
     */
    private String countType;

    public ReportItem() {
        reportItemCostCoords = new ArrayList<String>();
        reportItemCosts = new ArrayList<RoiItemCost>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoiReportId() {
        return roiReportId;
    }

    public void setRoiReportId(String roiReportId) {
        this.roiReportId = roiReportId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getCountBeanId() {
        return countBeanId;
    }

    public void setCountBeanId(String countBeanId) {
        this.countBeanId = countBeanId;
    }

    public String getCountType() {
        return countType;
    }

    public void setCountType(String countType) {
        this.countType = countType;
    }

    public List<String> getReportItemCostCoords() {
        return reportItemCostCoords;
    }

    public void setReportItemCostCoords(List<String> reportItemCostCoords) {
        this.reportItemCostCoords = reportItemCostCoords;
    }

    public void addReportItemCostId(String costId) {
        reportItemCostCoords.add(costId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RoiItemCost> getReportItemCosts() {
        return reportItemCosts;
    }

    public void setReportItemCosts(List<RoiItemCost> reportItemCosts) {
        this.reportItemCosts = reportItemCosts;
    }

    public void addReportItemCost(RoiItemCost cost) {
        reportItemCosts.add(cost);
    }

    public Integer getDisplayIndex() {
        return displayIndex;
    }

    public void setDisplayIndex(Integer displayIndex) {
        this.displayIndex = displayIndex;
    }

    /**
     * Computes the cost associated with this Roi Item according to the <code>report</code>
     *
     * <code>report</code> must be the parent of this roi item.
     *
     * @param report The parent of this roi item.
     * @return The cost associated with this roi item.
     */
    public Double computeCost(RoiReport report) {
        List<StageCost> reportCosts = new ArrayList<StageCost>();
        for (ReportStage stages : report.getStages()) {
            for (StageCost cost : stages.getCosts()) {
                for (RoiItemCost roiItemCost : getReportItemCosts()) {
                    if (roiItemCost.getStageCostId().equals(cost.getId())) {
                        reportCosts.add(cost);
                    }
                }
            }
        }

        Double totalCost = 0.0;
        for (StageCost stageCost : reportCosts) {
            // if (stageCost.getActualUnitsCountType() == StageCost.)
        }

        return totalCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReportItem that = (ReportItem) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
