/*
 * User: Behrang
 * Date: Jun 9, 2010
 * Time: 5:14:40 PM 
 */
package com.sok.runway.model;

public class User extends AbstractModel {

    private String id;
    private String firstName;
    private String lastName;
    private String username;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;

        return true;
    }

    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{ id: ").append(enquote(id)).append(", ");
        json.append(" firstName: ").append(enquote(firstName)).append(", ");
        json.append(" lastName: ").append(enquote(lastName)).append(", ");
        json.append(" username: ").append(enquote(username)).append("}");
        return json.toString();
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
