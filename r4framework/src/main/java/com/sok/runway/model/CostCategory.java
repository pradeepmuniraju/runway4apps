/*
 * User: Behrang
 * Date: Jun 15, 2010
 * Time: 7:14:32 PM 
 */
package com.sok.runway.model;

import java.util.ArrayList;
import java.util.List;

public class CostCategory extends AbstractModel {

    private String id;
    private String label;
    private String description;
    private List<CostLabel> costLabels;

    public CostCategory() {
        costLabels = new ArrayList<CostLabel>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CostLabel> getCostLabels() {
        return costLabels;
    }

    public void setCostLabels(List<CostLabel> costLabels) {
        this.costLabels = costLabels;
    }

    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{\n");
        json.append("id: \"").append(escapeHtml(id)).append("\",\n");
        json.append("label: \"").append(escapeHtml(label)).append("\",\n");
        json.append("desc: \"").append(escapeHtml(description)).append("\"}");
        return json.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CostCategory that = (CostCategory) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
