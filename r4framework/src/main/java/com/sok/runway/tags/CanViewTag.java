/*
 * CanViewTag.java
 *
 * Created on May 1, 2007, 5:13 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.sok.runway.tags;

import javax.servlet.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import com.sok.runway.*;
import com.sok.runway.security.*;
import com.sok.framework.*;
/**
 *
 * @author root
 */
public class CanViewTag extends TagSupport
{
   private String name = null;
   
   private String type = null;
   
   private String path = null;
   
   private String grouprelation = null;
   
   private boolean debug = false;
   
   private JspWriter out = null;
   
   public void setBeanName(String name)
   {
      this.name = name;
   }
   
   public void setBeanType(String type)
   {
      this.type = type;
   }
   
   public void setErrorIncludePath(String path)
   {
      this.path = path;
   }
   
   public void setGroupRelation(String grouprelation)
   {
      this.grouprelation = grouprelation;
   }   
   
   public int doStartTag()
   {
      boolean canview = false;
      boolean loaded = false;
      out = pageContext.getOut();
      Object obj = pageContext.getAttribute(name, PageContext.REQUEST_SCOPE);
      UserBean currentuser = (UserBean)pageContext.getAttribute(SessionKeys.CURRENT_USER, PageContext.SESSION_SCOPE);
      
      if(currentuser!=null && obj!=null)
      {
         if(obj instanceof GenRow)
         {
            GenRow row = (GenRow)obj;
            loaded = row.isSuccessful();
            if(loaded)
            {
               if(grouprelation!=null)
               {
                  canview = currentuser.getUser().canView(row, grouprelation)  || currentuser.getUser().canViewAssigned(row, grouprelation, row.getString("RepUserID"));
               }
               else
               {
                  canview = currentuser.canView(row) || currentuser.canViewAssigned(row);
               }
            }
         }
         else if(obj instanceof TableData)
         {
            TableData data = (TableData)obj;
            if(data.getTableSpec().hasField("CreatedDate"))
            {
               loaded = data.get("CreatedDate")!=null;
            }
            else
            {
               loaded = data.getError().length()==0;
            }
            if(loaded)
            {
               if(grouprelation!=null)
               {
                  canview = currentuser.getUser().canView(data, grouprelation) || currentuser.getUser().canViewAssigned(data, grouprelation, data.getString("RepUserID"));
               }
               else
               {
                  canview = currentuser.canView(data) || currentuser.canViewAssigned(data);
               }
            }
         }        
         else if(obj instanceof SecuredEntity)
         {
            SecuredEntity entity = (SecuredEntity)obj;
            loaded = entity.isLoaded();
            if(loaded)
            {
               if(grouprelation!=null)
               {
                  canview = currentuser.getUser().canView(entity.getRecord(), grouprelation);
               }
               else
               {
                  canview = currentuser.getUser().canView(entity);
               }
            }
         }         

      }
      if(path == null)
      {
         if(!loaded)
         {
            printNotLoadedError();
         }
         else if(!canview)
         {
            printCantViewError();
         }
      }
      else
      {
         if(!loaded)
         {
            try
            {
               pageContext.include(path+"noexist.jsp");
            }
            catch(Exception e)
            {
               printNotLoadedError();
            }
         }
         else if(!canview)
         {
            
            try
            {
               pageContext.include(path+"noview.jsp");
            }
            catch(Exception e)
            {
               printCantViewError();
            }
         }
      }
      
      if(!loaded || !canview)
      {
         return(SKIP_BODY);
      }
      return(EVAL_BODY_INCLUDE);
   }
   
   protected void printNotLoadedError()
   {
      print(new StringBuffer().append("<div class=\"pageerror\">The ")
      .append(type)
      .append(" you are looking for does not exists or has been deleted.</div>").toString());
   }
   
   protected void printCantViewError()
   {
      print(new StringBuffer().append("<div class=\"pageerror\">You do not have permission to view this ")
      .append(type)
      .append("</div>").toString());
   }
   
   protected void print(String str)
   {
      try
      {
         out.print(str);
      }
      catch(Exception e)
      {}
   }
   
   public int doEndTag()
   {
      return(EVAL_PAGE);
   }
   
   public void setDebug(boolean debug)
   {
      this.debug = debug;
   }
   
   public void release()
   {
      out = null;
      name = null;
      type = null;
      debug = false;
      path = null;
      grouprelation = null;
   }
}
