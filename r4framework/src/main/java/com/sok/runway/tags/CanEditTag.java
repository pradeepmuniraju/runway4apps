/*
 * CanViewTag.java
 *
 * Created on May 1, 2007, 5:13 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.sok.runway.tags;

import javax.servlet.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import com.sok.runway.*;
import com.sok.runway.security.SecuredEntity;
import com.sok.framework.*;
/**
 *
 * @author root
 */
public class CanEditTag extends TagSupport
{
   private String name = null;
   
   private String type = null;
   
   private String path = null;
   
   private String grouprelation = null;
   
   private boolean debug = false;
   
   private JspWriter out = null;
   
   public void setBeanName(String name)
   {
      this.name = name;
   }
   
   public void setBeanType(String type)
   {
      this.type = type;
   }
   
   public void setErrorIncludePath(String path)
   {
      this.path = path;
   }
   
   public void setGroupRelation(String grouprelation)
   {
      this.grouprelation = grouprelation;
   }     
   
   public int doStartTag()
   {
      boolean canedit = false;
      boolean loaded = false;
      out = pageContext.getOut();
      Object obj = pageContext.getAttribute(name, PageContext.REQUEST_SCOPE);
      UserBean currentuser = (UserBean)pageContext.getAttribute(SessionKeys.CURRENT_USER, PageContext.SESSION_SCOPE);
      
      if(obj instanceof GenRow)
      {
         GenRow row = (GenRow)obj;
         loaded = row.isSuccessful();
         if(loaded)
         {
            if(grouprelation!=null)
            {
               canedit = currentuser.getUser().canEdit(row, grouprelation);
            }
            else
            {
               canedit = currentuser.canEdit(row);
            }
         }
      }
      else if(obj instanceof TableData)
      {
         TableData data = (TableData)obj;
         if(data.getTableSpec().hasField("CreatedDate"))
         {
            loaded = data.get("CreatedDate")!=null;
         }
         else
         {
            loaded = data.getError().length()==0;
         }
         if(loaded)
         {
            if(grouprelation!=null)
            {
               canedit = currentuser.getUser().canEdit(data, grouprelation);
            }
            else
            {
               canedit = currentuser.canEdit(data);
            }
         }
      }
      else if(obj instanceof SecuredEntity)
      {
         SecuredEntity entity = (SecuredEntity)obj;
         loaded = entity.isLoaded();
         if(grouprelation!=null)
         {
            canedit = currentuser.getUser().canEdit(entity.getRecord(), grouprelation);
         }
         else
         {
            canedit = currentuser.getUser().canEdit(entity);
         }
      }         

      if(path == null)
      {
         if(!loaded)
         {
            printNotLoadedError();
         }
         else if(!canedit)
         {
            printCantEditError();
         }
      }
      else
      {
         if(!loaded)
         {
            try
            {
               pageContext.include(path+"noexist.jsp");
            }
            catch(Exception e)
            {
               printNotLoadedError();
            }
         }
         else if(!canedit)
         {
            
            try
            {
               pageContext.include(path+"noedit.jsp");
            }
            catch(Exception e)
            {
               printCantEditError();
            }
         }
      }
      
      if(!loaded || !canedit)
      {
         return(SKIP_BODY);
      }
      return(EVAL_BODY_INCLUDE);
   }
   
   protected void printNotLoadedError()
   {
      print(new StringBuffer().append("<div class=\"pageerror\">The ")
      .append(type)
      .append(" you are looking for does not exists or has been deleted.</div>").toString());
   }
   
   protected void printCantEditError()
   {
      print(new StringBuffer().append("<div class=\"pageerror\">You do not have permission to edit this ")
      .append(type)
      .append("</div>").toString());
   }
   
   protected void print(String str)
   {
      try
      {
         out.print(str);
      }
      catch(Exception e)
      {}
   }
   
   public int doEndTag()
   {
      return(EVAL_PAGE);
   }
   
   public void setDebug(boolean debug)
   {
      this.debug = debug;
   }
   
   public void release()
   {
      out = null;
      name = null;
      type = null;
      debug = false;
      path = null;
   }
}
