package com.sok.runway;

import java.io.File;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.VelocityEngine;

import com.sok.framework.ActionBean;
import com.sok.framework.DateToken;
import com.sok.framework.EmailAttachmentBuilder;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.KeyMaker;
import com.sok.framework.MailerBean;
import com.sok.framework.RowArrayBean;
import com.sok.framework.RowBean;
import com.sok.framework.RowSetBean;
import com.sok.framework.StringUtil;
import com.sok.framework.TableBean;
import com.sok.framework.TableData;
import com.sok.framework.VelocityManager;
import com.sok.framework.ViewSpec;
import com.sok.runway.email.AbstractEmailer;

public class EmailSender {
	
	public static final int MODE_SEND = 0;
	public static final int MODE_SAVE = 1;
	public static final int MODE_PREVIEW = 2;
	public static final int MODE_PREVIEW_TEXT = 3;
	
	String referrertypeid = "F470C7B5158025110118B5B53563C861";
	String referreetypeid = "D88436A3E060C0094742929101501175";
	String statusid = null;
	String defaultFrom = null;
	String urlhome = null;
	
	private RowBean template = null;
	private RowBean newsletter = null;
	
	private RowBean contactBean = null;
	private RowBean noteBean = null;
	private RowBean formBean = null;
	private RowBean authorBean = null;
	private StringBuffer errors = null;
	
	private ServletContext context = null;
	
	public EmailSender(RowBean formBean, ServletContext context) {
		
		this.formBean = formBean;
		init(context);
	}
	
	public EmailSender(HttpServletRequest request, ServletContext context) {
	   
      this.formBean = new TableBean();
		this.formBean.parseRequest(request);
		
		init(context);
   }

   private void init(ServletContext context) {
		statusid = InitServlet.getSystemParams().getProperty("WebFormStatusID");
		
		defaultFrom = InitServlet.getSystemParams().getProperty("DefaultEmail");
		
		urlhome = context.getInitParameter("URLHome");
		
		contactBean = new TableBean();
		contactBean.setJndiName(ActionBean.getJndiName());
		contactBean.setViewSpec("{email/}ContactEmailView");
		contactBean.setAction(ActionBean.SELECT);
		
		noteBean = new TableBean();
		noteBean.setJndiName(ActionBean.getJndiName());
		noteBean.setViewSpec("email/NoteEmailView");
		noteBean.setAction(ActionBean.INSERT);
		
		loadAuthorBean(formBean.getString("RepUserID"));
		/*authorBean = new TableBean();
		authorBean.setJndiName(ActionBean.getJndiName());
		authorBean.setViewSpec("UserView");
		authorBean.setColumn("UserID", formBean.getString("RepUserID"));
		authorBean.setAction(ActionBean.SELECT);
		authorBean.doAction();*/
		
		errors = new StringBuffer();
		
		this.context = context;
   }
	
	public void setCC(String cc) {
	   formBean.setColumn("CC", cc);
	}
	
	public void setBCC(String bcc) {
	   formBean.setColumn("BCC", bcc);
	}
	
	public void setTo(String to) {
	   formBean.setColumn("TO", to);
	}
	
	public void setFrom(String from) {
	   formBean.setColumn("FROM", from);
	}
	/**
	 * Returns NoteID of sent Email, null if errors occur.
	 *
	public String sendScheduledEmail(String templateID, String contactID) {	
		
		try {
			UserBean currentUser = getSystemUser();
			
			getContactBean(contactID, currentUser);
			
			formBean.put("CompanyID", contact.getString("CompanyID"));
			formBean.put("ContactID", contact.getString("ContactID"));
			formBean.put("Type", "Email");
			formBean.put("Completed", "Yes");
			formBean.put("NoteDate","NOW");
			formBean.put("NoteTime","NOW"); //incase is ever used again
			formBean.put("RepUserID", contact.getString("RepUserID"));
			formBean.put("GroupID", contact.getString("GroupID"));
			
			////////////////////////////////////
		
			formBean.put("CMSID", referrer.getString("CMSID"));
			formBean.put("Campaign", referrer.getString("Campaign"));
			formBean.put("CampaignID", referrer.getString("CampaignID"));
			formBean.put("TO",contactBean.getString("Email"));
			formBean.put("FROM",referrer.getString("Email"));
			
			///////////////////////////////////
			
			getTemplateBean(templateID.getString("TemplateID"), ""); 
			
			formBean.put("TemplateID", template.getString("TemplateID"));
			formBean.put("MasterTemplateID", template.getString("MasterTemplateID"));
			formBean.put("Subject", template.getString("Subject"));
			formBean.put("Headline", template.getString("Headline"));
			formBean.put("Body", template.getString("Body"));
	
			if (template.getString("TemplateID").length() != 0) {
				RowSetBean imagelist = new RowSetBean();
				imagelist.setViewSpec("LinkedDocumentView");
				imagelist.setJndiName(ActionBean.getJndiName());
				imagelist.setColumn("TemplateID|1", template.getString("TemplateID"));
				imagelist.setColumn("MasterTemplateID|1", template.getString("MasterTemplateID"));
				imagelist.setColumn("DefaultDocument","Y");
				imagelist.sortBy("MasterTemplateID",0);
				imagelist.sortOrder("DESC",0);
				imagelist.generateSQLStatment();
				imagelist.getResults();
		
				while (imagelist.getNext()) {
					formBean.put("Image"+imagelist.getString("Location"), urlhome + "/" + imagelist.getString("FilePath"));
				}
				imagelist.close();
			}
			
			return doEmail(currentUser, MODE_SEND);
		}
		catch (Exception e) {
			errors.append(ActionBean.writeStackTraceToString(e));
			return null;
		}
	}*/
	
	public void setTemplateDefaults(TableData template) {
		
		//System.out.println("Inside setTemplateDefaults%%%%%%%%%%%%%%%%%%"+template.getString("Subject")+"::"+template.getString("TemplateID"));
		
		   formBean.put("MasterTemplateID", template.getString("MasterTemplateID"));
			formBean.put("Type", "Email");
			formBean.put("Completed", "Yes");
			formBean.put("NoteDate","NOW");
			formBean.put("NoteTime","NOW");
			if (formBean.getString("Subject").length() == 0) {
			   formBean.put("Subject", template.getString("Subject"));
			}
			if (formBean.getString("Headline").length() == 0) {
			   formBean.put("Headline", template.getString("Headline"));
			}
			formBean.put("Body", template.getString("Body"));
			
//			RowSetBean imagelist = new RowSetBean();
//			imagelist.setViewSpec("LinkedDocumentView");
//			imagelist.setJndiName(ActionBean.getJndiName());
//			imagelist.setColumn("TemplateID|1",template.getString("TemplateID"));
//			imagelist.setColumn("MasterTemplateID|1",template.getString("MasterTemplateID"));
//			imagelist.setColumn("DefaultDocument","Y");
//			imagelist.sortBy("MasterTemplateID",0);
//			imagelist.sortOrder("DESC",0);
//			imagelist.generateSQLStatment();
//			imagelist.getResults();
//
//			while (imagelist.getNext()) {
//				formBean.put("Image"+imagelist.getString("Location"), urlhome + "/" + imagelist.getString("FilePath"));
//			}
//		   imagelist.close();
		}
	
	public void setReferTemplateDefaults(TableData template) {		
		//System.out.println("Inside setReferTemplateDefaults%%%%%%%%%%%%%%%%%%"+template.getString("Subject")+"::"+template.getString("TemplateID"));
	    formBean.put("MasterTemplateID", template.getString("MasterTemplateID"));
		formBean.put("Type", "Email");
		formBean.put("Completed", "Yes");
		formBean.put("NoteDate","NOW");
		formBean.put("NoteTime","NOW");
		
		if (formBean.getString("CMSID").length() > 0) {
			if (formBean.getString("Subject").length() == 0) {
			   GenRow cmsTemplate = new GenRow();
			   cmsTemplate.setViewSpec("CMSTemplateView");
			   cmsTemplate.setParameter("CMSID",template.getString("TemplateID"));
			   cmsTemplate.doAction("selectfirst");
			   
			   //System.out.println("cmsTemplate%%%%%%%%%%%%%%%%%%"+cmsTemplate.getStatement()+"::"+cmsTemplate.getString("Subject"));
			   
			   formBean.put("Subject", cmsTemplate.getString("Subject"));
			}
		}else{		
			if (formBean.getString("Subject").length() == 0) {
			   formBean.put("Subject", template.getString("Subject"));
			}
		}
		if (formBean.getString("Headline").length() == 0) {
		   formBean.put("Headline", template.getString("Headline"));
		}
		
		
		String actualBody = template.getString("Body");
        // sometimes we get a script that stops links being used, this is good for design, but not for emails
		try {
			int s = actualBody.indexOf("<script");
			int e = actualBody.lastIndexOf("</script>");
			if (s > 0 && e > 0) {
				actualBody = actualBody.substring(0, s)
						+ actualBody.substring(e + "</script>".length());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		String templateBody = evaluateSTAFBody(template.getString("SendToAFriendMsg"), actualBody);
		
		
		formBean.put("Body", templateBody);
		
		
//		
//		RowSetBean imagelist = new RowSetBean();
//		imagelist.setViewSpec("LinkedDocumentView");
//		imagelist.setJndiName(ActionBean.getJndiName());
//		imagelist.setColumn("TemplateID|1",template.getString("TemplateID"));
//		imagelist.setColumn("MasterTemplateID|1",template.getString("MasterTemplateID"));
//		imagelist.setColumn("DefaultDocument","Y");
//		imagelist.sortBy("MasterTemplateID",0);
//		imagelist.sortOrder("DESC",0);
//		imagelist.generateSQLStatment();
//		imagelist.getResults();
//
//		while (imagelist.getNext()) {
//			formBean.put("Image"+imagelist.getString("Location"), urlhome + "/" + imagelist.getString("FilePath"));
//		}
//	   imagelist.close();
	}
	
	
	private String evaluateSTAFBody(String stafMsg, String actualBody){
		//System.out.println("Inside evaluateSTAFBody");
		
		String templateBodyReplace = actualBody;
		
		String optinlink = context.getInitParameter("URLHome") + "/actions/optin.jsp?ID=${NoteID}";
		String templateBody = "<div style=\"background-color: #F9F2DB; border: 1px solid #D3B447; font-size: 12px; color: #333; font-family: Arial, Helvetica, sans-serif; font-weight: normal;\"><div style=\"padding: 10px; margin: 10px;\">"; 
		if(stafMsg != null && !"".equals(stafMsg)){
			templateBody = templateBody + stafMsg ;
		}else{
			templateBody = templateBody + "Dear ${FirstName}, Your friend ${RefFirstName} ${RefLastName} thought you might be interested in this email." ;
		}
		templateBody = templateBody + " You can subscribe to more communications from ${setup.Company} by <a href=\"" + optinlink +"\" style=\"color:#blue; text-decoration:underline;\">clicking here</a>.";
		templateBody = templateBody + "</div></div><br/>";
		
		int indexOfDear = (actualBody).toLowerCase().indexOf("dear");
		
		if(indexOfDear == -1){
			indexOfDear = (actualBody).toLowerCase().indexOf("hello");
		}
		
		if(indexOfDear > 0){
			String strIndexOfPTag = (actualBody).substring(0,indexOfDear);
			
			int lastIndexOfPTag = strIndexOfPTag.lastIndexOf("<p");
			int lastIndexOfBRTag = strIndexOfPTag.lastIndexOf("<br");
			
			if((lastIndexOfPTag >= 0 && lastIndexOfBRTag >=0 && lastIndexOfPTag > lastIndexOfBRTag ) || (lastIndexOfPTag >= 0 && lastIndexOfBRTag == -1) ){
				indexOfDear = indexOfDear - (strIndexOfPTag.length() - lastIndexOfPTag);
				
				int endPTag =  (actualBody).indexOf("</p>",indexOfDear);
				
				if(endPTag >=0){
					templateBodyReplace = (actualBody).substring(indexOfDear,(endPTag+4));
					
					templateBodyReplace = StringUtil.replace(actualBody,templateBodyReplace,templateBody);
					
				}
			}else{
				int indexOfBRTag = (actualBody).indexOf("<br",indexOfDear);
				int indexOfPTag = (actualBody).indexOf("<p",indexOfDear);
				
				
				if( (indexOfBRTag >= 0 && indexOfPTag >=0 && indexOfBRTag < indexOfPTag ) || (indexOfBRTag >= 0 && indexOfPTag == -1) ){
					int endBrTag = (actualBody).indexOf(">",indexOfBRTag);
					
					String strBetDearAndBr = (actualBody).substring(indexOfDear,(endBrTag+1));
					
					templateBodyReplace = StringUtil.replace(actualBody,strBetDearAndBr,templateBody);
					
				}else{				
					if(indexOfPTag > 0){
						String strBetDearAndP = (actualBody).substring(0,indexOfPTag);
						
						templateBodyReplace = StringUtil.replace(actualBody,strBetDearAndP,templateBody);
						
					}
				}
			}
		} else if(indexOfDear == 0) {
			int indexOfBRTag = (actualBody).toLowerCase().indexOf("<br");
			int indexOfPTag = (actualBody).toLowerCase().indexOf("<p");
			
			
			if( (indexOfBRTag >= 0 && indexOfPTag >=0 && indexOfBRTag < indexOfPTag ) || (indexOfBRTag >= 0 && indexOfPTag == -1) ){
				int endBrTag = (actualBody).indexOf(">",indexOfBRTag);
				
				String strBetDearAndBr = (actualBody).substring(indexOfDear,(endBrTag+1));
				
				templateBodyReplace = StringUtil.replace(actualBody,strBetDearAndBr,templateBody);
				
			}else{				
				if(indexOfPTag > 0){
					String strBetDearAndP = (actualBody).substring(0,indexOfPTag);
					
					templateBodyReplace = StringUtil.replace(actualBody,strBetDearAndP,templateBody);
					
				}
			}
		} else {
			templateBodyReplace = templateBody + actualBody;
		}
		
		
		//templateBody = templateBody + templateBodyReplace;
		//System.out.println("templateBody %%%%%%%%%%%%%%%%%"+templateBodyReplace);
		return (templateBodyReplace);
	}
	
	/**
	 * Returns NoteID of sent Email, null if errors occur.
	 */
	public String sendReferProductEmail(String contactID, String productID, String templateID) {
		try {
			RowBean referrer = new RowBean();
			referrer.setJndiName(ActionBean.getJndiName());
			referrer.setViewSpec("ContactView");
            referrer.setColumn("ContactID", contactID);
			if (contactID.length() > 0) {
				referrer.setAction("select");
				referrer.doAction();
			}
			
			formBean.setColumn("NoteID", KeyMaker.generate());
			
			if (templateID.length()!=0) {
				UserBean currentUser = getSystemUser();
	
				RowBean contactBean = getReferContactBean(referrer, currentUser);				
				
				if (contactBean != null) {
					contactBean.put("RefFirstName", referrer.getString("FirstName"));
					contactBean.put("RefLastName", referrer.getString("LastName"));
					contactBean.put("RefCompany", referrer.getString("Company"));
					
					formBean.put("CompanyID", contactBean.getString("CompanyID"));
					formBean.put("ContactID", contactBean.getString("ContactID"));
					formBean.put("RepUserID", referrer.getString("AuthorUserID"));
					formBean.put("GroupID", referrer.getString("GroupID"));
					formBean.put("ProductIDs", productID);
	
					getTemplateBean(templateID, "");
					formBean.put("MasterTemplateID", template.getString("MasterTemplateID"));

					formBean.put("TO",contactBean.getString("Email"));
										
					setTemplateDefaults(template);
		
					return doEmail(currentUser, MODE_SEND);
					
				}
				else {
					errors.append("Error creating refered contact");
					return null;
				}
			}
			else {
				errors.append("Referrer not found, or No Template specified");
				return null;
			}
		}
		catch (Exception e) {
			errors.append(ActionBean.writeStackTraceToString(e));
			return null;
		}
	}
	
	
	
	/**
	 * Returns NoteID of sent Email, null if errors occur.
	 */
	public String sendReferEmail(String contactID, String templateID) {
		
		try {
			RowBean referrer = new RowBean();
			referrer.setJndiName(ActionBean.getJndiName());
			referrer.setViewSpec("ContactView");
			referrer.setColumn("ContactID", contactID);
			if (contactID.length() > 0) {
				referrer.setAction("select");
				referrer.doAction();
			}
			
			formBean.setColumn("NoteID", KeyMaker.generate());
			
			if (templateID.length()!=0) {
				UserBean currentUser = getSystemUser();
	
				RowBean contactBean = getReferContactBean(referrer, currentUser);				
				
				if (contactBean != null) {
					
					contactBean.put("RefFirstName", referrer.getString("FirstName"));
					contactBean.put("RefLastName", referrer.getString("LastName"));
					contactBean.put("RefCompany", referrer.getString("Company"));
					
					formBean.put("CompanyID", contactBean.getString("CompanyID"));
					formBean.put("ContactID", contactBean.getString("ContactID"));
					formBean.put("RepUserID", contactBean.getString("RepUserID"));
					formBean.put("GroupID", contactBean.getString("GroupID"));
	
					getTemplateBean(templateID, "");
					formBean.put("MasterTemplateID", template.getString("MasterTemplateID"));

					formBean.put("TO",contactBean.getString("Email"));
										
					setReferTemplateDefaults(template);
		
					return doEmail(currentUser, MODE_SEND);
					
				}
				else {
					errors.append("Error creating refered contact");
					return null;
				}
			}
			else {
				errors.append("Referrer not found, or No Template specified");
				return null;
			}
		}
		catch (Exception e) {
			errors.append(ActionBean.writeStackTraceToString(e));
			return null;
		}
	}
	
	public String sendReferEmail() {
		//System.out.println("Inside sendReferEmail");
		try {
			RowBean referrer = new RowBean();
			referrer.setJndiName(ActionBean.getJndiName());
			referrer.setViewSpec("NoteReferView");
			if (formBean.getString("NoteID").length() > 0) {
				referrer.setColumn("NoteID", formBean.getString("NoteID"));
			}
			else {
				referrer.setColumn("NoteID", formBean.getString("ID"));
			}
			if (referrer.getColumn("NoteID").length() > 0) {
				referrer.setAction("select");
				referrer.doAction();
			}
			
			formBean.setColumn("NoteID", KeyMaker.generate());
			
			if (referrer.getString("TemplateUsed").length()!=0) {
				UserBean currentUser = getSystemUser();
	
				RowBean contactBean = getReferContactBean(referrer, currentUser);				
				
				if (contactBean != null) {
					
					//System.out.println("contactBean != null");
					
					contactBean.put("RefFirstName", referrer.getString("FirstName"));
					contactBean.put("RefLastName", referrer.getString("LastName"));
					contactBean.put("RefCompany", referrer.getString("Company"));
					
					
					formBean.put("CompanyID", contactBean.getString("CompanyID"));
					formBean.put("ContactID", contactBean.getString("ContactID"));
					formBean.put("RepUserID", referrer.getString("AuthorUserID"));
					if (formBean.getString("GroupID").length() == 0) {
						formBean.put("GroupID", referrer.getString("GroupID"));
					}
					
					if(formBean.getString("TemplateID").length()==0) {  
						//	If no Refer is set use the original Email Template
						if (referrer.getString("ReferTemplateID").length() == 0) {
							formBean.put("TemplateID", referrer.getString("TemplateUsed"));
						} else {
							formBean.put("TemplateID", referrer.getString("ReferTemplateID"));
						}
						formBean.put("MasterTemplateID", referrer.getString("MasterTemplateID"));
						getTemplateBean(formBean.getString("TemplateID"), formBean.getString("MasterTemplateID"));
					} else { 
						getTemplateBean(formBean.getString("TemplateID"), "");
						formBean.put("MasterTemplateID", template.getString("MasterTemplateID"));
					}
					
					formBean.put("CMSID", referrer.getString("CMSUsed"));
					formBean.put("Campaign", referrer.getString("Campaign"));
					formBean.put("CampaignID", referrer.getString("CampaignID"));
					formBean.put("TO",contactBean.getString("Email"));
					formBean.put("IsSTAFEmail", "true");
					
					/*String fromAddress = OfflineMailer.getFromAddress(template.getString("EmailFrom"), contactBean, currentUser, referrer);
					if (fromAddress == null || fromAddress.length() == 0) {
					   fromAddress = referrer.getString("AuthorEmail");
					}
					formBean.put("FROM", fromAddress);
					*/
					formBean.put("NOTIFY_TO",referrer.getString("EmailNotifyTo"));
					
					setReferTemplateDefaults(template);
		
					return doEmail(currentUser, MODE_SEND);
					
				}
				else {
					errors.append("Error creating refered contact");
					return null;
				}
			}
			else {
				errors.append("Referrer not found, or No Template specified");
				return null;
			}
		}
		catch (Exception e) {
			errors.append(ActionBean.writeStackTraceToString(e));
			return null;
		}
	}
	
	/**
	 * Returns NoteID of sent Email, null if errors occur.
	 */
	public String sendEmail(UserBean currentUser) {
		return sendSingleEmail(currentUser, MODE_SEND, true);
	}
	
	/**
	 * Returns NoteID of saved Email, null if errors occur.
	 */
	public String saveEmail(UserBean currentUser) {
		return sendSingleEmail(currentUser, MODE_SAVE, true);
	}
	
	/**
	 * Returns HTML of Email, null if errors occur.
	 */
	public String getEmailPreview(UserBean currentUser, boolean textOnly) {
		if (textOnly) {
			 StringBuffer testpreview = new StringBuffer();
			 testpreview.append("<html><head><title>Preview</title></head><body>");
			 testpreview.append(sendSingleEmail(currentUser, MODE_PREVIEW_TEXT, true));
			 testpreview.append("</body></html>");
			return testpreview.toString();
		}
		else {
			return sendSingleEmail(currentUser, MODE_PREVIEW, true);
		}
	}
	 
	 
	public String sendDefaultEmail(UserBean currentUser, String contactID, String authorID, String groupID, String templateID) {
	   
	   formBean.setColumn("ContactID", contactID);
	   formBean.setColumn("RepUserID", authorID);
	   formBean.setColumn("GroupID", groupID);
	   formBean.setColumn("TemplateID", templateID);
	   loadAuthorBean(authorID);
	   
	   return sendSingleEmail(currentUser, MODE_SEND, false);
	} 
	 
	 
	private String sendSingleEmail(UserBean currentUser, int mode, boolean useFrom) {
		try {
			getContactBean(formBean.getString("ContactID"), currentUser);
			getTemplateBean(formBean.getString("TemplateID"), formBean.getString("MasterTemplateID")); 
			
			if (!useFrom) {
			   setTemplateDefaults(template);
			}
			return doEmail(currentUser, mode);
		}
		catch (Exception e) {
			errors.append("Error finding Contact or Template");
			return null;
		}
	}
	
	private String doEmail(UserBean currentUser, int mode) {
		//System.out.println("Inside doEmail");
		try {
			 
			addUserEmailTokens(contactBean, currentUser, authorBean);
			addProfileTokens(contactBean, null);
			addSetupTokens(formBean, contactBean);
			
			if (formBean.getString("OrderID").length() > 0) {
				addOrderTokens(formBean.getString("OrderID"),contactBean);
			}
			if (template.getString("RewardID").length() > 0) {
				addRewardTokens(template.getString("RewardID"),contactBean,null);
			}
			if (formBean.getString("ProductID").length() > 0) {
				addProductTokens(formBean.getString("ProductID"),contactBean,null);
			}
			if (formBean.getString("ProductIDs").length() > 0) {
				addProductListTokens(formBean.getString("ProductIDs"),contactBean,null);
			}
			if (formBean.getString("OpportunityID").length() > 0) {
				addOpportunityTokens(formBean.getString("OpportunityID"),contactBean);
			}
			/*if (contactBean.getString("CompanyID").length() > 0) {
			   //addCompanyToken(contactBean);
			   
			   // ${company.Profile-}
			}*/
			
			RowBean noteBean = getNoteBean(formBean, currentUser);
			
			if (template.getString("TemplateID").length() == 0) {
				noteBean.setColumn("TemplateName","Custom Email");
			}
			contactBean.putAll(formBean);
			contactBean.setColumn("URLHome",context.getInitParameter("URLHome"));
			
			// Hmmm What to do here
			//requestBean.setColumn("NoteCode",notebean2.getColumn("NoteCode"));
			
			MailerBean mailerBean = new MailerBean();
			mailerBean.setHost(context.getInitParameter("SMTPHost"));
			mailerBean.setUsername(context.getInitParameter("SMTPUsername"));
			mailerBean.setPassword(context.getInitParameter("SMTPPassword"));
			mailerBean.setFrom(noteBean.getString("EmailFrom"));
			mailerBean.setTo(noteBean.getString("EmailTo"));
			mailerBean.setCc(noteBean.getString("EmailCC"));
			mailerBean.setBcc(noteBean.getString("EmailBCC"));
			mailerBean.setReplyTo(noteBean.getString("EmailReplyTo"));  
	
			TemplateURLs urllinks = new TemplateURLs();
			urllinks.getLinks(template.getColumn("TemplateID"),template.getColumn("MasterTemplateID"), context);
	
			String velocityError = null;	
			String emailBody = null;
			
			try {
				VelocityEngine ve = VelocityManager.getEngine();
			
				StringWriter subject = new StringWriter();
				StringWriter body = new StringWriter();
				StringWriter text = new StringWriter();
				String strbody = null;
				String htmlbody = formBean.getString("HTMLBody");
				if(htmlbody.length()==0)
				{
					 htmlbody = formBean.getString("Body");
				}
				
				//System.out.println("###################htmlbody#######################"+htmlbody);
				
				if (formBean.getString("CMSID").length() > 0) {
					noteBean.setColumn("CMSUsed",formBean.getString("CMSID"));
					noteBean.setColumn("TemplateUsed",template.getString("TemplateID"));
					
					RowBean newsletter = getNewsletterBean(formBean.getString("CMSID"));
					
					if(formBean.getString("Subject").length()==0) {
						formBean.setColumn("Subject",template.getString("Subject"));
					}
					
					contactBean.put("cms", newsletter);
					contactBean.put("contextpath",context.getInitParameter("URLHome"));
	
					ve.evaluate( contactBean, subject, "WebVisionEmail", formBean.getString("Subject"));
					subject.flush();
					subject.close();
					
					mailerBean.setSubject(subject.toString());
					noteBean.setColumn("Subject",subject.toString());
		
					TemplateURLs nlurllinks = new TemplateURLs();
					nlurllinks.getCMSLinks(formBean.getString("CMSID"),context);
				
				   newsletter.put("urllinks", nlurllinks);
				
		// HTML Newsletter bit
					StringWriter tempBody = new StringWriter();
					StringWriter tempText = new StringWriter();
			
					urllinks.setHTMLLinks(contactBean);
					
					ve.evaluate( contactBean, tempBody, "BodyText", StringUtil.encodeHTMLLinefeeds(htmlbody));
					tempBody.flush();
					tempBody.close();
		
					urllinks.setTextLinks(contactBean);
		
					ve.evaluate( contactBean, tempText, "BodyText", formBean.getString("Body"));
					tempText.flush();
					tempText.close();
		
					StringBuffer roottemplate = new StringBuffer();
					roottemplate.append("#parse(\"");
					roottemplate.append(newsletter.getString("EmailTemplate"));
					roottemplate.append(".vt\")");
		
					StringBuffer textroottemplate = new StringBuffer();
					textroottemplate.append("#parse(\"");
					textroottemplate.append(newsletter.getString("EmailTemplate"));
					textroottemplate.append("_text.vt\")");

					//System.out.println("ROOT TEMPLATE%%%%%%%%%%%%%%%%%%"+roottemplate.toString());
					
					contactBean.setColumn("Body",tempBody.toString());
					ve.evaluate( contactBean, body, "NewsLetter", roottemplate.toString());
					
					//System.out.println("body%%%%%%%%%%%%%%%%%%"+body.toString());
					
					if(formBean.getString("IsSTAFEmail") != null && "true".equals(formBean.getString("IsSTAFEmail"))){
						GenRow cmsTemplate = new GenRow();
						cmsTemplate.setViewSpec("CMSTemplateView");
						cmsTemplate.setParameter("CMSID",template.getString("TemplateID"));
						cmsTemplate.doAction("selectfirst");
						
						String stafMsg = "";
						
						if(cmsTemplate.getString("SendToAFriendMsg") != null && (cmsTemplate.getString("SendToAFriendMsg")).length() > 0){
							stafMsg = cmsTemplate.getString("SendToAFriendMsg");
						}
						
						String actualBody = body.toString();
						try {
							int s = actualBody.indexOf("<script");
							int e = actualBody.lastIndexOf("</script>");
							if (s > 0 && e > 0) {
								actualBody = actualBody.substring(0, s)
										+ actualBody.substring(e + "</script>".length());
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						strbody = evaluateSTAFBody(stafMsg, actualBody);
						cmsTemplate.close();
					}
					
					contactBean.setColumn("Body",tempText.toString());
					ve.evaluate( contactBean, text, "NewsLetter", textroottemplate.toString());
					

					tempBody = new StringWriter();
					tempText = new StringWriter();

					//CSM Links
					nlurllinks.setHTMLLinks(contactBean);
					ve.evaluate( contactBean, tempBody, "NewsLetter", strbody);
					tempBody.flush();
					tempBody.close();
					// Text Newsletter bit
									
					urllinks.setTextLinks(contactBean);
					ve.evaluate( contactBean, tempText, "NewsLetter", text.toString());
					tempText.flush();
					tempText.close();
					
					strbody = AbstractEmailer.fixHrefTags(AbstractEmailer.fixImageTags(AbstractEmailer.fixStandardLinks(tempBody.toString())));
					text = tempText;
					
					//System.out.println("strbody%%%%%%%%%%%%%%%%%%"+strbody.toString());
					
					/*strbody = body.toString();
					
					if(formBean.getString("IsSTAFEmail") != null && "true".equals(formBean.getString("IsSTAFEmail"))){
						strbody = evaluateSTAFBody("",strbody);
					}*/
					
					//System.out.println("###################body#######################"+strbody);
					//System.out.println("###################formBean.getString(IsSTAFEmail)#######################"+formBean.getString("IsSTAFEmail"));
				}
				else {
					noteBean.setColumn("TemplateUsed",template.getString("TemplateID"));
					noteBean.setColumn("MasterTemplateID",template.getString("MasterTemplateID"));
					
					ve.evaluate( contactBean, subject, "WebVisionEmail", formBean.getString("Subject"));
					subject.flush();
					subject.close();
										
					urllinks.setHTMLLinks(contactBean);
					ve.evaluate( contactBean, body, "WebVisionEmail", template.getString("HTMLHeader"));
					ve.evaluate( contactBean, body, "WebVisionEmail", StringUtil.encodeHTMLLinefeeds(htmlbody));
					ve.evaluate( contactBean, body, "WebVisionEmail", template.getString("HTMLFooter"));
					
					strbody = body.toString();
					
					body.flush();
					body.close();
					//body = buildEmailBody(ve, contactBean, StringUtil.replace(formBean.getString("Body"),"\n","<br>"));
					
					urllinks.setTextLinks(contactBean);
					ve.evaluate( contactBean, text, "WebVisionEmail", template.getString("TextHeader"));
					ve.evaluate( contactBean, text, "WebVisionEmail", "\r\n"+formBean.getString("Body")+"\r\n");
					ve.evaluate( contactBean, text, "WebVisionEmail", template.getString("TextFooter"));
					text.flush();
					text.close();
					
					
					
					//text = buildEmailBody(ve, contactBean, formBean.getString("Body"));
				}
				
				mailerBean.setSubject(subject.toString());
				noteBean.setColumn("Subject",subject.toString());
				
				if(!contactBean.getString("EmailType").equals("Plain Text") && mode != MODE_PREVIEW_TEXT) {
					mailerBean.setHtmlbody(strbody);
					emailBody = strbody;
				}
				else {
					emailBody = AbstractEmailer.fixHrefTags(AbstractEmailer.fixImageTags(AbstractEmailer.fixStandardLinks(text.toString())));
				}
				noteBean.setColumn("HTMLBody",strbody);
				
				mailerBean.setTextbody(AbstractEmailer.fixHrefTags(AbstractEmailer.fixImageTags(AbstractEmailer.fixStandardLinks(text.toString()))));
				noteBean.setColumn("Body",AbstractEmailer.fixHrefTags(AbstractEmailer.fixImageTags(AbstractEmailer.fixStandardLinks(text.toString()))));
			}
			catch (Exception e) {
				velocityError = e.toString();
			}
						
			if (mode != MODE_PREVIEW && mode != MODE_PREVIEW_TEXT) {
				
				mailerBean.setAttachments(getAttachments(noteBean.getString("NoteID")));
				
				if (mode == MODE_SEND) {
					if (velocityError == null) {
						String emailresponse = mailerBean.getResponse();
						
						if(emailresponse.equals("Message Sent")) { 
							noteBean.setColumn("Status","Sent");
							noteBean.setColumn("SentDate","NOW");
						}
						else {
							noteBean.setColumn("Status",emailresponse);
							errors.append(emailresponse);
						}
					}
					else {
						noteBean.setColumn("Status",velocityError);
						errors.append(velocityError);
					}
				}
				
				noteBean.doAction();
				
				noteBean.setTableSpec("Emails");
				noteBean.setColumn("EmailID", noteBean.getString("NoteID"));
				noteBean.doAction();
				
				saveDocuments(formBean, currentUser);
			}
			
			if (mode > 1) {
				return emailBody;
			}
			else {
				return noteBean.getString("NoteID");
			}
		}
		catch (Exception e) {
			errors.append(ActionBean.writeStackTraceToString(e));
			return null;
		}
	}
	
	public String getError() {
		return errors.toString();
	}
	
	private Collection getAttachments(String noteID) {
		if (EmailAttachmentBuilder.hasEmailAttachmentBuilder(noteID)) {
			EmailAttachmentBuilder builder = EmailAttachmentBuilder.getEmailAttachmentBuilder(noteID);
			return builder.getAttachedFiles();
		}
		else {
			return null;
		}
	}
	
	private void saveDocuments(RowBean formBean ,UserBean currentUser) {
		String noteID = formBean.getString("NoteID");
		
		if (EmailAttachmentBuilder.hasEmailAttachmentBuilder(noteID)) {
			String contactID = formBean.getString("ContactID");
			String companyID = formBean.getString("CompanyID");
			String opportunityID = formBean.getString("OpportunityID");

			EmailAttachmentBuilder builder = EmailAttachmentBuilder.getEmailAttachmentBuilder(noteID);
		
			Iterator iter = builder.getUploadedFiles().iterator();
			File file = null;
			File movedFile = null;
			File parentDir = null;
			File tempDir = null;
			while (iter.hasNext()) {
				file = (File)iter.next();
				
				if (file != null) {
					if (parentDir == null) {
						tempDir = file.getParentFile();
						parentDir = tempDir.getParentFile().getParentFile();
						parentDir = new File(parentDir,KeyMaker.generate());
						parentDir.mkdir();
					} 
					movedFile = new File(parentDir,file.getName());
					file.renameTo(movedFile);
					
					String filePath = movedFile.getAbsolutePath().substring(context.getRealPath("").length());
					if (File.separatorChar != '/') {
						filePath = StringUtil.replace(filePath,File.separatorChar,'/');
					}
					String documentID = KeyMaker.generate();
			 
					TableBean doc = new TableBean();
					doc.setJndiName(ActionBean.getJndiName());
					doc.setViewSpec("DocumentView");
					doc.putAll(formBean);
					doc.setColumn("DocumentID",documentID);
					doc.setColumn("DocumentType","-Unspecified-");
					doc.setColumn("FileName",movedFile.getName());
					doc.setColumn("Description","Email Attachment");
					doc.setColumn("FilePath",filePath);
					doc.setColumn("DocumentCategory","MailAttachment");
					doc.setColumn("ModifiedDate","NOW");
					doc.setColumn("CreatedDate","NOW");
					doc.setColumn("ModifiedBy",currentUser.getCurrentUserID());
					doc.setColumn("CreatedBy",currentUser.getCurrentUserID());
					doc.setAction("insert");
					doc.doAction();
					
					builder.moveUploadedFileToLinkedDocument(movedFile ,documentID);
				}
			}
			if (tempDir != null) {
				tempDir.delete();
			}
	
			iter = builder.getDocumentIDs().iterator();
			while (iter.hasNext()) {
				String documentID = (String)iter.next();
				
				TableBean doc = new TableBean();
				doc.setJndiName(ActionBean.getJndiName());
				doc.setViewSpec("LinkedDocumentView");
				doc.setColumn("LinkedDocumentID",KeyMaker.generate());
				doc.setColumn("DocumentID",documentID);
				doc.setColumn("NoteID",noteID);
				doc.setColumn("ContactID",contactID);
				doc.setColumn("OpportunityID",opportunityID);
				doc.setColumn("CompanyID",companyID);
				doc.setColumn("CreatedDate","NOW");
				doc.setColumn("CreatedBy",currentUser.getCurrentUserID());
				doc.setAction("insert");
				doc.doAction();
			}
		
			builder.removeAllFiles();
		}
	}
	
	private RowBean getTemplateBean(String templateID, String masterTemplateID) {
		if (template == null) {
			template = new RowBean();
			template.setJndiName(ActionBean.getJndiName());
			template.setViewSpec("TemplateView");
			template.setAction("select");
				
			if(templateID.length() > 0) {
				template.setColumn("TemplateID", templateID);
				template.doAction();
			}
				
			if(masterTemplateID.length() > 0 && !template.getString("MasterTemplateID").equals(masterTemplateID)) {
				RowBean mastertemplate = new RowBean();
				mastertemplate.setJndiName(ActionBean.getJndiName());
				mastertemplate.setViewSpec("MasterTemplateView");
				mastertemplate.setAction("select");
				mastertemplate.setColumn("MasterTemplateID",masterTemplateID);
				mastertemplate.doAction();
				template.putAll(mastertemplate);
			}
		}
		return template;
	}
	
	private RowBean getNewsletterBean(String cmsID) {
		if (newsletter == null) {

			newsletter = new RowBean();
			newsletter.setJndiName(ActionBean.getJndiName());
			newsletter.setViewSpec("PageView");
			newsletter.setColumn("CMSID",cmsID);
			newsletter.setColumn("ParentID","NULL+EMPTY");
			newsletter.setColumn("Active","Active");
			newsletter.retrieveData();
	
			RowArrayBean newslist = new RowArrayBean();
			newslist.setJndiName(ActionBean.getJndiName());
			newslist.setViewSpec("PageView");
			newslist.setColumn("ParentID",newsletter.getColumn("PageID"));
			newslist.setColumn("Active","Active");
			newslist.sortBy("PageLinks.SortOrder",0);
			newslist.generateSQLStatment();
			newslist.getResults();

			RowArrayBean productlist = null;
			productlist = new RowArrayBean();
			productlist.setJndiName(ActionBean.getJndiName());
			productlist.setViewSpec("PageProductView");
			productlist.setColumn("PageID",newsletter.getString("PageID"));
			productlist.generateSQLStatment();
			productlist.getResults();
			newsletter.put("products",productlist.getList());

			RowArrayBean newsitemlist = null;
			for(int i=0; i<newslist.getSize(); i++) {
				newsitemlist = new RowArrayBean();
				newsitemlist.setJndiName(ActionBean.getJndiName());
				newsitemlist.setViewSpec("PageView");
				newsitemlist.setColumn("ParentID",newslist.get(i).getColumn("PageID"));
				newsitemlist.setColumn("Active","Active");
				newsitemlist.sortBy("PageLinks.SortOrder",0);
				newsitemlist.getResults();
				newslist.get(i).put("newsitems",newsitemlist.getList());
	
				productlist = new RowArrayBean();
				productlist.setJndiName(ActionBean.getJndiName());
				productlist.setViewSpec("PageProductView");
				productlist.setColumn("PageID",newslist.get(i).getColumn("PageID"));
				productlist.generateSQLStatment();
				productlist.getResults();
				newslist.get(i).put("products",productlist.getList());
			}

			newsletter.put("news",newslist.getList());
		}
		return newsletter;
	}
	
	private RowBean getNoteBean(RowBean formBean, UserBean currentUser) {
		noteBean.clear();
		noteBean.setJndiName(ActionBean.getJndiName());
		noteBean.setAction(ActionBean.INSERT);
		boolean noteExists = false;
		if (formBean.getString("NoteID").length() == 0) {
			formBean.put("NoteID", KeyMaker.generate());
		}
		else {
			GenRow checkNoteBean = new GenRow();
			checkNoteBean.setJndiName(ActionBean.getJndiName());
			checkNoteBean.setViewSpec("NoteView");
			checkNoteBean.setParameter("NoteID",formBean.getString("NoteID"));
			checkNoteBean.setAction(ActionBean.SELECT);
			checkNoteBean.doAction();
			
			if (checkNoteBean.isSuccessful()) {
				noteBean.setAction(ActionBean.UPDATE);
				noteExists = true;
			}
		}
		
		noteBean.putAll(formBean);
		if (!noteExists) {
			noteBean.setColumn("CreatedDate","NOW");
			noteBean.setColumn("CreatedBy",currentUser.getCurrentUserID());
		}
		noteBean.setColumn("ModifiedDate","NOW");
		noteBean.setColumn("ModifiedBy",currentUser.getCurrentUserID());
		
		
		String cc = formBean.getString("CC");
		String bcc = formBean.getString("BCC");
		String to = formBean.getString("TO");
		String from = formBean.getString("FROM");
		String notifyTo = formBean.getString("NOTIFY_TO");
		String replyTo = formBean.getString("REPLY_TO");
		
		if (formBean.getString("CMSID").length() > 0) {
			if (from.length() == 0) {
			   GenRow cmsTemplate = new GenRow();
			   cmsTemplate.setViewSpec("CMSTemplateView");
			   cmsTemplate.setParameter("CMSID",template.getString("TemplateID"));
			   cmsTemplate.doAction("selectfirst");
			   
			   //System.out.println("cmsTemplate%%%%%%%%%%%%%%%%%%"+cmsTemplate.getStatement()+"::"+cmsTemplate.getString("EmailFrom"));
			   
			   from = cmsTemplate.getString("EmailFrom");
			}
		}
		
		String parentNoteID = formBean.getString("ParentNoteID");
		if(parentNoteID.length()>0) { 
			noteBean.setColumn("ParentNoteID",parentNoteID);
		}
		if(formBean.getString("GroupID").length()>0) 
		{
			noteBean.setColumn("GroupID",formBean.getString("GroupID"));
		}
		if(from.length() > 0){
			noteBean.setColumn("EmailFrom",from);
		}
		else if (template.getString("EmailFrom").length() > 0) {
		   noteBean.setColumn("EmailFrom",OfflineMailer.getFromAddress(template.getString("EmailFrom"), contactBean, currentUser, null));
		}
		else if (contactBean.getString("RepEmail").length() > 0) {
			noteBean.setColumn("EmailFrom",contactBean.getString("RepEmail"));
		}
		else {
			noteBean.setColumn("EmailFrom",defaultFrom);
		}
		
		if(to.length() > 0){ 
			noteBean.setColumn("EmailTo",to);
		}
		else if (template.getString("EmailTo").length() > 0) {
			noteBean.setColumn("EmailTo",OfflineMailer.getToAddress(template.getString("EmailTo"), contactBean, currentUser, null));
		}
		else {
			noteBean.setColumn("EmailTo",contactBean.getString("Email"));
		}
		
		if(cc.length() > 0){
			noteBean.setColumn("EmailCC",cc);
		}
		
		if(bcc.length() > 0){
			noteBean.setColumn("EmailBCC",bcc);
		}
		if(notifyTo.length() > 0){
			noteBean.setColumn("EmailNotifyTo",notifyTo);
		}
		if(replyTo.length() > 0) { 
			noteBean.setColumn("EmailReplyTo",replyTo); 
		} 
		
		
		return noteBean;
	}
		
		
	private RowBean getContactBean(String contactID, UserBean currentUser) {
		
		contactBean.clear();
		contactBean.setJndiName(ActionBean.getJndiName());
		contactBean.setAction(ActionBean.SELECT);
		contactBean.setColumn("ContactID",contactID);
		contactBean.doAction();
	
		
		return contactBean;
	}
	
	private void loadAuthorBean(String authorID) {
	   if (authorBean == null) {
		   authorBean = new TableBean();
		}
		authorBean.clear();
		authorBean.setJndiName(ActionBean.getJndiName());
		authorBean.setViewSpec("UserView");
		authorBean.setColumn("UserID", authorID);
		authorBean.setAction(ActionBean.SELECT);
		authorBean.doAction();
	}
	
	public static void addUserEmailTokens(TableData contactBean, TableData currentUser, TableData authorBean) {
		
		contactBean.setColumn("AuthorTitle", authorBean.getString("Title"));
		contactBean.setColumn("AuthorFirstName", authorBean.getString("FirstName"));
		contactBean.setColumn("AuthorLastName", authorBean.getString("LastName"));
		contactBean.setColumn("AuthorCompany",authorBean.getColumn("Company"));
		contactBean.setColumn("AuthorPosition",authorBean.getColumn("Position"));
		contactBean.setColumn("AuthorEmail",authorBean.getColumn("Email"));
		contactBean.setColumn("AuthorStreet",authorBean.getColumn("Street"));
		contactBean.setColumn("AuthorStreet2",authorBean.getColumn("Street2"));
		contactBean.setColumn("AuthorCity",authorBean.getColumn("City"));
		contactBean.setColumn("AuthorState",authorBean.getColumn("State"));
		contactBean.setColumn("AuthorPostcode",authorBean.getColumn("Postcode"));
		contactBean.setColumn("AuthorCountry",authorBean.getColumn("Country"));
		contactBean.setColumn("AuthorPhone",authorBean.getColumn("Phone"));
		contactBean.setColumn("AuthorFax",authorBean.getColumn("Fax"));
		contactBean.setColumn("AuthorMobile",authorBean.getColumn("Mobile"));
		contactBean.setColumn("AuthorSignatureImage",authorBean.getColumn("SignatureImagePath"));
		contactBean.setColumn("AuthorPhotoImage",authorBean.getColumn("PhotoImagePath"));
				
		contactBean.put("CurrentUserTitle",currentUser.getString("Title"));
		contactBean.put("CurrentUserFirstName",currentUser.getString("FirstName"));
		contactBean.put("CurrentUserLastName",currentUser.getString("LastName"));
		contactBean.put("CurrentUserCompany",currentUser.getString("Company"));
		contactBean.put("CurrentUserPosition",currentUser.getString("Position"));
		contactBean.put("CurrentUserEmail",currentUser.getString("Email"));
		contactBean.put("CurrentUserStreet",currentUser.getString("Street"));
		contactBean.put("CurrentUserStreet2",currentUser.getString("Street2"));
		contactBean.put("CurrentUserCity",currentUser.getString("City"));
		contactBean.put("CurrentUserState",currentUser.getString("State"));
		contactBean.put("CurrentUserPostcode",currentUser.getString("Postcode"));
		contactBean.put("CurrentUserCountry",currentUser.getString("Country"));
		contactBean.put("CurrentUserPhone",currentUser.getString("Phone"));
		contactBean.put("CurrentUserFax",currentUser.getString("Fax"));
		contactBean.put("CurrentUserMobile",currentUser.getString("Mobile"));
		contactBean.put("CurrentUserSignatureImage",currentUser.getColumn("SignatureImagePath"));
		contactBean.put("CurrentUserPhotoImage",currentUser.getColumn("PhotoImagePath"));
		contactBean.put("Date",new DateToken());
	}
	
	public static void addNoteTokens(TableData contactBean, Connection con) throws SQLException { 
		if (contactBean.getString("ContactID").length() > 0) {
			RowBean noteBean = new RowBean(); 
				if(con !=null && !con.isClosed()) {
					 noteBean.setConnection(con);
				 }
				 else {
						noteBean.setConnection((Connection)null);
						noteBean.setCloseConnection(true);
				 }
			noteBean.setViewSpec("NoteView"); 
			noteBean.setColumn("ContactID",contactBean.getString("ContactID")); 
			noteBean.setColumn("Type","Export"); 
			noteBean.sortBy("CreatedDate",0); 
			noteBean.sortOrder("DESC",0); 
			noteBean.setTop(1); 
			noteBean.retrieveData(); 
			
			contactBean.put("LastNoteDate",noteBean.getColumn("NoteDate"));
			contactBean.put("LastNoteSubject",noteBean.getColumn("Subject")); 
			contactBean.put("LastNoteBody",noteBean.getColumn("Body")); 
		}
	}
	
	public void addSetupTokens(TableData formBean, TableData contactBean) throws SQLException {
	   
      String userID = formBean.getString("RepUserID");
      String setupID = formBean.getString("SetupID");
	   
	   addSetupTokens(userID, contactBean, null, setupID);
	}
	
	public static void addSetupTokens(String userID, TableData contactBean, Connection con) throws SQLException { 
	   addSetupTokens(userID, contactBean, null, null);
	}
	
	public static void addSetupTokens(String userID, TableData contactBean, Connection con, String defaultSetupID) throws SQLException { 
		RowBean setupBean = new RowBean();
		boolean found = false;
		if(con !=null && !con.isClosed()) {
			setupBean.setConnection(con);
		}
		else {
			setupBean.setConnection((Connection)null);
			setupBean.setCloseConnection(true);
		}
		if (userID != null && userID.length() > 0) {
   		setupBean.setViewSpec("UserSetupView"); 
   		setupBean.setColumn("UserID",userID); 
   		setupBean.setAction(ActionBean.SELECT);
   		setupBean.doAction();
   		if (setupBean.getString("SetupID").length() > 0) {
   		   found = true;
   		}
		}
		if (!found) {
		   if (defaultSetupID == null || defaultSetupID.length() == 0) {
		      defaultSetupID = "Default";
		   }
   		setupBean.setViewSpec("SetupView"); 
         setupBean.setColumn("SetupID",defaultSetupID); 
   		setupBean.setAction(ActionBean.SELECT);
   		setupBean.doAction();
		}
		
		contactBean.put("setup",setupBean);
	}
	
	public static void addProfileTokens(TableData contactBean, Connection con) throws Exception {
		
		if (contactBean.getString("ContactID").length() > 0) {
			RowSetBean questionList = new RowSetBean();
			if(con !=null && !con.isClosed()) {
				questionList.setConnection(con);
			}
			else {
				 questionList.setConnection((Connection)null);
				 questionList.setCloseConnection(true);
			}
			questionList.setViewSpec("{answers/}ContactAnswerView");
			questionList.setColumn("ContactID",contactBean.getString("ContactID"));
			if (contactBean.getString("Type").length() > 0) {
				questionList.setColumn("ContactAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.ContactType",contactBean.getString("Type")+"+All");
			}
			else {
				questionList.setColumn("ContactAnswerQuestionSurveyQuestion.SurveyQuestionSurvey.ContactType","All");
			}
			questionList.generateSQLStatment();
			questionList.getResults();
			
			while(questionList.getNext()) {
				contactBean.put("Profile-"+StringUtil.ToAlphaNumeric(questionList.getString("QuestionLabel")),questionList.getString("AnswerText"));
				contactBean.put("Profile-"+questionList.getString("QuestionID"),questionList.getString("AnswerText"));
			}
			questionList.close();
		}
	}
	
	public static void addGroupNameTokens(TableData contactBean, Connection con) {
		List<String> groupNames = new ArrayList<String>();
		GenRow cg = new GenRow();
		cg.setViewSpec("ContactGroupView");
		cg.setParameter("ContactID", contactBean.getString("ContactID"));
		cg.sortBy("SortOrder", 0);
		cg.doAction("search");
		cg.getResults(true);

		ViewSpec vs = cg.getViewSpec();
		String key = "Contact_";
		
		if (cg.getNext()) {			
			for (int i = 0; i < vs.getLocalLength(); ++i) {
				contactBean.put(key + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
				contactBean.put(key + "Default_" + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
			}
			for (int i = 0; i < vs.getRelatedLength(); ++i) {
				contactBean.put(key + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
				contactBean.put(key + "Default_" + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
			}
			do {
				groupNames.add(cg.getString("Name"));
				String group = cg.getString("Name").replaceAll(" ", "_");

				for (int i = 0; i < vs.getLocalLength(); ++i) {
					contactBean.put(key + group + "_" + vs.getLocalItemName(i), cg.getString(vs.getLocalItemName(i)));
				}
				for (int i = 0; i < vs.getRelatedLength(); ++i) {
					contactBean.put(key + group + "_" + vs.getRelatedItemName(i), cg.getString(vs.getRelatedItemName(i)));
				}
			} while (cg.getNext());
		}
		
		contactBean.put("GroupNames", StringUtils.join(groupNames, ", "));
		cg.close();

	}
	
	public static void addOrderTokens(String orderID, TableData contactBean) throws Exception {
		addOrderTokens(orderID, contactBean, null);
	}
	
	public static void addOrderTokens(String orderID, TableData contactBean, Connection con) throws Exception {

		if (orderID != null && orderID.length() > 0) {
			RowBean orderBean = new RowBean();
			if(con !=null && !con.isClosed()) {
				orderBean.setConnection(con);
			}
			else {
				orderBean.setJndiName(ActionBean.getJndiName());
			}
			orderBean.setColumn("OrderID",orderID);
			orderBean.setViewSpec("{email/}OrderEmailView");
			orderBean.setAction(ActionBean.SELECT);
			orderBean.doAction();
			
			contactBean.put("order", orderBean);
	        
	        
        	DecimalFormat format = new DecimalFormat();
	        format.setDecimalSeparatorAlwaysShown(false);
	         
	        double remHours = SupportUtil.getRemainingHours(con, ActionBean.getJndiName(), orderID);
	        if(remHours > 0)
	        	orderBean.put("RemainingHours", format.format(remHours));
	        else
	        	orderBean.put("RemainingHours", format.format(0));
        
			
			RowArrayBean orderitemlist = new RowArrayBean();
			if(con !=null && !con.isClosed()) {
				orderitemlist.setConnection(con);
			}
			else {
				orderitemlist.setJndiName(ActionBean.getJndiName());
			}
			orderitemlist.setViewSpec("OrderItemsView");
			orderitemlist.setColumn("OrderID",orderID);
			orderitemlist.sortBy("SortNumber",0); 
			orderitemlist.generateSQLStatment();
			orderitemlist.getResults();
			
			RowSetBean profileResults = new RowSetBean();
			for(int i=0; i<orderitemlist.getSize(); i++) {
				
				if (orderitemlist.get(i).getString("ProductID").length() > 0) {
					profileResults.clear();
					profileResults.setViewSpec("{answers/}ProductAnswerView");
					if(con !=null && !con.isClosed()) {
						profileResults.setConnection(con);
					}
					else {
						profileResults.setJndiName(ActionBean.getJndiName());
					}
					profileResults.setColumn("ProductID", orderitemlist.get(i).getString("ProductID"));
					profileResults.generateSQLStatment();
					profileResults.getResults();
					
					while (profileResults.getNext()) {
						orderitemlist.get(i).put("Profile-"+StringUtil.ToAlphaNumeric(profileResults.getString("QuestionLabel")), profileResults.getString("AnswerText"));
					}
				}		
			}
			profileResults.close();	
			orderitemlist.close();

			orderBean.put("orderitems",orderitemlist.getList());
		}
	}
	
	public static void addOpportunityTokens(String opportunityID, TableData contactBean) throws Exception {
	   addOpportunityTokens(opportunityID, contactBean, null);
	}
	
	public static void addOpportunityTokens(String opportunityID, TableData contactBean, Connection con) throws Exception {

		if (opportunityID != null && opportunityID.length() > 0) {
			RowBean opportunityBean = new RowBean();
			if(con !=null && !con.isClosed()) {
				opportunityBean.setConnection(con);
			}
			else {
				opportunityBean.setJndiName(ActionBean.getJndiName());
			}
			opportunityBean.setColumn("OpportunityID",opportunityID);
			opportunityBean.setViewSpec("OpportunityView");
			opportunityBean.setAction(ActionBean.SELECT);
			opportunityBean.doAction();
			
			contactBean.put("opportunity", opportunityBean);
			
			RowSetBean profileResults = new RowSetBean();
			profileResults.setViewSpec("{answers/}OpportunityAnswerView");
			if(con !=null && !con.isClosed()) {
				profileResults.setConnection(con);
			}
			else {
				profileResults.setJndiName(ActionBean.getJndiName());
			}
			profileResults.setColumn("OpportunityID", opportunityID);
			profileResults.generateSQLStatment();
			profileResults.getResults();
			
			while (profileResults.getNext()) {
				opportunityBean.put("Profile-"+StringUtil.ToAlphaNumeric(profileResults.getString("QuestionLabel")), profileResults.getString("AnswerText"));
			}
			
			// Add opportunity linked Companies
			RowArrayBean linkedCompanies = new RowArrayBean();
			linkedCompanies.setViewSpec("OpportunityCompanyListView");
			if(con !=null && !con.isClosed()) {
				linkedCompanies.setConnection(con);
			}
			else {
				linkedCompanies.setJndiName(ActionBean.getJndiName());
			}
			linkedCompanies.setColumn("OpportunityID",opportunityID);
			linkedCompanies.generateSQLStatment();
			linkedCompanies.getResults();
			
			profileResults.clear();
			profileResults.setViewSpec("{answers/}CompanyAnswerView");
			for(int i=0; i<linkedCompanies.getSize(); i++) {
			   if (linkedCompanies.get(i).getString("CompanyID").length() > 0) {
   			   profileResults.setColumn("CompanyID", linkedCompanies.get(i).getString("CompanyID"));
      			profileResults.generateSQLStatment();
      			profileResults.getResults();
      			
      			while (profileResults.getNext()) {
      				linkedCompanies.get(i).put("Profile-"+StringUtil.ToAlphaNumeric(profileResults.getString("QuestionLabel")), profileResults.getString("AnswerText"));
      			}
      		}
			}
			
			opportunityBean.put("linkedCompanies", linkedCompanies.getList());
			
			profileResults.close();	
		}
	}
	
	
	public static void addProductTokens(String productID, TableData contactBean, Connection con) throws Exception {
	   
		if (productID != null && productID.length() > 0) {
			RowArrayBean productBean = new RowArrayBean();
			productBean.setViewSpec("ProductView");
			if(con !=null && !con.isClosed()) {
				productBean.setConnection(con);
			}
			else {
				productBean.setJndiName(ActionBean.getJndiName());
			}
			productBean.setColumn("ProductID",productID);
			productBean.doAction(ActionBean.SELECT);
						
			RowSetBean profileResults = new RowSetBean();
			profileResults.setViewSpec("{answers/}ProductAnswerView");
			if(con !=null && !con.isClosed()) {
				profileResults.setConnection(con);
			}
			else {
				profileResults.setJndiName(ActionBean.getJndiName());
			}
			profileResults.setColumn("ProductID", productID);
			profileResults.generateSQLStatment();
			profileResults.getResults();
			
			while (profileResults.getNext()) {
				productBean.put("Profile-"+StringUtil.ToAlphaNumeric(profileResults.getString("QuestionLabel")), profileResults.getString("AnswerText"));
			}
   			
			profileResults.close();
			productBean.close();	
			
			contactBean.put("product", productBean);
		}
   }
	
	public static void addProductListTokens(String productIDs, TableData contactBean, Connection con) throws Exception {

		if (productIDs != null && productIDs.length() > 0) {
			RowArrayBean productList = new RowArrayBean();
			productList.setViewSpec("ProductView");
			if(con !=null && !con.isClosed()) {
				productList.setConnection(con);
			}
			else {
				productList.setJndiName(ActionBean.getJndiName());
			}
			productList.setColumn("ProductID",productIDs);
			productList.sortBy("Products.Name",0); 
			productList.generateSQLStatment();
			productList.getResults();
						
			RowSetBean profileResults = new RowSetBean();
			for(int i=0; i<productList.getSize(); i++) {
			
   			profileResults.setViewSpec("{answers/}ProductAnswerView");
   			if(con !=null && !con.isClosed()) {
   				profileResults.setConnection(con);
   			}
   			else {
   				profileResults.setJndiName(ActionBean.getJndiName());
   			}
   			profileResults.setColumn("ProductID", productList.get(i).getString("ProductID"));
   			profileResults.generateSQLStatment();
   			profileResults.getResults();
   			
   			while (profileResults.getNext()) {
   				productList.get(i).put("Profile-"+StringUtil.ToAlphaNumeric(profileResults.getString("QuestionLabel")), profileResults.getString("AnswerText"));
   			}
   			
   		}
			profileResults.close();
			productList.close();	
			
			contactBean.put("products", productList.getList());
		}
	}

	public static void addRewardTokens(String rewardID, TableData contactBean, Connection con) throws Exception {
		
		RowBean rewardBean = new RowBean();
		if(con !=null && !con.isClosed()) {
			rewardBean.setConnection(con);
		}
		else {
			 rewardBean.setConnection((Connection)null);
			 rewardBean.setCloseConnection(true);
		}
		rewardBean.setViewSpec("RewardPointView");
		rewardBean.setColumn("RewardID",rewardID);
		rewardBean.setColumn("ContactID",contactBean.getString("ContactID"));
		rewardBean.retrieveData();
		
		contactBean.put("currentPointsBalance",rewardBean.getColumn("Balance"));

		if (rewardBean.getString("CompanyID").length() > 0 && rewardBean.getString("Type").equals("COMPANY_QUARTERLY_ORDER_FORECAST")) {
			
			RowBean forecast = new RowBean();
			if(con !=null && !con.isClosed()) {
				forecast.setConnection(con);
			}
			else {
				 forecast.setConnection((Connection)null);
				 forecast.setCloseConnection(true);
			}
			forecast.setViewSpec("ForecastView");
			forecast.setColumn("CompanyID",rewardBean.getString("CompanyID"));
			forecast.setColumn("Period","Quarter");
			forecast.setColumn("OrderStatusID","!EMPTY");
			forecast.setColumn("StartDate","<NOW");
			forecast.setColumn("EndDate",">NOW");
			forecast.sortBy("EndDate",0);
			forecast.sortOrder("DESC",0);
			forecast.retrieveData();
			
			contactBean.put("currentForecast",forecast.getColumn("OrderValue"));
			contactBean.put("currentActual",forecast.getColumn("OrderActual"));
			contactBean.put("currentPercentage",forecast.getColumn("Percentage")+"%");
			
			forecast.clear();
			if(con !=null && !con.isClosed()) {
				forecast.setConnection(con);
			}
			else {
				 forecast.setConnection((Connection)null);
				 forecast.setCloseConnection(true);
			}
			forecast.setViewSpec("ForecastView");
			forecast.setColumn("CompanyID",rewardBean.getString("CompanyID"));
			forecast.setColumn("Period","Quarter");
			forecast.setColumn("OrderStatusID","!EMPTY");
			forecast.setColumn("EndDate","<NOW");
			forecast.sortBy("EndDate",0);
			forecast.sortOrder("DESC",0);
			forecast.retrieveData();
			
			contactBean.put("previousForecast",forecast.getColumn("OrderValue"));
			contactBean.put("previousActual",forecast.getColumn("OrderActual"));
			contactBean.put("previousPercentage",forecast.getColumn("Percentage")+"%");
			
			
			forecast.clear();
			if(con !=null && !con.isClosed()) {
				forecast.setConnection(con);
			}
			else {
				 forecast.setConnection((Connection)null);
				 forecast.setCloseConnection(true);
			}
			forecast.setViewSpec("RewardTransactionView");
			forecast.setColumn("RewardPointID",rewardBean.getString("RewardPointID"));
			forecast.setColumn("TransactionValue",">=0");
			forecast.setColumn("TransactionID","NULL");
			forecast.sortBy("CreatedDate",0);
			forecast.sortOrder("DESC",0);
			forecast.retrieveData();
			
			contactBean.put("previousPeriodPoints",forecast.getColumn("TransactionValue"));
			
		}
	}
	
	private RowBean getReferContactBean(RowBean referrer, UserBean currentUser) {
		
		//System.out.println("Inside getReferContactBean");
		
		if (formBean.getString("FirstName").length() > 0 
			&& formBean.getString("LastName").length() > 0 
			&& formBean.getString("Email").length() > 0) {
			
			//System.out.println("Inside getReferContactBean 1");
				
			RowSetBean searchBean = new RowSetBean();
			searchBean.setViewSpec("ContactView");
			searchBean.setJndiName(ActionBean.getJndiName());
			searchBean.setColumn("FirstName", formBean.getString("FirstName"));
			searchBean.setColumn("LastName", formBean.getString("LastName"));
			searchBean.setColumn("Email", formBean.getString("Email"));
			searchBean.generateSQLStatment();
			searchBean.getResults(); 
			String contactID = null;
			
			
			
			
			if (!searchBean.getNext()) {
				//System.out.println("Inside getReferContactBean 2 if");
				contactID = KeyMaker.generate();
				contactBean.setColumn("ContactID",contactID);
				
				contactBean.putAll(formBean);
				
				String referrerGroupID = referrer.getString("GroupID");
				String referrerRepUserID = referrer.getString("RepUserID");
				
				if(formBean.getString("GroupID") != null && (formBean.getString("GroupID")).length() > 0){
					
					GenRow referrerContactGroup = new GenRow();
					referrerContactGroup.setTableSpec("ContactGroups");
					referrerContactGroup.setParameter("-select0","GroupID");
					referrerContactGroup.setParameter("-select1","RepUserID");
					referrerContactGroup.setParameter("ContactID",referrer.getString("ContactID"));
					referrerContactGroup.setParameter("GroupID",formBean.getString("GroupID"));
					referrerContactGroup.doAction("selectfirst");
					
					referrerGroupID = formBean.getString("GroupID") ;
					
					if(referrerContactGroup.getString("RepUserID") != null && (referrerContactGroup.getString("RepUserID")).length() > 0){
						referrerRepUserID = referrerContactGroup.getString("RepUserID");
					}
										
					referrerContactGroup.close();
					
				}else{
				
					GenRow referrerContactGroup = new GenRow();
					referrerContactGroup.setTableSpec("ContactGroups");
					referrerContactGroup.setParameter("-select0","GroupID");
					referrerContactGroup.setParameter("-select1","RepUserID");
					referrerContactGroup.setParameter("ContactID",referrer.getString("ContactID"));
					referrerContactGroup.doAction("selectfirst");
									
					
					if(referrerContactGroup.getString("GroupID") != null && (referrerContactGroup.getString("GroupID")).length() > 0){
						referrerGroupID = referrerContactGroup.getString("GroupID");
					}
					
					if(referrerContactGroup.getString("RepUserID") != null && (referrerContactGroup.getString("RepUserID")).length() > 0){
						referrerRepUserID = referrerContactGroup.getString("RepUserID");
					}
					referrerContactGroup.close();
				}
				
				//System.out.println("contactID:"+contactID);
				
				RowBean contactstatus = new RowBean();
				contactstatus.setViewSpec("ContactStatusView");
				contactstatus.setJndiName(ActionBean.getJndiName());
				contactstatus.createNewID();
				contactstatus.setColumn("CreatedDate","NOW");
				contactstatus.setColumn("CreatedBy",currentUser.getString("UserID"));
				contactstatus.setColumn("ContactID",contactID);
				contactstatus.setColumn("StatusID",InitServlet.getSystemParams().getProperty("ContactGroupOptOutStatusID"));
				contactstatus.setAction("insert");
				contactstatus.doAction();
				
				contactBean.setColumn("ModifiedDate","NOW");
				contactBean.setColumn("CreatedDate","NOW");
				contactBean.setColumn("CreatedBy",currentUser.getString("UserID"));
				contactBean.setColumn("ModifiedBy",currentUser.getString("UserID"));
				contactBean.setColumn("GroupID",referrerGroupID);
				contactBean.setColumn("ContactStatusID",contactstatus.getString("ContactStatusID"));
				contactBean.setColumn("RepUserID",referrerRepUserID);
				contactBean.setAction("insert");
				contactBean.doAction();
				
				//System.out.println("Referrer GroupID :"+referrer.getString("GroupID"));
				//System.out.println("Referrer ContactID :"+referrer.getString("ContactID"));
				
			    String newContactID = contactBean.getString("ContactID");
			    
			   // Create ContactGroup
			    GenRow contactGroupsBean = new GenRow();
			    contactGroupsBean.setTableSpec("ContactGroups");
			    contactGroupsBean.setToNewID("ContactGroupID");
			    contactGroupsBean.setParameter("ContactID", newContactID);
			    contactGroupsBean.setParameter("GroupID", referrerGroupID);
			    contactGroupsBean.setParameter("CreatedDate", "NOW");
			    contactGroupsBean.setParameter("RepUserID", referrerRepUserID);   
			    contactGroupsBean.setParameter("EmailOptOut", "Y");   
			    contactGroupsBean.setAction("insert");
			    contactGroupsBean.doAction();
			    
			    
			           
			    String newContactGroupID = contactGroupsBean.getString("ContactGroupID");
			    
			    //System.out.println("newContactGroupID:"+newContactGroupID);
			      
			   // Create GroupStatus
			    GenRow groupStatus = new GenRow();
			    groupStatus.setTableSpec("GroupStatus");
			    groupStatus.setToNewID("GroupStatusID");
			    groupStatus.setParameter("CreatedDate", "NOW");
			    groupStatus.setParameter("CreatedBy", currentUser.getString("UserID"));
			    groupStatus.setParameter("StatusID", InitServlet.getSystemParams().getProperty("ContactGroupOptOutStatusID"));
			    groupStatus.setParameter("ContactGroupID", newContactGroupID);
			    groupStatus.setAction("insert");
			    groupStatus.doAction();
			   
			    String newGroupStatusID = groupStatus.getString("GroupStatusID");
			    
			    //System.out.println("newGroupStatusID:"+newGroupStatusID);
			   
			   // Create ContactStatus
			    GenRow contactStatusBean = new GenRow();
			    contactStatusBean.setTableSpec("ContactStatus");
			    contactStatusBean.setToNewID("ContactStatusID");
			    contactStatusBean.setParameter("ContactID", newContactID);
			    contactStatusBean.setParameter("StatusID", InitServlet.getSystemParams().getProperty("ContactGroupOptOutStatusID"));
			    contactStatusBean.setParameter("CreatedDate", "NOW");
			    contactStatusBean.setParameter("RepUserID", currentUser.getString("UserID"));   
			    contactStatusBean.setAction("insert");
			    contactStatusBean.doAction();
			   
			    String newContactStatusID = contactStatusBean.getString("ContactStatusID");
			   
			   // Set ContactGroupStatusID for ContactGroups
			    contactGroupsBean.clear();
			    contactGroupsBean.setParameter("ContactGroupID", newContactGroupID);
			    contactGroupsBean.setParameter("ContactGroupStatusID", newGroupStatusID);
			    contactGroupsBean.setAction("update");
			    contactGroupsBean.doAction();
			   
			   // Set ContactGroupID for GroupStatus
			    groupStatus.clear();
			    groupStatus.setParameter("GroupStatusID", newGroupStatusID);
			    groupStatus.setParameter("ContactGroupID", newContactGroupID);
			    groupStatus.setAction("update");
			    groupStatus.doAction();
			   
			   // Set ContactGroupID for ContactStatus
			    contactStatusBean.clear();
			    contactStatusBean.setParameter("ContactStatusID", newContactStatusID);
			    contactStatusBean.setParameter("ContactGroupID", newContactGroupID);
			    contactStatusBean.setAction("update");
			    contactStatusBean.doAction();
			   
			   // Set ContactStatusID for Contact
			    contactBean.clear();
			    contactBean.setParameter("ContactID", newContactID);
			    contactBean.setParameter("ContactStatusID", newContactStatusID);
			    contactBean.setAction("update");
			    contactBean.doAction();
				
				
			}
			else {
				contactID = searchBean.getString("ContactID");
				//System.out.println("Inside getReferContactBean 2 else");
			}
			getContactBean(contactID, currentUser);
			
			searchBean.close();
			
			
			RowBean contactlink = new RowBean();
			contactlink.setTableSpec("ContactLinks");
			contactlink.createNewID();
			contactlink.setAction("insert");
			contactlink.setColumn("LinkerID",referrer.getString("ContactID"));
			contactlink.setColumn("LinkeeID",contactBean.getString("ContactID"));
			contactlink.setColumn("TypeID",referrertypeid);
			contactlink.doAction();
			contactlink.createNewID();
			contactlink.setColumn("LinkerID",contactBean.getString("ContactID"));
			contactlink.setColumn("LinkeeID",referrer.getString("ContactID"));
			contactlink.setColumn("TypeID",referreetypeid);
			contactlink.doAction();
			
			return contactBean;
		}
		return null;
	}
	
	public UserBean getSystemUser() {
		UserBean currentUser = new UserBean();
		currentUser.setJndiName(ActionBean.getJndiName());
		currentUser.setViewSpec("UserView");
		currentUser.setColumn("UserID", InitServlet.getSystemParams().getProperty("SystemUserID"));
		currentUser.setAction("select");
		currentUser.doAction();
		
		return currentUser;
	}
}