package com.sok.runway;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class NoteResponseServlet extends HttpServlet
{
    public void doPost (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
    {
	    doGet(req, res);
    }


   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException
   {
		//System.out.println(request.getHeader("Referrer"));
		response.setContentType("text/html");
		if(!request.isSecure()){
			   //no cache breaks downloading pdf in ie over https
				response.setHeader("Expires","Mon, 26 Jul 1997 05:00:00 GMT" );  // disable IE caching
				response.setHeader("Cache-Control","no-cache, must-revalidate" );
				response.setHeader("Pragma","no-cache" );
				response.setDateHeader ("Expires", 0);
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/images/pixel.gif");
		dispatcher.forward(request,response);
	}

}
