/**
 * 
 */
package com.sok.runway.existingproperty;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.naming.Context;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.offline.rpmManager.RPMtask;
import com.sok.service.crm.UserService;
import com.sok.service.crm.cms.properties.PlanService;
import com.sok.service.crm.cms.properties.PlanService.ExistingProperty;

public class CreateExistingProperty extends Thread {
	private static final UserService us = UserService.getInstance();

	private static final Logger logger = LoggerFactory.getLogger(CreateExistingProperty.class);

	private HashMap<String, GenRow> productMap = new HashMap<String, GenRow>();

	private ArrayList<String> createdproducts = new ArrayList<String>();
	private ArrayList<String> productIDs = null;

	private String userid = null;

	private HttpServletRequest request = null;

	private int maxPackages = 0;
	private int currentProducts = 0;
	private Date startTime = null;

	private double timePackage = 0;
	private double timePerProduct = 0;
	private double estimatedTime = 0;
	private double estimatedRemaining = 0;

	private boolean isRunning = true;

	private Connection con = null;
	private Context ctx = null;

	protected String dbconn;
	protected ServletContext context;
	protected String contextpath;
	protected String contextname = null;
	protected Locale thislocale;
	private boolean keepAddress = false;
	private boolean keepStatusDates = false;
	private boolean keepContacts = false;
	private boolean keepRep = false;
	private boolean keepPrice = false;
	private boolean keepPublishDetails = false;
	private boolean keepImages = false;
	private boolean keepDrips = false;
	private boolean keepHomeContents = false;
	private boolean keepLandContents = false;
	private boolean duplicateHome = false;
	private boolean duplicateLand = false;
	private boolean resetContents = false;
	private boolean updatePublishHeadline = false;
	private boolean updatePublishDescription = false;
	private boolean updateImages = false;

	String sourceProductID;
	String targetPropertyType;
	int noOfProducts = 0;
	boolean duplicate = false;

	String[] createdProductIDs;

	/**
	 * 
	 */

	public CreateExistingProperty(HttpServletRequest request, ServletContext context) {
		this.request = request;
		this.context = context;
		contextname = context.getServletContextName();
		contextpath = request.getContextPath();
		dbconn = (String) context.getInitParameter("sqlJndiName");
		thislocale = request.getLocale();
	}

	public void createExistingProperty(String sourceProductID, String targetPropertyType, boolean duplicate, String userid) {
		this.sourceProductID = sourceProductID;
		this.targetPropertyType = targetPropertyType;
		this.duplicate = duplicate;
		this.userid = userid;

		startTime = new Date();
		this.start();
	}

	public void resetContents(ArrayList<String> productids, String userid) {
		this.userid = userid;
		this.productIDs = productids;

		startTime = new Date();
		this.start();
	}

	public void run() {		
		Date now = new Date();

		now = new Date();
		timePackage = now.getTime() - startTime.getTime();
		timePerProduct = (double) timePackage / currentProducts;
		estimatedTime = (double) timePerProduct * maxPackages;
		estimatedRemaining = estimatedTime - timePackage;
		if (estimatedRemaining < 0)
			estimatedRemaining = 0;

		if (resetContents)
			resetContents(productIDs);
		else if (duplicate)
			duplicateEPs();
		else
			createMultipleEPs();
		
		isRunning = false;
	}

	private String createMultipleEPs() {
		createdProductIDs = new String[noOfProducts];

		for (int i = 0; i < noOfProducts; i++) {
			createdProductIDs[i] = KeyMaker.generate();
			copy("Products", "ProductID", sourceProductID, createdProductIDs[i], userid);
			if (keepAddress)
				copy("Addresses", "ProductID", sourceProductID, createdProductIDs[i], userid);

			if (keepContacts || keepRep)
				copyplan("ProductSecurityGroups", "ProductID", sourceProductID, createdProductIDs[i], userid);

			copyDrips(sourceProductID, createdProductIDs[i], "Land", userid);
			if (RPMtask.getInstance() != null)
				RPMtask.getInstance().doIndexRefreash("update", createdProductIDs[i], "Existing Property");
		}

		linkProducts(sourceProductID, createdProductIDs);

		return String.valueOf(noOfProducts);
	}

	private String duplicateEPs() {
		createdProductIDs = new String[noOfProducts];

		for (int i = 0; i < noOfProducts; i++) {
			createdProductIDs[i] = KeyMaker.generate();
			copy("Products", "ProductID", sourceProductID, createdProductIDs[i], userid);
			if (keepAddress)
				copy("Addresses", "ProductID", sourceProductID, createdProductIDs[i], userid);

			if (keepContacts || keepRep)
				copyplan("ProductSecurityGroups", "ProductID", sourceProductID, createdProductIDs[i], userid);

			if (keepDrips)
				copyDrips(sourceProductID, createdProductIDs[i], "Land", userid);

			if (keepImages)
				copyplan("LinkedDocuments", "ProductID", sourceProductID, createdProductIDs[i], userid);

			if (keepHomeContents) {
				copy("properties/ProductDetails", "ProductID", sourceProductID, createdProductIDs[i], "HomePlanDetails", userid);
			}

			if (keepLandContents)
				copy("properties/ProductDetails", "ProductID", sourceProductID, createdProductIDs[i], "LandDetails", userid);

			if (keepPrice)
				copy("properties/ProductDetails", "ProductID", sourceProductID, createdProductIDs[i], "Existing Property", userid);

			if (duplicateHome || duplicateLand) {
				ExistingProperty ep = PlanService.getInstance().getExistingProperty(this.getConnection(), us.getSimpleUser(this.getConnection(), userid), sourceProductID);

				if (duplicateHome && StringUtils.isNotBlank(ep.PlanProductID)) {
					String planProductID = duplicateplan(ep.PlanProductID, createdProductIDs[i], userid);
					insertProductProduct(createdProductIDs[i], planProductID, userid);
				}
				if (duplicateLand && StringUtils.isNotBlank(ep.LotProductID)) {
					String lotProductID = duplicateLand(ep.LotProductID, createdProductIDs[i]);
					insertProductProduct(createdProductIDs[i], lotProductID, userid);
				}
			}
			if (RPMtask.getInstance() != null)
				RPMtask.getInstance().doIndexRefreash("update", createdProductIDs[i], "Existing Property");
		}

		String parentProductID = getParentproductID(sourceProductID);
		if (StringUtils.isNotBlank(parentProductID)) {
			linkProducts(parentProductID, createdProductIDs);
		}

		return String.valueOf(noOfProducts);
	}

	private void resetContents(ArrayList<String> productids) {

		for (String pid : productids) {
			updatePublishFields(pid, userid);
		}
	}

	private String copy(String tablespecname, String productidfieldname, String fromproductid, String toproductid, String userid) {
		return copy(tablespecname, productidfieldname, fromproductid, toproductid, null, userid);
	}

	private String copy(String tablespecname, String productidfieldname, String fromproductid, String toproductid, String param, String userid) {

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);

		row.setConnection(this.getConnection());
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);

		if ("ProductDetails".equals(row.getTableSpec().getTableName())) {
			row.setParameter("ProductType", param);
		}
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.setConnection(this.getConnection());
			clone.putAllDataAsParameters(row);
			if ("Products".equals(row.getTableSpec().getTableName())) {

				clone.setParameter("CopiedFromProductID", fromproductid);
				clone.setParameter("ProductType", "Existing Property");
				clone.setParameter("UpdateByProcess", "Y");

				if (!keepStatusDates) {
					clone.remove("PreAvailableDate");
					clone.remove("AvailableDate");
					clone.remove("ExpiryDate");
					clone.remove("Active");
					clone.remove("ProductStatusID");
				}

				if (!keepContacts) {
					clone.remove("SellingAgent");
					clone.remove("SellingAgentContactID");
					clone.remove("Vendor");
				}

				// If create linked products, ProductSubType will be different from Parent Subtype
				if (StringUtils.isNotBlank(targetPropertyType)) {
					clone.setParameter("ProductSubType", targetPropertyType);
				}

				if ("Land".equals(row.getString("ProductSubType"))) {
					// Copy from Land to Resedentail EP
					clone.remove("Summary");
					clone.remove("Description");
					clone.remove("PublishDescription");
					clone.setParameter("Category", "");
					clone.remove("PreAvailableDate");
					clone.remove("AvailableDate");
					clone.remove("DripCost");
					clone.remove("DripResult");
					clone.remove("TotalCost");
					clone.remove("Image");
					clone.remove("ThumbnailImage");
				} else {
					// Duplicating Residential EP
					if (!keepPrice) {
						clone.remove("DripCost");
						clone.remove("DripResult");
						clone.remove("TotalCost");
						clone.remove("BaseCost");
						clone.remove("Cost");
						clone.remove("GST");
					}

					if (!keepPublishDetails) {
						clone.remove("Summary");
						clone.remove("Description");
						clone.remove("PublishDescription");
					}

					if (!keepImages) {
						clone.remove("Image");
						clone.remove("ThumbnailImage");
					}
				}
			}

			if ("ProductDetails".equals(row.getTableSpec().getTableName())) {
				clone.setParameter("CopiedFromProductID", fromproductid);
			}

			clone.setParameter("CreatedDate", "NOW");
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("CreatedBy", userid);
			clone.setParameter("ModifiedBy", userid);
			clone.remove(row.getTableSpec().getPrimaryKeyName());
			clone.createNewID();
			clone.setParameter(productidfieldname, toproductid);
			try {
				clone.doAction("insert");
			} catch (Exception e) {
				clone.clearStatement();
				clone.setParameter(productidfieldname, toproductid);
				clone.doAction("updated");
			}
		}
		row.close();
		return (clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	private String copyDripCostings(String tablespecname, String productidfieldname, String fromproductid, String toproductid, String userid) {

		HashMap<String, String> keys = new HashMap<String, String>();

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);

		row.setConnection(this.getConnection());
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		while (row.getNext()) {
			keys.put(row.getString(row.getTableSpec().getPrimaryKeyName()), KeyMaker.generate());
		}

		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.setConnection(this.getConnection());
			clone.putAllDataAsParameters(row);
			clone.setParameter("CreatedDate", "NOW");
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("CreatedBy", userid);
			clone.setParameter("ModifiedBy", userid);
			// clone.remove(row.getTableSpec().getPrimaryKeyName());
			// clone.createNewID();
			clone.setParameter(productidfieldname, toproductid);
			clone.setParameter(row.getTableSpec().getPrimaryKeyName(), keys.get(row.getString(row.getTableSpec().getPrimaryKeyName())));
			if (row.getString("ParentID").length() > 0)
				clone.setParameter("ParentID", keys.get(row.getString("ParentID")));
			try {
				clone.doAction("insert");
			} catch (Exception e) {
				clone.clearStatement();
				clone.setParameter(productidfieldname, toproductid);
				clone.doAction("updated");
			}
		}
		row.close();
		return (clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	private String copyCostings(String fromproductid, String toproductid, String userid) {

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", fromproductid);
		row.setParameter("ProductType", "%Cost%");
		row.setParameter("ProductProductsLinkedProduct.ProductType", "%Cost%");
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec("ProductProducts");
			clone.setConnection(this.getConnection());
			clone.putAllDataAsParameters(row);
			// if it is a Package Cost then clone that and use the clone
			if ("Package Cost".equals(row.getData("ProductType"))) {
				String newproductid = KeyMaker.generate();
				copyplan("Products", "ProductID", row.getString("LinkedProductID"), newproductid, userid);
				clone.setParameter("LinkedProductID", newproductid);
			}
			clone.setParameter("CreatedDate", "NOW");
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("CreatedBy", userid);
			clone.setParameter("ModifiedBy", userid);
			clone.setParameter("ProductID", toproductid);
			clone.createNewID();
			clone.doAction("insert");
		}
		row.close();
		return "";
	}

	private String findRootCostings(Connection conn, String productID) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setConnection(conn);
		row.setParameter("-select0", "*");
		row.setParameter("ProductID", productID);
		row.doAction("selectfirst");

		if (row.getString("PackageCostProductID").length() > 0) {
			String tmpID = findRootCostings(conn,row.getString("PackageCostProductID"));
			if (StringUtils.isNotBlank(tmpID)) {
				return tmpID;
			} else {
				return productID;
			}
		}

		return row.getData("ProductID");
	}

	private String copyDrips(String fromproductid, String toproductid, String fromProductType, String userid) {

		GenRow row = new GenRow();
		row.setViewSpec("ProductLinkedProductView");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", fromproductid);
		row.setParameter("Category", "Discount+Rebate+Inclusion+Promotion");
		row.setParameter("Product Type", "%Drip%");
		row.setParameter("ProductProductsLinkedProduct.ProductType", "%Drip%");
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow check = new GenRow();
		check.setViewSpec("ProductLinkedProductView");

		GenRow clone = new GenRow();
		while (row.getNext()) {
			boolean checked = false;
			String testProductID = findRootCostings(this.getConnection(), row.getString("PackageCostProductID"));
			// if we don't have a root productID then skip it
			if (testProductID == null || testProductID.length() == 0)
				continue;
			check.clear();
			check.setConnection(this.getConnection());
			check.setParameter("ProductID", toproductid);
			check.setParameter("Category", "Discount+Rebate+Inclusion+Promotion");
			check.setParameter("Product Type", "%Drip%");
			check.setParameter("ProductProductsLinkedProduct.ProductType", "%Drip%");
			check.doAction(ActionBean.SEARCH);
			check.getResults();

			while (check.getNext()) {
				String foundProductID = findRootCostings(this.getConnection(), check.getString("PackageCostProductID"));
				if (testProductID.equals(foundProductID)) {
					checked = true;
					break;
				}
			}

			check.close();

			// System.out.println(check.getStatement());

			// make sure it is not already there
			if (!checked) {
				clone.clear();
				clone.setTableSpec("ProductProducts");
				clone.setConnection(this.getConnection());
				clone.putAllDataAsParameters(row);
				// if it is a Package Cost then clone that and use the clone
				if (row.getData("ProductType").indexOf("Drip") >= 0) {
					String newproductid = KeyMaker.generate();
					copyplan("Products", "ProductID", row.getString("LinkedProductID"), newproductid, fromProductType, userid);
					clone.setParameter("LinkedProductID", newproductid);
				}
				clone.setParameter("CreatedDate", "NOW");
				clone.setParameter("ModifiedDate", "NOW");
				clone.setParameter("CreatedBy", userid);
				clone.setParameter("ModifiedBy", userid);
				clone.setParameter("ProductID", toproductid);
				clone.createNewID();
				clone.doAction("insert");

				String costingID = findRootCostings(getConnection(), row.getString("LinkedProductID"));

				if (costingID != null && costingID.length() > 0) {
					copyDripRules(costingID, clone.getString("LinkedProductID"));
					copyDripContent(costingID, clone.getString("LinkedProductID"));
				}
			}
		}
		row.close();
		return "";
	}

	private void copyDripRules(String fromProductID, String toProductID) {

		GenRow pcc = new GenRow();
		pcc.setTableSpec("PackageCostings");
		pcc.setConnection(getConnection());
		pcc.setParameter("ON-ParentProductID", toProductID);
		pcc.doAction("deleteall");

		copyDripCostings("PackageCostings", "ParentProductID", fromProductID, toProductID, userid);

	}

	private void copyDripContent(String fromProductID, String toProductID) {

		GenRow link = new GenRow();
		link.setTableSpec("LinkedDocuments");
		link.setConnection(getConnection());
		link.setParameter("-select0", "*");
		link.setParameter("ProductID", fromProductID);
		link.doAction("search");

		link.getResults();

		GenRow row = new GenRow();
		GenRow bean = new GenRow();

		while (link.getNext()) {
			row.clear();
			row.setViewSpec("DocumentView");
			row.setConnection(getConnection());
			row.setParameter("DocumentID", link.getString("DocumentID"));
			row.doAction("selectfirst");

			if (row.isSuccessful()) {
				String newDocumentID = KeyMaker.generate();

				bean.clear();
				bean.setViewSpec("DocumentView");
				bean.setConnection(getConnection());
				bean.putAllDataAsParameters(row);
				bean.setColumn("DocumentID", newDocumentID);
				bean.setAction("insert");
				bean.doAction();

				bean.clear();
				bean.setTableSpec("LinkedDocuments");
				String linkedDocumentID = KeyMaker.generate();
				bean.setColumn("LinkedDocumentID", linkedDocumentID);
				bean.setColumn("CreatedDate", "NOW");
				bean.setColumn("CreatedBy", userid);
				bean.setColumn("DocumentID", newDocumentID);
				bean.setColumn("ContentID", KeyMaker.generate());
				bean.setColumn("ProductID", toProductID);
				bean.setAction("insert");
				bean.doAction();

				/*
				 * row.clear(); row.setViewSpec("LinkedDocumentView");
				 * row.setColumn("LinkedDocumentID",linkedDocumentID); row.doAction("selectfirst");
				 */
			}
		}

		link.close();
	}

	private String copyplan(String tablespecname, String productidfieldname, String fromproductid, String toproductid, String userid) {
		return copyplan(tablespecname, productidfieldname, fromproductid, toproductid, null, userid);
	}

	private String copyplan(String tablespecname, String productidfieldname, String fromproductid, String toproductid, String fromProductType, String userid) {

		GenRow row = new GenRow();
		row.setTableSpec(tablespecname);

		row.setConnection(this.getConnection());
		row.setParameter("-select1", "*");
		row.setParameter(productidfieldname, fromproductid);
		row.doAction(ActionBean.SEARCH);
		row.getResults();

		GenRow clone = new GenRow();
		while (row.getNext()) {
			clone.clear();
			clone.setTableSpec(tablespecname);
			clone.setConnection(this.getConnection());
			clone.putAllDataAsParameters(row);
			if (fromProductType != null) {
				// clone.setParameter("Summary", fromProductType);
			}

			clone.setParameter("PackageCostProductID", fromproductid);
			clone.setParameter("CopiedFromProductID", fromproductid);
			clone.setParameter("CreatedDate", "NOW");
			clone.setParameter("ModifiedDate", "NOW");
			clone.setParameter("CreatedBy", userid);
			clone.setParameter("ModifiedBy", userid);
			clone.remove(row.getTableSpec().getPrimaryKeyName());
			clone.createNewID();
			clone.setParameter(productidfieldname, toproductid);
			try {
				clone.doAction("insert");
			} catch (Exception e) {
				clone.doAction("update");
			}
		}
		row.close();
		return (clone.getString(row.getTableSpec().getPrimaryKeyName()));
	}

	private void insertProductProduct(String productid, String linkedproductid, String userid) {
		if (linkedproductid != null && linkedproductid.length() != 0) {
			GenRow row = new GenRow();
			row.setConnection(this.getConnection());
			row.setTableSpec("ProductProducts");
			row.setParameter("ProductID", productid);
			row.setParameter("LinkedProductID", linkedproductid);
			row.setParameter("CreatedDate", "NOW");
			row.setParameter("ModifiedDate", "NOW");
			row.setParameter("CreatedBy", userid);
			row.setParameter("ModifiedBy", userid);
			row.createNewID();
			row.doAction("insert");
		}
	}

	protected void insertProductVariation(String parentproductid, String childproductid, String type) {
		GenRow productvariation = new GenRow();
		productvariation.setConnection(this.getConnection());
		productvariation.setTableSpec("ProductVariations");
		productvariation.setParameter("ParentProductID", parentproductid);
		productvariation.setParameter("ChildProductID", childproductid);
		productvariation.setParameter("Type", type);
		productvariation.createNewID();
		try {
			productvariation.doAction("insert");
		} catch (Exception e) {
			productvariation.doAction("update");
		}
	}

	private String duplicateplan(String oldProductID, String linkProductID, String userID) {
		return duplicateplan(oldProductID, KeyMaker.generate(), linkProductID, userID);
	}

	private String duplicateplan(String oldProductID, String newproductid, String linkProductID, String userID) {
		GenRow existingProduct = getCachedViewedProduct("properties/ProductPlanView", "ProductID", oldProductID);

		String homeProductID = existingProduct.getString("HomeProductID");

		copyplan("Products", "ProductID", oldProductID, newproductid, userID);
		copyplan("ProductSecurityGroups", "ProductID", oldProductID, newproductid, userID);
		insertProductVariation(homeProductID, newproductid, "Home");

		copyplan("LinkedDocuments", "ProductID", oldProductID, newproductid, userID);
		copyplan("properties/HomeDetails", "ProductID", oldProductID, newproductid, userID);
		copyplan("properties/ProductDetails", "ProductID", oldProductID, newproductid, userID);
		copyplan("ProductProducts", "ProductID", oldProductID, newproductid, userid);
		copyCostings(oldProductID, newproductid, userID);
		copyplan("ProductCommissions", "ProductID", oldProductID, newproductid, userID);

		GenRow duplicateProduct = new GenRow();
		duplicateProduct.setConnection(this.getConnection());
		duplicateProduct.setTableSpec("Products");
		duplicateProduct.setParameter("ProductID", newproductid);
		duplicateProduct.setParameter("CopiedFromProductID", oldProductID);
		duplicateProduct.setParameter("Category", "PlanSale");
		duplicateProduct.setParameter("UpdateByProcess", "Y");
		duplicateProduct.doAction("update");

		return newproductid;
	}

	protected String calculate(String string) {
		if (string == null || string.length() == 0)
			return "";

		if ("-".equals(string))
			return string;

		int count = 0;
		String result = string.replaceAll("  ", " ").replaceAll("  ", " ");
		// do this until there is no more maths or 20 attempts (so we don't get
		// into an endless loop due to bad maths
		while (result.length() != result.replaceAll("[\\+\\-\\*\\/]", "").length() && ++count < 20) {
			// tokenize it, make sure all double spaces become single space
			StringTokenizer st = new StringTokenizer(result, " ");
			while (st.hasMoreTokens()) {
				String token1 = st.nextToken();
				if (token1.indexOf("=") >= 0 || token1.indexOf("<") >= 0 || token1.indexOf(">") >= 0)
					continue;
				if (token1.indexOf("+") >= 0 || token1.indexOf("-") >= 0 || token1.indexOf("*") >= 0 || token1.indexOf("/") >= 0)
					break;
				if (st.hasMoreTokens()) {
					String maths = st.nextToken();
					if (maths.indexOf("=") >= 0 || maths.indexOf("<") >= 0 || maths.indexOf(">") >= 0)
						continue;
					if (maths.indexOf("-") == -1 && maths.indexOf("+") == -1 && maths.indexOf("*") == -1 && maths.indexOf("/") == -1)
						continue;
					if (st.hasMoreTokens()) {
						String token2 = st.nextToken();
						if (token2.indexOf("=") >= 0 || token2.indexOf("<") >= 0 || token2.indexOf(">") >= 0)
							continue;
						double a = 0;
						try {
							double d1 = Double.parseDouble(token1);
							double d2 = Double.parseDouble(token2);
							if ("+".equals(maths))
								a = d1 + d2;
							else if ("-".equals(maths))
								a = d1 - d2;
							else if ("*".equals(maths))
								a = d1 * d2;
							else if ("/".equals(maths))
								a = d1 / d2;
							a = (double) Math.floor((double) a * 1000) / 1000;
						} catch (Exception e) {
						}
						StringBuilder sb = (new StringBuilder(token1)).append(" \\").append(maths).append(" ").append(token2);
						if (!result.equals(result.replaceFirst(sb.toString(), String.valueOf(a)))) {
							result = result.replaceFirst(sb.toString(), String.valueOf(a));
						} else {
							sb = (new StringBuilder(token1)).append(" \\").append(maths).append(" ").append(token2);
							if (!result.equals(result.replaceFirst(sb.toString(), String.valueOf(a)))) {
								result = result.replaceFirst(sb.toString(), String.valueOf(a));
							} else {
								sb = (new StringBuilder(token1)).append(maths).append(token2);
								if (!result.equals(result.replaceFirst(sb.toString(), String.valueOf(a)))) {
									result = result.replaceFirst(sb.toString(), String.valueOf(a));
								} else {
									sb = (new StringBuilder(token1)).append("\\").append(maths).append(token2);
									if (!result.equals(result.replaceFirst(sb.toString(), String.valueOf(a)))) {
										result = result.replaceFirst(sb.toString(), String.valueOf(a));
									}

								}

							}
						}
					}
				}
			}
		}

		return result;
	}

	// we need to cache everything we can to speed this up a lot
	private GenRow getCachedTabledProduct(String tableSpec, String key, String value) {
		GenRow row = productMap.get(value + tableSpec);

		if (row != null)
			return row;

		row = new GenRow();
		row.setTableSpec(tableSpec);
		row.setConnection(this.getConnection());
		row.setParameter("-select", "*");
		row.setColumn(key, value);

		row.doAction("selectfirst");

		productMap.put(value + tableSpec, row);

		return row;
	}

	private GenRow getCachedViewedProduct(String viewSpec, String key, String value) {
		GenRow row = productMap.get(value + viewSpec.replaceAll("properties/Product", ""));

		if (row != null)
			return row;

		row = new GenRow();
		row.setViewSpec(viewSpec);
		row.setConnection(this.getConnection());
		row.setColumn(key, value);

		row.doAction("selectfirst");

		productMap.put(value + viewSpec.replaceAll("properties/Product", ""), row);

		return row;
	}

	protected ArrayList<String> getGalleryIDs(String productID) {
		ArrayList<String> galleryIDs = new ArrayList<String>();
		if (productID != null && productID.length() > 0) {

			GenRow row = new GenRow();
			row.setViewSpec("GalleryView");
			row.setConnection(this.getConnection());
			row.setParameter("ProductID", productID);
			row.doAction("search");
			row.getResults();

			while (row.getNext()) {
				galleryIDs.add(row.getData("GalleryID"));
			}

			row.close();
		}

		return galleryIDs;
	}

	protected String mergePublishers(String a, String b) {
		String[] parts = b.split("\\+");

		for (int p = 0; p < parts.length; ++p) {
			if (parts[p].length() > 0) {
				if (a.indexOf(parts[p]) == -1) {
					if (a.length() > 0)
						a += "+";
					a += parts[p];
				}
			}
		}

		return a;
	}

	protected void addDocumentToGallery(String productID, String galleryID, String documentID, String userID, String sortNumber) {
		GenRow row = new GenRow();
		row.setTableSpec("LinkedDocuments");
		row.setConnection(this.getConnection());
		row.setParameter("ProductID", productID);
		row.setParameter("GalleryID", galleryID);
		row.setParameter("DocumentID", documentID);
		row.setParameter("CreatedBy", userID);
		row.setParameter("CreatedDate", "NOW");
		row.setParameter("ShowOnGalleryOnly", "Y");
		row.setParameter("ShowOnGalleryOrder", sortNumber);
		row.setParameter("ShowOnREorder", sortNumber);
		row.setParameter("SortOrder", sortNumber);
		row.setParameter("LinkedDocumentID", KeyMaker.generate());

		row.doAction("insert");
	}

	private String duplicateLand(String oldProductID, String linkProductID) {
		String newproductid = KeyMaker.generate();

		GenRow existingProduct = getCachedViewedProduct("properties/ProductLotView", "ProductID", oldProductID);
		String stageProductID = existingProduct.getString("StageProductID");

		copyplan("Products", "ProductID", oldProductID, newproductid, userid);
		copyplan("ProductSecurityGroups", "ProductID", oldProductID, newproductid, userid);
		copyplan("LinkedDocuments", "ProductID", oldProductID, newproductid, userid);
		copyplan("properties/ProductDetails", "ProductID", oldProductID, newproductid, userid);
		copyplan("properties/HomeDetails", "ProductID", oldProductID, newproductid, userid);
		copy("Addresses", "ProductID", oldProductID, newproductid, userid);
		insertProductVariation(stageProductID, newproductid, "Stage");
		copyplan("ProductProducts", "ProductID", oldProductID, newproductid, userid);

		GenRow duplicateProduct = new GenRow();
		duplicateProduct.setConnection(this.getConnection());
		duplicateProduct.setTableSpec("Products");
		duplicateProduct.setParameter("ProductID", newproductid);
		duplicateProduct.setParameter("CopiedFromProductID", oldProductID);
		duplicateProduct.setParameter("UpdateByProcess", "Y");
		duplicateProduct.doAction("update");

		return newproductid;

	}

	private String getParentproductID(String productID) {
		GenRow parentProduct = new GenRow();
		parentProduct.setConnection(this.getConnection());
		parentProduct.setViewSpec("RelatedParentProductListView");
		parentProduct.setParameter("LinkedProductID", productID);
		parentProduct.doAction(GenerationKeys.SELECTFIRST);
		return parentProduct.getString("ProductID");
	}

	private void linkProducts(String productID, String[] linkedProductIDList) {
		GenRow relatedProducts = new GenRow();
		relatedProducts.setConnection(this.getConnection());
		relatedProducts.setTableSpec("RelatedProducts");
		relatedProducts.setParameter("-select0", " Max(SortNumber) as MaxSortNumber");
		relatedProducts.setParameter("ProductID", productID);
		relatedProducts.doAction(GenerationKeys.SELECTFIRST);
		int sortNumber = relatedProducts.getInt("MaxSortNumber");

		for (String linkedProduct : linkedProductIDList) {
			relatedProducts.createNewID();
			relatedProducts.setParameter("LinkedProductID", linkedProduct);
			relatedProducts.setParameter("SortNumber", ++sortNumber);
			relatedProducts.doAction(GenerationKeys.INSERT);
		}
	}

	protected Connection getConnection() {
		try {
			if (con == null || con.isClosed()) {
				con = ActionBean.connect();
			}
		} catch (Exception e) {
			return null;
		}

		return con;
	}

	public void parseRequestOptions(HttpServletRequest request) {
		if ("true".equals(request.getParameter("ResetContents")))
			resetContents = true;
		if ("true".equals(request.getParameter("KeepAddress")))
			keepAddress = true;
		if ("true".equals(request.getParameter("KeepStatusDates")))
			keepStatusDates = true;
		if ("true".equals(request.getParameter("KeepContacts")))
			keepContacts = true;
		if ("true".equals(request.getParameter("KeepRep")))
			keepRep = true;
		if ("true".equals(request.getParameter("KeepPublishDetails")))
			keepPublishDetails = true;
		if ("true".equals(request.getParameter("KeepPrice")))
			keepPrice = true;
		if ("true".equals(request.getParameter("KeepImages")))
			keepImages = true;
		if ("true".equals(request.getParameter("KeepDrips")))
			keepDrips = true;

		if ("true".equals(request.getParameter("KeepHomeContents")))
			keepHomeContents = true;
		if ("true".equals(request.getParameter("KeepLandContents")))
			keepLandContents = true;
		if ("true".equals(request.getParameter("DuplicateHome")))
			duplicateHome = true;
		if ("true".equals(request.getParameter("DuplicateLand")))
			duplicateLand = true;

		if ("true".equals(request.getParameter("UpdatePublishHeadline")))
			updatePublishHeadline = true;
		if ("true".equals(request.getParameter("UpdatePublishDescription")))
			updatePublishDescription = true;
		if ("true".equals(request.getParameter("UpdateImages")))
			updateImages = true;

		if (StringUtils.isNotBlank(request.getParameter("count"))) {
			try {
				noOfProducts = Integer.parseInt(request.getParameter("count"));
			} catch (Exception e) {
				// ignore
				logger.error("Ignoring error converting count");
			}
		}
	}

	public void updatePublishFields(String productid, String userid) {
		GenRow product = getCachedViewedProduct("properties/ProductExistingPropertyView", "ProductID", productid);

		GenRow pp = new GenRow();
		pp.setViewSpec("properties/ProductPublishingView");
		pp.setConnection(getConnection());

		boolean update = false;

		if (updatePublishHeadline && StringUtils.isNotBlank(product.getString("Summary"))) {
			update = true;
			pp.setParameter("Summary", product.getString("Summary"));
		}

		if (updatePublishDescription && StringUtils.isNotBlank(product.getString("PublishDescription"))) {
			update = true;
			pp.setParameter("Description", product.getString("PublishDescription"));
		}

		if (updateImages && StringUtils.isNotBlank(product.getString("Image"))) {
			update = true;
			pp.setParameter("HeroImageID", product.getString("Image"));
		}

		if (update) {
			pp.setParameter("ModifiedBy", userid);
			pp.setParameter("ModifiedDate", "NOW");
			pp.setParameter("ON-ProductID", productid);
			pp.doAction("updateall");
		}

		pp.close();
	}

	public ArrayList<String> getCreatedpackages() {
		if (!isRunning)
			return createdproducts;
		else
			return null;
	}

	public void stopRunning() {
		isRunning = false;
	}

	public String getProgressJSON() {

		double remaining = ((double) Math.round(estimatedRemaining / 1000));
		if (remaining < 1)
			remaining = 0;
		String remStr = "" + remaining;
		if (remaining < 10) {
			remStr = "0:0" + remaining;
		} else if (remaining < 60) {
			remStr = "0:" + remaining;
		} else {
			remStr = "" + Math.round(remaining / 60);
			remaining = remaining % 60;
			if (remaining == 0) {
				remStr += ":00";
			} else if (remaining < 10) {
				remStr += ":0" + remaining;
			} else if (remaining < 60) {
				remStr += ":" + remaining;
			}
		}
		if (remStr.indexOf(".") >= 0) {
			remStr = remStr.substring(0, remStr.indexOf("."));
		}
		/*
		String json = "{";
		json += "\"currentPackage\" : \"" + currentProducts + "\",";
		json += "\"maxPackage\" : \"" + maxPackages + "\",";
		json += "\"packageTime\" : \"" + Math.round(timePerProduct) + "\",";
		json += "\"packageTotal\" : \"" + ((double) Math.round(estimatedTime / 100) / 10) + "\",";
		json += "\"remainingTotal\" : \"" + remStr + "\",";
		// json += "\"duplicatesSkiped\" : \"" + failedDuplicates + "\",";
		// json += "\"passedRules\" : \"" + passedRules + "\",";
		// json += "\"failedRules\" : \"" + failedRules + "\",";
		// json += "\"recreateTask\" : \"" + recreateTask + "\",";
		json += "\"completed\" : \"" + (getCreatedpackages() != null ? "true" : "false") + "\"";
		/*
		 * if (getCreatedpackages() != null) json += ",\"message\" : \"" + StringUtil
		 * .urlEncode(failedMessages.length() > 0 ? failedMessages : (passedRules +
		 * " Package Created without any errors")) + "\"}"; else json += "}";
		 */
		//return json;		
		
		JSONObject obj = new JSONObject();
		obj.put("currentPackage", currentProducts);
		obj.put("maxPackage", maxPackages);
		obj.put("packageTime", Math.round(timePerProduct));
		obj.put("packageTotal", ((double) Math.round(estimatedTime / 100) / 10));
		obj.put("remainingTotal", remStr);
		obj.put("completed", (getCreatedpackages() != null ? "true" : "false"));
		if (getCreatedpackages() != null)
		{
			//obj.put("message",failedMessages.length() > 0 ? failedMessages : (passedRules + " Package Created without any errors"));			
		}		
		logger.trace(obj.toJSONString());
		return obj.toJSONString();
	}

	protected String arrayToString(ArrayList<String> array) {
		String text = "";

		Iterator<String> i = array.iterator();

		while (i.hasNext()) {
			if (text.length() > 0)
				text += "+";
			text += i.next();
		}

		return text;
	}
}