package com.sok.runway;

import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

public class JSPLogger {

	public static void debug(Object caller ,String arg0) {
		LoggerFactory.getLogger(caller.getClass()).debug(arg0);
	}

	public static void debug(Object caller ,String arg0, Object arg1) {
		LoggerFactory.getLogger(caller.getClass()).debug(arg0, arg1);
	}

	public static void debug(Object caller ,String arg0, Object[] arg1) {
		LoggerFactory.getLogger(caller.getClass()).debug(arg0, arg1);
	}

	public static void debug(Object caller ,String arg0, Throwable arg1) {
		LoggerFactory.getLogger(caller.getClass()).debug(arg0, arg1);
	}

	public static void debug(Object caller ,Marker arg0, String arg1) {
		LoggerFactory.getLogger(caller.getClass()).debug(arg0, arg1);
	}

	public static void debug(Object caller ,String arg0, Object arg1, Object arg2) {
		LoggerFactory.getLogger(caller.getClass()).debug(arg0, arg1, arg2);
	}

	public static void debug(Object caller ,Marker arg0, String arg1, Object arg2) {
		LoggerFactory.getLogger(caller.getClass()).debug(arg0, arg1, arg2);
	}

	public static void debug(Object caller ,Marker arg0, String arg1, Object[] arg2) {
		LoggerFactory.getLogger(caller.getClass()).debug(arg0, arg1, arg2);
	}

	public static void debug(Object caller ,Marker arg0, String arg1, Throwable arg2) {
		LoggerFactory.getLogger(caller.getClass()).debug(arg0, arg1, arg2);
	}

	public static void debug(Object caller ,Marker arg0, String arg1, Object arg2, Object arg3) {
		LoggerFactory.getLogger(caller.getClass()).debug(arg0, arg1, arg2, arg3);
	}
	
	public static void error(Object caller ,String arg0) {
		LoggerFactory.getLogger(caller.getClass()).error(arg0);
	}

	public static void error(Object caller ,String arg0, Object arg1) {
		LoggerFactory.getLogger(caller.getClass()).error(arg0, arg1);
	}

	public static void error(Object caller ,String arg0, Object[] arg1) {
		LoggerFactory.getLogger(caller.getClass()).error(arg0, arg1);
	}

	public static void error(Object caller ,String arg0, Throwable arg1) {
		LoggerFactory.getLogger(caller.getClass()).error(arg0, arg1);
	}

	public static void error(Object caller ,Marker arg0, String arg1) {
		LoggerFactory.getLogger(caller.getClass()).error(arg0, arg1);
	}

	public static void error(Object caller ,String arg0, Object arg1, Object arg2) {
		LoggerFactory.getLogger(caller.getClass()).error(arg0, arg1, arg2);
	}

	public static void error(Object caller ,Marker arg0, String arg1, Object arg2) {
		LoggerFactory.getLogger(caller.getClass()).error(arg0, arg1, arg2);
	}

	public static void error(Object caller ,Marker arg0, String arg1, Object[] arg2) {
		LoggerFactory.getLogger(caller.getClass()).error(arg0, arg1, arg2);
	}

	public static void error(Object caller ,Marker arg0, String arg1, Throwable arg2) {
		LoggerFactory.getLogger(caller.getClass()).error(arg0, arg1, arg2);
	}

	public static void error(Object caller ,Marker arg0, String arg1, Object arg2, Object arg3) {
		LoggerFactory.getLogger(caller.getClass()).error(arg0, arg1, arg2, arg3);
	}

	public static void info(Object caller ,String arg0) {
		LoggerFactory.getLogger(caller.getClass()).info(arg0);
	}

	public static void info(Object caller ,String arg0, Object arg1) {
		LoggerFactory.getLogger(caller.getClass()).info(arg0, arg1);
	}

	public static void info(Object caller ,String arg0, Object[] arg1) {
		LoggerFactory.getLogger(caller.getClass()).info(arg0, arg1);
	}

	public static void info(Object caller ,String arg0, Throwable arg1) {
		LoggerFactory.getLogger(caller.getClass()).info(arg0, arg1);
	}

	public static void info(Object caller ,Marker arg0, String arg1) {
		LoggerFactory.getLogger(caller.getClass()).info(arg0, arg1);
	}

	public static void info(Object caller ,String arg0, Object arg1, Object arg2) {
		LoggerFactory.getLogger(caller.getClass()).info(arg0, arg1, arg2);
	}

	public static void info(Object caller ,Marker arg0, String arg1, Object arg2) {
		LoggerFactory.getLogger(caller.getClass()).info(arg0, arg1, arg2);
	}

	public static void info(Object caller ,Marker arg0, String arg1, Object[] arg2) {
		LoggerFactory.getLogger(caller.getClass()).info(arg0, arg1, arg2);
	}

	public static void info(Object caller ,Marker arg0, String arg1, Throwable arg2) {
		LoggerFactory.getLogger(caller.getClass()).info(arg0, arg1, arg2);
	}

	public static void info(Object caller ,Marker arg0, String arg1, Object arg2, Object arg3) {
		LoggerFactory.getLogger(caller.getClass()).info(arg0, arg1, arg2, arg3);
	}

	public static boolean isDebugEnabled(Object caller) {
		return LoggerFactory.getLogger(caller.getClass()).isDebugEnabled();
	}

	public static boolean isDebugEnabled(Object caller ,Marker arg0) {
		return LoggerFactory.getLogger(caller.getClass()).isDebugEnabled(arg0);
	}

	public static boolean isErrorEnabled(Object caller) {
		return LoggerFactory.getLogger(caller.getClass()).isErrorEnabled();
	}

	public static boolean isErrorEnabled(Object caller ,Marker arg0) {
		return LoggerFactory.getLogger(caller.getClass()).isErrorEnabled(arg0);
	}
	public static boolean isInfoEnabled(Object caller) {
		return LoggerFactory.getLogger(caller.getClass()).isInfoEnabled();
	}

	public static boolean isInfoEnabled(Object caller ,Marker arg0) {
		return LoggerFactory.getLogger(caller.getClass()).isInfoEnabled(arg0);
	}

	public static boolean isTraceEnabled(Object caller) {
		return LoggerFactory.getLogger(caller.getClass()).isTraceEnabled();
	}

	public static boolean isTraceEnabled(Object caller ,Marker arg0) {
		return LoggerFactory.getLogger(caller.getClass()).isTraceEnabled(arg0);
	}

	public static boolean isWarnEnabled(Object caller) {
		return LoggerFactory.getLogger(caller.getClass()).isWarnEnabled();
	}

	public static void trace(Object caller ,String arg0) {
		LoggerFactory.getLogger(caller.getClass()).trace(arg0);
	}

	public static void trace(Object caller ,String arg0, Object arg1) {
		LoggerFactory.getLogger(caller.getClass()).trace(arg0, arg1);
	}

	public static void trace(Object caller ,String arg0, Object[] arg1) {
		LoggerFactory.getLogger(caller.getClass()).trace(arg0, arg1);
	}

	public static void trace(Object caller ,String arg0, Throwable arg1) {
		LoggerFactory.getLogger(caller.getClass()).trace(arg0, arg1);
	}

	public static void trace(Object caller ,Marker arg0, String arg1) {
		LoggerFactory.getLogger(caller.getClass()).trace(arg0, arg1);
	}

	public static void trace(Object caller ,String arg0, Object arg1, Object arg2) {
		LoggerFactory.getLogger(caller.getClass()).trace(arg0, arg1, arg2);
	}

	public static void trace(Object caller ,Marker arg0, String arg1, Object arg2) {
		LoggerFactory.getLogger(caller.getClass()).trace(arg0, arg1, arg2);
	}

	public static void trace(Object caller ,Marker arg0, String arg1, Object[] arg2) {
		LoggerFactory.getLogger(caller.getClass()).trace(arg0, arg1, arg2);
	}

	public static void trace(Object caller ,Marker arg0, String arg1, Throwable arg2) {
		LoggerFactory.getLogger(caller.getClass()).trace(arg0, arg1, arg2);
	}

	public static void trace(Object caller ,Marker arg0, String arg1, Object arg2, Object arg3) {
		LoggerFactory.getLogger(caller.getClass()).trace(arg0, arg1, arg2, arg3);
	}

	public static void warn(Object caller ,String arg0) {
		LoggerFactory.getLogger(caller.getClass()).warn(arg0);
	}

	public static void warn(Object caller ,String arg0, Object arg1) {
		LoggerFactory.getLogger(caller.getClass()).warn(arg0, arg1);
	}

	public static void warn(Object caller ,String arg0, Object[] arg1) {
		LoggerFactory.getLogger(caller.getClass()).warn(arg0, arg1);
	}

	public static void warn(Object caller ,String arg0, Throwable arg1) {
		LoggerFactory.getLogger(caller.getClass()).warn(arg0, arg1);
	}

	public static void warn(Object caller ,Marker arg0, String arg1) {
		LoggerFactory.getLogger(caller.getClass()).warn(arg0, arg1);
	}

	public static void warn(Object caller ,String arg0, Object arg1, Object arg2) {
		LoggerFactory.getLogger(caller.getClass()).warn(arg0, arg1, arg2);
	}

	public static void warn(Object caller ,Marker arg0, String arg1, Object arg2) {
		LoggerFactory.getLogger(caller.getClass()).warn(arg0, arg1, arg2);
	}

	public static void warn(Object caller ,Marker arg0, String arg1, Object[] arg2) {
		LoggerFactory.getLogger(caller.getClass()).warn(arg0, arg1, arg2);
	}

	public static void warn(Object caller ,Marker arg0, String arg1, Throwable arg2) {
		LoggerFactory.getLogger(caller.getClass()).warn(arg0, arg1, arg2);
	}

	public static void warn(Object caller ,Marker arg0, String arg1, Object arg2, Object arg3) {
		LoggerFactory.getLogger(caller.getClass()).warn(arg0, arg1, arg2, arg3);
	}
}
