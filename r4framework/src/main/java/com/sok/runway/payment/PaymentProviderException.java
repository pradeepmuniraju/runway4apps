package com.sok.runway.payment;

public class PaymentProviderException extends RuntimeException
{
    public PaymentProviderException(String message)
    {
        super(message);
    }
    
    public PaymentProviderException(Exception ex)
    {
        super(ex);
    }    
    
    public PaymentProviderException(String message, Exception ex)
    {
        super(message, ex);
    }       
}