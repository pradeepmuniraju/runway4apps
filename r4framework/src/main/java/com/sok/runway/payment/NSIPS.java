/*
 * NSIPS.java
 *
 * Created on April 20, 2007, 4:45 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.sok.runway.payment;

import java.util.*;

import com.sok.framework.*;
import com.sok.runway.*;
import java.security.MessageDigest;

/**
 *
 * @author root
 */
public class NSIPS extends AbstractPaymentProvider
{
   protected HashMap sendparameters = new HashMap();
   
   private static final String _merchid = "merchid";
   private static final String _mtid = "mtid";   //Invoice Number
   private static final String _amt = "amt"; //amount
   private static final String _successurl = "successurl";
   private static final String _failurl = "failurl";
   
   
   public NSIPS(PaymentBean bean)
   {
      super(bean);
   }
   
   public void sendPayment()
   {
      throw new NotImplementedException();
   }
   
   public String getPaymentURL()
   {
      setupParameters();
      StringBuffer params = new StringBuffer();
      params.append(bean.getServerURL());
      params.append("?");
      appendQueryFields(params, sendparameters);
      String paramstr = params.toString();
      log("SENT:");
      log(paramstr);
      writeLog();
      return(paramstr);
   }
   
   protected void setupParameters()
   {
      int type = bean.getType();
      
      if(type != PaymentBean.PAYMENT)
      {
         throw new NotImplementedException();
      }
      
      
      if(bean.isTest())
      {
         //sendparameters.put(_Merchant,"TEST" + bean.getMerchantID());
      }
      else
      {
         sendparameters.put(_merchid,bean.getMerchantID());
      }
      
      if(type == PaymentBean.PAYMENT)
      {
         sendparameters.put(_mtid,bean.getOrderID());
      }
      
      sendparameters.put(_successurl,bean.getReturnURL()+"?OrderID=#I#");
      sendparameters.put(_failurl,bean.getFailedURL()+"?OrderID=#I#");
      
      sendparameters.put(_amt,bean.getAmount());
      
      if(bean.getMode() != PaymentBean.SERVER_HOSTED)
      {
         throw new NotImplementedException();
      }
   }
   
   public void appendQueryFields(StringBuffer sb, Map m)
   {
      Iterator keys = sendparameters.keySet().iterator();
      String key = null;
      Object obj = null;
      String value = null;
      int sblength = sb.length();
      while(keys.hasNext())
      {
         key = (String)keys.next();
         obj = sendparameters.get(key);
         
         value = obj.toString();
         
         if(sb.length()!=sblength)
         {
            sb.append('&');
         }
         try
         {
            sb.append(StringUtil.urlEncode(key));
            sb.append('=');
            sb.append(StringUtil.urlEncode(value));
         }
         catch(Exception e)
         {}
         
         
      }
   }
   
   public void setPaymentResponse(Map response)
   {
      throw new NotImplementedException();
   }
   
   public String getRunwayPaymentStatusID()
   {
      throw new NotImplementedException();
   }
}
