package com.sok.runway.payment;

import com.sok.runway.*;
import java.util.logging.*;
import java.util.*;

public abstract class AbstractPaymentProvider implements PaymentProvider
{   
    protected StringBuffer log = new StringBuffer();
    protected StringBuffer templog = new StringBuffer();
    
    PaymentBean bean = null;
    
    public AbstractPaymentProvider(PaymentBean bean)
    {
        this.bean = bean;
    }
    
    abstract public void sendPayment();
    
    abstract public String getPaymentURL();
    
    abstract public void setPaymentResponse(Map response);
    
    abstract public String getRunwayPaymentStatusID();
    
    public void log(String message)
    {
        log.append(message);
        log.append("\r\n");
        templog.append(message);
        templog.append("\r\n");        
    }
    
    public void writeLog()
    {
        bean.log(Level.INFO, templog.toString());
        templog = new StringBuffer();
    }
    
    public String getLog()
    {
        return(log.toString());
    }
    
}