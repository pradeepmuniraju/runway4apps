/*
 * NotImplementedException.java
 *
 * Created on April 20, 2007, 4:50 PM
 *
 */

package com.sok.runway.payment;

/**
 *
 * @author root
 */
public class NotImplementedException extends RuntimeException
{
   
   /** Creates a new instance of NotImplementedException */
   public NotImplementedException()
   {
       super();
   }
   
}
