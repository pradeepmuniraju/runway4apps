package com.sok.runway.payment;

import java.util.ArrayList;

/**
 * The CreditCardTypes class holds the list of available credit card types.
 * This is a singleton class
 *  
 * @author raymond.dinata
 */
public class CreditCardTypes 
{
	// Private Static variable for a singleton
	private static CreditCardTypes _instance = null;;
	
	/** List of available credit card types */
	private ArrayList<CreditCardType> cardTypeList = null;
	
	/**
	 * Constructor - initialises the list of available credit card types with default values
	 */
	protected CreditCardTypes()
	{
		init();
	}
	
	/**
	 * Returns the CreditCardTypes instance
	 * @return the CreditCardTypes instance
	 */
	public static synchronized CreditCardTypes getCreditCardTypes()
	{
		if (_instance == null)
			_instance = new CreditCardTypes();
		return _instance;
	}
	
	/** 
	 * Initialises the class with a default available credit card types
	 */
	private void init()
	{
		cardTypeList = new ArrayList<CreditCardType>();
		
		cardTypeList.add(new CreditCardType("Visa", "4", "13,16"));
		cardTypeList.add(new CreditCardType("MasterCard", "51,52,53,54,55", "16"));
		cardTypeList.add(new CreditCardType("BankCard", "56", "16"));
		cardTypeList.add(new CreditCardType("Amex", "34,37", "15"));
		cardTypeList.add(new CreditCardType("American Express", "34,37", "15"));
		cardTypeList.add(new CreditCardType("Diners Club", "300,301,302,303,304,305,36,38", "14"));
	}

	/**
	 * Returns the list of available card types
	 * @return cardTypeList the list of available card types
	 */
	public ArrayList<CreditCardType> getCardTypeList() {
		return cardTypeList;
	}
	
	/**
	 * Returns the credit card type for a given type name
	 * @param typeName the credit card type name to search
	 * @return the credit card type
	 */
	public CreditCardType getCreditCardType(String typeName)
	{
		for (CreditCardType ccType: cardTypeList)
			if (ccType.getName().equalsIgnoreCase(typeName))
				return ccType;
		
		return null;
	}
}
