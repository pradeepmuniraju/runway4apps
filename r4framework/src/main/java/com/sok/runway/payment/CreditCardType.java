package com.sok.runway.payment;

import java.util.HashSet;

/**
 * The CreditCardType class represents a credit card type, which holds:
 * - credit card type name, e.g: Visa
 * - list of prefix numbers, e.g: 51,52,53,54,55 for MasterCard
 * - list of the credit card number lengths, e.g: 13,16 for Visa 
 * 
 * @author raymond.dinata
 */
public class CreditCardType 
{
	/** The credit card type name, e.g: Visa */
	private String name = null;

	/** The list of prefix numbers, e.g: 51,52,53,54,55 for MasterCard */
	private HashSet<String> prefixes = new HashSet<String>();

	/** The list of the credit card number lengths, e.g: 13,16 for Visa */
	private HashSet<String> numLengths = new HashSet<String>();

	/**
	 * Constructor - initialises the name, list of prefixes, and list of credit card number lengths
	 * @param name The credit card type name, e.g: Visa
	 * @param prefixes The list of prefix numbers in comma separated string , e.g: 51,52,53,54,55 for MasterCard 
	 * @param numLengths The list of the credit card number lengths in comma separated string, e.g: 13,16 for Visa
	 */
	public CreditCardType(String name, String prefixes, String numLengths)
	{
		this.name = name;
		
		if ( (prefixes != null) && (prefixes.trim().length() != 0) )
		{
			String[] prefixList = prefixes.split(",");
			for (String prefix: prefixList)
				addPrefix(prefix);
		}

		if ( (numLengths != null) && (numLengths.trim().length() != 0) )
		{
			String[] numLengthList = numLengths.split(",");
			for (String numLength: numLengthList)
				addNumLength(numLength);
		}
	}
	
	/**
	 * Adds a prefix to the current list of prefix numbers
	 * @param prefix prefix number to add
	 */
	public void addPrefix(String prefix)
	{
		prefixes.add(prefix);
	}

	/**
	 * Adds a number length to the current list of credit card number lengths
	 * @param numLength the number length to add
	 */
	public void addNumLength(String numLength)
	{
		numLengths.add(numLength);
	}
	
	/**
	 * Returns the credit card type name, e.g: Visa 
	 * @return the name the credit card type name, e.g: Visa
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the credit card type name, e.g: Visa
	 * @param name the credit card type name, e.g: Visa
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the list of prefix numbers, e.g: 51,52,53,54,55 for MasterCard 
	 * @return the prefixes the list of prefix numbers, e.g: 51,52,53,54,55 for MasterCard 
	 */
	public HashSet<String> getPrefixes() {
		return prefixes;
	}

	/**
	 * Sets the list of prefix numbers, e.g: 51,52,53,54,55 for MasterCard 
	 * @param prefixes the list of prefix numbers, e.g: 51,52,53,54,55 for MasterCard 
	 */
	public void setPrefixes(HashSet<String> prefixes) {
		this.prefixes = prefixes;
	}

	/**
	 * Returns the list of the credit card number lengths, e.g: 13,16 for Visa 
	 * @return the numLengths the list of the credit card number lengths, e.g: 13,16 for Visa
	 */
	public HashSet<String> getNumLengths() {
		return numLengths;
	}

	/**
	 * Sets the list of the credit card number lengths, e.g: 13,16 for Visa
	 * @param numLengths the list of the credit card number lengths, e.g: 13,16 for Visa
	 */
	public void setNumLengths(HashSet<String> numLengths) {
		this.numLengths = numLengths;
	}
}
