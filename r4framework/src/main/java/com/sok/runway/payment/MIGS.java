package com.sok.runway.payment;

import java.util.*;

import com.sok.framework.*;
import com.sok.runway.*;
import java.security.MessageDigest;

public class MIGS extends AbstractPaymentProvider
{   
	
    private static final String _Version = "vpc_Version";
    private static final String _Command = "vpc_Command";
    private static final String _AccessCode = "vpc_AccessCode";
    private static final String _Merchant = "vpc_Merchant";
    private static final String _OrderInfo = "vpc_OrderInfo";
    private static final String _Amount = "vpc_Amount";
    private static final String _CardNum = "vpc_CardNum";
    private static final String _CardExp = "vpc_CardExp";
    private static final String _CardSecurityCode = "vpc_CardSecurityCode";
    private static final String _User = "vpc_User";
    private static final String _Password = "vpc_Password";
    private static final String _Locale = "vpc_Locale";
    private static final String _ReturnURL = "vpc_ReturnURL";
    private static final String _SecureHash = "vpc_SecureHash";
    
    private static final String _TxnResponseCode = "vpc_TxnResponseCode";
    private static final String _TransactionNo = "vpc_TransactionNo";  
    private static final String _Message = "vpc_Message";  
    private static final String _ReceiptNo = "vpc_ReceiptNo";  
    private static final String _CardType = "vpc_CardType";      
    private static final String _MerchTxnRef = "vpc_MerchTxnRef";     	
	
    protected HashMap sendparameters = new HashMap();
    
    public MIGS(PaymentBean bean)
    {
        super(bean);
    }
    
    public String getRunwayPaymentStatusID()
    {
    	int code = -1;
    	try{
    		code = Integer.parseInt(bean.getResponseCode());
    	}catch(Exception e){}
    	if(code >= 0)
    	{
    		return(RunwayPaymentStatusIDs[code]);
    	}
    	return(null);
    }
    
    public void sendPayment()
    {
        setupParameters();
        HTTPClient http = new HTTPClient(true); 
        String params = getParameters();
        
        log("SENT:");
        log(params);
        writeLog();

        http.doPost(bean.getServerURL(), params);
        
        String response = http.getLog();
        log("RECEIVED:");
        log(response);
        writeLog();
        
        setResponse(response);
    }
    
    public String getPaymentURL()
    {
        setupParameters();
        StringBuffer params = new StringBuffer();
        params.append(bean.getServerURL());
        params.append("?");
        appendQueryFields(params,sendparameters);
        String paramstr = params.toString();
        log("SENT:");
        log(paramstr);
        writeLog();        
        return(paramstr);
    }
    
    public void setPaymentResponse(Map response)
    {        
        Iterator iter = response.keySet().iterator();
        
        String name = null;
        Object valueobj = null;
        String value = null;
        while (iter.hasNext())
        {
            name = (String)iter.next();
            valueobj = response.get(name);
            if(valueobj!=null)
            {
            	if(valueobj instanceof String[])
            	{
            		value = ((String[])valueobj)[0];
            	}
            	else
            	{
            		value = valueobj.toString();
            	}
	            if(value.length()!=0)
	            {
	                setResponse(name, value);
	            }
            }
        }
        
        log("RECEIVED:");
        StringBuffer temp = new StringBuffer();
        appendQueryFields(temp, response);
        log(temp.toString());
        writeLog();        
    }
    
  
    
    protected void setupParameters()
    {
        int type = bean.getType();
        
        sendparameters.put(_Version,"1");
        
        if(type == PaymentBean.PAYMENT)
        {
            sendparameters.put(_Command,"pay");
        }
        else if(type == PaymentBean.REFUND)
        {
            sendparameters.put(_Command,"Refund");
        }
        sendparameters.put(_MerchTxnRef,bean.getPaymentID());
        sendparameters.put(_AccessCode,bean.getMerchantAccessCode());

        if(bean.isTest())
        {
            sendparameters.put(_Merchant,"TEST" + bean.getMerchantID());
        }
        else
        {
            sendparameters.put(_Merchant,bean.getMerchantID());
        }

        if(type == PaymentBean.PAYMENT)
        {     
            sendparameters.put(_OrderInfo,bean.getOrderID());
        }
        else if(type == PaymentBean.REFUND)
        {
            sendparameters.put(_TransactionNo,bean.getTransactionID());
        }
        sendparameters.put(_Amount,bean.getIntegerAmount());

        if(type == PaymentBean.PAYMENT && bean.getCardNumber()!=null)
        {            
        	if(bean.getMode() == PaymentBean.MERCHANT_HOSTED)
        	{
	            sendparameters.put(_CardNum,bean.getCardNumber());
	            sendparameters.put(_CardExp,bean.getIntegerCardExpiry());
	
	            if(bean.getCardSecurtyCode()!=null)
	            {
	                sendparameters.put(_CardSecurityCode,bean.getCardSecurtyCode());
	            }
        	}
        }
        else if(type == PaymentBean.REFUND)
        {
            sendparameters.put(_User,bean.getUser());
            sendparameters.put(_Password,bean.getPassword());                
        }
        if(bean.getMode() == PaymentBean.SERVER_HOSTED)
        {
        	sendparameters.put(_Locale, "en_AU");
        	
	        if(bean.getReturnURL()!=null)
	        {
	        	sendparameters.put(_ReturnURL,bean.getReturnURL());
	        }
	        if(bean.getSecureSecret()!=null)
	        {
	            sendparameters.put(_SecureHash, hashAllFields(sendparameters, bean.getSecureSecret()));
	        }
        }
    }
    
    protected String getParameters()
    {
        StringBuffer params = new StringBuffer();
        appendQueryFields(params,sendparameters);
        return(params.toString());
    }
    
    
    protected void setResponse(String response)
    {
        StringTokenizer pst = new StringTokenizer(response, "&");
        String name = null;
        String value = null;
        while(pst.hasMoreTokens())
        {
            StringTokenizer nvst = new StringTokenizer(pst.nextToken(), "=");
            if(nvst.hasMoreTokens())
            {
                name = nvst.nextToken();
                if(name != null && name.length() != 0)
                {
                    if(nvst.hasMoreTokens())
                    {
                        value = nvst.nextToken();
                        if(value != null && value.length() != 0)
                        {
                            setResponse(StringUtil.urlDecode(name), StringUtil.urlDecode(value));
                        }
                    }
                }
            }
        }
    }
    
    protected void setResponse(String name, String value)
    {
        if(name.equals(_TxnResponseCode))
        {
            bean.setResponseCode(value);
        }
        else if(name.equals(_TransactionNo))
        {
            bean.setTransactionID(value);
        }
        else if(name.equals(_Message))
        {
            bean.setResponseMessage(value);
        }
        else if(name.equals(_ReceiptNo))
        {
            bean.setReceiptNo(value);
        }
        else if(name.equals(_CardType))
        {
            bean.setCardType(value);
        }          
        else if(name.equals(_MerchTxnRef))
        {
            bean.setPaymentID(value);
        }        
    }
    
    static final char[] HEX_TABLE = new char[] {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};    
    
    protected String hashAllFields(Map fields, String securesecret)
    {
        // create a list and sort it
        List fieldNames = new ArrayList(fields.keySet());
        Collections.sort(fieldNames);
        
        // create a buffer for the md5 input and add the secure secret first
        StringBuffer buf = new StringBuffer();
        buf.append(securesecret);
        
        // iterate through the list and add the remaining field values
        Iterator itr = fieldNames.iterator();
        
        while (itr.hasNext()) {
            String fieldName = (String) itr.next();
            String fieldValue = (String) fields.get(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                buf.append(fieldValue);
            }
        }
             
        MessageDigest md5 = null;
        byte[] ba = null;
        
        // create the md5 hash and UTF-8 encode it
        try {
            md5 = MessageDigest.getInstance("MD5");
            ba = md5.digest(buf.toString().getBytes("UTF-8"));
         } catch (Exception e) {} // wont happen
       
       return hex(ba);
    }
    
    protected static String hex(byte[] input) 
    {
        // create a StringBuffer 2x the size of the hash array
        StringBuffer sb = new StringBuffer(input.length * 2);

        // retrieve the byte array data, convert it to hex
        // and add it to the StringBuffer
        for (int i = 0; i < input.length; i++) {
            sb.append(HEX_TABLE[(input[i] >> 4) & 0xf]);
            sb.append(HEX_TABLE[input[i] & 0xf]);
        }
        return sb.toString();
    } 
    
    protected void appendQueryFields(StringBuffer buf, Map fields)
    {
        // create a list
        List fieldNames = new ArrayList(fields.keySet());
        Iterator itr = fieldNames.iterator();
        
        // move through the list and create a series of URL key/value pairs
        while (itr.hasNext())
        {
            String fieldName = itr.next().toString();
            Object fieldValue = fields.get(fieldName);
            String valueString;
            
            if ((fieldValue != null))
            {
            	if(fieldValue instanceof String[])
            	{
            		valueString = ((String[])fieldValue)[0];
            	}
            	else
            	{
            		valueString = fieldValue.toString();
            	}
            	if(valueString.length()!=0)
            	{
	                // append the URL parameters
	                buf.append(StringUtil.urlEncode(fieldName));
	                buf.append('=');
	                buf.append(StringUtil.urlEncode(valueString));
            	}
            }

            // add a '&' to the end if we have more fields coming.
            if (itr.hasNext())
            {
                buf.append('&');
            }
        }
    }    
}