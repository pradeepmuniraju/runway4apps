package com.sok.runway.payment;

import java.util.Map;

public interface PaymentProvider
{
	public static final String[] RunwayPaymentStatusIDs = {
		"0T1K116P3K066F3I0T2G1L321W7D", //Tranaction approved
		"0C1N1C6S3L006H3G0Q201W311Y7N", //Tranaction could not be processed
		"0E1B1A61300M683E0C2K1A3C1K7P", //Tranaction declined (contact issuer)
		"08161R6T3H0E6T3J022O1E371I6C", //No reply from Processing Host
		"0M1U1H6J3007683A0V2X1N311M6V", //Card has expired
		"0Z1C1A6M350F65330V2218391M6Q", //Insufficient credit
		"0M1M126Y3D0I653Z0N2W1D3Z1K6Q", //Communication Error (communicating with bank)
		"0J1Z1L6L3N0U6Y3N0X2I1X34187V", //Message Detail Error
		"0S1M106J3B0O6V380X2F1W30177I", //Tranaction type not supported
		"021813663Q0M6P3L932T2A6A1Y80" //Bank declined Tranaction
	};
	
	public String getRunwayPaymentStatusID();
	
    public void sendPayment();
    
    public String getPaymentURL();
    
    public void setPaymentResponse(Map response);    
    
    public String getLog();
}