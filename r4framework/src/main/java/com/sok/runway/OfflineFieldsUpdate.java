package com.sok.runway;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.sok.framework.ActionBean;
import com.sok.framework.InitServlet;
import com.sok.framework.RowBean;
import com.sok.framework.RowSetWrapperBean;
import com.sok.framework.StringUtil;
import com.sok.framework.TableData;
import com.sok.framework.TableSpec;
public class OfflineFieldsUpdate extends OfflineProcess
{
	public static final String DEBUG = "-debug";
	public static final String CHECKCOUNT = "-checkcount";
	public static final String BEANNAME = "-beanname";
	public static final String VIEW = "-view";
	StringBuffer status = new StringBuffer();
	StringBuffer errors = new StringBuffer();

	UserBean user = null;
	TableData search = null;
	String campaign = null;
	String template = null;
	boolean debug = false;
	ArrayList foundset = new ArrayList(500);

	Connection con = null;
	Context ctx = null;

	String statementstring = null;
	int total = 0;
	int updated = 0;
	int failed = 0;

	int checkcount = -1;


	String tablename = null;
	String keyname = null;
	String viewname = null;

	TableSpec tspec;

	static final String _blank = "";
	static final String _dot = ".";
	static final String _comma = ", ";
	static final String _and = " and ";
	static final String _or = " or ";
	static final String _as = " as ";
	static final String _omit = "omit";

	static final String NOW = "NOW";
	static final String NULL= "NULL";
	static final String EMPTY = "EMPTY";
	
	public static String _trimsuffix = "_trim";
	public static String _replacesuffix = "_replace";
	public static String _titlecasesuffix = "_titlecase";
	public static String _replacesuffixoldtext = "_oldText";
	public static String _replacesuffixnewtext = "_newText";
	public static String _appendtofrontsuffixtext = "_frontAppend";
	public static String _appendtoendsuffixtext = "_endAppend";
	
	protected static final HashMap<String, String> TABLE_VIEWS = new HashMap<String, String>();
	
	static {
	   TABLE_VIEWS.put("Contacts", "ContactView");
	   TABLE_VIEWS.put("Companies", "CompanyView");
	   TABLE_VIEWS.put("CompanyAnswers", "{answers/}CompanyAnswerView");
	   TABLE_VIEWS.put("ContactAnswers", "{answers/}ContactAnswerView");
	   TABLE_VIEWS.put("ProductAnswers", "{answers/}ProductAnswerView");
	   TABLE_VIEWS.put("OrderAnswers", "{answers/}OrderAnswerView");
	   TABLE_VIEWS.put("OpportunityAnswers", "{answers/}OpportunityAnswerView");
	   TABLE_VIEWS.put("NoteAnswers", "{answers/}NoteAnswerView");
	}

    public OfflineFieldsUpdate(HttpServletRequest request, ServletContext context, boolean autostart)
    {
        super(request, context);
        if(autostart)
        {
            initProcess(request, context);
            start();
        }
    }    
    
	public OfflineFieldsUpdate(HttpServletRequest request, ServletContext context)
	{		
		super(request, context);		
		initProcess(request, context);		
		start();
	}

	public OfflineFieldsUpdate(String query, HttpServletRequest request, ServletContext context)
	{
		super(query, request, context);
		initProcess(request, context);
		start();
	}

	public OfflineFieldsUpdate(RowBean bean, HttpServletRequest request, ServletContext context)
	{
		super(bean, request, context);
		initProcess(request, context);
		start();
	}	

	public void initProcess(HttpServletRequest request, ServletContext context)
	{		
		HttpSession session = request.getSession();
		user = (UserBean)session.getAttribute(SessionKeys.CURRENT_USER);
		String searchcriteria = (String)requestbean.get(BEANNAME);			
		search = (TableData)session.getAttribute(searchcriteria);	
						
		tspec = search.getTableSpec();
		tablename = getSourceTableName();
		keyname = tspec.getPrimaryKeyName();		
		
		search.setViewSpec(getViewSpec());
	}
   
   protected void processExtraFields(TableData current) throws SQLException {
      //This method exists for subclasses to do extra processing
   }
   
	void retrieveRecords()throws Exception
	{	      
		if(debug)
		{		  
			errors.append(search.getSearchStatement());
		}			
		try
		{
		   search.setConnection(con);
		   RowSetWrapperBean list = new RowSetWrapperBean();		
		   list.setConnection(con);
		   list.setSearchBean(search);
		   list.getResults();
		   
		   TableData resultset = list.getResultBean();		  		 		  
		   foundset = new ArrayList(500);
		   while (list.getNext()) {
				foundset.add(resultset.getString(keyname));			
				processExtraFields(resultset);
			}		   			  
			total = foundset.size();		      			
		}		
		finally
		{    		
			try{
            	con.close();
        	}catch (Exception ex){}    		  		    
		}
	}	

	protected boolean needsTrimming(String field)
	{		
		String value = requestbean.getString(field+_trimsuffix);
				
		if(value!=null && value.equals("true"))
		{
			return(true);
		}
		return(false);
	}
	
	protected boolean needsReplace(String field)
	{
		String oldText = requestbean.getString(field+_replacesuffixoldtext);		
	    String newText = requestbean.getString(field+_replacesuffixnewtext);	    	
		
		if(oldText!=null && newText != null && newText.length() !=0)
		{
			return(true);
		}
		return(false);
	}	
	protected boolean needsAppending(String field)
	{
		String endAppendText = requestbean.getString(field+_appendtoendsuffixtext);
		String frontAppendText = requestbean.getString(field+_appendtofrontsuffixtext);
		
		if((frontAppendText != null && frontAppendText.length() !=0) || (endAppendText != null && endAppendText.length() !=0))
		{
			return(true);
		}
		return(false);
	}	
	
	protected boolean needsCaseChange(String field)
	{
		String value = requestbean.getString(field+_titlecasesuffix);			
		
		if(value!=null && value.length()!=0)
		{
			return(true);
		}
		return(false);
	}	
	
	protected void setUpdateColumns(RowBean selectbean, RowBean updatebean)
	{
		String fieldname;
		String fieldvalue;									
		try{		
			for(int i=0; i<tspec.getFieldsLength(); i++)
			{					
				fieldname = tspec.getFieldName(i);												
				fieldvalue = selectbean.getString(fieldname);							
				if(needsTrimming(fieldname))
				{					
				  // TODO: update using fieldvalue 
					fieldvalue = updatebean.getString(fieldname);
					if (fieldvalue == null || fieldvalue.length() == 0){					
						fieldvalue = selectbean.getString(fieldname);
					}
					updatebean.setColumn(fieldname, StringUtil.trim(fieldvalue));			
				}
				
				if(needsReplace(fieldname))
				{				
					fieldvalue = updatebean.getString(fieldname);				
					if (fieldvalue == null || fieldvalue.length() == 0){					
						fieldvalue = selectbean.getString(fieldname);
					}
					
					String oldText = requestbean.getString(fieldname+_replacesuffixoldtext);					
					String newText = requestbean.getString(fieldname+_replacesuffixnewtext);					
																
					if (oldText != null && oldText.length() ==0  && fieldvalue.length() == 0){					
						updatebean.setColumn(fieldname, newText);							
					}else if (oldText != null && oldText.length()>0 && newText == null){					
						updatebean.setColumn(fieldname,StringUtil.replace(fieldvalue, oldText, ""));					
					}else if (oldText != null && oldText.length()>0 && newText != null && newText.length()>0){							
							updatebean.setColumn(fieldname, StringUtil.replace(fieldvalue, oldText, newText));							
					}
				}
				
				if(needsAppending(fieldname))
				{				
					fieldvalue = updatebean.getString(fieldname);
					if (fieldvalue == null || fieldvalue.length() == 0){					
						fieldvalue = selectbean.getString(fieldname);
					}
					
	                StringBuffer appendBuffer = new StringBuffer();
					
					String endAppendText = requestbean.getString(fieldname+_appendtoendsuffixtext);
					String frontAppendText = requestbean.getString(fieldname+_appendtofrontsuffixtext);
						
					if (frontAppendText != null && frontAppendText.length() >0){	
						appendBuffer.append(frontAppendText).append(fieldvalue);
						fieldvalue = appendBuffer.toString();
						updatebean.setColumn(fieldname, fieldvalue);
					}	

					if (endAppendText != null && endAppendText.length() >0 ){
						updatebean.setColumn(fieldname, fieldvalue + endAppendText);					
					}	
											
				}
													
				if(needsCaseChange(fieldname))
				{
					fieldvalue = updatebean.getString(fieldname);
					
					if (fieldvalue == null || fieldvalue.length() == 0){					
						fieldvalue = selectbean.getString(fieldname);
					}						
					String selection = requestbean.getString(fieldname+_titlecasesuffix);
					
					String caseAction = StringUtil.toTitleCase(fieldvalue);
					if (selection.equalsIgnoreCase("uppercase")){
						caseAction = fieldvalue.toUpperCase();
					}else if (selection.equalsIgnoreCase("lowercase")){
						caseAction = fieldvalue.toLowerCase();
					}				
					updatebean.setColumn(fieldname, caseAction);							
				}	
						
			}
		}catch(Exception e){
			errors.append(e.toString());
		 }
	}

	protected int updateRow(int index, RowBean selectbean, RowBean updatebean)
	{				
		int updated = 0;
		String id = (String)foundset.get(index);
		try
		{
			selectbean.clear();
			selectbean.setAction("select");		
			selectbean.setColumn(tspec.getPrimaryKeyName(),id);
			selectbean.doAction();
					
			updatebean.clear();
			updatebean.setAction("update");	
			updatebean.setColumn(tspec.getPrimaryKeyName(),id);
			
			setUpdateColumns(selectbean, updatebean);							
			updatebean.setColumn("ModifiedDate","NOW");				
			updatebean.doAction();						
				
			if(debug)
			{
				updated = 1;
			}
			else
			{
				updated = updatebean.getUpdatedCount();
			}
		}
		catch (Exception ex)
		{
		   setLastError(ex.toString());
				errors.append(ex.toString());
				errors.append(" - ");
				errors.append((String)context.getInitParameter("URLHome"));
				errors.append("/crm/");
				errors.append(viewname);
				errors.append(".jsp?-action=select&");
				errors.append(keyname);
				errors.append("=");
				errors.append(id);
				errors.append("\r\n");
		}
		return(updated);
					
	} 

	protected java.util.Date getDate(int specindex , String value)
	{
		if(value!=null && !value.equals(NOW))
		{
			SimpleDateFormat sdf = new SimpleDateFormat(tspec.getFormat(specindex), getLocale());
			try{
				return(sdf.parse(value));
			}catch(Exception e){}			
		}
		return(new java.util.Date());
	}
	
	void getConnection() throws Exception
	{		
		if(con==null || con.isClosed())
		{
			ctx = new InitialContext();

			DataSource ds = null;
				try{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource)envContext.lookup(dbconn);
				}catch(Exception e){}
			if(ds == null)
			{
				ds = (DataSource)ctx.lookup(dbconn);
			}
			con = ds.getConnection();
		}
	}
	
	protected String getViewSpec()
	{
	   String viewSpec = null;
	   
	   String tableName = tspec.getTableName();
	   if(TABLE_VIEWS.containsKey(tableName)) {
	      viewSpec = TABLE_VIEWS.get(tableName);
	   }
	   
		return(viewSpec);
	}
	
	void updateRecords()
	{		
		int success = 0;
		String id = null;
		
		RowBean selectbean = new RowBean();
		RowBean updatebean = new RowBean();	
		try {
			getConnection();
			selectbean.setConnection(con);
			updatebean.setConnection(con);				
			
			selectbean.setViewSpec(getViewSpec());
		    updatebean.setViewSpec(getViewSpec());					
				
			
			for(int i=0; i<total && !isKilled(); i++)
		    {				
				try
				{
					id = (String)foundset.get(i);						
					success = updateRow(i, selectbean, updatebean);							
				}
				catch(Exception e)
				{
					errors.append(e.toString());
					errors.append(" - ");
					errors.append((String)context.getInitParameter("URLHome"));
					errors.append("/crm/");
					errors.append(viewname);
					errors.append(".jsp?-action=select&");
					errors.append(keyname);
					errors.append("=");
					errors.append(id);
					errors.append("\r\n");
				}
				if(success==1)
				{				
					updated++;
				}
				else
				{
					errors.append("\r\n***Update Failed***\r\n");
					failed++;
				}
			}
		}
		catch (Exception ex) {
				errors.append(ex.toString());			
		}
	}

	public void execute()
	{		
		String debugstring = requestbean.getString(DEBUG);
		String checkcountstring = requestbean.getString(CHECKCOUNT);

		viewname = requestbean.getString(VIEW);

		if(debugstring.length()!=0)
		{
			debug = true;
		}
		if(checkcountstring.length()!=0)
		{
			checkcount = Integer.parseInt(checkcountstring);
		}
		con = null;
		ctx = null;
		try
		{						
			setEmailHeader();			
			if(user == null || search == null)
			{			
				status.append("Resources not found, offline process halted.\r\n");
				return;
			}
			
			requestbean.put("ModifiedDate","NOW");
			requestbean.put("ModifiedBy",user.getCurrentUserID());				
			getConnection();				
			retrieveRecords();					
			if(checkcount!=-1)
			{
				if(total!=checkcount)
				{
					status.append("Search results count does not match, offline process halted.\r\n");
					status.append("Current - ");
					status.append(String.valueOf(total));
					status.append(", Original - ");
					status.append(String.valueOf(checkcount));
					status.append("\r\n");
					return;	
				}
			}			
			updateRecords();			
			status.append(String.valueOf(total));
			status.append(" ");
			status.append(tablename);
			status.append(" found.\r\n");
			status.append(String.valueOf(updated));
			status.append(" ");
			status.append(getTargetTableName());
			status.append(" updated.\r\n");
			status.append(String.valueOf(failed));
			status.append(" ");
			status.append(getTargetTableName());
			status.append(" failed.\r\n");
			if (isKilled()) {
		      status.append("Offline process terminated by - ");
		      status.append(getKiller());
		      status.append("\r\n\r\n");
		   }
		   else {	   
		      status.append("Offline process completed.\r\n\r\n");
		   }

		}
		catch(Exception e)
		{			
			status.append("Offline process halted on ");
			status.append(String.valueOf(updated+failed));
			status.append(" of ");
			status.append(String.valueOf(total));
			status.append(".\r\n\r\n");
			status.append(e.toString());
		}
		finally
		{			
    		if (con != null) 
    		{
				try{
            	con.close();
					con = null;
        		}catch (Exception ex){}
    		}
    		if (ctx != null) 
    		{
        		try{       
            	ctx.close();
					ctx = null;
        		}catch (Exception ex){}
    		}

			if(errors.length()!=0)
			{
				status.append("\r\n");
				status.append(errors.toString());
			}
		}
	}
	void setEmailHeader()
	{
		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, getLocale());
		status.append(contextname);
		status.append(" Runway offline process - Update ");
		status.append(getTargetTableName());
		status.append("\r\n");
		status.append("Initiated ");
		status.append(dt.format(new java.util.Date()));
		status.append("\r\n");
	}

	public String getStatusMailBody()
	{
		return(status.toString());
	}

	public String getStatusMailSubject()
	{
		return(contextname + " Runway offline process - Update "+tablename);
	}

	public String getStatusMailRecipient()
	{
		return(user.getEmail());
	}
	
	protected String getTargetTableName() {
	   return tspec.getTableName();
	}
	
	protected String getSourceTableName() {
	   return tspec.getTableName();
	}
	
   public int getProcessSize() {
      return foundset.size();
   }
   
   public int getProcessedCount() {
      return updated+failed;
   }
   
   public int getErrorCount() {
      return failed;
   }
   public String getError(){
	   return errors.toString();
   }
}
