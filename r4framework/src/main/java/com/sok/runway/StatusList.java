package com.sok.runway;
import javax.sql.*;
import java.sql.*;
import java.util.*;
import javax.naming.*;
import javax.servlet.jsp.*; 
import javax.servlet.http.*;

import org.apache.commons.lang.StringUtils;

import com.sok.framework.*;
public class StatusList
{
		String[] statusID = null;
		String[] status = null;
		String[] statusValue = null;
		String[] mainPage = null;
      String[] groupID = null;	
      boolean[] inactive = null;
		int[] useCount = null;
		int listsize = 0;
		int lastselected = -1;

	class ListValues
	{
		String[] statusID = null;
		String[] status = null;
		String[] statusValue = null;
		String[] mainPage = null;
      String[] groupID = null;
      boolean[] inactive = null;          
		int[] useCount = null;
		int listsize = 0;
		int lastselected = -1;
		boolean editmode = false;
	}

	HashMap lists = null;

	Connection con = null;
	String datasource	= null;
	boolean closeconnection = true;
   boolean debug = false;
   boolean editmode = false;
   
	StringBuffer errors = new StringBuffer();

	static final String RN = "\r\n";

	static final int maxlistsize = 100;
	
	String statusFilter = null;
	String statusTable = null;
   UserBean currentuser = null;
   
   public void setDebug(boolean debug)
   {
      this.debug = debug;
   } 
   
   public synchronized void setListFilter(String statusFilter, String statusTable) 
   {
      this.statusFilter = statusFilter;
      this.statusTable = statusTable;
      this.currentuser = null;
      this.editmode = false;
		checkList();
   }
   
   public synchronized void setListFilter(String statusFilter, String statusTable, boolean editmode) 
   {
      this.statusFilter = statusFilter;
      this.statusTable = statusTable;
      this.currentuser = null;
      this.editmode = editmode; //shows inactive status items
      checkList();
   }   
   
   public synchronized void setListFilter(String statusFilter, String statusTable, UserBean currentuser) 
   {
      this.statusFilter = statusFilter;
      this.statusTable = statusTable;
      this.currentuser = currentuser;
      checkList();
   }   
   
	synchronized void checkList()
	{
		if(lists!=null)
		{
			ListValues current = (ListValues)lists.get(statusFilter);
			if(current!=null && this.editmode == current.editmode)
			{
				this.statusID = current.statusID;
				this.status = current.status;
				this.statusValue = current.statusValue;
				this.mainPage = current.mainPage;
				this.useCount = current.useCount;
				this.listsize = current.listsize;
				this.lastselected = current.lastselected;
	         this.inactive = current.inactive;
	         this.groupID = current.groupID;
			}
			else
			{
				retrieveList();
			}
		}
		else
		{
			retrieveList();
		}
	}
	
	synchronized void setCurrent()
	{
		if(lists==null)
		{
			lists = new HashMap();
		}
		ListValues current = (ListValues)lists.get(statusFilter);
		if(current == null)
		{
		 	current = new ListValues();
		}		
		current.statusID = this.statusID;
		current.status = this.status;
		current.statusValue = this.statusValue;
		current.mainPage = this.mainPage;
		current.useCount = this.useCount;
		current.lastselected = this.lastselected;
		current.listsize = this.listsize;
		current.editmode = this.editmode;
	   current.inactive = this.inactive;
	   current.groupID = this.groupID;
		lists.put(statusFilter,current);
	}

	public synchronized int getLength()
	{
		return(listsize);
	}

	public synchronized String getStatusID(int index)
	{
		return(statusID[index]);
	}
	
	public synchronized String getStatusID(String inputStatus)
	{
		if(StringUtils.isBlank(inputStatus)) return null;
		
		for (int i = 0 ; i < listsize ; i++) {
			if(inputStatus.equals(status [i]))
				return getStatusID(i);
		}
		
		return null;
	}

	public synchronized String getStatus(int index)
	{
		return(status[index]);
	}

	public synchronized String getStatusValue(int index)
	{
		return(statusValue[index]);
	}

	public synchronized String getMainPage(int index)
	{
		return(mainPage[index]);
	}

   public synchronized String getGroupID(int index)
   {
      return(groupID[index]);
   }
   
   public synchronized boolean getInactive(int index)
   {
      return(inactive[index]);
   }     
	
	public synchronized int getUseCount(int index)
	{
		String statusid = statusID[index];					
	try{	
		GenRow count = new GenRow();
		checkConnection();
		count.setConnection(con);
		count.setTableSpec(statusTable);
		
		if("Contact".equals(statusFilter)) {
			String statement = "Select count(*) AS CountNumber from GroupStatus where StatusID = '" +
					statusid+ "' and GroupStatusID in  (Select ContactGroupStatusID from ContactGroups)";
			
			count.setStatement(statement);
			count.getResults(true);
		} else {
			count.setParameter("-select","count(*) AS CountNumber");
			count.setParameter("StatusID",statusid);
			count.getResults();
		}
		
		if (count.getNext()) {
			return count.getInt("CountNumber"); 
		}
		else {
			return -1;
		}
	}	
	 catch (Exception ex){ 
		 errors.append("Error in getting status count ");
		 return -1;
	 }
		//return(useCount[index]);
	}

   synchronized void saveList()
   {
      GenRow row = null;
      try
      {
         checkConnection();
         row = new GenRow();
         row.setTableSpec("StatusList");
         int r = -1;

         for(int i=0; i<listsize; i++)
         {

            row.setConnection(con);
            row.setParameter("StatusID", statusID[i]);
            row.setParameter("Status", status[i]);
            row.setParameter("StatusValue", statusValue[i]);
            row.setParameter("MainPage", mainPage[i]);
            row.setParameter("GroupID", groupID[i]);
            row.setParameter("SortNumber", String.valueOf(i));
            row.setParameter("Inactive", inactive[i]?"Y":"N");
            row.setParameter("ListName", statusFilter);
            row.doAction("insert");
         }
         
         setCurrent();
      }
      catch (Exception ex)
      {
         errors.append("At saveList(): ");
         errors.append(ex.getMessage());
         errors.append(RN);
      }
      finally
      {  
         if (row != null) 
         {
            try{       
               row.close();
            }catch (Exception ex){}
         }
         if(closeconnection && con!=null)
         {
            try{
               con.close();
            }catch (Exception ex){}
         }
      }
   }	

   synchronized void updateList()
   {
      GenRow row = null;
      try
      {
         checkConnection();
         row = new GenRow();
         row.setTableSpec("StatusList");
         int r = -1;

         for(int i=0; i<listsize; i++)
         {

            row.setConnection(con);
            row.setParameter("StatusID", statusID[i]);
            row.setParameter("Status", status[i]);
            row.setParameter("StatusValue", statusValue[i]);
            row.setParameter("MainPage", mainPage[i]);
            row.setParameter("GroupID", groupID[i]);
            row.setParameter("SortNumber", String.valueOf(i));
            row.setParameter("Inactive", inactive[i]?"Y":"N");
            row.setParameter("ListName", statusFilter);
            try {
            	row.doAction("update");
            	if (!row.isSuccessful()) {
                	row.doAction("insert");
            	}
            } catch (Exception e) {
            	// can't update, try insert instead
            	row.doAction("insert");
            }
         }
         
         setCurrent();
      }
      catch (Exception ex)
      {
         errors.append("At updateList(): ");
         errors.append(ex.getMessage());
         errors.append(RN);
         
         
      }
      finally
      {  
         if (row != null) 
         {
            try{       
               row.close();
            }catch (Exception ex){}
         }
         if(closeconnection && con!=null)
         {
            try{
               con.close();
            }catch (Exception ex){}
         }
      }
   }	

	public synchronized void deleteList()
	{
		PreparedStatement deleteprestmt = null;
		try
		{
			checkConnection();
			deleteprestmt = con.prepareStatement("delete from StatusList where ListName = '" + statusFilter + "'");
			int r = deleteprestmt.executeUpdate();
		}
		catch (Exception ex)
		{
			errors.append("At deleteList(String): ");
			errors.append(ex.getMessage());
			errors.append(RN);
		}
		finally
		{	
			if (deleteprestmt != null) 
			{
		   	try{       
		     		deleteprestmt.close();
		   	}catch (Exception ex){}
			}
		}
	}

	synchronized void checkConnection() throws SQLException
	{
		if(con==null || con.isClosed())
		{
			con = ActionBean.connect();
			closeconnection = true;
		}
	}

	public synchronized void setJndiName(String t)
	{
		datasource = t;
	}

	public synchronized void setCloseConnection(boolean b)
	{
		closeconnection = b;
	}
   
	public synchronized void setStatusList(String t, String filter, String statusTable)
	{
		datasource = t;
		setListFilter(filter, statusTable);
	}

	public synchronized void setConnection(Object con)
	{
		if (con != null) {
			this.con = (Connection)con;
			closeconnection = false;
		}
	}

	public synchronized void setConnection(Connection con)
	{
		if (con != null) {
			this.con = con;
			closeconnection = false;
		}
	}

	public synchronized Connection getConnection()
	{
		return(con);
	}

	public synchronized void retrieveList()
	{
		GenRow row = null;
		try
		{
		   errors = new StringBuffer();
		   
			checkConnection();

         StringBuffer qry = new StringBuffer();
         
         qry.append("(select count(*) from ");
         qry.append(statusTable);
         qry.append(" where ");
         qry.append(" StatusID = StatusList.StatusID) AS Num ");

         row = new GenRow();
         row.setConnection(con);
         row.setTableSpec("StatusList");
         row.setParameter("-select1","StatusID");
         row.setParameter("-select2","Status");
         row.setParameter("-select3","StatusValue");
         row.setParameter("-select4","MainPage");
         row.setParameter("-select5","GroupID");
         row.setParameter("-select6","Inactive");
         row.setParameter("-select7",qry.toString());
         row.setParameter("ListName",statusFilter);
         row.setParameter("-sort1","SortNumber");
         row.setParameter("-order1","ASC");
         
         if(!editmode)
         {
            row.setParameter("Inactive","NULL+N");  
         }
         if(currentuser!=null)
         {
            currentuser.setConstraint(row,"StatusGroup",true,null,false,true);
         }
         
         row.doAction("search");
         
         if(debug)
         {
            errors.append("\r\n");
            errors.append(row.getStatement());
            errors.append("\r\n");
         }
         
         row.getResults(true);

			listsize = row.getSize();
			
			//System.out.println("List Size is " + listsize);

			statusID = new String[maxlistsize]; //can't use listsize, needs room to grow
			status = new String[maxlistsize];
			statusValue = new String[maxlistsize];
			mainPage = new String[maxlistsize];
			groupID = new String[maxlistsize];
			useCount = new int[maxlistsize];
			inactive = new boolean[maxlistsize] ;    
			int i=0;
			while(row.getNext())
			{
				statusID[i] = row.getString("StatusID");
				status[i] = row.getString("Status");
				statusValue[i] = row.getString("StatusValue");
				mainPage[i] = row.getString("MainPage");
				groupID[i] = row.getString("GroupID");
				useCount[i] = row.getInt("Num");
				inactive[i] = "Y".equals(row.getString("Inactive"));  
				i++;
			}
			setCurrent();
		}
		catch(Exception e)
		{
			errors.append("At retrieveList(): ");
			errors.append(e.toString());
		}
		finally
		{
	    	if (row != null) 
	    	{
	       	try{
	       	  row.close();
	       	}catch (Exception ex){}
	    	}
			if(closeconnection && con!=null)
			{
	       	try{
	           	con.close();
	       	}catch (Exception ex){}
			}
		}
	}

	public synchronized int getLastSelected()
	{
		return(lastselected);
	}

	public synchronized void setList(HttpServletRequest request)
	{
		setArray(statusID, request.getParameterValues("StatusID"));
		setArray(status, request.getParameterValues("Status"));
		setArray(statusValue, request.getParameterValues("StatusValue"));
		setArray(mainPage, request.getParameterValues("MainPage"));
      setArray(groupID, request.getParameterValues("GroupID"));
      setArray(inactive, request.getParameterValues("Inactive"));
	}

   synchronized void setArray(boolean[] source, String[] target)
   {
	   if(target != null) { 
	      int targetlength = target.length;
	      for(int i=0; i<source.length; i++)
	      {
	         if(i<targetlength)
	         {
	            source[i] = !"N".equals(target[i]);
	         }
	         else
	         {
	            source[i] = true;
	         }
	      }
	   } 
   }	
	
	synchronized void setArray(String[] source, String[] target)
	{
		if(target != null) { 
			int targetlength = target.length;
			for(int i=0; i<source.length; i++)
			{
				if(i<targetlength)
				{
					source[i] = target[i];
				}
				else
				{
					source[i] = null;
				}
			}
		} 
	}

	public synchronized void processRequest(HttpServletRequest request)
	{
		String action = request.getParameter("-action");

		if(action.equals("insert"))
		{
			checkList();//setList(request);
			add(request);
			setCurrent();
		}
		else if(action.equals("moveup"))
		{
			setList(request);
			moveUp(request);
			setCurrent();
		}
		else if(action.equals("movedown"))
		{
			setList(request);
			moveDown(request);
			setCurrent();
		}
		else if(action.equals("delete"))
		{
			setList(request);
			delete(request);
			setCurrent();
		}
		else if(action.equals("cancel"))
		{
			checkList();
		}
		else if(action.equals("update"))
		{
			setList(request);
			setCurrent();
			// this was changed as it removes any for contact and for company the statuses in the groups
			//deleteList();
			//saveList();
			updateList();
		}
      else if(action.equals("noaction"))
      {
         setList(request);
         setCurrent();
      }		
	}

	synchronized void add(HttpServletRequest request)
	{
		lastselected = listsize;
		status[listsize] = request.getParameter("NewStatus");
		statusValue[listsize] = request.getParameter("NewStatusValue");
		statusID[listsize] = KeyMaker.generate();
		mainPage[listsize] = request.getParameter("NewMainPage");
	   groupID[listsize] = request.getParameter("NewGroupID");
	   inactive[listsize] = "Y".equals(request.getParameter("NewActive"));
		listsize++;
	}

	synchronized void delete(HttpServletRequest request)
	{
		int deleteindex = Integer.parseInt(request.getParameter("SelectedIndex"));
		lastselected = -1;
		for(int i=deleteindex; i<listsize; i++)
		{
			status[i] = status[i+1];
			statusValue[i] = statusValue[i+1];
			statusID[i] = statusID[i+1];
			mainPage[i] = mainPage[i+1];
         groupID[i] = groupID[i+1];
         inactive[i] = inactive[i+1];
		}
		listsize--;
	}

	synchronized void moveDown(HttpServletRequest request)
	{
		int index = Integer.parseInt(request.getParameter("SelectedIndex"));
		lastselected = index + 1;
		if(index!=(listsize-1))
		{
			String tempstatus = status[index];
			String tempstatusid = statusID[index];
			String tempstatusvalue = statusValue[index];
			String tempmainpage = mainPage[index];
         String tempgroupid = groupID[index];
         boolean tempactive = inactive[index];
			int tempUseCount = useCount[index];

			status[index] = status[index+1];
			statusValue[index] = statusValue[index+1];
			statusID[index] = statusID[index+1];
			mainPage[index] = mainPage[index+1];
         groupID[index] = groupID[index+1];
			useCount[index] = useCount[index+1];
			inactive[index] = inactive[index+1];

			status[index+1] = tempstatus;
			statusValue[index+1] = tempstatusvalue;
			statusID[index+1] = tempstatusid;
			mainPage[index+1] = tempmainpage;
         groupID[index+1] = tempgroupid;
			useCount[index+1] = tempUseCount;
			inactive[index+1] = tempactive;
		}
	}

	synchronized void moveUp(HttpServletRequest request)
	{
		int index = Integer.parseInt(request.getParameter("SelectedIndex"));
		lastselected = index - 1;
		if(index!=0)
		{
			String tempstatus = status[index];
			String tempstatusid = statusID[index];
			String tempstatusvalue = statusValue[index];
			String tempmainpage = mainPage[index];
			String tempgroupid = groupID[index];
         boolean tempactive = inactive[index];
			int tempUseCount = useCount[index];

			status[index] = status[index-1];
			statusValue[index] = statusValue[index-1];
			statusID[index] = statusID[index-1];
			mainPage[index] = mainPage[index-1];
			groupID[index] = groupID[index-1];
			useCount[index] = useCount[index-1];
			inactive[index] = inactive[index-1];
         
			status[index-1] = tempstatus;
			statusValue[index-1] = tempstatusvalue;
			statusID[index-1] = tempstatusid;
			mainPage[index-1] = tempmainpage;
			groupID[index-1] = tempgroupid;
			useCount[index-1] = tempUseCount;
			inactive[index-1] = tempactive;
		}
	}

	public synchronized void close()
	{
		if(con!=null)
		{
			if(closeconnection)
			{
				try{
					con.close();
				}catch(Exception e){}
			}
			con = null;
		}
	}

	public synchronized String getErrors()
	{
		return(errors.toString());
	}

	public synchronized void printOptions(String suffix, JspWriter out)
	{
		printOptions(suffix, out, null);
	}

	public synchronized void printOptions(JspWriter out)
	{
		printOptions(null, out, null);
	}

	public synchronized void printOptions(JspWriter out, String selected)
	{
	   printOptions(null, out, selected);
	}
	   
	protected void printMissingStatus(JspWriter out, String selectedid, String suffix)
	{
      try{
         checkConnection();
   	   GenRow row = new GenRow();
         row.setConnection(con);
         row.setTableSpec("StatusList");
         row.setParameter("-select1","StatusID");
         row.setParameter("-select2","Status");
         row.setParameter("StatusID", selectedid);
         row.doAction("select");
         
         if(row.isSuccessful())
         {
            printOption(out, suffix, row.getString("StatusID"), row.getString("Status"), true);
         }
      }catch(Exception e){}
	}
	
	public synchronized void printOptions(String suffix, JspWriter out, String selectedid)
	{
		checkList();
		MultiField mf = new MultiField(selectedid);
		if(statusID!=null)
		{
		   boolean found = false;
		   boolean selected = false;
			for(int i=0; i<listsize; i++)
			{
			   selected = mf.contains(statusID[i]);
			   if(selected)
			   {
               found = true;
			   }
            printOption(out, suffix, statusID[i], status[i], selected);
			}
			if(!found && selectedid!=null && selectedid.length()!=0)
			{
			   printMissingStatus(out, selectedid, suffix);
			}
		}
	}
	
	public synchronized void printActiveOptions(String suffix, JspWriter out, String selectedid) {
		checkList();
		MultiField mf = new MultiField(selectedid);
		if (statusID != null) {
			boolean found = false;
			boolean selected = false;
			for (int i = 0; i < listsize; i++) {
				selected = mf.contains(statusID[i]);
				if (selected) {
					found = true;
				}

				if (!inactive[i]) {
					printOption(out, suffix, statusID[i], status[i], selected);
				}

			}
			if (!found && selectedid != null && selectedid.length() != 0) {
				printMissingStatus(out, selectedid, suffix);
			}
		}
	}

	protected void printOption(JspWriter out, String suffix, String statusid, String status, boolean selected)
	{
      try
      {
         out.print("<option value=\"");
         out.print(statusid);
         out.print("\"");
         if(selected)
         {
            out.print(" selected=\"selected\"");
         }
         out.print(">");
         if (suffix == null) {
            out.print(status);
         }
         else {
            out.print(status + suffix );
         }              
         out.println("</option>");
      }catch(Exception e){}
	}

}
