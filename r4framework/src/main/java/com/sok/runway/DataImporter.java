package com.sok.runway;

import javax.servlet.*;	  
import javax.servlet.http.*;
import javax.naming.*;
import java.io.*;
import java.util.*;
import java.text.*;
import javax.sql.*;
import java.sql.*;

import com.sok.framework.*;
import com.sok.framework.sql.*;

public class DataImporter {
		
	public static final String QUESTIONID = "QUESTIONID";
	public static final String STATUS_HISTORY = "STATUS_HISTORY";
	public static final String FOREIGN_TOKEN = "->";
	
	public static final String MATCH_EXACT = "X";
	public static final String MATCH_PARTIAL = "M";
	public static final String MATCH_PHONE = "P";
	public static final String MATCH_PRODUCTNUM = "N";
	public static final String MATCH_EMAIL = "E";
	public static final String MATCH_MOBILE = "B";
		
	ImportProcess importProcess = null;
	File importFile = null;
	ArrayList columnHeadings = null;
	ArrayList firstRecord = null;
	boolean hasColumnHeadings = false;
	String mode = "insert";
	
	TableSpec tableSpec = null;
	TableSpec relatedTableSpec = null;
	HashMap columnMapping = null;
	HashMap relatedColumnMapping = null;
	HashMap defaultColumnMapping = new HashMap();
	HashMap defaultRelatedColumnMapping = new HashMap();
	StringBuffer importLog = new StringBuffer();
	int numRecords = 0;
	int numSuccess = 0;
	int numDataErrors = 0;
	int numRecordErrors = 0;
	int numRelatedSuccess = 0;
	
	HashMap specFieldTypes = null;
	ArrayList specFieldNames = null;
	ArrayList specFieldLabels = null;
	HashMap relatedSpecFieldTypes = null;
	ArrayList relatedSpecFieldNames = null;
	ArrayList relatedSpecFieldLabels = null;
	
	String companyStatusID = null;
	String contactStatusID = null;
	String groupID = null;
	String source = null;
	String importName = null;
	String repUserID = null;
	String mgrUserID = null;
	String uniqueField = null;
	String uniqueField2 = null;
	String productGroupID = null; 
	String campaignID = null;
        
	boolean testMode = false;
	
	public void setCompanyStatusID(String id) {
		if (id == null) {
			companyStatusID = ActionBean._blank;
		}
		else {
			companyStatusID = id;
		}
	}
	
	private void incSuccess(boolean related) {
		if (related) {
			numRelatedSuccess++;
		}
		else {
			numSuccess++;
		}
	}
	
	public void setContactStatusID(String id) {
		if (id == null) {
			contactStatusID = ActionBean._blank;
		}
		else {
			contactStatusID = id;
		}
	}
	
	public void setName(String importName) {
		if (importName == null) {
			this.importName = ActionBean._blank;
		}
		else {
			this.importName = importName;
		}
	}
	
	public void setSource(String source) {
		if (source == null) {
			this.source = ActionBean._blank;
		}
		else {
			this.source = source;
		}
	}
	
	public void setRepUserID(String repUserID) {
		if (repUserID == null) {
			this.repUserID = ActionBean._blank;
		}
		else {
			this.repUserID = repUserID;
		}
	}
	
	public void setMgrUserID(String mgrUserID) {
		if (mgrUserID == null) {
			this.mgrUserID = ActionBean._blank;
		}
		else {
			this.mgrUserID = mgrUserID;
		}
	}
	public void setProductGroupID(String productGroupID) {
			if (productGroupID == null) {
		 	  this.productGroupID = ActionBean._blank;
			}
			else {
				this.productGroupID = productGroupID;
			}
	}
	
	public void setGroupID(String id) {
		groupID = id;
	}
	
	public void setUniqueField(String uniqueField) {
		if (uniqueField != null && uniqueField.length()>0) {
	 	  this.uniqueField = uniqueField; 
		  } else { 
			  this.uniqueField = null; 
		  }
	}
	
	public void setUniqueField2(String uniqueField2) {
		if (uniqueField2 != null && uniqueField2.length()>0) {
				this.uniqueField2 = uniqueField2;
		} else { 
			this.uniqueField2 = null; 
		}
	}
        
	public void setCampaignID(String campaignID) {
		if (campaignID != null && campaignID.length()>0) {
				this.campaignID = campaignID;
		} else { 
			this.campaignID = null; 
		}
	}        
	
	public DataImporter(HttpServletRequest request, boolean hasColumnHeadings, TableSpec tableSpec, String jndiName) {
		this(request, hasColumnHeadings, tableSpec, null, jndiName);
	}
								
	public DataImporter(HttpServletRequest request, boolean hasColumnHeadings, 
								TableSpec tableSpec, TableSpec relatedTableSpec, String jndiName) {
		this.importFile = getFile(request);
		this.hasColumnHeadings = hasColumnHeadings;
		determineColumnHeadings();
		this.tableSpec = tableSpec;
		this.relatedTableSpec = relatedTableSpec;
		buildFieldList(jndiName, tableSpec, false);
		buildFieldList(jndiName, relatedTableSpec, true);
	}
	
	public DataImporter(File file, boolean hasColumnHeadings, TableSpec tableSpec) {
		this.importFile = file;
		this.hasColumnHeadings = hasColumnHeadings;
		determineColumnHeadings();
		this.tableSpec = tableSpec;
	}
	
	public TableSpec getTableSpec() {
	   return tableSpec;
	}
	
	public TableSpec getRelatedTableSpec() {
	   return relatedTableSpec;
	}
	
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public boolean hasColumnHeadings() {
		return hasColumnHeadings;
	}
	
	public ArrayList getColumnHeadings() {
		return columnHeadings;
	}
	
	public ArrayList getFirstRecord() {
		return firstRecord;
	}
	
	public void setColumnMapping(HashMap columnMapping) {
		this.columnMapping = columnMapping;
	}
	
	public void setRelatedColumnMapping(HashMap columnMapping) {
		this.relatedColumnMapping = columnMapping;
	}
	
	public void setDefaultColumnMap(HashMap defaultColumnMapping) {
		this.defaultColumnMapping = defaultColumnMapping;
	}
	public void setDefaultRelatedColumnMap(HashMap defaultColumnMapping) {
		this.defaultRelatedColumnMapping = defaultColumnMapping;
	}
	
	public void importData(HttpServletRequest request, ServletContext context, boolean testMode) {
	   importData(request, context, testMode, false);
	}
	
	public void importData(HttpServletRequest request, ServletContext context, boolean testMode, boolean streamline) {
		this.testMode = testMode;
		importProcess = new ImportProcess(request, context, streamline);
	}
	
	private void parseFile() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(importFile));
			 
			ArrayList record = new ArrayList();
			String line = reader.readLine();
			while (line != null && !lineParse(record, line)) {
				String newLine = reader.readLine();
				line = line + "\r\n" + newLine;
				record.clear();
			}
			record.clear();
			line = reader.readLine();
			
			HashMap columns = new HashMap();
			while (line != null) {
				record = new ArrayList();
				if (!lineParse(record, line)) {
					String newLine = reader.readLine();
					if (newLine == null) {
						break;
					}
					line = line + "\r\n" + newLine;
				}
				else {
					columns.clear();
					for (int i=0; i < record.size() && i < columnHeadings.size(); i++) {
						//System.out.print(i + ". ");
						//System.out.println(record.get(i));
						//columns.put(columnHeadings.get(i), record.get(i));
					}
					numRecords++;
					importLog.append("\r\nProcessing record ");
					importLog.append(numRecords);
					importLog.append(".\r\n");
					line = reader.readLine();
				}
			}
		}
		catch (Exception e) { 
			e.printStackTrace();
			//importLog.append(ActionBean.writeStackTraceToString(e));
		}
		finally {
			try { reader.close(); }
			catch (Exception ex) {}
		}
	}
	
	private void createImportRecord(String importID) {
		//String importID = KeyMaker.generate();
		RowBean bean = new RowBean();
		bean.setTableSpec("Imports");
		bean.setConnection(importProcess.getConnection());
		bean.setColumn("ImportID", importID);
		bean.setColumn("CompanyStatusID", companyStatusID);
		bean.setColumn("ContactStatusID", contactStatusID);
		bean.setColumn("CreatedBy", importProcess.userid);
		bean.setColumn("CreatedDate", "NOW");
		bean.setColumn("GroupID",groupID);
		bean.setColumn("Source", source);
		bean.setColumn("ProductGroupID",productGroupID);
		bean.setColumn("Name", importName);
		bean.setColumn("RepUserID", repUserID);
		bean.setColumn("MgrUserID", mgrUserID);
      bean.setColumn("CampaignID", campaignID);
		bean.setColumn("Mode", mode);
		bean.setAction("insert");
		bean.doAction();
		
		if (bean.getError().length() > 0) {
			importLog.append("\r\nError creating import record - ");
			importLog.append(bean.getError());
			importLog.append(".\r\n");
			//return null;
		}
		//return importID;
	}
	
	private String processFile(String importID) {
				
		if (importID != null) {
			TableBean bean = new TableBean();
			bean.setConnection(importProcess.getConnection());
			bean.setTableSpec(tableSpec);
			
			TableBean relatedBean = null;
			if (relatedTableSpec != null) {
				relatedBean = new TableBean();
				relatedBean.setConnection(importProcess.getConnection());
				relatedBean.setTableSpec(relatedTableSpec);
			}
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(importFile));
				 
				ArrayList record = new ArrayList();
				String line = reader.readLine();
				if (hasColumnHeadings) {
					while (line != null && !lineParse(record, line)) {
						String newLine = reader.readLine();
						line = line + "\r\n" + newLine;
						record.clear();
					}
					record.clear();
					line = reader.readLine();
				}
			
				HashMap<String,String> columns = new HashMap<String,String>();
				String lastUniqueFieldValue = null;
				String currentUniqueFieldValue = null;
				String lastUniqueFieldValue2 = null;
				String currentUniqueFieldValue2 = null;
				
				while (line != null && !importProcess.isKilled()) {
					record = new ArrayList();
					if (!lineParse(record, line)) {
						String newLine = reader.readLine();
						if (newLine == null) {
							break;
						}
						line = line + "\r\n" + newLine;
					}
					else {
						columns.clear();
						for (int i=0; i < record.size() && i < columnHeadings.size(); i++) {
							columns.put((String)columnHeadings.get(i), StringUtil.trim((String)record.get(i)));
							if (uniqueField != null && columnHeadings.get(i).equals(uniqueField)) {
								currentUniqueFieldValue = StringUtil.trim((String)record.get(i));
							}
							if (uniqueField2 != null && columnHeadings.get(i).equals(uniqueField2)) {
								currentUniqueFieldValue2 = StringUtil.trim((String)record.get(i));
							}
						}
						numRecords++;
						
						int temp = numSuccess;
						int temp2 = numRelatedSuccess;
						
						/*
						 * assumes that there will only be a second unique field if there is a first one. 
						 */
						boolean checkOne = (uniqueField == null && uniqueField2 == null);  //if the unique fields are not used. 
						boolean checkTwo = (lastUniqueFieldValue == null || (uniqueField2 != null && lastUniqueFieldValue2 == null)); //if this is the first time this has been run. 
						boolean checkThree = (uniqueField != null && !currentUniqueFieldValue.equals(lastUniqueFieldValue));  //first unique fields don't match
						boolean checkFour = (uniqueField2 != null && !currentUniqueFieldValue2.equals(lastUniqueFieldValue2)); //second unique fields don't match
						
						boolean insertParentRecord = checkOne || checkTwo || checkThree || checkFour; 

						if (insertParentRecord) {
							insertRecord(importID, bean, columns, null);
						}
						lastUniqueFieldValue = currentUniqueFieldValue;
						lastUniqueFieldValue2 = currentUniqueFieldValue2;
						
						if (relatedTableSpec != null ) {
							insertRecord(importID, relatedBean, columns, bean);
						}
						if ((insertParentRecord && numSuccess == temp) || (relatedTableSpec != null && numRelatedSuccess == temp2)) {
							importLog.append("\r\n\r\nError occurred at record ");
							importLog.append(numRecords);
							importLog.append(".\r\n\r\n");
						}
						line = reader.readLine();
					}
				}
			}
			catch (Exception e) { 
				importLog.append("\r\nProcessing record ");
				importLog.append(numRecords);
				importLog.append(".\r\n");
				importLog.append(ActionBean.writeStackTraceToString(e));
			}
			finally {
				try { reader.close(); }
				catch (Exception ex) {}
			}
		}
		createImportRecord(importID);
		
		return importID;
	}
	
	private String getAnswerTable(TableBean bean) {
		if(bean.getTableSpec().getTableName().equals("Companies")) { 
			return "answers/CompanyAnswers"; 
		} else if(bean.getTableSpec().getTableName().equals("Contacts")) {
			return "answers/ContactAnswers";
		} else if(bean.getTableSpec().getTableName().equals("Products")) {
			return "answers/ProductAnswers";
		} else if(bean.getTableSpec().getTableName().equals("Orders")) {
                  
                   return "answers/OrderAnswers";
                   
                }
		if(mode.equals("direct")) {
			importLog.append("Spec as "+bean.getTableSpec().getTableName()+"not in custom list");
		} 
		return null; 
	}
	
	public RowBean insertRecord(String importID, TableBean bean, Map<String,String> recordColumns, TableBean parentBean) {//String relatedPK,
		TableSpec tSpec = tableSpec;
		HashMap curColumnMapping = columnMapping;
		HashMap curDefColumnMapping = defaultColumnMapping;
		ArrayList curSpecFieldNames = getFieldNames();
		bean.clear();
		bean.setConnection(importProcess.getConnection());
		
		if (parentBean != null) {
			tSpec = relatedTableSpec;
			curColumnMapping = relatedColumnMapping;
			curDefColumnMapping = defaultRelatedColumnMapping;
			curSpecFieldNames = getRelatedFieldNames();
		}
		
		String pkName = tSpec.getPrimaryKeyName();
		bean.setColumn(pkName, KeyMaker.generate());
		if (pkName.startsWith("Import")) {
			String pseudoKeyName = pkName.substring(6);
			bean.setColumn(pseudoKeyName, KeyMaker.generate());
		}
		
		if (parentBean != null) {	
			String fkName = tableSpec.getPrimaryKeyName();
			bean.setColumn(fkName,parentBean.getString(fkName));
			
			if (fkName.startsWith("Import")) {
				fkName = fkName.substring(6);
				bean.setColumn(fkName,parentBean.getString(fkName));
			}
		}
		
		ArrayList<TableBean> foreignBeans = new ArrayList<TableBean>();
		String fieldName;
		String columnName;
		String columnValue;
		for (int i=0; i < curSpecFieldNames.size(); i++) {
			fieldName = (String)curSpecFieldNames.get(i);
			columnName = (String)curColumnMapping.get(fieldName);
			if (columnName != null && columnName.length() > 0) {
				columnValue = recordColumns.get(columnName); /* removed cast due to use of generics */ 
				
				if (columnValue != null && columnValue.length() > 0) {
					if (fieldName.indexOf(FOREIGN_TOKEN) == -1) {
						if (validData(fieldName, columnValue)) {
							bean.setColumn(fieldName, columnValue);
						}
					}
					else {
						foreignBeans.add(processForeignData(importID, fieldName, columnValue, bean, getAnswerTable(bean)));
					}
				}
			}
		}
		Iterator iter = curDefColumnMapping.keySet().iterator();
		while (iter.hasNext()) {
			fieldName = (String)iter.next();
			columnValue = (String)curDefColumnMapping.get(fieldName);
			
			if (columnValue != null && columnValue.length() > 0) {
				if (fieldName.indexOf(FOREIGN_TOKEN) == -1) {
					if (validData(fieldName, columnValue)) {
						bean.setColumn(fieldName, columnValue);
					}
				}
				else {
					foreignBeans.add(processForeignData(importID, fieldName, columnValue, bean, getAnswerTable(bean)));
				}
			}
		}
		if (tSpec.getName().equals("Companies")) {
			foreignBeans.add(processForeignData(importID, STATUS_HISTORY+FOREIGN_TOKEN+"CompanyStatus", companyStatusID, bean, null));
		}
		else if (tSpec.getName().equals("Contacts")) {
			foreignBeans.add(processForeignData(importID, STATUS_HISTORY+FOREIGN_TOKEN+"ContactStatus", contactStatusID, bean, null));
		}
		bean.setColumn("ModifiedDate","NOW");
		bean.setColumn("CreatedDate","NOW");
		bean.setColumn("ModifiedBy",importProcess.userid);
		bean.setColumn("CreatedBy",importProcess.userid);
		bean.setColumn("ImportID",importID);
		bean.setColumn("GroupID",groupID);
		if (bean.get("Source") == null || "".equals(bean.get("Source"))) {
			bean.setColumn("Source", source);
		}
		bean.setColumn("RepUserID",repUserID);
		bean.setColumn("MgrUserID",mgrUserID);
		bean.setAction(ActionBean.INSERT);
		
		if (!testMode) {
			bean.doAction();
			if (bean.getError().length()==0) {
				incSuccess(parentBean != null);
				TableBean foreignBean;
				for (int i=0; i < foreignBeans.size(); i++) {
					foreignBean = foreignBeans.get(i);
					if (!testMode && foreignBean != null) {
						foreignBean.setColumn(pkName, bean.getString(pkName));
						foreignBean.doAction();
						if (foreignBean.getUpdatedCount()!=1) {
							importLog.append(foreignBean.getError());
						}
					} 
				}
			}
			else {
				numRecordErrors++;
				importProcess.setLastError(bean.getError());
				importLog.append(bean.getError());
			}
		}
		else {
			incSuccess(parentBean != null);
		}
		return bean;
	}
	
	public boolean validData(String fieldName, String columnValue) {
		Integer fieldType = (Integer)specFieldTypes.get(fieldName);
		if (fieldType != null) {
			try {
				switch (fieldType.intValue()) {
					case 93: //Date
						SimpleDateFormat formatter = new SimpleDateFormat("d/m/y h:m");
						Calendar calendar = new GregorianCalendar();
						calendar.set(1960,0,1);
						formatter.set2DigitYearStart(calendar.getTime());
						try {
							formatter.parse(columnValue);
						}
						catch (ParseException pe) {
							// second chance
							formatter.applyPattern("d/m/y");
							formatter.parse(columnValue);
						}
						break;
					case 6:  //Numeric
							Float.parseFloat(columnValue);
						break;
					case 12: //Text
						break;
				}
				return true;
			}
			catch (NumberFormatException e) {
				importLog.append("	Could not insert data '");
				importLog.append(columnValue);
				importLog.append("' into field [");
				importLog.append(fieldName);
				importLog.append("]. Data was not in numeric format - (ignored)\r\n");
				numDataErrors++;
			}
			catch (ParseException e) {
				importLog.append("	Could not insert data '");
				importLog.append(columnValue);
				importLog.append("' into field [");
				importLog.append(fieldName);
				importLog.append("]. Data was not in date format - (ignored)\r\n");
				numDataErrors++;
			}
			return false;
		}
		return true;
	}
	
	public TableBean processForeignData(String importID, String foreignString, String value, TableBean relatedBean, String answerSpec) {
		TableBean bean = new TableBean();
		bean.setConnection(importProcess.getConnection());
		TableSpec foreignSpec = null;;
		if (foreignString.startsWith(QUESTIONID)) {
			boolean directImport = false;
			if (relatedBean.getTableSpec().getName().startsWith("Import")) {
				foreignSpec = SpecManager.getTableSpec("ImportAnswers");
			}
			else if (answerSpec != null) {
				foreignSpec = SpecManager.getTableSpec(answerSpec);
				directImport = true;
			}
			if (foreignSpec != null) {
				bean.setTableSpec(foreignSpec);
				bean.setColumn("ImportID", importID);
				bean.setColumn(foreignSpec.getPrimaryKeyName(), KeyMaker.generate());
				bean.setColumn("Answer", value);
				if (directImport) {
					bean.setColumn("AnswerText", value);
					bean.setColumn("CreatedDate","NOW");
					bean.setColumn("CreatedBy",importProcess.userid);
					bean.setColumn("ModifiedDate","NOW");
					bean.setColumn("ModifiedBy",importProcess.userid);
					AnswerBean.setDateAnswer(value, bean);
					AnswerBean.setNumberAnswer(value, bean);
				}
				//bean.setColumn(pkName, pkValue);
				bean.setColumn("QuestionID", foreignString.substring(QUESTIONID.length() + FOREIGN_TOKEN.length()));
				bean.setAction(ActionBean.INSERT);
				return bean;
			}
			else {
				importLog.append("Foreign spec was null"+foreignString+" "+value);
				return null;
			}
		}
		else if (foreignString.startsWith(STATUS_HISTORY)) {
			foreignSpec = SpecManager.getTableSpec(foreignString.substring(STATUS_HISTORY.length() + FOREIGN_TOKEN.length()));
			bean.setTableSpec(foreignSpec);
			String newID = KeyMaker.generate();
			bean.setColumn(foreignSpec.getPrimaryKeyName(), newID);
			relatedBean.setColumn(foreignSpec.getPrimaryKeyName(), newID);
			bean.setColumn("StatusID", value);
			//bean.setColumn(pkName, pkValue);
			bean.setColumn("CreatedDate","NOW");
			bean.setColumn("CreatedBy",importProcess.userid);
			bean.setAction(ActionBean.INSERT);
			return bean;
		}
		return null;
	}
	
	private void detectConflicts(String importID) {
		
		RowBean group = new RowBean();
		group.setViewSpec("GroupView");
		group.setConnection(importProcess.getConnection());
		group.setColumn("GroupID", groupID);
		group.setAction("select");
		group.doAction();
		
		
		String sectionCode = group.getColumn("SectionCode");
		if (sectionCode.length() > 0 ) {
			sectionCode = sectionCode + "%";
		}
		else {
			sectionCode = "XXXXXX";
		}
		RowSetBean importBeans = new RowSetBean();
		importBeans.setConnection(importProcess.getConnection());
		
		importBeans.setViewSpec("ImportCompanyView");
		importBeans.setColumn("ImportID", importID);
		importBeans.generateSQLStatement();
		importBeans.getResults();
		while(importBeans.getNext()) {
			detectCompanyConflicts(importBeans.getString("ImportCompanyID"), sectionCode);
		}
		
		importBeans.clear();
		importBeans.setViewSpec("ImportContactView");
		importBeans.setColumn("ImportID", importID);
		importBeans.generateSQLStatement();
		importBeans.getResults();
		while(importBeans.getNext()) {
			detectContactConflicts(importBeans.getString("ImportContactID"), sectionCode);
		}
		
		importBeans.clear();
		importBeans.setViewSpec("ImportProductView");
		importBeans.setColumn("ImportID", importID);
		importBeans.generateSQLStatement();
		importBeans.getResults();
		while(importBeans.getNext()) {
			detectProductConflicts(importBeans.getString("ImportProductID"));
		}
	}
	
	private void detectCompanyConflicts(String importCompanyID, String sectionCode) {
		try {
			SqlDatabase database = ActionBean.getDatabase();
			
			RowBean bean = new RowBean();
			bean.setConnection(importProcess.getConnection());
			bean.setAction("execute");
			
			StringBuffer detectExact = new StringBuffer();
				detectExact.append("Update ImportCompanies set Conflicts = '");
				detectExact.append(MATCH_EXACT);
				detectExact.append("', ImportFlg = 'C' WHERE ImportCompanyID = '");
				detectExact.append(importCompanyID);
				detectExact.append("' AND Exists (SELECT * FROM Companies, Groups WHERE Companies.GroupID = Groups.GroupID AND Groups.SectionCode like '");
				detectExact.append(sectionCode);
				detectExact.append("' AND Companies.Company = ImportCompanies.Company)");
			bean.setSearchStatement(detectExact.toString());
			bean.doAction();
			
			bean.setConnection(importProcess.getConnection());
			StringBuffer setToNull = new StringBuffer();
				setToNull.append("UPDATE ImportCompanies set  Conflicts = '' WHERE ImportCompanyID = '");
				setToNull.append(importCompanyID);
				setToNull.append("' AND Conflicts is null");
			bean.setSearchStatement(setToNull.toString());
			bean.doAction();
			
			bean.setConnection(importProcess.getConnection());	
			StringBuffer detectPartialMatch = new StringBuffer();
				detectPartialMatch.append("UPDATE ImportCompanies set Conflicts = ");
				
				detectPartialMatch.append(database.getContatenateString("Conflicts","'"+MATCH_PARTIAL+"'"));
				
				detectPartialMatch.append(", ImportFlg = 'C' WHERE ImportCompanyID = '");
				detectPartialMatch.append(importCompanyID);
				detectPartialMatch.append("' AND Conflicts = '' AND Exists ");
						detectPartialMatch.append("(SELECT * FROM Companies ,Groups WHERE Companies.GroupID = Groups.GroupID AND Groups.SectionCode like '");
						detectPartialMatch.append(sectionCode);
						detectPartialMatch.append("' AND (");
						
						detectPartialMatch.append(database.getReplaceString("ImportCompanies.Company"," ", ""));
						detectPartialMatch.append(" LIKE ");
						detectPartialMatch.append(database.getContatenateString(database.getReplaceString("Companies.Company"," ", ""),"'%'"));
						detectPartialMatch.append(" OR ");
						
						detectPartialMatch.append(database.getReplaceString("ImportCompanies.Company"," ", ""));
						detectPartialMatch.append(" LIKE ");
						detectPartialMatch.append(database.getContatenateString("'%'",database.getReplaceString("Companies.Company"," ", "")));
						detectPartialMatch.append(" OR ");
						
						detectPartialMatch.append(database.getReplaceString("Companies.Company"," ", ""));
						detectPartialMatch.append(" LIKE ");
						detectPartialMatch.append(database.getContatenateString(database.getReplaceString("ImportCompanies.Company"," ", ""),"'%'"));
						detectPartialMatch.append(" OR ");
						
						detectPartialMatch.append(database.getReplaceString("Companies.Company"," ", ""));
						detectPartialMatch.append(" LIKE ");
						detectPartialMatch.append(database.getContatenateString("'%'",database.getReplaceString("ImportCompanies.Company"," ", "")));
						
						detectPartialMatch.append(") AND ");
						detectPartialMatch.append(database.getReplaceString("Companies.Company"," ", ""));
						detectPartialMatch.append(" <> '' AND ImportCompanies.Company <> Companies.Company)");
						
			bean.setSearchStatement(detectPartialMatch.toString());
			bean.doAction();
			
			bean.setConnection(importProcess.getConnection());
			StringBuffer detectPhoneMatch = new StringBuffer();
				detectPhoneMatch.append("UPDATE ImportCompanies set Conflicts = ");
				
				detectPhoneMatch.append(database.getContatenateString("Conflicts","'"+MATCH_PHONE+"'"));
				
				detectPhoneMatch.append(", ImportFlg = 'C' WHERE ImportCompanyID = '");
				detectPhoneMatch.append(importCompanyID);
				detectPhoneMatch.append("' AND Exists ");
					detectPhoneMatch.append("(SELECT * FROM Companies ,Groups WHERE Companies.GroupID = Groups.GroupID AND Groups.SectionCode like '");
						detectPhoneMatch.append(sectionCode);
												
						detectPhoneMatch.append("' AND ");
						detectPhoneMatch.append(database.getReplaceString("Companies.Phone"," ", ""));
						detectPhoneMatch.append(" = ");
						detectPhoneMatch.append(database.getReplaceString("ImportCompanies.Phone"," ", ""));
						detectPhoneMatch.append(")");
						
			bean.setSearchStatement(detectPhoneMatch.toString());
			bean.doAction();
			
			//setImportFlag("ImportCompanies", importID);
			/*StringBuffer setImportFlag = new StringBuffer();
				setImportFlag.append("UPDATE ImportCompanies set ImportFlg = 'Y' WHERE ImportFlg is null OR ImportFlg <> 'C'");
			bean.setSearchStatement(setImportFlag.toString());
			bean.doAction();*/
			importLog.append(bean.getError());
		}
		catch (Exception e) {
			importLog.append("\r\n Error initiating the Duplication Dection procedure - ");
			importLog.append(e.toString());
			importLog.append(".\r\n");
		}
	}
	
	private void detectContactConflicts(String importContactID, String sectionCode) {
		try {
			SqlDatabase database = ActionBean.getDatabase();
			
			RowBean bean = new RowBean();
			bean.setAction("execute");
			
			StringBuffer detectExact = new StringBuffer();
				detectExact.append("Update ImportContacts set Conflicts = '");
				detectExact.append(MATCH_EXACT);
				detectExact.append("', ImportFlg = 'C' WHERE ImportContactID = '");
				detectExact.append(importContactID);
				detectExact.append("' AND Exists (SELECT * FROM Contacts, Groups WHERE Contacts.GroupID = Groups.GroupID AND Groups.SectionCode like '");
				detectExact.append(sectionCode);
				detectExact.append("'AND Contacts.FirstName = ImportContacts.FirstName AND Contacts.LastName = ImportContacts.LastName)");
			bean.setSearchStatement(detectExact.toString());
			bean.doAction();
			importLog.append(bean.getError());
			StringBuffer setToNull = new StringBuffer();
				setToNull.append("UPDATE ImportContacts set  Conflicts = '' WHERE ImportContactID = '");
				setToNull.append(importContactID);
				setToNull.append("' AND Conflicts is null");
			bean.setSearchStatement(setToNull.toString());
			bean.doAction();
			
			StringBuffer detectEmailMatch = new StringBuffer();
				detectEmailMatch.append("UPDATE ImportContacts set Conflicts = ");
				
				detectEmailMatch.append(database.getContatenateString("Conflicts","'"+MATCH_EMAIL+"'"));
				
				detectEmailMatch.append(", ImportFlg = 'C' WHERE ImportContactID = '");
				detectEmailMatch.append(importContactID);
				detectEmailMatch.append("' AND Exists ");
					detectEmailMatch.append("(SELECT * FROM Contacts ,Groups WHERE Contacts.GroupID = Groups.GroupID AND Groups.SectionCode like '");
						detectEmailMatch.append(sectionCode);
					detectEmailMatch.append("' AND Contacts.Email = ImportContacts.Email)");
			bean.setSearchStatement(detectEmailMatch.toString());
			bean.doAction();
			importLog.append(bean.getError());
			
			StringBuffer detectPhoneMatch = new StringBuffer();
				detectPhoneMatch.append("UPDATE ImportContacts set  Conflicts = ");
				
				detectPhoneMatch.append(database.getContatenateString("Conflicts","'"+MATCH_PHONE+"'"));
				
				detectPhoneMatch.append(", ImportFlg = 'C' WHERE ImportContactID = '");
				detectPhoneMatch.append(importContactID);
				detectPhoneMatch.append("' AND Exists ");
					detectPhoneMatch.append("(SELECT * FROM Contacts ,Groups WHERE Contacts.GroupID = Groups.GroupID AND Groups.SectionCode like '");
						detectPhoneMatch.append(sectionCode);					
						
						detectPhoneMatch.append("' AND ");
						detectPhoneMatch.append(database.getReplaceString("Contacts.Phone"," ", ""));
						detectPhoneMatch.append(" = ");
						detectPhoneMatch.append(database.getReplaceString("ImportContacts.Phone"," ", ""));
						detectPhoneMatch.append(")");
			bean.setSearchStatement(detectPhoneMatch.toString());
			bean.doAction();
			importLog.append(bean.getError());
			
			StringBuffer detectMobileMatch = new StringBuffer();
				detectMobileMatch.append("UPDATE ImportContacts set  Conflicts = ");
				
				detectMobileMatch.append(database.getContatenateString("Conflicts","'"+MATCH_MOBILE+"'"));
				
				detectMobileMatch.append(", ImportFlg = 'C' WHERE ImportContactID = '");
				detectMobileMatch.append(importContactID);
				detectMobileMatch.append("' AND Exists ");
					detectMobileMatch.append("(SELECT * FROM Contacts ,Groups WHERE Contacts.GroupID = Groups.GroupID AND Groups.SectionCode like '");
						detectMobileMatch.append(sectionCode);					
						
						detectMobileMatch.append("' AND ");
						detectMobileMatch.append(database.getReplaceString("Contacts.Mobile"," ", ""));
						detectMobileMatch.append(" = ");
						detectMobileMatch.append(database.getReplaceString("ImportContacts.Mobile"," ", ""));
						detectMobileMatch.append(")");
			bean.setSearchStatement(detectMobileMatch.toString());
			bean.doAction();
			importLog.append(bean.getError());
			
			//setImportFlag("ImportContacts", importID);
			
			/*StringBuffer setImportFlag = new StringBuffer();
				setImportFlag.append("UPDATE ImportContacts set ImportFlg = 'Y' WHERE ImportFlg is null OR ImportFlg <> 'C'");
			bean.setSearchStatement(setImportFlag.toString());
			bean.doAction();*/
	importLog.append(bean.getError());
		}
		catch (Exception e) {
			importLog.append("\r\n Error initiating the Duplication Dection procedure - ");
			importLog.append(e.toString());
			importLog.append(".\r\n");
		}
	}
	
	private void detectProductConflicts(String importProductID) {
		try {
			SqlDatabase database = ActionBean.getDatabase();
			
			RowBean bean = new RowBean();
			bean.setAction("execute");
			
			StringBuffer detectExact = new StringBuffer();
				detectExact.append("Update ImportProducts set Conflicts = '");
				detectExact.append(MATCH_EXACT);
				detectExact.append("', ImportFlg = 'C' WHERE ImportProductID = '");
				detectExact.append(importProductID);
				detectExact.append("' AND Exists (SELECT * FROM Products WHERE Products.Name = ImportProducts.Name)");
			bean.setSearchStatement(detectExact.toString());
			bean.doAction();
			
			StringBuffer setToNull = new StringBuffer();
				setToNull.append("UPDATE ImportProducts set  Conflicts = '' WHERE ImportProductID = '");
				setToNull.append(importProductID);
				setToNull.append("' AND Conflicts is null");
			bean.setSearchStatement(setToNull.toString());
			bean.doAction();
				
			StringBuffer detectPartialMatch = new StringBuffer();
				detectPartialMatch.append("UPDATE ImportProducts set Conflicts = ");
				
				detectPartialMatch.append(database.getContatenateString("Conflicts","'"+MATCH_PARTIAL+"'"));
				
				detectPartialMatch.append(", ImportFlg = 'C' WHERE ImportProductID = '");
				detectPartialMatch.append(importProductID);
				detectPartialMatch.append("' AND Conflicts = '' AND Exists ");
						detectPartialMatch.append("(SELECT * FROM Products ");
						detectPartialMatch.append("WHERE (");
						
						/*detectPartialMatch.append("REPLACE(ImportProducts.Name, ' ', '') LIKE REPLACE(Products.Name, ' ', '') + '%' OR ");
						detectPartialMatch.append("REPLACE(ImportProducts.Name, ' ', '') LIKE '%' + REPLACE(Products.Name, ' ', '') OR ");
						detectPartialMatch.append("REPLACE(Products.Name, ' ', '') LIKE REPLACE(ImportProducts.Name, ' ', '') + '%' OR ");
						detectPartialMatch.append("REPLACE(Products.Name, ' ', '') LIKE '%' + REPLACE(ImportProducts.Name, ' ', '')) ");
						detectPartialMatch.append("AND ImportProducts.Name <> Products.Name)");*/
			
									
						detectPartialMatch.append(database.getReplaceString("ImportProducts.Name"," ", ""));
						detectPartialMatch.append(" LIKE ");
						detectPartialMatch.append(database.getContatenateString(database.getReplaceString("Products.Name"," ", ""),"'%'"));
						detectPartialMatch.append(" OR ");
						
						detectPartialMatch.append(database.getReplaceString("ImportProducts.Name"," ", ""));
						detectPartialMatch.append(" LIKE ");
						detectPartialMatch.append(database.getContatenateString("'%'",database.getReplaceString("Products.Name"," ", "")));
						detectPartialMatch.append(" OR ");
						
						detectPartialMatch.append(database.getReplaceString("Products.Name"," ", ""));
						detectPartialMatch.append(" LIKE ");
						detectPartialMatch.append(database.getContatenateString(database.getReplaceString("ImportProducts.Name"," ", ""),"'%'"));
						detectPartialMatch.append(" OR ");
						
						detectPartialMatch.append(database.getReplaceString("Products.Name"," ", ""));
						detectPartialMatch.append(" LIKE ");
						detectPartialMatch.append(database.getContatenateString("'%'",database.getReplaceString("ImportProducts.Name"," ", "")));
						
						detectPartialMatch.append(") AND ");
						detectPartialMatch.append(database.getReplaceString("Products.Name"," ", ""));
						detectPartialMatch.append(" <> '' AND ImportProducts.Name <> Products.Name)");
			bean.setSearchStatement(detectPartialMatch.toString());
			bean.doAction();
						
						
			
			StringBuffer detectProductNumMatch = new StringBuffer();
				detectProductNumMatch.append("UPDATE ImportProducts set Conflicts = ");
				
				detectPartialMatch.append(database.getContatenateString("Conflicts","'"+MATCH_PRODUCTNUM+"'"));
				
				detectProductNumMatch.append(", ImportFlg = 'C' WHERE ImportProductID = '");
				detectProductNumMatch.append(importProductID);
				detectProductNumMatch.append("' AND Exists ");
					detectProductNumMatch.append("(SELECT * FROM Products ");
					
					detectProductNumMatch.append("WHERE ");
					detectProductNumMatch.append(database.getReplaceString("Products.ProductNum"," ", ""));
					detectProductNumMatch.append(" = ");
					detectProductNumMatch.append(database.getReplaceString("ImportProducts.ProductNum"," ", ""));
					
					detectProductNumMatch.append(")");
					
					
			bean.setSearchStatement(detectProductNumMatch.toString());
			bean.doAction();
			
			//setImportFlag("ImportProducts", importID);
			
			/*StringBuffer setImportFlag = new StringBuffer();
				setImportFlag.append("UPDATE ImportProducts set ImportFlg = 'Y' WHERE ImportFlg is null OR ImportFlg <> 'C'");
			bean.setSearchStatement(setImportFlag.toString());
			bean.doAction();*/
	
		}
		catch (Exception e) {
			importLog.append("\r\n Error initiating the Duplication Dection procedure - ");
			importLog.append(e.toString());
			importLog.append(".\r\n");
		}
		
	}
	
	private void setImportFlag(String table, String importID) {

		GenRow row = new GenRow();
		row.setConnection(importProcess.getConnection());
		row.setTableSpec(table);
		row.setParameter("ImportFlg", "Y");
		row.setParameter("ON-ImportID", importID);
      row.setParameter("ON-ImportFlg", "NULL+!C");
      try
      {
         row.doAction("updaetall");
      }
      catch(Exception e)
      {
			importLog.append(e);
      }
	}
	
////////////////////////////////////////////////////////////////////////////////////
	// Setup data for Import
	public File getFile(HttpServletRequest request) {
		UploadWrapper multi = null;
		if (!(request instanceof UploadWrapper)) {
			if(request.getAttribute("MultipartWrapper")!=null) {
				multi = (UploadWrapper)request.getAttribute("MultipartWrapper");
			}
		}
		else {
			multi = (UploadWrapper) request;
		}
		
		if(multi!=null) {
			Enumeration files = multi.getFileNames();
			while (files.hasMoreElements()) {
				String name = (String)files.nextElement();
				File f = multi.getFile(name);
				if (f != null) {
					File folder = f.getParentFile().getParentFile();
					File processfolder = new File(folder,"imported");
					if (!processfolder.exists()) {
						processfolder.mkdir();
					}
					File dest = new File(processfolder,f.getName()+".imported");
					if (dest.exists()) {
						dest.delete();
					}
					f.renameTo(dest);
					return dest;
			  	}
			}
		}
		return null;
	}
	
	public void determineColumnHeadings() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(importFile));
			
			firstRecord = new ArrayList();
			String line = reader.readLine();
			
			while (line != null && !lineParse(firstRecord, line)) {
				String newLine = reader.readLine();
				line = line + "\r\n" + newLine;
				firstRecord.clear();
			}
	  
			if (hasColumnHeadings) {
				columnHeadings = firstRecord;
			}
			else {
				columnHeadings = createDefaultColumnHeaders(firstRecord.size());
			}
		}
		catch (IOException e) { 
		}
		finally {
			try { reader.close(); }
			catch (Exception ex) {}
		}
	}
	
	public boolean lineParse(ArrayList record, String line) {
		char[] chars = line.toCharArray();
		
		boolean inQuote = false;
		int startIndex = 0;
		for (int i=0; i < chars.length; i++) {
			if (chars[i] == '"') {
				inQuote = !inQuote;
			}
			else if (!inQuote && chars[i] == ',') {
				record.add(convertFragment(line.substring(startIndex,i)));
				startIndex = i+1;
			}
		}
		if (!inQuote) {
			if (startIndex == chars.length) {
				record.add("");
			}
			else if (startIndex < chars.length) {
				record.add(convertFragment(line.substring(startIndex)));
			}
		}
		return !inQuote;
	}
	
	public ArrayList createDefaultColumnHeaders(int noColumns) {
		DecimalFormat format = new DecimalFormat("C000");
		ArrayList columnHeadings = new ArrayList();
		
		for (int i=1; i <= noColumns; i++) {
			columnHeadings.add(format.format((long)i));
		}
		return columnHeadings;
	}

////////////////////////////////////////////////////////////////////////////////////
	
	public String convertFragment(String fragment) {
		if (fragment.startsWith("\"") && fragment.endsWith("\"")) {
			fragment = fragment.substring(1,fragment.length()-1);
		}
		fragment = StringUtil.replace(fragment, "\"\"","\"");
		return fragment;
	}
	
	public void buildFieldList(String jndiName, TableSpec tSpec, boolean related) {
		ArrayList tempFieldNames = new ArrayList();
		ArrayList tempFieldLabels = new ArrayList();
		HashMap tempFieldTypes = new HashMap();
		if (tSpec != null) {
			RowSetBean questions = new RowSetBean();
			questions.setJndiName(jndiName);
			questions.setViewSpec(SpecManager.getViewSpec("QuestionView"));
			
			for (int i=0; i < tSpec.getFieldsLength(); i++) {
				if (tSpec.getImport(i).equals("true")) {
					tempFieldNames.add(tSpec.getFieldName(i));
					tempFieldTypes.put(tSpec.getFieldName(i), new Integer(tSpec.getFieldType(i)));
					
					if (tSpec.getName().equals("Contacts") || tSpec.getName().equals("ImportContacts")) {
						tempFieldLabels.add(FieldLabeler.getContactLabelForField(tSpec.getFieldName(i)));
					}
					else {
						if (tSpec.getLabel(i) != null) {
							tempFieldLabels.add(tSpec.getLabel(i));
						}
						else {
							tempFieldLabels.add(tSpec.getFieldName(i));
						}
					}
				}
			}
			
			if (tSpec.getName().equals("Products") || tSpec.getName().equals("ImportProducts") ) {
				questions.setColumn("QuestionSurveyQuestion.SurveyQuestionSurvey.ProductProfile","Y");
			}
			else if (tSpec.getName().equals("Contacts") || tSpec.getName().equals("ImportContacts")) {
				questions.setColumn("QuestionSurveyQuestion.SurveyQuestionSurvey.DisplayInContactView","Y");
			}
			else if (tSpec.getName().equals("Companies") || tSpec.getName().equals("ImportCompanies")) {
				questions.setColumn("QuestionSurveyQuestion.SurveyQuestionSurvey.DisplayInCompanyView","Y");
			}
			else if (tSpec.getName().equals("Orders")) {
				questions.setColumn("QuestionSurveyQuestion.SurveyQuestionSurvey.OrderProfile","Y");
			}
			else {
				questions.setColumn("QuestionID","XXXXXXXXXXXXXXXX");
			}
			questions.generateSQLStatment();
			questions.getResults();
			while(questions.getNext()) {
				tempFieldNames.add(QUESTIONID + FOREIGN_TOKEN + questions.getColumn("QuestionID"));
				tempFieldLabels.add(questions.getColumn("Label"));
			}
			questions.close();
		
			if (related) {
				relatedSpecFieldTypes = tempFieldTypes;
				relatedSpecFieldNames = tempFieldNames;
				relatedSpecFieldLabels = tempFieldLabels;
			}
			else {
				specFieldTypes = tempFieldTypes;
				specFieldNames = tempFieldNames;
				specFieldLabels = tempFieldLabels;
			}
		}
	}
	
	public ArrayList getFieldLabels() {
		return specFieldLabels;
	}
	
	public ArrayList getFieldNames() {
		return specFieldNames;
	}
	
	public ArrayList getRelatedFieldLabels() {
		return relatedSpecFieldLabels;
	}
	
	public ArrayList getRelatedFieldNames() {
		return relatedSpecFieldNames;
	}
////////////////////////////////////////////////////////////////////////////////////

	private class ImportProcess extends OfflineProcess {
		
		StringBuffer status = new StringBuffer();
		String email = null;
		String userid = null;
		Connection connection = null;
		boolean streamline = false;
		String importID = null;
		OfflineImport theImportFinaliser = null;
		
		public ImportProcess(HttpServletRequest request, ServletContext context, boolean streamline) {
			super(request, context);
    		HttpSession session = request.getSession();
    		UserBean user = (UserBean)session.getAttribute(SessionKeys.CURRENT_USER);
    		this.userid = user.getCurrentUserID();
    		this.email = user.getEmail();
    		this.streamline = streamline;
    		importID = KeyMaker.generate();
    		
    		theImportFinaliser = new OfflineImport(request, context, false, importID);
    		start();
		}
	
   	public boolean sendEmail() {
   	   return !streamline;
   	}
		
		public Connection getConnection() {
			try {
				if (connection == null || connection.isClosed()) {
					Context ctx = new InitialContext();
				
					DataSource ds = null;
					try {
						dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
						Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
						ds = (DataSource)envContext.lookup(dbconn);
					}
					catch(Exception e) {}
					if(ds == null)
					{
						ds = (DataSource)ctx.lookup(dbconn);
					}
					try {
						ctx.close();
						ctx = null;
					}
					catch(Exception e) { importLog.append(ActionBean.writeStackTraceToString(e)); }
					connection = ds.getConnection();
				}
			}
			catch(Exception e) { importLog.append(ActionBean.writeStackTraceToString(e)); }
			return connection;
		}
		
		public void execute() {
			try {
				DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, getLocale());
					dt.setTimeZone(TimeZone.getTimeZone("Australia/Melbourne"));
				this.status.append(context.getServletContextName());
				this.status.append(" Runway offline process - ");
				this.status.append(tableSpec.getName());
				if (relatedTableSpec != null) {
					this.status.append(" and ");
					this.status.append(relatedTableSpec.getName());
				}
				this.status.append(" Data Import\r\n");
				this.status.append("Initiated ");
				this.status.append(dt.format(new java.util.Date()));
				this.status.append("\r\n");
				
				connection = getConnection();
				
				processFile(importID);
				
				status.append("Data Import processed ");
				status.append(numRecords);
				status.append(" records.\r\n");
				if (!testMode) {
					status.append("Successfully imported ");
				}
				else {
					status.append("Successfully tested ");
				}
				status.append(numSuccess);
				status.append(" records");
				if (numDataErrors > 0 || numRecordErrors > 0) {
					status.append(" (with errors - see below for details)");
				}
				status.append(".\r\n");
				
				if ("insert".equals(mode) && !streamline) {
					detectConflicts(importID);
				}
				setImportFlag("ImportCompanies", importID);
				setImportFlag("ImportContacts", importID);
				setImportFlag("ImportProducts", importID);
				
				
				if (isKilled()) {
					status.append("Offline process terminated by - ");
					status.append(getKiller());
					status.append("\r\n\r\n");
				}
				else {		
					status.append("Offline process completed.\r\n\r\n");
				}
				status.append(importLog);
				connection.close();
			} 
			catch (Exception e) {
				this.status.append(ActionBean.writeStackTraceToString(e));
			}
			finally {
				if (connection != null) {
					try { connection.close(); } catch (Exception e) {}
				}
				if (streamline && theImportFinaliser != null) {
			      theImportFinaliser.start();
				}
			}
		}
		
		// Implemented Abstract Methods
		public String getStatusMailBody() {
			return status.toString();
		}
	
		public String getStatusMailSubject() {
			return(context.getServletContextName() + " Runway offline process - " + tableSpec.getName() + " Data Import");
		}
	
		public String getStatusMailRecipient() {
			return(email);
		}
		
		public int getProcessSize() {
			return -1;
		}
		
		public int getProcessedCount() {
			return numRecords;
		}
		
		public int getErrorCount() {
			return numRecordErrors;
		}
	}
	
	
////////////////////////////////////////////////////////////////////////////////////
	// Testing puposes only
	public static void main(String[] args) {
		
		File file = new File("C:\\Documents and Settings\\mitch\\Desktop\\adrianne2.CSV");
		DataImporter i = new DataImporter(file, true, null);
		
		
		
		ArrayList a = i.getColumnHeadings();
		ArrayList b = i.getFirstRecord();
		System.out.println("#################################");
		for (int j =0; j < a.size(); j++) {
			System.out.println(a.get(j));
		}
		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		for (int j =0; j < b.size(); j++) {
			System.out.println(b.get(j));
		}
		
		i.parseFile();
	}
	
}
