package com.sok.runway.footytipping;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import com.sok.framework.*;

public class FootyBouncerFilter implements Filter
{
	static String logout = "logout";
	String loginpage = "/login.jsp";
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws ServletException, IOException
	{
		HttpServletRequest httprequest = (HttpServletRequest)request;
		
		if(!response.isCommitted()) {
		   
			HttpSession session = httprequest.getSession(false);
			if(session!=null) { 
			   
				Entrant tipster = (Entrant)session.getAttribute("tipster");
				if(tipster!=null) {
					chain.doFilter(request,response);
				}
				else {
					bounce(request ,response);
				}
			}
			else {
				bounce(request, response);
			}
		}
		else {
			chain.doFilter(request,response);
		}
	}
	
	public void bounce(ServletRequest request, ServletResponse response) throws IOException {
		StringBuffer bounceurl = new StringBuffer();
		bounceurl.append(((HttpServletRequest)request).getContextPath());
		bounceurl.append(loginpage);
			
		((HttpServletResponse)response).sendRedirect(bounceurl.toString());
	}
	
	public void init(FilterConfig config)throws ServletException {
		String page = config.getInitParameter("loginPage");
		if(page!=null && page.length()!=0) {
			loginpage = page;
		}
	}
	
	public void destroy() {
	}
}
