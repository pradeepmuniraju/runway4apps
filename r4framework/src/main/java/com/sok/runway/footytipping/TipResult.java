package com.sok.runway.footytipping;

import com.sok.runway.*;
import com.sok.framework.*;

import java.sql.*;
import java.util.*;

public class TipResult extends RunwayEntity {
   
   public TipResult(Connection con) {
      super(con, "footy/FootyTipResultView");
   }
   
   public TipResult(Connection con, String tipResultID) {
      this(con);
      load(tipResultID);
   }
   
   public static Collection getLadder(Connection con, String season) {
      ArrayList ids = new ArrayList();
      
      StringBuffer sql = new StringBuffer();
      sql.append("SELECT FootyEntrants.FootyEntrantID FROM FootyEntrants WHERE Season = '");
      sql.append(season);
      sql.append("' ORDER BY (SELECT SUM(Score) FROM FootyTipResults WHERE FootyTipResults.FootyEntrantID = FootyEntrants.FootyEntrantID) DESC;");
      
      //String ladderSelect = " GROUP BY FootyEntrantID ORDER BY SUM(Score) DESC;";
      
      RowSetBean ladder = new RowSetBean();
      ladder.setViewSpec("footy/FootyMatchView");
      ladder.setConnection(con);
      ladder.setSearchStatement(sql.toString());
      ladder.getResults();
      
      if (ladder.getError().length() > 0) {
         throw new RuntimeException(ladder.getError());
      }
      
      while (ladder.getNext()) {
         ids.add(ladder.getString("FootyEntrantID"));
      }
      ladder.close();
      return ids;
   }
   
   
   public static String getRoundAverage(Connection con, int roundNo, String season) {
      RowSetBean bean = new RowSetBean();
      bean.setViewSpec("footy/FootyTipResultView");
      bean.setConnection(con);
      
      StringBuffer sql = new StringBuffer();
      sql.append("SELECT AVG(Score) AS Score FROM FootyTipResults ");
      sql.append("INNER JOIN FootyEntrants ON FootyEntrants.FootyEntrantID = FootyTipResults.FootyEntrantID ");
      sql.append("WHERE FootyTipResults.RoundNo = ");
      sql.append(roundNo);
      sql.append(" AND FootyEntrants.Season = '");
      sql.append(season);
      sql.append("';");
      
      
      //bean.setSearchStatement("SELECT AVG(Score) AS Score FROM FootyTipResults WHERE RoundNo = " + roundNo);
      bean.setSearchStatement(sql.toString());
      bean.getResults();
      bean.getNext();
      return bean.getColumn("Score");
   }
   
   public static String getPostion(Connection con, String score, String season) {
      RowSetBean bean = new RowSetBean();
      bean.setViewSpec("footy/FootyEntrantView");
      bean.setConnection(con);
      
      StringBuffer sql = new StringBuffer();
      sql.append("SELECT COUNT(*) + 1 AS Score FROM (SELECT FootyEntrants.FootyEntrantID, SUM(Score) AS X FROM FootyTipResults ");
                        sql.append("INNER JOIN FootyEntrants ON FootyEntrants.FootyEntrantID = FootyTipResults.FootyEntrantID ");
                        sql.append("WHERE FootyEntrants.Season = '");
                        sql.append(season);
                        sql.append("' GROUP BY FootyEntrants.FootyEntrantID HAVING (SUM(Score) > ");
                        sql.append(score);
                        sql.append(")) DERIVEDTBL;");
      
      //String sql = "SELECT COUNT(*) + 1 AS Score FROM (SELECT ContactID, SUM(Score) AS X FROM FootyTipResults GROUP BY ContactID HAVING (SUM(Score) > " + score + ")) DERIVEDTBL";
      bean.setSearchStatement(sql.toString());
      bean.getResults();
      bean.getNext();
      return bean.getColumn("Score");
   }
   
   public static Collection getRoundLadder(Connection con, int roundNo, String season) {
      ArrayList ids = new ArrayList();
      
      
      StringBuffer sql = new StringBuffer();
      sql.append("SELECT FootyEntrants.FootyEntrantID FROM FootyTipResults ");
      sql.append("INNER JOIN FootyEntrants ON FootyEntrants.FootyEntrantID = FootyTipResults.FootyEntrantID ");
      sql.append("WHERE FootyTipResults.RoundNo = ");
      sql.append(roundNo);
      sql.append(" AND FootyEntrants.Season = '");
      sql.append(season);
      sql.append("' ORDER BY Score DESC;");
      
      //String ladderSelect = "SELECT ContactID FROM FootyTipResults WHERE RoundNo = " + roundNo + " ORDER BY Score DESC;";
      
      RowSetBean ladder = new RowSetBean();
      ladder.setViewSpec("footy/FootyMatchView");
      ladder.setConnection(con);
      ladder.setSearchStatement(sql.toString());
      ladder.getResults();
      
      if (ladder.getError().length() > 0) {
         throw new RuntimeException(ladder.getError());
      }
      
      while (ladder.getNext()) {
         ids.add(ladder.getString("FootyEntrantID"));
      }
      ladder.close();
      return ids;
   }
   
   public String getFootyTipResultID() {
      return getField("FootyTipResultID");
   }
   
   /*public String getContactID() {
      return getField("ContactID");
   }*/
   
   public String getRoundNo() {
      return getField("RoundNo");
   }
   
   public String getScore() {
      return getField("Score");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("FootyTipResult");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("FootyTipResultID","SYSTEM ERROR - FootyTipResultID");

         Validator.addValidator("FootyTipResult", val);
      }
      return val;
   }
}