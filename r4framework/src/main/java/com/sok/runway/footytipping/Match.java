package com.sok.runway.footytipping;

import com.sok.runway.*;
import com.sok.framework.*;
import com.sok.framework.generation.*;

import java.sql.*;
import java.util.*;

public class Match extends RunwayEntity {
   
   public static final String DRAW = "Draw";
   
   public Match(Connection con) {
      super(con, "footy/FootyMatchView");
   }
   
   public Match(Connection con, String matchID) {
      this(con);
      load(matchID);
   }
   
   public static int getCurrentRound(Connection con, String season) {      
      
      //StringBuffer sql = new StringBuffer();
      //sql.append("SELECT TOP 1 RoundNo FROM FootyMatches WHERE MatchDate > GETDATE() AND Season = '");
      //sql.append(season);
      //sql.append("' ORDER BY RoundNo;");
      
      GenRow rounds = new GenRow();
      rounds.setTableSpec("footy/FootyMatches");
      rounds.setConnection(con);
      rounds.setParameter("-select#1","RoundNo");
      rounds.setParameter("MatchDate",">NOW");
      rounds.setParameter("Season",season);
      rounds.setParameter("-top","1");
      rounds.setParameter("-sort1","RoundNo");
      
      rounds.doAction(GenerationKeys.SELECTFIRST);
      
      if (rounds.isSuccessful()) {
      	return rounds.getInt("RoundNo");
      }
      else {
      	return 23;
      }
      //String roundSelect = "SELECT TOP 1 RoundNo FROM FootyMatches WHERE MatchDate > GETDATE() ORDER BY RoundNo;";
      
      /*RowSetBean rounds = new RowSetBean();
      rounds.setViewSpec("footy/FootyMatchView");
      rounds.setConnection(con);
      rounds.setSearchStatement(sql.toString());
      rounds.getResults();
      
      if (rounds.getError().length() > 0) {
         throw new RuntimeException(rounds.getError());
      }
      
      if (rounds.getNext()) {
         try {
            return Integer.parseInt(rounds.getString("RoundNo"));
         }
         catch (Exception e) {
            return 22;
         }
      }
      else {
         return 22;
      }*/
   }
   
   public Collection getMatchesForRound(String round, String season) {
      ArrayList ids = new ArrayList();
      
      GenRow matches = new GenRow();
      matches.setTableSpec("footy/FootyMatches");
      matches.setConnection(getConnection());
      matches.setParameter("-select#1","FootyMatchID");
      matches.setParameter("RoundNo",round);
      matches.setParameter("Season",season);
      matches.setParameter("-sort1","MatchDate");
      matches.doAction(GenerationKeys.SEARCH);
      
      matches.getResults();
      
      while (matches.getNext()) {
         ids.add(matches.getData("FootyMatchID"));
      }
      matches.close();
      return ids;
      
      
      /*StringBuffer sql = new StringBuffer();
      sql.append("SELECT FootyMatchID FROM FootyMatches WHERE RoundNo = ");
      sql.append(round);
      sql.append(" AND Season = '");
      sql.append(season);
      sql.append("' ORDER BY MatchDate;");
      
      
      //String matchSelect = "SELECT FootyMatchID FROM FootyMatches Where RoundNo = " + round + " ORDER BY MatchDate;";
      
      RowSetBean matches = new RowSetBean();
      matches.setViewSpec("footy/FootyMatchView");
      matches.setConnection(getConnection());
      matches.setSearchStatement(sql.toString());
      matches.getResults();
      
      if (matches.getError().length() > 0) {
         throw new RuntimeException(matches.getError());
      }
      
      while (matches.getNext()) {
         ids.add(matches.getString("FootyMatchID"));
      }
      matches.close();
      return ids;*/
   }
   
   public static Collection getRounds(Connection con, String season) {
      ArrayList roundNos = new ArrayList();
      
      
      StringBuffer sql = new StringBuffer();
      sql.append("SELECT DISTINCT RoundNo FROM FootyMatches WHERE Season = '");
      sql.append(season);
      sql.append("' ORDER BY RoundNo;");
      
      //String roundSelect = "SELECT DISTINCT RoundNo FROM FootyMatches ORDER BY RoundNo;";
      
      RowSetBean rounds = new RowSetBean();
      rounds.setViewSpec("footy/FootyMatchView");
      rounds.setConnection(con);
      rounds.setSearchStatement(sql.toString());
      rounds.getResults();
      
      if (rounds.getError().length() > 0) {
         throw new RuntimeException(rounds.getError());
      }
      
      while (rounds.getNext()) {
         roundNos.add(rounds.getString("RoundNo"));
      }
      rounds.close();
      return roundNos;
   }
   
   public String getFootyMatchID() {
      return getField("FootyMatchID");
   }
   
   public String getSeason() {
      return getField("Season");
   }
   
   public String getRoundNo() {
      return getField("RoundNo");
   }
   
   public String getHomeTeam() {
      return getField("HomeTeam");
   }
   
   public String getAwayTeam() {
      return getField("AwayTeam");
   }
   
   public String getMatchDateDisplay() {
      return getField("MatchDate");
   }
   
   public java.util.Date getMatchDate() {
      return (java.util.Date)getObject("MatchDate");
   }
   
   public String getGround() {
      return getField("Ground");
   }
   
   public String getType() {
      return getField("Type");
   }
   
   public String getWinningTeam() {
      return getField("WinningTeam");
   }
   
   public Tip getTipForMatch(String footyEntrantID) {
      Tip tip = new Tip(getConnection());
      
      if (footyEntrantID.length() > 0 && getFootyMatchID().length() > 0) {
         RowBean tipBean = new RowBean();
         tipBean.setConnection(getConnection());
         tipBean.setViewSpec("footy/FootyTipView");
         tipBean.setColumn("FootyEntrantID", footyEntrantID);
         tipBean.setColumn("FootyMatchID", getFootyMatchID());
         tipBean.retrieveData();
         
         if (tipBean.getError().length() > 0) {
            throw new RuntimeException(tipBean.getError());
         }
      
         if (tipBean.getString("FootyTipID").length() > 0) {
            tip.load(tipBean.getString("FootyTipID"));
         }
      }
      return tip;
   }
      
   public boolean hasMatchBeenPlayed() {
      java.util.Date now = new java.util.Date(); 
      int compare = now.compareTo(getMatchDate());
      
      return compare >= 0;
   }
   
   public boolean isAwayTeamTipped(String tippedTeam) {
      return getAwayTeam().equals(tippedTeam);
   }
   
   public boolean isHomeTeamTipped(String tippedTeam) {
      return getHomeTeam().equals(tippedTeam);
   }
   
   public boolean isDrawTipped(String tippedTeam) {
      return DRAW.equals(tippedTeam);
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("FootyMatch");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("FootyMatchID","SYSTEM ERROR - FootyMatchID");

         Validator.addValidator("FootyMatch", val);
      }
      return val;
   }
}