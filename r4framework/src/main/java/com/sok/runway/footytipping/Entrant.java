package com.sok.runway.footytipping;

import com.sok.runway.*;
import com.sok.framework.*;

import java.sql.*;
import java.util.*;

public class Entrant extends RunwayEntity {
   
   String lastWeekScore = "0";
   Map roundScores = null;
   
   String season = null;
   
   public Entrant(Connection con, String season) {
      super(con, "footy/FootyEntrantView");
      this.season = season;
   }
   
   public Entrant(Connection con, String season, String entrantID) {
      this(con, season);
      load(entrantID);
   }
      
   public boolean login(String handle, String password) {
      
    	String[] names = {"Handle","Season"};
    	String[] values = {handle, season};
      
      loadFromField(names, values);
      return getPassword().equals(password) && 
            getHandle().equalsIgnoreCase(handle) && 
            getPrimaryKey().length() > 0;
   }
   
   public String getFootyEntrantID() {
      return getField("FootyEntrantID");
   }
   
   public String getSeason() {
      return season;
   }
   
   public String getContactID() {
      return getField("ContactID");
   }
   
   public String getNoteID() {
      return getField("NoteID");
   }
   
   public String getFirstName() {
      return getField("FirstName");
   }
   
   public String getLastName() {
      return getField("LastName");
   }
   
   public String getCompany() {
      return getField("Company");
   }
   
   public String getEmail() {
      return getField("Email");
   }
   
   public String getHandle() {
      return getField("Handle");
   }
   
   public String getPassword() {
      return getField("Password");
   }
   
   public String getFavTeam() {
      return getField("FavTeam");
   }
   
   public String getTotalScore() {
      return getField("TotalScore");
   }
   
   public String getLastWeekScore() {
	   calculateLastWeekScore();
      return lastWeekScore;
   }
   
   public String getScoreForRound(int roundNo) {
	   calculateLastWeekScore();
		  
      String score = (String)roundScores.get(String.valueOf(roundNo));
      if (score == null) {
         return "0";
      }
      else {
         return score;
      }
   }
   
   
   public void clear() {
      super.clear();
      if (roundScores != null) {
    	  roundScores.clear();
      }
   }
   
   public void postLoad() {
      super.postLoad();
      calculateLastWeekScore();
   }
   
   private void calculateLastWeekScore() {
      if (roundScores == null) {
		   roundScores = new HashMap();
	   }
      
      if (roundScores.isEmpty()) {
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT RoundNo, Score AS TotalScore FROM FootyTipResults WHERE FootyEntrantID = '");
         sql.append(getFootyEntrantID());
         sql.append("' ORDER BY RoundNo;");
         
         RowSetBean bean = new RowSetBean();
         bean.setViewSpec("footy/FootyEntrantView");
         bean.setConnection(getConnection());
         //bean.setSearchStatement("SELECT RoundNo, Score as TotalScore FROM FootyTipResults WHERE ContactID = '" + getContactID() + "' ORDER By RoundNo");
         bean.setSearchStatement(sql.toString());
         bean.getResults();
         
         while (bean.getNext()) {
            roundScores.put(bean.getString("RoundNo"), bean.getColumn("TotalScore"));
            
            lastWeekScore = bean.getColumn("TotalScore");
         }
      }
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("FootyEntrant");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("FootyEntrantID","SYSTEM ERROR - FootyEntrantID");
         val.addMandatoryField("ContactID","SYSTEM ERROR - ContactID");
         val.addMandatoryField("Email","Please specify an email address.");
         val.addMandatoryField("Handle","Please specify a unique handle.");
         val.addMandatoryField("Password","Please specify a password.");

         Validator.addValidator("FootyEntrant", val);
      }
      return val;
   }
}