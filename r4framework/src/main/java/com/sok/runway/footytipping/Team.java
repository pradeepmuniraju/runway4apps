package com.sok.runway.footytipping;

import com.sok.runway.*;
import com.sok.framework.*;

import java.sql.*;
import java.util.*;

public class Team extends RunwayEntity {
   
   public Team(Connection con) {
      super(con, "footy/FootyTeamView");
   }
   
   public Team(Connection con, String team) {
      this(con);
      load(team);
   }
   
   public static Collection getTeams(Connection con) {
      ArrayList teams = new ArrayList();
      
      String teamSelect = "SELECT FootyTeam FROM FootyTeams ORDER BY FootyTeam;";
      
      RowSetBean teamsBean = new RowSetBean();
      teamsBean.setViewSpec("footy/FootyTeamView");
      teamsBean.setConnection(con);
      teamsBean.setSearchStatement(teamSelect);
      teamsBean.getResults();
      
      if (teamsBean.getError().length() > 0) {
         throw new RuntimeException(teamsBean.getError());
      }
      
      while (teamsBean.getNext()) {
         teams.add(teamsBean.getString("FootyTeam"));
      }
      teamsBean.close();
      return teams;
   }
   
   public String getFootyTeamID() {
      return getField("FootyTeam");
   }
   
   public String getLadderPosition() {
      return getField("LadderPosition");
   }
   
   public String getWebsiteURL() {
      return getField("WebsiteURL");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("FootyTeam");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("FootyTeam","SYSTEM ERROR - FootyTeam");

         Validator.addValidator("FootyTeam", val);
      }
      return val;
   }
}