package com.sok.runway.footytipping;

import com.sok.runway.*;
import com.sok.framework.*;

import java.sql.*;
import java.util.*;

public class Tip extends RunwayEntity {
   
   public Tip(Connection con) {
      super(con, "footy/FootyTipView");
   }
   
   public Tip(Connection con, String tipID) {
      this(con);
      load(tipID);
   }
   
   public String getFootyTipID() {
      return getField("FootyTipID");
   }
   
   public String getFootyEntrantID() {
      return getField("FootyEntrantID");
   }
   
   /*public String getContactID() {
      return getField("ContactID");
   }*/
   
   public String getFootyMatchID() {
      return getField("FootyMatchID");
   }
   
   public String getTippedTeam() {
      return getField("TippedTeam");
   }
   
   public String getDateEntered() {
      return getField("DateEntered");
   }
   
   public Validator getValidator() {
      Validator val = Validator.getValidator("FootyTip");
      if (val == null) {
         val = new Validator();
         val.addMandatoryField("FootyTipID","SYSTEM ERROR - FootyTipID");

         Validator.addValidator("FootyTip", val);
      }
      return val;
   }
}