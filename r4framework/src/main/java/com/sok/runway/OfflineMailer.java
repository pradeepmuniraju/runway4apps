package com.sok.runway;

import javax.servlet.*;
import javax.servlet.http.*;

import java.util.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import java.text.*;
import java.io.*;
import javax.mail.*;
import javax.mail.internet.*;
import com.sok.framework.*;
import com.sok.runway.email.*;
import org.apache.velocity.app.*;

public class OfflineMailer extends OfflineProcess
{
	static final String SENT = "Sent";
	public static final String contactlist = "-contactList";
	StringBuffer status = null;

	private String smtpHost = null;
	private String smtpUsername = null;
	private String smtpPassword = null;
	private String smtpPort = null;


	RowBean contactselect = null;
	RowBean notesave = null;
	RowBean formbean = null;
	RowBean newletterdata = null;
	TemplateURLs urllinks = null;
	TemplateURLs cmsurllinks = null;

	String ContactLeftCompanyStatusID = ""; 
	String ContactOptOutStatusID = ""; 

	ArrayList contacts = null;

	Connection con = null;
	Context ctx = null;
	Transport transport = null;
	javax.mail.Session msession = null;
	VelocityEngine ve = null;

	int total = 0;
	int sent = 0;
	int failed = 0;

	int checkcount = -1;
	boolean newsletter = false;

	UserBean user = null;
	TableData contactsearch = null;	

	public OfflineMailer(HttpServletRequest request, ServletContext context)
	{
		super(request, context);

		status = new StringBuffer();
		contacts = new ArrayList(500);
		HttpSession session = request.getSession();
		user = (UserBean) session.getAttribute(SessionKeys.CURRENT_USER);
		contactsearch = (TableData) session.getAttribute(SessionKeys.CONTACT_SEARCH);

		if (contactsearch != null){
			contactsearch.setViewSpec("OfflineContactView");
			// we need to remove all group by
			for (int x = 0; x < 10; ++x) {
				contactsearch.remove("-groupby" + x);
				contactsearch.remove("-sort" + x);
				contactsearch.remove("-order" + x);
			}
			contactsearch.setParameter("-groupby0", "ContactID");
		}

		java.util.Properties sysprops = InitServlet.getSystemParams();

		smtpHost = sysprops.getProperty("SMTPHost");
		smtpPort = sysprops.getProperty("SMTPPort");
		smtpUsername = sysprops.getProperty("SMTPUsername");
		smtpPassword = sysprops.getProperty("SMTPPassword");

		start();		
	}


	void retrieveContactsFromList()
	{
		StringTokenizer st = new StringTokenizer(requestbean.getString(contactlist),"+");
		while(st.hasMoreTokens())
		{
			contacts.add(st.nextToken());
		}
		total = contacts.size();
	}

	void retrieveContactsFromSearch() throws Exception
	{	
		if(debug)
		{		  
			errors.append(contactsearch.getSearchStatement());
		}			
		try
		{
			getConnection();
			contactsearch.setConnection(con);
			RowSetWrapperBean list = new RowSetWrapperBean();		
			list.setConnection(con);
			list.setSearchBean(contactsearch);
			list.getResults();

			TableData resultset = list.getResultBean();
			while (list.getNext()) {
				contacts.add(resultset.getString("ContactID"));
			}		   			  
			total = contacts.size();		      			
		}		
		finally
		{    		
			try{
				con.close();
			}catch (Exception ex){}    		  		    
		}
	}

	void retrieveContacts() throws Exception
	{
		if(requestbean.getString(contactlist).length()==0)
		{
			retrieveContactsFromSearch();
		}
		else
		{
			retrieveContactsFromList();
		}
	}

	void sendEmails() throws Exception
	{
		try
		{
			Properties props = System.getProperties();


			ContactLeftCompanyStatusID = props.getProperty("ContactLeftCompanyStatusID"); 
			ContactOptOutStatusID = props.getProperty("ContactOptOutStatusID"); 

			String username = getSMTPUsername();
			String password = getSMTPPassword();
			String port = getSMTPPort();    
			props.put("mail.smtp.host", getSMTPHost());
			if(username != null && username.length()!=0 && password!=null && password.length()!=0)
			{
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.user", username);
				props.put("mail.smtp.password", password);     
			}          
			if(port != null && port.length()!=0)
			{
				props.put("mail.smtp.port", port );
			}            
			msession = javax.mail.Session.getDefaultInstance(props, null);
			msession.setDebug(false);
			transport = msession.getTransport("smtp");
			transport.connect(getSMTPHost(), username, password);
			ve = VelocityManager.getEngine();

			int success = 0;
			int reconnectAttempt = 0;
			int currentsent = 0;
			int currentfailed = 0;
			for(int i = 0; i < total && !isKilled(); i++)
			{
				if(reconnectAttempt > 10)
				{
					errors.append("\r\nAborting offline send at ");
					errors.append(String.valueOf(i));
					errors.append(", due to too many consecutive failed email server connections.\r\n");
					break;
				}
				if(!transport.isConnected())
				{
					reconnectAttempt = reconnectAttempt + 1;
					errors.append("\r\nReconnecting to email server (Attempt:");
					errors.append(String.valueOf(reconnectAttempt));
					errors.append(", current run S:");
					errors.append(String.valueOf(currentsent));
					errors.append(" F:");
					errors.append(String.valueOf(currentfailed));
					errors.append(").\r\n\r\n");

					transport.connect(getSMTPHost(), getSMTPUsername(), getSMTPPassword());
					currentsent = 0;
					currentfailed = 0;					
				}
				else
				{
					reconnectAttempt = 0;
				}

				success = sendEmail(newsletter, (String) contacts.get(i));

				if(success == 0)
				{
					sent++;
					currentsent++;
				}
				else
				{
					failed++;
					currentfailed++;
				}
			}
		}
		finally
		{
			if(transport != null)
			{
				try
				{
					transport.close();
					transport = null;
				}
				catch(Exception te)
				{
				}
			}
		}
	}

	protected int sendEmail(boolean newsletter, String contactid)
	{
		int success = 0;

		TableData contact = getContact(contactselect, contactid, this);
		if(contact == null)
		{
			appendError("Contact not found", contactid);
			return (1);
		}
		try {
			setUserEmailTokens(contactselect, user, formbean);

			EmailSender.addProfileTokens(contact, con);
			EmailSender.addNoteTokens(contact, con);
			EmailSender.addSetupTokens(contactselect.getString("RepUserID"),contact, con);

			if (formbean.getString("ProductID").length() > 0) {
				EmailSender.addProductTokens(formbean.getString("ProductID"),contact, con);
			}
			if (formbean.getString("ProductIDs").length() > 0) {
				EmailSender.addProductListTokens(formbean.getString("ProductIDs"),contact, con);
			}
			if (formbean.getString("OpportunityID").length() > 0) {
				EmailSender.addOpportunityTokens(formbean.getString("OpportunityID"),contact, con);
			}

			if (formbean.getString("RewardID").length() > 0 ) {
				EmailSender.addRewardTokens(formbean.getString("RewardID"), contact, con);
			}
		}
		catch (Exception e) {
			appendError(e.toString(), contactid);
		}
		String email = (String) contact.get("Email");

		String contactstatus = (String) contact.get("CurrentStatus");
		String currentstatusid = (String) contact.get("CurrentStatusID"); 

		if(contactstatus != null && (contactstatus.equals("OptOut") || currentstatusid.equals(ContactLeftCompanyStatusID)))
		{
			appendError("Opt-out contact", contactid);
			return (1);
		}
		if(contactstatus != null && (contactstatus.equals("Left Company") || currentstatusid.equals(ContactOptOutStatusID)))
		{
			appendError("Contact Left Company", contactid);
			return (1);
		}
		String contactstatusid = (String) contact.get("CurrentStatusID");

		if(contactstatusid != null
				&& contactstatusid.equals(InitServlet.getSystemParams().getProperty("ContactOptOutStatusID")))
		{
			appendError("Opt-out contact", contactid);
			return (1);
		}
		if(contactstatusid != null
				&& contactstatusid.equals(InitServlet.getSystemParams().getProperty("ContactLeftCompanyStatusID")))
		{
			appendError("Contact Left Company", contactid);
			return (1);
		}

		if(email == null || email.length() == 0)
		{
			appendError("Empty email address", contactid);
			return (1);
		}
		InternetAddress[] addresses = null;
		try
		{
			addresses = InternetAddress.parse(email, false);
		}
		catch(Exception ex)
		{
			appendError("Invalid email address", contactid);
			return (1);
		}

		String emailfrom = formbean.getString("from");
		String emailNotifyTo = formbean.getString("notifyTo");
		String emailReplyTo = getReplyToAddress(formbean.getString("replyTo"),contact,user,null); 
		String from = getFromAddress(emailfrom, contact, user, null);

		if(from == null || from.length() == 0)
		{
			appendError("Invalid from email address", contactid);
			return (1);
		}

		InternetAddress fromAddress = null;
		try
		{
			fromAddress = new InternetAddress(from);
		}
		catch(Exception ex)
		{
			appendError("Invalid from email address", contactid);
			return (1);
		}

		InternetAddress[] replyTo = null;
		if(emailReplyTo!=null && emailReplyTo.length()>0) { 
			try
			{
				replyTo = InternetAddress.parse(emailReplyTo, false);
				contact.setColumn("EmailReplyTo",emailReplyTo); 
			}
			catch(Exception ex)
			{
				appendError("Invalid replyTo address", contactid);
				return (1);
			}
		} 

		if(newsletter)
		{
			success = sendNewsLetter(contact, from, fromAddress, email, addresses, emailNotifyTo, replyTo);
		}
		else
		{
			success = sendEmail(contact, from, fromAddress, email, addresses, emailNotifyTo, replyTo);
		}
		return(success);
	}

	public static TableData getContact(TableData contactselect, String contactid, OfflineProcess process)
	{
		contactselect.clear();
		contactselect.setColumn("ContactID", contactid);

		if(process.isDebug())
		{
			process.appendError(contactselect.createdDataOnIDStatment().toString());

		}

		contactselect.doAction();

		if(process.isDebug())
		{
			if(contactselect.getError().length() != 0)
			{
				process.appendError(contactselect.getError(), contactid);
			}
		}

		return (contactselect);
	}

	public static void setUserEmailTokens(TableData contact, TableData user, RowBean formbean)
	{
		if(formbean!=null)
		{
			String groupID = contact.getString("GroupID");
			contact.putAll(formbean);
			contact.put("GroupID", groupID);
		}

		TableData authorBean = null;
		if (formbean.getString("RepUserID").length() > 0) {
			authorBean = new TableBean();
			authorBean.setJndiName(ActionBean.getJndiName());
			authorBean.setViewSpec("UserView");
			authorBean.setColumn("UserID", formbean.getString("RepUserID"));
			authorBean.setAction(ActionBean.SELECT);
			authorBean.doAction();
		}
		else {
			authorBean = user;
		}
		EmailSender.addUserEmailTokens(contact, user, authorBean);

	}

	int sendNewsLetter(TableData contact, String from, InternetAddress fromAddress, String email, InternetAddress[] addresses, 
			String notifyTo, InternetAddress[] replyTo)
	{
		String newnoteid = KeyMaker.generate();
		String emailstatus = SENT;
		String contactid = contact.getString("ContactID");

		contact.put("NoteID", newnoteid);
		contact.put("cms", newletterdata);
		contact.put("contextpath", context.getInitParameter("URLHome"));
		contact.put("EmailFrom", from);
		contact.put("EmailNotifyTo", notifyTo);

		contact.put("Date", new DateToken());

		StringWriter nlw = new StringWriter();
		StringWriter tnlw = new StringWriter();

		StringWriter htmlw = new StringWriter();
		StringWriter textw = new StringWriter();
		StringWriter sw = new StringWriter();

		String htmlbodyinput = formbean.getString("HTMLBody");
		if(htmlbodyinput.length()==0)
		{
			htmlbodyinput = formbean.getString("Body");
		}
		try
		{

			ve.evaluate(contact, sw, contactid, formbean.getString("Subject"));
			sw.flush();
			sw.close();
			String notesubject = sw.toString();

			urllinks.setHTMLLinks(contact);

			ve.evaluate(contact, htmlw, contactid, StringUtil.encodeHTMLLinefeeds(htmlbodyinput));
			htmlw.flush();
			htmlw.close();
			String htmlbody = htmlw.toString();

			urllinks.setTextLinks(contact);

			ve.evaluate(contact, textw, contactid, formbean.getString("Body"));
			textw.flush();
			textw.close();
			String textbody = textw.toString();

			StringBuffer roottemplate = new StringBuffer();
			roottemplate.append("#parse(\"");
			roottemplate.append(newletterdata.getString("EmailTemplate"));
			roottemplate.append(".vt\")");

			StringBuffer textroottemplate = new StringBuffer();
			textroottemplate.append("#parse(\"");
			textroottemplate.append(newletterdata.getString("EmailTemplate"));
			textroottemplate.append("_text.vt\")");

			StringWriter prenlw = new StringWriter();
			StringWriter pretnlw = new StringWriter();

			contact.put("Body", htmlbody);
			ve.evaluate(contact, prenlw, "NewsLetter", roottemplate.toString());
			prenlw.flush();
			prenlw.close();

			contact.put("Body", textbody);
			ve.evaluate(contact, pretnlw, "NewsLetter", textroottemplate.toString());
			pretnlw.flush();
			pretnlw.close();

			cmsurllinks.setHTMLLinks(contact);
			ve.evaluate(contact, nlw, "NewsLetter", prenlw.toString());
			nlw.flush();
			nlw.close();

			cmsurllinks.setTextLinks(contact);
			ve.evaluate(contact, tnlw, "NewsLetter", pretnlw.toString());
			tnlw.flush();
			tnlw.close();

			String noteletter = tnlw.toString();
			String htmlletter = nlw.toString();
			javax.mail.Message msg = new MimeMessage(msession);

			String emailType = (String) contact.get("EmailType");
			if(emailType == null || !emailType.equals("Plain Text"))
			{
				MimeBodyPart plainPart = new MimeBodyPart();
				MimeBodyPart htmlPart = new MimeBodyPart();
				plainPart.setText(noteletter);
				htmlPart.setContent(htmlletter, "text/html");
				Multipart alt = new MimeMultipart("alternative");
				alt.addBodyPart(plainPart);
				alt.addBodyPart(htmlPart);
				msg.setContent(alt);
			}
			else
			{
				msg.setText(noteletter);
			}

			msg.setRecipients(Message.RecipientType.TO, addresses);
			msg.setSubject(notesubject);
			msg.setFrom(fromAddress);

			if(replyTo!=null && replyTo.length>0) { 
				msg.setReplyTo(replyTo); 
			}

			msg.setHeader("X-Mailer", "SOKMailer");
			msg.setSentDate(new java.util.Date());
			msg.saveChanges();

			if(!debug)
			{
				transport.sendMessage(msg, addresses);
			}
			else
			{
				emailstatus = "Testing";
				errors.append(email);
				errors.append("\r\n");
			}

			saveNote(contact, newnoteid, noteletter, htmlletter, notesubject,
					emailstatus);
			return (0);
		}
		catch(Exception e)
		{
			appendError(e.toString(), contactid);
			return (1);
		}
	}

	public static String getFromAddress(String emailfrom, TableData contact, TableData user,
			TableData note){
		return(AbstractEmailer.getFromAddress(emailfrom, contact, user, note));
	}

	public static String getToAddress(String emailTo, TableData contact, TableData user, TableData note) {
		return(AbstractEmailer.getToAddress(emailTo, contact, user, note));
	}

	public static String getReplyToAddress(String replyTo, TableData contact, TableData user, TableData note) {
		return(AbstractEmailer.getReplyToAddress(replyTo, contact, user, note));
	}

	int sendEmail(TableData contact, String from, InternetAddress fromAddress, String email, InternetAddress[] addresses, 
			String notifyTo, InternetAddress[] replyTo)
	{
		String newnoteid = KeyMaker.generate();
		String emailstatus = SENT;
		String contactid = contact.getString("ContactID");
		contact.put("NoteID", newnoteid);
		contact.put("EmailFrom", from);
		contact.put("EmailNotifyTo", notifyTo);
		contact.put("Date", new DateToken());
		String htmlbodyinput = formbean.getString("HTMLBody");
		if(htmlbodyinput.length()==0)
		{
			htmlbodyinput = formbean.getString("Body");
		}


		StringWriter htmlw = new StringWriter();
		StringWriter textw = new StringWriter();
		StringWriter sw = new StringWriter();
		StringWriter hw = new StringWriter();

		try
		{
			ve.evaluate(contact, sw, contactid, formbean.getString("Subject"));
			sw.flush();
			sw.close();
			String notesubject = sw.toString();

			ve.evaluate(contact, hw, contactid, contact.getString("Headline"));
			hw.flush();
			hw.close();
			contact.setColumn("Headline", hw.toString());

			urllinks.setHTMLLinks(contact);

			ve.evaluate(contact, htmlw, contactid, formbean.getString("HTMLHeader"));
			ve.evaluate(contact, htmlw, contactid, StringUtil.encodeHTMLLinefeeds(htmlbodyinput));
			ve.evaluate(contact, htmlw, contactid, formbean.getString("HTMLFooter"));
			htmlw.flush();
			htmlw.close();
			String htmlbody = htmlw.toString();

			urllinks.setTextLinks(contact);
			ve.evaluate(contact, textw, contactid, formbean.getString("TextHeader"));
			ve.evaluate(contact, textw, contactid, formbean.getString("Body"));
			ve.evaluate(contact, textw, contactid, formbean.getString("TextFooter"));
			textw.flush();
			textw.close();
			String textbody = textw.toString();

			javax.mail.Message msg = new MimeMessage(msession);
			String emailType = (String) contact.get("EmailType");
			if(emailType == null || !emailType.equals("Plain Text"))
			{
				MimeBodyPart plainPart = new MimeBodyPart();
				MimeBodyPart htmlPart = new MimeBodyPart();
				plainPart.setText(textbody);
				htmlPart.setContent(htmlbody, "text/html");
				Multipart alt = new MimeMultipart("alternative");
				alt.addBodyPart(plainPart);
				alt.addBodyPart(htmlPart);
				msg.setContent(alt);
			}
			else
			{
				msg.setText(textbody);
			}

			msg.setRecipients(Message.RecipientType.TO, addresses);

			if(replyTo!=null && replyTo.length>0) { 
				msg.setReplyTo(replyTo); 
			}

			msg.setSubject(notesubject);
			msg.setFrom(fromAddress);
			msg.setHeader("X-Mailer", "SOKMailer");
			msg.setSentDate(new java.util.Date());
			msg.saveChanges();

			if(!debug)
			{
				transport.sendMessage(msg, addresses);
			}
			else
			{
				emailstatus = "Testing";
				errors.append(email);
				errors.append("\r\n");
			}

			saveNote(contact, newnoteid, textbody, htmlbody, notesubject, emailstatus);
			return (0);
		}
		catch(Exception e)
		{
			appendError(e.toString(), contactid);
			return (1);
		}
	}

	public void appendError(String msg, String contactid)
	{
		setLastError(msg);
		errors.append(msg);
		errors.append(" - ");
		errors.append(formbean.getString("URLHome"));
		errors.append("/crm/contactview.jsp?-action=select&ContactID=");
		errors.append(contactid);
		errors.append("\r\n");

	}

	void saveNote(TableData contact, String newnoteid, String notebody, String htmlbody,
			String notesubject, String status)
	{
		try
		{
			String userid = user.getUserID();

			notesave.clear();
			if(!con.isClosed())
			{
				notesave.setConnection(con);
			}
			else
			{
				notesave.setConnection((Connection)null);
				notesave.setCloseConnection(true);
			}
			notesave.setColumn("NoteID", newnoteid);
			notesave.setColumn("ContactID", contact.getString("ContactID"));
			notesave.setColumn("CompanyID", contact.getString("CompanyID"));
			notesave.setColumn("Type", "Email");
			notesave.setColumn("RepUserID", contact.getString("RepUserID"));
			notesave.setColumn("ModifiedBy", userid);
			notesave.setColumn("TemplateUsed", formbean.getString("TemplateID"));
			notesave.setColumn("CMSUsed", formbean.getString("CMSID"));
			notesave.setColumn("Campaign", formbean.getString("Campaign"));
			// This will be useful when we get rid of the Damn Campaign String
			notesave.setColumn("CampaignID", formbean.getString("CampaignID"));
			notesave.setColumn("OpportunityID", formbean.getString("OpportunityID"));
			notesave.setColumn("ProcessStageID", formbean.getString("ProcessStageID"));
			notesave.setColumn("Status", status);
			notesave.setColumn("Completed", "Yes");
			notesave.setColumn("Subject", notesubject);
			notesave.setColumn("Body", notebody);
			notesave.setColumn("HTMLBody", htmlbody);
			notesave.setColumn("CreatedBy", userid);
			notesave.setColumn("EmailTo", contact.getString("Email"));
			notesave.setColumn("EmailFrom", contact.getString("EmailFrom"));
			notesave.setColumn("EmailNotifyTo", contact.getString("EmailNotifyTo"));
			notesave.setColumn("EmailReplyTo", contact.getString("EmailReplyTo"));
			notesave.setColumn("MasterTemplateID", formbean.getString("MasterTemplateID"));

			if(status.equals(SENT))
			{
				notesave.setColumn("SentDate", "NOW");
			}
			notesave.setColumn("NoteDate", "NOW");
			notesave.setColumn("NoteTime", "NOW");
			notesave.setColumn("CreatedDate", "NOW");
			notesave.setColumn("ModifiedDate", "NOW");
			notesave.setColumn("GroupID", contact.getString("GroupID"));
			notesave.doAction();
			if(notesave.getUpdatedCount() == 0)
			{
				errors.append("Could not save note, ");
				errors.append(notesave.getError());
				errors.append(" - ");
				errors.append(contact.getString("ContactID"));
				errors.append(".\r\n");
			}
			if(debug)
			{
				if(notesave.getError().length() != 0)
				{
					appendError(notesave.getError(), contact.getString("ContactID"));
				}
			}
		}
		catch(Exception ex)
		{
			appendError("Could not save note, " + ex.toString(), contact.getString("ContactID"));
		}

	}

	void getConnection() throws Exception
	{
		if(con == null || con.isClosed())
		{
			try
			{
				if(ctx == null)
				{
					ctx = new InitialContext();
				}

				DataSource ds = null;
				try
				{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource) envContext.lookup(dbconn);
				}
				catch(Exception e)
				{
				}
				if(ds == null)
				{
					ds = (DataSource) ctx.lookup(dbconn);
				}
				con = ds.getConnection();
			}
			finally
			{
				try
				{
					ctx.close();
					ctx = null;
				}
				catch(Exception e)
				{
				}
			}
		}
	}

	public void execute()
	{
		con = null;
		ctx = null;
		try
		{

			if(user == null || (contactsearch == null && requestbean.getString(contactlist).length()==0))
			{
				status.append("Resources not found, offline process halted.\r\n");
				return;
			}

			setEmailHeader();

			if(requestbean.getString("-debug").length()!=0)
			{
				debug = true;
				status.append("Debug mode on.\r\n");
			}

			//RowBean requestbean = new RowBean();
			requestbean.setViewSpec("TemplateView");
			//requestbean.parseRequest(request);

			if(requestbean.getString("CMSID").length() != 0)
			{
				newsletter = true;
				newletterdata = new RowBean();
				newletterdata.setJndiName(dbconn);
				newletterdata.setViewSpec("PageView");
				newletterdata.setColumn("CMSID", requestbean.getString("CMSID"));
				newletterdata.setColumn("Active", "Active");
				newletterdata.setColumn("ParentID", "NULL+EMPTY");
				newletterdata.setLocale(thislocale);
				newletterdata.retrieveData();

				RowArrayBean newslist = new RowArrayBean();
				newslist.setJndiName(dbconn);
				newslist.setViewSpec("PageView");
				newslist.setLocale(thislocale);
				newslist.setColumn("ParentID", newletterdata.getColumn("PageID"));
				newslist.setColumn("Active","Active");
				newslist.sortBy("PageLinks.SortOrder", 0);
				newslist.getResults();

				RowArrayBean productlist = null;
				productlist = new RowArrayBean();
				productlist.setJndiName(dbconn);
				productlist.setViewSpec("PageProductView");
				productlist.setLocale(thislocale);
				productlist.setColumn("PageID", newletterdata.getString("PageID"));
				productlist.generateSQLStatment();
				productlist.getResults();
				newletterdata.put("products", productlist.getList());

				RowArrayBean newsitemlist = null;
				for(int i = 0; i < newslist.getSize(); i++)
				{
					newsitemlist = new RowArrayBean();
					newsitemlist.setJndiName(dbconn);
					newsitemlist.setViewSpec("PageView");
					newsitemlist.setLocale(thislocale);
					newsitemlist.setColumn("ParentID", newslist.get(i).getColumn("PageID"));
					newsitemlist.setColumn("Active","Active");
					newsitemlist.sortBy("PageLinks.SortOrder", 0);
					newsitemlist.getResults();
					newslist.get(i).put("newsitems", newsitemlist.getList());

					productlist = new RowArrayBean();
					productlist.setJndiName(dbconn);
					productlist.setViewSpec("PageProductView");
					productlist.setLocale(thislocale);
					productlist.setColumn("PageID", newslist.get(i).getColumn("PageID"));
					productlist.generateSQLStatment();
					productlist.getResults();
					newslist.get(i).put("products", productlist.getList());
				}

				newletterdata.put("news", newslist.getList());

				cmsurllinks = new TemplateURLs();
				cmsurllinks.getCMSLinks(requestbean.getString("CMSID"), context);

				newletterdata.put("urllinks", cmsurllinks);

			}

			if(requestbean.getString("TemplateID").length() != 0)
			{
				formbean = new RowBean();
				formbean.setViewSpec("TemplateView");
				formbean.put("URLHome", context.getInitParameter("URLHome"));
				formbean.put("contextPath", contextpath);
				formbean.setJndiName(super.dbconn);
				formbean.put("TemplateID", requestbean.getString("TemplateID"));
				formbean.setAction("select");
				formbean.doAction();
				formbean.put("URLHome", context.getInitParameter("URLHome"));
				formbean.put("contextPath", contextpath);
				formbean.putAll(requestbean);

				if(requestbean.getString("MasterTemplateID").length() != 0)
				{
					RowBean mastertemplate = new RowBean();
					mastertemplate.setJndiName(super.dbconn);
					mastertemplate.setViewSpec("MasterTemplateView");
					mastertemplate.setAction("select");
					mastertemplate.setColumn("MasterTemplateID", requestbean.getString("MasterTemplateID"));
					mastertemplate.doAction();
					formbean.putAll(mastertemplate);
				}
			}
			else
			{
				formbean = requestbean;
			}

			urllinks = new TemplateURLs();
			if(formbean.getColumn("TemplateID").length() != 0)
			{
				urllinks.getLinks(formbean.getColumn("TemplateID"),formbean.getColumn("MasterTemplateID"), context);
			}

			getConnection();

			if(formbean.getString("-checkcount").length() != 0)
			{
				checkcount = Integer.parseInt(formbean.getString("-checkcount"));
			}

			if(newsletter)
			{
				if(newletterdata.getString("PageID").length() == 0)
				{
					status.append("Template not found, offline process halted.\r\n");
					status.append(newletterdata.getError());
					return;
				}
			}
			else
			{
				if(formbean.getString("TemplateName").length() == 0)
				{
					status.append("Template not found, offline process halted.\r\n");
					status.append(formbean.getError());
					return;
				}
			}

			contactselect = new TableBean();
			contactselect.setJndiName(dbconn);
			contactselect.setLocale(thislocale);
			contactselect.setViewSpec("ContactEmailView");
			contactselect.setAction(ActionBean.SELECT);

			notesave = new TableBean();
			notesave.setJndiName(dbconn);
			notesave.setViewSpec("email/NoteEmailView");
			notesave.setAction(ActionBean.INSERT);

			retrieveContacts();

			if(checkcount != -1)
			{
				if(total != checkcount)
				{
					status.append("Search results count does not match, offline process halted.\r\n");
					status.append("Current - ");
					status.append(String.valueOf(total));
					status.append(", Original - ");
					status.append(String.valueOf(checkcount));
					status.append("\r\n");
					return;
				}
			}

			sendEmails();
			status.append(String.valueOf(total));
			status.append(" contacts found.\r\n");
			status.append(String.valueOf(sent));
			status.append(" emails sent.\r\n");
			status.append(String.valueOf(failed));
			status.append(" emails failed.\r\n");
			if (isKilled()) {
				status.append("Offline process terminated by - ");
				status.append(getKiller());
				status.append("\r\n\r\n");
			}
			else {	   
				status.append("Offline process completed.\r\n\r\n");
			}

		}
		catch(Exception e)
		{
			status.append("Offline process halted on ");
			status.append(String.valueOf(sent + failed));
			status.append(" of ");
			status.append(String.valueOf(total));
			status.append(".\r\n\r\n");
			status.append(ActionBean.writeStackTraceToString(e));

		}
		finally
		{
			if(con != null)
			{
				try
				{
					con.close();
					con = null;
				}
				catch(Exception ex)
				{
				}
			}
			if(ctx != null)
			{
				try
				{
					ctx.close();
					ctx = null;
				}
				catch(Exception ex)
				{
				}
			}
		}
	}

	void setEmailHeader()
	{
		status.append(contextname);
		status.append(" Runway offline process - Send Email\r\n");
		status.append("Initiated ");
		try
		{
			DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM,
					super.getLocale());
			status.append(dt.format(new java.util.Date()));
		}
		catch(Exception e)
		{
			status.append(new java.util.Date().toString());
		}
		status.append("\r\n");
	}

	String statusstring = null;

	public String getStatusMailBody()
	{
		if(statusstring == null)
		{
			if(errors != null && errors.length() != 0)
			{
				status.append("\r\n");
				status.append(errors.toString());
			}
			statusstring = status.toString();
		}
		return (statusstring);
	}

	public String getStatusMailSubject()
	{
		return (contextname + " Runway offline process - Send Email");
	}

	public String getStatusMailRecipient()
	{
		return (user.getEmail());
	}

	public int getProcessSize() {
		return total;
	}

	public int getProcessedCount() {
		return sent+failed;
	}

	public int getErrorCount() {
		return failed;
	}

	public String getSMTPHost() {
		return(smtpHost);
	}

	public String getSMTPUsername() {
		if (smtpUsername == null) {
			return "";
		}
		else {
			return(smtpUsername);
		}
	}
	public String getSMTPPassword() {
		if (smtpPassword == null) {
			return "";
		}
		else {
			return(smtpPassword);
		}
	}    

	public String getSMTPPort() {
		return smtpPort;
	}
}