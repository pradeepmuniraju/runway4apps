package com.sok.runway;

import java.util.List;
import java.util.ArrayList;

import org.dom4j.Document;
import org.dom4j.Element;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.runway.ReportBean.ReportCell;

public class HistoricReport extends GenRow {
	
	private static final Logger logger = LoggerFactory.getLogger(HistoricReport.class);
   
   // Tags
   public static final String _reportResults = "reportResults";
   public static final String _dataSet = "dataSet";
   public static final String _data = "data";
   
   // Attributes
   public static final String _reportSpec = "reportSpec";
   public static final String _rowSubsetName = "rowSubsetName";
   public static final String _colSubsetName = "colSubsetName";
   public static final String _chartType = "chartType";
   public static final String _name = "name";
  // public static final String _type = "type";
   public static final String _format = "format";
   public static final String _link = "link";
   
   // DB column names
	public static final String _historicReportID = "HistoricReportID";
	public static final String _createdDate = "CreatedDate";
	public static final String _results = "Results";
	public static final String _beanID = "BeanID";
	
	ReportBean reportBean;
    
	public HistoricReport() {
		logger.debug("HistoricReport()");
		this.setViewSpec("HistoricReportView");
		this.clear();
	}
	
   public void clear() {
	   logger.debug("clear()");
	   super.clear();
      this.reportBean = null;
   }
   
   @Override
	public void setParameter(String name, Object value) {
	   logger.debug("setParameter({}, {})", name, value.toString());
	   	if("ReportData".equals(name)) { 
	   		//TODO - compress the value ? 
	   		//perhaps a flag in the table spec is more appropriate. 
	   	}
		super.setParameter(name, value);
	}

	/* Historic Report Snapshot Creation methods */
	public void setResults(ReportBean reportBean) {
		logger.debug("setResults({})", reportBean != null ? "reportBean": "null");
		setReportBean(reportBean);
		
		if(this.reportBean != null) {
			List<String []> report = this.reportBean.report;
			if(report != null) {
				//lets keep this consistent and we can extend via JS - there's no guarantee that the id will be set at this point. 
				//perhaps this field should be called ChartResults ? Not sure. 
				//it is also highly likely that this data should be compressed. 
				this.setParameter("ReportData", this.reportBean.getChartJson("chartcontent"));
				
				Document document = XMLUtil.createDocument();
				Element root = document.addElement(_reportResults);	
				
				String reportSpecName = getReportSpecName();
				String chartType = getChartType();
				String rowSubsetName = getRowSubsetName();
				String colSubsetName = getColSubsetName();
				
				XMLUtil.addAttribute(root, _reportSpec, reportSpecName);
				XMLUtil.addAttribute(root, _chartType, chartType);
				if(true || "Custom".equals(reportBean.getString("-reportType"))) {
					//we do this always, as the row data will now include the totals always.
					XMLUtil.addAttribute(root, "custom", "true");
				}
				XMLUtil.addAttribute(root, _rowSubsetName, rowSubsetName);
				XMLUtil.addAttribute(root, _colSubsetName, colSubsetName);
				/*
				int rowCount = getRowCount();
				if(rowCount > 0) {
					for(int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
						appendRow(root, rowIndex);
					}
					
					String xmlResults = XMLUtil.getXmlFromDocument(document);
					
					if(xmlResults != null) {
						setParameter(_results, xmlResults);	
					}
				}
				*/
				boolean swap = "true".equals(this.reportBean.getString("-swapsummary"));
				logger.debug("output report swapped ? {}", swap);
				List<List<ReportCell>> chartData = this.reportBean.getChartData();
				if(chartData == null || chartData.size() == 0) {
					return;
				}
				if (!swap) {
					// set up column headers.
					List<ReportCell> headers = chartData.get(0);
					String[] colheaders = new String[headers.size()];
					int col = 0;
					for (ReportCell tc : headers) {
						if (col == 0) {
							col++;
							continue;
						}
						colheaders[col] = tc.getText();
						col++;
					}
					for (int i = 1; i < chartData.size(); i++) {
						List<ReportCell> row = chartData.get(i);
						Element rowDataSet = root.addElement(_dataSet);
						Element rowData = rowDataSet.addElement(_data);
						XMLUtil.addAttribute(rowData, _name, row.get(0)
								.getText());
						XMLUtil.addAttribute(rowData, _format,
								ActionBean._blank);

						Element colDataSet = rowData.addElement(_dataSet);
						for (int j = 1; j < row.size(); j++) {
							Element colData = colDataSet.addElement(_data);
							XMLUtil.addAttribute(colData, _name, colheaders[j]);
							XMLUtil.addAttribute(colData, _format,
									ActionBean._blank);
							XMLUtil.addText(colData, row.get(j).getText());
						}
					}
				} else if (chartData.size() > 0) {
					// these for loops are back to front
					for (int i = 1; i < chartData.get(0).size(); i++) {
						Element rowDataSet = root.addElement(_dataSet);
						Element rowData = rowDataSet.addElement(_data);
						XMLUtil.addAttribute(rowData, _name, chartData.get(0).get(i).getText());
						XMLUtil.addAttribute(rowData, _format,ActionBean._blank);
						Element colDataSet = rowData.addElement(_dataSet);

						for (int j = 1; j < chartData.size(); j++) {
							Element colData = colDataSet.addElement(_data);
							XMLUtil.addAttribute(colData, _name, chartData.get(j).get(0).getText());
							XMLUtil.addAttribute(colData, _format, ActionBean._blank);
							if (i < chartData.get(j).size()) {
								XMLUtil.addText(colData, chartData.get(j).get(i).getText());
							} else {
								XMLUtil.addText(colData,"");
							}
						}
					}
				}
				
				String xmlResults = XMLUtil.getXmlFromDocument(document);
				if(xmlResults != null) {
					setParameter(_results, xmlResults);	
				}
			}
		}
	}
	
	public void setReportBean(ReportBean report) {
	      this.reportBean = report;
	   }
	
	/**
	 * @deprectaed
	 */
	public void setCustomReportBean(CustomReportBean report) {
		this.reportBean = report;
	}
	/*
	private void appendRow(Element root, int rowIndex) {
		if(root != null) {	
			// Create row header as dataSet
			String name = getRowTitle(rowIndex);
			//String type = ActionBean._blank; // not applicable for row
			String format = ActionBean._blank; // not applicable for row
			String value = ActionBean._blank; // not applicable for row

			Element rowDataSet = root.addElement(_dataSet);
			Element rowData = rowDataSet.addElement(_data);
			
			XMLUtil.addAttribute(rowData, _name, name);
			//XMLUtil.addAttribute(rowData, _type, type);
			XMLUtil.addAttribute(rowData, _format, format);
			
			XMLUtil.addText(rowData, value);
			
			appendColumns(rowData, rowIndex);
		}
	}
	
	private void appendColumns(Element rowData, int rowIndex) {
		int rowCount = getRowCount();		
		if(rowData != null && rowCount > 0 && rowIndex < rowCount) {
			int colCount = getColCount();
			Element colDataSet = rowData.addElement(_dataSet);
			
			for(int colIndex = 0; colIndex < colCount; colIndex++) {
				String name = getColTitle(colIndex);
				//String type = getColType(colIndex);
				String format = getFormat(colIndex);
				String value = getValue(rowIndex, colIndex);
				
				Element colData = colDataSet.addElement(_data);
				XMLUtil.addAttribute(colData, _name, name);
				//XMLUtil.addAttribute(colData, _type, type);
				XMLUtil.addAttribute(colData, _format, format);
				
				XMLUtil.addText(colData, value);
			}
		}
	}
	*/
	
	public String getReportSpecName() {
	   String reportSpecName = ActionBean._blank;
	   
	   if(this.reportBean != null) {
	      reportSpecName = this.reportBean.getReportSpecName();
	   }
	   
	   return reportSpecName;
	}
	
	public String getChartType() {
	   String chartType = ActionBean._blank;
	   
	   if(this.reportBean != null) {
	      chartType = this.reportBean.getChartType();
	   }
	   
	   return chartType;
	}
	
	public String getRowSubsetName() {
	   String rowSubsetName = ActionBean._blank;
	   if(this.reportBean != null) {
	      rowSubsetName = this.reportBean.getRowSubsetName();
	   }
	   
	   return rowSubsetName;
	}
	
   public String getColSubsetName() {
      String colSubsetName = ActionBean._blank;
      if(this.reportBean != null) {
         colSubsetName = this.reportBean.getColSubsetName();
      }
      
      return colSubsetName;
   }	
	/*
	public int getRowCount() {
		int rowCount = 0;
		
		if(this.reportBean != null) {
			rowCount = this.reportBean.getRowCount();
		}
		
		return rowCount;
	}
	
	public int getColCount() {
		int colCount = 0;
		
		if(this.reportBean != null) {
			colCount = this.reportBean.getColumnCount();
		}
		
		return colCount;
	}
	
	public String getRowTitle(int rowIndex) {
		String title = ActionBean._blank;
		
		if(this.reportBean != null) {
			title = this.reportBean.getRowTitle(rowIndex);
		}
		
		return title;
	}
	
	public String getColTitle(int colIndex) {
		String title = ActionBean._blank;
		
		if(this.reportBean != null) {
			title = this.reportBean.getColumnTitle(colIndex);
		}
		
		return title;
	}
	*/
	/*
	public String getColType(int colIndex) {
		String colType = ActionBean._blank;
		
		if(this.reportBean != null && this.reportBean.coltypes != null && colIndex < this.reportBean.coltypes.length) {
			colType = String.valueOf(this.reportBean.coltypes[colIndex]);
		}
		
		return colType;
	}
	*/
	/**
	 * NOTE: Currently, format is assumed the same for all columns in a row
	 * so <code>colIndex</code> is not used, but is included for future use.
	 * 
	 * @param colIndex 
	 * @return
	 */
	public String getFormat(int colIndex) {
		String format = ActionBean._blank;
		
		if(this.reportBean != null) {
			format = this.reportBean.format;
		}
		
		return format;
	}
	/*
	public String getValue(int rowIndex, int colIndex) {
		String value = ActionBean._blank;
		
		if(this.reportBean != null && this.reportBean.report != null) {
			value = this.reportBean.getItem(rowIndex, colIndex);
		}
		
		return value;
	}
	*/
	/* End Historic Report Snapshot Creation methods */
	
	/* Report Bean Population methods */

   public ReportBean getCustomReportBean() {    
      if(this.reportBean == null) {
         populateCustomReportBean();
      }
      return this.reportBean;
   }
	
	private void populateCustomReportBean() {
	   String resultsXml = getString(_results);
	   logger.debug(resultsXml);
      if(resultsXml != null && resultsXml.length() > 0) {
         Document document = XMLUtil.createDocumentFromXml(resultsXml);
         Element root = document.getRootElement();
         
         prepareCustomReportBean();
         
         String reportSpecName = XMLUtil.getAttribute(root, _reportSpec);
         String chartType = XMLUtil.getAttribute(root, _chartType);
         String rowSubsetName = XMLUtil.getAttribute(root, _rowSubsetName);
         String colSubsetName = XMLUtil.getAttribute(root, _colSubsetName);
         
         boolean custom = "true".equals(XMLUtil.getAttribute(root, "custom"));
         
         setReportSpec(reportSpecName);
         setChartType(chartType);
         //setRowSubsetName(rowSubsetName); //TODO: store as a rowvar
         //setColSubsetName(colSubsetName); //TODO: store as a colvar
         
         if(root!=null) {
            Element[] rowDataSets = XMLUtil.getElementArray(root, _dataSet);
   
            int rowCount = XMLUtil.getCount(rowDataSets);
            
            prepareReportRows(rowCount);
            
            for(int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
               Element rowDataSet = rowDataSets[rowIndex];               
               Element rowData = XMLUtil.getFirstElement(rowDataSet, _data);
               
               String rowTitle = XMLUtil.getAttribute(rowData, _name);
               setRowTitle(rowTitle, rowIndex);
                  
               populateCustomReportBeanColumns(rowData, rowIndex, custom);
            }
            
            this.reportBean.populateRowCount();
            this.reportBean.populateColumnCount();
            if(!custom) {
            	this.reportBean.populateColumnSummary();
            	this.reportBean.addTotal();
            }
         } else {
            System.out.print("Could not find root element in document:\n\n" + resultsXml);
         }         
      }
	}
	
	private void prepareCustomReportBean() {
      if(this.reportBean == null) {
         this.reportBean = new CustomReportBean();
      }	   
	}
	
	private void setReportSpec(String reportSpecName) {
	   if(this.reportBean != null) {
	      this.reportBean.setReportSpec(reportSpecName);
	   }	   
	}
	
	private void setChartType(String chartType) {
	   if(this.reportBean != null) {
	      this.reportBean.setChartType(chartType);
	   }
	}
	
	private void prepareReportRows(int rowCount) {
	   if(this.reportBean != null) {	      
	      this.reportBean.rowtitles = new String[rowCount];
	   }
	}
   
   private void setRowTitle(String rowTitle, int rowIndex) {
      if(this.reportBean != null && this.reportBean.rowtitles != null && rowIndex < this.reportBean.rowtitles.length) {
         this.reportBean.rowtitles[rowIndex] = rowTitle;
      }
   }	
	
	private void populateCustomReportBeanColumns(Element rowData, int rowIndex, boolean isCustom) {
	   if(this.reportBean != null) {
   	   Element colDataSet = XMLUtil.getFirstElement(rowData, _dataSet);
   	   Element[] colData = XMLUtil.getElementArray(colDataSet, _data);
   	   int colCount = XMLUtil.getCount(colData);
   	   
   	   prepareReportColumns(colCount);
   	   this.reportBean.report.add(new String[colCount]);
   	   	   
   	   for(int colIndex = 0; colIndex < colCount; colIndex++) {
   	      Element colDatum = colData[colIndex];
   	      
   	      String colTitle = XMLUtil.getAttribute(colDatum, _name);
   	      //String colType = XMLUtil.getAttribute(colDatum, _type);
   	      String colFormat = XMLUtil.getAttribute(colDatum, _format);
   	      String colValue = XMLUtil.getText(colDatum);
   	      	      
   	      setColTitle(colTitle, colIndex);
   	      //setColType(colType, colIndex);
   	      setColFormat(colFormat, colIndex);
   	      
   	      setValue(colValue, rowIndex, colIndex);
   	   }
   	   
   	   String[] row = XMLUtil.getStringArrayByText(colData);
   	   	if(!isCustom) {
   	   		this.reportBean.populateRowSummary(row);
   	   	}
	   }
	}	
	
	private void prepareReportColumns(int colCount) {
      if(this.reportBean != null) {
         if(this.reportBean.coltitles == null) {
            this.reportBean.coltitles = new String[colCount];
         }
         if(this.reportBean.coltitles == null) {
            //this.reportBean.coltypes = new int[colCount];
         }
         if(this.reportBean.report == null) {
            this.reportBean.report = new ArrayList<String []>();
         }         
      }	   
	}
	
	private void setColTitle(String colTitle, int colIndex) {
      if(this.reportBean != null && this.reportBean.coltitles != null && colIndex < this.reportBean.coltitles.length) {
         this.reportBean.coltitles[colIndex] = colTitle;
      }	   
	}
	/*
	private void setColType(String colType, int colIndex) {
	   if(this.reportBean != null && this.reportBean.coltypes != null && colIndex < this.reportBean.coltypes.length) {
	      int colTypeAsInt = -1;
	      try {
	         colTypeAsInt = Integer.parseInt(colType);
	      } catch(NumberFormatException e) {
	         // set col type to -1 by default
	      }
	      
	      this.reportBean.coltypes[colIndex] = colTypeAsInt;
	   }
	}
	*/
	
	private void setColFormat(String colFormat, int colIndex) {
	   /* 
	    * NOTE: All columns in a report have the same format
	    * (could change in the future), so we only set this 
	    * value once and ignore the colIndex argument.
	    */
	   if(this.reportBean != null && (this.reportBean.format == null || this.reportBean.format.length() == 0)) {
	      this.reportBean.format = colFormat;
	   }
	}
	
	private void setValue(String colValue, int rowIndex, int colIndex) {	   
	   if(this.reportBean != null && this.reportBean.report != null && rowIndex < this.reportBean.report.size()) {
	      String[] row = (String[]) this.reportBean.report.get(rowIndex);
	      if(row != null && colIndex < row.length) {
	         row[colIndex] = (colValue != null) ? colValue : ActionBean._blank;
	      }
	   }
	}
	
	/* End Report Bean Population methods */
}
