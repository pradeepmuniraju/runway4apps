package com.sok.runway;
import javax.servlet.*;     
import javax.servlet.http.*;
import java.util.*;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import java.text.*;

import javax.mail.*;
import com.sok.framework.*;

public class OfflineValidateEmail extends OfflineProcess
{
   private static final int VALIDATOR_POOL_SIZE = 50;

	StringBuffer status = new StringBuffer();
	String contextpath = null;
	UserBean user = null;
	TableData search = null;

	boolean debug = false;
	ArrayList contacts = new ArrayList(500);

	Connection con = null;
	Context ctx = null;
	Transport transport = null;
	javax.mail.Session msession = null;

	int total = 0;
	int validated = 0;
	int couldnotvalidated = 0;
	int failed = 0;
	int processTotal = 0;

   ArrayList validationBeans = new ArrayList();
 
	//EmailValidationBean validation = new EmailValidationBean();
	static String updatestmt = "Update Contacts set EmailStatus = ? where ContactID = ?";

	public OfflineValidateEmail(HttpServletRequest request, ServletContext context)
	{
		super(request, context);
		HttpSession session = request.getSession();
		user = (UserBean)session.getAttribute(SessionKeys.CURRENT_USER);
		search = (TableData)session.getAttribute(SessionKeys.CONTACT_SEARCH);
		if (search != null){
			search.setViewSpec("OfflineContactView");
			// we need to remove all group by
			for (int x = 0; x < 10; ++x) {
				search.remove("-groupby" + x);
				search.remove("-sort" + x);
				search.remove("-order" + x);
			}
			search.setParameter("-groupby0", "ContactID");
		}
		
		for (int i=0; i < OfflineValidateEmail.VALIDATOR_POOL_SIZE; i++) {
		   validationBeans.add(new EmailValidationBean());
		}
		start();
	}

	void retrieveContacts()throws Exception
	{
		if(debug)
		{		  
			appendError(search.getSearchStatement());
		}			
		try
		{
		   search.setConnection(getConnection());
		   RowSetWrapperBean list = new RowSetWrapperBean();		
		   //list.setConnection(con);
		   list.setSearchBean(search);
		   list.getResults();
		   
		   TableData resultset = list.getResultBean();
		   while (list.getNext()) {
				//contacts.add(resultset.getString("ContactID"));
				contacts.add(new Contact(resultset.getString("ContactID"),resultset.getString("Email")));
			}		   			  
			total = contacts.size();		      			
		}		
		finally
		{    		
			try{
            	con.close();
        	}catch (Exception ex){}    		  		    
		}
		
		
		/*StringBuffer searchstmt = new StringBuffer();
		searchstmt.append("select Contacts.ContactID, Contacts.Email from Contacts where ");
		searchstmt.append(search.getSearchCriteria());

		if(debug)
		{
			appendError(searchstmt.toString());
		}

		ResultSet current = null;
		try
		{
			Statement stmt = con.createStatement();
			current = stmt.executeQuery(searchstmt.toString());
			while(current.next())
			{
				contacts.add(new Contact(current.getString(1),current.getString(2)));
			}
			total = contacts.size();
		}
		finally
		{
    		if (current != null) 
    		{
				try{
            	current.close();
        		}catch (Exception ex){}
    		}
		}*/

	}
   
   private synchronized EmailValidationBean getValidator() {
      EmailValidationBean evb = null;
      try {
         evb = (EmailValidationBean)validationBeans.remove(validationBeans.size()-1);           
      }
      catch (Exception e) {}
      return evb;
   }
   
   private synchronized void returnValidator(EmailValidationBean evb) {
      validationBeans.add(evb);
   }

	private void validateEmails()
	{
		Contact contact = null;
		for(int i=0; i<total && !isKilled(); i++) {
		  
      	contact = (Contact)contacts.get(i);
		   EmailValidationBean evb = getValidator();
		   while (evb == null) {
		      try {
		         Thread.sleep(1000);
		      }
		      catch (Exception e) {}
		      evb = getValidator();
		   }
		   
		   ValidateThread t = new ValidateThread(this, contact, evb, i);
		   
		   t.start();
		}

	}

   private class ValidateThread extends Thread {
      private Contact contact;
      private OfflineValidateEmail parent;
      private EmailValidationBean validator;
      private int index;
      
      public ValidateThread(OfflineValidateEmail ove, Contact contact, EmailValidationBean validator, int index) {
         this.contact = contact;
         this.parent = ove;
         this.validator = validator;
         this.index = index;
      }
      
      public void run() {
   		validator.setEmailAddress(contact.email);
   		int result = validator.getValidateEmail();
   		parent.updateContact(contact.contactid, EmailValidationBean.CODES[result]);
      	parent.logResult(result, contact);
         parent.returnValidator(validator);
      }
   }
   
   private void logResult(int result, Contact contact) {
      StringBuffer localError = new StringBuffer();
		if(result == EmailValidationBean.VALID)
		{ 
			validated++; //This email address has been verified.
		}
		else if(result == EmailValidationBean.INVALID)
		{ 
		   setLastError("The user of this email address does not exist.");
			failed++;//.
			localError.append("The user of this email address does not exist - ");
			localError.append((String)context.getInitParameter("URLHome"));
			localError.append("/runway/contact.view?-action=select&ContactID=");
			localError.append(contact.contactid);
			localError.append("\r\n");
		}
		else if(result == EmailValidationBean.INVALIDDOMAIN)
		{ 
		   setLastError("The domain of this email address is not valid.");
			failed++;//The domain of this email address is not valid.
			localError.append("The domain of this email address is not valid - ");
			localError.append((String)context.getInitParameter("URLHome"));
			localError.append("/runway/contact.view?-action=select&ContactID=");
			localError.append(contact.contactid);
			localError.append("\r\n");
		}
		else if(result == EmailValidationBean.INVALIDFORMAT)
		{ 
		   setLastError("The format of this email address is not valid.");
			failed++;//The format of this email address is not valid.
			localError.append("The format of this email address is not valid - ");
			localError.append((String)context.getInitParameter("URLHome"));
			localError.append("/runway/contact.view?-action=select&ContactID=");
			localError.append(contact.contactid);
			localError.append("\r\n");
		}
		else if(result == EmailValidationBean.ERROR)
		{ 
			couldnotvalidated++;//This email address could not be verified.
			localError.append("This email address could not be verified - ");
			localError.append((String)context.getInitParameter("URLHome"));
			localError.append("/runway/contact.view?-action=select&ContactID=");
			localError.append(contact.contactid);
			localError.append("\r\n");
		}
		else if(result == EmailValidationBean.ERROR_USER)
		{ 
			couldnotvalidated++;//This user could not be verified.
			localError.append("This user could not be verified - ");
			localError.append((String)context.getInitParameter("URLHome"));
			localError.append("/runway/contact.view?-action=select&ContactID=");
			localError.append(contact.contactid);
			localError.append("\r\n");
		}
		else if(result == EmailValidationBean.EMPTY)
		{ 
			couldnotvalidated++;//This user could not be verified.
			localError.append("No Email Address Specified - ");
			localError.append((String)context.getInitParameter("URLHome"));
			localError.append("/runway/contact.view?-action=select&ContactID=");
			localError.append(contact.contactid);
			localError.append("\r\n");
		}
		processTotal++;
		if (localError.length() > 0) {
		   appendError(localError.toString());
		}
   }

	public synchronized void updateContact(String contactid, String status)
	{
		PreparedStatement updatestm = null;

		try
		{
			updatestm = getConnection().prepareStatement(updatestmt);
			updatestm.setString(1, status);
			updatestm.setString(2, contactid);
			updatestm.executeUpdate();
		}
		catch (Exception ex)
		{
		   StringBuffer localError = new StringBuffer();
    		localError.append("Could not update contact, ");
			localError.append(ex.toString());
			localError.append(" - ");
			localError.append(contactid);
			localError.append(".\r\n");
		   appendError(localError.toString());
		}
		finally
		{
    		if (updatestm != null) 
    		{
        		try{       
            	updatestm.close();
        		}catch (SQLException ex){}
    		}
		}
	}

	private synchronized Connection getConnection() throws Exception
	{
	   if(con==null || con.isClosed())
	   {
			ctx = new InitialContext();

			DataSource ds = null;
				try{
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
					Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource)envContext.lookup(dbconn);
				}catch(Exception e){}
			if(ds == null)
			{
				ds = (DataSource)ctx.lookup(dbconn);
			}
			con = ds.getConnection();
		}
		return con;
	}

	public void execute()
	{
		setEmailHeader();

		String debugstring = requestbean.getString("-debug");

		if(debugstring.length()!=0)
		{
			debug = true;
		}

		con = null;
		ctx = null;
		try
		{
			if(user == null || search == null)
			{
				status.append("Resources not found, offline process halted.\r\n");
				return;
			}

			getConnection();

			retrieveContacts();

			validateEmails();

         // Check to see if all threads have completed.
         while (total > ( processTotal ) && !isKilled()) {
            try {
               Thread.sleep(1000);
            }
            catch (Exception e) {}
         }
         
			status.append(String.valueOf(total));
			status.append(" email addresses processed.\r\n");

			status.append(String.valueOf(validated));
			status.append(" valid email addresses.\r\n");

			status.append(String.valueOf(failed));
			status.append(" invalid email addresses.\r\n");

			status.append(String.valueOf(couldnotvalidated));
			status.append(" email addresses could not be validated.\r\n");
			
   		if (isKilled()) {
   	      status.append("Offline process terminated by - ");
   	      status.append(getKiller());
   	      status.append("\r\n\r\n");
   	   }
   	   else {	   
   	      status.append("Offline process completed.\r\n\r\n");
   	   }

		}
		catch(Exception e)
		{
			status.append("Offline process halted");
			status.append(".\r\n\r\n");
			status.append(e.toString());

		}
		finally
		{
    		if (con != null) 
    		{
				try{
            	con.close();
					con = null;
        		}catch (Exception ex){}
    		}
    		if (ctx != null) 
    		{
        		try{       
            	ctx.close();
					ctx = null;
        		}catch (Exception ex){}
    		}

			if(getError().length()!=0)
			{
				status.append("\r\n");
				status.append(getError());
			}
		}
	}



	void setEmailHeader()
	{
		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
		status.append(contextname);
		status.append(" Runway offline process - Validate Email Addresses\r\n");
		status.append("Initiated ");
		status.append(dt.format(new java.util.Date()));
		status.append("\r\n");
	}

	public String getStatusMailBody()
	{
		return(status.toString());
	}

	public String getStatusMailSubject()
	{
		return(contextname + " Runway offline process - Validate Email Addresses");
	}

	public String getStatusMailRecipient()
	{
		return(user.getEmail());
	}

	class Contact
	{
		String contactid;
		String email;

		public Contact(String contactid, String email)
		{
			this.contactid = contactid;
			this.email = email;
		}

	}
	
	public int getProcessSize() {
      return total;
   }
   
   public int getProcessedCount() {
      return processTotal;
   }
   
   public int getErrorCount() {
      return failed;
   }
}
