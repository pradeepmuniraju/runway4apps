package com.sok.runway;

import java.util.*;

public class ErrorMap extends HashMap {

   public ErrorMap() {
      super();
   }
   public ErrorMap(int size) {
	   super(size);
   }
   
   public String get(String key) {
      String s = (String)super.get(key);
      if (s == null) {
         return "";
      }
      else {
         return s;
      }
   }
}