package com.sok.runway;

import com.sok.framework.*;


public class QuestionBean extends RowBean {

   public QuestionBean() {
      super();      
      setViewSpec("QuestionView");
   }
  
   public boolean isCalculation() {
      return QuestionBean.isCalculation(this);
   }
      
   public static boolean isCalculation(TableData questionBean) {
      if (questionBean.getString("InputType").equals(Profile._calculation_profile_total)) {
         return true;
      }
      else {
         return questionBean.getString("InputType").equals(Profile._calculation)
                              && questionBean.getString("RelatedQuestionID").length() > 0
                              && questionBean.getString("CalculationQuestionID").length() > 0
                              && questionBean.getString("CalculationType").length() > 0;
      }
   }
      
   public static boolean isBulkCalculation(TableData questionBean) {
      return questionBean.getString("InputType").equals(Profile._calculation_sum_order)
                              && questionBean.getString("RelatedQuestionID").length() > 0;
   }
   
   
   /*public int getCalculatedAnswer(RowBean visitBean, String currentAnswerString) {
      return QuestionBean.getCalculatedAnswer(this, visitBean, currentAnswerString);
   }
   
   public int getCalculatedAnswer(String visitID, String questionID) {
      
      QuestionBean questionBean = new QuestionBean();
      questionBean.setConnection(getConnection());
      questionBean.setViewSpec("QuestionView");
      questionBean.setColumn("QuestionID", questionID);
      questionBean.setAction("select");
      questionBean.doAction();
      
      String calcPeriod = getString("CalculationPeriod");
      String calcType = getString("CalculationType");
      
      RowBean visitBean = new RowBean();
      visitBean.setConnection(getConnection());
      visitBean.setViewSpec("VisitView");
      visitBean.setColumn("VisitID", visitID);
      visitBean.setAction("select");
      visitBean.doAction();
      
      Calendar cal = new GregorianCalendar();
      try {
         cal.setTime((java.util.Date)visitBean.getObjectFromString("NoteDate"));
      }
      catch (Exception e) {}
      
      int relatedAnswer = 0;
      RowSetBean compareBean = new RowSetBean();
      compareBean.setConnection(getConnection());
      compareBean.setViewSpec("answers/VisitAnswerView");
      compareBean.setColumn("VisitID", visitID);
      compareBean.setColumn("QuestionID",questionBean.getString("CalculationQuestionID"));
      compareBean.generateSQLStatement();
      compareBean.getResults();
      if (compareBean.getNext()) {
         try { relatedAnswer = Integer.parseInt(compareBean.getString("Answer")); }
         catch (Exception e) {}
      }
      
      
      compareBean = new RowSetBean();
      compareBean.clear();
      compareBean.setConnection(getConnection());
      compareBean.setViewSpec("AnswerVisitView");
      compareBean.setColumn("AnswerVisit.CompetitorID", visitBean.getString("CompetitorID"));
      compareBean.setColumn("QuestionID",questionBean.getString("RelatedQuestionID"));
      
      StringBuffer theDate = new StringBuffer();
      theDate.append("<");
      if (calcPeriod.equals(ActionBean.LASTYEAR)) {
         cal.set(Calendar.DATE,1);
         cal.set(Calendar.MONTH,1);
      }
      else if (calcPeriod.equals(ActionBean.LASTMONTH)) {
         cal.set(Calendar.DATE,1);
         //compareBean.setColumn("AnswerVisit.VisitNote.NoteDate","<" + "01/" + Calendar.MONTH + "/" + cal.get(Calendar.MONTH));
      }
      else if (calcPeriod.equals(ActionBean.LASTWEEK)) {
         while(Calendar.DAY_OF_WEEK  != Calendar.MONDAY) {
            cal.roll(Calendar.DATE,-1);
         }
      }
      // else anything before the notedate
      if (cal.get(Calendar.DATE) < 10) {
         theDate.append("0");
      }
      theDate.append(cal.get(Calendar.DATE));
      theDate.append("/");
      if (cal.get(Calendar.MONTH)+1 < 10) {
         theDate.append("0");
      }
      theDate.append(cal.get(Calendar.MONTH)+1);
      theDate.append("/");
      theDate.append(cal.get(Calendar.YEAR));
      
      compareBean.setColumn("AnswerVisit.VisitNote.NoteDate",theDate.toString());
                     
      compareBean.setTop(1);
      compareBean.sortBy("NoteDate",0);
      compareBean.sortOrder("DESC",0);
      compareBean.generateSQLStatement();
      compareBean.getResults();
                     
      int calcAnswer = 0;
      if (compareBean.getNext()) {
         try { calcAnswer = Integer.parseInt(compareBean.getString("Answer")); }
         catch (Exception e) {}
      }
      
      if (calcType.equals(Profile._calculation_sum)) {
         calcAnswer = relatedAnswer + calcAnswer;
      }
      else if (calcType.equals(Profile._calculation_diff)) {
         calcAnswer = relatedAnswer - calcAnswer;
      }
      else if (calcType.equals(Profile._calculation_prod)) {
         calcAnswer = relatedAnswer * calcAnswer;
      }
      else if (calcType.equals(Profile._calculation_div)) {
         calcAnswer = relatedAnswer / calcAnswer;
      }
      else if (calcType.equals(Profile._calculation_percent)) {
         calcAnswer = (relatedAnswer / calcAnswer) * 100;
      }
      
      
      return calcAnswer;
   }*/

}
