package com.sok.runway;

import com.sok.Debugger;
import com.sok.framework.*;
import com.sok.framework.generation.*;
import com.sok.framework.generation.util.*;
import com.sok.framework.sql.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;

import com.sok.runway.externalInterface.resources.UtilityResource;
import com.sok.runway.security.SecurityFactory;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.*;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.laures.cewolf.DatasetProducer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.data.general.DefaultPieDataset;
import de.laures.cewolf.links.CategoryItemLinkGenerator;
import de.laures.cewolf.links.PieSectionLinkGenerator;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;
import de.laures.cewolf.tooltips.PieToolTipGenerator;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.taglib.CewolfChartFactory;
import org.jfree.data.general.Dataset;
import org.jfree.chart.*;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;

import de.laures.cewolf.tooltips.ToolTipGenerator;
import javax.servlet.jsp.JspWriter;
import javax.sql.DataSource;

/**
 * 
 * @author Jong
 * 
 * @author Michael Dekmetzian
 * @version 2 - April-May 2011 added support for caching and a secondary
 *          reportspec
 * @version 3 - Feb 2012 adding support for a standard output format for
 *          different media representations (json, xml, html, pdf, etc)
 */
public class ReportBean extends RowBean implements DatasetProducer,
		CalendarKeys {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String noaction = "-noaction";
	public static final String begindate = "-begin";
	public static final String enddate = "-end";
	public static final String begin = "begin";
	public static final String end = "end";
	public static final String altend = "altend";
	public static final String reportspec = "-reportSpec";
	public static final String reportspecright = "-reportSpecRight";
	public static final String reportUnits = "reportUnits";
	public static final String chartwidth = "-chartwidth";
	public static final String chartheight = "-chartheight";
	public static final String charttype = "-charttype";
	public static final String charttyper = "-charttyper";
	public static final String rowtrigger = "-rowtrigger";
	public static final String coltrigger = "-coltrigger";
	public static final String filtertrigger = "-filtertrigger";
	public static final String _rowtrigger = "_rowtrigger";
	public static final String _coltrigger = "_coltrigger";
	public static final String _filtertrigger = "_filtertrigger";

	public static final Logger logger = LoggerFactory
			.getLogger(ReportBean.class.getName());
	public static final String righttrigger = "-righttrigger";
	public static final String _righttrigger = "_righttrigger";

	public static final String NONE = "NONE";
	public static final String zero = "0";

	public static final String campaign = "Campaign";
	public static final String beanid = "BeanID";
	public static final String beanname = "searchdescription";
	public static final String repuserid = "RepUserID";
	public static final String createdby = "CreatedBy";
	public static final String repuseridname = "-RepUserID";
	public static final String createdbyname = "-CreatedBy";
	public static final String tokensets = "-tokensets";
	public static final String set = "_Set";
	public static final String url = "_URL";
	public static final String selected = "selected=\"selected\"";
	public static final String checked = "checked=\"checked\"";
	public static final String blank = "";
	public static final String labelsperator = ". ";

	public static final String datefield = "datefield";
	public static final String searchdatefield = "searchdatefield";

	public static final String CHART_TYPE_PIE = "pie";
	public static final String CHART_TYPE_CATEGORY = "category";

	/*
	 * current max for calendar is 17, this is safe for now. Calendar.QUARTER
	 * doesn't exist.
	 */
	public static final int CALENDAR_QUARTER = 501;
	public static final int CALENDAR_FORTNIGHT = 502;
	public static final int CALENDAR_FINYEAR = 503;
	protected List<List<ReportCell>> chartData = null;
	protected JSONObject chartJson = null;

	static final boolean _row = true;
	static final boolean _column = false;
	static final boolean _right = false;
	protected static final int names = 0;
	protected static final int values = 1;
	protected static final int types = 2;
	protected static final int group = 3;

	List<String[]> report = new ArrayList<String[]>();
	List<List<String>> links = new ArrayList<List<String>>();

	List<String[]> rightreport = new ArrayList<String[]>();
	List<List<String>> rightlinks = new ArrayList<List<String>>();

	ArrayList<String> filter = new ArrayList<String>();
	protected GenRow searchbean = new GenRow();
	protected UserBean currentuser = null;

	boolean debug = false;

	protected String[][] rowvars = new String[2][];
	protected String[][] colvars = new String[2][];
	protected String[][] rightvars = new String[2][];
	protected String[][] filtervars = new String[2][];
	String[] colsubset = null;
	String[] rightsubset = null;
	String[] rowsubset = null;
	String[] filtersubset = null;

	protected String[] altdates = null;
	String[] coltitles = null;
	String[] righttitles = null;
	String[] rowtitles = null;
	/* Removed as not used anywhere. */
	// int[] coltypes = null;
	// int[] righttypes = null;
	double[] rowsummary = null;
	double[] colsummary = null;
	double[] rightrowsummary = null;
	double[] rightsummary = null;
	int avcols = 0;
	int avright = 0;
	int[] avrows = null;
	double total = 0;
	double righttotal = 0;
	int colvar = -1;
	int rowvar = -1;
	int rightvar = -1;
	int filtervar = -1; // FOR TEST
	int colcount = 1;
	int rowcount = 1;
	int rightcount = 1;
	int cwidth = 0;
	int cheight = 0;
	String format = null;
	String rightformat = null;
	String endtype = null;

	boolean swapaxis = false;
	boolean pie = false;
	boolean manualbean = false;

	ReportSetSpec rpspec = null;
	ReportSetSpec rpspecr = null;

	VelocityEngine ve = null;

	DecimalFormat formatter = null;

	// public static final String database = VelocityDatabase.DATABASE;

	// we need our connection to be more stable, the one in rowbean is not
	// stable
	private Connection con = null;
	private Context ctx = null;

	protected String dbconn;
	protected ServletContext context;
	protected String contextpath;
	protected String contextname = null;
	protected Locale thislocale;

	public ReportBean() {
		super();
		javax.imageio.ImageIO.setUseCache(false);
		// org.jfree.chart.encoders.ImageEncoderFactory.setImageEncoder("png","org.jfree.chart.encoders.KeypointPNGEncoderAdapter");
		put(VelocityDatabase.DATABASE,
				new VelocityDatabase(ActionBean.getDatabase()));
		put("reportutil", new ReportUtil());
	}

	public static class ReportUtil implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public String getDateYesterday(String dateString) {
			try {
				java.text.DateFormat df = new java.text.SimpleDateFormat(
						"dd/MM/yyyy");
				java.util.Date d;
				d = df.parse(dateString);
				d.setTime(d.getTime() - (1000 * 60 * 60 * 24));
				return df.format(d);
			} catch (ParseException e) {
				e.printStackTrace();

				// make it really obvious that it hasn't worked, the previous
				// implementation would use a day that was one day out.
				return "01/01/1970";
			}
		}
	}

	@Override
	public void setError(String s) {
		super.setError(s);
	}

	public void clear() {
		super.clear();
		report.clear();
		links.clear();
		rightreport.clear();
		rightlinks.clear();

		searchbean.clear();
		debug = false;

		rowvars = new String[2][];
		colvars = new String[2][];
		rightvars = new String[2][];

		colsubset = null;
		rowsubset = null;
		rightsubset = null;
		filtersubset = null;

		altdates = null;
		coltitles = null;
		rowtitles = null;
		righttitles = null;
		// coltypes = null;
		rowsummary = null;
		colsummary = null;
		rightsummary = null;
		total = 0;
		righttotal = 0;
		colvar = -1;
		rowvar = -1;
		rightvar = -1;
		filtervar = -1;
		colcount = 1;
		rowcount = 1;
		rightcount = 1;
		cwidth = 0;
		cheight = 0;
		format = null;
		rightformat = null;
		endtype = null;
		avcols = 0;
		avrows = null;
		avright = 0;

		swapaxis = false;
		pie = false;
		manualbean = false;

		rpspec = null;
		rpspecr = null;

		formatter = null;
		put(VelocityDatabase.DATABASE,
				new VelocityDatabase(ActionBean.getDatabase()));
		put("reportUtil", new ReportUtil());
	}

	public void setDebug(boolean b) {
		debug = b;
	}

	public void setSwap() {
		swapaxis = true;
	}

	public void setPie() {
		pie = true;
	}

	/**
	 * Sets the legacy variant chart type. This has not been modified to
	 * preserve backwards compatibility.
	 */
	public void setChartType(String chartType) {
		this.setParameter(ReportBean.charttype, chartType);
		if (chartType.startsWith("stacked")) {
			this.setChartOption("stacking");
		}
	}

	public void setChartTypeRight(String chartType) {
		this.setParameter(ReportBean.charttyper, chartType);
	}

	public boolean isEmpty() {
		if (report.size() > 0 && rowsummary != null) {
			for (int i = 0; i < rowsummary.length; i++) {
				if (rowsummary[i] > 0) {
					return false;
				}
			}
		}
		return true;
	}

	public int getRowCount() {
		return (report.size());
	}

	public int getColumnCount() {
		if (report.size() > 0) {
			String[] row = (String[]) report.get(0);
			if (row != null) {
				return (row.length);
			}
		}
		return (0);
	}

	public int getRightCount() {
		if (rightreport.size() > 0) {
			String[] row = (String[]) rightreport.get(0);
			if (row != null) {
				return (row.length);
			}
		}
		return (0);
	}

	public String getValue(int r, int c) {
		return getValue(r, c, ListType.Col);
	}

	public String getValue(int r, int c, ListType l) {
		List<String[]> report = (l.equals(ListType.Col)) ? this.report
				: this.rightreport;
		if (r < report.size()) {
			String[] row = (String[]) report.get(r);
			if (row != null && c < row.length && row[c] != null) {
				if (format != null) {
					return (format(row[c]));
				}
				return (row[c]);
			}
		}
		return (ActionBean._blank);
	}

	public String getItem(int r, int c) {
		return getItem(r, c, ListType.Col);
	}

	public String getItem(int r, int c, ListType l) {
		List<String[]> report = (l.equals(ListType.Col)) ? this.report
				: this.rightreport;
		if (r < report.size()) {
			String[] row = (String[]) report.get(r);
			if (row != null && c < row.length && row[c] != null) {
				return (row[c]);
			}
		}
		return ("0");
	}

	public String getColumnTitle(int i) {
		if (i >= 0) {
			if (coltitles != null && i < coltitles.length) {
				return (coltitles[i]);
			}
		}
		return (ActionBean._blank);
	}

	public String getRightTitle(int i) {
		if (i >= 0) {
			if (righttitles != null && i < righttitles.length) {
				return (righttitles[i]);
			}
		}
		return (ActionBean._blank);
	}

	/**
	 * Use getRowSummary
	 * 
	 * @deprecated
	 */
	public String getRowTotal(int i) {
		return getRowSummary(i);
	}

	public String getRowSummary(int i) {
		if (i >= 0) {
			if (rowsummary != null && i < rowsummary.length) {
				if (rowsummary[i] != 0) {
					if (format != null) {
						return (format(rowsummary[i]));
					}
					return (String.valueOf(rowsummary[i]));
				}
			}
		}
		return (zero);
	}

	/**
	 * Use getColumnSummary
	 * 
	 * @deprecated
	 */
	public String getColumnTotal(int i) {
		return getColumnSummary(i);
	}

	public String getColumnSummary(int i) {
		if (i >= 0) {
			if (colsummary != null && i < colsummary.length) {
				if (colsummary[i] != 0) {
					if (format != null) {
						return (format(colsummary[i]));
					}
					return (String.valueOf(colsummary[i]));
				}
			}
		}
		return (zero);
	}

	public String getRightRowSummary(int i) {
		if (i >= 0) {
			if (rightrowsummary != null && i < rightrowsummary.length) {
				if (rightrowsummary[i] != 0) {
					if (rightformat != null) {
						return (format(rightrowsummary[i], rightformat));
					}
					return (String.valueOf(rightrowsummary[i]));
				}
			}
		}
		return (zero);
	}

	public String getRightSummary(int i) {
		if (i >= 0) {
			if (rightsummary != null && i < rightsummary.length) {
				if (rightsummary[i] != 0) {
					if (rightformat != null) {
						return (format(rightsummary[i], rightformat));
					}
					return (String.valueOf(rightsummary[i]));
				}
			}
		}
		return (zero);
	}

	/**
	 * use getSummary
	 * 
	 * @deprecated
	 */
	public String getTotal() {
		return getSummary();
	}

	public String getSummary() {
		if (total != 0) {
			double val = total;
			if ("AVG".equals(endtype)) {
				return ActionBean._blank;
				/*
				 * SKIP as cannot garuntee results if(avcols == 0) { return
				 * (zero); } val = total / colcount;
				 */
			}
			if (format != null) {
				return (format(val));
			}
			return (String.valueOf(val));
		}
		return (zero);
	}

	public String getRightSummary() {
		if (righttotal != 0) {
			if (rightformat != null) {
				return (format(righttotal, rightformat));
			}
			return (String.valueOf(righttotal));
		}
		return (zero);
	}

	public String format(String value) {
		if (StringUtils.isBlank(format) || "error".equals(value))
			return value;
		try {
			return (format(Double.parseDouble(value), format));
		} catch (Exception e) {
			e.printStackTrace();
			return value;
		}
	}

	public String format(String value, String format) {
		if (StringUtils.isBlank(format) || "error".equals(value))
			return value;
		try {
			return (format(Double.parseDouble(value), format));
		} catch (Exception e) {
			logger.error("error formatting string to double", e);
			return value;
		}
	}

	public String format(double value) {
		return format(value, format);
	}

	public String format(double value, String format) {
		logger.trace("formatting {} as {}", value, format);
		if (format == null)
			return String.valueOf(value);
		if (format.equals(ActionBean.TYPE_CURRENCY)) {
			NumberFormat nf = NumberFormat.getCurrencyInstance();
			return (nf.format(value));
		} else if (format.equals(ActionBean.TYPE_DECIMAL)) {
			DecimalFormat nf = new DecimalFormat();
			return (nf.format(value));
		} else {
			formatter = new DecimalFormat(format);
			logger.trace("formatting decimal to {}", formatter.format(value));
		}
		return (formatter.format(value));
	}

	/**
	 * @deprecated
	 */
	public boolean hasLink(int r, int c) {
		return hasLink(r, c, ListType.Col);
	}

	public boolean hasLink(int r, int c, ListType l) {
		List<List<String>> links = (l.equals(ListType.Col)) ? this.links
				: this.rightlinks;
		if (r > -1 && r < links.size()) {
			ArrayList<String> row = (ArrayList<String>) links.get(r);
			if (row != null && c > -1 && c < row.size()) {
				String link = row.get(c);
				if (link != null && link.length() != 0) {
					return (true);
				}
			}
		}
		return (false);
	}

	public String getLink(int r, int c) {
		return getLink(r, c, ListType.Col);
	}

	public String getLink(int r, int c, ListType l) {
		List<List<String>> links = (l.equals(ListType.Col)) ? this.links
				: this.rightlinks;
		if (r < links.size()) {
			ArrayList<String> row = (ArrayList<String>) links.get(r);
			if (row != null && row.size() > c) {
				String link = row.get(c);
				if (link != null && link.length() != 0) {
					return (link);
				}
			}
		}
		return (ActionBean._blank);
	}

	public boolean hasRowTitles() {
		if (rowtitles != null && rowtitles.length != 0) {
			return (true);
		}
		return (false);
	}

	public boolean hasColTitles() {
		if (coltitles != null && coltitles.length != 0) {
			return (true);
		} else if (rpspec != null && rpspec.getReportUnitsCount() > 1) {
			return (true);
		}
		return (false);
	}

	public boolean hasRightTitles() {
		if (righttitles != null && righttitles.length != 0) {
			return (true);
		} else if (rpspecr != null && rpspecr.getReportUnitsCount() > 1) {
			return (true);
		}
		return (false);
	}

	/**
	 * Use new method hasRowSummary
	 * 
	 * @deprecated
	 */
	public boolean hasRowTotals() {
		return hasRowSummary();
	}

	public boolean hasRowSummary() {
		if (report.size() > 1 && rowsummary != null) {
			return (true);
		}
		return (false);
	}

	/**
	 * Use new method hasColSummary
	 * 
	 * @deprecated
	 */
	public boolean hasColTotals() {
		return hasColSummary();
	}

	public boolean hasColSummary() {
		if (colcount > 1 && colsummary != null) {
			return (true);
		}
		return (false);
	}

	public boolean hasRightSummary() {
		if (rightcount > 1 && rightsummary != null) {
			return (true);
		}
		return (false);
	}

	public String getSummaryLabel() {
		if ("AVG".equals(endtype)) {
			return "Average";
		}
		return "Total";
	}

	public String getRowTitle(int i) {
		if (rowtitles != null && i >= 0 && i < rowtitles.length) {
			return (rowtitles[i]);
		}
		return (ActionBean._blank);
	}

	public String getTitle() {
		if (rpspec != null) {
			return (rpspec.getTitle());
		}
		return (ActionBean._blank);
	}

	public String getModuleType() {
		if (rpspec != null) {
			return rpspec.getModuleType();
		}
		return ActionBean._blank;
	}

	public String getRefineSearchUrl() {
		if (rpspec != null) {
			return rpspec.getRefineSearchURL();
		}
		return ActionBean._blank;
	}

	public String getSubTitle() {
		StringBuffer subtitle = new StringBuffer();
		String prefix = " - ";
		String coltitle = getColSubsetName();
		String righttitle = getRightSubsetName();
		String rowtitle = getRowSubsetName();
		if (coltitle.length() != 0 || rowtitle.length() != 0) {
			subtitle.append(" with ");
			if (coltitle.length() != 0) {
				subtitle.append(coltitle);
				if (rowtitle.length() != 0) {
					subtitle.append(" by ");
				}
			}
			if (rowtitle.length() != 0) {
				subtitle.append(" ");
				subtitle.append(rowtitle);
			}
			if (righttitle.length() != 0) {
				subtitle.append(" and ").append(righttitle);
			}
		}
		if (getString(campaign).length() != 0) {
			if (subtitle.length() != 0) {
				subtitle.append(',');
			} else {
				subtitle.append(prefix);
			}
			subtitle.append(" Campaign: ");
			subtitle.append(getString(campaign));
		}
		if (getString(beanid).length() != 0) {
			if (subtitle.length() != 0) {
				subtitle.append(',');
			} else {
				subtitle.append(prefix);
			}
			subtitle.append(" Search: ");
			subtitle.append(getString(beanname));
		}
		if (getString(createdby).length() != 0) {
			if (subtitle.length() != 0) {
				subtitle.append(',');
			} else {
				subtitle.append(prefix);
			}
			subtitle.append(" Created by: ");
			subtitle.append(getString(createdbyname));
		}
		if (getString(repuserid).length() != 0) {
			if (subtitle.length() != 0) {
				subtitle.append(',');
			} else {
				subtitle.append(prefix);
			}
			subtitle.append(" Sales rep: ");
			subtitle.append(getString(repuseridname));
		}
		if (getString(begindate).length() != 0
				&& getString(enddate).length() != 0) {
			if (subtitle.length() != 0) {
				subtitle.append(',');
			} else {
				subtitle.append(prefix);
			}
			subtitle.append(' ');
			subtitle.append(getString(begindate));
			subtitle.append('-');
			subtitle.append(getString(enddate));
		} else if (getString(begindate).length() != 0) {
			if (subtitle.length() != 0) {
				subtitle.append(',');
			} else {
				subtitle.append(prefix);
			}
			subtitle.append(" From: ");
			subtitle.append(getString(begindate));
		} else if (getString(enddate).length() != 0) {
			if (subtitle.length() != 0) {
				subtitle.append(',');
			} else {
				subtitle.append(prefix);
			}
			subtitle.append(" To: ");
			subtitle.append(getString(enddate));
		}
		String chart = getString(charttype);
		if (chart.length() != 0) {
			subtitle.append(" Chart: ");
			subtitle.append(getString(enddate));
		}
		/*
		 * chart = getString(charttyper); if(chart.length()!=0) {
		 * subtitle.append(" Chart: "); subtitle.append(getString(enddate)); }
		 */
		return (subtitle.toString());
	}

	public String getReportType() {

		if (getReportSpec() != null) {
			String specName = getReportSpec().getName();
			if (specName.indexOf("Contact") >= 0) {
				return "Contacts";
			} else if (specName.indexOf("Compan") >= 0) {
				return "Companies";
			} else if (specName.indexOf("Note") >= 0) {
				return "Notes";
			} else if (specName.indexOf("Response") >= 0) {
				return "Responses";
			} else if (specName.indexOf("Opportunit") >= 0) {
				return "SimpleOpportunities";
			} else if (specName.indexOf("Process") >= 0) {
				return "Opportunities";
			} else if (specName.indexOf("Document") >= 0) {
				return "Documents";
			} else if (specName.indexOf("Order") >= 0
					|| specName.indexOf("Sale") >= 0) {
				return "Orders";
			} else if (specName.indexOf("Product") >= 0) {
				return "Products";
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public String getReportSpecName() {
		if (rpspec != null) {
			return rpspec.getName();
		}
		return _blank;
	}

	public String getReportSpecRightName() {
		if (rpspecr != null) {
			return rpspecr.getName();
		}
		return _blank;
	}

	public void setSearchBean(TableData row) {
		searchbean.parseRequest(row.toString());
		manualbean = true;
	}

	public GenRow getSearchBean() {
		return searchbean;
	}

	protected String setSearchBeanViewSpec(String searchtype) {
		String constraintRelation = null;
		if (searchtype.equals("Contacts")) {
			searchbean.setViewSpec("{reports/}ContactReportView");
			constraintRelation = "ContactContactGroups.ContactGroupGroup";
		} else if (searchtype.equals("Companies")) {
			searchbean.setViewSpec("{reports/}CompanyReportView");
			constraintRelation = "CompanyCompanyGroups.CompanyGroupGroup";
		} else if (searchtype.equals("Notes")) {
			searchbean.setViewSpec("{reports/}NoteReportView");
			constraintRelation = "NoteGroup";
		} else if (searchtype.equals("Responses")) {
			searchbean.setViewSpec("{reports/}ResponseReportView");
			constraintRelation = "ResponseNote.NoteGroup";
		} else if (searchtype.equals("Opportunities")) {
			searchbean.setViewSpec("{reports/}OpportunityReportView");
			constraintRelation = "OpportunityGroup";
		} else if (searchtype.equals("SimpleOpportunities")) {
			searchbean.setViewSpec("{reports/}SimpleOpportunityReportView");
			constraintRelation = "SimpleOpportunityGroup";
		} else if (searchtype.equals("Orders")) {
			searchbean.setViewSpec("{reports/}OrderReportView");
			constraintRelation = "OrderGroup";
		} else if (searchtype.equals("Documents")) {
			searchbean.setViewSpec("{reports/}DocumentReportView");
			constraintRelation = "DocumentGroup";
		}
		return (constraintRelation);
	}

	protected void generateSearchBeanStatement(String searchtype,
			String constraintRelation) {
		try {
			if (currentuser != null && constraintRelation != null
					&& searchbean.getViewSpec() != null) {
				currentuser.setConstraint(searchbean, constraintRelation);
				searchbean.setJndiName(this.getJndiName());

				searchbean.doAction(ActionBean.SEARCH);
				if (searchbean.getSearchCriteria().length() > 0) {
					setColumn(searchtype + "Search",
							searchbean.getSearchCriteria());
					setColumn(searchtype + "Query",
							searchbean.toString(true, false));
				}
			}
		} catch (Exception e) {
			setError("Search Bean generate statement error - ");
			setError(e.getMessage());
			setError("\r\n");
		}
	}

	public void setSearchBean() {
		searchbean.clear();
		searchbean.setConnection(ActionBean.connect());
		String constraintRelation = null;
		String searchtype = null;

		if (getString("BeanID").length() != 0) {
			searchbean.setViewSpec("BeanView");
			searchbean.setAction(ActionBean.SELECT);
			searchbean.setColumn("BeanID", getString("BeanID"));
			searchbean.doAction();
			if (searchbean.getString("Bean").length() == 0) {
				setError("Search Bean error - ");
				setError(searchbean.getError());
				setError("\r\n");
			}
			setColumn(beanname, searchbean.getString("Description"));
			String searchquery = searchbean.getString("Bean");
			searchtype = searchbean.getString("Type");
			searchbean.clear();

			constraintRelation = setSearchBeanViewSpec(searchtype);

			searchbean.setAction(ActionBean.SEARCH);
			searchbean.parseRequest(searchquery);
			searchbean.setParameter(ActionBean.CURRENTUSERID,
					this.getString(ActionBean.CURRENTUSERID));
		} else {
			searchbean.clear();

			searchtype = getReportType();
			if (searchtype != null) {
				constraintRelation = setSearchBeanViewSpec(searchtype);
			}
		}
		generateSearchBeanStatement(searchtype, constraintRelation);
	}

	public void parseRequest(HttpServletRequest request) {
		ActionBean.parseRequest(request, this, false);
		if (get("-chartopts") != null
				&& request.getParameterValues("-chartopts") != null) {
			put("-chartopts", request.getParameterValues("-chartopts"));
		}
		if (get("-charttype") != null) {
			this.setChartType((String) get("-charttype"));
		}
		put(_rowtrigger, get(rowtrigger));
		put(_coltrigger, get(coltrigger));
		put(_righttrigger, get(righttrigger));
		if (!this.getChartOptionSelected("third-axis")) {
			remove(_righttrigger);
			remove(righttrigger);
		}
		put(_filtertrigger, get(filtertrigger));
		setup();
	}

	public void setRequest(HttpServletRequest request) {
		super.setRequest(request);
		searchbean.setRequest(request);
	}

	public void parseRequest(String s) {
		ActionBean.parseRequest(s, this, false);
		setup();
	}
	
	public void parseRequest(String s, boolean allowEmpty) {
		ActionBean.parseRequest(s, this, allowEmpty);
		setup();
	}

	public void setCurrentUser(UserBean currentuser) {
		this.currentuser = currentuser;
	}

	public void setup() {
		if ("true".equals(get("-swaplraxis"))) {
			remove("-swaplraxis");

			Object origGroups = get("GroupID");
			Object origGroupsSet = get("GroupID" + set);
			put("GroupID", get("RightGroupID"));
			put("GroupID" + set, get("RightGroupID" + set));
			put("RightGroupID", (String) origGroups);
			put("RightGroupID" + set, (LinkedList<String>) origGroupsSet);

			String rt = (String) get(righttrigger);
			String _rt = (String) get(_righttrigger);
			String rs = (String) get("-reportSpecRight");
			put(righttrigger, get(coltrigger));
			put(_righttrigger, get(_coltrigger));
			put(reportspecright, get(reportspec));
			put(coltrigger, rt);
			put(_coltrigger, _rt);
			put(reportspec, rs);
		}

		setReportSpec(getString(reportspec));
		if (get(reportspecright) != null) {
			this.rpspecr = SpecManager
					.getReportSetSpec((String) get(reportspecright));
		} else {
			// make sure we clear this out when not in use.
			this.rpspecr = null;
		}

		if (rpspec != null) {
			try {
				if (ve == null) {
					ve = VelocityManager.getEngine();
				}
			} catch (Exception e) {
				setError("Error encountered at setSubsets: ");
				setError(ActionBean.writeStackTraceToString(e));
				setError("\r\n\r\n");
			}
			setTokenSets();
			if (!manualbean) {
				setSearchBean();
			} else {
				String searchtype = getReportType();
				generateSearchBeanStatement(searchtype,
						setSearchBeanViewSpec(searchtype));
			}
			setDates();
			setDateType();
			setList(ListType.Row);
			setList(ListType.Col);
			setList(ListType.Right);
			setList(ListType.Filter);
			setSubsets();

			if (get("-selectedPreset") != null) {
				String preset = (String) get("-selectedPreset");

				for (GraphPreset gp : presetSet) {
					if (gp.getPresetName().equals(preset)) {

						put("-chartopts", gp.getOptions());
						String chartType = gp.getChartType();
						if (getChartOptionSelected("stacking")
								|| getChartOptionSelected("stacking-percentage")) {
							if (chartType == "bar") {
								chartType = "stackedHorizontalBar";
							} else if (chartType == "column") {
								chartType = "stackedVerticalBar";
							}
						} else {
							if (chartType == "bar") {
								chartType = "horizontalBar";
							} else if (chartType == "column") {
								chartType = "verticalBar";
							}
						}
						this.setChartType(chartType);
					}
				}
			}
		}
	}

	protected void setDateType() {
		if (getString("ondatetype").length() != 0) {
			int dtindex = rpspec
					.getDateTypeIndexForValue(getString("ondatetype"));
			put(datefield, rpspec.getDateTypeField(dtindex));
			put(searchdatefield, rpspec.getDateTypeSearch(dtindex));
		}
	}

	public String getRowTriggerIsSelected(int i) {
		if (rpspec != null
				&& rpspec.isSelectedRowTrigger(i, getString(rowtrigger))) {
			return (selected);
		}
		return (blank);
	}

	public String getColTriggerIsSelected(int i) {
		if (rpspec != null
				&& rpspec.isSelectedColTrigger(i, getString(coltrigger))) {
			return (selected);
		}
		return (blank);
	}

	public String getRightTriggerIsSelected(int i) {
		if (rpspecr != null
				&& rpspecr.isSelectedColTrigger(i, getString(righttrigger))) {
			return (selected);
		}
		return (blank);
	}

	public String getFilterTriggerIsSelected(int i) {
		if (rpspec != null
				&& rpspec.isSelectedFilterTrigger(i, getString(filtertrigger))) {
			return (selected);
		}
		return (blank);
	}

	/*
	 * this needed to be changed as ReportSetContactStatusDuration was being
	 * taged as ReportSetContactStatus hasValue does and equals and a indexOf so
	 * is not good for this situration
	 */
	public String getIsSelected(String name, String value) {
		String spec = getString(name);
		if (spec.equals(value)) {
			return selected;
		}

		// if (hasValue(name, value)) {
		// return (selected);
		// }
		return (blank);
	}

	public String getIsChecked(String name, String value) {
		if (hasValue(name, value)) {
			return (checked);
		}
		return (blank);
	}

	// TODO - used for ex to see if a string of checkboxes is checked.
	@SuppressWarnings("unchecked")
	public boolean hasValue(String name, String value) {
		if (containsKey(name)) {
			String[] ll = null;
			if (get(name) instanceof String[]) {
				ll = (String[]) get(name);
			} else {
				ll = new String[1];
				ll[0] = (String) get(name);
				if (ll[0].indexOf("%2B") >= 0) ll[0] = StringUtil.urlDecode(ll[0]);
				if (ll[0].indexOf("+") >= 0) ll = ll[0].split("[\\+]");
			}
			if (ll != null) {
				for (int i = 0; i < ll.length; i++) {
					if (ll[i] != null && ll[i].equals(value)) {
						return (true);
					}
				}
			}
		} else if (containsKey(name + set)) {
				LinkedList<String> ll = (LinkedList<String>) get(name + set);
				if (ll != null) {
					for (int i = 0; i < ll.size(); i++) {
						if (ll.get(i).equals(value)) {
							return (true);
						}
					}
				}
		} else if (containsKey(name + url)) {
			String[] ll = null;
			if (get(name + url) instanceof String[]) {
				ll = (String[]) get(name + url);
			} else {
				ll = new String[1];
				ll[0] = (String) get(name + url);
				if (ll[0].indexOf("%2B") >= 0) ll[0] = StringUtil.urlDecode(ll[0]);
				if (ll[0].indexOf("+") >= 0) ll = ll[0].split("[\\+]");
			}
			if (ll != null) {
				for (int i = 0; i < ll.length; i++) {
					if (ll[i] != null && ll[i].equals(value)) {
						return (true);
					}
				}
			}
		} else if (getString(name).equals(value)
				|| getString(name).indexOf(value) >= 0) {
			return (true);
		}
		return (false);
	}

	public void setTokenSets() {
		String tokens = getString(tokensets);
		if (tokens.length() != 0) {
			StringTokenizer st = new StringTokenizer(tokens, ",");
			while (st.hasMoreTokens()) {
				setTokenSet(st.nextToken());
			}
		}
	}

	public void setTokenSet(String token) {
		String tokenvalues = getString(token);
		if (tokenvalues.length() != 0) {
			StringTokenizer st = new StringTokenizer(tokenvalues, "+");
			LinkedList<String> valueset = new LinkedList<String>();
			while (st.hasMoreTokens()) {
				valueset.add(st.nextToken());
			}
			put(token + set, valueset);
			put(token + url, StringUtil.urlEncode(tokenvalues));
		}
	}

	/**
	 * TODO - Confirm
	 * 
	 * @comment depreciated - or will be soon
	 */
	public int getChartWidth() {
		String chartwidthstring = getString(chartwidth);
		if (chartwidthstring.length() == 0) {
			int index = rpspec.getChartIndexForTrigger(
					(String) get(ReportSetSpec._coltrigger),
					(String) get(ReportSetSpec._rowtrigger));
			if (index >= 0) {
				String minwidth = rpspec.getChartMinWidth(index);
				String colwidth = rpspec.getChartColumnWidth(index);
				int col = Integer.parseInt(colwidth);
				int min = Integer.parseInt(minwidth);
				cwidth = col * getColumnCount();
				if (cwidth < min) {
					cwidth = min;
				}
			} else {
				cwidth = 800;
			}

			// put(chartwidth,String.valueOf(cwidth));
		} else {
			cwidth = Integer.parseInt(chartwidthstring);
		}
		return (cwidth);
	}

	/**
	 * TODO - Confirm
	 * 
	 * @comment depreciated - or will be soon
	 */
	public int getChartHeight() {
		String chartheightstring = getString(chartheight);
		if (chartheightstring.length() == 0) {
			int index = rpspec.getChartIndexForTrigger(
					(String) get(ReportSetSpec._coltrigger),
					(String) get(ReportSetSpec._rowtrigger));
			if (index >= 0) {
				String minheight = rpspec.getChartMinHeight(index);
				String rowheight = rpspec.getChartRowHeight(index);
				int row = Integer.parseInt(rowheight);
				int min = Integer.parseInt(minheight);
				cheight = row * report.size();
				if (cheight < min) {
					cheight = min;
				}
			} else {
				cheight = 500;
			}

			// put(chartheight,String.valueOf(cheight));
		} else {
			cheight = Integer.parseInt(chartheightstring);
		}
		return (cheight);
	}

	static enum Axis {
		Left, Right
	}

	public String getChartType() {
		return getChartType(Axis.Left);
	}

	public String getChartType(Axis a) {
		String ctype = getString(a == Axis.Left ? charttype : charttyper);
		if (ctype.length() != 0) {
			if (ctype.equals(CHART_TYPE_PIE)) {
				setPie();
			}
		} else if (report != null && report.size() == 1) {
			ctype = CHART_TYPE_PIE;
			setPie();
		} else if (rpspec != null && a == Axis.Left) {
			int index = rpspec.getChartIndexForTrigger(
					(String) get(ReportSetSpec._coltrigger),
					(String) get(ReportSetSpec._rowtrigger));
			ctype = rpspec.getChartType(index);
		} else if (rpspecr != null && a == Axis.Right) {
			int index = rpspecr.getChartIndexForTrigger(
					(String) get(ReportSetSpec._coltrigger),
					(String) get(ReportSetSpec._rowtrigger));
			ctype = rpspecr.getChartType(index);
		}
		return (ctype);
	}

	public void setReportSpec(String rs) {
		if (rs != null && rs.length() != 0) {
			rpspec = SpecManager.getReportSetSpec(rs);
		}
	}

	public boolean needsColTriggerRefresh(int index) {
		if (index != colvar) {
			if (rpspec.isListSubset(index)) {
				return (true);
			} else if (hasColSubset()) {
				return (true);
			}
		}
		return (false);
	}

	public boolean needsRightTriggerRefresh(int index) {
		if (index != rightvar) {
			if (rpspecr.isListSubset(index)) {
				return (true);
			} else if (hasRightSubset()) {
				return (true);
			}
		}
		return (false);
	}

	public boolean needsFilterTriggerRefresh(int index) {
		if (index != filtervar) {
			if (rpspec.isListSubset(index)) {
				return (true);
			} else if (hasFilterSubset()) {
				return (true);
			}
		}
		return (false);
	}

	public boolean needsRowTriggerRefresh(int index) {
		if (index != rowvar) {
			if (rpspec.isListSubset(index)) {
				return (true);
			} else if (hasRowSubset()) {
				return (true);
			}
		}
		return (false);
	}

	public boolean hasColSubset() {
		if (colvar > -1) {
			return (rpspec.isListSubset(colvar));
		}
		return (false);
	}

	public boolean hasRightSubset() {
		if (rightvar > -1) {
			return (rpspecr.isListSubset(rightvar));
		}
		return (false);
	}

	public boolean hasFilterSubset() {
		if (filtervar > -1) {
			return (rpspec.isListSubset(filtervar));
		}
		return (false);
	}

	public boolean hasRowSubset() {
		if (rowvar > -1) {
			return (rpspec.isListSubset(rowvar));
		}
		return (false);
	}

	public String getColSubsetName() {
		if (colvar > -1) {
			if (rpspecr != null) {
				return (rpspec.getModuleType(true) + " - " + rpspec
						.getListTitle(colvar));
			}
			return (rpspec.getListTitle(colvar));
		}
		return (ActionBean._blank);
	}

	public String getRightSubsetName() {
		if (rightvar > -1) {
			return (rpspecr.getModuleType(true) + " - " + rpspecr
					.getListTitle(rightvar));
		}
		return (ActionBean._blank);
	}

	public String getFilterSubsetName() {
		if (colvar > -1) {
			return (rpspec.getListTitle(filtervar));
		}
		return (ActionBean._blank);
	}

	public String getRowSubsetName() {
		if (rowvar > -1) {
			return (rpspec.getListTitle(rowvar));
		}
		return (ActionBean._blank);
	}

	public String getColSubsetInputName() {
		if (colvar > -1) {
			return (rpspec.getListSubset(colvar));
		}
		return (ActionBean._blank);
	}

	public String getRightSubsetInputName() {
		if (rightvar > -1) {
			return (rpspecr.getListSubset(rightvar));
		}
		return (ActionBean._blank);
	}

	public String getFilterSubsetInputName() {
		if (filtervar > -1) {
			return (rpspec.getListSubset(filtervar));
		}
		return (ActionBean._blank);
	}

	public String getRowSubsetInputName() {
		if (rowvar > -1) {
			return (rpspec.getListSubset(rowvar));
		}
		return (ActionBean._blank);
	}

	public String getColSubsetItemName(int i) {
		if (colvars[names] != null) {
			return (colvars[names][i]);
		}
		return (ActionBean._blank);
	}

	public String getFilterSubsetItemName(int i) {
		if (filtervars[names] != null) {
			return (filtervars[names][i]);
		}
		return (ActionBean._blank);
	}

	public String getRightSubsetItemName(int i) {
		if (rightvars[names] != null) {
			return (rightvars[names][i]);
		}
		return (ActionBean._blank);
	}

	public String getColSubsetItemValue(int i) {
		if (colvars[values] != null) {
			return (colvars[values][i]);
		}
		return (ActionBean._blank);
	}

	public String getColSubsetItemGroup(int i) {
		if (colvars.length > 3 && colvars[group] != null) {
			return (colvars[group][i]);
		}
		return (ActionBean._blank);
	}

	public String getRightSubsetItemValue(int i) {
		if (rightvars[values] != null) {
			return (rightvars[values][i]);
		}
		return (ActionBean._blank);
	}

	public String getRightSubsetItemGroup(int i) {
		if (rightvars.length > 3 && rightvars[group] != null) {
			return (rightvars[group][i]);
		}
		return (ActionBean._blank);
	}

	public int getColSubsetSize() {
		if (colvars != null && colvars[values] != null) {
			return (colvars[values].length);
		}
		return (0);
	}

	public int getRightSubsetSize() {
		if (rightvars != null && rightvars[values] != null) {
			return (rightvars[values].length);
		}
		return (0);
	}

	public String getRowSubsetItemName(int i) {
		if (rowvars[names] != null) {
			return (rowvars[names][i]);
		}
		return (ActionBean._blank);
	}

	public String getRowSubsetItemValue(int i) {
		if (rowvars[values] != null) {
			return (rowvars[values][i]);
		}
		return (ActionBean._blank);
	}

	public String getRowSubsetItemGroup(int i) {
		if (filtervars.length > 3 && rowvars[group] != null) {
			return (rowvars[group][i]);
		}
		return (ActionBean._blank);
	}

	public String getFilterSubsetItemValue(int i) {
		if (filtervars[values] != null) {
			return (filtervars[values][i]);
		}
		return (ActionBean._blank);
	}

	public String getFilterSubsetItemGroup(int i) {
		if (filtervars.length > 3 && filtervars[group] != null) {
			return (filtervars[group][i]);
		}
		return (ActionBean._blank);
	}

	public boolean isColSubsetItemSelected(int i) {
		return (contains(colsubset, colvars[values][i]));
	}

	public boolean isRightSubsetItemSelected(int i) {
		return (contains(rightsubset, rightvars[values][i]));
	}

	public boolean isRowSubsetItemSelected(int i) {
		return (contains(rowsubset, rowvars[values][i]));
	}

	public boolean isFilterSubsetItemSelected(int i) {
		return (contains(filtersubset, filtervars[values][i]));
	}

	public int getFilterSubsetSize() {
		if (filtervars != null && filtervars[values] != null) {
			return (filtervars[values].length);
		}
		return (0);
	}

	public int getRowSubsetSize() {
		if (rowvars != null && rowvars[values] != null) {
			return (rowvars[values].length);
		}
		return (0);
	}

	public boolean isSubsetDependentOn(String n) {
		String dep = null;
		if (hasColSubset()) {
			dep = rpspec.getListSubsetDep(colvar);
			if (dep != null && dep.indexOf(n) >= 0) {
				return (true);
			}
		}
		if (hasRightSubset()) {
			dep = rpspecr.getListSubsetDep(rightvar);
			if (dep != null && dep.indexOf(n) >= 0) {
				return (true);
			}
		}
		if (hasRowSubset()) {
			dep = rpspec.getListSubsetDep(rowvar);
			if (dep != null && dep.indexOf(n) >= 0) {
				return (true);
			}
		}
		return (false);
	}

	public void setSubsets() {
		if (rpspec != null) {
			String subset = ActionBean._blank;
			if (hasColSubset()) {
				subset = getString(rpspec.getListSubset(colvar));
				if (subset.length() != 0) {
					colsubset = getTokenArray(subset);
				} else {
					colsubset = null;
				}
			}
			if (rpspecr != null && hasRightSubset()) {
				subset = getString(rpspecr.getListSubset(rightvar));
				if (subset.length() != 0) {
					rightsubset = getTokenArray(subset);
				} else {
					rightsubset = null;
				}
			}
			if (hasRowSubset()) {
				subset = getString(rpspec.getListSubset(rowvar));
				if (subset.length() != 0) {
					rowsubset = getTokenArray(subset);
				} else {
					rowsubset = null;
				}
			}
			if (hasFilterSubset()) {
				subset = getString(rpspec.getListSubset(filtervar));
				if (subset.length() != 0) {
					filtersubset = getTokenArray(subset);
				} else {
					filtersubset = null;
				}
			}
		}
	}

	public String[] getTokenArray(String subset) {
		String[] set = null;
		if (subset != null && subset.length() != 0) {
			List<String> tokens = new ArrayList<String>();
			StringTokenizer st = new StringTokenizer(subset, ActionBean._plus);

			while (st.hasMoreTokens()) {
				tokens.add(st.nextToken());
			}

			// set = (String[])tokens.toArray();

			// int size = StringUtil.occurrencesOf(subset,ActionBean._plus);
			set = new String[tokens.size()];
			// StringTokenizer st = new StringTokenizer(subset,
			// ActionBean._plus);
			for (int i = 0; i < set.length; i++) {
				set[i] = tokens.get(i);
			}
		}
		return (set);
	}

	public ReportSetSpec getReportSpec() {
		return (rpspec);
	}

	public ReportSetSpec getReportSpecRight() {
		return (rpspecr);
	}

	void setDates() {
		String beginstring = (String) get(begindate);
		String endstring = (String) get(enddate);
		logger.debug("setDates({},{})", beginstring, endstring);
		if (beginstring != null) {
			if (beginstring.indexOf(LAST) == 0 && beginstring.indexOf(DAYS) > 0) {
				put(begin,
						getDateString(CalendarFactory
								.getLastNumDaysCal(beginstring)));
				if (!beginstring.endsWith(WITHTODAY)) {
					put(end, getDateString(CalendarFactory.getYesterdayCal()));
				} else {
					put(end, getDateString(GregorianCalendar.getInstance()));
				}
			} else if (beginstring.indexOf(LAST) == 0
					&& beginstring.indexOf(WEEKS) > 0) {
				put(begin,
						getDateString(CalendarFactory
								.getLastNumWeeksCal(beginstring)));
				put(end,
						getDateString(CalendarFactory
								.getLastNumWeeksInitCal(beginstring)));
			} else if (beginstring.indexOf(LAST) == 0
					&& beginstring.indexOf(MONTHS) > 0) {
				put(begin,
						getDateString(CalendarFactory
								.getLastNumMonthsCal(beginstring)));
				put(end,
						getDateString(CalendarFactory
								.getLastNumMonthsInitCal(beginstring)));
			} else if (beginstring.indexOf(NEXT) == 0
					&& beginstring.indexOf(DAYS) > 0) {
				put(begin, getDateString(CalendarFactory.getTomorrowCal()));
				put(end,
						getDateString(CalendarFactory
								.getNextNumDaysCal(beginstring)));
			} else if (beginstring.indexOf(NEXT) == 0
					&& beginstring.indexOf(WEEKS) > 0) {
				put(begin,
						getDateString(CalendarFactory
								.getLastNumWeeksInitCal(beginstring)));
				put(end,
						getDateString(CalendarFactory
								.getNextNumWeeksCal(beginstring)));
			} else if (beginstring.indexOf(NEXT) == 0
					&& beginstring.indexOf(MONTHS) > 0) {
				put(begin,
						getDateString(CalendarFactory
								.getLastNumMonthsInitCal(beginstring)));
				put(end,
						getDateString(CalendarFactory
								.getNextNumMonthsCal(beginstring)));

			} else if (beginstring.indexOf(LAST) == 0
					&& beginstring.indexOf(QUARTERS) > 0) {

				put(begin,
						getDateString(CalendarFactory
								.getLastNumQuartersCal(beginstring)));
				put(end,
						getDateString(CalendarFactory
								.getLastNumQuartersInitCal(beginstring)));

			} else if (beginstring.indexOf(LAST) == 0
					&& beginstring.indexOf(YEARS) > 0) {
				if (beginstring.indexOf(FINYEARS) > 0) {
					put(begin,
							getDateString(CalendarFactory
									.getLastNumFinYearsCal(beginstring)));
				} else {
					put(begin,
							getDateString(CalendarFactory
									.getLastNumFinYearsCal(beginstring)));
				}
				put(end,
						getDateString(CalendarFactory
								.getLastNumYearsInitCal(beginstring)));
			} else if (beginstring.indexOf(NEXT) == 0
					&& beginstring.indexOf(QUARTERS) > 0) {
				put(begin,
						getDateString(CalendarFactory
								.getNextNumQuartersInitCal(beginstring)));
				put(end,
						getDateString(CalendarFactory
								.getNextNumQuartersCal(beginstring)));
			} else if (beginstring.indexOf(NEXT) == 0
					&& beginstring.indexOf(YEARS) > 0) {
				put(begin,
						getDateString(CalendarFactory
								.getNextNumYearsInitCal(beginstring)));
				if (beginstring.indexOf(FINYEARS) > 0) {
					put(end,
							getDateString(CalendarFactory
									.getNextNumFinYearsCal(beginstring)));
				} else {
					put(end,
							getDateString(CalendarFactory
									.getNextNumYearsCal(beginstring)));
				}
			} else if (beginstring.equals(TODAY)) {
				GregorianCalendar cal1 = new GregorianCalendar();
				put(begin, getDateString(cal1));
				put(end, getDateString(cal1));
			} else if (beginstring.equals(CURRENTMONTH)) {
				put(begin, getDateString(CalendarFactory.getStartOfMonthCal()));
				put(end, getDateString(CalendarFactory.getEndOfMonthCal()));
			} else if (beginstring.equals(CURRENTYEAR)) {
				put(begin, getDateString(CalendarFactory.getStartOfYearCal()));
				put(end, getDateString(CalendarFactory.getEndOfYearCal()));
			} else if (beginstring.equals(CURRENTFINYEAR)) {
				put(begin,
						getDateString(CalendarFactory.getStartOfFinYearCal()));
				put(end, getDateString(CalendarFactory.getEndOfFinYearCal()));
			} else if (beginstring.equals(CURRENTWEEK)) {
				put(begin, getDateString(CalendarFactory.getStartOfWeekCal()));
				put(end, getDateString(CalendarFactory.getEndOfWeekCal()));
			} else if (beginstring.equals(LASTWEEK)) {
				put(begin,
						getDateString(CalendarFactory.getStartOfLastWeekCal()));
				put(end, getDateString(CalendarFactory.getEndOfLastWeekCal()));
			} else if (beginstring.equals(LASTMONTH)) {
				put(begin,
						getDateString(CalendarFactory.getStartOfLastMonthCal()));
				put(end, getDateString(CalendarFactory.getEndOfLastMonthCal()));
			} else if (beginstring.equals(LASTYEAR)) {
				put(begin,
						getDateString(CalendarFactory.getStartOfLastYearCal()));
				put(end, getDateString(CalendarFactory.getEndOfLastYearCal()));
			} else if (beginstring.equals(LASTYEAR)) {
				put(begin,
						getDateString(CalendarFactory
								.getStartOfLastFinYearCal()));
				put(end,
						getDateString(CalendarFactory.getEndOfLastFinYearCal()));
			} else {
				put(begin, beginstring);
				put(end, get(enddate));
				put(altend, get(enddate));
			}
		} else {
			put(end, get(enddate));
			put(altend, get(enddate));
		}

		if (endstring != null && endstring.equals(ActionBean.TODAY)) {
			GregorianCalendar cal1 = new GregorianCalendar();
			String dstr = getDateString(cal1);
			put(end, dstr);
			put(enddate, dstr);
		}
	}

	public void doAction() {
		chartData = null;
		chartJson = null;
		if (rpspec != null) {
			try {
				setData();
			} catch (Exception e) {
				setError("Error encountered at doAction: ");
				setError(ActionBean.writeStackTraceToString(e));
				setError("\r\n\r\n");
			}
		}
		remove("reportUnits");
	}

	public static enum ListType {
		Col, Row, Right, Filter
	}

	public void setList(boolean isrow) {
		if (isrow) {
			setList(ListType.Row);
		} else {
			setList(ListType.Col);
		}
	}

	public void setList(ListType l) {
		String trigger = null;
		ReportSetSpec rpspec = l == ListType.Right ? this.rpspecr : this.rpspec;
		switch (l) {
		case Row:
			trigger = getString(rowtrigger);
			break;
		case Col:
			trigger = getString(coltrigger);
			break;
		case Filter:
			trigger = getString(filtertrigger);

			break;
		case Right:
			trigger = getString(righttrigger);
			rpspec = this.rpspecr;
		default:
		}
		if (!NONE.equals(trigger) && rpspec != null) {
			int index = -1;

			if (trigger.length() != 0) {

				index = rpspec.getListIndexForTrigger(trigger);
			} else {
				switch (l) {
				case Row:
					index = rpspec.getDefaultListIndex(ReportSetSpec.row);
					put(rowtrigger, rpspec.getListTrigger(index));
					break;
				case Col:
					index = rpspec.getDefaultListIndex(ReportSetSpec.column);
					put(coltrigger, rpspec.getListTrigger(index));
					break;
				case Filter:
					index = rpspec.getDefaultListIndex(ReportSetSpec.filter);
					put(filtertrigger, rpspec.getListTrigger(index));
					break;
				case Right:
					// put(righttrigger,rpspec.getListTrigger(rpspec.getDefaultListIndex(ReportSetSpec.right)));
					// I suspect this will be fine as we won't want a default
					// right trigger.
					put(righttrigger, "");
				default:
				}

				/*
				 * if(isrow) { index =
				 * rpspec.getDefaultListIndex(ReportSetSpec.row); } else { index
				 * = rpspec.getDefaultListIndex(ReportSetSpec.column); }
				 * 
				 * trigger = rpspec.getListTrigger(index);
				 * 
				 * if(isrow) { put(rowtrigger,trigger); } else {
				 * put(coltrigger,trigger); }
				 */
			}

			switch (l) {
			case Row:
				rowvar = index;
				break;
			case Col:
				colvar = index;
				break;
			case Filter:
				filtervar = index;
				break;
			case Right:
				rightvar = index;
			default:
			}
			/*
			 * if(isrow) { rowvar = index; } else { colvar = index; }
			 */

			if (index >= 0) {
				setList(l, index);
			}
		}
	}

	public void setList(boolean isrow, int index) {
		if (isrow) {
			setList(ListType.Row, index);
		} else {
			setList(ListType.Col, index);
		}
	}

	public void setList(ListType l, int index) {
		ReportSetSpec rpspec = l == ListType.Right ? this.rpspecr : this.rpspec;
		String type = rpspec.getListType(index);
		logger.trace("loading list for type {} - index {}", type, index);

		if (type.equals(ReportSetSpec.monthrange)) {
			switch (l) {
			case Row:
				rowvars = getRange(Calendar.MONTH);
				break;
			case Col:
				colvars = getRange(Calendar.MONTH);
				break;
			case Filter:
				filtervars = getRange(Calendar.MONTH);
				break;
			case Right:
				rightvars = getRange(Calendar.MONTH);
			default:
			}
			/*
			 * if(isrow) { rowvars = getRange(Calendar.MONTH); } else { colvars
			 * = getRange(Calendar.MONTH); }
			 */

		} else if (type.equals(ReportSetSpec.finyearrange)) {
			switch (l) {
			case Row:
				rowvars = getRange(CALENDAR_FINYEAR);
				break;
			case Col:
				colvars = getRange(CALENDAR_FINYEAR);
				break;
			case Filter:
				filtervars = getRange(CALENDAR_FINYEAR);
				break;
			case Right:
				rightvars = getRange(CALENDAR_FINYEAR);
			default:
			}

		} else if (type.equals(ReportSetSpec.yearrange)) {
			switch (l) {
			case Row:
				rowvars = getRange(Calendar.YEAR);
				break;
			case Col:
				colvars = getRange(Calendar.YEAR);
				break;
			case Filter:
				filtervars = getRange(Calendar.YEAR);
				break;
			case Right:
				rightvars = getRange(Calendar.YEAR);
			default:
			}

		} else if (type.equals(ReportSetSpec.fortnightrange)) {
			switch (l) {
			case Row:
				rowvars = getRange(CALENDAR_FORTNIGHT);
				break;
			case Col:
				colvars = getRange(CALENDAR_FORTNIGHT);
				break;
			case Filter:
				filtervars = getRange(CALENDAR_FORTNIGHT);
				break;
			case Right:
				rightvars = getRange(CALENDAR_FORTNIGHT);
			default:
			}

			/*
			 * if(isrow) { rowvars = getRange(Calendar.YEAR); } else { colvars =
			 * getRange(Calendar.YEAR); }
			 */
		} else if (type.equals(ReportSetSpec.weekrange)) {
			switch (l) {
			case Row:
				rowvars = getRange(Calendar.WEEK_OF_YEAR);
				break;
			case Col:
				colvars = getRange(Calendar.WEEK_OF_YEAR);
				break;
			case Filter:
				filtervars = getRange(Calendar.WEEK_OF_YEAR);
				break;
			case Right:
				rightvars = getRange(Calendar.WEEK_OF_YEAR);
				;
			default:
			}
			/*
			 * if(isrow) { rowvars = getRange(Calendar.WEEK_OF_YEAR); } else {
			 * colvars = getRange(Calendar.WEEK_OF_YEAR); }
			 */
		} else if (type.equals(ReportSetSpec.dayrange)) {
			switch (l) {
			case Row:
				rowvars = getRange(Calendar.DAY_OF_MONTH);
				break;
			case Col:
				colvars = getRange(Calendar.DAY_OF_MONTH);
				break;
			case Filter:
				filtervars = getRange(Calendar.DAY_OF_MONTH);
				break;
			case Right:
				rightvars = getRange(Calendar.DAY_OF_MONTH);
				;
			default:
			}/*
			 * if(isrow) { rowvars = getRange(Calendar.DAY_OF_MONTH); } else {
			 * colvars = getRange(Calendar.DAY_OF_MONTH); }
			 */
		} else if (type.equals(ReportSetSpec.quarterrange)) {
			switch (l) {
			case Row:
				rowvars = getRange(CALENDAR_QUARTER);
				break;
			case Col:
				colvars = getRange(CALENDAR_QUARTER);
				break;
			case Filter:
				filtervars = getRange(CALENDAR_QUARTER);
				break;
			case Right:
				rightvars = getRange(CALENDAR_QUARTER);
			default:
			}/*
			 * if(isrow) { rowvars = getRange(Calendar.DAY_OF_MONTH); } else {
			 * colvars = getRange(Calendar.DAY_OF_MONTH); }
			 */
		} else if (type.equals(ReportSetSpec.statement)) {
			logger.debug("getting statement for index {}", index);
			switch (l) {
			case Row:
				rowvars = getListDataFormDB(index, ListType.Row);
				break;
			case Col:
				colvars = getListDataFormDB(index, ListType.Col);
				break;
			case Filter:
				filtervars = getListDataFormDB(index, ListType.Filter);
				break;
			case Right:
				rightvars = getListDataFormDB(index, ListType.Right);
			default:
			}/*
			 * if(isrow) { rowvars = getListDataFormDB(index); } else { colvars
			 * = getListDataFormDB(index); }
			 */
		}
	}

	public String[][] getListDataFormDB(int index) {
		return getListDataFormDB(index, ListType.Col);
	}

	protected static String hashStatement(String text) {
		String hash = null;
		try {
			hash = SecurityFactory.hashString(text);
			// logger.log(Level.INFO,"Generated Hash value : "+ hash);
		} catch (Exception e1) {
			logger.error("Hashing failure", e1);
			hash = text;
		}
		return hash;
	}

	public String[][] getListDataFormDB(int index, ListType l) {
		ReportSetSpec rpspec = l == ListType.Right ? this.rpspecr : this.rpspec;

		String[][] vars = null;
		String stmt = evaluateStatement(ReportSetSpec._trigger,
				rpspec.getListStatement(index), l);

		logger.debug("getListDataFormDB index [{}], type [{}]", index, l.name());
		logger.debug(stmt);
		if (stmt.length() != 0) {
			/*
			 * statement cache disabled as data might change. String hash =
			 * hashStatement(stmt.toString()); Cache c =
			 * InitServlet.getCache("reportingCache"); if(c != null) { Element e
			 * = null;
			 * logger.log(Level.INFO,"List Cache active, checking for "+stmt
			 * .toString()); if((e = c.get(hash)) != null) {
			 * logger.log(Level.INFO,"Cache hit for list key " + hash); vars =
			 * (String[][])e.getValue(); //use getObjectValue if using
			 * non-serializable objects. } else {
			 * logger.log(Level.INFO,"Cache miss for list key " + hash); vars =
			 * getListArray(stmt, rpspec.getListLabel(index),
			 * rpspec.getListValue(index)); c.put(new Element(hash, vars));
			 * logger.log(Level.INFO,"Added vars to list cache" + hash); }
			 * return vars; }
			 */
			vars = getListArray(stmt, rpspec.getListLabel(index),
					rpspec.getListValue(index), rpspec.getListGroup(index));
		}
		return (vars);
	}

	private UtilityResource.Progress progress = null;

	public void setProgress(UtilityResource.Progress p) {
		progress = p;
	}

	protected void setProgress(int item, int total) {
		logger.debug("setProgress(item={},total={})", item, total);
		if (progress != null && total != 0) {
			progress.count = item;
			progress.total = total;
			progress.p = ((double)item / (double)total) * 100;
		}
	}

	public void setData() {
		logger.debug("setData()"); 

		// otherwise multiple doAction() calls will add additional data.
		this.report.clear();
		this.rightreport.clear();

		String tokenname = rpspec.getListValue(rowvar);
		String datestart = rpspec.getListRangeStart(rowvar);
		String dateend = rpspec.getListRangeEnd(rowvar);
		String type = rpspec.getListType(rowvar);

		boolean doFilter = hasFilterSubset();
		if (doFilter) {
			put(rpspec.getListValue(filtervar) + set, filtersubset);
		}

		ArrayList<String> rowlist = new ArrayList<String>();
		if (rowvars[values] != null) {
			logger.debug("rowvars[values] != null"); 
			int tot = rowvars[names].length;
			for (int i = 0; i < rowvars[names].length; i++) {
				setProgress(i, tot);
				if (rowsubset == null
						|| contains(rowsubset, rowvars[values][i])) {
					if (type.indexOf("range") > 0) {
						put(datestart, rowvars[values][i]);
						put(dateend, rowvars[values][i + 1]);
						put(altend, altdates[i + 1]);
					} else {
						put(tokenname, rowvars[values][i]);
					}
					setRowFromDB();
					rowlist.add(rowvars[names][i]);
				}
			}

			// clean up inter values
			if (type.indexOf("range") > 0) {
				remove(datestart);
				remove(dateend);
				remove(altend);
			} else {
				remove(tokenname);
			}
			// System.out.println("schecking rowlist size");
			if (rowlist.size() != 0) {
				// System.out.println("setting rowtitles");
				rowtitles = new String[rowlist.size()];
				rowtitles = (String[]) rowlist.toArray(rowtitles);
			} // else {
				// System.out.println("rowlist size was zero");
				// }
		} else {
			setProgress(1, 2);
			setRowFromDB();
			// System.out.println("set row from DB");
		}

		populateRowCount();
		populateColumnCount();
		populateRightCount();
		populateColumnSummary();
		populateRightSummary();
		addTotal();
		addRightTotal();
		if (doFilter) {
			remove(rpspec.getListValue(filtervar) + set);
		}
	}

	public void populateColumnSummary() {
		if ("AVG".equals(endtype)) {
			addColumnAverage();
			addRowAverageCalc();
		} else {
			addColumnTotals();
		}
	}

	public void populateRightSummary() {
		/*
		 * TODO - do we need this? I say.. no. if("AVG".equals(endtype)) {
		 * addRightAverage(); addRowAverageCalc(); } else { addRightTotals(); }
		 */
		addRightTotals();
	}

	public void populateRowCount() {
		rowcount = 1;

		if (rowtitles != null && rowtitles.length != 0) {
			rowcount = rowtitles.length;
		}
	}

	public void populateColumnCount() {
		colcount = 1;

		if (coltitles != null) {
			colcount = coltitles.length;
		}
	}

	void populateRightCount() {
		rightcount = 1;

		if (righttitles != null) {
			rightcount = righttitles.length;
		}
	}

	private void loadCells(List<CellData> cells) {
		/* cache variables */
		Cache c = InitServlet.getCache("reportingCache");
		Element e = null;

		List<CellData> toLoad;

		if (c == null) {
			/* if we don't have a cache, load everything */
			toLoad = cells;
		} else {
			toLoad = new ArrayList<CellData>();
			for (CellData cell : cells) {
				if (cell.cache) {
					if ((e = c.get(cell.hash)) != null) {
						/* value should be cached and exists within the cache */
						cell.val = (String) e.getValue();
					} else {
						/*
						 * value should be cached, but doesn't exist in the
						 * cache
						 */
						toLoad.add(cell);
					}
				} else {
					/* value should not be cached, lets load it */
					toLoad.add(cell);
				}
			}
		}
		/* get a non-cached data from the database */
		if (toLoad.size() > 0) {
			/* concatenates the statements for more efficient loading */
			String stmt = getCellsStatement(toLoad);
			/* retrieve the results, do not use the cache */
			String[] result = getResultArray(stmt, ListType.Row, true);
			if (result != null && result.length == toLoad.size()) {
				for (int i = 0; i < toLoad.size(); i++) {
					toLoad.get(i).val = result[i];
					/*
					 * if we have a cache and the result should be cached, lets
					 * cache it
					 */
					if (c != null && toLoad.get(i).cache) {
						c.put(new Element(toLoad.get(i).hash, toLoad.get(i).val));
					}
				}
			} else {
				/* something has gone wrong with our generation statement */
				logger.error("cells statement != result length");
			}
		}
	}

	/**
	 * 
	 */
	public void setRowFromDB() {
		logger.debug("setRowFromDB()");
		/* full list of cell data */
		List<CellData> cells = getCellStatements(ListType.Col);
		/* list of cell data that requires a database trip - to be calculated */
		loadCells(cells);

		/* these will now all be populated */
		String[] result = new String[cells.size()];
		String[] labels = new String[cells.size()];
		// int[] types = new int[cells.size()];
		for (int i = 0; i < cells.size(); i++) {
			result[i] = cells.get(i).val;
			labels[i] = cells.get(i).name;
		}
		/*
		 * it is highly likely that the coltitles and labels will be different,
		 * as the coltitles are loaded in getResultArray() for backwards
		 * compatibility with older code. Could probably be removed in that
		 * method and always set the labels here, there's probably some
		 * optimisation that could be done here memory wise as the same labels
		 * would exist a few times in memory with the same values.
		 */
		if (coltitles == null /* unlikely */|| coltitles.length != labels.length) {
			coltitles = labels;
		}
		if (cells.size() > 0) {
			populateRowSummary(result);
			report.add(result);
		} else {
			if (debug) {
				setError("Both base(row) and right(col) variables list has no values");
				setError("\r\n\r\n");
			}
			logger.debug("Both base(row) and right(col) variables list has no values");
		}

		if (this.rpspecr != null) {
			cells.clear();
			cells = getCellStatements(ListType.Right);
			loadCells(cells);

			/* these will now all be populated */
			result = new String[cells.size()];
			labels = new String[cells.size()];

			for (int i = 0; i < cells.size(); i++) {
				result[i] = cells.get(i).val;
				labels[i] = cells.get(i).name;
			}

			if (righttitles == null /* unlikely */
					|| righttitles.length != labels.length) {
				righttitles = labels;
			}

			if (cells.size() > 0) {
				rightreport.add(result);
				populateRightRowSummary(result);
			} else {
				if (debug) {
					setError("Both base(row) and right(col) variables list for right have no values");
					setError("\r\n\r\n");
				}
				logger.debug("Both base(row) and right(col) variables list for right have no values");
			}
		}
	}

	public void populateRowSummary(String[] row) {
		if ("AVG".equals(endtype)) {
			addRowAverage(row);
		} else {
			addRowTotals(row);
		}
	}

	public void populateRightRowSummary(String[] row) {
		if (row != null) {
			if (rightrowsummary == null) {
				rightrowsummary = new double[row.length];
				for (int i = 0; i < rightrowsummary.length; i++) {
					rightrowsummary[i] = 0;
				}
			}
			for (int i = 0; i < row.length; i++) {
				if (row[i] != null) {
					try {
						rightrowsummary[i] = rightrowsummary[i]
								+ Double.parseDouble(row[i]);
					} catch (Exception e) {
					}
				}
			}
		}
	}

	public void addTotal() {
		if (colsummary != null) {
			for (int i = 0; i < colsummary.length; i++) {
				total = total + colsummary[i];
			}
		}
	}

	public void addRightTotal() {
		if (rightsummary != null) {
			for (int i = 0; i < rightsummary.length; i++) {
				righttotal = righttotal + rightsummary[i];
			}
		}
	}

	public void addColumnTotals() {

		if (colsummary == null) {
			colsummary = new double[report.size()];
			for (int i = 0; i < colsummary.length; i++) {
				colsummary[i] = 0;
			}
		}
		for (int i = 0; i < colsummary.length; i++) {
			String[] row = (String[]) report.get(i);
			if (row != null) {
				for (int j = 0; j < row.length; j++) {
					if (row[j] != null) {
						try {
							colsummary[i] = colsummary[i]
									+ Double.parseDouble(row[j]);
						} catch (Exception e) {
						}
					}
				}
			}
		}
	}

	public void addRightTotals() {

		if (rightsummary == null) {
			rightsummary = new double[rightreport.size()];
			for (int i = 0; i < rightsummary.length; i++) {
				rightsummary[i] = 0;
			}
		}
		for (int i = 0; i < rightsummary.length; i++) {
			String[] row = (String[]) rightreport.get(i);
			if (row != null) {
				for (int j = 0; j < row.length; j++) {
					if (row[j] != null) {
						try {
							rightsummary[i] = rightsummary[i]
									+ Double.parseDouble(row[j]);
						} catch (Exception e) {
						}
					}
				}
			}
		}
	}

	/**
	 * TESTED OK
	 */
	public void addColumnAverage() {
		com.sok.Debugger d = com.sok.Debugger.getDebugger();
		d.debug("adding col average");

		if (colsummary == null) {
			colsummary = new double[report.size()];
			for (int i = 0; i < colsummary.length; i++) {
				colsummary[i] = 0;
			}
		}
		for (int i = 0; i < colsummary.length; i++) {
			String[] row = (String[]) report.get(i);
			int colstoav = 0;
			if (row != null) {
				for (int j = 0; j < row.length; j++) {
					if (row[j] != null) {
						try {
							double val = Double.parseDouble(row[j]);

							if (val > 0) {
								colsummary[i] = colsummary[i] + val;
								colstoav++;
							}
						} catch (Exception e) {
						}
					}
				}
			}
			if (colstoav > 0) {
				colsummary[i] = colsummary[i] / colstoav;
				avcols++;
			}
		}
	}

	public void addRowTotals(String[] row) {
		if (row != null) {
			if (rowsummary == null) {
				rowsummary = new double[row.length];
				for (int i = 0; i < rowsummary.length; i++) {
					rowsummary[i] = 0;
				}
			}
			for (int i = 0; i < row.length; i++) {
				if (row[i] != null) {
					try {
						rowsummary[i] = rowsummary[i]
								+ Double.parseDouble(row[i]);
					} catch (Exception e) {
					}
				}
			}
		}
	}

	public void addRowAverage(String[] row) {
		// com.sok.Debugger d = com.sok.Debugger.getDebugger();
		// d.debug("adding row average");
		if (row != null) {
			if (rowsummary == null) {
				rowsummary = new double[row.length];
				for (int i = 0; i < rowsummary.length; i++) {
					rowsummary[i] = 0;
				}
			}
			if (avrows == null) {
				avrows = new int[row.length];
				for (int i = 0; i < avrows.length; i++) {
					avrows[i] = 0;
				}
			}
			for (int i = 0; i < row.length; i++) {
				if (row[i] != null) {
					try {
						double val = Double.parseDouble(row[i]);
						if (val > 0) {
							rowsummary[i] = rowsummary[i] + val;
							avrows[i]++;
						}
					} catch (Exception e) {
					}
				}
			}
		}
	}

	public void addRowAverageCalc() {
		// com.sok.Debugger d = com.sok.Debugger.getDebugger();
		// d.debug("calculating average");
		for (int i = 0; i < rowsummary.length; i++) {
			// d.debug("Row 1, row total: "+rowsummary[i] +
			// ", numrows "+avrows[i]);
			if (rowsummary[i] > 0 && avrows[i] > 0) {
				rowsummary[i] = rowsummary[i] / avrows[i];
				// d.debug("calculating..");
			}
			// d.debug("value "+rowsummary[i]);
		}

	}

	/**
	 * Represents a returned row's worth of data and metadata Implements
	 * Serializable as we may want it to be cached.
	 * 
	 * @author mikey
	 * @date 3 May 2011
	 */
	public static class RowResult implements Serializable {

		private static final long serialVersionUID = 1L;
		public String[] data;
		public String[] labels;

		// public int[] types;

		/**
		 * Populate the object based on the results of the Query. Method assumes
		 * next() already called on resultset.
		 * 
		 * @param rs
		 *            ResultSet with next() called
		 * @throws SQLException
		 */
		public RowResult(ResultSet rs) throws SQLException {
			ResultSetMetaData rsmd = rs.getMetaData();
			int columncount = rsmd.getColumnCount();

			data = new String[columncount];
			labels = new String[columncount];
			// types = new int[columncount];
			/* ResultSets start at 1 */
			int j = 0;
			for (int i = 0; i < columncount; i++) {
				j = i + 1;
				/*
				 * should we be returning longs/doubles? The result has always
				 * been a numeric value.
				 */
				data[i] = rs.getString(j);
				labels[i] = rsmd.getColumnName(j);
				// types[i] = rsmd.getColumnType(j);
			}
		}
	}

	/**
	 * Returns a row based on the statement, first checking the cache for a
	 * pre-returned row.
	 * 
	 * @param stmtstring
	 *            - Evaluated SQL Statement ready for the DB.
	 * @param ListType
	 *            l : Expected l = ListType.Col or ListType.Right
	 * @return String[] of result data, with each cell as a String
	 */
	public String[] getResultArray(String stmtstring) {
		return getResultArray(stmtstring, ListType.Col);
	}

	/**
	 * Returns a row based on the statement, first checking the cache for a
	 * pre-returned row.
	 * 
	 * @param stmtstring
	 *            - Evaluated SQL Statement ready for the DB.
	 * @param ListType
	 *            l : Expected l = ListType.Col or ListType.Right
	 * @return String[] of result data, with each cell as a String
	 */
	public String[] getResultArray(String stmtstring, ListType l) {
		return getResultArray(stmtstring, l, false);
	}

	/**
	 * Updated to enable caching of entire rows. The issue with this is that we
	 * may have already retrieved part of a row before, and it makes little
	 * sense to retrieve those records again. For now, it makes previously
	 * loaded reports using the existing generation code a bunch faster. With
	 * the addition of the caching code it *is* a bit bloated, but hey. (to be) @deprecated
	 * 
	 * @param stmtstring
	 *            - Evaluated SQL Statement ready for the DB.
	 * @param ListType
	 *            l : Expected l = ListType.Col or ListType.Right
	 * @param nocache
	 *            - used when we will cache the individual results and do not
	 *            want to cache the full row.
	 * @return String[] of result data, only ever a single row with each cell
	 *         value in a String.
	 */
	public String[] getResultArray(String stmtstring, ListType l,
			boolean nocache) {
		ResultSet current = null;
		Statement stmt = null;

		String[] valuearray = null;
		try {
			/* cache variables, set to null if nocache = true */
			Cache c = nocache ? null : InitServlet.getCache("reportingCache");
			Element e = null;
			/* don't bother with the hashing if we aren't using cache */
			String hash = c == null ? null : hashStatement(stmtstring);

			com.sok.Debugger.getDebugger().debug(stmtstring);
			RowResult r = null;
			if (c != null) {
				/* check the cache for the hashed row result */
				if ((e = c.get(hash)) != null) {
					r = (RowResult) e.getValue();
				}
			}
			if (r == null) {
				/*
				 * if(c!=null) { //cache miss in this case }
				 */
				Connection con = getConnection();
				// if (con == null) {
				// connect();
				// setCloseConnection(true);
				// con = getConnection();
				// }
				stmt = con.createStatement();
				current = stmt.executeQuery(stmtstring);
				if (current.next()) {
					/*
					 * populate RowResult from the ResultSet - also checks RSMD
					 * and gathers titles, etc
					 */
					r = new RowResult(current);
					if (c != null) {
						/* add the RowResult to the cache if we have one */
						c.put(new Element(hash, r));
					}
				}
			}
			/*
			 * if the result was returned from cache or the statment returned
			 * any results
			 */
			if (r != null) {
				valuearray = r.data;

				/*
				 * arguably if we're using the cache this should not be used,
				 * but lets keep it simple
				 */
				if (l == ListType.Col && coltitles == null) {
					coltitles = r.labels;
					// coltypes is never read.
					// coltypes = r.types;
				} else if (l == ListType.Right && righttitles == null) {
					righttitles = r.labels;
					// righttypes is never read.
					// righttypes = r.types;
				}
			}
		} catch (Exception ex) {
			setError("Error encountered at getResultArray: ");
			setError(ex.getMessage());
			setError("\r\n");
			setError(stmtstring);
			setError("\r\n\r\n");

			logger.error("Error encountered at getResultArray: ", ex);
			logger.error(stmtstring);

		} finally {
			if (current != null) {
				try {
					current.close();
				} catch (SQLException ex) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException ex) {
				}
			}
			close();
		}
		return (valuearray);
	}

	/**
	 * Backwards compatible method to generate the full left axis row statment.
	 * 
	 * @return Row Statement as String
	 * @deprecated
	 */
	public String getRowStatement() {
		return getRowStatement(ListType.Col);
	}

	/**
	 * Backwards Compatible method to generate the full row statement as done
	 * previously.
	 * 
	 * @param ListType
	 *            l : Expected l = ListType.Col or ListType.Right
	 * @return Row Statement as String
	 * @deprecated
	 */
	public String getRowStatement(ListType l) {
		return getCellsStatement(getCellStatements(l));
	}

	/**
	 * Generate a row statement based on a list of cells. This is typically
	 * called based on a subset of cells that require loading.
	 * 
	 * @param List
	 *            <CellData> - list of cells to be loaded.
	 * @return String with all statements ie select $stmt as $name, $stmt2 as
	 *         $name2 etc.
	 */
	public String getCellsStatement(List<CellData> list) {
		/*
		 * this could be updated to use reportUnits, if we find out it actually
		 * does something.
		 */
		if (list.size() > 0) {
			StringBuilder stmt = new StringBuilder("select ");
			for (CellData data : list) {
				if (stmt.length() > 7) {
					stmt.append(", ");
				}
				stmt.append(data.stmt).append(" as ").append("\"")
						.append(data.name).append("\"");
			}
			stmt.append("\r\n/").append("* REPORT ");
			stmt.append(getString(GenerationKeys.key_CURRENTUSERID));
			stmt.append(" *").append("/");

			return stmt.toString();
		}
		return null;
	}

	/**
	 * Represents a single cell of data
	 * 
	 * @author Michael Dekmetzian
	 * @date 3 May 2011
	 */
	public static class CellData implements Serializable {
		private static final long serialVersionUID = 1L;
		/* the unhashed statment */
		public String stmt;
		/* statment hashed, for cache key */
		public String hash;
		/* the name of the report field */
		public String name;
		/* the link associated with the field */
		public String link = null;
		/*
		 * the value associated with the field You could argue that we should be
		 * storing doubles, but lets not complicate things
		 */
		public String val = null;
		/*
		 * whether this value should be cached. At present, we are caching
		 * values that are generated with dates that occur in the past. We could
		 * keep today+future values for a given period of time (say an hour),
		 * but lets keep this simple and not cache them in that case.
		 */
		public boolean cache = false;

		public CellData(String stmt, String name) {
			this.stmt = stmt;
			this.name = name;
			this.hash = ReportBean.hashStatement(stmt);
		}

		public CellData(String stmt, String name, String link) {
			this(stmt, name);
			this.link = link;
		}

		public CellData(String stmt, String name, String link, boolean cache) {
			this(stmt, name, link);
			this.cache = cache;
		}
	}

	/**
	 * Method to produce a list of cell statements for the current row, names,
	 * links which can then be individually evaluated against the query cache if
	 * required.
	 * 
	 * @param ListType
	 *            l : Expected l = ListType.Col or ListType.Right
	 * @return List<RowData> for each row.
	 */
	public List<CellData> getCellStatements(ListType l) {
		/* choose which variable to used based on the Report ListType */
		int colvar = l == ListType.Right ? this.rightvar : this.colvar;
		ReportSetSpec rpspec = l == ListType.Right ? this.rpspecr : this.rpspec;

		/*
		 * gets field marked <reportUnit />, at present this would always
		 * resolve to 1
		 */
		int unitcount = rpspec.getReportUnitsCount();

		/*
		 * we want to check that calendars are before tomorrow, as that's how
		 * alt end / end is used within the reportspecs ie:
		 * $database.getStrToTomorrowDateStmt($end) I could increment every
		 * altend/end but lets just decrement this.
		 */
		Calendar today = new GregorianCalendar();
		today.add(Calendar.DAY_OF_MONTH, -1);
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 1);
		today.set(Calendar.MILLISECOND, 0);

		/*
		 * list of cell info; the statement for generating the result, name/link
		 * and if it's to be cached
		 */
		List<CellData> fields = new ArrayList<CellData>();
		/*
		 * unit count is always 1, unless the spec isn't built properly. Useful?
		 * Maybe.
		 */
		if (unitcount >= 0) {
			String tokenname = rpspec.getListValue(colvar);
			String datestart = rpspec.getListRangeStart(colvar);
			String dateend = rpspec.getListRangeEnd(colvar);

			String type = rpspec.getListType(colvar);
			// String unittitle = null;
			String unitstmt = null;

			int linktextindex = rpspec.getLinkIndexForTrigger(
					(String) get(ReportSetSpec._coltrigger),
					(String) get(ReportSetSpec._rowtrigger));
			String linktext = rpspec.getLinkURL(linktextindex);

			/* rowlinks is - for now - a necessary evil */
			ArrayList<String> rowlinks = new ArrayList<String>();
			boolean cache = false;

			int unitcolumns = -1;
			// boolean needcomma = false;
			/*
			 * this is a bit useless, but whatever. Not sure what it'd do if
			 * there *was* more than one
			 */
			for (int i = 0; i < unitcount; i++) {
				/*
				 * this relates to the (typically <reportUnit column="0" />
				 * value. it is presently 0 for *all* reports
				 */
				unitcolumns = rpspec.getReportUnitColumns(i);
				/*
				 * this relates to the (typically <reportUnit title="whatever"
				 * /> value. It is actually different for many reports (ie
				 * Contacts, Opportunities, etc) but we're not using it.
				 */
				// unittitle = rpspec.getReportUnitTitle(i);
				unitstmt = rpspec.getReportUnitStatement(i);

				/*
				 * this relates to the (typically <reportUnit format="whatever"
				 * /> value. Expected values include 'currency' or number
				 * formatting strings ie '#,##0;(#,##0)'
				 */
				if (l == ListType.Right) {
					rightformat = rpspec.getReportUnitFormat(i);
					if (rightformat != null && rightformat.length() == 0) {
						rightformat = null;
					}
				} else {
					format = rpspec.getReportUnitFormat(i);
					if (format != null && format.length() == 0) {
						format = null;
					}
				}
				/*
				 * I added this in way back in the day, but can't find it
				 * actually being used in any reportSpecs. I'd remove it, but
				 * who knows what client specific code may rely on it.
				 */
				endtype = rpspec.getReportUnitEndType(i);

				/*
				 * select which variables to use depending on the list type
				 * (axis) in use.
				 */
				String[][] varsarray = l.equals(ListType.Col) ? colvars
						: rightvars;
				String[] varsubset = l.equals(ListType.Col) ? colsubset
						: rightsubset;

				if (unitcolumns == 0 && varsarray[values] != null
						&& varsarray[values].length > 0
						&& varsarray[names] != null
						&& varsarray[names].length > 0) {
					/*
					 * date ranges have an extra value as it needs an additional
					 * 'end' value as all names have values between
					 * values[names] and values[names+1]
					 */
					// colvars[values].length will not work as it has
					// colvars[names].length + 1 for date ranges
					for (int j = 0; j < varsarray[names].length; j++) {
						if (varsubset == null
								|| contains(varsubset, varsarray[values][j])) {
							if (type != null && type.indexOf("range") > 0) {
								put(datestart, varsarray[values][j]);
								put(dateend, varsarray[values][j + 1]);
								put(altend, altdates[j + 1]);
							} else {
								put(tokenname, varsarray[values][j]);
							}

							/* use the cache for this cell? */
							cache = false;

							/*
							 * only cache results that apply for dates that do
							 * not include today. getDate will return a current
							 * date so it's ok that we're not checking for null
							 * we do this outside of the above as the date may
							 * have been set earlier in the process.
							 */
							if ((get(altend) != null
									&& this.getDate((String) get(altend))
											.before(today.getTime()) || (get(dateend) != null && getDate(
									(String) get(dateend)).before(
									today.getTime())))) {
								cache = true;
							}

							String stmt = evaluateStatement(
									ReportSetSpec.reportUnit, unitstmt, l);
							StringBuilder fieldname = new StringBuilder();

							if (rpspec.getReportUnitsCount() > 1) {
								fieldname.append(rpspec.getReportUnitTitle(i))
										.append(" ");
							}

							if (varsarray[names][j] == null) {
								fieldname.append("NULL");
							} else if (varsarray[names][j].length() == 0) {
								fieldname.append("EMPTY");
							} else {
								fieldname.append(varsarray[names][j]);
							}
							String link = null;
							if (linktext != null) {
								link = evaluateStatement(
										ReportSetSpec.reportUnit, linktext, l);
								rowlinks.add(link);
							}
							if (!cache) {
								// setError("added " + fieldname.toString() +
								// " with " + stmt);
							}
							fields.add(new CellData(stmt, fieldname.toString(),
									link, cache));
						}
					}
					// remove inter values
					if (type != null && type.indexOf("range") > 0) {
						remove(datestart);
						remove(dateend);
						remove(altend);
					} else {
						remove(tokenname);
					}
				} else // if(unitcolumns > 0)
				{
					/*
					 * this section will be triggered if a column has not been
					 * set but a row(base) has been the setup for the vars will
					 * have been done previously
					 */

					/* use the cache for this cell? */
					cache = false;
					/*
					 * only use the cache for statements that aren't searching
					 * on today's values
					 */
					if ((get(altend) != null
							&& this.getDate((String) get(altend)).before(
									today.getTime()) || (get(dateend) != null && getDate(
							(String) get(dateend)).before(today.getTime())))) {
						cache = true;
					}

					/*
					 * check to see if this cell has a link evaluated. Usually
					 * exists, not always depending on the reportSpec as some
					 * Runway search links cannot be readily crafted
					 */
					String link = null;
					if (linktext != null) {
						link = evaluateStatement(ReportSetSpec.reportUnit,
								linktext, l);
						rowlinks.add(link);
					}
					fields.add(new CellData(evaluateStatement(
							ReportSetSpec.reportUnit, unitstmt, l), rpspec
							.getReportUnitTitle(i), link, cache));
				}
			}
			if (fields.size() > 0) {
				(l.equals(ListType.Right) ? this.rightlinks : this.links)
						.add(rowlinks);
				return fields;
			} else {
				setError("Error encountered at getRowStatements: ");
				setError("no fields were found for search");
				setError("\r\n\r\n");
			}
			/*
			 * Removed reportUnits code - Not convinced all this is nessesary,
			 * the reportUnits just returns a select + reportUnits. why bother?
			 * Could be evaluated in getCellsStatement if it's really required.
			 */
		} else {
			setError("Error encountered at getRowStatement: ");
			setError("no report units found");
			setError("\r\n\r\n");
		}
		return (null);
	}

	@SuppressWarnings("unchecked")
	public void setCurrentUserID() {
		Iterator<String> keys = keySet().iterator();
		String key = null;
		Object obj = null;
		String value = null;
		while (keys.hasNext()) {
			key = (String) keys.next();
			obj = get(key);
			if (obj instanceof String) {
				value = (String) obj;
				if (value.equals(ActionBean.CURRENTUSER)) {
					put(key, get(ActionBean.CURRENTUSERID));
					setCurrentUserID();
					return;
				}
			}
		}
	}

	/**
	 * Evaluate the report statement, using standard group parameters (GroupID,
	 * GroupID+set)
	 * 
	 * @param name
	 *            - I don't believe this is used except perhaps in exceptions.
	 * @param stmt
	 *            - the statment to be evaluated.
	 * @return String - evaluated statement using
	 */
	public String evaluateStatement(String name, String stmt) {
		return evaluateStatement(name, stmt, ListType.Col);
	}

	/**
	 * Adding a list type here allows the addition of a second group filter when
	 * selecting report parameters without changing any of the current reports.
	 * 
	 * @param name
	 *            - I don't believe this is used except perhaps in exceptions.
	 * @param stmt
	 *            - The statment to be evaluated.
	 * @param l
	 *            - the ListType (and in effect, which GroupID variables to use)
	 * @return String - evaluated statement.
	 */
	public String evaluateStatement(String name, String stmt, ListType l) {
		StringWriter sw = new StringWriter();
		
		checkForSets();
		
		try {
			if (containsValue(ActionBean.CURRENTUSER)) {
				setCurrentUserID();
			}
			if (ListType.Col.equals(l) || ListType.Row.equals(l)) {
				ve.evaluate(this, sw, name, stmt);
			} else {
				Object origGroups = get("GroupID");
				Object origGroupsSet = get("GroupID" + set);
				put("GroupID", get("RightGroupID"));
				put("GroupID" + set, get("RightGroupID" + set));
				ve.evaluate(this, sw, name, stmt);
				put("GroupID", origGroups);
				put("GroupID" + set, origGroupsSet);
			}
			sw.flush();
			sw.close();
		} catch (Exception e) {
			setError("Error encountered at evaluateStatement: ");
			setError(e.getMessage());
			setError("\r\n\r\n");
			setError(stmt);
			setError("\r\n\r\n");
			setError(this.toString());
			setError("\r\n\r\n");
		}
		Debugger.getDebugger().debug(sw.toString());
		logger.trace("Report Statement is {}", sw.toString());
		return (sw.toString());
	}
	
	public void checkForSets() {
		Object[] keys = this.getKeys();
		if (keys != null) {
			for (int k = 0; k < keys.length; ++k) {
				String key = (String) keys[k];
				if (StringUtils.isNotBlank(key)) {
					if (key.indexOf(set) == -1 && !this.isSet(key + set) && this.getString(key).indexOf("+") != -1) {
						this.put(key + set, this.getString(key).split("\\+"));
					}
				}
			}
		}
	}

	public int getColumnIndex(ResultSetMetaData meta, String name) {
		try {
			int columncount = meta.getColumnCount();
			if (name != null && name.length() != 0) {
				for (int i = 1; i <= columncount; i++) {
					if (name.equals(meta.getColumnName(i))
							|| name.equals(meta.getColumnLabel(i))) {
						return (i);
					}
				}
			}
		} catch (Exception ex) {
			setError("Error encountered at getColumnIndex: ");
			setError(ex.getMessage());
			setError("\r\n\r\n");
		}
		return (-1);
	}

	public boolean contains(String[] list, String s) {
		if (list != null && s != null) {
			for (int i = 0; i < list.length; i++) {
				if (list[i].equals(s)) {
					return (true);
				}
			}
		}
		return (false);
	}

	public String[][] getListArray(String stmtstring, String labelcolumn,
			String valuecolumn, String groupcolumn) {

		logger.trace(
				"getListArray stmt [{}], label [{}], value[{}], group [{}]",
				new String[] { stmtstring, labelcolumn, valuecolumn,
						groupcolumn });

		ResultSet current = null;
		Statement stmt = null;
		String[][] listset = new String[4][];
		ArrayList<String> labellist = new ArrayList<String>();
		ArrayList<String> valuelist = new ArrayList<String>();
		ArrayList<String> grouplist = null;
		try {
			Connection con = getConnection();
			// if (con == null) {
			// connect();
			// setCloseConnection(true);
			// con = getConnection();
			// }
			stmt = con.createStatement();
			current = stmt.executeQuery(stmtstring);

			logger.trace("retrieving list");
			logger.trace(stmtstring);

			ResultSetMetaData meta = current.getMetaData();

			int labelindex = getColumnIndex(meta, labelcolumn);
			int valueindex = getColumnIndex(meta, valuecolumn);
			int groupindex = getColumnIndex(meta, groupcolumn);

			if (valueindex > 0) {
				while (current.next()) {
					if (labelindex > 0) {
						labellist.add(current.getString(labelindex));
					} else {
						labellist.add(ActionBean.escapeQuote(current
								.getString(valueindex)));
					}
					valuelist.add(ActionBean.escapeQuote(current
							.getString(valueindex)));

					if (groupindex > -1) {
						if (grouplist == null)
							grouplist = new ArrayList<String>();
						grouplist.add(ActionBean.escapeQuote(current
								.getString(groupindex)));
					}
				}

				String[] labelsarray = new String[labellist.size()];
				labelsarray = (String[]) labellist.toArray(labelsarray);

				String[] valuearray = new String[valuelist.size()];
				valuearray = (String[]) valuelist.toArray(valuearray);

				listset[names] = labelsarray;
				listset[values] = valuearray;

				if (grouplist != null) {
					String[] grouparray = new String[grouplist.size()];
					grouparray = (String[]) grouplist.toArray(grouparray);
					listset[group] = grouparray;
				}
			} else {
				logger.error(
						"Error encountered at getListArray: column not found {}",
						valuecolumn);

				setError("Error encountered at getListArray: ");
				setError("column not found - ");
				if (valuecolumn != null) {
					setError(valuecolumn);
				} else {
					setError(" null");
				}
				setError(" - Looking for: " + meta.getColumnCount());
				for (int i = 0; i < meta.getColumnCount(); i++) {

					setError(meta.getColumnName(i + 1));
					setError(" or " + meta.getColumnLabel(i + 1));
				}
				setError("\r\n\r\n");
				setError(stmtstring + "\r\n");

			}
		} catch (Exception ex) {
			logger.error("Error encountered at getListArray", ex);
			setError("Error encountered at getListArray: ");
			setError(ex.getMessage());
			setError("\r\n\r\n");
			setError(stmtstring);
			setError("\r\n\r\n");
		} finally {
			if (current != null) {
				try {
					current.close();
				} catch (SQLException ex) {
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException ex) {
				}
			}
			close();
		}
		return (listset);
	}

	public String getColumnFormatType(int i) {
		return (rpspec.getReportUnitFormatType(i));
	}

	public String getColumnFormat(int i) {
		return (rpspec.getReportUnitFormat(i));
	}

	public String getRightFormatType(int i) {
		return (rpspecr.getReportUnitFormatType(i));
	}

	public String getRightFormat(int i) {
		return (rpspecr.getReportUnitFormat(i));
	}

	public Calendar getCal(String datestring) {
		Calendar cal = null;
		if (datestring.length() != 0) {
			java.util.Date date = getDate(datestring);
			cal = Calendar.getInstance();
			if (date != null) {
				cal.setTime(date);
			}
		}
		return (cal);
	}

	public String[][] getRange(int type) {
		String beginstring = getString(begindate);
		String endstring = getString(enddate);
		logger.debug("have begin [{}], end [{}]", beginstring, endstring);
		Calendar cal1 = null;
		Calendar cal2 = null;

		String[][] listset = null;

		if ("NEXTFINYEAR".equals(beginstring)) {
			beginstring = "NEXT1FINYEARS";
		} else if ("NEXTYEAR".equals(beginstring)) {
			beginstring = "NEXT1YEARS";
		}

		if (beginstring.indexOf(LAST) == 0 && beginstring.indexOf(WEEKS) > 0) {
			cal1 = CalendarFactory.getLastNumWeeksCal(beginstring);
			cal2 = CalendarFactory.getLastNumWeeksInitCal(beginstring);
		} else if (beginstring.indexOf(LAST) == 0
				&& beginstring.indexOf(DAYS) > 0) {
			cal1 = CalendarFactory.getLastNumDaysCal(beginstring);
			cal2 = CalendarFactory.getYesterdayCal();
		} else if (beginstring.indexOf(LAST) == 0
				&& beginstring.indexOf(MONTHS) > 0) {
			cal1 = CalendarFactory.getLastNumMonthsCal(beginstring);
			cal2 = CalendarFactory.getLastNumMonthsInitCal(beginstring);
		} else if (beginstring.indexOf(LAST) == 0
				&& beginstring.indexOf(QUARTERS) > 0) {
			cal1 = CalendarFactory.getLastNumQuartersCal(beginstring);
			cal2 = CalendarFactory.getLastNumQuartersInitCal(beginstring);
		} else if (beginstring.indexOf(LAST) == 0
				&& beginstring.indexOf(YEARS) > 0) {
			if (beginstring.indexOf(FINYEARS) > 0) {
				cal1 = CalendarFactory.getLastNumFinYearsCal(beginstring);
			} else {
				cal1 = CalendarFactory.getLastNumYearsCal(beginstring);
			}
			cal2 = CalendarFactory.getLastNumYearsInitCal(beginstring);
		} else if (beginstring.indexOf(NEXT) == 0
				&& beginstring.indexOf(WEEKS) > 0) {
			cal1 = CalendarFactory.getNextNumWeeksInitCal(beginstring);
			cal2 = CalendarFactory.getNextNumWeeksCal(beginstring);
		} else if (beginstring.indexOf(NEXT) == 0
				&& beginstring.indexOf(DAYS) > 0) {
			cal1 = CalendarFactory.getTomorrowCal();
			cal2 = CalendarFactory.getNextNumDaysCal(beginstring);
		} else if (beginstring.indexOf(NEXT) == 0
				&& beginstring.indexOf(MONTHS) > 0) {
			cal1 = CalendarFactory.getNextNumMonthsInitCal(beginstring);
			cal2 = CalendarFactory.getNextNumMonthsCal(beginstring);
		} else if (beginstring.indexOf(NEXT) == 0
				&& beginstring.indexOf(QUARTERS) > 0) {
			cal1 = CalendarFactory.getNextNumQuartersInitCal(beginstring);
			cal2 = CalendarFactory.getNextNumQuartersCal(beginstring);
		} else if (beginstring.indexOf(NEXT) == 0
				&& beginstring.indexOf(YEARS) > 0) {
			cal1 = CalendarFactory.getNextNumYearsInitCal(beginstring);
			if (beginstring.indexOf(FINYEARS) > 0) {
				cal2 = CalendarFactory.getNextNumFinYearsCal(beginstring);
			} else {
				cal2 = CalendarFactory.getNextNumYearsCal(beginstring);
			}
		} else if (beginstring.equals(TODAY)) {
			cal1 = new GregorianCalendar();
			cal2 = cal1;
		} else if (beginstring.equals(CURRENTMONTH)) {
			cal1 = CalendarFactory.getStartOfMonthCal();
			cal2 = CalendarFactory.getEndOfMonthCal();
		} else if (beginstring.equals(CURRENTQUARTER)) {
			cal1 = CalendarFactory.getStartOfQuarterCal();
			cal2 = CalendarFactory.getEndOfQuarterCal();
		} else if (beginstring.equals(CURRENTYEAR)) {
			cal1 = CalendarFactory.getStartOfYearCal();
			cal2 = CalendarFactory.getEndOfYearCal();
		} else if (beginstring.equals(CURRENTFINYEAR)) {
			cal1 = CalendarFactory.getStartOfFinYearCal();
			cal2 = CalendarFactory.getEndOfFinYearCal();
		} else if (beginstring.equals(CURRENTWEEK)) {
			cal1 = CalendarFactory.getStartOfWeekCal();
			cal2 = CalendarFactory.getEndOfWeekCal();
		} else if (beginstring.equals(LASTWEEK)) {
			cal1 = CalendarFactory.getStartOfLastWeekCal();
			cal2 = CalendarFactory.getEndOfLastWeekCal();
		} else if (beginstring.equals(LASTMONTH)) {
			cal1 = CalendarFactory.getStartOfLastMonthCal();
			cal2 = CalendarFactory.getEndOfLastMonthCal();
		} else if (beginstring.equals(LASTYEAR)) {
			cal1 = CalendarFactory.getStartOfLastYearCal();
			cal2 = CalendarFactory.getEndOfLastYearCal();
		} else if (beginstring.equals(LASTFINYEAR)) {
			cal1 = CalendarFactory.getStartOfLastFinYearCal();
			cal2 = CalendarFactory.getEndOfLastFinYearCal();
		} else {
			cal1 = getCal(beginstring);
			if (endstring.equals(TODAY)) {
				cal2 = CalendarFactory.getYesterdayCal();
			} else {
				cal2 = getCal(endstring);
			}
		}

		if (cal1 != null && cal2 != null) {
			listset = getRange(cal1, cal2, type);
		} else if (cal1 != null) {
			listset = setSingleRangeValue(cal1);
		} else if (cal2 != null) {
			listset = setSingleRangeValue(cal2);
		} else {
			setError("date range start/end not found\r\n\r\n");
		}

		return (listset);
	}

	public String[][] setSingleRangeValue(Calendar cal) {
		String[][] listset = new String[2][];
		String[] labellist = null;
		String[] valuelist = null;
		valuelist = new String[2];
		valuelist[0] = getDateString(cal);
		valuelist[1] = valuelist[0];
		listset[names] = labellist;
		listset[values] = valuelist;
		return (listset);
	}

	private static long MILLIS_PER_DAY = 1000 * 60 * 60 * 24;

	public boolean moreThanOneDayDiffernce(Calendar cal1, Calendar cal2) {
		long deltaMillis = cal2.getTimeInMillis() - cal1.getTimeInMillis();
		if (deltaMillis <= MILLIS_PER_DAY) {
			return (false);
		}
		return (true);
	}

	public String[][] getRange(Calendar cal1, Calendar cal2, int type) {
		String[][] listset = new String[2][];
		ArrayList<String> dates = new ArrayList<String>();
		ArrayList<String> dateends = new ArrayList<String>();
		ArrayList<String> dateslabels = new ArrayList<String>();

		if (moreThanOneDayDiffernce(cal1, cal2)) {
			dates.add(getDateString(cal1));
			dateends.add(getYesterdayString(cal1));

			// String nd = getNextDate(cal1, type);
			// System.out.println("NextDate: " + nd);
			dateslabels.add(getNextDate(cal1, type));
		}

		while (cal1.before(cal2)) {
			dates.add(getDateString(cal1));
			dateends.add(getYesterdayString(cal1));
			// String nd = getNextDate(cal1, type);
			// System.out.println("NextDate: " + nd);
			dateslabels.add(getNextDate(cal1, type));
		}

		if (type != Calendar.DAY_OF_MONTH) {
			dateslabels.remove(dateslabels.size() - 1);
			// String ld = getLastDate(cal1, cal2, type);
			// System.out.println("LastDate: "+ld);
			dateslabels.add(getLastDate(cal1, cal2, type));

			dates.add(getDateString(cal2));
			dateends.add(getYesterdayString(cal2));
		} else {
			dates.add(getDateString(cal1));
			dateends.add(getYesterdayString(cal1));
			dateslabels.add(getNextDate(cal1, type));
			dates.add(getDateString(cal1));
			dateends.add(getYesterdayString(cal1));
		}

		String[] valuelist = new String[dates.size()];
		valuelist = (String[]) dates.toArray(valuelist);
		listset[values] = valuelist;

		String[] namelist = new String[dateslabels.size()];
		namelist = (String[]) dateslabels.toArray(namelist);
		listset[names] = namelist;

		altdates = new String[dateends.size()];
		altdates = (String[]) dateends.toArray(altdates);

		return (listset);
	}

	public String getNextDate(Calendar cal, int type) {
		StringBuffer sb = new StringBuffer();
		sb.append(getDateString(cal));

		if (type != Calendar.DAY_OF_MONTH) {
			sb.append(" - ");
		}
		if (type == CALENDAR_QUARTER) {
			cal.add(Calendar.MONTH, 3);
		} else if (type == CALENDAR_FORTNIGHT) {
			cal.add(Calendar.DAY_OF_MONTH, 14);
		} else if (type == CALENDAR_FINYEAR) {
			// System.out.println(cal.getTime().toString());
			cal.setTime(CalendarFactory.getEndOfFinYearCal(cal).getTime());
			cal.add(Calendar.DAY_OF_MONTH, 1);
		} else {
			cal.add(type, 1);
		}

		if (type != Calendar.DAY_OF_MONTH) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			sb.append(getDateString(cal));
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		// System.out.println("next date: " + sb.toString());
		return (sb.toString());
	}

	public String getLastDate(Calendar cal1, Calendar cal2, int type) {
		StringBuffer sb = new StringBuffer();

		if (type == CALENDAR_QUARTER) {
			cal1.add(Calendar.MONTH, -3);
		} else if (type == CALENDAR_FORTNIGHT) {
			cal1.add(Calendar.DAY_OF_MONTH, -14);
		} else if (type == CALENDAR_FINYEAR) {
			// cal has already been incremented, we need to go back.
			cal1.add(Calendar.YEAR, -1);
			cal1.setTime(CalendarFactory.getStartOfFinYearCal(cal1).getTime());
		} else {
			cal1.add(type, -1);
		}

		sb.append(getDateString(cal1));
		if (type != Calendar.DAY_OF_MONTH) {
			sb.append(" - ");
			sb.append(getDateString(cal2));
			cal2.add(Calendar.DAY_OF_MONTH, 1);
		}
		return (sb.toString());
	}

	public String getDateString(Calendar cal) {
		StringBuffer temp = new StringBuffer();
		temp.append(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
		temp.append("/");
		temp.append(String.valueOf(cal.get(Calendar.MONTH) + 1));
		temp.append("/");
		temp.append(String.valueOf(cal.get(Calendar.YEAR)));
		return (temp.toString());
	}

	public String getYesterdayString(Calendar cal) {
		cal.add(Calendar.DAY_OF_MONTH, -1);
		StringBuffer temp = new StringBuffer();
		temp.append(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
		temp.append("/");
		temp.append(String.valueOf(cal.get(Calendar.MONTH) + 1));
		temp.append("/");
		temp.append(String.valueOf(cal.get(Calendar.YEAR)));
		cal.add(Calendar.DAY_OF_MONTH, 1);
		return (temp.toString());
	}

	public java.util.Date getDate(String value) {
		if (value != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			try {
				return (sdf.parse(value));
			} catch (Exception e) {
				setError("Could not parse date - ");
				setError(value);
				setError("\r\n\r\n");
			}
		}
		return (new java.util.Date());
	}

	public String getPieTitle(int r, int c) {
		StringBuffer temp = new StringBuffer();
		temp.append(getValue(r, c));
		temp.append(" (");
		temp.append(getPieSectionPercentage(r, c));
		temp.append("%) - ");
		if (r == 0) {
			temp.append(getColumnTitle(c));
		} else {
			temp.append(getRowTitle(r));
		}
		return (temp.toString());
	}

	public String getPieSectionPercentage(int r, int c) {
		double value = Double.parseDouble(getItem(r, c));
		double percent = 0d;
		if (r == 0) {
			percent = value / rowsummary[0] * 100;
		} else {
			percent = value / colsummary[0] * 100;
		}
		DecimalFormat formatter = new DecimalFormat("#,##0.0;(#,##0.0)");
		return (formatter.format(percent));
	}

	public boolean isColLabelsUnique() {
		HashSet<String> names = new HashSet<String>();
		int cols = getColumnCount();
		for (int i = 0; i < cols; i++) {
			names.add(getColumnTitle(i));
		}
		return (names.size() == cols);
	}

	/*
	 * boolean isRightLabelsUnique() { HashSet<String> names = new
	 * HashSet<String>(); int rights = getRightCount(); for (int i = 0; i <
	 * cols; i++) { names.add(getRightTitle(i)); } return(names.size()==rights);
	 * }
	 */

	public boolean isRowLabelsUnique() {
		HashSet<String> names = new HashSet<String>();
		int rows = getRowCount();
		for (int series = 0; series < rows; series++) {
			names.add(getRowTitle(series));
		}
		return (names.size() == rows);
	}

	/**
	 * This is going, leave it.
	 */
	public Object produceDataset(Map params) throws DatasetProduceException {
		DefaultCategoryDataset dataset = null;
		DefaultPieDataset pieds = null;

		if (pie) {
			pieds = new DefaultPieDataset();
		} else {
			dataset = new DefaultCategoryDataset();
		}

		double value = 0d;

		int rows = getRowCount();

		int cols = getColumnCount();

		String collabel = ActionBean._blank;
		String rowlabel = ActionBean._blank;

		boolean isColLabelsUnique = isColLabelsUnique();
		boolean isRowLabelsUnique = isRowLabelsUnique();

		for (int series = 0; series < rows; series++) {
			for (int i = 0; i < cols; i++) {
				try {
					value = Double.parseDouble(getItem(series, i));
				} catch (Exception e) {
					value = 0d;
				}
				collabel = isColLabelsUnique ? getColumnTitle(i) : String
						.valueOf(i + 1) + labelsperator + getColumnTitle(i);
				rowlabel = isRowLabelsUnique ? getRowTitle(series) : String
						.valueOf(series + 1)
						+ labelsperator
						+ getRowTitle(series);
				if (pie) {
					if (swapaxis) {
						pieds.setValue(rowlabel, new Double(value));
					} else {
						pieds.setValue(collabel, new Double(value));
					}
				} else {
					if (swapaxis) {
						dataset.addValue(value, rowlabel, collabel);
					} else {
						dataset.addValue(value, collabel, rowlabel);
					}
				}
			}
		}
		if (pie) {
			return (pieds);
		}
		return (dataset);
	}

	public boolean hasExpired(Map params, java.util.Date since) {
		return (System.currentTimeMillis() - since.getTime()) > 5000;
	}

	public String getProducerId() {
		return "DatasetProducer";
	}

	public Object getLinkGenerator() {
		if (pie) {
			return (getPieLinkGenerator());
		}
		return (getCategoryLinkGenerator());
	}

	public CategoryLinkGenerator getCategoryLinkGenerator() {
		return (new CategoryLinkGenerator(this));
	}

	public PieLinkGenerator getPieLinkGenerator() {
		return (new PieLinkGenerator(this));
	}

	public int getIndexForCategory(String[] catlist, Object category) {
		if (catlist != null) {
			String cat = category.toString();

			for (int i = 0; i < catlist.length; i++) {
				if (cat.equals(catlist[i])) {
					return (i);
				}
			}

			int index = cat.indexOf(labelsperator);
			if (index >= 0) {
				cat = cat.substring(index + labelsperator.length(),
						cat.length());
			}
			for (int i = 0; i < catlist.length; i++) {
				if (cat.equals(catlist[i])) {
					return (i);
				}
			}
		}

		return (0);
	}

	public class PieLinkGenerator implements PieSectionLinkGenerator,
			PieToolTipGenerator, ToolTipGenerator {
		ReportBean report;
		double pietotal = -1d;

		public PieLinkGenerator(ReportBean report) {
			this.report = report;
		}

		public java.lang.String generateLink(Object dataset, Object category) {
			int x = 0;
			int y = 0;
			String[] catlist = null;

			if (report.swapaxis || report.pie) {
				// catlist = report.colvars[names];
				catlist = coltitles;
				x = report.getIndexForCategory(catlist, category);
				y = 0;
			} else {
				// catlist = report.rowvars[names];
				catlist = rowtitles;
				x = 0;
				y = report.getIndexForCategory(catlist, category);
			}

			if (report.hasLink(y, x)) {
				return (report.getLink(y, x));
			}

			// return("javascript:;");
			return (null);
		}

		void setTotal(PieDataset data) {
			int count = data.getItemCount();
			pietotal = 0d;
			for (int j = 0; j < count; j++) {
				pietotal = pietotal + data.getValue(j).doubleValue();
			}
		}

		String getSectionPercentage(PieDataset data, int i) {
			if (pietotal < 0) {
				setTotal(data);
			}

			double value = data.getValue(i).doubleValue();
			double percent = value / pietotal * 100;

			DecimalFormat formatter = new DecimalFormat("#,##0.0;(#,##0.0)");
			return (formatter.format(percent));
		}

		public String generateToolTip(PieDataset data, Comparable c, int i) {
			StringBuffer tip = new StringBuffer();
			String label = null;
			String value = null;

			if (report.swapaxis) {
				label = report.getRowTitle(i);
				value = report.getItem(i, 0);
			} else {
				label = report.getColumnTitle(i);
				value = report.getItem(0, i);
			}

			String percentage = getSectionPercentage(data, i);

			tip.append(value);
			tip.append(" (");
			tip.append(percentage);
			tip.append("%) - ");
			tip.append(label);

			return (tip.toString());
		}
	}

	public class CategoryLinkGenerator implements CategoryItemLinkGenerator,
			CategoryToolTipGenerator, ToolTipGenerator {
		ReportBean report;

		public CategoryLinkGenerator(ReportBean report) {
			this.report = report;
		}

		public String generateLink(Object data, int series, Object category) {
			int x = 0;
			int y = 0;
			String[] catlist = null;

			if (report.swapaxis) {
				// catlist = report.colvars[values];
				catlist = coltitles;
				x = report.getIndexForCategory(catlist, category);
				y = series;
			} else {
				// catlist = report.rowvars[values];
				catlist = rowtitles;
				x = series;
				y = report.getIndexForCategory(catlist, category);
			}

			if (report.hasLink(y, x)) {
				return (report.getLink(y, x));
			}

			// return("javascript:;");
			return (null);
		}

		public String generateToolTip(CategoryDataset arg0, int series, int i) {

			int x = 0;
			int y = 0;

			if (report.swapaxis) {
				x = i;
				y = series;
			} else {
				x = series;
				y = i;
			}

			StringBuffer tip = new StringBuffer();
			String collabel = report.getColumnTitle(x);
			String rowlabel = report.getRowTitle(y);

			String value = report.getItem(y, x);

			tip.append(value);

			if (collabel.length() != 0 || rowlabel.length() != 0) {
				tip.append(" - ");
				if (rowlabel.length() != 0) {
					tip.append(rowlabel);
					tip.append(", ");
				}
				tip.append(collabel);
			}

			return (tip.toString());
		}
	}

	/*
	 * protected JFreeChart getJFreeChart() { JFreeChart chart = null; try {
	 * chart = CewolfChartFactory.getChartInstance(getChartType(), "", "", "",
	 * (Dataset) this.produceDataset(new HashMap()));
	 * chart.setBackgroundPaint((java.awt.Paint)java.awt.Color.white); }
	 * catch(Exception e) {
	 * 
	 * } return(chart); }
	 */

	protected JFreeChart getJFreeChart() {

		JFreeChart chart = null;
		try {
			if (CHART_TYPE_PIE.equals(getChartType())) {
				/*
				 * For the label format, use {0} where the pie section key
				 * should be inserted, {1} for the absolute section value and
				 * {2} for the percent amount of the pie section, e.g.
				 * <code>"{0} = {1} ({2})"</code> will display as <code>apple =
				 * 120 (5%)</code>.
				 */
				PiePlot plot = new PiePlot(
						(PieDataset) this.produceDataset(new HashMap()));
				plot.setLabelGenerator(new StandardPieSectionLabelGenerator(
						"{0} ({1})", new java.text.DecimalFormat("0"),
						new java.text.DecimalFormat("0.00%")));
				chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot,
						true);
				chart.setBackgroundPaint((java.awt.Paint) java.awt.Color.white);
			} else if (CHART_TYPE_CATEGORY.equals(getChartType())) {
				chart = CewolfChartFactory.getChartInstance(getChartType(), "",
						"", "", (Dataset) this.produceDataset(new HashMap()));
				chart.setBackgroundPaint((java.awt.Paint) java.awt.Color.white);
			} else {
				// if chart type is unspecified, assume 'category' chart type
				chart = CewolfChartFactory.getChartInstance(getChartType(), "",
						"", "", (Dataset) this.produceDataset(new HashMap()));
				chart.setBackgroundPaint((java.awt.Paint) java.awt.Color.white);
			}
		} catch (Exception e) {
		}

		return (chart);

	}

	public void saveChartToJpegFile(String filename) throws java.io.IOException {
		saveChartToJpegFile(new File(filename));
	}

	public void saveChartToJpegFile(java.io.File file)
			throws java.io.IOException {
		JFreeChart chart = getJFreeChart();
		ChartUtilities.saveChartAsJPEG(file, chart, getChartWidth(),
				getChartHeight());
	}

	public boolean loadedReportSpec() {
		return (rpspec != null);
	}

	public boolean hasFilterOptions() {
		if (rpspec != null) {
			int triggercount = rpspec.getVariablesCount();
			for (int i = 0; i < triggercount; i++) {
				if (rpspec.isFilterTrigger(i)) {
					return true;
				}
			}
		}
		return false;
	}

	public void printFilterOptions(JspWriter out) {
		if (rpspec != null) {
			try {
				out.print("<option value=\"NONE\" ");
				if (needsFilterTriggerRefresh(-1)) {
					out.print("id=\"RefreshRow\"");
				} else {
					out.print("id=\"NoRefreshRow\"");
				}
				out.print("></option>");
				int triggercount = rpspec.getVariablesCount();
				for (int i = 0; i < triggercount; i++) {
					if (rpspec.isFilterTrigger(i)) {
						out.print("<option value=\"");
						out.print(rpspec.getListTrigger(i));
						out.print("\" ");
						out.print(getFilterTriggerIsSelected(i));
						out.print(" ");
						if (needsFilterTriggerRefresh(i)) {
							out.print("id=\"RefreshRow");
							out.print(i);
							out.print("\"");
						} else {
							out.print("id=\"NoRefreshRow");
							out.print(i);
							out.print("\"");
						}
						out.print(">");
						out.print(rpspec.getListTitle(i));
						out.print("</option>\r\n");
					}
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	public void printColumnOptions(JspWriter out) {
		if (rpspec != null) {
			try {
				out.print("<option value=\"NONE\" ");
				if (needsColTriggerRefresh(-1)) {
					out.print("id=\"RefreshRow\"");
				} else {
					out.print("id=\"NoRefreshRow\"");
				}
				out.print("></option>");
				int triggercount = rpspec.getVariablesCount();
				for (int i = 0; i < triggercount; i++) {
					if (rpspec.isColTrigger(i)) {
						out.print("<option value=\"");
						out.print(rpspec.getListTrigger(i));
						out.print("\" ");
						out.print(getColTriggerIsSelected(i));
						out.print(" ");
						if (needsColTriggerRefresh(i)) {
							out.print("id=\"RefreshRow");
							out.print(i);
							out.print("\"");
						} else {
							out.print("id=\"NoRefreshRow");
							out.print(i);
							out.print("\"");
						}
						out.print(">");
						out.print(rpspec.getListTitle(i));
						out.print("</option>\r\n");
					}
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	public void printRightOptions(JspWriter out) {
		if (rpspecr != null) {
			try {
				out.print("<option value=\"NONE\" ");
				if (needsRightTriggerRefresh(-1)) {
					out.print("id=\"RefreshRow\"");
				} else {
					out.print("id=\"NoRefreshRow\"");
				}
				out.print("></option>");
				int triggercount = rpspecr.getVariablesCount();
				for (int i = 0; i < triggercount; i++) {
					if (rpspecr.isColTrigger(i) && !rpspecr.isDateTrigger(i)) {
						out.print("<option value=\"");
						out.print(rpspecr.getListTrigger(i));
						out.print("\" ");
						out.print(getRightTriggerIsSelected(i));
						out.print(" ");
						if (needsRightTriggerRefresh(i)) {
							out.print("id=\"RefreshRight");
							out.print(i);
							out.print("\"");
						} else {
							out.print("id=\"NoRefreshRight");
							out.print(i);
							out.print("\"");
						}
						out.print(">");
						out.print(rpspecr.getListTitle(i));
						out.print("</option>\r\n");
					}
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	public void printRowOptions(JspWriter out) {
		if (rpspec != null) {
			try {
				out.print("<option value=\"NONE\" ");
				if (needsRowTriggerRefresh(-1)) {
					out.print("id=\"RefreshCol\"");
				} else {
					out.print("id=\"NoRefreshCol\"");
				}
				out.print("></option>");
				int triggercount = rpspec.getVariablesCount();
				for (int i = 0; i < triggercount; i++) {
					if (rpspec.isRowTrigger(i)) {
						boolean print = true;
						if (rpspecr != null) {
							print = false;
							// if we have a right spec, row triggers must exist
							// in both the left and right specs
							// to ensure the interface works correctly.
							int tc = rpspecr.getVariablesCount();
							for (int j = 0; j < tc; j++) {
								if (rpspecr.isRowTrigger(j)
										&& rpspecr.getListTrigger(j).equals(
												rpspec.getListTrigger(i))) {
									print = true;
									break;
								}
							}
						}
						if (print) {
							out.print("<option value=\"");
							out.print(rpspec.getListTrigger(i));
							out.print("\" ");
							out.print(getRowTriggerIsSelected(i));
							out.print(" ");
							if (needsRowTriggerRefresh(i)) {
								out.print("id=\"RefreshCol");
								out.print(i);
								out.print("\"");
							} else {
								out.print("id=\"NoRefreshCol");
								out.print(i);
								out.print("\"");
							}
							out.print(">");
							out.print(rpspec.getListTitle(i));
							out.print("</option>\r\n");
						}
					}
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	public boolean hasDateTypeOptions() {
		return ((rpspec != null && rpspec.getDateTypesCount() > 0) || (rpspecr != null && rpspecr
				.getDateTypesCount() > 0));
	}

	public void printDateTypeOptions(JspWriter out) {
		if (rpspec != null) {
			try {
				int typecount = rpspec.getDateTypesCount();
				int typecountr = -1;
				if (rpspecr != null) {
					typecountr = rpspecr.getDateTypesCount();
				}

				String typevalue = null;
				for (int i = 0; i < typecount; i++) {
					typevalue = rpspec.getDateTypeValue(i);
					if (typecountr != -1) {
						boolean found = false;
						for (int j = 0; j < typecountr; j++) {
							if (rpspecr.getDateTypeValue(j).equals(typevalue)) {
								found = true;
								break;
							}
						}
						// ie, only print the option if it exists in both for
						// dual-axis graphs.
						if (!found) {
							continue;
						}
					}
					out.print("<option value=\"");
					out.print(typevalue);
					out.print("\" ");
					out.print(getString("ondatetype").equals(typevalue) ? "selected"
							: "");
					out.print(">");
					out.print(rpspec.getDateTypeName(i));
					out.print("</option>");
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	public List<List<ReportCell>> getChartData() {
		if (chartData != null) {
			return chartData;
		}
		int cols = this.getColumnCount();
		int rows = this.getRowCount();
		int right = this.getRightCount();
		chartData = new LinkedList<List<ReportCell>>();
		/* produce a an array of tabular type data */
		List<ReportCell> rowData = null;
		if (this.hasColTitles() || this.hasRightTitles()) {
			rowData = new LinkedList<ReportCell>();
			chartData.add(rowData);
			if (this.hasRowTitles())
				rowData.add(new BlankCell());
			for (int j = 0; j < cols; j++) {
				rowData.add(new TitleCell(this.getColumnTitle(j)));
			}
			if (this.hasColSummary()) {
				rowData.add(new TitleCell(this.getSummaryLabel()));
			}
			for (int j = 0; j < right; j++) {
				rowData.add(new TitleCell(this.getRightTitle(j)));
			}
			if (this.hasRightSummary()) {
				rowData.add(new TitleCell(this.getSummaryLabel()));
			}
		}

		for (int i = 0; i < rows; i++) {
			rowData = new LinkedList<ReportCell>();
			chartData.add(rowData);
			if (this.hasRowTitles())
				rowData.add(new TitleCell(this.getRowTitle(i)));
			for (int j = 0; j < cols; j++) {
				rowData.add(buildCellValue(i, j, ListType.Col));
			}
			if (this.hasColSummary()) {
				rowData.add(new TotalCell(this.getColumnSummary(i)));
			}
			for (int j = 0; j < right; j++) {
				rowData.add(buildCellValue(i, j, ListType.Right));
			}
			if (this.hasRightSummary()) {
				rowData.add(new TotalCell(this.getRightSummary(i)));
			}
		}

		if (this.hasRowSummary()) {
			rowData = new LinkedList<ReportCell>();
			chartData.add(rowData);
			if (this.hasRowTitles())
				rowData.add(new TotalCell(this.getSummaryLabel()));

			for (int j = 0; j < cols; j++) {
				rowData.add(new TotalCell(this.getRowSummary(j)));
			}
			if (this.hasColSummary()) {
				rowData.add(new TotalCell(this.getSummary()));
			}
			for (int j = 0; j < right; j++) {
				rowData.add(new TotalCell(this.getRightRowSummary(j)));
			}

			if (this.hasRightSummary()) {
				rowData.add(new TotalCell(this.getRightSummary()));
			}
		}
		return chartData;
	}

	public String buildCellHTML(ReportBean report, int i, int j, ListType type) {
		boolean link = (report.hasLink(i, j, type) && report
				.getLink(i, j, type).indexOf("javascript:;") < 0);
		StringBuilder cell = new StringBuilder();
		if (link) {
			cell.append("<a href=\"").append(report.getLink(i, j, type))
					.append("\">");
		}
		cell.append(report.getValue(i, j, type));
		if (link) {
			cell.append("</a>");
		}
		return cell.toString();
	}

	/*
	 * 
	 * Example implementation for Cell Renderer - still experimental
	 * 
	 * public class HTMLReportCellAppender implements
	 * ReportBean.ReportCellRenderer { Appendable sw = null; public
	 * HTMLReportCellAppender(Appendable sw) { this.sw = sw; } public void
	 * render(ReportCell cell) throws IOException { if(sw == null) { sw = new
	 * StringBuilder(); } if(cell.hasLink()) {
	 * sw.append("<a href=\"").append(cell.getLink()).append("\" ");
	 * 
	 * //System.out.println(String.format("Appending  get [%2$s] direct [%3$s]",
	 * _blank, // cell.getLink(), cell.link));
	 * 
	 * } else if(cell.hasTitle() || cell.hasStyle()) { sw.append("<span "); }
	 * if(cell.hasTitle()) {
	 * sw.append("title=\"").append(cell.getTitle()).append("\""); }
	 * if(cell.hasStyle()) {
	 * sw.append("class=\"").append(cell.getStyle()).append("\""); }
	 * if(cell.hasTitle() || cell.hasLink() || cell.hasStyle()) {
	 * sw.append(">"); } sw.append(cell.getText()); if(cell.hasLink()) {
	 * sw.append("</a>"); } else if (cell.hasTitle()) { sw.append("</span>"); }
	 * } }
	 */

	public interface ReportCellRenderer {
		public void render(ReportCell cell) throws IOException;
	}

	public ReportCellRenderer getDefaultCellRenderer(Appendable sw) {
		return new HTMLReportCellAppender(sw);
	}

	public class HTMLReportCellAppender implements ReportCellRenderer {
		Appendable sw = null;

		public HTMLReportCellAppender(Appendable sw) {
			this.sw = sw;
		}

		public void render(ReportCell cell) throws IOException {
			if (sw == null) {
				sw = new StringBuilder();
			}
			if (cell.hasLink()) {
				String id = KeyMaker.generate();
				sw.append("<a searchurl=\"").append(cell.getLink()).append("\" ").append(" href=\"#\" onclick=\"javascript:showsearch(this); return false;\" ").append(" id=\"" + id +"\" ");
			} else if (cell.hasTitle() || cell.hasStyle()) {
				sw.append("<span ");
			}
			if (cell.hasTitle()) {
				sw.append("title=\"").append(cell.getTitle()).append("\"");
			}
			if (cell.hasStyle()) {
				sw.append("class=\"").append(cell.getStyle()).append("\"");
			}
			if (cell.hasTitle() || cell.hasLink() || cell.hasStyle()) {
				sw.append(">");
			}
			sw.append(cell.getText());
			if (cell.hasLink()) {
				sw.append("</a>");
			} else if (cell.hasTitle() || cell.hasStyle()) {
				sw.append("</span>");
			}
		}
	}

	public ReportCell buildCellValue(int i, int j, ListType type) {
		return buildCellValue(this, i, j, type);
	}

	/**
	 * @deprecated
	 */
	public ReportCell buildCellValue(ReportBean report, int i, int j,
			ListType type) {
		ValueCell c = new ValueCell();
		if (report.hasLink(i, j, type)) {
			c.setLink(report.getLink(i, j, type));
			// System.out.println(String.format("Report had: [%1$s] get [%2$s] direct [%3$s]",
			// report.getLink(i, j, type),
			// c.getLink(), c.link));

		}
		c.setText(report.getValue(i, j, type));

		String cellformat = (rpspecr != null && type.equals(ListType.Right)) ? rightformat
				: format;
		if ("currency".equals(cellformat)) {
			c.setStyle("currency");
		}
		// System.out.println(String.format("Report had: [%1$s] get [%2$s] direct [%3$s]",
		// report.getValue(i, j, type),
		// c.getText(), c.text));

		return c;
	}

	public static interface ReportTableRenderer {

		/**
		 * Set the number of cols that the output is going to use. Some output
		 * formats (ie pdf) need to know how high to make the table
		 */
		public void setCols(int cols);

		/**
		 * Set the number of rows that the output is going to use. Some output
		 * formats (ie pdf) need to know how wide to make the table
		 * 
		 * @param rows
		 */
		public void setRows(int rows);

		/**
		 * Allows the user to set the title before the table start. Depending on
		 * the implementation, this might be required before or after the other
		 * info.
		 * 
		 * @param title
		 */
		public void reportTitle(String title);

		public void tableStart() throws IOException;

		public void rowStart() throws IOException;

		public void cell(ReportCell cell) throws IOException;

		public void blankCell() throws IOException;

		public void titleCell(String title) throws IOException;

		public void totalCell(String total) throws IOException;

		public void rowEnd() throws IOException;

		public void tableEnd() throws IOException;
	}

	/**
	 * The idea behind this method is to allow implementations to provide an
	 * output method which conforms to the Renderer Interface so that report
	 * data does not have to be duplicated in memory to perform output
	 * 
	 * v1 - using generic ValueCells v2 - tba using object methods for cell
	 * types - less memory overhead.
	 * 
	 * In practice this was slightly more efficient than the previously
	 * implemented means of creating lists of strings/objects but not
	 * significantly so (1-15ms faster). Experimental stage - not in use in
	 * Runway code.
	 * 
	 * It is intended that the rendering mechanism implement ReportTableRenderer
	 * for it's output method and then have that object passed to the report to
	 * be rendered appropriately. This removes some of the display logic from
	 * the output file that would have to be duplicated regardless of the output
	 * method.
	 * 
	 * There is a bit of repetition in this method though, to be able to handle
	 * the different view orientations. Not sure if there is a better way of
	 * doing this.
	 * 
	 * @author Michael Dekmetzian
	 * 
	 * @param ReportTableRenderer
	 */
	public void renderReport(ReportTableRenderer r) throws IOException {

		boolean swap = "true".equals(get("-swapsummary"));

		int cols = this.getColumnCount();
		int rows = this.getRowCount();
		int right = this.getRightCount();

		int rc = rows;
		if (hasRowTitles())
			rc++;
		if (hasRowSummary() /* || hasRightRowSummary() */)
			rc++;

		int cc = cols + right;
		if (hasColTitles())
			cc++;
		if (hasColSummary())
			cc++;

		r.setCols(swap ? rc : cc);
		r.setRows(swap ? cc : rc);
		r.reportTitle(getTitle());

		r.tableStart();

		if (!swap) {
			// do the normal way first
			/* produce a an array of tabular type data */

			if (this.hasColTitles() || this.hasRightTitles()) {
				r.rowStart();
				if (this.hasRowTitles())
					r.blankCell();
				for (int j = 0; j < cols; j++) {
					r.titleCell(getColumnTitle(j));
				}
				if (this.hasColSummary()) {
					r.titleCell(getSummaryLabel());
				}
				for (int j = 0; j < right; j++) {
					r.titleCell(getRightTitle(j));
				}
				if (this.hasRightSummary()) {
					r.titleCell(getSummaryLabel());
				}
				r.rowEnd();
			}

			for (int i = 0; i < rows; i++) {
				r.rowStart();
				if (this.hasRowTitles())
					r.titleCell(getRowTitle(i));
				for (int j = 0; j < cols; j++) {
					r.cell(buildCellValue(i, j, ListType.Col));
				}
				if (this.hasColSummary()) {
					r.totalCell(getColumnSummary(i));
				}
				for (int j = 0; j < right; j++) {
					r.cell(buildCellValue(i, j, ListType.Right));
				}
				if (this.hasRightSummary()) {
					r.totalCell(this.getRightSummary(i));
				}
				r.rowEnd();
			}

			if (this.hasRowSummary()) {
				r.rowStart();
				if (this.hasRowTitles())
					r.totalCell(this.getSummaryLabel());

				for (int j = 0; j < cols; j++) {
					r.totalCell(this.getRowSummary(j));
				}
				if (this.hasColSummary()) {
					r.totalCell(this.getSummary());
				}
				for (int j = 0; j < right; j++) {
					r.totalCell(this.getRightSummary(j));
				}
				if (this.hasRightSummary()) {
					r.totalCell(this.getRightSummary());
				}
				r.rowEnd();
			}
		} else {

			boolean firstCol = hasColTitles() || hasRightTitles();
			boolean lastCol = hasColSummary() || hasRightSummary();

			if (hasRowTitles()) {
				r.rowStart();
				if (firstCol)
					r.cell(new BlankCell());

				for (int i = 0; i < rows; i++) {
					if (hasRowTitles())
						r.titleCell(getRowTitle(i));
					else
						r.blankCell();
				}
				if (lastCol)
					r.cell(new BlankCell());
				r.rowEnd();
			}
			for (int j = 0; j < cols; j++) {
				r.rowStart();
				if (firstCol) {
					if (hasColTitles())
						r.titleCell(getColumnTitle(j));
					else
						r.blankCell();
				}
				for (int i = 0; i < rows; i++) {
					r.cell(buildCellValue(i, j, ListType.Col));
				}
				if (lastCol)
					if (hasRowSummary())
						r.totalCell(getRowSummary(j));
					else
						r.blankCell();
				r.rowEnd();
			}
			if (hasColSummary()) {
				r.rowStart();
				if (firstCol)
					r.titleCell(getSummaryLabel());
				for (int i = 0; i < rows; i++) {
					r.totalCell(getColumnSummary(i));
				}
				if (lastCol)
					r.totalCell(getSummary());
				r.rowEnd();
			}

			if (right > 0) {

				for (int j = 0; j < cols; j++) {
					r.rowStart();
					if (firstCol) {
						if (hasRightTitles())
							r.titleCell(getRightTitle(j));
						else
							r.blankCell();
					}
					for (int i = 0; i < right; i++) {
						r.cell(buildCellValue(i, j, ListType.Right));
					}
					if (lastCol)
						if (hasRightSummary())
							r.totalCell(getRightSummary(j));
						else
							r.blankCell();
					r.rowEnd();
				}
				/*
				 * This stuff doesn't exist yet, I think.
				 */
				// right summary label
				// right row total 0 < right ie this.getRightRowSummary(k)
				// blank
			}
		}
		r.tableEnd();
	}

	public static class TotalCell extends ReportCell {
		private static final long serialVersionUID = 1L;

		public TotalCell(String text) {
			this.text = text;
		}

		public TotalCell(String text, String title) {
			this.text = text;
			this.title = title;
		}

		public boolean isTotal() {
			return true;
		}
	}

	public static class TitleCell extends ReportCell {
		private static final long serialVersionUID = 1L;

		public TitleCell() {
		}

		public TitleCell(String text) {
			this.text = text;
		}

		public TitleCell(String text, String title) {
			this.text = text;
			this.title = title;
		}

		public TitleCell setTitle(String title) {
			this.title = title;
			return this;
		}

		public String getTitle() {
			return StringUtils.trimToEmpty(this.title);
		}
	}

	public static class BlankCell extends ReportCell {
		private static final long serialVersionUID = 1L;

		public BlankCell() {
			this.text = _blank;
		}
	}

	public static class ValueCell extends ReportCell {
		private static final long serialVersionUID = 1L;

		public ValueCell() {
		}

		public ValueCell(String text) {
			this.text = text;
		}

		public ValueCell(String text, String link) {
			this.text = text;
			this.setLink(link);
		}

		public ValueCell setLink(String link) {
			this.link = link != null && link.indexOf("javascript:;") == -1 ? StringUtils
					.trimToNull(link) : null;
			return this;
		}

		public boolean hasLink() {
			return this.link != null;
		}

		public String getLink() {
			return StringUtils.trimToEmpty(this.link);
		}
	}

	public static class ErrorCell extends ReportCell {
		private static final long serialVersionUID = 1L;

		public ErrorCell(Exception e) {
			this.text = e.getMessage();
		}
	}

	public abstract static class ReportCell implements Serializable {
		private static final long serialVersionUID = 1L;
		protected String text = null, link = null, title = null,
				style = _blank;

		public boolean hasLink() {
			return this.link != null;
		}

		public String getLink() {
			return _blank;
		}

		public boolean hasTitle() {
			return this.title != null;
		}

		public String getTitle() {
			return _blank;
		}

		public boolean isTotal() {
			return false;
		}

		public String getStyle() {
			return this.style;
		}

		public boolean hasStyle() {
			return StringUtils.isNotBlank(style);
		}

		public ReportCell setStyle(String style) {
			this.style = style;
			return this;
		}

		public ReportCell setText(String text) {
			this.text = text;
			return this;
		}

		public String getText() {
			return this.text;
		}

		public String toString() {
			return StringUtils.trimToEmpty(this.text);
		}
	}

	public void writeChartJson(String renderTo, Writer out) throws IOException {
		getChartObject(renderTo).writeJSONString(out);
	}

	public String getChartJson(String renderTo) {
		return getChartObject(renderTo).toJSONString();
	}

	/**
	 * Return JSON Object as required by highcharts. This does tie RW to HC to
	 * some extent, and could be split out later.
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public JSONObject getChartObject(String renderTo) {

		if (chartJson != null) {
			try {
				synchronized (chartJson) {
					((JSONObject) chartJson.get("chart")).put("renderTo",
							renderTo);
				}
				return chartJson;
			} catch (RuntimeException re) {
				logger.debug(
						"Was unable to set render to on saved chart, recreating",
						re);
			}
		}

		JSONObject obj = new JSONObject();

		JSONObject chart = getJElem("renderTo", renderTo);
		String jChartType = this.getJsonChartType();
		if (!this.getChartOptionSelected("average")) {
			chart.put("defaultSeriesType", this.getSeriesType(jChartType));
		}
		/*
		 * chart:: plotBackgroundColor null plotBorderWidth null plotShadow
		 * false
		 */
		JSONArray margins = new JSONArray();
		margins.add(50);
		margins.add(50);
		margins.add(50);
		margins.add(50);
		chart.put("margins", margins);

		/* can actually take a x, y, xy, "" */
		if (this.getChartOptionSelected("zoomable")) {
			if (jChartType.equals("bar")) {
				chart.put("zoomType", "y");
			} else {
				chart.put("zoomType", "x");
			}
		}
		if (this.getChartOptionSelected("inverted")) {
			chart.put("inverted", new Boolean(true));
		}

		obj.put("chart", chart);
		String sub = this.getSubTitle();
		if (getString("Description").length() > 0) {
			sub = getString("Description");
			// chart.put("marginTop",20);
			/*
			 * JSONArray items = new JSONArray(); chart = new JSONObject();
			 * chart.put("html", getString("Description")); chart.put("style",
			 * new JSONData("{ bottom: '30px', align: 'center' }"));
			 * items.add(chart); obj.put("labels", getJElem("items",items));
			 */
		}

		chart = new JSONObject();
		// to disable: chart.put("enabled",new Boolean(false));
		chart.put("href", "");
		chart.put("text", "");
		obj.put("credits", chart);

		String export = InitServlet.getSystemParams().getProperty("exportURL");
		if (export == null || export.length() == 0) {
			export = "/exporter/";
		}
		obj.put("exporting", getJElem("url", export));

		obj.put("title", getJElem("text", this.getTitle()));
		obj.put("subtitle", getJElem("text", sub));

		String tooltip = " return '<b>'+ (this.point.name?this.point.name:this.point.series.name) +'</b>: '+ this.y; ";

		if (getRowCount() > 0 && getColumnCount() > 0) {
			tooltip = " return '<b>'+ (this.point.name?this.point.name:'') + '</b> : <b>' + (this.point.series.name?this.point.series.name:'') +'</b>: '+ this.y; ";
		} else if (this.getChartOptionSelected("pie-summary")) {
			tooltip = " return '<b>'+ (this.point.name?this.point.name:this.point.series.name) +'</b>: '+ this.y + (this.point.perc?(' - '+this.point.perc):''); ";
		}

		obj.put("tooltip",
				getJElem("formatter",
						new JSONFunction(tooltip, (String[]) null)));

		if (this.getColumnCount() > 8) {
			// obj.put("legend",getJElem("enabled",false));
		}

		/* plot options */
		JSONObject typeOptions = new JSONObject();
		JSONObject plotOptions = getJElem(this.getSeriesType(jChartType),
				typeOptions);
		/* lets try and keep a degree of backwards compatibility */
		if (this.getChartOptionSelected("stacking-percentage")) {
			plotOptions.put("series", getJElem("stacking", "percent"));
			// typeOptions.put("stacking", "percent");
		} else if (/* this.getChartType().contains("stacked") || */this
				.getChartOptionSelected("stacking")) {
			plotOptions.put("series", getJElem("stacking", "normal"));
			// typeOptions.put("stacking", "normal");
		}

		obj.put("plotOptions", plotOptions);

		if (this.getChartOptionSelected("labels")
				|| this.getChartOptionSelected("labels-rotated")) {

			JSONObject dl = getJElem("enabled", new Boolean("true"));
			if (this.getChartOptionSelected("labels-rotated")) {
				dl.put("rotation", new Integer(-90));
				dl.put("x", -3);
				dl.put("y", 10);
				dl.put("align", "right");
				dl.put("color", "#FFFFFF");
			}
			typeOptions.put("dataLabels", dl);

			/*
			 * dataLabels: { enabled: true, color: Highcharts.theme.textColor ||
			 * '#000000', connectorColor: Highcharts.theme.textColor ||
			 * '#000000', formatter: function() { return '<b>'+ this.point.name
			 * +'</b>: '+ this.y +' %'; } }
			 */
		}
		/*
		 * I'd like this to eventually be a context menu type operation. Shows a
		 * jQuery UI box at present
		 */
		typeOptions.put("events", getJElem("click", new PointInfo()));

		/*
		 * line: { events: { click: function(event) {
		 * console.log(event.point.config.link); } } }
		 */
		if ("bar".equals(jChartType)) {
			typeOptions.put("allowPointSelect", "true");
			typeOptions.put("cursor", "pointer");
		} else if ("pie".equals(this.getChartType())) {
			typeOptions.put("allowPointSelect", "true");
			typeOptions.put("cursor", "pointer");
			/* display the legend at the bottom of the page */
			typeOptions.put("showInLegend", new Boolean(true));
			// TODO
			JSONObject dLabels = new JSONObject();
			String pielabel = " return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage) + '%'; ";
			dLabels.put("formatter",
					new JSONFunction(pielabel, (String[]) null));
			dLabels.put("enabled", "true");
			typeOptions.put("dataLabels", dLabels);
		}

		boolean rightaxis = true;

		/* we can use this if we want pie charts restricted to one series */
		if ("pie".equals(this.getChartType())) {
			JSONObject s1 = null;
			JSONArray series = new JSONArray();

			if ("territory_disabled".equals(this.getString(coltrigger))) {
				NumberFormat nf = new DecimalFormat("0.00%");
				double d = (1.0d / (double) this.getColumnCount()) * 0.9d;
				if (this.getRowCount() > 0) {
					int i = 0;

					for (int j = (this.getColumnCount() - 1); j >= 0; j--) {
						// for(int j=0; j<this.getColumnCount(); j++) {
						s1 = new JSONObject();
						s1.put("type", this.getJsonChartType());
						s1.put("name", this.getColumnTitle(j));
						s1.put("size", nf.format(1d - d
								* ((double) (this.getColumnCount() - j))));
						JSONArray data = new JSONArray();
						data.add(getJData(StringUtil.replace(
								this.getColumnTitle(j), String.valueOf('+'),
								"<br/>"), new Double(this.getItem(i, j)), this
								.getLink(i, j)));
						s1.put("data", data);
						series.add(s1);
					}
				}
			} else {
				s1 = new JSONObject();
				s1.put("type", this.getJsonChartType());
				s1.put("name", this.getTitle());

				JSONArray data = new JSONArray();

				if (this.getRowCount() > 0) {
					// for(int i=0; i<this.getRowCount(); i++) {
					int i = 0;
					for (int j = 0; j < this.getColumnCount(); j++) {
						data.add(getJData(StringUtil.replace(
								this.getColumnTitle(j), String.valueOf('+'),
								"<br/>"), new Double(this.getItem(i, j)), this
								.getLink(i, j)));
					}
					// }
					s1.put("data", data);
					series.add(s1);
				}
			}
			obj.put("series", series);
		} else {
			// IE NOT A PIE
			JSONArray series = new JSONArray();

			JSONArray data = null;
			JSONObject sVal = null;
			JSONObject sVal2 = null;

			// NumberFormat nf = new DecimalFormat("0.00%");
			// double d = (1.0d/(double)this.getColumnCount())*0.9d;
			boolean ts = false;
			long range = 0;
			Calendar sdc = null;
			if (this.getChartOptionSelected("timeseries")) {

				if ("hourrange".equals(get("-rowtrigger"))) {
					range = 3600L * 1000L;
				} else if ("dayrange".equals(get("-rowtrigger"))) {
					range = 3600L * 24L * 1000L;
				} else if ("weekrange".equals(get("-rowtrigger"))) {
					range = 3600L * 24L * 7L * 1000L;
				} else if ("monthrange".equals(get("-rowtrigger"))) {
					/* TODO: this may be different month to month */
					range = 3600L * 24L * 28L * 1000L;
				} else if ("yearrange".equals(get("-rowtrigger"))) {
					range = 3600L * 365L * 1000L;
				}
			}

			for (int j = 0; j < this.getColumnCount(); j++) {
				sVal = new JSONObject();
				if (this.getChartOptionSelected("timeseries")) {
					// default to hour
					if (range != 0) {
						if (rowvars.length > 0) {
							java.util.Date sd = getDate(rowvars[1][0]);
							sdc = new GregorianCalendar();
							sdc.setTime(getDate(rowvars[1][0]));
							sVal.put("name", this.getColumnTitle(j));
							sVal.put("pointInterval", new Long(range));
							sVal.put(
									"pointStart",
									new JSONData("\"Date.UTC("
											+ sdc.get(Calendar.YEAR) + ","
											+ sdc.get(Calendar.MONTH) + ","
											+ sdc.get(Calendar.DAY_OF_MONTH)
											+ ")\""));

							data = new JSONArray();
							for (int i = 0; i < this.getRowCount(); i++) {
								// data.add(new Double(getItem(i,j)));
								data.add(getJData(StringUtil.replace(
										this.getRowTitle(i),
										String.valueOf('+'), "<br/>"),
										new Double(this.getItem(i, j)), this
												.getLink(i, j)));
							}
							sVal.put("data", data);
							ts = true;
						}
					}
				}

				if (this.getChartOptionSelected("pie-summary")) {
					NumberFormat f = NumberFormat.getPercentInstance();
					if (sVal2 == null) {
						sVal2 = new JSONObject();
						sVal2.put("type", "pie");
						sVal2.put("data", new JSONArray());
						sVal2.put("size", 120);
						sVal2.put("center", new JSONData("[65,20]"));
						sVal2.put("showInLegend", false);
						sVal2.put("dataLabels", getJElem("enabled", false));
					}
					long sum = 0L;
					for (int i = 0; i < this.getRowCount(); i++) {
						sum += Long.parseLong(this.getItem(i, j));
					}
					JSONObject o = new JSONObject();
					o.put("name", this.getColumnTitle(j));
					o.put("y", sum);
					o.put("perc", f.format(sum / total));
					o.put("color", new JSONData(
							"\"Highcharts.getOptions().colors["
									+ ((JSONArray) sVal2.get("data")).size()
									+ "]\""));
					((JSONArray) sVal2.get("data")).add(o);
				}

				if (!ts) {
					sVal.put("type", getSeriesType(jChartType));
					/*
					 * unreachable at present. if("pie".equals(jChartType)) {
					 * if(this.getColumnCount()>1) {
					 * sVal.put("dataLabels",getJElem
					 * ("enabled",j==(this.getColumnCount()-1)?"true":"false"));
					 * sVal.put("size",
					 * nf.format(d*((double)(this.getColumnCount()-j)))); //in
					 * some cases need innerSize } }
					 */
					sVal.put("name", this.getColumnTitle(j));
					data = new JSONArray();
					for (int i = 0; i < this.getRowCount(); i++) {
						// data.add(new Double(this.getValue(i, j)));
						data.add(getJData(
								StringUtil.replace(this.getRowTitle(i),
										String.valueOf('+'), "<br/>"),
								new Double(this.getItem(i, j)),
								this.getLink(i, j)));
					}
					sVal.put("data", data);
				}
				series.add(sVal);
			}

			if (sVal2 != null) {
				// we want to add this last explicitly.
				series.add(sVal2);
			}

			/*
			 * we're actually calculating this in the opposite direction to the
			 * above, so iterating twice is *probably* better.
			 */
			if (this.getChartOptionSelected("average-trailing") && ts
					&& range != 0 && sdc != null) {

				int points = 0;

				String spoints = (String) get("-points");
				if (spoints == null || spoints.length() == 0) {
					spoints = "6";
				}
				try {
					points = Integer.parseInt(spoints);
				} catch (Exception e) {
				}

				if (points == 0) {
					if ("hourrange".equals(get("-rowtrigger"))) {
						// range = 3600 * 1000;
						points = 24 * 44;
					} else if ("dayrange".equals(get("-rowtrigger"))) {
						// range = 3600 * 24 * 1000;
						points = 44;
					} else if ("weekrange".equals(get("-rowtrigger"))) {
						// range = 3600 * 24 * 7 * 1000;
						points = 6;
					} else if ("monthrange".equals(get("-rowtrigger"))) {
						// range = 3600 * 24 * 28 * 1000;
						points = 1;
					} else if ("yearrange".equals(get("-rowtrigger"))) {
						// range = 3600 * 365 * 1000;
					}
					if (points > 0)
						put("-points", String.valueOf(points));
				}

				if (points > 0) {
					/*
					 * A trailing average is the 3 month range around a
					 * particular point. To do this, we need to average out the
					 * values forward - and if available - back from a given
					 * data point To produce the average.
					 */
					for (int j = 0; j < this.getColumnCount(); j++) {
						sVal = new JSONObject();
						sVal.put("name", this.getColumnCount() > 0 ? "Tr Av - "
								+ this.getColumnTitle(j) : "Trailing Average");
						sVal.put("type", "spline");

						sVal.put("pointInterval", new Long(range));
						sVal.put(
								"pointStart",
								new JSONData("\"Date.UTC("
										+ sdc.get(Calendar.YEAR) + ","
										+ sdc.get(Calendar.MONTH) + ","
										+ sdc.get(Calendar.DAY_OF_MONTH)
										+ ")\""));

						data = new JSONArray();

						int rowmax = this.getRowCount() - 1;
						for (int i = 0; i < this.getRowCount(); i++) {
							double val = 0d;
							int counted = 0;
							int start = i - points;
							int end = i + points;
							if (start < 0)
								start = 0;
							if (end > rowmax) {
								end = rowmax;
							}
							for (int k = start; k <= end; k++) {
								// doing this a zillion times is craptastic.
								// maybe cast the whole result set?
								val += Double.parseDouble(this.getItem(k, j));
								counted++;
							}
							if (val > 0) {
								val = val / counted;
							}
							data.add(new Double(val));
						}

						sVal.put("data", data);
						series.add(sVal);
					}
				}

			} else if (this.getChartOptionSelected("average")) {
				sVal = new JSONObject();
				sVal.put("name", "Average");
				sVal.put("type", "spline");
				data = new JSONArray();
				for (int i = 0; i < this.getRowCount(); i++) {
					double val = 0d;
					for (int j = 0; j < this.getColumnCount(); j++) {
						val += Double.parseDouble(this.getItem(i, j));
					}
					if (val > 0) {
						val = val / this.getColumnCount();
					}
					data.add(new Double(val));
				}
				sVal.put("data", data);
				series.add(sVal);
			}

			if (this.rpspecr != null) {
				for (int j = 0; j < this.getRightCount(); j++) {
					sVal = new JSONObject();
					sVal.put("type", "spline");

					// sVal.put("type",getSeriesType(jChartType));
					sVal.put("name", this.getRightTitle(j));
					data = new JSONArray();
					for (int i = 0; i < this.getRowCount(); i++) {
						// data.add(new Double(this.getValue(i, j)));
						data.add(getJData(
								StringUtil.replace(this.getRightTitle(j) + "-"
										+ this.getRowTitle(i),
										String.valueOf('+'), "<br/>"),
								new Double(this.getItem(i, j, ListType.Right)),
								this.getLink(i, j, ListType.Right)));
					}
					if (this.rpspec == this.rpspecr
							&& this.getRightTitle(j).equals(
									this.getColumnTitle(j))) {
						rightaxis = false;
					} else {
						sVal.put("yAxis", 1);
					}
					sVal.put("data", data);
					series.add(sVal);
				}
				JSONObject po = (JSONObject) obj.get("plotOptions");
				if (po == null) {
					po = new JSONObject();
					obj.put("plotOptions", po);
				}
				po.put("spline",
						getJElem("events", getJElem("click", new PointInfo())));
			}

			obj.put("series", series);

			/* axis labels */

			JSONObject axis = new JSONObject();

			if (this.hasRowTitles() && !ts) {
				JSONArray labels = new JSONArray();
				for (int j = 0; j < this.getRowCount(); j++) {
					labels.add(this.getRowTitle(j));
				}
				axis.put("categories", labels);

				if (labels.size() > 10) {
					axis.put(
							"labels",
							new JSONData(
									"{ \"x\": 5, \"y\" : 20, \"rotation\": -45, \"align\": \"right\" }"));
				}
			}
			if (ts) {
				axis.put("type", "datetime");
				axis.put("maxZoom", new Double(14 / 24 * 60 * 60 * 1000));
			}
			axis.put("title", this.getRowSubsetName());

			if (axis.get("title").equals(ActionBean._blank)) {
				axis.put("title", "Records");
			}

			/*
			 * if(this.hasColTitles()) { JSONArray labels = new JSONArray();
			 * for(int i=0; i<this.getColumnCount(); i++) {
			 * labels.add(this.getColumnTitle(i)); }
			 * axis.put("categories",labels); }
			 */
			obj.put("xAxis", axis);

			axis = new JSONObject();

			if (this.hasColTitles()) {
				/*
				 * We don't need col titles, they're defined in the series
				 * JSONArray labels = new JSONArray(); for(int j=0;
				 * j<this.getColumnCount(); j++) {
				 * labels.add(this.getColumnTitle(j)); }
				 * axis.put("categories",labels);
				 */
			}
			if (this.hasColSummary()) {
				axis.put("title", getJElem("text", this.getColSubsetName()));
			} else {
				axis.put("title", getJElem("text", "Records"));
			}
			axis.put("min", new Integer(0));

			if (this.rpspecr != null && rightaxis) {
				/* add an extra y axis */
				JSONArray yAxis = new JSONArray();
				yAxis.add(axis);
				axis = new JSONObject();
				axis.put("min", 0);
				if (this.hasRightSubset()) {
					axis.put("title",
							getJElem("text", this.getRightSubsetName()));
				} else {
					axis.put("title", getJElem("text", "count"));
				}
				axis.put("opposite", new Boolean(true));
				yAxis.add(axis);
				obj.put("yAxis", yAxis);
			} else {
				obj.put("yAxis", axis);
			}
		}
		return (chartJson = obj);
	}

	public String getSeriesType() {
		return getSeriesType(getJsonChartType());
	}

	public String getSeriesType(String ct) {
		if (ct.equals("line")) {
			if (getChartOptionSelected("spline")) {
				if (getChartOptionSelected("area")) {
					return "areaspline";
				}
				return "spline";
			} else if (getChartOptionSelected("area")) {
				return "area";
			}
		}
		return ct;
	}

	public List<String> getChartTypes() {
		List<String> types = new ArrayList<String>();
		types.add("bar");
		types.add("column");
		types.add("line");
		types.add("pie");
		return types;
	}

	public boolean getChartOptionAvailable(String option) {
		return getChartOptions() != null && getChartOptions().contains(option);
	}

	public boolean getChartOptionSelected(String option) {
		String[] opts = null;
		if (this.get("-chartopts") instanceof String) {
			opts = new String[1];
			opts[0] = (String) this.get("-chartopts");
		} else {
			opts = (String[]) this.get("-chartopts");
		}
		if (opts != null && option != null) {
			for (String o : opts) {
				if (o != null && o.equals(option)) {
					return getChartOptionAvailable(option);
				}
			}
		}
		return false;
	}

	public void setChartOption(String option) {
		String[] opts = null;
		if (this.get("-chartopts") instanceof String) {
			opts = new String[1];
			opts[0] = (String) this.get("-chartopts");
		} else {
			opts = (String[]) this.get("-chartopts");
		}
		if (opts == null) {
			opts = new String[] { option };
			this.put("-chartopts", opts);
		} else if (!this.getChartOptionSelected(option)
				&& this.getChartOptionAvailable(option)) {
			// List list = Arrays.asList(opts);
			// list.add(option);
			String[] opts2 = new String[opts.length + 1];
			for (int i = 0; i < opts.length; i++) {
				opts2[i] = String.valueOf(opts[i]);
			}
			opts2[opts.length] = option;
			this.put("-chartopts", opts2);
		}
	}

	private static final Map<String, String[]> optionsMap = new HashMap<String, String[]>();
	private static final Set<GraphPreset> presetSet = new LinkedHashSet<GraphPreset>();

	public static final String CATEGORY_LINE = "Line";
	public static final String CATEGORY_AREA = "Area";
	public static final String CATEGORY_SPLINE = "Spline";
	public static final String CATEGORY_BAR = "Bar";
	public static final String CATEGORY_COLUMN = "Column";
	public static final String CATEGORY_PIE = "Pie";
	public static final String CATEGORY_3RD = "Combination";

	static {
		optionsMap.put("labels",
				new String[] { "line", "bar", "column", "pie" });
		optionsMap.put("labels-rotated", new String[] { "column" });
		optionsMap.put("stacking", new String[] { "line", "bar", "column" });
		optionsMap.put("zoomable", new String[] { "line", "bar", "column" });
		optionsMap.put("spline", new String[] { "line" });
		optionsMap.put("area", new String[] { "line" });
		optionsMap.put("inverted", new String[] { "line" });
		optionsMap.put("stacking-percentage", new String[] { "line", "bar",
				"column" });
		optionsMap.put("average", new String[] { "line", "bar", "column" });
		optionsMap.put("average-trailing", new String[] { "line", "bar",
				"column" });
		optionsMap.put("timeseries", new String[] { "line" });
		optionsMap.put("legend",
				new String[] { "line", "bar", "column", "pie" });
		optionsMap.put("third-axis", new String[] { "line", "bar", "column" });
		optionsMap.put("pie-summary", new String[] { "line", "bar", "column" });

		// legend
		presetSet.add(new GraphPreset("Basic line", CATEGORY_LINE, "line",
				(String) null));
		presetSet.add(new GraphPreset("Line with data labels", CATEGORY_LINE,
				"line", "labels"));
		presetSet.add(new GraphPreset("Line Time series, zoomable",
				CATEGORY_LINE, "line", "timeseries", "zoomable"));
		presetSet.add(new GraphPreset(
				"Line Time series, zoomable with trailing average",
				CATEGORY_LINE, "line", "average-trailing", "timeseries",
				"zoomable"));

		presetSet.add(new GraphPreset("Spline", CATEGORY_SPLINE, "line",
				"spline"));
		presetSet.add(new GraphPreset("Spline with inverted axes",
				CATEGORY_SPLINE, "line", "spline", "inverted"));

		presetSet.add(new GraphPreset("Area Time series, zoomable",
				CATEGORY_AREA, "line", "area", "timeseries", "zoomable"));

		presetSet.add(new GraphPreset("Basic area", CATEGORY_AREA, "line",
				"area"));
		presetSet.add(new GraphPreset("Stacked area", CATEGORY_AREA, "line",
				"area", "stacking"));
		presetSet.add(new GraphPreset("Percentage area", CATEGORY_AREA, "line",
				"area", "stacking-percentage"));
		// presetSet.add(new
		// GraphPreset("Area with inverted axes",CATEGORY_AREA,"line","area","inverted"));
		presetSet.add(new GraphPreset("Area-spline", CATEGORY_AREA, "line",
				"area", "spline"));

		presetSet.add(new GraphPreset("Basic bar", CATEGORY_BAR, "bar",
				"labels")); // legend
		presetSet.add(new GraphPreset("Stacked bar", CATEGORY_BAR, "bar",
				"stacking"));
		presetSet
				.add(new GraphPreset("Basic column", CATEGORY_COLUMN, "column")); // legend
		presetSet.add(new GraphPreset("Stacked column", CATEGORY_COLUMN,
				"column", "stacking")); // legend
		presetSet.add(new GraphPreset("Stacked percentage column",
				CATEGORY_COLUMN, "column", "stacking-percentage")); // legend
		presetSet.add(new GraphPreset("Column with labels rotated",
				CATEGORY_COLUMN, "column", "labels-rotated"));
		presetSet.add(new GraphPreset("Column with line average",
				CATEGORY_COLUMN, "column", "average"));
		presetSet.add(new GraphPreset("Column with trailing average",
				CATEGORY_COLUMN, "line", "average-trailing"));

		// Column with rotated labels
		presetSet
				.add(new GraphPreset("Pie", CATEGORY_PIE, "pie", (String) null));
		presetSet.add(new GraphPreset("Pie with labels", CATEGORY_PIE, "pie",
				"legend"));
		presetSet.add(new GraphPreset("3rd Axis bar & line", CATEGORY_3RD,
				"bar", "third-axis"));
		presetSet.add(new GraphPreset("3rd Axis column & line", CATEGORY_3RD,
				"column", "third-axis"));
		presetSet.add(new GraphPreset("3rd Axis Stacked column & line",
				CATEGORY_3RD, "column", "stacking", "third-axis")); // legend

		presetSet.add(new GraphPreset("Line with pie summary", CATEGORY_3RD,
				"line", "pie-summary")); // legend
		presetSet.add(new GraphPreset("Stacked column with pie summary",
				CATEGORY_3RD, "column", "stacking", "pie-summary")); // legend
	}

	public static Set<GraphPreset> getGraphPresets() {
		return presetSet;
	}

	public static class GraphPreset {
		private String presetName;
		private String chartType;
		private String category;
		private String[] options;

		public GraphPreset(String presetName, String category,
				String chartType, String... options) {
			this.presetName = presetName;
			this.category = category;
			this.chartType = chartType;
			if (options == null) {
				this.options = new String[0];
			} else {
				this.options = options;
			}
		}

		public final String getPresetName() {
			return this.presetName;
		}

		public final String getCategory() {
			return this.category;
		}

		public final String getChartType() {
			return this.chartType;
		}

		public final String[] getOptions() {
			return this.options;
		}
	}

	public List<String> getChartOptions() {
		List<String> opts = new ArrayList<String>();
		// String chartType = getJsonChartType();
		for (Map.Entry<String, String[]> s : optionsMap.entrySet()) {
			// was using this to only display applicable options; not really
			// relevant now.
			// for (String t: s.getValue()) {
			// if(t.equals(chartType)) {
			opts.add(s.getKey());
			// break;
			// }
			// }
		}
		return opts;
	}

	protected String getJsonChartType() {
		if (this.getChartType().equals("pie")) {
			return "pie";
		} else if (this.getChartType().contains("erticalBar")) {
			return "column";
		} else if (this.getChartType().contains("orizontalBar")) {
			return "bar";
		} else {
			return "line";
		}
	}

	@SuppressWarnings("unchecked")
	protected class PointInfo implements org.json.simple.JSONAware {

		@Override
		public String toJSONString() {
			StringBuilder sb = new StringBuilder("function(event) {");

			JSONObject opts = new JSONObject();
			JSONObject bad = new JSONObject();
			sb.append("window.lastjsevent = event;");
			opts.put("pageOrigin", new JSONData(
					"{ x: event.pageX, y: event.pageY }"));
			opts.put("headingText", new JSONData("event.point.series.name"));
			opts.put(
					"maincontentText",
					new JSONData(
							"event.point.config.name + ': ' + event.point.config.y + ' records<br/>"
									+ "<a searchurl=\"'+event.point.config.link+'\" href=\"#\" onclick=\"showsearch(this); return false;\">View these records</a>'"));
			opts.put("width", new Integer(200));
			bad.put("pageOrigin", new JSONData(
					"{ x: event.pageX, y: event.pageY }"));
			bad.put("headingText", new JSONData("event.point.series.name"));
			bad.put(
					"maincontentText",
					new JSONData(
							"'Custom Report charts have no click throughs, the table below may have click throughs'"));
			bad.put("width", new Integer(300));
			sb.append("if (typeof event.point.config == 'undefined' || typeof event.point.config.name == 'undefined') ");
			sb.append("createDialog(");
			sb.append(bad.toJSONString());
			sb.append(");");
			sb.append(" else ");
			sb.append("createDialog(");
			sb.append(opts.toJSONString());
			sb.append(");");
			sb.append("}");
			return "\"" + StringEscapeUtils.escapeJava(sb.toString()) + "\"";
		}
	}

	protected class JSONData implements org.json.simple.JSONAware {
		private String str;

		public JSONData(String str) {
			this.str = str;
		}

		@Override
		public String toJSONString() {
			return this.str;
		}
	}

	protected class JSONFunction implements org.json.simple.JSONAware {

		private String str;
		private String[] vars;

		public JSONFunction(String str, String... vars) {
			this.str = str;
			this.vars = vars;
		}

		public void set(String str, String... vars) {
			this.str = str;
			this.vars = vars;
		}

		@Override
		public String toJSONString() {
			StringBuilder sb = new StringBuilder("function (");
			if (this.vars != null) {
				for (String s : vars) {
					sb.append(s).append(",");
				}
				sb.deleteCharAt(sb.length() - 1);
			}
			sb.append(") { ").append(this.str).append("}");
			return "\"" + StringEscapeUtils.escapeJava(sb.toString()) + "\"";
		}
	}

	@SuppressWarnings("unchecked")
	public static JSONObject getJElem(String sub, Object value) {
		JSONObject v = new JSONObject();
		v.put(sub, value);
		return v;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject getJData(String name, Double value, String link) {
		JSONObject v = new JSONObject();
		v.put("name", name);
		v.put("y", value);
		v.put("link", link);
		return v;
	}

	/* is there generic methods for these? */
	private static final String[][] NUMBER_FORMATS = { { "Standard", _blank },
			{ "Rounded", "#0", "NUM_R" }, { "2 Dec.", "#0.00", "NUM_2D" },
			{ "Currency Rounded", "$#", "CURR_R" },
			{ "Currency 2 Dec.", "$#0.00", "CURR_2D" },
			{ "Percentage Rounded", "#0%", "PERC_R" },
			{ "Percentage 2 Dec.", "#0.00%", "PERC_2D" } };
	private static final String[][] REPORT_TYPES = {
			{ "Standard", _blank },
			{ "Custom", "Custom", "ReportSetCustom" },
			{ "Status to Status", "StatusToStatus",
					"ReportSetStatusToStatusDuration" } };

	private static final String[][] SUMMARY_TYPES = { { "None", _blank },
			{ "Total" }, { "Average" } };

	public static void printSummaryOptions(Writer out, String currentValue)
			throws IOException {
		printReportOptions(out, currentValue, SUMMARY_TYPES);
	}

	public static void printFormatOptions(Writer out, String currentValue)
			throws IOException {
		printReportOptions(out, currentValue, NUMBER_FORMATS);
	}

	public static void printTypeOptions(Writer out, String currentValue)
			throws IOException {
		printReportOptions(out, currentValue, REPORT_TYPES);
	}

	public static void printReportOptions(Writer out, String currentValue,
			String[][] options) throws IOException {
		for (String[] option : options) {
			printReportOption(out, currentValue, option);
		}
	}

	public static void printReportOptions(Writer out, String currentValue,
			List<String[]> options) throws IOException {
		for (String[] option : options) {
			printReportOption(out, currentValue, option);
		}
	}

	public static void printReportOption(Writer out, String currentValue,
			String[] option) throws IOException {
		out.write("<option value=\"");
		out.write(option[option.length > 1 ? values : names]);
		if (option.length > 2) {
			out.write("\" data-type=\"");
			out.write(option[types]);
		}
		out.write("\"");
		if (option[option.length > 1 ? values : names].equals(currentValue)) {
			out.write(" selected=\"selected\"");
		}
		out.write(">");
		out.write(option[names]);
		out.write("</option>");
	}

	@Override
	public Connection getConnection() {
		try {
			if (con == null || con.isClosed()) {
				con = ActionBean.connect();
				//this.setCloseConnection(true);
				/*
				ctx = new InitialContext();

				DataSource ds = null;
				try {
					dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");  
					Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
					ds = (DataSource) envContext.lookup(dbconn);
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Error making a connection to DB1 ", e);
				}
				if (ds == null) {
					ds = (DataSource) ctx.lookup(dbconn);
				}
      			try {
   					ctx.close();
   					ctx = null;
   				}
   				catch(Exception e) { 
   					e.printStackTrace();
   					logger.error("Error making a connection to DB2 ", e);
   				}
				con = ds.getConnection();
				*/
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error making a connection to DB3 ", e);
			return null;
		}

		return con;
	}

	/*
	 * public static void main(String[] args) { CustomReportBean rb = new
	 * CustomReportBean(); rb.put(begindate,"LAST3DAYS");
	 * //rb.put(enddate,"2/12/2000"); rb.rowvars =
	 * rb.getRange(Calendar.DAY_OF_MONTH); //rb.rowvars =
	 * rb.getRange(Calendar.MONTH); //rb.rowvars = rb.getRange(Calendar.YEAR);
	 * if(rb.rowvars[1]!=null) { for(int i=0; i<rb.rowvars[values].length; i++)
	 * { if(i<rb.rowvars[names].length) { System.out.print("name: ");
	 * System.out.println(rb.rowvars[names][i]); } System.out.print("value: ");
	 * System.out.println(rb.rowvars[values][i]); } } else {
	 * System.out.println("rowvalues is null"); }
	 * System.out.println(rb.getError()); }
	 * 
	 * 
	 * public static void main(String[] args) { ArrayList<java.util.Date> dates
	 * = new ArrayList<java.util.Date>(); dates.add(new java.util.Date());
	 * dates.add(new java.util.Date()); dates.add(new java.util.Date());
	 * 
	 * String[] valuelist = new String[dates.size()]; valuelist = (String[])
	 * dates.toArray(valuelist);
	 * 
	 * for(Object o: valuelist) { System.out.println(o.getClass().getName());
	 * System.out.println(String.valueOf(o)); } }
	 */
}
