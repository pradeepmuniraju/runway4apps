package com.sok.runway;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Arrays;
import java.util.Map; 

import com.sok.framework.GenRow;

public class HTMLUtil { 
	 
	private static int value = 0;
	private static int name = 1;
	
	private static final String OPTION_PRE = "<option value=\"";
	private static final String OPTION_END = "</option>";
	
	private static final String SELECT_PRE = "<select";
	private static final String SELECT_END = "</select>";
	
	private static final String SPACE = " ";
	private static final String EQUALS = "=";
	private static final String BRACKET = "\"";
	private static final String SELECTED = "selected=\"selected\"";
	private static final String HTML_CLOSE = ">";
	
	public static void printSelect(String params, Collection<String[]> values, String selected, Writer out) throws IOException
	{
		out.write(SELECT_PRE);
		out.write(SPACE);
		out.write(params);
		out.write(HTML_CLOSE);
		printOptions(values, selected, out);
		out.write(SELECT_END);
	}
	
	public static void printSelect(String params, String[][] values, String selected, Writer out) throws IOException {
		HTMLUtil.printSelect(params, Arrays.asList(values), selected, out);
	}
	
	public static void printSelect(Map<String, String> params, String[][] values, String selected, Writer out) throws IOException {
		HTMLUtil.printSelect(params, Arrays.asList(values), selected, out);
	}
	
	public static void printSelect(Map<String, String> params, Collection<String[]> values, String selected, Writer out) throws IOException
	{
		out.write(SELECT_PRE);
		for(Map.Entry<String, String> e: params.entrySet())
		{
			out.write(SPACE);
			out.write(e.getKey());
			out.write(EQUALS);
			out.write(BRACKET);
			out.write(e.getValue());
			out.write(BRACKET);
		}
		out.write(HTML_CLOSE);
		printOptions(values, selected, out);
		out.write(SELECT_END);
	}
	
	public static void printOptions(String[][] values, String selected, Writer out) throws IOException {
		HTMLUtil.printOptions(Arrays.asList(values), selected, out);
	}
	
	public static <T> void printOptions(Collection<T> values, String selected, Writer out) throws IOException { 
		for(T obj: values) 
		{
			if(obj instanceof String) 
				printOption((String)obj,selected,(String)obj,out);
			else if (obj instanceof String[]) 
			{
				String[] item = (String[])obj; 
				printOption(item[value],selected,item[name],out);
			}
		}
	}
	
	public static void printOptions(GenRow rows, String selected, Writer out) throws IOException { 
		printOption("",selected,"",out);
		while(rows.getNext()) 
		{
			printOption(rows.getData("Value"),selected,rows.getData("Name"),out);
		}
	}
	//public static void printOptions(Collection<String[]> values, String selected, Writer out) throws IOException { 
	//	for(String[] item: values) 
	//		printOption(item[value],selected,item[name],out);
	//}
	
	public static void printOption(String value, String selected, String label, Writer out) throws IOException { 
		out.write(OPTION_PRE); 
		out.write(value);
		out.write(BRACKET);
		if(value.equals(selected)) { 
			out.write(SELECTED);
		}
		out.write(HTML_CLOSE);
		out.write(label); 
		out.write(OPTION_END);
	}
}
