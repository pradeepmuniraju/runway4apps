/**
 * 
 */
package com.sok.runway5.properties.entity;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.sok.runway5.properties.dao.PropertyLandList;
import com.sok.runway5.properties.dao.PropertyPlanList;

/**
 * @author dion
 *
 */
public class PropertyPlan extends PropertyEntity {

	public final String			EntityType = "Home Plan";
	
	private PropertyPlanList	planList = null;
	/**
	 * @param id
	 * @param name
	 * @param active
	 */
	public PropertyPlan(String id, String name, String active) {
		super(id,name,active);
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#getType()
	 */
	@Override
	public String getType() {
		return EntityType;
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
	}
	
	@Override
	public void loadChildren(HttpServletRequest request) {
		loadChildren(request, false);
	}

	@Override
	public void loadChildren(HttpServletRequest request, boolean loadALL) {
		planList = new PropertyPlanList("ProductParentStageLot.ProductVariationStageLot.ProductID", getId());
		if (!loadALL) planList.setActive(true);
		planList.loadData(request,loadALL);
		
		entityMinimum = (planList.getMinimum() > 0)? planList.getMinimum() : 0; ;
		entityMaximum = (planList.getMaximum() > entityMinimum)? planList.getMaximum() : (entityMinimum + 1);
	}

	@Override
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", getId());
		obj.put("name", getName());
		obj.put("active", getActive());
		obj.put("minimum", entityMinimum);
		obj.put("maximum", entityMaximum);
		
		if (planList != null) {
			obj.put("children", planList.getItems().size());
			JSONArray array = new JSONArray();
			
			for (int v = 0; v < planList.size(); ++v) {
				array.add(planList.getItems().get(v).getJson());
			}
			
			obj.put("entities", array);
		}
		return obj;
	}
	
}
