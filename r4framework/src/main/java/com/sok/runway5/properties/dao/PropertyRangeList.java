/**
 * 
 */
package com.sok.runway5.properties.dao;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway5.properties.entity.PropertyRange;

/**
 * @author dion
 *
 */
public class PropertyRangeList extends PropertyEntityList {
	
	private String					active = "";
	private final static String		productType = "Home Range";

	/**
	 * 
	 */
	public PropertyRangeList() {
		super(productType);
	}

	/**
	 * @param type
	 */
	private PropertyRangeList(String type) {
		super(type);
	}

	/**
	 * @param key
	 * @param value
	 */
	public PropertyRangeList(String key, String value) {
		super(key, value, productType);
	}

	/**
	 * @param key
	 * @param value
	 * @param type
	 */
	private PropertyRangeList(String key, String value, String type) {
		super(key, value, type);
	}

	/**
	 * Load up all the ids, names and active
	 * 
	 * @param request
	 */
	public void loadData(HttpServletRequest request) {
		loadData(request,false);
	}

	/**
	 * Load up all the ids, names and active, if loadAll is true then all data is also loaded into a Map
	 * 
	 * @param request
	 * @param loadALL 
	 */
	public void loadData(HttpServletRequest request, boolean loadAll) {
		GenRow row = new GenRow();
		row.setViewSpec("ProductShortView");
		if (super.getFilterType() != null && super.getFilterType().length() > 0) row.setColumn("ProductType", super.getFilterType());
		if (super.getFilterKey() != null && super.getFilterValue() != null && super.getFilterKey().length() > 0 && super.getFilterValue().length() > 0) row.setColumn(super.getFilterKey(), super.getFilterValue());
		if (active != null && active.length() > 0) row.setColumn("Active", active);
		row.sortBy("Name", 0);
		row.setParameter("-groupby0", "ProductID");
		row.doAction(GenerationKeys.SEARCH);
		
		row.getResults();
		
		while (row.getNext()) {
			PropertyRange pe = new PropertyRange(row.getString("ProductID"), row.getString("Name"), row.getString("Active"));
			if (loadAll) pe.loadChildren(request);
			pe.loadSecurityGroups(request, row.getData("GroupID"), row.getData("SectionCode"));
			itemList.add(pe);
			itemMap.put(pe.getId(), pe);
		}
		
		row.close();
		
		getRange(request);
	}

	protected void getRange(HttpServletRequest request) {
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		row.setParameter("-select1", "MIN(TotalCost) AS MinimumCost");
		row.setParameter("-select2", "MAX(TotalCost) AS MaximumCost");
		row.setColumn("ProductType", "Home Plan");
		row.setParameter("Category", "NULL");
		row.doAction(GenerationKeys.SELECTFIRST);
		
		if (row.getDouble("MinimumCost") < entityMinimum || entityMinimum < 0) entityMinimum = row.getDouble("MinimumCost"); 
		if (row.getDouble("MaximumCost") > entityMaximum || entityMaximum < 0) entityMaximum = row.getDouble("MaximumCost"); 
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		if (active.toUpperCase().startsWith("Y")) {
			this.active = "Y";
		} else if (active.toUpperCase().startsWith("N")) {
			this.active = "N";
		} else {
			this.active = "";
		}
	}
	
	public void setActive(boolean active) {
		if (active) {
			this.active = "Y";
		} else {
			this.active = "";
		}
	}

	@Override
	public Object get(Object obj) {
		obj.toString();
		return null;
	}
	
}
