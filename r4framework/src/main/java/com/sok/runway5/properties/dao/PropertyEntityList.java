package com.sok.runway5.properties.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway5.properties.entity.PropertyEntity;

public abstract class PropertyEntityList {
	private final String		filterKey;
	private final String		filterValue;
	
	private final String 		filterType;
	
	protected Vector<PropertyEntity> itemList = new Vector<PropertyEntity>();
	protected HashMap<String,PropertyEntity> itemMap = new HashMap<String,PropertyEntity>();
	
	protected double					entityMinimum = -1;
	protected double					entityMaximum = -1;
	
	private Context						ctx = null;
	private String 						dbconn = null;
	
	protected int activeCount = 0;
	protected int inactiveCount = 0;
	
	protected int activePackages = -1;
	

	public PropertyEntityList() {
		filterKey = "";
		filterValue = "";
		filterType = "";
	}

	public PropertyEntityList(String type) {
		filterKey = "";
		filterValue = "";
		filterType = type;
	}

	public PropertyEntityList(String key, String value) {
		filterKey = key;
		filterValue = value;
		filterType = "";
	}

	public PropertyEntityList(String key, String value, String type) {
		filterKey = key;
		filterValue = value;
		filterType = type;
	}
	
	public abstract void loadData(HttpServletRequest request);
	public abstract void loadData(HttpServletRequest request, boolean loadAll);
	
	public Vector<PropertyEntity> getItems() {
		return itemList;
	}
	
	public abstract void setActive(boolean active);
	
	public PropertyEntity getItemByID(String id) {
		return itemMap.get(id);
	}

	public String getFilterKey() {
		return filterKey;
	}

	public String getFilterValue() {
		return filterValue;
	}

	public String getFilterType() {
		return filterType;
	}
	
	public int size() {
		return itemList.size();
	}

	public abstract Object get(Object obj);
	
	public JSONObject getJson() {
		return getJson(null);
	}
	
	public JSONObject getJson(UserBean user) {
		JSONObject obj = new JSONObject();
		obj.put("minimum", entityMinimum);
		obj.put("maximum", entityMaximum);
		
		JSONArray array = new JSONArray();
		obj.put("entities", array);
		int k = 0;
		for(PropertyEntity pe: itemList) {
			if(user == null || pe.canView(user)) {
				array.add(pe.getJson());
				k++;
			}
		}
		obj.put("children", k);
		return obj;
	}
	
	public String getJsonString(UserBean user) {
		return getJson(user).toJSONString();
	}
	
	public String getJsonString() {
		return getJson(null).toJSONString();
	}

	public PropertyEntityList filter(UserBean user) {
		return new FilteredPropertyEntityList(this, user);
	}
	
	public static class FilteredPropertyEntityList extends PropertyEntityList {
		
		private final PropertyEntityList pel;
		private final UserBean user;
		
		public FilteredPropertyEntityList(PropertyEntityList pel, UserBean user) {
			this.pel = pel;
			this.user = user;
			
			for(PropertyEntity pe: pel.getItems()) {
				if(pe.canView(user)) {
					this.itemList.add(pe);
					this.itemMap.put(pe.getId(), pe);
				}
			}
		}

		public String getFilterKey() {
			return pel.filterKey;
		}

		public String getFilterValue() {
			return pel.filterValue;
		}

		public String getFilterType() {
			return pel.filterType;
		}
		
		@Override
		public String getJsonString() {
			return pel.getJsonString(user);
		}
		@Override
		public JSONObject getJson() {
			return pel.getJson(user);
		}
		
		@Override
		public void loadData(HttpServletRequest request) {
			throw new UnsupportedOperationException("Not supported on filtered object");
		}

		@Override
		public void loadData(HttpServletRequest request, boolean loadAll) {
			throw new UnsupportedOperationException("Not supported on filtered object");
		}

		@Override
		public Object get(Object obj) {
			return pel.get(obj);
		}
		
		public double getMinimum() {
			return pel.getMinimum();
		}

		public double getMaximum() {
			return pel.getMaximum();
		}
		
		@Override
		public void setActive(boolean active) {
			throw new UnsupportedOperationException("Not supported on filtered object");
		}
		
		@Override
		protected Connection getConnection() {
			return pel.getConnection();
		}
	}
	
	
	public double getMinimum() {
		return (entityMinimum > 0)? entityMinimum : 0;
	}

	public double getMaximum() {
		return (entityMaximum > 0)? entityMaximum : 1;
	}
	
	protected Connection getConnection()
	{
		Connection con = null;
		
		try
		{
			if(ctx == null)
			{
				ctx = new InitialContext();
			}

			DataSource ds = null;
			try
			{
				dbconn = (String) InitServlet.getConfig().getServletContext().getInitParameter("sqlJndiName");
				Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
				ds = (DataSource) envContext.lookup(dbconn);
			}
			catch(Exception e)
			{
			}
			if(ds == null)
			{
				ds = (DataSource) ctx.lookup(dbconn);
			}
			con = ds.getConnection();
		}
		catch(Exception e)
		{
		}
		finally
		{
			try
			{
				ctx.close();
				ctx = null;
			}
			catch(Exception e)
			{
			}
		}
		
		return con;
	}

	/**
	 * @return the activeCount
	 */
	public int getActiveCount() {
		return activeCount;
	}

	/**
	 * @return the inactiveCount
	 */
	public int getInactiveCount() {
		return inactiveCount;
	}

	/**
	 * @return the activePackages
	 */
	public int getActivePackages() {
		return activePackages;
	}

}
