/**
 * 
 */
package com.sok.runway5.properties.entity;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.sok.runway5.properties.dao.PropertyDesignList;
import com.sok.runway5.properties.dao.PropertyLandList;

/**
 * @author dion
 *
 */
public class PropertyRange extends PropertyEntity {

	public final String			EntityType = "Home Range";
	
	private PropertyDesignList	designList = null;
	/**
	 * @param id
	 * @param name
	 * @param active
	 */
	public PropertyRange(String id, String name, String active) {
		super(id,name,active);
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#getType()
	 */
	@Override
	public String getType() {
		return EntityType;
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
	}
	
	@Override
	public void loadChildren(HttpServletRequest request) {
		loadChildren(request, false);
	}

	@Override
	public void loadChildren(HttpServletRequest request, boolean loadALL) {
		designList = new PropertyDesignList("ProductParentHomeRange.ProductVariationHomeRange.ProductID", getId());
		if (!loadALL) designList.setActive(true);
		designList.loadData(request,loadALL);
		
		entityMinimum = (designList.getMinimum() > 0)? designList.getMinimum() : 0;
		entityMaximum = (designList.getMaximum() > entityMinimum)? designList.getMaximum() : (entityMinimum + 1);
	}

	@Override
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", getId());
		obj.put("name", getName());
		obj.put("active", getActive());
		obj.put("minimum", entityMinimum);
		obj.put("maximum", entityMaximum);
		
		if (designList != null) {
			obj.put("children", designList.getItems().size());
			JSONArray array = new JSONArray();
			
			for (int v = 0; v < designList.size(); ++v) {
				array.add(designList.getItems().get(v).getJson());
			}
			
			obj.put("entities", array);
		}
		return obj;
	}
	
}
