/**
 * 
 */
package com.sok.runway5.properties.entity;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.sok.runway5.properties.dao.PropertyApartmentList;

/**
 * @author dion
 *
 */
public class PropertyBuildingStage extends PropertyEntity {

	public final String			EntityType = "Building Stage";
	
	private PropertyApartmentList	landList = null;
	/**
	 * @param id
	 * @param name
	 * @param active
	 */
	public PropertyBuildingStage(String id, String name, String active) {
		super(id,name,active);
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#getType()
	 */
	@Override
	public String getType() {
		return EntityType;
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
	}
	
	@Override
	public void loadChildren(HttpServletRequest request) {
		loadChildren(request, false);
	}

	@Override
	public void loadChildren(HttpServletRequest request, boolean loadALL) {
		landList = new PropertyApartmentList("ProductParentBuildingStageApartment.ProductVariationBuildingStageApartment.ProductID", getId());
		if (!loadALL) landList.setActive(true);
		landList.loadData(request,loadALL);
		
		entityMinimum = (landList.getMinimum() > 0)? landList.getMinimum() : 0; ;
		entityMaximum = (landList.getMaximum() > entityMinimum)? landList.getMaximum() : (entityMinimum + 1);
	}

	@Override
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", getId());
		obj.put("name", getName());
		obj.put("active", getActive());
		obj.put("minimum", entityMinimum);
		obj.put("maximum", entityMaximum);
		
		if (landList != null) {
			obj.put("children", landList.getItems().size());
			JSONArray array = new JSONArray();
			
			for (int v = 0; v < landList.size(); ++v) {
				array.add(landList.getItems().get(v).getJson());
			}
			
			obj.put("entities", array);
		}
		return obj;
	}
	
}
