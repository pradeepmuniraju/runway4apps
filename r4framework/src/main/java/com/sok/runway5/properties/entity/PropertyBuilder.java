/**
 * 
 */
package com.sok.runway5.properties.entity;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.sok.runway5.properties.dao.PropertyRangeList;

/**
 * @author dion
 *
 */
public class PropertyBuilder extends PropertyEntity {

	public final String			EntityType = "Brand";
	
	private PropertyRangeList	rangeList = null;
	/**
	 * @param id
	 * @param name
	 * @param active
	 */
	public PropertyBuilder(String id, String name, String active) {
		super(id,name,active);
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#getType()
	 */
	@Override
	public String getType() {
		return EntityType;
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
	}
	
	@Override
	public void loadChildren(HttpServletRequest request) {
		loadChildren(request, false);
	}

	@Override
	public void loadChildren(HttpServletRequest request, boolean loadALL) {
		rangeList = new PropertyRangeList("ProductParentBrandRange.ProductVariationBrandRange.ProductID", getId());
		if (!loadALL) rangeList.setActive(true);
		rangeList.loadData(request,loadALL);
		
		entityMinimum = (rangeList.getMinimum() > 0)? rangeList.getMinimum() : 0; ;
		entityMaximum = (rangeList.getMaximum() > entityMinimum)? rangeList.getMaximum() : (entityMinimum + 1);
	}

	@Override
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", getId());
		obj.put("name", getName());
		obj.put("active", getActive());
		obj.put("minimum", entityMinimum);
		obj.put("maximum", entityMaximum);
		
		if (rangeList != null) {
			obj.put("children", rangeList.getItems().size());
			JSONArray array = new JSONArray();
			
			for (int v = 0; v < rangeList.size(); ++v) {
				array.add(rangeList.getItems().get(v).getJson());
			}
			
			obj.put("entities", array);
		}
		return obj;
	}
	
}
