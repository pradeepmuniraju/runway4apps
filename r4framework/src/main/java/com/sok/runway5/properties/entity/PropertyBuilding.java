/**
 * 
 */
package com.sok.runway5.properties.entity;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.sok.runway5.properties.dao.PropertyBuildingStageList;

/**
 * @author dion
 *
 */
public class PropertyBuilding extends PropertyEntity {

	private final String		EntityType = "Building";
	
	private PropertyBuildingStageList	stageList = null;

	/**
	 * @param id
	 * @param name
	 * @param active
	 */
	public PropertyBuilding(String id, String name, String active) {
		super(id,name,active);
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#getType()
	 */
	@Override
	public String getType() {
		return EntityType;
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
	}

	@Override
	public void loadChildren(HttpServletRequest request) {
		loadChildren(request, false);
	}

	@Override
	public void loadChildren(HttpServletRequest request, boolean loadALL) {
		stageList = new PropertyBuildingStageList("ProductParentBuildingBuildingStage.ProductVariationBuildingBuildingStage.ProductID", getId());
		if (!loadALL) stageList.setActive(true);
		stageList.loadData(request,loadALL);

		entityMinimum = (stageList.getMinimum() > 0)? stageList.getMinimum() : 0; ;
		entityMaximum = (stageList.getMaximum() > entityMinimum)? stageList.getMaximum() : (entityMinimum + 1);
	}
	
	
	public PropertyBuildingStageList getBuildingStageList() {
		return stageList;
	}
	
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", getId());
		obj.put("name", getName());
		obj.put("active", getActive());
		obj.put("minimum", entityMinimum);
		obj.put("maximum", entityMaximum);
		
		if (stageList != null) {
			obj.put("children", stageList.getItems().size());
			JSONArray array = new JSONArray();
			
			for (int v = 0; v < stageList.size(); ++v) {
				array.add(stageList.getItems().get(v).getJson());
			}
			
			obj.put("entities", array);
		}
		return obj;
	}
	
	
	
}
