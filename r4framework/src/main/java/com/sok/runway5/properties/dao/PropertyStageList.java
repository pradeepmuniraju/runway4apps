/**
 * 
 */
package com.sok.runway5.properties.dao;

import javax.servlet.http.HttpServletRequest;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway5.properties.entity.PropertyStage;

/**
 * @author dion
 *
 */
public class PropertyStageList extends PropertyEntityList {
	
	private String					active = "";
	private boolean					customOnly = false;
	private final static String		productType = "Stage";

	/**
	 * 
	 */
	public PropertyStageList() {
		super(productType);
	}

	/**
	 * @param type
	 */
	private PropertyStageList(String type) {
		super(type);
	}

	/**
	 * @param key
	 * @param value
	 */
	public PropertyStageList(String key, String value) {
		super(key, value, productType);
	}

	/**
	 * @param key
	 * @param value
	 * @param type
	 */
	private PropertyStageList(String key, String value, String type) {
		super(key, value, type);
	}

	/**
	 * Load up all the ids, names and active
	 * 
	 * @param request
	 */
	public void loadData(HttpServletRequest request) {
		loadData(request,false);
	}

	/**
	 * Load up all the ids, names and active, if loadAll is true then all data is also loaded into a Map
	 * 
	 * @param request
	 * @param loadALL 
	 */
	public void loadData(HttpServletRequest request, boolean loadAll) {
		GenRow row = new GenRow();
		row.setViewSpec("ProductShortView");
		if (super.getFilterType() != null && super.getFilterType().length() > 0) row.setColumn("ProductType", super.getFilterType());
		if (super.getFilterKey() != null && super.getFilterValue() != null && super.getFilterKey().length() > 0 && super.getFilterValue().length() > 0) row.setColumn(super.getFilterKey(), super.getFilterValue());
		if (active != null && active.length() > 0) row.setColumn("Active", active);
		// Used to only show stages marked for custom packaging during custom package creation
		if (customOnly) row.setColumn("Stage", "Y+NULL");
		//row.sortBy("Name", 0);
		// Substringing by 7 assuming that "Stage " will be prefixed to all stage names, works fine even if it has not been prefixed as rest of the sorts take care of such conditions
		row.sortBy("ABS(SUBSTRING(Products.Name,7))", 0);
		row.sortBy("ABS(Products.Name)",1);
		row.sortBy("Products.Name",2);
		row.setParameter("-groupby0", "ProductID");
		row.doAction(GenerationKeys.SEARCH);
		
		row.getResults();
		
		while (row.getNext()) {
			PropertyStage pe = new PropertyStage(row.getString("ProductID"), row.getString("Name"), row.getString("Active"));
			if ("Y".equals(row.getString("Active")))
				++this.activeCount;
			else
				++this.inactiveCount;
			if (loadAll) pe.loadChildren(request, loadAll); 
			pe.loadSecurityGroups(request, row.getData("GroupID"), row.getData("SectionCode"));
			itemList.add(pe);
			itemMap.put(pe.getId(), pe);
		}
		
		row.close();
		
		getRange(request);
	}

	protected void getRange(HttpServletRequest request) {
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductLotRPMView");
		row.setParameter("-select1", "MIN(Products.TotalCost) AS MinimumCost");
		row.setParameter("-select2", "MAX(Products.TotalCost) AS MaximumCost");
		row.setParameter("ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ProductID", super.getFilterValue());
		row.setColumn("ProductType", "Land");
		row.doAction(GenerationKeys.SELECTFIRST);
		
		if (row.getDouble("MinimumCost") < entityMinimum || entityMinimum < 0) entityMinimum = row.getDouble("MinimumCost"); 
		if (row.getDouble("MaximumCost") > entityMaximum || entityMaximum < 0) entityMaximum = row.getDouble("MaximumCost"); 
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		if (active.toUpperCase().startsWith("Y")) {
			this.active = "Y";
		} else if (active.toUpperCase().startsWith("N")) {
			this.active = "N";
		} else {
			this.active = "";
		}
	}
	
	public void setActive(boolean active) {
		if (active) {
			this.active = "Y";
		} else {
			this.active = "";
		}
	}
	
	public void setCustomOnly(boolean customOnly) {
		this.customOnly = customOnly;
	}

	@Override
	public Object get(Object obj) {
		obj.toString();
		return null;
	}
	
}
