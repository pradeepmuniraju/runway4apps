/**
 * 
 */
package com.sok.runway5.properties.dao;

import javax.servlet.http.HttpServletRequest;

import com.restfb.util.StringUtils;
import com.sok.framework.GenRow;
import com.sok.framework.InitServlet;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway5.properties.entity.PropertyLand;

/**
 * @author dion
 *
 */
public class PropertyLandList extends PropertyEntityList {
	
	private String					active = "";
	private final static String		productType = "Land";
	
	/**
	 * 
	 */
	public PropertyLandList() {
		super(productType);
	}

	/**
	 * @param type
	 */
	private PropertyLandList(String type) {
		super(type);
	}

	/**
	 * @param key
	 * @param value
	 */
	public PropertyLandList(String key, String value) {
		super(key, value, productType);
	}

	/**
	 * @param key
	 * @param value
	 * @param type
	 */
	private PropertyLandList(String key, String value, String type) {
		super(key, value, type);
	}

	/**
	 * Load up all the ids, names and active
	 * 
	 * @param request
	 */
	public void loadData(HttpServletRequest request) {
		loadData(request,false);
	}

	/**
	 * Load up all the ids, names and active, if loadAll is true then all data is also loaded into a Map
	 * 
	 * @param request
	 * @param loadALL 
	 */
	public void loadData(HttpServletRequest request, boolean loadAll) {
		GenRow row = new GenRow();
		row.setViewSpec("properties/ProductLotView");
		if (super.getFilterType() != null && super.getFilterType().length() > 0) row.setColumn("ProductType", super.getFilterType());
		if (super.getFilterKey() != null && super.getFilterValue() != null && super.getFilterKey().length() > 0 && super.getFilterValue().length() > 0) row.setColumn(super.getFilterKey(), super.getFilterValue());
		if (active != null && active.length() > 0) row.setColumn("Active", active);
		row.sortBy("Name", 0);
		row.setParameter("-groupby0", "ProductID");
		row.doAction(GenerationKeys.SEARCH);
		
		row.getResults();
		
		while (row.getNext()) {
			PropertyLand pe = new PropertyLand(row.getString("ProductID"), row.getString("Name"), row.getString("Active"),
					row.getDouble("Area"), row.getDouble("LotWidth"), row.getDouble("LotDepth"), row.getString("CurrentStatusID"), row.getString("CurrentStatus"));
			if ("Y".equals(row.getString("Active")) && row.getString("CurrentStatusID").equals(InitServlet.getSystemParam("ProductAvailableStatus")))
				++this.activeCount;
			else
				++this.inactiveCount;
			// should not be calling on it's self
			//if (loadAll) pe.loadChildren(request);
			pe.loadSecurityGroups(request, row.getData("GroupID"), row.getData("SectionCode"));
			itemList.add(pe);
			itemMap.put(pe.getId(), pe);
			if (row.getDouble("TotalCost") < entityMinimum || entityMinimum < 0) entityMinimum = row.getDouble("TotalCost"); 
			if (row.getDouble("TotalCost") > entityMaximum || entityMaximum < 0) entityMaximum = row.getDouble("TotalCost"); 
		}
		
		row.close();
	}
	/*
	public int doPackageCount(HttpServletRequest request, String productID) {
		if (StringUtils.isBlank(productID)) return 0;
		
		GenRow row = new GenRow();
		row.setTableSpec("properties/ProductQuickIndex");
		//row.setRequest(request);
		row.setParameter("-select0", "Count(*) AS Count");
		row.setParameter("StageID", productID);
		//row.setParameter("ProductProductProducts#Land.ProductProductsLinkedProductLand.ProductParentStageLot.ProductVariationStageLot.ProductParentEstateStage.ProductVariationEstateStage.ProductID", "!NULL;!EMPTY");
		//row.setParameter("ProductProductProducts#House.ProductProductsLinkedProductHouse.ProductParentHomeDesign.ProductVariationHomeDesign.ProductParentHomeRange.ProductVariationHomeRange.ProductID", "!NULL;!EMPTY");
		row.setParameter("Active", "Y");
		row.setParameter("ProductType", "House and Land");
		row.setParameter("QuickIndexCurrentStatus.ProductStatusList.StatusID", InitServlet.getSystemParam("ProductAvailableStatus"));
		row.doAction("selectfirst");
		
		//System.out.println(row.getStatement());
		
		return row.getInt("Count");
	}
	*/
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		if (active.toUpperCase().startsWith("Y")) {
			this.active = "Y";
		} else if (active.toUpperCase().startsWith("N")) {
			this.active = "N";
		} else {
			this.active = "";
		}
	}
	
	public void setActive(boolean active) {
		if (active) {
			this.active = "Y";
		} else {
			this.active = "";
		}
	}

	@Override
	public Object get(Object obj) {
		obj.toString();
		return null;
	}
}
