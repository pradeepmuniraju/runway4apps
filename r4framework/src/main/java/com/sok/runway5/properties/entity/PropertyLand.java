/**
 * 
 */
package com.sok.runway5.properties.entity;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.sok.runway5.properties.dao.PropertyStageList;

/**
 * @author dion
 *
 */
public class PropertyLand extends PropertyEntity {

	private final String		EntityType = "Land";
	
	private double				area = 0;
	private double				frontage = 0;
	private double				depth = 0;
	private String				statusID = "";
	private String				status = "";
	
	private PropertyStageList	stageList = null;

	/**
	 * @param id
	 * @param name
	 * @param active
	 */
	public PropertyLand(String id, String name, String active) {
		super(id,name,active);
	}

	/**
	 * @param id
	 * @param name
	 * @param active
	 */
	public PropertyLand(String id, String name, String active, double area, double frontage, double depth, String statusID, String status) {
		super(id,name,active);
		this.area = area;
		this.frontage = frontage;
		this.depth = depth;
		this.statusID = statusID;
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#getType()
	 */
	@Override
	public String getType() {
		return EntityType;
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
	}

	@Override
	public void loadChildren(HttpServletRequest request) {
		loadChildren(request, false);
	}

	@Override
	public void loadChildren(HttpServletRequest request, boolean loadALL) {
	}
	
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", getId());
		obj.put("name", getName());
		obj.put("active", getActive());
		obj.put("area", area);
		obj.put("frontage",frontage);
		obj.put("depth", depth);
		obj.put("statusID", statusID);
		obj.put("status", status);
		
		return obj;
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double getFrontage() {
		return frontage;
	}

	public void setFrontage(double frontage) {
		this.frontage = frontage;
	}

	public double getDepth() {
		return depth;
	}

	public void setDepth(double depth) {
		this.depth = depth;
	}

	public String getStatusID() {
		return statusID;
	}

	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
