/**
 * 
 */
package com.sok.runway5.properties.entity;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.sok.runway5.properties.dao.PropertyEstateList;
import com.sok.runway5.properties.dao.PropertyStageList;

/**
 * @author dion
 *
 */
public class PropertyDeveloper extends PropertyEntity {

	private final String		EntityType = "Developer";
	
	private PropertyEstateList	estateList = null;

	/**
	 * @param id
	 * @param name
	 * @param active
	 */
	public PropertyDeveloper(String id, String name, String active) {
		super(id,name,active);
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#getType()
	 */
	@Override
	public String getType() {
		return EntityType;
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
	}

	@Override
	public void loadChildren(HttpServletRequest request) {
		loadChildren(request, false);
	}

	@Override
	public void loadChildren(HttpServletRequest request, boolean loadALL) {
		estateList = new PropertyEstateList("ProductParentDeveloperEstate.ProductVariationDeveloperEstate.ProductID", getId());
		if (!loadALL) estateList.setActive(true);
		estateList.loadData(request,loadALL);

		entityMinimum = (estateList.getMinimum() > 0)? estateList.getMinimum() : 0; ;
		entityMaximum = (estateList.getMaximum() > entityMinimum)? estateList.getMaximum() : (entityMinimum + 1);
	}
	
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", getId());
		obj.put("name", getName());
		obj.put("active", getActive());
		obj.put("minimum", entityMinimum);
		obj.put("maximum", entityMaximum);
		
		if (estateList != null) {
			obj.put("children", estateList.getItems().size());
			JSONArray array = new JSONArray();
			
			for (int v = 0; v < estateList.size(); ++v) {
				array.add(estateList.getItems().get(v).getJson());
			}
			
			obj.put("entities", array);
		}
		return obj;
	}
	
	
	
}
