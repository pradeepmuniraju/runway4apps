package com.sok.runway5.properties.entity;

import javax.servlet.http.HttpServletRequest;

public class PropertyMonth extends PropertyEntity {

	public PropertyMonth(String id, String name, String active) {
		super(id, name, active);
	}

	private final String		EntityType = "Month";
	
	@Override
	public String getType() {
		return EntityType;
	}

	@Override
	public void setType(String type) {
		
	}

	@Override
	public void loadChildren(HttpServletRequest request) {
		loadChildren(request, false);

	}

	@Override
	public void loadChildren(HttpServletRequest request, boolean loadALL) {

	}

}
