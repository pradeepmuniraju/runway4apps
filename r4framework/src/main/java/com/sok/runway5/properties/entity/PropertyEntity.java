package com.sok.runway5.properties.entity;

import java.util.HashMap;
import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.UserBean;
import com.sok.runway.security.SecurityFactory;

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

public abstract class PropertyEntity {

	private static final Logger logger = LoggerFactory.getLogger(PropertyEntity.class);
	private Map<String, String[]> groupIDs = Collections.emptyMap();
	private String		name = "";
	private String		id = "";
	private String		active = "Y";
	
	protected double					entityMinimum = -1;
	protected double					entityMaximum = -1;

	public PropertyEntity(String id, String name, String active) {
		this.name = name;
		this.id  = id;
		this.active = active;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public abstract String getType();
	public abstract void setType(String type);
	public abstract void loadChildren(HttpServletRequest request);
	public abstract void loadChildren(HttpServletRequest request, boolean loadALL);

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", getId());
		obj.put("name", getName());
		obj.put("active", getActive());
		
		return obj;
	}
	
	public String getJsonString() {
		return getJson().toJSONString();
	}
	
	public boolean canView(UserBean user) {
		for(Map.Entry<String, String[]> e: groupIDs.entrySet()) {
			String code = e.getValue()[0];
			String rep = StringUtils.trimToNull(e.getValue()[1]);
			if(SecurityFactory.getSecurityFactory().canView(user.getUser(), "Products", code, rep)) {
				if(logger.isDebugEnabled()) logger.debug("user {} can view product with name {} and code {}", new String[]{user != null ? user.getUserName() : "null" , getName(), code});
				return true;
			}
		}
		if(logger.isDebugEnabled()) logger.debug("user {} cannot view product with name {}", user != null ? user.getUserName() : "null" , getName());
		return false;
	}
	
	public void loadSecurityGroups(HttpServletRequest request) {
		
		GenRow row = new GenRow();
		row.setTableSpec("Products");
		if(request != null) 
			row.setRequest(request);
		row.setParameter("ProductID", getId());
		row.setParameter("-select1","GroupID");
		row.setParameter("-join1","ProductGroup");
		row.setParameter("-select2","ProductGroup.SectionCode");
		row.doAction(GenerationKeys.SELECT);
		
		if(row.isSuccessful()) 
			loadSecurityGroups(request, row.getData("GroupID"), row.getData("SectionCode"));
		else
			logger.warn("Could not find product for loadSecurityGroups with id {} and error {}", getId(), row.getError());
	}
	
	public void loadSecurityGroups(HttpServletRequest request, String groupID, String code) {
		if(logger.isTraceEnabled()) logger.trace("loadSecurityGroups(req, {}, {})", groupID, code);
		GenRow psg = new GenRow();
		if(request != null) psg.setRequest(request);
		psg.setViewSpec("ProductSecurityGroupShortView");
		psg.setParameter("ProductID", getId());
		
		boolean init = false;
		try { 
			if(psg.isSet("ProductID")) {
				psg.getResults(true);
				if(psg.getNext()) {
					init = true;
					groupIDs = new HashMap<String,String[]>(psg.getSize() + 1);
					do {
						groupIDs.put(psg.getString("GroupID"), new String[]{psg.getString("SectionCode"), psg.getString("RepUserID")});
					} while(psg.getNext());	
				}
			}
		} finally {
			psg.close();
		}
		//add the product's groupID if it hasn't already been added with a specific rep.
		if(!init) groupIDs = new HashMap<String,String[]>(1);
		if(!groupIDs.containsKey(groupID)) groupIDs.put(groupID, new String[]{code, null});
	}
}
