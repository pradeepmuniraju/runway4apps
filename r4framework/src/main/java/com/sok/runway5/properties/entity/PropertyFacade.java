package com.sok.runway5.properties.entity;

import javax.servlet.http.HttpServletRequest;

public class PropertyFacade extends PropertyEntity {
	
	private String description = ""; 

	public PropertyFacade(String id, String name, String active) {
		super(id, name, active);
	}

	public PropertyFacade(String id, String name, String description, String active) {
		super(id, name, active);
		
		this.description = description;
	}

	private final String		EntityType = "Facade";
	private boolean				isDefault = false;
	
	@Override
	public String getType() {
		return EntityType;
	}

	@Override
	public void setType(String type) {
		
	}

	@Override
	public void loadChildren(HttpServletRequest request) {
		this.loadChildren(request, false);

	}

	@Override
	public void loadChildren(HttpServletRequest request, boolean loadALL) {
		
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
	
	public String getStyle() {
		if (isDefault)
			return "*";
		else
			return " ";
	}

	public String getSelected() {
		if (isDefault)
			return "selected";
		else
			return "";
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
