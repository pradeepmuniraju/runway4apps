/**
 * 
 */
package com.sok.runway5.properties.entity;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.sok.runway5.properties.dao.PropertyStageList;

/**
 * @author dion
 *
 */
public class PropertyEstate extends PropertyEntity {

	private final String		EntityType = "Estate";
	
	private PropertyStageList	stageList = null;

	/**
	 * @param id
	 * @param name
	 * @param active
	 */
	public PropertyEstate(String id, String name, String active) {
		super(id,name,active);
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#getType()
	 */
	@Override
	public String getType() {
		return EntityType;
	}

	/* (non-Javadoc)
	 * @see com.sok.runway5.properties.dao.PropertyEntity#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
	}

	@Override
	public void loadChildren(HttpServletRequest request) {
		loadChildren(request, false);
	}
	
	public PropertyStageList getStageList() {
		return stageList;
	}
	
	// Used to show stages marked for custom packaging in custom package creation
	public void loadCustomChildren(HttpServletRequest request, boolean loadALL) {
		stageList = new PropertyStageList("ProductParentEstateStage.ProductVariationEstateStage.ProductID", getId());
		if (!loadALL) stageList.setActive(true);
		stageList.setCustomOnly(true);
		stageList.loadData(request,loadALL);
		entityMinimum = (stageList.getMinimum() > 0)? stageList.getMinimum() : 0; ;
		entityMaximum = (stageList.getMaximum() > entityMinimum)? stageList.getMaximum() : (entityMinimum + 1);
	}
	
	@Override
	public void loadChildren(HttpServletRequest request, boolean loadALL) {
		stageList = new PropertyStageList("ProductParentEstateStage.ProductVariationEstateStage.ProductID", getId());
		if (!loadALL) stageList.setActive(true);
		stageList.loadData(request,loadALL);

		entityMinimum = (stageList.getMinimum() > 0)? stageList.getMinimum() : 0; ;
		entityMaximum = (stageList.getMaximum() > entityMinimum)? stageList.getMaximum() : (entityMinimum + 1);
	}
	
	public JSONObject getJson() {
		JSONObject obj = new JSONObject();
		obj.put("id", getId());
		obj.put("name", getName());
		obj.put("active", getActive());
		obj.put("minimum", entityMinimum);
		obj.put("maximum", entityMaximum);
		
		if (stageList != null) {
			obj.put("children", stageList.getItems().size());
			JSONArray array = new JSONArray();
			
			for (int v = 0; v < stageList.size(); ++v) {
				array.add(stageList.getItems().get(v).getJson());
			}
			
			obj.put("entities", array);
		}
		return obj;
	}
	
	
	
}
