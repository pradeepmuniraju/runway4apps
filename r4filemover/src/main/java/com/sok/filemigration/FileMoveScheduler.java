package com.sok.filemigration;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Properties;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by donovan on 10/08/2016.
 */
public class FileMoveScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileMoveScheduler.class);

    private static final String MOVE_CRON_SCHEDULE = "move.cron.schedule";

    private static FileMoveScheduler INSTANCE;

    private static FileMoveMap MAP_INSTANCE;

    private FileMover fileMover;

    private Properties properties;
    private String propertiesPath;


    private FileMoveScheduler(String propertiesPath) {
        try {
            MAP_INSTANCE = FileMoveMap.getInstance(propertiesPath);

            properties = new Properties();
            LOGGER.info(propertiesPath);
            this.propertiesPath = propertiesPath;
            fileMover = new FileMover(propertiesPath);

            properties.load(new FileReader(propertiesPath));

            this.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static FileMoveScheduler getInstance(String propertiesPath) {
        if (INSTANCE==null)
            INSTANCE = new FileMoveScheduler(propertiesPath);
        return INSTANCE;
    }

    public static FileMoveScheduler getInstance() {
        if (INSTANCE==null)
            throw new RuntimeException("Cannot provide instance of FileMoveSchedule as none of the required properties " +
                    "are set. Ensure the AppListener has been loaded.");
        return INSTANCE;
    }


    public String getDocumentURLByFilePath(String urlPath, String fileName, String documentID) {

        return MAP_INSTANCE.getDocumentURLByFilePath(urlPath, fileName, documentID);
    }

    public void deleteResource(String filePath, String fileName, String documentID) {

        MAP_INSTANCE.deleteResource(filePath, fileName, documentID);
    }

    private void init() {
        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.start();

            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put("propertiesPath", propertiesPath);

            JobDetail job = newJob(FileMoveJob.class)
                    .setJobData(jobDataMap)
                    .withIdentity("FileMoveJob", "FileMoveJobGroup")
                    .build();

            Trigger trigger = newTrigger()
                    .forJob(job)
                    .withIdentity("FileMoveTrigger")
                    .withSchedule(cronSchedule(properties.getProperty(MOVE_CRON_SCHEDULE)))
                    .build();

            scheduler.scheduleJob(job, trigger);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public static void cleanup(Connection conn, Statement stmt, ResultSet rs) {
        if (rs!=null) try { rs.close(); } catch (SQLException e) {}
        if (stmt!=null) try { stmt.close(); } catch (SQLException e) {}
        if (conn!=null) try { conn.close(); } catch (SQLException e) {}
    }

    public void clearMovedMap() {
        MAP_INSTANCE.clearMovedMap();
    }

    public void addMovedMap(String key, String value) {
        MAP_INSTANCE.addMovedMap(key, value);
    }

    public void removeMovedMap(String key) {
        MAP_INSTANCE.removeMovedMap(key);
    }

}
