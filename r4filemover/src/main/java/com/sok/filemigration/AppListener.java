package com.sok.filemigration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by donovan on 21/09/2016.
 */
public class AppListener implements ServletContextListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
    	try {
        String propertiesPath = servletContextEvent.getServletContext().getRealPath("WEB-INF/properties/filemove.properties");
        LOGGER.info("Initialising file move scheduling.");
        LOGGER.info("Setting the base url to " + servletContextEvent.getServletContext().getRealPath("/"));
        FileMoveScheduler.getInstance(propertiesPath);
        FileMoveJob.setBaseUrl(servletContextEvent.getServletContext().getRealPath("/"));
        LOGGER.info("AppListener Started");

    	} catch (Exception e) {
        	e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
