package com.sok.filemigration;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sok.framework.InitServlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by donovan on 2/09/2016.
 */
public final class FileFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        String url = ((HttpServletRequest)req).getRequestURL().toString();
        
        System.out.println("FileFilter: " + url);
        if (url.contains("filedownloader.jsp")) {
            chain.doFilter(req, resp);
        } else {

            String [] sArr = null;
            if (url.indexOf("/files/") > 0) {
                url = url.substring(url.indexOf("/files/"));
                sArr = url.split("/");
            } else if (url.indexOf("/documents/") > 0) {
                url = url.substring(url.indexOf("/documents/"));
                sArr = url.split("/");
            }
            String filename = null;
            String documentId = null;
            if (sArr.length>2) {
                //This here is not great, it assumes that the file path will contain 3 or more split values with the last
                // element being the filename and the second last one being the documentId. This might not always be the case though.
                filename = sArr[sArr.length- 1];
                documentId = sArr[sArr.length - 2];
            }

            //first check if the file exists locally before attempting to get the image

            String realPath = ""; //req.getServletContext().getRealPath(url);
            boolean fileExists = new File(realPath).exists();
            if(!fileExists){
                String decodedPath = decodePath(realPath); //The url may be encoded, decode and try.
                String decodedUrl = decodePath(url);
                LOGGER.debug(String.format("The Real Path is changed from %1s to %2s", realPath, decodedPath));
                LOGGER.debug(String.format("The URL is changed from %1s to %2s", realPath, decodedUrl));
                url = decodedUrl;
                fileExists = new File(decodedPath).exists();
            }
            if (!fileExists && StringUtils.isNotBlank(InitServlet.getSystemParam("Pipeline-ClientID"))) {
            	try {
            		LOGGER.debug("FileFilter: " + url + " [" + filename + "] " + documentId);
	                String s3url = FileMoveScheduler.getInstance().getDocumentURLByFilePath(url, filename, documentId);
	                LOGGER.debug("FileFilter: S3 " + s3url);
	                ((HttpServletResponse) resp).sendRedirect(s3url);
				} catch (Exception e) {
					e.printStackTrace();
	                chain.doFilter(req, resp);
				}
            } else {
                chain.doFilter(req, resp);
            }
        }
    }

    @Override
    public void destroy() {

    }

    private static final Map<String, String> ENCODING_MAP = new HashMap<String, String>();

    static {
        ENCODING_MAP.put("%20", " ");
        ENCODING_MAP.put("%28", "(");
        ENCODING_MAP.put("%29", ")");
        ENCODING_MAP.put("%2B", "+");
        ENCODING_MAP.put("%5B", "[");
        ENCODING_MAP.put("%5D", "]");
    }

    private static String decodePath(String path) {
        if (StringUtils.isBlank(path)) {
            return path;
        }
        String decodedPath = path;
        for (Map.Entry<String, String> encoding : ENCODING_MAP.entrySet()) {
            decodedPath = decodedPath.replace(encoding.getKey(), encoding.getValue());
        }
        return decodedPath;
    }
}
