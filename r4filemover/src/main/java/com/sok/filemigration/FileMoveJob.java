package com.sok.filemigration;

import com.sok.framework.ActionBean;
import com.sok.framework.InitServlet;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.actors.threadpool.Arrays;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by donovan on 10/08/2016.
 */
public class FileMoveJob implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileMover.class);

    private static final String MOVE_IGNORED_FILES = "move.ignored.files";
    private static final String MOVE_TESTING_URL = "move.url";
    private static final String MOVE_FILE_LIMIT = "move.file.limit";
    private static final String MOVE_INCLUDED_DIRS = "move.included.dirs";


    /* System Parameter keys required for uploading assets t AWS S3 via API */
    private static final String PIPELINE_CLIENTID = "Pipeline-ClientID";
    private static final String R6_AWSS3BUCKETNAME = "R6-AWSS3BucketName";
    private static final String R6_AWSS3FOLDERKEY = "R6-AWSS3FolderKey";
    private static final String R6_AWSUPLOADAPI = "R6-AWSUploadAPIURL";
    private static final String R6_AWSUPLOADAPIKEY = "R6-AWSUploadAPIKey";


    private static String baseUrl;

    private Properties properties = new Properties();
    private FileMover fileMover;
    private int fileLimit;
    private int fileCount;
    private String pipelineClientID = null;
    private String awsUploadAPIKey = null;
    private String awsUploadAPIURL = null;
    private String awsS3BucketName = null;
    private String awsS3FolderKey = null;



    static void setBaseUrl(String baseUrl) {
        FileMoveJob.baseUrl = baseUrl;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Executing file move job.");
        String propertiesPath = context.getMergedJobDataMap().getString("propertiesPath");
        boolean systemParametersExist = false;

        try {
            properties.load(new FileReader(propertiesPath));
        } catch (IOException e) {
            throw new JobExecutionException("Unable to load properties for " + propertiesPath + " " + e.getMessage(), e);
        }

        //Initialising required values from SystemParameters
        pipelineClientID = InitServlet.getSystemParam(PIPELINE_CLIENTID);
        awsUploadAPIKey = InitServlet.getSystemParam(R6_AWSUPLOADAPIKEY);
        awsUploadAPIURL = InitServlet.getSystemParam(R6_AWSUPLOADAPI);
        awsS3BucketName = InitServlet.getSystemParam(R6_AWSS3BUCKETNAME);
        awsS3FolderKey = InitServlet.getSystemParam(R6_AWSS3FOLDERKEY);

        if (((awsUploadAPIKey != null) && (!awsUploadAPIKey.isEmpty()))
                && ((awsUploadAPIURL != null) && (!awsUploadAPIURL.isEmpty()))
                && ((awsS3BucketName != null) && (!awsS3BucketName.isEmpty()))
                && ((awsS3FolderKey != null) && (!awsS3FolderKey.isEmpty()))) {
            LOGGER.info("AWS File Mover, all mandatory system parameters set.");
            systemParametersExist = true;
        }

        if (systemParametersExist) {
            fileLimit = Integer.parseInt(properties.getProperty(MOVE_FILE_LIMIT, "200"));
            fileMover = new FileMover(propertiesPath);

            String dirList = properties.getProperty(MOVE_INCLUDED_DIRS);
            String [] dirs = dirList.split(",");
            for (String dir : dirs) {
                File fileDir = new File(baseUrl + dir);

                List<File> includedFiles = new ArrayList<>();
                if (fileDir.isDirectory())
                    includedFiles.addAll(getFiles(fileDir));

                processFiles(includedFiles, dir);
                outputResultsToFile();
                outputResultsToTable();
            }
        } else {
            LOGGER.warn("AWS File Mover, one or more system parameters missing. FileMove process aborted.");
        }
    }

    @SuppressWarnings("unchecked")
    private List<File> getFiles(File directory) {
        File [] files = directory.listFiles();
        List<File> includedFiles = new ArrayList<>();
        List<String> ignoreFiles = Arrays.asList(properties.getProperty(MOVE_IGNORED_FILES).split(","));
        if (files!=null) {
            if (directory.isDirectory() && files.length == 0) {
                directory.delete();
            }
            for (File file : files) {
                if (!ignoreFiles.contains(file.getName())) {
                    if (file.isDirectory()) {
                        includedFiles.addAll(getFiles(file));
                    } else {
                        if (++fileCount > fileLimit)
                            break;
                        else
                            includedFiles.add(file);
                    }
                } else {
                    LOGGER.info("Ignoring " + file.getAbsolutePath());
                }
            }
        }
        return includedFiles;
    }

    @SuppressWarnings("unchecked")
    private void processFiles(List<File> files, String dirName) {
        LOGGER.info("Processing files");

        for (File file : files) {
            String[] tmp = file.getAbsolutePath().substring(file.getAbsolutePath().indexOf(dirName) + 6).split("/");
            String productId = null;
            String documentId = null;

            if (tmp.length == 2) {
                productId = tmp[0];
            } else if (tmp.length == 3) {
                productId = tmp[0];
                documentId = tmp[1];
            }

            // changed this to URLHome this is more reliable
            String filePath = InitServlet.getSystemParam("URLHome") + "/" + file.getAbsolutePath()
                    .substring(file.getAbsolutePath().indexOf(dirName));

            if (file.length()>0) {

                if (fileMover.saveResourceToS3(awsUploadAPIKey, awsUploadAPIURL, awsS3BucketName, awsS3FolderKey, file.getName(), filePath, documentId)) {
                    boolean status = false;

                    if ("true".equals(InitServlet.getSystemParam("PipelineFileDelete"))) {
                        status = file.delete();
                    } else {
                        //status = file.renameTo(new File(file.getAbsolutePath().replaceFirst("files/", "movedfiles/")));
                        status = moveUploadedFile(file);
                    }
                    if (!status) {
                        LOGGER.warn("File " + file.getAbsolutePath() + " not deleted.");
                    } else {
                        LOGGER.info("Deleting/Moving " + file.getName());
                    }
                } else {
                    LOGGER.error("File " + file.getAbsolutePath() + " not saved to S3");
                }
            } else {
                LOGGER.warn("Ignoring and deleting empty file " + file.getAbsolutePath());
                file.delete();
            }
        }
    }

    private boolean moveUploadedFile(File file) {
        boolean status = false;
        try {

            File dest = new File(file.getAbsolutePath().replaceFirst("", ""));
            dest.getParentFile().mkdirs();
            status = file.renameTo(dest);

        } catch (Exception e) {
            LOGGER.error("File " + file.getAbsolutePath() + " not deleted.");
        }

        return status;
    }

    private void outputResultsToFile() {
        Set<FileMover.MigrationResource> migrationResources = fileMover.getMigrationResourceList();
        if (migrationResources==null || migrationResources.size()==0)
            return;
        StringBuilder sb = new StringBuilder();
        for (FileMover.MigrationResource mr : migrationResources) {
            sb.append(mr.getOrigin()).append(",").append(mr.getDestination()).append("\n");
        }

        File f = new File("resourceMigration/");
        if (!f.exists()) {
            LOGGER.info("Creating directory " + f.getAbsoluteFile());
            f.mkdir();
        }
        f = new File("resourceMigration/" + pipelineClientID);
        if (!f.exists())
            f.mkdir();

        DateFormat df = new SimpleDateFormat("yyyyMMdd-HH:mm");
        f = new File("resourceMigration/" + pipelineClientID + "/" + df.format(new Date()) + ".csv");

        LOGGER.info("Saving output to " + f.getAbsolutePath());
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            fos.write(sb.toString().getBytes());
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos!=null) try { fos.close(); } catch(IOException e) {}
        }
    }

    private void outputResultsToTable() {
        Set<FileMover.MigrationResource> migrationResources = fileMover.getMigrationResourceList();
        if (migrationResources==null || migrationResources.size()==0)
            return;

        final String sql = "insert into MigratedResources (resourceId, source, dest, move_date) values (?,?,?, now())";
        Connection conn = ActionBean.connect();
        PreparedStatement pStmt = null;


        try {
            pStmt = conn.prepareStatement(sql);
            for (FileMover.MigrationResource mr : migrationResources) {
                String [] sArr = mr.getOrigin().split("/");
                String documentId = null;
                // This as with other places where we split on the forward slash assumes that the string is at a minimum in the format
                // /documentid/filename. So the document id should always be the second last element of the array.
                // Should probably check if the id is a documentid otherwise it should be null.
                if (sArr.length>2) {
                    documentId = sArr[sArr.length-2];
                }
                if (checkDocument(documentId))
                    pStmt.setString(1, documentId);
                else
                    pStmt.setString(1, null);
                pStmt.setString(2, mr.getOrigin());
                pStmt.setString(3, mr.getDestination());
                try {
                    pStmt.executeUpdate();
                } catch (SQLException e) {
                    LOGGER.error("Could not insert entry in MigratedResources ERROR:=" + e.getMessage());
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            FileMoveScheduler.cleanup(conn, pStmt, null);
        }
    }

    /**
     * Checks if the given documentId is in fact a document. This is done because of the way the id is extracted from the file path.
     *
     * @param documentId
     * @return
     */
    private boolean checkDocument(String documentId) {
        final String sql = "select DocumentID from Documents where DocumentID=?";
        Connection conn = ActionBean.connect();
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        boolean isDocument = false;
        try {
            pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, documentId);
            rs = pStmt.executeQuery();
            isDocument = rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            FileMoveScheduler.cleanup(conn, pStmt, rs);
        }
        return isDocument;
    }

}
