package com.sok.filemigration;

import com.sok.framework.InitServlet;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jettison.json.JSONException;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashSet;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

/**
 * Created by donovan on 10/08/2016.
 */
class FileMover {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileMover.class);

    private static final String AWS_API_URL = "aws.api.url";
    private static final String AWS_AUTH_URL = "aws.auth.url";
    private static final String AWS_GET_URL = "aws.get.url";
    private static final String AWS_DELETE_URL = "aws.delete.url";
    private static final String AWS_AUTH_USERNAME = "aws.auth.username";
    private static final String AWS_AUTH_PASSWORD = "aws.auth.password";
    private static final String AWS_ACCESSNAME = "aws.accessname";
    private static final String AWS_ACCESSPASSWORD = "aws.accesspassword";
    private static final String AWS_HEADER_SECURITYTOKEN = "aws.header.securitytoken";
    private static final String AWS_HEADER_USER_CLIENTKEY = "aws.header.User-ClientKey";
    private static final String AWS_HEADER_USER_ACCESSKEY = "aws.header.User-AccessKey";

    private static final String PIPELINE_CLIENTID = "Pipeline-ClientID";

    private Properties properties;
    private Set<MigrationResource> migrationResourceList = new HashSet<>();

    FileMover(String propertiesPath) {
        properties = new Properties();
        try {
            properties.load(new FileReader(propertiesPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public AccessToken authenticate() {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(properties.getProperty(AWS_AUTH_URL));
        post.addHeader("Content-Type", "application/json");

        String auth = properties.getProperty(AWS_AUTH_USERNAME) + ":" + properties.getProperty(AWS_AUTH_PASSWORD);

        String authHeader = "Basic " + new String(Base64.encodeBase64(auth.getBytes()));

        post.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
        post.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

        JSONObject object = new JSONObject();
        object.put("accessname", properties.getProperty(AWS_ACCESSNAME));
        object.put("accesspassword", properties.getProperty(AWS_ACCESSPASSWORD));
        AccessToken token = null;

        BufferedReader br = null;
        try {
            post.setEntity(new StringEntity(object.toJSONString()));
            HttpResponse resp = client.execute(post);
            HttpEntity entity = resp.getEntity();
            br = new BufferedReader(new InputStreamReader(entity.getContent()));
            String s = br.readLine();

            try {
                org.codehaus.jettison.json.JSONObject jsonObject = new org.codehaus.jettison.json.JSONObject(s);

                String clientId = InitServlet.getSystemParam(PIPELINE_CLIENTID);

                if (clientId==null)
                    clientId = jsonObject.get("clientid").toString();

                token = new AccessToken(jsonObject.get("securitytoken").toString(),
                        clientId, jsonObject.get("accessid").toString());
            } catch (JSONException e) {
                LOGGER.error("Failed to parse json string ERROR:=" + e.getMessage());
            }

        } catch (IOException e) {
            LOGGER.error("Failed to authenticate correctly ERROR:=" + e.getMessage(), e);
        } finally {
            if (br!=null) try { br.close(); } catch (IOException e1) { }
        }
        return token;
    }

    /**
     * Returns the s3 url string for the given documentId.
     *
     * @param documentId - the id of the document
     * @param token - the security token
     * @return the s3 url
     */
    public String getResource(String documentId, String documentName, AccessToken token) {
        HttpClient client = HttpClientBuilder.create().build();
        String auth = properties.getProperty(AWS_AUTH_USERNAME) + ":" + properties.getProperty(AWS_AUTH_PASSWORD);
        String authHeader = "Basic " + new String(Base64.encodeBase64(auth.getBytes()));
        URIBuilder uriBuilder = null;
        HttpGet get;
        try {
            uriBuilder = new URIBuilder(properties.getProperty(AWS_GET_URL))
                    .addParameter("runwaydocumentid", documentId)
                    .addParameter("filename", documentName);


            get = new HttpGet(uriBuilder.build());
            get.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
            get.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            get.setHeader(properties.getProperty(AWS_HEADER_SECURITYTOKEN, "securitytoken"), token.getTokenKey());
            get.setHeader(properties.getProperty(AWS_HEADER_USER_CLIENTKEY, "User-ClientKey"), token.getClientKey());
            get.setHeader(properties.getProperty(AWS_HEADER_USER_ACCESSKEY, "User-AccessKey"), token.getAccessKey());

        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to create URL for API GET request " + e.getMessage());
        }
        BufferedReader br = null;
        String s3Url = null;
        try {
            HttpResponse resp = client.execute(get);
            if (resp!=null) {
                if (resp.getEntity()!=null)
                    br = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));
                if (br!=null) {
                    org.codehaus.jettison.json.JSONObject obj = new org.codehaus.jettison.json.JSONObject(br.readLine());
                    s3Url = obj.getString("asseturl");
                }
            } else {
                LOGGER.error("Response from resource retrieval is null.");
            }


        } catch (IOException | JSONException e) {
            LOGGER.error("Failed to read response ERROR:=" + e.getMessage(), e);
        } finally {
            if (br!=null) try { br.close(); } catch (IOException e1) { }
        }
        return s3Url;
    }

    /**
     *
     * Sends a request to aws uploadasset api to upload asset to S3.
     *
     * @param awsUploadAPIKey - api key created for this client
     * @param awsUploadAPIURL - Asset upload API URL.
     * @param awsS3BucketName - bucket name for this runway
     * @param awsS3FolderKey - aws folder key..
     * @param filename - the actual name of the file
     * @param fileUrl - the actual file to be saved
     * @return true if the document was saved successfully, false otherwise
     */
    @SuppressWarnings("unchecked")
    boolean saveResourceToS3(String awsUploadAPIKey, String awsUploadAPIURL,
                             String awsS3BucketName, String awsS3FolderKey,
                             String filename, String fileUrl, String documentId) {
        HttpClient client = HttpClientBuilder.create().build();

        /* Using AWS API with Lambda to upload assets to S3  - Begin */
        String s3FileName = null;
        JSONObject uploadRequestJson = new JSONObject();
        uploadRequestJson.put("r4asseturl", fileUrl);
        uploadRequestJson.put("s3bucketname", awsS3BucketName);
        uploadRequestJson.put("folderkey", awsS3FolderKey);
        if (documentId != null) {
            s3FileName = documentId + "_" + filename;
        } else {
            s3FileName = filename;
        }
        uploadRequestJson.put("filename", s3FileName);

        boolean saved = false;
        BufferedReader br = null;
        String s3AssetURL = null;
        HttpPost docPost = new HttpPost(awsUploadAPIURL);
        docPost.setHeader("Content-Type", "application/json");
        docPost.setHeader("x-api-key", awsUploadAPIKey);
        try {
            docPost.setEntity(new StringEntity(uploadRequestJson.toJSONString()));
            HttpResponse resp = client.execute(docPost);
            br = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));
            try {
                org.codehaus.jettison.json.JSONObject respObject = new org.codehaus.jettison.json.JSONObject(br.readLine());
                saved = resp.getStatusLine().getStatusCode()== HttpStatus.SC_OK;
                if (saved) {
                    s3AssetURL = respObject.getString("s3asseturl");
                    LOGGER.info("Migrated " + fileUrl + " to " + s3AssetURL);

                    int awSize = Integer.parseInt(respObject.getString("s3assetsize"));
                    int r4Size = tryGetFileSize(new URL(fileUrl));
                    if (awSize > 0 && awSize == r4Size) {
                        migrationResourceList.add(new MigrationResource(fileUrl, s3AssetURL));
                        FileMoveScheduler.getInstance().addMovedMap(documentId + fileUrl.substring(InitServlet.getSystemParam("URLHome").length()), s3AssetURL);
                    } else {
                        saved = false;
                        LOGGER.error("File did not match the size, it was " + awSize);
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("Unable to parse response ERROR:=" + e.getMessage());
            }

        } catch (IOException e) {
            LOGGER.error("Error on resource POST ERROR:=" + e.getMessage(), e);
        } finally {
            if (br!=null) try { br.close(); } catch (IOException e) {}
        }

        /* Using AWS API with Lambda to upload assets to S3  - End */

        return saved;
    }

    /**
     *
     * Sends a request to runwayrest api to go and retrieve the given resource from the runway instance and store it in S3.
     *
     * @param token - the security token
     * @param fileUrl - the actual file to be saved
     * @param productId - the productId the file is linked to
     * @param documentId - the documentId of the file
     * @return true if the document was saved successfully, false otherwise
     */
    @SuppressWarnings("unchecked")
    boolean saveResource(AccessToken token, String filename, String fileUrl, String productId, String documentId) {
        HttpClient client = HttpClientBuilder.create().build();

        String auth = properties.getProperty(AWS_AUTH_USERNAME) + ":" + properties.getProperty(AWS_AUTH_PASSWORD);
        String authHeader = "Basic " + new String(Base64.encodeBase64(auth.getBytes()));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("clientid", BigDecimal.valueOf(Double.parseDouble(token.getClientKey())));
        jsonObject.put("runway4Productid", productId);
        jsonObject.put("runwaydocumentid", documentId);
        jsonObject.put("asseturl", fileUrl);
        jsonObject.put("assetname", documentId + "_" + filename);

        HttpPost docPost = new HttpPost(properties.getProperty(AWS_API_URL));
        docPost.setHeader("Content-Type", "application/json");

        docPost.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
        docPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        docPost.setHeader(properties.getProperty(AWS_HEADER_SECURITYTOKEN, "securitytoken"), token.getTokenKey());
        docPost.setHeader(properties.getProperty(AWS_HEADER_USER_CLIENTKEY, "User-ClientKey"), token.getClientKey());
        docPost.setHeader(properties.getProperty(AWS_HEADER_USER_ACCESSKEY, "User-AccessKey"), token.getAccessKey());
        boolean saved = false;
        BufferedReader br = null;
        try {
            docPost.setEntity(new StringEntity(jsonObject.toJSONString()));

            HttpResponse resp = client.execute(docPost);
            br = new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));
            try {
                org.codehaus.jettison.json.JSONObject respObject = new org.codehaus.jettison.json.JSONObject(br.readLine());
                LOGGER.info("Migrated " + fileUrl + " to " + respObject.getString("asseturl"));
                saved = resp.getStatusLine().getStatusCode()== HttpStatus.SC_OK;
                if (saved) {
                    int awSize = tryGetFileSize(new URL(respObject.getString("asseturl")));
                    if (awSize > 0 && awSize == tryGetFileSize(new URL(fileUrl))) {
                        migrationResourceList.add(new MigrationResource(fileUrl, respObject.getString("asseturl")));
                        FileMoveScheduler.getInstance().addMovedMap(documentId + fileUrl.substring(InitServlet.getSystemParam("URLHome").length()), respObject.getString("asseturl"));
                    } else {
                        saved = false;
                        LOGGER.error("File did not match the size, it was " + awSize);
                    }
                }
            } catch (JSONException e) {
                LOGGER.error("Unable to parse response ERROR:=" + e.getMessage());
            }
        } catch (IOException e) {
            LOGGER.error("Error on resource POST ERROR:=" + e.getMessage(), e);
        } finally {
            if (br!=null) try { br.close(); } catch (IOException e) {}
        }
        return saved;
    }

    private int tryGetFileSize(URL url) {
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("HEAD");
            conn.getInputStream();
            return conn.getContentLength();
        } catch (IOException e) {
            return -1;
        } finally {
            conn.disconnect();
        }
    }

    @SuppressWarnings("unchecked")
    public void deactivateResource(String documentId, String filename, AccessToken token) {
        HttpClient client = HttpClientBuilder.create().build();
        String auth = properties.getProperty(AWS_AUTH_USERNAME) + ":" + properties.getProperty(AWS_AUTH_PASSWORD);
        String authHeader = "Basic " + new String(Base64.encodeBase64(auth.getBytes()));

        try {
            HttpPost post = new HttpPost(properties.getProperty(AWS_DELETE_URL));
            post.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
            post.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            post.setHeader("securitytoken", token.getTokenKey());
            post.setHeader("User-ClientKey", token.getClientKey());
            post.setHeader("User-AccessKey", token.getAccessKey());

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("clientid", token.getClientKey());
            jsonObject.put("runwayrecordid", documentId);
            jsonObject.put("filename", filename);

            post.setEntity(new StringEntity(jsonObject.toJSONString()));
            HttpResponse resp  = client.execute(post);
            assert (resp.getStatusLine().getStatusCode()==HttpStatus.SC_OK);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    Set<MigrationResource> getMigrationResourceList() {
        return migrationResourceList;
    }

    class MigrationResource {
        private String origin;
        private String destination;

        private MigrationResource(String origin, String destination) {
            this.origin = origin;
            this.destination = destination;
        }

        public String getOrigin() {
            return origin;
        }

        public String getDestination() {
            return destination;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MigrationResource that = (MigrationResource) o;
            return Objects.equals(origin, that.origin);
        }

        @Override
        public int hashCode() {
            return Objects.hash(origin);
        }
    }

    class AccessToken {
        private String tokenKey;
        private String clientKey;
        private String accessKey;

        public AccessToken(String tokenKey, String clientKey, String accessKey) {
            this.tokenKey = tokenKey;
            this.clientKey = clientKey;
            this.accessKey = accessKey;
        }

        String getTokenKey() {
            return tokenKey;
        }

        public void setTokenKey(String tokenKey) {
            this.tokenKey = tokenKey;
        }

        String getClientKey() {
            return clientKey;
        }

        public void setClientKey(String clientKey) {
            this.clientKey = clientKey;
        }

        public String getAccessKey() {
            return accessKey;
        }

        public void setAccessKey(String accessKey) {
            this.accessKey = accessKey;
        }
    }

}
