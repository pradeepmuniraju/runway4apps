package com.sok.clients.dfc;

import com.sok.Debugger;
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.servlet.HttpSession;
import com.sok.runway.RunwayEntity;
import com.sok.runway.SessionKeys;
import com.sok.runway.autopilot.AbstractActionTypeEmail;
import com.sok.runway.autopilot.AutoPilotSchedule;
import com.sok.runway.security.User;

import javax.servlet.ServletContext;
import java.sql.Connection;
import java.util.ArrayList;

public class MgrNewLeadsAdvisory extends AbstractActionTypeEmail {
	
	GenRow searchbean = null; 
	protected HttpSession session = null; //need to have own session, don't have access to autopilots
	
	Debugger d = null; 
	
	public boolean needsSending() {
		RunwayEntity entity = getEntity(); 
		if (entity instanceof User && entity.isLoaded()) {
			searchbean = generateSearch(entity.getPrimaryKey());
			if(searchbean != null) { 
				searchbean.doAction(GenerationKeys.SELECTFIRST);
				if(searchbean.isSuccessful()) {
					return true; 
				}
			}
		}
		searchbean = null;
		return false; 
	}
	
	public void init(ServletContext context, AutoPilotSchedule schedule, Connection con) {
		this.session = new HttpSession(context);  
		this.d = Debugger.getDebugger(); 		
		super.init(context, schedule, con); 
	}
	
public GenRow generateSearch(String mgrID) {
		
		GenRow mgrGroup = new GenRow(); 
		mgrGroup.setViewSpec("GroupView"); 
		mgrGroup.setConnection(getAutoPilotSchedule().getConnection());
		mgrGroup.setParameter("-join1","GroupUsers.GroupUser");
		mgrGroup.setParameter("GroupUsers.GroupUser.UserID",mgrID); 
		mgrGroup.setParameter("Applicable","N");
		mgrGroup.setParameter("SectionCode","AAA%");
		mgrGroup.setParameter("GroupID^1","!0Q121Y46030J6L5N1X08767O7A9L"); //not sok group
		mgrGroup.setParameter("GroupID^2","!041O163G7Y3X7Y592K378K6G7J52"); //not finance group
		mgrGroup.getResults(); 
		
		ArrayList<String> users = new ArrayList<String>(); 
		
		GenRow mgrUsers = new GenRow(); 
		mgrUsers.setConnection(getAutoPilotSchedule().getConnection());
		mgrUsers.setViewSpec("UserView"); 
		
		while(mgrGroup.getNext()) { 
			mgrUsers.clear(); 
			mgrUsers.setParameter("UserGroups.UserGroup.SectionCode", mgrGroup.getData("SectionCode")); 
			mgrUsers.setParameter("UserGroups.UserGroup.Applicable","Y"); 
			mgrUsers.getResults(); 
			
			while(mgrUsers.getNext()) { 
				users.add(mgrUsers.getString("UserID")); 
			}
		}
		
		if(users.size() > 0) { 
			
			String userlist = StringUtil.replace(users.toString(),", ","+"); 
			userlist = userlist.substring(1,userlist.length()-1);

		    searchbean = new GenRow();
		    searchbean.setConnection(getAutoPilotSchedule().getConnection());
		    searchbean.setViewSpec("ContactListView");
		    //searchbean.setParameter("RepUserID", userlist);
		    searchbean.setParameter("ContactContactGroups.RepUserID",userlist);
		    //searchbean.setParameter("ContactCurrentStatus.StatusID","40848170A4424749256580E11154F302");
		    searchbean.setParameter("CreatedDate",">"+getAutoPilotSchedule().getFormattedField("LastRunDateTime", "dd/MM/yyyy HH:mm:ss")); 
		    
		    searchbean.sortBy("ContactRepUser.FirstName",1);
		    searchbean.sortBy("ContactRepUser.LastName",2);
		    searchbean.sortBy("ContactCurrentStatus.CreatedDate",3);
		    
		    searchbean.sortOrder("ASC",1);
		    searchbean.sortOrder("ASC",2); 
		    searchbean.sortOrder("ASC",3); 
			//rep.setConstraint(searchbean,"ContactGroup"); If the Rep is of the Contact, then by its nature the Group should be rep-viewable. 
			searchbean.doAction(ActionBean.SEARCH);
			
			d.debug(searchbean.toString()+"<br/>"); 
			d.debug(searchbean.getStatement()+"<br/><br/>"); 
		    return searchbean; 
		} 
		searchbean = null; 
		return null; 
	}
	
	public boolean generateEmail(String noteID) {
		
		getAutoPilotSchedule().getAutoPilot().getSession().setAttribute(SessionKeys.CONTACT_SEARCH, searchbean);
        
        setEmailFrom("info@switched-on.com.au");
        //setEmailCC("gwilson@denniscorp.com.au");
        
        String pdfPath = getAutoPilotSchedule().getAutoPilot().generatePDF(getAutoPilotSchedule(), "/specific/autopilot/newcontactlistpdf.jsp?-action=search",getEntity().getPrimaryKey());
        
        try { 
        	write(getAutoPilotSchedule().getAutoPilot().generateEmail(getAutoPilotSchedule(), "/specific/autopilot/newcontactlist.jsp?pdfPath="+StringUtil.urlEncode(pdfPath)));
        } catch (Exception e) {

            session.setAttribute(SessionKeys.CONTACT_SEARCH, null);
        	write("Exception thrown on generation: "+e.toString());
        	return false; 
        }

        getAutoPilotSchedule().getAutoPilot().getSession().setAttribute(SessionKeys.CONTACT_SEARCH, null);
        return true;
		
	}
	
}
