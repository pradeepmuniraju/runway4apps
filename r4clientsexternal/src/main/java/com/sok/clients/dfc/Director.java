package com.sok.clients.dfc;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Director implements Filter {
	
	public static final String START = "http://www.dennisfamily.com.au"; 
	public static final String SN = "www.dennisfamily.com.au"; 
	public static final String QM = "?"; 

	/**
	 * Class to perform 301 redirects on any domain that is encountered
	 * The idea being that any other domain will dilute the search ranking of the main domain.
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest httprequest = (HttpServletRequest)request;
		HttpServletResponse httpresponse = (HttpServletResponse)response;
		String servername = httprequest.getServerName();			
			
		if(servername.indexOf(SN) > -1) { 
			chain.doFilter(request, response);
		} else { 
			StringBuilder sb = new StringBuilder(START); 
			
			if(httprequest.getRequestURI() != null) {
				sb.append(httprequest.getRequestURI()); 
			} 
			if(httprequest.getQueryString() != null) {
				sb.append(QM).append(httprequest.getQueryString()); 
			} 
			/*
				System.out.println(httprequest.getServerName() + httprequest.getRequestURI() + " " + sb.toString()); 
			*/
			//httpresponse.setStatus(301);
			//httpresponse.setHeader( "Location", sb.toString() );
			//httpresponse.setHeader( "Connection", "close" );
			httpresponse.sendRedirect(START);
		} 
	}

	public void init(FilterConfig arg0) throws ServletException {
		// doesn't need to do anything 
	}

	public void destroy() {
		//doesn't need to do anything 
	}
}
