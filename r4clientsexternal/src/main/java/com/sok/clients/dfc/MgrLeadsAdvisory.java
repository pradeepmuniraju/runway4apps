package com.sok.clients.dfc;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;

import java.util.ArrayList;

public class MgrLeadsAdvisory extends RepLeadsAdvisory{

	
	public GenRow generateSearch(String mgrID) {
		
		GenRow mgrGroup = new GenRow(); 
		mgrGroup.setViewSpec("GroupView"); 
		mgrGroup.setConnection(getAutoPilotSchedule().getConnection());
		mgrGroup.setParameter("-join1","GroupUsers.GroupUser");
		mgrGroup.setParameter("GroupUsers.GroupUser.UserID",mgrID); 
		mgrGroup.setParameter("Applicable","N");
		mgrGroup.setParameter("SectionCode","AAA%");
		mgrGroup.setParameter("GroupID^1","!0Q121Y46030J6L5N1X08767O7A9L"); //not sok group
		mgrGroup.setParameter("GroupID^2","!041O163G7Y3X7Y592K378K6G7J52"); //not finance group
		mgrGroup.getResults(); 
		
		ArrayList<String> users = new ArrayList<String>(); 
		
		GenRow mgrUsers = new GenRow(); 
		mgrUsers.setConnection(getAutoPilotSchedule().getConnection());
		mgrUsers.setViewSpec("UserView"); 
		
		while(mgrGroup.getNext()) { 
			mgrUsers.clear(); 
			mgrUsers.setParameter("UserGroups.UserGroup.SectionCode", mgrGroup.getData("SectionCode")); 
			mgrUsers.setParameter("UserGroups.UserGroup.Applicable","Y"); 
			mgrUsers.getResults(); 
			
			while(mgrUsers.getNext()) { 
				users.add(mgrUsers.getString("UserID")); 
			}
		}
		
		if(users.size() > 0) { 
			
			String userlist = StringUtil.replace(users.toString(),", ","+"); 
			userlist = userlist.substring(1,userlist.length()-1);

		    searchbean = new GenRow();
		    searchbean.setConnection(getAutoPilotSchedule().getConnection());
		    searchbean.setViewSpec("ContactListView");
		    //searchbean.setParameter("RepUserID", userlist);
		    searchbean.setParameter("ContactContactGroups.RepUserID",userlist);
		    searchbean.setParameter("ContactContactGroups.ContactGroupCurrentStatus.StatusID","40848170A4424749256580E11154F302G");
		    
		    searchbean.sortBy("ContactRepUser.FirstName",1);
		    searchbean.sortBy("ContactRepUser.LastName",2);
		    searchbean.sortBy("ContactCurrentStatus.CreatedDate",3);
		    
		    searchbean.sortOrder("ASC",1);
		    searchbean.sortOrder("ASC",2); 
		    searchbean.sortOrder("ASC",3); 
			//rep.setConstraint(searchbean,"ContactGroup"); If the Rep is of the Contact, then by its nature the Group should be rep-viewable. 
			searchbean.doAction(ActionBean.SEARCH);
			
			d.debug(searchbean.toString()+"<br/>"); 
			d.debug(searchbean.getStatement()+"<br/><br/>"); 
		    return searchbean; 
		} 
		searchbean = null; 
		return null; 
	}
	
	public boolean generateEmail(String noteID) {
		
		setEmailFrom("info@switched-on.com.au");
		//setEmailCC("gwilson@denniscorp.com.au");
		return super.generateEmail(noteID); 
	}
	
}
