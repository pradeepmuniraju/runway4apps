package com.sok.clients.dfc;

import com.sok.Debugger;
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.servlet.HttpSession;
import com.sok.runway.RunwayEntity;
import com.sok.runway.SessionKeys;
import com.sok.runway.autopilot.AbstractActionTypeEmail;
import com.sok.runway.autopilot.AutoPilotSchedule;
import com.sok.runway.security.User;

import javax.servlet.ServletContext;
import java.sql.Connection;

public class RepLeadsAdvisory extends AbstractActionTypeEmail {
	
	GenRow searchbean = null; 
	protected HttpSession session = null; //need to have own session, don't have access to autopilots
	
	Debugger d = null; 
	
	public boolean needsSending() {
		RunwayEntity entity = getEntity(); 
		if (entity instanceof User && entity.isLoaded()) {
			searchbean = generateSearch(entity.getPrimaryKey());
			if(searchbean != null) { 
				searchbean.doAction(GenerationKeys.SELECTFIRST);
				if(searchbean.isSuccessful()) {
					return true; 
				}
			}
		}
		searchbean = null;
		return false; 
	}
	
	public void init(ServletContext context, AutoPilotSchedule schedule, Connection con) {
		this.session = new HttpSession(context);  
		this.d = Debugger.getDebugger(); 		
		super.init(context, schedule, con); 
	}
	
	public GenRow generateSearch(String userID) { 
		
		if(userID != null && userID.length()>0) { 

		    searchbean = new GenRow();
		    searchbean.setConnection(getAutoPilotSchedule().getConnection()); 
		    searchbean.setViewSpec("ContactListView");
		    //searchbean.setParameter("RepUserID", userID);
		    searchbean.setParameter("ContactContactGroups.RepUserID",userID);
		    searchbean.setParameter("ContactContactGroups.ContactGroupCurrentStatus.StatusID","40848170A4424749256580E11154F302G"); 
			//rep.setConstraint(searchbean,"ContactGroup"); If the Rep is of the Contact, then by its nature the Group should be rep-viewable.
		    
		    searchbean.sortBy("ContactRepUser.FirstName",1);
		    searchbean.sortBy("ContactRepUser.LastName",2);
		    searchbean.sortBy("ContactCurrentStatus.CreatedDate",3);
		    
		    searchbean.sortOrder("ASC",1);
		    searchbean.sortOrder("ASC",2); 
		    searchbean.sortOrder("ASC",3); 
		    
			searchbean.doAction(ActionBean.SEARCH);
			
			d.debug(searchbean.toString()+"<br/>"); 
			d.debug(searchbean.getStatement()+"<br/><br/>"); 
		    return searchbean; 
		} 
		searchbean = null; 
		return null; 
	}
	
	public boolean generateEmail(String noteID) {
		
		getAutoPilotSchedule().getAutoPilot().getSession().setAttribute(SessionKeys.CONTACT_SEARCH, searchbean);
        
        setEmailFrom("info@switched-on.com.au");
        
        String pdfPath = getAutoPilotSchedule().getAutoPilot().generatePDF(getAutoPilotSchedule(), "/specific/autopilot/contactlistpdf.jsp?-action=search",getEntity().getPrimaryKey());
        
        try { 
        	write(getAutoPilotSchedule().getAutoPilot().generateEmail(getAutoPilotSchedule(), "/specific/autopilot/contactlist.jsp?pdfPath="+StringUtil.urlEncode(pdfPath)));
        } catch (Exception e) {

            session.setAttribute(SessionKeys.CONTACT_SEARCH, null);
        	write("Exception thrown on generation: "+e.toString());
        	return false; 
        }

        getAutoPilotSchedule().getAutoPilot().getSession().setAttribute(SessionKeys.CONTACT_SEARCH, null);
        return true;
		
	}
	
}
