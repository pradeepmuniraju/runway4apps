package com.sok.clients.xero;

public interface XeroInterface {
	public final String XERO_URL_CONTACT = "https://networktest.xero.com/api.xro/1.0/contact";
	public final String XERO_URL_INVOICE = "https://networktest.xero.com/api.xro/1.0/invoice";

}
