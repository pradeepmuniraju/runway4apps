package com.sok.clients.xero;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.servlet.ServletContext;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.sok.clients.realex.RealExConnection;
import com.sok.framework.ActionBean;
import com.sok.framework.servlet.ResponseOutputStream;

/**
 * A class to establish connection with Xero and make API calls.
 * @author irwan
 * @since 27-02-2009
 * @version 1.0
 */
public class Xero implements XeroInterface {
	private String xero_apiKey;
	private String xero_customerKey;
	private Connection con = null;
	private com.sok.framework.servlet.HttpSession session = null;
	private ServletContext context = null;
	private static final String LINE_BREAK = "\r\n"; 
	private static	URL remoteURL; 
	private static	URLConnection connection;
	private static HttpURLConnection huc = null;
	private static	PrintWriter printwriter; 
	private static	BufferedReader bufferedreader;
	
	/**
	 * 
	 * @param session
	 * @param apiKey
	 * @param customerKey
	 */
	public Xero(ServletContext context, String apiKey, String customerKey)
	{
		this.context = context;
		this.session = new com.sok.framework.servlet.HttpSession(context);
		this.xero_apiKey = apiKey;
		this.xero_customerKey = customerKey;
	}
	
	/**
	 * 
	 * @return
	 */
	public String test()
	{
		return xero_apiKey + "/" + xero_customerKey;
	}
	
	/**
	 * 
	 * @param xmlGeneratorURL
	 * @return
	 */
	public Map<String,String> postContact(String xmlGeneratorURL)
	{
		String xml = generateXML(xmlGeneratorURL);
		String responseStr = getResponse(xml, constructURL(XERO_URL_CONTACT));
		Map<String,String> responseMap = new HashMap<String,String>();
		try {
			responseMap = parseResponse(responseStr);
			responseMap.put("-fullRequestXML", xml); 
			responseMap.put("-fullResponseStr", responseStr);
			return responseMap;  
		} catch (Exception e) {
			responseMap.put("-error", e.getMessage()); 
		} 
		return responseMap; 
	}
	
	/**
	 * 
	 * @param xmlGeneratorURL
	 * @return
	 */
	public Map<String,String> postInvoice(String xmlGeneratorURL)
	{
		com.sok.Debugger.getDebugger().debug("<p>generator url: " + xmlGeneratorURL);
		String xml = generateXML(xmlGeneratorURL); com.sok.Debugger.getDebugger().debug("XML: " + xml);
		String responseStr = getResponse(xml, constructURL(XERO_URL_INVOICE), "PUT");
		Map<String,String> responseMap = new HashMap<String,String>();
		try {
			responseMap = parseResponse(responseStr);
			responseMap.put("-requestXML", xml); 
			responseMap.put("-responseStr", responseStr);
			return responseMap;  
		} catch (Exception e) {
			responseMap.put("-error", e.getMessage()); 
		} 
		return responseMap; 
	}
	
	/**
	 * 
	 * @param xmlGeneratorURL
	 * @return
	 */
	public Map<String,String> postInvoiceXML(String xml)
	{
		String responseStr = getResponse(xml, constructURL(XERO_URL_INVOICE), "PUT"); com.sok.Debugger.getDebugger().debug("XML: " + xml);
		Map<String,String> responseMap = new HashMap<String,String>();
		try {
			responseMap = parseResponse(responseStr);
			responseMap.put("-requestXML", xml); 
			responseMap.put("-responseStr", responseStr);
			return responseMap;  
		} catch (Exception e) {
			responseMap.put("-error", e.getMessage()); 
		} 
		return responseMap; 
	}

	/**
	 * 
	 * @param requesturl
	 * @param apiKey
	 * @param customerKey
	 * @return
	 */
	private String constructURL(String requesturl) {
		return requesturl + "?apiKey=" + xero_apiKey + "&xeroKey=" + xero_customerKey;
	}
	
	private Connection getConnection() {
		Context ctx = null;
		try {
			if (con == null || con.isClosed()) {
		      ctx = new InitialContext();
		
		      DataSource ds = null;
		      try {
		      	Context envContext = (Context) ctx.lookup(ActionBean.subcontextname);
		         ds = (DataSource) envContext.lookup(ActionBean.getJndiName());
		      } catch (Exception e) {
		      }
		      
		      if (ds == null) {
		      	ds = (DataSource) ctx.lookup(ActionBean.getJndiName());
		      }
		      con = ds.getConnection();
			}
	      } catch (Exception ex) {
	      } finally {
	         try {
	            ctx.close();
	            ctx = null;
	         } catch (Exception e) {
	         }
	      }
	      return con;
	   }
	
	/**
	 * 
	 * @param urlPath
	 * @return
	 */
	public String generateXML(String urlPath) {

		ResponseOutputStream 	responseOS 	= null;
		ByteArrayOutputStream 	baos 			= new ByteArrayOutputStream();

		try 
		{
			responseOS = new ResponseOutputStream(session, baos, urlPath);
			responseOS.service(getConnection()); com.sok.Debugger.getDebugger().debug("<p>connection: " + getConnection());
		} 
		catch (Exception e) 
		{
			com.sok.Debugger.getDebugger().debug("<p>EXCEPTION.1: " + e);
		}
		finally 
		{
			if(baos != null) 
			{
				com.sok.Debugger.getDebugger().debug("<p>baos not null");
				
				try 
				{
					baos.flush();
					baos.close();
				} 
				catch (Exception ex) 
				{
					com.sok.Debugger.getDebugger().debug("<p>EXCEPTION.2: " + ex);
				}
			}
			
			if (responseOS != null) 
			{
				com.sok.Debugger.getDebugger().debug("<p>responseOS not null");
				responseOS.flush();
				responseOS.close();
			}
		}
		
		com.sok.Debugger.getDebugger().debug("<p>baos: " + baos.toString());

    	return baos.toString();
	}
	
	/**
	 * 
	 * @param xml
	 * @param url
	 * @return
	 */
	private static String getResponse(String xml, String url) {
		return getResponse(xml, url, "POST");
	}
	
	/**
	 * 
	 * @param xml
	 * @param url
	 * @return
	 */
	private static String getResponse(String xml, String url, String method) { 
		// establish connection
		
		try { 
			remoteURL = new URL(url);
			connection = remoteURL.openConnection();
		} 
		catch (MalformedURLException mue) {
			return "Error in hostname: " + url + ": "+mue.getMessage();      
		} 
		catch (InterruptedIOException iioe) { // thread died while talking - do a retry
			return ("Died while connecting: " + iioe.getMessage());   
		} 
		catch (IOException ioe) { // other exception - probably config error - stop
			return ("Cannot connect to " + url + ": "+ioe.getMessage());
		}        
		
		// send xml
		if(xml != null && xml.length() > 0) {
			try {
				huc = (HttpURLConnection) connection;
				huc.setDoOutput(true);
				huc.setRequestMethod(method);
	         printwriter = new PrintWriter(huc.getOutputStream());
	         printwriter.println(xml);
	         printwriter.close();
			} 
			catch (IOException ioe) { // can anything here be recoverable? - stop
				return ("Error writing xml to payandshop: "+ioe.getMessage());
			}
		}
        
		StringBuilder response = new StringBuilder(); 
		
		// read response
		try {
			bufferedreader = new BufferedReader(new InputStreamReader(huc.getInputStream()));
         String inputLine = null;
         while ((inputLine = bufferedreader.readLine()) != null) {
         	response.append(inputLine).append(LINE_BREAK); 
			}
         bufferedreader.close();
		} 
		catch (IOException ioe) {
			return ("Error reading response xml: "+ ioe.getMessage());
		}

		return response.toString(); 
	}
	
	/**
	 * 
	 * @param xml
	 * @return
	 * @throws DocumentException
	 */
	protected static Map<String, String> parseResponse(String xml) throws DocumentException { 
		
		Document document = DocumentHelper.parseText(xml);
		Map<String, String> xmlResponses = new HashMap<String,String>(); 
		
		Element root = document.getRootElement(); 
		
		for ( int i = 0, size2 = root.attributeCount(); i < size2; i++ ) {
			Attribute childAttribute = root.attribute(i);
			xmlResponses.put(root.getName()+"_"+childAttribute.getName(), childAttribute.getText());					
		}		
		
		for ( int i = 0, size = root.nodeCount(); i < size; i++ ) {
			Node node = root.node(i); 
			
			if(node instanceof Element) { 
				Element thisNode = (Element)node; 
				xmlResponses.put(node.getName(), node.getText());

				for ( int i2 = 0, size2 = thisNode.nodeCount(); i2 < size2; i2++ ) {
					Node childNode = thisNode.node(i2); 
					if(childNode instanceof Element) { 
						Element thisChildNode = (Element)childNode; 
						xmlResponses.put(node.getName()+"_"+thisChildNode.getName(), thisChildNode.getText());
					} 
				} 
			}
		}
		
		xmlResponses.put("-fullResponse",xml);
		
		return xmlResponses;
	}
	
	/**
	 * 
	 * @param document
	 * @return
	 */
	protected static String getXmlFromDocument(Document document) {
    	String ret = "FAILED";
    	try {
		    OutputFormat format = OutputFormat.createPrettyPrint();
		    StringWriter sw = new StringWriter();
		    XMLWriter writer = new XMLWriter( sw, format );
		    writer.write( document );
		    sw.flush();
		    ret = sw.toString(); 
		    sw.close();
    	} 
    	catch (IOException e) { }
    	
    	return ret; 
	}
	
	/**
	 * 
	 * @param args
	 */
	/*
	public static void main(String[] args) {
		
		System.out.println("BEGIN XERO");
		
		// GET CONTACT
		String getcontacturl = constructURL(XERO_URL_CONTACT, XERO_APIKEY_ESAY, XERO_CUSTOMERKEY_ESAY_DEMO); System.out.println(getcontacturl);
		String resp = "";
		
		resp = getResponse(null, getcontacturl + "&contactID=2E68080D-B3BD-4051-B33C-9F031550B958"); System.out.println(resp);
		
		Map <String, String> responseMap = null;
		try
		{
			responseMap = parseResponse(resp);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		System.out.println(responseMap);
		
		// POST CONTACT
		
		Document document = DocumentHelper.createDocument();
		Element root = null;
		Element sub = null;
		
		root = document.addElement("Contact");
		
		root.addElement("ContactNumber").addText("123");
		root.addElement("ContactStatus").addText("ACTIVE");
		root.addElement("Name").addText("Irwan");
		root.addElement("EmailAddress").addText("irwan@switched-on.com.au");
		
		Element addresses = root.addElement("Addresses");
		Element address = addresses.addElement("Address");
		
		address.addElement("AddressType").addText("STREET");
		address.addElement("AddressLine1").addText("187 Ferrars Street");
		address.addElement("City").addText("SouthMelbourne");
		address.addElement("Region").addText("VIC");
		address.addElement("PostalCode").addText("3502");
		address.addElement("Country").addText("Australia");
		
		String xmldoc = getXmlFromDocument(document); System.out.println(xmldoc);
		
		resp = getResponse(xmldoc, getcontacturl); System.out.println(resp);
		
		try
		{
			responseMap = parseResponse(resp);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		System.out.println(responseMap);
		
		// GET INVOICE
		
		String getinvoiceturl = constructURL(XERO_URL_INVOICE, XERO_APIKEY_ESAY, XERO_CUSTOMERKEY); System.out.println(getinvoiceturl);
		
		resp = getResponse(null, getinvoiceturl + "&invoiceID=36d9a8ee-227f-488b-86dd-27d5aa441674"); System.out.println(resp);
		
		try
		{
			responseMap = parseResponse(resp);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		System.out.println(responseMap);
	}
	*/
}
