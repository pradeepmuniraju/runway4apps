package com.sok.clients.esay;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.runway.email.Emailer;
import com.sok.runway.email.EmailerFactory;
import com.sok.runway.RunwayEntity;
import com.sok.runway.UserBean;
import com.sok.runway.SessionKeys;
import com.sok.runway.autopilot.ActionTypeUpdate;
import com.sok.runway.autopilot.AutoPilotQueue;
import com.sok.runway.crm.Order;
import com.sok.runway.crm.Company;

/**
 * Send reminder email to suppliers about items of particular statuses after a period of time elapses
 * @note Even though this uses ActionTypeUpdate, it is not technically an update. Search parameters of OrderItems 
 * and TemplateID ("-templateID") to be sent must be specified in the UpdateString field of the AutoPilotActionType entry.  
 * @author irwan
 */
public class CustomerReminder implements ActionTypeUpdate 
{
	public boolean validRecord(RunwayEntity entity) 
	{
		return (entity != null && entity.isLoaded() && entity instanceof Order); 
	}
	
	/**
	 * @param updString Expected to contain TemplateID. This parameter should be set via Database
	 * access to AutoPilotActionTypes table. Example:
	 * update AutoPilotActionTypes 
	 * set UpdateString = 'TemplateID=0I1W2X17714L8051218M4M9F1A52' 
	 * where AutoPilotActionTypeID = 'OrderReturnDispatched';
	 */
	public void updateRecord(RunwayEntity entity, String updString, AutoPilotQueue apq, boolean debug) 
	{	
		com.sok.Debugger.getDebugger().debug("<br />updateRecord");
		
		if(validRecord(entity)) 
		{
			com.sok.Debugger.getDebugger().debug("<br />valid: " + entity.getPrimaryKey());
			Order order = (Order)entity;
			
			UserBean userBean = new UserBean();

			userBean.setViewSpec("UserView");
			userBean.setConnection(entity.getConnection());
			userBean.setParameter("UserID", order.getCurrentUser().getUserID());
			userBean.doAction(ActionBean.SELECT); 

			String	emailstatus 	 = ""; 
					
			GenRow emailBean = new GenRow();
			emailBean.parseRequest(updString); com.sok.Debugger.getDebugger().debug("<br />templateid: " + emailBean.getParameter("TemplateID"));
			emailBean.setParameter("OrderID", order.getPrimaryKey()); com.sok.Debugger.getDebugger().debug("<br />orderid in emailbean: " + order.getPrimaryKey());
			emailBean.setParameter("ContactID", order.getField("ContactID"));
			emailBean.putInternalToken(SessionKeys.CURRENT_USER, userBean);

			Emailer emailer = EmailerFactory.getContactEmailer(emailBean, apq.getServletContext());
			emailstatus = emailer.generateSendAndSave(); com.sok.Debugger.getDebugger().debug("<br />emailstatus: " + emailstatus);
		}
		else if(debug) 
		{ 
			apq.appendInfo("Skipped, not valid"); 
		} 
	}
}
