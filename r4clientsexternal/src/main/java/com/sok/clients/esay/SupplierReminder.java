package com.sok.clients.esay;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.runway.email.Emailer;
import com.sok.runway.email.EmailerFactory;
import com.sok.runway.RunwayEntity;
import com.sok.runway.UserBean;
import com.sok.runway.SessionKeys;
import com.sok.runway.autopilot.ActionTypeUpdate;
import com.sok.runway.autopilot.AutoPilotQueue;
import com.sok.runway.crm.Order;
import com.sok.runway.crm.Company;

/**
 * Send reminder email to suppliers about items of particular statuses after a period of time elapses
 * @note Even though this uses ActionTypeUpdate, it is not technically an update. Search parameters of OrderItems 
 * and TemplateID ("-templateID") to be sent must be specified in the UpdateString field of the AutoPilotActionType entry.  
 * @author irwan
 */
public class SupplierReminder implements ActionTypeUpdate 
{
	public boolean validRecord(RunwayEntity entity) 
	{
		return (entity != null && entity.isLoaded() && entity instanceof Order); 
	}
	
	/**
	 * 
	 * 
	 * @param updString Expected to contain item search parameters (to filter the relevant items so 
	 * that suppliers can be found and sent email), and email TemplateID. This parameter should be set via Database
	 * access to AutoPilotActionTypes table. Example:
	 * update AutoPilotActionTypes 
	 * set UpdateString = 'OrderItemStatus=Arrived&OrderItemStatusDate=LAST5DAYSEXCEPT4DAYS&TemplateID=0I1W2X17714L8051218M4M9F1A52' 
	 * where AutoPilotActionTypeID = 'OrderReturnArrived';
	 */
	public void updateRecord(RunwayEntity entity, String updString, AutoPilotQueue apq, boolean debug) 
	{
		if(validRecord(entity)) 
		{
			Order order = (Order)entity;
			
			UserBean userBean = new UserBean();

			userBean.setViewSpec("UserView");
			userBean.setConnection(entity.getConnection());
			userBean.setParameter("UserID", order.getCurrentUser().getUserID());
			userBean.doAction(ActionBean.SELECT);
			
			GenRow orderitems = new GenRow();
			
			orderitems.setConnection(entity.getConnection());
			orderitems.setViewSpec("[specific/]OrderItemsView");
			orderitems.parseRequest(updString);
			orderitems.setParameter("OrderID", entity.getPrimaryKey());
			orderitems.setParameter("-groupby0", "SupplierCompanyID");
			orderitems.doAction(ActionBean.SEARCH);
			orderitems.getResults(); 
			
			Company 	supplierCompany = new Company(order.getConnection());
			String	emailstatus 	 = ""; 
			GenRow 	emailBean		 = new GenRow();
			Emailer 	emailer			 = null;
			
			while(orderitems.getNext())
			{
				// debug
				com.sok.Debugger.getDebugger().debug("<br />AutoPilot.OrderItemID: " + orderitems.getData("OrderItemID"));

				String orderItemStatus = orderitems.getParameter("OrderItemStatus");
				
				if("Processing".equals(orderItemStatus))
				{					
					supplierCompany.load(orderitems.getData("SupplierCompanyID"));

					emailBean = new GenRow();
					emailBean.setParameter("OrderID", order.getPrimaryKey());
					emailBean.setParameter("OrderItemID", orderitems.getData("OrderItemID"));
					emailBean.setParameter("TemplateID", orderitems.getParameter("TemplateID"));
					emailBean.setParameter("CompanyID", supplierCompany.getPrimaryKey());
					emailBean.setParameter("ContactID", order.getField("ContactID"));
					emailBean.putInternalToken(SessionKeys.CURRENT_USER, userBean);

					emailer = EmailerFactory.getCompanyEmailer(emailBean, apq.getServletContext());
					emailstatus = emailer.generateSendAndSave();
				}
				else if("Picked".equals(orderItemStatus) || "Arrived".equals(orderItemStatus))
				{
					GenRow warehousestaffs = new GenRow();
					warehousestaffs.setConnection(entity.getConnection());
					warehousestaffs.setTableSpec("Contacts");
					warehousestaffs.setParameter("-select#1", "ContactID");
					warehousestaffs.setParameter("-select#2", "FirstName");
					warehousestaffs.setParameter("-join1", "ContactCompanyContactLinkStock");
					warehousestaffs.setParameter("-join2", "ContactSeeAllWHAnswer");
					warehousestaffs.setParameter("ContactCompanyContactLinkStock.CompanyID|1", orderitems.getData("WarehouseCompanyID")); // Staff who can see THIS Warehouse, OR ...
					
					if("Picked".equals(orderItemStatus))
					{
						warehousestaffs.setParameter("ContactSeeAllWHAnswer.Answer|1", "%Stock%"); // ... staff who can see ALL Stock Warehouses, AND ...
						warehousestaffs.setParameter("ContactSupplierAccessibility.Answer", "%Logistics%"); // ... staff who has access to Logistics area
					}
					else if("Arrived".equals(orderItemStatus))
					{
						warehousestaffs.setParameter("ContactSeeAllWHAnswer.Answer|1", "%Return%"); // ... staff who can see ALL Return Warehouses, AND ...
						warehousestaffs.setParameter("ContactSupplierAccessibility.Answer", "%Returns%"); // ... staff who has access to Returns area
					}
										
					warehousestaffs.doAction(ActionBean.SEARCH);
					warehousestaffs.getResults();
					
					// use OrderItemID to filter which OrderItems to be displayed in the email
					while(warehousestaffs.getNext())
					{
						emailBean = new GenRow();
						emailBean.setParameter("OrderID", order.getPrimaryKey());
						emailBean.setParameter("OrderItemID", orderitems.getData("OrderItemID"));
						emailBean.setParameter("TemplateID", orderitems.getParameter("TemplateID"));
						emailBean.setParameter("CompanyID", orderitems.getData("WarehouseCompanyID"));
						emailBean.setParameter("ContactID", warehousestaffs.getData("ContactID"));
						emailBean.putInternalToken(SessionKeys.CURRENT_USER, userBean);

						emailer = EmailerFactory.getContactEmailer(emailBean, apq.getServletContext());
						emailstatus = emailer.generateSendAndSave();
					}
				}
			}
		}
		else if(debug) 
		{ 
			apq.appendInfo("Skipped, not valid"); 
		} 
	}
}
