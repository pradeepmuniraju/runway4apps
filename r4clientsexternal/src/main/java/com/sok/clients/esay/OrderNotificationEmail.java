/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sok.clients.esay;

import java.util.HashMap;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.runway.autopilot.AbstractActionTypeEmail;
import com.sok.runway.crm.Order;

/**
 * @author(s) emil, irwan
 */
public class OrderNotificationEmail extends AbstractActionTypeEmail 
{
	private HashMap<String,String> map = null;
	
	public OrderNotificationEmail() 
	{		
		map = new HashMap<String,String>();
		map.put("Returning", "0Z1923167T47749V813L051V2A06");
		map.put("Arrived", "0I1W2X17714L8051218M4M9F1A52");
		map.put("Return Dispatched", "0O1C2Y1F7M58504Q4T1G0S3J8N3U");
		map.put("Processing", "0J1A27117A5K53414I7J74528V9H");
		map.put("Picked", "08142Z1M735F5Z4G5W248E0F5A30");		
	}

	public void setSpecificEmailTokens(String orderID, GenRow requestRow) 
	{	   
		Order order = new Order(con);
		order.load(orderID);
		
		GenRow productSearch = new GenRow();
		productSearch.setConnection(con);
		productSearch.setViewSpec("ProductView");
		productSearch.setParameter("ProductID",order.getField("ProductID"));
		productSearch.doAction(ActionBean.SELECT);
		productSearch.getResults();		   
	   
		String productDetails = productSearch.getData("ProductDetails");
	   String productName = productSearch.getData("Name");
	   	
	   	
	   GenRow orderItemSearch = new GenRow();
	   orderItemSearch.setConnection(con);
	   orderItemSearch.setViewSpec("OrderItemsView");
	   orderItemSearch.setParameter("OrderID",orderID);
	   orderItemSearch.doAction(ActionBean.SEARCH);
	   orderItemSearch.getResults();
	   	
	   String statusOrderDate = null;
	   	
	   while(orderItemSearch.getNext())
	   {		   		
	   	String templateID = requestRow.getString("TemplateID");
	   	String orderItemStatus = orderItemSearch.getData("OrderItemStatus");
	   	if(map.get(orderItemStatus).equals(templateID)/**/) 
	   	{
	   		statusOrderDate = orderItemSearch.getData("OrderItemStatusDate");	
	   		com.sok.Debugger.getDebugger().debug("<br>Jubiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii"); 
	   	}
	   }
	   	
	   productSearch.setParameter("OrderID",order.getField("ProductID"));
	   	
   	/* Parse in tokens for use in email templates */  
   	requestRow.setParameter("order", order.getRecord());
   	requestRow.setParameter("returnNumber",order.getField("OrderNum"));
   	requestRow.setParameter("productName",productName);
   	requestRow.setParameter("returnNumberDate", statusOrderDate /*order.getField("CreatedDate")*/);
     
   	requestRow.setParameter("orderNumber",order.getField("OrderNum"));
   	requestRow.setParameter("productDetails",productDetails);       
   	requestRow.setParameter("statusOrderDate",statusOrderDate);
   	
   	/* Setup emailer */                            	   	
	}

	public boolean needsSending() 
	{
		// TODO Auto-generated method stub
		return false;
	}
	
}