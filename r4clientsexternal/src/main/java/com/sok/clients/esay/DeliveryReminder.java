package com.sok.clients.esay;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;

import javax.servlet.ServletContext;

import com.sok.Debugger;
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.framework.generation.GenerationKeys;
import com.sok.runway.autopilot.AbstractActionTypeEmail;
import com.sok.runway.autopilot.AutoPilotSchedule;
import com.sok.runway.RunwayEntity; 
import com.sok.runway.SessionKeys;
import com.sok.runway.crm.Contact; 

import com.sok.framework.servlet.HttpSession;
import com.sok.framework.servlet.ResponseOutputStream;

public class DeliveryReminder extends AbstractActionTypeEmail
{
   GenRow searchbean = null; 
   
   protected HttpSession session = null; // needs to have own session, don't have access to autopilots
   
   Debugger d = null; 
   
   /** 
    * Always return true since all contacts returned by DeliveryReminder's 
	 * search bean has at least one order item pending. 
	 * Search bean for order items cannot be generated here due to the execution 
	 * sequence in SendGeneratedEmail action type block for Contact. 
	 * Contrast with User type. 
	 */
   public boolean needsSending() 
   {      
   	return true; 
   }
   
   public void init(ServletContext context, AutoPilotSchedule schedule, Connection con) 
   {
   	com.sok.Debugger.getDebugger().debug("<p>init");
   	
   	searchbean = new GenRow();
   	
      this.session   = new HttpSession(context);  
      this.d         = Debugger.getDebugger();     
      
      super.init(context, schedule, con);
   }
   
   public boolean generateEmail(String noteID) 
   {
   	com.sok.Debugger.getDebugger().debug("<p>generateEmail");
      
   	searchbean.setConnection(getAutoPilotSchedule().getConnection());
      searchbean.setParameter("OrderItemOrder.ContactID", getEntity().getPrimaryKey());
   	
      getAutoPilotSchedule().getAutoPilot().getSession().setAttribute(SessionKeys.ORDER_SEARCH, searchbean);

      try 
      { 
      	com.sok.Debugger.getDebugger().debug("<p>write");
         write(getAutoPilotSchedule().getAutoPilot().generateEmail(getAutoPilotSchedule(), "/specific/autopilot/orderitemlist.jsp"));
      } 
      catch (Exception e) 
      {
      	com.sok.Debugger.getDebugger().debug("<p>exception");
         session.setAttribute(SessionKeys.ORDER_SEARCH, null);
         write("Exception thrown on generation: "+e.toString());
         return false; 
      }

      getAutoPilotSchedule().getAutoPilot().getSession().setAttribute(SessionKeys.ORDER_SEARCH, null);

      return true;
   }
   
}
