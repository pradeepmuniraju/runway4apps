package com.sok.clients.esay.tags;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import com.sok.runway.*;
import com.sok.framework.*;
import com.sok.runway.crm.*;

public class CanViewTag extends TagSupport
{
   private String 	name 	= null;
   private String 	type 	= null;
   private String 	path 	= null;
   private boolean 	debug = false;
   private JspWriter out 	= null;
   
   public void setBeanName(String name)
   {
      this.name = name;
   }
   
   public void setBeanType(String type)
   {
      this.type = type;
   }
   
   public void setErrorIncludePath(String path)
   {
      this.path = path;
   }
   
   public int doStartTag()
   {
   	com.sok.Debugger.getDebugger().debug("<p>start");
   	
      boolean 	canview 	= false;
      boolean 	loaded 	= false;

      out = pageContext.getOut();
      
      Object 	obj 		= pageContext.getAttribute(name, PageContext.REQUEST_SCOPE);
      Contact 	contact 	= (Contact)pageContext.getAttribute("CURRENT_LOGIN", PageContext.SESSION_SCOPE);
      
      if(contact != null && obj != null)
      {
      	com.sok.Debugger.getDebugger().debug("<p>contact: " + contact.getPrimaryKey());
      	
         if(obj instanceof GenRow)
         {
            GenRow row = (GenRow)obj;
            
            loaded = row.isSuccessful();
            
            if(loaded)
            {
               canview = contact.getPrimaryKey().equals(row.getData("ContactID"));
            }
         }
         else if(obj instanceof TableData)
         {
            TableData data = (TableData)obj;
            
            if(data.getTableSpec().hasField("CreatedDate"))
            {
               loaded = data.get("CreatedDate") != null;
            }
            else
            {
               loaded = data.getError().length() == 0;
            }
            if(loaded)
            {
               canview = contact.getPrimaryKey().equals(data.getData("ContactID"));
            }
         }
         else if(obj instanceof RunwayEntity)
         {         	
            RunwayEntity entity = (RunwayEntity)obj;
            
            com.sok.Debugger.getDebugger().debug("<p>runwayentity object: " + entity.getPrimaryKey());
            
            loaded = entity.isLoaded();
            
            com.sok.Debugger.getDebugger().debug("<p>loaded: " + String.valueOf(loaded));
            
            if(loaded)
            {
               canview = contact.getPrimaryKey().equals(entity.getField("ContactID"));
               
               com.sok.Debugger.getDebugger().debug("<p>loaded: " + String.valueOf(canview));
            }
         }
      }

      if(path == null)
      {
         if(!loaded)
         {
            printNotLoadedError();
         }
         else if(!canview)
         {
            printCantViewError();
         }
      }
      else
      {
         if(!loaded)
         {
            try
            {
               pageContext.include(path+"noexist.jsp");
            }
            catch(Exception e)
            {
               printNotLoadedError();
            }
         }
         else if(!canview)
         {
            
            try
            {
               pageContext.include(path+"noview.jsp");
            }
            catch(Exception e)
            {
               printCantViewError();
            }
         }
      }
      
      if(!loaded || !canview)
      {
         return(SKIP_BODY);
      }
      return(EVAL_BODY_INCLUDE);
   }
   
   protected void printNotLoadedError()
   {
      print(new StringBuffer().append("<div class=\"pageerror\">The ")
      .append(type)
      .append(" you are looking for does not exists or has been deleted.</div>").toString());
   }
   
   protected void printCantViewError()
   {
      print(new StringBuffer().append("<div class=\"pageerror\">You do not have permission to view this ")
      .append(type)
      .append("</div>").toString());
   }
   
   protected void print(String str)
   {
      try
      {
         out.print(str);
      }
      catch(Exception e)
      {}
   }
   
   public int doEndTag()
   {
      return(EVAL_PAGE);
   }
   
   public void setDebug(boolean debug)
   {
      this.debug = debug;
   }
   
   public void release()
   {
      out = null;
      name = null;
      type = null;
      debug = false;
      path = null;
   }
}
