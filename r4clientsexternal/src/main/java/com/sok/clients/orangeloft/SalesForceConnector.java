package com.sok.clients.orangeloft;

import java.rmi.RemoteException;
import javax.xml.rpc.ServiceException;

import com.sforce.soap.enterprise.LoginResult;
import com.sforce.soap.enterprise.QueryOptions;
import com.sforce.soap.enterprise.QueryResult;
import com.sforce.soap.enterprise.SaveResult;
import com.sforce.soap.enterprise.SessionHeader;
import com.sforce.soap.enterprise.SforceServiceLocator;
import com.sforce.soap.enterprise.SoapBindingStub;
import com.sforce.soap.enterprise.fault.ExceptionCode;
import com.sforce.soap.enterprise.fault.InvalidFieldFault;
import com.sforce.soap.enterprise.fault.InvalidIdFault;
import com.sforce.soap.enterprise.fault.InvalidSObjectFault;
import com.sforce.soap.enterprise.fault.LoginFault;
import com.sforce.soap.enterprise.fault.UnexpectedErrorFault;
import com.sforce.soap.enterprise.sobject.Lead;


public class SalesForceConnector {
	private SoapBindingStub binding;

	
	public boolean createLead(java.util.Map<String, String> leaddata){
		SalesForceConnector sfc = new SalesForceConnector(); 
		
		try { 
			if(sfc.login()) { 
				System.out.println("Login Success"); 
			} else { 
				System.out.println("Login Failed"); 
			}
		} catch (ServiceException se) { 
			System.out.println("Service Exception : "+se.getMessage()); 
		}
		
		/* need specs for rw integration */ 
		java.util.Map<String, String> f = leaddata; 
		
		String fn = f.get("FirstName"); 
		String ln = f.get("LastName"); 
		String em = f.get("Email"); 
		
		StringBuilder fc = new StringBuilder("select Id, FirstName, LastName, Email from Lead");
		
		
		fc.append(" where FirstName = '").append(fn).append("' and "); 
		fc.append(" LastName = '").append(ln).append("' and ");
		fc.append(" Email = '").append(em).append("';");
		
		Lead lead = (Lead)sfc.getRecord(fc.toString()); 
		
		String id = null; 

		if(lead == null) { 
			System.out.println("Lead not found");
			
			String desc = "Query Type : "+f.get("QueryType") +"\nComments : "+f.get("Comments");
			
			lead = new Lead();
			lead.setFirstName(f.get("FirstName"));
			lead.setLastName(f.get("LastName")); 
			lead.setCompany(f.get("Company")); 
			lead.setStreet(f.get("Street")); 
			lead.setCity(f.get("City")); 
			lead.setState(f.get("State")); 
			lead.setCountry(f.get("Country")); 
			lead.setPostalCode(f.get("PostCode")); 
			lead.setPhone(f.get("Phone")); 
			lead.setEmail(f.get("Email")); 
			lead.setDescription(desc);
			//lead.set
			//lead.setQT(f.get("QueryType"));
			//lead.setComments(f.get("Comments"));
			
			id = sfc.saveRecord(lead,"insert");  
			
			if(id == null) { 
				throw new RuntimeException("Save did not complete correctly"); 
			}
			
		} else { 
			System.out.println("Congrats, you exist"); 
			System.out.println("ID : " +lead.getId());
			System.out.println("FN : " +lead.getFirstName());
			System.out.println("LN : " +lead.getLastName());
			System.out.println("EM : " +lead.getEmail());
			
			lead.setId(lead.getId());
			lead.setFirstName(f.get("FirstName"));
			lead.setLastName(f.get("LastName")); 
			lead.setCompany(f.get("Company")); 
			lead.setStreet(f.get("Street")); 
			lead.setCity(f.get("City")); 
			lead.setState(f.get("State")); 
			lead.setCountry(f.get("Country")); 
			lead.setPostalCode(f.get("PostCode")); 
			lead.setPhone(f.get("Phone")); 
			lead.setEmail(f.get("Email"));
			
			id = sfc.saveRecord(lead,"update");
			
		}
		
		System.out.println("Lead ID : "+id); 
		
		/*Note n = new Note(); 
		n.setParentId(id); 
		n.setTitle(f.get("QueryType")); 
		n.setBody(f.get("Comments"));*/		
				
		if(id!=null)
			return true;
		else
			return false;
	}
	
	private String saveRecord(com.sforce.soap.enterprise.sobject.SObject lead,String type)  { 
		
		com.sforce.soap.enterprise.sobject.SObject[] createObj = {lead}; 
		
		try {
			
			SaveResult[] result = null;
			
			if(type!=null && type.equals("insert"))
				result = binding.create(createObj);
			else if(type.equals("update"))
				result = binding.update(createObj);

			System.out.println(result[0].toString());

			return result[0].getId(); 
		} catch (InvalidSObjectFault e) {
			System.out.println("InvalidSObjectFault :");
			e.printStackTrace();
		} catch (InvalidFieldFault e) {
			System.out.println("InvalidFieldFault :");
			e.printStackTrace();
		} catch (InvalidIdFault e) {
			System.out.println("InvalidIdFault :");
			e.printStackTrace();
		} catch (UnexpectedErrorFault e) {
			System.out.println("UnexpectedErrorFault :");
			e.printStackTrace();
		} catch (RemoteException e) {
			System.out.println("RemoteException :");
			e.printStackTrace();
		} 
		
		return null; 
	}
	
	/**
     * The sample client application executes a query by invoking the query call, 
     * passing a simple query string ("select FirstName, LastName from Lead") 
     * and iterating through the returned QueryResult.
     */
    private com.sforce.soap.enterprise.sobject.SObject getRecord(String query) {
        QueryOptions qo = new QueryOptions();
        qo.setBatchSize(200);
        binding.setHeader(new SforceServiceLocator().getServiceName().getNamespaceURI(), 
             "QueryOptions", qo);
        try {
            QueryResult qr = binding.query(query);
            
            if (qr.getSize() > 0) {
                      System.out.println("Logged in user can see "
                          + qr.getRecords().length + " lead records. ");
                      do {
                          // output lead records
                          for (int i = 0; i < qr.getRecords().length; i++) {
                              Lead lead = (Lead) qr.getRecords(i);
                              String fName = lead.getFirstName();
                              String lName = lead.getLastName();
                              if (fName == null) {
                                  System.out.println("Lead " + (i + 1) + ": "
                                   + lName);
                              } else {
                                      System.out.println("Lead " + (i + 1) + ": "
                                       + fName + " " + lName);
                              }
                          }
                          
                      if (!qr.isDone()) {
                              qr = binding.queryMore(qr.getQueryLocator());
                      } else {
                              break;
                      }
                       
                   } while (qr.getSize() > 0);
                      
                   return qr.getRecords(0); 
           } else {
                    System.out.println("No records found.");
           }            

     } catch (RemoteException ex) {
         System.out.println("\nFailed to execute query succesfully, error message was:" + 
                            "\n" + ex.getMessage());
     }
     return (com.sforce.soap.enterprise.sobject.SObject)null; 
    }
		
    /**
     * The login call is used to obtain a token from Salesforce.
     * This token must be passed to all other calls to provide
     * authentication.
     */
    public boolean login() throws ServiceException {
        String userName = "michael@switched-on.com.au";
        String password = "Sw1tch3d0n";
        /** Next, the sample client application initializes the binding stub.
         * This is our main interface to the API through which all 
         * calls are made. The getSoap method takes an optional parameter,
         * (a java.net.URL) which is the endpoint.
         * For the login call, the parameter always starts with 
         * http(s)://www.salesforce.com. After logging in, the sample 
         * client application changes the endpoint to the one specified 
         * in the returned loginResult object.
         */
        binding = (SoapBindingStub) new com.sforce.soap.enterprise.SforceServiceLocator().getSoap();
        
        // Time out after a minute
        binding.setTimeout(60000);
        // Test operation
        LoginResult loginResult;
        try {
            System.out.println("LOGGING IN NOW....");
            loginResult = binding.login(userName, password);
        }
        catch (LoginFault ex) {
            // The LoginFault derives from AxisFault
            ExceptionCode exCode = ex.getExceptionCode();
            if (exCode == ExceptionCode.FUNCTIONALITY_NOT_ENABLED ||
                exCode == ExceptionCode.INVALID_CLIENT ||
                exCode == ExceptionCode.INVALID_LOGIN ||
                exCode == ExceptionCode.LOGIN_DURING_RESTRICTED_DOMAIN ||
                exCode == ExceptionCode.LOGIN_DURING_RESTRICTED_TIME ||
                exCode == ExceptionCode.ORG_LOCKED ||
                exCode == ExceptionCode.PASSWORD_LOCKOUT ||
                exCode == ExceptionCode.SERVER_UNAVAILABLE ||
                exCode == ExceptionCode.TRIAL_EXPIRED ||
                exCode == ExceptionCode.UNSUPPORTED_CLIENT) {
                System.out.println("Please be sure that you have a valid username " +
                     "and password.");
            } else {
                // Write the fault code to the console
                System.out.println(ex.getExceptionCode());
                // Write the fault message to the console
                System.out.println("An unexpected error has occurred." + ex.getMessage());
            }
            return false;
        } catch (Exception ex) {
            System.out.println("An unexpected error has occurred: " + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
        // Check if the password has expired
        if (loginResult.isPasswordExpired()) {
            System.out.println("An error has occurred. Your password has expired.");
            return false;
        }
        /** Once the client application has logged in successfully, it will use 
         *  the results of the login call to reset the endpoint of the service  
         *  to the virtual server instance that is servicing your organization.  
         *  To do this, the client application sets the ENDPOINT_ADDRESS_PROPERTY 
         *  of the binding object using the URL returned from the LoginResult.
         */
        binding._setProperty(SoapBindingStub.ENDPOINT_ADDRESS_PROPERTY, 
            loginResult.getServerUrl());
        /** The sample client application now has an instance of the SoapBindingStub 
         *  that is pointing to the correct endpoint. Next, the sample client application 
         *  sets a persistent SOAP header (to be included on all subsequent calls that 
         *  are made with the SoapBindingStub) that contains the valid sessionId
         *  for our login credentials. To do this, the sample client application 
         *  creates a new SessionHeader object and set its sessionId property to the 
         *  sessionId property from the LoginResult object.
         */
        // Create a new session header object and add the session id
        // from the login return object
        SessionHeader sh = new SessionHeader();
        sh.setSessionId(loginResult.getSessionId());
        /** Next, the sample client application calls the setHeader method of the 
         *  SoapBindingStub to add the header to all subsequent method calls. This  
         *  header will persist until the SoapBindingStub is destroyed until the header 
         *  is explicitly removed. The "SessionHeader" parameter is the name of the 
         *  header to be added.
         */
        // set the session header for subsequent call authentication
        binding.setHeader(new SforceServiceLocator().getServiceName().getNamespaceURI(),
                          "SessionHeader", sh);
        // return true to indicate that we are logged in, pointed
        // at the right url and have our security token in place.
        return true;
    }


}
