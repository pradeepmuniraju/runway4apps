package com.sok.clients.ebg;

import java.sql.Connection;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.sok.runway.OfflineProcess; 

import com.sok.framework.ActionBean;
import com.sok.framework.KeyMaker;
import com.sok.framework.RowBean;
import com.sok.framework.RowSetBean;
import com.sok.framework.SpecManager;
import com.sok.framework.TableData;
import com.sok.framework.TableSpec;
import com.sok.framework.ViewSpec;

public class DMSCommitter extends OfflineProcess {
	
	int numRecords = 0; 
	int numErrors = 0; 
	int numProcessed = 0; 
	int numExisted = 0; 
	
	Connection connection;
	StringBuffer status = new StringBuffer(); 
	StringBuffer importLog = new StringBuffer(); 
	String importID; 
	HashMap productMap; 
	RowBean actionBean; 
	RowBean importData; 
	String notificationEmail = "michael@switched-on.com.au"; 
	
	TableSpec tspec; 
	ViewSpec vspec; 
	
	//Static Order StatusID - this could be moved into the ImportView etc if needs be. 
	private static final String orderStatusID = "1d3f4d3e-0e77-1029-a94a-0040d043a09c"; 

	public DMSCommitter(HttpServletRequest arg0, ServletContext arg1, String importID) {
		super(arg0, arg1);
		this.importID = importID; 
		buildProductMap();
		loadImportData(); 
	}

	public DMSCommitter(String arg0, HttpServletRequest arg1, ServletContext arg2, String importID) {
		super(arg0, arg1, arg2);
		this.importID = importID; 
		buildProductMap(); 
		loadImportData(); 
	}

	public DMSCommitter(RowBean arg0, HttpServletRequest arg1, ServletContext arg2, String importID) {
		super(arg0, arg1, arg2);
		this.importID = importID; 
		buildProductMap(); 
		loadImportData(); 
	}

	private void loadImportData() { 
	
		importData = new RowBean(); 
		importData.setConnection(getConnection());
		importData.setViewSpec("ImportView"); 
		importData.setColumn("ImportID",importID); 
		importData.setAction(ActionBean.SELECT); 
		importData.doAction(); 
		
		if(importData.getError().length()>0) { 
			errors.append("Error loading initial Import \r\n"); 
			errors.append(importData.getError()); 
			this.kill("Import Error"); 
		}
	}
	
	public Connection getConnection() {
   	   try {
      	   if (connection == null || connection.isClosed()) {
         	   Context ctx = new InitialContext();
            
      			DataSource ds = null;
      			try {
      				Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
      				ds = (DataSource)envContext.lookup(dbconn);
      			}
      			catch(Exception e) {}
      			if(ds == null)
      			{
      				ds = (DataSource)ctx.lookup(dbconn);
      			}
      			try {
   					ctx.close();
   					ctx = null;
   				}
   				catch(Exception e) { errors.append(ActionBean.writeStackTraceToString(e)); }
      			connection = ds.getConnection();
      		}
			}
			catch(Exception e) { errors.append(ActionBean.writeStackTraceToString(e)); }
   		return connection;
   	}
	
	public void execute() {
		try {
      		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, getLocale());
      		dt.setTimeZone(TimeZone.getTimeZone("Australia/Melbourne"));
      		
      		this.status.append(context.getServletContextName());
      		this.status.append(" Runway offline process - DMS ");
      		this.status.append(" Data Import\r\n");
      		this.status.append("Initiated ");
      		this.status.append(dt.format(new java.util.Date()));
      		this.status.append("\r\n");
      		
   			connection = getConnection();
   			
   			StringBuffer count = new StringBuffer("select count(*) as cnt from Specific_ImportDMS where ImportID='");
   			count.append(importID); 
   			count.append("' and ImportFlg='"); 
   			
   			RowBean initCount = new RowBean(); 
   			initCount.setConnection(getConnection()); 
   			initCount.setSearchStatement(count.toString()); 
   			initCount.retrieveDataOnStmt(); 
   			
   			try { 
   				Integer.parseInt(initCount.getString("cnt"));
   			} catch (Exception e) { 
   				numRecords = -1; 
   			}
   			
   			RowSetBean dmsData = new RowSetBean(); 
   			dmsData.setConnection(getConnection()); 
   			dmsData.setViewSpec(vspec); 
   			dmsData.setColumn("ImportID",importID); 
   			dmsData.generateSQLStatement(); 
   			dmsData.getResults(); 
   			
   			actionBean = new RowBean(); 
   			actionBean.setConnection(getConnection()); 
   			
   			while (dmsData.getNext() && !this.isKilled()) { 
   			
   				if(!orderExists(dmsData)) { 
		   			//insert the needed contacts 
					if(!"M".equals(dmsData.getString("ImportFlg"))) { 
						importDMSContact(dmsData); 
					} else { 
						updateDMSContact(dmsData);
					}				
		   			importProfileAnswers(dmsData);
					importDMSOrder(dmsData);
					
					numProcessed++; 
   				} else { 
   					numExisted++; 
   				}
   			}
   			dmsData.close(); 
   				
   			//add counts to importlog 
   			updateImport(importID); 
   			   			
      		status.append("Total Records : ").append(numRecords).append("\r\n"); 
      		status.append("Records processed : ").append(numProcessed).append("\r\n");
      		status.append("Records skipped due to duplication : ").append(numExisted).append("\r\n");
      		status.append("Errors : ").append(numErrors).append("\r\n");
      		
      		status.append("\r\n\r\n Import Log: \r\n" ); 
      		status.append(importLog);
      		
      		if(errors.length()>0) { 
	      		System.out.println("\r\n\r\n DMS Import Errors: ");
	      		System.out.print(errors.toString()); 
      		} 
      		
      		connection.close();
   			
   		} catch (Exception e) { 
   			status.append(ActionBean.writeStackTraceToString(e)); 
   		}
	}
	
	private boolean orderExists(TableData bean) { 
		
		StringBuffer sql = new StringBuffer("select OrderID from Orders where Email = '"); 
		sql.append(bean.getString("BatchNo")).append(">>");
		sql.append(bean.getString("RecordNo")).append(">>"); 
		sql.append(bean.getString("SourceId")).append("';");
		
		actionBean.clear(); 
		actionBean.setSearchStatement(sql.toString()); 
		actionBean.retrieveDataOnStmt(); 
		
		return (actionBean.getString("OrderID").length()>0);
	}
	
	private void updateImport(String importID) { 
		
		actionBean.clear(); 
		actionBean.setViewSpec("ImportView"); 
		actionBean.setColumn("ImportID",importID); 
		actionBean.setColumn("Mode","DMSImport-D"); 
		actionBean.setColumn("CommittedDate","NOW"); 
		actionBean.setAction(ActionBean.UPDATE); 
		actionBean.doAction(); 
		
		if (actionBean.getError().length()>0) { 
			errors.append("\r\n\r\nError updating import record :\r\n"); 
			errors.append(actionBean.getError()); 
			numErrors++; 
		}
		
	}
	
	private void importDMSContact(TableData bean) {
		
		String statusID = KeyMaker.generate(); 
		
		actionBean.clear(); 
		//*actionBean.setConnection(getConnection()); 
		actionBean.setViewSpec("ContactView"); 
		actionBean.setColumn("ContactID",bean.getString("ContactID")); 
		actionBean.setColumn("GroupID",importData.getString("GroupID"));
		actionBean.setColumn("Source",importData.getString("Source")); 
		actionBean.setColumn("FirstName",bean.getString("FirstName"));
		actionBean.setColumn("LastName",bean.getString("LastName"));
		actionBean.setColumn("Street",bean.getString("Street"));
		actionBean.setColumn("City",bean.getString("City"));
		actionBean.setColumn("State",bean.getString("State"));
		actionBean.setColumn("Postcode",bean.getString("Postcode"));
		actionBean.setColumn("Phone",bean.getString("Phone"));
		actionBean.setColumn("Email",bean.getString("Email"));
		actionBean.setColumn("Notes",bean.getString("Notes"));
		
		actionBean.setColumn("RepUserID",importData.getString("RepUserID"));
		actionBean.setColumn("MgrUserID",importData.getString("MgrUserID"));
		
	//Didn't work	bean.putAll(actionBean); 
		
		actionBean.setColumn("ContactStatusID",statusID); 
		actionBean.setColumn("CreatedDate", "NOW");
		actionBean.setColumn("ModifiedDate", "NOW");
		actionBean.setColumn("CreatedBy",importData.getColumn("CreatedBy")); 
		actionBean.setColumn("ModifiedBy",importData.getColumn("CreatedBy"));
		actionBean.setAction(ActionBean.INSERT); 
		actionBean.doAction(); 
		
		if (actionBean.getError().length() > 0) {
			importLog.append("\r\nError inserting Contact record - ").append(bean.getString("ContactID"));
			importLog.append(".\r\n");
			errors.append(".\r\n").append(actionBean.getError());
			numErrors++;
		}
		
		actionBean.clear(); 
		actionBean.setViewSpec("ContactStatusView"); 
		actionBean.setColumn("ContactID",bean.getColumn("ContactID"));
		actionBean.setColumn("ContactStatusID",statusID); 
		actionBean.setColumn("CreatedDate","NOW"); 
		actionBean.setColumn("CreatedBy",importData.getColumn("CreatedBy")); 
		actionBean.setColumn("StatusID",importData.getColumn("ContactStatusID")); 
		actionBean.setAction(ActionBean.INSERT); 
		actionBean.doAction(); 
	}
	
	private void updateDMSContact(TableData bean) { 
	
		actionBean.clear(); 
		actionBean.setViewSpec("ContactView"); 
		actionBean.setColumn("ContactID",bean.getString("ContactID")); 
		actionBean.setAction(ActionBean.SELECT); 
		actionBean.doAction(); 
		
		if(!actionBean.getColumn("Phone").equals(bean.getString("Phone"))) { 
			if (actionBean.getColumn("Phone").length()==0) { 
				actionBean.setColumn("Phone",bean.getString("Phone")); 
			}else if (actionBean.getColumn("HomePhone").length()==0) { 
				actionBean.setColumn("Phone",bean.getString("Phone")); 
			} else if (actionBean.getColumn("Mobile").length()==0) { 
				actionBean.setColumn("Phone",bean.getString("Phone")); 
			}
		}
				
		if(!actionBean.getColumn("Email").equals(bean.getString("Email"))) { 
			if (actionBean.getColumn("Email").length()==0) { 
				actionBean.setColumn("Email",bean.getString("Email")); 
			}else if (actionBean.getColumn("Email2").length()==0) { 
				actionBean.setColumn("Email",bean.getString("Email")); 
			} 
		}
		if(!actionBean.getColumn("Street").equals(bean.getString("Street"))) { 
			if (actionBean.getColumn("Street").length()==0) { 
				actionBean.setColumn("Street",bean.getString("Street"));
				actionBean.setColumn("City",bean.getString("City")); 
				actionBean.setColumn("PostCode",bean.getString("PostCode"));
				actionBean.setColumn("State",bean.getString("State"));
			}else if (actionBean.getColumn("HomeStreet").length()==0) { 
				actionBean.setColumn("HomeStreet",bean.getString("Street"));
				actionBean.setColumn("HomeCity",bean.getString("City")); 
				actionBean.setColumn("HomePostCode",bean.getString("PostCode"));
				actionBean.setColumn("HomeState",bean.getString("State"));
			} 
		}
		
		actionBean.remove("CreatedDate"); 
		actionBean.setColumn("ModifiedDate", "NOW");
		actionBean.setColumn("ModifiedBy",importData.getColumn("CreatedBy"));
		actionBean.setAction(ActionBean.UPDATE); 
		actionBean.doAction(); 
		
		if (actionBean.getError().length() > 0) {
			importLog.append("\r\nError updating Contact record - ").append(bean.getString("ContactID"));
			importLog.append(".\r\n");
			errors.append(".\r\n").append(actionBean.getError());
			numErrors++;
		}
	}
	
	private void importProfileAnswers(TableData bean) { 
		/*
		 * IF PROFILE ANSWERS ARE REQUIRED DO IT HERE
		 */
		String SourceID = "0M1W1Y5U067W5O7V4P8G799O268E"; 
		String BatchNo = "0F1F1E5020688Y9P365236194I6C"; 
		String RecordNo = "00161R5A2Z6S8S9Z4U7B1T1A1X2S";
		String Site = "0M1W10530134370I0F1D6T9Y1N2D"; 
		String CCSPDate = "021T195T8Z5P314R5X1P7J0H8S58";
		String EHW = "0C161I54023538733O2W6E4K6R6E"; 
		String GHW = "0D181T5004363V7Q4D0T0Y221Y6K"; 
		String UHW = "07111J5X0P3F307T4M5E6V8S9R7Q";  
		
		String OptOut = "041Z135R845C6Y2P82828T4C365P"; 
		String TruEnergy = "0C1W1I5W0D1T8K1D3Q7H449L5T51"; 
		String dupYes = "0P1F185I8N8U8K6G5K7W4G445A7N"; 
		String dupText = "091N1O598J8R8K6455932C9Q0E4P";

		actionBean.clear(); 
		actionBean.setViewSpec("answers/OrderAnswerView"); 
		actionBean.setToNewID("AnswerID"); 
		actionBean.setColumn("OrderID",bean.getColumn("OrderID"));
		actionBean.setColumn("QuestionID",SourceID);
		actionBean.setColumn("Answer",bean.getColumn("SourceId")); 
		actionBean.setColumn("AnswerText",bean.getColumn("SourceId"));
		
		actionBean.setColumn("CreatedDate", "NOW");
		actionBean.setColumn("ModifiedDate", "NOW");
		actionBean.setColumn("CreatedBy",importData.getColumn("CreatedBy")); 
		actionBean.setColumn("ModifiedBy",importData.getColumn("CreatedBy"));
		actionBean.setAction(ActionBean.INSERT);
		actionBean.doAction(); 
		
		if (actionBean.getError().length() > 0) {
			importLog.append("\r\nError inserting SourceId - ").append(bean.getString("OrderID"));
			importLog.append(".\r\n");
			errors.append("\r\n").append(actionBean.getError());
			numErrors++; 
		}
		
		actionBean.setToNewID("AnswerID"); 
		actionBean.setColumn("QuestionID",BatchNo); 
		actionBean.setColumn("Answer",bean.getColumn("BatchNo")); 
		actionBean.setColumn("AnswerText",bean.getColumn("BatchNo"));
		actionBean.doAction(); 
		
		if (actionBean.getError().length() > 0) {
			importLog.append("\r\nError inserting BatchNo - ").append(bean.getString("OrderID"));
			importLog.append(".\r\n");
			errors.append("\r\n").append(actionBean.getError());
			numErrors++; 
		}
		
		actionBean.setToNewID("AnswerID"); 
		actionBean.setColumn("QuestionID",RecordNo); 
		actionBean.setColumn("Answer",bean.getColumn("RecordNo")); 
		actionBean.setColumn("AnswerText",bean.getColumn("RecordNo"));
		actionBean.doAction(); 
		
		if (actionBean.getError().length() > 0) {
			importLog.append("\r\nError inserting RecordNo - ").append(bean.getString("OrderID"));
			importLog.append(".\r\n");
			errors.append("\r\n").append(actionBean.getError());
			numErrors++; 
		}
		
		actionBean.setToNewID("AnswerID"); 
		actionBean.setColumn("QuestionID",Site); 
		actionBean.setColumn("Answer",bean.getColumn("Site")); 
		actionBean.setColumn("AnswerText",bean.getColumn("Site"));
		actionBean.doAction(); 
		
		if (actionBean.getError().length() > 0) {
			importLog.append("\r\nError inserting RecordNo - ").append(bean.getString("OrderID"));
			importLog.append(".\r\n");
			errors.append("\r\n").append(actionBean.getError());
			numErrors++; 
		}
		
		actionBean.setToNewID("AnswerID"); 
		actionBean.setColumn("QuestionID",CCSPDate); 
		actionBean.setColumn("Answer",bean.getColumn("SignedDate")); 
		actionBean.setColumn("AnswerText",bean.getColumn("SignedDate"));
		actionBean.doAction(); 
		
		if (actionBean.getError().length() > 0) {
			importLog.append("\r\nError inserting SignedDate - ").append(bean.getString("OrderID"));
			importLog.append(".\r\n");
			errors.append("\r\n").append(actionBean.getError());
			numErrors++; 
		}
		
		if(bean.getColumn("EHW").equals("1")) { 
			actionBean.setToNewID("AnswerID"); 
			actionBean.setColumn("QuestionID",EHW); 
			actionBean.setColumn("Answer","Yes"); 
			actionBean.setColumn("AnswerText","Yes");
			actionBean.doAction(); 
			
			if (actionBean.getError().length() > 0) {
				importLog.append("\r\nError inserting EHW Profile - ").append(bean.getString("OrderID"));
				importLog.append(".\r\n");
				errors.append("\r\n").append(actionBean.getError());
				numErrors++; 
			}
		}
		
		if(bean.getColumn("GHW").equals("1")) { 
			actionBean.setToNewID("AnswerID"); 
			actionBean.setColumn("QuestionID",GHW); 
			actionBean.setColumn("Answer","Yes"); 
			actionBean.setColumn("AnswerText","Yes");
			actionBean.doAction(); 
			
			if (actionBean.getError().length() > 0) {
				importLog.append("\r\nError inserting EHW Profile - ").append(bean.getString("OrderID"));
				importLog.append(".\r\n");
				errors.append("\r\n").append(actionBean.getError());
				numErrors++; 
			}
		}
		
		if(bean.getColumn("UHW").equals("1")) { 
			actionBean.setToNewID("AnswerID"); 
			actionBean.setColumn("QuestionID",UHW); 
			actionBean.setColumn("Answer","Yes"); 
			actionBean.setColumn("AnswerText","Yes");
			actionBean.doAction(); 
			
			if (actionBean.getError().length() > 0) {
				importLog.append("\r\nError inserting EHW Profile - ").append(bean.getString("OrderID"));
				importLog.append(".\r\n");
				errors.append("\r\n").append(actionBean.getError());
				numErrors++; 
			}
		}
		
		if(bean.getColumn("Duplicate").equals("1")) { 	
			actionBean.setToNewID("AnswerID"); 
			actionBean.setColumn("QuestionID",dupYes); 
			actionBean.setColumn("Answer","Yes"); 
			actionBean.setColumn("AnswerText","Yes");
			actionBean.doAction(); 

			if (actionBean.getError().length() > 0) {
				importLog.append("\r\nError inserting DupYes - ").append(bean.getString("ContactID"));
				importLog.append(".\r\n");
				errors.append("\r\n").append(actionBean.getError());
				numErrors++; 
				
			} else { 
				actionBean.setToNewID("AnswerID"); 
				actionBean.setColumn("QuestionID",dupText); 
				actionBean.setColumn("Answer",bean.getColumn("Note")); 
				actionBean.setColumn("AnswerText",bean.getColumn("Note"));
				actionBean.doAction(); 

				if (actionBean.getError().length() > 0) {
					importLog.append("\r\nError inserting Duplicate Text - ").append(bean.getString("ContactID"));
					importLog.append(".\r\n");
					errors.append("\r\n").append(actionBean.getError());
					numErrors++; 
				}
			}
		}
		
		
		/*
		 * CONTACT PROFILE ANSWERS AFTER THIS POINT! 
		 */
		
		actionBean.remove("OrderID"); 
		actionBean.setColumn("ContactID",bean.getColumn("ContactID")); 
		actionBean.setViewSpec("answers/ContactAnswerView"); 
		
		if(bean.getColumn("OptOut").equals("1")) { 	
			actionBean.setToNewID("AnswerID"); 
			actionBean.setColumn("QuestionID",OptOut); 
			actionBean.setColumn("Answer","Yes"); 
			actionBean.setColumn("AnswerText","Yes");
			actionBean.doAction(); 

			if (actionBean.getError().length() > 0) {
				importLog.append("\r\nError inserting Opt Out - ").append(bean.getString("ContactID"));
				importLog.append(".\r\n");
				errors.append("\r\n").append(actionBean.getError());
				numErrors++; 
			}
		}
		

		
		if(bean.getColumn("Info").equals("1")) { 	
			actionBean.setToNewID("AnswerID"); 
			actionBean.setColumn("QuestionID",TruEnergy); 
			actionBean.setColumn("Answer","Yes"); 
			actionBean.setColumn("AnswerText","Yes");
			actionBean.doAction(); 

			if (actionBean.getError().length() > 0) {
				importLog.append("\r\nError inserting TruEnergy - ").append(bean.getString("ContactID"));
				importLog.append(".\r\n");
				errors.append("\r\n").append(actionBean.getError());
				numErrors++; 
			}
		}
	
	}
	
	private void importDMSOrder(TableData bean) { 
		
		String statusID = KeyMaker.generate(); 
		
		actionBean.clear(); 
		//*actionBean.setConnection(getConnection()); 
		actionBean.setViewSpec("OrderView"); 
		actionBean.setColumn("OrderID",bean.getColumn("OrderID")); 
		actionBean.setColumn("Name","DMS - "+bean.getColumn("FullName")); 
		actionBean.setColumn("ContactID",bean.getColumn("ContactID")); 
		actionBean.setColumn("DateOfSale",bean.getColumn("SignedDate"));
		
		actionBean.setColumn("GroupID",importData.getString("GroupID")); 
		actionBean.setColumn("OrderStatusID",statusID); 
		actionBean.setColumn("RepUserID",importData.getString("RepUserID"));
		actionBean.setColumn("MgrUserID",importData.getString("MgrUserID"));
		
		actionBean.setColumn("Email",bean.getString("BatchNo")+">>"+bean.getString("RecordNo")+">>"+bean.getString("SourceId"));
				
		actionBean.setColumn("CreatedDate", "NOW");
		actionBean.setColumn("ModifiedDate", "NOW");
		actionBean.setColumn("CreatedBy",importData.getColumn("CreatedBy")); 
		actionBean.setColumn("ModifiedBy",importData.getColumn("CreatedBy"));
		actionBean.setAction(ActionBean.INSERT); 
		actionBean.doAction(); 
		
		if (actionBean.getError().length() > 0) {
			importLog.append("\r\nError inserting Order record - ").append(bean.getString("OrderID"));
			importLog.append(".\r\n");
			errors.append("\r\n").append(actionBean.getError());
			numErrors++; 
		} else {
			insertOrderItems(bean); 
		}
		
		actionBean.clear(); 
		actionBean.setViewSpec("OrderStatusView"); 
		actionBean.setColumn("OrderID",bean.getColumn("OrderID"));
		actionBean.setColumn("OrderStatusID",statusID); 
		actionBean.setColumn("CreatedDate","NOW"); 
		actionBean.setColumn("CreatedBy",importData.getColumn("CreatedBy")); 
		actionBean.setColumn("StatusID",orderStatusID); 
		actionBean.setAction(ActionBean.UPDATE); 
		actionBean.doAction(); 
		
		if (actionBean.getError().length() > 0) {
			importLog.append("\r\nError inserting Order Status record - ").append(bean.getString("OrderID"));
			importLog.append(".\r\n");
			errors.append("\r\n").append(actionBean.getError());
			numErrors++; 
		}
	}

	private void insertOrderItems(TableData bean) { 
		
		actionBean.clear(); 
		actionBean.setViewSpec("OrderItemsView");
		
		RowBean productBean = new RowBean(); 
		productBean.setViewSpec("ProductView"); 
		
		Iterator iterator = productMap.keySet().iterator();
		String key = ActionBean._blank; 
		
		double cost = 0.00; 
		double gst = 0.00; 
		double totalcost = 0.00; 		
		double quantity = 0.00; 
		
		double tcost = 0.00; 
		double tgst = 0.00; 
		double ttotalcost = 0.00; 		
		double tquantity = 0.00; 
		
		
		while (iterator.hasNext()) { 
			key = (String)iterator.next(); 
			
			if (bean.getString(key).length()>0 && !"0".equals(bean.getString(key))) { 
				productBean.clear(); 
				productBean.setColumn("ProductID",(String)productMap.get(key)); 
				productBean.setAction(ActionBean.SELECT); 
				productBean.doAction(); 
				
				//productBean.putAll(actionBean); 	DOESN'T WORK. 
				actionBean.setColumn("ProductID",productBean.getColumn("ProductID"));
				actionBean.setColumn("Name",productBean.getColumn("Name")); 
				actionBean.setColumn("Description",productBean.getColumn("Description")); 
				try { 
					if(productBean.getColumn("Cost").length()==0) { 
						cost = 0; 
					} else { 
						cost = Double.parseDouble(productBean.getColumn("Cost")); 
					}
					quantity = Double.parseDouble(bean.getString(key));
					cost = cost * quantity; 
					gst = cost * 0.1; 
					totalcost = cost + gst; 
					
					tcost += cost; 
					tgst += gst; 
					ttotalcost =+ totalcost; 
					tquantity =+ quantity; 
				
					actionBean.setColumn("Cost",cost+""); 
					actionBean.setColumn("GST",gst+"");
					actionBean.setColumn("TotalCost",totalcost+"");
					
				} catch (Exception e) { 
					errors.append("\r\n Error calculating product cost OrderID = ").append(bean.getColumn("OrderID")); 
					errors.append(" Item column = ").append(key); 
				}
				actionBean.setColumn("ProductGroupID",productBean.getColumn("ProductGroupID"));
				
				actionBean.setColumn("OrderID",bean.getColumn("OrderID")); 
				actionBean.setColumn("Quantity",bean.getString(key));
				actionBean.setToNewID("OrderItemID"); 
				actionBean.remove("CreatedDate"); 
				actionBean.remove("CreatedBy"); 
				actionBean.setAction(ActionBean.INSERT); 
				actionBean.doAction(); 
				
				if (actionBean.getError().length() > 0) {
					importLog.append("\r\nError updating Order Item record - ").append(bean.getString("OrderID"));
					importLog.append("\r\nItem - ").append(key);
					importLog.append(".\r\n");
					errors.append("\r\n").append(actionBean.getError());
					numErrors++; 
				}
				
				actionBean.clear(); 
				productBean.clear();
			}
		}
		
		//Update the order with the up d costs
		actionBean.clear(); 
		actionBean.setViewSpec("OrderView"); 
		actionBean.setColumn("OrderID", bean.getString("OrderID"));
		actionBean.setAction(ActionBean.UPDATE);

		actionBean.setColumn("Cost", tcost+"");
		actionBean.setColumn("Quantity", tquantity+"");
		actionBean.setColumn("GST", tgst+"");	
		actionBean.setColumn("TotalCost", ttotalcost+"");
		actionBean.doAction(); 
		
		if (actionBean.getError().length() > 0) {
			importLog.append("\r\nError updating Order Cost / Quantitys- ").append(bean.getString("OrderID"));
			importLog.append(".\r\n");
			errors.append("\r\n").append(actionBean.getError());
			numErrors++; 
		}
	}
	
	private void buildProductMap() { 
		
	/*
	 * These are the ProductID's and their matching column's in the table.
	 */
		productMap = new HashMap(); 
		productMap.put("6000Hr","0T1F185Z0H2M4Q4B0T373G3A7R8G");
		productMap.put("8000Hr","011R1A5W0521404Z358U8E833S6G");
		productMap.put("15000Hr","0S1B12560V2E48735L6J592Q7S0S");
		productMap.put("ShLE","0F1J1G5X0H20435Y5L1Y3S1C9A8E");
		productMap.put("ShLG","04181E5Z0T2I4L5O5F5E4L3R6K5B");
		productMap.put("ShLU","06141Z580W2T4L536L6V267I326M");
		/*
		 * Lights only pack disabled on EBG Request
		 * productMap.put("L","0J1C1F570Z2K4E7F306M923V2B3S");
		 */
		productMap.put("ShE","021C165Q0D24487E4V043E181Q8T");
		productMap.put("ShG","0V191A5B0P2A467A4K5N836G1R7D");
		productMap.put("ShU","0A1Q1354072K407A5B0L7K0G4U6I");
	}
	
	public void setTableSpec(TableSpec tspec) { 
		this.tspec = tspec; 
	}
	public void setViewSpec(ViewSpec vspec) { 
		this.vspec = vspec; 
	}
	
	public void setTableSpec(String tspec) { 
		this.tspec = SpecManager.getTableSpec(tspec); 
	}
	
	public void setEmail(String email)  {
		if (email!= null && email.length()>0) { 
			notificationEmail = email;
		}
	}
	
	public void startProcess() { 
		super.start(); 
	}
	   
	public String getStatusMailBody() {
		return status.toString();
	}

	public String getStatusMailSubject() {
		return  "DMS Import Committed";
	}

	public String getStatusMailRecipient() {
		return notificationEmail;
	}

	public int getProcessSize() {
		return numRecords;
	}

	public int getProcessedCount() {
		return numProcessed;
	}

	public int getErrorCount() {
		return numErrors;
	}
}
