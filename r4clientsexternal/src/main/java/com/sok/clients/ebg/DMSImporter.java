package com.sok.clients.ebg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.sok.framework.*;
import com.sok.runway.OfflineProcess;
import com.sok.framework.RowBean;

public class DMSImporter extends OfflineProcess {
	
	private String groupID = "0P1T1G5U1X6F3F533Z47756Q8H0Z"; //EBG DMS Data
	private String repUserID = "E3B3653517125741C947061350C510E4"; //System Internal
	private String mgrUserID = "E3B3653517125741C947061350C510E4"; //System Internal
	private String source = ActionBean._blank;
	private String contactStatusID = "0S1J1M5W0K13843P8P4Q2W9U1W6V";  //Customer
	private String userID = ActionBean._blank;
	private String name = ActionBean._blank; 
	private String notificationEmail = "michael@switched-on.com.au"; 
	private StringBuffer status = new StringBuffer(); 
	private StringBuffer importLog = new StringBuffer(); 
	private StringBuffer matchedContacts = new StringBuffer(); 
	private File importFile = null; 
	private ArrayList columnHeadings = null; 
	private RowBean updateBean = new RowBean();  
	
	private TableSpec tspec = null; 
	private ViewSpec vspec = null; 
	
	private int numErrors = 0; 
	private int numProcessed = 0; 
	
	private int numRecords = 0;
	private int numSuccess = 0;
    private int numRecordErrors = 0;
	
   	private Connection connection = null;
	
	public DMSImporter(HttpServletRequest arg0, ServletContext arg1) {
		super(arg0, arg1);
		this.importFile = getFile(arg0); 
	}

	public DMSImporter(String arg0, HttpServletRequest arg1, ServletContext arg2) {
		super(arg0, arg1, arg2);
		this.importFile = getFile(arg1); 
	}

	public DMSImporter(RowBean arg0, HttpServletRequest arg1, ServletContext arg2) {
		super(arg0, arg1, arg2);
		this.importFile = getFile(arg1); 
	}

	public void setGroupID(String groupID) { 
		if (groupID != null) { 
			this.groupID = groupID; 
		}
	}
	public void setRepUserID(String repUserID) { 
		if (repUserID != null) { 
			this.repUserID = repUserID; 
		}
	}
	public void setMgrUserID(String mgrUserID) { 
		if (mgrUserID != null) { 
			this.mgrUserID = mgrUserID;
		}
	}
	public void setSource(String source) { 
		if (source != null) { 
			this.source = source; 
		}
	}
	
	public void setUserID(String userID) { 
		if (userID!=null) { 
			this.userID = userID; 
		}
	}
   
	public void setImportName(String name) { 
		if (name != null) { 
			this.name = name; 
		}
	}
	
	public void setTableSpec(TableSpec tspec) { 
		this.tspec = tspec; 
	}
	public void setViewSpec(ViewSpec vspec) { 
		this.vspec = vspec; 
	}
	
	public void setTableSpec(String tspec) { 
		this.tspec = SpecManager.getTableSpec(tspec); 
	}
	
	public void setEmail(String email) { 
		if (email!= null && email.length()>0) { 
			notificationEmail = email;
		}
	}
   
   public void setContactStatusID(String contactStatusID) {
      if (contactStatusID != null) {
         this.contactStatusID = contactStatusID;
      }
   }

   public void startProcess() { 
	   super.start(); 
   }
   
	protected void createImportRecord(String importID) {
	      //String importID = KeyMaker.generate();
	      RowBean bean = new RowBean();
	      bean.setTableSpec("Imports");
	      bean.setConnection(this.getConnection());
	      bean.setColumn("ImportID", importID);
	      bean.setColumn("ContactStatusID", contactStatusID);
	      bean.setColumn("CreatedBy", this.userID);
	      bean.setColumn("CreatedDate", "NOW");
	      bean.setColumn("GroupID",groupID);
	      bean.setColumn("Source", source);
	      bean.setColumn("Name", name);
	      bean.setColumn("RepUserID", repUserID);
	      bean.setColumn("MgrUserID", mgrUserID);
	      bean.setColumn("Mode", "DMS Import");
	      bean.setColumn("CommittedDate","NOW"); 
	      bean.setAction("insert");
	      bean.doAction();
	     
	      
	      if (bean.getError().length() > 0) {
	         errors.append("\r\nError creating import record - ");
	         errors.append(bean.getError());
	         errors.append(".\r\n");
	         
	      }
	   }
	
	public Connection getConnection() {
   	   try {
      	   if (connection == null || connection.isClosed()) {
         	   Context ctx = new InitialContext();
            
      			DataSource ds = null;
      			try {
      				Context envContext  = (Context)ctx.lookup(ActionBean.subcontextname);
      				ds = (DataSource)envContext.lookup(dbconn);
      			}
      			catch(Exception e) {}
      			if(ds == null)
      			{
      				ds = (DataSource)ctx.lookup(dbconn);
      			}
      			try {
   					ctx.close();
   					ctx = null;
   				}
   				catch(Exception e) { errors.append(ActionBean.writeStackTraceToString(e)); }
      			connection = ds.getConnection();
      		}
			}
			catch(Exception e) { errors.append(ActionBean.writeStackTraceToString(e)); }
   		return connection;
   	}
	
	public void execute() {
   		try {
      		DateFormat dt = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, getLocale());
      		dt.setTimeZone(TimeZone.getTimeZone("Australia/Melbourne"));
      		
      		this.status.append(context.getServletContextName());
      		this.status.append(" Runway offline process - DMS ");
      		this.status.append(" Data Import\r\n");
      		this.status.append("Initiated ");
      		this.status.append(dt.format(new java.util.Date()));
      		this.status.append("\r\n");
      		
   			connection = getConnection();
   			
   			determineColumnHeadings(); 
   			
   			String importID = processFile(); 
   			
   			detectConflicts(importID); 
   			
   			markForImport(importID); 
   			
   			importLog.append("\r\n\r\nImport Summary: \r\n"); 
   			
   			StringBuffer count = new StringBuffer("select count(*) as cnt from Specific_ImportDMS where ImportID='");
   			count.append(importID); 
   			count.append("' and ImportFlg='"); 
   			
   			RowBean counts = new RowBean();   
   			counts.setConnection(getConnection()); 
   			counts.setSearchStatement(count.toString()+"Y'");
   			counts.retrieveDataOnStmt(); 

			importLog.append("\r\n");
			importLog.append(counts.getString("cnt"));
			importLog.append(" to be imported as new contacts");
			
			counts.clear(); 
   			counts.setSearchStatement(count.toString()+"M'");
   			counts.retrieveDataOnStmt(); 
			
			importLog.append("\r\n");
			importLog.append(counts.getString("cnt"));   				
			importLog.append(" matched to current contacts"); 
   			
      		status.append(importLog);
      		
      		if(matchedContacts.length()>0) { 
	      		status.append("\r\n\r\n Matched Contacts: \r\n");
	      		status.append(matchedContacts.toString()); 
      		} 
      		
      		if(errors.length()>0) { 
	      		System.out.println("\r\n\r\n DMS Import Errors: ");
	      		System.out.print(errors.toString()); 
      		} 
      		
      		connection.close();
   			
   		} catch (Exception e) { 
   			status.append("Exception : "+ActionBean.writeStackTraceToString(e)); 
   		}
	}

	private void markForImport(String importID) { 
		
		RowBean mi = new RowBean(); 
		mi.setConnection(getConnection()); 
		
		mi.setAction(ActionBean.EXECUTE);

		StringBuffer setImportFlag = new StringBuffer();
		setImportFlag.append("UPDATE Specific_ImportDMS");
		setImportFlag.append(" set ImportFlg = 'Y' WHERE ImportID = '");
		setImportFlag.append(importID);
		setImportFlag.append("' AND (ImportFlg is null OR (ImportFlg <> 'M' AND ImportFlg <> 'C'))");
		mi.setSearchStatement(setImportFlag.toString());
		mi.doAction();
		
		errors.append(mi.getError()); 
	}
	
	private String processFile() {

		String importID = KeyMaker.generate();
	
		if (importID != null) {
			TableBean bean = new TableBean();
			bean.setConnection(this.getConnection());
			bean.setTableSpec(this.tspec);
			
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(importFile));
	
				ArrayList record = new ArrayList();
				String line = reader.readLine();
				
				while (line != null && !lineParse(record, line)) {
					String newLine = reader.readLine();
					line = line + "\r\n" + newLine;
					record.clear();
				}
				record.clear();
				line = reader.readLine();
				// Run twice to get rid of the status line 
	            /*
					while (line != null && !lineParse(record, line)) {
		                String newLine = reader.readLine();
		                line = line + "\r\n" + newLine;
		                record.clear();
		             }
		             record.clear();
		             line = reader.readLine();
	             */
	             //end run twice 
	             
				HashMap columns = new HashMap();
	
				while (line != null && !this.isKilled()) {
					record = new ArrayList();
					if (!lineParse(record, line)) {
						String newLine = reader.readLine();
						if (newLine == null) {
							break;
						}
						line = line + "\r\n" + newLine;
					}
					else {
						columns.clear();
						for (int i=0; i < record.size() && i < columnHeadings.size(); i++) {
							columns.put(columnHeadings.get(i), StringUtil.trim((String)record.get(i)));
					}
					numRecords++;
	
					int temp = numSuccess;
	
					insertRecord(importID, bean, columns);
						
					if (numSuccess == temp) {
						importLog.append("\r\n\r\nError occurred at record ");
						importLog.append(numRecords);
						importLog.append(".\r\n\r\n");
						importLog.append(bean.getError()); 
						importLog.append(".\r\n\r\n");
						
						numErrors++; 
					}
					line = reader.readLine();
				}
			}
		}
		catch (Exception e) { 
			importLog.append("\r\nProcessing record ");
			importLog.append(numRecords);
			importLog.append(".\r\n");
			importLog.append(ActionBean.writeStackTraceToString(e));
		}
		finally {
			try { reader.close(); }
			catch (Exception ex) {}
		}
		}
		createImportRecord(importID);
	
		return importID;
	}
	
	private void detectConflicts(String importID) { 
		
		/*
		 * This will detect conflicts in the DMS data vs Contacts Based on :
		 * FirstName, LastName, Phone
		 * FirstName, LastName, Email
		 * FirstName, LastName, Address
		 * TODO DMSID - which we don't yet have.. 
		 */
		
		RowSetBean dmsData = new RowSetBean(); 
		dmsData.setConnection(getConnection()); 
		dmsData.setViewSpec(vspec); 
		dmsData.setColumn("ImportID",importID); 
		dmsData.generateSQLStatement(); 

		dmsData.getResults();
		
		RowBean check = new RowBean(); 
		check.setViewSpec("ContactListView"); 
		check.setConnection(getConnection()); 
		
		RowBean update = new RowBean(); 
		update.setViewSpec(vspec); 
		
		boolean checkPhone;
		boolean checkEmail; 
		boolean checkAddress; 
		boolean matched; 
		
		while (dmsData.getNext() && !this.isKilled()) { 
			matched = false; 
			checkPhone = dmsData.getString("Phone").length()>0;
			checkEmail = dmsData.getString("Email").length()>0; 
			checkAddress = dmsData.getString("Street").length()>0 && dmsData.getString("City").length()>0; 
			
			if (checkPhone) { 
				check.clear(); 
				check.setColumn("FirstName",dmsData.getColumn("FirstName"));
				check.setColumn("LastName",dmsData.getColumn("LastName")); 
				check.setColumn("Phone",dmsData.getColumn("Phone")); 
				check.retrieveData(); 
				
				if (check.getColumn("ContactID").length()>0) { 
					matched = updateMatch(dmsData.getString("ImportDMSID"), check.getColumn("ContactID")); 
					matchedContacts.append("http://www.easybeinggreen.net/crm/contactview.jsp?-action=select&ContactID="); 
					matchedContacts.append(check.getColumn("ContactID")).append(" ");
					matchedContacts.append(check.getColumn("FirstName")).append(" ");
					matchedContacts.append(check.getColumn("LastName")).append("</a>\r\n");					
				} 
			} 
			if (checkEmail && !matched) { 
				check.clear(); 
				check.setColumn("FirstName",dmsData.getColumn("FirstName"));
				check.setColumn("LastName",dmsData.getColumn("LastName")); 
				check.setColumn("Email",dmsData.getColumn("Email")); 
				check.retrieveData(); 
				
				if (check.getColumn("ContactID").length()>0) { 
					matched = updateMatch(dmsData.getString("ImportDMSID"), check.getColumn("ContactID"));
					matchedContacts.append("http://www.easybeinggreen.net/crm/contactview.jsp?-action=select&ContactID=");
					matchedContacts.append(check.getColumn("ContactID")).append(" ");
					matchedContacts.append(check.getColumn("FirstName")).append(" ");
					matchedContacts.append(check.getColumn("LastName")).append("\r\n");	
				} 
			} 
			if (checkAddress && !matched) { 
				check.clear(); 
				check.setColumn("FirstName",dmsData.getColumn("FirstName"));
				check.setColumn("LastName",dmsData.getColumn("LastName")); 
				check.setColumn("Street",dmsData.getColumn("Street"));
				check.setColumn("City",dmsData.getColumn("City"));
				check.retrieveData(); 
				
				if (check.getColumn("ContactID").length()>0) { 
					matched = updateMatch(dmsData.getString("ImportDMSID"), check.getColumn("ContactID"));
					matchedContacts.append("http://www.easybeinggreen.net/crm/contactview.jsp?-action=select&ContactID=");
					matchedContacts.append(check.getColumn("ContactID")).append(" ");
					matchedContacts.append(check.getColumn("FirstName")).append(" ");
					matchedContacts.append(check.getColumn("LastName")).append("\r\n");	
				} 
			} 
			numProcessed++;
		}
		dmsData.close(); 
	}
	
	private boolean updateMatch(String ImportDMSID, String contactID) { 
		
		updateBean.clear(); 
		updateBean.setViewSpec(vspec);
		updateBean.setConnection(getConnection()); 
		updateBean.setColumn("ImportDMSID", ImportDMSID); 
		updateBean.setColumn("ContactID",contactID); 
		updateBean.setColumn("ImportFlg","M"); //M For Match
		updateBean.setColumn("Conflicts","Match"); 
		updateBean.setAction(ActionBean.UPDATE); 
		updateBean.doAction(); 
	
		if (updateBean.getError().length()>0) { 
			errors.append("Error with Match update").append(updateBean.getError()); 
			numRecordErrors++; 
			return false; 
		} else {
			return true; 
		}
	}
	
	private void insertRecord(String importID, RowBean bean, HashMap data) { 
		
		bean.clear(); 
		
		bean.setColumn("ImportID",importID); 
		bean.setColumn("ImportDMSID", KeyMaker.generate());
		bean.setColumn("ContactID", KeyMaker.generate());
		bean.setColumn("OrderID", KeyMaker.generate());
		
		//loop through the data record, and put the data where it needs to go.
		
		Iterator iterator = data.keySet().iterator();
		String key = ActionBean._blank; 
		
		while (iterator.hasNext()) { 
			key = (String) iterator.next(); 
			bean.setColumn(key,(String)data.get(key)); 
		}
		
		bean.setColumn("Street",(String)data.get("Address")); 
		bean.setColumn("City",(String)data.get("Suburb"));
		String name = (String)data.get("Name"); 
		if (name.indexOf(",")>0) { 
			bean.setColumn("FirstName",name.substring(0,name.indexOf(","))); 
			bean.setColumn("LastName",StringUtil.trim(name.substring(name.indexOf(",")+1)));
		}else { 
			bean.setColumn("LastName",name); 
		}
		bean.setColumn("FullName",name);
		
		bean.setAction(ActionBean.INSERT); 
		bean.doAction(); 
		
		if (bean.getError().length()==0) {
		    numSuccess++; 
		 }
		 else {
		    numRecordErrors++;
		   // importProcess.setLastError(bean.getError());
		    errors.append(bean.getError());
		 }

	}
	
	public void determineColumnHeadings() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(importFile));

			ArrayList firstRecord = new ArrayList();
			String line = reader.readLine();

			while (line != null && !lineParse(firstRecord, line)) {
				String newLine = reader.readLine();
				line = line + "\r\n" + newLine;
				firstRecord.clear();
			}
			columnHeadings = firstRecord;
		}
		catch (IOException e) { 
		}
		finally {
			try { reader.close(); }
			catch (Exception ex) {}
		}
	}
	
	public File getFile(HttpServletRequest request) {
		UploadWrapper multi = null;
		if (!(request instanceof UploadWrapper)) {
			if(request.getAttribute("MultipartWrapper")!=null) {
				multi = (UploadWrapper)request.getAttribute("MultipartWrapper");
			}
		}
		else {
			multi = (UploadWrapper) request;
		}

		if(multi!=null) {
			Enumeration files = multi.getFileNames();
			while (files.hasMoreElements()) {
				String name = (String)files.nextElement();
				File f = multi.getFile(name);
				if (f != null) {
					File folder = f.getParentFile().getParentFile();
					File processfolder = new File(folder,"imported");
					if (!processfolder.exists()) {
						processfolder.mkdir();
					}
					File dest = new File(processfolder,f.getName()+".imported");
					if (dest.exists()) {
						dest.delete();
					}
					f.renameTo(dest);
				return dest;
				}
			}
		}
		return null;
	}
	
	public boolean lineParse(ArrayList record, String line) {
		char[] chars = line.toCharArray();

		boolean inQuote = false;
		int startIndex = 0;
		for (int i=0; i < chars.length; i++) {
			if (chars[i] == '"') {
				inQuote = !inQuote;
			}
			else if (!inQuote && chars[i] == ',') {
				record.add(convertFragment(line.substring(startIndex,i)));
				startIndex = i+1;
			}
		}
		if (!inQuote) {
			if (startIndex == chars.length) {
				record.add("");
			}
			else if (startIndex < chars.length) {
				record.add(convertFragment(line.substring(startIndex)));
			}
		}
		return !inQuote;
	}
	
	public String convertFragment(String fragment) {
		if (fragment.startsWith("\"") && fragment.endsWith("\"")) {
			fragment = fragment.substring(1,fragment.length()-1);
		}
		fragment = StringUtil.replace(fragment, "\"\"","\"");
		return fragment;
	}
	
	public String getStatusMailBody() {
		return status.toString();
	}

	public String getStatusMailSubject() {
		return(context.getServletContextName() + " Runway offline process - " + tspec.getName() + " Data Import");
	}

	public String getStatusMailRecipient() {
		return notificationEmail; 
	}

	public int getProcessSize() {
		return numProcessed;
	}

	public int getProcessedCount() {
		return numSuccess;
	}

	public int getErrorCount() {
		return numErrors;
	}
}
