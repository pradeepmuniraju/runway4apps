package com.sok.clients.bp;

import com.sok.framework.GenRow;

import java.security.SecureRandom;
import java.util.ArrayList;

public class WildBeanDraw {
	
	private final ArrayList<WildBeanEntrant> entrants = new ArrayList<WildBeanEntrant>();  
	private final SecureRandom sr = new SecureRandom();		
	
	public int getPoolSize() { 
		return entrants.size(); 
	}
	
	public void setEntrant(String id, float score) { 	
		entrants.add(new WildBeanEntrant(id, score));
	}
	
	public void swapEntrants(int i, int j) { 
		WildBeanEntrant iE = entrants.get(i);
		WildBeanEntrant jE = entrants.get(j);
		
		float iS = iE.getScore(); 
		iE.setScore(jE.getScore()); 
		jE.setScore(iS); 
	}
	
	public WildBeanEntrant getEntrant(int index) { 
		return entrants.get(index); 
	}
	
	public SecureRandom getRNG() { 
		return sr; 
	}
	
	public static void main(String args[]) {  
		long total = 0; 
		final int tests = 100; 
		
		for (int i=0; i<tests; i++) { 
			int c = 0; 
			
			while(!testDraw(1000).equals("999")) { 
				c++; 
				
				if(c % 100 == 0) { 
					System.out.print(c + ","); 
				}
			}
			total += c; 
			System.out.println("Result found in "+c+" tries");
		} 
		System.out.println("Average Times Taken : "+(total / tests));
	}
	
	private static String testDraw(int entrants) { 
		
		WildBeanDraw wbd = new WildBeanDraw(); 
	 	
		for(int i=0; i<entrants; i++) { 
			wbd.setEntrant(String.valueOf(i), wbd.getRNG().nextFloat()); 
		}
		
		return pickWinner(wbd);
	}
	
	private static String pickWinner(WildBeanDraw wbd) { 
		
		randomiseEntrants(wbd); 
		
		float f = 0.0f; 
		String winner = null;
		
		for(int i=0; i<wbd.getPoolSize(); i++) { 
			WildBeanEntrant wbe = wbd.getEntrant(i); 
			 
			if(wbe.getScore() > f) { 
				winner = wbe.getId(); 
				f = wbe.getScore(); 
			}
		}
		
		return winner; 
	}

	public static String runwayIntegration(String search) { 
		
		WildBeanDraw wbd = new WildBeanDraw(); 
		
		GenRow x = new GenRow(); 
		x.setViewSpec("ContactListView"); 
		x.parseRequest(search); 
		x.getResults(); 
		
		while(x.getNext()) { 
			wbd.setEntrant(x.getData("ContactID"), wbd.getRNG().nextFloat()); 
		}
		x.close(); 
		
		return pickWinner(wbd);
	}
	
	private static void randomiseEntrants(WildBeanDraw wbd) { 
		
		for(int i=0; i<wbd.getPoolSize(); i++) { 
			int r = (int)( wbd.getRNG().nextFloat() * ( wbd.getPoolSize() ) );
			wbd.swapEntrants(i,r); 
		}
	}
	
	private class WildBeanEntrant { 
		
		String id; 
		float score; 
		
		private WildBeanEntrant(String id, float score) { 
			this.id = id; 
			this.score = score; 
		}
		
		private void setScore(float score) { 
			this.score = score; 
		}
		
		private String getId() { 
			return id; 
		}
		
		private float getScore() { 
			return score; 
		}
		
	}
}
