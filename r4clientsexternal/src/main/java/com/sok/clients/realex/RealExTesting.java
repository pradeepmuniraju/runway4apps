package com.sok.clients.realex;


import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.runway.crm.Contact;
import com.sok.runway.crm.Order;

/**
 * Contains Testing Classes and Previous Versions
 * Probably should have done version control instead.. 
 * @author Michael Dekmetzian 13/12/2007 - 14/12/2007
 */
//public static final String MD5HASH = "md5hash";
//public static final String ENCODING_TYPE = "iso-8859-1"; //latin type encoding

public class RealExTesting extends RealEx_eSay_RealEFT {

	private static final String test = "simon" + " hi";
	
	public static void main(String[] args) {
		
		System.out.println(test);
		
		
		
		
		
		
		
		// TODO Auto-generated method stub
		//System.out.print(genXML());
		//System.out.print(genStatic()); 
	//	genHash("20010403123245","thestore","ORD453-11","EUR","29900","5105105105105100"); 
		//System.out.println(generateRealexHash("20010403123245","ORD453-11","EUR","29900","5105105105105100")); 
		
		/*
		try {
			RealExConnection rec = new RealExConnection();
			String xml = super.genXML(RealExTesting.createOrder()); 
			System.out.println(xml); 
			System.out.println(""); 
			String resp = rec.getRealAUTHResponseXML(xml); 
			System.out.println(resp);
			Map <String, String> rm = RealEx.parseResponse(resp);
			System.out.println(rm.toString()); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.print(e.getMessage());
		} */ 
					/*
					String cid = "05191I9X3M318X4G547O1P7N6D1J"; //Keymaker.generate(); 
					String pid = "0A152Q024Z1B5A856T88623Z0H18"; //Keymaker.generate();
					
					Contact c = createContact(cid); 
					Map<String, String> resp = new HashMap(); 
					*/
		//resp = RealEx_eSay_RealEFT.sendAddPayerRequest(c);
		//System.out.println(resp); 
		//c.setField("AccountNo",resp.get("pasref")); 
		//c.update(); 
		
					//GenRow payment = createPayment(c, pid);
		/*
		resp = RealEx_eSay_RealEFT.sendModifyCardRequest(payment, false); 
		System.out.println(resp); 
		payment.setParameter("CardExpiry","10/10"); 
		resp = RealEx_eSay_RealEFT.sendModifyCardRequest(payment, true); 
		System.out.println(resp); 
		*/
					//Order order = createOrder(); 
		
					//resp = RealEx_eSay_RealEFT.makePayment(c, order, payment);
		
		//System.out.println(resp); 
		//System.out.println("TRANSACTIONID: "+ resp.get("pasref"));
		//System.out.println("RECEIPTNO: "+ resp.get("authcode"));
		
					/*
					payment.setParameter("OrderID", order.getField("OrderID"));
					payment.setParameter("TransactionID", resp.get("pasref")); // pasref
					payment.setParameter("ReceiptNo", resp.get("authcode")); // authcode
					
					resp = new HashMap();
					*/
		//resp = RealEx_eSay_RealEFT.makeRefundAuth(payment, 10);
		
					//System.out.println(resp); 
		
		//System.out.print(RealEx_eSay_RealAUTH.sendRequest(RealExTesting.createOrder())); 
	}
	
	public static String genStatic() { 
		
		StringBuilder ln = new StringBuilder(); 
		String[] fields = {"merchantid","account","orderid","amount","currency","card","number","expdate","chname","type","issueno","cvn","number","presind","card","autosettle","flag","comments","commnt","id","tssinfo","custnum","prodid","varref","custipaddress","address","type","code","country","narrative","establishmentname","establishmentcity","establishmentstate","chargedescription","sha1hash","md5hash"}; 
		
		for (String f:fields) { 
			ln.append("public static final String ").append(f.toUpperCase()).append(" = \"").append(f).append("\";\r\n"); 
		}
		
		return ln.toString(); 
	}

	
	public static String genHash(String ts, String mid, String ord, String cur, String amt, String card) { 
		
		StringBuilder h = new StringBuilder(); 
		
		h.append(ts).append(PERIOD);
		h.append(mid).append(PERIOD);
		h.append(ord).append(PERIOD);
		h.append(amt).append(PERIOD);
		h.append(cur).append(PERIOD);
		h.append(card);
		 
		System.out.println("First Concat the String");
		System.out.println(h.toString()); 
		System.out.println(""); 
		System.out.println("Now get the hash"); 
		
		String hash = "c5d02900c2ed43e0015d5e6792e2071a7b20afd5";
		
		String calchash = ""; 
		
		try {
			calchash = hashString(h.toString(), "SHA-1","UTF-8");
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} catch (UnsupportedEncodingException ue ) {
			
			System.out.println(ue.getMessage());
		}
		System.out.println("SHA-1 hash: "+calchash); 
		System.out.println("Is as expected? "+hash.equals(calchash)); 
		
		h.delete(0,h.length()); 
		h.append(calchash).append(PERIOD).append("mysecret"); //replace with eSay shared secret
		
		String recalchash = ""; 
		
		try {
			recalchash = hashString(h.toString(), "SHA-1","UTF-8"); 
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} catch (UnsupportedEncodingException ue ) {
			
			System.out.println(ue.getMessage());
		}
		String rehash = "9af7064afd307c9f988e8dfc271f9257f1fc02f6"; 
		
		System.out.println("SHA-1 hash: "+recalchash); 
		System.out.println("Is as expected? "+rehash.equals(recalchash));
		
		return recalchash; 
		
	}
	
	/**
	 * Parse the xml response given by RealEx into a HashMap that we can access
	 * @param xml
	 * @return HashMap - will have '-hashfailed = true' if the hashes do not match
	 * @throws DocumentException
	 */
	public static Map<String, String> parseResponse(String xml) throws DocumentException { 
		
		Document document = DocumentHelper.parseText(xml);
		Map<String, String> xmlResponses = new HashMap<String,String>(); 
		
		Element root = document.getRootElement(); 
		
		for ( int i = 0, size2 = root.attributeCount(); i < size2; i++ ) {
			Attribute childAttribute = root.attribute(i);
			xmlResponses.put(root.getName()+"_"+childAttribute.getName(), childAttribute.getText());					
		}		
		
		for ( int i = 0, size = root.nodeCount(); i < size; i++ ) {

			Node node = root.node(i); 
			
			if(node instanceof Element) { 
				Element thisNode = (Element)node; 
 
				xmlResponses.put(node.getName(), node.getText());
				

				
				for ( int i2 = 0, size2 = thisNode.nodeCount(); i2 < size2; i2++ ) {
					Node childNode = thisNode.node(i2); 
					if(childNode instanceof Element) { 
						Element thisChildNode = (Element)childNode; 
						xmlResponses.put(node.getName()+"_"+thisChildNode.getName(), thisChildNode.getText());
					} 
				} 
			} 
		} 
		
		StringBuffer h = new StringBuffer(); 
		h.append(xmlResponses.get("response_timestamp"));
		h.append(PERIOD).append(MERCHANTID_VALUE);
		h.append(PERIOD).append(xmlResponses.get("orderid"));
		h.append(PERIOD).append(xmlResponses.get("result"));
		h.append(PERIOD).append(xmlResponses.get("message"));
		h.append(PERIOD).append(xmlResponses.get("pasref"));
		h.append(PERIOD).append(xmlResponses.get("authcode"));
		
		String hash = generateRealexHash(h); 
			
		System.out.println(xmlResponses.toString());
		
		System.out.println(xmlResponses.get("sha1hash")); 
		System.out.println(hash);
		
		System.out.println("Hash codes equal "+ hash.equals(xmlResponses.get("sha1hash")));
		
		if(hash.equals(xmlResponses.get("sha1hash"))) { 
			xmlResponses.put("-hashfailed","true");
		}
		
		
		return xmlResponses;

	}
	
	
	public static String genXML() { 
		
		StringBuffer x = new StringBuffer();
		
		Document document = DocumentHelper.createDocument();
		Element root = document.addElement(REQUEST);
		Element sub; 
		Element subsub; 
		
		String ts = getTimestamp(); 
		String oi = "XXX"; 
		String cur = "GBP"; 
		String amt = "2100"; 
		String crd = "490303400005718902"; 
		
		String transactionType="auth"; 
		
		root.addAttribute(TIMESTAMP, ts);
		root.addAttribute(TYPE,transactionType); 
		root.addElement(MERCHANTID).addText(MERCHANTID_VALUE); 
        //root.addElement(ACCOUNT).addText("account to use"); // non mandatory
        root.addElement(ORDERID).addText(oi);
        
    	if (transactionType.equals("auth") ||
    			transactionType.equals("refund") ||
    			transactionType.equals("tss") ||
    			transactionType.equals("offline")) {
        
	        sub = root.addElement(AMOUNT); 
	        sub.addAttribute(CURRENCY,cur); 
	        sub.addText(amt); 
	        
	        sub = root.addElement(CARD); 
	        sub.addElement(NUMBER).addText(crd); 
	        sub.addElement(EXPDATE).addText("0403");
	        sub.addElement(CHNAME).addText("John Doe");
	        sub.addElement(TYPE).addText("MC"); //card type
	        sub.addElement(ISSUENO).addText("");
	        
	        subsub = sub.addElement(CVN); 
	        subsub.addElement(NUMBER).addText("453");
	        subsub.addElement(PRESIND).addText("1"); 
	        
	        root.addElement(AUTOSETTLE).addAttribute(FLAG,"1");
        
    	}
    	
    	if (transactionType.equals("void")) {
    		root.addElement(PASREF).addText("pasreftext"); 
    	}
    	
    	if (transactionType.equals("offline")) {
    		root.addElement(AUTHCODE).addText("authorisationcode"); 
    	}
        
        sub = root.addElement(COMMENTS); 
        sub.addElement(COMMENT).addAttribute(ID,"1").addText("a comment");
        sub.addElement(COMMENT).addAttribute(ID,"2").addText("another comment");
        
        sub = root.addElement(TSSINFO);
        
        sub.addElement(CUSTNUM).addText("customer number");
        sub.addElement(PRODID).addText("product id");
        sub.addElement(VARREF).addText("variable reference");
        sub.addElement(CUSTIPADDRESS).addText("www.xxx.yyy.zzz");
        
        subsub = sub.addElement(ADDRESS); 
        subsub.addAttribute(TYPE, "billing"); 
        subsub.addElement(CODE).addText("zip/postal code");
        subsub.addElement(COUNTRY).addText("country");
        
        subsub = sub.addElement(ADDRESS); 
        subsub.addAttribute(TYPE, "shipping"); 
        subsub.addElement(CODE).addText("zip/postal code");
        subsub.addElement(COUNTRY).addText("country");
        
        subsub = sub.addElement(NARRATIVE); 
        subsub.addElement(ESTABLISHMENTNAME).addText("Merchant Name");
        subsub.addElement(ESTABLISHMENTCITY).addText("CITY"); 
        subsub.addElement(ESTABLISHMENTSTATE).addText("CC"); 
        subsub.addElement(CHARGEDESCRIPTION).addText("DESCRIPTION"); 
        
    	StringBuffer h = new StringBuffer(); 
		h.append(ts).append(PERIOD);
		h.append(MERCHANTID_VALUE).append(PERIOD);
		h.append(oi).append(PERIOD);
		h.append(amt).append(PERIOD);
		h.append(cur).append(PERIOD);
		h.append(crd);
        
        root.addElement(SHA1HASH).addText(generateRealexHash(h)); 
        //root.addElement(MD5HASH).addText("XXXX");
        
        try { 
		    OutputFormat format = OutputFormat.createPrettyPrint();
		    StringWriter sw = new StringWriter(); 
		    XMLWriter writer = new XMLWriter( sw, format );
		    writer.write( document );
		    sw.flush(); 
		    x.append(sw.toString()); 
		    sw.close(); 
	    	
    	}catch (IOException e) { 
    		x.append("FAILED"); 
    	}
		
		return x.toString(); 
	}
	
	/**
     * Produces a string representing the current time, for use with RealEx payments. 
     * @return current timestamp in format yyMMddhhmmss
     */
    public static String getTimestamp() {
		String timestamp;
		Calendar now = Calendar.getInstance();
		
		timestamp = "" + now.get(Calendar.YEAR);

		if ((now.get(Calendar.MONTH) + 1) < 10) {
			timestamp += "0" + (now.get(Calendar.MONTH) + 1);
		} else {
			timestamp += "" + (now.get(Calendar.MONTH) + 1);
		}

		if (now.get(Calendar.DAY_OF_MONTH) < 10) {
			timestamp += "0" + now.get(Calendar.DAY_OF_MONTH);
		} else {
			timestamp += "" + now.get(Calendar.DAY_OF_MONTH);
		}

		if (now.get(Calendar.HOUR_OF_DAY) < 10) {
			timestamp += "0" + now.get(Calendar.HOUR_OF_DAY);
		} else {
			timestamp += "" + now.get(Calendar.HOUR_OF_DAY);
		}

		if (now.get(Calendar.MINUTE) < 10) {
			timestamp += "0" + now.get(Calendar.MINUTE);
		} else {
			timestamp += "" + now.get(Calendar.MINUTE);
		}

		if (now.get(Calendar.SECOND) < 10) {
			timestamp += "0" + now.get(Calendar.SECOND);
		} else {
			timestamp += "" + now.get(Calendar.SECOND);
		}
			
		return timestamp;
	}
    
    /**
     * Produces a string representing the current time, for use with RealEx payments. 
     * Adapted from the version provided by realex.
     * @return current timestamp in format yyMMddhhmmss
     */
    public static String getTimestamp2() {
		StringBuilder timestamp;
		Calendar now = Calendar.getInstance();
		timestamp = new StringBuilder().append(now.get(Calendar.YEAR)); //doesn't like int as constructor 
		addValue(timestamp, (now.get(Calendar.MONTH)+1));
		addValue(timestamp, now.get(Calendar.DAY_OF_MONTH));
		addValue(timestamp, now.get(Calendar.HOUR_OF_DAY));
		addValue(timestamp, now.get(Calendar.MINUTE));
		addValue(timestamp, now.get(Calendar.SECOND));

		return timestamp.toString();
	}
    
	private static final void addValue(StringBuilder s, int i) { 
		if(i < 10) { 
			s.append("0"); 
		} 
		s.append(i); 
	}
	
    /** 
     * creates an order with test data. 
     */
    public static Order createOrder() {  
    
    	Order order = new Order((java.sql.Connection)null); 
    	order.setField("OrderID",KeyMaker.generate());System.out.println("ORDERID: "+order.getField("OrderID"));
    	order.setField("Name","eSay Web Order");       
    	order.setField("TotalCost",20.34d); 
    	order.setField("CreditName","Joseph H Bloggs Esq"); 
    	order.setField("CreditType","Visa"); 
    	order.setField("CreditExpire","05/09"); 
    	order.setField("CreditNum","4263971921001307"); //success card  
    	order.setField("CreditCVV","000");  
    	order.setField("ContactName","Joe Bloggs");
    	order.setField("Currency","GBP");
    	
    	order.setField("-transactionType",AUTH);
    	order.setField("-autoSettle","0");
    	order.setField("-comment1","Comment 1");
    	order.setField("-comment2","Comment 2");
    	
    	return order; 
    } 
    
    public static Contact createContact(String cid) { 
    	Contact contact = new Contact((java.sql.Connection)null); 
    	contact.setField("ContactID",cid);    
    	contact.setField("FirstName","Billy");
    	contact.setField("LastName","Bloggs");
    	
    	return contact; 
    }
    
    public static GenRow createPayment(Contact c, String pid) { 
    	
    	GenRow payment = new GenRow(); 
    	payment.setParameter("ContactID",c.getPrimaryKey());
    	payment.setParameter("PaymentMethodID",pid);
    	payment.setParameter("CardNumber","4263971921001307");
		payment.setParameter("CardName",c.getField("FirstName")+" "+c.getField("LastName"));
		payment.setParameter("CardType","VISA");
		payment.setParameter("CardExpiry","05/09");
		
		return payment; 		
    }
    
    public static GenRow createCredit(Contact c) { 
    	
    	GenRow cred = new GenRow(); 
    	cred.setParameter("ReferralCreditID",KeyMaker.generate()); 
    	cred.setParameter("Amount", "50.00"); 
    	
    	return cred; 
    }
	
}
