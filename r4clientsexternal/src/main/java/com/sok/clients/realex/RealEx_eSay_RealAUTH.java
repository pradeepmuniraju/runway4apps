package com.sok.clients.realex;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.sok.framework.GenRow;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.Order;

public class RealEx_eSay_RealAUTH extends RealEx_eSay {
	
	
	/**
	 * submit 3DSecure-verify-enrolled request
	 * @param orderBean to grab the OrderID
	 * @param paymentMethod to grab card details
	 * @return responseMap
	 */
	public static Map<String,String> submit3dsverifyenrolled(GenRow orderBean, GenRow paymentMethod) 
	{	
		Map <String, String> responseMap; 
		
		try 
		{
			RealExConnection rec = new RealExConnection();
			
			String 	req 	= gen3dsverifyenrolledXML(orderBean, paymentMethod); 
			String 	resp 	= rec.getRealAUTHResponseXML(req); // changed from getRealEFTResponseXML
			
			responseMap = parseResponse(resp);
			responseMap.put("REQUEST",req); 
			
			return responseMap;  
		}
		catch (Exception e) 
		{
			System.out.print(e.getMessage());
			responseMap = new HashMap<String,String>();
			responseMap.put("ERROR",e.getMessage()); 
		}
		return responseMap;
	}
	
	/**
	 * generate XML for 3D-verify-enrolled request
	 * @param orderBean
	 * @param paymentMethod
	 * @return the required XML doc
	 */
	private static String gen3dsverifyenrolledXML(GenRow orderBean, GenRow paymentMethod) 
	{
		Document document 	= DocumentHelper.createDocument();
		Element 	root 			= document.addElement(REQUEST);
		Element 	sub; 
		Element 	subsub;

		String timeStamp = getTimestamp(); 
		root.addAttribute(TIMESTAMP, timeStamp);
		root.addAttribute(TYPE, _3DSVERIFYENROLLED);
		root.addElement(MERCHANTID).addText(MERCHANTID_VALUE);
		root.addElement(ORDERID).addText(orderBean.getColumn("OrderID"));

		sub = root.addElement(AMOUNT);
		sub.addAttribute(CURRENCY, GBP);
		DecimalFormat wn = new DecimalFormat("#");
		String amount = wn.format(orderBean.getDouble("TotalCost") * 100); com.sok.Debugger.getDebugger().debug("<p>refund.amount: "+amount);
		sub.addText(amount);
		
		sub = root.addElement(CARD); 
      sub.addElement(NUMBER).addText(paymentMethod.getColumn("CardNumber"));
      sub.addElement(EXPDATE).addText(StringUtil.replace(paymentMethod.getColumn("CardExpiry"),"/",""));
	   sub.addElement(TYPE).addText(paymentMethod.getColumn("CardType"));  
	   sub.addElement(CHNAME).addText(paymentMethod.getColumn("CardName"));

	   StringBuffer h = new StringBuffer(); 
		h.append(timeStamp).append(PERIOD);
		h.append(MERCHANTID_VALUE).append(PERIOD);
		h.append(orderBean.getColumn("OrderID")).append(PERIOD);
		h.append(amount).append(PERIOD);
		h.append(GBP).append(PERIOD);
		h.append(paymentMethod.getColumn("CardNumber"));
		
		root.addElement(SHA1HASH).addText(generateRealexHash(h, false, ALGORITHM));
		
		return getXmlFromDocument(document); 
	}
	
	public static Map<String,String> makeCredit(GenRow orderBean, GenRow paymentMethod) 
	{	
		Map <String, String> responseMap; 
		
		try 
		{
			RealExConnection rec = new RealExConnection();
			
			String 	req 	= genCreditXML(orderBean, paymentMethod); 
			String 	resp 	= rec.getRealAUTHResponseXML(req);
			
			responseMap = parseResponse(resp);
			responseMap.put("REQUEST",req); 
			
			return responseMap;  
		} 
		catch (Exception e) 
		{
			System.out.print(e.getMessage());
			responseMap = new HashMap<String,String>();
			responseMap.put("ERROR",e.getMessage()); 
		}
		return responseMap;
	}
	
	/**
	 * 
	 */
	private static String genCreditXML(GenRow orderBean, GenRow paymentMethod) 
	{
		Document document 	= DocumentHelper.createDocument();
		Element 	root 			= document.addElement(REQUEST);
		Element 	sub; 
		Element 	subsub;

		String timeStamp = getTimestamp(); 
		root.addAttribute(TIMESTAMP, timeStamp);
		root.addAttribute(TYPE, CREDIT);
		root.addElement(MERCHANTID).addText(MERCHANTID_VALUE);
		root.addElement(ACCOUNT).addText(CARDTYPE_AMEX.equals(paymentMethod.getColumn("CardType")) ? ACCOUNT_AMEXUK : ACCOUNT_INTERNET);

		root.addElement(ORDERID).addText(orderBean.getColumn("OrderID"));

		sub = root.addElement(AMOUNT);
		sub.addAttribute(CURRENCY, GBP);
		
		DecimalFormat wn = new DecimalFormat("#");
		String amount = wn.format(orderBean.getDouble("TotalCost") * 100); com.sok.Debugger.getDebugger().debug("<p>refund.amount: "+amount);
		sub.addText(amount);
		
		root.addElement(REFUNDHASH).addText(generateRefundHash(new StringBuffer(REFUND_PASSWORD)));
		
		sub = root.addElement(CARD); 
      sub.addElement(NUMBER).addText(paymentMethod.getColumn("CardNumber"));
      sub.addElement(EXPDATE).addText(StringUtil.replace(paymentMethod.getColumn("CardExpiry"),"/",""));
	   sub.addElement(CHNAME).addText(paymentMethod.getColumn("CardName"));
	   sub.addElement(TYPE).addText(paymentMethod.getColumn("CardType"));  

	   if(paymentMethod.getColumn("IssueNo") != null && paymentMethod.getColumn("IssueNo").length() > 0)
	   {
	   	sub.addElement(TYPE).addText(paymentMethod.getParameter("IssueNo"));
	   }
	   
	   root.addElement(AUTOSETTLE).addAttribute(FLAG, "1");

	   StringBuffer h = new StringBuffer(); 
		h.append(timeStamp).append(PERIOD);
		h.append(MERCHANTID_VALUE).append(PERIOD);
		h.append(orderBean.getColumn("OrderID")).append(PERIOD);
		h.append(amount).append(PERIOD);
		h.append(GBP).append(PERIOD);
		h.append(paymentMethod.getColumn("CardNumber"));
		
		root.addElement(SHA1HASH).addText(generateRealexHash(h, false, ALGORITHM));
		
	   /* 
	   
	   not sure what these are for:
		
		if(amount.indexOf(PERIOD) > -1)
		{ 
			amount = amount.substring(0,amount.indexOf(PERIOD)); 
		}
			   
		com.sok.Debugger.getDebugger().debug("<p>REFUND-SHA-1: "+h.toString());
		root.addElement(REFUNDHASH).addText(generateRealexHash(h,true,ALGORITHM));

		root.addElement(MD5HASH).addText(generateRealexHash(h,true,ALGORITHM_MD5));
		
		*/
		
		return getXmlFromDocument(document); 
	}

	/**
	 * Provides single convenience method to send the request based on a RunwayEntity Order
	 * @author Michael Dekmetzian 14/12/2007
	 * @param order representing the sale. 
	 * @return Response HashMap, contains Exception in ERROR field if one occurs. 
	 */
	public static Map<String,String> sendRequest(Order order) {
		
		Map <String, String> responseMap; 
		try {
			RealExConnection rec = new RealExConnection();
			String resp = rec.getRealAUTHResponseXML(genXML(order)); 
			responseMap = parseResponse(resp);
			return responseMap;  
		} catch (Exception e) {
			System.out.print(e.getMessage());
			responseMap = new HashMap<String,String>();
			responseMap.put("ERROR",e.getMessage()); 
		} 
		return responseMap;  
	}
	
	@SuppressWarnings("unused")
	private static String genRefundXML(GenRow payment, double value) { 
		
		Document document = DocumentHelper.createDocument();
		
		Element root = document.addElement(REQUEST);
		Element sub; 
		Element subsub;
		
		String timeStamp = getTimestamp(); 
		String orderID = payment.getData("OrderID");
		String currency = GBP; 
		
		root.addAttribute(TIMESTAMP, timeStamp);
		root.addAttribute(TYPE,REBATE); 
		root.addElement(MERCHANTID).addText(MERCHANTID_VALUE); 
	    //root.addElement(ACCOUNT).addText("account to use"); // non mandatory
	    root.addElement(ORDERID).addText(orderID);
		sub = root.addElement(AMOUNT); 
		sub.addAttribute(CURRENCY, currency); 
		
		String amount = BLANK + (value * 100);
		if(amount.indexOf(PERIOD) > -1) { 
			amount = amount.substring(0,amount.indexOf(PERIOD)); 
		}
		sub.addText(amount);
		
		root.addElement(REFUNDHASH).addText("XXX"); 
		root.addElement(AUTOSETTLE).addAttribute(FLAG, "1");
		
		StringBuffer h = new StringBuffer(); 
		h.append(timeStamp).append(PERIOD);
		h.append(MERCHANTID_VALUE).append(PERIOD);
		h.append(orderID).append(PERIOD);
		h.append(amount).append(PERIOD);
		h.append(currency).append(PERIOD);
		h.append(BLANK);
	    
	    root.addElement(SHA1HASH).addText(generateRealexHash(h)); 
		
		return getXmlFromDocument(document); 
	}

	
	/**
	 * Generates the request xml to send to RealEx based on a given RunwayEntity Order. 
	 * @param order
	 * @return xml for the request.
	 */
	protected static String genXML(Order order) { 
		
		Document document = DocumentHelper.createDocument();
		Element root = document.addElement(REQUEST);
		Element sub;
		Element subsub; 
		
		String timeStamp = getTimestamp(); 
		String orderID = order.getField("OrderID"); 
		String currency = GBP; 
		
		double d = Double.parseDouble(order.getField("TotalCost")); 
		
		String amount = BLANK + (d * 100);
		if(amount.indexOf(PERIOD) > -1) { 
			amount = amount.substring(0,amount.indexOf(PERIOD)); 
		}
		String cardNumber = order.getField("CreditNum");
		cardNumber = StringUtil.replace(StringUtil.replace(cardNumber,"-","")," ",""); 
		
		// TODO Set Transaction type in code 
		String transactionType = RealEx.AUTH; //String transactionType = order.getField("-transactionType"); 
		
		root.addAttribute(TIMESTAMP, timeStamp);
		root.addAttribute(TYPE,transactionType); 
		root.addElement(MERCHANTID).addText(MERCHANTID_VALUE); 
	   //root.addElement(ACCOUNT).addText("account to use"); // non mandatory
	   root.addElement(ORDERID).addText(orderID);
	    
		if (transactionType.equals(AUTH) ||
			transactionType.equals(REFUND) ||
			transactionType.equals(TSS) ||
			transactionType.equals(OFFLINE)) {
	    
	      sub = root.addElement(AMOUNT); 
	      sub.addAttribute(CURRENCY,currency); 
	      sub.addText(amount); 
	        
	      sub = root.addElement(CARD); 
	      sub.addElement(NUMBER).addText(cardNumber); 
	      sub.addElement(EXPDATE).addText(StringUtil.replace(order.getField("CreditExpire"),"/",""));
	      sub.addElement(CHNAME).addText(order.getField("CreditName"));
	      sub.addElement(TYPE).addText(order.getField("CreditType")); //card type
	        
	      //Update if we use the switch card type. 
	      if(order.getField("CreditType").equals("switch")) { 
	      	sub.addElement(ISSUENO).addText("");
	      } 
	        
	      subsub = sub.addElement(CVN); 
	      subsub.addElement(NUMBER).addText(order.getField("CreditCVV"));
	      subsub.addElement(PRESIND).addText(order.getField("CreditCVV").length()>0?"1":"4");
	        
	      /* Presence Indicator. 
	       * 1: cvn present
				2: cvn illegible
				3: cvn not on card
				4: cvn not requested **default if not specified in above. 
	       */
	        
	      //root.addElement(AUTOSETTLE).addAttribute(FLAG,order.getField("-autoSettle"));
	      root.addElement(AUTOSETTLE).addAttribute(FLAG,"1");
	      /*
	       * 0 = will sit in the db until manually authorised 
	       * 1 = will be settled overnight 
	       */
		}
		
		if (transactionType.equals("void")) {
			root.addElement(PASREF).addText("pasreftext"); 
		}
		
		if (transactionType.equals("offline")) {
			root.addElement(AUTHCODE).addText("authorisationcode"); 
		}
	    
		if(order.getField("-comment1").length()>0) { 
			sub = root.addElement(COMMENTS); 
	      sub.addElement(COMMENT).addAttribute(ID,"1").addText(order.getField("-comment1"));
	      if(order.getField("-comment2").length()>0) {
	      	sub.addElement(COMMENT).addAttribute(ID,"2").addText(order.getField("-comment2"));
	      } 
		} 

		boolean phase2 = false; 
		
		/**
		 * This code ties into the RealScore service, which provides credit intelligence on
		 * purchasers habits. It will be required for phase 2 development. 
		 */
		if(phase2) { 
	        sub = root.addElement(TSSINFO);
	        
	        sub.addElement(CUSTNUM).addText("customer number");
	        sub.addElement(PRODID).addText("product id");
	        sub.addElement(VARREF).addText("variable reference");
	        sub.addElement(CUSTIPADDRESS).addText("www.xxx.yyy.zzz");
	        
	        subsub = sub.addElement(ADDRESS); 
	        subsub.addAttribute(TYPE, "billing"); 
	        subsub.addElement(CODE).addText("zip/postal code");
	        subsub.addElement(COUNTRY).addText("country");
	        
	        subsub = sub.addElement(ADDRESS); 
	        subsub.addAttribute(TYPE, "shipping"); 
	        subsub.addElement(CODE).addText("zip/postal code");
	        subsub.addElement(COUNTRY).addText("country");
	        
	        subsub = sub.addElement(NARRATIVE); 
	        subsub.addElement(ESTABLISHMENTNAME).addText("Merchant Name");
	        subsub.addElement(ESTABLISHMENTCITY).addText("CITY"); 
	        subsub.addElement(ESTABLISHMENTSTATE).addText("CC");
	        subsub.addElement(CHARGEDESCRIPTION).addText("DESCRIPTION");
		} 
		
		StringBuffer h = new StringBuffer();
		h.append(timeStamp).append(PERIOD);
		h.append(MERCHANTID_VALUE).append(PERIOD);
		h.append(orderID).append(PERIOD);
		h.append(amount).append(PERIOD);
		h.append(currency).append(PERIOD);
		h.append(cardNumber);
	    
	    root.addElement(SHA1HASH).addText(generateRealexHash(h, false)); 
	    /*
	     * Don't need to do both types of hash, and SHA1 is supposedly better. 
	     * root.addElement(MD5HASH).addText("###");
	     */
	    
	   return getXmlFromDocument(document); 
	}
}
