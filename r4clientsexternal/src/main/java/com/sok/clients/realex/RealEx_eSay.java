package com.sok.clients.realex;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;

abstract class RealEx_eSay extends RealEx {

	protected 	static final String MERCHANTID_VALUE_TEST	= "esaylimitedtest";
	private 		static final String SHARED_SECRET_TEST 	= "secret";
	
	protected 	static final String MERCHANTID_VALUE_LIVE	= "esaylimited";
	private 		static final String SHARED_SECRET_LIVE 	= "S4RK9pUDrt";
	
	protected 	static final String MERCHANTID_VALUE 		= MERCHANTID_VALUE_TEST; 
	private 		static final String SHARED_SECRET 			= SHARED_SECRET_TEST;
	
	protected 	static final String REBATE_PASSWORD_TEST  = "rebate"; 
	protected 	static final String REFUND_PASSWORD_TEST	= "rebate";
	
	protected 	static final String REBATE_PASSWORD_LIVE	= "rebate"; 
	protected 	static final String REFUND_PASSWORD_LIVE	= "iremix100";
	
	protected 	static final String REBATE_PASSWORD 		= REBATE_PASSWORD_TEST; 
	protected 	static final String REFUND_PASSWORD 		= REFUND_PASSWORD_TEST;

	/**
	 * Parse the xml response given by RealEx into a HashMap that we can access
	 * @author Michael Dekmetzian 14/12/2007
	 * @param xml
	 * @return HashMap - will have '-hashfailed = true' if the hashes do not match
	 * @throws DocumentException
	 */
	protected static Map<String, String> parseResponse(String xml) throws DocumentException { 
		
		Document document = DocumentHelper.parseText(xml);
		Map<String, String> xmlResponses = new HashMap<String,String>(); 
		
		Element root = document.getRootElement(); 
		
		for ( int i = 0, size2 = root.attributeCount(); i < size2; i++ ) {
			Attribute childAttribute = root.attribute(i);
			xmlResponses.put(root.getName()+"_"+childAttribute.getName(), childAttribute.getText());					
		}		
		
		for ( int i = 0, size = root.nodeCount(); i < size; i++ ) {
			Node node = root.node(i); 
			
			if(node instanceof Element) { 
				Element thisNode = (Element)node; 
				xmlResponses.put(node.getName(), node.getText());

				for ( int i2 = 0, size2 = thisNode.nodeCount(); i2 < size2; i2++ ) {
					Node childNode = thisNode.node(i2); 
					if(childNode instanceof Element) { 
						Element thisChildNode = (Element)childNode; 
						xmlResponses.put(node.getName()+"_"+thisChildNode.getName(), thisChildNode.getText());
					} 
				} 
			} 
		} 
		
		StringBuffer h = new StringBuffer(); 
		h.append(xmlResponses.get("response_timestamp"));
		h.append(PERIOD).append(MERCHANTID_VALUE);
		h.append(PERIOD).append(xmlResponses.get("orderid"));
		h.append(PERIOD).append(xmlResponses.get("result"));
		h.append(PERIOD).append(xmlResponses.get("message"));
		h.append(PERIOD).append(xmlResponses.get("pasref"));
		h.append(PERIOD).append(xmlResponses.get("authcode"));
		
		if(!generateRealexHash(h).equals(xmlResponses.get("sha1hash"))) { 
			xmlResponses.put("-hashfailed","true");
		}
		xmlResponses.put("-fullResponse",xml); 
		
		return xmlResponses;
	}
	
	/**
	 * Backwards compatibility method, assumes is not rebate. 
	 * @author Michael Dekmetzian 08/01/2008
	 * @param text
	 * @return generateRealexHash(text,false); 
	 */
	protected static String generateRealexHash(StringBuffer text) { 
		return generateRealexHash(text, false, ALGORITHM); 
	}
	
	/**
	 * Backwards compatibility method, assumes SHA-1 is used. 
	 * @author Michael Dekmetzian 08/01/2008
	 * @param text
	 * @return generateRealexHash(text,false); 
	 */
	protected static String generateRealexHash(StringBuffer text, boolean isRebate) { 
		return generateRealexHash(text, isRebate, ALGORITHM); 
	}
	
	/**
	 * Generates the SHA-1 Hash required for Realex Transactions. 
	 * Uses static merchantid, shared secret, encoding, and algorithm.
	 * @author Michael Dekmetzian 13/12/2007
	 * Updated 08/01/2008 to allow for rebates
	 * @param text - text to be hashed, in StringBuffer format for convenience.
	 * @param isRefund - refund hashes use different secret 
	 * @return hashed value
	 */
	protected static String generateRealexHash(StringBuffer text, boolean isRebate, String algorithm) {
		String calchash = "ERROR"; 
		
		try {
			calchash = hashString(text.toString(), algorithm, ENCODING_TYPE);
			text.delete(0,text.length()); 
			text.append(calchash).append(PERIOD).append(isRebate ? REFUND_PASSWORD : SHARED_SECRET ); 

			calchash = "ERROR"; 
			calchash = hashString(text.toString(),algorithm, ENCODING_TYPE); 
			
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e.getMessage());
		} catch (UnsupportedEncodingException ue ) {
			System.out.println(ue.getMessage());
		}		
		
		return calchash; 
	}
	
	protected static String generateRefundHash(StringBuffer text) {
		
		String calchash = "ERROR"; 
		
		try {
			calchash = hashString(text.toString(), ALGORITHM, ENCODING_TYPE);			
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e.getMessage());
		} catch (UnsupportedEncodingException ue ) {
			System.out.println(ue.getMessage());
		}

		return calchash; 
	}
}
