package com.sok.clients.realex;

import java.util.HashMap;
import java.util.Map;
import java.text.DecimalFormat;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.sok.framework.GenRow;
import com.sok.framework.KeyMaker;
import com.sok.framework.StringUtil;
import com.sok.runway.crm.Contact;
import com.sok.runway.crm.Order;

public class RealEx_eSay_RealEFT extends RealEx_eSay {

	
	protected static final String PAYER = "payer";
	protected static final String PAYER_NEW = "payer-new";
	protected static final String PAYER_EDIT = "payer-edit";
	
	protected static final String CARD_NEW = "card-new";
	protected static final String CARD_EDIT = "eft-update-expiry-date";
	
	protected static final String DDI = "ddi";
	protected static final String DDI_NEW = "ddi-new";
	protected static final String DDI_EDIT = "ddi-edit";
	protected static final String REF = "ref";
	protected static final String PAYERREF = "payerref";
	protected static final String TITLE = "title"; 
	protected static final String FIRSTNAME = "firstname"; 
	protected static final String SURNAME = "surname";
	protected static final String ACCOUNTS = "accounts";
	protected static final String ACCOUNTNUMBER = "accountnumber"; 
	protected static final String SORTCODE = "sortcode"; 
	protected static final String EFT = "eft"; 
	protected static final String BANK = "bank"; 
	protected static final String NAME = "name";
	protected static final String STATUS = "status"; 
	protected static final String PAYMENTMETHOD = "paymentmethod"; 
	
	/**
	 * sendAddPayerRequest
	 */
	public static Map<String,String> sendAddPayerRequest(Contact c) 
	{ 	
		Map <String, String> responseMap; 
		try {
			RealExConnection rec = new RealExConnection();
			String resp = rec.getRealEFTResponseXML(addPayer(c)); 
			responseMap = parseResponse(resp);
			return responseMap;  
		} catch (Exception e) {
			System.out.print(e.getMessage());
			responseMap = new HashMap<String,String>();
			responseMap.put("ERROR",e.getMessage()); 
		} 
		return responseMap;  		
	}
	
	/**
	 * sendModifyCardRequest
	 * @param payment
	 * @param isEdit
	 * @return
	 */
	public static Map<String,String> sendModifyCardRequest(GenRow payment, boolean isEdit) 
	{ 	
		Map <String, String> responseMap; 
		try {
			RealExConnection rec = new RealExConnection();
			String xml = getCreditCardXML(payment, isEdit);
			
			System.out.println(xml); 
			
			String resp = rec.getRealEFTResponseXML(xml); 
			responseMap = parseResponse(resp);
			return responseMap;  
		} catch (Exception e) {
			System.out.print(e.getMessage());
			responseMap = new HashMap<String,String>();
			responseMap.put("ERROR",e.getMessage()); 
		} 
		return responseMap;  		
	}
	
	/**
	 * makePayment
	 * @param contact
	 * @param o
	 * @param payMethod
	 * @return
	 */
	public static Map<String,String> makePayment(Contact contact, Order o, GenRow payMethod)
	{	
		Map <String, String> responseMap; 
		try {
			RealExConnection rec = new RealExConnection();
			String req = genChargeXML(contact, o, payMethod); 
			String resp = rec.getRealEFTResponseXML(req); 
			responseMap = parseResponse(resp);
			responseMap.put("REQUEST",req); 
			return responseMap;  
		} catch (Exception e) {
			System.out.print(e.getMessage());
			responseMap = new HashMap<String,String>();
			responseMap.put("ERROR",e.getMessage()); 
		} 
		return responseMap;  
	}
	
	/**
	 * addPayer
	 */
	public static String addPayer(Contact contact) 
	{ 
		return editPayer(contact, true); 
	}
	
	/**
	 * editPayer
	 * @param contact
	 * @param isNew
	 * @return
	 */
	public static String editPayer(Contact contact, boolean isNew) 
	{ 
		if (isNew) 
		{ 
			return getPayerXML(contact, PAYER_NEW);
		}
		return getPayerXML(contact, PAYER_EDIT); 
	}
	
	/**
	 * genChargeXML
	 * @param contact
	 * @param order
	 * @param payMethod
	 * @return
	 */
	protected static String genChargeXML(Contact contact, Order order, GenRow payMethod) 
	{	
		String timeStamp = getTimestamp(); 
		Element sub; 
		
		String orderID = order.getField("OrderID");  
		
		Document document = DocumentHelper.createDocument();
		Element root = document.addElement(REQUEST);
		
		root.addAttribute(TYPE,"receipt-in"); 		
		root.addAttribute(TIMESTAMP, timeStamp);
		root.addElement(MERCHANTID).addText(MERCHANTID_VALUE);
		root.addElement(ACCOUNT).addText(CARDTYPE_AMEX.equals(payMethod.getColumn("CardType")) ? ACCOUNT_AMEXUK : ACCOUNT_INTERNET);
      root.addElement(ORDERID).addText(orderID);
      
      String amount = "";
      DecimalFormat wn = new DecimalFormat("#");
      try
      {
      	amount = wn.format(Double.parseDouble(order.getParameter("TotalCost")) * 100);
      }
      catch(Exception e) { amount = "0"; }
        
      sub = root.addElement(AMOUNT); 
      sub.addAttribute(CURRENCY,GBP); 
      sub.addText(amount); 
         
      root.addElement(PAYERREF).addText(contact.getField("ContactID"));
      root.addElement(PAYMENTMETHOD).addText(payMethod.getColumn("PaymentMethodID"));
        
    	StringBuffer h = new StringBuffer(); 
		h.append(timeStamp).append(PERIOD);
		h.append(MERCHANTID_VALUE).append(PERIOD);
		h.append(orderID).append(PERIOD);
		h.append(amount).append(PERIOD);
		h.append(GBP).append(PERIOD);
		h.append(contact.getField("ContactID"));
        
		root.addElement(SHA1HASH).addText(generateRealexHash(h)); 
        
      return getXmlFromDocument(document); 
	}
	
	/**
	 * getPayerXML
	 */
	private static String getPayerXML(Contact contact, String method) 
	{ 
		String timeStamp = getTimestamp(); 
		String orderID = KeyMaker.generate(); 

		Element sub; 
		
		Document document = DocumentHelper.createDocument();
		Element root = document.addElement(REQUEST);
		root.addAttribute(TYPE,PAYER_NEW); 		
		root.addAttribute(TIMESTAMP, timeStamp);
		
		root.addElement(MERCHANTID).addText(MERCHANTID_VALUE);
      root.addElement(ORDERID).addText(orderID);
        
      sub = root.addElement(PAYER); 
        
      sub.addAttribute(TYPE,"Business"); //TODO APPROPRIATE
      sub.addAttribute(REF,contact.getPrimaryKey());
        
      if(contact.getField("Title").length()>0) { 
        	root.addElement(TITLE).addText(contact.getField("Title"));
      }
        
      root.addElement(FIRSTNAME).addText(contact.getField("FirstName"));
      root.addElement(SURNAME).addText(contact.getField("Surname"));
        
    	StringBuffer h = new StringBuffer(); 
		h.append(timeStamp).append(PERIOD);
		h.append(MERCHANTID_VALUE).append(PERIOD);
		h.append(orderID).append(PERIOD);
		h.append(BLANK).append(PERIOD);
		h.append(BLANK).append(PERIOD);
		h.append(contact.getPrimaryKey());
        
        root.addElement(SHA1HASH).addText(generateRealexHash(h)); 
        
        return getXmlFromDocument(document); 
	}
	
	/**
	 * getCreditCardXML
	 */
	private static String getCreditCardXML(GenRow payment, boolean isEdit) {
		String timeStamp = getTimestamp(); 
		String orderID = KeyMaker.generate(); 
		
		Document document = DocumentHelper.createDocument();
		Element root = document.addElement(REQUEST);
		root.addAttribute(TYPE,isEdit?CARD_EDIT:CARD_NEW); 		
		root.addAttribute(TIMESTAMP, timeStamp);
		root.addElement(MERCHANTID).addText(MERCHANTID_VALUE);
      
		if(!isEdit) 
      { 
        	root.addElement(ORDERID).addText(orderID);
      } 
        
      Element sub = root.addElement(CARD); 
      sub.addElement(REF).addText(payment.getParameter("PaymentMethodID")); 
      sub.addElement(PAYERREF).addText(payment.getParameter("ContactID"));
        
      if(!isEdit) 
      {         
        	sub.addElement(NUMBER).addText(payment.getParameter("CardNumber"));
      } 
      
      sub.addElement(EXPDATE).addText(StringUtil.replace(payment.getParameter("CardExpiry"),"/",""));
      
      if(!isEdit) 
      {         
	      sub.addElement(CHNAME).addText(payment.getParameter("CardName"));
	      sub.addElement(TYPE).addText(payment.getParameter("CardType")); 
      } 
        
    	StringBuffer h = new StringBuffer(); 
		h.append(timeStamp).append(PERIOD);
		h.append(MERCHANTID_VALUE).append(PERIOD);
		if(isEdit) { 
			h.append(payment.getParameter("ContactID")).append(PERIOD);
			h.append(payment.getParameter("PaymentMethodID")).append(PERIOD);
			h.append(StringUtil.replace(payment.getParameter("CardExpiry"),"/",""));
		} else { 
			h.append(orderID).append(PERIOD);
			h.append(BLANK).append(PERIOD);
			h.append(BLANK).append(PERIOD);
			h.append(payment.getParameter("ContactID")).append(PERIOD);
			h.append(payment.getParameter("CardName")).append(PERIOD);
			h.append(payment.getParameter("CardNumber"));
		}
		System.out.println(h.toString()); 
		
		root.addElement(SHA1HASH).addText(generateRealexHash(h)); 
		
		return getXmlFromDocument(document); 
	}
}
