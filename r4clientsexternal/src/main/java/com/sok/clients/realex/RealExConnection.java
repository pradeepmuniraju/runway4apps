package com.sok.clients.realex;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Convenience class for calling the RealEx Merchant Service. 
 * @author Michael Dekmetzian 13/12/2007
 * Updated 14/12/2007 to provide debugging. 
 */
public class RealExConnection {
/*
	public RealExConnection () throws Exception { 
		
		
        System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        
		// if you have a proxy then you should set the following to true:
		System.setProperty("proxySet", "false");
		
	}
	*/ 
	public boolean debug = false; 
	public RealExConnection(boolean debug) { 
		this.debug = debug; 
	}
	public RealExConnection() { 
		
	}
	/**
	 * Sends a request to RealEx, and returns the Response XML
	 * Adapted from RealEx provided sample code. 
	 * @author Michael Dekmetzian 13/12/2007
	 * @param xml - the request
	 * @return xml response
	 */
	private static final String REALAUTH_CONNECT_URL = "https://epage.payandshop.com/epage-remote.cgi";
	private static final String REALEFT_CONNECT_URL = "https://epage.payandshop.com/epage-remote-plugins.cgi";
	private static final String LINE_BREAK = "\r\n"; 
	private URL remoteURL; 
	private URLConnection connection; 
	private PrintWriter printwriter; 
	private BufferedReader bufferedreader; 
	
	/**
	 * Send/Recieve from RealEx - RealAUTH.
	 * Will print debug output to System.out
	 * @param xml request
	 * @return xml response
	 */
	protected String getRealAUTHResponseXML(String xml) { 
		String resp = getResponse(xml, REALAUTH_CONNECT_URL); 
		if(debug) { 
			System.out.println(resp); 
		}
		return resp; 
	}
	
	/**
	 * Send/Recieve from RealEx - RealEFT.
	 * Will print debug output to System.out
	 * @param xml request
	 * @return xml response
	 */
	protected String getRealEFTResponseXML(String xml) { 
		String resp = getResponse(xml, REALEFT_CONNECT_URL); 
		if(debug) { 
			System.out.println(resp); 
		}
		return resp; 
	}
	
	/**
	 * Send/Recieve from RealEx.
	 * @param xml request
	 * @return xml response
	 */
	private String getResponse(String xml, String url) { 
		
		try { 
			remoteURL = new URL(url);
			connection = remoteURL.openConnection();
			if(debug) {
				System.out.println(connection.toString());
			} 
		
        } catch (MalformedURLException mue) {
            return "Error in hostname: "+url+": "+mue.getMessage();
            
        } catch (InterruptedIOException iioe) {
            // thread died while talking - do a retry
            return ("Died while connecting to payandshop: " + iioe.getMessage());
            
        } catch (IOException ioe) {
            // other exception - probably config error - stop
            return ("Cannot connect to "+url+": "+ioe.getMessage());
        }
		
        connection.setRequestProperty("USER_AGENT", "payandshop.com Connect V1.0 [Java 1.3.0]");
        
        try {
            connection.setDoOutput(true);
            printwriter = new PrintWriter(connection.getOutputStream());
            printwriter.println(xml);
            printwriter.close();
        } catch (IOException ioe) {
            // can anything here be recoverable? - stop
            return ("Error writing xml to payandshop: "+ioe.getMessage());
        }
        
        StringBuilder response = new StringBuilder(); 
        try {
            bufferedreader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine = null; 
        
            while ((inputLine = bufferedreader.readLine()) != null) {
            	response.append(inputLine).append(LINE_BREAK); 
			}
            bufferedreader.close();
            
        } catch (IOException ioe) {
            // ????
            return ("Error reading xml from payandshop: "+ ioe.getMessage());
        }
		return response.toString(); 
	}
	
	
}
