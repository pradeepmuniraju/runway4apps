package com.sok.clients.realex;


/**
 * Realex Payment Gateway Integration
 * @author Michael Dekmetzian
 * @date 13/12/2007 
 */

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper; 
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map; 
import java.util.HashMap; 

abstract class RealEx {

	protected static final String REQUEST = "request"; 
	protected static final String TIMESTAMP = "timestamp";
	protected static final String MERCHANTID = "merchantid";
	protected static final String ACCOUNT = "account";
	protected static final String ORDERID = "orderid";
	protected static final String AMOUNT = "amount";
	protected static final String CURRENCY = "currency";
	protected static final String CARD = "card";
	protected static final String NUMBER = "number";
	protected static final String EXPDATE = "expdate";
	protected static final String CHNAME = "chname";
	protected static final String TYPE = "type";
	protected static final String ISSUENO = "issueno";
	protected static final String CVN = "cvn";
	protected static final String PRESIND = "presind";
	protected static final String AUTOSETTLE = "autosettle";
	protected static final String FLAG = "flag";
	protected static final String PASREF = "pasref";
	protected static final String AUTHCODE = "authcode";
	protected static final String COMMENTS = "comments";
	protected static final String COMMENT = "comment";
	protected static final String ID = "id";
	protected static final String TSSINFO = "tssinfo";
	protected static final String CUSTNUM = "custnum";
	protected static final String PRODID = "prodid";
	protected static final String VARREF = "varref";
	protected static final String CUSTIPADDRESS = "custipaddress";
	protected static final String ADDRESS = "address";
	protected static final String CODE = "code";
	protected static final String COUNTRY = "country";
	protected static final String NARRATIVE = "narrative";
	protected static final String ESTABLISHMENTNAME = "establishmentname";
	protected static final String ESTABLISHMENTCITY = "establishmentcity";
	protected static final String ESTABLISHMENTSTATE = "establishmentstate";
	protected static final String CHARGEDESCRIPTION = "chargedescription";
	
	protected static final String STATUS_ACTIVE = "A";
	protected static final String STATUS_PENDING = "P";
	protected static final String STATUS_CANCELLED = "C";
	
	protected static final String SHA1HASH = "sha1hash";
	protected static final String MD5HASH = "md5hash";
	protected static final String REFUNDHASH = "refundhash"; 
	protected static final String HYPHEN = "-"; 
	protected static final String PERIOD = "."; 
	protected static final String BLANK = "";
	
	protected static final String AUTH = "auth";
	protected static final String REFUND = "refund";
	protected static final String CREDIT = "credit";
	protected static final String _3DSVERIFYENROLLED = "3ds-verifyenrolled";
	protected static final String TSS = "tss";
	protected static final String OFFLINE = "offline";
	protected static final String REBATE = "rebate";
	protected static final String ACCOUNT_AMEXUK = "amexuk";
	protected static final String ACCOUNT_INTERNET = "internet";
	
	protected static final String CARDTYPE_AMEX = "AMEX";
	
	protected static final String ENCODING_TYPE = "UTF-8"; 
	protected static final String ALGORITHM = "SHA-1";
	protected static final String ALGORITHM_MD5 = "MD5";
	
	protected static final String GBP = "GBP"; 
	protected static final String EUR = "EUR";
	
	private static final SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddhhmmss");
	
	/**
	 * Hashes a given string using specified algorithm and encoding. 
	 * Adapted from http://www.anyexample.com/programming/java/java_simple_class_to_compute_sha_1_hash.xml
	 * @author Michael Dekmetzian 13/12/2007.
	 * @param text to be hashed
	 * @param algorithm to be used
	 * @param encoding to be used
	 * @return hashed string
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
    protected static String hashString(String text, String algorithm, String encoding) 
				throws NoSuchAlgorithmException, UnsupportedEncodingException  {
		MessageDigest md;
		md = MessageDigest.getInstance(algorithm);
		byte[] data = new byte[40];
		md.update(text.getBytes(encoding));
		data = md.digest();
		
		StringBuffer sha1hash = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
		        if ((0 <= halfbyte) && (halfbyte <= 9))
		            sha1hash.append((char) ('0' + halfbyte));
		        else
		        	sha1hash.append((char) ('a' + (halfbyte - 10)));
		        halfbyte = data[i] & 0x0F;
			} while(two_halfs++ < 1);
		}
		return sha1hash.toString();
	}  
	
    /**
     * Generates a Timestamp value for use with RealEx Payments. 
     * @author Michael Dekmetzian 14/12/2007
     * @return timestamp representing the current time. 
     */
    protected static String getTimestamp() { 
    	return timeStampFormat.format(new Date()); 
	}
    
    protected static String getXmlFromDocument(Document document) { 

    	String ret = "FAILED"; 
    	try { 
		    OutputFormat format = OutputFormat.createPrettyPrint();
		    StringWriter sw = new StringWriter(); 
		    XMLWriter writer = new XMLWriter( sw, format );
		    writer.write( document );
		    sw.flush(); 
		    ret = sw.toString(); 
		    sw.close(); 
	    	
    	}catch (IOException e) { }
    	
    	return ret; 
    }
    
    /* 
    public static void main(String[] args) { 
    	
    	Contact c = new Contact((java.sql.Connection)null); 
    	c.setField("Currency","GBP");
    	
    	GenRow refCredit = new GenRow(); 
    	refCredit.setColumn("ReferralCreditID",com.sok.framework.KeyMaker.generate());

    }
    */
}
