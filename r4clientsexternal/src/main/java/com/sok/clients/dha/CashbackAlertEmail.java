package com.sok.clients.dha;

import java.sql.Connection;
import java.util.ArrayList;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.runway.crm.Contact;
import com.sok.runway.crm.Product;
import com.sok.runway.crm.Setup;
import com.sok.runway.security.User;

public class CashbackAlertEmail extends PropertyAlertEmail {

	private final String CAMPAIGN_QUESTION_ID = "081G2Q0W3F809W6Q491D1J4V017A"; 
		
   public boolean generateHTMLEmail(String noteID) {
		clear(); 
		
		String contextPath = getContextPath();
		String sitePath = contextPath + "/cms/websites/dha";
		
		String TERMSLINK = contextPath + "/offer/home/terms.sok"; 

		com.sok.runway.autopilot.AutoPilotSchedule schedule = getAutoPilotSchedule();
			
		Contact contact = (Contact)getEntity();
		
		ArrayList<Product> matchingProductList = getMatchingProducts(contact);
		
		boolean productsFound = matchingProductList != null && matchingProductList.size() > 0;
		
		if (productsFound) {
		
			write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n");
			write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n");
			write("<head>\r\n");
			write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\r\n");
			write("<title>Defence Housing Australia</title>\r\n");
			write("<style>\r\n");
			write("body { padding:0; margin:0; font:11px Verdana, Arial, Helvetica, sans-serif; color:#1e2f5d; text-align:center; background:url(");
			write(sitePath);
			write("/images/body_bg.jpg) repeat-x; background-color:#5E80AB; vertical-align:top; }\r\n");
			write("p { margin:0; padding:4px 15px 4px 15px; text-align:left; }\r\n");
			write("a { color:#1e2f5d; text-decoration:underline; }\r\n");
			write("a:hover { color:#85bce1; text-decoration:underline; }\r\n");
			write("a img { border:none; }\r\n");
			write("ul { list-style:url(.");
			write(sitePath);
			write("/images/bullet_propertydetails.gif); }\r\n");
			write("li { padding:0 0 5px 0; }\r\n");
			write("\r\n");
			write("h1 { font:8px Verdana, Arial, Helvetica, sans-serif; color:#fff; font-weight:bold; margin:0 4px 0 4px; padding:0 6px 0 6px; background:#ec1d26; }\r\n");
			write("h2 { font-size:6px; margin:0 auto; margin:0 7px 5px 7px; padding:0; border-bottom:1px dotted #85bce1; }\r\n");
			write("h3 { font:22px Verdana, Arial, Helvetica, sans-serif bold; color:#1e2f5d; margin:0; padding:0; text-align:left; top:-3px; position:relative; }\r\n");
			write("h4 { font:12px Verdana, Arial, Helvetica, sans-serif; color:#fff; font-weight:bold; margin:0 auto; margin:8px 5px 5px 5px; padding:4px 7px 4px 7px; text-align:left; background:#ec1d26; }\r\n");
			write("h5 { font:11px Verdana, Arial, Helvetica, sans-serif; color:#1e2f5d; font-weight:bold; margin:0; padding:0; text-align:left; }\r\n");
			write("h6 { font:11px Verdana, Arial, Helvetica, sans-serif; color:#fff; font-weight:bold; margin:7px 0 0 0; padding:3px 6px 3px 6px; text-align:right; background:#ec1d26; }\r\n");
			write("\r\n");
			write(".contentHeaderContainer { width:523px; height:66px; background:url(");
			write(sitePath);
			write("/images/contentHeader_bg.jpg) top repeat-x; margin: 0 auto; text-align:center;}\r\n");
			write("td.contentHeader_l { width:30px; height:66px; background:url(");
			write(sitePath);
			write("/images/contentHeader_bg_l.jpg) top left no-repeat; }\r\n");
			write("td.contentHeader_r { width:2px; height:66px; background:url(");
			write(sitePath);
			write("/images/contentHeader_bg_r.jpg) top right no-repeat; }\r\n");
			write("\r\n");
			write(".subContent { width:523px; margin:13px 0 0 0; margin: 0 auto; text-align:center;}\r\n");
			write(".subContentTitles { width:521px; height:25px; border:1px solid #85bce1; background:url(");
			write(sitePath);
			write("/images/subContent_bg.jpg) repeat-x; }\r\n");
			write("td.subContentTitles_l { width:20px; height:25px; background:url(");
			write(sitePath);
			write("/images/subContent_bg_l.jpg) no-repeat; }\r\n");
			write("td.subContentTitles_r { width:25px; height:25px; }\r\n");
			write("\r\n");
			write(".subContentDetails { width:521px; margin:10px 0 0 0; }\r\n");
			write("td.subContentDetails_l { width:182px; vertical-align:top; text-align:right; }\r\n");
			write(".subContentDetails_l img { border:1px solid #85bce1; }\r\n");
			write(".subContentDetails_l a { text-decoration:none; display:block; color:#1e2f5d; }\r\n");
			write(".subContentDetails_l a:hover { color:#2F64BB; }\r\n");
			write(".subContentDetails_l a img { margin:0 0 -6px 0; position:relative; border:none; }\r\n");
			write("td.subContentDetails_r { padding:0 10px 0 0; vertical-align:top; padding:0 0 0 11px; text-align:left; }\r\n");
			write(".subContentDetails_r p { margin:0; padding:10px 5px 0 5px; }\r\n");
			write("\r\n");
			write(".propertyListPanel { width:328px; height:54px; border:1px solid #85bce1; background:url(");
			write(sitePath);
			write("/images/propertyListPanel_bg.jpg) repeat-x; }\r\n");
			write("\r\n");
			write(".propertyIcons { text-align:right; padding:4px 4px 15px 4px; }\r\n");
			write(".propertyIcons img { border:none; padding:4px 0 0 3px; }\r\n");
			write("\r\n");
			write("td.viewDetailsBtn { width:102px; height:54px; background:url(");
			write(sitePath);
			write("/images/propertyListPanel_btn_bg.jpg) no-repeat; vertical-align:top; }\r\n");
			write("td.viewDetailsBold { font:11px Verdana, Arial, Helvetica, sans-serif; font-weight:bold; padding:0 8px 0 7px; }\r\n");
			write("\r\n");
			write("#imageContainer { position:relative; padding:0; margin:0; }\r\n");
			write("#soldBanner { position:absolute; margin:0 auto; padding:0; z-index:1000; top:0; left:0; display:block; }\r\n");
			write("\r\n");
			write("td.shellBorder_l { background:url(");
			write(sitePath);
			write("/images/pageShadow_bg_l.gif) repeat-y; width:8px; vertical-align:top; }\r\n");
			write("td.shellBorder_r { background:url(");
			write(sitePath);
			write("/images/pageShadow_bg_r.gif) repeat-y; width:8px; vertical-align:top; }\r\n");
			write("td.shellBorder_bl { background:url(");
			write(sitePath);
			write("/images/pageShadow_bg_bl.gif) no-repeat; width:8px; height:8px; }\r\n");
			write("td.shellBorder_b { background:url(");
			write(sitePath);
			write("/images/pageShadow_bg_b.gif) repeat-x; height:8px; }\r\n");
			write("td.shellBorder_br { background:url(");
			write(sitePath);
			write("/images/pageShadow_bg_br.gif) no-repeat; width:8px; height:8px; }\r\n");
			write("\r\n");
			write("td.headerLogo_l { height:41px; text-align:left; background:url(");
			write(sitePath);
			write("/images/popup/pop_dha_logo.gif) left no-repeat; }\r\n");
			write("td.headerLogo_r { height:41px; text-align:right; background:url(");
			write(sitePath);
			write("/images/popup/pop_invest_logo.gif) right no-repeat; }\r\n");
			write("table.buttons td { text-align:center; }\r\n");
			write("div.footer { width:521px; border-top:1px solid #85bce1; padding:3px 0 0 0; margin:0 auto; text-align:center; }\r\n");
			write("div.copyright { width:490px; text-align:left; height:22px; padding:3px 0 0 8px; margin:0 auto; background:url(");
			write(sitePath);
			write("/images/img_133DHA.gif) top right no-repeat; font:10px Verdana, Arial, Helvetica, sans-serif; color:#999; }\r\n");
			write("div.optOut { font:9px Verdana, Arial, Helvetica, sans-serif normal; color:#0D2E8E; text-align:center; padding:0 0 10px 0; }\r\n");
			write("\r\n");
			write("</style>\r\n");
			write("</head>\r\n");
			write("<body>\r\n");
	
			write("\r\n");
			write("<table width=\"550\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"align=\"center\" bgcolor=\"#FFFFFF\">\r\n");
			write("\r\n");
			write("\r\n");
			write("<tr>\r\n");
			write("<td class=\"shellBorder_l\"><img src=\"");
			write(sitePath);
			write("/images/pageShadow_l.jpg\" width=\"8\" height=\"735\" /></td>\r\n");
			write("<td valign=\"top\">\r\n");
			write("<div class=\"optOut\"><a href=\"");
			write(contextPath);
			write("/cms/emails/shared/notehtmlview.jsp?ID=");
			write(noteID);
			write("&version=html\">Having trouble viewing this email? ~ Click here to view as a webpage</a></div>\r\n");
			write("\r\n");
			write("<div>\r\n");
			write("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
			write("<tr>\r\n");
			write("<td class=\"headerLogo_l\">&nbsp;</td>\r\n");
			write("<td class=\"headerLogo_r\">&nbsp;</td>\r\n");
			write("</tr>\r\n");
			write("</table>\r\n");
			write("</div>\r\n");
			write("<h1>&nbsp;</h1>\r\n");
			write("<h2>&nbsp;</h2>\r\n");
			write("<img src=\"");
			write(contextPath); 
			write("/specific/autopilot/promotional/dha_special_promotion_header.jpg\" height=\"67\" width=\"517\" border=\"0\">\r\n");
			write("<p>Dear ");
			if (contact.getField("FirstName").length() >= 2) {
			write(contact.getField("FirstName"));
			}
			else {
			write("Investor");
			}
			write("</p>\r\n");
			write("\r\n");
			
			User repUser = contact.getRepUser();
			
			String repEmail = "sales@dha.gov.au"; 
			
			if (repUser != null && !repUser.getPrimaryKey().equals(INACTIVE_USER_ID)) {
				repEmail = contact.getField("RepEmail"); 
			}
			
			setEmailFrom("Troy Latter <automated.dha@runway.net.au>");
			setEmailReplyTo(repEmail);      
			
			write("\r\n");
			write("<p><strong>Great news!</strong></p>\r\n");
			write("<p>As a registered DHA member I'm pleased to give you advance notice of a special promotion.</p>\r\n"); 
			write("<p>When you buy a selected DHA investment property and settle the contract on or before 27 June 2008, we'll increase the property's rental yield to 5.7%  (<a href=\""); 
			write(TERMSLINK); 
			write("\">terms and conditions apply</a>).</p>\r\n");
			write("<p>With DHA's rental guarantee, this means your rent will never drop below the revised amount for the life of the lease.</p>\r\n"); 
			write("<p>This promotion is by invitation only for a limited time. It will be marketed to the general public on Friday 4 April 2008.</p>"); 
			write("<p>So be quick! Contact your <a href=\"");
			write(contextPath); 
			write("/offer/home/contact.sok?ID="); 
			write(noteID); 
			write("\">sales consultant</a> now to place a property on hold or obtain information about other DHA investment properties.</p>\r\n"); 
			write("<p>Kind Regards</p>\r\n");
			write("<p>&nbsp;</p>\r\n");
			write("<p><b>Troy Latter</b></p>\r\n");
			write("<p><b>National Sales Manager</b></p>\r\n");
			
			write("<p>26 Brisbane Avenue Barton ACT 2600</p>\r\n");
			write("<p>T: 02 6217 8407</p>\r\n");
			write("<p>M: 0421 041 010</p>\r\n");
			write("<p>F: 02 6277 1180</p>\r\n");
			write("<p>E: troy.latter@dha.gov.au</p>\r\n");
			
			write("\r\n");
			write("\r\n");
			write("\r\n");
			write("<h2>&nbsp;</h2>\r\n");
			write("<table class=\"buttons\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>\r\n");
			
			/* contact dha */ 
			write("<td><a href=\"");
			write(contextPath);
			write("/offer/home.sok?ID=");
			write(noteID);
			write("\"><img src=\"");
			write(contextPath);
			write("/specific/autopilot/promotional/0802_cashback_properties.jpg\" alt=\"View full list of promo properties\" width=\"165\" height=\"57\" border=\"0\" /></a></td>\r\n");
			/* end contact dha */
			
			/* contact dha */ 
			write("<td><a href=\"");
			write(contextPath);
			write("/offer/home/contact.sok?ID=");
			write(noteID);
			write("\"><img src=\"");
			write(contextPath);
			write("/specific/autopilot/promotional/0802_cashback_contact.jpg\" alt=\"Contact sales consultant for more information\" width=\"165\" height=\"57\" border=\"0\" /></a></td>\r\n");
			/* end contact dha */
			
			/* visit dha */ 
			write("<td><a href=\"");
			write(contextPath);
			write("/dha/home.sok?ID=");
			write(noteID);
			write("\"><img src=\"");
			write(contextPath);
			write("/specific/autopilot/promotional/0802_cashback_visit.jpg\" alt=\"Visit DHA\" width=\"165\" height=\"57\" border=\"0\" /></a></td>\r\n");
			/* end visit dha */
			write("</tr></table>\r\n");
			write("<h2>&nbsp;</h2>\r\n");
			write("<h4>Your property alert</h4>\r\n");
			write("\r\n");
			write("<div class=\"subContent\">\r\n");
	
			for (Product product : matchingProductList) {
				writeHTMLProduct(product, contextPath, sitePath, noteID, "/offer/home/property.sok?ProductID="); 
			}
			matchingProductList = null;
	
			write("\r\n");
			write("<p><strong><a href=\""); 
			write(TERMSLINK); 
			write("\">Click to view terms and conditions</a></strong></p>\r\n");
			write("</td>\r\n");
			write("<td class=\"shellBorder_r\"><img src=\"");
			write(sitePath);
			write("/images/pageShadow_r.jpg\" width=\"8\" height=\"735\" /></td>\r\n");
			write("</tr>\r\n");
			write(" \r\n");
			write("<!-- START footer ADDED !-->\r\n");
			write("<tr>\r\n");
			write("<td class=\"shellBorder_l\"></td>\r\n");
			write("<td align=\"center\">\r\n");
			write("<div class=\"footer\">\r\n");
			write("<div class=\"copyright\">&copy; Copyright Defence Housing Australia 2007</div>\r\n");
			write("</div>\r\n");
			write("</td>\r\n");
			write("<td class=\"shellBorder_r\"></td>\r\n");
			write("</tr>\r\n");
			write("<!-- END footer ADDED !-->\r\n");
			write("\r\n");
			write("<tr>\r\n");
			write("<td class=\"shellBorder_bl\"><img src=\"");
			write(contextPath);
			write("/images/pixel.gif\" width=\"1\" height=\"1\" /></td>\r\n");
			write("<td class=\"shellBorder_b\"><img src=\"");
			write(contextPath);
			write("/images/pixel.gif\" width=\"1\" height=\"1\" /></td>\r\n");
			write("<td class=\"shellBorder_br\"><img src=\"");
			write(contextPath);
			write("/images/pixel.gif\" width=\"1\" height=\"1\" /></td>\r\n");
			write("</tr>\r\n");
			write("</table>\r\n");
			write("<div class=\"optOut\"><a href=\"");
			write(contextPath);
			write("/dha/optout.jsp?ID=");
			write(noteID);
			write("\">Click here to opt-out of future communications from Defence Housing Australia</a></div>\r\n");
			write("\r\n");
			write("<img src=\""); 
			write(contextPath); 
			write("/RESPONSE/"); 
			write(noteID); 
			write("/Viewed_Email.gif\" width=\"1\" height=\"1\">\r\n"); 
			write("</body>\r\n");
			write("</html>\r\n");
		}
		return productsFound;
	}
   
   public boolean generateTextEmail(String noteID) {
	      super.generateTextEmail(noteID);
	      
		   String contextPath = getContextPath();
		   String sitePath = contextPath + "/cms/websites/dha";

	      com.sok.runway.autopilot.AutoPilotSchedule schedule = getAutoPilotSchedule();
			
	      Contact contact = (Contact)getEntity();
		   
		   ArrayList<Product> matchingProductList = getMatchingProducts(contact);
		   
		   boolean productsFound = matchingProductList != null && matchingProductList.size() > 0;
		   
		   if (productsFound) {
		   
	         write("Defence Housing Australia\r\n");
	         write("=========================\r\n");
	         write("Follow the following link to view an online version of this email.\r\n");
	         write(contextPath);
	         write("/dha/etext.jsp?ID=");
	         write(noteID);
	         write("\r\n");
	         write("Dear ");
	         if (contact.getField("FirstName").length() >= 2) {
	            write(contact.getField("FirstName"));
	         }
	         else {
	            write("Investor");
	         }
	         write("\r\n");
	         
	         User repUser = contact.getRepUser();
	         
	         write("Great News.\r\n\r\n");
	         write("As a registered DHA member I'm pleased to give you advance notice of a special promotion.\r\n\r\n");
	         write("When you buy a selected DHA investment property and settle the contract on or before 27 June 2008, we'll increase the property's rental yield to 5.7% (terms and conditions apply).\r\n\r\n");
	         write("With DHA's rental guarantee, this means your rent will never drop below the revised amount for the life of the lease.\r\n\r\n");
	         write("This promotion is by invitation only for a limited time. It will be marketed to the general public on Monday 7 April 2008.\r\n\r\n");
	         write("So be quick! Contact your sales consultant now to place a property on hold or obtain information about other DHA investment properties.\r\n\r\n");
	         
	         if (repUser != null && !repUser.getPrimaryKey().equals(INACTIVE_USER_ID)) {
	         
	            //setEmailFrom(contact.getField("RepEmail"));
	   			setEmailFrom(contact.getField("RepFirstName") + " " + contact.getField("RepLastName") + " <automated.dha@runway.net.au>");
	   			setEmailReplyTo(contact.getField("RepEmail"));  
	         }
	         else { 
	   			//setEmailFrom("sales@dha.gov.au");  
	   			setEmailFrom("Troy Latter <automated.dha@runway.net.au>");
	   			setEmailReplyTo("sales@dha.gov.au");  
	      
	         } 
	         
	         for (Product product : matchingProductList) {
	        	 writeTextProduct(product, contextPath, sitePath, noteID, "/offer/home/property.sok?ProductID="); 
	         }
	         matchingProductList = null;

	         write("\r\n");
	         write("\r\n");
	         write("================================================================================================\r\n");
	         write("\r\n");
	         write("Copyright Defence Housing Australia 2007\r\n");
	         write("\r\n");
	         write("Follow the following link to opt out of future communications from Defence Housing Australia\r\n");
	         write(contextPath);
	         write("/dha/optout.jsp?ID=");
	         write(noteID);
	         write("\r\n");
	         write("\r\n");
		   }
		   
	      return productsFound;
	   }
   
   protected synchronized void loadProducts(Connection con) {
       
	    this.productList = new ArrayList();
	      
	   	GenRow products = new GenRow();
	   	products.setConnection(con);
	   	products.setViewSpec("ProductIDView");
	   	//products.setParameter("Active","Y");
	   	//TODO APPROPRIATE	
	   	//products.setParameter("AvailableDate", "CURRENT;>"+getAutoPilotSchedule().getFormattedField("LastRunDateTime", "dd/MM/yyyy HH:mm:ss"));
	   	//products.setParameter("AvailableDate", ">04/08/2007");
		
	   	// Specific relationships		
	   	//products.setParameter("-join1", "ProductPropertyStatusAnswer");
	   	//products.setParameter("ProductPropertyStatusAnswer.Answer","Listed");
	   	
	   	products.setParameter("-join1", "ProductCurrentStatus.ProductStatusList");
	   	products.setParameter("ProductCurrentStatus.ProductStatusList.Status","Listed");
	   	
	   	//products.setParameter("-join2", "ProductValuationExpiryAnswer");
	   	//products.setParameter("ProductValuationExpiryAnswer.AnswerDate","!OVERDUE");
	   	
	   	/*
	      private static final String RESERVE_STOCK_QUESTION_ID = "0A1K14882U9B1K1U579O0P1J7V32";
	   	products.setParameter("ProductReserveStockAnswer.Answer","Yes");
	   	products.setParameter("-ProductReserveStockAnswer","omit");
	   	*/
	   	
		products.setParameter("-join3","ProductListingTypeAnswer"); // for sort only
	   	products.sortBy("ProductListingTypeAnswer.Answer",0);
	   	products.sortOrder("DESC",0);
		products.sortBy("ProductStatusList.Status",1); 
		products.sortOrder("ASC",1); 
	   	products.setParameter("ProductAnswer#ProductAnswerQuestionID="+CAMPAIGN_QUESTION_ID+".Answer","08 April Promo"); 
	   	products.getResults();
	   	
	   	Product product = null;
	   	
	   	while(products.getNext()) {
	   	   product = new Product(con);
	   	   product.load(products.getString("ProductID"));
	   	   if (product.isLoaded()) {
	   	      product.loadProfiles();
	   	      productList.add(product);
	   	   }
	   	}
	   	
	   }
}
