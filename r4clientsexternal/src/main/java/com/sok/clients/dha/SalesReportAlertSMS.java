/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sok.clients.dha;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.framework.PagingRowSetBean;
import com.sok.framework.generation.util.CalendarFactory;
import com.sok.runway.autopilot.AbstractActionTypeSMS;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author emil
 */
public class SalesReportAlertSMS extends AbstractActionTypeSMS {

   private int holdSize;
   private int offerSize;
   private int unconditionalSize;
   private int settleSize;

   private int holdSizeFiYear;
   private int offerSizeFiYear;
   private int unconditionalSizeFiYear;
   private int settleSizeFiYear;
   
   private static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

   private int getResultSetSize(GenRow search,String financialYear, String constraint) {

	  search.clear();
      search.setViewSpec("OpportunityListView");
      if(financialYear != null) {
    	  search.setParameter("OpportunityOpportunityStatus.CreatedDate",financialYear);
   	  } 
      search.setParameter("OpportunityOpportunityStatus.StatusID", constraint);
      search.setParameter("OpportunityProcess.Name","SLB Property Sale");
      search.setAction(ActionBean.SEARCH);
      search.doAction();

      PagingRowSetBean sarchList = new PagingRowSetBean();
      sarchList.setSearchBean(search);
      sarchList.getResults();
      return sarchList.getSize();
   }

   public void generateSearch() {
	   
      String financialYear = dateFormat.format(CalendarFactory.getStartOfCurrentFinancialYear().getTime())+"-"+dateFormat.format(CalendarFactory.getEndOfCurrentFinancialYear().getTime());
             
      GenRow search = new GenRow();
      /* Finds properties with the status of hold, offer, unconditional and settled */ 	   
      holdSize = getResultSetSize(search,null,"0C171H8T4U712H2Y7X627S8R4N4B");
      offerSize = getResultSetSize(search,null,"0L1F1D8Z4J732K2074842L4Q5R6K");
      unconditionalSize = getResultSetSize(search,null,"0O18158F4A7I2O2V759F2G556A1Y");
      settleSize = getResultSetSize(search,null,"0A1Z1P8O4Q7Q292K8L0R4Y0Q8N9D");

	  /* Finds properties with the status of hold, offer, unconditional and settled for the current financial year*/ 	   
      holdSizeFiYear = getResultSetSize(search,financialYear,"0C171H8T4U712H2Y7X627S8R4N4B");
      offerSizeFiYear = getResultSetSize(search,financialYear,"0L1F1D8Z4J732K2074842L4Q5R6K");
      unconditionalSizeFiYear = getResultSetSize(search,financialYear,"0O18158F4A7I2O2V759F2G556A1Y");
      settleSizeFiYear = getResultSetSize(search,financialYear,"0A1Z1P8O4Q7Q292K8L0R4Y0Q8N9D");
          
      /*
      com.sok.Debugger.getDebugger().debug("holdSize: "+holdSize+", offerSize: "+offerSize+", "+
      "unconditionalSize: "+unconditionalSize+", settleSize: "+settleSize);
      
      com.sok.Debugger.getDebugger().debug("holdSizeFiYear: "+holdSizeFiYear);
      com.sok.Debugger.getDebugger().debug("offerSizeFiYear: "+offerSizeFiYear);
      com.sok.Debugger.getDebugger().debug("unconditionalSizeFiYear: "+unconditionalSizeFiYear);
      com.sok.Debugger.getDebugger().debug("settleSizeFiYear: "+settleSizeFiYear);
      */
   }

   public void generateSearch24_10_2008() {	   		 
		   
	      String lastMonth = dateFormat.format(CalendarFactory.getStartOfMonthCal().getTime())+"-"+dateFormat.format(CalendarFactory.getEndOfMonthCal().getTime());
	       
	      GenRow search = new GenRow();
	      /* Finds properties with the status of hold, offer, unconditional and settled for the current month */ 
	      holdSize = getResultSetSize(search,lastMonth,"0C171H8T4U712H2Y7X627S8R4N4B");
	      offerSize = getResultSetSize(search,lastMonth,"0L1F1D8Z4J732K2074842L4Q5R6K");
	      unconditionalSize = getResultSetSize(search,lastMonth,"0O18158F4A7I2O2V759F2G556A1Y");
	      settleSize = getResultSetSize(search,lastMonth,"0A1Z1P8O4Q7Q292K8L0R4Y0Q8N9D");
	      
	      
	      dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	      String financialYear = dateFormat.format(CalendarFactory.getStartOfCurrentFinancialYear().getTime())+"-"+dateFormat.format(CalendarFactory.getEndOfCurrentFinancialYear().getTime());
	             
	      /* Finds properties with the status of hold, offer, unconditional and settled for the current financial year*/ 
	      holdSizeFiYear = getResultSetSize(search,financialYear,"0C171H8T4U712H2Y7X627S8R4N4B");
	      offerSizeFiYear = getResultSetSize(search,financialYear,"0L1F1D8Z4J732K2074842L4Q5R6K");
	      unconditionalSizeFiYear = getResultSetSize(search,financialYear,"0O18158F4A7I2O2V759F2G556A1Y");
	      settleSizeFiYear = getResultSetSize(search,financialYear,"0A1Z1P8O4Q7Q292K8L0R4Y0Q8N9D");
	          
	      /*
	      com.sok.Debugger.getDebugger().debug("holdSize: "+holdSize+", offerSize: "+offerSize+", "+
	      "unconditionalSize: "+unconditionalSize+", settleSize: "+settleSize);
	      
	      com.sok.Debugger.getDebugger().debug("holdSizeFiYear: "+holdSizeFiYear);
	      com.sok.Debugger.getDebugger().debug("offerSizeFiYear: "+offerSizeFiYear);
	      com.sok.Debugger.getDebugger().debug("unconditionalSizeFiYear: "+unconditionalSizeFiYear);
	      com.sok.Debugger.getDebugger().debug("settleSizeFiYear: "+settleSizeFiYear);
	      */
   }
   
   public boolean generateSMS(String noteID) {	   
	   
	  /* Suggested Format
	   Sales report 21/07/2008:
	   Hold - 50/20
	   Offer - 20/10
	   Unconditional - 30/15
	   Settled - 100	   
	   */ 
      String today = dateFormat.format(new java.util.Date());
      //generateSearch();
      generateSearch24_10_2008();
      write("Sales report ");
      write(today);
      write(": \r\nHold - ");
      write(String.valueOf(holdSizeFiYear)+"/"+String.valueOf(holdSize));
      write("\r\nOffer - ");
      write(String.valueOf(offerSizeFiYear)+"/"+String.valueOf(offerSize));
      write("\r\nUnconditional - ");
      write(String.valueOf(unconditionalSizeFiYear)+"/"+String.valueOf(unconditionalSize));
      write("\r\nSettled - ");
      write(String.valueOf(settleSizeFiYear)+"/"+String.valueOf(settleSize));
      return true;
   }
}