package com.sok.clients.dha;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sok.framework.GenRow;
import com.sok.framework.generation.GenerationKeys; 

public class PropertyOnHoldImageServlet extends HttpServlet {
	
	private int PIXEL = 0; 
	private int ONHOLD = 1; 
	
	private byte[][] IMAGES; 
	
	private boolean PIXEL_LOADED; 
	private boolean ONHOLD_LOADED;
	
	private String BR = "</br>"; 
	
	private String PIXEL_PATH = "/images/pixel.gif";
	private String ONHOLD_PATH = "/specific/autopilot/propertysendimages/onhold.gif"; 
	
	private boolean debug = false; 
	
	public void init() {
		IMAGES = new byte[2][]; 
		
		ONHOLD_LOADED = loadImage(ONHOLD_PATH, ONHOLD);
		PIXEL_LOADED = loadImage(PIXEL_PATH, PIXEL); 
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, FileNotFoundException, IOException {
	   doRequest(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, FileNotFoundException, IOException {
	   doRequest(req, res);
	}
	
	public void doRequest(HttpServletRequest req, HttpServletResponse res) throws ServletException, FileNotFoundException, IOException {
	   
		res.setContentType( "text/html" );
		if(!req.isSecure()){
			   //no cache breaks downloading pdf in ie over https
			res.setHeader("Expires","Mon, 26 Jul 1997 05:00:00 GMT" );  // disable IE caching
			res.setHeader("Cache-Control","no-cache, must-revalidate" );
			res.setHeader("Pragma","no-cache" );
			res.setDateHeader ("Expires", 0);
		}
		
		GenRow pr = new GenRow(); 
		pr.setTableSpec("Products"); 
		pr.setParameter("-join1", "ProductCurrentStatus.ProductStatusList"); 
		pr.setParameter("-select1","ProductID"); 
		pr.setParameter("-select2","ProductCurrentStatus.ProductStatusList.Status as ProductStatus");
		
		String path = req.getPathInfo(); 
		
		StringBuffer msg = new StringBuffer("Init Path:").append(path).append(BR);  
		if (true || path.startsWith("/")) { 
			path = path.substring(1); 
			msg.append("Cut Path:").append(path).append(BR); 
		}
		
		String[] options = path.split("/"); 
		String productID = options[0]; 
		if(options.length>0) { 
			debug = "debug.gif".equals(options[1]);
		} 
		
		msg.append("ProductID : ").append(productID).append(BR); 
		
		pr.setParameter("ProductID",productID); 
		pr.doAction(GenerationKeys.SELECT); 
		
		if (!PIXEL_LOADED){ 
			PIXEL_LOADED = loadImage(PIXEL_PATH, PIXEL, msg);  
		}
		if (!ONHOLD_LOADED){ 
			ONHOLD_LOADED = loadImage(ONHOLD_PATH, ONHOLD, msg);
		}
		
		ServletOutputStream out = res.getOutputStream();
		
		
			
			
		if(pr.isSuccessful() && "Hold".equals(pr.getData("ProductStatus"))) {
			if(!debug) { 
				res.setContentType("image/gif");
				out.write(IMAGES[ONHOLD]);
				res.setContentLength(IMAGES[ONHOLD].length);
			} 
		} else {
			if(!debug) { 
				res.setContentType("image/gif");
				out.write(IMAGES[PIXEL]); 
				res.setContentLength(IMAGES[PIXEL].length);
			} 
		}
		if (debug) { 
			res.setContentType("text/html");
			msg.append("Product loaded : ").append(pr.isSuccessful()).append(BR);
			if(pr.isSuccessful())
				msg.append("Product status : ").append(pr.getData("ProductStatus")).append(BR);
			
			out.print(msg.toString()); 
			
		}
	}
	
	private synchronized boolean loadImage(String path, int loadTo) {
		return loadImage(path, loadTo, (StringBuffer)null); 
	}
	
	private synchronized boolean loadImage(String path, int loadTo, StringBuffer debugmsg) { 
		try { 
			
			//Get the pixel gif, and confert to a FileInputStream
			StringBuffer imagepath = new StringBuffer(this.getServletContext().getRealPath("")); 		
			imagepath.append(path);
			FileInputStream gifis = new FileInputStream(new File(imagepath.toString()));
			
			//Convert the FileInputStream to a byte array
			IMAGES[loadTo] = new byte[gifis.available()];
			gifis.read(IMAGES[loadTo]);
			if (gifis.available() == 0) {
				return true; 
			} else { 
				if(debugmsg!=null) { 
					debugmsg.append(imagepath.toString()).append(" did not load correctly").append(BR); 
					debugmsg.append("Remaining ").append(gifis.available()).append(BR); 
					debugmsg.append("Saved ").append(IMAGES[loadTo].length).append(BR); 
				}
				return false;
			}
		}catch (FileNotFoundException e) {  
			//Send an email to SysAdmin to say that the file was not found.
			if(debugmsg!=null) { 
				debugmsg.append("FNF Exception : ").append(e.getMessage()).append(BR); 
			}
			/*
			 * TODO : Add Email Creation Code
			 */
			
			return false; 
		}catch (Exception e) { 
			if(debugmsg!=null) { 
				debugmsg.append("Other Exception : "); 
				debugmsg.append(e.toString()); 
			}
			return false; 
		}
	}
	
}
