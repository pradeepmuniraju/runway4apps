/**
 * 
 */
package com.sok.clients.dha;

import com.sok.framework.DaemonFactory;
import com.sok.framework.DaemonProcess;

import com.sok.Debugger; 
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow; 
import com.sok.framework.RowBean; 
import com.sok.framework.RowSetBean; 
import com.sok.framework.generation.GenerationKeys;

import java.sql.Connection; 

/**
 * This class will monitor the DHA Database.
 * This task is currently being performed by a crontab installed on the sok office FTP server. 
 * Presumably this could be configured for other clients, though making this generic wasn't covered due to time constraints. 
 * @author mikey
 */
public class DBMonitor extends DaemonProcess {
	
	
	private Debugger d = Debugger.getDebugger(); 
	private RowSetBean rs = null; 
	private RowBean exec = null; 
	private GenRow log = null; 
	private static final int DEAD_TIME = 150; 
	private static final String SELECT_STRING = "SELECT";
	private int kills = 0; 
	private static String DATABASE_NAME = "DHA"; 
	
	public DBMonitor(DaemonFactory daemonFactory) {
		super(daemonFactory);
		rs = new RowSetBean(); 
		rs.setSearchStatement("SHOW FULL PROCESSLIST");
		exec = new RowBean();
		log = new GenRow();
		log.setTableSpec("[specific/]Specific_Queries"); 
		
	}

	public String getName() {
		return "DHA DB Monitor - " + kills + " threads terminated";
	}

	public void run() {
		runCheck(); 
		
        while (!isKilled()) {
            setRunning(true);
            runCheck(); 
    		setRunning(false);
    		
    		wait(60); 
        } 
	}
	
   private void wait(int seconds) {
      try {
         Thread.sleep(seconds * 1000);
      } catch (InterruptedException ie) { d.debug("sleeping thread interrupted"); }
	}
	
	public void runCheck() { 

		rs.setConnection((Connection)null); 
		rs.getResults(); 
		 
		exec.setConnection(rs.getConnection()); 
		log.setConnection(rs.getConnection()); 

		StringBuilder nextS = new StringBuilder(); 
		
		while(rs.getNext()) { 
				if(rs.getString("db").equals(DATABASE_NAME) && !"Sleep".equals(rs.getString("Command"))) { 
					String next = rs.getString("Info"); 
					nextS.delete(0,nextS.length());
					
					for(int i=0; i<next.length(); i++) { 
						char c = next.charAt(i); 
						if(c != " ".charAt(0) && c != '\r' && c != '\n') { 
							nextS.append(c); 
						}
						if(nextS.length()>10) { 
							break;
						}
					}
					
					try { 
						if(Integer.parseInt(rs.getString("Time")) > DEAD_TIME && nextS.toString().toUpperCase().startsWith(SELECT_STRING)) { 
							exec.setSearchStatement("kill "+rs.getString("Id")); 
							exec.setAction(ActionBean.EXECUTE);
							exec.doAction(); 
							
							log.clear(); 
							log.setAction(GenerationKeys.INSERT); 
							log.createNewID(); 
							log.setParameter("CreatedDate","NOW"); 
							log.parseRequest(rs.toString()); 
							log.doAction(); 
							
							kills++; 
						}
					} catch (NumberFormatException nfe) { }
				} 
		} 
		
		rs.close();
		
	}
}
