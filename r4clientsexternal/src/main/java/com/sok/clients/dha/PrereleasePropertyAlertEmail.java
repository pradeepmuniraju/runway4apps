package com.sok.clients.dha;

import java.sql.Connection;
import java.util.ArrayList;

import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.runway.crm.Contact;
import com.sok.runway.crm.Product;
import com.sok.runway.security.User;

public class PrereleasePropertyAlertEmail extends PropertyAlertEmail {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/** ensure that products are printed without links */ 
	protected void writeHTMLProduct(Product product, String contextPath,
			String sitePath, String noteID) {
		writeHTMLProduct(product, contextPath, sitePath, noteID, null);
	}
	/** ensure that products are printed without links */
	protected void writeTextProduct(Product product, String contextPath,
			String sitePath, String noteID) {
		writeTextProduct(product, contextPath, sitePath, noteID, null);
	}

	protected void writeHTMLBody(Contact contact, User repUser) { 
		write("<p>The following properties which match your investment criteria are due for release this Thursday at 12 noon (AEST).</p>\r\n");
		write("<p>Please email or call me to:"); 
		write("<ul style=\"text-align: left\"><li>advise of properties you are interested in placing 'on hold', or</li>"); 
		write("<li>obtain more detailed information.</li></ul></p>");
		write("<p>Note, due to current high demand, you may wish to detail preferences of several properties you are interested in. "); 
		write("I cannot guarantee the ability to place a property 'on hold' for you, however will do my best to secure you one as soon as possible.</p>");
	}
	
	protected void writeTextBody(Contact contact, User repUser) { 
		write("The following properties which match your investment criteria are now available.\r\n\r\n");
		write("If you would like more information about these or other DHA investment properties please email me at ");
		write(contact.getField("RepEmail"));
		write(" or call ");
		write(repUser.getField("Phone"));
		write(".\r\n\r\n");
	}
	/*
	protected void writeHTMLSignature(Contact contact, User repUser) { 
		super.writeHTMLSignature(contact, repUser); 
		
	}
	
	protected void writeTextSignature(Contact contact, User repUser) { 
		super.writeTextSignature(contact, repUser);
	}*/
	
	/** prerelease product load method */
	protected synchronized void loadProducts(Connection con) {

		this.productList = new ArrayList<Product>();
		GenRow products = new GenRow();
		products.setConnection(con);
		products.setViewSpec("ProductIDView");
		products.setParameter("Active", "Y");
		products.setParameter("AvailableDate", "NEXT3DAYSEXCEPT2DAYS");
		products.setParameter("-join1","ProductCurrentStatus.ProductStatusList");
		products.setParameter("ProductCurrentStatus.ProductStatusList.Status","Prerelease");
		products.setParameter("-join2", "ProductValuationExpiryAnswer");
		products.setParameter("ProductValuationExpiryAnswer.AnswerDate","!OVERDUE");
		products.setParameter("-join3", "ProductListingTypeAnswer"); // f
		products.sortBy("ProductListingTypeAnswer.Answer", 0);
		products.sortOrder("DESC", 0);
		products.sortBy("ProductStatusList.Status", 1);
		products.sortOrder("DESC", 1);
		products.doAction(ActionBean.SEARCH);
		products.getResults();
		Product product = null;

		while (products.getNext()) {
			product = new Product(con);
			product.load(products.getString("ProductID"));
			if (product.isLoaded()) {
				product.loadProfiles();
				productList.add(product);
			}
		}

	}
}
