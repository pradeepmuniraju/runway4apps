package com.sok.clients.dha;

import java.sql.Connection;
import javax.servlet.ServletContext;
import com.sok.Debugger;
import com.sok.framework.ActionBean;
import com.sok.framework.GenRow;
import com.sok.runway.autopilot.AbstractActionTypeEmail;
import com.sok.runway.autopilot.AutoPilotSchedule;
import com.sok.runway.crm.Note;
import com.sok.runway.RunwayEntity; 
import com.sok.runway.SessionKeys;
import com.sok.runway.security.User; 

import com.sok.framework.generation.GenerationKeys;
import com.sok.framework.servlet.HttpSession;

public class EventListEmail extends AbstractActionTypeEmail {
	
	GenRow searchbean = null; 
	protected HttpSession session = null; //need to have own session, don't have access to autopilots
	
	Debugger d = null; 
	
	public boolean needsSending() {		
		return true; 
	}
	
	public void init(ServletContext context, AutoPilotSchedule schedule, Connection con) {
		this.session = new HttpSession(context);  
		this.d = Debugger.getDebugger(); 		
		super.init(context, schedule, con); 
	}
	
	public void setSpecificEmailTokens(String id, GenRow search) {
				
		search.doAction(ActionBean.SEARCH);
        search.getResults();
		searchbean = search;
	}
	
	public boolean generateEmail(String noteID) {
		
		getAutoPilotSchedule().getAutoPilot().getSession().setAttribute(SessionKeys.NOTE_SEARCH, searchbean);		

        setEmailFrom("info@switched-on.com.au");
        
                
        try { 
        	
        	write(getAutoPilotSchedule().getAutoPilot().generateEmail(getAutoPilotSchedule(), "/specific/autopilot/eventlist.jsp"));
        	
        } catch (Exception e) {
        	com.sok.Debugger.getDebugger().debug("In exception:");
            session.setAttribute(SessionKeys.NOTE_SEARCH, null);
        	write("Exception thrown on generation: "+e.toString());
        	return false; 
        }
        
        return true;		
	}	
}