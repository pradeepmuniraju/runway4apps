package com.sok.clients.dha;

import com.sok.framework.ImageUtil; 
import java.io.File; 

/**
	this was built to handle automatic conversions of images to standard sizes. 
	The problem with floorplans is that we do not have a standardised format to start with, 
	so some portrait images may end up being upside down. 
*/ 
public class ImageConverter {
	
	public static final int FLOORPLAN = 1;
	public static boolean FORCE = false;
	
	private ImageUtil img; 
	private String oldPath; 
	private String newPath; 
	
	
   public static void main(String[] args)
   {
	   String path = "G:\\TEMP\\img\\3149299_floorplan.JPG";
	   ImageConverter ic = new ImageConverter(FLOORPLAN, path); 
	   ic.flip(FLOORPLAN); 
	   
   } 
	public ImageConverter(int type, String imagePath) { 
		
		oldPath = imagePath; 
		newPath = makePath(oldPath); 
		File of = new File(oldPath); 
		File f = new File(newPath);
		if((!f.exists() && of.exists()) || FORCE) { 
			img = new ImageUtil(imagePath, of);
			img.setAllowEnlarge(true); 
			convert(type); 
		} 
	}
	
	public void convertImage(int type, String imagePath) {
		oldPath = imagePath;
		newPath = makePath(oldPath);
		File of = new File(oldPath); 
		File f = new File(newPath);
		if((!f.exists() && of.exists()) || FORCE) { 
			newPath = null; 
			img.loadImage(imagePath, of); 
			convert(type); 
		} 
	}
	
	public void flip(int type) { 
		img = new ImageUtil(newPath);
		img.setAllowEnlarge(true);
		convert(type, true); 
	}
	
	public String getNewPath() { 
		return newPath; 
	}
	
	private void convert(int type) { 
		convert(type, false); 
	}
	
	private void convert(int type, boolean flip) { 
		switch (type) { 
			case FLOORPLAN: 
				makeFloorplan(flip); 
				break; 
			default: 
				throw new RuntimeException("Unsupported Image Type"); 
		} 
	}
	
	private void makeFloorplan(boolean flip) { 
	   
		if(img.isPortrait()) { 
			img.rotate(flip?180:90);  
		}
		img.resizeToWidth(600);
		img.setJpegQuality(0.80f); 
		img.saveImage(makePath(oldPath));
	}
	
	public static String makePath(String p) { 
		return p.substring(0,p.lastIndexOf('.'))+"_web"+p.substring(p.lastIndexOf('.'));
	}
}
