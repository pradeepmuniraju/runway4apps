/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sok.clients.dha;

import com.sok.runway.autopilot.AbstractActionTypeAppointment;
import com.sok.runway.autopilot.AutoPilot;
import com.sok.runway.crm.Contact;

/**
 *
 * @author emil
 */
public class AppointmentAlert extends AbstractActionTypeAppointment {

  
   private static final String EMAIL_TYPE_QUESTION_ID = "0L1A1K8H611X2B3W7K9S51798M4W";
   
   public boolean needsSending() {
  
      
      
      //return (productList != null && productList.size() > 0);      
      
      throw new UnsupportedOperationException("Not supported yet.");
   }
   
   public boolean generateEmail(String noteID) {

//      super.generateEmail(noteID);
      if (getEntity().isLoaded() && getEntity() instanceof Contact) {
         if (AutoPilot.EMAIL_TYPE_PLAIN_TEXT.equals(((Contact)getEntity()).getProfileField(EMAIL_TYPE_QUESTION_ID))) {
            return generateTextEmail(noteID);
         }
         else {
            return generateHTMLEmail(noteID);
         }
      }
      else {
         return false;
      }
   }

   public boolean generateSMS(String noteID) {
      write("Please remember your appointment on: "+note.getField("NoteDate")+"\r\n");
      write("Time: "+note.getField("NoteTime")+"\r\n");
      write("Subject: "+note.getField("Subject")+"\r\n");
      write("Venue: "+note.getField("Venue")+"\r\n");
      
      return true;
   }   
   
   public boolean generateHTMLEmail(String noteID) {
      
         write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n");
         write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n");
         write("<head>\r\n");
         write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\r\n");        
         write("</head>\r\n");
         write("<body>\r\n");
         write("Please remember your appointment on "+note.getField("NoteDate")+"<br/>\r\n");
         write("Time: "+note.getField("NoteTime")+"<br/>\r\n");
         write("Subject: "+note.getField("Subject")+"<br/>\r\n");
         write("Venue: "+note.getField("Venue")+"<br/>\r\n");
         write("</body>\r\n");
         write("</html>\r\n");
         
      return true;
   }

   public boolean generateTextEmail(String noteID) {
      write("Please remember your appointment on "+note.getField("NoteDate")+"\r\n");
      write("Time: "+note.getField("NoteTime")+"\r\n");
      write("Subject: "+note.getField("Subject")+"\r\n");
      write("Venue: "+note.getField("Venue")+"\r\n");
      
      return true;
   }
   
   

}
