package com.sok.clients.dha;

import com.sok.framework.*;
import com.sok.runway.autopilot.*;
import com.sok.runway.crm.*;
import com.sok.runway.security.*;

import java.sql.*;
import java.io.*;
import java.util.*;
import java.util.Date;

import javax.servlet.ServletContext;

public class PropertyAlertEmail extends AbstractActionTypeEmail {

	private static final String EMAIL_TYPE_QUESTION_ID = "0L1A1K8H611X2B3W7K9S51798M4W";

	protected static final String INACTIVE_USER_ID = "0G161V8S63415B762R2M7K8K9S5N";

	private static final String LEASE_QUESTION_ID = "0S1Y158M2A961C0X7E7D027H940K";
	private static final String OPTION_QUESTION_ID = "0F1U1H8X2L9D1T1Z3J7N2D6W4S2X";
	private static final String RENT_QUESTION_ID = "0M1K1Y8L2W9W1109555O757K025I";
	private static final String BEDROOMS_QUESTION_ID = "0C1C1O822S9B1I1S6J1O2T455X65";
	private static final String BATHROOMS_QUESTION_ID = "0U1K1G8G2293141H6Q295F9V6P0Q";
	private static final String PARKING_QUESTION_ID = "0O1X1N8023901E1M7L02158I2V8Z";

	private static final String SUBURB_QUESTION_ID = "0P1C1H8Q2Y991G721G0A2B8Z8F1Q";
	private static final String REGION_QUESTION_ID = "0E1J1D8C279L1N0C4N1D4F066T3C";
	private static final String STATE_QUESTION_ID = "0H13128424981E7D191B8Q0X4925";

	private static final String PROPERTY_TYPE_QUESTION_ID = "011Q1R8E2X961C175K0T6H30942G";

	private static final String CONTACT_PROPERTY_UPDATES_QUESTION_ID = "0Q1X1A8H38079O605Q653C5O5L5P";

	private static final String CONTACT_PROPERTY_TYPE_QUESTION_ID = "0T121O88370F9B5K682G7W5J2D10";
	private static final String CONTACT_PRICE_RANGE_QUESTION_ID = "0J1617853D0G9U5H6D560Y669J55";

	private static final String CONTACT_LOCATION_ALL_QUESTION_ID = "00131W85401Y3H2R7J0L1R9X5J1B";
	private static final String[] CONTACT_LOCATION_QUESTION_IDS = {
			"001R148U3D0G9N5999372D4U6377", "0V181O8D3A0Y9H5H898I9T40563J",
			"0M131F823U0Z9F5C9L111A8V846I", "0G1I1T813J0L9Y6J0W1G7T1R4I2Z",
			"0010198D3D0Y9Z6N083E8Y6K755U", "0I1O108B3G009R5R9G9H3R0Z8M16",
			"04141Y8N3H0M9C5Z9T7J1U6K2645", "0J1C178A33019C5B9F55369U5K5W" };

	protected ArrayList<Product> productList = null;
	protected java.text.NumberFormat nf = java.text.NumberFormat.getCurrencyInstance(); 
	

	protected GenRow update = null;
	protected Date d = null;

	public PropertyAlertEmail() {
	}

	public void init(ServletContext context, AutoPilotSchedule schedule,
			Connection con) {
		super.init(context, schedule, con);

		loadProducts(con);
		d = new Date();

		update = new GenRow();
		update.setConnection(con);
		update.setTableSpec("Contacts");
		update.setParameter("FollowUpDate", "NOW");
		update.setAction("update");

	}

	public boolean needsSending() {
		return (productList != null && productList.size() > 0);
	}

	public boolean generateEmail(String noteID) {
		if (getEntity() instanceof Contact && getEntity().isLoaded()) {

			if (AutoPilot.EMAIL_TYPE_PLAIN_TEXT.equals(((Contact) getEntity())
					.getProfileField(EMAIL_TYPE_QUESTION_ID))) {
				return generateTextEmail(noteID);
			} else {
				return generateHTMLEmail(noteID);
			}
		} else {
			return false;
		}
	}

	public boolean generateHTMLEmail(String noteID) {
		super.generateHTMLEmail(noteID);

		String contextPath = getContextPath();
		String sitePath = contextPath + "/cms/websites/dha";

		com.sok.runway.autopilot.AutoPilotSchedule schedule = getAutoPilotSchedule();

		// only called if entity instanceof Contact, no need to check again.
		Contact contact = (Contact) getEntity();

		Date cd = contact.getDate("FollowUpDate");

		if (d != null && cd != null) {

			if ((d.getTime() - cd.getTime()) < 64800000 /* 18 hours */) {
				return false;
			}
		}

		ArrayList<Product> matchingProductList = getMatchingProducts(contact);

		boolean productsFound = matchingProductList != null
				&& matchingProductList.size() > 0;

		if (productsFound) {
			if (contact.isLoaded()) { // Just to be sure..
				update.setParameter("ContactID", contact.getPrimaryKey());
				update.doAction();
			}

			write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n");
			write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n");
			write("<head>\r\n");
			write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\r\n");
			write("<title>Defence Housing Australia</title>\r\n");
			write("<style>\r\n");
			write("body { padding:0; margin:0; font:11px Verdana, Arial, Helvetica, sans-serif; color:#1e2f5d; text-align:center; background:url(");
			write(sitePath);
			write("/images/body_bg.jpg) repeat-x; background-color:#5E80AB; vertical-align:top; }\r\n");
			write("p { margin:0; padding:4px 15px 8px 15px; text-align:left; }\r\n");
			write("a { color:#1e2f5d; text-decoration:none; }\r\n");
			write("a:hover { color:#85bce1; }\r\n");
			write("a img { border:none; }\r\n");
			write("ul { list-style:url(.");
			write(sitePath);
			write("/images/bullet_propertydetails.gif); }\r\n");
			write("li { padding:0 0 5px 0; }\r\n");
			write("\r\n");
			write("h1 { font:8px Verdana, Arial, Helvetica, sans-serif; color:#fff; font-weight:bold; margin:0 4px 0 4px; padding:0 6px 0 6px; background:#ec1d26; }\r\n");
			write("h2 { font-size:6px; margin:0 auto; margin:0 7px 5px 7px; padding:0; border-bottom:1px dotted #85bce1; }\r\n");
			write("h3 { font:22px Verdana, Arial, Helvetica, sans-serif bold; color:#1e2f5d; margin:0; padding:0; text-align:left; top:-3px; position:relative; }\r\n");
			write("h4 { font:12px Verdana, Arial, Helvetica, sans-serif; color:#fff; font-weight:bold; margin:0 auto; margin:8px 5px 5px 5px; padding:4px 7px 4px 7px; text-align:left; background:#ec1d26; }\r\n");
			write("h5 { font:11px Verdana, Arial, Helvetica, sans-serif; color:#1e2f5d; font-weight:bold; margin:0; padding:0; text-align:left; }\r\n");
			write("h6 { font:11px Verdana, Arial, Helvetica, sans-serif; color:#fff; font-weight:bold; margin:7px 0 0 0; padding:3px 6px 3px 6px; text-align:right; background:#ec1d26; }\r\n");
			write("\r\n");
			write(".contentHeaderContainer { width:523px; height:66px; background:url(");
			write(sitePath);
			write("/images/contentHeader_bg.jpg) top repeat-x; margin: 0 auto; text-align:center;}\r\n");
			write("td.contentHeader_l { width:30px; height:66px; background:url(");
			write(sitePath);
			write("/images/contentHeader_bg_l.jpg) top left no-repeat; }\r\n");
			write("td.contentHeader_r { width:2px; height:66px; background:url(");
			write(sitePath);
			write("/images/contentHeader_bg_r.jpg) top right no-repeat; }\r\n");
			write("\r\n");
			write(".subContent { width:523px; margin:13px 0 0 0; margin: 0 auto; text-align:center;}\r\n");
			write(".subContentTitles { width:521px; height:25px; border:1px solid #85bce1; background:url(");
			write(sitePath);
			write("/images/subContent_bg.jpg) repeat-x; }\r\n");
			write("td.subContentTitles_l { width:20px; height:25px; background:url(");
			write(sitePath);
			write("/images/subContent_bg_l.jpg) no-repeat; }\r\n");
			write("td.subContentTitles_r { width:25px; height:25px; }\r\n");
			write("\r\n");
			write(".subContentDetails { width:521px; margin:10px 0 0 0; }\r\n");
			write("td.subContentDetails_l { width:182px; vertical-align:top; text-align:right; }\r\n");
			write(".subContentDetails_l a { text-decoration:none; display:block; color:#1e2f5d; }\r\n");
			write(".subContentDetails_l a:hover { color:#2F64BB; }\r\n");
			write(".subContentDetails_l a img { margin:0 0 -6px 0; position:relative; border:none; }\r\n");
			write("td.subContentDetails_r { padding:0 10px 0 0; vertical-align:top; padding:0 0 0 11px; text-align:left; }\r\n");
			write(".subContentDetails_r p { margin:0; padding:10px 5px 0 5px; }\r\n");
			write("\r\n");
			write(".propertyListPanel { width:328px; height:54px; border:1px solid #85bce1; background:url(");
			write(sitePath);
			write("/images/propertyListPanel_bg.jpg) repeat-x; }\r\n");
			write("\r\n");
			write(".propertyIcons { text-align:right; padding:4px 4px 15px 4px; }\r\n");
			write(".propertyIcons img { border:none; padding:4px 0 0 3px; }\r\n");
			write("\r\n");
			write("td.viewDetailsBtn { width:102px; height:54px; background:url(");
			write(sitePath);
			write("/images/propertyListPanel_btn_bg.jpg) no-repeat; vertical-align:top; }\r\n");
			write("td.viewDetailsBold { font:11px Verdana, Arial, Helvetica, sans-serif; font-weight:bold; padding:0 8px 0 7px; }\r\n");
			write("\r\n");
			write("#imageContainer { position:relative; padding:0; margin:0; border-left:1px solid #85bce1; border-right:1px solid #85bce1;border-top:1px solid #85bce1; background-color:#85bce1; }\r\n");
			write("#soldBanner { position:absolute; margin:0 auto; padding:0; z-index:1000; top:0; left:0; display:block; }\r\n");
			write("\r\n");
			write("td.shellBorder_l { background:url(");
			write(sitePath);
			write("/images/pageShadow_bg_l.gif) repeat-y; width:8px; vertical-align:top; }\r\n");
			write("td.shellBorder_r { background:url(");
			write(sitePath);
			write("/images/pageShadow_bg_r.gif) repeat-y; width:8px; vertical-align:top; }\r\n");
			write("td.shellBorder_bl { background:url(");
			write(sitePath);
			write("/images/pageShadow_bg_bl.gif) no-repeat; width:8px; height:8px; }\r\n");
			write("td.shellBorder_b { background:url(");
			write(sitePath);
			write("/images/pageShadow_bg_b.gif) repeat-x; height:8px; }\r\n");
			write("td.shellBorder_br { background:url(");
			write(sitePath);
			write("/images/pageShadow_bg_br.gif) no-repeat; width:8px; height:8px; }\r\n");
			write("\r\n");
			write("td.headerLogo_l { height:41px; text-align:left; background:url(");
			write(sitePath);
			write("/images/popup/pop_dha_logo.gif) left no-repeat; }\r\n");
			write("td.headerLogo_r { height:41px; text-align:right; background:url(");
			write(sitePath);
			write("/images/popup/pop_invest_logo.gif) right no-repeat; }\r\n");
			write("table.buttons td { text-align:center; }\r\n");
			write("div.footer { width:521px; border-top:1px solid #85bce1; padding:3px 0 0 0; margin:0 auto; text-align:center; }\r\n");
			write("div.copyright { width:490px; text-align:left; height:22px; padding:3px 0 0 8px; margin:0 auto; background:url(");
			write(sitePath);
			write("/images/img_133DHA.gif) top right no-repeat; font:10px Verdana, Arial, Helvetica, sans-serif; color:#999; }\r\n");
			write("div.optOut { font:9px Verdana, Arial, Helvetica, sans-serif normal; color:#0D2E8E; text-align:center; padding:0 0 10px 0; }\r\n");
			write("\r\n");
			write("</style>\r\n");
			write("</head>\r\n");
			write("<body>\r\n");

			write("\r\n");
			write("<table width=\"550\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"align=\"center\" bgcolor=\"#FFFFFF\">\r\n");
			write("\r\n");
			write("\r\n");
			write("<tr>\r\n");
			write("<td class=\"shellBorder_l\"><img src=\"");
			write(sitePath);
			write("/images/pageShadow_l.jpg\" width=\"8\" height=\"735\" /></td>\r\n");
			write("<td valign=\"top\">\r\n");
			write("<div class=\"optOut\"><a href=\"");
			write(contextPath);
			write("/cms/emails/shared/notehtmlview.jsp?ID=");
			write(noteID);
			write("&version=html\">Having trouble viewing this email? ~ Click here to view as a webpage</a></div>\r\n");
			write("\r\n");
			write("<div>\r\n");
			write("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
			write("<tr>\r\n");
			write("<td class=\"headerLogo_l\">&nbsp;</td>\r\n");
			write("<td class=\"headerLogo_r\">&nbsp;</td>\r\n");
			write("</tr>\r\n");
			write("</table>\r\n");
			write("</div>\r\n");
			write("<h1>&nbsp;</h1>\r\n");
			write("<h2>&nbsp;</h2>\r\n");
			write("<p>Dear ");
			if (contact.getField("FirstName").length() >= 2) {
				write(contact.getField("FirstName"));
			} else {
				write("Investor");
			}
			write("</p>\r\n");
			write("\r\n");

			User repUser = contact.getRepUser();

			if (repUser != null
					&& !repUser.getPrimaryKey().equals(INACTIVE_USER_ID)) {

				// setEmailFrom(contact.getField("RepEmail"));
				setEmailFrom(contact.getField("RepFirstName") + " "
						+ contact.getField("RepLastName")
						+ " <automated.dha@runway.net.au>");
				setEmailReplyTo(contact.getField("RepEmail"));

				write("\r\n");
				writeHTMLBody(contact, repUser); 	
				
				write("<p>Kind regards</p>\r\n");
				write("\r\n");
				
				/* signature block */ 
				writeHTMLSignature(contact, repUser); 
				/* signature block */ 
			} else {
				// setEmailFrom("sales@dha.gov.au");
				setEmailFrom("DHA Sales <automated.dha@runway.net.au>");
				setEmailReplyTo("sales@dha.gov.au");

				write("\r\n");
				write("<p>The following properties which match your investment criteria are now available.</p>\r\n");
				write("<p>If you would like more information about these or other DHA investment properties please <a href=\"mailto:sales@dha.gov.au\">email</a> me or call 133 DHA.</p>\r\n");
				write("<p>Kind regards</p>\r\n");
				writeHTMLSignature(contact, null);
			}

			write("\r\n");
			write("\r\n");
			write("\r\n");
			write("<h2>&nbsp;</h2>\r\n");
			write("<table class=\"buttons\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>\r\n");
			/* contact */
			write("<td><a href=\"");
			write(contextPath);
			/*
			write("/dha/home/contact.sok?ID=");
			write(noteID);
			*/
			write("/cms/websites/dha/actions/dha_emailagentaction.jsp?-requestedContact=true&CID="); 
			write(contact.getField("ContactID")); 
			write("\"><img src=\"");
			write(contextPath);
			write("/specific/autopilot/propertysendimages/btn_contactdha.jpg\" alt=\"Contact DHA\" width=\"165\" height=\"57\" border=\"0\" /></a></td>\r\n");
			/* end contact */
			/* website */
			write("<td><a href=\"");
			write(contextPath);
			write("/dha/home.sok?ID=");
			write(noteID);
			write("\"><img src=\"");
			write(contextPath);
			write("/specific/autopilot/propertysendimages/btn_visitwebsite.jpg\" alt=\"Visit DHA\" width=\"165\" height=\"57\" border=\"0\" /></a></td>\r\n");
			/* end website */
			/* update */
			write("<td><a href=\"");
			write(contextPath);
			write("/dha/update.jsp?ID=");
			write(noteID);
			write("\"><img src=\"");
			write(contextPath);
			write("/specific/autopilot/propertysendimages/btn_updatedetails.jpg\" alt=\"Update your details\" width=\"165\" height=\"57\" border=\"0\" /></a></td>\r\n");
			/* end update */
			write("</tr></table>\r\n");
			write("<h2>&nbsp;</h2>\r\n");
			write("<h4>Your property alert</h4>\r\n");
			write("\r\n");
			write("<div class=\"subContent\">\r\n");

			for (Product product : matchingProductList) {
				writeHTMLProduct(product, contextPath, sitePath, noteID);
			}
			matchingProductList = null;

			write("\r\n");
			write("</td>\r\n");
			write("<td class=\"shellBorder_r\"><img src=\"");
			write(sitePath);
			write("/images/pageShadow_r.jpg\" width=\"8\" height=\"735\" /></td>\r\n");
			write("</tr>\r\n");
			write(" \r\n");
			write("<!-- START footer ADDED !-->\r\n");
			write("<tr>\r\n");
			write("<td class=\"shellBorder_l\"></td>\r\n");
			write("<td align=\"center\">\r\n");
			write("<div class=\"footer\">\r\n");
			write("<div class=\"copyright\">&copy; Copyright Defence Housing Australia 2009</div>\r\n");
			write("</div>\r\n");
			write("</td>\r\n");
			write("<td class=\"shellBorder_r\"></td>\r\n");
			write("</tr>\r\n");
			write("<!-- END footer ADDED !-->\r\n");
			write("\r\n");
			write("<tr>\r\n");
			write("<td class=\"shellBorder_bl\"><img src=\"");
			write(contextPath);
			write("/images/pixel.gif\" width=\"1\" height=\"1\" /></td>\r\n");
			write("<td class=\"shellBorder_b\"><img src=\"");
			write(contextPath);
			write("/images/pixel.gif\" width=\"1\" height=\"1\" /></td>\r\n");
			write("<td class=\"shellBorder_br\"><img src=\"");
			write(contextPath);
			write("/images/pixel.gif\" width=\"1\" height=\"1\" /></td>\r\n");
			write("</tr>\r\n");
			write("</table>\r\n");
			write("<div class=\"optOut\"><a href=\"");
			write(contextPath);
			write("/dha/optout.jsp?ID=");
			write(noteID);
			write("\">Click here to opt-out of future communications from Defence Housing Australia</a></div>\r\n");
			write("\r\n");
			write("<img src=\"");
			write(contextPath);
			write("/RESPONSE/");
			write(noteID);
			write("/Viewed_Email.gif\" width=\"1\" height=\"1\">\r\n");
			write("</body>\r\n");
			write("</html>\r\n");
		}

		return productsFound;
	}
	
	protected void writeHTMLBody(Contact contact, User repUser) { 
		/* copy changes 12/8/09
		write("<p>The following properties which match your investment criteria are now available.</p>\r\n");
		write("<p>If you would like more information about these or other DHA investment properties please <a href=\"mailto:");
		write(contact.getField("RepEmail"));
		write("\">email</a> me or call ");
		write(repUser.getField("Phone"));
		write(".</p>");
		*/
		write("<p>The following properties which match your investment criteria were released for sale today.</p>");
		write("<p>Properties marked as 'on hold' were reserved for investors. DHA has chosen to display these properties to show you the type, quality and range of stock typically offered.</p>");
		write("<p>Please email or call me to indicate interest in placing a property 'on hold' now or in the immediate future, or to obtain more detailed information.</p>");
	}
	
	protected void writeHTMLSignature(Contact contact, User repUser) { 
		
		if(repUser == null) { 
			write("<p><b>Defence Housing Australia</b></p>\r\n");
		} else { 
			
			write("<p><strong>");
			write(contact.getField("RepFirstName"));
			write(" ");
			write(contact.getField("RepLastName"));
			write("<br/>\r\n");
			/*
			 * insert company here if required. 
			 */
			
			if (repUser.getField("Position").length() == 0) {
				write("Contracted Sales Consultant");
			} else {
				write(repUser.getField("Position"));
			}
			write("</strong></p>\r\n");
	
			Setup setup = repUser.getSetup();
			if (setup != null) {
				write("<p>");
				write(setup.getField("Street"));
				write(" ");
				write(setup.getField("Street2"));
				write("<br/>");
				write(setup.getField("City"));
				write(" ");
				write(setup.getField("State"));
				write(" ");
				write(setup.getField("Postcode"));
				/*
				 * if
				 * (!setup.getField("Street").equals(setup.getField("PostalStreet"))) {
				 * write("<br>"); write(setup.getField("PostalStreet"));
				 * write(" "); write(setup.getField("PostalStreet2"));
				 * write(" "); write(setup.getField("PostalCity")); write("
				 * "); write(setup.getField("PostalState")); write(" ");
				 * write(setup.getField("PostalPostcode")); }
				 */
				write("</p>\r\n");
			}
	
			write("<p>");
			if (repUser.getField("Phone").length() != 0) {
				write("T: ");
				write(repUser.getField("Phone"));
				write("<br/>");
			}
			if (repUser.getField("Mobile").length() != 0) {
				write("M: ");
				write(repUser.getField("Mobile"));
				write("<br/>");
			}
			if (repUser.getField("Fax").length() != 0) {
				write("F: ");
				write(repUser.getField("Fax"));
				write("<br/>");
			}
			if (repUser.getField("Email").length() != 0) {
				write("E: ");
				write(repUser.getField("Email"));
				write("<br/>");
			}
			write("</p>\r\n");
		}
		write("<p>&nbsp;</p>");
		write("<p>Attention: Investment in a DHA property is subject to the terms of the lease and those terms take precedence over the information contained in this email and any attachments. Investors retain some responsibilities and risks. Before purchasing a DHA investment property you should: i) seek independent advice; ii) make (and rely upon) your own assessment and enquiries; and iii) ensure you understand the terms of the lease. DHA endeavours to ensure that information is accurate at the date of publication. However, errors and changes may occur. To the extent permitted by law, DHA will not be liable for any loss, damage, cost or expense incurred or arising by reason of any person relying on the information in this email message or any attached files.</p>\r\n");
	}
	
	protected void writeTextBody(Contact contact, User repUser) { 
		/* copy changes 12/08/2009
			write("The following properties which match your investment criteria are now available.\r\n\r\n");
			write("If you would like more information about these or other DHA investment properties please email me at ");
			write(contact.getField("RepEmail"));
			write(" or call ");
			write(repUser.getField("Phone"));
			write(".\r\n\r\n");
		*/
		write("The following properties which match your investment criteria were released for sale today.\r\n\r\n");
		write("Properties marked as 'on hold' were reserved for investors. DHA has chosen to display these properties to show you the type, quality and range of stock typically offered.\r\n\r\n");
		write("Please email or call me to indicate interest in placing a property 'on hold' now or in the immediate future, or to obtain more detailed information.\r\n\r\n");
	}
	
	protected void writeTextSignature(Contact contact, User repUser) { 
		if(repUser == null) { 
			write("Defence Housing Australia\r\n");
		} else { 
			write(contact.getField("RepFirstName"));
			write(" ");
			write(contact.getField("RepLastName"));
			write("\r\n");
	
			if (repUser.getField("Position").length() == 0) {
				write("Contracted Sales Consultant\r\n\r\n");
			} else {
				write(repUser.getField("Position"));
				write("\r\n\r\n");
			}
	
			Setup setup = repUser.getSetup();
			if (setup != null) {
				write(setup.getField("Street"));
				if (setup.getField("Street2").length() != 0) {
					write(" ");
					write(setup.getField("Street2"));
				}
				write("\r\n");
				if (setup.getField("City").length() != 0) {
					write(" ");
					write(setup.getField("City"));
				}
				if (setup.getField("State").length() != 0) {
					write(" ");
					write(setup.getField("State"));
				}
				if (setup.getField("Postcode").length() != 0) {
					write(" ");
					write(setup.getField("Postcode"));
				}
	
				/*
				 * if
				 * (!setup.getField("Street").equals(setup.getField("PostalStreet"))) {
				 * write("\r\n"); write(setup.getField("PostalStreet")); if
				 * (setup.getField("PostalStreet2").length() != 0) { write("
				 * "); write(setup.getField("PostalStreet2")); } if
				 * (setup.getField("PostalCity").length() != 0) { write("
				 * "); write(setup.getField("PostalCity")); } if
				 * (setup.getField("PostalState").length() != 0) { write("
				 * "); write(setup.getField("PostalState")); } if
				 * (setup.getField("PostalPostcode").length() != 0) {
				 * write(" "); write(setup.getField("PostalPostcode")); } }
				 */
				write("\r\n\r\n");
			}
	
			if (repUser.getField("Phone").length() != 0) {
				write("T: ");
				write(repUser.getField("Phone"));
				write("\r\n");
			}
			if (repUser.getField("Mobile").length() != 0) {
				write("M: ");
				write(repUser.getField("Mobile"));
				write("\r\n");
			}
			if (repUser.getField("Fax").length() != 0) {
				write("F: ");
				write(repUser.getField("Fax"));
				write("\r\n");
			}
			if (repUser.getField("Email").length() != 0) {
				write("E: ");
				write(repUser.getField("Email"));
				write("\r\n");
			}
		}
		write("\r\n\r\n"); 
		write("\r\nAttention: Investment in a DHA property is subject to the terms of the lease and those terms take precedence over the information contained in this email and any attachments. Investors retain some responsibilities and risks. Before purchasing a DHA investment property you should: i) seek independent advice; ii) make (and rely upon) your own assessment and enquiries; and iii) ensure you understand the terms of the lease. DHA endeavours to ensure that information is accurate at the date of publication. However, errors and changes may occur. To the extent permitted by law, DHA will not be liable for any loss, damage, cost or expense incurred or arising by reason of any person relying on the information in this email message or any attached files.\r\n");
	}

	protected void writeHTMLProduct(Product product, String contextPath,
			String sitePath, String noteID) {
		writeHTMLProduct(product, contextPath, sitePath, noteID,
				"/dha/home/forsale/search/propertydetails.sok?ProductID=");
	}

	protected void writeHTMLProduct(Product product, String contextPath,
			String sitePath, String noteID, String viewProductPath) {

		write("\r\n");
		write("<div class=\"subContentTitles\">\r\n");
		write("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>\r\n");
		write("<td class=\"subContentTitles_l\"></td>\r\n");
		write("<td><h5>");
		write(product.getProfileField(SUBURB_QUESTION_ID));
		write(",\r\n");
		write(product.getProfileField(REGION_QUESTION_ID));
		write(",\r\n");
		write(product.getProfileField(STATE_QUESTION_ID));
		write("&nbsp;</h5></td>\r\n");
		write("</tr></table>\r\n");
		write("</div>\r\n");
		write("\r\n");
		write("<div class=\"subContentDetails\">\r\n");
		write("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>\r\n");
		write("<td class=\"subContentDetails_l\">\r\n");

		write("\r\n");

		write("<div id=\"imageContainer\">");
		if (product.getField("ImagePath").length() != 0) {
			write("<img src=\"");
			write(contextPath);
			write("/");
			write(product.getField("ImagePath"));
			write("\" width=\"180\" height=\"120\" />");
		}
		write("<img src=\"");
		write(contextPath);
		write("/ONHOLD/");
		write(product.getField("ProductID"));
		write("/image.gif\" />");
		write("</div>\r\n");

		write("\r\n");
		write("<h6>Property ID# ");
		write(product.getField("ProductNum"));
		write("</h6>\r\n");
		write("<div class=\"propertyIcons\">\r\n");
		write("<img src=\"");
		write(sitePath);
		write("/images/icons/propertyDetails_numIcon_");
		write(product.getProfileField(BEDROOMS_QUESTION_ID));
		write(".gif\" alt=\"");
		write(product.getProfileField(BEDROOMS_QUESTION_ID));
		write("\" width=\"11\" height=\"16\" /><img src=\"");
		write(sitePath);
		write("/images/icons/propertyDetails_icon_bedrooms.gif\" width=\"33\" height=\"16\" />\r\n");
		write("<img src=\"");
		write(sitePath);
		write("/images/icons/propertyDetails_numIcon_");
		write(product.getProfileField(BATHROOMS_QUESTION_ID));
		write(".gif\" alt=\"");
		write(product.getProfileField(BATHROOMS_QUESTION_ID));
		write("\" width=\"11\" height=\"16\" /><img src=\"");
		write(sitePath);
		write("/images/icons/propertyDetails_icon_bathrooms.gif\" width=\"33\" height=\"16\" />\r\n");
		write("<img src=\"");
		write(sitePath);
		write("/images/icons/propertyDetails_numIcon_");
		write(product.getProfileField(PARKING_QUESTION_ID));
		write(".gif\" alt=\"");
		write(product.getProfileField(PARKING_QUESTION_ID));
		write("\" width=\"11\" height=\"16\" /><img src=\"");
		write(sitePath);
		write("/images/icons/propertyDetails_icon_carparks.gif\" width=\"33\" height=\"16\" />\r\n");
		write("</div>\r\n");
		write("</td>\r\n");
		write(" \r\n");
		write("<td class=\"subContentDetails_r\">\r\n");
		write("<div class=\"propertyListPanel\">\r\n");
		write("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>\r\n");
		write("<td>\r\n");
		write("<table height=\"48px\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
		write("<tr>\r\n");
		write("<td class=\"viewDetailsBold\">Price:</td>\r\n");
		write("<td><!-- Hack Fix for now-->\r\n");
		write(StringUtil.replace(nf.format(product.getDouble("TotalCost")), ".00", ""));
		write("</td>\r\n");
		write("</tr>\r\n");
		write("<tr>\r\n");
		write("<td class=\"viewDetailsBold\">Rent:</td>\r\n");
		write("<td>$");
		write(product.getProfileField(RENT_QUESTION_ID));
		write(" per week</td>\r\n");
		write("</tr>\r\n");

		String leaseLength = product.getProfileField(LEASE_QUESTION_ID);
		if (leaseLength.length() != 0 && !leaseLength.equals("0")) {

			write("\r\n");
			write("<tr>\r\n");
			write("<td class=\"viewDetailsBold\">Lease:</td>\r\n");
			write("<td>\r\n");

			write(leaseLength);
			String leaseOption = product.getProfileField(OPTION_QUESTION_ID);

			write("\r\n");

			if (leaseOption.length() != 0 && !leaseOption.equals("0")) {
				write(" \r\n");
				write(" + ");
				write(leaseOption);
				write("\r\n");
			}
			write("\r\n");
			write(" years\r\n");
			write("</td>\r\n");
			write("</tr>\r\n");
			write(" ");
		}

		write("\r\n");
		write("</table>\r\n");
		write("</td>\r\n");
		if(viewProductPath != null) { 
			write("<td class=\"viewDetailsBtn\">"); 
			write("<a href=\"");
			write(contextPath);
			write(viewProductPath);
			write(product.getPrimaryKey());
			write("&ID=");
			write(noteID);
			write("\"><img src=\"");
			write(sitePath);
			write("/images/propertyListPanel_btn.jpg\" alt=\"view details\" width=\"102\" height=\"54\" border=\"0\" /></a>");
			write("</td>\r\n");
		}
		write("</tr></table>\r\n");
		write("</div>\r\n");
		write("\r\n");
		write("<p>");
		write(StringUtil.encodeHTMLLinefeeds(product.getField("Description")));
		write("</p>\r\n");
		write("<br>\r\n");
		write("</td>\r\n");
		write("</tr></table>\r\n");
		write("</div>\r\n");
	}

	public boolean generateTextEmail(String noteID) {
		super.generateTextEmail(noteID);

		String contextPath = getContextPath();
		String sitePath = contextPath + "/cms/websites/dha";

		com.sok.runway.autopilot.AutoPilotSchedule schedule = getAutoPilotSchedule();

		Contact contact = (Contact) getEntity();

		ArrayList<Product> matchingProductList = getMatchingProducts(contact);

		boolean productsFound = matchingProductList != null
				&& matchingProductList.size() > 0;

		if (productsFound) {

			write("Defence Housing Australia\r\n");
			write("=========================\r\n");
			write("Follow the following link to view an online version of this email.\r\n");
			write(contextPath);
			write("/dha/etext.jsp?ID=");
			write(noteID);
			write("\r\n");
			write("Dear ");
			if (contact.getField("FirstName").length() >= 2) {
				write(contact.getField("FirstName"));
			} else {
				write("Investor");
			}
			write(",\r\n");

			User repUser = contact.getRepUser();

			if (repUser != null
					&& !repUser.getPrimaryKey().equals(INACTIVE_USER_ID)) {

				// setEmailFrom(contact.getField("RepEmail"));
				setEmailFrom(contact.getField("RepFirstName") + " "
						+ contact.getField("RepLastName")
						+ " <automated.dha@runway.net.au>");
				setEmailReplyTo(contact.getField("RepEmail"));

				write("\r\n");
				
				writeTextBody(contact, repUser); 
				
				write("Kind regards\r\n\r\n");
				writeTextSignature(contact, repUser); 
				
			} else {
				// setEmailFrom("sales@dha.gov.au");
				setEmailFrom("DHA Sales <automated.dha@runway.net.au>");
				setEmailReplyTo("sales@dha.gov.au");

				write("\r\n");
				write("The following properties which match your investment criteria are now available.\r\n\r\n");
				write("If you would like more information about these or other DHA investment \r\n");
				write("properties please <a href=\"mailto:sales@dha.gov.au\">email</a> me or call 133 DHA.\r\n");
				write("Kind regards\r\n\r\n");
				writeTextSignature(contact, null); 
			}

			write("\r\n");
			write("================================================================================================\r\n");
			write("\r\n");
			write("Personal Information\r\n");
			write("--------------------\r\n");
			write("Name: ");
			write(contact.getField("Title"));
			write(" ");
			write(contact.getField("FirstName"));
			write(" ");
			write(contact.getField("LastName"));
			write("\r\n");
			write("Email: ");
			write(contact.getField("Email"));
			write("\r\n");
			write("Phone: ");
			write(contact.getField("Phone"));
			write("\r\n");
			write("Mobile: ");
			write(contact.getField("Mobile"));
			write("\r\n");
			write("Address: ");
			write(contact.getField("Address"));
			write("\r\n");
			write("City/Suburb: ");
			write(contact.getField("City"));
			write("\r\n");
			write("State: ");
			write(contact.getField("State"));
			write("\r\n");
			write("Postcode: ");
			write(contact.getField("Poastcode"));
			write("\r\n");
			write("\r\n");
			write("Follow the following link to edit your Personal Investment Finder.\r\n");
			write(contextPath);
			write("/dha/update.jsp?ID=");
			write(noteID);
			write("\r\n");
			write("\r\n");
			write("\r\n");
			write("Your property alert");

			for (Product product : matchingProductList) {
				writeTextProduct(product, contextPath, sitePath, noteID);
			}
			matchingProductList = null;

			write("\r\n");
			write("\r\n");
			write("================================================================================================\r\n");
			write("\r\n");
			write("Copyright Defence Housing Australia 2009\r\n");
			write("\r\n");
			write("Follow the following link to opt out of future communications from Defence Housing Australia\r\n");
			write(contextPath);
			write("/dha/optout.jsp?ID=");
			write(noteID);
			write("\r\n");
			write("\r\n");
		}

		return productsFound;
	}

	protected void writeTextProduct(Product product, String contextPath,
			String sitePath, String noteID) {
		writeTextProduct(product, contextPath, sitePath, noteID,
				"/dha/view.jsp?PropertyID=");
	}

	protected void writeTextProduct(Product product, String contextPath,
			String sitePath, String noteID, String productLink) {

		write("\r\n");
		write("================================================================================================\r\n");
		write(product.getProfileField(SUBURB_QUESTION_ID));
		write(",\r\n");
		write(product.getProfileField(REGION_QUESTION_ID));
		write(", ");
		write(product.getProfileField(STATE_QUESTION_ID));
		write("\r\n");
		write("Property ID - ");
		write(product.getField("ProductNum"));
		write("\r\n");

		if ("Hold".equals(product.getField("CurrentStatus"))) {
			write("Status: Hold\r\n");
		}

		write("Price: ");
		write(StringUtil.replace(nf.format(product.getDouble("TotalCost")), ".00", ""));
		write("\r\n");

		String leaseLength = product.getProfileField(LEASE_QUESTION_ID);

		if (leaseLength.length() != 0 && !leaseLength.equals("0")) {
			write("Lease: ");
			write(leaseLength);
			String leaseOption = product.getProfileField(OPTION_QUESTION_ID);

			if (leaseOption.length() != 0 && !leaseOption.equals("0")) {
				write(" + " + leaseOption);
			}
			write("years\r\n");
		}

		write("Rent: $");
		write(product.getProfileField(RENT_QUESTION_ID));
		write(" per week\r\n");
		write("Bedrooms: ");
		write(product.getProfileField(BEDROOMS_QUESTION_ID));
		write("\r\n");
		write("Bathrooms: ");
		write(product.getProfileField(BATHROOMS_QUESTION_ID));
		write("\r\n");
		write("Car Spaces: ");
		write(product.getProfileField(PARKING_QUESTION_ID));
		write("\r\n");
		write("\r\n");
		write(product.getField("Description"));
		write("\r\n");
		write("\r\n");
		if(productLink != null) { 
			write("Details Here\r\n");
			write(contextPath);
			write(productLink);
			write(product.getProductNum());
			write("&ID=");
			write(noteID);
		}
	}

	public ArrayList<Product> getMatchingProducts(Contact contact) {

		ArrayList<Product> matchingProductList = null;

		if (this.productList != null
				&& "Yes".equalsIgnoreCase(contact
						.getProfileField(CONTACT_PROPERTY_UPDATES_QUESTION_ID))) {

			matchingProductList = new ArrayList();

			MultiField propertyTypes = new MultiField(contact
					.getProfileField(CONTACT_PROPERTY_TYPE_QUESTION_ID));
			MultiField priceRange = new MultiField(contact
					.getProfileField(CONTACT_PRICE_RANGE_QUESTION_ID));
			MultiField[] locations = new MultiField[CONTACT_LOCATION_QUESTION_IDS.length];
			for (int i = 0; i < CONTACT_LOCATION_QUESTION_IDS.length; i++) {
				locations[i] = new MultiField(contact.getProfileField(
						CONTACT_LOCATION_QUESTION_IDS[i]).toUpperCase());
			}

			for (Product product : this.productList) {

				boolean matchingProperty = true;

				if (!propertyTypes.contains("All")
						&& !propertyTypes.contains(product
								.getProfileField(PROPERTY_TYPE_QUESTION_ID))
						&& contact.getProfileField(
								CONTACT_PROPERTY_TYPE_QUESTION_ID).length() != 0) {
					matchingProperty = false;
				}

				if (matchingProperty
						&& !priceRange.contains("All")
						&& contact.getProfileField(
								CONTACT_PRICE_RANGE_QUESTION_ID).length() != 0) {
					boolean inRange = false;
					for (int i = 0; i < priceRange.length(); i++) {
						Range range = new Range(priceRange.get(i), "-");
						if (range.inRange(product.getDouble("TotalCost"))) {
							inRange = true;
						}
					}
					matchingProperty = inRange;
				}

				if (matchingProperty
						&& !"Y"
								.equals(contact
										.getProfileField(CONTACT_LOCATION_ALL_QUESTION_ID))) {
					boolean locationMatch = false;
					boolean hasAnswer = false;
					for (int i = 0; i < locations.length; i++) {
						if (locations[i].length() != 0) {
							if (!(locations[i].length() == 1 && locations[i]
									.contains(ActionBean._blank))) {
								hasAnswer = true;
								if (locations[i].contains(product
										.getProfileField(REGION_QUESTION_ID)
										.toUpperCase())) {
									locationMatch = true;
									break;
								}
							}
						}
					}
					matchingProperty = !hasAnswer || locationMatch;
				}

				if (matchingProperty) {
					matchingProductList.add(product);
					product = null;
				}
			}
		}
		return matchingProductList;
	}

	protected synchronized void loadProducts(Connection con) {

		this.productList = new ArrayList();

		GenRow products = new GenRow();
		products.setConnection(con);
		products.setViewSpec("ProductIDView");
		products.setParameter("Active", "Y");
		products.setParameter("AvailableDate", "CURRENT;>"
				+ getAutoPilotSchedule().getFormattedField("LastRunDateTime",
						"dd/MM/yyyy HH:mm:ss"));

		products.setParameter("-join1",
				"ProductCurrentStatus.ProductStatusList");
		products.setParameter("ProductCurrentStatus.ProductStatusList.Status",
				"Listed+Hold");

		products.setParameter("-join2", "ProductValuationExpiryAnswer");
		products.setParameter("ProductValuationExpiryAnswer.AnswerDate",
				"!OVERDUE");

		products.setParameter("-join3", "ProductListingTypeAnswer"); // for
																		// sort
																		// only
		products.sortBy("ProductListingTypeAnswer.Answer", 0);
		products.sortOrder("DESC", 0);

		products.sortBy("ProductStatusList.Status", 1);
		products.sortOrder("DESC", 1);

		products.doAction(ActionBean.SEARCH);

		products.getResults();

		Product product = null;

		while (products.getNext()) {
			product = new Product(con);
			product.load(products.getString("ProductID"));
			if (product.isLoaded()) {
				product.loadProfiles();
				productList.add(product);
			}
		}

	}

	/*
	 * public String getStatusMailBody();
	 * 
	 * public String getStatusMailSubject() {
	 * return(context.getServletContextName() + " Runway offline process - Apply
	 * Campaign"); }
	 * 
	 * public File getStatusAttachment() { return null; }
	 * 
	 * public int getProcessSize();
	 * 
	 * public int getProcessedCount();
	 * 
	 * public int getErrorCount();
	 */

}