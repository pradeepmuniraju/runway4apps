package com.sok.clients.dha;

import com.sok.framework.GenRow; 
import com.sok.runway.RunwayEntity;
import com.sok.runway.crm.Contact; 
import com.sok.runway.autopilot.AbstractActionTypeUpdate; 

public class DormantContactUpdate extends AbstractActionTypeUpdate {
	
	   private GenRow dn; 		
	   private GenRow dr; 		
	   
	   /**
	    * overrides validContact from AbstractActionTypeUpdate, to dha 
	    * specifications for dormant contacts. 
	    * @param Contact - the contact to be validated 
	    * @return boolean - true if the contact should be updated
	    */
	   public boolean validRecord(RunwayEntity re) { 	
		   
		   Contact c = null; 
		   if(re instanceof Contact) { 
			   c = (Contact)re; 
		   }else { 
			   return false; 
		   }
		   	
		   if(dn == null) { 	
			   dn = new GenRow(); 
			   dn.setViewSpec("email/NoteEmailView");
		   } 
		   dn.clear(); 
		   dn.setConnection(c.getConnection());
		   dn.setParameter("-select1","NoteID"); 
		   dn.setParameter("Type","Email"); 
		   dn.setTop(10);
		   dn.setParameter("ContactID",c.getPrimaryKey());
		   dn.sortBy("CreatedDate",0);
		   dn.sortOrder("DESC",0); 
		   dn.getResults();	

		   if(dr == null) { 	
			   dr = new GenRow(); 
			   dr.setTableSpec("Responses"); 
		   } 	
		   dr.setConnection(c.getConnection()); 	
		   int invalid = 0; 
		   while(dn.getNext()) {
			   dr.clear(); 
			   dr.setParameter("-select1","ResponseID");
			   dr.setParameter("NoteID",dn.getData("NoteID")); 
			   
			   dr.doAction(com.sok.framework.generation.GenerationKeys.SELECTFIRST); 
			   
			   if(dr.isSuccessful()) { 
				   return false;
			   }
			   invalid++; 
		   }	
		   if(invalid > 9) { 
			   return true; 
		   }
		   return false; 	
	   }		
}
