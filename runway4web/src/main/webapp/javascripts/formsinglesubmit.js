var submittedForms = new Array();

var campaignFormSubmitted = false;

function canSubmitForm(form) {
   campaignFormSubmitted = submittedForms[form.name];

   if (!campaignFormSubmitted) {
      submittedForms[form.name] = true;
      campaignFormSubmitted = true;
      return true;
   }
   return false;
}