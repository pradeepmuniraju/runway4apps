/**
 * 
 * tabableset.js
 * 
 * Dependencies: /javascripts/lib/jquery-1.3.2.js
 * 
 */

function TabableSet(tabableSetIds) {	
	this.contructor = function(tabableSetIds) {
		this.originalStyles = new Array();
		this.tabableSetIds = new Array();
		
		if(tabableSetIds) {
			this.addAll(tabableSetIds);
		}		
	}
	
	this.addAll = function(tabableSetIds) {
		if(tabableSetIds) {
			for(var i=0; i < tabableSetIds.length; i++) {
				var tabableItemId = tabableSetIds[i];
				this.add(tabableItemId);
			}
		}
	};
	
	this.add = function(tabableItemId) {
		if(tabableItemId) {
			this.tabableSetIds.push(tabableItemId);
		}
	};
	
	this.clear = function() {
		this.originalStyles.clear();
		this.tabableSetIds.clear();
	};
	
	this.selectTab = function(tabableItemId) {
		if(tabableItemId) {
			this.hideAll();
			this.show(tabableItemId);
		}
	};
	
	this.hideAll = function() {
		for(var i=0; i < this.tabableSetIds.length; i++) {
			var tabableItemId = this.tabableSetIds[i];
			this.hide(tabableItemId);
		}
	};
	
	this.hide = function(tabableItemId) {
		if(tabableItemId) {
			var tabableItem = $("#"+tabableItemId);
			
			this.recordOriginalStyle(tabableItem);
			
			tabableItem.css("visibility", "hidden");			
			tabableItem.css("display", "none");
		}
	};
	
	this.recordOriginalStyle = function(tabableItem) {
		if(!this.originalStyles[tabableItem.attr("id")]) {
			this.originalStyles[tabableItem.attr("id")] = {visibility: tabableItem.css("visibility"), display: tabableItem.css("display")};
		}
	};
		
	this.show = function(tabableItemId) {
		if(tabableItemId) {
			var tabableItem = $("#"+tabableItemId);
			this.restoreOriginalStyle(tabableItem);
		}
	};
	
	this.restoreOriginalStyle = function(tabableItem) {
		if(tabableItem) {
			var originalStyle = this.originalStyles[tabableItem.attr("id")];
			if(originalStyle) {
				tabableItem.css("visibility", originalStyle.visibility);
				tabableItem.css("display", originalStyle.display);
				
				delete this.originalStyles[tabableItem.attr("id")];
			}
		}
	};
	
	this.initialise = function(initiallySelectedTabItemId) {
		if(this.tabableSetIds.length > 0) {
			if(!initiallySelectedTabItemId) {
				var initiallySelectedTabItemId = this.tabableSetIds[0];
			}
			this.selectTab(initiallySelectedTabItemId);
		}
	};
	
	this.contructor(tabableSetIds);
}

