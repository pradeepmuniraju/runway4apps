(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return ({}).hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var require = function(name, loaderPath) {
    var path = expand(name, '.');
    if (loaderPath == null) loaderPath = '/';

    if (has(cache, path)) return cache[path].exports;
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex].exports;
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '" from '+ '"' + loaderPath + '"');
  };

  var define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has(bundle, key)) {
          modules[key] = bundle[key];
        }
      }
    } else {
      modules[bundle] = fn;
    }
  };

  var list = function() {
    var result = [];
    for (var item in modules) {
      if (has(modules, item)) {
        result.push(item);
      }
    }
    return result;
  };

  globals.require = require;
  globals.require.define = define;
  globals.require.register = define;
  globals.require.list = list;
  globals.require.brunch = true;
})();
require.register("initialize_api", function(exports, require, module) {
(function() {

  $(function() {
    rivets.configure({
      adapter: {
        subscribe: function(obj, keypath, callback) {
          callback.wrapped = function(m, v) {
            callback(v);
          };
          obj.on('change:' + keypath, callback.wrapped);
        },
        unsubscribe: function(obj, keypath, callback) {
          obj.off('change:' + keypath, callback.wrapped);
        },
        read: function(obj, keypath) {
          return obj.get(keypath);
        },
        publish: function(obj, keypath, value) {
          obj.set(keypath, value);
        }
      }
    });
    _.extend(rivets.formatters, {
      date: function(value) {
        return moment(value).calendar();
      },
      dashifzero: function(value) {
        if ('0' === value || 0 === value || "0.00" === value) return "-";
        return value;
      },
      commaifnotempty: function(value) {
        if (value && value.length) return "" + value + ",";
        return value;
      },
      money: function(text) {
        return RW.Formatters.Money(text, 0);
      },
      yesno: function(text) {
        if ('Y' === text || 'Yes' === text || true === text) {
          return 'Yes';
        } else {
          return 'No';
        }
      },
      name: function(obj) {
        if (obj) {
          if (obj.Name) {
            return obj.Name;
          } else if (obj.FirstName && obj.LastName) {
            return "" + obj.FirstName + " " + obj.LastName;
          }
        }
        return obj;
      }
    });
    /*
      implementation similar to org.apache.commons.lang.StringUtils.abbreviate
      {{{abbreviate SomeKeyName lower=5 upper=10 append="..."}}}
    */
    return Handlebars.registerHelper('abbreviate', function(text, options) {
      var retval;
      retval = (function(text, lower, upper, append) {
        var i;
        if (text.length <= lower || text.length <= upper) return text;
        i = upper > 0 ? upper : lower;
        while (i >= 0) {
          if (text.charAt(i) === ' ' && (i <= lower || i <= upper)) {
            return text.substring(0, i) + append;
          }
          i--;
        }
        return text.substring(0, lower) + append;
      })(text, options.hash['lower'] || text.length, options.hash['upper'] || -1, options.hash['append'] || '...');
      return new Handlebars.SafeString(retval);
    });
  });

  _.extend(window, {
    RW: {
      CompanyCollection: require('models/companies'),
      ContactCollection: require('models/contacts'),
      UsersCollection: require('models/users'),
      ExistingProperty: require('models/cms/properties/existingproperty'),
      ProductDetail: require('models/cms/properties/productdetail'),
      Home: require('models/cms/properties/home'),
      Lot: require('models/cms/properties/lot'),
      HouseLandPackage: require('models/cms/properties/package'),
      User: require('models/user'),
      ItemList: require('models/itemlist'),
      ContactUserSearchView: require('views/contactusersearchview'),
      CompanySearchView: require('views/companysearchview'),
      PlanSelectView: require('views/planselectview'),
      LotSelectView: require('views/lotselectview'),
      LinkDocumentView: require('views/linkdocumentview'),
      DropzoneView: require('views/dropzoneview'),
      Progress: require('models/progress'),
      ProgressbarView: require('views/progressview'),
      linkDocument: function(p, callback, fallback) {
        var _linkDoc;
        _linkDoc = function() {
          var $lightboxEle, searchView;
          searchView = new RW.LinkDocumentView({
            object: p
          });
          $lightboxEle = $('#lightbox');
          if (!$lightboxEle.length) {
            $('<div/>').attr('id', 'lightbox').appendTo($('body'));
            $lightboxEle = $('#lightbox');
          }
          $lightboxEle.html(searchView.render().el);
          $.colorbox({
            inline: true,
            href: $lightboxEle,
            open: true,
            modal: true,
            opacity: 0.6,
            onComplete: function() {
              return setTimeout(function() {
                return $.colorbox.resize();
              }, 500);
            },
            onClosed: function() {
              searchView.close();
              return typeof callback === "function" ? callback() : void 0;
            }
          });
        };
        if (!$.browser || ($.browser.msie === true)) {
          return typeof fallback === "function" ? fallback() : void 0;
        } else {
          if (p.p) {
            return p.p.success(_linkDoc).error(fallback);
          } else {
            return _linkDoc();
          }
        }
      },
      linkDocumentAlbum: function(p, g, callback, fallback) {
        var _linkDoc;
        _linkDoc = function() {
          var $lightboxEle, searchView;
          searchView = new RW.LinkDocumentView({
            object: p,
            galleryID: g
          });
          $lightboxEle = $('#lightbox');
          if (!$lightboxEle.length) {
            $('<div/>').attr('id', 'lightbox').appendTo($('body'));
            $lightboxEle = $('#lightbox');
          }
          $lightboxEle.html(searchView.render().el);
          $.colorbox({
            inline: true,
            href: $lightboxEle,
            open: true,
            modal: true,
            opacity: 0.6,
            onComplete: function() {
              return setTimeout(function() {
                return $.colorbox.resize();
              }, 500);
            },
            onClosed: function() {
              searchView.close();
              return typeof callback === "function" ? callback() : void 0;
            }
          });
        };
        if (!$.browser || ($.browser.msie === true)) {
          return typeof fallback === "function" ? fallback() : void 0;
        } else {
          if (p.p) {
            return p.p.success(_linkDoc).error(fallback);
          } else {
            return _linkDoc();
          }
        }
      },
      Formatters: {
        Money: function(number, decimals, decimal_separator, thousands_separator) {
          var c, i, j, n, sign, x, y, z;
          if (number == null) number = 0;
          if (decimals == null) decimals = 2;
          if (decimal_separator == null) decimal_separator = ".";
          if (thousands_separator == null) thousands_separator = ",";
          n = number;
          c = isNaN(decimals) ? 2 : Math.abs(decimals);
          sign = n < 0 ? "-$" : "$";
          i = parseInt(n = Math.abs(n).toFixed(c)) + '';
          j = (j = i.length) > 3 ? j % 3 : 0;
          x = j ? i.substr(0, j) + thousands_separator : '';
          y = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_separator);
          z = c ? decimal_separator + Math.abs(n - i).toFixed(c).slice(2) : '';
          return sign + x + y + z;
        }
      },
      DomBinder: {
        bind: function(ele, context, bindValidation) {
          var bindContext;
          if (bindValidation == null) bindValidation = true;
          bindContext = rivets.bind(ele, context);
          if (bindValidation) {
            _.each(context, function(obj) {
              if (obj.validation) {
                return Backbone.Validation.bind(new Backbone.View({
                  model: obj
                }));
              }
            });
          }
          return bindContext;
        }
      },
      Validator: {
        bind: function(view) {
          Backbone.Validation.bind(view);
        },
        bindModel: function(model) {
          Backbone.Validation.bind(new Backbone.View({
            model: model
          }));
        }
      }
    },
    locale: {
      "fileupload": {
        "errors": {
          "maxFileSize": "File is too big",
          "minFileSize": "File is too small",
          "acceptFileTypes": "Filetype not allowed",
          "maxNumberOfFiles": "Max number of files exceeded",
          "uploadedBytes": "Uploaded bytes exceed file size",
          "emptyResult": "Empty file upload result"
        },
        "error": "Error",
        "start": "Start",
        "cancel": "Cancel",
        "destroy": "Delete"
      }
    }
  });

}).call(this);

});

;require.register("models/address", function(exports, require, module) {
(function() {
  var Address, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Address = (function(_super) {

    __extends(Address, _super);

    function Address() {
      Address.__super__.constructor.apply(this, arguments);
    }

    return Address;

  })(Model);

}).call(this);

});

;require.register("models/cms/document", function(exports, require, module) {
(function() {
  var Document, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Document = (function(_super) {

    __extends(Document, _super);

    function Document() {
      Document.__super__.constructor.apply(this, arguments);
    }

    Document.prototype.idAttribute = 'DocumentID';

    Document.prototype.defaults = {
      DocumentID: null
    };

    Document.prototype.url = function() {
      return this.getCtx() + '/api/documents/' + (this.get("DocumentID") ? this.get("DocumentID") : "");
    };

    return Document;

  })(Model);

}).call(this);

});

;require.register("models/cms/documents", function(exports, require, module) {
(function() {
  var Collection, Document, DocumentCollection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Document = require('./document');

  Collection = require('models/collection');

  module.exports = DocumentCollection = (function(_super) {

    __extends(DocumentCollection, _super);

    function DocumentCollection() {
      DocumentCollection.__super__.constructor.apply(this, arguments);
    }

    DocumentCollection.prototype.model = Document;

    DocumentCollection.prototype.baseURL = '/api/documents/';

    DocumentCollection.prototype.initialize = function(models, opts) {
      DocumentCollection.__super__.initialize.apply(this, arguments);
      this.reqParams = opts;
      if (this.reqParams.baseURL) {
        this.baseURL = this.reqParams.baseURL;
        delete this.reqParams.baseURL;
      }
    };

    DocumentCollection.prototype.url = function() {
      var url;
      url = this.getCtx() + this.baseURL;
      _.each(this.reqParams, function(v, k) {
        url += url.indexOf('?') !== -1 ? '&' : '?';
        return url += k + '=' + encodeURIComponent(v);
      });
      return url;
    };

    return DocumentCollection;

  })(Collection);

}).call(this);

});

;require.register("models/cms/linkeddocument", function(exports, require, module) {
(function() {
  var LinkedDocument, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = LinkedDocument = (function(_super) {

    __extends(LinkedDocument, _super);

    function LinkedDocument() {
      LinkedDocument.__super__.constructor.apply(this, arguments);
    }

    LinkedDocument.prototype.idAttribute = 'LinkedDocumentID';

    LinkedDocument.prototype.initialize = function(model, opts) {
      return LinkedDocument.__super__.initialize.apply(this, arguments);
    };

    LinkedDocument.prototype.defaults = {
      LinkedDocumentID: null,
      GalleryID: null
    };

    LinkedDocument.prototype.url = function() {
      if (this.get('ProductID')) {
        return this.getCtx() + '/api/cms/properties/products/' + this.get('ProductID') + '/documents';
      }
      return false;
    };

    return LinkedDocument;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/apartment", function(exports, require, module) {
(function() {
  var Apartment, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Apartment = (function(_super) {

    __extends(Apartment, _super);

    function Apartment() {
      Apartment.__super__.constructor.apply(this, arguments);
    }

    Apartment.prototype.idAttribute = "ProductID";

    Apartment.prototype.defaults = {
      ProductID: null,
      Name: "",
      Cost: 0,
      GST: 0,
      TotalCost: 0
    };

    Apartment.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Apartment.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    Apartment.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/buildings/levels/apartments/" + (this.get("ProductID") ? this.get("ProductID") : "");
    };

    return Apartment;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/brand", function(exports, require, module) {
(function() {
  var Brand, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Brand = (function(_super) {

    __extends(Brand, _super);

    function Brand() {
      Brand.__super__.constructor.apply(this, arguments);
    }

    Brand.prototype.idAttribute = 'BrandID';

    Brand.prototype.defaults = {
      BrandID: null,
      Name: ''
    };

    Brand.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Brand.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    Brand.prototype.url = function() {
      return this.getCtx() + '/api/2/brands/' + (this.id ? this.id : '');
    };

    return Brand;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/builder", function(exports, require, module) {
(function() {
  var Builder, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Builder = (function(_super) {

    __extends(Builder, _super);

    function Builder() {
      Builder.__super__.constructor.apply(this, arguments);
    }

    Builder.prototype.idAttribute = "ProductID";

    Builder.prototype.defaults = {
      ProductID: null,
      Name: "",
      Cost: 0,
      GST: 0,
      TotalCost: 0
    };

    Builder.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Builder.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    Builder.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/builders/" + (this.get("ProductID") ? this.get("ProductID") : "");
    };

    return Builder;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/buildercontact", function(exports, require, module) {
(function() {
  var BuilderContact, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = BuilderContact = (function(_super) {

    __extends(BuilderContact, _super);

    function BuilderContact() {
      BuilderContact.__super__.constructor.apply(this, arguments);
    }

    BuilderContact.prototype.idAttribute = "BuilderContactID";

    BuilderContact.prototype.defaults = {
      BuilderContactID: null,
      BuilderID: null,
      ContactID: null
    };

    BuilderContact.prototype.url = function() {
      if (!this.has('BuilderID')) throw "BuilderID is required";
      return this.getCtx() + ("/api/2/builders/" + (this.get('BuilderID')) + "/contacts/") + (this.get("BuilderContactID") ? this.get("BuilderContactID") : "");
    };

    return BuilderContact;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/builders", function(exports, require, module) {
(function() {
  var Builder, BuilderCollection, Collection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Builder = require('./builder');

  Collection = require('models/collection');

  module.exports = BuilderCollection = (function(_super) {

    __extends(BuilderCollection, _super);

    function BuilderCollection() {
      BuilderCollection.__super__.constructor.apply(this, arguments);
    }

    BuilderCollection.prototype.model = Builder;

    BuilderCollection.prototype.initialize = function(models, options) {
      return this.BrandProductID = options.BrandProductID;
    };

    BuilderCollection.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/" + (this.BrandProductID ? "brands/" + this.BrandProductID : "") + "/builders/";
    };

    return BuilderCollection;

  })(Collection);

}).call(this);

});

;require.register("models/cms/properties/building", function(exports, require, module) {
(function() {
  var Building, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Building = (function(_super) {

    __extends(Building, _super);

    function Building() {
      Building.__super__.constructor.apply(this, arguments);
    }

    Building.prototype.idAttribute = "ProductID";

    Building.prototype.defaults = {
      ProductID: null,
      Name: "",
      Cost: 0,
      GST: 0,
      TotalCost: 0
    };

    Building.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Building.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    Building.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/buildings/" + (this.get("ProductID") ? this.get("ProductID") : "");
    };

    return Building;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/buildingcontact", function(exports, require, module) {
(function() {
  var BuildingContact, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = BuildingContact = (function(_super) {

    __extends(BuildingContact, _super);

    function BuildingContact() {
      BuildingContact.__super__.constructor.apply(this, arguments);
    }

    BuildingContact.prototype.idAttribute = "BuildingContactID";

    BuildingContact.prototype.defaults = {
      BuildingContactID: null,
      BuildingID: null,
      ContactID: null
    };

    BuildingContact.prototype.url = function() {
      if (!this.has('BuildingID')) throw "BuildingID is required";
      return this.getCtx() + ("/api/2/buildings/" + (this.get('BuildingID')) + "/contacts/") + (this.get("BuildingContactID") ? this.get("BuildingContactID") : "");
    };

    return BuildingContact;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/buildings", function(exports, require, module) {
(function() {
  var Building, BuildingCollection, Collection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Building = require('./building');

  Collection = require('models/collection');

  module.exports = BuildingCollection = (function(_super) {

    __extends(BuildingCollection, _super);

    function BuildingCollection() {
      BuildingCollection.__super__.constructor.apply(this, arguments);
    }

    BuildingCollection.prototype.model = Building;

    BuildingCollection.prototype.initialize = function(models, options) {};

    BuildingCollection.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/buildings/";
    };

    return BuildingCollection;

  })(Collection);

}).call(this);

});

;require.register("models/cms/properties/details", function(exports, require, module) {
(function() {
  var Details, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Details = (function(_super) {

    __extends(Details, _super);

    function Details() {
      Details.__super__.constructor.apply(this, arguments);
    }

    Details.prototype.validation = {
      Area: function(value) {
        if (!_.isFinite(new Number(value))) return "Area must be numeric";
      }
    };

    return Details;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/developer", function(exports, require, module) {
(function() {
  var Developer, Lots, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Lots = require('./lots');

  Model = require('models/model');

  module.exports = Developer = (function(_super) {

    __extends(Developer, _super);

    function Developer() {
      Developer.__super__.constructor.apply(this, arguments);
    }

    Developer.prototype.idAttribute = "DeveloperID";

    Developer.prototype.defaults = {
      DeveloperID: null,
      ProductID: null,
      Name: ""
    };

    Developer.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Developer.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    return Developer;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/estate", function(exports, require, module) {
(function() {
  var Estate, Lots, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Lots = require('./lots');

  Model = require('models/model');

  module.exports = Estate = (function(_super) {

    __extends(Estate, _super);

    function Estate() {
      Estate.__super__.constructor.apply(this, arguments);
    }

    Estate.prototype.idAttribute = "EstateID";

    Estate.prototype.defaults = {
      EstateID: null,
      ProductID: null,
      Name: ""
    };

    Estate.prototype.url = function() {
      return this.getCtx() + "/api/2/estates/" + (this.get("EstateID") ? this.get("EstateID") : "");
    };

    Estate.prototype.getLots = function() {
      if (this.isNew()) {
        throw "No support exists for new Estates, they must be created via Runway";
      }
      if (!this.products) {
        this.products = new Lots([], {
          EstateProductID: this.id
        });
      }
      return this.products;
    };

    Estate.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Estate.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    return Estate;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/estatecontact", function(exports, require, module) {
(function() {
  var EstateContact, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = EstateContact = (function(_super) {

    __extends(EstateContact, _super);

    function EstateContact() {
      EstateContact.__super__.constructor.apply(this, arguments);
    }

    EstateContact.prototype.idAttribute = "EstateContactID";

    EstateContact.prototype.defaults = {
      EstateContactID: null,
      EstateID: null,
      ContactID: null
    };

    EstateContact.prototype.url = function() {
      if (!this.has('EstateID')) throw "EstateID is required";
      return this.getCtx() + ("/api/2/estates/" + (this.get('EstateID')) + "/contacts/") + (this.get("EstateContactID") ? this.get("EstateContactID") : "");
    };

    return EstateContact;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/estates", function(exports, require, module) {
(function() {
  var Collection, Estate, EstateCollection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Estate = require('./estate');

  Collection = require('models/collection');

  module.exports = EstateCollection = (function(_super) {

    __extends(EstateCollection, _super);

    function EstateCollection() {
      EstateCollection.__super__.constructor.apply(this, arguments);
    }

    EstateCollection.prototype.model = Estate;

    EstateCollection.prototype.initialize = function(models, options) {};

    EstateCollection.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/estates/";
    };

    return EstateCollection;

  })(Collection);

}).call(this);

});

;require.register("models/cms/properties/existingproperty", function(exports, require, module) {
(function() {
  var Address, ExistingProperty, Model, ProductDetails, ProductSecurityGroup,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  Address = require('models/address');

  ProductDetails = require('./productdetails');

  ProductSecurityGroup = require('./productsecuritygroup');

  module.exports = ExistingProperty = (function(_super) {

    __extends(ExistingProperty, _super);

    function ExistingProperty() {
      ExistingProperty.__super__.constructor.apply(this, arguments);
    }

    ExistingProperty.prototype.idAttribute = 'ProductID';

    ExistingProperty.prototype.updating = false;

    ExistingProperty.prototype.defaults = {
      ProductID: null,
      ProductType: 'Existing Property'
    };

    ExistingProperty.prototype.initialize = function() {
      var firstGroup;
      ExistingProperty.__super__.initialize.apply(this, arguments);
      this.address = new Address(this.get('Address') || {});
      this.address.on('change', function() {
        return this.save('Address', this.address.toJSON(), {
          silent: true
        });
      }, this);
      this.on('change:Address', function(model, addr) {
        return this.address.set(addr);
      }, this);
      firstGroup = _.first(this.get('ProductSecurityGroups') || []);
      if (!firstGroup) firstGroup = {};
      this.singlegroup = new ProductSecurityGroup(firstGroup);
      this.singlegroup.updating = false;
      this.singlegroup.on('change', function() {
        if (this.singlegroup.updating !== true) return this.singlegroup.save();
      }, this);
      this.productdetails = new ProductDetails([], {
        ProductID: this.get('ProductID')
      });
      this.productdetails.add(this.get('ProductDetails') || []);
      this.on('change:ProductID', function(model, pid) {
        this.productdetails.ProductID = pid;
        this.singlegroup.set('ProductID', pid);
        this.productdetails.each(function(obj) {
          return obj.set({
            'ProductID': pid
          }, {
            'silent': true
          });
        });
      });
    };

    ExistingProperty.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    ExistingProperty.prototype.url = function() {
      return ("" + (this.getCtx()) + "/api/cms/properties/existingproperties/") + (this.id ? this.id : '');
    };

    ExistingProperty.prototype.fetch = function() {
      var firstGroup;
      ExistingProperty.__super__.fetch.apply(this, arguments);
      firstGroup = _.first(this.get('ProductSecurityGroups') || []);
      if (firstGroup) {
        this.singlegroup.updating = true;
        this.singlegroup.set(firstGroup);
        this.singlegroup.updating = false;
      } else {
        this.singlegroup.set('ProductID', this.id);
      }
      this.address.set(this.get('Address') || {});
      _.each(this.get('ProductDetails') || [], function(det) {
        return this.productdetails.add(det);
      }, this);
    };

    return ExistingProperty;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/home", function(exports, require, module) {
(function() {
  var Home, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Home = (function(_super) {

    __extends(Home, _super);

    function Home() {
      Home.__super__.constructor.apply(this, arguments);
    }

    Home.prototype.idAttribute = 'ProductID';

    Home.prototype.defaults = {
      ProductID: null,
      Name: '',
      ImagePath: null,
      TotalCost: null
    };

    Home.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Home.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    Home.prototype.url = function() {
      return this.getCtx() + '/api/cms/properties/homes/' + (this.id ? this.id : '');
    };

    return Home;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/homes", function(exports, require, module) {
(function() {
  var Collection, Home, HomeCollection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Home = require('./home');

  Collection = require('models/collection');

  module.exports = HomeCollection = (function(_super) {

    __extends(HomeCollection, _super);

    function HomeCollection() {
      HomeCollection.__super__.constructor.apply(this, arguments);
    }

    HomeCollection.prototype.model = Home;

    HomeCollection.prototype.initialize = function(coll, opts) {
      HomeCollection.__super__.initialize.apply(this, arguments);
      return this.RangeProductID = opts.RangeProductID || null;
    };

    HomeCollection.prototype.url = function() {
      return this.getCtx() + '/api/cms/properties/homes/' + (this.RangeProductID ? "?RangeProductID=" + this.RangeProductID : "");
    };

    return HomeCollection;

  })(Collection);

}).call(this);

});

;require.register("models/cms/properties/level", function(exports, require, module) {
(function() {
  var Level, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Level = (function(_super) {

    __extends(Level, _super);

    function Level() {
      Level.__super__.constructor.apply(this, arguments);
    }

    Level.prototype.idAttribute = "ProductID";

    Level.prototype.defaults = {
      ProductID: null,
      Name: "",
      Cost: 0,
      GST: 0,
      TotalCost: 0
    };

    Level.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Level.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    Level.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/buildings/levels/" + (this.get("ProductID") ? this.get("ProductID") : "");
    };

    return Level;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/lot", function(exports, require, module) {
(function() {
  var Estate, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Estate = (function(_super) {

    __extends(Estate, _super);

    function Estate() {
      Estate.__super__.constructor.apply(this, arguments);
    }

    Estate.prototype.idAttribute = "ProductID";

    Estate.prototype.defaults = {
      ProductID: null,
      Name: "",
      Cost: 0,
      GST: 0,
      TotalCost: 0
    };

    Estate.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Estate.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    Estate.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/lots/" + (this.get("ProductID") ? this.get("ProductID") : "");
    };

    return Estate;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/lots", function(exports, require, module) {
(function() {
  var Collection, Lot, LotCollection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Lot = require('./lot');

  Collection = require('models/collection');

  module.exports = LotCollection = (function(_super) {

    __extends(LotCollection, _super);

    function LotCollection() {
      LotCollection.__super__.constructor.apply(this, arguments);
    }

    LotCollection.prototype.model = Lot;

    LotCollection.prototype.initialize = function(models, options) {
      return this.EstateProductID = options.EstateProductID;
    };

    LotCollection.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/" + (this.EstateProductID ? "estates/" + this.EstateProductID : "") + "/lots/";
    };

    return LotCollection;

  })(Collection);

}).call(this);

});

;require.register("models/cms/properties/package", function(exports, require, module) {
(function() {
  var HouseLandPackage, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = HouseLandPackage = (function(_super) {

    __extends(HouseLandPackage, _super);

    function HouseLandPackage() {
      HouseLandPackage.__super__.constructor.apply(this, arguments);
    }

    HouseLandPackage.prototype.idAttribute = 'ProductID';

    HouseLandPackage.prototype.defaults = {
      ProductID: null,
      Name: '',
      HomeProductID: null,
      LandProductID: null,
      ImagePath: null,
      TotalCost: null
    };

    HouseLandPackage.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    HouseLandPackage.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    HouseLandPackage.prototype.url = function() {
      return this.getCtx() + '/api/cms/properties/packages/' + this.get('ProductID');
    };

    return HouseLandPackage;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/plan", function(exports, require, module) {
(function() {
  var Model, Plan,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Plan = (function(_super) {

    __extends(Plan, _super);

    function Plan() {
      Plan.__super__.constructor.apply(this, arguments);
    }

    Plan.prototype.idAttribute = 'ProductID';

    Plan.prototype.defaults = {
      ProductID: null,
      Name: '',
      HomeProductID: null,
      ImagePath: null,
      TotalCost: null,
      QuoteViewID: null
    };

    Plan.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Plan.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    Plan.prototype.url = function() {
      return this.getCtx() + '/api/cms/properties/plans/' + (this.id ? this.id : '');
    };

    return Plan;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/plans", function(exports, require, module) {
(function() {
  var Collection, Plan, PlanCollection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Plan = require('./plan');

  Collection = require('models/collection');

  module.exports = PlanCollection = (function(_super) {

    __extends(PlanCollection, _super);

    function PlanCollection() {
      PlanCollection.__super__.constructor.apply(this, arguments);
    }

    PlanCollection.prototype.model = Plan;

    PlanCollection.prototype.initialize = function(models, options) {
      return this.HomeProductID = options.HomeProductID;
    };

    PlanCollection.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/" + (this.HomeProductID ? "homes/" + this.HomeProductID : "") + "/plans/";
    };

    return PlanCollection;

  })(Collection);

}).call(this);

});

;require.register("models/cms/properties/productdetail", function(exports, require, module) {
(function() {
  var Model, ProductDetail,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = ProductDetail = (function(_super) {

    __extends(ProductDetail, _super);

    function ProductDetail() {
      ProductDetail.__super__.constructor.apply(this, arguments);
    }

    ProductDetail.prototype.idAttribute = 'ProductDetailsID';

    ProductDetail.prototype.defaults = {
      ProductDetailsID: null,
      ProductID: null
    };

    ProductDetail.prototype.url = function() {
      return ("" + (this.getCtx()) + "/api/cms/properties/products/" + (this.get('ProductID')) + "/details/") + (this.id ? this.id : '');
    };

    return ProductDetail;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/productdetails", function(exports, require, module) {
(function() {
  var Collection, ProductDetail, ProductDetails,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  ProductDetail = require('./productdetail');

  Collection = require('models/collection');

  module.exports = ProductDetails = (function(_super) {

    __extends(ProductDetails, _super);

    function ProductDetails() {
      ProductDetails.__super__.constructor.apply(this, arguments);
    }

    ProductDetails.prototype.model = ProductDetail;

    ProductDetails.prototype.saving = false;

    ProductDetails.prototype.initialize = function(arr, opts) {
      ProductDetails.__super__.initialize.apply(this, arguments);
      if (opts && opts.ProductID) this.ProductID = opts.ProductID;
      this.on('add', function(obj, coll, ixobj) {
        obj.on('change', function() {
          if (this.saving !== true) obj.save();
        }, this);
      }, this);
    };

    ProductDetails.prototype.url = function() {
      if (this.ProductID) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.ProductID + "/details";
      } else {
        return null;
      }
    };

    return ProductDetails;

  })(Collection);

}).call(this);

});

;require.register("models/cms/properties/productsecuritygroup", function(exports, require, module) {
(function() {
  var Model, ProductSecurityGroup,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = ProductSecurityGroup = (function(_super) {

    __extends(ProductSecurityGroup, _super);

    function ProductSecurityGroup() {
      ProductSecurityGroup.__super__.constructor.apply(this, arguments);
    }

    ProductSecurityGroup.prototype.idAttribute = 'ProductSecurityGroupID';

    ProductSecurityGroup.prototype.defaults = {
      ProductSecurityGroupID: null,
      ProductID: null
    };

    ProductSecurityGroup.prototype.url = function() {
      return ("" + (this.getCtx()) + "/api/cms/properties/products/" + (this.get('ProductID')) + "/productsecuritygroups/") + (this.id ? this.id : '');
    };

    return ProductSecurityGroup;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/range", function(exports, require, module) {
(function() {
  var Model, Range,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Range = (function(_super) {

    __extends(Range, _super);

    function Range() {
      Range.__super__.constructor.apply(this, arguments);
    }

    Range.prototype.idAttribute = 'ProductID';

    Range.prototype.defaults = {
      ProductID: null,
      Name: '',
      HomeProductID: null,
      ImagePath: null,
      TotalCost: null,
      QuoteViewID: null
    };

    Range.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Range.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    Range.prototype.url = function() {
      return this.getCtx() + '/api/cms/properties/ranges/' + (this.id ? this.id : '');
    };

    return Range;

  })(Model);

}).call(this);

});

;require.register("models/cms/properties/ranges", function(exports, require, module) {
(function() {
  var Collection, Range, RangeCollection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Range = require('./range');

  Collection = require('models/collection');

  module.exports = RangeCollection = (function(_super) {

    __extends(RangeCollection, _super);

    function RangeCollection() {
      RangeCollection.__super__.constructor.apply(this, arguments);
    }

    RangeCollection.prototype.model = Range;

    RangeCollection.prototype.url = function() {
      return this.getCtx() + '/api/cms/properties/ranges/';
    };

    return RangeCollection;

  })(Collection);

}).call(this);

});

;require.register("models/cms/properties/stage", function(exports, require, module) {
(function() {
  var Model, Stage,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('models/model');

  module.exports = Stage = (function(_super) {

    __extends(Stage, _super);

    function Stage() {
      Stage.__super__.constructor.apply(this, arguments);
    }

    Stage.prototype.idAttribute = "ProductID";

    Stage.prototype.defaults = {
      ProductID: null,
      Name: "",
      Cost: 0,
      GST: 0,
      TotalCost: 0
    };

    Stage.prototype.setLinkedDocumentFields = function(obj) {
      return obj.set('ProductID', this.id);
    };

    Stage.prototype.getDocumentURL = function() {
      if (this.id) {
        return "" + (this.getCtx()) + "/api/cms/properties/products/" + this.id + "/documents/";
      } else {
        return false;
      }
    };

    Stage.prototype.url = function() {
      return this.getCtx() + "/api/cms/properties/estates/stages/" + (this.get("ProductID") ? this.get("ProductID") : "");
    };

    return Stage;

  })(Model);

}).call(this);

});

;require.register("models/collection", function(exports, require, module) {
(function() {
  var Collection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  module.exports = Collection = (function(_super) {

    __extends(Collection, _super);

    function Collection() {
      Collection.__super__.constructor.apply(this, arguments);
    }

    Collection.prototype.getCtx = function() {
      return window.CTX_PATH;
    };

    Collection.prototype.retrieve = function() {
      this.promise = this.fetch();
      return this;
    };

    return Collection;

  })(Backbone.Collection);

}).call(this);

});

;require.register("models/companies", function(exports, require, module) {
(function() {
  var Collection, Company, CompanyCollection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Company = require('./company');

  Collection = require('models/collection');

  module.exports = CompanyCollection = (function(_super) {

    __extends(CompanyCollection, _super);

    function CompanyCollection() {
      CompanyCollection.__super__.constructor.apply(this, arguments);
    }

    CompanyCollection.prototype.model = Company;

    CompanyCollection.prototype.initialize = function(models, opts) {
      CompanyCollection.__super__.initialize.apply(this, arguments);
      return this.reqParams = opts;
    };

    CompanyCollection.prototype.url = function() {
      var url;
      url = this.getCtx() + '/api/companies/';
      _.each(this.reqParams, function(v, k) {
        url += url.indexOf('?') !== -1 ? '&' : '?';
        return url += k + '=' + encodeURIComponent(v);
      });
      return url;
    };

    return CompanyCollection;

  })(Collection);

}).call(this);

});

;require.register("models/company", function(exports, require, module) {
(function() {
  var Company, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('./model');

  module.exports = Company = (function(_super) {

    __extends(Company, _super);

    function Company() {
      Company.__super__.constructor.apply(this, arguments);
    }

    Company.prototype.idAttribute = 'CompanyID';

    Company.prototype.defaults = {
      CompanyID: null,
      Company: '',
      PhysicalAddress: {},
      PostalAddress: {}
    };

    Company.prototype.name = function() {
      return "" + (this.get('Company'));
    };

    Company.prototype.url = function() {
      return this.getCtx() + '/api/companies/' + (this.get("CompanyID") ? this.get("CompanyID") : "");
    };

    return Company;

  })(Model);

}).call(this);

});

;require.register("models/contact", function(exports, require, module) {
(function() {
  var Contact, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('./model');

  module.exports = Contact = (function(_super) {

    __extends(Contact, _super);

    function Contact() {
      Contact.__super__.constructor.apply(this, arguments);
    }

    Contact.prototype.idAttribute = 'ContactID';

    Contact.prototype.defaults = {
      ContactID: null,
      Title: '',
      FirstName: '',
      LastName: '',
      Company: '',
      PhysicalAddress: {},
      PostalAddress: {}
    };

    Contact.prototype.name = function() {
      return "" + (this.get('FirstName')) + " " + (this.get('LastName'));
    };

    Contact.prototype.url = function() {
      return this.getCtx() + '/api/contacts/' + (this.get("ContactID") ? this.get("ContactID") : "");
    };

    return Contact;

  })(Model);

}).call(this);

});

;require.register("models/contacts", function(exports, require, module) {
(function() {
  var Collection, Contact, ContactCollection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Contact = require('./contact');

  Collection = require('models/collection');

  module.exports = ContactCollection = (function(_super) {

    __extends(ContactCollection, _super);

    function ContactCollection() {
      ContactCollection.__super__.constructor.apply(this, arguments);
    }

    ContactCollection.prototype.model = Contact;

    ContactCollection.prototype.initialize = function(models, opts) {
      ContactCollection.__super__.initialize.apply(this, arguments);
      return this.reqParams = opts;
    };

    ContactCollection.prototype.url = function() {
      var url;
      url = this.getCtx() + '/api/contacts/';
      _.each(this.reqParams, function(v, k) {
        url += url.indexOf('?') !== -1 ? '&' : '?';
        return url += k + '=' + encodeURIComponent(v);
      });
      return url;
    };

    return ContactCollection;

  })(Collection);

}).call(this);

});

;require.register("models/initialize_legacy", function(exports, require, module) {
(function() {

  module.exports = {
    loadPanel: function(id, urlorform) {
      return $('#' + id).load(urlorform);
    }
  };

}).call(this);

});

;require.register("models/itemlist", function(exports, require, module) {
(function() {
  var ItemList, Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('./model');

  module.exports = ItemList = (function(_super) {

    __extends(ItemList, _super);

    function ItemList() {
      ItemList.__super__.constructor.apply(this, arguments);
    }

    ItemList.prototype.initialize = function(opts) {
      return this.listname = opts.listname;
    };

    ItemList.prototype.url = function() {
      return this.getCtx() + '/api/util/itemlists/' + encodeURIComponent(this.listname);
    };

    return ItemList;

  })(Model);

}).call(this);

});

;require.register("models/model", function(exports, require, module) {
(function() {
  var Model,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  module.exports = Model = (function(_super) {

    __extends(Model, _super);

    function Model() {
      Model.__super__.constructor.apply(this, arguments);
    }

    Model.prototype.initialize = function() {
      Model.__super__.initialize.apply(this, arguments);
      this.on('validated:valid', function(model) {
        console.log('validated');
        console.log(model);
      });
      return this.on('validated:invalid', function(model, errors) {
        return _.each(errors, function(e, k) {
          alert(e);
          return this.set(k, '');
        }, model);
      });
    };

    Model.prototype.retrieve = function() {
      this.promise = this.fetch();
      return this;
    };

    Model.prototype.getCtx = function() {
      return window.CTX_PATH;
    };

    return Model;

  })(Backbone.Model);

}).call(this);

});

;require.register("models/progress", function(exports, require, module) {
(function() {
  var Model, User,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('./model');

  module.exports = User = (function(_super) {

    __extends(User, _super);

    function User() {
      User.__super__.constructor.apply(this, arguments);
    }

    User.prototype.defaults = {
      id: null,
      p: 0
    };

    User.prototype.initialize = function() {
      User.__super__.initialize.apply(this, arguments);
      this.start = moment();
      return this.on('change:count', function() {
        var count, end, time, total;
        end = moment();
        count = this.get('count');
        total = this.get('total');
        time = end - this.start;
        try {
          if (_.isNumber(count) && _.isNumber(total) && count > 0 && total > 0) {
            this.perms = time / count;
            return this.leftms = this.perms * (total - count);
          }
        } catch (e) {
          return console.log(e);
        }
      }, this);
    };

    User.prototype.url = function() {
      return this.getCtx() + '/api/util/progress/' + (this.id ? this.id : "");
    };

    return User;

  })(Model);

}).call(this);

});

;require.register("models/user", function(exports, require, module) {
(function() {
  var Model, User,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  Model = require('./model');

  module.exports = User = (function(_super) {

    __extends(User, _super);

    function User() {
      User.__super__.constructor.apply(this, arguments);
    }

    User.prototype.idAttribute = 'UserID';

    User.prototype.defaults = {
      UserID: null,
      Title: '',
      FirstName: '',
      LastName: '',
      Company: '',
      PhysicalAddress: {},
      PostalAddress: {}
    };

    User.prototype.name = function() {
      return "" + (this.get('FirstName')) + " " + (this.get('LastName'));
    };

    User.prototype.url = function() {
      return this.getCtx() + '/api/admin/users/' + (this.get("UserID") ? this.get("UserID") : "");
    };

    return User;

  })(Model);

}).call(this);

});

;require.register("models/users", function(exports, require, module) {
(function() {
  var Collection, User, UserCollection,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  User = require('./user');

  Collection = require('models/collection');

  module.exports = UserCollection = (function(_super) {

    __extends(UserCollection, _super);

    function UserCollection() {
      UserCollection.__super__.constructor.apply(this, arguments);
    }

    UserCollection.prototype.model = User;

    UserCollection.prototype.initialize = function(models, opts) {
      UserCollection.__super__.initialize.apply(this, arguments);
      return this.reqParams = opts;
    };

    UserCollection.prototype.url = function() {
      var url;
      url = this.getCtx() + '/api/admin/users/';
      if (this.reqParams.GroupID) {
        url = this.getCtx() + '/api/admin/groups/' + this.reqParams.GroupID + '/users';
      } else {
        _.each(this.reqParams, function(v, k) {
          var _ref;
          url += (_ref = url.indexOf('?') !== -1) != null ? _ref : {
            '&': '?'
          };
          return url += k + '=' + encodeURIComponent(v);
        });
      }
      return url;
    };

    return UserCollection;

  })(Collection);

}).call(this);

});

;require.register("views/companysearchview", function(exports, require, module) {
(function() {
  var Companies, CompanySearchView, View, companySearch, companySearchResults,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  View = require('./view');

  companySearch = require('views/templates/company-search');

  companySearchResults = require('views/templates/company-search-results');

  Companies = require('models/companies');

  module.exports = CompanySearchView = (function(_super) {

    __extends(CompanySearchView, _super);

    function CompanySearchView() {
      CompanySearchView.__super__.constructor.apply(this, arguments);
    }

    CompanySearchView.prototype.template = companySearch;

    CompanySearchView.prototype.resultTemplate = companySearchResults;

    CompanySearchView.prototype.companySearch = false;

    CompanySearchView.prototype.initialize = function(opts) {
      if (opts.company) {
        this.companySearch = true;
        this.companyCallback = opts.company;
      }
      return this.companyParams = opts.companyParams || {};
    };

    CompanySearchView.prototype.events = {
      "change .matchable": "doSearch",
      "click .do-company-search": "doSearch",
      "click tbody.search-results tr.company a": "selectCompany",
      "click .cancel-search": "closeBox"
    };

    CompanySearchView.prototype.closeBox = function() {
      $.colorbox.close();
      $('#company-search').hide();
      return false;
    };

    CompanySearchView.prototype.close = function() {
      this.$el.remove();
      return $('#company-search').empty();
    };

    CompanySearchView.prototype.getFormParams = function(def) {
      def = _.extend({}, def);
      return _.reduce(this.$('form .matchable'), function(memo, obj) {
        var el, name, value;
        el = $(obj);
        name = el.attr('name');
        value = $.trim(el.val());
        if (value.length) memo[name] = value;
        return memo;
      }, def);
    };

    CompanySearchView.prototype.doSearch = function() {
      var params, reqs, _this;
      params = this.getFormParams(this.companyParams);
      _this = this;
      if (_.size(params) > 0) {
        this.$('tbody.search-results').html('<tr><td>&nbsp;</td><td colspan="6">Loading...</td></tr>');
        reqs = [];
        if (this.companySearch) {
          if (!this.companys) this.companys = new Companies();
          reqs.push(this.companys.fetch({
            data: _.extend(params, {
              "-quickSearch": "true"
            })
          }));
        }
        $.when.apply(this, reqs).then(function() {
          return _this.showResults.call(_this);
        });
      }
      return false;
    };

    CompanySearchView.prototype.showResults = function() {
      this.$('tbody.search-results').html(this.resultTemplate({
        ctxPath: this.getCtx(),
        companys: this.companys && this.companys.length ? this.companys.toJSON() : false
      }));
      return this.delegateEvents();
    };

    CompanySearchView.prototype.selectCompany = function(evt) {
      var $el, company, companyID, row;
      $el = $(evt.target);
      row = $el.parents('tr:first');
      companyID = row.data('companyId');
      company = this.companys.find(function(obj) {
        return obj.id === companyID;
      });
      console.log('select company with id ' + companyID);
      if (this.companyCallback) this.companyCallback(companyID, company);
      return false;
    };

    CompanySearchView.prototype.render = function() {
      this.$el.html(this.template({
        ctxPath: this.getCtx()
      }));
      return this;
    };

    return CompanySearchView;

  })(View);

}).call(this);

});

;require.register("views/contactusersearchview", function(exports, require, module) {
(function() {
  var ContactUserSearchView, Contacts, Users, View, contactSearch, contactSearchResults,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  View = require('./view');

  contactSearch = require('views/templates/contact-search');

  contactSearchResults = require('views/templates/contact-search-results');

  Contacts = require('models/contacts');

  Users = require('models/users');

  module.exports = ContactUserSearchView = (function(_super) {

    __extends(ContactUserSearchView, _super);

    function ContactUserSearchView() {
      ContactUserSearchView.__super__.constructor.apply(this, arguments);
    }

    ContactUserSearchView.prototype.template = contactSearch;

    ContactUserSearchView.prototype.resultTemplate = contactSearchResults;

    ContactUserSearchView.prototype.contactSearch = false;

    ContactUserSearchView.prototype.userSearch = false;

    ContactUserSearchView.prototype.contactParams = {};

    ContactUserSearchView.prototype.userParams = {};

    ContactUserSearchView.prototype.initialize = function(opts) {
      if (opts.contact) {
        this.contactSearch = true;
        this.contactCallback = opts.contact;
      }
      if (opts.user) {
        this.userSearch = true;
        this.userCallback = opts.user;
      }
      this.contactParams = opts.contactParams || {};
      this.userParams = opts.userParams || {};
      if (!this.contactSearch && !this.userSearch) {
        return console.log('Error - ContactUserSearchView :: You must supply a contact or user callback');
      }
    };

    ContactUserSearchView.prototype.events = {
      "change .matchable": "doSearch",
      "click .do-search": "doSearch",
      "click tbody.search-results tr.contact a": "selectContact",
      "click tbody.search-results tr.user a": "selectUser",
      "click .cancel-search": "closeBox"
    };

    ContactUserSearchView.prototype.closeBox = function() {
      $.colorbox.close();
      $('#contact-search').hide();
      return false;
    };

    ContactUserSearchView.prototype.close = function() {
      this.$el.remove();
      return $('#contact-search').empty();
    };

    ContactUserSearchView.prototype.getFormParams = function(def) {
      def = _.extend({}, def);
      return _.reduce(this.$('form .matchable'), function(memo, obj) {
        var el, name, value;
        el = $(obj);
        name = el.attr('name');
        value = $.trim(el.val());
        if (value.length) memo[name] = value;
        return memo;
      }, def);
    };

    ContactUserSearchView.prototype.doSearch = function(evt) {
      var contactParams, reqs, userParams, _this;
      if (evt && _.isFunction(evt.preventDefault)) evt.preventDefault();
      contactParams = this.contactSearch ? this.getFormParams(this.contactParams) : {};
      userParams = this.userSearch ? this.getFormParams(this.userParams) : {};
      _this = this;
      if (_.size(userParams) > 0 || _.size(contactParams) > 0) {
        this.$('tbody.search-results').html('<tr><td>&nbsp;</td><td colspan="6">Loading...</td></tr>');
        reqs = [];
        if (this.contactSearch) {
          if (!this.contacts) this.contacts = new Contacts();
          reqs.push(this.contacts.fetch({
            data: _.extend(contactParams, {
              "-quickSearch": "true"
            })
          }));
        }
        if (this.userSearch) {
          if (!this.users) this.users = new Users();
          reqs.push(this.users.fetch({
            data: userParams
          }));
        }
        $.when.apply(this, reqs).then(function() {
          return _this.showResults.call(_this);
        });
      }
      return false;
    };

    ContactUserSearchView.prototype.showResults = function() {
      this.$('tbody.search-results').html(this.resultTemplate({
        ctxPath: this.getCtx(),
        contacts: this.contacts && this.contacts.length ? this.contacts.toJSON() : false,
        users: this.users && this.contacts.length ? this.users.toJSON() : false
      }));
      return this.delegateEvents();
    };

    ContactUserSearchView.prototype.selectContact = function(evt) {
      var $el, contact, contactID, row;
      $el = $(evt.target);
      row = $el.parents('tr:first');
      contactID = row.data('contactId');
      contact = this.contacts.find(function(obj) {
        return obj.id === contactID;
      });
      if (this.contactCallback) this.contactCallback(contactID, contact);
      return false;
    };

    ContactUserSearchView.prototype.selectUser = function(evt) {
      var $el, row, user, userID;
      $el = $(evt.target);
      row = $el.parents('tr:first');
      userID = row.data('userId');
      user = this.users.find(function(obj) {
        return obj.id === userID;
      });
      if (this.userCallback) this.userCallback(userID, user);
      return false;
    };

    ContactUserSearchView.prototype.render = function() {
      this.$el.html(this.template({
        ctxPath: this.getCtx()
      }));
      return this;
    };

    return ContactUserSearchView;

  })(View);

}).call(this);

});

;require.register("views/dropzoneview", function(exports, require, module) {
(function() {
  var DropzoneView, LinkDocumentView, View,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  View = require('./view');

  LinkDocumentView = require('./linkdocumentview');

  /*
  
  The drop zone view describes an element which monitors for dropped files, and when it encounters them launches the 
  Link document view with a callback provided for linking the actual files.
  */

  module.exports = DropzoneView = (function(_super) {

    __extends(DropzoneView, _super);

    function DropzoneView() {
      DropzoneView.__super__.constructor.apply(this, arguments);
    }

    DropzoneView.prototype.id = 'dropzone';

    DropzoneView.prototype.initialize = function(opts) {
      this.lightboxEl = opts.lightboxEl;
      this.object = opts.object;
      this.callback = opts.callback;
      this.$el.attr('id', _.uniqueId('dropzone'));
      return this.content = opts.content || ' Drop Files Here ';
    };

    DropzoneView.prototype.render = function() {
      var _this;
      _this = this;
      if (_.isFunction(this.content)) {
        this.$el.html(this.content({
          ctxPath: this.getCtx()
        }));
      } else {
        this.$el.html(this.content);
      }
      this.$el.fileupload().on('fileuploaddrop', function(evt, fo) {
        _this.ldView = window.searchView = new LinkDocumentView({
          object: _this.object,
          files: fo
        });
        _this.$el.fileupload('disable');
        $.colorbox({
          inline: true,
          href: _this.lightboxEl.html(_this.ldView.render().el),
          open: true,
          modal: true,
          opacity: 0.6,
          onClosed: function() {
            _this.ldView.close();
            _this.$el.fileupload('enable');
          }
        });
      });
      return this;
    };

    return DropzoneView;

  })(View);

}).call(this);

});

;require.register("views/linkdocumentview", function(exports, require, module) {
(function() {
  var Document, Documents, ItemList, LinkDocumentView, LinkedDocument, View, docLineTemplate, docUploadTemplate, optionTemplate, searchResultTemplate,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  View = require('./view');

  Documents = require('models/cms/documents');

  Document = require('models/cms/document');

  LinkedDocument = require('models/cms/linkeddocument');

  ItemList = require('models/itemlist');

  docUploadTemplate = require('./templates/upload-documents');

  docLineTemplate = require('./templates/upload-document-line');

  optionTemplate = require('views/templates/option-list');

  searchResultTemplate = require('views/templates/link-document-search-result');

  module.exports = LinkDocumentView = (function(_super) {

    __extends(LinkDocumentView, _super);

    function LinkDocumentView() {
      LinkDocumentView.__super__.constructor.apply(this, arguments);
    }

    LinkDocumentView.prototype.template = docUploadTemplate;

    LinkDocumentView.prototype.id = 'link-document-view';

    LinkDocumentView.prototype.initialize = function(opts) {
      this.obj = opts.object;
      this.galID = opts.galleryID;
      this.docs = new Documents([], {
        baseURL: this.obj.getDocumentURL()
      });
      if (opts.subTypes) {
        this.subTypes = opts.subTypes;
      } else {
        this.subTypes = new ItemList({
          listname: 'DocumentSubType'
        });
        this.subTypes.fetch({
          async: false
        });
      }
      this.types = new ItemList({
        listname: 'DocumentType'
      });
      this.types.fetch({
        async: false
      });
      this.initFiles = opts.files;
      if (!!opts.docSubType) this.docSubType = opts.docSubType;
      return console.log('init link document view');
    };

    LinkDocumentView.prototype.events = {
      'click a#link_documents': 'linkExistingDocuments',
      'click a#upload_documents': 'uploadNewDocuments',
      'click .cancel-upload': 'closeBox',
      'click .save-uploads': 'saveUploads',
      'click #document-search': 'documentSearch',
      'click .add-document': 'addDocument',
      'click .remove-document': 'removeDocument',
      'click span.dropzone-button': 'showHideDropzone'
    };

    LinkDocumentView.prototype.addDocument = function(evt) {
      var $t, doc, docID;
      if (evt && _.isFunction(evt.stopPropagation)) evt.stopPropagation();
      $t = $(evt.currentTarget);
      docID = $t.data('documentId');
      doc = this.search.find(function(doc) {
        return doc.get('DocumentID') === docID;
      });
      if (doc && !this.docs.contains(doc)) {
        this.$('.selected_matches').append(searchResultTemplate(_.extend({
          ctxPath: this.getCtx(),
          FileIcon: this.getIcon(doc.get('FileName')),
          add: false
        }, doc.toJSON())));
        this.docs.add(doc);
      }
      return false;
    };

    LinkDocumentView.prototype.showHideDropzone = function(e) {
      var $btn;
      $btn = $(e.target || e.currentTarget);
      if ($btn.hasClass('showing')) {
        $btn.removeClass('showing');
        this.$('.dropzone').slideUp();
      } else {
        $btn.addClass('showing');
        this.$('.dropzone').slideDown();
      }
      return $.colorbox.resize();
    };

    LinkDocumentView.prototype.removeDocument = function(evt) {
      var $t, doc, docID;
      if (evt && _.isFunction(evt.stopPropagation)) evt.stopPropagation();
      $t = $(evt.currentTarget);
      docID = $t.data('documentId');
      doc = this.docs.find(function(doc) {
        return doc.get('DocumentID') === docID;
      });
      if (doc) this.docs.remove(doc);
      $t.parents('tr:first').remove();
      return false;
    };

    LinkDocumentView.prototype.documentSearch = function(e) {
      var form, params, _this;
      if (e && _.isFunction(e.preventDefault)) e.preventDefault();
      _this = this;
      form = this.$('#document-search-form');
      params = _.reduce(form.serializeArray(), function(memo, obj) {
        memo[obj.name] = obj.value;
        return memo;
      }, {});
      this.search = new Documents([], params);
      this.search.fetch({
        success: function() {
          var m;
          m = _this.$('.matches').empty();
          _this.search.each(function(doc) {
            if (!doc.has('Name') || $.trim(doc.get('Name')) === '') {
              doc.set('Name', doc.get('FileName'));
            }
            m.append(searchResultTemplate(_.extend({
              FileIcon: _this.getIcon(doc.get('FileName')),
              add: true,
              ctxPath: _this.getCtx()
            }, doc.toJSON())));
          });
        }
      });
      return false;
    };

    LinkDocumentView.prototype.saveUploads = function() {
      var reqs, txt, _this;
      reqs = [];
      _this = this;
      txt = this.$("#progresstext").html();
      if (txt !== "" && txt !== "&nbsp;") {
        alert("Please wait for all uploads to complete before saving");
        return false;
      }
      this.docs.each(function(doc) {
        var ld;
        ld = new LinkedDocument({
          DocumentID: doc.get('DocumentID'),
          GalleryID: this.galID
        });
        if (_.isFunction(this.obj.setLinkedDocumentFields)) {
          this.obj.setLinkedDocumentFields(ld);
        } else {
          ld.set(this.obj.idAttribute, this.obj.id);
        }
        return reqs.push(ld.save());
      }, this);
      $.when.apply(this, reqs).then(function() {
        $.colorbox.close();
        return _this.$el.hide();
      });
      return false;
    };

    LinkDocumentView.prototype.linkExistingDocuments = function(evt) {
      this.$('.link_documents').show();
      this.$('.upload_documents').hide();
      return false;
    };

    LinkDocumentView.prototype.uploadNewDocuments = function(evt) {
      this.$('.link_documents').hide();
      this.$('.upload_documents').show();
      return false;
    };

    LinkDocumentView.prototype.closeBox = function() {
      while (this.docs.length) {
        this.docs.at(0).destroy();
      }
      $.colorbox.close();
      this.$el.hide();
      return false;
    };

    LinkDocumentView.prototype.close = function() {
      this.$el.remove();
      return this.$el.empty();
    };

    LinkDocumentView.prototype.render = function() {
      var _this;
      _this = this;
      this.$el.html(this.template({
        title: 'Link Documents',
        ctxPath: this.getCtx(),
        GroupID: this.obj.get('GroupID'),
        DocumentTypes: this.types.get('ListItems'),
        DocumentSubTypes: this.subTypes.get('ListItems')
      }));
      this.$('.link_documents').hide();
      this.$('.matches').on('click', '.add-document', function() {
        return _this.addDocument.apply(_this, arguments);
      });
      this.$('.selected_matches').on('click', '.remove-document', function() {
        return _this.removeDocument.apply(_this, arguments);
      });
      this.$('#fileupload .fileinput-button input[type=file]').on('click', function(e) {
        if (_.isFunction(e.stopPropagation)) return e.stopPropagation();
      });
      this.$('#fileupload .fileinput-button').on('click', function(e) {
        return $(this).find('input[type=file]').click();
      });
      this.$('#fileupload').fileupload({
        process: [
          {
            action: 'load',
            fileTypes: /^image\/(gif|jpeg|png)$/,
            maxFileSize: 20000000
          }, {
            action: 'save'
          }
        ],
        filesContainer: _this.$('#file_list'),
        uploadTemplateId: null,
        downloadTemplateId: null,
        dataType: 'json',
        autoUpload: true,
        submit: function() {
          return $.colorbox.resize();
        },
        done: function(e, data) {
          var $ctx, binding, doc;
          $ctx = $(data.context[0]);
          doc = new Document(data.result[0]);
          doc.on('change', function() {
            if (doc.saving !== true) {
              doc.saving = true;
              return doc.save().success(function() {
                return doc.saving = false;
              });
            }
          });
          doc.set({
            'DocumentType': _this.getDocumentType(doc.get('FileName')),
            'DocumentSubType': $ctx.find('select.sub-type').val(),
            'Name': $ctx.find('input.doc-name').val()
          });
          binding = doc.binding = rivets.bind($ctx, {
            doc: doc
          });
          $ctx.find('.cancel button').on('click', function() {
            binding.unbind();
            delete doc.binding;
            doc.destroy();
            return $ctx.remove();
          });
          return _this.docs.add(doc);
        },
        uploadTemplate: function(o) {
          var renderList, rows;
          renderList = _.map(o.files, function(file) {
            return {
              FileName: file.name,
              FileSize: o.formatFileSize(file.size),
              error: file.error || false,
              SubTypes: _this.subTypes.get('ListItems')
            };
          });
          rows = $(docLineTemplate({
            files: renderList
          }));
          return rows;
        },
        downloadTemplate: function(o) {
          console.log('download template, not used?');
          return false;
        }
      }).bind('fileuploadstart', this.showprogress).bind('fileuploadstop', this.hideprogress);
      /*
              .bind('fileuploadsubmit', @debugfn)
              .bind('fileuploadsend', @debugfn)
              .bind('fileuploaddone', @debugfn)
              .bind('fileuploadfail', @debugfn)
              .bind('fileuploadalways', @debugfn)
              .bind('fileuploadprogress', @debugfn)
              .bind('fileuploadprogressall', @debugfn)
              .bind('fileuploadchange', @debugfn)
              .bind('fileuploadpaste', @debugfn)
              .bind('fileuploadadd', @debugfn)  #(e, { files: [f], originalFiles: [f1, f2] } #called multiple times
              .bind('fileuploaddrop', @debugfn) #(e, { files: [f1, f2, etc } )
              #.bind('fileuploaddragover', @debugfn);
      */
      this.fel = this.$('#fileupload');
      if (this.initFiles) this.fel.fileupload('add', this.initFiles);
      return this;
    };

    LinkDocumentView.prototype.showprogress = function() {
      $('.fileupload-progress').show();
    };

    LinkDocumentView.prototype.hideprogress = function() {
      $('.fileupload-progress').hide();
    };

    LinkDocumentView.prototype.debugfn = function() {
      return console.log(arguments);
    };

    LinkDocumentView.prototype.typeMap = {
      'Excel': ['xls', 'xslx'],
      'Image': ['jpg', 'gif', 'png', 'tiff', 'jpeg'],
      'PDF': ['pdf '],
      'Powerpoint': ['ppt'],
      'Word': [' doc', 'txt', 'rtf'],
      'Zip': ['zip']
    };

    LinkDocumentView.prototype.getDocumentType = function(name) {
      var ext, type;
      type = '-unspecified-';
      ext = _.last(name.toLowerCase().split('.'));
      _.each(this.typeMap, function(exts, docType) {
        if (_.contains(exts, ext)) {
          type = docType;
          return {};
        }
      });
      return type;
    };

    LinkDocumentView.prototype.iconMap = {
      'excel.jpg': ['xls', 'xslx'],
      'image.jpg': ['jpg', 'gif', 'png', 'tiff', 'jpeg'],
      'pdf.jpg': ['pdf '],
      'powerpoint.jpg': ['ppt'],
      'word.jpg': [' doc', 'txt', 'rtf'],
      'zip.jpg': ['zip']
    };

    LinkDocumentView.prototype.getIcon = function(name) {
      var ext, icon;
      ext = _.last(name.toLowerCase().split('.'));
      icon = 'plain_doc.jpg';
      _.each(this.iconMap, function(exts, file) {
        if (_.contains(exts, ext)) {
          icon = file;
          return {};
        }
      });
      return "" + (this.getCtx()) + "/images/tabui/filetypes/" + icon;
    };

    return LinkDocumentView;

  })(View);

}).call(this);

});

;require.register("views/lotselectview", function(exports, require, module) {
(function() {
  var Estates, LotSelectView, Lots, View, lotSearch,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  View = require('./view');

  lotSearch = require('views/templates/lot-search');

  Estates = require('models/cms/properties/estates');

  Lots = require('models/cms/properties/lots');

  module.exports = LotSelectView = (function(_super) {

    __extends(LotSelectView, _super);

    function LotSelectView() {
      LotSelectView.__super__.constructor.apply(this, arguments);
    }

    LotSelectView.prototype.template = lotSearch;

    LotSelectView.prototype.lotSearch = false;

    LotSelectView.prototype.initialize = function(opts) {
      if (opts.lot) {
        this.lotSearch = true;
        this.lotCallback = opts.lot;
      }
      this.estates = new Estates([], {});
      return this.estatePromise = this.estates.fetch();
    };

    LotSelectView.prototype.events = {
      "change .estate-select": "estateSelect",
      "change .stage-select": "stageSelect",
      "click .save-button": "selectLot",
      "click .cancel-search": "closeBox"
    };

    LotSelectView.prototype.closeBox = function() {
      $.colorbox.close();
      $('#lot-search').hide();
      return false;
    };

    LotSelectView.prototype.close = function() {
      this.$el.remove();
      return $('#lot-search').empty();
    };

    LotSelectView.prototype.estateSelect = function() {
      var estate, estateID, ssel, _this;
      _this = this;
      estateID = this.$('.estate-select').val();
      estate = this.estates.find(function(o) {
        return o.id === estateID;
      });
      ssel = _this.$('.stage-select').html('<option value="">- loading -</option>');
      if (estate) {
        this.lots = window.lots = estate.getLots();
        this.lots.fetch({
          success: function() {
            ssel.html('<option value="">- select a stage -</option>');
            _this.lots.chain().reduce(function(memo, lot) {
              var ex;
              ex = _.any(memo, function(o) {
                return o.StageID === lot.get('StageProductID');
              });
              if (ex === false) {
                memo.push({
                  StageID: lot.get('StageProductID'),
                  StageName: lot.get('StageName')
                });
              }
              return memo;
            }, []).sortBy(function(o) {
              return o.StageName;
            }).each(function(stage) {
              var opt;
              opt = $('<option/>').val(stage.StageID).html(stage.StageName);
              opt.appendTo(ssel);
            });
          }
        });
      } else {
        ssel.html('<option value="">- no stages found -</option>');
      }
    };

    LotSelectView.prototype.stageSelect = function() {
      var lsel, ssv;
      ssv = this.$('.stage-select').val();
      lsel = this.$('.lot-select');
      if (ssv.length) {
        lsel.html('<option value="">- select a lot -</option>');
        return this.lots.chain().filter(function(lot) {
          return lot.get('StageProductID') === ssv;
        }).sortBy(function(lot) {
          return new Number(lot.get('Name')).valueOf();
        }).each(function(lot) {
          var opt;
          opt = $('<option/>').val(lot.id).html(lot.get('Name'));
          return opt.appendTo(lsel);
        });
      } else {
        return lsel.html('<option value="">- select a stage -</option>');
      }
    };

    LotSelectView.prototype.selectLot = function() {
      var lot, lotID;
      lotID = this.$('.lot-select').val();
      if (lotID.length) {
        lot = this.lots.find(function(obj) {
          return obj.id === lotID;
        });
        if (this.lotCallback) return this.lotCallback(lotID, lot);
      }
    };

    LotSelectView.prototype.render = function() {
      var _this;
      _this = this;
      this.$el.html(this.template({
        ctxPath: this.getCtx()
      }));
      $.when(this.estatePromise).then(function() {
        var rsel;
        rsel = _this.$('.estate-select').html('<option value="">- select an estate -</option>');
        _this.estates.each(function(estates) {
          var opt;
          opt = $('<option/>').val(estates.id).html(estates.get('Name'));
          opt.appendTo(rsel);
        });
      });
      return this;
    };

    return LotSelectView;

  })(View);

}).call(this);

});

;require.register("views/planselectview", function(exports, require, module) {
(function() {
  var Homes, PlanSelectView, Plans, Ranges, View, planSearch,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  View = require('./view');

  planSearch = require('views/templates/plan-search');

  Ranges = require('models/cms/properties/ranges');

  Homes = require('models/cms/properties/homes');

  Plans = require('models/cms/properties/plans');

  module.exports = PlanSelectView = (function(_super) {

    __extends(PlanSelectView, _super);

    function PlanSelectView() {
      PlanSelectView.__super__.constructor.apply(this, arguments);
    }

    PlanSelectView.prototype.template = planSearch;

    PlanSelectView.prototype.planSearch = false;

    PlanSelectView.prototype.initialize = function(opts) {
      if (opts.plan) {
        this.planSearch = true;
        this.planCallback = opts.plan;
      }
      this.ranges = new Ranges([], {});
      return this.rangePromise = this.ranges.fetch();
    };

    PlanSelectView.prototype.events = {
      "change .range-select": "rangeSelect",
      "change .design-select": "designSelect",
      "click .save-button": "selectPlan",
      "click .cancel-search": "closeBox"
    };

    PlanSelectView.prototype.closeBox = function() {
      $.colorbox.close();
      $('#plan-search').hide();
      return false;
    };

    PlanSelectView.prototype.close = function() {
      this.$el.remove();
      return $('#plan-search').empty();
    };

    PlanSelectView.prototype.rangeSelect = function() {
      var dsel, rangeID, _this;
      _this = this;
      rangeID = this.$('.range-select').val();
      this.designs = new Homes([], {
        RangeProductID: rangeID
      });
      dsel = _this.$('.design-select').html('<option value="">- loading -</option>');
      return this.designs.fetch({
        success: function() {
          dsel.html('<option value="">- select a design -</option>');
          _this.$('.plan-select').html('<option value="">- select a design -</option>');
          _this.designs.each(function(design) {
            var opt;
            opt = $('<option/>').val(design.id).html(design.get('Name'));
            opt.appendTo(dsel);
          });
        }
      });
    };

    PlanSelectView.prototype.designSelect = function() {
      var designID, psel, _this;
      _this = this;
      designID = this.$('.design-select').val();
      this.plans = new Plans([], {
        HomeProductID: designID
      });
      psel = _this.$('.plan-select').html('<option value="">- loading -</option>');
      return this.plans.fetch({
        success: function() {
          psel.html('<option value="">- select a plan -</option>');
          _this.plans.each(function(plan) {
            var opt;
            opt = $('<option/>').val(plan.id).html(plan.get('Name'));
            opt.appendTo(psel);
          });
        }
      });
    };

    PlanSelectView.prototype.selectPlan = function() {
      var plan, planID;
      planID = this.$('.plan-select').val();
      plan = this.plans.find(function(obj) {
        return obj.id === planID;
      });
      if (this.planCallback) return this.planCallback(planID, plan);
    };

    PlanSelectView.prototype.render = function() {
      var _this;
      _this = this;
      this.$el.html(this.template({
        ctxPath: this.getCtx()
      }));
      $.when(this.rangePromise).then(function() {
        var rsel;
        rsel = _this.$('.range-select').html('<option value="">- select a range -</option>');
        _this.ranges.each(function(range) {
          var opt;
          opt = $('<option/>').val(range.id).html(range.get('Name'));
          opt.appendTo(rsel);
        });
      });
      return this;
    };

    return PlanSelectView;

  })(View);

}).call(this);

});

;require.register("views/progressview", function(exports, require, module) {
(function() {
  var Progress, ProgressView, View, progressTemplate,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  View = require('./view');

  Progress = require('models/progress');

  progressTemplate = require('views/templates/display-progress');

  /*
  Goal: Provide a standard JS function which can generate a progress bar in a dialog.
  */

  module.exports = ProgressView = (function(_super) {

    __extends(ProgressView, _super);

    function ProgressView() {
      ProgressView.__super__.constructor.apply(this, arguments);
    }

    ProgressView.prototype.template = progressTemplate;

    ProgressView.prototype.progress = false;

    ProgressView.prototype.initialize = function(opts) {
      window.lastProgress = this;
      this.progressCallback = opts.callback || false;
      if (opts.key) {
        this.progress = new Progress({
          id: opts.key
        });
        this.progress.on('change:p', function(model, p) {
          return this.setProgress(p, model.get('count'), model.get('total'), model.get('title'));
        }, this);
      }
      this.title = opts.title || 'Progress';
      this.icon = opts.icon || 'guy-add';
      return this.start = moment();
    };

    ProgressView.prototype.events = {
      'click .cancel-progress': 'closeBox'
    };

    ProgressView.prototype.setProgress = function(perc, item, total, title) {
      var dur, durstr, end, perms, remaining, time, timestr;
      this.debug('setProgress(perc={},item={},total={},title={}', perc, item, total, title);
      this.$('.progress-element').progressbar({
        value: perc
      });
      if (_.isString(title)) {
        this.$('.progress-title').html(title + "&nbsp;&nbsp;");
      }
      timestr = moment().format('MMMM Do YYYY, h:mm:ss a');
      if (_.isNumber(item) && _.isNumber(total)) {
        this.$('.progress-num').html(item);
        this.$('.progress-total').html(total);
        end = moment();
        time = end - this.start;
        try {
          if (item > 0 && total > 0) {
            perms = time / item;
            remaining = perms * (total - item);
            if (_.isNumber(remaining) && _.isFinite(item) && item > 0) {
              dur = moment.duration(remaining);
              durstr = dur.minutes() === 0 && dur.seconds() > 4 ? 'less than a minute' : dur.humanize();
              timestr += ' <strong>Estimated Remaining:</strong> ' + durstr;
            }
          }
        } catch (e) {
          console.log(e);
        }
      }
      this.$('.progress-time').html(timestr);
    };

    ProgressView.prototype.setSubProgress = function(subId, perc, item, total, remaining) {
      var dur, durstr, subDiv;
      subDiv = this.$('#' + subId);
      subDiv.find('.progress').progressbar({
        value: perc
      });
      if (_.isNumber(item) && _.isNumber(total)) {
        subDiv.find('.prog-item').html(item);
        subDiv.find('.prog-total').html(total);
        if (_.isNumber(remaining) && _.isFinite(item) && item > 0) {
          dur = moment.duration(remaining);
          durstr = dur.minutes() === 0 && dur.seconds() > 4 ? 'less than a minute' : dur.humanize();
          subDiv.find('.prog-remaining').html('&nbsp;Remaining: ' + durstr);
        }
      }
      $.colorbox.resize();
    };

    ProgressView.prototype.createSubProgress = function(name) {
      var div, subDiv, subId;
      subId = _.uniqueId('sub-progress');
      div = this.subProgressHolder || (this.subProgressHolder = this.$('#subprogress'));
      subDiv = $('<div/>').attr('id', subId).html('<div>' + name + ': <span class="prog-item">-</span>/<span class="prog-total">-</span>&nbsp;<span class="prog-remaining"></span></div><div class="progress"></div>').appendTo(div);
      subDiv.find('.progress').progressbar({
        value: 0
      });
      $.colorbox.resize();
      return subId;
    };

    ProgressView.prototype.removeSubProgress = function(subId) {
      (this.subProgressHolder || (this.subProgressHolder = this.$('#subprogress'))).find('#' + subId).remove();
      $.colorbox.resize();
    };

    ProgressView.prototype.monitorProgress = function() {
      var val, _this;
      if (this.progress) {
        _this = this;
        return this.progress.fetch().success(function() {
          if (_this.progress.get('complete') === false) {
            return setTimeout(function() {
              return _this.monitorProgress.call(_this);
            }, 1000);
          } else {
            return _this.closeBox();
          }
        });
      } else if (_.isFunction(this.progressCallback)) {
        val = new Number(this.progressCallback());
        if (_.isNumber(val) && _.isFinite(val)) {
          this.setProgress(val.valueOf());
          return setTimeout(function() {
            return _this.monitorProgress.call(_this);
          }, 1000);
        }
      }
    };

    ProgressView.prototype.render = function() {
      var _this;
      _this = this;
      this.$el.html(this.template({
        ctxPath: this.getCtx(),
        title: this.title,
        icon: this.icon
      }));
      this.setProgress(0);
      setTimeout(function() {
        return _this.monitorProgress.call(_this);
      }, 1000);
      return this;
    };

    ProgressView.prototype.closeBox = function() {
      if (this.progress) this.progress.destroy();
      $.colorbox.close();
      this.$el.hide();
      return false;
    };

    ProgressView.prototype.close = function() {
      return this.$el.remove();
    };

    return ProgressView;

  })(View);

}).call(this);

});

;require.register("views/templates/company-search-results", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n<tr style=\"background-color: #FFFFFF; font-size:12px; vertical-align:top;\" class=\"company\" data-company-id=\"";
  foundHelper = helpers.CompanyID;
  stack1 = foundHelper || depth0.CompanyID;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "CompanyID", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">\n   	<td style=\"width: 5px; height:24px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" valign=\"bottom\">&nbsp;</td>\n       <td style=\"text-align: left; height:24px; width: 100px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.Company;
  stack1 = foundHelper || depth0.Company;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Company", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 60px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n    <a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.Phone;
  stack1 = foundHelper || depth0.Phone;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Phone", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 120px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.Email;
  stack1 = foundHelper || depth0.Email;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Email", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 90px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		";
  foundHelper = helpers.PostalAddress;
  stack1 = foundHelper || depth0.PostalAddress;
  stack2 = helpers['with'];
  tmp1 = self.program(2, program2, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n       <td style=\"width: 10px; height:24px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" valign=\"bottom\">&nbsp;</td>\n   </tr>\n";
  return buffer;}
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n		<a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.City;
  stack1 = foundHelper || depth0.City;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "City", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 40px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.State;
  stack1 = foundHelper || depth0.State;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "State", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n		";
  return buffer;}

  foundHelper = helpers.companys;
  stack1 = foundHelper || depth0.companys;
  stack2 = helpers.each;
  tmp1 = self.program(1, program1, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }});
});

;require.register("views/templates/company-search", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


  buffer += "<form name=\"quicknoteForm\">\n	<div class=\"quickNoteBox\">\n	  <div class=\"quickNoteHeader\">\n	  		<div class=\"quickNoteTitle\"><strong><span class=\"sprite-50 guy-add\">&nbsp;</span>&nbsp;&nbsp;Find Company</strong></div>\n	  		<div class=\"quickNoteClose\"><a href=\"#\" class=\"cancel-search\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/quicknote/close_box.png\" alt=\"Close\" width=\"19\" height=\"19\" border=\"0\"></a></div>\n	  </div>\n	  <div class=\"quickNoteBody\">\n	    <div class=\"quickNoteSearchFields\">\n	    <table>\n	    <tbody><tr>\n	       <td><input name=\"-Company\" type=\"text\" id=\"Company\" placeholder=\"Company\" class=\"matchable\" size=\"20\"></td>\n	       <td><input name=\"-Email\" type=\"text\" id=\"Email\" placeholder=\"Email\" class=\"matchable\" size=\"20\"></td>\n	   	   <td><a href=\"#\" class=\"do-company-search\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/go.png\" alt=\"Company search\" style=\"vertical-align: bottom; margin-left: 10px; margin-top: 8px;\"></a></td>\n	   	</tr>\n	   	</tbody></table>\n	 </div>\n	<div class=\"quickNoteMatches\" id=\"companymatches\"><table width=\"862\" cellpadding=\"0\" cellspacing=\"0\">\n		<tbody><tr>\n			<td>&nbsp;</td>\n			<td style=\"border: 1px solid #DDDDDD; padding: 0px;\">\n			<table width=\"862\" style=\"background-color: #FFF; \" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n			<thead>\n			<tr>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; width: 10px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">&nbsp;</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 160px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">Company</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 96px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">Phone</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 191px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">Email</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 143px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">Suburb</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 91px; padding: 4px 0px 4px 0px; \">State</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; width: 10px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">&nbsp;</td>\n			</tr>\n			</thead>\n				</table>\n				<div style=\"padding-right: 3px; padding-left: 3px; height: 220px; overflow-y: scroll;overflow-x: hidden; background-color: #FFFFFF; vertical-align:top; \">\n					<!-- TODO, combine these tables as per vpb options -->\n					<table width=\"100%\" style=\"background-color: #FFF;\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n						<tbody class=\"search-results\">\n				        \n						</tbody>\n					</table>\n				</div>\n			</td>\n		</tr>\n	</tbody></table></div>\n	  </div>\n	  <div class=\"quickNoteFooter\"><!--  <img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/quicknote/spacer.gif\" width=\"20\" height=\"35\" />-->\n	  <a href=\"#\" class=\"cancel-search\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/cancel.png\" width=\"136\" height=\"35\" alt=\"Cancel\" border=\"0\"></a>\n	  </div>\n	</div>\n</form>";
  return buffer;});
});

;require.register("views/templates/contact-search-results", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n<tr style=\"background-color: #FFFFFF; font-size:12px; vertical-align:top;\" class=\"user\" data-user-id=\"";
  foundHelper = helpers.UserID;
  stack1 = foundHelper || depth0.UserID;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "UserID", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">\n	<td style=\"width: 5px; height:24px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" valign=\"bottom\">&nbsp;</td>\n	<td style=\"text-align: left; height:24px; width: 120px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" colspan=\"6\">\n		<strong>Contact Results:</strong>\n	</td>\n	<td style=\"width: 5px; height:24px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" valign=\"bottom\">&nbsp;</td>\n</tr>\n";
  return buffer;}

function program3(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n<tr style=\"background-color: #FFFFFF; font-size:12px; vertical-align:top;\" class=\"contact\" data-contact-id=\"";
  foundHelper = helpers.ContactID;
  stack1 = foundHelper || depth0.ContactID;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ContactID", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">\n   	<td style=\"width: 5px; height:24px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" valign=\"bottom\">&nbsp;</td>\n       <td style=\"text-align: left; height:24px; width: 100px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this contact\">";
  foundHelper = helpers.FirstName;
  stack1 = foundHelper || depth0.FirstName;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "FirstName", { hash: {} }); }
  buffer += escapeExpression(stack1) + " ";
  foundHelper = helpers.LastName;
  stack1 = foundHelper || depth0.LastName;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "LastName", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 100px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this contact\">";
  foundHelper = helpers.Company;
  stack1 = foundHelper || depth0.Company;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Company", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 60px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this contact\">";
  foundHelper = helpers.Phone;
  stack1 = foundHelper || depth0.Phone;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Phone", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 120px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this contact\">";
  foundHelper = helpers.Email;
  stack1 = foundHelper || depth0.Email;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Email", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n		";
  foundHelper = helpers.PhysicalAddress;
  stack1 = foundHelper || depth0.PhysicalAddress;
  stack2 = helpers['with'];
  tmp1 = self.program(4, program4, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n       <td style=\"width: 10px; height:24px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" valign=\"bottom\">&nbsp;</td>\n   </tr>\n";
  return buffer;}
function program4(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n	       <td style=\"text-align: left; height:24px; width: 90px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n			<a href=\"#\" title=\"select this contact\">";
  foundHelper = helpers.City;
  stack1 = foundHelper || depth0.City;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "City", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n	       <td style=\"text-align: left; height:24px; width: 40px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n			<a href=\"#\" title=\"select this contact\">";
  foundHelper = helpers.State;
  stack1 = foundHelper || depth0.State;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "State", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n		";
  return buffer;}

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n<tr style=\"background-color: #FFFFFF; font-size:12px; vertical-align:top;\" class=\"user\" data-user-id=\"";
  foundHelper = helpers.UserID;
  stack1 = foundHelper || depth0.UserID;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "UserID", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">\n	<td style=\"width: 5px; height:24px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" valign=\"bottom\">&nbsp;</td>\n	<td style=\"text-align: left; height:24px; width: 120px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" colspan=\"6\">\n		<strong>User Results:</strong>\n	</td>\n	<td style=\"width: 5px; height:24px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" valign=\"bottom\">&nbsp;</td>\n</tr>\n";
  return buffer;}

function program8(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n<tr style=\"background-color: #FFFFFF; font-size:12px; vertical-align:top;\" class=\"user\" data-user-id=\"";
  foundHelper = helpers.UserID;
  stack1 = foundHelper || depth0.UserID;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "UserID", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">\n   	<td style=\"width: 5px; height:24px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" valign=\"bottom\">&nbsp;</td>\n       <td style=\"text-align: left; height:24px; width: 100px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.FirstName;
  stack1 = foundHelper || depth0.FirstName;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "FirstName", { hash: {} }); }
  buffer += escapeExpression(stack1) + " ";
  foundHelper = helpers.LastName;
  stack1 = foundHelper || depth0.LastName;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "LastName", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 100px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.Company;
  stack1 = foundHelper || depth0.Company;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Company", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 60px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.Phone;
  stack1 = foundHelper || depth0.Phone;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Phone", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 120px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.Email;
  stack1 = foundHelper || depth0.Email;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Email", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 90px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		";
  foundHelper = helpers.Address;
  stack1 = foundHelper || depth0.Address;
  stack2 = helpers['with'];
  tmp1 = self.program(9, program9, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n       <td style=\"width: 10px; height:24px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\" valign=\"bottom\">&nbsp;</td>\n   </tr>\n";
  return buffer;}
function program9(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n		<a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.City;
  stack1 = foundHelper || depth0.City;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "City", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n       <td style=\"text-align: left; height:24px; width: 40px; padding: 4px 0px 4px 0px; border-bottom:1px solid #999999;\">\n		<a href=\"#\" title=\"select this user\">";
  foundHelper = helpers.State;
  stack1 = foundHelper || depth0.State;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "State", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</a></td>\n		";
  return buffer;}

  foundHelper = helpers.contacts;
  stack1 = foundHelper || depth0.contacts;
  stack2 = helpers['if'];
  tmp1 = self.program(1, program1, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  foundHelper = helpers.contacts;
  stack1 = foundHelper || depth0.contacts;
  stack2 = helpers.each;
  tmp1 = self.program(3, program3, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  foundHelper = helpers.users;
  stack1 = foundHelper || depth0.users;
  stack2 = helpers['if'];
  tmp1 = self.program(6, program6, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  foundHelper = helpers.users;
  stack1 = foundHelper || depth0.users;
  stack2 = helpers.each;
  tmp1 = self.program(8, program8, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;});
});

;require.register("views/templates/contact-search", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


  buffer += "<form name=\"quicknoteForm\">\n	<div class=\"quickNoteBox\">\n	  <div class=\"quickNoteHeader\">\n	  		<div class=\"quickNoteTitle\"><strong><span class=\"sprite-50 guy-add\">&nbsp;</span>&nbsp;&nbsp;Find Contact</strong></div>\n	  		<div class=\"quickNoteClose\"><a href=\"#\" class=\"cancel-search\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/quicknote/close_box.png\" alt=\"Close\" width=\"19\" height=\"19\" border=\"0\"></a></div>\n	  </div>\n	  <div class=\"quickNoteBody\">\n	    <div class=\"quickNoteSearchFields\">\n	    <table>\n	    <tbody><tr>\n	       <td><input name=\"-FirstName\" type=\"text\" id=\"FirstName\" placeholder=\"First Name\" class=\"matchable\" size=\"15\"></td>\n	       <td><input name=\"-LastName\" type=\"text\" id=\"LastName\" placeholder=\"Last Name\" class=\"matchable\" size=\"15\"></td>\n	       <td><input name=\"-Company\" type=\"text\" id=\"Company\" placeholder=\"Company\" class=\"matchable\" size=\"20\"></td>\n	       <td><input name=\"-Email\" type=\"text\" id=\"Email\" placeholder=\"Email\" class=\"matchable\" size=\"20\"></td>\n	   	   <td><a href=\"#\" class=\"do-search\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/go.png\" alt=\"Contact search\" style=\"vertical-align: bottom; margin-left: 10px; margin-top: 8px;\"></a></td>\n	   	</tr>\n	   	</tbody></table>\n	 </div>\n	<div class=\"quickNoteMatches\" id=\"contactmatches\"><table width=\"862\" cellpadding=\"0\" cellspacing=\"0\">\n		<tbody><tr>\n			<td>&nbsp;</td>\n			<td style=\"border: 1px solid #DDDDDD; padding: 0px;\">\n			<table width=\"862\" style=\"background-color: #FFF; \" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n			<thead>\n			<tr>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; width: 10px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">&nbsp;</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 160px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">Name</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 160px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">Company</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 96px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">Phone</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 191px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">Email</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 143px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">Suburb</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; text-align: left; width: 91px; padding: 4px 0px 4px 0px; \">State</td>\n				<td nowrap=\"\" style=\"background-color: #e0e3e7; width: 10px; padding: 4px 0px 4px 0px;\" valign=\"bottom\">&nbsp;</td>\n			</tr>\n			</thead>\n				</table>\n				<div style=\"padding-right: 3px; padding-left: 3px; height: 220px; overflow-y: scroll;overflow-x: hidden; background-color: #FFFFFF; vertical-align:top; \">\n					<!-- TODO, combine these tables as per vpb options -->\n					<table width=\"100%\" style=\"background-color: #FFF;\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n						<tbody class=\"search-results\">\n				        \n						</tbody>\n					</table>\n				</div>\n			</td>\n		</tr>\n	</tbody></table></div>\n	  </div>\n	  <div class=\"quickNoteFooter\"><!--  <img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/quicknote/spacer.gif\" width=\"20\" height=\"35\" />-->\n	  <a href=\"#\" class=\"cancel-search\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/cancel.png\" width=\"136\" height=\"35\" alt=\"Cancel\" border=\"0\"></a>\n	  </div>\n	</div>\n</form>";
  return buffer;});
});

;require.register("views/templates/display-progress", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


  buffer += "<form name=\"quicknoteForm\">\n	<div class=\"quickNoteBox\">\n	  <div class=\"quickNoteHeader\">\n	  		<div class=\"quickNoteTitle\"><strong><span class=\"sprite-50 ";
  foundHelper = helpers.icon;
  stack1 = foundHelper || depth0.icon;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "icon", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">&nbsp;</span>&nbsp;&nbsp;<span class=\"progress-title\">";
  foundHelper = helpers.title;
  stack1 = foundHelper || depth0.title;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "title", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</span></strong></div>\n	  		<div class=\"quickNoteClose\"><a href=\"#\" class=\"cancel-progress\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/quicknote/close_box.png\" alt=\"Close\" width=\"19\" height=\"19\" border=\"0\"></a></div>\n	  </div>\n	  <div class=\"quickNoteBody\">\n	  			<div class=\"progress-element\"></div>\n	  			<table>\n	  				<tr><td class=\"progress-title\"></td><td><strong>Complete: </strong><td class=\"progress-num\"></td><td><strong>of</strong></td><td class=\"progress-total\"></td><td><strong>Last Checked:</strong></td><td class=\"progress-time\"></td>\n	  				</tr>\n	  			</table>\n	  			<div id=\"subprogress\"></div>\n	  </div>\n	  <div class=\"quickNoteFooter\"><!--  <img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/quicknote/spacer.gif\" width=\"20\" height=\"35\" />-->\n	  <a href=\"#\" class=\"cancel-progress\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/close.png\" width=\"136\" height=\"35\" alt=\"Cancel\" border=\"0\"></a>\n	  </div>\n	</div>\n</form>";
  return buffer;});
});

;require.register("views/templates/link-document-search-result", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<a href=\"#\" class=\"add-document\" data-document-id=\"";
  foundHelper = helpers.DocumentID;
  stack1 = foundHelper || depth0.DocumentID;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "DocumentID", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/properties/arrowleft.png\"></a>";
  return buffer;}

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<a href=\"#\" class=\"remove-document\" data-document-id=\"";
  foundHelper = helpers.DocumentID;
  stack1 = foundHelper || depth0.DocumentID;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "DocumentID", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/icons/delete_icon.png\"></a>";
  return buffer;}

  buffer += "<tr>\n	<td style=\"width: 100px; width: 100px; padding: 5px 5px 5px 0px; border-top: 1px solid #D7DAE8;\"><img width=\"45\" height=\"60\" src=\"";
  foundHelper = helpers.FileIcon;
  stack1 = foundHelper || depth0.FileIcon;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "FileIcon", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\"></td>\n	<td style=\"font-size: 12px; border-top: 1px solid #D7DAE8; \">";
  foundHelper = helpers.Name;
  stack1 = foundHelper || depth0.Name;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Name", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</td>\n	<td style=\"font-size: 12px; border-top: 1px solid #D7DAE8;\"></td>\n	<td width=\"50%\" align=\"right\" style=\"padding: 0px 5px 0px 5px; border-top: 1px solid #D7DAE8;\">";
  foundHelper = helpers.add;
  stack1 = foundHelper || depth0.add;
  stack2 = helpers['if'];
  tmp1 = self.program(1, program1, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.program(3, program3, data);
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</td>\n</tr>";
  return buffer;});
});

;require.register("views/templates/lot-search", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


  buffer += "<table id=\"runwayMapsLBMain\" width=\"330\" height=\"100%\" align=\"center\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n<tbody>\n  <tr>\n    <td class=\"runwayMapsLBCorner\" width=\"5\" align=\"right\" valign=\"top\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/lb_topLeft.png\" alt=\" \" width=\"5\" height=\"28\" /></td>\n    <td class=\"runwayMapsLBBar\" id=\"runwayMapsLBTitle\">Select a Lot</td>\n    <td class=\"runwayMapsLBBar\" align=\"right\"><a href=\"#\" class=\"cancel-search\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/bttn_LBClose.jpg\" alt=\"Close\" width=\"28\" height=\"28\" border=\"0\" /></a></td>\n    <td class=\"runwayMapsLBCorner\" width=\"5\" align=\"left\" valign=\"top\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/lb_topRight.png\" alt=\" \" width=\"5\" height=\"28\" /></td>\n  </tr>\n<tr>\n    <td class=\"runwayMapsBorderLeft\">&nbsp;</td>\n	  <td colspan=\"2\"><table width=\"100%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"details\">\n		<tr>\n			<td class=\"details_bold\">Estate</td>\n			<td><select class=\"estate-select searchfieldautowidth\"><option value=\"\">- loading -</option></select></td>\n		</tr>\n		<tr>\n			<td class=\"details_bold\">Stage</td>\n			<td><select class=\"searchfieldautowidth stage-select\">\n        		<option value=\"\">- select an estate -</option>\n				</select></td>\n		</tr>\n		<tr>\n			<td class=\"details_bold\">Lot</td>\n			<td><select class=\"searchfieldautowidth lot-select\">\n        		<option value=\"\">- select an estate -</option>\n				</select></td>\n		</tr>\n		<tr>\n			<td colspan=\"2\">&nbsp;\n			</td>\n		</tr>\n		<tr>\n			<td colspan=\"2\"  align=\"center\">\n				<a href=\"#\" class=\"cancel-search\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/cancel.png\" /></a>\n				<a href=\"#\" class=\"save-button\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/save.png\" /></a>\n			</td>\n		</tr>				\n		</table>\n		</form>\n	</td>\n	<td class=\"runwayMapsBorderRight\">&nbsp;</td>\n</tr>\n  <tr>\n    <td class=\"runwayMapsLBCorner\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/lb_bttmLeft.png\" alt=\" \" width=\"5\" height=\"5\" /></td>\n    <td class=\"runwayMapsBorderBttm\">&nbsp;</td>\n    <td class=\"runwayMapsBorderBttm\">&nbsp;</td>\n    <td class=\"runwayMapsLBCorner\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/lb_bttmRight.png\" alt=\" \" width=\"5\" height=\"5\" /></td>\n  </tr>\n</tbody>\n</table>";
  return buffer;});
});

;require.register("views/templates/option-list", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "<option value=\"";
  foundHelper = helpers.value;
  stack1 = foundHelper || depth0.value;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "value", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">";
  foundHelper = helpers.text;
  stack1 = foundHelper || depth0.text;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "text", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</option>";
  return buffer;}

  foundHelper = helpers.options;
  stack1 = foundHelper || depth0.options;
  stack2 = helpers.each;
  tmp1 = self.program(1, program1, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }});
});

;require.register("views/templates/plan-search", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


  buffer += "<table id=\"runwayMapsLBMain\" width=\"310\" height=\"100%\" align=\"center\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n<tbody>\n  <tr>\n    <td class=\"runwayMapsLBCorner\" width=\"5\" align=\"right\" valign=\"top\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/lb_topLeft.png\" alt=\" \" width=\"5\" height=\"28\" /></td>\n    <td class=\"runwayMapsLBBar\" id=\"runwayMapsLBTitle\">Select a Plan</td>\n    <td class=\"runwayMapsLBBar\" align=\"right\"><a href=\"#\" class=\"cancel-search\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/bttn_LBClose.jpg\" alt=\"Close\" width=\"28\" height=\"28\" border=\"0\" /></a></td>\n    <td class=\"runwayMapsLBCorner\" width=\"5\" align=\"left\" valign=\"top\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/lb_topRight.png\" alt=\" \" width=\"5\" height=\"28\" /></td>\n  </tr>\n<tr>\n    <td class=\"runwayMapsBorderLeft\">&nbsp;</td>\n	  <td colspan=\"2\"><table width=\"100%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"details\">\n		<tr>\n			<td class=\"details_bold\">Range</td>\n			<td><select class=\"range-select searchfieldautowidth\"><option value=\"\">- loading -</option></select></td>\n		</tr>\n		<tr>\n			<td class=\"details_bold\">Design</td>\n			<td> \n				<select class=\"design-select searchfieldautowidth\">\n	          <option value=\"\">- select a range -</option>\n				</select></td>\n		</tr>	\n		<tr>\n			<td class=\"details_bold\">Plan</td>\n			<td><select class=\"searchfieldautowidth plan-select\">\n        <option value=\"\">- select a range -</option>\n				</select></td>\n		</tr>\n		<tr>\n			<td colspan=\"2\">&nbsp;\n			</td>\n		</tr>\n		<tr>\n			<td colspan=\"2\"  align=\"center\">\n				<a href=\"#\" class=\"cancel-search\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/cancel.png\" /></a>\n				<a href=\"#\" class=\"save-button\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/save.png\" /></a>\n			</td>\n		</tr>				\n		</table>\n		</form>\n	</td>\n	<td class=\"runwayMapsBorderRight\">&nbsp;</td>\n</tr>\n  <tr>\n    <td class=\"runwayMapsLBCorner\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/lb_bttmLeft.png\" alt=\" \" width=\"5\" height=\"5\" /></td>\n    <td class=\"runwayMapsBorderBttm\">&nbsp;</td>\n    <td class=\"runwayMapsBorderBttm\">&nbsp;</td>\n    <td class=\"runwayMapsLBCorner\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/lb_bttmRight.png\" alt=\" \" width=\"5\" height=\"5\" /></td>\n  </tr>\n</tbody>\n</table>";
  return buffer;});
});

;require.register("views/templates/upload-document-line", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, stack3;
  buffer += "\n<tr class=\"template-upload grey-spacer\">\n	<td class=\"preview\"><span></span></td>\n	<td class=\"name\" title=\"";
  foundHelper = helpers.FileName;
  stack1 = foundHelper || depth0.FileName;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "FileName", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">";
  foundHelper = helpers.FileName;
  stack1 = foundHelper || depth0.FileName;
  stack2 = {};
  stack3 = 15;
  stack2['lower'] = stack3;
  stack3 = 20;
  stack2['upper'] = stack3;
  foundHelper = helpers.abbreviate;
  stack3 = foundHelper || depth0.abbreviate;
  tmp1 = {};
  tmp1.hash = stack2;
  if(typeof stack3 === functionType) { stack1 = stack3.call(depth0, stack1, tmp1); }
  else if(stack3=== undef) { stack1 = helperMissing.call(depth0, "abbreviate", stack1, tmp1); }
  else { stack1 = stack3; }
  buffer += escapeExpression(stack1) + "<!--<input type=\"text\" class=\"doc-name\" value=\"\" data-value=\"doc.Name\" />--></td>\n	<td class=\"size\">\n		<select class=\"sub-type\" data-value=\"doc.DocumentSubType\">\n			<option value=\"\">&nbsp;</option>\n			";
  foundHelper = helpers.SubTypes;
  stack1 = foundHelper || depth0.SubTypes;
  stack2 = helpers.each;
  tmp1 = self.program(2, program2, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		</select>\n	</td>\n	<td class=\"caption\"><input type=\"text\" class=\"doc-cap\" value=\"";
  foundHelper = helpers.DocumentCaption;
  stack1 = foundHelper || depth0.DocumentCaption;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "DocumentCaption", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\" data-value=\"doc.DocumentCaption\" /></td>\n    <!--\n	<td class=\"remove-document\"><span class=\"cancel-button runway-sprite cross-large\">&nbsp;</span></td>\n    -->\n</tr>\n";
  return buffer;}
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n				<option value=\"";
  foundHelper = helpers.Value;
  stack1 = foundHelper || depth0.Value;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Value", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">";
  foundHelper = helpers.Label;
  stack1 = foundHelper || depth0.Label;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Label", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</option>\n			";
  return buffer;}

  foundHelper = helpers.files;
  stack1 = foundHelper || depth0.files;
  stack2 = helpers.each;
  tmp1 = self.program(1, program1, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }});
});

;require.register("views/templates/upload-documents", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, stack2, foundHelper, tmp1, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n						  	 <option value=\"";
  foundHelper = helpers.Value;
  stack1 = foundHelper || depth0.Value;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Value", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">";
  foundHelper = helpers.Label;
  stack1 = foundHelper || depth0.Label;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Label", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</option>\n						  ";
  return buffer;}

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n						  	 <option value=\"";
  foundHelper = helpers.Value;
  stack1 = foundHelper || depth0.Value;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Value", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\">";
  foundHelper = helpers.Label;
  stack1 = foundHelper || depth0.Label;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "Label", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</option>\n						  ";
  return buffer;}

  buffer += "<div class=\"quickNoteBox\">\n	<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"padding: 0px 0px 0px 0px; margin:0px 0px 0px 0px;\">\n	<tr>\n		<td style=\"background: #DDDFE8; padding: 13px; border-bottom: 1px solid #99A0C1; font-size: 20px; overflow: hidden; font-size: 24px;\" nowrap><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/properties/image_upload_icon.png\" alt=\"";
  foundHelper = helpers.title;
  stack1 = foundHelper || depth0.title;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "title", { hash: {} }); }
  buffer += escapeExpression(stack1) + "\" style=\"vertical-align: -40%;\"/>&nbsp;&nbsp;";
  foundHelper = helpers.title;
  stack1 = foundHelper || depth0.title;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "title", { hash: {} }); }
  buffer += escapeExpression(stack1) + "</td>\n		\n		<td style=\"background: #DDDFE8; padding: 13px; border-bottom: 1px solid #99A0C1; font-size: 20px;\" width=\"80%\">&nbsp;</td>\n\n		<td style=\"background: #DDDFE8; padding: 0px 0px 0px 0px; margin:0px 0px 0px 0px; vertical-align:bottom;\" valign=\"bottom\" nowrap><div class=\"link_documents\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/properties/link_tab_active.png\" style=\"vertical-align: bottom;\" /><a href=\"#\" id=\"upload_documents\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/properties/upload_tab_inactive.png\"  /></a></div>\n			<div class=\"upload_documents\"><a href=\"#\" id=\"link_documents\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/properties/link_tab_inactive.png\" style=\"vertical-align: bottom;\" /></a><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/properties/upload_tab_active.png\"  /></div></td>\n		<td style=\"background: #DDDFE8; padding: 13px 13px 13px 23px; border-bottom: 1px solid #99A0C1; font-size: 20px;\" ><a href=\"#\" class=\"cancel-upload\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/quicknote/close_box.png\" alt=\"Close\" width=\"19\" height=\"19\" border=\"0\" /></a></td>\n	</tr>\n	</table>\n	<div class=\"link_documents\">\n		<form id=\"document-search-form\">\n			<table cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">\n			<tr>\n				<td style=\"padding: 10px 10px 10px 20px; background: #EBECF0; font-size: 18px; width: 120px;\">Search:</td>\n				<td align=\"left\" style=\"padding: 10px 10px 10px 0px; background: #EBECF0;\">\n					<input type=\"text\" name=\"search\" value=\"\" size=\"20\"/>	\n				</td>\n				<td align=\"left\" style=\"padding: 10px 10px 10px 0px; background: #EBECF0;\">\n					<select name=\"DocumentType\" size=\"1\" >\n					      <option value=\"\">Type</option>\n						  ";
  foundHelper = helpers.DocumentTypes;
  stack1 = foundHelper || depth0.DocumentTypes;
  stack2 = helpers.each;
  tmp1 = self.program(1, program1, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n				      </select>	\n				</td>		\n				<td align=\"left\" style=\"padding: 10px 10px 10px 0px; background: #EBECF0;\">\n					<select name=\"DocumentSubType\" size=\"1\" >\n					      <option value=\"\">Sub Type</option>\n						  ";
  foundHelper = helpers.DocumentSubTypes;
  stack1 = foundHelper || depth0.DocumentSubTypes;
  stack2 = helpers.each;
  tmp1 = self.program(3, program3, data);
  tmp1.hash = {};
  tmp1.fn = tmp1;
  tmp1.inverse = self.noop;
  stack1 = stack2.call(depth0, stack1, tmp1);
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n				      </select>	\n				</td>\n				<td style=\"padding: 10px 10px 10px 0px; background: #EBECF0;\"><input type=\"image\" src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/go.png\" style=\"vertical-align: bottom;\" id=\"document-search\" /></td>\n				<td width=\"80%\" style=\"padding: 10px 0px 10px 0px; background: #EBECF0;\">&nbsp;</td>\n			</tr>\n			</table>\n			</form>\n			<div class=\"quickNoteBody\" style=\"align: center; padding: 10px 20px 0px 20px;\">\n				<table cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">\n				<tr>\n					<td style=\"padding-right: 5px;\" width=\"50%\">\n						<div id=\"matchesbox\" style=\"height: 400px; overflow-y:auto;\">\n						<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n						<tr>\n							<td style=\"font-size: 16px; padding: 5px 5px 5px 0px;\" colspan=\"4\">Found Items</td>\n						</tr>\n						</table>\n						<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"matches\">\n						\n						</table>\n						<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n						<tr>\n							<td style=\"font-size: 16px; border-top: 1px solid #D7DAE8;\" colspan=\"4\">&nbsp</td>\n						</tr>\n						</table>	\n						</div>\n					</td>\n					<td style=\"padding-left: 5px;\" width=\"50%\">\n						<div style=\"height: 400px; overflow-y:auto;\">\n						<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n						<tr>\n							<td style=\"font-size: 16px; padding: 5px 5px 5px 0px;\" colspan=\"4\">Selected Items</td>\n						</tr>\n						</table>\n						<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"selected_matches\">\n						\n						</table>\n						<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n						<tr>\n							<td style=\"font-size: 16px; border-top: 1px solid #D7DAE8;\" colspan=\"4\">&nbsp;</td>\n						</tr>\n						</table>\n						</div>\n					</td>\n				</tr>\n			</table>\n		</div>\n	</div>\n	<div class=\"upload_documents gallery-inner\">\n		<table cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\" class=\"header\">\n		<tr>\n			<td class=\"font20\">Upload Images:</td>\n			<td><form id=\"fileupload\" action=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/api/documents/\" method=\"POST\" enctype=\"multipart/form-data\" onsubmit=\"return false;\">\n					<div class=\"row fileupload-buttonbar\">\n						<div class=\"span7\">\n							<span class=\"fileinput-button font14\">\n								<button>Browse</button>\n								<input type=\"file\" name=\"files[]\" multiple>\n							</span>\n						</div>\n						<!-- The global progress information -->\n						<div class=\"span5 fileupload-progress fade\">\n							<!-- The global progress bar -->\n							<div class=\"progress progress-success progress-striped active\" role=\"progressbar\" aria-valuemin=\"0\" aria-valuemax=\"100\">\n								<div class=\"bar\" style=\"width:0%;\"></div>\n							</div>\n							<!-- The extended global progress information -->\n							<div id=\"progresstext\" class=\"progress-extended\">&nbsp;</div>\n						</div>\n\n					</div>\n				</form>\n			</td>\n			<td align=\"right\"><span class=\"dropzone-button showing\">&nbsp;</span>&nbsp;&nbsp;</td>\n		</tr>\n		</table>\n		<div class=\"imagelist\" style=\"max-height: 400px; min-height: 300px; overflow-y:scroll;\">\n			<table id=\"doclist\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n			<thead>\n				<tr class=\"font14\">\n					<td colspan=\"2\">Image</td>\n					<td>Image Sub Type</td>\n					<td>Caption</td>\n					<td>&nbsp;</td>\n				</tr>\n			</thead>\n			<!-- add image to here on upload -->\n			<tbody class=\"dropzone content\" style=\"display: table-row-group\">\n			<tr>\n				<td colspan=\"5\" class=\"dropzone-area\">&nbsp;</td>\n			</tr>\n			</tbody>\n			<tbody class=\"content\" id=\"file_list\">\n				\n\n			</tbody>\n			</table>\n		</div>\n	</div>\n	<table style=\"width: 100%; background: #DDDFE8; padding: 13px; font-size: 20px; border-top: 1px solid #99A0C1; text-align: center;\">\n	<tr>\n		<td><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/pixel.gif\" width=\"10\" height=\"1\" alt=\"\" border=\"0\"/></td>\n		<td>\n            <a href=\"#\" class=\"cancel-upload\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/cancel.png\"  style=\"cursor:pointer\" border=\"0\"></a>  \n            <a href=\"#\" class=\"save-uploads\"><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/buttons/form/save.png\"  style=\"cursor:pointer\" border=\"0\"></a>  \n		</td>\n		<td><img src=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/images/pixel.gif\" width=\"10\" height=\"1\" alt=\"\" border=\"0\"/></td>\n	</tr>\n	</table>\n</div>";
  return buffer;});
});

;require.register("views/templates/upload-form", function(exports, require, module) {
module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  helpers = helpers || Handlebars.helpers;
  var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


  buffer += "\n		<form id=\"fileupload\" action=\"";
  foundHelper = helpers.ctxPath;
  stack1 = foundHelper || depth0.ctxPath;
  if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
  else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "ctxPath", { hash: {} }); }
  buffer += escapeExpression(stack1) + "/api/documents/\" method=\"POST\" enctype=\"multipart/form-data\" onsubmit=\"return false\">\n		        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->\n		        <div class=\"row fileupload-buttonbar\">\n		            <div class=\"span7\">\n		                <!-- The fileinput-button span is used to style the file input field as button -->\n		                <span class=\"btn btn-success fileinput-button\">\n		                    <i class=\"icon-plus icon-white\"></i>\n		                    <span>Add files...</span>\n		                    <input type=\"file\" name=\"files[]\" multiple>\n		                </span>\n		                <button type=\"submit\" class=\"btn btn-primary start\">\n		                    <i class=\"icon-upload icon-white\"></i>\n		                    <span>Start upload</span>\n		                </button>\n		                <button type=\"reset\" class=\"btn btn-warning cancel\">\n		                    <i class=\"icon-ban-circle icon-white\"></i>\n		                    <span>Cancel upload</span>\n		                </button>\n		                <button type=\"button\" class=\"btn btn-danger delete\">\n		                    <i class=\"icon-trash icon-white\"></i>\n		                    <span>Delete</span>\n		                </button>\n		                <input type=\"checkbox\" class=\"toggle\">\n		            </div>\n		            <!-- The global progress information -->\n		            <div class=\"span5 fileupload-progress fade\">\n		                <!-- The global progress bar -->\n		                <div class=\"progress progress-success progress-striped active\" role=\"progressbar\" aria-valuemin=\"0\" aria-valuemax=\"100\">\n		                    <div class=\"bar\" style=\"width:0%;\"></div>\n		                </div>\n		                <!-- The extended global progress information -->\n		                <div class=\"progress-extended\">&nbsp;</div>\n		            </div>\n		        </div>\n		    </form>\n		";
  return buffer;});
});

;require.register("views/view", function(exports, require, module) {
(function() {
  var View,
    __hasProp = Object.prototype.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

  module.exports = View = (function(_super) {

    __extends(View, _super);

    function View() {
      View.__super__.constructor.apply(this, arguments);
    }

    View.prototype.template = function(params) {
      var url;
      url = 'undef-template : ';
      _.each(params, function(v, k) {
        url += url.indexOf('?') !== -1 ? '&' : '?';
        return url += k + '=' + encodeURIComponent(v);
      });
      return url;
    };

    View.prototype.getCtx = function() {
      return window.CTX_PATH;
    };

    View.prototype.render = function() {
      var _this;
      _this = this;
      return this.$el.html(this.template({
        ctxPath: this.getCtx()
      }));
    };

    View.prototype.debug = function() {
      var str, sub;
      try {
        str = arguments[0];
        sub = _.tail(arguments);
        _.each(sub, function(s) {
          return str = str.replace(/{}/, s);
        });
        if (window.console) return console.log(str);
      } catch (e) {
        if (window.console) return console.log(e);
      }
    };

    return View;

  })(Backbone.View);

}).call(this);

});

;
//# sourceMappingURL=runway-api.js.map