<%@ page contentType="text/javascript" %>
<%
    String contextPath = request.getContextPath();
%>
var over = "";
 
function setupCity(cityInput, stateInput, postcodeInput) {
	var city = document.getElementById(cityInput);
	var postcode = document.getElementById(postcodeInput);
	
	if (typeof(city) != "undefined" && city != null) {
		var cityDiv = document.createElement("div");
		cityDiv.style.backgroundColor = "#FFF";
		cityDiv.style.borderColor = "#000";
		cityDiv.style.borderStyle = "solid";
		cityDiv.style.borderWidth = "1px";
		cityDiv.style.padding = "5px";
		cityDiv.style.display = "none";
		cityDiv.style.position = "absolute";
		
		cityDiv.id = "citylookup-" + cityInput;
		if (cityDiv.addEventListener) {
			cityDiv.addEventListener("mouseover","setCityOver(this.id)");
			cityDiv.addEventListener("mouseout","setCityOver('')");
		} else {
			cityDiv.attachEvent("mouseover","setCityOver(this.id)");
			cityDiv.attachEvent("mouseout","setCityOver('')");
		}		
		city.parentNode.appendChild(cityDiv);
	}

	if (typeof(postcode) != "undefined" && postcode != null) {
		var postcodeDiv = document.createElement("div");
		postcodeDiv.style.backgroundColor = "#FFF;";
		postcodeDiv.style.border = "border: #000 solid 1px;";
		postcodeDiv.style.padding = "5px;";
		postcodeDiv.style.display = "none;";
		postcodeDiv.style.position = "absolute;";
		
		postcodeDiv.id = "postcodelookup-" + postcodeInput;
		postcodeDiv.onmouseover = "setpostcodeOver(this.id);";
		postcodeDiv.onmouseout = "setpostcodeOver('');";
		
		postcode.parentNode.appendChild(postcodeDiv);
	}
}
function blurCity(input) {
	var id = input.id;
	if (over != "citylookup-" + id) {
		var div = document.getElementById("citylookup-" + id);
		div.innerHTML = "";
		div.style.display = "none";
		foundDiv = false;
	}
}

function setCityOver(o) {
	over = o;
}

var found = 0;
var foundMax = 0;
var foundDiv = false;
function findCity(key, input) {
	var id = input.id;
	var value = input.value;

	var keyunicode = key.charCode || key.keyCode

	if (keyunicode == 40 && foundDiv) {
		if (found < foundMax) {
			if (found > 0) {
				var f = document.getElementById("found" + found);
				if (typeof(f) != 'undefined' && f != null) {
					f.style.backgroundColor = "#FFFFFF";
				}
			}
			++found;
			var f = document.getElementById("found" + found);
			if (typeof(f) != 'undefined' && f != null) {
				f.style.backgroundColor = "#E8E9EE";
			}
		}
	} else if (keyunicode == 38 && foundDiv) {
		if (found > 0) {
			var f = document.getElementById("found" + found);
			if (typeof(f) != 'undefined' && f != null) {
				f.style.backgroundColor = "#FFFFFF";
			}
			--found;
			if (found > 0) {
				var f = document.getElementById("found" + found);
				if (typeof(f) != 'undefined' && f != null) {
					f.style.backgroundColor = "#E8E9EE";
				}
			}
		}
	} else if (keyunicode == 13 && foundDiv) {
		if (found > 0) {
			var f = document.getElementById("foundlink" + found);
			if (typeof(f) != 'undefined' && f != null) {
				eval(f.href);
			}
		}
	} else if (value.length > 1) {
		var args = "?-action=search&-mode=setcity&-id=" + id + "&-value=" + encodeURIComponent(value);
		$.post("<%= contextPath %>/actions/contactaddressaction.jsp" + args,{},function(data){
			found = 0;
			data = data.replace(/^\s*|\s*$/g,'');
			if (data.length > 0) {
				var l = data.split("\n");
				var tag = l[0];
				if (tag != null) tag = tag.replace(/^\s*|\s*$/g,'');
				var div = document.getElementById("citylookup-" + tag);
				var cIn = document.getElementById(tag);
				if (div != null && cIn != null) {
					if (cIn.style.top != "") div.style.top = cIn.style.top + "px";
					if (cIn.style.left != "") div.style.left = cIn.style.left + "px";
					div.innerHTML = "";
					foundDiv = true;
					var lines = data.split("\n");
					if (lines.length > 1) {
						for (var l = 1; l < lines.length; ++l) {
							var parts = lines[l].split("|");
							div.style.display = "block";
							if(parts[4] == "New Zealand"){
								div.innerHTML = div.innerHTML + "<div id=\"found" + l +"\" style=\"cursor: pointer; padding: 2px 5px;\"><a href=\"javascript:setData('" + tag + "','" + parts[0] + "','" + parts[1] + "','" + parts[2] + "','" + parts[3] +"','" + parts[4] +"');\" id=\"foundlink" + l +"\" >" + parts[0] + " " + parts[3] + " " + parts[2] + "</a></div>";
							}else{
								div.innerHTML = div.innerHTML + "<div id=\"found" + l +"\" style=\"cursor: pointer; padding: 2px 5px;\"><a href=\"javascript:setData('" + tag + "','" + parts[0] + "','" + parts[1] + "','" + parts[2] + "','" + parts[3] +"','" + parts[4] +"');\" id=\"foundlink" + l +"\" >" + parts[0] + " " + parts[1] + " " + parts[2] + "</a></div>";
							}
							foundMax = l;
						}
					} else { 
						div.innerHTML = "";
						div.style.display = "none";
						foundDiv = false;
					}
				}
			}				
		});
	} else {
		var div = document.getElementById("citylookup-" + id);
		div.innerHTML = "";
		div.style.display = "none";
		foundDiv = false;
	}
}

function setData(id, city, postcode, state, district, country) {
	var div = document.getElementById("citylookup-" + id);
	if (typeof(div) != "undefined" && div != null) { 
		div.innerHTML = "";
		div.style.display = "none";
	
		if (id.lastIndexOf("-") > 0) {
			id = id.substr(id.lastIndexOf("-"));
			var cityTag = document.getElementById("input-city" + id);
			if (typeof(cityTag) != "undefined" && cityTag != null) cityTag.value = decodeURIComponent(city);
			var postcodeTag = document.getElementById("input-postcode" + id);
			if (typeof(postcodeTag) != "undefined" && postcodeTag != null) postcodeTag.value = decodeURIComponent(postcode);
			var stateTag = document.getElementById("input-state" + id);
			if (typeof(stateTag) != "undefined" && stateTag != null) {
				for (var o = 0; o < stateTag.options.length; ++o) {
					if (stateTag.options[o].value == decodeURIComponent(state)) {
						stateTag.selectedIndex = o;
						break;
					}
				}
			}
			if(country == "New Zealand"){
				var street2Tag = document.getElementById("input-street2" + id);
				if (typeof(street2Tag) != "undefined" && street2Tag != null) street2Tag.value = decodeURIComponent(district);
				$("#input-country" + id).val("New Zealand");
			}else{
				$("#input-country" + id).val("Australia");
			}
		}
	} 
	
}

