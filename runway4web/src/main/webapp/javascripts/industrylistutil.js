function IndustryTypeObj(name, type)
{
	this.name = name;
	this.type = type;
}

function selectIndustry()
{
	var index = thiscompanyform.Industry.selectedIndex;
	var industryname = thiscompanyform.Industry.options[index].value;
	if(industryname.length!=0)
	{
		clearIndustryType();
		insertIndustryType(industryname);
	}
	else
	{
		clearIndustryType();
	}
}

function insertIndustryType(industryname)
{
	var added = 1;
	thiscompanyform.IndustryType.options[0] = new Option("             ","");
	for(var i=0; i<industrylist.length; i++)
	{
		if(industrylist[i].name == industryname)
		{
			thiscompanyform.IndustryType.options[added] = new Option(industrylist[i].type,industrylist[i].type);
			if(industryname == currentindustryname && industrylist[i].type == currentindustrytype)
			{
				thiscompanyform.IndustryType.options[added].selected = true;
			}
			added++;
		}
	}	
}

function clearIndustryType()
{
	var typelength = thiscompanyform.IndustryType.length;
	for(var j=typelength-1; j>=0; j--)
	{
		thiscompanyform.IndustryType.options[j] = null;
	}
	thiscompanyform.IndustryType.options[0] = new Option("             ","");
}