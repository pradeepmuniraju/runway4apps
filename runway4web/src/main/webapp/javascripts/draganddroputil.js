function getPlaceHolder(placeHolderID, placeHolderClass,styleattr) {
	if(styleattr!=null){
		return '<div id="' + placeHolderID + '" class="' + placeHolderClass + '"><table class="details" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #FEFFCE; height:37px; border-bottom-style: solid; border-bottom-width: 1px; border-color: #DCDDE1;"><tr title="Please wait, adding item."><td style='+styleattr+'>Inserting item...</td></tr></table></div>';
	}else{
		return '<div id="' + placeHolderID + '" class="' + placeHolderClass + '"><table class="details" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #FEFFCE; height:37px; border-bottom-style: solid; border-bottom-width: 1px; border-color: #DCDDE1;"><tr title="Please wait, adding item."><td style="width: 16px; height: 16px; padding-left: 15px; padding-right: 15px; text-align: center;">Inserting item...</td></tr></table></div>';
	}
}

//var placeHolderIndex = 0;

getNextPlaceHolderID.placeHolderIndex = 0;

function getNextPlaceHolderID() {
	var placeHolderID = "placeHolder_" + getNextPlaceHolderID.placeHolderIndex;
	getNextPlaceHolderID.placeHolderIndex++;
	
	return placeHolderID;
}
