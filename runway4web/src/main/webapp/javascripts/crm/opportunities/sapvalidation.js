$(function() {
	var $sapForm = $('form#sendToSAPform', window.parent.document), oppID = $sapForm.length ? $sapForm.find('input[name=OpportunityID]').val() || '' : '' /* future other logic */,
		productType = $sapForm.length ? $sapForm.find('input[name="-ProductType"]').val() || '' : ''; 
	window.console = window.console || {
		log: function() {}
	}
	if( $sapForm.length && oppID.length ) { 
		var sapProfile = null, campaignList = [], companyList = null;

		/* these were previously dynamic, now static 10/6 */
		campaignList.push({ Name: "Red Hot No Worries" }); 
		campaignList.push({ Name: "Payment on completion" }); 
		campaignList.push({ Name: "Low deposit" }); 
		campaignList.push({ Name: "Double the Grant" }); 

		/*$.getJSON(window.CTX_PATH + '/crm/cms/templates/actions/load.jsp?-load=campaign&Type=Product&Status=Active', function(data) { 
			campaignList = data ? data.campaigns || [] : []; 
		}), */
		$.when( $.getJSON(window.CTX_PATH + '/api/companies/?Type=Developer&-quickSearch=true', function(data) {
			companyList = data || []; 
		}), $.getJSON(window.CTX_PATH + '/api/processes/' + oppID + '/profiles', function(data) {
			var ps = $.grep(data, function(p) { return p.SurveyID == "0O1S3H7Z045C367P13542G2I377G"; }); 
			if(ps.length) sapProfile = ps[0]; 
		})).then(function() {
			var getQuestion = (function(profile) { 
				if(!profile) return null;
				return function(id) {
					var q = $.grep(profile.Questions, function(qn) { return qn.QuestionID == id; });
					if(q.length) return q[0]; 
					throw Error("could not find question for id " + id);
				}
			})(sapProfile); 
			if(!sapProfile) {
				alert('Sorry, the SAP Profile must be configured on this runway to use this function'); 
				return;
			}			
			// "08113D7N0G5T327W4G7Z0K8W0F6F"	"Product Type"
			// "0N1N3Q740D5G3C7V2V4W7K2X0W48"	"Buyer Type"
			// "0Q1C3B7T0D5Q3O7R533O5E6C1E9P"	Developer Promotion
			// "0S1L3F770H5J3L746Z6U4B5F3T3D"	Developer
			// "0U1Y3R7Y0A5K3G7G2Q1J6T7T382U"	Buyer Segment
			// "0X1V3H7S085N3K7O672M58234H5X"	Estate
			// "0Z1S3Y7J0K51377L39355E974E6M"	Job Type
			// "0V1M3G9O74485O937Y2G3H5H8H0A"	Incentive Given at Sale
			// "0K1Q3D9G7A4Y5M906I9Y7F2Y7U0S"	Potential Incentive
			// "0P1Z3F9J7F4G5G9S7E7I4F3D3L67"	Incentive Given Detail
			

			window.sapWizard = [
				{
				    "id": "001",
				    "name": "Buyer Type",
				    "qid": "0N1N3Q740D5G3C7V2V4W7K2X0W48",
				    "options": [
				        {
				            "id": "001_001",
				            "value": "Owner Occupier"
				        },
				        {
				            "id": "001_002",
				            "value": "Investor"
				        }
				    ]
				},
				{
				    "id": "002",
				    "name": "Buyer Segment",
				    "qid": "0U1Y3R7Y0A5K3G7G2Q1J6T7T382U",
				    "options": [
				        {
				            "id": "002_001",
				            "value": "FHB",
				            "requires": "001_001"
				        },
				        {
				            "id": "002_002",
				            "value": "Downsizer",
				            "requires": "001_001"
				        },
				        {
				            "id": "002_003",
				            "value": "Upgrader",
				            "requires": "001_001"
				        },
				        {
				            "id": "002_004",
				            "value": "SMSF",
				            "requires": "001_002",
				            "chooses": [
				                "003_002",
				                "004_004"
				            ]
				        },
				        {
				            "id": "002_005",
				            "value": "Traditional",
				            "requires": "001_002"
				        }
				    ]
				},
				{
				    "id": "003",
				    "name": "Job Type",
				    "qid": "0Z1S3Y7J0K51377L39355E974E6M",
				    "options": [
				        {
				            "id": "003_001",
				            "value": "Order",
				            "requires": [
				                "002_001",
				                "002_002",
				                "002_003",
				                "002_005"
				            ]
				        },
				        {
				            "id": "003_002",
				            "value": "H&L"
				        },
				        {
				            "id": "003_003",
				            "value": "Display",
				            "requires": [
				                "002_001",
				                "002_002",
				                "002_003"
				            ]
				        },
				        {
				            "id": "003_004",
				            "value": "Spec Home",
				            "requires": [
				                "002_001",
				                "002_002",
				                "002_003"
				            ]
				        }
				    ]
				},
				{
				    "id": "004",
				    "name": "Product Type",
				    "qid": "08113D7N0G5T327W4G7Z0K8W0F6F",
				    "options": (function(defs) { 
				    	var ix = 5; 
				    	$.each(campaignList, function(i, campaign) {
				    		defs.push({
				    			"id": "004_00" + (ix++),
					            "value": campaign.Name,
					            "requires": ["003_002", "001_001"]
				    		})
				    	})
				    	return defs; 
				    })([
				        {
				            "id": "004_001",
				            "value": "Traditional",
				            "requires": [
				                "003_001",
				                "003_002"
				            ]
				        },
				        {
				            "id": "004_002",
				            "value": "KDR",
				            "requires": [
				                "003_001"
				            ]
				        },
				        {
				            "id": "004_003",
				            "value": "Dual Occ",
				            "requires": [
				                "003_001"
				            ]
				        }
				        ,
				        {
				            "id": "004_004",
				            "value": "Payment on completion",
				            "requires": "001_002"
				        }
				    ])
				},
				{
				    "id": "005",
				    "name": "Developer Promotion",
				    "qid": "0Q1C3B7T0D5Q3O7R533O5E6C1E9P",
				    //"requires": ["004_001", "004_002","004_003","004_004"],
				    "options": (function() {
				    	var ix = 1;
				    	return $.merge([{
				    			"id": "005_00" + (ix++),
					            "value": '-No Developer Promotion-' 
					            //"requires": ["004_001", "004_002","004_003","004_004"]
				    	}], $.map( getQuestion("0Q1C3B7T0D5Q3O7R533O5E6C1E9P").List.ListItems, function(opt, i) {
				    		return {
				    			"id": "005_00" + (ix++),
					            "value": opt.Label 
					            //"requires": ["004_001", "004_002","004_003","004_004"]
				    		};
				    	})); 
				    	return opts; 
				    })()
				},
				{
				    "id": "006",
				    "name": "Estate",
				    "qid": "0X1V3H7S085N3K7O672M58234H5X",
				    "requires": [
				        "003_001",
				        "003_002",
				        "003_003"
				    ],
				    "options": (function() {
				    	var ix = 1;
				    	return $.merge([/*{
				    			"id": "005_00" + (ix++),
					            "value": '-No Estate-', 
					            "requires": "004_001"
				    	}*/], $.map( getQuestion("0X1V3H7S085N3K7O672M58234H5X").List.ListItems, function(opt, i) {
				    		return {
				    			"id": "006_00" + (ix++),
					            "value": opt.Label
				    		};
				    	})); 
				    	return opts; 
				    })()
				},
				{
				    "id": "007",
				    "name": "Developer",
				    "qid": "0S1L3F770H5J3L746Z6U4B5F3T3D",
				    "requires": [
							        "003_008",
							        "003_002",
							        "003_003"
							   ],
				    "options": $.map(companyList, function(company, ix) {
					    	return {
					    		"id": "007_00" + (ix+1), 
					    		"value": company.Company
					    	}
				    })
				},
				{
				    "id": "008",
				    "name": "Incentive Given at Sale",
				    "qid": "0V1M3G9O74485O937Y2G3H5H8H0A",
				    "options" : "free_text"
				},
				{
				    "id": "009",
				    "name": "Potential Incentive",
				    "qid": "0K1Q3D9G7A4Y5M906I9Y7F2Y7U0S",
				    "options" : "free_text"
				},
				{
				    "id": "010",
				    "name": "Incentive Given Detail",
				    "qid": "0P1Z3F9J7F4G5G9S7E7I4F3D3L67",
				    "options" : "free_text"
				}
			], valid = true; 
			$.each(sapWizard, function(ix, qn) {
				var q = getQuestion(qn.qid); 
				if(!q) {
					alert('We could not find a configuration for ' + qn.name);
					return (valid = false); 
				}
				qn.q = q; 
				if(q.Answer.AnswerText.length == 0 && qn.id == "003" && productType == 'House and Land') {
					q.Answer.Answer = q.Answer.AnswerText = 'H&L'; 
				}
				// clone the options to all options, so we can limit options later. 
				if(qn.options != 'free_text') {
					qn.alloptions = $.extend(true, [], qn.options); 
				}
			});
			if(valid) {
				// set up questions 
				var tBody = $('#question_list').html(''), 
					selectTemplate = Handlebars.compile($('#select-template').text()), 
					textTemplate = Handlebars.compile($('#text-template').text()), 
					checkQuestions = function() {
						var validAnswer = true, choices = []; 
						$.each(sapWizard, function(ix, qn) {
							//console.log('processing ' + qn.id); 
							if(!validAnswer) {
								/* changing a question should remove all questions after it */
								$('tr#row' + qn.id).remove(); 
								alert('we shouldn\'t get here anymore'); 
							} else {
								var currAnswer = qn.q.Answer.Answer, htmlAnswer = $('tr#row' + qn.id + ' #Answer' + qn.qid).val(), 
									isAnswered = (htmlAnswer !== undefined && htmlAnswer.length != 0); 

								//console.log('q= ' + qn.name + ' has answer ' + currAnswer);
								//console.log(qn.q.Answer); 

								if(htmlAnswer === undefined) {
									var foundValid = false, $el = null, singleChoice = null;
									// we need to add it in 
									if(choices.length) {
										$.each(choices, function(cx, cv) {
											var c = cv.split('_')[0]; 
											//console.log('compare ' + qn.id + ' ' + c); 
											if(qn.id == c) {
												singleChoice = cv; 
											}
										});
									}
									if(qn.options == 'free_text') {
										qn.value = qn.q.Answer.Answer; 
										$el = $(textTemplate(qn)); 
									} else {
										qn.options = []; 
										$.each(qn.alloptions, function(ix, opt) {
											var add = null; 
											if(opt.requires) {
												if($.isArray(opt.requires)) {
													$.each(opt.requires, function(orix, orv) {
														var rs = orv.split('_'),
															sel = $('tr#row'+rs[0]+' select').get(0), 
															sId = $(sel.options[sel.selectedIndex]).data('id'); 

														if(orv == sId) {
															if(!singleChoice || singleChoice == opt.id) add = $.extend(true, {}, opt); 
															return false; 
														}
													});
												} else {
													var rs = opt.requires.split('_'),
														sel = $('tr#row'+rs[0]+' select').get(0), 
														sId = sel ? $(sel.options[sel.selectedIndex]).data('id') : ''; 
													if(opt.requires === sId) {
														if(!singleChoice || singleChoice == opt.id) add = $.extend(true, {}, opt); 
													}
												}
											} else {
												if(!singleChoice || singleChoice == opt.id) add = $.extend(true, {}, opt); 
											}
											if(add) 
											{
												if(add.name == qn.q.Answer.Answer) {
													add.selected = 'selected="selected"'; 
													// this will work if it's pre-selected, not if the user selects it 
													if(add.chooses) {
														$.each(add.chooses, function(acx, ac) {
															choices.push(ac);
														});
													}
												} 	
												qn.options.push(add); 
											}
										});
										$el = $(selectTemplate(qn)); 
									}
									if(qn.options.length != 0) { // if no options, skip this. 
										var $field = $el.find('select, input[type=text]'), opts = $field.find('option'), 
											changeAction = function() {
											//console.log('setting value to ' + $(this).val()); 
											qn.q.Answer.Answer = qn.q.Answer.AnswerText = $(this).val(); 
											var t = $el.get(0), found = false, rem = []; 
											$.each($el.parent().children(), function(ix, ixc) {
												if(found) {
													rem.push(ixc); 
												} else if(ixc == t) {
													found = true; 
												}
											});
											$.each(rem, function(rx, r) {
												$(r).remove();
											});
											checkQuestions(); 
										};
										//console.log($field);
										tBody.append($el); 
										if(opts.length==2) {
											//console.log('setting value ' + $(opts[1]).attr('value'));
											$field.val($(opts[1]).attr('value')); 
										} else if(currAnswer.length) {
											$field.val(currAnswer); 
										}
										$field.change(changeAction); 
										if($field.get(0).nodeName == 'INPUT') {
											$field.keyup(changeAction); 
										}
										//console.log('field val is ' + $field.val())
										if($field.val().length == 0) { // this one isn't answered, so dont load the next. 
											validAnswer = false; 
										}
										return validAnswer;
									} else {
										var opts = $field.find('option');
									}
								} else { 
									// the answer is in the page, does this answer have choices? 
									if($.isArray(qn.options)) {
										//console.log('qn.options was array for ' + qn.id); 
										$.each(qn.options, function(aix, aiv) {
											//console.log('comparing ' + aiv.value + ' and ' + qn.q.Answer.Answer); 
											if(aiv.value == qn.q.Answer.Answer) {
												//console.log('match'); 
												if(aiv.chooses) {
													//console.log('this opt has choices'); 
													//console.log(aiv.chooses)
													$.each(aiv.chooses, function(acx, ac) {
															choices.push(ac);
													});
												}
												return false;
											}
										})
									}
								}
							}
							// if we have a valid answer, keep going in the list
							if(!validAnswer) {
								//console.log('short circuit on '); 
								//console.log(qn);
								// remove all following questions in this case. 
								return false;
							}
						}); 
						window.doSubmit = validAnswer ? function() {
							alert('Please wait, we are submitting your responses'); 
							$.ajax({
								type: 'PUT',
								url: window.CTX_PATH + '/api/processes/' + oppID + '/profiles/' + sapProfile.SurveyID, 
								data: JSON.stringify(sapProfile), 
								contentType: 'application/json',
								accept:'application/json', 
								success: function() {
									window.parent.sendToSap(); 
								}, 
								error: function() {
									alert('Sorry, an internal error occurred'); 
									console.log(arguments); 
								}
							});
						}:function() {
							alert('Please complete the form before continuing'); 
						}; 
					};
					checkQuestions(); 
			}
			console.log('sap done'); 
		}); 
	} else {
		alert('I couldn\'t find the SAP form, so you shouldn\'t be here. Please note the steps taken and contact support'); 
	}
})