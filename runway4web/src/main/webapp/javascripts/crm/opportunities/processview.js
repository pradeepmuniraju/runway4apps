$(function() {
	var $sapForm = $('#sendToSAPform'), oppID = $sapForm.length ? $sapForm.find('input[name=OpportunityID]').val() || '' : '' /* future other logic */; 

	/* submit the response for a checklist answer */
	$('form.checklist-form input.checklist-answer').click(function(evt) {
		var form = $(this.form);
		$.get(form.attr('action'), form.serializeArray(), function() {
			if(window.console) console.log('Checklist answer saved...');
		}).error(function() {
			alert('There was an error updating the checklist status');
		});
	});
	// only do this if the form is in the dom 
	if( $sapForm.length && oppID.length ) { 
		var sapProfile = null, campaignList = null; 
		
		window.sendToSapActual = window.popLightBox; 
		window.sendToSap = function() {
			// fallback default implementation 
			closeWindow('sap_profiles');
			window.sendToSapActual(); 
		}		
		window.popLightBox = function() {
			var args = arguments, _this = this; 
			window.sendToSap = function() {
				closeWindow('sap_profiles');
				// call it with the same arguments, because you never know what may get added. 
				window.sendToSapActual.apply(_this, args); 
			}		
			openInLightBox(window.CTX_PATH + '/crm/popup/processes/sapvalidationbox.jsp?OpportunityID=' + oppID, 'sap_profiles', 500, 500, false, false, false); 
			return false; 
		}
	}
});