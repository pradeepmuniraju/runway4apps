/**
 * 
 * Filename: groupstatussearch.js 
 * Object Name: GroupStatusSearch 
 * Author: Ulpian Cesana
 * 
 * Dependencies: javascripts/lib/jquery-1.3.2.js javascripts/formutils.js
 * 
 */
function addSelectOption(selectObj, text, value, isSelected){
	  if(selectObj != null && selectObj.options != null){
	    var newOpt = new Option(text,value);
	    newOpt.selected = isSelected;
	    selectObj.add(newOpt);
	  }
	}

function GroupStatusSearch(contextPath, groupIDRelation, 
        repUserIDRelation, statusIDRelation, statusHistoryIDRelation, currentUserID) {
    this.contextPath = contextPath;
    this.groupIDRelation = groupIDRelation;
    this.repUserIDRelation = repUserIDRelation;
    this.statusIDRelation = statusIDRelation;
    this.statusHistoryIDRelation = statusHistoryIDRelation;
    this.type = "";
    
    this.selectGroup = function() {
        this.loadSalesRepsByGroup();
        this.loadStatusesByGroup();
    }

    this.selectGroupForCompanies = function() {
    	this.type = "&ForCompanies=Y"
        this.loadSalesRepsByGroup();
        this.loadStatusesByGroup();
        this.type = "";
    }
    
    this.selectGroupForContacts = function() {
    	this.type = "&ForContacts=Y"
        this.loadSalesRepsByGroup();
        this.loadStatusesByGroup();
        this.type = "";
    }
    
    this.loadSalesRepsByGroup = function() {
        this.getSalesRepDropdown().empty();

        var groupID = this.getGroupID();
        this.loadSalesReps(groupID);
    }
    
    this.getSalesRepDropdown = function() {
        return $("select#" + this.getRepUserIDRelation());
    }
    
    this.loadSalesReps = function (groupID) {
        var url = this.getRepUserOptionsUrl(groupID);
        var reps = this.selectedRep ? this.selectedRep.split(/\+/) : [];

        var salesRepDropdownPanelID = this.getRepUserIDRelation();
        
        //loadPanel(salesRepDropdownPanelID, url);
        // Load panel uses inner html which doesnot work in ie. Hence changed to use ajax and json.
        $.ajax({ 
			url : url,
			async: false,
			dataType: 'json',
			success: function(data) {				
				var selectObj = document.getElementById(salesRepDropdownPanelID);
				selectObj.options.length = 0;
				$.each(data.RepOptions, function(i, item) {
					var option = item.split(":");					
					//addSelectOption(selectObj, option[1], option[0], currentUserID == option[0]);
					addSelectOption(selectObj, option[1], option[0], $.grep(reps,function(v) { return v == option[0]; }).length);
				});
					
			}        
		});
    }

    this.loadStatusesByGroup = function() {
        this.getStatusDropdown().empty();

        var groupID = this.getGroupID();
        this.loadStatuses(groupID);
    }

    this.loadStatuses = function(groupID) {
        var url = this.getStatusOptionsUrl(groupID);

        /* Group Status */
        var statusDropdownPanelID = this.getStatusIDRelation();
        //console.log(statusDropdownPanelID)
        //loadPanel(statusDropdownPanelID, url);
        
        $.ajax({ 
			url : url,
			async: false,
			dataType: 'json',
			success: function(data) {				
				var selectObj = document.getElementById(statusDropdownPanelID);
				selectObj.options.length = 0;
				
				$.each(data.StatusOptions, function(i, item) {
					var option = item.split(":");
					addSelectOption(selectObj, option[1], option[0], false);
				});
					
			}        
		});

        /* Group Status History */
        var statusHistoryDropdownPanelID = this.getHistoryStatusIDRelation();
        //console.log(statusHistoryDropdownPanelID)
        //loadPanel(statusHistoryDropdownPanelID, url);
        
        $.ajax({ 
			url : url,
			async: false,
			dataType: 'json',
			success: function(data) {				
				var selectObj = document.getElementById(statusHistoryDropdownPanelID);
				selectObj.options.length = 0;
				
				$.each(data.StatusOptions, function(i, item) {
					var option = item.split(":");
					addSelectOption(selectObj, option[1], option[0], false);
				});
					
			}        
		});
    }
    

    this.getStatusDropdown = function() {
        return $("select#" + this.getStatusIDRelation());
    }

    this.getStatusOptionsUrl = function(groupID) {
        var url = this.getContextPath()
                + "/crm/includes/groups/linkedstatusoptions.jsp";
        var params = "?emptyoption=true&-isJSON=true";
        params += "&GroupID=" + groupID + this.type;
        
        return url + params;
    }
    
    this.getRepUserOptionsUrl = function(groupID) {
        var url = this.getContextPath()
            + "/crm/popup/statusalerts/includes/repfilteroptions.jsp";
        var params = "?-groupID=" + groupID;
        params += "&-showCurrentUser=false";
        params += "&-showOnlySalesReps=true";
        params += "&-search=true";
        params += "&-isJSON=true";
        return url + params;
    }

    this.getContextPath = function() {
        return this.contextPath;
    }

    this.getGroupID = function() {
    	// this code only returns the 1st select value, it doesn't work with multi selected values
        //return FormUtils.getSelectedValue(this.getGroupIDRelation());
    	
    	var groupIDs = "";
    	var sel = document.getElementById(this.getGroupIDRelation());
    	if (typeof sel != 'undefined' && sel != null) {
    		for (var g = 0; g < sel.options.length; ++g) {
    			if (sel.options[g].selected) {
    				if (groupIDs.length > 0) groupIDs += "%2B";
    				groupIDs += sel.options[g].value; 
    			}
    		}
    	}
    	return groupIDs;
    		
    }

    this.getGroupIDRelation = function() {
        return this.groupIDRelation;
    }
    
    this.getRepUserIDRelation = function() {
        return this.repUserIDRelation;
    }

    this.getStatusIDRelation = function() {
        return this.statusIDRelation;
    }

    this.getHistoryStatusIDRelation = function() {
        return this.statusHistoryIDRelation;
    }
}