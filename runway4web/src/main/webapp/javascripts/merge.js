	var mergeIds = new Array();
	var mergeCount = 0;
	initCount();

	function initCount()
	{
		mergeIds = new Array();
		mergeCount = 0;
		//alert("Inside initCount");
		var numforms = this.document.forms.length;
		//alert("Have "+numforms+" forms to check");
		var formIndex;
		var formName;
		var mergeElement;
		for (formIndex = 0; formIndex < numforms; formIndex++) {
			//alert("Looking at form #"+formIndex);
			formName = this.document.forms[formIndex].name;
			//alert("Form name = "+formName);
			if ((formName.indexOf("merge") == 0) && (formName.indexOf(mergeRecordTypePlural) == -1)) {
				//alert("This is a merge form");
				mergeElement = this.document.forms[formIndex].merge;
				//alert("Merge element in form: "+mergeElement.name+" (type = "+mergeElement.type+")");
				if (mergeElement.checked) {
					//alert("Control '"+mergeElement.name+"' is checked");
					toggleForMerge(mergeElement.value);
				}
			}
		}
		//alert("Finished initCount: mergeCount set to "+mergeCount);
	}

	function toggleForMerge(recordid)
	{
		var idIndex = -1;
		var ix;
		for (ix = 0; ix < mergeCount; ix++) {
			if (mergeIds[ix] == recordid) {
				idIndex = ix;
				break;
			}
		}
		if (idIndex == -1) {
			if (mergeCount == 3) {
				var formname = "merge"+recordid;
				var mergeform = this.document.forms[formname];
				mergeform.merge.checked = false;
				alert("You may only merge up to 3 "+mergeRecordTypePlural+" at once. Please unselect another "+mergeRecordType+" first.");
			} else {
				mergeIds[mergeCount] = recordid;
				mergeCount++;
			}
		} else {
			mergeCount--;
			mergeIds[idIndex] = mergeIds[mergeCount];
			mergeIds[mergeCount] = 0;
		}
	}

	function doMerges(urlprefix)
	{
		if (mergeCount < 2) {
			alert("You need to select 2-3 "+mergeRecordTypePlural+" to be merged.");
		} else if (mergeCount > 3) {
			alert("You may only merge up to 3 "+mergeRecordTypePlural+" at once.");
		} else {
			var mergeForm = this.document.forms["merge"+mergeRecordTypePlural];
			mergeForm.elements[mergeRecordType+"IDs"].value = mergeIds;
			var url = ''; 
			if (typeof(urlprefix) != "undefined")
			{	
				url = urlprefix; 
			}
			url = url + "actions/merge"+mergeRecordTypePlural+".jsp?-action=select&"+mergeRecordType+"IDs="+mergeIds+"&camefrom="+mergeForm.camefrom.value;
			if (typeof(mergeForm.elements['-mergeaction']) != "undefined") 
			{
				if(mergeForm.elements['-mergeaction'].value.length>0) { 
					url = url + "&-mergeaction="+mergeForm.elements['-mergeaction'].value;
				} 
			}
			window.open(url);
			//mergeForm.submit();
		}
	}