<%@ page contentType="text/javascript" %>
<%
    String contextPath = request.getContextPath();
%>

var datepickerlibs = []; 
var x = new Array(
    "<%= contextPath %>/javascripts/lib/jquery/ui/ui.core.js",   
    "<%= contextPath %>/javascripts/lib/jquery/ui/ui.datepicker.js",
    "<%= contextPath %>/javascripts/lib/jquery/ui/ui.all.css"
);

var loadedDatePicker;

function loadDateLibs() { 
	if(typeof $ != 'undefined' && typeof $.fn != 'undefined' && typeof jQuery.ui != 'undefined') {
		if (typeof $.fn.datepicker == 'undefined') {
			if (typeof console != 'undefined' && typeof console.log != 'undefined') console.log("loading internal scripts");
			loadLibScripts(datepickerlibs);
		}
		$(document).ready(function() { 
			loadDatePicker();
		}); 
	} else { 
		setTimeout("loadDateLibs()",1000);
		if (typeof console != 'undefined' && typeof console.log != 'undefined') console.log("missing jQuery");
	}
}

function loadDatePicker() { 
	if(typeof $ != 'undefined' && typeof $.fn != 'undefined' && typeof $.fn.datepicker != 'undefined') { 
	loadedDatePicker == true;
	//search for all inputs with "Date" in the name or title and add a date picker
		$("input[name*='Date'][type=text],input[title*='Date'][type=text]").datepicker({
			showOn: 'button', buttonImage: '<%= contextPath %>/images/date_selection_padded_icon.gif',
			buttonImageOnly: true, buttonText: 'select date', dateFormat: 'dd/mm/yy',
			constrainInput: false,
			onClose: datePickerDone });
	} else {

		setTimeout("loadDatePicker()",1000);
		if (typeof console != 'undefined' && typeof console.log != 'undefined') console.log("missing date picker");
	}	
}
//if (typeof loadedDatePicker == 'undefined') {
	loadedDatePicker = false; 
	loadDateLibs();
//}

function datePickerDone(){
	if(typeof(datePickerClosed)!= "undefined"){
		datePickerClosed();
	}
}

function loadDatePickerForElement(htmlname) { 
	if(typeof $ != 'undefined' && typeof $.fn != 'undefined' && typeof $.fn.datepicker != 'undefined') { 
	loadedDatePicker == true;
	//search for all inputs with "Date" in the name or title and add a date picker
		$("input[name*='"+htmlname+"']").datepicker({
			showOn: 'button', buttonImage: '<%= contextPath %>/images/date_selection_padded_icon.gif',
			buttonImageOnly: true, buttonText: 'select date', dateFormat: 'dd/mm/yy',
			constrainInput: false,
			onClose: datePickerDone });
	} else {

		setTimeout("loadDatePicker()",1000);
		if (typeof console != 'undefined' && typeof console.log != 'undefined') console.log("missing date picker");
	}	
}
