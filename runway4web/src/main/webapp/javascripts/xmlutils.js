var jstables = new Array();

function getHeaderText(str, index, tname)
{
	return("<a href=\"#\" title=\"Sort\" onclick=\"return sortTable('"+ tname +"', "+ index +")\">"+ str +"</a>");
}

function tabledata(tablename, tableid, headerlist, datalist, tablestyle, headerrowstyle, rowstyle, headstyle, bodystyle)
{
	this.tablename = tablename;
	this.tableid = tableid;	
	this.headerlist = headerlist;
	this.datalist = datalist;
	this.sortcolumn = 0;
	this.tablestyle = tablestyle;
	this.headerrowstyle = headerrowstyle;
	this.rowstyle = rowstyle;
	this.headstyle = headstyle;
	this.bodystyle = bodystyle;		
}

function tablerow(rowcolumns, tableobj, rowindex)
{
	this.columns = rowcolumns;
	this.table = tableobj;
	this.index = rowindex;
	this.selected = false;
}

function columnmeta(hname, htype, hformat, sortdirection, ascfunction, descfunction, headerstyle, columnstyle, columnwidth, columnalign)
{
	this.name = hname;
	this.type = htype;
	this.format = hformat;
	this.sort = sortdirection;
	this.ascfunction = ascfunction;
	this.descfunction = descfunction;
	this.headerstyle = headerstyle;
	this.columnstyle = columnstyle;
	this.width = columnwidth;
	this.align = columnalign;		
}

function getRowIndex(tdata, index)
{
	for(var i=0; i< tdata.datalist.length; i++)
	{
		if(tdata.datalist[i].index == index)
		{
			return(i);	
		}	
	}
	return(-1);
}

function removeArrayItem(list, index)
{
	var endlength = list.length-1;
	for(var i=index; i<endlength-1; i++)
	{
		list[i] = list[i+1];
	}
	list.length = endlength;
}

function removeRow(tableid, listindex)
{
	var tdata = getTable(tableid);	
	var rowindex = getRowIndex(tdata, listindex);
	if(rowindex>=0)
	{
		removeArrayItem(tdata.datalist, rowindex);	
		var tbody = document.getElementById(tableid + "TBODY");
		tbody.deleteRow(rowindex+1);
	}
}


function loadTable()
{
	if (xmlreq.readyState == 4)
	{
    var oneRecord;

    // node tree
    //alert("Hello");
    var data = xmlreq.responseXML;

    for(var i=0; i<data.childNodes.length; i++)
    {
        if(data.childNodes[i].nodeType == 1)
        {
    		data = data.childNodes[i];
    		break;
    	}
    }

	var tdata = new tabledata();
	var index = getTableIndex(tbodystr);
	if(index = -1)
	{
		jstables[jstables.length] = tdata;
	}
	else
	{
		jstables[index] = tdata;
	}
	tdata.tableid = tbodystr;
	tdata.datalist = new Array();
	
	var header = data.getElementsByTagName("header")[0];
	tdata.tablename = header.getAttribute("tablename");
	loadTableHeader(tdata, header);

	var rows = data.getElementsByTagName("row");
    for (var i = 0; i < rows.length; i++)
    {
        // use only 1st level element nodes to skip 1st level text nodes in NN
        if (rows[i].nodeType == 1)
        { 
			loadTableRow(tdata, rows[i]);
        }
    }
            
    drawTable(tbodystr);
  }
}

function getColumnIndexForType(tdata, coltype)
{
	for(var i=0; i<tdata.headerlist.length; i++)
	{
		if(tdata.headerlist[i].type == coltype)
		{
			return(i);
		}
	}
	return(-1);
}

function getPrimaryKeyIndex(tdata, pkey)
{
	var pkeycolindex = getColumnIndexForType(tdata, "PrimaryKey");
	if(pkeycolindex >= 0)
	{
		for(var i=0; i<tdata.datalist.length; i++)
		{
			if(tdata.datalist[i].columns[pkeycolindex] == pkey)
			{
				return(i);	
			}	
		}	
	}
	return(-1);
}

function getSortOrderQueryString(tbodystr)
{
	var tdata = getTable(tbodystr);
	var pkeycolindex = getColumnIndexForType(tdata, "PrimaryKey");
	var snumcolindex = getColumnIndexForType(tdata, "SortNumber");
	var submiturl = "-tableSpec=" + tdata.tablename + "&-pkeyname=" + tdata.headerlist[pkeycolindex].name + "&-snumname=" + tdata.headerlist[snumcolindex].name;
	for(var i=0; i<tdata.datalist.length; i++)
	{
		submiturl = submiturl + "&" + tdata.headerlist[pkeycolindex].name + "=" + tdata.datalist[i].columns[pkeycolindex];
		submiturl = submiturl + "&" + tdata.headerlist[snumcolindex].name + "=" + i;
	}
	return(submiturl)
}

function moveSortItemDown(tbodystr, pkey)
{
	var tdata = getTable(tbodystr);
	var tbody = document.getElementById(tbodystr);
	var listlength = tdata.datalist.length;
	var selindex = getPrimaryKeyIndex(tdata, pkey);

	if(selindex>=0 && selindex!=(listlength-1))
	{
		swapData(tdata, selindex, selindex+1);
		drawTable(tbodystr);
	}
}

function moveSortItemUp(tbodystr, pkey)
{
	var tdata = getTable(tbodystr);
	var tbody = document.getElementById(tbodystr);
	var listlength = tbody.rows.length;
	var selindex = getPrimaryKeyIndex(tdata, pkey);
	if(selindex>0)
	{
		swapData(tdata, selindex-1, selindex);
		drawTable(tbodystr);
	}
}

function swapData(tdata, indexa, indexb)
{
	var a = tdata.datalist[indexa];
	var b = tdata.datalist[indexb];
	tdata.datalist[indexa] = b;
	tdata.datalist[indexb] = a;
}

function clearSelected(tbodystr)
{
	var tdata = getTable(tbodystr);
	for(var i=0; i<tdata.datalist.length; i++)
	{
		if(tdata.datalist[i].selected)
		{
			tdata.datalist[i].selected = false;
		}
	}
}

function getSelectedIndex(tbodystr)
{
	var tdata = getTable(tbodystr);
	for(var i=0; i<tdata.datalist.length; i++)
	{
		if(tdata.datalist[i].selected)
		{
			return(i);
		}
	}
	return(-1);
}

function setSelectedByKey(tbodystr, pkey)
{
	var tdata = getTable(tbodystr);
	var pkeyrowindex = getPrimaryKeyIndex(tdata, pkey);
	tdata.datalist[pkeyrowindex].selected = true;
	return(pkeyrowindex);
}

function setTableRow(tdata, tbody, rowindex)
{
	var record = tdata.datalist[rowindex];
	var td, tr;
    tr = tbody.rows[rowindex];
	var i=0;
    for(var j=0; j<record.columns.length; j++)
    {
		if(includeColumn(tdata, j))
		{    	
	        td = tr.cells[i];
	        i++;
	        td.innerHTML = record.columns[j] + "&nbsp;"; 
		}     
	}
}

function clearElements(treenode)
{
	for(var i=treenode.childNodes.length-1; i>=0 ; i--)
	{
		treenode.removeChild(treenode.childNodes[i]);
	} 
}

function drawTable(tbodystr)
{
    var tdata = getTable(tbodystr);
    var tbody = document.getElementById(tbodystr);
    clearTable(tbody);

    if(tdata.tableid.length!=0)
    {
    	if(tdata.tablestyle != "")
			{
				tbody.className = tdata.tablestyle;
			}
		
    	var theadtag = tbody.createTHead(); 
    	var mytablebody = document.createElement("TBODY");
    	tbody.appendChild(mytablebody);	
	    drawTableHeader(theadtag, tdata);
	    if(tdata.headstyle != "")
	    {
	    	theadtag.className = tdata.headstyle;
	    }
	    	    
	    for (var i = 0; i < tdata.datalist.length; i++)
	    {  
				drawTableRow(mytablebody, tdata, i);
	    }
    }
}

function clearTable(tbody)
{
    while (tbody.rows.length > 0)
    {	
        tbody.deleteRow(0);
    }
}

function getTable(namestr)
{
	var index = getTableIndex(namestr);
	if(index >= 0)
	{
		return(jstables[index]);
	}
	return(new tabledata("","",""));
}

function getTableIndex(namestr)
{
	for(var i=0; i<jstables.length; i++)
	{
		if(jstables[i].tableid == namestr)
		{
			return(i);
		}
	}
	return(-1);
}

function loadTableRow(tdata, oneRecord)
{
	var oneColumn;
	var alist = new Array();
	var nextindex = tdata.datalist.length;

	for(var j=0; j<oneRecord.childNodes.length; j++)
	{
		oneColumn = oneRecord.childNodes[j];
		if (oneColumn.nodeType == 1)
		{
			if(oneColumn.childNodes.length!=0)
			{
				alist[alist.length] = oneColumn.firstChild.nodeValue;
			}
			else
			{
				alist[alist.length] = "";
			}
		}
	}

	tdata.datalist[nextindex] = new tablerow(alist, tdata, nextindex + 1);
}

//tdata - tabledata object, xmlnode - header node
function loadTableHeader(tdata, xmlnode)
{
	var oneColumn;
	var hname = "";
	var htype, hformat, chstyle, cstyle, calign, cwidth;
	var ascf, descf;
	var sortdir = "ASC";
	var alist = new Array();
    for(var j=0; j<xmlnode.childNodes.length; j++)
    {
    	oneColumn = xmlnode.childNodes[j];
        if (oneColumn.nodeType == 1)
        {
        	if(oneColumn.nodeName == "column")
        	{
	        	if(oneColumn.childNodes.length!=0)
	        	{
	        		hname = oneColumn.firstChild.nodeValue;
				}
				else
				{
					hname = "";
				}
        		htype = oneColumn.getAttribute("type");
				hformart = oneColumn.getAttribute("format");
				chstyle = typeof oneColumn.getAttribute("headerstyle") != "undefined"?oneColumn.getAttribute("headerstyle"):"";
				cstyle = typeof oneColumn.getAttribute("style") != "undefined"?oneColumn.getAttribute("style"):"";
				calign = typeof oneColumn.getAttribute("align") != "undefined"?oneColumn.getAttribute("align"):"";
				cwidth = typeof oneColumn.getAttribute("width") != "undefined"?oneColumn.getAttribute("width"):"";
				ascf = getSortFunction(htype, hformart, "ASC");
				descf = getSortFunction(htype, hformart, "DESC");
				alist[alist.length] = new columnmeta(hname, htype, hformat, sortdir, ascf, descf, chstyle, cstyle, cwidth, calign);
			}
			else if(oneColumn.nodeName == "styles")
			{
				var tstyle = oneColumn.getAttribute("table");
				var hrstyle = oneColumn.getAttribute("headerrow");
				var rstyle = oneColumn.getAttribute("row");
				var thstyle = oneColumn.getAttribute("tablehead");
				var tbstyle = oneColumn.getAttribute("tablebody");
				tdata.tablestyle = tstyle;
				tdata.headerrowstyle = hrstyle;
				tdata.rowstyle = rstyle;
				tdata.headstyle = thstyle;
				tdata.bodystyle = tbstyle;								
			}		
        }
	}
	tdata.headerlist = alist;
}

function includeColumn(tdata, colindex)
{
	return(tdata.headerlist[colindex].type != "PrimaryKey" && tdata.headerlist[colindex].type != "SortNumber");
}

function drawTableRow(tbody, tdata, rowindex)
{
	var record = tdata.datalist[rowindex];
	var td;
	var tr;
	var clength = tbody.rows.length;
  tr = tbody.insertRow(clength);
  if(tdata.rowstyle != "")
	{
		tr.className = tdata.rowstyle;
	}
  for(var j=0; j<record.columns.length; j++)
  {
		if(includeColumn(tdata, j))
		{   
	    td = tr.insertCell(tr.cells.length);
			td.innerHTML = record.columns[j] + "&nbsp;";
			setStyle(td, tdata.headerlist[j]);	    
		}
	}
}

function setStyle(ele, colmeta)
{
	if(colmeta.headerstyle != "")
	{
		ele.className = colmeta.headerstyle;
	}
	if(colmeta.align != "")
	{
		ele.style.textAlign = colmeta.align;
	}
	if(colmeta.width != "")
	{
		ele.style.width = colmeta.width;
	}
}

function drawTableHeader(tbody, tdata)
{
	var oneRecord = tdata.headerlist;
	var tname = tdata.tableid;
	var td, tr;
	var clength = tbody.rows.length;
	tr = tbody.insertRow(clength);
	if(tdata.headerrowstyle != "")
	{
		tr.className = tdata.headerrowstyle;
	}
	for(var j=0; j<oneRecord.length; j++)
	{
		if(includeColumn(tdata, j))
		{
		    td = tr.insertCell(tr.cells.length);
		    td.innerHTML = getHeaderText(oneRecord[j].name, j, tname) + "&nbsp;";
			setStyle(td, tdata.headerlist[j]);
		}
	}
}

var xmlreq;

function verifySupport(xFile, callbackMethod) {
    // branch for native XMLHttpRequest object
    //alert("new");
    if (window.XMLHttpRequest) {
        xmlreq = new XMLHttpRequest();
        xmlreq.onreadystatechange = callbackMethod;
        xmlreq.open("GET", xFile, true);
        xmlreq.send(null);
        return true;
    // branch for IE/Windows ActiveX version
    } else if (window.ActiveXObject) {
        xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
        if (xmlreq) {
            xmlreq.onreadystatechange = callbackMethod;
            xmlreq.open("GET", xFile, true);
            xmlreq.send();
            return true;
        }
    }
    alert("Runway requires a browser with XML support, " +
            "such as IE5+/Windows, Firefox, Safari or Netscape 6+.");
	return false;
}

var tbodystr;

function init(xFile, eleid) {
	tbodystr = eleid;
    // confirm browser supports needed features and load .xml file
    if (verifySupport(xFile, loadTable)) {
        // let file loading catch up to execution thread
		//var str = "loadTable(\'" + eleid + "\')";
    //    setTimeout(str, 2000);
   }
}

// Sorting function dispatcher (invoked by table name, column index)
function sortTable(tname, index)
{
	var tdata = getTable(tname);
	var listdata = tdata.datalist;
	var coldata = tdata.headerlist[index];
	tdata.sortcolumn = index;
	if(coldata.sort == "ASC")
	{
    	listdata.sort(coldata.ascfunction);
    	coldata.sort = "DESC";
	}
	else
	{
		listdata.sort(coldata.descfunction);
		coldata.sort = "ASC";
	}
    drawTable(tname);
    return false;
}

function getSortFunction(coltype, colformat, colsort)
{
	if(coltype == "Date")
	{
		if(colformat == "dd/MM/yyyy")
		{
			if(colsort == "ASC")
			{
				return(sortByDDMMYYYYAsc);
			}
			else
			{
				return(sortByDDMMYYYYDesc);
			}	
		}	
	}
	else if(coltype == "Number")
	{
		if(colsort == "ASC")
		{
			return(sortByNumberAsc);
		}
		else
		{
			return(sortByNumberDesc);
		}
	}	
	else  //default string sort
	{
		if(colsort == "ASC")
		{
			return(sortByTextAsc);
		}
		else
		{
			return(sortByTextDesc);
		}		
	}
}

function sortByTextAsc(al, bl)
{
	var columnindex = al.table.sortcolumn;
	a = al.columns[columnindex];
	b = bl.columns[columnindex];
    a = a.toLowerCase( );
    b = b.toLowerCase( );
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
}

function sortByTextDesc(a, b)
{
	return(sortByTextAsc(b, a));
}

function sortByNumberAsc(al, bl)
{
	var columnindex = al.table.sortcolumn;
	a = al.columns[columnindex];
	b = bl.columns[columnindex];
    return b - a;
}

function sortByNumberDesc(a, b)
{
	return(sortByNumberAsc(b, a));
}

function sortByDateAsc(ay, am, ad, by, bm, bd)
{
	var a = new Date(ay, am, ad);
	var b = new Date(by, bm, bd);
	return ((a < b) ? -1 : ((a > b) ? 1 : 0));
}

function sortByDDMMYYYYAsc(al, bl)
{
	var columnindex = al.table.sortcolumn;
	ad = al.columns[columnindex];
	bd = bl.columns[columnindex];
	
	if(ad.length == 0 && bd.length == 0)
	{
		return(0);	
	}
	else if(ad.length == 0)
	{
		return(-1);
	}
	else if(bd.length == 0)
	{
		return(1);	
	}

	var slash = "/";
	var a = ad.split(slash);
	var b = bd.split(slash);
	return(sortByDateAsc(a[2],a[1],a[0],b[2],b[1],b[0]));
}

function sortByDDMMYYYYDesc(ad, bd)
{
	return(sortByDDMMYYYYAsc(bd, ad));	
}

function urlEncode(plaintext)
{
	// The Javascript escape and unescape functions do not correspond
	// with what browsers actually do...
	var SAFECHARS = "0123456789" +					// Numeric
					"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +	// Alphabetic
					"abcdefghijklmnopqrstuvwxyz" +
					"-_.!~*'()";					// RFC2396 Mark characters
	var HEX = "0123456789ABCDEF";

	var encoded = "";
	for (var i = 0; i < plaintext.length; i++ ) {
		var ch = plaintext.charAt(i);
	    if (ch == " ") {
		    encoded += "+";				// x-www-urlencoded, rather than %20
		} else if (SAFECHARS.indexOf(ch) != -1) {
		    encoded += ch;
		} else {
		    var charCode = ch.charCodeAt(0);
			if (charCode > 255) {
			    //Unicode Character cannot be encoded using standard URL encoding.
				//URL encoding only supports 8-bit characters.)\n
				//A space (+) will be substituted." 
				encoded += "+";
			} else {
				encoded += "%";
				encoded += HEX.charAt((charCode >> 4) & 0xF);
				encoded += HEX.charAt(charCode & 0xF);
			}
		}
	} // for

	return(encoded);
};

function urlDecode(encoded)
{
   // Replace + with ' '
   // Replace %xx with equivalent character
   // Put [ERROR] in output if %xx is invalid.
   var HEXCHARS = "0123456789ABCDEFabcdef"; 
   var plaintext = "";
   var i = 0;
   while (i < encoded.length) {
       var ch = encoded.charAt(i);
	   if (ch == "+") {
	       plaintext += " ";
		   i++;
	   } else if (ch == "%") {
			if (i < (encoded.length-2) 
					&& HEXCHARS.indexOf(encoded.charAt(i+1)) != -1 
					&& HEXCHARS.indexOf(encoded.charAt(i+2)) != -1 ) {
				plaintext += unescape( encoded.substr(i,3) );
				i += 3;
			} else {
				//Ignore bad escape combination
				i++;
			}
		} else {
		   plaintext += ch;
		   i++;
		}
	} // while

   return(plaintext);
};