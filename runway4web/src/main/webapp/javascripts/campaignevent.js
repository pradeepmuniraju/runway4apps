function incrDate(formname, amount)
{

	var thisform = this.document.forms[formname];
	var eventdatestr = thisform.elements['EventDate'].value;
	var eventtimestr = thisform.elements['EventTime'].value;
	
	if(typeof amount == 'undefined' || amount == null || amount.length == 0){
		amount = "1:0";
	}
	else{
		eventdatestr = thisform.elements['-EventDate'].value;
		eventtimestr = thisform.elements['-EventTime'].value;
	}
	
	var eventdatevalues = eventdatestr.split("/");
	var eventtimevalues = eventtimestr.split(":");

	if(eventdatevalues.length == 3)
	{
		var amounts = amount.split(":");
		if (amounts.length < 2) {
			amounts = new Array(amount,"0");
		}
		if (amounts[0].length == 0) amounts[0] = "0";
		if (amounts[1].length == 0) amounts[1] = "0";

		//eventdatevalues[0] = new Number(eventdatevalues[0]) + amount;
		var eventdate = new Date(eventdatevalues[2],eventdatevalues[1]-1,eventdatevalues[0],eventtimevalues[0],eventtimevalues[1]);
		
		if (eventdate < new Date()) {
		   eventdate = new Date();
		}
		var adjust = Number(amounts[0]) * 24 * 60 * 60;
		adjust += Number(amounts[1]) * 60 * 60;
		adjust *= 1000;

		// set the new time;
		eventdate = new Date(eventdate.getTime() + adjust);
		
		// adjust to the next 15 minutes
		var minutesAdj = eventdate.getMinutes();
		
		adjust = (15 - (Number(minutesAdj) % 15)) * 1000;

		eventdate = new Date(eventdate.getTime() + adjust);

		var eventday = eventdate.getDate();
		
	    var eventyearnum = eventdate.getYear();
		if(eventyearnum < 1000 && eventyearnum > 99)
		{
			eventyearnum = eventyearnum+1900;
		}
		
		var eventmonthnum = eventdate.getMonth() + 1;
		if (eventday < 10) {
		   eventday = "0" + eventday.toString();
		}
		if (eventmonthnum < 10) {
		   eventmonthnum= "0" + eventmonthnum;
		}
		var newdatevalue = eventday + "/" + eventmonthnum + "/" + eventyearnum;
		thisform.elements['EventDate'].value = newdatevalue;
		
		var eventHrsnum = eventdate.getHours();
		if (eventHrsnum < 10) {
			eventHrsnum = "0" + eventHrsnum.toString();
		}
		eventMin = eventdate.getMinutes();
		if (eventMin < 10) {
			eventMin= "0" + eventMin.toString();
		}

		for (var s = 0; s < thisform.elements['EventTime'].options.length; ++s) {
			if ((eventHrsnum + ":" + eventMin) == thisform.elements['EventTime'].options[s].value) {
				thisform.elements['EventTime'].selectedIndex = s;
				break;
			}
		}

	}
	else
	{
		alert("The dispatch date is not in dd/mm/yyyy format");
	}
}

function incrDateInline(panelid, formname){
	incrDate(formname);
	var thisform = document.forms[formname];
	loadPanel(panelid, thisform);
}

function setToday(formname) {
   var thisform = this.document.forms[formname];

   var eventdate = new Date();
	var eventday = eventdate.getDate();
			
   var eventyearnum = eventdate.getYear();
	if(eventyearnum<1000 && eventyearnum>99)
	{
		eventyearnum = eventyearnum+1900;
	}
	var eventmonthnum = eventdate.getMonth()+1;
	if (eventday < 10) {
	   eventday = "0" + eventday.toString();
	}
	if (eventmonthnum < 10) {
	   eventmonthnum= "0" + eventmonthnum;
	}
	var newdatevalue = eventday+"/"+eventmonthnum+"/"+eventyearnum;
	thisform.elements['EventDate'].value = newdatevalue;
}

function setNow(formname) {

   var eventdate = new Date();
   
   var eventhours = eventdate.getHours();
   var eventminutes = eventdate.getMinutes();
   if (eventminutes <= 15) {
      eventminutes = "15";
   }
   else if (eventminutes <= 30) {
      eventminutes = "30";
   }
   else if (eventminutes <= 45) {
      eventminutes = "45";
   }
   else {
      eventminutes = "00";
      eventhours++;
   }   
   var eventhours = eventdate.getHours();
	if (eventhours < 10) {
	   eventhours = "0" + eventhours.toString();
	}
	var time = eventhours + ":" + eventminutes;
	var select = this.document.forms[formname].elements['EventTime'];
	for (i=0; i<select.options.length; i++) {
	   if (select.options[i].value == time) {
	      select.options[i].selected = true;
	      break
	   }
	}
}

function incrAttempt(formname, eventid, imagepath)
{
	var thisform = this.document.forms[formname];
	var attpstr = thisform.elements['Attempts'].value;

	attpstr = new Number(attpstr) + 1;
	thisform.elements['Attempts'].value = attpstr;
	
	var alloc = thisform.elements['AttemptsAllocated'].value
	if(alloc == attpstr)
	{
		setStatus(formname, eventid, imagepath);
	}
}

function incrAttemptInline(panelid, formname, eventid)
{
	var thisform = this.document.forms[formname];
	var attpstr = thisform.elements['Attempts'].value;

	attpstr = new Number(attpstr) + 1;
	thisform.elements['Attempts'].value = attpstr;
	loadPanel(panelid, thisform);
}


function setStatus(formname, eventid, imagepath)
{
	var formname = "form" + eventid;
	var imagename = "image" + eventid;
	var thisform = document.forms[formname];
	var nextvalue = "";
	var inputobj = null;

	inputobj = thisform.Status;

	if(inputobj.value=="Active")
	{
		nextvalue = "Inactive";
	}
	else
	{
		nextvalue ="Active";
	}
	document.images[imagename].title = "Currently " + nextvalue + ", click to toggle";

	inputobj.value = nextvalue;

	var imageurl = "";
	
	imageurl = imagepath+nextvalue+"_icon.gif";

	changeImages(imagename, imageurl);
}