String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function doAllCheckBoxCheck() {
	$("#allcb").attr("checked", true);
	$(".r5searchheading").each(function(){
		var id = $(this).parent().parent().attr("id");
		
		if (id.indexOf("main") == 0) {
			id = id.substring(4);
		
			var box = $("#main" + id).attr("style");
			var boxOn = box.search("display: none;") == -1;
			if (boxOn) {
				$('#' + id + 'cb').attr("checked", true);
			} else {
				$("#allcb").attr("checked", false);
			}
		}
	});		
}

function doToggleSingleMenuItem(itemId) { 
	var chkboxId = itemId + "cb";
	var chkbox = document.getElementById(chkboxId);
	var checkFlag = chkbox.checked; 
	
	toggleMenuItem("main" + itemId, checkFlag);
	doAllCheckBoxCheck();
}

function toggleMenuItem(itemId, checkFlag) {
	if (checkFlag == false) {
		hidebox(itemId);
		updateBoxLayout(itemId, '<%=requestPath%>', '${contextPath}');
	} else {
		openbox(itemId);	
		updateBoxLayout(itemId, '<%=requestPath%>', '${contextPath}');
	}
}

function showhideAll() { 
	var checkFlag = document.getElementById("allcb").checked; 
	$(".r5searchheading").each(function(){
		var id = $(this).parent().parent().attr("id");
		
		if (id.indexOf("main") == 0) {
			id = id.substring(4);

			toggleMenuItem("main" + id, checkFlag);
			
			$('#' + id + 'cb').attr("checked", checkFlag);
		}
	});		
}

<!-- show hide stuff here -->
function showhide(id) {
	var div = document.getElementById("box" + id);
	if (typeof div != 'undefined' && div != null) {
		if (div.style.display == 'none') {
			div.style.display = "";
		} else {
			div.style.display = "none";
		}
	}
}

function toggle(id) {
	var div = document.getElementById("box" + id);
	var img = document.getElementById("updown" + id);
	if (typeof div != 'undefined' && div != null && typeof img != 'undefined' && img != null) {
		if (div.style.display == 'none') {
			img.src = img.src.replace("up","down");
		} else {
			img.src = img.src.replace("down","up");
		}
	}
}

function openbox(id) { 
	//alert("open " + id);
	var div = document.getElementById(id);
	if (typeof div != 'undefined' && div != null) {
		if (div.style.display == 'none') {
			div.style.display = "";
		} 
	}
	doAllCheckBoxCheck();
}

function hidebox(id) { 
	//alert("hide " + id);
	var div = document.getElementById(id); 
	if (typeof div != 'undefined' && div != null) { 
		if (div.style.display != 'none') {
			div.style.display = "none";
		}
	}
}

function closebox(Id) {
	var itemId = Id;
	if (itemId == 'estateList' || itemId == 'stageList' || itemId == 'lotList') itemId = 'estate';
	var chkboxId = itemId + "cb";
	var chkbox = document.getElementById(chkboxId);
	if (typeof chkbox != 'undefined' && chkbox != null) chkbox.checked = false;

	toggleMenuItem("main" + Id, false);
	doAllCheckBoxCheck();
}

$(document).ready(function() {
	$(".r5searchheading").each(function(){
		var text = $(this).text();
		var id = $(this).parent().parent().attr("id");
		
		if (id.indexOf("main") == 0) {
			id = id.substring(4);
			
			var input = '<input type="checkbox" id="' + id + 'cb" value="N" onclick="javascript:doToggleSingleMenuItem(\'' + id + '\')"><label for="' + id + 'cb">' + text.toProperCase() + '</label><br>';

			$("#dropmenuPropertyMain").append(input);
		} else if (id.indexOf("nohide") == 0) {
			id = id.substring(6);
			
			var input = '<input type="checkbox" id="' + id + 'cb" value="N" checked="checked" disabled="disabled" onclick="javascript:doToggleSingleMenuItem(\'' + id + '\')"><label for="' + id + 'cb">' + text.toProperCase() + '</label><br>';

			$("#dropmenuPropertyMain").append(input);
		}
	});
	setTimeout(doAllCheckBoxCheck, 1000);
});
