function getDroppedElementID(jobj){
	var idstr = jobj.attr("id");
	idstr = idstr.substring(idstr.indexOf("_")+1, idstr.length);
	return(idstr);
}

$(document).ready(function(){
	if (typeof $(".imageblock") != 'undefined' && typeof $(".imageblock").draggable != 'undefined') {
		$(".imageblock").draggable({helper:'clone'}); 
		$(".mainimagedrop").droppable({
			accept: ".imageblock",
			activeClass: 'droppable-active',
			hoverClass: 'droppable-hover',
			drop: function(ev, ui) {
				processMainImageDrop(getDroppedElementID($(ui.draggable)));
			}
		});
		$(".thumbnailimagedrop").droppable({
			accept: ".imageblock",
			activeClass: 'droppable-active',
			hoverClass: 'droppable-hover',
			drop: function(ev, ui) {
				processThumnailImageDrop(getDroppedElementID($(ui.draggable)));
			}
		});
		$(".albumimagedrop").droppable({
			accept: ".imageblock",
			activeClass: 'droppable-active',
			hoverClass: 'droppable-hover',
			drop: function(ev, ui) {
				var div = $(ev.target), doc = $(ui.draggable);
				processalbumImageDrop(div.data('galleryId'), getDroppedElementID(doc));
			}
		});
	}
});