//This is used for quick search && filter display on report screen
function submitForm(searchfield,suppliedvalue)
{	
	var currentDashboard = '';
	if ( selectedReportsHasDashboard() == "true" ) {
		var currentDashboard = $("#selectedreportspanel p:first").attr("id").substring(9);
	}
	
	var useform = document.getElementById("form1");
	if( suppliedvalue ) {
		useform.elements[4].value = suppliedvalue;
	}
	useform.elements[1].value = searchfield;
	useform.elements[2].value = currentDashboard;
	useform.elements[3].value = selectedReportsHasDashboard();
	useform.submit();	
}

// formelement is the ID of the element that has the text to be changed
// recordid is the id of the record to be updated
// entity is the tablespec name (i.e the table that the record id belongs in)
// fieldname is the name in the tablespec of the field to update
// length is the max characters to display
// maxlengthdb is the length allowed for the field in DB
// callbackfunction - speaks for itself
//callback - this is a real callback function. It is invoked after the input field has been edited and reverted back to a label.
//callbackargs - optional array of string arguments for callback function
function makeFieldEditable(formelement,recordid,entity,fieldname,length,maxlengthdb,callbackfunction,callback,callbackargs)
{
	var freshrequest = new Date().getTime();
	var y = new String(document.getElementById(formelement).innerHTML);
	var size = new Number(y.length) + 10;
	
	var length = length ? length : 45;
	var maxlengthdb = maxlengthdb ? maxlengthdb : 100;
	
	if( size > 40 ) { size = 40 };
	
	$.get("../reportgenerator/ajax/getName.jsp?uniqueid=" + freshrequest, {Recordid: recordid, Entity: entity, Field: fieldname }, function(data){	
		
		document.getElementById(formelement).removeAttribute('onclick');
		var fieldCurrentlyEditableID = "fieldCurrentlyEditable" + formelement;
		document.getElementById(formelement).innerHTML = "<input type='text' id='" + fieldCurrentlyEditableID + "'/>";
		var currentField = document.getElementById(fieldCurrentlyEditableID); 
		currentField.value = data.replace(/^\s*|\s*$/g,'');
		currentField.size= size;
		currentField.focus();
		$("#" + fieldCurrentlyEditableID).blur(function(){
			 toggleMakeFieldEditable(this.value, formelement, recordid, entity, fieldname, length, maxlengthdb, callbackfunction,callback,callbackargs);
		});  
		
		$("#" + fieldCurrentlyEditableID).keydown(function(event){
			 checkEnterKeyPressed(event.keyCode,this.value, formelement, recordid, entity, fieldname, length, maxlengthdb, callbackfunction,callback,callbackargs);
		});  
		
		$("#" + fieldCurrentlyEditableID).click(function(){
			 return false;
		}); 		
		
		currentField.setAttribute('maxlength', maxlengthdb);
	});	

}

function toggleMakeFieldEditable(newname,formelement,recordid,entity,fieldname,length,maxlengthdb, callbackfunction,callback,callbackargs)
{
	var freshrequest = new Date().getTime();
	$.get("../reportgenerator/ajax/editName.jsp?uniqueid=" + freshrequest, {Recordid: recordid, Name: newname, Entity: entity, Field: fieldname, Maxlengthdb: maxlengthdb }, function(data){	
		var success = data.replace(/^\s*|\s*$/g,'');
		if(success != "true")
		{
			//is this necessary ???????????	
		}
			if(newname.length > length)
			{
				newname = newname.substring(0,length) + '...';
			}
			document.getElementById(formelement).innerHTML = newname;
			document.getElementById(formelement).setAttribute('onClick', "makeFieldEditable(this.id,'" + recordid + "','" + entity + "','" + fieldname + "','" + length + "','" + maxlengthdb + "','" + callbackfunction + "','" + callback + "'," + arrayToArrayString(callbackargs) + ")");
			if(callbackfunction) {
				eval(callbackfunction + "('" + recordid + "','" + urlEncode(newname) + "')");
			}
			if(callback) {
				var args = "";
				if(callbackargs && callbackargs.length > 0) {
					args = "'" + callbackargs[0] + "'";
					for(var i=1; i<callbackargs.length; i++) {
					  args = args + ", '" + callbackargs[i] + "'";
					}
				}
			    eval(callback + "(" + args + ")");
			}
	});
	
}

function arrayToArrayString(array) {
	var arrayString = "[]";
	
	if(array && array.length > 0) {
		arrayString = "['" + array[0] + "'";
	  
		for(var i=1; i<array.length; i++) {
			arrayString = arrayString + ", '" + array[i] + "'";
		}
		
		arrayString = arrayString + "]";
	}	
	
	return arrayString;
}

function checkEnterKeyPressed(keycode,value,formelement,recordid,entity,fieldname,length,maxlengthdb, callbackfunction)
{
	if (keycode == 13 || keycode == 10)
	{	
		toggleMakeFieldEditable(value,formelement,recordid,entity,fieldname,length,maxlengthdb,callbackfunction);
	}
}

function preventUserFromSelectingReportsOnVirtualDashboard()
{
	//prevent user from adding reports already on virtual dashboard
	var $getReports = $("#beginselectedreports").children();
	var $allReports = new Array();
	
	if( $getReports ) {
		for(p=0; p < $getReports.length; p++)
		{
			$allReports[p] = $getReports[p].getAttribute("name").substring(6);
		}


		for(p=0; p < $allReports.length; p++)
		{
			var prepareHide = "tr[name='tablerow" + $allReports[p] + "']"
			
			var $toHide = $(prepareHide);

			if ( $toHide.length > 0 ) 
			{
				for(t=0; t < $toHide.length; t++)
				{
					$toHide[t].getElementsByTagName("li")[1].style.visibility = "hidden";
					$toHide[t].getElementsByTagName("li")[1].style.display = "none";
				}
			}

		}
	}
}

function selectedReportsHasDashboard()
{
	var panel = document.getElementById("selectedreportspanel");
	if (panel != null) {
		var ul = panel.getElementsByTagName("ul");
		if (ul != null) {
			var x = ul[0];
		
			if(x==null || x.getAttribute("name") =="noDashboardSelected") {
				return "false"
			}
		
			else {
				return "true"
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}


/* Number of reports on selected reports canvas */
function numberOfReports()
{
	var $kids = $("#beginselectedreports").children(); //returns the immediate children in an array
	return $kids.length;
}	


/* Add use from the drop downs associated with each dashboard on the search results screen */
function showUseFromDashboards()
{
	var $hideUse = $("tr[id^='dashboard']");
	for(p=0; p < $hideUse.length; p++)
	{
		$hideUse[p].getElementsByTagName("li")[1].style.visibility = "visible";
		$hideUse[p].getElementsByTagName("li")[1].style.display = "inline";
	}
}

/* Shows use from the drop downs associated with eeach report on the search results screen */
function showUseFromReports()
{	
	var $toHide = $("tr[name^='tablerow']");
	for(p=0; p < $toHide.length; p++)
	{
		$toHide[p].getElementsByTagName("li")[1].style.visibility = "visible";
		$toHide[p].getElementsByTagName("li")[1].style.display = "inline";
	}
}

function removeAllDashboardItems(dashboardid,tablespec,tablekey)
{
		var URLString = "../reportgenerator/ajax/deleteAllDashboardItems.jsp?" + tablekey + "=" + dashboardid + "&-tableSpec=" + tablespec;
		var defresponse = new DefaultResponse(
			function(obj) {				
				
				$("form[currentpage='true'] input[name='-beanID'] ").attr("value", "");
				$("form[currentpage='true'] input[name='-dashboardID'] ").attr("value", dashboardid);
				$("form[currentpage='true'] input[name='-selectedReportsHasDashboard'] ").attr("value", "true");
				$("form[currentpage='true']").submit();
				
								
			}
		);
		var xmlreq = new XmlRequest(defresponse, dashboardid);
		xmlreq.send(URLString);		
}

//e.g. genericUserLayout(this, 'other_off', 'other_active', '-tableSpec=Contacts');
function genericUserLayout( obj, defaultimage, swapimage, querydata, contextpath )
{
	var currentsrc = obj.src;
	if( currentsrc.indexOf( defaultimage ) != -1)
	{	
		obj.src = currentsrc.replace(defaultimage, swapimage);
		var URLString = contextpath + "/crm/reportgenerator/ajax/userlayoutaction.jsp?-action=insert&" + querydata;
		var defresponse = new DefaultResponse(
			function(obj) {				
								
			}
		);
		var xmlreq = new XmlRequest(defresponse, new Date().getTime());
		xmlreq.send(URLString);		//insert to DB
	}
	else
	{
		obj.src = currentsrc.replace(swapimage, defaultimage);
		var URLString = contextpath + "/crm/reportgenerator/ajax/userlayoutaction.jsp?-action=delete&" + querydata;
		var defresponse = new DefaultResponse(
			function(obj) {				
								
			}
		);
		var xmlreq = new XmlRequest(defresponse, new Date().getTime());
		xmlreq.send(URLString);		//delete to DB		
	}
	

	
}


