/**
 * author: Ulpian Cesana
 * 
 * Create a SimpleLogger on your page and write log messages to different 
 * logger types using the info, debug, error, and warning functions. 
 * 
 * You can toggle each type of logger to output using the setInfo, setDebug, 
 * setError and setWarning functions. All loggers are turned on by default, 
 * except for the DEBUG logger.
 * 
 * Logs are written to the alert popup by default. You can specify your own 
 * writer by using the setWriter function.
 * 
 * Usage:
 * 
 *     var id = 1234;
 *     var name = "test name";
 *     
 *     var logger = new SimpleLogger();
 *     logger.setDebug(true);
 *     logger.debug("ID: ", "1234", ", ", "name: ", "name");
 *     
 * will create the following alert:
 * 
 *     [DEBUG] ID: 1234, name: test name
 */

/**
 * Creates a SimpleLogger object.
 */
function SimpleLogger() {
    
}

/**
 * A writer that writes messages to the alert popup. 
 */
SimpleLogger.WRITER_ALERT = {
        write: function(message) {
                alert(message);
        }
};

/**
 * Info logger type
 */
SimpleLogger.INFO = {
        type: "INFO", 
        isOn: true,
        writer: SimpleLogger.WRITER_ALERT
};

/**
 * Debug logger type
 */
SimpleLogger.DEBUG = {
        type: "DEBUG", 
        isOn: false,
        writer: SimpleLogger.WRITER_ALERT
};

/**
 * Error logger type
 */
SimpleLogger.ERROR = {
        type: "ERROR", 
        isOn: true,
        writer: SimpleLogger.WRITER_ALERT
};

/**
 * Warning logger type
 */
SimpleLogger.WARNING = {
        type: "WARNING", 
        isOn: true,
        writer: SimpleLogger.WRITER_ALERT
};

/**
 * Convenience function to set the isOn flag on the info logger
 */
SimpleLogger.prototype.setInfo = function(isOn) {
    this.setOn(SimpleLogger.INFO, isOn);
};

/**
 * Convenience function to set the isOn flag on the debug logger
 */
SimpleLogger.prototype.setDebug = function(isOn) {
    this.setOn(SimpleLogger.DEBUG, isOn);
};

/**
 * Convenience function to set the isOn flag on the error logger
 */
SimpleLogger.prototype.setError = function(isOn) {
    this.setOn(SimpleLogger.ERROR, isOn);
};

/**
 * Convenience function to set the isOn flag on the warning logger
 */
SimpleLogger.prototype.setWarning = function(isOn) {
    this.setOn(SimpleLogger.WARNING, isOn);
};

/**
 * Convenience function to write messages to the info logger
 */
SimpleLogger.prototype.info = function() {
    this.log(SimpleLogger.INFO, this.info.arguments);
};

/**
 * Convenience function to write messages to the debug logger
 */
SimpleLogger.prototype.debug = function() {
    this.log(SimpleLogger.DEBUG, this.debug.arguments);
};

/**
 * Convenience function to write messages to the error logger
 */
SimpleLogger.prototype.error = function() {
    this.log(SimpleLogger.ERROR, this.error.arguments);
};

/**
 * Convenience function to write messages to the warning logger
 */
SimpleLogger.prototype.warning = function() {
    this.log(SimpleLogger.WARNING, this.warning.arguments);
};

/**
 * @param logger the type of logger
 * @param isOn the isOn to set on the specified logger 
 */
SimpleLogger.prototype.setOn = function(logger, isOn) {
    logger.isOn = isOn;
};

/**
 * @param logger the type of logger
 * 
 * @return true if the specified logger is turned on, otherwise return false
 */
SimpleLogger.prototype.isOn = function(logger) {
    return (logger && logger.isOn);
};

/**
 * Log the messages to the specified logger.
 * 
 * @param logger the type of logger
 * @param messages is an array of strings and numbers 
 */
SimpleLogger.prototype.log = function(logger, messages) {
    if(this.isOn(logger)) {
         var message = "[" + logger.type + "] ";
 
         for (var i = 0; i < messages.length; i++) {
             message += messages[i];
         }
         logger.writer.write(message);
    }
};

/**
 * Set the writer to the specified logger. 
 * 
 * @param logger the type of logger
 * @param writer is a map object with a write function:
 * 
 *     var myWriter = {
 *             write: function(message) {
 *                 // Your writer code here
 *             }
 *     }
 */
SimpleLogger.prototype.setWriter = function(logger, writer) {
    logger.writer = writer;
};