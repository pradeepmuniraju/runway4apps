var FADE_IN_TIME = 200;
var FADE_OUT_TIME = (FADE_IN_TIME * 2) / 3;
var QUICK_FADEOUT_TIME = FADE_OUT_TIME / 2;
var EDIT_MODE = 'EDIT_MODE';
var NEW_MODE = 'NEW_MODE';
var categoryPanel = null;
var costLabelPanel = null;

$(document).ready(function() {

    $("#create-new-cost-label").click(function() {
        costLabelPanel = createCostLabelPanel(NEW_MODE);
        clearContentPane();
        $("#content-pane").append(costLabelPanel);
        resetCostLabelPanel(NEW_MODE);
        costLabelPanel.fadeIn(FADE_IN_TIME);
        $("#cost-label-field").focus();
    });

    $("#create-new-category").click(function() {
        categoryPanel = createCategoryPanel(NEW_MODE);
        clearContentPane();
        $("#content-pane").append(categoryPanel);
        resetCategoryPanel(NEW_MODE);
        categoryPanel.fadeIn(FADE_IN_TIME);
        $("#category-name-field").focus();
    });

    $(".delete-category").click(deleteCategory);
    $(".edit-category").click(editCategory);
    $(".edit-label").click(editCostLabel);
    $(".delete-label").click(deleteCostLabel);
});

function clearContentPane() {    
    $("#content-pane > *").remove();
}

function createCategoryPanel(mode) {
    var title = mode == EDIT_MODE ? 'Edit Category' : 'Create New Category';
    var panel = $("<div id='new-category-panel'></div>")
        .append("<div class='panel-title'><h3>" + title + "</h3></div>")
        .append(
            $("<form id='new-category-form'></form>")
            .append(mode == EDIT_MODE ? "<input type='hidden' name='CostCategoryID' id='cost-category-id-field'>" : "")
            .append(
                $("<table width='350' id='new-category-table'></table>")
                    .append("<col width='150'>")
                    .append("<col")
                    .append("<tr><td>Category Name</td><td><input type='text' name='Label' class='text' id='category-name-field'></td></tr>")
                    .append("<tr><td>Category Description</td><td><input id='category-desc-field' type='text' name='Description' class='text'></td></tr>")
                    .append("<tr><td colspan='2' style='text-align: right;'><input type='button' value='Save' id='save-category-submit'/></td></tr>")
            )
        );

    return panel;
}

function resetCategoryPanel(mode) {
    var title = mode == EDIT_MODE ? 'Edit Category' : 'Create New Category';
    var callback = mode == EDIT_MODE ? updateCategory : saveCategory;
    $("#new-category-panel").css("display", "none");
    $("#new-category-form input[type=text]").val("");
    $(".panel-title h3").text(title);
    $("#save-category-submit").click(callback);
}

function saveCategory() {
    var success = false;
    var json = null;
    $.ajax({
        url: "ajax/save_category.jsp",
        data: $("#new-category-form").serialize(),
        async: false,
        dataType: "json",
        success: function(data) {
            success = data.success;
            json = data;
        }
    });

    if (success) {
        $("<tr id='category-" + json.id + "' class='category-record' style='display: none;'><td>" + json.label + "</td></tr>")
                .append(
                    $("<td></td>")
                        .append(
                            $('<a href="#" class="edit-category">Edit</a>').click(
                                editCategory
                            )
                        )
                        .append(" | ")
                        .append(
                            $('<a href="#" class="delete-category">Delete</a>').click(
                                deleteCategory
                            )
                        )

                )
                .appendTo($("#side-bar"))
                .fadeIn(FADE_IN_TIME);
    }

    categoryPanel.fadeOut(FADE_OUT_TIME);
    return false;
}

function deleteCategory() {
    var elemId = $(this).parent().parent().attr("id");
    var id = elemId.substring(elemId.indexOf("-") + 1);
    var success = false;

    $.ajax({
        url: "ajax/delete_category.jsp",
        data: {"CostCategoryID" : id},
        async: false,
        dataType: "json",
        success: function(data) {
            success = data.success;            
        }
    });

    if(success) {
        $(".label-category-" + id).remove();
        $("#category-" + id).remove();
    }

    return false;
}

function createCostLabelPanel(mode) {
    var catSelect = $("<select id='cost-label-category-id' name='CostCategoryID'></select>");
    var cats = [];
    var success = false;
    $.ajax({
        url: "ajax/categories.jsp",
        async: false,
        dataType: "json",                                           
        success: function(data) {
            success = data.success;
            cats = data.categories;
        }
    });

    for (var i = 0; i < cats.length; i++) {
        var cat = cats[i];
        catSelect.append("<option value='" + cat.id + "'>" + cat.label + "</option>")
    }    

    var title = mode == NEW_MODE ? "Create New Cost Label" : "Edit Cost Label"; 
    var panel = $("<div id='new-cost-label-panel'></div>")
        .append("<div class='panel-title'><h3>" + title + "</h3></div>")
        .append(
            $("<form id='new-cost-label-form'></form>")
            .append(mode == NEW_MODE ? "" : "<input type='hidden' name='CostLabelID' id='cost-label-id'>")
            .append(
                $("<table width='350' id='new-cost-label-table'></table>")
                    .append("<col width='150'>")
                    .append("<col")
                    .append("<tr><td>Label</td><td><input type='text' name='Label' class='text' id='cost-label-field'></td></tr>")
                    .append("<tr><td>Description</td><td><input type='text' name='Description' class='text' id='cost-desc-field'></td></tr>")
                    .append("<tr><td>Default Cost ($)</td><td><input type='text' name='DefaultCost' class='text' id='default-cost-field'></td></tr>")
                    .append(
                        $("<tr><td>Category</td></tr>").append($("<td id='cat-select-cell'></td>").append(catSelect))
                    )
                    .append("<tr><td colspan='2' style='text-align: right;'><input type='button' value='Save' id='save-cost-label-submit'/></td></tr>")
            )
        );

    return panel;
}

function resetCostLabelPanel(mode) {
    var callback = mode == NEW_MODE ? saveCostLabel : updateCostLabel;
    $("#new-cost-label-form input[type=text]").val("");
    $("#save-cost-label-submit").click(callback);
    var catSelect = $("<select id='cost-label-category-id' name='CostCategoryID'></select>");
    var cats = [];
    var success = false;
    $.ajax({
        url: "ajax/categories.jsp",
        async: false,
        dataType: "json",
        success: function(data) {
            success = data.success;
            cats = data.categories;
        }
    });

    if (success) {
        for (var i = 0; i < cats.length; i++) {
            var cat = cats[i];
            catSelect.append("<option value='" + cat.id + "'>" + cat.label + "</option>")
        }

        $("#cat-select-cell select").remove();
        $("#cat-select-cell").append(catSelect);
    }
}

function saveCostLabel() {
    var success = false;
    var json = null;
    $.ajax({
        url: "ajax/save_label.jsp",
        data: $("#new-cost-label-form").serialize(),
        async: false,
        dataType: "json",
        success: function(data) {
            success = data.success;
            json = data;
        }
    });

    if (success) {
        $("<tr id='label-" + json.label.id + "' class='label-record label-category-" + json.label.categoryId + "' style='display: none;'><td class='label-name'>" + json.label.label + ' ($' + json.label.defaultCost + ')' +  "</td></tr>")
                .append(
                    $("<td></td>")
                        .append(
                            $('<a href="#" class="edit-label">Edit</a>').click(editCostLabel)
                        )
                        .append(" | ")
                        .append(
                            $('<a href="#" class="delete-label">Delete</a>')
                        )

                )
                .insertAfter($("#side-bar #category-" + json.label.categoryId))
                .fadeIn(FADE_IN_TIME);
    }

    costLabelPanel.fadeOut(FADE_OUT_TIME);
    return false;
}

function editCostLabel() {
    clearContentPane();
    costLabelPanel = createCostLabelPanel(EDIT_MODE);
    $("#content-pane").append(costLabelPanel);
    resetCostLabelPanel(EDIT_MODE);

    var elemId = $(this).parent().parent().attr("id");
    var labelId = elemId.substring(elemId.indexOf("-") + 1);
    var label = loadCostLabel(labelId);

    $("#cost-label-id").val(labelId);
    $("#cost-label-field").val(label.label);
    $("#cost-desc-field").val(label.description);
    $("#default-cost-field").val(label.defaultCost);

    costLabelPanel.fadeIn(FADE_IN_TIME);
    $("#cost-label-field").focus();
}

function updateCostLabel() {
    var json = null;
    var success = false;
    $.ajax({
        url: "ajax/update_label.jsp",
        data: $("#new-cost-label-form").serialize(),
        async: false,
        dataType: "json",
        success: function(data) {
            success = data.success;
            json = data;
        }
    });

    $("#label-" + json.label.id)
            .removeClass("label-category-" + json.oldCategoryId)
            .remove()
            .addClass("label-category-" + json.label.categoryId)
            .insertAfter("#category-" + json.label.categoryId);
    $("#label-" + json.label.id + " td:first-child")
            .text(json.label.label + " ($" + json.label.defaultCost + ")");
    $("#label-" + json.label.id).effect("pulsate", {times : 2}, 400);
    $("#label-" + json.label.id + " .edit-label").click(editCostLabel);

    costLabelPanel.fadeOut(FADE_OUT_TIME);
    return false;
}

function deleteCostLabel() {
    var elemId = $(this).parent().parent().attr("id");
    var id = elemId.substring(elemId.indexOf("-") + 1);
    var success = false;

    $.ajax({
        url: "ajax/delete_label.jsp",
        data: {"CostLabelID" : id},
        async: false,
        dataType: "json",
        success: function(data) {
            success = data.success;
        }
    });

    if(success) {
        $("#label-" + id).remove();
    }

    return false;
}

function loadCostLabel(clId) {
   // var success = false;
    var json = null;
    $.ajax({
        url: "ajax/cost_label.jsp",
        data: {"CostLabelID" : clId},
        async: false,
        dataType: "json",
        success: function(data) {
            json = data;
        }
    });
    return json;
}

function loadCategory(id) {
    // var success = false;
    var json = null;
    $.ajax({
        url: "ajax/category.jsp",
        data: {"CostCategoryID" : id},
        async: false,
        dataType: "json",
        success: function(data) {
            json = data;
        }
    });
    return json;
}

function editCategory() {
    clearContentPane();
    categoryPanel = createCategoryPanel(EDIT_MODE);
    $("#content-pane").append(categoryPanel);
    resetCategoryPanel(EDIT_MODE);

    var elemId = $(this).parent().parent().attr("id");
    var catId = elemId.substring(elemId.indexOf("-") + 1);
    var cat = loadCategory(catId);

    $("#category-name-field").val(cat.label);
    $("#category-desc-field").val(cat.desc);
    $("#cost-category-id-field").val(cat.id);

    categoryPanel.fadeIn(FADE_IN_TIME);
    $("#category-name-field").focus();
}

function updateCategory() {
    var json = null;
    var success = false;
    $.ajax({
        url: "ajax/update_category.jsp",
        data: $("#new-category-form").serialize(),
        async: false,
        dataType: "json",
        success: function(data) {
            success = data.success;
            json = data;
        }
    });

    $("#category-" + json.id + " td:first-child").text(json.label);
    $("#category-" + json.id).effect("pulsate", {times : 2}, 400);

    categoryPanel.fadeOut(FADE_OUT_TIME);
    return false;
}