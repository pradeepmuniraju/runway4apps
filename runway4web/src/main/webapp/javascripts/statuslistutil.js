function StatusObj(name, id)
{
	this.name = name;
	this.id = id;
}

function fillStatusArray(statusSelect,items) {
		
	for (i=0; i < statusSelect.length; i++) {
	   fillStatus(statusSelect[i], items);
	}
}

function fillStatus(statusSelect,items) {
	clearStatus(statusSelect);
		
   for (j=0; j < items.length; j++) {
      statusSelect.options[j] = new Option(items[j].name,items[j].id);
   }
}

function clearStatusArray(statusSelect)
{
   for (i=0; i < statusSelect.length; i++) {
      clearStatus(statusSelect[i]);
	}
}
function clearStatus(statusSelect)
{
   for (j=statusSelect.length-1; j >= 0; j--) {
      statusSelect.options[j] = null
   }
}