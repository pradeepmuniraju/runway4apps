function spawnChild( target, windowname, width, height, showMenu, scroll, resize) {
   if (typeof(width) == "undefined") {
      width = 620;
   }
   if (typeof(height) == "undefined") {
      height = 600;
   }
   if (typeof(showMenu) == "undefined") {
      showMenu = "no";
   }
   if (typeof(scroll) == "undefined") {
      scroll = "yes";
   }
   if (typeof(resize) == "undefined") {
      resize = "yes";
   }
   if (typeof(windowname) != "undefined") {
      while (windowname.indexOf(' ') != -1) {
         windowname = windowname.replace(' ','');
      }
   }
   var helpwinpopup = window.open(target,windowname,'width='+width+', height='+height+', toolbar=no, scrollbars='+scroll+', menubar='+showMenu+', location=no, status=no, resizable='+resize);
	helpwinpopup.moveTo(((screen.width-width)/2),((screen.height-height)/2));
	helpwinpopup.focus();
}
function spawnChild_ex( target, windowname, width, height, showMenu, scroll, resize) {
   if (typeof(width) == "undefined") {
      width = 620;
   }
   if (typeof(height) == "undefined") {
      height = 600;
   }
   if (typeof(showMenu) == "undefined") {
      showMenu = "no";
   }
   var helpwinpopup = window.open(target,windowname,'width='+width+', height='+height+', toolbar=no, scrollbars='+scroll+', menubar='+showMenu+', location=no, status=no, resizable='+resize);
	helpwinpopup.moveTo(((screen.width-width)/2),((screen.height-height)/2));
	helpwinpopup.focus();
}
function spawnChild_ex( target, windowname, width, height, showMenu, scroll, resize) {
	   if (typeof(width) == "undefined") {
	      width = 620;
	   }
	   if (typeof(height) == "undefined") {
	      height = 600;
	   }
	   if (typeof(showMenu) == "undefined") {
	      showMenu = "no";
	   }
	   var helpwinpopup = window.open(target,windowname,'width='+width+', height='+height+', toolbar=no, scrollbars='+scroll+', menubar='+showMenu+', location=no, status=no, resizable='+resize);
		helpwinpopup.moveTo(((screen.width-width)/2),((screen.height-height)/2));
		helpwinpopup.focus();
}
function openInLightBox( target, windowname, width, height, showMenu, scroll, resize) {
	   if (typeof(width) == "undefined") {
	      width = 650;
	   }
	   if (typeof(height) == "undefined") {
	      height = 300;
	   }
	   if (typeof(showMenu) == "undefined") {
	      showMenu = "no";
	   }
	   if (typeof(scroll) == "undefined") {
	      scroll = "yes";
	   } 
	   if (typeof(resize) == "undefined") {
	      resize = "yes";
	   }
	   if (typeof(windowname) != "undefined") {
	      while (windowname.indexOf(' ') != -1) {
	         windowname = windowname.replace(' ','');
	      }
	   }

		var lightboxwin = new IFrameWindow(windowname);
		lightboxwin.frameid = 'ifrm';
		lightboxwin.width = width;
		lightboxwin.height = height;
		lightboxwin.winShadeOpacity = 60;
		lightboxwin.src = target;
		if(lightboxwin.src.indexOf("?")>0)
		{
			lightboxwin.src = lightboxwin.src + "&-id=" + windowname;
		}
		else
		{
			lightboxwin.src = lightboxwin.src + "?-id=" + windowname;
		}
		lightboxwin.create();
}
function refreshParentAndClose() {
   //window.opener.history.go(-1);
	window.opener.location.reload();
	window.opener.focus();
	window.close();
}

function spawnChildAndGoBack(pageurl) {
 //,status=no,menubar=no,toolbar=no
	var helpwinpopup = window.open(pageurl,'Assign','width=620,height=600,scrollbars=yes,scrollable=yes,resizable=yes');
	helpwinpopup.moveTo(((screen.width/2)-310),((screen.height/2)-300));
	
   history.go(-1);
   
	helpwinpopup.focus();
	
}