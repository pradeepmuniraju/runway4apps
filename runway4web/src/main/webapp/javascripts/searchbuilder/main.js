/**
 * @author Ulpian Cesana
 */

var fadeSpeed = "slow";
var contextPath = "";
var searchStatementId = "";

function init(contextPathArg, searchStatementIdArg) {
    contextPath = contextPathArg;
    searchStatementId = searchStatementIdArg;
    
    $("#SearchType").change(selectSearchType);
    hideButtons();
    setupEventHandlers();
}

function selectSearchType(event) {
    var searchTypeInput = $(event.target);
    var searchTypeValue = searchTypeInput.val();
    var searchName = $("input#SearchName").val();
    var searchDescription = $("input#SearchDescription").val();
    
    if(searchTypeValue && searchTypeValue.length > 0) {
        document.searchBuilderForm.submit();
        
        /*
        $.ajax({
            type: "POST",
            url: "searchbuilder.ajax.createsearchstatement",
            data: {
                    "-SearchName": searchName, 
                    "-SearchDescription": searchDescription, 
                    "-SearchType": searchTypeValue, 
                    "-SearchSetOperator": "Union"
            },
            success: function(resp) {
                var respHtml = $(resp);
                var searchSetID = getSearchSetId(respHtml);
                
                var searchSetDeleteButtonID = "SearchSetDeleteButton-" + searchSetID;
                
                respHtml.find("#" + searchSetDeleteButtonID).click(function(event) {
                    var searchSet = $("div#SearchSetID-" + searchSetID);
                    searchSet.fadeOut(fadeSpeed).remove();
                    deleteSearchSet(searchSetID);
                });
                
                fadeAppend(respHtml, $("div#SearchSets"));
                showButtons();
                setupEventHandlers();
            }
        });
        */
    }
}

function addSearchSet() {
    $.ajax({
        type: "POST",
        url: "searchbuilder.ajax.appendsearchset",
        data: {
                "-SearchStatementID": searchStatementId, 
                "-SearchSetOperator": "Union"
        },
        success: function(resp) {
            var respHtml = $(resp);
            
            var searchSetID = getSearchSetId(respHtml);
            var searchSetDeleteButtonID = "SearchSetDeleteButton-" + searchSetID;
            
            respHtml.find("div#" + searchSetDeleteButtonID).click(function(event) {
                var searchSet = $("div#SearchSetID-" + searchSetID);
                searchSet.fadeOut(fadeSpeed).remove();
                deleteSearchSet(searchSetID);
            });
            fadeAppend(respHtml, $("div#SearchSets"));
            setupEventHandlers();
        }
    });
}

function subtractSearchSet() {
  
}

function deleteSearchSet(searchSetID) {
  
}

function selectFieldType(event) {
    var $target = $(event.target);

    var fieldTypeID = $target.parent().attr("id");
    var fieldTypeVal = $target.val();
  
    var fieldNameID = fieldTypeID.replace("FieldType", "FieldName");
    var fieldValueID = fieldTypeID.replace("FieldType", "FieldValue");
  
    $.ajax({
        type: "POST",
        url: "searchbuilder.ajax.selectfieldtype",
        data: {
                "-SearchStatementID": searchStatementId,        
                "-FieldTypeID": fieldTypeID, 
                "-FieldTypeVal": fieldTypeVal
        },
        success: function(resp) {
            var respHtml = $(resp);
            var $fieldNameElm = $("div#" + fieldNameID);
            fadeRemove($fieldNameElm.children())
            fadeRemove($("div#" + fieldValueID).children());
            fadeAppend(respHtml, $fieldNameElm);
            setupEventHandlers();
        }
    });
}

function selectFieldName(event) {
    var $target = $(event.target);

    var fieldNameID = $target.parent().attr("id");
    var selectedValue = $target.val();
  
    var fieldValueID = fieldNameID.replace("FieldName", "FieldValue");  
  
    $.ajax({
        type: "POST",
        url: "searchbuilder.ajax.selectfieldname",
        data: {
                "-SearchStatementID": searchStatementId,
                "-FieldNameID": fieldNameID, 
                "-SelectedValue": selectedValue
        },
        success: function(resp) {
            var respHtml = $(resp);
            var $fieldValueElm = $("div#" + fieldValueID);
            fadeRemove($fieldValueElm.children());
            fadeAppend(respHtml, $fieldValueElm);
            setupEventHandlers();
        }
    });
}

function selectFieldValue(event) {
    var $target = $(event.target);

    var addButtonID = $target.parent().attr("id");
    
    var fieldValueID = addButtonID.replace("SearchCriteriaAddButton", "FieldValue");
    var selectedValue = $("div#" + fieldValueID + " select, div#" + fieldValueID + " input").val();
    
    $.ajax({
        type: "POST",
        url: "searchbuilder.ajax.selectfieldvalue",
        data: {
                "-SearchStatementID": searchStatementId,
                "-FieldValueID": fieldValueID, 
                "-SelectedValue": selectedValue
        },
        success: function(resp) {
            var respHtml = $(resp);
            
            var searchSetID = getSearchSetId(respHtml);
            var searchSetDeleteButtonID = "SearchSetDeleteButton-" + searchSetID;
            
            respHtml.find("div#" + searchSetDeleteButtonID).click(function(event) {
                var searchSet = $("div#SearchSetID-" + searchSetID);
                searchSet.fadeOut(fadeSpeed).remove();
                deleteSearchSet(searchSetID);
            });
            fadeAppend(respHtml, $("div#SearchSets"));
            setupEventHandlers();            
        }
    });
}

function resetSearchStatement() {
}

function getSearchSetId($searchSetDiv) {
    var searchSetId = $searchSetDiv.attr("id").replace("SearchSetID-", "");
    
    return searchSetId;
}

function hideButtons() {
    getSearchStatementButtons().hide();
}

function showButtons() {
    getSearchStatementButtons().fadeIn(fadeSpeed);
}

function getSearchStatementButtons() {
    return $("#resetSearchStatementButton", "#createSearchStatementButton");
}

function setupEventHandlers() {
    $("#SearchSetAddButton").click(addSearchSet);
  
    $("div.FieldType select").change(selectFieldType);
  
    $("div.FieldName select").change(selectFieldName);
  
    $("div.add-icon").click(selectFieldValue);
}

function fadeAppend($element, $target) {
    $element.appendTo($target).hide().fadeIn(fadeSpeed);
}

function fadeRemove($element) {
    $element.fadeOut(fadeSpeed).remove();
}