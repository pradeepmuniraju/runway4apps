$(function() {
	var $f = $('form'), 
		cancel = $f.find('input[name=cancel]'),
		submit = $f.find('input[name=submit]'),
		redirect = $f.find('#redirect_uri');
	
	cancel.on('click', function() {
		var url = redirect.val();
		if(url.indexOf('?')>-1) {
			url += '&message=cancelled';
		} else {
			url += '?message=cancelled';
		}
		window.location.href = url;
	});
})