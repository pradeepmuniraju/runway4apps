function process(key,value) {
	//console.log(typeof(value) + " " + value);
	if (value.toString().indexOf("function") == 0) {
		try {
			return eval("(" + value.toString() + ")");
		} catch (e) {
			return value.toString();
		}
	} else if (value.toString().indexOf("Highcharts") == 0) {
		try {
			return eval("(" + value.toString() + ")");
		} catch (e) {
			return value.toString();
		}
	} else if (value.toString().indexOf("Date") == 0) {
		try {
			return eval("(" + value.toString() + ")");
		} catch (e) {
			return value.toString();
		}
	}
	
	return value;
}

function json2highcharts(o,func) {
	if (typeof func == 'undefined') func = process;
    for (i in o) {
    	o[i] = func.apply(this,[i,o[i]]);      
        if (typeof(o[i])=="object") {
                //going on step down in the object tree!!
        	json2highcharts(o[i],func);
        }
    }
    return o;
}