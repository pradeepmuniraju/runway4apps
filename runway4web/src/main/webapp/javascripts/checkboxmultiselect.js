/**
 * 
 * checkboxmultiselect.js 
 * 
 * There are two ways to use this object:
 * 1. Perform checkbox multi-select operations to all checkboxes in a form with 
 *    a specific input name by supplying the form name and the checkbox input name 
 *    when invoking the selectRange, selectInverse, selectAll, and selectNone functions.
 * 2. Perform checkbox multi-select operations to a set of checkbox DOM objects by 
 *    adding the checkbox DOM objects using the addCheckbox or addAllCheckboxes methods,
 *    and invoking the same select functions without supplying any arguments. 
 *    
 */

/**
 * @param formCheckboxes: 
 */
function CheckboxMultiSelect(checkBoxes) {
	this.checkBoxes = new Array();	
	
	if(checkBoxes) {
		this.addAllCheckboxes(checkBoxes);
	}
		
	this.addAllCheckboxes = function (checkBoxes) {
		for(var i=0; i < checkBoxes.length; i++) {
			this.addCheckbox(checkBoxes[i]);
		}
	};

	this.addCheckbox = function (checkBox) {
		this.checkBoxes.push(checkBox);
	};
	
	this.removeAllCheckboxes = function () {
		this.checkBoxes.clear();
	};	
		
	this.getCheckboxes = function (formName, inputName) {
		if(formName && inputName) {
	      var formObj = document.forms[formName];
	      return formObj.elements[inputName];
		} else {
		  return this.checkBoxes;
		}
	};
	
	this.selectRange = function (formName, inputName) {
	   var checkBoxes = this.getCheckboxes(formName, inputName);
	   var flip = false;
	   for(var i=0; i<checkBoxes.length; i++)
	   {
	      if(checkBoxes[i].checked)
	      {
	         if(flip)
	         {
	            break;
	         }
	         else
	         {
	            flip = true;
	         }
	      }

	      if(flip)
	      {
	         checkBoxes[i].checked = true;
	      }
	   }
	};
	
	this.selectRange = function (formName, inputName) {
	   var checkBoxes = this.getCheckboxes(formName, inputName);
   
	   var flip = false;
	   for(var i=0; i<checkBoxes.length; i++)
	   {
	      if(checkBoxes[i].checked)
	      {
	         if(flip)
	         {
	            break;
	         }
	         else
	         {
	            flip = true;
	         }
	      }

	      if(flip)
	      {
	         checkBoxes[i].checked = true;
	      }
	   }
	};
	
	this.selectAll = function (formName, inputName)
	{
	   var checkBoxes = this.getCheckboxes(formName, inputName);	   
	   if(checkBoxes) {
		   if(checkBoxes.length == undefined) 
		   {
			 // There is only one input element
		     checkBoxes.checked = true;
		   } 
		   else 
		   {
			   for(var i=0; i<checkBoxes.length; i++)
			   {
			      checkBoxes[i].checked = true;
			   }		   
		   }
	   }
	};

	this.selectNone = function (formName, inputName)
	{
	   var checkBoxes = this.getCheckboxes(formName, inputName);
	   if(checkBoxes) {
		   if(checkBoxes.length == undefined) 
		   {
			 // There is only one input element
		     checkBoxes.checked = false;
		   } 
		   else 
		   {
			   for(var i=0; i<checkBoxes.length; i++)
			   {
			      checkBoxes[i].checked = false;
			   }		   
		   }
	   }
	};

	this.selectInverse = function (formName, inputName)
	{
	   var checkBoxes = this.getCheckboxes(formName, inputName);	   
	   if(checkBoxes) {
		   if(checkBoxes.length == undefined) 
		   {
			 // There is only one input element
			   checkBoxes.checked = !checkBoxes.checked;
		   } 
		   else 
		   {
			   for(var i=0; i<checkBoxes.length; i++)
			   {
				   checkBoxes[i].checked = !checkBoxes[i].checked;
			   }		   
		   }
	   }   
	};
}



