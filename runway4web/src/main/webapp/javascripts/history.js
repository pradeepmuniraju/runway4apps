

function hideHistory(question, profile) {
	var div = document.getElementById(question + ":" + profile);
	if (typeof(div) != 'undefined' && div != null) {
		div.style.display = "none";
	}
}

function showHistory(key, id, question, profile, path) {
	var div = document.getElementById(question + ":" + profile);
	if (typeof(div) != 'undefined' && div != null) {
		div.innerHTML = "<img src=\"" + path + "/images/loading/loading_large.gif\" />"
		div.style.display = "";
		var args = path + "/crm/actions/questionhistory.jsp?" + key + "=" + id + "&-questionID=" + question + "&-profileID=" + profile;
		$.post(args,{},function(data){
			data = data.replace(/^\s*|\s*$/g,'');
			if (data.length > 0) {
				var lines = data.split("\n");
				if (lines.length > 0) {
					var div = document.getElementById(lines[0]);
					var cIn = document.getElementById("img-" + lines[0]);
					if (typeof(div) != 'undefined' && div != null && typeof(cIn) != 'undefined' && cIn != null) {
						div.style.top = cIn.style.top + "px";
						div.style.left = cIn.style.left + "px";
						var text = data.substring(lines[0].length);
						div.innerHTML = text;
					}
				}
			}
		});
	}
}