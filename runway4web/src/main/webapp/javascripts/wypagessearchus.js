function usStates(name, code)
{
	this.name = name;
	this.code = code;
}

var statecodes = new Array(
		new usStates("Alabama","AL"),
		new usStates("Alaska","AK"),
		new usStates("Arizona","AZ"),
		new usStates("Arkansas","AR"),
		new usStates("California","CA"),
		new usStates("Colorado","CO"),
		new usStates("Connecticut","CT"),
		new usStates("District Of Columbia","DC"),
		new usStates("Delaware","DE"),
		new usStates("Florida","FL"),
		new usStates("Georgia","GA"),
		new usStates("Hawaii","HI"),
		new usStates("Idaho","ID"),
		new usStates("Illinois","IL"),
		new usStates("Indiana","IN"),
		new usStates("Iowa","IA"),
		new usStates("Kansas","KS"),
		new usStates("Kentucky","KY"),
		new usStates("Louisiana","LA"),
		new usStates("Maine","ME"),
		new usStates("Maryland","MD"),
		new usStates("Massachusetts","MA"),
		new usStates("Michigan","MI"),
		new usStates("Minnesota","MN"),
		new usStates("Mississippi","MS"),
		new usStates("Missouri","MO"),
		new usStates("Montana","MT"),
		new usStates("Nebraska","NE"),
		new usStates("Nevada","NV"),
		new usStates("New Hampshire","NH"),
		new usStates("New Jersey","NJ"),
		new usStates("New Mexico","NM"),
		new usStates("New York","NY"),
		new usStates("North Carolina","NC"),
		new usStates("North Dakota","ND"),
		new usStates("Ohio","OH"),
		new usStates("Oklahoma","OK"),
		new usStates("Oregon","OR"),
		new usStates("Pennsylvania","PA"),
		new usStates("Rhode Island","RI"),
		new usStates("South Carolina","SC"),
		new usStates("South Dakota","SD"),
		new usStates("Tennessee","TN"),
		new usStates("Texas","TX"),
		new usStates("Utah","UT"),
		new usStates("Vermont","VT"),
		new usStates("Virginia","VA"),
		new usStates("Washington","WA"),
		new usStates("West Virginia","WV"),
		new usStates("Wisconsin","WI"),
		new usStates("Wyoming","WY")
	);

var ypstatecodes = new Array(
		new usStates("1", "Alabama"),
		new usStates("2", "Alaska"),
		new usStates("4", "Arizona"),
		new usStates("5", "Arkansas"),
		new usStates("6", "California"),
		new usStates("8", "Colorado"),
		new usStates("9", "Connecticut"),
		new usStates("10", "Delaware"),
		new usStates("11", "District Of Columbia"),
		new usStates("12", "Florida"),
		new usStates("13", "Georgia"),
		new usStates("15", "Hawaii"),
		new usStates("16", "Idaho"),
		new usStates("17", "Illinois"),
		new usStates("18", "Indiana"),
		new usStates("19", "Iowa"),
		new usStates("20", "Kansas"),
		new usStates("21", "Kentucky"),
		new usStates("22", "Louisiana"),
		new usStates("23", "Maine"),
		new usStates("24", "Maryland"),
		new usStates("25", "Massachusetts"),
		new usStates("26", "Michigan"),
		new usStates("27", "Minnesota"),
		new usStates("28", "Mississippi"),
		new usStates("29", "Missouri"),
		new usStates("30", "Montana"),
		new usStates("31", "Nebraska"),
		new usStates("32", "Nevada"),
		new usStates("33", "New Hampshire"),
		new usStates("34", "New Jersey"),
		new usStates("35", "New Mexico"),
		new usStates("36", "New York"),
		new usStates("37", "North Carolina"),
		new usStates("38", "North Dakota"),
		new usStates("39", "Ohio"),
		new usStates("40", "Oklahoma"),
		new usStates("41", "Oregon"),
		new usStates("42", "Pennsylvania"),
		new usStates("44", "Rhode Island"),
		new usStates("45", "South Carolina"),
		new usStates("46", "South Dakota"),
		new usStates("47", "Tennessee"),
		new usStates("48", "Texas"),
		new usStates("49", "Utah"),
		new usStates("50", "Vermont"),
		new usStates("51", "Virginia"),
		new usStates("53", "Washington"),
		new usStates("54", "West Virginia"),
		new usStates("55", "Wisconsin"),
		new usStates("56", "Wyoming")
	);

function getStreet(street1, street2)
{
	var tempst = street1;

	if(street2.length!=0)
	{
		tempst = street2;
	}
	if(tempst.indexOf(',')>=0)
	{
		tempst = tempst.split(",")[1];
	}
	tempst = trim(tempst);

	return(tempst);
}

function getStateCode(statename)
{
	for(var i=0; i<ypstatecodes.length; i++)
	{
		if(ypstatecodes[i].name == statename)
		{
			return(ypstatecodes[i].code);
		}
	}
	return("");
}

function getStateName(code)
{
	for(var i=0; i<statecodes.length; i++)
	{
		if(statecodes[i].code == code)
		{
			return(statecodes[i].statename);
		}
	}
	return("");
}

function getYPStateCode(statename)
{
	for(var i=0; i<ypstatecodes.length; i++)
	{
		if(ypstatecodes[i].name == statename)
		{
			return(ypstatecodes[i].code);
		}
	}
	return("");
}

function trim(word)
{
	var returnvalue = word;
	if(returnvalue.charAt(0)==" ")
	{
		returnvalue = returnvalue.substr(1,returnvalue.length);
	}
	if(returnvalue.charAt(returnvalue.length-1)==" ")
	{
		returnvalue = returnvalue.substr(0,returnvalue.length-1);
	}
	if(returnvalue.charAt(0)==" " || returnvalue.charAt(returnvalue.length-1)==" ")
	{
		return(trim(returnvalue));
	}
	return(returnvalue);
}

function whitepagesSearch(company, city, state, postcode)
{
	wpform = document.whitepagessearchform;
	wpform.elements['name'].value = trim(company);
 	wpform.elements['state_id'].value = trim(state);
	wpform.elements['city_zip_npa'].value = trim(city);
	wpform.submit();
}

function whitepagesContactSearch(lastname, city, state, postcode)
{
	wpform = document.whitepagescontactsearchform;
	wpform.elements['name'].value = trim(lastname);
 	wpform.elements['state_id'].value = trim(state);
	wpform.elements['city_zip'].value = trim(city);
	wpform.submit();
}

function yellowpagesSearch(company, city, state, postcode)
{
	ypform = document.yellowpagesform;
	ypform.elements['N'].value = trim(company);
 	ypform.elements['S'].value = getYPStateCode(getStateName(trim(state)));
	ypform.elements['CI'].value = city;
	ypform.submit();
}

function googleSearch(searchterm)
{
	ypform = document.googleform;
	ypform.elements['q'].value = trim(searchterm);
	ypform.submit();
}

function streetdirectorySearch(street1, street2, city, state, postcode)
{
	var streetvalues = getStreet(street1, street2);

	sdform = document.streetdirectoryform;
	sdform.elements['address'].value = getStreet(street1, street2);
	sdform.elements['city'].value = trim(city);;
	sdform.elements['zipcode'].value = trim(postcode);
	sdform.elements['state'].value = getStateCode(trim(state));
	sdform.submit();
}

