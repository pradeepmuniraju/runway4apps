var findQuestions = new Object();

findQuestions.addQuestionToSelectedProfile = function(questionID, duplicate) {
	// get the current Profile ID
	var profileID = $("#findreportsform input[name='-dashboardid']").attr("value");

	//window.alert("Profile ID = " + profileID + " Question ID = " + questionID);
	
	if (profileID == "" || profileID == "undefined") {
		$.post("./ajax/getvirtualprofile.jsp", {}, function(data){
			var profileID = data.replace(/^\s*|\s*$/g,'');
			
			// make sure it is empty 'SurveyQuestions','ON-SurveyID'
			var URLString = "../reportgenerator/ajax/deleteAllDashboardItems.jsp?ON-SurveyID=" + profileID + "&-tableSpec=SurveyQuestions";
			var defresponse = new DefaultResponse(
				function(obj) {				
									
				}
			);
			var xmlreq = new XmlRequest(defresponse, profileID);
			xmlreq.send(URLString);
			
			// add the questions to the virtual
			$.post("./actions/addquestiontoprofile.jsp", {ProfileID: profileID, QuestionID: questionID, selectedReportsHasDashboard: selectedReportsHasDashboard(), duplicate : duplicate}, function(data){
				$("#selectedreportspanel").empty();
				$.post("./ajax/useprofile.jsp", {profile: profileID, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports()}, function(data){
					  if( selectedReportsHasDashboard() == "false" ) {
						  $("#selectedreportspanel").append(data);		  
					  }
					  else {
						  $("#beginselectedreports").append(data);
					  }
					
					  // set the current dashboardID on the hidden form
					  $("#findreportsform input[name='-dashboardid']").attr("value", profileID);
					  
					  disableQuestion(questionID,false);
					  updateInstructionPanelVisability();
					});
			});
		});
	} else {		
		// add the questions to the virtual
		$.post("./actions/addquestiontoprofile.jsp", {ProfileID: profileID, QuestionID: questionID, selectedReportsHasDashboard: selectedReportsHasDashboard(), duplicate : duplicate}, function(data){
			$("#selectedreportspanel").empty();
			$.post("./ajax/useprofile.jsp", {profile: profileID, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports()}, function(data){
				  if( selectedReportsHasDashboard() == "false" ) {
					  $("#selectedreportspanel").append(data);		  
				  }
				  else {
					  $("#beginselectedreports").append(data);
				  }

				  // set the current dashboardID on the hidden form
				  $("#findreportsform input[name='-dashboardid']").attr("value", profileID);
				  
				  disableQuestion(questionID,false);
				  updateInstructionPanelVisability();
				});
		});
	}
};

//findQuestions.addDuplicateQuestionToSelectedProfile = function(questionID) {
//	// get the current Profile ID
//	var profileID = $("#findreportsform input[name='-dashboardid']").attr("value");
//
//	//window.alert("Profile ID = " + profileID + " Question ID = " + questionID);
//	
//	if (profileID == "" || profileID == "undefined") {
//		$.post("./ajax/getvirtualprofile.jsp", {}, function(data){
//			var profileID = data.replace(/^\s*|\s*$/g,'');
//			
//			document.location = "./actions/addquestiontoprofile.jsp?-ProfileID=" + profileID + "&-QuestionID=" + questionID + "&-selectedReportsHasDashboard=false&duplicate=true";
//		});
//	} else {		
//		document.location = "./actions/addquestiontoprofile.jsp?-ProfileID=" + profileID + "&-QuestionID=" + questionID + "&-selectedReportsHasDashboard=true&duplicate=true";
//	}
//}
	
findQuestions.getSelectedProfileID = function() {
	//pluck the current profileID from the hidden form in findquestions.jsp 
	return $("#findreportsform input[name='-dashboardid']").attr("value");
};


/* Select a question from the search results */
function useQuestion(questionName,questionID,contextpath,newreport,profileID)
{
	if (profileID == null || profileID == "") {
	// get the current Profile ID
		profileID = $("#findreportsform input[name='-dashboardid']").attr("value");
	}

	// Prevent the user from selecting the report a second time
	
	//alert(questionName + " " + questionID);

	var prepareHide = "tr[name='tablerow" + questionID + "']"
	var $toHide = $(prepareHide);
	for(p=0; p < $toHide.length; p++)
	{
		$toHide[p].getElementsByTagName("li")[1].style.visibility = "hidden";
		$toHide[p].getElementsByTagName("li")[1].style.display = "none";
	}

	//alert("ProfileID " + profileID);
	
	// add question to profile
	if (profileID == "" || profileID == "undefined") {
//		$.post("./ajax/getvirtualprofile.jsp", {}, function(data){
//			var profileID = data.replace(/^\s*|\s*$/g,'');
//			
//			// make sure it is empty 'SurveyQuestions','ON-SurveyID'
//			var URLString = "../reportgenerator/ajax/deleteAllDashboardItems.jsp?ON-SurveyID=" + profileID + "&-tableSpec=SurveyQuestions";
//			var defresponse = new DefaultResponse(
//				function(obj) {				
//									
//				}
//			);
//			var xmlreq = new XmlRequest(defresponse, profileID);
//			xmlreq.send(URLString);
			
//			// add the questions to the virtual
//			$.post("./actions/addquestiontoprofile.jsp", {ProfileID: profileID, QuestionID: questionID, selectedReportsHasDashboard: selectedReportsHasDashboard(), duplicate : false}, function(data){
//				$("#selectedreportspanel").empty();
				var useform = document.getElementById("viewquestion");
				useform.elements[0].value = questionID;
				useform.elements[1].value = selectedReportsHasDashboard();		
				useform.elements[2].value = "";
				useform.submit();			
//			});
//		});
	} else {		
		// add the questions to the virtual
		$.post("./actions/addquestiontoprofile.jsp", {ProfileID: profileID, QuestionID: questionID, selectedReportsHasDashboard: selectedReportsHasDashboard(), duplicate : false}, function(data){
			$("#selectedreportspanel").empty();
			var useform = document.getElementById("viewquestion");
			useform.elements[0].value = questionID;
			useform.elements[1].value = "false";		
			useform.elements[2].value = profileID;
			useform.submit();
		});
	}
}

function useProfile(profileID, contextpath, tablespec, tablekey, beanidhighlight)
{		
		// find out if there is a current profile selected 
		var currentProfile = $("#findreportsform input[name='-dashboardid']").attr("value");

		//need to add checking if this has been modified since selected
		var hasBeenModified = false;

		if (hasBeenModified) {
			// need to alert the use that it has been modified and ask to save or discard
		} else {
			// if there is a profile there, then lets get rid of it
			if (currentProfile != "") {
				$("#selectedreportspanel").empty();
			}
			//$("tr[id='dashboardidentifier" + profileID + "'] ul.sf_menu li:eq(1) ul.useDashboardAction").css( {display:'none', visibility:'hidden'} ); 
			$.post("./ajax/useprofile.jsp", {profile: profileID, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports()}, function(data){
				  if( selectedReportsHasDashboard() == "false" ) {
					  $("#selectedreportspanel").append(data);		  
				  }
				  else {
					  $("#beginselectedreports").append(data);
				  }

				 //$allReports = null;
				
				  if(beanidhighlight)
				  {
					  $("li[name='" + beanidhighlight + "']").css( {'background-color':'#FFFFCC'} ); 
				  }
				  
				  // set the current dashboardID on the hidden form
				  $("#findreportsform input[name='-dashboardid']").attr("value", profileID);
				  $("#quickview input[name='ProfileID']").attr("value", profileID);
				  $("#quickviewresults input[name='ProfileID']").attr("value", profileID);
				  
				  disableQuestion(profileID,false);
				  updateInstructionPanelVisability();
				});
			
		}

}

function selectProfile(profileID,contextpath, tablespec, tablekey, beanidhighlight)
{
	$("#findreportsform input[name='-dashboardid']").attr("value","");
	appendProfile(profileID,contextpath, tablespec, tablekey, beanidhighlight);
}

function appendProfile(profileID,contextpath, tablespec, tablekey, beanidhighlight)
{		
	// find out if there is a current profile selected 
	var currentProfile = $("#findreportsform input[name='-dashboardid']").attr("value");
	
	//need to add checking if this has been modified since selected
	var hasBeenModified = false;

	if (hasBeenModified) {
		// need to alert the use that it has been modified and ask to save or discard
	} else {
		// if there is no profile then we will make a new and then add by reference
		if (currentProfile == "") {
			$("#selectedreportspanel").empty();
			$("#beginselectedreports").empty();
			// get a virtual profile
			$.post("./ajax/getvirtualprofile.jsp", {}, function(data){
				var newID = data.replace(/^\s*|\s*$/g,'');
				
				// make sure it is empty
				var URLString = "../reportgenerator/ajax/deleteAllDashboardItems.jsp?" + tablekey + "=" + newID + "&-tableSpec=" + tablespec;
				var defresponse = new DefaultResponse(
					function(obj) {				
										
					}
				);
				var xmlreq = new XmlRequest(defresponse, newID);
				xmlreq.send(URLString);

				// add the questions to the virtual
				$.post("./ajax/duplicateprofile.jsp", {ProfileID: profileID, currentDashBoardID: newID, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports(), duplicate : "false"}, function(data){
					  
					$.post("./ajax/useprofile.jsp", {profile: newID, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports()}, function(data){
						  if( selectedReportsHasDashboard() == "false" ) {
							  $("#selectedreportspanel").append(data);		  
						  }
						  else {
							  $("#beginselectedreports").append(data);
						  }
						
						  if(beanidhighlight)
						  {
							  $("li[name='" + beanidhighlight + "']").css( {'background-color':'#FFFFCC'} ); 
						  }
						  
						  // set the current dashboardID on the hidden form
						  $("#findreportsform input[name='-dashboardid']").attr("value", newID);
						  
						  
						  disableQuestion(profileID,false);
						  updateInstructionPanelVisability();
						});
				});
			});
		} else {
			// else we just add the questions to the current profile by refernce
			$("#selectedreportspanel").empty();
			$("#beginselectedreports").empty();

			// add the questions to the virtual
			$.post("./ajax/duplicateprofile.jsp", {ProfileID: profileID, currentDashBoardID: currentProfile, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports(), duplicate : "false"}, function(data){
				  
				$.post("./ajax/useprofile.jsp", {profile: currentProfile, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports()}, function(data){
					  if( selectedReportsHasDashboard() == "false" ) {
						  $("#selectedreportspanel").append(data);		  
					  }
					  else {
						  $("#beginselectedreports").append(data);
					  }
					
					  if(beanidhighlight)
					  {
						  $("li[name='" + beanidhighlight + "']").css( {'background-color':'#FFFFCC'} ); 
					  }
					  
					  // set the current dashboardID on the hidden form
					  $("#findreportsform input[name='-dashboardid']").attr("value", currentProfile);
					  
					  
					  disableQuestion(profileID,false);
					  updateInstructionPanelVisability();
					});
			});
		}
	}
}

function duplicateProfile(profileID,contextpath, tablespec, tablekey, beanidhighlight)
{		
	// find out if there is a current profile selected 
	var currentProfile = $("#findreportsform input[name='-dashboardid']").attr("value");
	
	//need to add checking if this has been modified since selected
	var hasBeenModified = false;

	if (hasBeenModified) {
		// need to alert the use that it has been modified and ask to save or discard
	} else {
		// if there is a profile there, then lets get rid of it
		if (currentProfile != "") {
			$("#selectedreportspanel").empty();
			$("#beginselectedreports").empty();
		}

		// get a virtual profile
		$.post("./ajax/getvirtualprofile.jsp", {}, function(data){
			var newID = data.replace(/^\s*|\s*$/g,'');
			
			// make sure it is empty
			var URLString = "../reportgenerator/ajax/deleteAllDashboardItems.jsp?" + tablekey + "=" + newID + "&-tableSpec=" + tablespec;
			var defresponse = new DefaultResponse(
				function(obj) {				
									
				}
			);
			var xmlreq = new XmlRequest(defresponse, newID);
			xmlreq.send(URLString);

			// add the questions to the virtual
			$.post("./ajax/duplicateprofile.jsp", {ProfileID: profileID, currentDashBoardID: newID, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports(), duplicate : "true"}, function(data){
				  
				$.post("./ajax/useprofile.jsp", {profile: newID, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports()}, function(data){
					  if( selectedReportsHasDashboard() == "false" ) {
						  $("#selectedreportspanel").append(data);		  
					  }
					  else {
						  $("#beginselectedreports").append(data);
					  }
					
					  if(beanidhighlight)
					  {
						  $("li[name='" + beanidhighlight + "']").css( {'background-color':'#FFFFCC'} ); 
					  }
					  
					  // set the current dashboardID on the hidden form
					  $("#findreportsform input[name='-dashboardid']").attr("value", newID);
					  
					  
					  disableQuestion(profileID,false);
					  updateInstructionPanelVisability();
					});
			});
		});
	}
}

function duplicateToProfile(profileID,contextpath, tablespec, tablekey, beanidhighlight)
{		
	// find out if there is a current profile selected 
	var currentProfile = $("#findreportsform input[name='-dashboardid']").attr("value");
	
	if (currentProfile == null || currentProfile == "") {
		duplicateProfile(profileID,contextpath, tablespec, tablekey, beanidhighlight);
		return;
	}
	
	//need to add checking if this has been modified since selected
	var hasBeenModified = false;

	if (hasBeenModified) {
		// need to alert the use that it has been modified and ask to save or discard
	} else {
		// if there is a profile there, then lets get rid of it
		if (currentProfile != "") {
			$("#selectedreportspanel").empty();
			$("#beginselectedreports").empty();
		}

		// get a virtual profile
//		$.post("./ajax/getvirtualprofile.jsp", {}, function(data){
//			var newID = data.replace(/^\s*|\s*$/g,'');
			
			// make sure it is empty
//			var URLString = "../reportgenerator/ajax/deleteAllDashboardItems.jsp?" + tablekey + "=" + newID + "&-tableSpec=" + tablespec;
//			var defresponse = new DefaultResponse(
//				function(obj) {				
//									
//				}
//			);
//			var xmlreq = new XmlRequest(defresponse, newID);
//			xmlreq.send(URLString);

			// add the questions to the virtual
			$.post("./ajax/duplicateprofile.jsp", {ProfileID: profileID, currentDashBoardID: currentProfile, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports(), duplicate : "true"}, function(data){
				  
				$.post("./ajax/useprofile.jsp", {profile: currentProfile, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports()}, function(data){
					  if( selectedReportsHasDashboard() == "false" ) {
						  $("#selectedreportspanel").append(data);		  
					  }
					  else {
						  $("#beginselectedreports").append(data);
					  }
					
					  if(beanidhighlight)
					  {
						  $("li[name='" + beanidhighlight + "']").css( {'background-color':'#FFFFCC'} ); 
					  }
					  
					  // set the current dashboardID on the hidden form
					  $("#findreportsform input[name='-dashboardid']").attr("value", newID);
					  
					  
					  disableQuestion(profileID,false);
					  updateInstructionPanelVisability();
					});
			});
//		});
	}
}


function duplicateQuestion(questionID,contextpath)
{
	if(selectedReportsHasDashboard() == "false")
	{

		if (numberOfReports() == 0)
		{			
			$.get("./ajax/addprofile.jsp", {}, function(data){
					$("#selectedreportspanel").append(data);
			});
		}
		
		$.post("./ajax/getvirtualprofile.jsp", {}, function(data){
				var profileid = data.replace(/^\s*|\s*$/g,'');
							
				$.post("./ajax/duplicatequestion.jsp", {questionID: questionID, profileID: profileid, numberOfReports: numberOfReports(), selectedReportsHasDashboard: selectedReportsHasDashboard()}, function(data){
						//moveLastSelectedReportUp(contextpath);	
						$("#beginselectedreports").append(data);
												
						  disableQuestion(questionID,false);
				});
		});
	}
	else
	{		
		var currentDashboard = document.getElementById("selectedreportspanel").getElementsByTagName("p")[0];
		var profileid= currentDashboard.getAttribute("id").substring(9);
		
		$.post("./ajax/duplicatequestion.jsp", {questionID: questionID, numberOfReports: numberOfReports(), profileID: profileid, selectedReportsHasDashboard: selectedReportsHasDashboard()}, function(data){
			  	//moveLastSelectedReportUp(contextpath);	
				$("#beginselectedreports").append(data);
			
				  disableQuestion(questionID,false);
		});	
		
	}
}

function saveAsNewProfile(tablespec,tablekey)
{
		var profileid = $("#findreportsform input[name='-dashboardid']").attr("value");
		if (profileid != "") {
		    var url = './popup/editprofile.jsp?-profileID=' + profileid
			openInLightBox(url, 'EditProfile', 1000, 300, "no", "no");  			  			
		}
//		var $allReports = new Array();
//		// Copy over the reports that are on selected reports but don't belong to a dashboard 
//		if(selectedReportsHasDashboard() == "false") {	
//			var $getReports = $("#beginselectedreports").children();
//					
//			for(p=0; p < $getReports.length; p++)
//			{
//				$allReports[p] = $getReports[p].getAttribute("name").substring(6);
//				
//			}
//			$getReports = null;
//
//					$.post("./ajax/getvirtualprofile.jsp", {}, function(data){
//							var dashboardid = data.replace(/^\s*|\s*$/g,'');
//							
//							var URLString = "../reportgenerator/ajax/deleteAllDashboardItems.jsp?" + tablekey + "=" + dashboardid + "&-tableSpec=" + tablespec;
//							var defresponse = new DefaultResponse(
//								function(obj) {				
//													
//								}
//							);
//							var xmlreq = new XmlRequest(defresponse, dashboardid);
//							xmlreq.send(URLString);				
//					});
//
//		}
//		else {
//			
//			var $getReports = $("li[name^='beanid']");
//
//			for(p=0; p < $getReports.length; p++)
//			{
//				$allReports[p] = $getReports[p].getAttribute("name").substring(6);				
//			}
//			$getReports = null;
//						
//		}
//						
//		$("#selectedreportspanel").empty();
//					
//		$.post("./ajax/saveasnewprofile.jsp", {}, function(data){
//			var profileid = data.replace(/^\s*|\s*$/g,'');
//						
//			$("#findreportsform input[name='-dashboardid']").attr("value",profileid);
//			$("#findreportsform input[name='-selectedReportsHasDashboard']").attr("value","true");
//						
//				$.post("./ajax/useprofile.jsp", {profile: profileid, selectedReportsHasDashboard: "false", numberOfReports: "0", reportIDs: $allReports}, function(data){
//				  
//				  $("#selectedreportspanel").append(data);
//				  var url = './popup/editprofile.jsp?-profileID=' + profileid
//				  openInLightBox(url, 'EditProfile', 700, 750, "no", "no");  			  			
//				});	
//			
//		});
}

function newQuestion(contextpath, profileID)
{
	
	$.post("./ajax/createquestion.jsp", {}, function(data){
		var questionid = data.replace(/^\s*|\s*$/g,'');
		//alert(questionid);
		useQuestion('Unnamed Question', questionid, contextpath, 'true', profileID);		
	});	
}

function deleteQuestion(reportID, dashboardid)
{
		$.post("./ajax/deletequestion.jsp", {QuestionID: reportID, ProfileID: dashboardid}, function(data){
						
			var deleteCheck = data.replace(/^\s*|\s*$/g,'');
			if( deleteCheck.length > 1 )
			{
				alert(deleteCheck);
				return false;
			}
			else
			{
				$("#findreportsform input[name='-beanID'] ").attr("value", reportID);
				$("#findreportsform input[name='-dashboardid'] ").attr("value", dashboardid);
				$("#findreportsform input[name='-selectedReportsHasDashboard'] ").attr("value", selectedReportsHasDashboard());		
				$("#findreportsform").submit();
			}
					
		});
}

function removeProfileItem(questionid, surveyquestionid, contextpath, surveyid)
{
	
	var toremove = document.getElementById('paragraph' + surveyquestionid).parentNode.parentNode;
	toremove.removeChild(document.getElementById('paragraph' + surveyquestionid).parentNode);			
		
	//reset the sort numbers
	var $kids = $("#beginselectedreports").children();

	for(var p=0; p < $kids.length; p++)
	{	
		$kids[p].setAttribute("sortNumber", p)	
	}
	
	$.post("./ajax/genrowaction.jsp", {'SurveyQuestionID': surveyquestionid, '-tableSpec' : 'SurveyQuestions', '-action' : 'delete'}, function(data){
		
		$("li[beanid='" + questionid + "']").css( {display:'inline', visibility:'visible'} );
		
		$.post("./ajax/updatesortordernumbers.jsp", {'SurveyID': surveyid, '-tableSpec' : 'SurveyQuestions'}, function(data){
		
	
		});
	});
}

function displayQuestionsVirtualProfile(profileid,reportid)
{
	var profileid = profileid;

	$.get("./ajax/addprofile.jsp", {}, function(data){
			$("#selectedreportspanel").append(data);
			
				if( !profileid)
				{
					$.post("./ajax/getvirtualprofile.jsp", {}, function(data){
						profileid = data.replace(/^\s*|\s*$/g,'');

								$.post("./ajax/useprofile.jsp", {profile: profileid, selectedReportsHasDashboard: "false"}, function(data){
 										$("#beginselectedreports").append(data);
										
									//prevent user from adding reports already on virtual dashboard
									preventUserFromSelectingReportsOnVirtualDashboard()

								});	
					
					});
				}
				else
				{			
					$.post("./ajax/useprofile.jsp", {profileid: profile, selectedReportsHasDashboard: "false"}, function(data){
			 
							  $("#beginselectedreports").append(data);
							  
							  if(reportid)
							  {
								  $("li[name='" + reportid + "']").css( {'background-color':'#FFFFCC'} );
							  }

							  
							  		//prevent user from adding reports already on virtual dashboard
									preventUserFromSelectingReportsOnVirtualDashboard()
					  
					});		
				}
		
	});
}

function clearSelectedProfileCanvas(tablespec,tablekey)
{

	$("div[id='dropmenuSelectedReports'] a:eq(2)").remove();
	//$("div[id='dropmenuSelectedReports']").append("<a class='grey' href='javascript:deleteAllReports();'>Delete all reports</a>");

	$("#selectedreportspanel").empty();
	
	showUseFromReports();
	showUseFromDashboards();	
	
	if ( selectedReportsHasDashboard() == "false" )
	{
		
		
		$.post("./ajax/getvirtualprofile.jsp", {}, function(data){
				var dashboardid = data.replace(/^\s*|\s*$/g,'');

				var URLString = "../reportgenerator/ajax/deleteAllDashboardItems.jsp?" + tablekey + "=" + dashboardid + "&-tableSpec=" + tablespec;
				var defresponse = new DefaultResponse(
				function(obj) {	
						
					$("#findreportsform input[name='-beanID'] ").attr("value", "");
					$("#findreportsform input[name='-dashboardid'] ").attr("value", "");
					$("#findreportsform input[name='-selectedReportsHasDashboard'] ").attr("value", false);		
					$("#findreportsform").submit();
					$("#quickview input[name='ProfileID'] ").attr("value", "");
					$("#quickviewresults input[name='ProfileID'] ").attr("value", "");
				}
				);
				var xmlreq = new XmlRequest(defresponse, dashboardid);
				xmlreq.send(URLString);
				$("#findreportsform input[name='-dashboardid']").attr("value","");
				$("#quickview input[name='ProfileID'] ").attr("value", "");
				$("#quickviewresults input[name='ProfileID'] ").attr("value", "");
		});
		
	}
	else
	{
		$("#findreportsform input[name='-beanID'] ").attr("value", "");
		$("#findreportsform input[name='-dashboardid'] ").attr("value", "");
		$("#findreportsform input[name='-selectedReportsHasDashboard'] ").attr("value", false);		
		$("#findreportsform").submit();
		$("#quickview input[name='ProfileID'] ").attr("value", "");
		$("#quickviewresults input[name='ProfileID'] ").attr("value", "");
	}
	
	//disableSelect("","");
	displaySelectedProfilePanel()	
}

function disableSelect(profileType, groupName) {
	//if (profileType == "" && groupName == "") return;
	
	var $li = $("li[wanted='true']");
	//alert(groupName + " = " + profileType + " > " + $li.length);
	for(p=0; p < $li.length; p++)
	{
		var parts = $li[p].type.split(":");
		//if (p > 10 && p < 30) alert(parts[0] + " = " + parts[1]);
		if (profileType == "" && groupName == "") {
			$li[p].style.visibility = "visible";
			$li[p].style.display = "block";
			
		} else if (parts.length ==2 && (profileType != "" && profileType == parts[0])) {
			// && (groupName != "" && groupName == parts[1])
			// alert(parts[0] + " = " + parts[1]);
			// these are the ones we want to keep, disable the rest
		} else {
			$li[p].style.visibility = "hidden";
			$li[p].style.display = "none";
		}
	}
}

function disableQuestion(beanid, status) {
	//if (profileType == "" && groupName == "") return;
	
	if (status == null || status == "") status = false;
	
	var $li = $("li[beanid='" + beanid + "']");
	//alert(beanid + " = " + status + " > " + $li.length);
	for(p=0; p < $li.length; p++)
	{
		if ($li[p].type != "edit") {
			if (status) {
				$li[p].style.visibility = "visible";
				$li[p].style.display = "block";
			} else {
				$li[p].style.visibility = "hidden";
				$li[p].style.display = "none";
			}
		}
	}
}

function confirmDeleteProfileWrapper(profileid,prompt)
{
	confirmDeleteProfile(profileid,prompt);
}

function confirmDeleteProfile(profileid,prompt)
{
	var r = true;
	if(prompt){
		r = confirm("Are you sure you wish to delete this profile?");
	}
	
	if(r == true)
	{
	
		$.post("./ajax/deleteprofile.jsp", {'ProfileID': profileid}, function(data){
				
			var deleteCheck = data.replace(/^\s*|\s*$/g,'');
			if( deleteCheck.length > 1 )
			{
				alert(deleteCheck);
				return false;
			}
			else
			{
				if (profileid == $("#findreportsform input[name='-dashboardid']").attr("value")) $("#findreportsform input[name='-dashboardid']").attr("value","");
				$("#findreportsform").submit();
				//$("form[currentpage='true'] input[name='-beanID'] ").attr("value", "");
				//$("form[currentpage='true'] input[name='-dashboardid'] ").attr("value", "");
				//$("form[currentpage='true'] input[name='-selectedReportsHasDashboard'] ").attr("value", "false");
				//$("form[currentpage='true']").submit();
			}
		});	
	}
	
	return r;
}

function submitFormFilterDisplay(search,contextpath,sortby,sortorder)
{	
	var currentDashboard = '';
	if ( selectedReportsHasDashboard() == "true" ) {
		var currentDashboard = $("#selectedreportspanel p:first").attr("id").substring(9);
	}

	var url = contextpath + "/crm/profilegenerator/findquestions.jsp?search=true";
	if ( sortby )
	{
		var sorted = "&-sortBy=" + sortby + "&-sortOrder=" + sortorder;
		url += sorted;
	}
	
	var useform = document.getElementById("findreportsform");
	useform.action = url;
	useform.submit();	
}

function isDashboardSelected() {
	return $("#dashboardSelected").length > 0;
}

function selectedQuestionsCommandsHover() {
	//TODO: update help text
	
	var caption  = '<div style=\'text-align: left; padding: 10px;\'>RUNWAY Help</div>';
	var helptext = '<div style=\'text-align: left; padding: 10px; white-space: wrap;\'><p>You can re-arrange the sort order of the questions in this dashboard by dragging individual reports up and down.</p></div>'
	return overlib(helptext, CAPTION, caption, OFFSETX, -210, OFFSETY, -95, WIDTH, 200, FGCOLOR, "#DCDEEA");
}

function updateInstructionPanelVisability(){
	//only display #selectedQuestionsInstructionPanel if the questions list is empty
	
	//HACK - test if "dashboard" has is present
	if(isDashboardSelected()){
		$('#selectedQuestionsInstructionPanel').show();
	}
	else
	{
		$('#selectedQuestionsInstructionPanel').hide();	
	}
}

function gotoTopPage() {
	window.location.href="#topPage";
}

/*Start : Added by puja to support Clear all functionality in profiles*/
function isSelectedQuestionPanelEmpty() {
	var isSelectedReportsPanelEmpty = true;
	
	var beginSelectedReportsEl = $("#beginselectedreports");
	
	if(beginSelectedReportsEl) {
	    var selectedReports = beginSelectedReportsEl.children("li");
	    if(selectedReports.length > 0) {
	    	isSelectedReportsPanelEmpty = false;
	    }
	}
	
	var selectedDashboardsEl = $("#dashboardSelected");
	if(selectedDashboardsEl) {
	    var selectedDashboards = selectedDashboardsEl.children("li");
	    if(selectedDashboards.length > 0) {
	    	isSelectedReportsPanelEmpty = false;
	    }
	}
	
	return isSelectedReportsPanelEmpty;
}

function displayMessage(id, message) {
	var el = $("#" + id);
	if(el) {
		el.append(message);
	}
}

function clearMessage(id) {
	var el = $("#" + id);
	if(el) {
		el.empty();
	}
}

function displaySelectedProfilePanel() {

	clearMessage("selectedQuestionsInstructionPanel");
  
    if(isSelectedQuestionPanelEmpty()) {
    	$('#selectedQuestionsInstructionPanel').show();
      displayMessage("selectedQuestionsInstructionPanel", "<div style='margin: 15px 40px 15px 40px; font-family:Arial;font-size:13px;text-align: center; white-space: normal;'>Please select a profile or question from the question search list by hovering the cursor over the arrow icon and clicking \"Select\"</div>");
    }
}

function profileNewEdit(target) {
	var profileID = $("#findreportsform input[name='-dashboardid']").attr("value");
	if (profileID != "") openInLightBox(target + "?SurveyID=" + profileID,'EditProfile',1000,300);
}
/*End : Added by puja to support Clear all functionality in profiles*/