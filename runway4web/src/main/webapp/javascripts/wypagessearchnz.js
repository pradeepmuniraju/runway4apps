var stTypes = new Array(
	new stType("10","Avenue"),
	new stType("2","Drive"),
	new stType("56","Highway"),
	new stType("71","Lane"),
	new stType("21","Place"),
	new stType("33","Road"),
	new stType("57","Street"),
	new stType("63","Terrace"),
	new stType("82","Way"),
	new stType("0","Other"),
	new stType("-1","ALL"),
	new stType("","")
);

function stType(code, name)
{
	this.code = code;
	this.name = name;
}

function getStType(value)
{	
	for(var i=0; i<stTypes.length; i++)
	{
		if(value == stTypes[i].name)
		{
			return(stTypes[i].code);
		}
	}
	return(-1);
}

function getSDStreet(street1, street2)
{
	var returnvalue = new Array("","","");

	var tempst = street1;

	if(street2.length!=0)
	{
		tempst = street2;
	}
	if(tempst.indexOf(',')>=0)
	{
		tempst = tempst.split(",")[1];
	}
	tempst = trim(tempst);

	var stwords = tempst.split(" ");

	for(var i=0; i<stwords.length; i++)
	{
		if(i==0 && stwords.length>2 )
		{
			returnvalue[0] = stwords[0];
		}
		else if(i==stwords.length-1)
		{
			returnvalue[2] = getStType(stwords[i]);
		}
		else
		{
			if(stwords[i].length>1)
			{
				if(i!=1)
				{
					returnvalue[1] = returnvalue[1] + " ";
				}
				returnvalue[1] = returnvalue[1] + stwords[i];
			}
		}
	}

	return(returnvalue);
}


function trim(word)
{
	var returnvalue = word;
	if(returnvalue.charAt(0)==" ")
	{
		returnvalue = returnvalue.substr(1,returnvalue.length);
	}
	if(returnvalue.charAt(returnvalue.length-1)==" ")
	{
		returnvalue = returnvalue.substr(0,returnvalue.length-1);
	}
	if(returnvalue.charAt(0)==" " || returnvalue.charAt(returnvalue.length-1)==" ")
	{
		return(trim(returnvalue));
	}
	return(returnvalue);
}

function whitepagesContactSearch(company, city, state, postcode)
{
	whitepagesSearch(company, city, state, postcode);
}

function whitepagesSearch(company, city, state, postcode)
{
	wpform = document.whitepagessearchform;
	wpform.elements['key'].value = trim(company);
 	wpform.elements['lkey'].value = trim(city);
	wpform.submit();
}

function yellowpagesSearch(company, city, state, postcode)
{
	ypform = document.yellowpagesform;
	ypform.elements['key'].value = trim(company);
 	ypform.elements['lkey'].value = trim(city);
	ypform.submit();
}

function googleSearch(searchterm)
{
	ypform = document.googleform;
	ypform.elements['q'].value = trim(searchterm);
	ypform.submit();
}

function streetdirectorySearch(street1, street2, city, state, postcode)
{
	var streetvalues = getSDStreet(street1, street2);

	sdform = document.streetdirectoryform;
	sdform.elements['street'].value = streetvalues[1];
	sdform.elements['sttype'].value = streetvalues[2];
	sdform.elements['town'].value = trim(city);
	sdform.elements['cname'].value = streetvalues[0];
	sdform.submit();
}