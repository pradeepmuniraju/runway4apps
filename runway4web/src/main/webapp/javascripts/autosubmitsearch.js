this.document.onkeyup = autosubmit;
var focussed = true;

function autosubmit(evnt)
{
	if(navigator.appName == "Microsoft Internet Explorer")
	{
		if ((event.keyCode == 10 || event.keyCode == 13) && focussed)
		{
			doautosubmit();
		}
	}
	else
	{
		if ((evnt.which == 10 || evnt.which == 13) && focussed)
		{
			doautosubmit();
		}
	}
	return true;
}

function doautosubmit()
{
	if(typeof(autosubmitformname) == "undefined")
	{
		autosubmitformname = "searchForm";
	}
	if (typeof(submittrapped) == "undefined") {
		if (typeof document.forms[autosubmitformname] != 'undefined') document.forms[autosubmitformname].submit();
	}
	else if(submittrapped)
	{
		if(notSubmitted())
		{
			if (typeof doSubmit != 'undefined') doSubmit();
			else if (typeof document.forms[autosubmitformname] != 'undefined') document.forms[autosubmitformname].submit();
		}
	}
	else
	{
		if (typeof doSubmit != 'undefined') doSubmit();
		else if (typeof document.forms[autosubmitformname] != 'undefined') document.forms[autosubmitformname].submit();
	}
}