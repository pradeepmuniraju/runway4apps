/**
 * Filename:      stringutils.js
 * Author:        Ulpian Cesana
 *  
 * Dependencies:  None  
 */

/**
 * Creates a StringUtils helper object to perform common
 * string operations.
 */
function StringUtils() {
}

/** Ellipses appended on the end of truncated string (see StringUtils#truncateString) */
StringUtils.ELLIPSES = "...";

/**
 * @param str
 * @return true is the str is not empty; false otherwise
 */
StringUtils.isNotEmpty = function (str) {
    var isNotEmpty = false;
    if(str && typeof str == 'string' && str.length > 0) {
        isNotEmpty = true;
    }
    return isNotEmpty;
};

/**
 * @param str
 * @param maxLength
 * @return the truncated str up to maxLength characters with an 
 * ellipses "..." appended to the end, if str has more characters 
 * than maxLength, otherwise return the original str
 */
StringUtils.prototype.truncateString = function (str, maxLength) {
    var truncatedStr = str;
    
    if (str.length > maxLength) {
        truncatedStr = str.substring(0, maxLength) + StringUtils.ELLIPSES;
    }
    
    return truncatedStr;
};