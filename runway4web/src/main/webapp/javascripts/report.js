/* Select a report from the search results */
function useReport(reportName,reportID,contextpath,newreport)
{
	// Prevent the user from selecting the report a second time

	hideReport(reportID);

	var numreports = numberOfReports();
	
	if(selectedReportsHasDashboard() == "true") {
		
		var dashboardid = getCurrentDashboard();
		
		$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/listitem.jsp", {name: reportName, reportID: reportID, dashboardID: dashboardid, numberOfReports: numreports, selectedReportsHasDashboard: "true"}, function(data){
			  $("#beginselectedreports").append(data);
				if( newreport == 'true')
				{
					submitUseForm(reportID, selectedReportsHasDashboard(), dashboardid);
				}
				makeSortable(contextpath);	  
		});															
	}
	else {
			
		if(numreports == 0)
		{		
			addDashboard();
		}
  
			$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/listitem.jsp", {name: reportName, reportID: reportID, numberOfReports: numreports, selectedReportsHasDashboard: "false"}, function(data){
				  $("#beginselectedreports").append(data);

					  if( newreport == 'true')
					  {				  
							$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/getvirtualdashboard.jsp", {}, function(data){
								var dashboardid = data.replace(/^\s*|\s*$/g,'');
								submitUseForm(reportID, "false", dashboardid);
							});
					  }					
					  makeSortable(contextpath);
			});					  
	}
	clearMessage("selectedReportsInstructionPanel");
}

function submitUseForm(reportID, hasDashboard, dashboardid){
	var useform = document.getElementById("viewreport");
	useform.elements[0].value = reportID;
	useform.elements[1].value = hasDashboard;		
	useform.elements[2].value = dashboardid;
	useform.submit();		
}

function useReports(reportIDs, contextpath)
{
	if(reportIDs){
		// Prevent the user from selecting the report a second time
		for(var i=0; i<reportIDs.length; i++){
			hideReport(reportIDs[i]);
		}
		
		var numreports = numberOfReports();
		
		if(selectedReportsHasDashboard() == "true") {
			
			var dashboardid = getCurrentDashboard();
			
			$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/listitems.jsp", {reportID: reportIDs, dashboardID: dashboardid, selectedReportsHasDashboard: "true"}, function(data){
				  $("#beginselectedreports").append(data);
					makeSortable(contextpath);	  
			});															
		}
		else {
				if(numreports == 0)
				{		
					addDashboard();
				}
	  
				$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/listitems.jsp", {reportID: reportIDs, selectedReportsHasDashboard: "false"}, function(data){
					  $("#beginselectedreports").append(data);
						  makeSortable(contextpath);
				});					  
		}
		clearMessage("selectedReportsInstructionPanel");
	}
}

function getCurrentDashboard()
{
	var currentDashboard = document.getElementById("selectedreportspanel").getElementsByTagName("p")[0];
	var dashboardid= currentDashboard.getAttribute("id").substring(9);
	return(dashboardid);
}

function addDashboard()
{
	$("#selectedreportspanel").empty();
	$.get(window.CTX_PATH + "/crm/reportgenerator/ajax/addDashboard.jsp", {}, function(data){
			$("#selectedreportspanel").append(data);
			makeSortable(contextpath);
	});
}

function hideReport(reportID)
{
	var prepareHide = "tr[name='tablerow" + reportID + "']"
	var $toHide = $(prepareHide);
	for(p=0; p < $toHide.length; p++)
	{
		$toHide[p].getElementsByTagName("li")[1].style.visibility = "hidden";
		$toHide[p].getElementsByTagName("li")[1].style.display = "none";
	}
}

function useDashboard(dashboardID, contextpath, tablespec, tablekey, beanidhighlight) {
	useDashboard(dashboardID, contextpath, tablespec, tablekey, beanidhighlight, 'false');
}

function useDashboard(dashboardID, contextpath, tablespec, tablekey, beanidhighlight, repView)
{		
		if (typeof repView == 'undefined' || repView == null) repView = "false";
		// Copy over the reports that are on selected reports but don't belong to a dashboard 				
		var $allReports = new Array();
		if(selectedReportsHasDashboard() == "false") {	
			
			var $getReports = $("#beginselectedreports").children();
					
			for(p=0; p < $getReports.length; p++)
			{
				$allReports[p] = $getReports[p].getAttribute("name").substring(6);
			}

			$getReports = null;
			$("#selectedreportspanel").empty();
			
					$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/getvirtualdashboard.jsp", {}, function(data) {
						var dashboardid = data.replace(/^\s*|\s*$/g,'');
						
						var URLString = "../reportgenerator/ajax/deleteAllDashboardItems.jsp?" + tablekey + "=" + dashboardid + "&-tableSpec=" + tablespec;
						var defresponse = new DefaultResponse(
							function(obj) {				
												
							}
						);
						var xmlreq = new XmlRequest(defresponse, dashboardid);
						xmlreq.send(URLString);
						
						makeSortable(contextpath);			
					});
					
		}
		else
		{
			$("#beginselectedreports").empty();
		}
			
		//prevent the user from selecting the dashboard a second time
		var $toHide = $("tr[dashboard='" + dashboardID + "']");				
		for(var j=0; j<$toHide.length; j++)
		{
			var beanid = $toHide[j].getAttribute("name").substring(8);
			$("li[beanid='" + beanid + "']").css( {display:'none', visibility:'hidden'} ); 
			
		}
		
		$("tr[id='dashboardidentifier" + dashboardID + "'] li:eq(1)").css( {display:'none', visibility:'hidden'} ); 
		$("div[id='dropmenuSelectedReports'] a:eq(3)").remove();
		
		
		// get the current dashboard id if it exists
		var currentdashboardid = getCurrentDashboardID();
	
		$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/useDashboard.jsp", {"-repView" : repView, dashboard: dashboardID, currentDashBoardID: currentdashboardid, selectedReportsHasDashboard: selectedReportsHasDashboard(), numberOfReports: numberOfReports(), reportIDs: $allReports}, function(data){
		  if( selectedReportsHasDashboard() == "false" ) {
			  $("#selectedreportspanel").append(data);		
		  }
		  else {
			  $("#beginselectedreports").append(data);
		  }

		  $allReports = null;
		
		  if(beanidhighlight)
		  {
			  $("li[name='" + beanidhighlight + "']").css( {'background-color':'#FFFFCC'} ); 
		  }
		  
		  makeSortable(contextpath);
		});
		clearMessage("selectedReportsInstructionPanel");
		
		$("#findreportsform input[name='-dashboardID']").attr("value",dashboardID);
}

function getCurrentDashboardID() {
	var currentDashboardID = undefined;
	var dashboardIDHolder = document.getElementById("selectedreportspanel").getElementsByTagName("p")[0];
	
	if(dashboardIDHolder && dashboardIDHolder.className == 'dashboard') {
		var currentDashboard = document.getElementById("selectedreportspanel").getElementsByTagName("p")[0];
		currentDashboardID = currentDashboard.getAttribute("id").substring(9);	
	}
	
	return currentDashboardID;
}

function duplicateDashboard(dashboardID,contextpath)
{		
		// Copy over the reports that are on selected reports but don't belong to a dashboard 	
		var $allReports = new Array();
		if(selectedReportsHasDashboard() == "false") {	

			var $getReports = $("#beginselectedreports").children();
			for(p=0; p < $getReports.length; p++)
			{
				$allReports[p] = $getReports[p].getAttribute("name").substring(6);
			}
			$getReports = null;
			$("#selectedreportspanel").empty();

		}
		else
		{
			$("#beginselectedreports").empty();
		}
		
		// get the current dashboard id if it exists
		if(document.getElementById("selectedreportspanel").getElementsByTagName("p")[0] != null) {
			var currentDashboard = document.getElementById("selectedreportspanel").getElementsByTagName("p")[0];
			var currentdashboardid= currentDashboard.getAttribute("id").substring(9);	
		}
		
		$("div[id='dropmenuSelectedReports'] a:eq(2)").remove();
		
	
	$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/duplicateDashboard.jsp", {dashboard: dashboardID, currentDashBoardID: currentdashboardid, selectedReportsHasDashboard: selectedReportsHasDashboard(), reportIDs: $allReports, numberOfReports: numberOfReports()}, function(data){
		  
		  if( selectedReportsHasDashboard() == "false" ) {
			  $("#selectedreportspanel").append(data);
			  document.getElementById("selectedreportspanel").getElementsByTagName("ul")[0].setAttribute("name", "dashboardSelected");

		  }
		  else {
			  $("#beginselectedreports").append(data);			  
		  }		    		  	
		  $allReports = null;	
		  
		  makeSortable(contextpath);
	});

	clearMessage("selectedReportsInstructionPanel");
}


function duplicateReport(reportID,contextpath)
{
	if(numberOfReports() == 0)
	{

		$.get(window.CTX_PATH + "/crm/reportgenerator/ajax/addDashboard.jsp", {}, function(data){
			  	$("#selectedreportspanel").append(data);
			  	makeSortable(contextpath);
		});
							
		$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/duplicateReport.jsp", {reportID: reportID, numberOfReports: "0", selectedReportsHasDashboard: selectedReportsHasDashboard()}, function(data){
				$("#beginselectedreports").append(data);
				makeSortable(contextpath);
		});	
	}
	else
	{		
		var currentDashboard = document.getElementById("selectedreportspanel").getElementsByTagName("p")[0];
		var dashboardid= currentDashboard.getAttribute("id").substring(9);
		
		$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/duplicateReport.jsp", {reportID: reportID, numberOfReports: numberOfReports(), dashboardID: dashboardid, selectedReportsHasDashboard: selectedReportsHasDashboard()}, function(data){
				$("#beginselectedreports").append(data);
				makeSortable(contextpath);
		});	
		
	}
	clearMessage("selectedReportsInstructionPanel");
}

function saveAsNewDashboard(tablespec,tablekey)
{
		
		var $allReports = new Array();
		// Copy over the reports that are on selected reports but don't belong to a dashboard 
		if(selectedReportsHasDashboard() == "false") {	
			var $getReports = $("#beginselectedreports").children();	
		
			for(p=0; p < $getReports.length; p++)
			{
				$allReports[p] = $getReports[p].getAttribute("name").substring(6);
				
			}
			$getReports = null;

					$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/getvirtualdashboard.jsp", {}, function(data){
							var dashboardid = data.replace(/^\s*|\s*$/g,'');
							
							var URLString = window.CTX_PATH + "/crm/reportgenerator/ajax/deleteAllDashboardItems.jsp?" + tablekey + "=" + dashboardid + "&-tableSpec=" + tablespec;
							var defresponse = new DefaultResponse(
								function(obj) {				
													
								}
							);
							var xmlreq = new XmlRequest(defresponse, dashboardid);
							xmlreq.send(URLString);
							makeSortable(window.CTX_PATH);				
					});

		}
		else {
			
			var $getReports = $("li[name^='beanid']");
			for(p=0; p < $getReports.length; p++)
			{
				$allReports[p] = $getReports[p].getAttribute("name").substring(6);				
			}
			$getReports = null;
						
		}
						
		$("#selectedreportspanel").empty();
					
		$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/saveAsNewDashboard.jsp", {reportIDs: $allReports}, function(data){
			var dashboardid = data.replace(/^\s*|\s*$/g,'');
						
			$("#findreportsform input[name='-dashboardID']").attr("value",dashboardid);
			$("#findreportsform input[name='-selectedReportsHasDashboard']").attr("value","true");
						
				$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/useDashboard.jsp", {dashboard: dashboardid, selectedReportsHasDashboard: "false", dashboardName: "", numberOfReports: "0", reportIDs: $allReports}, function(data){
				  
				  $("#selectedreportspanel").append(data);
				  var url = window.CTX_PATH + '/crm/reportgenerator/popup/editdashboard.jsp?dashboardID=' + dashboardid;
				  openInLightBox(url, 'EditDashboard', 700, 750, "no", "no");
				});	
				makeSortable(window.CTX_PATH);
		});

}

function newReport(contextpath)
{
	$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/createReport.jsp", {}, function(data){
		var reportid = data.replace(/^\s*|\s*$/g,'');
		useReport('Unnamed report', reportid, contextpath, 'true');		
		makeSortable(contextpath);
	});	
	
	clearMessage("selectedReportsInstructionPanel");
}

function deleteReport(reportID, dashboardid)
{

	var r = confirm("All dashboards with this report will be affected.  Are you sure?");
	if(r == true)	
	{
		var currentreport = $("form[name='searchForm'] input[name='-beanID'] ").attr("value");
		$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/deleteReport.jsp", {reportID: reportID}, function(data){
						
			
			if(  currentreport != reportID )
			{
				$("form[currentpage='true'] input[name='-beanID'] ").attr("value", currentreport);
			}
				
			$("form[currentpage='true'] input[name='-dashboardID'] ").attr("value", dashboardid);
			$("form[currentpage='true'] input[name='-selectedReportsHasDashboard'] ").attr("value", selectedReportsHasDashboard());		
			$("form[currentpage='true']").submit();
					
		});

	}
	displaySelectedReportsPanel()
}

function deleteAllReports(dashboardid)
{
	
	var r = confirm("All dashboards with these reports will be affected.  Are you sure?");
	if(r == true)	
	{
			
			if ( selectedReportsHasDashboard() == "false" )
			{
				$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/getvirtualdashboard.jsp", {}, function(data){
						var dashboardid = data.replace(/^\s*|\s*$/g,'');
												
						$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/deleteAllReports.jsp", {dashboardid: dashboardid}, function(data){

							$("form[currentpage='true'] input[name='-beanID'] ").attr("value", "");
							$("form[currentpage='true'] input[name='-dashboardID'] ").attr("value", "");
							$("form[currentpage='true'] input[name='-selectedReportsHasDashboard'] ").attr("value", "false");
							$("form[currentpage='true']").submit();

						});
				});
			
			
			}
			else
			{
				$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/deleteAllReports.jsp", {dashboardid: dashboardid}, function(data){
							
							$("form[currentpage='true'] input[name='-beanID'] ").attr("value", "");
							$("form[currentpage='true'] input[name='-dashboardID'] ").attr("value", dashboardid);
							$("form[currentpage='true'] input[name='-selectedReportsHasDashboard'] ").attr("value", "true");
							$("form[currentpage='true']").submit();

				});
			}
	}
	displaySelectedReportsPanel()
}

function removeDashboardItem(reportid, dashboarditemid, contextpath)
{

	$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/deleteDashboardItem.jsp", {dashboarditemID: dashboarditemid}, function(data){
		$("li[beanid='" + reportid + "']").css( {display:'inline', visibility:'visible'} );
	});
		
	var toremove = document.getElementById('paragraph' + dashboarditemid).parentNode.parentNode;
	toremove.removeChild(document.getElementById('paragraph' + dashboarditemid).parentNode);			
		
	//reset the sort numbers
	var $kids = $("#beginselectedreports").children();

	for(var p=0; p < $kids.length; p++)
	{	
		$kids[p].setAttribute("sortNumber", p)	
	}
		
	displaySelectedReportsPanel()
}

function displayReportsVirtualDashboard(dashboardidstr, reportidstr, contextpath, usenew)
{
	var dashboardid = dashboardidstr;

	$.get(window.CTX_PATH + "/crm/reportgenerator/ajax/addDashboard.jsp", {}, function(data){
			$("#selectedreportspanel").append(data);
			
				if( !dashboardid || dashboardid.length==0 || 'true' == usenew)
				{
					var reportid = reportidstr;
					if(reportid && reportid.indexOf("beanid")==0){
						reportid = reportid.substring(6, reportid.length);
					}
					$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/getvirtualdashboard.jsp", { beanid:reportid, '-usenew':usenew }, function(data){
						dashboardid = data.replace(/^\s*|\s*$/g,'');

								$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/usedashboardvirtual.jsp", {dashboard: dashboardid, selectedReportsHasDashboard: "false"}, function(data){
 										$("#beginselectedreports").append(data);
										
									//prevent user from adding reports already on virtual dashboard
									preventUserFromSelectingReportsOnVirtualDashboard()
								});
					});
				}
				else
				{
					$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/usedashboardvirtual.jsp", {dashboard: dashboardid, selectedReportsHasDashboard: "false"}, function(data){
			 
					  $("#beginselectedreports").append(data);
					  
					  if(reportid)
					  {
						  $("li[name='" + reportid + "']").css( {'background-color':'#FFFFCC'} );
					  }
							  
				  	  //prevent user from adding reports already on virtual dashboard
					  preventUserFromSelectingReportsOnVirtualDashboard();
					});		
				}
				makeSortable(contextpath);
		});
}
							
function shareReport(reportid,contextpath,idname)
{
	var target = contextpath + "/crm/popup/contacts/sharedashboardtocontactsearch.jsp?" + idname + "=" + reportid + "&-mode=ReportGenerator";
	openInLightBox(target,'UpdateSavedReport', '800', '300', 'no','no');			
}

function editDetails(reportid,contextpath)
{
	var search = $("form[currentpage='true'] input[name='search'] ").attr("value");
	var target = contextpath + "/crm/reportgenerator/popup/updatedetails.jsp?BeanID=" + reportid + "&search=" + search;
	openInLightBox(target,'UpdateDesc','600', '150', 'no','no');
}

function submitFormFilterDisplay(search,contextpath,sortby,sortorder)
{
	var currentDashboard = '';
	if ( selectedReportsHasDashboard() == "true" ) {
		var currentDashboard = $("#selectedreportspanel p:first").attr("id").substring(9);
	}
	
	var url = contextpath + "/crm/reportgenerator/findreports.jsp?search=" + search + "&-dashboardID=" + currentDashboard + "&-selectedReportsHasDashboard=" + selectedReportsHasDashboard();
	if ( sortby )
	{
		var sorted = "&-sortBy=" + sortby + "&-sortOrder=" + sortorder;
		url += sorted;
	}
	
	var useform = document.getElementById("form1");
	if(useform == null)
		useform = document.getElementById("viewreport");
	useform.action = url;
	useform.submit();
}

function clearSelectedReportsCanvas(tablespec,tablekey)
{
	$("form[currentpage='true'] input[name='-beanID'] ").attr("value", "");
	$("form[currentpage='true'] input[name='-dashboardID'] ").attr("value", "");
	$("form[currentpage='true'] input[name='-selectedReportsHasDashboard'] ").attr("value", "false");
	
	$("div[id='dropmenuSelectedReports'] a:eq(3)").remove();
	$("div[id='dropmenuSelectedReports']").append("<a class='grey' href='javascript:deleteAllReports();'>Delete all reports</a>");

	$("#selectedreportspanel").empty();
	
	showUseFromReports();
	showUseFromDashboards();	
	
	if ( selectedReportsHasDashboard() == "false" )
	{
		
		
		$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/getvirtualdashboard.jsp", {}, function(data){
				var dashboardid = data.replace(/^\s*|\s*$/g,'');
				
				var URLString = "../reportgenerator/ajax/deleteAllDashboardItems.jsp?" + tablekey + "=" + dashboardid + "&-tableSpec=" + tablespec;
				var defresponse = new DefaultResponse(
				function(obj) {		
						
					$("form[currentpage='true'] input[name='-beanID'] ").attr("value", "");
					$("form[currentpage='true'] input[name='-dashboardID'] ").attr("value", "");
					$("form[currentpage='true'] input[name='-selectedReportsHasDashboard'] ").attr("value", "false");
					//$("form[currentpage='true']").submit();	
			
				}
				);
				var xmlreq = new XmlRequest(defresponse, dashboardid);
				xmlreq.send(URLString);				
		});
		
	}
	else
	{
		clearQueryParameters();
	}
	
	// $("#viewreport input[name='-dashboardID']").attr("value", "");
	// $("#findreportsform input[name='-dashboardID']").attr("value", "");
	 
	displaySelectedReportsPanel() 	
}

function clearQueryParameters() {
	$("form[currentpage='true'] input[name='-beanID'] ").attr("value", "");
	$("form[currentpage='true'] input[name='-dashboardID'] ").attr("value", "");
	$("form[currentpage='true'] input[name='-selectedReportsHasDashboard'] ").attr("value", "false");
	$("form[currentpage='true']").submit();		
}

function confirmDeleteDashboard(dashboardid)
{
	var r = confirm("Are you sure you wish to delete this dashboard?");
	if(r == true)
	{
	
		$.post(window.CTX_PATH + "/crm/reportgenerator/ajax/deletedashboard.jsp", {'-dashboardID': dashboardid}, function(data){
			$("form[currentpage='true'] input[name='-beanID'] ").attr("value", "");
			$("form[currentpage='true'] input[name='-dashboardID'] ").attr("value", "");
			$("form[currentpage='true'] input[name='-selectedReportsHasDashboard'] ").attr("value", "false");
			$("form[currentpage='true']").submit();
				
		});	

	}
	
}
//callback method
function updatesearchresults(recordid,newname)
{	
	var name = urlDecode(newname);
	$("span[spanbeanid='" + recordid + "'] ").each(function () {
		$(this).text(name);		
	});
	$("p[spanbeanid='" + recordid + "'] ").each(function () {
		$(this).text(name);		
	});	

}
//callback method
function updatedashboardnames(recordid,newname)
{	
	var name = urlDecode(newname);
	$("span[spandashboardid='" + recordid + "'] ").each(function () {
		$(this).text(name);		
	});

}

function isSelectedReportsPanelEmpty() {
	var isSelectedReportsPanelEmpty = true;
	
	var beginSelectedReportsEl = $("#beginselectedreports");
	
	if(beginSelectedReportsEl) {
	    var selectedReports = beginSelectedReportsEl.children("li");
	    if(selectedReports.length > 0) {
	    	isSelectedReportsPanelEmpty = false;
	    }
	}
	
	var selectedDashboardsEl = $("#dashboardSelected");
	if(selectedDashboardsEl) {
	    var selectedDashboards = selectedDashboardsEl.children("li");
	    if(selectedDashboards.length > 0) {
	    	isSelectedReportsPanelEmpty = false;
	    }
	}
	
	return isSelectedReportsPanelEmpty;
}

function displayMessage(id, message) {
	var el = $("#" + id);
	if(el) {
		el.append(message);
	}
}

function clearMessage(id) {
	var el = $("#" + id);
	if(el) {
		el.empty();
	}
}

function displaySelectedReportsPanel() {
	clearMessage("selectedReportsInstructionPanel");
  
    if(isSelectedReportsPanelEmpty()) {
      displayMessage("selectedReportsInstructionPanel", "<div style='margin: 15px 40px 15px 40px; text-align: center; white-space: normal;'>Please select a report or dashboard from the report search list by hovering the cursor over the arrow icon and clicking \"Select\" or \"Duplicate\"</div>");
    }
}

function makeSortable(contextPath) {
  if(!contextPath) 
	contextPath = window.CTX_PATH;

  if(contextPath) {
	  $("ul#beginselectedreports").sortable({
	    axis: 'y',
	    stop: function() {
		  var indeces = new Array();
	
		  var dashboardItems = $("ul#beginselectedreports").children();
		  
		  dashboardItems.each(function() {
			  var index = dashboardItems.index(this);
			  var dashboardItemID = $(this).find("p").attr("id");
			  
			  if(dashboardItemID) {
				 dashboardItemID = dashboardItemID.substring(9);
				 indeces.push({index: index, dashboardItemID: dashboardItemID});
			  } 
		  });
		  
		  var params = "";
		  for(var i = 0; i < indeces.length; i++) {
		    var index = indeces[i].index;
			var dashboardItemID = indeces[i].dashboardItemID;	
			params += "SortNumber="+dashboardItemID+":"+index+"&";
		  }
		  
		  var url = contextPath + "/crm/reportgenerator/ajax/updatedashboarditemsortnumbers.jsp";
		  var ajaxRequestID = new Date().getTime();
		  sendForm(ajaxRequestID, url+"?"+params); 	  
	    }
	  }).attr("title", "Drag this report to change its sort order")
		.css("cursor", "pointer");
  }
}

function addReportToNewDashboard(contextPath, reportID) {
	target = contextPath + "/crm/dashboard/popup/newdashboard.jsp?-mode=newdashboard&-reportID=" + reportID;
	openInLightBox(target,'Dashboard','650','650');
}

function addReportToExistingDashboard(contextPath, reportID) {
	target = contextPath + "/crm/dashboard/popup/addreporttoexstingdashboard.jsp?-beanID=" + reportID;
	openInLightBox(target,'Add Report to Existing Dashboard','400','650');
}
