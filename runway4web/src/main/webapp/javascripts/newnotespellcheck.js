var spellCheckURL="/jspell/JSpell.jsp"; // change to point to the JSpell Spell Check Servlet
var styleSheetURL="/jspell/jspell.css"; 
var imagePath="/jspell/images"; // relative URL to JSpell button images directory

var blankURL="about:blank"; // used for preview panels, if you are using SSL you'll need an
//var blankURL="/jspell/blank.html"; // actual HTML file to be the 'blank' panel, in which case uncomment this line

var ww;	// holds reference to popup
var disableLearn=false; // set to true, to remove the Learn words capability
var forceUpperCase=false; // force suggestions and spell checker to use upper case
var ignoreIrregularCaps=false;	// ignore lower case sentence beginnings, etc.
var ignoreFirstCaps=false;	// ignore if first character in a field is lowercase
var ignoreNumbers=false; // ignore words with embedded numbers
var ignoreUpper=false; // ignore words in upper case
var ignoreDouble=false; // ignore repeated words
var confirmAfterLearn=false; // show warning before user 'learns' a word
var confirmAfterReplace=true; // show warning when replacing using a word not in the suggestions list.
var supplementalDictionary=""; // optional supplemental word list kept at server.
var hidePreviewPanel=false; // You can use this to hide the preview panel when running in directEdit mode in IE
var directmode=false; // is highlighting done in original text control or is there a preview panel (IE Windows only)

function getSpellCheckItem(jspell_n) {
	var fieldsToCheck=getSpellCheckArray();
	return fieldsToCheck[jspell_n];
}
function getSpellCheckArray() {
	var fieldsToCheck=new Array();

	// make sure to enclose form/field object reference in quotes!
	fieldsToCheck[fieldsToCheck.length]='document.newnoteForm.Subject';
	fieldsToCheck[fieldsToCheck.length]='document.newnoteForm.Body';
	return fieldsToCheck;
}
function spellcheck() {
	var width=640; var height=300;
	if (navigator.appName == 'Microsoft Internet Explorer' && navigator.userAgent.toLowerCase().indexOf("opera")==-1 && hidePreviewPanel==false) {	
		directmode=true; width=300;
	}

	if(hidePreviewPanel==true)
		width=300;

	var w = 1024, h = 768;
	if (document.all || document.layers)
	{
		w=eval("scre"+"en.availWidth"); h=eval("scre"+"en.availHeight");
	}

	var leftPos = (w/2-width/2), topPos = (h/2-height/2);
	
	// need to check if window is already open.
	ww=window.open("/jspell/jspellpopup.html", "checker", "width="+width+",height="+height+",top="+topPos+",left="+leftPos+",toolbar=no,status=no,menubar=no,directories=no,resizable=yes");
	ww.focus();
}
function updateForm(jspell_m,newvalue) { eval(getSpellCheckItemValue(jspell_m)+"=newvalue"); }
function getSpellCheckItemValue(jspell_j) { return getSpellCheckItem(jspell_j)+".value"; }
function getSpellCheckItemValueValue(jspell_k) { return eval(getSpellCheckItemValue(jspell_k)); }
