function getSDStreet(street1, street2)
{
	var returnvalue = new Array("","","");

	var tempst = street1;

	if(street2.length!=0)
	{
		tempst = street2;
	}
	if(tempst.indexOf(',')>=0)
	{
		tempst = tempst.split(",")[1];
	}
	tempst = trim(tempst);

	var stwords = tempst.split(" ");

	for(var i=0; i<stwords.length; i++)
	{
		if(i==0 && stwords.length>2 )
		{
			returnvalue[0] = stwords[0];
		}
		else if(i==stwords.length-1)
		{
			returnvalue[2] = stwords[i];
		}
		else
		{
			if(stwords[i].length>1)
			{
				if(i!=1)
				{
					returnvalue[1] = returnvalue[1] + " ";
				}
				returnvalue[1] = returnvalue[1] + stwords[i];
			}
		}
	}

	return(returnvalue);
}

function trim(word)
{
	var returnvalue = word;
	if(returnvalue.charAt(0)==" ")
	{
		returnvalue = returnvalue.substr(1,returnvalue.length);
	}
	if(returnvalue.charAt(returnvalue.length-1)==" ")
	{
		returnvalue = returnvalue.substr(0,returnvalue.length-1);
	}
	if(returnvalue.charAt(0)==" " || returnvalue.charAt(returnvalue.length-1)==" ")
	{
		return(trim(returnvalue));
	}
	return(returnvalue);
}

function getInitialPostCode(postcode)
{
	return(postcode.charAt(0));
}

function whitepagesSearch(company, city, state, postcode)
{
	wpform = document.whitepagessearchform;
	wpform.elements['NAM'].value = trim(company);
	if(city.length != 0)
	{
		wpform.elements['LOC'].value = trim(city);
	}
	else
	{
		wpform.elements['PCD'].value = getInitialPostCode(trim(postcode));
	}
	wpform.submit();
}

function getPostCodeOrCity(city, postcode)
{
	if(city.length != 0)
	{
		return(trim(city));
	}
	else
	{
		return(trim(postcode));
	}
}

function whitepagesContactSearch(lastname, city, state, postcode)
{
	wpform = document.whitepagescontactsearchform;
	wpform.elements['NAM'].value = trim(lastname);
	wpform.elements['LOC'].value = getPostCodeOrCity(city, getInitialPostCode(postcode));

	wpform.submit();
}

function yellowpagesSearch(company, city, state, postcode)
{
	ypform = document.yellowpagesform;
	ypform.elements['keywords'].value = trim(company);
	ypform.elements['location'].value = getPostCodeOrCity(city, postcode);
	ypform.submit();
}

function googleSearch(searchterm)
{
	ypform = document.googleform;
	ypform.elements['q'].value = trim(searchterm);
	ypform.submit();
}

function streetdirectorySearch(street1, street2, city, state, postcode)
{
	var streetvalues = getSDStreet(street1, street2);

	sdform = document.streetdirectoryform;
	sdform.elements['txtAddress'].value = streetvalues[1];
	sdform.elements['txtPostcode'].value = trim(postcode);
	sdform.elements['txtCity'].value = trim(city);
	sdform.submit();
}