/**
 * Filename:      maputils.js
 * Author:        Ulpian Cesana
 * Dependencies:  None
 * Description:   Basic Map operations for Javascript map objects.
 */

function MapUtils() {
}

MapUtils.put = function(map, key, value) {
  if(map && value) {
    map[key] = value;
  }
};

MapUtils.containsKey = function(map, key) {
  var containsKey = false;
  if(map) {
    var value = map[key];
    if(value) {
      containsKey = true;
    }
  }
  return containsKey;
};

MapUtils.get = function(map, key) {
  var value = null;
  if(MapUtils.containsKey(key)) {
    value = map[key];
  }
  return value;
};

MapUtils.remove = function(map, key) {
  if(MapUtils.containsKey(key)) {
    delete map[key];
  }
};
