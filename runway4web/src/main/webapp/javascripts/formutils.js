/**
 * 
 * Filename:      formutils.js
 * Object Name:	  FormUtils
 * Author:		    Ulpian Cesana
 *  
 * Dependencies:  javascripts/lib/jquery-1.3.2.js
 * 
 */
function FormUtils() {
}

FormUtils.getSelectedValue = function (selectID) {
    var value = "";
    var $selectedOption = FormUtils.getSelectedOption(selectID);
    if($selectedOption) {
        value = $selectedOption.attr("value");
    }
    return value; 
}

FormUtils.getSelectedOption = function (selectID) {
    return $("select#"+FormUtils.escapeJQueryOperatorChars(selectID)+" option:selected");
}

FormUtils.getInputValue = function(inputID) {
    return $("input#"+FormUtils.escapeJQueryOperatorChars(inputID)).val();
}

FormUtils.setInputValue = function(inputID, value) {
    $("input#"+FormUtils.escapeJQueryOperatorChars(inputID)).val(value)
}
  
/**
 * Element ID and NAME must begin with a letter ([A-Za-z]) and may be followed by any number of letters, digits ([0-9]), hyphens ("-"), underscores ("_"), colons (":"), and periods (".").
 * See http://www.w3.org/TR/html4/types.html#h-6.3
 *
 * The period and colon are used by JQuery as operators, so to append an ID or NAME to a selector string, you must escape these characters.
 *
 */
FormUtils.escapeJQueryOperatorChars = function (selectID) {    
    var escapedSelectID = selectID;
    if(escapedSelectID) {
        escapedSelectID = escapedSelectID.replace(/\./g, "\\.");
        escapedSelectID = escapedSelectID.replace(/:/g, "\\:");
    }
    return escapedSelectID;
}

FormUtils.setPostParameter = function (formName, paramName, paramValue) {
    if(formName && paramName && paramName.length > 0 && paramValue && paramValue.length > 0) {
        var form = document.forms[formName];
        if(form) {
            form.elements[paramName].value = paramValue;
        }
    }
}

FormUtils.addPostParameter = function (formName, paramName, paramValue) {
    if(formName && paramName && paramName.length > 0 && paramValue && paramValue.length > 0) {
        var field = document.createElement("input");
        field.setAttribute("type", "hidden");
        field.setAttribute("value", paramValue);
        field.setAttribute("name", paramName);
        var form = document.forms[formName];
        if(form) {
            form.appendChild(field);
        }
    }
}