var ypstatecodes = new Array(
		"NSW","VIC","WA","QLD","SA","NT","ACT","TAS","ALL"
	);

var sdstatecodes = new Array(
		"VIC","NSW","WA","QLD","TAS","SA","ACT","NT"
	);


function getWPState(statevalue)
{	
	return(statevalue);
}

function getYPState(statevalue)
{	
	for(var i=0; i<ypstatecodes.length; i++)
	{
		if(statevalue == ypstatecodes[i])
		{
			return(i+1);
		}
	}
	return(9);
}

function getSDState(statevalue)
{	
	for(var i=0; i<sdstatecodes.length; i++)
	{
		if(statevalue == sdstatecodes[i])
		{
			return(i+1);
		}
	}
	return("");
}

function getSDStreet(street1, street2)
{
	var returnvalue = new Array("","","");

	var tempst = street1;

	if(street2.length!=0)
	{
		tempst = street2;
	}
	if(tempst.indexOf(',')>=0)
	{
		tempst = tempst.split(",")[1];
	}
	tempst = trim(tempst);

	var stwords = tempst.split(" ");

	for(var i=0; i<stwords.length; i++)
	{
		if(i==0 && stwords.length>2 )
		{
			returnvalue[0] = stwords[0];
		}
		else if(i==stwords.length-1)
		{
			returnvalue[2] = getSDStreetType(stwords[i]);
		}
		else
		{
			if(stwords[i].length>1)
			{
				if(i!=1)
				{
					returnvalue[1] = returnvalue[1] + " ";
				}
				returnvalue[1] = returnvalue[1] + stwords[i];
			}
		}
	}

	return(returnvalue);
}

function getSDStreetType(sttype)
{
	if(sttype == "Avenue" || sttype == "Ave")
	{
		return("AVENUE");
	}
	else if(sttype == "Boulevar" || sttype == "Blvd")
	{
		return("BOULEVARD");
	}
	else if(sttype == "Circuit")
	{
		return("CIRCUIT");
	}
	else if(sttype == "Close")
	{
		return("CLOSE");
	}
	else if(sttype == "Crescent" || sttype == "Cres")
	{
		return("CRESCENT");
	}
	else if(sttype == "Court" || sttype == "Ct" || sttype == "Crt")
	{
		return("COURT");
	}
	else if(sttype == "Freeway" || sttype == "Fwy")
	{
		return("FREEWAY");
	}
	else if(sttype == "Grove" || sttype == "Grv" || sttype == "Gv")
	{
		return("GROVE");
	}
	else if(sttype == "Highway" || sttype == "Hwy")
	{
		return("HIGHWAY");
	}
	else if(sttype == "Lane" || sttype == "Ln")
	{
		return("LANE");
	}
	else if(sttype == "Mews")
	{
		return("MEWS");
	}
	else if(sttype == "Parade" || sttype == "Pde")
	{
		return("PARADE");
	}
	else if(sttype == "Promenade")
	{
		return("PROMENADE");
	}
	else if(sttype == "Place" || sttype == "Pl")
	{
		return("PLACE");
	}
	else if(sttype == "Road" || sttype == "Rd")
	{
		return("ROAD");
	}
	else if(sttype == "Rise")
	{
		return("RISE");
	}
	else if(sttype == "Square" || sttype == "Sq")
	{
		return("SQUARE");
	}
	else if(sttype == "Street" || sttype == "St")
	{
		return("STREET");
	}
	else if(sttype == "Terrace" || sttype == "Ter")
	{
		return("TERRACE");
	}
	else if(sttype == "Track" || sttype == "Trak")
	{
		return("TRACK");
	}
	else if(sttype == "Way")
	{
		return("WAY");
	}
	else if(sttype == "Walk")
	{
		return("WALK");
	}
	return("ALL");
}

function trim(word)
{
	var returnvalue = word;
	if(returnvalue.charAt(0)==" ")
	{
		returnvalue = returnvalue.substr(1,returnvalue.length);
	}
	if(returnvalue.charAt(returnvalue.length-1)==" ")
	{
		returnvalue = returnvalue.substr(0,returnvalue.length-1);
	}
	if(returnvalue.charAt(0)==" " || returnvalue.charAt(returnvalue.length-1)==" ")
	{
		return(trim(returnvalue));
	}
	return(returnvalue);
}

function whitepagesContactSearch(lastname, city, state, postcode)
{
	wpform = document.whitepagesresidentialsearchform;
	wpform.elements['subscriberName'].value = trim(lastname);
 	wpform.elements['state'].value = getWPState(trim(state));
	wpform.elements['suburb'].value = trim(city);
	wpform.submit();
}

function whitepagesSearch(company, city, state, postcode)
{
	wpform = document.whitepagessearchform;
	wpform.elements['subscriberName'].value = trim(company);
 	wpform.elements['state'].value = getWPState(trim(state));
	wpform.elements['suburb'].value = trim(city);
	wpform.submit();
}

function yellowpagesSearch(company, city, state, postcode)
{
	ypform = document.yellowpagesform;
	ypform.elements['businessName'].value = trim(company);
 	ypform.elements['location'].value = getYPState(trim(state));
	ypform.elements['suburbPostcode'].value = city.length!=0?trim(city):trim(state);
	ypform.submit();
}

function googleSearch(searchterm)
{
	ypform = document.googleform;
	ypform.elements['q'].value = trim(searchterm);
	ypform.submit();
}

function streetdirectorySearch(street1, street2, city, state, postcode)
{
	var streetvalues = getSDStreet(street1, street2);

	sdform = document.streetdirectoryform;
	sdform.elements['StreetNo'].value = streetvalues[0];
	sdform.elements['StreetName'].value = streetvalues[1];
	//sdform.elements['StreetType'].value = streetvalues[2];
	sdform.elements['FrmStateID'].value = getSDState(trim(state));
	sdform.elements['Suburb'].value = trim(city);
	sdform.elements['CountryName'].value = trim(state).toLowerCase();
	sdform.elements['PostCode'].value = trim(postcode);
	sdform.submit();
}