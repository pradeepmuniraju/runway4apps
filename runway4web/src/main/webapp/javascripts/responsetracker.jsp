<%@ page import="com.sok.framework.*,com.sok.runway.*, java.util.*,org.apache.commons.lang.StringUtils" %>
<%
	String contextPath = request.getContextPath();
	String noteID = (String) session.getAttribute("trackerNoteID");
	
	if (noteID == null || noteID.length() == 0) noteID = (String) session.getAttribute("NoteID");
	
	if (noteID == null || noteID.length() == 0) {
		GenRow row = new GenRow();
		row.setViewSpec("NoteView");
		row.setRequest(request);
		row.setParameter("FirstName","Anonymous");
		row.setParameter("LastName","WebUser");
		row.setParameter("Subject","WebTracker");
		row.doAction("selectfirst");
		
		noteID = row.getString("NoteID");
	}

	out.println("var noteID = '" + noteID + "';");
%>

	function 	responseTracker(response) {
		responseTracker(response, noteID);
	}
	
	function	responseTracker(response, id) {
		if (id == null || id.length == 0) id = noteID;
		if (id == null || id.length == 0) {
			if (typeof console != 'undefined') console.log("NoteID is blank for " + response);
		} else {
			$.post("<%= contextPath %>/actions/setresponse.jsp",{'NoteID' : id, 'response' : encodeURIComponent(response), '-redirect' : ''},function(data){});
		}
	}
	
	function	responseContactTracker(response, id) {
		$.post("<%= contextPath %>/actions/setresponse.jsp",{'ContactID' : id, 'response' : encodeURIComponent(response), '-redirect' : ''},function(data){});
	}	