/**
 * 
 */
 
function Product(id, name, description, baseCost, margin, cost, tax, totalCost, variablePrice, pricingType, taxExempt) {
	this.id = id;
	this.name = name;
	this.description = description;
	this.baseCost = new Number(baseCost);
	this.margin = new Number(margin);
	this.cost = new Number(cost);
	this.tax = new Number(tax);
	this.totalCost = new Number(totalCost);
	this.variablePrice = variablePrice;
	this.pricingType = pricingType;
	this.taxExempt = taxExempt;
}

var productList = new Array();
productList.keys = new Array();

productList.addProduct = function(id, product){ 
   productList.keys[productList.keys.length] = id;
   productList[id] = product;
}

productList.getProduct = function (productID) {
   return productList[productID];
}

productList.getProductAt = function (index) {
   return productList.getProduct(productList.keys[index]);
}

productList.size = function () {
   return productList.keys.length;
}

/**
 * 
 */
function productSelected(productID) {
   var product = productList.getProduct(productID);
   
	var quantity = new Number(document.getElementById("quantity").value);
	
	document.getElementById("itemCost").value = formatCurrency(product.cost);
	document.getElementById("margin").value = formatNumber(product.margin, 5);
	
	document.getElementById("baseCost").value = formatCurrency(product.baseCost * quantity);
	
	document.getElementById("cost").value = formatCurrency(product.cost * quantity);
	document.getElementById("tax").value = formatCurrency(product.tax * quantity);
	document.getElementById("totalCost").value = formatCurrency(product.totalCost * quantity);	
	
	document.getElementById("taxExempt").value = product.taxExempt;	
	if (product.variablePrice == "Y") {
	   document.getElementById("cost").disabled = false;
	   document.getElementById("tax").disabled = false;
	   document.getElementById("totalCost").disabled = false;
	}
	else {
	   document.getElementById("cost").disabled = true;
	   document.getElementById("tax").disabled = true;
	   document.getElementById("totalCost").disabled = true;
	}
}

function isTaxExemp() {
	return document.getElementById("taxExempt").value == "Y";
}

function quantityModified(strQuantity) {
	var quantity = new Number(strQuantity);
	var itemCost = new Number(document.getElementById("itemCost").value);
	var taxRate = new Number(document.getElementById("taxRate").value);
	var taxExempt = isTaxExemp();
	
	var cost = itemCost * quantity;
	var tax = cost * taxRate;
	if (taxExempt) {
	   tax = 0;
	}
	var totalCost = cost + tax;
	
	document.getElementById("cost").value = cost;
	document.getElementById("tax").value = tax;
	document.getElementById("totalCost").value = totalCost;
}

function baseCostModified(strBaseCost) {
	var taxExempt = isTaxExemp();
	var margin = new Number(document.getElementById("margin").value);
	var baseCost = new Number(strBaseCost);
	
	var cost = baseCost / (1 - margin);
	document.getElementById("cost").value = formatCurrency(cost);
	
	if (taxExempt) {
		document.getElementById("tax").value = formatCurrency("0");
		document.getElementById("totalCost").value = formatCurrency(cost);
	}
	else {
		var taxRate = new Number(document.getElementById("taxRate").value);
		var tax = cost * taxRate;
		document.getElementById("tax").value = formatCurrency(tax);
		document.getElementById("totalCost").value = formatCurrency(cost + tax);
	}
	
	if (typeof changeDisplayPrice == 'function') { changeDisplayPrice(); }
}


function costModified(strCost) {
	var quantity = "1";
   if (document.getElementById("quantity") != null) {
      quantity = new Number(document.getElementById("quantity").value);
   }
   
	var cost = new Number(strCost) * quantity;
	var taxExempt = isTaxExemp();
	var baseCost = new Number(document.getElementById("baseCost").value);
	
	var margin = new Number(document.getElementById("margin").value);
	if (cost > 0) {
	   margin = 1 - (baseCost / cost) ;
	}
	
	setItemCost(cost);
	
	document.getElementById("margin").value = formatNumber(margin, 5);
	setMarginDisplay(margin);
	
	if (taxExempt) {
		document.getElementById("tax").value = formatCurrency("0");
		document.getElementById("totalCost").value = formatCurrency(cost);
	}
	else {
		var taxRate = new Number(document.getElementById("taxRate").value);
		var tax = cost * taxRate;
		document.getElementById("tax").value = formatCurrency(tax);
		document.getElementById("totalCost").value = formatCurrency(cost + tax);
	}
	if (typeof changeDisplayPrice == 'function') { changeDisplayPrice(); }
}

function setItemCost(cost) {
   if (document.getElementById("itemCost") != null) {
	   var quantity = new Number(document.getElementById("quantity").value);
	   if (quantity > 0) {
         document.getElementById("itemCost").value = formatCurrency(cost/quantity);
      }
	}
}

function taxModified(strTax) {
	costModified(document.getElementById("cost").value);
}


function totalCostModified(strTotalCost) {
	var quantity = "1";
   if (document.getElementById("quantity") != null) {
      quantity = new Number(document.getElementById("quantity").value);
   }
   
	var taxExempt = isTaxExemp();
	var totalCost = new Number(strTotalCost) * quantity;
	var cost;
	
	if (taxExempt) {
		cost = totalCost;
		document.getElementById("tax").value = formatCurrency("0");
	}
	else {
	   if (document.getElementById("taxRate") != null) {
   		var taxRate = new Number(document.getElementById("taxRate").value);
   		var cost = totalCost / (1 + taxRate);
   		document.getElementById("tax").value = formatCurrency(cost * taxRate);
   	}
	}
	if (document.getElementById("cost") != null) {
	   document.getElementById("cost").value = formatCurrency(cost);
	}
	
	var baseCost = 0;
	if (document.getElementById("baseCost") != null) {
	   baseCost = new Number(document.getElementById("baseCost").value);
	}
	
	if (document.getElementById("margin") != null) {
   	var margin = new Number(document.getElementById("margin").value);
   	if (cost > 0) {
   	   margin = 1 - (baseCost / cost) ;
   	}
	   document.getElementById("margin").value = formatNumber(margin, 5);
	}
	setItemCost(cost);
	
	setMarginDisplay(margin);

	if (typeof changeDisplayPrice == 'function') { changeDisplayPrice(); }
}


function marginModified(strMargin) {
	var margin = new Number(strMargin);
	var taxExempt = isTaxExemp();
	var baseCost = new Number(document.getElementById("baseCost").value);
	
	var cost = baseCost / (1 - margin);
	
	document.getElementById("cost").value = formatCurrency(cost);
	
	if (taxExempt) {
		document.getElementById("tax").value = formatCurrency("0");
		document.getElementById("totalCost").value = formatCurrency(cost);
	}
	else {
		var taxRate = new Number(document.getElementById("taxRate").value);
		var tax = cost * taxRate;
		document.getElementById("tax").value = formatCurrency(tax);
		document.getElementById("totalCost").value = formatCurrency(cost + tax);
	}
}

function marginDisplayModified(strMargin) {
	var margin = new Number(strMargin) / 100;
	document.getElementById("margin").value = formatNumber(margin, 5);
	marginModified(new String(margin));
	if (typeof changeDisplayPrice == 'function') { changeDisplayPrice(); }
}

function taxExemptChange() {
	costModified(document.getElementById("cost").value);
}

function setMarginDisplay(margin) {
	var element = document.getElementById("-margin");
	if (element != null) {
		element.value = formatNumber(margin*100,2);
	}
}

/*function updateDisplayFields(exclude) {
	var fields = new Array("baseCost","itemCost","margin","cost","tax","totalCost","quantity");
	var element;
	
	for (i=0; i< fields.length; i++) {
		if (exclude != fields[i]) {
			element = document.getElementById("-"+fields[i]);
			if (element != null) {
				if (fields[i] == "margin") {
					element.value = new Number(document.getElementById(fields[i]).value) * 100;
				}
				else {
					element.value = document.getElementById(fields[i]).value
				}
			}
		}
	}
}*/

function formatCurrency(cvalue) {
   return formatNumber(cvalue, 2)
	/*var stringValue = new String(cvalue);
	var pointIndex = stringValue.indexOf(".");
	
	if(pointIndex < 0) { //no point
		return(stringValue+".00");
	}
	else if(pointIndex == 0) { //point in front
		return(formatCurrency(new String("0")+stringValue));
	}
	else if(pointIndex == (stringValue.length - 1) ) { //end in point
		return(stringValue+"00");
	}	
	else if(pointIndex == (stringValue.length - 2) ) { //one dec place
		return(stringValue+"0");
	}
	else if(pointIndex == (stringValue.length - 3) ) {	//just right exit
		return(stringValue);
	}
	else if(pointIndex < (stringValue.length - 3) ) { //may need rounding
		tenx = cvalue * 100;
		tenx = Math.round(tenx);
		tenx = tenx / 100;
		stringValue = new String(tenx);
		return(formatCurrency(stringValue));
	}	*/
}

function getZeros(num) {
   var newValue = "";
   for(i=0; i < num; i++) {
      newValue = newValue + "0";
   }
   return newValue;
}

function formatNumber(cvalue, decimalPlaces) {
	var stringValue = new String(cvalue);
	var pointIndex = stringValue.indexOf(".");
	var result = 0;
	
	if(pointIndex < 0) { //no point
		result = stringValue + "." + getZeros(decimalPlaces);
	}
	else if(pointIndex == 0) { //point in front
		result = formatNumber("0"+stringValue, decimalPlaces);
	}
	else {
	   var numPlaces = stringValue.length - pointIndex -1;
	   if (cvalue < 0) {
	      numPlaces = numPlaces
	   }
	   if (numPlaces == decimalPlaces) { //just right exit
	      result = stringValue;
	   }
	   else if (numPlaces < decimalPlaces) { // append zeroes
	      result = stringValue + getZeros(decimalPlaces - numPlaces);
	   }
	   else { // round off
   		tenx = cvalue * Math.pow(10, decimalPlaces);
   		tenx = Math.round(tenx);
   		tenx = tenx / Math.pow(10, decimalPlaces);
   		stringValue = new String(tenx);
   		result = stringValue;
   		return(formatNumber(stringValue, decimalPlaces));
	   }
	}
	return result;
	
}