	function doOplink(contactid)
	{
		var linkform = this.document.forms[contactid];
		var configform = this.document.linkconfig;
		var role = configform.Role.value;

		if(role.length==0)
		{
			alert("A contact role must be entered");
			linkform.linked.checked = false;
			configform.Role.focus();
		}
		else
		{
			linkform.Role.value = role;
			linkform.submit();
		}
	}

	function dolink(contactid)
	{
		var linkform = this.document.forms[contactid];
		if(linkform.linked.checked)
		{
			setlink(linkform,contactid);
		}
		else
		{
			unsetlink(linkform,contactid);
		}
	}

	function setlink(linkform,contactid)
	{
		var configform = this.document.linkconfig;
		//var statusid = configform.StatusID.options[configform.StatusID.selectedIndex].value;
		var linktype = configform.LinkTypeID.options[configform.LinkTypeID.selectedIndex].value;
		if(linktype.length==0)
		{
			alert("A link type must be selected");
			linkform.linked.checked = false;
			configform.LinkTypeID.focus();
		}
		else
		{
			//linkform.StatusID.value = statusid;
			linkform.LinkTypeID.value = linktype;
			linkform.submit();
		}
	}

	function unsetlink(linkform,contactid)
	{
		linkform.submit();
	}