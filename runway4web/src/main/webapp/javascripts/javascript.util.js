/**
 * Analagous to java.util.Map interface
 *
 * An object that maps keys to values. A map cannot contain duplicate keys; each key can map to at most one value.
 */
function Map(top) {
   var key = null;
   var value = null;
   var lessThan = null;
   var greaterThan = null;
}

Map.prototype.clear = mapClear;
Map.prototype.containsKey = mapContainsKey;
Map.prototype.containsValue = mapContainsValue;
Map.prototype.put = mapPut;
Map.prototype.get = mapGet;
Map.prototype.remove = mapRemove;
Map.prototype.isEmpty = mapIsEmpty;
Map.prototype.size = mapSize;
Map.prototype.keySet = mapKeySet;
Map.prototype.values = mapValues;
Map.prototype.putAll = mapPutAll;

/**
 * Associates the specified value with the specified key in this map
 */
function mapPut(key, value) {
   if (key != null) {
      if (this.key == null || this.key == key) {
         this.key = key;
         this.value = value;
      }
      else if (key < this.key) {
         if (this.lessThan == null) {
            this.lessThan = new Map();
         }
         this.lessThan.put(key, value);
      }
      else {
         if (this.greaterThan == null) {
            this.greaterThan = new Map();
         }
         this.greaterThan.put(key, value);
      }
   }
}

/**
 * Returns the value to which this map maps the specified key.
 */
function mapGet(key) {
   if (key == null ) {
      return null;
   }
   else if (this.key == null || this.key == key) {
      return this.value;
   }
   else if (key < this.key) {
      if (this.lessThan != null) {
         return this.lessThan.get(key);
      }
      else {
         return null;
      }
   }
   else {
      if (this.greaterThan != null) {
         return this.greaterThan.get(key);
      }
      else {
         return null;
      }
   }
}

/**
 * Returns true if this map contains a mapping for the specified key.
 */
function mapContainsKey(key) {
   return key != null && this.get(key) != null;
}

/**
 * Returns true if this map maps one or more keys to the specified value.
 */
function mapContainsValue(value) {
   if (this.value == value) {
      return true;
   }
   else if (this.lessThan != null && this.lessThan.containsValue(value)) {
      return true;
   }
   else if (this.greaterThan != null && this.greaterThan.containsValue(value)) {
      return true;
   }
   return false;
}

/**
 * Removes the mapping for this key from this map if it is present
 */
function mapRemove(key) {
   if (key != null && this.key != null) {

      if (this.key == key) {
         if (this.lessThan != null) {
            this.key = this.lessThan.key;
            this.value = this.lessThan.value;
            this.lessThan.remove(this.lessThan.key);
            if (this.lessThan.key == null || this.key == this.lessThan.key) {
               this.lessThan = null;
            }
         }
         else if (this.greaterThan != null) {
            this.key = this.greaterThan.key;
            this.value = this.greaterThan.value;
            this.greaterThan.remove(this.greaterThan.ke);
            if (this.greaterThan.key == null || this.key == this.greaterThan.key) {
               this.greaterThan = null;
            }
         }
         else {
            this.key = null;
         }
      }
      else if (key < this.key) {
         if (this.lessThan != null) {
            this.lessThan.remove(key);
            if (this.lessThan.key == null) {
               this.lessThan = null;
            }
         }
      }
      else if (this.greaterThan != null) {
         this.greaterThan.remove(key);
         if (this.greaterThan.key == null) {
            this.greaterThan = null;
         }
      }
   }
}

/**
 * Returns true if this map contains no key-value mappings.
 */
function mapIsEmpty(key) {
   return this.key == null;
}

/**
 * Returns the number of key-value mappings in this map.
 */
function mapSize() {
   count = 0;
   if (this.key != null) {
      count = count + 1;
      if (this.lessThan != null) {
         count = count +  this.lessThan.size();
      }
      if (this.greaterThan != null) {
         count = count +  this.greaterThan.size();
      }
   }
   return count;
}

/**
 * Returns an Array of the keys contained in this map.
 */
function mapKeySet(array) {
   if (array == null) {
      array = new Array();
   }
   if (this.key != null) {
      array[array.length] = this.key;
      if (this.lessThan != null) {
         this.lessThan.keySet(array);
      }
      if (this.greaterThan != null) {
         this.greaterThan.keySet(array);
      }
   }
   return array;
}

/**
 * Returns an Array of the values contained in this map.
 */
function mapValues(array) {
   if (array == null) {
      array = new Array();
   }
   array[array.length] = this.value;
   if (this.lessThan != null) {
      this.lessThan.values(array);
   }
   if (this.greaterThan != null) {
      this.greaterThan.values(array);
   }
   return array;
}

/**
 * Removes all mappings from this map
 */
function mapClear() {
   if (this.lessThan != null) {
      this.lessThan.clear();
      this.lessThan = null;
   }
   if (this.greaterThan != null) {
      this.greaterThan.clear();
      this.greaterThan = null;
   }
   this.key = null;
   this.value = null;
}

/**
 * Copies all of the mappings from the specified map to this map
 */
function mapPutAll(map) {
   if (map != null) {
      keySet = map.keySet();
      for (i=0; i < keySet.length; i++) {
         this.put(keySet[i],map.get(keySet[i]));
      }
   }
}