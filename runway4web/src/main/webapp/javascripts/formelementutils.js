/**
 * This function switches a form select element between
 * single and multi select modes
 */
function switchSelectMode(select, image, size) {
   
   var multiSize = 4;
   if (typeof(size) != "undefined") {
      multiSize = size;
   }
   select.multiple = !select.multiple;
   if (select.multiple) {
      select.size = multiSize;
      if (image != null) {
         image.src = image.src.replace("_expand","_contract");
         image.alt = "switch to single selection mode";
         image.title = "switch to single selection mode";
      }
   }
   else {
      select.size = 1;
      if (image != null) {
         image.src = image.src.replace("_contract","_expand");
         image.alt = "switch to multiple selection mode";
         image.title = "switch to multiple selection mode";
      }
   }
}

function buildSelectModeSwitchOld(contextPath, formName, elementName, size) {
   var multiSize = 4;
   if (typeof(size) != "undefined") {
      multiSize = size;
   }
   document.write("<img style=\"vertical-align: top; cursor: pointer;\" ");
   document.write("onClick=\"switchSelectMode(document.forms['" + formName + "'].elements['" + elementName + "'],this," + multiSize + ")\" ");
   document.write("alt=\"switch to multiple selection mode\" ");
   document.write("title=\"switch to multiple selection mode\" ");
   document.write("src=\"" + contextPath + "/images/icons/form/select_expand.gif\"/>");
}

function buildSelectModeSwitch(contextPath, elementID, size) {
      var multiSize = 4;
      if (typeof(size) != "undefined") {
         multiSize = size;
      }
      document.write("<img style=\"vertical-align: top; cursor: pointer;\" ");
      document.write("onClick=\"switchSelectMode(document.getElementById('" + elementID + "'),this," + multiSize + ")\" ");
      document.write("alt=\"switch to multiple selection mode\" ");
      document.write("title=\"switch to multiple selection mode\" ");
      document.write("src=\"" + contextPath + "/images/icons/form/select_expand.gif\"/>");
}


/**
 * This function toggles the omit functionality 
 * on a relationship via hidden input
 */
var omitInputs = new Array();
var idCounter = 0;

function buildOmitToggle(contextPath, elementID) {

   var element = document.getElementById(elementID);
   if (element != null) {
      var idArray = omitInputs[element.name];
   
      if (idArray == null) {
         document.write("<input type=\"hidden\" ");
         document.write("name=\"" +element.name+ "\" ");
         document.write("value=\"\" />");
         idArray = new Array();
         omitInputs[element.name] = idArray;
      }
      idArray[idArray.length] = "omitImage"+idCounter;
      
      document.write("<img id=\"omitImage" + idCounter + "\" ");
      document.write("onClick=\"toggleOmit('" +elementID+ "');\" ");
      document.write("style=\"vertical-align: top; cursor: pointer;\" ");
      document.write("name=\"-CompanyCompanyStatus\" ");
      document.write("title=\"including these options, click to omit\" ");
      document.write("alt=\"including these options, click to omit\" ");
      document.write("src=\"" + contextPath + "/images/icons/form/include.gif\" />");
      
      idCounter++;
   }
}

function toggleOmit(elementID) {
   var idArray = null;
   var element = document.getElementById(elementID);
   if (element != null) {
      if (element.value != "") {
         element.value = "";
         
         idArray = omitInputs[element.name];
         var image = null;
         for (i=0; i < idArray.length; i++) {
            image = document.getElementById(idArray[i]);
            image.src = image.src.replace("omit.gif","include.gif");
            image.title = "including these options, click to omit";
            image.alt = "including these options, click to omit";
         }
      }
      else {
         element.value = "omit";
         
         idArray = omitInputs[element.name];
         var image = null;
         for (i=0; i < idArray.length; i++) {
            image = document.getElementById(idArray[i]);
            image.src = image.src.replace("include.gif","omit.gif");
            image.title = "omitting these options from search, click to include";
            image.alt = "omitting these options from search, click to include";
         }
      }
   }
}


/**
 * This function switches a form element between
 * a select and text input
 *
 * Passing [optionally] a Name for the TextInput allows the text element reltionship to be different
 * ie. Searching on CampiagnID by select and Campaign.Name via text input
 */
function buildSelectTextSwitch(contextPath, elementID, textName) {
   var element = document.getElementById(elementID);
   if (element != null) {
      document.write("<input id=\"");
      document.write("input"+elementID);
      document.write("\" type=\"text\" name=\"--ignore--\" style=\"display: none;\" value=\"\" >");
      
      document.write("&nbsp;<img style=\"vertical-align: top; cursor: pointer;\" ");
      if (textName == null) {
         document.write("onClick=\"switchSelectTextMode(document.getElementById('" + elementID + "'),this)\" ");
      }
      else {
         document.write("onClick=\"switchSelectTextModeByLabel(document.getElementById('" + elementID + "'),this,'" +element.name+ "','" +textName+ "')\" ");
      }
      document.write("alt=\"switch to text input mode\" ");
      document.write("title=\"switch to select mode\" ");
      document.write("src=\"" + contextPath + "/images/icons/form/select_to_text.gif\"/>");
   }
}

function switchSelectTextMode(element, image) {
   if (element != null) {
      var inputElement = document.getElementById("input"+element.id);
      var tempValue = inputElement.value;
      var textMode = (inputElement.style.display == "inline");
      if (textMode) {
         inputElement.style.display = "none";
         element.style.display = "inline";
         element.name = inputElement.name;
         inputElement.name = "--ignore--"
         var found = false;
         if (tempValue.length > 0) {
            for (i=0; !found && i < element.options.length; i++) {
               if (tempValue == element.options[i].value) {
                  element.selectedIndex = i;
                  found = true;
               }
            }
            if (!found) {
               element.options[i] = new Option(tempValue,tempValue);
               element.selectedIndex = i;
            }
         }
         else {
            element.selectedIndex=0;
         }
         inputElement.value = "";
         image.src = image.src.replace("text_to_select.gif","select_to_text.gif");
         image.title = "switch to text mode";
         image.alt = "switch to text mode";
      }
      else {
         element.style.display = "none";
         inputElement.style.display = "inline";
         inputElement.name = element.name;
         element.name = "--ignore--"
         if(element.selectedIndex != null && element.selectedIndex > -1) {
            inputElement.value = element.options[element.selectedIndex].value
         }
         element.selectedIndex = 0;
         
         image.src = image.src.replace("select_to_text.gif","text_to_select.gif");
         image.title = "switch to select mode";
         image.alt = "switch to select mode";
      }
   }
}


function switchSelectTextModeByLabel(element, image, selectName, textName) {
   if (element != null) {
      var inputElement = document.getElementById("input"+element.id);
      var tempValue = inputElement.value;
      var textMode = (inputElement.style.display == "inline");
      if (textMode) {
         inputElement.style.display = "none";
         element.style.display = "inline";
         element.name = selectName;
         inputElement.name = "--ignore--"
         if (tempValue.length > 0) {
            for (i=0; i < element.options.length; i++) {
               if (tempValue == element.options[i].text) {
                  element.selectedIndex = i;
               }
            }
         }
         else {
            element.selectedIndex=0;
         }
         inputElement.value = "";
         image.src = image.src.replace("text_to_select.jpg","select_to_text.gif");
         image.title = "switch to text mode";
         image.alt = "switch to text mode";
      }
      else {
         element.style.display = "none";
         inputElement.style.display = "inline";
         inputElement.name = textName;
         element.name = "--ignore--"
         if(element.selectedIndex != null && element.selectedIndex > -1) {
            inputElement.value = element.options[element.selectedIndex].text
         }
         element.selectedIndex = 0;
         
         image.src = image.src.replace("select_to_text.gif","text_to_select.jpg");
         image.title = "switch to select mode";
         image.alt = "switch to select mode";
      }
      alert(inputElement.name);
      alert(element.name);
   }
}

/**
 * This function switches a form element between
 * a select and text input
 */
function buildDatePicker(src, elementID, nostring) {
   var element = document.getElementById(elementID);
   if (element != null) {
      document.write("<img onClick=\"show_calendar('getElementById(\\'" +elementID+ "\\')');\" src=\"" + src + "\" align=\"absmiddle\" title=\"use popup selector\" border=\"0\">");
      if(!nostring) document.write(" <span class=\"detailsmall\">dd/mm/yyyy</span>");
   }
}



function buildVisibilityToggle(contextPath, elementID, expanded) {   
      
   document.write("<img style=\"cursor: pointer;\" id=\"img" + elementID + "\" ");
   document.write("onClick=\"toggleVisibility(document.getElementById('" + elementID + "'))\" ");
   if (expanded) {
      document.write("alt=\"collapse\" ");
      document.write("title=\"collapse\" ");
      document.write("src=\"" + contextPath + "/images/interface/collapse_icon.gif\"/>");
   }
   else {
      document.write("alt=\"expand\" ");
      document.write("title=\"expand\" ");
      document.write("src=\"" + contextPath + "/images/interface/expand_icon.gif\"/>");
   }
}

function buildVisibilityTextToggle(contextPath, elementID, expanded) {
   document.write("<a href=\"javascript:toggleVisibility(document.getElementById('" + elementID + "'))\">");
   if (expanded) {
      document.write("<span id=\"text" + elementID + "\">Collapse</span>");
   }
   else {
      document.write("<span id=\"text" + elementID + "\">Expand</span>");
   }
   document.write("</a>");
}

function toggleVisibility(element) {
   var display = true;
   if (element.style.display == "none") {
      element.style.display = "";
   }
   else {
      element.style.display = "none";
      display = false;
   }
   var span = document.getElementById("text"+element.id);
   if (span != null) {
      if (display) {
         span.innerHTML="Collapse";
      }
      else {
         span.innerHTML="Expand";
      }
   }
      
   var image = document.getElementById("img"+element.id);
   if (image != null) {
      if (display) {
         image.alt="collapse";
         image.title="collapse";
         image.src = image.src.replace("expand","collapse");
      }
      else {
         image.alt="expand";
         image.title="expand";
         image.src = image.src.replace("collapse","expand");
      }
   }
}

function toggleCheckbox(formobj, contains){
		var formeles = formobj.elements;
		var setvalue = null;
		for(var i=0; i<formeles.length; i++){
			if (typeof(formeles[i].name) != "undefined" && formeles[i].name.indexOf(contains)>=0) {
				if (typeof(formeles[i].type) != 'undefined' && formeles[i].type == "checkbox") {
					if(setvalue == null){
						setvalue = !formeles[i].checked;
					}
					formeles[i].checked = setvalue;
				}
			}
		}
}