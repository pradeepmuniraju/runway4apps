function changeImages()
{
	for (var i=0; i<changeImages.arguments.length; i+=2)
	{
		document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
	}
}

//toggle Y N
function toggleActive(id) {
   var inputObj = document.getElementById('input'+id);
   var imageObj = document.getElementById('image'+id);
   
   if (inputObj != null) {
      if (inputObj.value == "Y") {
         inputObj.value = "N";
      }
      else {
         inputObj.value = "Y";
      }
   }
   else{
	   alert("Could not find input");
   }   
   if (imageObj != null) {
	   var tempimage = new Image();
      if(inputObj.value == "Y") {
	   	  tempimage.src = imageObj.src.replace("Inactive","Active");
	   	imageObj.src = tempimage.src;
	   	imageObj.title = "Currently Active, click to toggle";
      }
      else {
    	  tempimage.src = imageObj.src.replace("Active","Inactive");
    	  document.getElementById('image'+id).src = tempimage.src;
    	  imageObj.title = "Currently Inactive, click to toggle";
      }
   }
   else{
	   alert("Could not find image");
   }
   sendForm(id, inputObj.form);
}

//toggle Active Inactive
function toggleActiveInactive(id) {
	   var inputObj = document.getElementById('input'+id);
	   var image = document.getElementById('image'+id);
	   
	   if (inputObj != null) {
	      if (inputObj.value == "Active") {
	         inputObj.value = "Inactive";
	      }
	      else {
	         inputObj.value = "Active";
	      }
	   }

	   if (image != null) {
		   var tempimage = new Image();
	      if(inputObj.value == "Active") {
	    	  tempimage.src = image.src.replace("Inactive","Active");
	    	  image.src = tempimage.src;
	         image.title = "Currently Active, click to toggle";
	      }
	      else {
	    	  tempimage.src = image.src.replace("Active","Inactive");
	    	  image.src = tempimage.src;
	         image.title = "Currently Inactive, click to toggle";
	      }
	   }

	   sendForm(id, inputObj.form);
}

function toggleStatus(id, imagepath, prefix, inputname)
{
	var formname = "form" + id;
	var imagename = "image" + id;
	var thisform = document.forms[formname];
	var nextvalue = "";

	var inputobj = null;
	if(arguments.length==2)
	{
		inputobj = thisform.Status;
	}
	else
	{
		inputobj = thisform.elements[inputname];
	}

   if (inputname=="Active") {
   	if(inputobj.value=="Y") {
   		nextvalue = "N";
   	}
   	else {
   		nextvalue ="Y";
   	}
   }
   else {
   	if(inputobj.value=="Active") {
   		nextvalue = "Inactive";
   	}
   	else {
   		nextvalue = "Active";
   	}
   }
	document.images[imagename].title = "Currently " + nextvalue + ", click to toggle";

	inputobj.value = nextvalue;

	var imageurl = "";
	if(arguments.length==2)
	{	
		imageurl = imagepath+nextvalue+"_icon.gif";
	}
	else
	{
		imageurl = imagepath+prefix+nextvalue+"_icon.gif";
	}
	thisform.submit();
	changeImages(imagename, imageurl);
}

var ltgray = "#EEEEEE";
var white = "#FFFFFF";

// Change row color when MouseOver occurs
function rowOn(objid)
{
	if(objid.style)
	{
		objid.style.backgroundColor = white;
	}
}

function rowOff(objid)	
{
	if(objid.style)
	{
		objid.style.backgroundColor = ltgray;
	}
}