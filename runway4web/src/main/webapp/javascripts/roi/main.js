/**
 * Each stage in the ROI builder page is identified in the #step-SS format where SS is the step number.
 * In other words, the steps are identified as #step-1, #step-2, etc. Stage and Step are synonyms in
 * this context.
 *
 * Each step can have many components. Each component is identified in the #component-SS-CC format
 * where SS is the step that the component belongs to and CC is the number of the component. As an
 * example, components of the first step are identified as: #component-1-1, #component-1-2, etc.
 *
 * The SS-CC number for each component is called its coordinates and variable names like coord
 * in the code refer to these coordinates.
 *
 * @author Behrang Saeedzadeh 
 */

var FADE_IN_SPEED = 1500;
var FAST_FADE_IN_SPEED = 600;
var VERY_FAST_FADE_IN_SPEED = 200;
var ROLLED_UP = false;
var NEW_MODE = 'NEW_MODE';
var VIEW_MODE = 'VIEW_MODE';

$(document).ready(init);

function init() {
    var queryString = window.top.location.search.substring(1);
    var mode = NEW_MODE;
    if (queryString != null && queryString.indexOf("roiReportId") >= 0) {
        mode = VIEW_MODE;
    } else {
        $(".count-count-hidden").val("");
    }
    $("#report-name-field").focus();

    initMainFields();

    $(".add-component").click(addComponent);

    // Prepares a dialog for selecting the number of companies/contacts involved in the
    // stage. When the user selects one of the items and presses the OK button, the
    // form refreshes to update the values according to the new count.
    $("#select-stage-count-dialog").dialog({modal: true, autoOpen: false,
        buttons: {
            "OK": function() {
                var stepNo = $("#selected-step-no").val();
                var countMode = $("#select-stage-count-dialog input:radio:checked").val();
                var actualCount;
                if (countMode == 'Company') {
                    actualCount = $("#company-stage-count").text();
                } else if (countMode == 'Contact') {
                    actualCount = $("#contact-stage-count").text();
                }
                // $("#step-" + stepNo + " .no-actual-units-span").val(actualCount);
                $("#step-" + stepNo + " .no-actual-units-span").each(function(index, elem) {
                    var id = $(elem).attr("id");
                    var coord = id.substring("no-actual-units-".length);
                    if (!("manual" == $("#no-actual-units-type-" + coord).val())) {
                        $(elem).val(actualCount)
                    }                    
                });
                $("#step-count-" + stepNo).val(actualCount);
                $("#step-count-mode-" + stepNo).val(countMode);
                updateStepValues(stepNo);
                updateActualCostSubtotal(stepNo);
                updateGrandActualCost();
                updateGrandDifferences();
                $(this).dialog('close');
            },
            "Cancel": function() {
                $(this).dialog('close');
            }
        }
    });

    // Prepares a dialog for letting the user select the number of actual units involved in a component
    // manually. When the user selects the actual number of a component manually, changing the stage count
    // won't affect the actual number for that component anymore.
    $("#edit-actual-unit-count-dialog").dialog({modal: true, autoOpen: false,
        buttons: {
            "OK": function() {
                var coord = $("#component-coord").val();
                var stepNo = coord.substring(0, coord.indexOf("-"));

                $("#no-actual-units-" + coord).val($("#edit-actual-unit-count-field").val());
                $("#no-actual-units-type-" + coord).val("manual");
                updateActualCost(coord);
                updateDifference(coord);
                updateActualCostSubtotal(stepNo);
                updateDifferenceSubtotal(stepNo);
                updateGrandActualCost();
                updateGrandDifferences();
                $(this).dialog('close');
            },
            "Cancel": function() {
                $(this).dialog('close');
            }
        }
    });

    $("#select-roi-count-dialog").dialog({modal: true, autoOpen: false, minWidth: 400, width: 400,
        buttons: {
            "OK": function() {
                var beanId = $("#select-roi-count-dialog input[type=radio]:checked").attr("value");
                $.ajax({
                    url: "ajax/beans.jsp",
                    data: {"BeanID": beanId},
                    dataType: "json",
                    success: function(resp) {
                        $("#roi-act-sale-count-" + $("#selected-row-item-no").val()).val(resp.count);
                        $("#roi-act-sale-bean-id-" + $("#selected-row-item-no").val()).val(resp.beanId);
                        updateRoiItemCost($("#selected-row-item-no").val());
                    },
                    async: false
                });
                $(this).dialog('close');
            },
            "Cancel": function() {
                $(this).dialog('close');
            }
        }
    });

    // Listens to clicks on the .stage-count-link links (there's one per each stage of the report)
    // and brings up the stage count dialog when clicked.
    $(".stage-count-link").click(function() {
        var elemId     = $(this).attr("id");
        var stepNo     = elemId.substring(elemId.lastIndexOf("-") + 1);
        var eventId    = $("#step-event-id-" + stepNo).val();
        var campaignId = $("#step-campaign-id-" + stepNo).val();
        $.ajax({
            url: "ajax/step_count.jsp",
            data: {"CampaignID": campaignId, "EventID": eventId},
            success: function(data) {                
                $("#company-stage-count").text(data.companyCount);
                $("#contact-stage-count").text(data.contactCount);
                $("#selected-step-no").val(stepNo);
                $("#select-stage-count-dialog").dialog("open");
            },
            dataType: "json"
        });        

        return false;
    });

    $("#add-roi-item").click(addRoiItem);

    $("#save-button").click(function() {
        $("#roi-form").submit();
        return false;
    });

    $("#roll-up-link").click(function() {
        if (ROLLED_UP) {
            ROLLED_UP = false;
            $(".row").fadeIn(FADE_IN_SPEED);
            $(".component").each(function(index, elem) {
                if (!($(elem).hasClass("footer"))) {
                    $(elem).fadeIn(FADE_IN_SPEED);
                }
            });
        } else {
            ROLLED_UP = true;
            $(".row").fadeOut(FAST_FADE_IN_SPEED);
            $(".component").each(function(index, elem) {
                if (!($(elem).hasClass("footer"))) {
                    $(elem).fadeOut(FAST_FADE_IN_SPEED);
                }
            });            
        }

        return false;
    });

    $("#tabs").tabs();

    if (mode == VIEW_MODE) {
        var stepElems = $(".report-step");
        for (var k = 0; k < stepElems.length; k++) {
            var stepId = $(stepElems[k]).attr("id");
            var stepNo = stepId.substring("step-".length);
            updateStepValues(stepNo)
        }

        var roiElems = $(".roi-row");
        for (var l = 0; l < roiElems.length; l++) {
            var roiId = $(roiElems[l]).attr("id");
            var roiNo = roiId.substring("roi-row-".length);
            updateRoiCost(roiNo);
            updateRoiItemCost(roiNo);
        }
        
    }
}

/**
 * Initializes event handling for Forecasted Sales Volume, Average Product Value, Forecasted Sales Amount,
 * GP %, and GP Value fields.
 */
function initMainFields() {
    $("#fc-sales-vol, #ave-prod-val, #gp-percent").keyup(updateMainFields);
}

function updateMainFields(e) {    
    var fcSalesVol = $("#fc-sales-vol").val();
    var aveProdVal = $("#ave-prod-val").val();
    var fcSalesAmount = fcSalesVol * aveProdVal;
    if (isNumber(fcSalesAmount)) {
        $("#fc-sales-amount").val(fcSalesAmount);
    }
    var gpPercent = $("#gp-percent").val();
    var gpVal = (gpPercent * fcSalesAmount) / 100.0;
    if (isNumber(gpVal)) {
        $("#gp-val").val(gpVal);
    }    
}

function updateDifference(coord) {
    var forecastCost = parseFloat($("#forecast-cost-" + coord).val());
    var actualCost = parseFloat($("#component-cost-" + coord).text());
    var diff = actualCost - forecastCost;
    var diffPercent = diff / actualCost;
    if (!isNaN(diff)) {
        $("#component-diff-" + coord).text(diff.toFixed(2));
    }
    if (!isNaN(diffPercent)) {
        $("#component-diff-percent-" + coord).text((diffPercent.toFixed(2) * 100) + "%");
    }    
}

// Updates forecast cost and actual cost for all the components of the
// given step. Also updates the sub-total values for the step.
function updateStepValues(stepNo) {
    var comps = $("#step-" + stepNo + " .component");
    for (var i = 0; i < comps.length; i++) {
        var comp = comps[i];
        if (!$(comp).hasClass("footer") && !$(comp).hasClass("header")) {
            var compId = $(comp).attr("id");
            var coord = compId.substring(compId.indexOf("-") + 1);
            updateForecastCost(coord);
            updateActualCost(coord);
            updateDifference(coord);
        }
    }
    updateForecastCostSubtoal(stepNo);
    updateActualCostSubtotal(stepNo);
    updateDifferenceSubtotal(stepNo);
    updateGrandActualCost();
    updateGrandForecastCost();
    updateGrandDifferences();    
}

/**
 * Updates the forecast cost of the component identified by the given component coordinates.
 *
 * @param coord The coordinate of the component that its forecast value will be updated.
 */
function updateForecastCost(coord) {
    var unitCost = parseFloat($("#unit-cost-" + coord).val());
    var unitForecasted = parseFloat($("#no-forecast-units-" + coord).val());
    var forecastCost = unitCost * unitForecasted;
    if (!isNaN(forecastCost)) {
        $("#forecast-cost-" + coord).val(forecastCost.toFixed(2));
    } else {
        $("#forecast-cost-" + coord).val("");
    }
}

/**
 * Updates the actual cost of the component identified by the given component coordinates. 
 *
 * @param coord The coordinate of the component that its actual value will be updated.
 */
function updateActualCost(coord) {
    var unitCost = parseFloat($("#unit-cost-" + coord).val());
    var actualUnits = parseFloat($("#no-actual-units-" + coord).val());
    var actualCost = unitCost * actualUnits;
    if (!isNaN(actualCost)) {
        $("#component-cost-" + coord).text(actualCost.toFixed(2));
    } else {
        $("#component-cost-" + coord).text("");
    }
}

function updateComponentValues(coord) {
    var stepNo = coord.substring(0, coord.indexOf("-"));
    updateForecastCost(coord);
    updateActualCost(coord);
    updateDifference(coord);
    updateSubtotals(stepNo);
}

/**
 * Updates the actual cost sub-total of the given step.
 *
 * @param stepNo The step that it actual cost sub-total will be updated.
 */
function updateActualCostSubtotal(stepNo) {
    var actualCostSub = 0;
    var actualCostElems = $("#step-" + stepNo + " .actual-cost-value");
    for (var i = 0; i < actualCostElems.length; i++) {
        var floatVal = parseFloat($(actualCostElems[i]).text());
        if (!isNaN(floatVal)) {
            actualCostSub += floatVal;
        }
    }
    $("#actual-subtotal-" + stepNo).text(actualCostSub.toFixed(2));
    updateGrandActualCost();
}

/**
 * Updates the forecast cost sub-total of the given step.
 * 
 * @param stepNo The step that its forecast cost sub-total will be updated.
 */
function updateForecastCostSubtoal(stepNo) {
    var forecastCostSub = 0;
    var forecastCostElems = $("#step-" + stepNo + " .forecast-cost-field");
    for (var i = 0; i < forecastCostElems.length; i++) {
        var floatVal = parseFloat($(forecastCostElems[i]).val());
        if (!isNaN(floatVal)) {
            forecastCostSub += floatVal;
        }
    }
    $("#forecast-subtotal-" + stepNo).text(forecastCostSub.toFixed(2));
    updateGrandForecastCost();
}

function updateDifferenceSubtotal(stepNo) {
    var actualSubtotal = parseFloat($("#actual-subtotal-" + stepNo).text());
    var forecastedSubtotal = parseFloat($("#forecast-subtotal-" + stepNo).text());
    var diff = actualSubtotal - forecastedSubtotal;
    var diffPercent = diff / actualSubtotal;

    if (isNumber(diff)) {
        $("#component-difference-" + stepNo).text(diff.toFixed(2));
    }
    if (isNumber(diffPercent)) {
        $("#component-difference-percent-" + stepNo).text((diffPercent.toFixed(2) * 100) + "%");
    }
}

function updateGrandDifferences() {

    var grandForecastCost = parseFloat($("#grand-forecasted-cost").text());
    var grandActualCost = parseFloat($("#grand-actual-cost").text());
    var grandDiff = grandActualCost - grandForecastCost;
    var grandDiffPercent = grandDiff / grandActualCost;
    if (isNumber(grandDiff)) {
        $("#grand-diff").text(grandDiff.toFixed(2));
    }
    if (isNumber(grandDiffPercent)) {
        $("#grand-diff-percent").text((grandDiffPercent.toFixed(2) * 100) + "%");
    }
}

function isNumber(num) {
    return !(num == Number.POSITIVE_INFINITY ||
            num == Number.NEGATIVE_INFINITY ||
            isNaN(num));
}

/**
 * Updates the forecast cost subtotal and actual cost subtotal of the given step.
 *
 * @param stepNo The step that its forecast cost sub-total and actual cost sub-total values will be updated.
 */
function updateSubtotals(stepNo) {
    updateActualCostSubtotal(stepNo);
    updateForecastCostSubtoal(stepNo);
    updateGrandDifferences();
}

function updateGrandActualCost() {
    var grandActualCost = 0;
    var actualCostElems = $(".actual-cost-value");
    for (var i = 0; i < actualCostElems.length; i++) {
        var floatVal = parseFloat($(actualCostElems[i]).text());
        if (!isNaN(floatVal)) {
            grandActualCost += floatVal;
        }
    }
    $("#grand-actual-cost").text(grandActualCost.toFixed(2));
}

function updateGrandForecastCost() {
    var grandForecastedCost = 0;
    var forecastCostElems = $(".forecast-cost-field");
    for (i = 0; i < forecastCostElems.length; i++) {
        var floatVal = parseFloat($(forecastCostElems[i]).val());
        if (!isNaN(floatVal)) {
            grandForecastedCost += floatVal;
        }
    }
    $("#grand-forecasted-cost").text(grandForecastedCost.toFixed(2));
}                                                                               

function updateRoiItemCost(itemNo) {
    var roiCount = parseInt($("#roi-row-count-field-" + itemNo).val());
    var roiItemCost = parseFloat($("#roi-row-cost-field-" + itemNo).val());
    var roiCost = roiItemCost / roiCount;
    if (!isNaN(roiCost)) {
        $("#roi-row-item-cost-field-" + itemNo).val(roiCost.toFixed(2));
    } else {
        $("#roi-row-item-cost-field-" + itemNo).val("");
    }
}

function updateRoiCost(itemNo) {
    var cost = 0;
    var roiCostCoords = $("#roi-row-selected-cost-coords-" + itemNo).val().split(",");
    for (var i = 0; i < roiCostCoords.length; i++) {
        var c = parseFloat($(".component-" + roiCostCoords[i] + " .actual-cost-value").text());
        if (!isNaN(c)) {
            cost += c;
        }
    }

    $("#roi-row-cost-field-" + itemNo).val(cost.toFixed(2));
}

function addComponent() {                                                                                         
    var stepId = $(this).attr("id");    
    var stepNo = stepId.substring(stepId.lastIndexOf("-") + 1);
    var compNo = $("#step-" + stepNo + " .component").length;
    $.ajax({
        url: "ajax/stage_cost.jsp",
        data: {stepNo: stepNo, compNo: compNo},
        success: function(resp) {
            var respHtml = $(resp);
            respHtml.find("#component-name-" + stepNo + "-" + compNo).change(componentNameChanged);
            respHtml.find("#no-actual-units-" + stepNo + "-" + compNo).val($("#step-count-" + stepNo).val());
            respHtml.find("#unit-cost-" + stepNo + "-" + compNo).keyup(unitCostChanged);            
            respHtml.find("#no-forecast-units-" + stepNo + "-" + compNo).keyup(noForecastUnitsChanged);
            respHtml.find("#edit-count-" + stepNo + "-" + compNo).click(editActualCountClicked);
            respHtml.insertBefore("#footer-" + stepNo);            
            $("#component-" + stepNo + "-" + compNo).fadeIn(FAST_FADE_IN_SPEED)
        }
    });
    return false;
}

var roiItemGlobal;

function addRoiItem() {
    var itemNo = $("#roi tr.roi-row").length;
    $.ajax({
        url: "ajax/roi_item.jsp",
        data: {itemNo: itemNo},
        success: function(resp) {
            var roiItem = $(resp.trim());
            roiItem.insertAfter("#roi > *:last");
//            roiItem.find("#roi-row-select-cost-link-" + itemNo).click(roiSelectLinkClicked);
//            roiItem.find("#roi-row-cancel-cost-link-" + itemNo).click(roiCancelLinkClicked);
//            roiItem.find("#roi-row-apply-cost-link-" + itemNo).click(roiApplyLinkClicked);
            roiItem.find("#edit-act-sale-count-" + itemNo).click(editRoiCountClicked);
//            roiItem.fadeIn(FAST_FADE_IN_SPEED);
        }
    });
    return false;
}

function editRoiCountClicked(e) {
    var id = $(e.currentTarget).attr("id");
    var itemNo = id.substring("edit-act-sale-count-".length);
    $("#selected-row-item-no").val(itemNo);
    $("#select-roi-count-dialog").dialog("open");
    return false;
}

function roiCancelLinkClicked(e) {
    $(".component-select-box").fadeOut(FAST_FADE_IN_SPEED);
    $(".component-select-box input[type=checkbox]").attr("checked", false);
    var id = $(e.target).attr("id");
    var itemNo = id.substring("roi-row-cancel-cost-link-".length);
    $("#roi-row-apply-cost-link-" + itemNo).hide();
    $("#roi-row-cancel-cost-link-" + itemNo).hide();
    $("#roi-row-select-cost-link-" + itemNo).fadeIn(VERY_FAST_FADE_IN_SPEED);    
    return false;
}

function roiSelectLinkClicked(e) {
    $(".component-select-box").fadeIn(FAST_FADE_IN_SPEED);
    $(".component-select-box input[type=checkbox]").attr("checked", false);
    var itemId = $(this).attr("id");
    var itemNo = itemId.substring("roi-row-select-cost-link-".length);
    $(this).hide();
    $("#roi-row-apply-cost-link-" + itemNo).show();
    $("#roi-row-cancel-cost-link-" + itemNo).show();

    return false;
}

function roiApplyLinkClicked(e) {
    var selectedCosts = $(".component-select-box input[type=checkbox]:checked");
    var coords = [];
    for (var i = 0; i < selectedCosts.length; i++) {
        var id = $(selectedCosts[i]).attr("id");
        var coord = id.substring("component-select-".length);
        coords.push(coord);
    }

    var costLabelIds = [];
    for (i = 0; i < coords.length; i++) {
        var costLabelId = $("#component-name-" + coords[i] + " option:selected").val();
        costLabelIds.push(costLabelId);
    }

    var cost = 0;
    for (i = 0; i < coords.length; i++) {
        cost += parseFloat($("#component-cost-" + coords[i]).text());
    }

    var itemId = $(this).attr("id");
    var itemNo = itemId.substring("roi-row-apply-cost-link-".length);

    $("#roi-row-cost-field-" + itemNo).val(cost.toFixed(2));
    $("#roi-row-selected-cost-coords-" + itemNo).val(coords.join(","));

    $(".component-select-box").fadeOut(FAST_FADE_IN_SPEED);
    $(".component-select-box input[type=checkbox]").attr("checked", false);
    $("#roi-row-apply-cost-link-" + itemNo).hide();
    $("#roi-row-cancel-cost-link-" + itemNo).hide();
    $("#roi-row-select-cost-link-" + itemNo).show();    

    updateRoiItemCost(itemNo);

    return false;
}

function unitCostChanged(e) {
    var id     = $(e.target).parent().parent().attr("id");
    var coord  = id.substring(id.indexOf("-") + 1);
    var stepNo = coord.substring(0, coord.indexOf("-"));
    updateForecastCost(coord);
    updateForecastCostSubtoal(stepNo);
    updateActualCost(coord);
    updateActualCostSubtotal(stepNo);
    updateDifference(coord);
    updateDifferenceSubtotal(stepNo);
    updateGrandDifferences();
}

/**
 * This event listener will be invoked when the user clicks on the icon for editing
 * the actual count of a component.
 * Brings up the dialog to let the user change the actual count of the clicked component.
 * Finds the target component by querying the passed in event object's target property.
 *
 * @param e The event object
 */
function editActualCountClicked(e) {
    var componentId = $(e.target).parent().attr("id");
    var componentCoord =  componentId.substring("edit-count-".length);
    var currentCount = $("#no-actual-units-" + componentCoord).val();
    $("#component-coord").val(componentCoord);
    $("#edit-actual-unit-count-field").val(currentCount);
    $("#edit-actual-unit-count-dialog").dialog("open");
    return false;
}

/**
 * This event listener will be invoked when a field that contains number of actual
 * units for a component is changed, and updates the forecasted cost for that component.
 * Also updates the forecast cost subtotal of the associated step.
 *
 * @param e The event object
 */
function noForecastUnitsChanged(e) {
    var id     = $(e.target).parent().parent().attr("id");
    var coord  = id.substring(id.indexOf("-") + 1);
    var stepNo = coord.substring(0, coord.indexOf("-"));
    updateForecastCost(coord);
    updateDifference(coord);
    updateForecastCostSubtoal(stepNo);
    updateDifferenceSubtotal(stepNo);
    updateGrandDifferences();
}

/**
 * This function is invoked when the user changes the selected item in the component name
 * select box. When triggered, fetches the default cost of the component from the server
 * via AJAX and updates the unit-cost fields of the associated component. Finally invokes
 * the _updateValue method to update the actual cost and forecast cost of the component
 * as well as the sub-total of the step. 
 *
 * @param e The event object
 */
function componentNameChanged(e) {
    var id = $(this).attr("id");
    var costLabelId = $(this).val();
    if (costLabelId == null || costLabelId.length == 0) {
        return false;
    }
    var coord  = id.substring("component-name-".length);
    var stepNo = coord.substring(0, coord.indexOf("-"));
    $.ajax({
        url: "../costs/ajax/cost_label.jsp",
        data: {"CostLabelID": costLabelId},
        success: function(resp) {
            $("#unit-cost-" + coord).val(resp.defaultCost);
            updateComponentValues(coord);
            updateGrandDifferences();
        },
        dataType: "json"
    });

    return false;    
}

function loadBeans() {
    var result = null;
    $.ajax({
        url: "ajax/beans.jsp",
        dataType: "json",
        async: false,
        success: function(resp) {
            result = resp;
        }
    });

    return result;
}