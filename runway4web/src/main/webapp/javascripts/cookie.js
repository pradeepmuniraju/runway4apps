function CookieItem(namestr, valuestr)
{
	this.name = namestr;
	this.value = valuestr;	
}

function Cookie(document, name, days, path, domain, secure)
{
    this.document = document;
    this.name = name;

    if(days)
	{
        this.expiration = new Date((new Date()).getTime() + days*24*3600000);
	}
    else
    {
    	this.expiration = null;
	}
	
    if(path)
    {
    	this.path = path;
    }
    else
    {
    	this.path = null;
	}
	
    if(domain)
    {
    	this.domain = domain;
    }
    else
    {
    	this.domain = null;
	}
	
    if(secure)
    {
    	this.secure = true;
    }
    else
    {
    	this.secure = false;
    }
    
    this.datamap = new Array();
}

Cookie.prototype.put = function(namestr, valuestr)
{
	var nameindex = getItemIndex(this, namestr);
	if(nameindex == -1)
	{
		this.datamap[this.datamap.length] = new CookieItem(namestr, valuestr);
	}
	else
	{
		this.datamap[nameindex].value = valuestr;
	}
}

Cookie.prototype.size = function(namestr)
{
	return(this.datamap.length);
}

Cookie.prototype.get = function(namestr)
{
	var nameindex = getItemIndex(this, namestr);
	if(nameindex > -1)
	{
		return(this.datamap[nameindex].value);
	}
	return(null);
}

Cookie.prototype.getString = function(namestr)
{
	var valuestr = this.get(namestr);
	if(valuestr !=null)
	{
		return(valuestr);	
	}
	return("");
}

Cookie.prototype.hasName = function(namestr)
{
	var nameindex = getItemIndex(this, namestr);
	if(nameindex > -1)
	{
		return(true);
	}
	return(false);
}

function getItemIndex(arraylist, namestr)
{
	for(var i=0; i<arraylist.length; i++)
	{
		if(arraylist[i].name == namestr)
		{
			return(i);	
		}
	}
	return(-1);
}

Cookie.prototype.save = function()
{
    var cookieval = "";

    for(var i=0; i<this.size(); i++)
    {		
        if (cookieval != "")
        {
        	cookieval += '&';
		}
        cookieval += this.datamap[i].name + ':' + escape(this.datamap[i].value);
    }

    var cookie = this.name + '=' + cookieval;

    if(this.expiration)
   	{
        cookie += '; expires=' + this.expiration.toGMTString();
	}
	
    if (this.path)
   	{
   		cookie += '; path=' + this.path;
	}
	
    if(this.domain)
    {
    	cookie += '; domain=' + this.domain;
	}
	
    if(this.secure)
    {
    	cookie += '; secure';
	}

    this.document.cookie = cookie;
}

Cookie.prototype.load = function()
{ 
    var allcookies = this.document.cookie;

    if(allcookies == "")
    {
    	return false;
	}

    var start = allcookies.indexOf(this.name + '=');

    if(start == -1)
    {
    	return false;
	}
	
    start += this.name.length + 1;

    var end = allcookies.indexOf(';', start);

    if(end == -1)
    {
    	end = allcookies.length;
	}
	
    var cookieval = allcookies.substring(start, end);

    var a = cookieval.split('&');

    for(var i=0; i < a.length; i++)
    {
        a[i] = a[i].split(':');
	}

    for(var i = 0; i < a.length; i++)
    {
        this.put(a[i][0], unescape(a[i][1]));
    }

    return true;
}

Cookie.prototype.remove = function()
{
    var cookie;

    cookie = this.$name + '=';

    if(this.path)
    {
    	cookie += '; path=' + this.path;
	}
	
    if(this.domain)
    {
    	cookie += '; domain=' + this.domain;
	}
	
    cookie += '; expires=Fri, 02-Jan-1970 00:00:00 GMT';

    this.document.cookie = cookie;
}

//form functions

function FormPersister(documentobj, formobj, nameprefix)
{
	this.formexpire = 8000;
	this.cookieobj = null;
	this.document = documentobj;
	this.form = formobj;
	this.nameprefix = nameprefix;
	this.excludelist = new Array();
	this.includelist = new Array();	
}

FormPersister.prototype.setExpire = function(intdays)
{
	this.formexpire = intdays;
}

FormPersister.prototype.addExclude = function(namestr)
{
	this.excludelist[this.excludelist.length] = namestr;
}

FormPersister.prototype.addInclude = function(namestr)
{
	this.includelist[this.includelist.length] = namestr;
}

FormPersister.prototype.setExcludeList = function(listobj)
{
	this.excludelist = listobj;
}

FormPersister.prototype.setIncludeList = function(listobj)
{
	this.includelist = listobj;
}

FormPersister.prototype.getName = function()
{
	return(this.nameprefix + this.form.name);	
}

FormPersister.prototype.listIndex = function(listobj, namestr)
{
	for(var i=0; i<listobj.length; i++)
	{
		if(listobj[i] == namestr)
		{
			return(i);	
		}
	}
	return(-1);
}

FormPersister.prototype.checkInExclude = function(namestr)
{
	if(this.excludelist.length == 0 && this.includelist.length == 0)
	{
		return(true);	
	}
	else if(this.listIndex(this.excludelist, namestr) >=0)
	{
		return(false);
	}
	else if(this.includelist.length == 0)
	{
		return(true);
	}
	else if(this.listIndex(this.includelist, namestr) >=0)
	{
		return(true);
	}	
	return(false);
}

FormPersister.prototype.save = function()
{
	var setname = this.getName();
	var formdata = new Cookie(this.document, setname, this.formexpire);
	for(var i=0; i<this.form.elements.length; i++)
	{
		if(this.checkInExclude(this.form.elements[i].name) && this.shouldSave(this.form.elements[i]))
		{
			formdata.put(this.form.elements[i].name, this.getInputValue(this.form.elements[i]));
		}	
	}
	formdata.save();
}

FormPersister.prototype.load = function()
{
	var setname = this.getName();
	var formdata = new Cookie(this.document, setname, this.formexpire);
	formdata.load();
	var inputindex = -1;
	for(var i=0; i<formdata.size(); i++)
	{
		inputindex = this.getFormElementIndex(this.form, formdata.datamap[i].name, formdata.datamap[i].value);
		if(inputindex>-1 && this.checkInExclude(formdata.datamap[i].name))
		{
			this.setInputValue(this.form.elements[inputindex], formdata.datamap[i].value);	
		}
	}
	cookieobj = formdata;
	return(cookieobj);
}

FormPersister.prototype.getFormElementIndex = function(formobj, namestr, valuestr)
{
	for(var i=0; i<formobj.elements.length; i++)
	{
		if(typeof(formobj.elements[i].type) != "undefined" &&
			(formobj.elements[i].type == "checkbox" || formobj.elements[i].type == "radio"))
		{
			if(formobj.elements[i].name == namestr && formobj.elements[i].value == valuestr)
			{
				return(i);	
			}
		}	
		else if(formobj.elements[i].name == namestr)
		{
			return(i);
		}
	}
	return(-1);
}

FormPersister.prototype.getInputValue = function(inputobj)
{
	if(typeof(inputobj.type) != "undefined")
	{
		if(inputobj.type == "select-one")
		{
			return(inputobj.options[inputobj.selectedIndex].value);
		}
		else if(inputobj.type == "select-multiple")
		{
			return(inputobj.options[inputobj.selectedIndex].value);
		}
	}
	return(inputobj.value);
}

FormPersister.prototype.setInputValue = function(inputobj, valuestr)
{
	if(inputobj.type == "checkbox" || inputobj.type == "radio")
	{
		inputobj.checked = true;
	}
	else
	{
		inputobj.value = valuestr;
	}
}


FormPersister.prototype.shouldSave = function(inputobj)
{
	if(inputobj.name)
	{
		if(typeof(inputobj.type) != "undefined"
			&& inputobj.type != "password"
			&& inputobj.type != "file"
	        && inputobj.type != "hidden")
	    {
	    	
			if(inputobj.type == "checkbox" || inputobj.type == "radio")
			{
				return(inputobj.checked);
			}
			else if(typeof(inputobj.value) != "undefined" && inputobj.value != "")
			{    	
				return(true);
			}
		}
	}
	return(false);
}