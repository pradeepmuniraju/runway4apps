function changeImages()
{
	for (var i=0; i<changeImages.arguments.length; i+=2)
	{
		document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
	}
}

function toggleStatus(eventid, imagepath, prefix, inputname)
{
	var formname = "form" + eventid;
	var imagename = "image" + eventid;
	var thisform = document.forms[formname];
	var nextvalue = "";
	var inputobj = null;
	if(arguments.length==2)
	{
		inputobj = thisform.Status;
	}
	else
	{
		inputobj = thisform.elements[inputname];
	}

	if(inputobj.value=="Active")
	{
		nextvalue = "Inactive";
	}
	else
	{
		nextvalue ="Active";
	}
	document.images[imagename].title = "Currently " + nextvalue + ", click to toggle";

	inputobj.value = nextvalue;

	var imageurl = "";
	
	if(arguments.length==2)
	{	
		imageurl = imagepath+nextvalue+"_icon";
	}
	else
	{
		imageurl = imagepath+prefix+nextvalue+"_icon";
	}
	
	if(document.images[imagename].src.indexOf(".gif")>0)
	{
		imageurl = imageurl + ".gif";
	}
	else
	{
		imageurl = imageurl + ".jpg";
	}

	thisform.submit();
	changeImages(imagename, imageurl);
}

var ltgray = "#EEEEEE";
var white = "#FFFFFF";

function rowBG(objid, colour)
{
	if(objid.style)
	{
		objid.style.backgroundColor = colour;
	}
}

// Change row color when MouseOver occurs
function rowOn(objid)
{
	rowBG(objid, ltgray);
}

function rowOff(objid)	
{
	rowBG(objid, white);
}