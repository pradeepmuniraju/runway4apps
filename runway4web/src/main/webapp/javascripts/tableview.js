// http://my.opera.com/GreyWyvern/blog/show.dml/1725165
Object.prototype.clone = function() {
  // create a new empty array or map
  var newObj = (this instanceof Array) ? [] : {};
  for (i in this) {
    if (i == 'clone') {
      // ignore this clone function
      continue;
    }
    if (this[i] && typeof this[i] == "object") {
      // this is an object, so recursively clone it
      newObj[i] = this[i].clone();
    } else {
      newObj[i] = this[i];
    }
  } return newObj;
};

function TableView(viewspec) {
  if(viewspec) {
    this.viewSpec(viewspec);
  }
  
  this.fields = null;
  this.ready = false;  // true if records have been fetched and not currently updating
  this.callback = null;
}

TableView.setViewSpec = function(viewspec) {
  this.viewspec = viewspec;
};

TableView.getViewSpec = function() {
  return this.viewspec;
};

TableView.fetch = function() {
  this.ready = false;
  
};

TableView.getReady = function() {
  return this.ready;
};

TableView.getField = function(fieldName) {
  var outField = null;
  if(this.fields != null && this.ready == true) {
    outField = this.fields(fieldName).clone();
  }
  return outField;
};

TableView.setField = function(fieldName, fieldValue) {
  
};

TableView.getFields = function(fieldNames) {
  var outFields = null;
  if(this.fields != null && this.ready == true) {
    outFields = this.fields.clone();
  }  
  return outFields;
};

TableView.setFields = function(fields) {
};
