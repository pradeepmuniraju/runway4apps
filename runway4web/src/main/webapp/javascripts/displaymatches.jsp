<%@ page contentType="text/javascript" %>

<%-- 
  Dependencies:  /ajax/javascripts/displayutil.jsp
                 /javascripts/assignutils.js 
--%>

<%
  String contextPath = request.getContextPath();
%>

/* Global variable set by initMatchableFields function */
var prevFieldNames;
var prevFieldValues;
var matchesActionUrl;

/* 
 * Invoke this function once at the end of page to use Matches functionality. 
 * args       id - ID of div element where matches content is placed
 *            url - The URL of the JSP action that builds the matches content. The URL can contain paramaters. 
 */
function initMatchableFields(id, url) {
  matchesActionUrl = url;
  
  var matchableInputs = getMatchableInputs();
  var matchableSelects = getMatchableSelects();
  
  var numberFields = matchableInputs.length + matchableSelects.length;
  prevFieldNames = new Array(numberFields);
  prevFieldValues = new Array(numberFields);
  var prevFieldIndex = 0;

  for(var i = 0; i < matchableInputs.length; i++) 
  {
	  putField(matchableInputs[i].name, "", prevFieldIndex);
	  prevFieldIndex++;
  }
  
  for(var i = 0; i < matchableSelects.length; i++) 
  {
    putField(matchableSelects[i].name, "", prevFieldIndex);
    prevFieldIndex++;
  }
}

/* 
 * Event handler for input or select elements where a match request is made.
 * Put the class "matchable" on all input and select elements that are intended for making matches.
 * args       id - ID of div element where match content is placed.
 */
var isMatching = false;

function getMatches(id, usestandardsearch) {
	//alert("usestandardsearch" + usestandardsearch);
	var url = new String(matchesActionUrl);
	
	// Add parameters
	var urlParams = getInputNameValueParams(usestandardsearch);
	var url = url+urlParams;
	// Perform AJAX request (see displayutil.jsp and ajax.js)
    if (!isMatching) {
    	isMatching = true;
    	loadPanel(id, url, stopMatches);
    }	
}

function getMatchesWithURL(id, usestandardsearch, overrideurl) {
	//alert("overrideurl" + overrideurl);
	var url = new String(matchesActionUrl);
	
	if(overrideurl != null && overrideurl != undefined){
		url = new String(overrideurl);
	}
	//alert("url" + url);
	// Add parameters
	var urlParams = getInputNameValueParams(usestandardsearch);
	var url = url+urlParams;
	// Perform AJAX request (see displayutil.jsp and ajax.js)
    if (!isMatching) {
    	isMatching = true;
    	loadPanel(id, url, stopMatches);
    }	
}

function stopMatches() {
	isMatching = false;
}

/** Start Helper Functions **/

function getInputNameValueParams(usestandardsearch)
{
  var urlParams = "";
  
  if(matchesActionUrl.indexOf('?') < 0) { 
	  urlParams += "?";
	}	else {
		var lastIndex = matchesActionUrl.length - 1;
		if(matchesActionUrl[lastIndex] != '&') {
		  urlParams += "&";
		}
  }
	
	// Get all Input elements with class="matchable"
	var matchableInputs = getMatchableInputs();

	for(var i = 0; i < matchableInputs.length; i++) 
	{
		if(matchableInputs[i].name != null) {
		  updateField(matchableInputs[i].name, matchableInputs[i].value);
			urlParams = urlParams+matchableInputs[i].name;
			urlParams = urlParams+"=";
			if(!usestandardsearch){
				urlParams = urlParams+ ((matchableInputs[i].name == null || matchableInputs[i].value.length == 0) ? "empty" : encodeURIComponent(matchableInputs[i].value));
			}
			else{
				urlParams = urlParams+ encodeURIComponent(matchableInputs[i].value);
			}
			urlParams = urlParams+"&";
		}
	}
	
	// Get all Select elements with class="matchable"
	var matchableSelects = getMatchableSelects();
	for(var i = 0; i < matchableSelects.length; i++) 
	{
		if(matchableSelects[i].name != null) {
      updateField(matchableSelects[i].name, matchableSelects[i].value);
			urlParams = urlParams+matchableSelects[i].name;
			urlParams = urlParams+"=";
			if(!usestandardsearch){
				urlParams = urlParams+ ((matchableSelects[i].name == null || matchableSelects[i].value.length == 0) ? "empty" : matchableSelects[i].value);
			}
			else{
				urlParams = urlParams+ matchableSelects[i].value;
			}			
			urlParams = urlParams+"&";
		}
	}
	
	return urlParams;
}

function getMatchableInputs()
{
	return getElementsByClassName(document, "matchable", "input");
}

function getMatchableSelects()
{
	return getElementsByClassName(document, "matchable", "select");
}

/*
 *  source:      [http://lists.evolt.org/pipermail/javascript/2006-January/010010.html]
 *  modified by: Ulpian Cesana
 *
 *  args:        doc = DOM document
 *               argClassName = class you're seeking
 *               argTagName = [optional] tag name to limit search
 *
 *  returns: array of matching elements
 */
function getElementsByClassName(doc, argClassName, argTagName)
{
	// we'll be searching for the requested class name,
	// optionally bracketed by spaces to handle multiple classes
	var reClassMatch = new RegExp("(^| )" + argClassName + "( |$)");
	// prepare to return the results in an array
	var aResult = new Array();
	// default = search all page elements
	var sTagName = "*";
	
	// if one tag was requested, limit the search
	if (argTagName) 
	  sTagName = argTagName;
	
	// get array of all elements [with matching tag if requested]
	var aEls = doc.getElementsByTagName(sTagName);
	
	// collect elements with matching classNames
	for (var iEl=0; iEl < aEls.length; iEl++)
	{
	  if (reClassMatch.test(aEls[iEl].className))
	  {
	    aResult[aResult.length] = aEls[iEl];
	  }
	}
	  
	return aResult;
}

/*
 * This function will update the previous field's 
 * value to the current value.
 */
function updateField(fieldName, fieldValue)
{
  var index = findFieldIndex(fieldName);
  
  if(index >= 0) 
  {
    // update the previous field value to the current value
    prevFieldValues[index] = fieldValue;
	}
}

/*
 * Returns the index of the specified fieldName.
 */
function findFieldIndex(fieldName) {
  var index = 0;
  for(index = 0; index < prevFieldNames.length; index++) 
  {
     if(prevFieldNames[index] == fieldName) {
        return index;
     }
  }

  // Index Not Found
  return -1;
}

/*
 * Assumes fieldName-fieldValue pair does not already exist
 */
function putField(fieldName, fieldValue, index)
{
  prevFieldNames[index] = fieldName;
  prevFieldValues[index] = fieldValue;  
}

/*
 * Simple debug function to print text.
 * Note: This is a temporary function. If it has been committed, 
 * feel free to delete it!
 */
function debug(text) {
	if(text==null) {text = "Testing, testing, 1, 2, 3."; } 
    
	alert(text); 
}

/** End Helper Functions **/