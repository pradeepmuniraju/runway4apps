var noteid = "";
var receiverurl = "http://web1.switched-on.com.au/sok2/setresponse.jsp";

function setNoteID()
{
	var thisurl = this.window.location.href;
	var urlparts = thisurl.split("?");
	var foundid = false;
	if(urlparts.length>1)
	{
		var queryparams = urlparts[1].split("&");
		for(var i=0; i<queryparams.length; i++)
		{
			var parampair = queryparams[i].split("=");
			if(parampair[0] == "noteid")
			{
				noteid = parampair[1];
				var expiredate = new Date();
				expiredate.setTime(expiredate.getTime() + 24 * 60 * 60 * 1000);
				setCookie(parampair[0], parampair[1], expiredate);
				foundid = true;
			}
		}
	}

	if(!foundid)
	{
		noteid = getCookie("noteid");
	}
}

function setLinks()
{
	var thislinks = this.document.links;
	for(var i=0; i<thislinks.length; i++)
	{
		var responsemsg = isResponse(thislinks[i].href);
		if(responsemsg.length>0)
		{	
			if(noteid.length != 0)
			{
				thislinks[i].href = receiverurl +
				"?noteid=" + noteid +
				"&response=" + escape(responsemsg) +
				"&target=" + escape(thislinks[i].href.split("?")[0]) +
				"&num=" + Math.random();
			}
			else
			{
				thislinks[i].href = thislinks[i].href.split("?")[0];
			}
		}
	}
}

function isResponse(valuestring)
{
	if(valuestring.length>0)
	{
		var urlpair = valuestring.split("?");
		if(urlpair.length>1)
		{
			var parampairs = urlpair[1].split("&");
			for(var j=0; j<parampairs.length; j++)
			{
				var parampair = parampairs[j].split("=");
				if(parampair.length>1)
				{
					if(parampair[0] == "response")
					{
						return(parampair[1]);
					}
				}
			}
		}
	}
	return("");
}

function setOptionLinks()
{
	var thisforms = this.document.forms;
	if(thisforms.length>0)
	{
		for(var i=0; i<thisforms.length; i++)
		{
			var formelements = thisforms[i].elements;
			if(formelements.length>0)
			{
				for(var j=0; j<formelements.length; j++)
				{
					if(formelements[j].type == "select-one")
					{
						var selectoptions = formelements[j].options;
						for(var n=0; n<selectoptions.length; n++)
						{
							var responsemsg = isResponse(selectoptions[n].value);
							if(responsemsg.length>0)
							{	
								if(noteid.length != 0)
								{
									selectoptions[n].value = receiverurl +
									"?noteid=" + noteid +
									"&response=" + escape(responsemsg) +
									"&target=" + escape(selectoptions[n].value.split("#")[0].split("?")[0]) +
									"&num=" + Math.random();
								}
								else
								{
									selectoptions[n].value = selectoptions[n].value.split("#")[0].split("?")[0];
								}
							}
						}
					}
				}
			}
		}
	}
}

function setPopUpLinks()
{
	if(typeof(linkArray) != "undefined")
	{  
		for(var j=0; j<linkArray.length; j++)
		{
			var responsemsg = isResponse(linkArray[j]);
			if(responsemsg.length>0)
			{	
				if(noteid.length != 0)
				{
					linkArray[j] = receiverurl +
					"?noteid=" + noteid +
					"&response=" + escape(responsemsg) +
					"&target=" + escape(linkArray[j].split("?")[0]) +
					"&num=" + Math.random();
				}
				else
				{
					linkArray[j] = linkArray[j].split("?")[0];
				}
			}
		}
	}
}

function setCookie(name, value, expires, path, domain, secure)
{
	var curCookie = name + "=" + escape(value) +
      ((expires) ? "; expires=" + expires.toGMTString() : "") +
      ((path) ? "; path=" + path : "") +
      ((domain) ? "; domain=" + domain : "") +
      ((secure) ? "; secure" : "");
	document.cookie = curCookie;
}

function getCookie(name)
{
	var dc = document.cookie;
	var prefix = name + "=";
	var begin = dc.indexOf("; " + prefix);
	if (begin == -1)
	{
		begin = dc.indexOf(prefix);
		if (begin != 0)
		{
			return("");
		}
	}
	else
	{
		begin += 2;
	}
	var end = document.cookie.indexOf(";", begin);
	if (end == -1)
	{
		end = dc.length;
	}
	return unescape(dc.substring(begin + prefix.length, end));
}

function initlinks()
{
	setNoteID();
	setLinks();
	setOptionLinks();
	setPopUpLinks();
}

initlinks();