/**
 * Requirements:
 *
 *  The calling page must have a variable named CONTEXT_PATH that is set to the
 *  calling page's context path. 
 */

/**
 * These values are used as keys for stroing user preferences in th DB.
 */
var USER_PREFERENCES = {
    TEMPLATE_KEYWORD: "compose-email.image-flow.template-keyword",
    TEMPLATE_TYPE: "compose-email.image-flow.template-type",
    TEMPLATE_CAMPAIGN: "compose-email.image-flow.template-campaign",
    TEMPLATE_RECENT: "compose-email.image-flow.template-recent",
    TEMPLATE_CREATED_BY: "compose-email.image-flow.template-created-by"
};

/**
 * This variable is set to the field that should be populated with email addresses.
 * It is used by the To, CC, BCC, and Test Email contact lookup buttons.
 */
var TARGET_FIELD = null;

/**
 * This field is set to the currently selected master template id. This helps us avoiding
 * to reload templates when the user clicks on a master template thumbnail multiple times. 
 */
var SELECTED_MASTER_TEMPLATE_ID = null;

var SELECTED_MASTER_NEWSLETTER_ID = null;

/**
 * This value is used for fadeIn and fadeOut effects. 
 */
var FADE_IN_SPEED = 250;


$(document).ready(function() {

	if(typeof(context)!="undefined"){
	    /* NOTE: TEMP FIX FOR PARENT ELEMENT's PADDING and BG color */
	    $(".background_actions").css("padding", 0);
	    $(".boxwithnavBackground").css("background", "##E8E9EE");
	
	    initUserPreferenceListeners();
	    //initImageFlowListeners();
	    //initImageFlow();
	    initHoverImages();
	    initTemplateCombo();
	    initRecipientButtons();
	    initSideBar();
	    initForm();
	    initMessageDialog();
	    //addWindowEventListener("focus",editModeAllOff);
	    //addDocumentEventListener("focus",editModeAllOff);
	    
	}
});

function getEditorCount() {
	var ifrm = document.getElementById("ifrm");
	if (ifrm != null && ifrm.contentWindow.getEditorCount) {
		return ifrm.contentWindow.getEditorCount();
	}
	return 0;
}

function editModeAllOff(evt) {
	var ifrm = document.getElementById("ifrm");
	if (ifrm != null && ifrm.contentWindow.editModeAllOff) ifrm.contentWindow.editModeAllOff(evt);
}

function stopAll() {
	var ifrm = document.getElementById("ifrm");
	if (ifrm != null && ifrm.contentWindow.windowClick) 
		ifrm.contentWindow.windowClick();
}

function simulateClick() {
	var ifrm = document.getElementById("ifrm");
	if (ifrm != null && ifrm.contentWindow) {
		var a = ifrm.contentDocument.getElementsByTagName("a");
		if (typeof a != 'undefined' && a != null && a.length > 1) {
			a[0].click();
		}
	}
}

/* BEGIN: PREFERENCES */

/**
 * Attaches the listeners required for updating the selected keyword, type, campaign, recent, and created by
 * fields in user preferences. 
 */
function initUserPreferenceListeners() {
    $("#temp-kw-fld").change(saveKeywordUserPref);
    $("#temp-type-combo").change(saveTypeUserPref);
    $("#temp-camp-combo").change(saveCampaignUserPref);
    $("#temp-recent-combo").change(saveRecentUserPref);
    $("#temp-created-by-combo").change(saveCreatedByUserPref);
}

function saveKeywordUserPref() {
    var prefValue = $("#temp-kw-fld").val();    
    $.ajax({
        url: CONTEXT_PATH + "/crm/ajax/preferences/setuserpreference.jsp",
        data: {
            "-preferenceKey": USER_PREFERENCES.TEMPLATE_KEYWORD,
            "-preferenceValue": prefValue},
        dataType: "json",
        type: "POST"
    });
}

function saveTypeUserPref() {
    var prefValue = $("#temp-type-combo").val();
    $.ajax({
        url: CONTEXT_PATH + "/crm/ajax/preferences/setuserpreference.jsp",
        data: {
            "-preferenceKey": USER_PREFERENCES.TEMPLATE_TYPE,
            "-preferenceValue": prefValue},
        dataType: "json",
        type: "POST"
    });
}

function saveCampaignUserPref() {
    var prefValue = $("#temp-camp-combo").val();
    $.ajax({
        url: CONTEXT_PATH + "/crm/ajax/preferences/setuserpreference.jsp",
        data: {
            "-preferenceKey": USER_PREFERENCES.TEMPLATE_CAMPAIGN,
            "-preferenceValue": prefValue},
        dataType: "json",
        type: "POST"
    });
}

function saveRecentUserPref() {
    var prefValue = $("#temp-recent-combo").val();
    $.ajax({
        url: CONTEXT_PATH + "/crm/ajax/preferences/setuserpreference.jsp",
        data: {
            "-preferenceKey": USER_PREFERENCES.TEMPLATE_RECENT,
            "-preferenceValue": prefValue},
        dataType: "json",
        type: "POST"
    });
}

function saveCreatedByUserPref() {
    var prefValue = $("#temp-created-by-combo").val();
    $.ajax({
        url: CONTEXT_PATH + "/crm/ajax/preferences/setuserpreference.jsp",
        data: {
            "-preferenceKey": USER_PREFERENCES.TEMPLATE_CREATED_BY,
            "-preferenceValue": prefValue},
        dataType: "json",
        type: "POST"
    });
}

/* END: PREFERENCES */

/* BEGIN: IMAGE-FLOW*/

// the current image-flow implementation needs the context path to be stored in a variable
//  named context

function initImageFlowListeners() {
    $("#temp-kw-fld").change(reloadImageFlow);    
    $("#temp-type-combo").change(reloadImageFlow);
    $("#temp-camp-combo").change(reloadImageFlow);
    $("#temp-recent-combo").change(reloadImageFlow);
    $("#temp-created-by-combo").change(reloadImageFlow);
    $("#temp-find-btn").click(function(e) {
        e.preventDefault();
        e.stopPropagation();        
    });

    imf.onClickCallback = loadTemplates;
    imf.doubleClickCallback = function(e) {
        hideShow('ifContainer');
        swapImage('ifContainer_btn');
    };

    /**
     * When selecting an item from the Quick Select combo, we update
     * the Template Combo accordingly and then load the selected template.
     */
    $("#quick-select-combo").change(function(e) {
        SELECTED_MASTER_TEMPLATE_ID = null;
        $("#temp-combo option").remove();

        var temp = $("#quick-select-combo option:selected");
        var tempVal = $(temp).val();
        if (tempVal != null && $.trim(tempVal).length > 0) {
            var opt = $("<option>").attr("value", $(temp).val()).text($(temp).text());
            $("#temp-combo").append(opt);
        }
        loadTemplate(true);        
    });
}

/**
 * Loads all the templates that are children of the given master template id.
 *
 * @param masterTemplateId ID of the master template that we want to load its templates.
 */
function loadTemplates(masterTemplateId) {
	loadTemplates(masterTemplateId,"");
}
function loadTemplates(masterTemplateId, groupId) {
    if (masterTemplateId == SELECTED_MASTER_TEMPLATE_ID) {
        return;
    }
    if (groupId == null || typeof(groupId) == "undefined") {
    	groupId = "";
    	var parse = document.getElementById("parsedGroups");
    	if (parse != null && typeof(parse) != "undefined") groupId = parse.value;
    }
    $.ajax({
        url: CONTEXT_PATH + "/crm/email/ajax/load-templates.jsp",
        data: {MasterTemplateID: masterTemplateId, "-groupid" : groupId},
        dataType: "json",
        type: "POST",
        async: false,
        success: function(data) {
            cleanTemplate();
            $("#temp-combo option").remove();            
            var temps = data.Templates;
            for (var i = 0; i < temps.length; i++) {
                $("<option>").
                        val(temps[i].TemplateID).
                        text(temps[i].TemplateName).
                        appendTo("#temp-combo");
            }
            loadTemplate(false);
            SELECTED_MASTER_TEMPLATE_ID = masterTemplateId;
        }
    });
}

function loadTemplatesSet(masterTemplateId, templateSet) {
    if (masterTemplateId == SELECTED_MASTER_TEMPLATE_ID) {
        return;
    }
    if (templateSet == null || typeof(templateSet) == "undefined") {
    	templateSet = "";
    }
    $.ajax({
        url: CONTEXT_PATH + "/crm/email/ajax/load-templates.jsp",
        data: {MasterTemplateID: masterTemplateId, "TemplateID" : templateSet},
        dataType: "json",
        type: "POST",
        success: function(data) {
            cleanTemplate();
            $("#temp-combo option").remove();            
            var temps = data.Templates;
            for (var i = 0; i < temps.length; i++) {
                $("<option>").
                        val(temps[i].TemplateID).
                        text(temps[i].TemplateName).
                        appendTo("#temp-combo");
            }
            loadTemplate(false);
            SELECTED_MASTER_TEMPLATE_ID = masterTemplateId;
        }
    });
}

/**
 * Loads all the templates that are children of the given master template id.
 *
 * @param masterTemplateId ID of the master template that we want to load its templates.
 */
function loadNewsletters(cmsId) {
	loadTemplates(cmsId,"");
}
function loadNewsletters(cmsId, groupId) {
    if (cmsId == SELECTED_MASTER_NEWSLETTER_ID) {
        return;
    }
    if (groupId == null || typeof(groupId) == "undefined") {
    	groupId = "";
    	var parse = document.getElementById("parsedGroups");
    	if (parse != null && typeof(parse) != "undefined") groupId = parse.value;
    }
    $.ajax({
        url: CONTEXT_PATH + "/crm/email/ajax/load-newsletters.jsp",
        data: {TemplateCMSID: cmsId, "-groupid" : groupId},
        dataType: "json",
        type: "POST",
        success: function(data) {
            cleanTemplate();
            $("#temp-combo option").remove();            
            var temps = data.Templates;
            for (var i = 0; i < temps.length; i++) {
                $("<option>").
                        val(temps[i].TemplateID).
                        text(temps[i].TemplateName).
                        appendTo("#temp-combo");
            }
            $("#temp-combo").change(loadNewsletter);
            $("#temp-combo").attr("name","CMSID");
            $("#cmsid-fld").attr("name","TemplateID");
            $("#cmsid-fld").attr("value",$("#nltid-fld").attr("value"));
            loadNewsletter();
            SELECTED_MASTER_NEWSLETTER_ID = cmsId;
        }
    });
}

/**
 * Serializes the template search fields. The returned value is used in an Ajax
 * call to retrieve matching templates from the server side.
 */
function serializeTemplateOptions() {

    var type = null;
    if (typeof TEMPLATE_TYPE != "undefined") {
        type = TEMPLATE_TYPE;
    } else {
        type = $("#temp-type-combo").val();
    }

    var name = $("#temp-kw-fld").val();
    if (name != null && $.trim(name).length > 0) {
        name = "%" + name + "%";
    }

    if (typeof MASTER_TEMPLATE_LIST != "undefined") {
        // We load a fixed set of master templates
        return {
            "-load": "mastertemplate",
            "MasterTemplateID": MASTER_TEMPLATE_LIST
        };
    } else {
    	var eventid = $("#event-id").val();
    	if(typeof(eventid) != 'undefined' && eventid != null && eventid != ""){
	        return {
	            "-load": "mastertemplate",
	            "Name": name,
	            "Type": type,
	            "MasterTemplateTemplate.TemplateLinkedTemplate.EventID": eventid,
	            "MasterTemplateTemplate.TemplateLinkedTemplate.LinkedTemplateEvent.EventCampaign.CampaignID": $("#temp-camp-combo").val(),
	            "-recent": $("#temp-recent-combo").val(),
	            "CreatedBy": $("#temp-created-by-combo").val()
	        };
    	}
    	else{
	        return {
	            "-load": "mastertemplate",
	            "Name": name,
	            "Type": type,
	            "MasterTemplateTemplate.TemplateLinkedTemplate.LinkedGroupID": $("#group-id").val(),
	            "MasterTemplateTemplate.TemplateCampaign.CampaignID": $("#temp-camp-combo").val(),
	            "-recent": $("#temp-recent-combo").val(),
	            "CreatedBy": $("#temp-created-by-combo").val()
	        };
    	}
    }
}

function initImageFlow() {
    var params = serializeTemplateOptions();
    // When we want to show a fixed set of master templates but no master template ids are passed
    // don't create the image flow
    if (typeof MASTER_TEMPLATE_LIST != "undefined" && MASTER_TEMPLATE_LIST.length == 0) {
        return;
    }
    createMasterImageFlow(context, params);    
}

/**
 * Whenever one of the search criteria changes, this function is invoked to
 * update the contents of the image flow.
 */
function reloadImageFlow() {
    var params = serializeTemplateOptions();
    createMasterImageFlow(context, params);
}

/* END: IMAGE-FLOW*/

/* BEGIN: COMPOSE BAR */

function initHoverImages() {
    enableHoverEffect("#to-btn img", "/images/email/bttn_to_0.png", "/images/email/bttn_to_1.png");
    enableHoverEffect("#cc-btn img", "/images/email/bttn_cc_0.png", "/images/email/bttn_cc_1.png");
    enableHoverEffect("#bcc-btn img", "/images/email/bttn_bcc_0.png", "/images/email/bttn_bcc_1.png");
    enableHoverEffect("#send-btn", "/images/email/bttn_sendemail_0.png", "/images/email/bttn_sendemail_1.png");
    enableHoverEffect("#save-btn", "/images/email/bttn_saveemail_0.png", "/images/email/bttn_saveemail_1.png");
    enableHoverEffect("#preview-btn", "/images/email/bttn_previewtext_0.png", "/images/email/bttn_previewtext_1.png");
    enableHoverEffect("#cancel-btn", "/images/email/bttn_cancel_0.png", "/images/email/bttn_cancel_1.png");
}

function enableHoverEffect(imageBtnId, normalImg, hoverImg) {
    var elem = $(imageBtnId);    
    elem.hover(
        function() {
            elem.attr("src", CONTEXT_PATH + hoverImg);
        },
        function() {
            elem.attr("src", CONTEXT_PATH + normalImg);
        }
    );
}

function initTemplateCombo() {
    $("#temp-combo").change(loadTemplate);
}

function getUrlVars()
{
    var vars = {};
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var hash = null;
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function getUrlFromVars(vars)
{
	var s = '';
    for ( var e in vars )
    {
    	if(s.length!=0){
    		s += "&";
    	}
       s += e + "=" + vars[e] ;
    }

    return s;
}

/**
 * Loads the currently selected template from the server side, updates the subject fields accordingly,
 * and displays the template in the preview/editing area.
 */
function loadTemplate() {
	loadTemplate(true);
}

function loadTemplate(savepref) {
    var tempId = $("#temp-combo").val();
    
    if(savepref)
    	saveTemplatePreference();
    
    if (tempId == null || tempId.length == 0) {
        cleanTemplate();
        return;
    }

    // The TemplateUsed field should be stored in the database 
    $("#template-used").val(tempId);

    var params = getUrlVars(); //pass along parameters so that emailer could react to them
    params['TemplateID'] = tempId;
    params['GroupID'] = $("#group-id").val();
    params['RepUserID'] = $("#rep-user-id").val();
    params['-editorID'] = "Visual";
    params['EDITOR'] = "true";
    params['-load'] = "true";
    params['-editmode'] = "inline";
    params['NoteID'] = $('input[name="NoteID"]','form#compose-form').val();
    
    var src = CONTEXT_PATH + "/crm/email/editor/editor.jsp?" + getUrlFromVars(params);
    
    $("#ifrm").attr("src", src);
    $.ajax({
        url: CONTEXT_PATH + "/crm/email/ajax/template-name.jsp",
        dataType: "json",
        data: {TemplateID: tempId},
        success: function(resp) {
        	var note = false;
        	$("#SpamScore").html(resp.SpamScore);
        	$("#SpamReport").html(resp.SpamReport);
            $("#master-template-id").val(resp.MasterTemplateID);
            $("#email-subject").val(resp.Subject);
            if (resp.EmailTo != null && resp.EmailTo.length > 0) {
            	if (resp.EmailTo == "${RepEmail}") {
            		$("#to-fld").val($("#to-repemail").val());
            		note = true;
            	} else if (resp.EmailTo == "${MgrEmail}") {
            		$("#to-fld").val($("#to-mgremail").val());
            		note = true;
            	} else {
            		$("#to-fld").val(resp.EmailTo);
            	}
            } else {
            	$("#to-fld").val($("#to-default").val());
            }
            if (resp.EmailCC != null && resp.EmailCC.length > 0) {
            	if (resp.EmailCC == "${RepEmail}") {
            		$("#cc-fld").val($("#to-repemail").val());
            		note = true;
            	} else if (resp.EmailCC == "${MgrEmail}") {
            		$("#cc-fld").val($("#to-mgremail").val());
            		note = true;
            	} else {
            		$("#cc-fld").val(resp.EmailCC);
            	}
            } else {
            	$("#cc-fld").val($("#cc-default").val());
            }
            //if ($("#cc-fld").val() == "") $("#cc-fld").val("-");
            if (resp.EmailBCC != null && resp.EmailBCC.length > 0) {
            	if (resp.EmailBCC == "${RepEmail}") {
            		$("#bcc-fld").val($("#to-repemail").val());
            		note = true;
            	} else if (resp.EmailBCC == "${MgrEmail}") {
            		$("#bcc-fld").val($("#to-mgremail").val());
            		note = true;
            	} else {
            		$("#bcc-fld").val(resp.EmailBCC);
            	}
            } else {
            	$("#bcc-fld").val($("#bcc-default").val());
            }
            //if ($("#bcc-fld").val() == "") $("#bcc-fld").val("-");
            if (resp.EmailFrom != null && resp.EmailFrom.length > 0) {
                $("#email-from").val(resp.EmailFrom);
                //if ($("#email-from") != null) $("#email-from").val(resp.EmailFrom);
            } else {
            	//$("#email-from").val($("#reply-default").val());
            	$("#email-from").val($("#from-default").val());
            }
            if (note) {
                $("#noteaction").val("true");
            } else {
            	$("#noteaction").val($("#noteaction-default").val());
            }
            if (resp.EmailReplyTo != null && resp.EmailReplyTo.length > 0) {
                $("#email-reply-to").val(resp.EmailReplyTo);
            }	
            if (resp.CMSID != null && resp.CMSID.length > 0) {
                $("#cmsid-fld").val(resp.CMSID);
            }else{
            	$("#cmsid-fld").val("");
            }	
        }
    });
}

/**
 * Loads the currently selected template from the server side, updates the subject fields accordingly,
 * and displays the template in the preview/editing area.
 */
function loadNewsletter() {
    var tempId = $("#temp-combo").val();
    if (tempId == null || tempId.length == 0) {
        cleanTemplate();
        return;
    }

    // The TemplateUsed field should be stored in the database 
    $("template-used").val(tempId);

    var params = getUrlVars(); //pass along parameters so that emailer could react to them
    params['CMSID'] = tempId;
    params['GroupID'] = $("#group-id").val();
    params['RepUserID'] = $("#rep-user-id").val();
    params['noform'] = "true";
    params['-nolinks'] = "true";
    params['ContactID'] = $("#contact-id").val();
    
    var src = CONTEXT_PATH + "/crm/cms/sites/actions/newsletterpreview.jsp?" + getUrlFromVars(params);
    
    $("#ifrm").attr("src", src);
    $.ajax({
        url: CONTEXT_PATH + "/crm/email/ajax/newsletter-name.jsp",
        dataType: "json",
        data: {CMSID: tempId},
        success: function(resp) {
        	var note = false;
            $("#master-template-id").val(resp.MasterTemplateID);
            $("#email-subject").val(resp.Subject);
            if (resp.EmailTo != null && resp.EmailTo.length > 0) {
            	if (resp.EmailTo == "${RepEmail}") {
            		$("#to-fld").val($("#to-repemail").val());
            		note = true;
            	} else if (resp.EmailTo == "${MgrEmail}") {
            		$("#to-fld").val($("#to-mgremail").val());
            		note = true;
            	} else {
            		$("#to-fld").val(resp.EmailTo);
            	}
            } else {
            	$("#to-fld").val($("#to-default").val());
            }
            if (resp.EmailCC != null && resp.EmailCC.length > 0) {
            	if (resp.EmailCC == "${RepEmail}") {
            		$("#cc-fld").val($("#to-repemail").val());
            		note = true;
            	} else if (resp.EmailCC == "${MgrEmail}") {
            		$("#cc-fld").val($("#to-mgremail").val());
            		note = true;
            	} else {
            		$("#cc-fld").val(resp.EmailCC);
            	}
            } else {
            	$("#cc-fld").val($("#cc-default").val());
            }
            //if ($("#cc-fld").val() == "") $("#cc-fld").val("-");
            if (resp.EmailBCC != null && resp.EmailBCC.length > 0) {
            	if (resp.EmailBCC == "${RepEmail}") {
            		$("#bcc-fld").val($("#to-repemail").val());
            		note = true;
            	} else if (resp.EmailBCC == "${MgrEmail}") {
            		$("#bcc-fld").val($("#to-mgremail").val());
            		note = true;
            	} else {
            		$("#bcc-fld").val(resp.EmailBCC);
            	}
            } else {
            	$("#bcc-fld").val($("#bcc-default").val());
            }
            //if ($("#bcc-fld").val() == "") $("#bcc-fld").val("-");
            if (resp.EmailFrom != null && resp.EmailFrom.length > 0) {
                $("#email-from").val(resp.EmailFrom);
                //if ($("#email-from") != null) $("#email-from").val(resp.EmailFrom);
            } else {
            	//$("#email-from").val($("#reply-default").val());
            	$("#email-from").val($("#from-default").val());
            }
            if (note) {
                $("#noteaction").val("true");
            } else {
            	$("#noteaction").val($("#noteaction-default").val());
            }
            if (resp.EmailReplyTo != null && resp.EmailReplyTo.length > 0) {
                $("#email-reply-to").val(resp.EmailReplyTo);
            }	
            if (resp.CMSID != null && resp.CMSID.length > 0) {
                $("#cmsid-fld").val(resp.CMSID);
            }else{
            	$("#cmsid-fld").val("");
            }	
        }
    });
}

function initRecipientButtons() {
    enableContactSelection("#to-btn", "#to-fld");
    enableContactSelection("#cc-btn", "#cc-fld");
    enableContactSelection("#bcc-btn", "#bcc-fld");
}

function enableContactSelection(elem, targetField) {
    $(elem).click(function() {
        TARGET_FIELD = $(targetField);
        $("#dlg-search-results input[type=checkbox]").removeAttr("checked");
        $("#search-contacts-dialog").dialog("open");
        return false;
    })
}

function cleanTemplate() {
    $("#email-subject").val("");
    $("#ifrm").removeAttr("src");
}

/**
 * Populate HTMLBody and Body with the HTML and Text formats of the email content
 * respectively.
 */
function preSubmit() {
	try {
	    $("#email-html-body").val(decodeURI($("#ifrm").contents().find("#EDITOR_1_Overview").html()));
	    $("#email-body").val(decodeURI($("#ifrm").contents().find("#EDITOR_1_Overview").text()));
	} catch (err) {
	    $("#email-html-body").val($("#ifrm").contents().find("#EDITOR_1_Overview").html());
	    $("#email-body").val($("#ifrm").contents().find("#EDITOR_1_Overview").text());
	}
	if ($("#cc-fld").val() == "") $("#cc-fld").val("-");
	if ($("#bcc-fld").val() == "") $("#bcc-fld").val("-");
    var composeForm = $("#compose-form");
    composeForm.removeAttr("target");
    return true;
}

function initForm() {    
    $("#send-btn").click(function() {
        if (getEditorCount() > 0) {
        	simulateClick();
        	stopAll();
        	// need a little delay here to fix issues with IE
        	var a
        	for (var x = 0; x < 10000; ++x) a = a + 1 - a;
            if (getEditorCount() > 0) {
	        	alert("Please click out of the editor before sending this email...");
	        	return false;
            }
        }
        preSubmit();
        return true;
    });
    
    $("#save-btn").click(function() {
        if (getEditorCount() > 0) {
        	simulateClick();
        	stopAll();
        	// need a little delay here to fix issues with IE
        	var a
        	for (var x = 0; x < 10000; ++x) a = a + 1 - a;
            if (getEditorCount() > 0) {
	        	alert("Please click out of the editor before saving this email...");
	        	return false;
            }
        }
        preSubmit();
        return true;
    });       

    $("#preview-btn").click(previewClicked);
}

function previewClicked() {    
    if (getEditorCount() > 0) {
    	simulateClick();
    	stopAll();
        if (getEditorCount() > 0) {
	    	alert("Please click out of the editor before previewing this email...");
	    	return false;
        }
    }
    preSubmit();
    var composeForm = $("#compose-form");
    // open the form in a new window
    composeForm.attr("target", "_blank");
    composeForm.submit();
    return true;
}

/* END: COMPOSE BAR */

/* BEGIN: SIDE BAR */

function initSideBar() {
    $("#select-attachments-btn").click(attachButtonClicked);
    $("#attachments-fld").click(attachButtonClicked);
    enableContactSelection("#select-test-contacts", "#test-email-fld");
    initSendTestEmailField();
    initSpamCheckButton();
}

function attachButtonClicked() {    
    var src = CONTEXT_PATH + "/crm/popup/attach/selectattachfile.jsp?&NoteID=";
    src += $("#note-id").val();
    src += "&closeCallback=closeAttachmentsDialog";
    src += "&-notlightbox=true";
    //openInLightBox(src, 'AttachFile',600,500);
    //return false;
    
    $('<iframe title="Select Files" id="attach-dialog" src="' + src + '">').dialog({
        width: 701,
        height: 300,
        modal: true,
        dialogClass: 'attach-dialog',
        resizable: true
    }).width(700).height(650).css("padding", "0px");
}

function populateAttachments(attachments) {
    $("#attachments-fld").val(attachments);

}

function closeAttachmentsDialog() {
	//closeWindow('AttachFile');
    $("#attach-dialog").dialog("close");
    $("#attach-dialog").remove();
}

function initSendTestEmailField() {
    $("#test-email-fld").keypress(function(e) {        
        if (e.keyCode === 13 && this.value.indexOf("@") > 0) {
            sendTestEmail();
            e.preventDefault();
            e.stopPropagation();
            return false;
        } else if (e.keyCode === 13) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        } 	
    });
}

function sendTestEmail() {
    var url = CONTEXT_PATH + '/crm/email/actions/templateaction.jsp';
    var html = document.getElementById('email-html-body').value;

    var params = {
        TemplateID: $("#temp-combo").val(),
        actiontype: 'send',
        Subject: $('#email-subject').val(),
        FROM: $('email-from').val(),
        TO: $('#test-email-fld').val(),
        Body: html,
        ContactID: $('#contact-id').val(),
        "-action": "testsend",        
        '-mode': 'notarget'};

    var rt = $('#email-reply-to').val();
    if (rt.length > 0) {
        params['REPLY_TO'] = rt;
    }

    $.get(url, params, function(data) {
        var resp = $(data).find("response");
        if (resp != null && resp !== undefined) {
            if (resp.text() == 'Sent') {
                alert("Sent test email");
            }
        } else {
            alert("Test email failed");
        }
    }, 'xml');

    window.location.href = $("#test-action").val(); 
}

function initSpamCheckButton() {
    $("#spam-check-btn").click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        spamCheck();
    });
}

function spamCheck() {
    var params = {
        TemplateID: $("#temp-combo").val(),
        actiontype: 'send',
        Subject: $('#email-subject').val(),
        FROM: $('email-from').val(),
        TO: $("#user-email").val(),
        EDITOR: 'false',
        spamCheck: 'true'
    };

    var rt = $('#email-reply-to').val();
    if (rt.length > 0)
        params['REPLY_TO'] = rt;

    var latestReport = '';
    $('#spam-score').html('Testing...');
    $('#spam-score').fadeIn(FADE_IN_SPEED);

    $.ajax({
        url: CONTEXT_PATH + '/crm/email/articlelistedit.jsp',
        data: params,
        dataType: 'xml',
        success: function(data) {
            var xml = $(data).find("spamtest");
            if (xml != null && xml.length > 0) {
                var score = xml.attr("score");
                var result = xml.attr("isspam");
                var report = xml.find("report");
                var text = (result == 'false' ? 'Success ' : 'Failed ') + '(' + score + ')';
                latestReport = report.text().replace(/<br>/g, '\\r\\n');
                $('#spam-score').html('');
                $('<a/>').html(text).click(function () {
                    // alert(latestReport);
                    $("#message-dialog .title").text("Spam Checking Report");
                    $("#message-dialog .message").text(latestReport);
                    $("#message-dialog").dialog("open");
                })
                .attr('title', 'View Report').appendTo($('#spam-score'));
                $('#spam-score-save').val(score);
                $('#spam-report').val(latestReport);
                $('#spam-score-save').fadeIn(FADE_IN_SPEED);
                $('#spam-report').fadeIn(FADE_IN_SPEED);
            } else {
                $("#message-dialog .title").text("Spam Checking Unavailable");
                $("#message-dialog .message").text("Spam checking server is not up at the moment. Please try again later.");
                $("#message-dialog").dialog("open");
                $('#spam-score').html('(Service unavailable)');
            }
        },
        error: function(req, status, errorThrown) {
            $("#message-dialog .title").text("Spam Checking Failed");
            $("#message-dialog .message").text("Please try again later.");
            $("#message-dialog").dialog("open");
        }
    });
}

function initMessageDialog() {
    $("#message-dialog").dialog({
        autoOpen: false,
        modal: true
    });
}

/* END: SIDE BAR */