$(document).ready(function() {
	var dialogobj = $("#search-contacts-dialog");
	if(dialogobj && dialogobj.dialog){
		dialogobj.dialog({
	        width: 630,
	        height: 560,
	        modal: true,
	        autoOpen: false,
	        buttons: {
	            "Select": dialogSelectClicked,
	            "Search": dialogSearchClicked,
	            "Cancel": dialogCancelClicked
	        }
	    });
	}
});

function dialogSearchClicked() {
    var actionUrl = CONTEXT_PATH + "/crm/actions/contactaction.jsp";
    var resultUrl = CONTEXT_PATH + "/crm/email/ajax/contact-search.jsp";
    var params = $("#dlg-form").serialize();
    $.ajax({
        url: actionUrl,
        data: params,
        dataType: "text",
        type: "POST",
        success: function(data) {
            $("#dlg-search-results").fadeOut(250);
            $.ajax({
                url: resultUrl,
                dataType: "text",
                type: "GET",
                success: function(results) {
                    $("#dlg-search-results > tr").remove();
                    $("#dlg-search-results").append(results);
                    $("#dlg-search-results").fadeIn(250);
                }
            });
        }
    });
    return false;
}

function dialogCancelClicked() {
    $("#search-contacts-dialog").dialog("close");
}

function dialogSelectClicked() {
    var selectedContacts = $("#dlg-search-results input[type=checkbox]:checked");
    var emails = "";
    for (var i = 0; i < selectedContacts.length; i++) {
        var email = selectedContacts[i].value;
        if (email.trim().length > 0) {
            emails += email + ", ";
        }
    }
    $(TARGET_FIELD).val($(TARGET_FIELD).val() + emails);
    $("#search-contacts-dialog").dialog("close");
}