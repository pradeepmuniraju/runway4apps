
function MultiSelector( list_target, max ){

	// table to write the list
	this.table = document.createElement('table');
	this.tbody = document.createElement('tbody');
	this.table.appendChild(this.tbody);

	list_target.appendChild(this.table);
	
	// How many elements?
	this.count = 0;
	// How many elements?
	this.id = 0;
	// Is there a maximum?
	if( max ){
		this.max = max;
	} else {
		this.max = -1;
	};
	
	/**
	 * Add a new file input element
	 */
	this.addElement = function( fileinput ){

		// Make sure it's a file input element
		if( fileinput.tagName == 'INPUT' && fileinput.type == 'file' ){

			// Element name -- what number am I?
			fileinput.name = 'file_' + this.id++;

			// Add reference to this object
			fileinput.multi_selector = this;

			// What to do when a file is selected
			fileinput.onchange = function(){

				// New file input
				var new_element = document.createElement( 'input' );
				new_element.type = 'file';

				// Add new element
				this.parentNode.insertBefore( new_element, this );

				// Apply 'update' to element
				this.multi_selector.addElement( new_element );

				// Update list
				this.multi_selector.addListRow( this );

				// Hide this
				this.style.display = 'none';

			};
			// If we've reached maximum number, disable input element
			if( this.max != -1 && this.count >= this.max ){
				fileinput.disabled = true;
			};

			// File element counter
			this.count++;
			// Most recent element
			this.current_element = fileinput;
			
		} else {
			// This can only be applied to file input elements!
			alert( 'Error: not a file input element' );
		};

	};

	/**
	 * Add a new row to the list of files
	 */
	this.addListRow = function( fileinput ){

		// Row 
		var new_row = document.createElement('tr');

		// Delete button
		var new_row_button = document.createElement( 'input' );
		new_row_button.type = 'button';
		new_row_button.value = 'Delete';

		// References
		//new_row.element = element;

		// Delete function
		new_row_button.onclick= function(){

			//alert(this.form.elements.length);
			// Remove element from form
			//this.form.removeChild( this );

			// Remove this row from the table
			this.parentNode.parentNode.parentNode.removeChild( this.parentNode.parentNode  );

			// Decrement counter
			this.parentNode.element.multi_selector.count--;

			// Re-enable input element (if it's disabled)
			this.parentNode.element.multi_selector.current_element.disabled = false;

			// Appease Safari
			//    without it Safari wants to reload the browser window
			//    which nixes your already queued uploads
			return false;
		};

		var new_cell = document.createElement("td");

		// Set row value
		new_cell.innerHTML = fileinput.value;
		new_row.appendChild(new_cell);
		
		// Add button
		new_cell = document.createElement("td");
		new_cell.appendChild( new_row_button );
		new_row.appendChild(new_cell);

		// Add it to the list
		this.tbody.appendChild(new_row);
		
	};

};