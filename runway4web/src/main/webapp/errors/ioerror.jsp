<%@ page import="com.sok.framework.*,com.sok.runway.*" %>
<%@ page import="java.io.*, javax.servlet.ServletException" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String contextPath = request.getContextPath();
	
	CharArrayWriter cl = new CharArrayWriter();
	CharArrayWriter c = new CharArrayWriter();
	PrintWriter p = new PrintWriter(c);
	PrintWriter pl = new PrintWriter(cl);
	java.util.Properties sysprops = InitServlet.getSystemParams();
	if(exception != null) {
		p.write(exception.toString()); 
		exception.printStackTrace(pl);
		
		Throwable cause = exception.getCause();
		
		if (cause != null) {
			p.write("\r\nCaused By: ");
			p.write(cause.toString()); 

			pl.write("\r\n\r\nCaused By:\r\n\r\n");
			cause.printStackTrace(pl);
		}
		
		if(exception instanceof ServletException) { 
			ServletException se = (ServletException)exception; 

			cause = se.getRootCause(); 

			if (cause != null) {
			   p.write("\r\n\r\nRoot Cause: ");	
			   p.write(cause.toString()); 

			   pl.write("\r\n\r\nRoot Cause:\r\n\r\n");
			   cause.printStackTrace(pl);
			}
		} 

	} else {
		p.write("Exception was null");
	}

	out.print("<!--");
	out.print(cl.toString());
	out.print("-->");

	MailerBean mailer = new MailerBean();
	String defaultFrom = (sysprops != null)? sysprops.getProperty("DefaultEmail") : null;
   if (defaultFrom == null || defaultFrom.length()==0) {
	  defaultFrom = "Runway<info@switched-on.com.au>";
   }
   StringBuffer info = new StringBuffer("User: ").append(request.getHeader("User-Agent")+"\r\n"); 
   info.append("\r\nReferrer : ").append(request.getHeader("Referer")); 
	info.append("\r\nURI : ").append(request.getRequestURI()); 
   if (request.getQueryString() != null) {
      info.append("\r\nURL : ").append(request.getRequestURL()).append("?").append(request.getQueryString()); 
   }
   info.append("\r\nIPAddress :").append(request.getRemoteAddr());
   
	RowBean bean = new RowBean(); 
	bean.parseRequest(request); 

	info.append("\r\n\r\nRequest Contents : \r\n").append(StringUtil.replace(bean.toString(),"&","\r\n&")); 

   	UserBean currentuser = (UserBean)session.getAttribute("currentuser");

	info.append("\r\n"); 

	if(currentuser!=null) { 

		info.append("\r\nUser :").append(currentuser.getString("Username")); 
	} else { 
		info.append("\r\nUser was null \r\n"); 
	} 

	info.append("\r\nException : ").append(c.toString()); 

	info.append("\r\n\r\nStack Trace: \r\n").append(cl.toString()); 

	mailer.setFrom(defaultFrom);
	mailer.setTo("support@switched-on.com.au");
	mailer.setHost(pageContext.getServletContext().getInitParameter("SMTPHost"));
	mailer.setUsername(pageContext.getServletContext().getInitParameter("SMTPUsername"));
	mailer.setPassword(pageContext.getServletContext().getInitParameter("SMTPPassword"));
	mailer.setPort(pageContext.getServletContext().getInitParameter("SMTPPort"));
	mailer.setTextbody(info.toString());

	

	mailer.setSubject(pageContext.getServletContext().getServletContextName()+ " : Runway Error Follows");
	//mailer.getResponse();
%>
<html>
<head>
<title>Runway Request Error</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= contextPath %>/styles/main.css" rel="stylesheet" type="text/css">
<link href="<%= contextPath %>/styles/master.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
-->
</style>
</head>
<!-- Navigation Image Maps -->
<body leftmargin="20" topmargin="20" marginwidth="20" marginheight="20">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td width="100%">
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EDEFF1">
  <tr> 
    <td width="20"><img src="<%= contextPath %>/images/error_topleft.gif" width="20" height="25"></img></td>
    <td width="100%" class="errorbgtop">&nbsp;</td>
    <td width="20"><img src="<%= contextPath %>/images/error_topright.gif" width="20" height="25"></img></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td valign="top">
		<table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
        <tr class="listrowtitlestyle"> 
          <td><div align="left"><strong><font size="3">Request Error</font></strong></div></td>
        </tr>
		<tr class="listrowtitlestyle"> 
        <td><img src="<%= contextPath %>/images/divider_top.gif" width="100%" height="2"></img></td>
		</tr>
      </table>
      <br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="bodytypestyle"><p>This request could not be processed.<br><p><%=StringUtil.replace(info.toString(),"\r\n","<br>")%></p>
       	</td>
        </tr>
      </table>
      <p><br>
      </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" align="left"><img src="<%= contextPath %>/images/pixel.gif" width="100%" height="1"></img></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
   	<td width="20" height="21" nowrap><img src="<%= contextPath %>/images/footer_left.gif" width="20" height="21" border="0"></img></td>
   	<td width="100%" height="21" valign="bottom" background="<%= contextPath %>/images/footer_center.gif" nowrap><img src="<%= contextPath %>/images/footer_center.gif" border="0"></img></td>
   	<td width="240" height="21" align="right" nowrap><img src="<%= contextPath %>/images/footer_right.gif" width="240" height="21"></img></td>
   </tr>
   <tr class="textVersion">
   	<td width="20" height="25"></td>
   	<td width="100%" height="25" valign="top" align="left"><jsp:include page="/sysadmin/version.txt" flush="false"/>&nbsp;<%= StringUtil.getFileDate(request.getRealPath("/sysadmin/version.txt")) %></td>
   	<td width="240" height="25" valign="top" align="right">&nbsp;</td>
   </tr>
</table>
</td></tr>
</table>
  <br>

</body>
</html>
