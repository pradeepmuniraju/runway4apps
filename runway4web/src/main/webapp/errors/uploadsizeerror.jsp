<%@ page import="com.sok.framework.*,com.sok.runway.*" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	String contextpath = request.getContextPath();
%>
<html>
<head>
<title>Runway Upload Size Error</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= contextpath %>/styles/main.css" rel="stylesheet" type="text/css">
<link href="<%= contextpath %>/styles/master.css" rel="stylesheet" type="text/css">
</head>
<!-- Navigation Image Maps -->
<body leftmargin="20" topmargin="20" marginwidth="20" marginheight="20">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td width="100%">
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EDEFF1">
  <tr> 
    <td width="20"><img src="<%= contextpath %>/images/error_topleft.gif" width="20" height="25"></img></td>
    <td width="100%" class="errorbgtop">&nbsp;</td>
    <td width="20"><img src="<%= contextpath %>/images/error_topright.gif" width="20" height="25"></img></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td valign="top">
		<table width="100%" height="30" border="0" cellpadding="0" cellspacing="0">
        <tr class="listrowtitlestyle"> 
          <td><div align="left"><strong><font size="3">Request Error</font></strong></div></td>
        </tr>
		<tr class="listrowtitlestyle"> 
        <td><img src="<%= contextpath %>/images/divider_top.gif" width="100%" height="2"></img></td>
		</tr>
      </table>
      <br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="bodytypestyle"><p>Uploaded file exceeded maximum size.<br>
       	</td>
        </tr>
      </table>
      <p><br>
      </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" align="left"><img src="<%= contextpath %>/images/pixel.gif" width="100%" height="1"></img></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
   	<td width="20" height="21" nowrap><img src="<%= contextpath %>/images/footer_left.gif" width="20" height="21" border="0"></img></td>
   	<td width="100%" height="21" valign="bottom" background="<%= contextpath %>/images/footer_center.gif" nowrap><img src="<%= contextpath %>/images/footer_center.gif" border="0"></img></td>
   	<td width="240" height="21" align="right" nowrap><img src="<%= contextpath %>/images/footer_right.gif" width="240" height="21"></img></td>
   </tr>
   <tr class="textVersion">
   	<td width="20" height="25"></td>
   	<td width="100%" height="25" valign="top" align="left"><jsp:include page="/sysadmin/version.txt" flush="false"/>&nbsp;<%= StringUtil.getFileDate(request.getRealPath("/sysadmin/version.txt")) %></td>
   	<td width="240" height="25" valign="top" align="right">&nbsp;</td>
   </tr>
</table>
</td></tr>
</table>
  <br>

</body>
</html>
