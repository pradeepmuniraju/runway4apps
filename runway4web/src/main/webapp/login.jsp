<%@ page import="com.sok.framework.*,com.sok.runway.*, org.apache.commons.lang.StringUtils, java.util.*" %>
<%@ taglib uri="/WEB-INF/sok-taglib.tld" prefix="sok" %>
<%@ page session="false" %><%!

	String processTarget(HttpServletRequest request, String target)
	{
   		String processedtarget = target;
   		if(processedtarget!=null)
   		{
			if(request.isSecure())
			{
			   if(processedtarget.indexOf("https")!=0){
			      processedtarget = StringUtil.replace(processedtarget, "http", "https");
			   }
			}	
			if(processedtarget.indexOf(".list?-action=search")>0)
			{
			   processedtarget = null; //search results will not work
			}
			else if(processedtarget.indexOf("list.jsp?-action=search")>0)
			{
			   processedtarget = null; 
			}			
			else if(processedtarget.indexOf("/admin/import/")>0)
			{
			   processedtarget = null; 
			}
   		}
		return(processedtarget);
	}

%><%
	boolean locations = "true".equals(InitServlet.getSystemParam("LoginCheckLocations"));
	
    String remoteIP = request.getRemoteAddr();
	if (remoteIP.startsWith("150.70.") || remoteIP.startsWith("216.104.")) {
		out.println("You are not authorised to use this system.");
		System.out.println("Connection from banned IP " + remoteIP);
	} else if(false && !request.isSecure() && !remoteIP.startsWith("127.0.0.")) { 
		String target = request.getParameter("-target");
		target = processTarget(request, target);
		String url = "https://" + request.getServerName()  +request.getContextPath() +"/runway.login";
		if (StringUtils.isNotBlank(target)) url += "?-target=" + StringUtil.urlEncode(target);
		response.sendRedirect(url);	
    	%><html>
    	   <head><title>Runway</title>
    	   <META HTTP-EQUIV=REFRESH CONTENT="0; URL=<%= url %>">
    	</head>
    	<body>Please <a href="<%= url %>">click here</a> if you are not forwarded automatically.</body>
    	</html>
    	<%
    	return; 
	} else {
%>
<sok:include page="/specific/login.jsp" ifExists="true" >
<sok:includeNotFound>
<%

	String contextpath = request.getContextPath();
	
	String retrievePassword = request.getParameter("-retrievepassword");		
	String target = null;
	String username = request.getParameter("Username");
	String action = request.getParameter("-action");
	String login = null;
	
	int disabledIndex = -1;
	String[] disableMessage = {"Runway is currently being upgraded.<br>Please try again in 30 minutes.",
	                           "Runway has been disabled.<br>Please contact switched on knowledge for resolution.",
	                           "Runway is temporarily unavailable. We apologise for any inconvenience."};
	
	String messageText = request.getParameter("-messageText");
		
	if(action!=null)
	{
		target = request.getParameter("-target");
		target = processTarget(request, target);
		if(action.equals("logout"))
		{
			login = "logout";
		}
		else if(action.equals("expired"))
		{
			login = "expired";
		}
	}

	if(username==null)
	{
		username = "";
	}
	
	ServletContext context = getServletContext();
	String allowIP1 = context.getInitParameter("allowIP1");
	String allowIP2 = context.getInitParameter("allowIP2");
	String allowIP3 = context.getInitParameter("allowIP3");
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link href="<%=contextpath%>/styles/main.css" rel="stylesheet" type="text/css">
<link href="<%= contextpath %>/styles/master.css" rel="stylesheet" type="text/css">
<link href="<%= contextpath %>/styles/runway.css" rel="stylesheet" type="text/css">
<title>Login</title>
<script type="text/javascript" src="<%= contextpath %>/javascripts/google/jquery-1.6.2.min.js" ></script>
<script language="JavaScript1.1" src="<%= contextpath %>/javascripts/submittrap.js" type="text/javascript"></script>
<script language="JavaScript1.2" type="text/javascript"><!--
var autosubmitformname = "loginForm";
function doLogin()
{
	//alert("doLogin");
	var jsonResponseValue = "true";
	if(document.loginForm.Username.value=="")
	{
		alert("You must supply a Username");
		document.loginForm.Username.focus();
		return(false);
	}

	if(document.loginForm.Password.value=="")
	{
		alert("You must supply a Password");
		document.loginForm.Password.focus();
		return(false);
	}

	<% if (locations) { %>
	 $.ajax({url: "<%=contextpath%>/actions/retriveuserlocations.jsp", success: function(data) {
    	 var jsonResponse = data, locations = jsonResponse.locations;   
		 if(locations.length == 1){
	    	document.getElementById("-locationidhidden").value=locations[0].contactlocationid;
	     }else if(locations.length > 1){
	        document.getElementById("locationTR").style.display="";
	        if(document.getElementById("locations").value == ""){
	        	alert("Please specify location");
	        	jsonResponseValue = "false";
       		}else{
       			jsonResponseValue = "true";
       		}
	     }
     }, dataType: "json", async: false, data: "Username=" + encodeURIComponent(document.getElementById("Username").value) + "&Password=" + encodeURIComponent(document.getElementById("Password").value) });	
	 if(jsonResponseValue == "true"){
		 return(notSubmitted());
	 }
	 <% } else { %>
	 return(notSubmitted());
	 <% } %>
}

function submitlogin()
{
	if(doLogin())
	{
		document.loginForm.submit();
	}
}

function checkForInput()
{
   if(document.formRetrievePassword.retrieveInput.value=="")
	{
		alert("You must supply your Email or Username");
		document.formRetrievePassword.retrieveInput.focus();
		return(false);
	}
	var enteredInputValue = document.formRetrievePassword.retrieveInput.value;
	
	if (echeck(enteredInputValue)==false){
		enteredInputValue="";
		document.formRetrievePassword.retrieveInput.focus();
		return (false);
	}
	return (true);
}
function submitRetrievePassword()
{
   if(checkForInput())
	{
		document.formRetrievePassword.submit();
	}
}

function echeck(str) 
{

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)

   if(str.indexOf(at) > 0)
   {		
   		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
   		   alert("Invalid email address")
   		   return false
   		}
   
   		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
   		    alert("Supply a valid email address")
   		    return false
   		}
   
   		 if (str.indexOf(at,(lat+1))!=-1){
   		    alert("Supply a valid email address")
   		    return false
   		 }
   		
   		 if (str.indexOf(" ")!=-1){
   		    alert("Supply a valid email address")
   		    return false
   		 }
    }
 	 return true					
}

function checkFrame()
{
	if(parent && parent.location != window.location)
	{
		parent.location = '<%=contextpath%>/login.jsp';	
	}
}

function changeFocus(field)
{
	var temp = document.getElementById(field);
	temp.focus();
}

function submitEnter()
{
	if (window.event.keyCode == 13) {
		submitlogin();
	}
	
	else
		return true;
}

checkFrame();

// -->
</script>

<!-- IE PNG Fix -->
<!--[if IE 6]>
<style type="text/css">
	img, div { behavior: url(<%= contextpath %>/images/png_fix_IE/iepngfix.htc) }
</style> 
<![endif]-->

</head>
<body>
<div id="loginmain">
  <table border="0" cellspacing="0" cellpadding="0" width="428">
    <tr> 
		<td align="left"><img src="<%= contextpath %>/specific/images/logo.gif" > <%-- <img src="<%= contextpath %>/images/login/logo.jpg" width="150" height="100">--%></td>
		<td align="right">
		<%if(RunwayUtil.isAllowIP(request.getRemoteAddr()) && !"10.144.1.254".equals(request.getRemoteAddr())) {%>
					<br/> <a href="<%=contextpath%>/sysadmin/userlogin.jsp<%=StringUtils.isNotBlank(request.getParameter("-target")) ? ("?-target=" + StringUtil.urlEncode(request.getParameter("-target"))) : "" %>">User Login Screen</a>
		<%}%>&nbsp;
		</td>
        <td align="right"><%-- <img src="<%= contextpath %>/specific/images/logo.gif" >--%>&nbsp;<!--<sok:include page="/specific/includes/login_upper_info.jsp" ifExists="true" />--></td>
    </tr>

      

  </table>
	 
	 	<div id="logindiv">

			  <table class="loginfields" bgcolor="#F5F5F5">
			  	<tr>
					<td colspan="3" class="login_top_shade" style="padding: 0;"> </td>
				</tr>
				<tr> 
				  <td class="login_l_shade"> </td>
				  <td class="login_background" align="left">
				  
				  	<% if (disabledIndex == -1) { %>
						<form name="loginForm" action="<%= contextpath %>/actions/loginaction.jsp" method="post" onsubmit="return doLogin()">
									<input type="hidden" name="-action" value="login">
									<input type="hidden" name="-locationidhidden" id="-locationidhidden" value=""/>
									<% if (target != null) { %>
									<input type="hidden" name="-target" value="<%=target%>">
									<% } %><% } %>
					<table border="0" cellspacing="0" cellpadding="2" align="center">		
								   <% if (disabledIndex > -1) { %>
										<tr>
											<td colspan="3" align="left"><font size="2"><%=disableMessage[disabledIndex]%></font></td>
										</tr>
										</table>
									<% } 
									   else { %>		
										<% if(action!=null && action.equals(UserBean.loginfailed)) { %>
											<tr>
												<td colspan="3"><font size="1">You have entered your Username or Password incorrectly.</font></td>
											</tr>
											<tr>
												<td colspan="3" align="right" class="warning"><font size="1">Please try again.</font></td>
											</tr>
										<% } else if(login!=null && login.equals("logout")) { %>
											<tr>
												<td colspan="3"><font size="2">You are now logged out.</font></td>
											</tr>			
										<% } else if(login!=null && login.equals("expired")) { %>
											<tr>
												<td colspan="3"><font size="2">Your session has expired.</font></td>
											</tr>							
										<% } else if(action!=null && action.equals("3")) { %>
											<tr>
												<td colspan="3"><font size="2">Your are not able to login at this time, please try later.</font></td>
											</tr>							
										<% } %>
										<tr>
										   <td colspan="4">
											   <% if (messageText !=null){ %>
													 <div style="color: red; font-size: 11px;"><%= messageText %></div>							      
											   <% } %> 
										   </td>
										</tr>
										
										<tr>
											<td><font size="2">Username</font></td>
											<td></td>
											<td align="right"><input type="text" name="Username" id="Username" value="<%= username %>" class="formFieldLogin" onBlur="focussed = false;" onkeydown="return autosubmit(event)"></td>
										</tr>
										<tr>
											<td><font size="2">Password</font></td>
											<td></td>
											<td align="right"><input type="password" name="Password" id="Password" value="" class="formFieldLogin" onBlur="focussed = false;" onkeydown="return autosubmit(event)" onchange="javascript:retriveUserLocations();"></td>
										</tr>
										<tr id="locationTR" style="display:none;">
											<td><font size="2">Location</font></td>
											<td></td>
											<td align="left">&nbsp;<select id="locations" name="-locationid" value="" onkeydown="return autosubmit(event)">
												<option value="">&nbsp;</option>
											</select>
											</td>
										</tr>
										<tr>
											<td colspan="3" align="right" style="cursor:pointer">
												<img src="<%= contextpath %>/images/login/bttn_login.jpg" onclick="submitlogin()" title="Login">
											</td>
										</tr>
										</table>
										<% } %>
										
								<script language="javascript1.2" type="text/javascript">
									document.loginForm.Username.focus();
								</script>										
								</form>	
							  

						<!-- Retrieve Password submit boxes -->
						  <% if (retrievePassword != null && retrievePassword.equals("true")){ %>
								<form action="<%= contextpath %>/actions/retrievePassword.jsp" style="display:inline;" name="formRetrievePassword" onsubmit="return checkForInput()">
									<table bgcolor="#F5F5F5" align="center" width="69%">	
									  <tr>
											<td class="retrievePasswordForm" colspan="3">Please enter your email address or username</td>
									  </tr>												   						      						      							      
									  <tr>
									  	<td>&nbsp;</td>
										 <td colspan="2" align="right"><input id="retrieveInputField" type="text" name="retrieveInput" value="" class="formFieldLogin" onBlur="focussed = false;">
										 </td>	    
									  </tr>					      
									  <tr>
									  	<td colspan="2">&nbsp;</td>
									  	<td align="right" style="cursor:pointer" ><img src="<%= contextpath %>/images/login/bttn_submit.jpg" title="Retrieve Password" onclick="submitRetrievePassword()"></td>
									  </tr>
									 </table> 
									</form>		
					</td>				
					<script language="javascript1.2" type="text/javascript">
						changeFocus('retrieveInputField');
					</script>
					<% } %>					
					<td class="login_r_shade"></td>
				</tr>						  
			  </table>		  
			
			</div> <!-- loginfields div -->
	
		
		
		<% if (retrievePassword == null){ 
				%>
				<div id="retrievePasswordLink">		
				<br />
				<a href="<%=contextpath %>/runway.login?-retrievepassword=true" title="Retrieve your password if you have forgotten" >Retrieve Password?</a>
				</div>
		<% } else { %>
		
			<div id="retrievePasswordLink">								
						</div>			
		
		<% } %>
				
							

			
					 

 </div> <!-- parent div -->
  
  
<sok:include page="/specific/includes/login_lower_info.jsp" ifExists="true" />

<script language="Javascript1.1" type="text/javascript"><!--

function autosubmit(evnt)
{

	if(navigator.appName == "Microsoft Internet Explorer")
	{
		if (document.loginForm.Password.value!="" && (event.keyCode == 13 || event.keyCode == 10) && document.loginForm.Username.value!="" )
		{
			if(doLogin()){
				document.loginForm.submit();
			}
		}
		else if(event.keyCode == 13 || event.keyCode == 10)
		{
			setTimeout("doLogin()",200);		
		}
	}
	else
	{			
		
		if (document.loginForm.Password.value!="" && (evnt.which == 13 || evnt.which == 10) && document.loginForm.Username.value!="")
		{	
			if(doLogin()){
				document.loginForm.submit();
			}
		}
		else if(evnt.which == 13 || evnt.which == 10)
		{
			setTimeout("doLogin()",200);			
		}

	}

	return true;
}

function retriveUserLocations(){
	<% if (locations) { %>
    var jsonResponse = {};
    $.ajax({url: "<%=contextpath%>/actions/retriveuserlocations.jsp", success: function(data) {
    	jsonResponse = data;
        var locations = jsonResponse.locations || [];
        for (var i = 0; i < locations.length; i++) {
            var option = "<option value='" + locations[i].contactlocationid + "'>" + locations[i].regionname + " - " + locations[i].locationname + "</option>";
            $("#locations").append(option);
        }
        if(locations.length == 1){
        	document.getElementById("-locationidhidden").value=locations[0].contactlocationid;
        }else if(locations.length > 1){
            document.getElementById("locationTR").style.display="";
        }
    }, dataType: "json", async: false, data: "Username=" + encodeURIComponent(document.getElementById("Username").value) + "&Password=" + encodeURIComponent(document.getElementById("Password").value) });
    <% } %>
}

if (window.location.pathname.indexOf("runway.login") == -1 && window.location.pathname.indexOf("login.jsp") == -1) {
	window.location = "<%= contextpath %>/runway.login";
}

-->
</script>
<!-- 
<% out.print(request.getRemoteAddr()); %>  
<% out.print(InitServlet.getSystemParams().get("AllowIPs")); %>
-->
</body>
</html>

   </sok:includeNotFound>
</sok:include>
<% } %>
